#!/usr/bin/env python3
import itertools

import glob
import io
import os
import datetime
import calendar
import re
import subprocess
import sys
import atomicwrites
import yaml
from tqdm import tqdm
from atomicwrites import atomic_write
import json
from pathlib import Path
from contextlib import contextmanager
import argparse
import rich.console
import textwrap
console = rich.console.Console(highlight=False)
print = console.print

parser = argparse.ArgumentParser(description='Update the whisper wiki.')
parser.add_argument('--no-download', action='store_false', help='do not download new videos', dest='download')
parser.add_argument('--no-playlists', action='store_false', help='do not download playlists', dest='playlists')
parser.add_argument('--no-transcribe', action='store_false', help='do not transcribe new videos', dest='transcribe')
parser.add_argument('--no-update', action='store_false', help='do not update the local copy of wiki markdown files', dest='update')
parser.add_argument('--no-upload', action='store_false', help='do not upload the changes', dest='upload')
parser.add_argument('--summarize', action='store_true', help='enable openai-based summarize', dest='summarize')
parser.add_argument('--no-update-write', action='store_false', help='do not write changes made in update step (but run the code as though we were going to write it)', dest='update_write')

def dedent(x):
    return textwrap.dedent(x).strip()

@contextmanager
def pushd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def step__check_yt_dlp():
    try:
        subprocess.run(['pip', 'install', '--upgrade', 'yt-dlp'], check=True)
    except FileNotFoundError:
        print("ERROR: need yt-dlp. please download it and put it on your PATH.")
        print("if you're on mac or linux and have python, you can do this with:")
        print("\n    pip install --user yt-dlp\n")
        print("if you're on windows, then presumably you ran this with mingw, so put yt-dlp.exe")
        print("somewhere in your mingw path.")
        sys.exit(1)

def filter_keys(obj):
    def allowed_key(x):
        return ("__" not in x
            and x not in ["_version", "availability", "thumbnails", "ie_key"])
    if type(obj) == dict:
        if 'entries' in obj:
            obj = dict(obj, entries=sorted(obj['entries'], key=lambda x: x['title']))
        return {x: filter_keys(y) for x, y in obj.items() if allowed_key(x) and y is not None}
    elif type(obj) in [list, tuple]:
        return [filter_keys(x) for x in obj]
    else:
        return obj

def visit_vids(obj, vidgetter=(lambda x: x), types=('videos', 'streams', 'shorts')):
    if not obj: return []
    def visit_items(obj):
        return list(itertools.chain.from_iterable(visit_vids(x, vidgetter, types) for x in obj['entries']))
    if obj['_type'] == 'playlist' and any(x['_type'] == 'playlist' for x in obj['entries']):
        return visit_items(obj)
    elif obj['_type'] == 'playlist' and obj.get('webpage_url_basename', os.path.basename(obj['webpage_url'])) in types:
        return visit_items(obj)
    elif obj['_type'] == 'playlist' and 'videos' in types and obj.get('original_url') == obj.get('channel_url'):
        return visit_items(obj)
    elif obj['_type'] == 'url':
        return [vidgetter(obj)]
    return []

def step__download_channel(chans, channel):
    chan_info = json.loads(subprocess.check_output(["yt-dlp", channel, "-sJ", "--flat-playlist"]).decode("utf-8", 'replace'))
    chans[chan_info['id']] = filter_keys(chan_info)
    with atomic_write('videos.json', overwrite=True) as f:
        f.write(json.dumps(chans, indent=1, sort_keys=True))

    with pushd("audio"):
        subprocess.run(['yt-dlp', '-f', 'ba', '-x', '--audio-format', 'vorbis', '--force-write-archive', '--download-archive', 'archive_list_audio.txt', '--windows-filenames', '--restrict-filenames', '--no-lazy-playlist', '--no-break-on-existing', '-o', '%(upload_date)s_%(id)s_%(title)s.%(ext)s', f'{channel}/videos'])
        print("Done downloading audio")
    with pushd("playlists"):
        print(f"Downloading list of playlists for {channel}...")
        print("Done downloading list of playlists")

def filename_date(filename):
    return re.sub(r'^(....)(..)(..)_.*$', r'\1/\2', filename)

def not_a_date(filename):
    return not re.match(r'^\d{8}_.*$', os.path.basename(filename))

def yearmonth_layout(directory):
    for filename in Path(directory).glob('*.{txt,lrc,ogg}'):
        if not_a_date(filename) or not filename.exists():
            print(f"skip, no file {filename}")
            continue

        filename_date_str = filename_date(filename.name)
        new_directory = Path(directory) / filename_date_str
        new_directory.mkdir(parents=True, exist_ok=True)
        filename.replace(new_directory / filename.name)


openai_client = None


NO_FMTS = ["{}"]
FMTS = ["{}", " {}"]
ALLCASES = [str.lower, str.capitalize]
IDENT = [lambda x: x]
filters = [
    # words that are maybe allowed if they're really necessary but should be strongly avoided
    (ALLCASES, FMTS, -10, [
        "highlight", "highlights", "highlighting", "highlighted",
        "emphasize", "emphasizes", "emphasizing",
        "conversation", "conversations",
        "complexity", "complexities",
        "discusses", "discussions",
        "evaluate", "evaluates",
        "critique", "critiques",
        "discuss", "discussion",
        "explore", "explores",
        "promote", "promotes",
        "reflect", "reflects",
        "assess", "reassess",
        "review", "reviews",
        "delve", "delves",
        "spotlighting",
        "investigate",
        "opportunity",
        "excellent",
        "essential",
        "important",
        "dialogue",
        "language",
        "remember",
        "consider",
        "consider",
        "educate",
        "explore",
        "crucial",
        "engage",
        "forget",
        "would",
        "check",
        "model",
        "feel",
        "...",
        ("examine", True),
        ("align", True),
        ("aligned", True),
        #'"',
        #"#"
    ]),

    # words that are allowed in summaries but should be avoided in favor of softer words
    (ALLCASES, FMTS, -1, [
        "responsibility",
        "individual",
        "personal",
        "market",
        "racism",
        "racist",
        "self",
        "racists",
        "pandemic",
    ])
]
import functools
from collections import Counter

tokenizer_ = None
def tokenizer():
    global tokenizer_
    if tokenizer_ is None:
        import tiktoken
        tokenizer_ = tiktoken.encoding_for_model("gpt-4")
    return tokenizer_

logit_bias_ = None
def LOGIT_BIAS():
    global logit_bias_
    if logit_bias_ is None:
        bias = {}
        print("LOGIT_BIAS:")
        for caps, fmts, boost, words in filters:
            for word in words:
                skip_multi = False
                if type(word) == tuple:
                    word, skip_multi = word
                for cap in caps:
                    capitalized = cap(word)
                    c = tokenizer().encode(capitalized)
                    for fmt in fmts:
                        adjusted = fmt.format(capitalized)
                        w = tokenizer().encode(adjusted)
                        j = tokenizer().encode(fmt.format(""))
                        if not (set(w) - (set(c) | set(j))) and j:
                            #print(f"skip [white on black]{word} as {fmt}[/white on black] because [blue]{w}[/blue] - [green]{w}[/green]|[yellow]{j}[/yellow] = [red]{(set(w) - (set(c) | set(j)))}[/red]")
                            continue
                        thisboost = boost
                        s = tokenizer().decode([w[0]])
                        if len(w) > 1 and adjusted.lower().strip() == word:
                            print(f"[black on white]{adjusted}[/black on white] is >1 token:", (", ").join(f"[black on white]{tokenizer().decode([x])}[/black on white]" for x in w), w)
                            if (len(s) < 4 and not s.startswith(" ")) or skip_multi:
                                continue
                            thisboost = thisboost / 10
                        if thisboost < -50:
                            boost_style = "red"
                        elif thisboost < -8:
                            boost_style = "bright yellow"
                        elif thisboost < 0:
                            boost_style = "yellow"
                        elif thisboost < 5:
                            boost_style = "green"
                        else:
                            boost_style = "bright green"
                        print(f"[{boost_style}]{thisboost}[/{boost_style}] {word} -> [black on {boost_style}]{s}[/black on {boost_style}]")
                        bias[w[0]] = min(bias.get(w[0], thisboost), thisboost)
        logit_bias_ = bias
    return logit_bias_


def logit_bias_for_transcript(transcript):
    logit_bias = dict(LOGIT_BIAS())
    import re
    #r = re.sub("#.*$", "", yamlformat, flags=re.MULTILINE)
    tokens = tokenizer().encode(transcript)
    len(tokens)
    for t, x, y in sorted([(x, tokenizer().decode([x]), y) for x, y in Counter(tokens).items()], key=lambda x: -1.4**len(x[1]) + x[2] - x[0]/1000):
        if not x.startswith(" "): continue
        if x.lower().strip() in ['this', 'that', 'there', 'those', 'with']: continue
        if not len(x.strip()): continue
        if len(x.strip()) < 4: continue
        if len(logit_bias) > 299: break
        if t in logit_bias: continue
        logit_bias[t] = 0.5
        #print(f"[black on red]{repr(x)}[/black on red]: {y}")
    return logit_bias

costs = []

def summarize(transcript, author, model="gpt-3.5-turbo-0125"):
    global openai_client
    from openai import OpenAI
    if openai_client is None:
        openai_client = OpenAI()
    logit_bias = logit_bias_for_transcript(transcript)
    enct = tokenizer().encode(transcript)
    print(f"transcript len: {len(transcript)} chars, {len(enct)} tokens")
    tokenlimit = min(max(768, int((len(enct)**0.5)*20)), 1536)
    res2 = openai_client.chat.completions.create(
        messages=[
            {
                "role": "system",
                "content": dedent("""
                    Include any deadlines or dates.

                    NO "REMEMBER,"
                    NO SPOTLIGHT METAPHORS.
                    NO PRO-POLICE ACTIONS. We stan community policing, not police. Remember the victims of police.

                    Let’s get a quick cheat sheet together. Serve up the the transcript like this:
                    ```
                    # Bits

                    <Name of speaker> says:

                    - Give an overview of the moments in the transcript.
                    - Use the original examples rather than generated abstract summaries.
                    - Prefer extraction over summary.
                    - Show don't tell: what happened? Why? The who, what, when, where, why, emotions, and dynamics.
                    - Ten to fifteen short and precise nuggets.
                    - Keep the flow of the point.
                    - Up to fifteen bullet points:
                    - ...
                    - ...
                    - ...
                    - ...
                    - ...
                    - ...
                    - ...

                    # Quotes

                    Provide the most memorable slogan-worthy quotes from the text to remember and share. Max five.

                    # Oneliner

                    Copy the most important Bits line, usually the first one, plus summary of the others. Ideally should cover beginning, middle, end, and MUST cover core takeaway. SHOULD include every point from Bits. THIS SECTION IS MOST IMPORTANT.

                    # Audience

                    In three to five words, who can take action?

                    # On-the-ground actions from transcript

                    <!-- Skip this section if there aren't physical actions described or suggested. -->
                    - Actionable, specific, completeable community, prep, aid stuff. Fewer is better unless author had many. Include ONLY actions described in the transcript. Focus do-now ones. On-the-ground actions MUST begin with direct, practical, concrete, physical, social, community network actions, concrete interactive verbs, examples: Contact people, Join organizations, Meet people, Build an object, Give someone an object, Go to your community, Organize an event, Create an org, Complain to your..., Ask each other, Bake neighbors a..., Coordinate neighbors to..., Distribute aid to..., Feed your people, ...; In parenthesis, end each "On-the-ground actions from transcript" line with whether transcript ( suggested, exemplified, implied ) the action, or if it was ( generated ).
                    - DON'T say to "educate yourself". DON'T include general, vague, ambient, always-on actions. NEVER USE "reflect on..." or "notice your..." or "discuss..." type actions! Those ain't actions. Actions are done with HANDS, not brain.
                    - Actions MUST have a clear completion point. DO say specifically what to do on the ground with other people in your community.

                    # Whats missing in summary

                    10-20 words on what can be best gotten from watching the full thing

                    # Tags

                    #UpToTen #Topic #Keywords #Someone #might #SearchFor
                    ```

                    Wordpart token limit {tokenlimit}
                """).format(tokenlimit=tokenlimit)
            },
            {
                "role": "user",
                "content": dedent("""

                    transcript
                    ```
                    {transcript}
                    ```
                    from {author}
                    transcript length: {tokens} wordpart tokens
                """).format(transcript=transcript, tokens=len(enct), author=author),
            }
        ],
        logit_bias=logit_bias,
        max_tokens=tokenlimit,
        presence_penalty=0.10,
        frequency_penalty=0.05,
        temperature=0.75,
        model=model,
        seed=1,
        top_p=0.99
    )
    costs.append((model, res2.usage))
    return res2.choices[0].message.content


def step__download_playlists():
    with pushd("playlists"):
        with open("playlists.txt", encoding="utf-8") as file:
            for playlist in file:
                playlist = playlist.strip().replace("youtubetab ", "")
                print(f"Downloading title of playlist {playlist}...")
                playlist_title = subprocess.run(['yt-dlp', '--skip-download', '-O', "playlist:'%(title)s'", '--flat-playlist', f'https://www.youtube.com/playlist?list={playlist}'], capture_output=True, text=True).stdout.strip()
                playlist_title_sanitized = re.sub(r'[^a-zA-Z0-9.]', '_', playlist_title)
                print(f"Downloading list of videos for playlist title {playlist_title}...")
                with open(f"playlist_{playlist}_{playlist_title_sanitized}.txt", "w", encoding="utf-8") as out_file:
                    subprocess.run(['yt-dlp', '--skip-download', '-O', '%(upload_date)s_%(id)s', '--flat-playlist', f'https://www.youtube.com/playlist?list={playlist}'], stdout=out_file)
                print("Done downloading playlist info")

def step__transcribe():
    subprocess.run([sys.executable, '-u', 'transcribe.py'])

def ensureparent(path):
    try:
        os.makedirs(os.path.dirname(os.path.abspath(path)))
    except FileExistsError:
        pass

def step__create_wiki_files(update_write, vids_by_id, playlists, do_summarize):
    if not Path("btfcwiki").exists():
        assert update_write
        subprocess.run(['git', 'clone', 'https://gitlab.com/DerSaege/btfcwiki.git'])

    if update_write:
        with pushd("btfcwiki"):
            # ensure `wiki` branch is checked out
            subprocess.run(['git', 'checkout', 'wiki'])
            subprocess.run(['git', 'branch', '--set-upstream-to=origin/wiki', 'wiki'])
            subprocess.run(['git', 'pull'])

    with open("templates/video.md", "r", encoding="utf-8") as file:
        template = file.read()
    assert "{{Url}}" in template
    assert "{{Transcript}}" in template
    assert "{{Date}}" in template

    # input title format: whisper_txt/2018/09/20180905_mfDasT0zSpg_Let_s_talk_about_burning_shoes.txt
    # output title format: btfcwiki/videos/2018/09/05/Lets_talk_about_burning_shoes.md
    filenames = glob.glob("whisper_txt/**/*.txt", recursive=True)
    md_by_id = {}
    id_by_md = {}
    summaryparsed = {}
    for filename in tqdm(filenames):
        if not_a_date(filename):
            print(f"skip, no date in {filename}")
            continue
        # get the youtube id from the filename by grabbing everything up to the '_Let'
        #youtube_id = re.sub(r'^whisper_txt/[0-9]+/[0-9]+/[0-9]{8}_(.{11})_.+$', r'\1', filename)
        name_match = re.match(r'^whisper_txt[\\/][0-9]+[\\/][0-9]+/(....)(..)(..)_(.{11})_(.+).txt$', filename)
        if not name_match:
            print("WARNING: failed to parse youtube id for file: " + filename)
            print("skipping, please fix manually, and fix or report this bug!")
            continue
        youtube_id = name_match.group(4)
        if youtube_id not in vids_by_id:
            print(f'WARNING: missing video: {filename} - skipping')
            continue
        video_url = f"[Youtube](https://www.youtube.com/watch?v={youtube_id})"
        realtitle = vids_by_id[youtube_id]['title']
        realtitle_path = re.sub("[^a-zA-Z0-9\-]+", '_', realtitle.replace("Let's", "Lets")).strip("_")
        year = name_match.group(1)
        month = name_match.group(2)
        day = name_match.group(3)
        new_filename = f"btfcwiki/videos/{year}/{month}/{day}/{realtitle_path}.md"
        #matching_ids = glob.glob(new_filename)
        #matching_ids_2 = glob.glob(orig_new_filename)
        #if not len(matching_ids) == 1:
        #    print("missing", realtitle_path)
        #    print("have", matching_ids_2)
        #    input("enter to move, ctrl+c to cancel")
        #    os.rename(orig_new_filename, new_filename)

        #junk_title="*".join([(x[1:-1] if len(x) > 3 else x) for x in title.split("_")])
        #matching_ids = glob.glob(f"btfcwiki/videos/{year}/{month}/{day}/*{junk_title}*")
        #if len(matching_ids) >= 2:
        #    print(f"multiple files for btfcwiki/videos/{year}/{month}/{day}/*{junk_title}*")
        #    print(matching_ids)
        published_on = f"{year}/{month}/{day}"
        id_by_md[new_filename.partition('/')[-1]] = youtube_id
        md_by_id[youtube_id] = new_filename.partition('/')[-1]
        y = {}
        if os.path.exists(new_filename):
            formatted = open(new_filename, "r", encoding="utf-8").read()
            if formatted.strip().startswith("---"):
                formatted_yaml, _, formatted = formatted.partition("---")[-1].partition("\n---\n")
                y = yaml.safe_load(formatted_yaml)
        else:
            with open(filename, encoding="utf-8") as file:
                transcript = file.read()
            formatted = (
                template
                    .replace("{{Url}}", video_url)
                    .replace("{{Transcript}}", transcript)
                    .replace("{{Date}}", published_on)
            )
            os.makedirs(os.path.dirname(new_filename), exist_ok=True)
        if do_summarize and "# bits" not in formatted.lower():
            summaryfiles = glob.glob(f"summaries_gpt3/{year}/{month}/*{youtube_id}*")
            with open(filename, encoding="utf-8") as file:
                transcript = file.read()
            if not summaryfiles:
                summary = summarize(transcript.replace(" Bo ", " Beau "), "Beau")
                sumf = filename.replace('whisper_txt', 'summaries_gpt3').rpartition('.')[0] + ".md"
                print("writing to", sumf)
                ensureparent(sumf)
                with atomic_write(sumf) as writer:
                    writer.write(summary)
            else:
                assert len(summaryfiles) == 1
                with open(summaryfiles[0], "r", encoding="utf-8") as file:
                    summary = file.read()
            assert '{{summary}}' in formatted.lower(), new_filename
            formatted = re.sub(r"^#+[^\n]*summary *\n+{{summary}}\n+", "# AI Summary (verify and correct me, please!)\n\n" + re.sub("^# ", "### ", summary.strip() + "\n\n\n", flags=re.MULTILINE), formatted, flags=re.IGNORECASE | re.MULTILINE)

        y['title'] = realtitle
        formatted = "---\n"+yaml.safe_dump(y, width=256).strip()+"\n---\n\n"+formatted.strip()
        #print(formatted)
        if update_write:
            with atomic_write(new_filename, overwrite=True) as file:
                file.write(formatted)
        summaryparsed[youtube_id] = summaryparse(formatted)
        #import pprint
        #pprint.pprint(summaryparsed)
        #1/0

    return summaryparsed, md_by_id, id_by_md

def load_playlists():
    beau_playlists = [x.split()[1] for x in open(os.path.join("playlists", "playlists.txt"), "r", encoding="utf-8").read().split("\n") if x.strip()]

    playlist_urls_by_video = {}
    playlist_names_by_video = {}
    playlist_idxs_by_video = {}
    for idx, playlist in enumerate(beau_playlists):
        filename = sorted(glob.glob(os.path.join("playlists", f"*{playlist}*")), key=lambda x: -os.stat(x).st_mtime)[0]
        playlist_contents = [x[3:] for x in open(filename, "r", encoding="utf-8").read().split("\n") if x.strip()]
        for vidid in playlist_contents:
            playlist_idxs_by_video[vidid] = idx
            playlist_urls_by_video[vidid] = f"https://www.youtube.com/playlist?list={playlist}"
            playlist_names_by_video[vidid] = os.path.basename(filename)[len(f"playlist_{playlist}_"):-4].lower().replace("q_a", "Q&A").replace("_", " ").replace("women s", "women's").replace("men s", "men's").replace("let s", "let's").replace("with beau of the fifth column", "").replace("with beau", "").replace("let's talk about ", "").replace(".", "").strip().title().replace("Beau S", "Beau's").replace("'S", "'s")
    return {'urls': playlist_urls_by_video, 'names': playlist_names_by_video, 'idxs': playlist_idxs_by_video}

def summaryparse(contents):
    partial_category = []
    category = None
    categories = {}

    def end_category(partial_category):
        if category == "transcript": return
        categories[category] = partial_category

    for line in contents.split("\n"):
        line = line.strip()
        if re.match(r"^#+ +", line):
            if category: end_category(partial_category)
            category = line.strip("# ").strip().lower()
            partial_category = []
            continue
        elif not line:
            continue
        elif re.match("^[^-].* says:?", line, flags=re.IGNORECASE):
            continue
        elif line.startswith("#"):
            partial_category.extend(line.split(" "))
        else:
            partial_category.append(line.strip("- "))
    end_category(partial_category)
    return categories

def all_videos__line_format(id, p, year, month, day, title, filename, level):
    p = dict(p)
    date = f"{year}-{month}-{day}"
    p['id'] = id
    p['date'] = date
    p['filename'] = re.sub(r"\.md$", "", filename)
    p['title'] = title
    p['joined_quotes'] = '\n'.join(p['quotes']) if 'quotes' in p else ''
    p['joined_oneliner'] = ' '.join(p['oneliner']) if 'oneliner' in p else ''
    p['joined_bits'] = '\n'.join(f"{x}" for x in p['bits']) if p.get('bits') else ''
    p['missing'] = ' '.join([' '.join(x) for key, x in p.items() if 'missing' in key.lower()])
    #if date == "2018-09-05":
    #    import pudb; pudb.set_trace()
    p['joined_actions'] = '\n'.join(['\n'.join(x) for key, x in p.items() if 'action' in key.lower()])
    p['joined_audience'] = ' '.join(p['audience']).lower() if 'audience' in p else ''
    summary_title = (
        '### AI summary (High error rate! Edit errors on video page)'
        if any(("ai summary" in x.lower()) for x in p.keys())
        else '### Community Summary (Edit on video page)'
    )
    fragments = []
    fragments.append(dedent("""
        <summary>
        {date}: {title} (<a href="https://youtube.com/watch?v={id}">watch</a> || <a href="/{filename}">transcript &amp; editable summary</a>)

        {joined_oneliner}

        </summary>
    """).format(**p))
    if level >= 1 and p['joined_quotes']:
        fragments.append(dedent("""
        {joined_quotes}
        """).format(**p))
    if level >= 2 and p['joined_bits']:
        fragments.append(dedent(summary_title).format(**p))
        fragments.append(dedent("""
            {joined_bits}
        """).format(**p))

    if level >= 3 and p['joined_actions'] and p['joined_audience']:
        fragments.append(dedent("""
            Actions:

            for {joined_audience},
            {joined_actions}
        """).format(**p))
    frags = "\n\n".join(fragments)
    return f"<details>\n{frags}\n</details>\n"

@contextmanager
def changechecking_open(filename, mode):
    ensureparent(filename)
    exclude_re = r"Note: this page[^\n]+\n"
    try:
        orig_contents = re.sub(exclude_re, "", open(filename, "r", encoding="utf-8").read())
    except FileNotFoundError:
        orig_contents = ""
    filename_new = filename + ".new"
    try:
        with open(filename_new, mode, encoding="utf-8") as writer:
            yield writer
        new_contents = re.sub(exclude_re, "", open(filename_new, "r", encoding="utf-8").read())
        if orig_contents != new_contents:
            print("changed:", filename)
            os.rename(filename_new, filename)
        else:
            os.unlink(filename_new)
    except:
        if os.path.exists(filename_new): os.unlink(filename_new)
        raise


def step__generate_month_year_pages(update_write, playlists, summaries, vids_by_id, md_by_id, id_by_md):
    assert Path("btfcwiki").exists(), "run step__create_wiki_files first"
    # for each month, generate a page with all the videos from that month
    # for each year, generate a page with all the monthly pages for that year and how many videos are in each month
    # for each year, also generate a page with all the videos from that year, linked to from year index
    # and then update the overall index with links to each year index and month indices?
    # templates don't exist yet. probably need real jinja for something this complicated...
    # meh lets do it the shitty way looolll
    def maybe_open(filename, mode):
        if not update_write:
            return io.StringIO()
        else:
            return changechecking_open(filename, mode)

    with pushd("btfcwiki"):
        filenames = glob.glob(os.path.join("videos", "*", "*", "*", "*.md"))
        videos_by_month = {}
        videos_by_year = {}
        months_by_year = {}
        all_videos = []
        now = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        generated_msg = f"Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated {now}\n"
        for filename in filenames:
            name_match = re.match(r'^videos[\\/](....)[\\/](..)[\\/](..)[\\/](.+).md$', filename)
            if not name_match:
                print("failed to parse filename for file: " + filename)
                print("skipping, please fix manually, and fix or report this bug!")
                continue
            id = id_by_md[filename]
            year = name_match.group(1)
            month = name_match.group(2)
            day = name_match.group(3)
            title = vids_by_id[id]["title"]
            year_list = videos_by_year.setdefault(year, [])
            year_list.append((month, day, title, filename))
            month_list = videos_by_month.setdefault((year, month), [])
            month_list.append((day, title, filename))
            all_videos.append((year, month, day, title, filename))
            months_by_year.setdefault(year, set()).add(month)
        all_videos.sort()
        with maybe_open("videos/all_videos.md", "w") as file:
            file.write("# All videos\n")
            file.write(generated_msg)
            file.write("\nThis page is very large and so only has partial summaries. View individual year or individual month pages for full summaries. Click on a video summary for some quotes, though.\n")
            last_year = None
            last_month = None
            for year, month, day, title, filename in all_videos[::-1]:
                id = id_by_md[filename]
                month_name = calendar.month_name[int(month)]
                if year != last_year:
                    file.write(f"\n## {year}\n")
                    last_year = year
                    file.write(f"[See only videos just from {year}](/videos/{year}/all_videos.md) - [month indices](/videos/{year}/months_index.md)\n")
                if month != last_month:
                    file.write(f"\n### {month_name}\n")
                    last_month = month
                    file.write(f"[See only videos from {year}/{month}](/videos/{year}/{month}/all_videos.md)\n")
                file.write(all_videos__line_format(id, summaries[id], year, month, day, title, filename, level=1))
        for year, year_list in sorted(videos_by_year.items()):
            year_list.sort()
            with maybe_open(f"videos/{year}/all_videos.md", "w") as file:
                file.write(f"# All videos from {year}\n")
                file.write(generated_msg)
                file.write("\nClick on a video summary for some quotes, a point by point summary, and some possible actions for the topic.\n")
                beforeafter = []
                if os.path.exists(f"videos/{int(year)-1}/all_videos.md"):
                    beforeafter.append(f"[&lt; {int(year)-1}](/videos/{int(year)-1}/all_videos.md)")
                beforeafter.append(str(year))
                if os.path.exists(f"videos/{int(year)+1}/all_videos.md"):
                    beforeafter.append(f"[{int(year)+1} &gt;](/videos/{int(year)+1}/all_videos.md)")
                file.write(" - ".join(beforeafter) + '\n')
                last_month = None
                last_day = None
                for month, day, title, filename in sorted(year_list, key=lambda x: (int(x[0]), int(x[1])))[::-1]:
                    id = id_by_md[filename]
                    month_name = calendar.month_name[int(month)]
                    if month != last_month:
                        file.write(f"\n## {month_name}\n")
                        last_month = month
                    file.write(all_videos__line_format(id, summaries[id], year, month, day, title, filename, level=3))
            with maybe_open(f"videos/{year}/months_index.md", "w") as file:
                file.write(f"# {year} Month indices\n")
                file.write(generated_msg)
                for month in sorted(months_by_year[year], key=int):
                    month_name = calendar.month_name[int(month)]
                    file.write(f"- [All videos from {month_name}](/videos/{year}/{month}/all_videos.md)\n")
                file.write(f"- [All videos from {year}](/videos/{year}/all_videos.md)\n")
        for (year, month), month_list in sorted(videos_by_month.items()):
            month_list.sort()
            with maybe_open(f"videos/{year}/{month}/all_videos.md", "w") as file:
                month_name = calendar.month_name[int(month)]
                file.write(f"# All videos from {month_name}, {year}\n")
                file.write(generated_msg)
                last_day = None
                for day, title, filename in month_list[::-1]:
                    id = id_by_md[filename]
                    file.write(all_videos__line_format(id, summaries[id], year, month, day, title, filename, level=3))


def step__upload_wiki_files():
    subprocess.run(['git', 'add', '-A'])
    date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    subprocess.run(['git', 'commit', '-m', f'update {date}'])
    subprocess.run(['git', 'push'])
    with pushd("btfcwiki"):
        subprocess.run(['git', 'add', '-A'])
        subprocess.run(['git', 'commit', '-m', f'update {date}'])
        subprocess.run(['git', 'push'])



def main():
    args = parser.parse_args()

    print("Making directories...")
    os.makedirs("lrc", exist_ok=True)
    os.makedirs("playlists", exist_ok=True)
    os.makedirs("audio", exist_ok=True)

    try:
        chans = json.loads(open("videos.json", "r", encoding="utf-8").read())
    except FileNotFoundError:
        chans = {}

    if args.download:
        print("Checking for yt-dlp...")
        step__check_yt_dlp()

        print("Downloading audio...")
        step__download_channel(chans, "https://www.youtube.com/c/BeauoftheFifthColumn")
        step__download_channel(chans, "https://www.youtube.com/channel/UC_x7nc3Vi4BPgmNnMsz774A")
        if args.playlists:
            print("Downloading playlists...")
            step__download_playlists()

        print("Moving files into year/month layout...")
        yearmonth_layout("audio")

    if args.transcribe:
        print("Transcribing audio...")
        step__transcribe()

    if args.update:
        print("Updating wiki files...")
        vids_by_id = {}
        for c in chans.values():
            vids = visit_vids(c)
            for v in vids:
                vids_by_id[v['id']] = v
        playlists = load_playlists()
        summaries, md_by_id, id_by_md = step__create_wiki_files(args.update_write, vids_by_id, playlists, do_summarize=args.summarize)
        step__generate_month_year_pages(args.update_write, playlists, summaries, vids_by_id, md_by_id, id_by_md)

    if args.upload:
        print("Uploading wiki files...")
        step__upload_wiki_files()


if __name__ == "__main__":
    #import pudb; pudb.set_trace()
    main()
