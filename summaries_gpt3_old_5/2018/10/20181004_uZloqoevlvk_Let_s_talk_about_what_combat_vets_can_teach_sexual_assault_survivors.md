```

# Bits

- Beau addresses the stigma surrounding dating sexual assault survivors by comparing their emotional responses to combat vets.
- He challenges the notion that survivors are "damaged goods" and unworthy of love, stating that trauma affects everyone in some way.
- Beau shares a story of a combat vet's experience during a mission, showcasing the impact of trauma on memory.
- He encourages reflection on the similarities in emotional responses between survivors and combat vets.
- Beau concludes by urging viewers to reconsider the stereotypes and stigmas attached to survivors of sexual assault.

# Audience
Survivors, allies, advocates

# On-the-ground actions from transcript
- Challenge stigmas about dating sexual assault survivors (exemplified)
- Support survivors by listening and understanding their experiences (exemplified)
- Educate others on the emotional responses of survivors and combat vets (generated)

# Oneliner
Combat veterans and sexual assault survivors share similar emotional responses, challenging stigmas and stereotypes.

# Quotes
- "You are not damaged goods."
- "Everybody's broke in some way."
- "It is amazing what trauma can do to your memory."

# What's missing in summary
The detailed emotional journey conveyed by Beau can be fully experienced by watching the entire video.

# Tags
#Stigma #SexualAssault #CombatVets #Trauma #EmotionalResponse #SupportSurvivors

```