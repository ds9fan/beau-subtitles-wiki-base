```

# Bits

- Beau received a message from a young lady who was alienated by her father's comments.
- One out of five women have been sexually assaulted.
- Beau addresses common arguments used to doubt victims of sexual assault.
- The perception that a victim is to blame for being assaulted is refuted.
- Beau urges men to talk to the women in their lives and apologize for justifying harmful comments.

# Audience
Men, fathers, sons, individuals.

# On-the-ground actions from transcript
- Have a real talk with women in your life and explain your comments (exemplified).
- Apologize for justifying harmful comments (exemplified).

# Oneliner
Beau calls for men to have honest talks with women in their lives and apologize for justifying harmful comments about sexual assault.

# Quotes
- "You don't get to rape her."
- "You need to have that talk."
- "Just apologize."

# Whats missing in summary
The emotional impact of the message and the importance of taking responsibility for harmful rhetoric.

# Tags
#SexualAssault #Men #Responsibility #Apology #Relationships #Communication
```