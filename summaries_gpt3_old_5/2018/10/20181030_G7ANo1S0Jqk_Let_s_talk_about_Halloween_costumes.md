```

# Bits

- Beau talks about the consequences of offensive Halloween costumes and behaviors.
- He points out that being offensive is not a substitute for having a sense of humor.
- Beau reflects on the difference in societal norms regarding humor and offensive behavior over time.
- He criticizes the attitude of some individuals towards edginess and shock value both online and in real life.
- Beau advocates for being a good person rather than solely focusing on being edgy or shocking.

# Audience

Individuals seeking guidance on appropriate behavior and understanding the consequences of offensive actions.

# On-the-ground actions from transcript
- Refrain from wearing offensive Halloween costumes or engaging in offensive behavior (exemplified)
- Choose humor that does not rely on being offensive or edgy (exemplified)
- Stand up against bigotry and strive for positive change in societal attitudes (exemplified)
- Hold individuals accountable for their actions and words, especially when they are offensive (exemplified)

# Oneliner

Offensive costumes have consequences; humor should not rely on being offensive. Stand against bigotry and strive for positive change.

# Quotes
- "Being offensive and edgy is not actually a substitute for having a sense of humor."
- "You can do whatever you want. But the rest of us are going to look down on you."
- "We are trying to change the way you think because the way you think is critically flawed."
- "Nobody's going back to that. Nobody's going to think that's OK."
- "You can do what you want, it's just a joke. But you can't be mad when you suffer the consequences of your actions."

# Whats missing in summary

Further insights on the importance of social responsibility and accountability when it comes to humor and behavior.

# Tags

#Halloween #Consequences #OffensiveBehavior #Bigotry #SocietalChange #Accountability