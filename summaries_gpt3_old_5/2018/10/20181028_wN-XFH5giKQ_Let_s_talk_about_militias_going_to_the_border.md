# Bits

- Beau Ginn called to protect America on the Mexican border with his militia but won't stop refugees.
- They won't violate U.S., Mexican, or international law by turning back asylum seekers at gunpoint.
- Upholding core principles like Constitution and due process for everyone entering the United States.
- Backing up DHS and hanging out with them, showing them how their militia works.
- Concerns about DHS knowing their operations if they ever follow through on aggressive rhetoric.

# Audience
Militia members, border security advocates.

# On-the-ground actions from transcript
- Join organizations supporting immigrants (exemplified)
- Advocate for humane treatment of asylum seekers (exemplified)
- Support organizations providing legal aid to refugees (implied)

# Oneliner
Beau Ginn's militia on the Mexican border shows support for DHS while upholding core principles on due process for asylum seekers.

# Quotes
- "Protecting America with our guns, ready to kill unarmed asylum seekers who are following the law."
- "We look good on the border protecting America with our guns."

# What's missing in summary
The full transcript provides more context on the dynamics between militia members and government agencies and the potential risks of aggressive rhetoric being exposed.

# Tags
#Militia #BorderSecurity #Refugees #DHS #DueProcess