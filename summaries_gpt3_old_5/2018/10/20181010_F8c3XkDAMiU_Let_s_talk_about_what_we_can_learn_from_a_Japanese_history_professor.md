```

# Bits

- Beau shares a story of a history professor in Japan who predicted the war and moved his family to an island to avoid the impending conflict.
- The professor could have influenced policy or gathered people to possibly stop the war but chose to remain silent, showing complicity with oppression.
- Beau urges people to speak up against tyranny and oppression, as remaining silent is siding with the oppressor.
- He advocates for having open and honest dialogues about money, politics, and religion, rather than avoiding these topics.
- Beau stresses the importance of individuals speaking out against tyranny to protect freedom everywhere.

# Audience
Activists, Advocates, Community Members

# On-the-ground actions from transcript
- Initiate open dialogues on sensitive topics with friends and family members (suggested)
- Speak out against oppression and tyranny in your community (exemplified)
- Challenge the norm of avoiding certain topics and encourage meaningful discourse (exemplified)

# Oneliner
Silence is complicity; speak out against oppression to safeguard freedom everywhere.

# Quotes
- "When you remain silent in the face of tyranny or oppression, you have chosen a side."
- "Your silence is only helping them."
- "Tyranny anywhere is a threat to freedom everywhere."

# Whats missing in summary
The full transcript provides more historical context and details on the professor's story, which enriches the narrative.

# Tags
#Silence #Oppression #Tyranny #Freedom #Dialogue #Activism

```