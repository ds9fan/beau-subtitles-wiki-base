```

# Bits

- Beau expresses concern over Facebook and Twitter censoring independent news outlets opposed to government policies.
- Beau differentiates between sensationalist outlets and real news outlets, stressing the importance of diverse perspectives in media.
- Beau shares personal connections with individuals affected by the censorship and suggests following journalist Carrie Wedler.
- Beau urges people to seek alternative social media platforms like Steam It and MeWe due to increasing censorship on Facebook and Twitter.
- Beau warns that the censorship of dissenting voices on social media platforms signals a threat to free discourse.
- Beau encourages individuals to be prepared to move to new platforms to continue accessing diverse information sources.

# Audience
Social media users

# On-the-ground actions from transcript
- Transition to alternative social media platforms like Steam It and MeWe for accessing diverse information sources and supporting independent news outlets (exemplified)
- Share content from independent news outlets on new platforms to ensure information dissemination continues (exemplified)

# Oneliner
Beau warns of the dangers of Facebook and Twitter censorship on independent news outlets and advocates for transitioning to alternative platforms for free discourse.

# Quotes
- "This is going to be the end of free [...] on Facebook."
- "Be ready. Look, I know that some of these outlets are setting up on places called Steam It."
- "Even the worst among them wasn't that level, not even close."

# Whats missing in summary
The detailed examples and nuances of Beau's personal relationships with individuals affected by the censorship.

# Tags
#Censorship #AlternativePlatforms #FreeSpeech #IndependentMedia #SocialMediaUsers #Transition

```