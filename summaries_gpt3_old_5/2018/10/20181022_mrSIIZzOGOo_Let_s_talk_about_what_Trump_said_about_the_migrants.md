```

# Bits

- Beau expresses concern about the President cutting off aid to Central American countries for not stopping their people from coming to the US.
- Beau explains his fear by pointing out that the President believes it is the head of state's duty to keep people in their country.
- Beau warns that the border wall could be used to keep Americans in, not just to keep others out.
- He cautions against the slow creep of tyranny and the potential implications of such actions.
- Beau criticizes those who support such measures, stating that they have compromised their ideals for a red hat and a slogan.
- He concludes by reiterating that the wall could serve as a prison barrier, restricting Americans' freedom of movement.

# Audience
American citizens, activists

# On-the-ground actions from transcript
- Contact elected officials to express opposition to cutting off aid (exemplified)
- Support organizations working to protect civil liberties and human rights (exemplified)

# Oneliner
Beau warns of the potential tyranny in using a border wall to restrict Americans' freedom of movement, urging vigilance and activism.

# Quotes
- "That wall is a prison wall. It's not a border wall."
- "It's gonna work both ways."

# Whats missing in summary
The emotional delivery and passion in Beau's voice are best experienced by watching the full video.

# Tags
#USPolitics #Tyranny #CivilRights #Activism #FreedomOfMovement #BorderWall