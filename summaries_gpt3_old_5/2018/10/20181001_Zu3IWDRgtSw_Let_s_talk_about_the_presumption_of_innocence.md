```

# Bits

- Beau expresses surprise at the focus on presumption of innocence in relation to Kavanaugh.
- Beau points out Kavanaugh's support for law enforcement overreach and lack of respect for rights.
- Beau criticizes those who defend Kavanaugh without considering his rulings.
- Beau warns against party politics leading to erosion of rights and potential future consequences.
- Beau urges people to think critically about the implications of Supreme Court nominations.
- Beau condemns blind support for political figures without considering their actions or impact.

# Audience
Political commentators, activists, voters

# On-the-ground actions from transcript
- Contact local representatives to express concerns about Supreme Court nominations. (generated)
- Get involved in grassroots efforts to protect civil liberties and rights. (exemplified)

# Oneliner
Beau criticizes blind support for Kavanaugh, warns against party politics eroding rights, and urges critical thinking in Supreme Court nominations.

# Quotes
- "You've bought into bumper sticker politics and you're trading away your country for a red hat."
- "You can sit there and you can wave your flag and you can talk about making America great again."

# Whats missing in summary
The emotional impact of Beau's passionate condemnation of blind political allegiance.

# Tags
#SupremeCourt #Kavanaugh #PresumptionOfInnocence #PartyPolitics #CivilLiberties #CriticalThinking