```

# Bits

- Beau expresses his lack of shock at the President mocking sexual assault survivors on television and how the bar has been set low.
- He points out the concerted effort in the country to silence sexual assault claims, led by the President.
- Beau criticizes the President's behavior, contrasting him with Obama and questioning his integrity.
- He talks about the dangerous nature of labeling perpetrators as monsters and the importance of recognizing them as people.
- Beau draws parallels between Nazis and sexual assault predators, stressing the danger of remaining silent in the face of evil.

# Audience

Advocates against sexual assault.

# On-the-ground actions from transcript

- Reach out to and support survivors of sexual assault. (suggested)
- Speak out against efforts to silence those coming forward with assault claims. (exemplified)
- Challenge the normalization of derogatory behavior towards survivors. (generated)

# Oneliner

President's actions on sexual assault criticized, urging recognition of perpetrators as people, not monsters.

# Quotes

- "There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim, and it's being led by the President of the United States."
- "That is dangerous because they're not monsters, guys. They're people."
- "The scariest Nazi wasn't a monster. He was your neighbor."

# What's missing in summary

The detailed nuances and emotional depth of Beau's commentary.

# Tags

#SexualAssault #President #Accountability #SilencingSurvivors #Nazis #Perpetrators