```

# Bits

- Beau describes the aftermath of hurricane Michael and how people can prepare for natural disasters.
- Law enforcement's response to looting during disasters can hinder volunteer relief efforts.
- Beau criticizes law enforcement for prioritizing protecting property over saving lives during disasters.
- Government agencies' response to disasters may not always be reliable, so individuals should be prepared with emergency kits.
- Beau stresses the importance of having a well-prepared emergency bag with essentials for survival during a disaster.

# Audience
Individuals in disaster-prone areas.

# On-the-ground actions from transcript
- Prepare an emergency kit with essentials for survival (exemplified)
- Gather food, water, fire, shelter, medical supplies, and a knife in the emergency kit (exemplified)
- Include extra medication for family members who require daily meds in the emergency kit (exemplified)
- Organize the emergency kit in one spot for easy access (exemplified)

# Oneliner
Beau shares insights on preparing for disasters, criticizing law enforcement's response to looting, and urging individuals to create emergency kits for survival.

# Quotes
- "Two van fulls of wet TVs are worth more than your life."
- "Protecting the inventory of some large corporation should pretty much be at the bottom of any law enforcement agencies list."
- "Some of the things that your government agencies may do will make it worse for you."
- "We're not talking about building a bunker and preparing for doomsday."
- "Please take the time to read it, to put one of those bags together."

# What's missing in summary
The detailed context of how media coverage influences public perception during disasters.

# Tags
#NaturalDisasters #EmergencyPreparedness #CommunitySupport #LawEnforcement #GovernmentResponse #MediaInfluence
```