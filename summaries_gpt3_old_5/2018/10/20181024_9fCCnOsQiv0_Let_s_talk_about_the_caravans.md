```

# Bits

- Beau talks about the history of caravans coming to the US for 15 years due to drug war policies and US interventions in countries.
- He explains that the violence in Honduras is a big reason why people are fleeing in caravans.
- Beau compares the murder rate in San Pedro Sula, Honduras, to that of Chicago to provide context for American viewers.
- He addresses questions about who pays for the caravan migrants, mentioning the Office of Refugee Resettlement's budget.
- Beau contrasts the cost of helping migrants get on their feet with the expenses of detaining them, pointing out the economic benefits of assisting them.
- The reason for migrants traveling in large groups is likened to the safety in numbers concept, similar to kids trick-or-treating.
- He explains why migrants carry their countries' flags and wear military fatigues during the journey.
- Beau clarifies that claiming asylum is a legal process and that migrants are following US laws by presenting themselves at the border.
- He encourages viewers to contact their representatives to address foreign policy and drug war impacts instead of blaming the migrants.
- Beau challenges those advocating for turning away migrants, asking for a single good reason to send people back to face potential death.

# Audience

Foreign policy advocates, asylum supporters.

# On-the-ground actions from transcript

- Contact your representative to address foreign policy impacts (exemplified)
- Advocate for changes in drug war policies (exemplified)

# Oneliner

Caravans to the US are a consequence of foreign policy and violence, asylum seekers are following legal processes, and turning them away could mean sending them to their deaths.

# Quotes

- "It's cheaper to be a good person."
- "You can't preach freedom and then deny it, send people back to their death for no good reason other than fear."
- "Give me one good reason to send these people back to their deaths. One. I can't think of one."

# Whats missing in summary

Deeper insights into the complexities of US foreign policy and its impact on migration.

# Tags

#Caravans #AsylumSeekers #USForeignPolicy #Immigration #HumanRights #ViolenceInHonduras

```