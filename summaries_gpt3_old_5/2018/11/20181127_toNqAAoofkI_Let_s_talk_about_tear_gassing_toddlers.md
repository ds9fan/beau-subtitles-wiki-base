```

# Bits

- Beau Ginn questions reactions to tear gas on children.
- Speculates on potential violence along the border.
- Challenges the nation's response to fear-mongering.
- Condemns tear-gassing of kids and government behavior.
- Considers implications of lack of violence.
- Beau Ginn signs off with a thought-provoking message.

# Audience

Citizens, activists, parents.

# On-the-ground actions from transcript

- Contact representatives to demand accountability for tear-gassing children. (suggested)
- Join or support organizations advocating for humane border policies. (exemplified)
- Organize or participate in peaceful protests against violence on the border. (generated)

# Oneliner

Beau Ginn questions violence, fear, and government actions along the border, urging reflection on our response to tear-gassing children.

# Quotes

- "Sometimes, we tear gassed kids."
- "Are you going to admit to yourself that that fear monger in the Oval Office has turned this entire nation into cowards?"
- "If there is not a massive upsurge in violence along the border, it says a whole lot about those people."

# What's missing in summary

Deeper insights on the impact of fear-mongering and violence on society.

# Tags

#TearGas #Children #BorderViolence #FearMongering #Accountability #Activism