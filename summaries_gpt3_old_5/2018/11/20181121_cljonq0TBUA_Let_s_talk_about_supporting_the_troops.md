```

# Bits

- Beau tells a story of Dennis and Terry helping hurricane victims in Florida.
- They deliver supplies and food, collaborating with a community center.
- All involved in the aid are veterans, showing the instinct to help in tough times.
- Beau urges true support for veterans beyond just rhetoric.

# Audience

Supporters of veterans' well-being.

# On-the-ground actions from transcript

- Contact local aid organizations to assist hurricane victims (suggested).
- Volunteer at community centers providing relief efforts (exemplified).
- Advocate for veterans' rights and benefits (implied).
- Verify information before supporting military actions (generated).

# Oneliner

Veterans lead in aiding communities; true support goes beyond words to actions.

# Quotes

- "If you really want to help veterans, stop turning them into combat veterans because some politician waved a flag and sold you a pack of lies that you didn't..."
- "Before you can support the troops, you've got to support the truth."

# Whats missing in summary

The emotional depth and personal connection in Beau's storytelling.

# Tags

#Veterans #Support #CommunityAid #Truth #Action

```