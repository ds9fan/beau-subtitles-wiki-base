```

# Bits

- Beau talks about police militarization and the idea of the "warrior cop".
- He mentions that the military understands the importance of civil affairs and infrastructure in keeping people safe.
- Beau criticizes the shift in law enforcement from protecting the public to strictly enforcing the law.
- He points out the dangers of propaganda that perpetuates the belief in a "war on cops".
- Beau describes the misuse of statistics to inflate the dangers faced by law enforcement.
- He delves into the issue of SWAT teams, their training, and the discrepancies in perceived versus actual capabilities.
- Beau stresses the need for law enforcement to rethink their approach and move away from being warrior cops.

# Audience

Law enforcement officers, policymakers

# On-the-ground actions from transcript

- Rethink approach to law enforcement duties and prioritize protecting the public over strictly enforcing laws (exemplified)
- Ensure proper training for law enforcement officers to handle equipment and tools effectively (suggested)
- Address discrepancies between perceived and actual capabilities in SWAT teams (generated)

# Oneliner

Beau criticizes police militarization and the "warrior cop" mentality, urging law enforcement to prioritize public protection over enforcing laws to prevent unnecessary fatalities.

# Quotes

- "If your interest is no longer protecting the public and it's only enforcing the law, you're just accepting orders and taking a paycheck to do it using violence."
- "There is no war on cops. There should be no warrior cops."
- "There's going to be more and more fatalities."

# Whats missing in summary

Deeper insights into the impact of police militarization on communities and potential solutions beyond rethinking law enforcement's role.

# Tags

#PoliceMilitarization #WarriorCop #LawEnforcement #SWATTeams #PublicProtection #Propaganda