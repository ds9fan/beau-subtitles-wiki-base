```

# Bits

- Beau talks about the shooting in Alabama and the confusion around it, focusing on biases and the public narrative.
- Beau mentions the police admitting they killed the wrong person and the lack of clarity in their statements.
- Addressing the military service of the individual involved, Beau delves into the complexities of discharge and training completion.
- Beau challenges the notion of the man being a "disgrace to the uniform" based on a photo and questions the media narrative.
- Concerning the racial aspect, Beau points out the history of armed black individuals being killed by police despite legal ownership.
- Beau urges for acknowledging biases and using positive threat identification in law enforcement situations.

# Audience

Community members, activists, law enforcement officers.

# On-the-ground actions from transcript

- Challenge biases in law enforcement (exemplified)
- Use time, distance, and cover for threat identification (exemplified)
- Advocate for accurate representation and support for armed black individuals (exemplified)

# Oneliner

Beau challenges biases in the public narrative surrounding a shooting incident, urging for greater awareness and understanding.

# Quotes

- "Being armed and black is not a crime."
- "Biases exist. If you don't acknowledge them, they will persist."
- "Nothing suggests this man did anything wrong."

# Whats missing in summary

The full transcript provides a detailed exploration of biases, public narratives, and law enforcement perspectives surrounding a shooting incident.

# Tags

#Shooting #Biases #LawEnforcement #RacialJustice #CommunitySafety #Beau

```