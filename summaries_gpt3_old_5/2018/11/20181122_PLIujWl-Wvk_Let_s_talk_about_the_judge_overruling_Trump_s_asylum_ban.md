```

# Bits

- Beau explains the significance of a judge overruling Trump's asylum ban, clarifying the legality of seeking asylum at the border.
- He criticizes news outlets and Trump for misleading the public into believing that seeking asylum is illegal.
- Beau condemns Trump's attempt to change asylum laws through proclamation, labeling it as dictatorial behavior and a direct challenge to the Constitution.
- He praises the judicial branch for stopping Trump's actions but expresses disappointment in Congress for not holding him accountable.
- Beau questions the patriotism of those who support Trump's actions and warns against undermining the Constitution.

# Audience
Americans, voters, activists

# On-the-ground actions from transcript
- Call or email representatives to express support for upholding the Constitution (exemplified)
- Participate in civic engagement by staying informed and holding leaders accountable (exemplified)

# Oneliner
Beau explains the legality of seeking asylum and criticizes Trump's attempt to circumvent the Constitution, calling for accountability.

# Quotes
- "If it's your party, they can set fire to the Constitution and you'll roast a marshmallow over it."
- "You don't care about the Constitution."
- "That's how far this country has gone."

# What's missing in summary
The full video provides a detailed analysis of Trump's actions and their implications for the Constitution.

# Tags
#AsylumBan #Trump #Constitution #Judiciary #Accountability #Patriotism #CivicEngagement