```

# Bits

- Beau shares a Kennedy conspiracy theory that Green Berets were sworn to help Americans fight back against tyranny.
- Kennedy met with selected Green Berets in 1960, and 46 Green Berets were part of Kennedy's honor guard at his funeral.
- Command Sergeant Major Ruddy placed his green beret on Kennedy's casket, which is now displayed at the JFK Presidential Library.
- A group of active-duty Green Berets hold a ceremony at Kennedy's grave site annually.
- Beau warns about the suspension of habeas corpus, linked to the current president's considerations.

# Audience

History buffs, conspiracy enthusiasts, activists.

# On-the-ground actions from transcript

- Visit the JFK Presidential Library to view Command Sergeant Major Ruddy's green beret on display (exemplified).
- Research habeas corpus and understand its significance (exemplified).
- Attend or organize ceremonies to honor past leaders (exemplified).

# Oneliner

Beau unveils a Kennedy conspiracy theory involving Green Berets and warns against the suspension of habeas corpus, urging action.

# Quotes

- "The suspension of habeas corpus is the end of the rule of law."
- "If they don't have rights because they're not U.S. citizens, why suspend habeas corpus?"
- "Support your country or support your president. The suspension of habeas corpus erases the rule of law."

# Whats missing in summary

The full transcript delves into the mysterious legend surrounding President Kennedy's meeting with Green Berets and the potential implications of suspending habeas corpus.

# Tags

#KennedyConspiracy #GreenBerets #HabeasCorpus #Warning #ActionNeeded
```