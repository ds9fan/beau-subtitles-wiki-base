# Bits

- Beau talks about protesters upset with the government's actions.
- He compares the current situation to the Boston Massacre.
- Beau mentions the use of human shields by protesters.
- He criticizes the idea of using children as human shields.
- The term "human shield" is discussed in the context of warfare.
- Beau questions the morality of opening fire on unarmed crowds.
- He expresses disappointment in the current state of affairs in America.

# Audience
Protesters, activists, concerned citizens

# On-the-ground actions from transcript
- Join peaceful protests against government actions (generated)
- Advocate for the protection of civilians during protests (generated)

# Oneliner
Beau examines protest dynamics, referencing history to question the morality of using human shields and opening fire on civilians.

# Quotes
- "You do not punish the child for the sins of the father."
- "You do not open fire on unarmed crowds."
- "Betraying everything that this country stood for, at least pretended to stand for."

# What's missing in summary
The full impact of Beau's emotional delivery and the depth of his disappointment may be best understood by watching the entire video.

# Tags
#Protest #HumanShields #Morality #America #CivilRights