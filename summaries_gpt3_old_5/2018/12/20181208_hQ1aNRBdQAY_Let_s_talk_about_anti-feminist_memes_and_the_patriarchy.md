```

# Bits

- Beau talks about anti-feminist memes and the patriarchy triggered by a t-shirt with Disney princesses.
- Two types of anti-feminist memes: ones about cooking/cleaning and ones about feminism making women ugly.
- Beau challenges the shallow mentality of valuing women based on cooking skills and physical appearance.
- He suggests that those who push these memes may have unresolved issues from childhood.
- Beau explains the harmful nature of objectifying women and using their appearance to discredit their ideas.
- He mentions the criticism of male feminists being labeled as "soft" or "soy boys."
- Beau counters the argument that feminism is unnecessary by pointing out the continued existence of patriarchy in power dynamics.
- He provides statistics on female representation in political positions to illustrate the persistence of patriarchy.
- Beau challenges the notion that disparities in promotions and opportunities are solely based on qualifications.
- He calls for respecting women's autonomy and choices regardless of others' opinions or insecurities.

# Audience
Those interested in understanding and challenging anti-feminist narratives and patriarchal structures.

# On-the-ground actions from transcript
- Challenge anti-feminist rhetoric online by sharing educational resources and promoting gender equality. (exemplified)
- Support and amplify the voices of women in leadership roles to combat patriarchal power imbalances. (exemplified)
- Encourage critical thinking about gender stereotypes and biases in personal and professional contexts. (implied)

# Oneliner
Beau dismantles anti-feminist memes and exposes the persistence of patriarchy in power dynamics, advocating for women's autonomy and respect.

# Quotes
- "All women have to be two things, and that's it. Who and what they want."
- "What you think doesn't matter."
- "The fact that she intimidates you doesn't matter. That's your hangup, not hers."

# What's missing in summary
Deeper insights into the nuances of feminist movements and the intersectionality of gender issues.

# Tags
#AntiFeminism #Patriarchy #GenderEquality #Objectification #FeministMovement #GenderRoles #EqualityInPolitics #RespectAutonomy #ChallengeStereotypes

```