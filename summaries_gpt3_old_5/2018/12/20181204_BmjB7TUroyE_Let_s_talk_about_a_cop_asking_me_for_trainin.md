```

# Bits

- Beau shares a story about a cop suggesting hitting a suspect with a baton, revealing the issue of unchecked power in law enforcement.
- He stresses the importance of time, distance, and cover in police work to prevent unnecessary harm to suspects.
- Beau explains auditory exclusion in high-stress situations and the implications for police interactions.
- He warns against excessive force and the dangers of positional asphyxiation in arrests.
- Beau advocates for cops wearing protective vests, staying behind cover in shootouts, and receiving appropriate training.
- He recounts a personal story to illustrate the importance of using discretion and understanding the spirit of the law over the letter.
- Beau advises against approaching superiors for advanced training without external support to avoid risking one's job.

# Audience
Law enforcement officers.

# On-the-ground actions from transcript
- Implement time, distance, and cover approach in police work to prevent unnecessary harm (exemplified).
- Be aware of auditory exclusion in high-stress situations and take necessary precautions (exemplified).
- Wear protective vests for safety during duty (exemplified).
- Stay behind cover during shootouts to improve accuracy and reduce risk (exemplified).
- Exercise discretion and understand the spirit of the law to make informed decisions (exemplified).

# Oneliner
Beau's insights urge law enforcement to prioritize safety, discretion, and proper training to enhance community interactions and officer well-being.

# Quotes
- "Time, distance, and cover, please start to use it."
- "The most dangerous people on the planet are true believers."
- "When you run across somebody that is ideologically motivated, and they are breaking the law, but the crime has no victim, keep rolling."

# Whats missing in summary
The nuanced storytelling and emotional depth of Beau's experiences.

# Tags
#LawEnforcement #PoliceTraining #SafetyProtocols #UseOfForce #SpiritOfTheLaw #AuditoryExclusion
```