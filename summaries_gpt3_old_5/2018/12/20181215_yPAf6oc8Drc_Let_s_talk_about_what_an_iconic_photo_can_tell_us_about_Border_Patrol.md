```

# Bits

- Beau shares a story from the Vietnam War about humanity in the midst of conflict.
- He criticizes Border Patrol agents for dumping out water left for migrants, likening it to inhumane behavior.
- Beau condemns the actions of Border Patrol agents, urging them to quit and take responsibility for their actions.
- He stresses that barriers like walls cannot deter the human drive for safety, freedom, and opportunities.
- Beau warns Border Patrol agents that they will be held accountable for their actions, urging them to resign.

# Audience

Advocates, border patrol agents

# On-the-ground actions from transcript

- Call for Border Patrol agents to quit their jobs and take responsibility for their actions (exemplified)
- Advocates for humane treatment of migrants to continue providing water and aid along routes (exemplified)

# Oneliner

Amidst conflict and inhumanity, Beau urges Border Patrol agents to quit and take accountability for their actions.

# Quotes

- "Your badge is not going to protect you from that. You need to quit."
- "Just following orders isn't going to cut it."
- "The human spirit will find a way."

# What's missing in summary

The emotional depth and storytelling elements present in Beau's delivery.

# Tags

#Humanity #Inhumanity #Accountability #BorderPatrol #Migrants #Safety #Freedom #Opportunity #Resignation
```