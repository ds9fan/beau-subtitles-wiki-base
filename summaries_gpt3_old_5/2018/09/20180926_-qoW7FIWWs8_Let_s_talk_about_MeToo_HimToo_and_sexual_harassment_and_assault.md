```

# Bits

- Beau talks about sexual harassment and assault in light of the Kavanaugh nomination.
- He provides advice on avoiding such situations, including safeguarding reputation and being careful with signals.
- Beau addresses men's fear of false accusations, stressing accountability for actions.
- He challenges victim-blaming and excuses for rape based on clothing and behavior.

# Audience

Men, individuals discussing sexual harassment and assault.

# On-the-ground actions from transcript

- Safeguard your reputation to attract the right kind of people. (exemplified)
- End dates at the front door for safety. (exemplified)
- Avoid taking or going home with anyone when drinking. (exemplified)
- Watch what you say to avoid sending unintentional signals. (exemplified)
- Keep your hands to yourself to prevent misunderstandings. (exemplified)

# Oneliner

Beau offers advice on avoiding sexual harassment situations, stressing accountability for actions and challenging victim-blaming excuses.

# Quotes

- "If you're that worried about it, it's because you've done something."
- "There is one cause of rape, and that's rapists."

# What's missing in summary

In-depth exploration of societal attitudes towards sexual harassment and assault.

# Tags

#SexualHarassment #Assault #Accountability #VictimBlaming #Prevention #Men'sIssues