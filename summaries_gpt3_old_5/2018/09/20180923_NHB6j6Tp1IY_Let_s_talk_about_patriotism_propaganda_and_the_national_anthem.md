```

# Bits

- Beau responds to a story attacking a woman for kneeling during the national anthem.
- Beau questions the author's understanding of patriotism and protests.
- Beau challenges the spokesman for Law Enforcement on his knowledge of the purpose of the protest.
- Beau contrasts nationalism with patriotism through a personal anecdote.
- Beau encourages a deeper understanding of patriotism beyond symbols.
- Beau offers to connect the Law Enforcement spokesman with a veteran for insight on patriotism.

# Audience
Patriotic individuals seeking to understand the distinction between nationalism and patriotism.

# On-the-ground actions from transcript
- Reach out to veterans for insights on patriotism. (exemplified)
- Have meaningful dialogues on the true meaning of patriotism. (exemplified)
- Challenge misinformation about protests and patriotism. (exemplified)
- Encourage critical thinking on the concepts of nationalism and patriotism. (implied)

# Oneliner
Beau challenges misconceptions on patriotism, urging dialogue and understanding over symbols and nationalism.

# Quotes
- "Patriotism is correcting your government when it is wrong."
- "I believe patriotism is displayed through actions, not worship of symbols."
- "There is a very marked difference between nationalism and patriotism."
- "Sir, patriotism is not doing what the government tells you to."
- "You can kiss my patriotic behind."

# What's missing in summary
Deeper insights on the nuances of patriotism and the importance of understanding its true essence beyond mere symbols.

# Tags
#NationalAnthem #Protests #Patriotism #Dialogue #Misconceptions #CommunityEngagement

```