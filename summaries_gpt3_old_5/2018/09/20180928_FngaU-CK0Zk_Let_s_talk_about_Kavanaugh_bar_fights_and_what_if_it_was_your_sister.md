```

# Bits

- Beau shares a story from college where he steps in to protect a girl from a pushy guy at a bar.
- Beau reflects on the importance of respecting women's boundaries and property rights.
- Beau relates the bar incident to the current Kavanaugh hearing and urges viewers to empathize by considering their friends in similar situations.
- He breaks down statistics on sexual assault among women to illustrate the prevalence and impact within social circles.
- Beau questions why men who were sexually assaulted by Catholic priests took so long to come forward and challenges misconceptions about male emotions.
- He explains that sexual assault is about power and control, not just sex, and links it to concerns about Kavanaugh having power over millions of women through the Supreme Court.
- Beau encourages reflection on how comments dismissing assault survivors can impact relationships and trust.
- He concludes by urging viewers to reconsider their stance on such issues and the potential harm caused by not supporting survivors.

# Audience
Supporters of respecting boundaries.

# On-the-ground actions from transcript
- Stand up for others in uncomfortable situations (exemplified)
- Empathize with friends by considering their perspectives (exemplified)
- Educate oneself on the prevalence and impact of sexual assault (exemplified)
- Refrain from making dismissive comments about assault survivors (exemplified)

# Oneliner
Beau's powerful message urges empathy for assault survivors and reflection on the impact of dismissive attitudes within social circles.

# Quotes
- "Because it's very relevant today."
- "Man, so if you see somebody that took that power and control away from you, stripped it from you..."
- "How many of them do you know? How many of them talked about it with you?"

# What's missing in summary
The emotional delivery and personal anecdotes from Beau are best experienced by watching the full video.

# Tags
#Respect #Empathy #SexualAssault #Boundaries #SupportSurvivors #Reflection