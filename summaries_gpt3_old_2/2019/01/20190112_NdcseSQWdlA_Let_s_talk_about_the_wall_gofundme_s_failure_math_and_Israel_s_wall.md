# Bits

- GoFundMe money being refunded for the wall project.
- Request to redirect donations to a new non-profit raises concerns.
- Raised funds were a small fraction of what is needed for the wall.
- Americans are not in favor of the wall for various reasons.
- Comparison between the proposed US wall and Israel's wall.
- Effectiveness of walls in different countries and their purposes.

# Audience
Donors, activists, policymakers.

# On-the-ground actions from transcript
- Contact local representatives to express opinions on border wall funding. (implied)
- Support organizations working towards immigration reform. (implied)
- Educate others on the complexities and implications of border wall construction. (generated)

# Oneliner
The GoFundMe refund reveals Americans' reluctance towards the border wall project, citing moral, ethical, legal, and economic concerns.

# Quotes
- "Americans don't want the wall. They know that it's morally wrong, ethically wrong, legally wrong, economically wrong, and mathmatically wrong."
- "If just a third of the people in the United States wanted the wall and were willing to give a dollar, it could have raised more than a hundred million dollars but it didn't."
- "We don't want to join the ranks of countries that engage in apartheid."

# What's missing
In-depth analysis of the impact of public opinion on government decisions regarding border security and immigration policies.

# Tags
#BorderWall #GoFundMe #Immigration #USGovernment #PublicSentiment