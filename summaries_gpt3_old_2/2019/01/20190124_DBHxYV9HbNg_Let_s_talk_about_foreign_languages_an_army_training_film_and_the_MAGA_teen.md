```

# Bits

- Switching to the native languages of others to aid in communication is key in activism and academia, even if it means using terminology that may seem outdated or reinforcing negative concepts (Beau exemplified).
- Understanding the importance of speaking the audience's "native" languages is vital to effective communication, as shown in an old army training film (Beau exemplified).
- The significance of the way information is presented can often overshadow the actual content presented (Beau exemplified).
- Being aware of how messages are perceived and avoiding succumbing to fake narratives is vital in the fight against misinformation (Beau exemplified).

# Audience

Activists, academics, communicators

# On-the-ground actions from transcript

- Adapt your communication style to speak the audience's "native" languages (Beau exemplified).
- Ensure you are the right messenger for the message you want to deliver (Beau exemplified).
- Be conscious of how information is presented and perceived to combat fake narratives (Beau suggested).

# Oneliner

Understanding and speaking the audience's "native" languages is key to effective communication and combatting misinformation, as shown in an old army training film. (Beau exemplified)

# Quotes

- "Sometimes it may not be that you're constructing a poor argument. It may be that you're just not the person that can deliver it to the audience." - Beau
- "The way something is presented is often times more significant than the information presented in it." - Beau

# What's missing

Detailed examples of how misinformation can be countered through effective communication strategies.

# Tags

#Communication #Activism #Misinformation #EffectiveMessaging #Academia