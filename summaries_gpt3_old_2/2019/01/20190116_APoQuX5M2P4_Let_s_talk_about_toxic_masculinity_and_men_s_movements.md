```

# Bits

- Bo discusses the concept of toxic masculinity and its roots in men's movements.
- He outlines the main ideas of the mythopoetic men's movement from the 80s and 90s.
- The term "toxic masculinity" was coined by the first MRAs, not feminism.
- Bo delves into the glorification of violence and the loss of masculine ideals.
- He stresses the importance of understanding and addressing toxic masculinity for a safer society.

# Audience

Men, activists, educators.

# On-the-ground actions from transcript

- Educate others on toxic masculinity (implied)
- Join or support movements advocating for healthy masculinity (suggested)
- Address and call out toxic behavior in your community (generated)

# Oneliner

Bo dives into the history and consequences of toxic masculinity, urging action for a safer society and healthier masculinity norms.

# Quotes

- "Men have wanted to hold on to the violence and to that dominating spirit, but they lost all the ideals that went with it."
- "That ideal [of masculinity] should still be ingrained in you. It's not there."
- "Not saying hello can get a woman killed."

# What's missing

In-depth discussion on practical steps individuals can take to challenge and change toxic masculine behaviors.

# Tags

#ToxicMasculinity #Men #Movements #GenderEquality #Activism