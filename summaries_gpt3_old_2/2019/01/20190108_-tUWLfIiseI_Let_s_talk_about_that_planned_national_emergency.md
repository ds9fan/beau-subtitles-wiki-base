```

# Bits

- Border apprehensions at a 40-year low on the southern border, challenging the need for a national emergency over the border wall.
- Trump's actions likened to Hitler's declaration of national emergency after the Reichstag burned, with parallels drawn to fascism.
- Detailed elements of fascism discussed, including blending government and economic power, disdain for human rights, and obsession with nationalism and security.
- Constitution of the United States questioned in the context of Trump's potential national emergency declaration.
- The importance of Congress as a checks-and-balances system against tyranny emphasized, urging support for the constitution over Trump's actions.
- Choice presented between supporting the constitution or Trump, drawing comparisons to Germans facing Hitler's regime.
- Encouragement to choose patriotism by upholding the constitution rather than betraying it for personal interests.
- Historical references to August Landmesser and Sophie Scholl used to illustrate the significance of making the right choice in challenging times.

# Audience

Citizens, lawmakers, activists.

# On-the-ground actions from transcript

- Contact your representatives to voice opposition to a national emergency over the border wall. (implied)
- Educate others on the importance of Congress as a checks-and-balances system. (generated)
- Support organizations defending human rights and democracy. (suggested)

# Oneliner

In a thought-provoking comparison, Trump's potential national emergency mirrors Hitler's actions, urging a choice between patriotism and betrayal of the constitution.

# Quotes

- "You can support the constitution of the United States or you can support Mango Mussolini."
- "You're a patriot or a traitor."

# Whats missing

Deeper exploration of the historical context and consequences of betraying democratic principles.

# Tags

#BorderWall #NationalEmergency #Fascism #Constitution #ChecksAndBalances #Patriotism #Democracy