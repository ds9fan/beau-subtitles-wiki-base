```

# Bits

- A father live tweets mocking kids building Lego structures, including his own son, to appear tough and manly on Twitter.
- Bo criticizes the father for belittling creative skills like building robots, a valuable skill his son has developed.
- Bo suggests the father should apologize to his son and the other kids he mocked on Twitter.

# Audience

Parents, fathers, educators

# On-the-ground actions from transcript

- Reach out and apologize to the kids mocked on Twitter (suggested)
- Encourage and appreciate kids' creative skills and interests (implied)

# Oneliner

Father mocks kids' creativity to look tough, Bo urges him to apologize and appreciate valuable skills like building robots.

# Quotes

- "That's manly. That is masculine. Something out of nothing, sheer will bringing something into existence, that is cool."
- "At the end of the day, your kid's got a pretty valuable skill there already developed, and you're mocking it."

# Whats missing

The full video provides more context on toxic masculinity and the importance of supporting children's creative endeavors.

# Tags

#Parenting #ToxicMasculinity #Creativity #Apologize #SupportKids