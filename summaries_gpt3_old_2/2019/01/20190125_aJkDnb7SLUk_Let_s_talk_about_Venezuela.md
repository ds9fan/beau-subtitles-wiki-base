```

# Bits

- Bo discusses the US involvement in trying to start a coup in Venezuela for regime change.
- Low voter turnout in the 2018 election indicates that it wasn't rigged.
- The real motivation behind US intervention could be related to oil companies like Exxon and Chevron.
- US foreign policy is not genuinely driven by concerns for human rights.
- Bo questions the necessity of US intervention by comparing it to situations in the Philippines and Saudi Arabia.
- The potential consequences of a successful coup in Venezuela could lead to a refugee crisis.
- Bo challenges the argument against socialism in Venezuela, questioning the role of economic warfare in destabilizing the country.
- Bo suggests that the US should reconsider its involvement in Venezuela and showcases America's hypocrisy in foreign interference.

# Audience

Activists, policymakers, concerned citizens.

# On-the-ground actions from transcript

- Contact policymakers to express opposition to US intervention in Venezuela. (suggested)
- Support organizations working to address the humanitarian crisis in Venezuela. (suggested)
- Educate others about the complexities of foreign intervention and its consequences. (implied)

# Oneliner

Bo questions US motives in Venezuela, citing oil interests, hypocrisy, and potential refugee crises, urging reevaluation of involvement.

# Quotes

- "The real reason? Exxon and Chevron getting thrown out?"
- "US foreign policy is not genuinely driven by human rights."
- "Maybe this isn't our job."

# What's missing

Deeper exploration of the impact on Venezuelan citizens and potential solutions beyond critiquing US intervention.

# Tags

#Venezuela #USForeignPolicy #HumanRights #RefugeeCrisis #Socialism #Hypocrisy