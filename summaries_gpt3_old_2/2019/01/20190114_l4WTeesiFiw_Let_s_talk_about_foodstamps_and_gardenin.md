```

# Bits

- Snap benefits can be used to buy seeds, fruit trees, and bushes for growing food.
- More than 10% of Americans, including working families, rely on food stamps.
- Misconceptions about food stamp recipients include stereotypes and lack of understanding.
- The prevalence of food stamps is a result of a broken system, not individual faults.
- Growing your own food can be a beneficial solution for increasing access to fresh produce.
- Solutions for urban residents include vertical gardening, community gardens, and utilizing unused land.
- Building community networks through sharing and growing food together can provide food security.

# Audience

Gardeners, urban residents, community organizers, food security advocates.

# On-the-ground actions from transcript

- Utilize Snap benefits to purchase seeds, fruit trees, and bushes for growing food (suggested).
- Start vertical gardening using pallets or shoe organizers for small spaces (explained).
- Collaborate with church community centers or neighbors to set up community gardens (generated).
- Seek permission to grow food on someone else's property to increase access to fresh produce (implied).
- Plant apple trees or blueberry bushes for long-term food security and stability (implied).

# Oneliner

Use Snap benefits to grow food, dispel misconceptions about food stamps, and build community through urban gardening.

# Quotes

- "It's not what you think it is."
- "If you are doing well in this system, you might just want to eat this one."
- "Every little bit helps in that regard."
- "It's very hard to get out."
- "Throw seeds on the ground and spit on them and something grows."

# Whats missing

The full video may provide additional tips and practical advice on urban gardening and community building.

# Tags

#SnapBenefits #FoodStamps #UrbanGardening #CommunityBuilding #FoodSecurity #Misconceptions #GrowYourOwn #CommunityGardens #FreshProduce #BuildingNetworks
```