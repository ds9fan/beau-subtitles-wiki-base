```

# Bits

- President Trump's negotiation tactics involve offering protection to Dreamers and those in Temporary Protected Status (TPS) in exchange for $5.7 billion for his wall.
- Dreamers, slang for DACA recipients, are not a threat and are more American in heart and mind than many natural-born citizens.
- The president's offer of three years of protection for Dreamers and TPS individuals is seen as an act of good faith to achieve his own agenda.
- The president's tactics are likened to those of hostage takers, using the well-being of vulnerable groups as leverage for personal gain.
- The proposal to provide a path to citizenship for Dreamers is questioned, as the president is aware that they are not a detriment to the country.
- President Trump's actions are criticized as criminal and thuggery, with Congress and the Senate failing to hold him accountable.
- The transcript ends with a call to establish a path to citizenship for Dreamers and condemnation of the president's disruptive tactics.

# Audience

Congress members, Senate, activists

# On-the-ground actions from transcript

- Contact Congress to establish a path to citizenship for Dreamers (implied)
- Advocate for holding President Trump accountable for his actions (generated)
- Support organizations working towards protecting vulnerable immigrant populations (implied)

# Oneliner

President Trump's negotiation tactics with vulnerable immigrant groups are likened to those of a hostage taker, using their well-being as leverage for personal gain, while Congress and the Senate fail to hold him accountable.

# Quotes

- "The President of the United States is acting like a criminal, and he's using the guns of federal law enforcement as his soldiers in the street." 
- "Dreamers, that term is confusing because a lot of people think it has to do with the Dream Act. Well, the Dream Act never passed, that's not a thing."
- "The president knows they're not a detriment, the president knows they're not a threat, but that will not stop him from destroying their lives to get what he wants."

# What's missing

The full video may provide more context on the specific legislative actions needed to protect Dreamers and TPS recipients.

# Tags

#PresidentTrump #Immigration #HostageTakers #PathToCitizenship #Congress #Accountability
```