```

# Bits

- Bo stirred controversy comparing AOC to Ron Paul, sparking backlash from libertarian friends.
- Ron Paul was a libertarian congressman known as Dr. No for voting against anything not Constitution-authorized.
- AOC and Ron Paul differ in beliefs, with AOC focusing on corporate greed as a hindrance to the little guy.
- AOC suggests redistributing corporate profits to significantly increase employee pay.
- Bo questions the dichotomy of corporate vs. government establishments, suggesting it's establishment vs. the people.
- Bo sees potential for AOC to grow into a powerhouse like Ron Paul, but wonders if the Democratic Party establishment will allow it.

# Audience

Political activists, libertarians, progressives

# On-the-ground actions from transcript

- Contact local representatives to advocate for policies that address corporate greed and income inequality. (Generated)
- Organize community discussions on the role of government and corporations in society. (Implied)

# Oneliner

Bo compares AOC to Ron Paul, sparking debate on establishment politics and income inequality.

# Quotes

- "What if it's not corporate establishment versus government establishment? What if it's just establishment versus you?"
- "With a little bit of refinement and a little bit of time, she'll grow just like everybody does."
- "It can definitely happen."

# What's missing

In-depth analysis of the role of political establishments and potential for change in the current system.

# Tags

#PoliticalDebate #IncomeInequality #EstablishmentPolitics #AOC #RonPaul #ChangeIsPossible