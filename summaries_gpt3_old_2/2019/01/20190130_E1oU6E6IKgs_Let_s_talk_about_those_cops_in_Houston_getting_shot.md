```

# Bits

- Four cops shot during a drug raid in Houston due to potential lack of proper threat assessment.
- Focus on a statement made by the police union president threatening those critical of police.
- Offer for free training for 15 officers if the president resigns, promoting freedom of speech and accountability.
- Suggests that being critical of the government should not cast individuals as enemies.

# Audience

Police departments, community activists.

# On-the-ground actions from transcript

- Contact the group in Texas for free training for officers. (suggested)
- Advocate for accountability and freedom of speech within law enforcement. (generated)

# Oneliner

Four cops shot during drug raid, police union threatens critics, free training offered with condition of president's resignation.

# Quotes

- "If you don't want to be cast as the enemy, don't be the enemy of freedom."
- "You representing an armed branch of government do not get to tell people what they can and cannot talk about."

# What's missing

The impact of Joe Gimaldi's resignation on the police department and the training of officers.

# Tags

#PoliceTraining #FreedomOfSpeech #Accountability #CommunitySafety #HoustonShootings