```

# Bits

- Bo discusses the lack of recognition for cops killed in the line of duty, contrasting their anonymity with the high-profile cases of illegal immigrants killing officers.
- He challenges the narrative around immigration, pointing out the hypocrisy in how different cases are framed and perceived.
- Bo criticizes the politicization of tragedies to fit specific agendas and urges for a more nuanced understanding of complex issues.
- He delves into the racial undertones of discussions around immigration, citing examples of biased organizations and debunked studies.
- The comparison between reactions to different tragedies, such as Benghazi and incidents in Western Africa, showcases the selective outrage and narrative-driven responses in the media and public discourse.

# Audience

- Activists
- Law enforcement
- Advocates

# On-the-ground actions from transcript

- Advocate for fair and unbiased representation of all tragic incidents (implied)
- Challenge biased narratives and misinformation through education and dialogue (suggested)
- Support initiatives that address systemic issues in law enforcement and immigration policies (generated)

# Oneliner

Bo challenges biased narratives on cop killings and immigration, urging for fair representation and nuanced understanding of complex issues.

# Quotes

- "Stop standing on the graves, the corpses, the flag-covered coffins of people far more honorable than you to make a political point they might not agree with."
- "You're not outraged about a cop getting killed, you didn't know the names of the other ones."
- "It's numbers at that point. Not saying it's right. I'm saying that's the reality of it."
- "There's no way in the world that those places [biased organizations] are completely accurate, positive."
- "Reminds me a lot of Benghazi too. What happened in Benghazi?"

# What's missing

Insight into specific actions that can be taken to address the issues discussed and foster a more inclusive and informed dialogue.

# Tags

#LawEnforcement #Immigration #Narrative #Bias #Tragedies

```