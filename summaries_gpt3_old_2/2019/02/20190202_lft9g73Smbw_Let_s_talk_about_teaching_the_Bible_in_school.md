```

# Bits

- President Trump wants to put the Bible back in school, sparking discussions among students.
- Christian students don't support the idea, feeling they already receive enough religious teachings on Sundays.
- Advocates for teaching religious texts in schools, focusing on philosophical aspects rather than enforcing moral codes.
- Stresses the importance of religious tolerance and learning from various religious texts.
- Mentions teachings from the Quran, Torah, Buddhism, Hinduism, and Christianity to showcase common moral values.
- Points out the hypocrisy within Christianity, especially in politics, as a reason for its decline.
- Argues that understanding and teaching various religious texts may reduce division and increase tolerance.
- Asserts that the Church of Satan has successfully demanded their texts be taught in schools to uphold religious freedom.
- Advocates for teaching philosophy in schools to encourage critical thinking and understanding different perspectives.

# Audience

Educators, school administrators, religious leaders

# On-the-ground actions from transcript

- Contact school boards to advocate for teaching religious texts with a focus on philosophical aspects (advocated)
- Organize discussions or workshops on religious tolerance and understanding different faiths (generated)
- Join efforts to incorporate philosophy education in school curriculums (implied)

# Oneliner

Teaching religious texts in schools should focus on philosophy, not indoctrination, to encourage tolerance and critical thinking.

# Quotes

- "Philosophy is something that is sorely lacking in this country. People don't think."
- "The hypocrisy is so blatant you could choke on it."
- "If you don't follow your own code of ethics, why on earth you expect someone else to?"
- "The goal of education should not be to teach you what to think. It should be to teach you how to think."
- "Today's Church of Satan embodies most of the ideals of Christianity better than most Christian churches."

# What's missing

Discussion on the practical steps individuals can take to advocate for inclusive religious education.

# Tags

#ReligiousEducation #Philosophy #Tolerance #Teaching #CriticalThinking