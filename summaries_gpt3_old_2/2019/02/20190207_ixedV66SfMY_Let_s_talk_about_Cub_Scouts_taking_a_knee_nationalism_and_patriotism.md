```

# Bits

- A ten-year-old Cub Scout took a knee during the Pledge of Allegiance, challenging the blend of racism and nationalism in the country (implied).
- People struggle to fit their usual responses to this unique situation (generated).
- The young boy's actions demonstrate independent thought, self-reliance, and a rejection of blind nationalism (implied).
- Patriotism is about correcting your country, not blind obedience, which the Scout embodies (implied).
- The speaker, a former Scout, praises the boy's actions and sees hope in the next generation challenging racism (implied).

# Audience

Parents, educators, community leaders

# On-the-ground actions from transcript

- Contact local Scout groups to discuss promoting independent thought and standing up against injustice (generated).
- Support young individuals who take a stand against racism and nationalism (suggested).

# Oneliner

A ten-year-old Scout challenges racism and nationalism by taking a knee during the Pledge of Allegiance, embodying true patriotism.

# Quotes

- "Patriotism is correcting your country when it's wrong. It's not blind obedience, that's nationalism."
- "That kid has it down, he's got it. He's embodying the Scouts and he's embodying patriotism at 10."

# What's missing

The full video can provide more insights on the reactions and discussions around the young Scout's actions.

# Tags

#Scout #Racism #Nationalism #Patriotism #CommunityChallenge #YouthActivism