```

# Bits

- The warrant for the Houston shooting was entirely fabricated, with confidential informants never buying heroin from the house. (what, when, why)
- The officer involved had heroin in his car, raising suspicion about other possible drugs. (who, what, why)
- Houston Police Department targeted an independent journalist in 2016, leading to surveillance and trumped-up charges. (who, when, what)
- The concept of the thin blue line extends to journalists cultivating sources within government agencies. (what, why)
- Public face of the police chief deflects blame, portraying the incident as isolated and demonizing the victim. (who, what, dynamics)
- Private face suggests tearing apart cases beyond just the officers involved in the raid, indicating a broader issue within the division. (what, dynamics)
- Calls for bringing in the FBI to conduct a thorough investigation free from loyalty biases within the police department. (what, why)
- Questions raised about the origin of heroin, potential evidence tampering, and payment of confidential informants. (what, why)
- Encouragement to stop demonizing the victim and acknowledge the lack of substantial evidence for the raid. (what, why)
- Overall, a call for transparency and accountability within the Houston Police Department. (what, why)

# Audience

Police reform activists, investigative journalists, Houston residents

# On-the-ground actions from transcript

- Contact local representatives to demand an independent investigation into the Houston Police Department. (suggested)
- Support independent journalists and whistleblowers facing harassment or surveillance. (suggested)
- Engage with local communities to raise awareness about police misconduct and demand accountability. (suggested)

# Oneliner

Houston shooting reveals fabricated warrant and heroin found in officer's car, prompting calls for FBI investigation and end to victim demonization.

# Quotes

- "Please stop demonizing the victim."
- "You need to bring in the FBI because there's going to be a whole bunch of questions you don't even want to ask, much less know the answer to."
- "Where did the heroin come from? Did it come out of the evidence locker?"

# What's missing

Insights on specific actions individuals can take to support investigative journalism and police accountability efforts.

# Tags

#HoustonPolice #FabricatedWarrant #PoliceAccountability #FBI #VictimDemonization #IndependentJournalism

```