```

# Bits

- A man in Florida is victimized and threatened not to go to the cops because he is undocumented, showcasing the need for sanctuary cities to protect victims of violent crimes. (who, what, when, why)
- Sanctuary cities exist to allow victims of violent crimes who are undocumented to report to local law enforcement without fear of deportation, ensuring they are treated as human beings with rights. (what, why, dynamics)
- Local law enforcement walking around like the Gestapo asking for papers prevents victims from seeking help, leading to the necessity of sanctuary cities. (why, dynamics)
- The speaker criticizes the idea that undocumented workers in sanctuary cities do not assimilate, countering with statistics that show lower violent crime rates among illegal immigrants compared to native-born individuals. (what, why)
- Private prisons may be pushing for a shift from the war on drugs to a war on immigration for financial gain, as they anticipate marijuana legalization and seek profit from non-violent immigration violations. (why, dynamics)

# Audience

- Advocates for immigrant rights

# On-the-ground actions from transcript

- Contact local representatives to support and advocate for sanctuary cities. (implied)
- Educate communities on the importance of sanctuary cities in protecting victims of violent crimes, regardless of immigration status. (implied)
- Support organizations that provide resources and aid to undocumented individuals facing threats or victimization. (suggested)

# Oneliner

Sanctuary cities are vital to ensure undocumented victims of violent crimes can seek help without fear of deportation, protecting their rights and humanity.

# Quotes

- "Sanctuary cities exist because if the cops, local cops, have to run everybody that they won't run into through ICE's database, [...] victims of violent crime can't go to the cops without fear of being deported." 
- "No reason for your local deputy or your local cop to walk around like the Gestapo and further enforce the idea that undocumented workers are subhuman and have no rights."

# What's missing

Insights on how individuals can directly support and engage with local sanctuary city initiatives.

# Tags

#SanctuaryCities #ImmigrantRights #VictimsProtection #CommunityAction #Advocacy
```