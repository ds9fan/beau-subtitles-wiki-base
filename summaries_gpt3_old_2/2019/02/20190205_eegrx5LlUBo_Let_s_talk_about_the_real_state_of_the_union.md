```

# Bits

- The United States is politically divided, comparable to desegregation era.
- Trump's wall dominates headlines despite low priority for Americans.
- $3.4 billion spent on lobbying and corruption.
- News focuses on emotion rather than information.
- Issues include family separations, environment destruction, drug war, and systemic racism.
- Efforts to discredit and mock youth of the country.
- Increase in hate crimes and presence of literal Nazis.
- Future outlook: Americans fighting against injustices, researching, and taking action to combat issues.
- Communities strengthening themselves from within, reducing the power of DC politicians.
- Independent journalists globally working to discern fact from fiction.
- Defiance against tyranny through creating alternative systems silently.

# Audience

Activists, concerned citizens, community organizers

# On-the-ground actions from transcript

- Join independent journalism efforts (generated)
- Research environmental impact of corporations (suggested)
- Support domestic violence shelters financially (implied)
- Speak out against systemic racism (generated)
- Engage in community building and strengthening (suggested)

# Oneliner

In a politically divided America, defiance is rising against oppression through community action and independent journalism.

# Quotes

- "We don't defeat tyranny and oppression by complying with it while we fight it. We defeat it by ignoring it while we quietly create our own systems to replace it."
- "The state of the Union? I don't have a clue. The state of those people who care? Well, they're in a state of defiance."

# Whats missing

The emotional impact of the current state of the union and the importance of individual action in creating change.

# Tags

#StateOfTheUnion #PoliticalDivision #CommunityAction #Defiance #IndependentJournalism