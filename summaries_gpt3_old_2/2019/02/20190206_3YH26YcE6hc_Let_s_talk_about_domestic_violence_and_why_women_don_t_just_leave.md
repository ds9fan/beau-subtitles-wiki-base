```

# Bits

- Domestic violence affects everyone, regardless of race, gender, or class.
- Leaving an abusive relationship is not as simple as it sounds.
- Domestic violence shelters lower the risk of violence and solve practical problems like finances, housing, and pets.
- 78% of women killed at work from 2003 to 2008 were murdered by a former or current intimate partner.
- 15% of all violent crime in the US is intimate partner violence.
- Shelter House of Northwest Florida is a forward-thinking organization addressing domestic violence.
- They provide safe housing for victims and even address the stigma associated with domestic violence.
- Donations can be made to support organizations like Shelter House of Northwest Florida, including old cell phones for communication.
- Friends and family may not be safe options for victims due to the risk of violence.
- Kits provided by organizations can support rape victims with fresh clothes after undergoing traumatic experiences.

# Audience

Supporters of domestic violence victims.

# On-the-ground actions from transcript

- Contact Shelter House of Northwest Florida for support or donation (generated)
- Donate money or old cell phones to organizations supporting domestic violence victims (generated)
- Avoid suggesting friends' houses as safe options for victims (implied)
- Sponsor a family or support organizations helping victims of domestic violence (suggested)

# Oneliner

Domestic violence affects everyone, and leaving an abusive relationship is not as simple as it seems; support organizations like Shelter House of Northwest Florida to provide practical help.

# Quotes

- "Domestic violence is a huge issue in the United States."
- "Any obstacle that's standing in the way, they get rid of it."
- "It means nothing to you could save someone's life."
- "Normally by the time the abused calls the police they've been assaulted 35 times anyway."

# What's missing

In-depth personal stories or survivor testimonies could provide a deeper understanding of the impact of domestic violence.

# Tags

#DomesticViolence #SupportVictims #Donate #ShelterHouse #RaiseAwareness #TakeAction
```