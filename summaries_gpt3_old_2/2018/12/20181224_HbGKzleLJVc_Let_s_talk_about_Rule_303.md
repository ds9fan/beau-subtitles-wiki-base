```

# Bits

- Rule 303 originated as a term used by military contractors during the Iraq and Afghanistan wars and made its way to law enforcement.
- Rule 303 was based on the caliber of a rifle, symbolizing the responsibility to act when one has the means at hand.
- Rule 303 has evolved into "might makes right" in law enforcement, contrary to its original meaning of duty and responsibility.
- Examples of Rule 303 include the response to an ambush in Western Africa and the movie Tears of the Sun.
- Discusses the duty of law enforcement officers to act, especially in situations like the Parkland shooting where duty to protect was debated.
- Addresses excuses made by law enforcement officers who fail to act, such as low pay, lack of proper equipment, and fear of risk.
- Argues that a school resource officer's duty is to protect students, regardless of personal risk, and challenges officers to reconsider their roles if they cannot fulfill this duty.


# Audience

Law enforcement officers, school resource officers, military contractors


# On-the-ground actions from transcript

- Contact local law enforcement agencies to discuss training and protocols for handling active shooter situations (implied)
- Join community discussions on the responsibilities of law enforcement officers in protecting citizens (generated)
- Organize a workshop on risk assessment and duty to act for law enforcement officers (suggested)


# Oneliner

Rule 303: From military contractors to law enforcement, the responsibility to act when you have the means at hand, despite risk or excuses.


# Quotes

- "Your duty is not bounded by risk."
- "Nobody cares if you go home at the end of your shift after a school shooting."
- "If you cannot truly envision yourself laying down your life to protect those kids, you need to get a different assignment."


# Whats missing

Discussion on the broader societal implications of law enforcement officers failing to act in critical situations.


# Tags

#DutyToAct #LawEnforcement #Responsibility #Risk #Protection