```

# Bits

- Globalism vs nationalism: motivations and implications.
- Globalist used as code in extremist circles.
- Nationalism and globalism both keep people kicking down instead of punching up.
- Questioning the true representation in democracy.
- Rejecting both nationalism and globalism, advocating for a better system.
- Nationalists seen as low-ambition globalists.
- Power dynamics and buzzword use in political discourse.

# Audience

Political activists, community organizers.

# On-the-ground actions from transcript

- Question the representation of elected officials (implied)
- Advocate for systems that empower people (suggested)
- Challenge stereotypes and divisive rhetoric (generated)

# Oneliner

Globalism vs nationalism: how power dynamics shape political discourse, keeping people kicking down instead of punching up.

# Quotes

- "It's just a method of motivating people through different means to kick down instead of punching up."
- "I think there are probably better ways to run the world than in trusting the few to make decisions for everybody."
- "All a nationalist is, is a low-ambition globalist."
- "Once you have reached the pinnacle of power in your country, well, you want more."
- "People with identical ideologies to the one they're criticizing use it as an insult."

# Whats missing

Deeper exploration of alternative systems to nationalism and globalism.

# Tags

#Globalism #Nationalism #PowerDynamics #PoliticalDiscourse #Advocacy