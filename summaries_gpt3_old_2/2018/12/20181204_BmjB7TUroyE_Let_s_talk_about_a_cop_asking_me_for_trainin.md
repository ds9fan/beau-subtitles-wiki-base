# Bits

- Bo shares a story about an encounter with law enforcement officers during his training session, revealing concerning attitudes and practices within the police force.
- He discusses the importance of concepts like time, distance, and cover in police work to prevent unnecessary harm to individuals.
- Bo addresses issues such as auditory exclusion and the dangers of miscommunication and excessive force in law enforcement encounters.
- He advises officers to wear protective gear, stay behind cover in shootouts, and avoid shooting out of fear or stress.
- Bo recounts a personal anecdote about the significance of understanding the spirit, rather than just the letter, of the law in potentially dangerous situations.

# Audience

Law enforcement officers, police departments.

# On-the-ground actions from transcript

- Implement time, distance, and cover principles in police work to prevent unnecessary harm (generated).
- Educate officers about auditory exclusion and its impacts on perception in high-stress situations (implied).
- Ensure officers wear protective vests for their safety in potentially dangerous encounters (suggested).
- Encourage officers to stay behind cover in shootouts to avoid unnecessary risks (implied).
- Understand the spirit of the law in addition to the letter, especially in sensitive situations (generated).

# Oneliner

Bo shares insights on police training, urging officers to prioritize safety protocols and understand the nuances of the law for effective and humane law enforcement.

# Quotes

- "Time, distance, and cover. Please start to use it."
- "Positional asphyxiation. For you guys, this is what you do."
- "The most dangerous people on the planet are true believers."

# What's missing

Deeper exploration of community perspectives on interactions with law enforcement and the impact of police training on public trust and safety.

# Tags

#PoliceTraining #LawEnforcement #SafetyProtocols #SpiritOfTheLaw #CommunityTrust