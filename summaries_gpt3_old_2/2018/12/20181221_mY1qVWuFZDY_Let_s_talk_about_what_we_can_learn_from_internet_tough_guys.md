```

# Bits

- Internet tough guys as a symptom and analogy for American foreign policy, both insulated from consequences (analyzed)
- Americans insulated from consequences of their actions due to the military machine (explained)
- Discussion on Americans' views on foreign aid and involvement (discussed)
- Importance of wealth and income in affecting change globally (explained)
- Money as a catalyst for change and the role of the wealthy in addressing global issues (explained)
- Calls for individual responsibility in affecting change through consumer choices (stressed)
- Need for global revolution rather than isolated movements (discussed)

# Audience

Global citizens, activists, consumers

# On-the-ground actions from transcript

- Contact local representatives to address foreign policy and military actions (generated)
- Educate oneself on global income disparities and wealth distribution (suggested)
- Support ethical and sustainable companies through purchasing decisions (implied)
- Engage in discussions and movements for global change (generated)

# Oneliner

Internet tough guys and American foreign policy serve as analogies for insulation from consequences, urging individual responsibility for global change.

# Quotes

- "Money makes the world go round, right?"
- "We are telling those massive corporations what kind of world we want, we're voting with our debit cards."
- "Any revolution that is not global, it's not a revolution."
- "It has to start with you."

# Whats missing

Deeper insights into specific actions individuals can take to address global issues and foster change.

# Tags

#GlobalChange #WealthDisparity #Responsibility #ConsumerActivism #Revolution