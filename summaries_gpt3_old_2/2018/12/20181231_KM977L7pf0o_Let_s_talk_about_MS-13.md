```

# Bits

- MS-13, often viewed as a boogeyman, has its roots in the aftermath of a coup in El Salvador in 1979.
- The gang was formed by refugees in California who found safety in numbers, evolving from a minor street gang to a transnational criminal enterprise.
- Key factors in MS-13's growth were the return of members to El Salvador post-Civil War and the influence of a US military-trained leader.
- The US military played a significant role in training and shaping the leadership of MS-13, contributing to its evolution.
- The narrative questions the effectiveness of a border wall in addressing issues like MS-13, suggesting a reevaluation of American foreign policy instead.
- Calls for stricter scrutiny and vetting not only for asylum seekers but also for individuals trained by the US military to engage in violence and oppression.


# Audience

Policy makers, activists, researchers.


# On-the-ground actions from transcript

- Advocate for policy reforms in American foreign policy (generated)
- Support organizations working towards comprehensive immigration reform (suggested)
- Research and raise awareness about the impacts of US military training abroad (implied)


# Oneliner

MS-13's roots in US foreign policy and military training underscore the need for a reevaluation of approaches to address transnational criminal organizations.


# Quotes

- "MS-13 is an American creation, not just in the sense that it was founded here, but in the sense that every step of the way, it was American foreign policy that built it."
- "Maybe a better idea is to curtail our foreign policy instead of building a wall."
- "You never hear them calling for strict scrutiny or vetting of those people that the U.S. military trains to murder, torture, assassinate, and dominate a community by fear."


# Whats missing

Further insights on the specific impact of MS-13 on communities and potential solutions beyond policy changes.


# Tags

#MS13 #USForeignPolicy #Immigration #USMilitaryTraining #PolicyChange
```