# Bits

- Bo discusses the concept of revolution and differentiates between riots, rebellions, and true revolutions.
- He brings up the Yellow Vest Movement and its origins as a riot, not a revolution.
- Bo mentions the need for a new idea and form of government to define a revolution.
- He talks about the impact of outside influence on revolutions, giving examples like Iran and the US revolution.
- Bo warns about the potential consequences of a violent revolution in the US, citing ethnic tensions and cultural divides.
- He explains how a violent revolution could lead to severe disruptions in the supply chain, affecting food, fuel, and medical supplies.
- Bo advocates for focusing on non-violent means to bring about change and suggests exploring new governmental forms that empower more people.

# Audience

- Activists
- Community organizers
- Policy advocates

# On-the-ground actions from transcript

- Organize a discussion on non-violent approaches to political change. (implied)
- Research and propose governmental structures that empower more people. (implied)
- Prepare emergency supplies in case of disruptions in the supply chain. (generated)

# Oneliner

Bo delves into the complexities of revolution, warning against violent uprisings in the US and advocating for non-violent change through new ideas and government structures.

# Quotes

- "There's two ways to change somebody's mind. You can put a bullet in it, or you can put a new idea in it." 
- "If you're going to change the government, it can be done with bullets and lamp posts."
- "A revolution is bringing a new idea to the table."
- "In the United States, a violent revolution will be horrible."
- "What kind of governments could give more power to more people?"

# What's missing

In-depth analysis of historical revolutions and their impacts on society.

# Tags

#Revolution #ViolentUprisings #GovernmentChange #NonViolence #Empowerment