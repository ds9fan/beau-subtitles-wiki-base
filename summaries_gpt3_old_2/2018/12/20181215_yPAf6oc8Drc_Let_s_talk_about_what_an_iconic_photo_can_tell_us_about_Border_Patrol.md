```

# Bits

- Bo shares a story from the Vietnam War about humanity in the midst of conflict.
- Discusses Border Patrol agents dumping out water left by volunteers for migrants.
- Condemns the inhumanity and lack of compassion in Border Patrol actions.
- Urges Border Patrol agents to quit and take accountability for their actions.
- Talks about the futility of deterrence and the resilience of the human spirit in seeking safety and freedom.
- Calls out the wrongdoing of Border Patrol agents and the consequences they may face in the future.

# Audience

Border Patrol agents, volunteers aiding migrants.

# On-the-ground actions from transcript

- Quit your job at Border Patrol. ( urged, exemplified )
- Support organizations leaving water for migrants. ( exemplified )
- Advocate for humane treatment of migrants. ( exemplified )

# Oneliner

Bo reflects on humanity in conflict and condemns Border Patrol's inhumane actions, urging accountability and compassion.

# Quotes

- "You cannot deter the drive for safety, for freedom, for opportunities."
- "Your badge is not going to protect you from depraved indifference."
- "Just following orders isn't going to cut it. Last time people used that defense, we hung them."

# Whats missing

Full context and detailed examples of Border Patrol actions.

# Tags

#Humanity #BorderPatrol #Accountability #Migrants #Compassion