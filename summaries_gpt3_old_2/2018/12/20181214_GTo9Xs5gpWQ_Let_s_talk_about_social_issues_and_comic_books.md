```

# Bits

- Bo addresses criticism for using Elf on the Shelf to discuss social issues, comparing it to how feminists are criticized for addressing social issues in comics.
- Bo discusses how comic books like Punisher and G.I. Joe actually address social issues and feminism in their themes and characters.
- He points out the strong feminist undercurrent in G.I. Joe and how it addresses social issues throughout the series.
- Bo talks about how even comics like X-Men and Superman have central themes related to civil rights and social issues.
- He argues that social issues have always been present in comics, but some audiences have been too dense to notice them.
- Bo comments on the reactions to his use of Elf on the Shelf, contrasting the outrage with the lack of attention given to real social issues.
- He brings up the story of Libertad and Esperanza as a true story that reflects the harsh realities faced by many.

# Audience

Comic book enthusiasts, social activists.

# On-the-ground actions from transcript

- Start a conversation about how comic books address social issues (generated)
- Analyze and discuss the themes of feminism and social issues in comic books (implied)
- Engage in dialogue about the representation of social issues in popular culture (generated)

# Oneliner

Bo discusses the intersection of social issues and comics, challenging audiences to rethink their perceptions.

# Quotes

- "Social issues have always been in comics. You were just too dense to catch it."
- "If you have a privilege for so long, and you start to lose it, it feels like oppression."
- "Those two characters are now more human than the actual humans who are going through this down at the border."
- "This is what happens on a daily basis."
- "Y'all have a good night."

# What's missing

Deeper exploration of specific examples from comics and how they address social issues.

# Tags

#Comics #SocialIssues #Feminism #Representation #ElfOnTheShelf #LibertadAndEsperanza