```

# Bits

- Beau discusses the issue of trophy hunting, specifically targeting exotic animals.
- Trophy hunting is often done as a status symbol among the wealthy.
- The fees paid for canned hunts do contribute to conservation efforts, but they also fuel market demand for artifacts, contributing to poaching.
- Beau proposes an alternative to hunting: investing the hunting fees in the local community for economic development.
- He suggests volunteering with anti-poaching task forces as a more meaningful way to engage with conservation efforts.
- Beau recommends following Kristen Davis, an advocate for animal rights in Africa, particularly elephants.

# Audience

Conservationists, animal rights advocates, hunters

# On-the-ground actions from transcript

- Invest hunting fees in local communities for economic development (suggested)
- Volunteer with anti-poaching task forces (suggested)
- Support organizations like the David Sheldrick Wildlife Trust (suggested)

# Oneliner

Beau challenges the ethics of trophy hunting, suggesting investing hunting fees in local communities for conservation and economic growth.

# Quotes

- "If it was about conservation, they could do the safari with a camera or an icon. Go photograph it."
- "The thing is, my alternative satisfies everybody."
- "They don't want aid, they want business."
- "You want to hunt dangerous game? Hunt the most dangerous animal of all."
- "Kristen Davis is an amazing advocate for animals in Africa."

# What's missing

In-depth discussion on the impact of trophy hunting on local communities and ecosystems. 

# Tags

#TrophyHunting #Conservation #AnimalRights #CommunityDevelopment #AntiPoaching #DavidSheldrickWildlifeTrust