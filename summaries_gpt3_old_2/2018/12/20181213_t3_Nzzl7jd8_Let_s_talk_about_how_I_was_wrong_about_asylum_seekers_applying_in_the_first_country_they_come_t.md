```

# Bits

- Bo admits to being wrong about asylum seekers having to apply for asylum in the first country they come to, after finding information on the Safe Third Country Agreement. (admission, correction, discovery)
- The Safe Third Country Agreement is a treaty between the United States and Canada that states asylum seekers must apply for asylum in the first country they arrive in. (treaty, bilateral, application process)
- The agreement does not apply to asylum seekers coming from Mexico, and there are discussions within Canada about scrapping the agreement due to the United States not meeting the safety requirements. (scrapping, safety concerns, bilateral implications)
- President Trump attempted to designate Mexico as a safe third country, but this was met with ridicule due to Mexico not meeting the safety standards outlined in the agreement. (Trump's attempt, safety standards, international response)
- People's misconception about asylum application processes had experts puzzled and debating where this idea originated from. (misconception, expert puzzlement, origin debate)

# Audience

- Policy researchers
- Immigration advocates
- Law students

# On-the-ground actions from transcript

- Contact immigration advocacy groups to understand the implications of the Safe Third Country Agreement. (implied)
- Join discussions on asylum policies to advocate for fair treatment of asylum seekers. (generated)
- Organize a workshop to educate others on the complexities of international asylum laws. (suggested)

# Oneliner

Bo admits being wrong about asylum application rules and sheds light on the Safe Third Country Agreement between the US and Canada, revealing its limited scope.

# Quotes

- "Don't get your legal information from memes."
- "Knowledge is power. Don't get your legal information from memes."
- "People that know immigration law were so dumbfounded by this widespread belief."
- "President Trump is completely aware of this."
- "Unless you're moving along the U.S.-Canadian border."

# What's missing

Insights on the specific implications for asylum seekers under the Safe Third Country Agreement.

# Tags

#ImmigrationPolicy #AsylumSeekers #SafeThirdCountryAgreement #USCanadaRelations #Misconceptions