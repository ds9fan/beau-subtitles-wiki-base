```

# Bits

- Duty as a prevalent theme in recent news, from court rulings on law enforcement's duty to protect citizens to General Mattis's resignation driven by duty.
- General Mattis's reputation for honor, integrity, and duty among Marines, showcasing the Marine Corps's values.
- Mattis's resignation due to a sense of duty conflicting with geopolitical concerns, particularly regarding leaving allies like the Kurdish people in Syria.
- The Kurdish people's unique identity and spread across countries like Turkey, Syria, Iraq, and Iran, and the implications of the US withdrawal on their bid for independence.
- The absence of a duty for law enforcement to protect citizens, even in extreme circumstances like mass shootings, reflecting on the government's role in controlling rather than ensuring protection.

# Audience

- Activists
- Law enforcement officers
- Advocates for Kurdish rights

# On-the-ground actions from transcript

- Contact local representatives to advocate for legislation that empowers citizens to protect themselves (implied).
- Join advocacy groups supporting Kurdish rights and independence (generated).
- Organize community discussions on the role of law enforcement and government in citizen protection (suggested).
- Support organizations providing aid to vulnerable groups like the Kurdish people (implied).

# Oneliner

From exploring duty in recent news to the lack of obligation for law enforcement to protect citizens, this transcript delves into complex themes with real-world implications.

# Quotes

- "Law enforcement is not there to protect you. We do not live under a protectorment." 
- "Duty to continue that fight, as Mattis felt he had a duty not to leave a man behind."
- "The US will not get involved in that fight again. So in a weird way, this may be the best shot for Kurdish independence."

# What's missing

Deeper insights into the geopolitical implications of the US withdrawal from Syria and the potential consequences for the Kurdish people could be gained from watching the full transcript.

# Tags

#Duty #LawEnforcement #KurdishRights #Advocacy #GovernmentControl
```