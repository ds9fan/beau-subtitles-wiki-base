```

# Bits

- Bo Ginn on the Mexican border with his militia commander to "protect America" without breaking the law. (who, where, what)
- Not stopping refugees on the Mexican side or sending them back at gunpoint due to legal and ethical reasons. (what, why)
- Backing up DHS and hanging out with them, despite being labeled as potential terrorists by the government agency. (what, dynamics)
- Acknowledging the risk of revealing their operations if they ever follow through with extreme rhetoric. (what, dynamics)
- Mentioning the importance of optics and looking good while protecting America with guns. (why, how)


# Audience

Militia members, community organizers, activists


# On-the-ground actions from transcript

- Contact local community organizations to support asylum seekers in a legal and ethical manner. (generated)
- Join advocacy groups working towards comprehensive immigration reform. (generated)


# Oneliner

On the Mexican border, Bo Ginn and his militia navigate legal and ethical dilemmas while "protecting America" with guns.


# Quotes

- "We're down here on the border protecting America with our guns, ready to kill a bunch of unarmed asylum seekers who are following the law."
- "They're going to know exactly who we are and how we operate."


# What's missing

Deeper insight into the potential consequences of extremist actions.


# Tags

#BorderSecurity #Ethics #Immigration #CommunityAction #Advocacy