```

# Bits

- President cutting off aid to Central America for not stopping people from leaving is scary.
- President believes head of state's job is to keep people from leaving their country.
- Wall along the southern border can be used to keep Americans in.
- The slow creep of tyranny is starting to show its face.
- Supporting ideals of freedom and liberty means not supporting actions like cutting off aid to keep people in.
- The wall is seen as a prison wall, not just a border wall.

# Audience

- Citizens concerned about freedom and tyranny.

# On-the-ground actions from transcript

- Contact local representatives to express concerns about cutting off aid to Central America. (suggested)
- Join or support organizations working to protect civil liberties and human rights. (implied)

# Oneliner

The President's actions to cut off aid raise concerns about creeping tyranny and the potential use of a border wall to keep Americans in.

# Quotes

- "That wall is a prison wall. It's not a border wall."
- "Supporting ideals of freedom and liberty means not supporting actions like cutting off aid to keep people in."

# Whats missing

Detailed examples of how the wall could be used to restrict movement within the country.

# Tags

#Tyranny #Freedom #BorderWall #CivilLiberties #HumanRights #CentralAmerica