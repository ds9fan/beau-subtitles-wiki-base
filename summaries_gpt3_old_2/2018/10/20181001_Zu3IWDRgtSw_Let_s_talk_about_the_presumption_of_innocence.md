```

# Bits

- Comment section filled with discussion on presumption of innocence after video on sexual assault and Kavanaugh (who, what)
- Kavanaugh's support for law enforcement stopping people on the street for identification and search, NSA collecting metadata warrantlessly, complicity in indefinite detention of innocent people (who, what)
- Importance of presumption of innocence in American justice system (why)
- Concern over party politics leading to signing away rights and potential consequences of executive power in the hands of another party (why)
- Warning against blindly supporting candidates without understanding their rulings or implications for constitutional rights (why)

# Audience

Advocates, activists, voters

# On-the-ground actions from transcript

- Contact elected officials to express concerns about nominees' stances on civil rights and liberties (suggested)
- Educate others on the implications of supporting candidates without considering their track record on constitutional rights (implied)

# Oneliner

Discussion on presumption of innocence and the importance of understanding candidates' stances on civil liberties to prevent potential consequences of party politics.

# Quotes

- "You're trading away your country for a red hat."
- "Don't expect anybody who knows anything about this man's rulings or knows anything about the Constitution to believe a word you say."

# What's missing

Deeper analysis on the impact of party politics on judicial appointments and civil liberties.

# Tags

#PresumptionOfInnocence #CivilLiberties #PartyPolitics #JudicialAppointments #Constitution