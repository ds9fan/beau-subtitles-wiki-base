```

# Bits

- President of the United States mocked sexual assault survivors on TV, setting a shockingly low bar.
- Effort to silence sexual assault survivors led by the President.
- Addressing the dangerous narrative of sexual predators being seen as monsters.
- Comparison between the perception of Nazis as monsters and the reality that they were just people.
- Warning against being passive bystanders in the face of evil or injustice.

# Audience
Advocates, Activists, Educators

# On-the-ground actions from transcript

- Contact local advocacy groups to support sexual assault survivors (suggested)
- Organize or participate in discussions on changing harmful narratives around sexual assault (generated)
- Support education on consent and respect in schools and communities (generated)

# Oneliner
President's actions normalize harmful behavior, challenging society's perception of sexual assault.

# Quotes
- "The scariest Nazi wasn't a monster. He was your neighbor."
- "The scariest rapist or rape apologist, well they're your neighbor too."
- "There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim."
- "People said the same thing about Nazis. They were monsters. They weren't. They're just people."
- "That speaks to something that's really dangerous."

# What's missing
In-depth analysis of the impact of societal perceptions on sexual assault survivors.

# Tags
#SexualViolence #ToxicNarratives #Advocacy #SocialJustice #HarmfulNorms 

```