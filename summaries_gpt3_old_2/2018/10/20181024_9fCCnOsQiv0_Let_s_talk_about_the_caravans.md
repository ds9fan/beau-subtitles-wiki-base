```

# Bits

- Caravans have been coming for 15 years due to US drug war policy and interventions in other countries.
- Caravan from San Pedro Sula fleeing high violence with a murder rate 159 per 100,000, compared to Chicago's 15-20 per 100,000.
- Office of Refugee Resettlement spends half a billion dollars a year to help migrants get on their feet.
- It's cheaper and more humane to help migrants rather than detain them.
- Migrants travel in large groups for safety, similar to trick-or-treating in numbers.
- Carrying other countries' flags in the caravan allows people to connect with fellow nationals.
- Wearing military fatigues is practical due to durability for long journeys on foot.
- Claiming asylum at the border is legal and the appropriate way to seek asylum in the US.
- Blaming migrants for fleeing dangerous conditions is misguided; it's influenced by US foreign policy and drug war impact.
- Sending migrants back to their deaths goes against preaching freedom and humanity.

# Audience

Advocates, policymakers, community organizers

# On-the-ground actions from transcript

- Contact your representative to discuss how US foreign policy impacts other countries (generated)
- Advocate for a humane approach towards migrants and asylum seekers (implied)
- Educate others on the realities faced by migrants rather than blaming them (suggested)

# Oneliner

Caravans fleeing violence for years due to US policy; help them, don't blame them.

# Quotes

- "It's cheaper to be a good person."
- "They are doing it legally. That's how the law works."
- "Sending people back to their deaths for no good reason other than fear is not what the US stands for."

# Whats missing

Deeper insights into the root causes of migration and potential solutions.

# Tags

#Immigration #USPolicy #HumanitarianAid #Advocacy #AsylumSeekers #ForeignPolicy