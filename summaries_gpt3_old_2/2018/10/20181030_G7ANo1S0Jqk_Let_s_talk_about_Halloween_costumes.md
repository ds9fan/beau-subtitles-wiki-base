```

# Bits

- Bo discusses the consequences of offensive Halloween costumes, including blackface and culturally insensitive outfits.
- Being offensive is not a substitute for having a sense of humor.
- He criticizes those who use edginess as an excuse for offensive behavior, especially targeting marginalized groups.
- Bo addresses the shift from anonymity on the internet to real-life consequences for online actions.
- He advocates for being a good person rather than just aiming to shock or offend.
- Bo challenges the idea that political correctness is limiting freedom, stating that it's about changing flawed thinking.
- He warns that actions have consequences, and people will be remembered for their behavior.

# Audience

Individuals, Social Media Users, Educators

# On-the-ground actions from transcript

- Speak out against offensive behavior (implied)
- Educate others on the impact of insensitive costumes (suggested)
- Support and uplift marginalized communities (implied)
- Foster a culture of respect and empathy (generated)

# Oneliner

Bo discusses the consequences of offensive Halloween costumes, challenges the notion that edginess excuses offensive behavior, and advocates for being a good person over seeking shock value.

# Quotes

- "Being offensive and edgy is not actually a substitute for having a sense of humor."
- "Nobody's going back to that. Nobody's going to think that's okay."
- "You can do what you want, it's just a joke. But you can't be mad when you suffer the consequences of your actions."

# Whats missing

The full transcript provides a deeper understanding of the impact of offensive behavior and the importance of being respectful and empathetic in interactions both online and in real life.

# Tags

#Halloween #OffensiveBehavior #Consequences #Respect #Empathy #SocialMedia #PoliticalCorrectness