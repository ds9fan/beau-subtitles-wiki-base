# Bits

- Bo expresses concern about Facebook and Twitter censoring independent news outlets opposing government policies.
- He differentiates these outlets from Alex Jones, noting that even the worst among them wasn't at his level.
- Bo mentions journalist Carrie Wedler, whose outlet and personal Twitter account were banned in a single day.
- He warns that Facebook and Twitter are no longer free discussion platforms and suggests finding alternative social media networks like Steam It and MeWe.
- Bo stresses the importance of seeking information from other platforms as censorship on Facebook may escalate.
- He underlines the significant number of subscribers affected by the censorship and the quality reporting of the banned outlets.

# Audience

Social media users, independent news supporters.

# On-the-ground actions from transcript

- Research alternative social media networks like Steam It and MeWe for independent news (suggested).
- Support independent news outlets by seeking information on their websites directly (suggested).
- Share information from independent outlets on Facebook to bypass censorship (suggested).

# Oneliner

Facebook and Twitter's censorship of independent news outlets signals the end of free discussion platforms, urging users to seek information elsewhere.

# Quotes

- "Now the two things you need to take away from this is first and foremost don't compare them to Alex Jones."
- "Facebook and Twitter are no longer free discussion platforms."
- "When we're talking about it, we're talking about tens of millions of subscribers that were simply taken off the rolls by Facebook."

# What's missing

In-depth discussion on strategies for maintaining free discourse in the face of increasing censorship on mainstream social media platforms.

# Tags

#Censorship #IndependentNews #SocialMedia #Facebook #Twitter