```

# Bits

- Bo talks about the aftermath of Hurricane Michael hitting inland unexpectedly and causing damage.
- Addresses the issue of looting during natural disasters and the law enforcement response.
- Points out the problem of law enforcement shutting down areas after disasters, preventing volunteer aid.
- Discusses media portrayal of looting after disasters and its racial biases.
- Advocates for individual emergency preparedness with a basic emergency kit.
- Recognizes the prompt response and aid provided by Florida National Guard during the disaster.

# Audience

Communities affected by natural disasters.

# On-the-ground actions from transcript

- Prepare an emergency kit with essentials like food, water, fire, shelter, medical supplies, and a knife (suggested).
- Organize your emergency supplies in one spot for easy access (implied).
- Share resources for creating emergency kits with different budget levels (suggested).

# Oneliner

Bo discusses the impact of Hurricane Michael, the issue of looting during disasters, and the importance of individual emergency preparedness.

# Quotes

- "Two van fulls of wet TVs are worth more than your life."
- "You can't count on government response."
- "Putting together a kit, a bag, for emergencies is something that every family should do."

# Whats missing

The importance of community support and mutual aid during and after natural disasters.

# Tags

#HurricaneMichael #NaturalDisasters #EmergencyPreparedness #Looting #CommunitySupport #FloridaNationalGuard