```

# Bits

- Bo addresses the stigmas and misconceptions around dating combat vets and sexual assault survivors, comparing the emotional responses of both groups. (addressing stigmas, comparing emotional responses) 
- He challenges the idea of survivors being "damaged goods" and discusses the trauma experienced by combat vets and survivors. (challenging stereotypes, discussing trauma)
- Bo shares a personal story about a combat vet's experience in a traumatic situation, illustrating the impact of trauma on memory. (personal story, trauma's impact on memory)

# Audience

Survivors, combat vets, advocates

# On-the-ground actions from transcript

- Contact support organizations for survivors and combat vets for resources and assistance. (suggested)
- Organize events or discussions to challenge stigmas and misconceptions about survivors and combat vets. (implied)
- Support and validate survivors and combat vets in their experiences and emotions. (generated)

# Oneliner

Bo challenges stigmas around dating combat vets and sexual assault survivors, pointing out the commonality in emotional responses and debunking the idea of being "damaged goods."

# Quotes

- "You are not damaged goods. You are certainly not unworthy of being loved."
- "We are all damaged goods in some way."
- "It is amazing what trauma can do to your memory."

# What's missing

Deeper exploration of the societal factors contributing to these stigmas and more strategies for supporting survivors and combat vets.

# Tags

#CombatVets #SexualViolenceSurvivors #ChallengingStigmas #Trauma #SupportActions