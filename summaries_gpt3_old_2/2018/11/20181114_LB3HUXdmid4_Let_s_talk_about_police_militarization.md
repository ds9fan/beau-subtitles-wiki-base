```

# Bits

- Police militarization discussed, equating warrior with killer and the need for toys to match (who, what)
- Shift from protecting the public to enforcing the law, leading to moral responsibility for unjust laws (why, impact)
- Propaganda around officer deaths inflated, leading to hyper-vigilance and quick trigger decisions (why, impact)
- Use of SWAT teams and military equipment without adequate training leading to dangerous outcomes (what, impact)
- Need for law enforcement to rethink their role as public servants and protectors of the public (call to action)

# Audience

Law enforcement officers

# On-the-ground actions from transcript

- Join a community dialogue on police militarization and the role of law enforcement (generated)
- Advocate for proper training before the use of military equipment in law enforcement (suggested)
- Contact local authorities to address unjust laws and the enforcement of them (implied)

# Oneliner

Examining police militarization, from equating warriors with killers to the use of military equipment without proper training, calls for a reevaluation of law enforcement's role as public servants.

# Quotes

- "If your interest is no longer protecting the public and it's only enforcing the law, you're just accepting orders and taking a paycheck to do it using violence."
- "You have those two issues that have shifted."
- "Being a cop, not really that dangerous. Not a lot of deaths really. It's not even in the top 10."
- "There is no war on cops. There should be no warrior cops."
- "The more they step away from their role as public servants and from their role as protectors of the public and more into, we're just going to enforce whatever law we're told and we're going to use these tools that we have, these weapons to do it."

# Whats missing

Discussion on community involvement in police reform efforts.

# Tags

#PoliceMilitarization #LawEnforcement #Training #PublicServants #CommunityDialogue