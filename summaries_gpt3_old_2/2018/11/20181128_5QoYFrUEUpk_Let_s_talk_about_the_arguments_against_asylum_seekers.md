```

# Bits

- Bo breaks down arguments against letting asylum seekers in, citing legal frameworks like the U.S. Constitution and international treaties.
- Points out that asylum seekers are following the law and those trying to stop them are violating it.
- Addresses the financial aspect, comparing the cost of refugee resettlement to other expenses.
- Challenges the notion that asylum seekers are coming for benefits, pointing out that they may simply be seeking safety.
- Discusses how the situation of asylum seekers is connected to U.S. intervention in countries like Honduras.
- Calls out those who suggest that people should stay and fix their own countries, pointing to the responsibility of outside influence.
- Illustrates the scale of tragedies like the refugee crisis using comparisons to U.S. historical events and wars.
- Mocks the bravado of internet commentators who talk tough about violence but likely have never experienced it firsthand.

# Audience

Advocates, activists, policymakers.

# On-the-ground actions from transcript

- Research U.S. laws and international treaties on asylum seekers (suggested)
- Support refugee resettlement organizations financially or through volunteering (implied)
- Advocate for policies that address root causes of displacement (generated)

# Oneliner

Bo dismantles anti-asylum arguments, citing laws, costs, and historical responsibility, urging a shift in perspective and action towards refugees.

# Quotes

- "Asylum seekers are following the law, and those trying to stop them are violating it."
- "It's cheaper to be a good person."
- "Accept some responsibility for your apathy and stay and fix it."
- "People who talk tough about violence likely have never experienced it firsthand."
- "It's more people than U.S. soldiers died in all wars combined. Times two."

# What's missing

Deeper exploration of the complexities of refugee crises and potential solutions beyond legal frameworks.

# Tags

#AsylumSeekers #Refugees #LegalFrameworks #Responsibility #HumanitarianAction #SocialChange #USIntervention
```