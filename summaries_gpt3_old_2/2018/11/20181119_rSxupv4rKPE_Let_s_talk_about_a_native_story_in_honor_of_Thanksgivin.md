# Bits

- Story of Matoaka, the real Pocahontas.
- Kidnapped, held hostage, forced into marriage.
- Popular culture distorts Native stories.
- Lack of authentic Native representation.

# Audience

Educators, storytellers, activists.

# On-the-ground actions from transcript

- Research accurate Native history (implied).
- Support authentic Native representation (generated).

# Oneliner

Unveiling the real, heartbreaking story of Pocahontas and the distorted Native narratives in popular culture.

# Quotes

- "Most of what you know of Native culture, myths, stories is fabricated."
- "Not a whole lot of stories about natives just being native."
- "Popular culture distorts Native stories."

# What's missing

Insight into how to actively challenge and change misrepresentations of Native history and culture in popular media.

# Tags

#NativeStories #Pocahontas #Misrepresentation #ColonialHistory #NativeCulture