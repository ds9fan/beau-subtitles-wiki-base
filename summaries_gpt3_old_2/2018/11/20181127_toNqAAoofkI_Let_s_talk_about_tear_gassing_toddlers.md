```

# Bits

- Bo Ginn addresses tear gas used on toddlers in the United States, questioning responses to violence and fear.
- Describes potential reactions to tear gassing children and criticizes the administration's approach.
- Raises concerns about the impact of fear-mongering and violence on society.
- Compares current events to actions seen in dictatorships and questions the nation's response.
- Expresses disappointment in potential lack of backlash and calls for reflection on societal values.

# Audience

- Activists
- Parents
- Advocates

# On-the-ground actions from transcript

- Contact local representatives to voice opposition to use of tear gas on children. (suggested)
- Join advocacy groups working to protect vulnerable populations from violence. (implied)
- Organize community discussions on non-violent solutions to social issues. (generated)

# Oneliner

Bo Ginn questions societal response to tear gas on toddlers, urging reflection on fear and violence in the United States.

# Quotes

- "We tear gassed kids like any other two-bit dictatorship."
- "If there is not a massive upsurge in violence along the border, it says a whole lot about those people."

# Whats missing

Insights on potential solutions to address violence and fear in society.

# Tags

#TearGas #Violence #Fear #Society #Advocacy