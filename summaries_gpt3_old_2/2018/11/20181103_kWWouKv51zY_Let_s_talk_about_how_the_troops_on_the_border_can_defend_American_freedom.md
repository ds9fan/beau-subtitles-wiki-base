```

# Bits

- In war, orders start vague and get specific as they reach the ground level for implementation (illustrated).
- Border personnel have a unique chance to defend American freedom by being at the frontlines (explained).
- Mission creep is cautioned against with examples like Afghanistan (detailed).
- Soldiers are warned against illegal orders and urged to stay within the laws of war (stressed).
- Legal asylum application processes for migrants heading to the US are explained (clarified).
- Soldiers are reminded to maintain integrity, follow the law, and not fall prey to political manipulation (advised).

# Audience

Soldiers, border personnel, migrants.

# On-the-ground actions from transcript

- Join legally permitted actions only (advised).
- Stay within the laws of war and UCMJ (stressed).
- Educate oneself on legal asylum processes for migrants (recommended).

# Oneliner

Soldiers and border personnel reminded to uphold integrity, follow laws, and avoid political manipulation in current deployments.

# Quotes

- "Order comes down, it's vague. Gets more specific until it gets to those guys on the ground."
- "You want to defend American freedom, you keep that weapon slung and you keep it on safe."
- "Honor, integrity, those things. It's bred into you, right?"

# What's missing

The speaker could elaborate on the potential consequences of not following legal orders and the impacts of political manipulation on soldiers and border personnel.

# Tags

#Soldiers #BorderPersonnel #LegalAsylum #MissionCreep #Integrity