```

# Bits

- Red flag gun seizures: good idea in theory, flawed execution.
- Cops using military tactics without understanding the implications.
- Need for proper training before implementing new tactics.
- Importance of due process in seizing guns.
- Suggestions for better tactical implementation to ensure safety and fairness.

# Audience

- Gun control advocates
- Pro-gun individuals
- Advocates for due process in law enforcement

# On-the-ground actions from transcript

- Contact your representative to advocate for fixing flaws in red flag gun seizure laws (suggested).
- Advocate for proper training for law enforcement officers before implementing new tactics (suggested).
- Support the importance of due process in gun seizures (implied).

# Oneliner

Flawed execution of red flag gun seizures calls for immediate action to ensure due process and safety in law enforcement tactics.

# Quotes

- "The idea is sound, okay, the execution isn't."
- "It's a good idea. The execution is bad and the execution can be fixed very easily."
- "You've got a good idea, guys. If you're in the gun control crowd, you need to be on the horn with your representative to fix this immediately."

# What's missing

In-depth discussion on the impact of flawed red flag laws on communities and individuals.

# Tags

#RedFlagLaws #GunControl #DueProcess #LawEnforcement #TacticalImplementation #Advocacy