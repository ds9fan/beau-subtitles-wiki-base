```

# Bits

- President Kennedy met Green Berets in 1963 to discuss fighting against a tyrannical government, passing on an oath to do so (who, what, when, where, why)
- Green Berets served as Kennedy's honor guard with mysterious orders after his assassination in 1963, suggesting a secretive mission (what, when, where)
- A legend exists about a Special Forces Underground organization dedicated to fighting tyranny, investigated by the DOD and deemed not racist (what, dynamics)
- Active-duty Green Berets hold a ceremony at Kennedy's grave site annually, suggesting continued respect and loyalty (who, dynamics)
- Kennedy's criteria for fighting tyranny included the suspension of habeas corpus, a move that current president is considering, raising concerns about the rule of law and national security (what, why, dynamics)

# Audience

Historians, activists, military personnel

# On-the-ground actions from transcript

- Contact local representatives to express opposition to the suspension of habeas corpus (implied)
- Organize educational sessions on habeas corpus and its importance in upholding the rule of law (suggested)
- Join advocacy groups working to protect civil liberties and legal rights (implied)

# Oneliner

President Kennedy's secret meeting with Green Berets in 1963 reveals a legend of fighting tyranny, sparking concerns about the suspension of habeas corpus today.

# Quotes

- "The suspension of habeas corpus is the end of the rule of law. That is what it is."
- "At this point you can support your country or you can support your president. Those are your options."
- "The rule of law will be erased completely. It's not hyperbole."
- "You need to realize exactly what it means for [habeas corpus] to be gone."
- "This is dangerous."

# Whats missing

Deeper exploration of the potential implications and consequences if the suspension of habeas corpus were to occur.

# Tags

#KennedyConspiracy #HabeasCorpus #GreenBerets #Tyranny #NationalSecurity