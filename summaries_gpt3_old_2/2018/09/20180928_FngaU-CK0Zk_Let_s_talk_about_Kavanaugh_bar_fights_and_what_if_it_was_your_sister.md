```

# Bits

- Bo shares a story about intervening to protect a girl from a pushy guy at a bar, focusing on property rights over respecting her as a person.
- He relates this to the Kavanaugh hearing commentary, urging people to imagine their friends being sexually assaulted and the impact of their comments on survivors.
- Bo contrasts the reactions to male and female survivors coming forward, pointing out the power dynamics and motivations involved.
- He criticizes the lack of empathy and understanding in discussions around sexual assault, particularly in high-profile cases like the Kavanaugh hearing.

# Audience

Men, women, allies, survivors

# On-the-ground actions from transcript

- Reach out to friends to offer support and understanding for survivors of sexual assault (suggested)
- Educate yourself on the dynamics of sexual assault and the impact of comments on survivors (implied)
- Challenge harmful beliefs and attitudes regarding sexual assault survivors (generated)

# Oneliner

Bo recounts a bar incident to illustrate respecting women as people, urging empathy for sexual assault survivors in light of high-profile cases like Kavanaugh, calling for reflection on impact of comments.

# Quotes

- "Do the math and then think about it. How many of them do you know? How many of them talked about it with you? I don't think any of them will now because they're not gonna trust you." 
- "You're altering real-life relationships over bumper sticker politics by proclaiming you know what happened when you don't and by being the very reason they don't come forward."

# What's missing

Bo's perspective on supporting survivors and promoting empathy could be further explored through personal anecdotes or specific actions individuals can take to create a safer environment for survivors of sexual assault.

# Tags

#SexualViolence #Empathy #Survivors #Respect #SupportWomen #BelieveSurvivors #SocialChange #Accountability #ToxicMasculinity
```