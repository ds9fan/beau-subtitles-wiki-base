# Bits

- Bo discusses the idea of burning shoes to symbolize a protest against a company's actions.
- He points out the importance of understanding the implications of such actions and suggests more meaningful alternatives like donating the shoes to those in need.
- Bo questions the true motives behind burning symbols and draws parallels with other forms of symbolic protests.
- He criticizes the inconsistency of being upset over a commercial but turning a blind eye to larger issues like sweatshops and slave labor.
- Bo challenges the idea of loving symbols of freedom more than actual freedom itself.

# Audience

Individuals concerned about ethical consumerism and meaningful activism.

# On-the-ground actions from transcript

- Donate unwanted shoes to shelters or homeless veterans. (suggested)
- Drop off shoes at thrift stores near military bases for those in need. (suggested)

# Oneliner

Burning symbols won't solve real issues; donate to those in need instead.

# Quotes

- "You're loving that symbol of freedom more than you love freedom."
- "Take them and drop them off at a shelter."
- "You don't really care about the worker."

# What's missing

Bo's perspective on how to address systemic issues beyond individual actions.

# Tags

#SymbolicProtest #EthicalConsumerism #Activism #Donation #SocialResponsibility