```

# Bits

- Bo responds to a story about a woman attacked for kneeling during the national anthem at a county fair.
- Bo questions the author's understanding of patriotism and purpose of protests.
- Bo points out the difference between nationalism and patriotism, advocating for correcting the government when wrong.
- He recalls a conversation where patriotism was defined as actions, not just symbols like flag lapel pins.

# Audience

- Activists
- Patriots
- Community members

# On-the-ground actions from transcript

- Contact Bo to connect with the person defining patriotism through actions (suggested)
- Educate others about the difference between nationalism and patriotism (implied)
- Correct the government when it is wrong (implied)

# Oneliner

Bo addresses misconceptions on patriotism and nationalism, advocating for actions over symbols in defining patriotism.

# Quotes

- "Patriotism is not doing what the government tells you to."
- "Patriotism is correcting your government when it is wrong."
- "Patriotism was displayed through actions, not worship of symbols."

# Whats missing

Insightful examples showcasing the impact of actions over symbolic gestures could be better understood by watching the full video.

# Tags

#Patriotism #Nationalism #Protests #Symbols #CommunityLeadership #Activism