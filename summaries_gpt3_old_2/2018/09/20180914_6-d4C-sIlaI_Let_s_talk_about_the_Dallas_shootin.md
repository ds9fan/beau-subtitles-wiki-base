```

# Bits

- Bo discusses the shooting in Dallas, questioning the officer's motive and pointing out inconsistencies in her story.
- He mentions the concept of prepared liars and suggests that the officer's prior noise complaints against the victim may indicate motive.
- Bo raises the issue of the officer's actions constituting a home invasion and the potential legal consequences she may face.
- He brings up the lack of justice for black lives in the case and criticizes the police's conduct in searching the victim's home post-mortum.
- Bo condemns police corruption, the lack of accountability within the Dallas PD, and the impact of such behavior on public perception of law enforcement.

# Audience

- Activists
- Police accountability advocates
- Community organizers

# On-the-ground actions from transcript

- Contact local officials demanding transparency and accountability in the investigation (generated)
- Join or support local organizations working on police reform efforts (implied)
- Organize a community forum to discuss police misconduct and accountability (suggested)

# Oneliner

Bo questions the officer's motive and calls for justice in the Dallas shooting, shedding light on police corruption and lack of accountability.

# Quotes

- "If this is the amount of justice they get, they don't."
- "Not one cop has crossed the thin blue line to say that's wrong."
- "It's a few bad apples spoils the bunch."

# What's missing

Insights on specific actions individuals can take to support justice and police reform efforts.

# Tags

#Justice #PoliceAccountability #BlackLivesMatter #HomeInvasion #CommunityOrganizing
```