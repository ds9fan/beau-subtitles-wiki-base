#!/usr/bin/env python

import glob
import os
import tiktoken
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--overhead', default=0, type=int, nargs='?')
parser.add_argument('--incost', default=0.01, type=float, nargs='?')
parser.add_argument('--outcost', default=0.03, type=float, nargs='?')
args = parser.parse_args()
tokenizer = tiktoken.get_encoding("cl100k_base")


files = glob.glob('whisper_txt/*/*/*.txt')
total = 0
total_out = 0
for file in files:
    with open(file, 'r') as f:
        text = f.read()
    tokens = tokenizer.encode(text)
    tokenlimit = min(max(512, int((len(tokens)**0.5)*20)), 1024)
    total += len(tokens) + args.overhead
    total_out += tokenlimit
print(f"{total} tokens, ${(total/1000) * args.incost}")
print(f"{total_out} out tokens, ${(total_out/1000) * args.outcost}")
print(f"${((total/1000) * args.incost + (total_out/1000) * args.outcost)}")
