# Bits

- Beau recalls watching the 9/11 attacks, knowing it heralded war.
- He notes freedom as a casualty of 9/11, eroded by laws and government actions.
- Beau argues the US lost the war on terror by allowing freedoms to be stripped away in exchange for an illusion of security.
- Terrorism aims to provoke overreactions that lead to government crackdowns, sowing discontent.
- Beau criticizes the normalization of surveillance and militarized police, urging resistance.
- He proposes community-level actions: educating children on abnormality of current state, preparing for self-reliance, practicing counter-economics.
- Beau stresses the importance of building supportive communities and aiding the downtrodden.
- He advocates for being a "force multiplier" by educating and mobilizing others to restore freedoms.
- Beau warns against the gradual erosion of freedoms, calling for proactive engagement to prevent further loss.

# Audience

Community organizers, activists, educators

# On-the-ground actions from transcript

- Teach children about the abnormal state of surveillance and militarization (suggested).
- Become self-reliant and prepare for natural disasters (suggested).
- Practice counter-economics using cryptocurrencies and local trade (suggested).
- Support and employ individuals marginalized by the system, like homeless vets and ex-prisoners (suggested).
- Be a "force multiplier" by educating and mobilizing your community (suggested).

# Oneliner

Beau reflects on the erosion of freedoms post-9/11, urging action at the community level to resist surveillance and militarization. He proposes educating the young, self-reliance, counter-economics, and community mobilization as keys to restoring freedoms.

# Quotes

- "We knew then it wasn't an accident, and we knew what that meant."
- "Freedom's breath got more and more shallow with every right and every freedom the government took from you."
- "The American people just basically, here, take my rights, take my freedom."
- "You're going to defeat an overreaching government by ignoring it."
- "The face of tyranny is always mild at first."

# Whats missing in summary

The emotional impact of Beau's personal recollections and the detailed strategy for community-level activism.

# Tags

#Freedom #9/11 #CommunityAction #CounterEconomics #Activism