# Bits

- Beau responds to a story where a woman kneels during the national anthem, igniting rage in the author.
- The author, identified with "lecturer on patriotism," criticizes the kneeling woman for seeking attention, not understanding the protest's purpose.
- Beau argues true patriotism is about correcting the government when it's wrong, not blind adherence to symbols.
- The kneeling protest is linked to grievances against police brutality, contrasting the author's role as a spokesman for law enforcement.
- Beau questions whether the author is unaware of the protest's cause or deliberately omitting details for propaganda.
- The author's stance is seen as an attack on constitutionally protected rights, contrasting his likely oath to defend the Constitution.
- Beau recalls a story of a veteran who believes patriotism is shown through actions, not symbols, undermining the author's focus on flag etiquette.
- The video ends with Beau differentiating between nationalism and patriotism, criticizing the author's narrow view.

# Audience

Activists, Patriots, Constitutionalists

# On-the-ground actions from transcript

- Practice and articulate a nuanced understanding of patriotism beyond symbols (suggested).
- Support and defend constitutionally protected rights in your community (exemplified).
- Challenge nationalism that contradicts constitutional principles (implied).

# Oneliner

Beau tackles a narrative conflating nationalism with patriotism, defending a woman's right to protest as a true patriotic act. He argues patriotism involves correcting the government and supporting community over blind allegiance to symbols.

# Quotes

- "Patriotism is correcting your government when it is wrong."
- "Patriotism is displayed through actions, not worship of symbols."
- "For you, that's that woman in the crowd that you're attacking. That's who you're supposed to show loyalty to."
- "You can kiss my patriotic behind."

# Whats missing in summary

The emotional tone and Beau's personal connection to the story's broader implications for American society and patriotism.

# Tags

#Patriotism #ConstitutionalRights #Protest #Nationalism #PoliceBrutality