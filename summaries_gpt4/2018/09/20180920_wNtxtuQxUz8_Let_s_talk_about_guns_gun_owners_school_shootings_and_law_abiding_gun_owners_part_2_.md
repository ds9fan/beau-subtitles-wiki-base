# Bits

- Beau opens by urging viewers to watch the previous video for context on the firearms and gun control debate.
- The failure of gun control is attributed to legislation crafted by individuals unfamiliar with firearms.
- The term "assault weapons" is criticized as ambiguous, leading to ineffective criteria in bans.
- Beau uses a toy AR-15 to demonstrate misconceptions around features like collapsible stocks and pistol grips.
- The inefficacy of banning high-capacity magazines is argued, citing the ease of changing magazines and making them at home.
- Banning specific models like the AR-15 could lead to the use of even more powerful firearms in crimes.
- The potential consequences of banning semi-automatic rifles are discussed, including the feasibility and unintended outcomes.
- Beau mentions that prohibition can make guns cheaper and more accessible illegally.
- He criticizes focusing solely on gun deaths rather than overall murder rates when evaluating gun control in other countries.
- Beau proposes raising the age to purchase firearms to 21 as a simple solution to mitigate school shootings.
- The issue of domestic violence perpetrators retaining access to firearms due to legal loopholes is addressed.
- Beau concludes that while some regulations can mitigate issues, legislation alone is not the solution to gun violence.

# Audience

Policy-makers, gun owners, activists

# On-the-ground actions from transcript

- Advocate for raising the legal firearm purchase age to 21 (suggested).
- Work towards closing legal loopholes allowing domestic violence perpetrators to own guns (suggested).

# Oneliner

Beau tackles the inefficacy of traditional gun control measures, debunking popular proposals and suggesting raising the purchase age to 21 and closing domestic violence loopholes as actionable solutions, but reminds us that legislation isn't the sole answer.

# Quotes

- "The failure of gun control is attributed to legislation crafted by individuals unfamiliar with firearms."
- "When you make something illegal like that, you're just creating a black market."
- "Advocate for raising the legal firearm purchase age to 21."
- "Legislation alone is not the solution to gun violence."

# Whats missing in summary

The detailed explanations and demonstrations using a toy AR-15, and Beau's nuanced perspective on the complexities of gun control.

# Tags

#GunControl #Firearms #PolicyMaking #Legislation #Safety