```
# Bits

- Beau jests about burning Nikes because of a commercial, revealing it to be a thought experiment.
- He criticizes those who destroy their Nikes as emotional, entitled, and thoughtless.
- Suggests donating unwanted Nikes to shelters, homeless vets, or thrift stores as a constructive alternative.
- Questions the logic of burning shoes, pointing out it may inadvertently insult laborers rather than corporate leaders.
- Compares shoe burning to flag burning, noting both are symbolic acts meant to draw attention to causes.
- Challenges viewers who reject Nike over a commercial but ignore the company's history of labor abuses.
- Accuses such critics of valuing symbols of freedom over actual freedom.

# Audience

Thoughtful consumers, activists

# On-the-ground actions from transcript

- Take unwanted Nikes and drop them off at a shelter (suggested).
- Give them to a homeless vet (suggested).
- Put them in a thrift store near a military base (suggested).

# Oneliner

Beau criticizes the act of burning Nikes over a commercial, suggesting donating them instead and questioning the prioritization of symbols over substantive issues of freedom and labor rights.

# Quotes

- "Those are the actions of some man child who's emotional, entitled, and too stupid to really think about things."
- "You don't really get to have an opinion about freedom because you don't know what it is."
- "You're loving that symbol of freedom more than you love freedom."

# Whats missing in summary

The emotional delivery and sarcastic tone used by Beau to drive his points home.

# Tags

#BeauOfTheFifthColumn #NikeControversy #ConsumerActivism #LaborRights #SymbolismVsSubstance
```