```
# Bits

- Beau talks about the shooting in Dallas, pondering the credibility of the officer's story.
- He distinguishes between bad liars and good liars, suggesting the officer might be the latter.
- The officer claimed she entered the wrong apartment by mistake and shot the resident when he didn't obey her commands.
- Beau raises doubts, mentioning reports that the officer had previously filed noise complaints against the victim.
- He suggests an alternative scenario where the officer, annoyed by noise, confronts the victim leading to the fatal shooting.
- Beau argues that even if the officer's entry was a mistake, it still constitutes a home invasion when she used violence.
- He points out that under Texas law, killing someone during a home invasion could lead to capital murder charges.
- Beau reflects on a past experience with Kevin Crosby, a Panther, discussing whether Black Lives Matter in America.
- He criticizes the Dallas Police Department for searching the victim's home, implying an attempt to justify the shooting.
- Beau condemns the lack of police accountability, suggesting systemic corruption within the department.
- He expresses frustration with the "few bad apples" excuse, arguing it's indicative of a broader issue within law enforcement.
- Beau concludes by voicing hope for justice in this case, while lamenting the erosion of public trust in the police.

# Audience

Social justice advocates

# On-the-ground actions from transcript

- (No direct actions described or suggested)

# Oneliner

Beau scrutinizes the Dallas shooting, questioning the officer's narrative and suggesting alternative motives rooted in prior complaints. He denounces police corruption and systemic failures, calling for accountability and justice in law enforcement.

# Quotes

- "There's two kinds of liars in the world."
- "Her being a cop doesn't make her walking in this man's house lawful."
- "You're facing capital murder. Capital murder."
- "Police corruption down here, it's a sport."
- "That is why people don't back the blue anymore."

# Whats missing in summary

The emotional tone, Beau's personal reflections, and the nuanced exploration of systemic issues are best experienced through the full transcript.

# Tags

#SocialJustice #PoliceAccountability #DallasShooting #BeauOfTheFifthColumn #SystemicCorruption #BlackLivesMatter
```