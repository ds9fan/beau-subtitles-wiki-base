# Bits

Beau says:

- Recalls his experience on September 11th, watching the attacks unfold on TV.
- Notes the casualty of that day that often goes unnamed - the erosion of freedom.
- Describes the gradual loss of freedoms post-9/11 due to new laws and government actions.
- Stresses the strategic nature of terrorism in provoking overreactions to achieve goals.
- Emphasizes the importance of not letting the erosion of freedom become normalized.
- Urges for action at the community level to reinstate the value of freedom.
- Proposes teaching children about the abnormality of certain government actions.
- Advocates for self-reliance in preparation for natural disasters and reducing dependence on the government.
- Recommends counter-economics to reduce government control over financial transactions.
- Encourages building a supportive community focused on preserving freedom and helping those in need.
- Mentions the effectiveness of U.S. Army Special Forces in training local populations.
- Calls for individuals to be force multipliers in their communities to counter an overreaching government.

# Oneliner

Beau stresses the erosion of freedom post-9/11, urging action at the community level to reinstate its value and combat overreaching government control.

# Audience

Community members

# On-the-ground actions from transcript

- Teach children about the abnormality of certain government actions (suggested)
- Prepare for natural disasters and reduce dependence on the government by being self-reliant (implied)
- Practice counter-economics to reduce government control over financial transactions (implied)
- Build a supportive community focused on preserving freedom and helping those in need (suggested)
- Be a force multiplier in your community by discussing lost freedoms and solutions (suggested)

# Quotes

- "We can't let this become normal."
- "The face of tyranny is always mild at first."
- "You're going to defeat an overreaching government by ignoring it."
- "We have to do this from the bottom up."
- "Y'all been giving freedom CPR."

# Whats missing in summary

The full transcript provides a detailed insight into the erosion of freedoms post-9/11 and the importance of community action in reinstating and preserving freedom.

# Tags

#Freedom #CommunityAction #CounterEconomics #Terrorism #USArmy #SelfReliance