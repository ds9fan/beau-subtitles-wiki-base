# Bits

Beau says:

- Explains the division within the gun community between Second Amendment supporters and gun nuts/firearm enthusiasts.
- Second Amendment supporters advocate for minorities to have firearms and have outreach programs for this purpose.
- Second Amendment supporters aim to arm minorities as a defense against government tyranny targeting marginalized groups first.
- Describes the violent rhetoric and racial elements within the Second Amendment community.
- Mentions a banned Facebook post featuring a lynching image that is popular in the Second Amendment community.
- Contrasts the philosophies of Second Amendment supporters who want to decentralize power and gun nuts who focus on might makes right.
- Talks about a program by the Department of Defense called the Civilian Marksmanship Program to differentiate between Second Amendment supporters and gun nuts.
- Addresses the question of whether gun control is low-key racist and provides examples of how it can be unintentionally or intentionally racist.
- Gives examples of how certain gun control measures disproportionately affect minority groups, showing the racial implications.
- Emphasizes the historical racial component of gun control and how the Second Amendment is meant to protect minorities against government oppression.

# Oneliner

Gun control, race, and the divide between Second Amendment supporters and gun nuts explored through historical and contemporary racial implications.

# Audience

Advocates, policymakers, activists.

# On-the-ground actions from transcript

- Join outreach programs that advocate for minorities to have firearms (implied).
- Support legislation that aims to arm minorities for self-defense (implied).
- Advocate for equal access to firearms for marginalized groups (implied).
- Educate others on the racial implications of certain gun control measures (implied).

# Quotes

- "Second Amendment supporters want to make sure that if the government ever comes from minorities in the U.S., nobody has to say anything because it's going to get so loud everybody's going to know."
- "Even when they're saying something rude they're doing it with the intent of getting you to buy a gun."
- "Doesn't seem racist, doesn't seem to make sense, but it doesn't sound racist."

# Whats missing in summary

The full transcript provides a detailed exploration of the historical and contemporary racial implications of gun control, showcasing the divide between Second Amendment supporters and gun nuts.

# Tags

#GunControl #Race #SecondAmendment #Racism #CommunityPolicing