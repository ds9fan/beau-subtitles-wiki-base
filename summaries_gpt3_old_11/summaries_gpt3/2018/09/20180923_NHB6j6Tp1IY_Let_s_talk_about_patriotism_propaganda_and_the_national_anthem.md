# Bits

Beau says:

- Responding to an op-ed story shared by two people about a woman attacked for kneeling during the national anthem at a county fair.
- Expresses irritation and reads the author's bio, prompting a response due to the content.
- Questions the purpose of a protest, citing the Constitution's provision for redress of grievances.
- Criticizes the author for focusing on flag etiquette rather than the underlying reasons for protests.
- Calls out a spokesperson for law enforcement for their lack of understanding or potential propaganda tactics regarding protests.
- Defines patriotism as correcting the government when it is wrong, contrasting it with blind obedience.
- Shares a story about a civilian who believed patriotism is shown through actions, not symbols, and suggests consulting him for a better understanding.
- Emphasizes the distinction between nationalism and patriotism, concluding with a defiant message.

# Oneliner

Beau responds to an op-ed story, challenging the misconceptions about patriotism and nationalism while defending the right to protest.

# Audience

Activists, Patriots

# On-the-ground actions from transcript

- Contact Beau to connect with the civilian who served for 30 years to gain insights on patriotism (suggested).
- Educate others on the distinction between nationalism and patriotism through actions (implied).

# Quotes

- "Patriotism is correcting your government when it is wrong."
- "There is a very marked difference between nationalism and patriotism."
- "Sir, patriotism is not doing what the government tells you to."
- "Patriotism was displayed through actions, not worship of symbols."
- "You can kiss my patriotic behind."

# What's missing in summary

The full transcript provides a deeper understanding of the importance of standing up against injustice and the true meaning of patriotism through actions, not symbols.

# Tags

#Patriotism #Protest #NationalAnthem #LawEnforcement #Activism