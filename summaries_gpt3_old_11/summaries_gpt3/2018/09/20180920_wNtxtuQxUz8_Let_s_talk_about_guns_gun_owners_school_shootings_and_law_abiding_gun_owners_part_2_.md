# Bits

Beau says:
- Gun control has failed due to being written by those unfamiliar with firearms, leading to ineffective measures.
- The term "assault weapon" is made up and lacks a concrete definition within firearms vocabulary.
- The criteria set by the assault weapons ban were arbitrary and did not address weapon lethality.
- Banning high-capacity magazines is flawed as changing magazines is quick, making rushing a shooter impractical.
- Banning specific firearms like the AR-15 could lead to the use of even deadlier weapons like the Garand or M14.
- Banning semi-automatic rifles altogether is challenging due to their prevalence and ease of production.
- The ban on fully automatic rifles like the M16 resulted in cheaper illegal options, showing bans can backfire.
- Suggestions like raising the age limit to 21 for purchasing firearms post-Parkland shooting are seen as effective solutions.
- Addressing domestic violence histories and closing loopholes in gun ownership laws are deemed necessary steps.

# Oneliner

Gun control measures have failed due to a lack of firearm knowledge, leading to ineffective bans and flawed solutions, with alternative solutions focusing on age restrictions and addressing loopholes.

# Audience

Policy makers, lawmakers, gun control advocates

# On-the-ground actions from transcript
- Raise the age limit for purchasing firearms to 21 (suggested)
- Address loopholes in gun ownership laws regarding domestic violence histories (suggested)

# Quotes

- "Legislation is not the answer here in any way shape or form."
- "Banning them, if this becomes a punishable offense just possessing it, it's going to make people more violent when they have one."
- "The idea behind gun control is to limit death, right? It's to get rid of that problem."

# What's missing in summary

The full transcript provides a detailed breakdown of why traditional gun control measures have failed and offers alternative solutions such as age restrictions and addressing loopholes in existing laws.

# Tags

#GunControl #Firearms #Policy #DomesticViolence #AgeRestrictions