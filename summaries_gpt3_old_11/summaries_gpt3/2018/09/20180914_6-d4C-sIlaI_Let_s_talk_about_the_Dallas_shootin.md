# Bits

Beau says:
- Analyzes the shooting in Dallas from a non-law enforcement perspective, referencing experience with liars and watching Andy Griffith.
- Questions the official narrative of the shooting, suggesting a potential motive based on past noise complaints filed by the officer against the victim.
- Points out that the officer's actions could be considered a home invasion, even if the door was open, leading to potential capital murder charges in Texas.
- Recalls a past encounter with Kevin Crosby discussing Black Lives Matter and All Lives Matter, raising concerns about the justice system's treatment of black lives.
- Criticizes the police for searching the victim's home post-shooting and the lack of accountability within the Dallas PD for their actions.

# Oneliner

Beau questions the official narrative of a shooting in Dallas, suggesting a potential motive and criticizing the lack of accountability within law enforcement, raising concerns about justice for black lives.

# Audience

Community members

# On-the-ground actions from transcript

- Hold law enforcement accountable for their actions (suggested)
- Advocate for justice for victims of police violence (implied)

# Quotes

- "If this is the amount of justice they get, they don't."
- "It's a few bad apples spoils the bunch, and that certainly appears to have happened in Dallas."
- "You're trying to slander and villainize an unarmed man that was gunned down in his home."

# Whats missing in summary

Full emotional impact and detailed analysis of the shooting incident in Dallas.

# Tags

#Justice #PoliceViolence #Accountability #BlackLivesMatter #Community