# Bits

Beau says:

- Beau wants to burn something inside because it's raining and he feels it's really important to burn it today.
- He mentions not owning Nikes due to his dislike of the company, but his son has a pair which he plans to burn.
- Beau questions the importance of burning the shoes for the Skycloth to create liberty, sarcastically referencing freedom and basic witchcraft.
- He criticizes the idea of burning shoes as emotional and entitled behavior, suggesting donating them to those in need instead.
- Beau questions the motives behind burning the shoes, whether to insult the workers who made them or just out of anger towards the company.
- He compares burning shoes as a symbol to burning the American flag and urges people to think about their actions.
- Beau points out the hypocrisy of being mad at a commercial while turning a blind eye to the sweatshops and slave labor used by the company.
- He suggests that loving the symbol of freedom more than actual freedom shows a lack of understanding of what freedom truly means.

# Oneliner

Beau questions the symbolism behind burning shoes, criticizing hypocrisy and urging a deeper understanding of freedom.

# Audience

Consumers

# On-the-ground actions from transcript

- Donate unwanted items to shelters or homeless veterans (suggested)
- Give away items to thrift stores near military bases (suggested)

# Quotes

- "You're loving that symbol of freedom more than you love freedom."
- "If you're that mad and you can't wear a pair of Nikes because of a commercial you don't like, take them and drop them off at a shelter."

# Whats missing in summary

The full video provides a detailed exploration of consumer behavior, symbolism, and freedom, urging viewers to think critically about their actions and beliefs.

# Tags

#ConsumerBehavior #Symbolism #Freedom #Hypocrisy #Donation