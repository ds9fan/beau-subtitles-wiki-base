# Bits

Beau says:

- Beau shares the real story of Matoaka, also known as Pocahontas, not the romanticized version popularized in culture.
- Matoaka was kidnapped, held hostage, forced into marriage, and died an early death, contrary to the love story narrative.
- Native stories in popular culture often involve a white savior element, distorting the reality of Native experiences.
- There is a lack of narratives where Natives are simply portrayed as themselves without the white savior trope.
- Beau mentions a movie where a character leaves the tribe, becomes an FBI agent, and returns as a hero to save the day, reflecting a problematic portrayal of Native experiences.
- Leonard Peltier's case is brought up as an example of the harsh treatment Natives have faced from the FBI.
- Beau urges reflection on the fabricated nature of many popular Native myths and stories.
- The holiday season prompts Beau to remind viewers of the importance of understanding the true history and experiences of Native cultures.
- Native culture and stories have often been misrepresented and distorted in mainstream media and popular culture.
- Beau's message serves as a call to critically analyze and challenge the inaccurate narratives surrounding Native peoples.

# Oneliner

Beau shares the harsh reality of Matoaka's story, exposing the distorted narratives of Native cultures perpetuated in popular media.

# Audience

Advocates for accurate representation

# On-the-ground actions from transcript

- Research and share accurate Native American histories (suggested)
- Support Native activists and causes (suggested)

# Quotes

- "Most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."
- "There's not a whole lot of stories about natives just being native."
- "It's a good time to bring it up and just kind of remind that most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."

# Whats missing in summary

Full emotional impact and depth of Beau's storytelling

# Tags

#NativeAmerican #History #Narratives #Misrepresentation #Activism