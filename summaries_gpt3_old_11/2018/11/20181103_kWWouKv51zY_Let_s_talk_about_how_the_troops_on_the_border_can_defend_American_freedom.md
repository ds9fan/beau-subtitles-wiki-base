# Bits

Beau says:
- Tells an anecdote about a general during the Tet Offensive in 1968 who successfully took a hill with no casualties by giving a vague order that got more specific as it reached the ground.
- Talks about the unique chance for service members at the border to truly defend American freedom, a rare occurrence historically.
- Mentions Mission Creep using Afghanistan as an example and warns against vague orders leading to potential legal issues for soldiers.
- Advises soldiers to stay within the law, keep weapons safe, and not get involved in potentially illegal activities.
- Urges those at the border to understand the legal process of applying for asylum in the US and clarifies the legality of migrants seeking asylum.
- Points out the power dynamics and political motivations behind soldiers being deployed to the border.
- Expresses concern that soldiers may be used as political tools before midterm elections, compromising their honor and integrity.
- Warns against blindly following vague orders, encourages seeking clarification to protect oneself legally.
- Stresses the importance of following the law and not allowing mission creep to jeopardize one's safety and reputation.

# Oneliner
Soldiers at the border are cautioned against mission creep, vague orders, and being used as political pawns, urged to uphold the law and protect themselves legally.

# Audience
Soldiers, service members

# On-the-ground actions from transcript
- Seek clarification on vague orders to protect oneself legally (suggested)
- Understand the legal process of applying for asylum in the US (suggested)
- Keep weapons safe and stay within the law (implied)

# Quotes
- "You want to defend American freedom, you want to defend the freedoms of Americans, you keep that weapon slung and you keep it on safe."
- "Honor, integrity, those things. It's bred into you, right?"
- "If you allow that mission creep to happen, you're going to need it. You're going to need a defense."

# Whats missing in summary
The full transcript provides detailed insights on the historical context of defending American freedom, the legalities of soldiers' actions at the border, and the risks of mission creep and being manipulated for political purposes.

# Tags
#Soldiers #Border #MissionCreep #LegalAdvice #PoliticalManipulation