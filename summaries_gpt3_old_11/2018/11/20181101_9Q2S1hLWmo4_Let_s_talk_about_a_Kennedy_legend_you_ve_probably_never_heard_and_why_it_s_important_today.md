# Bits

Beau says:

- Tells a legend about President Kennedy meeting with selected Green Berets in 196l, where they took an oath to fight back against a tyrannical government.
- Mentions that evidence supporting the theory includes Kennedy's visit to Fort Bragg in October 196l and the subsequent departure of 46 Green Berets to DC after Kennedy's assassination.
- Describes the peculiar nature of 21 Green Berets serving as Kennedy's honor guard, a unique occurrence in presidential history.
- Speculates on the actions of the other 25 Green Berets who accompanied Jackie Kennedy but whose orders remain unknown.
- Mentions the legend surrounding Command Sergeant Major Ruddy placing his green beret on Kennedy's casket, now displayed at the JFK Presidential Library.
- Narrates the investigation into a rumored organization called the Special Forces Underground, which was found not to be racist by the Department of Defense.
- Explains the significance of Green Berets visiting Kennedy's grave site annually and performing a brief ceremony.
- Raises concerns about the suspension of habeas corpus under the current president, linking it to Kennedy's criteria for Green Berets to act against tyranny.
- Questions the necessity of suspending habeas corpus in dealing with migrants, pointing out the legal avenues available for handling immigration issues.
- Argues that the suspension of habeas corpus poses a greater threat to national security than unarmed refugees seeking asylum.

# Oneliner

President Kennedy's alleged oath to Green Berets reveals a legend tied to fighting tyranny, with implications for today's suspension of habeas corpus under the current president.

# Audience

Activists, citizens, historians

# On-the-ground actions from transcript

- Visit the JFK Presidential Library to see Command Sergeant Major Ruddy's green beret on display (exemplified)
- Research and understand the concept of habeas corpus and its implications (suggested)
- Advocate against the suspension of habeas corpus and its threat to the rule of law (exemplified)

# Quotes

- "The suspension of habeas corpus is something that can't be tolerated."
- "The rule of law will be erased completely."
- "You can support your country or you can support your president."
- "This is dangerous."
- "It's just a legend, maybe. It's just a story."

# Whats missing in summary

Full context and depth of Beau's storytelling and analysis on the alleged Kennedy conspiracy and its relevance to current political issues.

# Tags

#KennedyConspiracy #GreenBerets #HabeasCorpus #Tyranny #CurrentEvents