# Bits

Beau says:

- Beau delves into the issue of police militarization, sparked by viewer interest after mentioning it in a previous video.
- He criticizes the concept of the "warrior cop" who equates being a warrior with being a killer.
- Beau points out that only four percent of the military are considered killers, contrary to the perception of many police officers.
- He challenges the shift in law enforcement's focus from protecting the public to strictly enforcing the law, leading to moral dilemmas for officers.
- Beau questions the propaganda used to justify police militarization and the perception of a "war on cops."
- He sheds light on the misuse of statistics to inflate the dangers faced by law enforcement officers.
- Beau draws attention to the high rate of unarmed killings by police officers and the skewed justifications used to defend such actions.
- He compares the dangers faced by pizza delivery drivers to those faced by police officers, challenging the narrative of policing being an exceptionally hazardous profession.
- Beau criticizes the lack of proper training accompanying the militarization of police forces, leading to dangerous consequences during operations.
- He stresses the importance of law enforcement rethinking their role as public servants and protectors of the community instead of merely enforcers of the law.

# Oneliner
Beau challenges the misconceptions surrounding police militarization, questioning the "warrior cop" mentality and the shift from public protection to law enforcement.

# Audience
Law enforcement agencies

# On-the-ground actions from transcript
- Rethink the role of law enforcement as public servants, prioritizing community protection over strict law enforcement (implied).

# Quotes
- "If your interest is no longer protecting the public and it's only enforcing the law, you're just accepting orders and taking a paycheck to do it using violence."
- "There is no war on cops. There should be no warrior cops."
- "You're the weapon if you're a warrior."
- "You want to be a warrior, maybe somebody in the comment section will acquaint you with what the way of the warrior really is."
- "Being a cop, not really that dangerous. Not a lot of deaths really."

# Whats missing in summary
The full transcript provides in-depth insights into the dangers of police militarization, the misinterpretation of what it means to be a warrior, and the importance of redefining law enforcement's role in society.

# Tags
#PoliceMilitarization #WarriorCop #LawEnforcement #Propaganda #CommunityProtection