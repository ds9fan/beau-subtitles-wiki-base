# Bits

Beau says:

- Addressing a recent shooting incident in Alabama involving the police.
- Police initially claimed to have killed the shooter, then later stated he did not shoot anyone.
- Beau credits the police for admitting their mistake but criticizes their lack of transparency.
- The police implied that the man fleeing the scene might have been involved in the altercation, painting him in a negative light.
- Beau questions the rationality of shooting someone who is fleeing from a shooter in a mall.
- He challenges the notion that being armed means one will automatically shoot back in a dangerous situation.
- There is no evidence to suggest that the man did anything wrong in this case.
- Beau delves into the military service of the individual involved, addressing discrepancies in statements about his discharge.
- He points out the media's use of a photo to portray the man negatively.
- Beau brings up instances where legally armed black individuals were killed by police, drawing attention to biases and injustices.

# Oneliner

Police admit to shooting wrong person in Alabama mall incident, raising questions on biases and injustices against armed black individuals.

# Audience

Law enforcement officers and individuals fighting against racial biases.

# On-the-ground actions from transcript

- Challenge biases and injustices in your community (implied).
- Advocate for transparency and accountability in police actions (implied).

# Quotes

- "Being armed in black isn't a crime."
- "Nothing suggests this man did anything wrong."
- "Biases exist. If you don't acknowledge them, they will persist."

# Whats missing in summary

The full transcript provides a detailed analysis of a shooting incident, biases, and injustices faced by armed black individuals. It offers a critical perspective on police actions and media portrayal.

# Tags

#PoliceShooting #RacialBias #Injustice #Transparency #CommunityPolicing