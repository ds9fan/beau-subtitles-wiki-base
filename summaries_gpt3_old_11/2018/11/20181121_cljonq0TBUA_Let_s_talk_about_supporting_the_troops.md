# Bits

Beau says:

- Dennis heard about a group in need north of Panama City after the hurricane.
- Terry was informed by Dennis and reached out to Beau for help.
- Beau went to Hope Project to pick up supplies with a lady filling in for David.
- Beau loaded up his truck with supplies, including baby formula.
- On the way, they stopped at a community center in Fountain, Florida.
- The community center needed food, so Beau exchanged supplies.
- An 89-year-old man helped load food into another truck at the community center.
- Beau emphasized that Dennis, Terry, David, the lady, the truck owner, and the elderly man were all veterans.
- Beau discussed the lack of support for veterans facing issues like delayed GI bill payments and benefits being reduced.
- He pointed out the discrepancy in attention given to veterans compared to other political issues.
- Beau urged for a deeper understanding of veterans' nature and character.
- He emphasized the importance of not using veterans as an excuse to avoid helping others.
- Beau encouraged genuine care and involvement to support veterans.
- He stressed the significance of supporting truth before claiming to support the troops.

# Oneliner

Dennis, Terry, and Beau assist hurricane-affected community, shedding light on veteran support and the importance of truth over political exploitation.

# Audience

Activists, policymakers, community members

# On-the-ground actions from transcript

- Help distribute supplies to communities in need (exemplified)
- Support veterans through advocacy and aid efforts (exemplified)

# Quotes

"Before you can support the troops, you've got to support the truth."
"Stop turning them into combat veterans because some politician waved a flag and sold you a pack of lies that you didn't even bother to look into."

# Whats missing in summary

The full transcript provides a detailed account of community support efforts led by veterans, addressing misconceptions and advocating for genuine care towards veterans.

# Tags

#CommunitySupport #Veterans #TruthOverPolitics #HumanitarianAid #SupportTheTroops