# Bits

Beau Ginn says:

- Addresses tear gas used on children in the United States.
- Mentions justifications for violence seen on social media.
- Questions how one might react if their child was tear-gassed.
- Raises concerns about Border Patrol presence in certain areas.
- Comments on the heavy presence of cartels in those areas.
- Suggests that every dead Border Patrol agent is a win for the cartels.
- Challenges the potential outcome if violence doesn't occur.
- Questions the impact of fearmongering by political figures.
- Criticizes tear-gassing of children as behavior seen in dictatorships.
- Comments on falling poll numbers among troops due to government actions.
- Expresses thoughts on the behavior of the government.
- Contemplates the response if violence does not escalate along the border.

# Oneliner

Beau Ginn addresses tear gas used on children, questions reactions, and criticizes government behavior, challenging societal perceptions.

# Audience

United States citizens

# On-the-ground actions from transcript

- Contact local representatives to express concerns about the use of tear gas on children (suggested)
- Join community organizations advocating for non-violent solutions (implied)

# Quotes

"Sometimes, we tear gassed kids."

"If there is not a massive upsurge in violence along the border, it says a whole lot about those people."

"Step on my flag, I'll step on your face."

# Whats missing in summary

The full transcript provides a deeper exploration of the impact of fearmongering and violence on society.

# Tags

#TearGas #Violence #GovernmentBehavior #BorderPatrol #Fearmongering