# Bits

Beau says:
- Explains red flag gun seizures, where local cops can temporarily seize guns with a court order if a person is deemed a threat.
- Points out the flawed execution of red flag laws, despite the sound idea behind them.
- Raises concerns about the violation of due process when guns are seized without proper legal procedures.
- Criticizes the tactical implementation of red flag seizures, citing a recent incident where a person was killed during a 5 a.m. gun seizure.
- Draws parallels between military tactics and police actions in gun seizures, questioning the appropriateness of such tactics.
- Expresses the need for cops to understand the implications of using military tactics in civilian settings.
- Advocates for proper training for law enforcement officers who are equipped with military-grade weapons.
- Suggests a different approach to executing red flag gun seizures by apprehending the individual on their way home rather than at their residence.
- Stresses the importance of due process in handling cases of potential threats.
- Urges both gun control advocates and pro-gun individuals to work towards fixing the flawed execution of red flag laws for the greater good.

# Oneliner

Red flag gun seizures have a sound idea but flawed execution, prompting Beau to advocate for a more thoughtful and just approach to prevent unnecessary violence.

# Audience

Legislators, law enforcement, activists

# On-the-ground actions from transcript

- Contact your representatives to urge immediate fixes to the flawed execution of red flag laws (suggested).
- Advocate for proper training for law enforcement officers involved in red flag gun seizures (suggested).
- Work towards ensuring due process is upheld in cases of potential threats (suggested).

# Quotes

- "The idea is sound, okay, the execution isn't."
- "Cops are picking up these tactics, and they're using them in a police setting when they're not designed to be."
- "That due process is so incredibly vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally, vitally."
- "It's a good idea. The execution is bad and the execution can be fixed very easily."
- "Y'all have a good night."

# Whats missing in summary

The full transcript provides a detailed analysis of the flawed execution of red flag gun seizures and proposes practical solutions to improve the process and prevent unnecessary harm.

# Tags

#RedFlagLaws #GunSeizures #DueProcess #LawEnforcement #CommunitySafety