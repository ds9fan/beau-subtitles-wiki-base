# Bits

Beau says:

- Correcting arguments against asylum seekers' entry after a wild response in the comments section.
- Explaining the legality of asylum seekers under U.S. law and international treaties.
- Addressing the misconception of entering a country illegally while claiming asylum.
- Challenging the notion of needing personal involvement to have an opinion on asylum seekers.
- Pointing out the hypocrisy in using homeless vets as a reason not to help asylum seekers.
- Comparing the costs of refugee resettlement to other government expenses.
- Clarifying that asylum seekers are not just seeking benefits but safety.
- Arguing that the situation with asylum seekers is indeed the U.S.'s problem due to past involvements in Central American countries.
- Calling out individuals who suggest staying in their own country instead of seeking asylum.
- Providing a stark comparison of the Syrian crisis scaled up to U.S. proportions.
- Dismissing unrealistic claims of combat readiness and tough talk from internet commentators.
- Expressing a desire for constructive and respectful discourse.

# Oneliner

Explaining the legality and moral imperative of supporting asylum seekers against common arguments, with a call for empathy and understanding.

# Audience

Policy advocates, humanitarian organizations, activists.

# On-the-ground actions from transcript

- Support asylum seekers through local organizations (suggested).
- Advocate for policy changes to improve asylum seeker reception (exemplified).

# Quotes

- "It's cheaper to be a good person."
- "Stop projecting your greed onto them. Maybe they just want safety."
- "You put their kid in harm's way because you wouldn't fix your country."
- "Die. That's it."
- "I hope you're with me because it's hot in the desert and your canteen still be cool because you got ice in your veins."

# Whats missing in summary

The full transcript provides a detailed breakdown of legal and moral arguments in support of asylum seekers, along with a stark comparison of the Syrian crisis to U.S. proportions, challenging misconceptions and tough rhetoric around immigration issues.

# Tags

#AsylumSeekers #LegalRights #MoralResponsibility #ImmigrationPolicy #RefugeeCrisis