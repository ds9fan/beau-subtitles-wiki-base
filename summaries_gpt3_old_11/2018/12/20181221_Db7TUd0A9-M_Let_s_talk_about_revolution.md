# Bits

Beau says:

- Explains the difference between a riot, rebellion, regime change, and revolution.
- Mentions how some revolutions are just schemes to put new people in power.
- Talks about the influence of foreign governments in revolutions.
- Defines a revolution as bringing a new idea to the table, changing forms of government, and decentralizing power.
- Points out that a revolution in the US could lead to the country's breakup due to its diversity.
- Warns about the violent nature and potential consequences of a revolution in the United States.
- Raises concerns about the US's reliance on transportation and the impact of interruptions during a revolution.
- Argues that violent revolutions may not bring the desired change and suggests focusing on new ideas for transformation.
- Expresses curiosity about government structures that could empower more people.

# Oneliner
A revolution in the US could be violent and lead to catastrophic consequences due to its diverse nature and reliance on transportation, urging a focus on new ideas for transformation.

# Audience
Activists, citizens

# On-the-ground actions from transcript
- Prepare for emergencies (implied)
- Share new ideas for government structures (implied)

# Quotes
- "You can put a bullet in it, or you can put a new idea in it."
- "A violent revolution will be horrible."
- "If you're going to change the government, yeah, it can be done with bullets and lamp posts."

# Whats missing in summary
In-depth analysis of potential government structures empowering more people.

# Tags
#Revolution #Government #Violence #US #Transformation #Ideas