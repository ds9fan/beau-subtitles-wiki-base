# Bits

Beau says:

- Reacts to backlash over wearing a t-shirt with Disney princesses and delves into anti-feminist memes on Facebook comments.
- Identifies two main types of anti-feminist memes: the "make me a sandwich" stereotype and the "feminism makes you ugly" trope.
- Challenges the shallow nature of valuing a woman based on cooking abilities and criticizes those who cyber-stalk feminists to body shame them.
- Points out the immaturity of individuals who resort to using appearance as a basis for discrediting feminist arguments.
- Counters the notion that feminism is unnecessary by citing the continued existence of patriarchy in society.
- Presents statistics on the underrepresentation of women in positions of power in government to illustrate the persistence of patriarchy.
- Addresses common justifications for gender disparities in job promotions and challenges the stereotypes that hinder women's progress.
- Supports diversity of tactics in the feminist movement and underscores the importance of allowing women to define themselves on their own terms.

# Oneliner

Beau delves into anti-feminist memes, challenges shallow stereotypes, and underscores the ongoing relevance of feminism in combating patriarchy.

# Audience

Social media users

# On-the-ground actions from transcript

- Challenge anti-feminist rhetoric online by engaging in constructive dialogues and promoting gender equality (implied).
- Support feminist causes and organizations advocating for gender equity in your community (implied).

# Quotes

- "The objectification of women, guys, that is not an argument against feminism. That's why feminism exists."
- "At the end of the day, all women have to be two things, and that's it. Who and what they want."

# Whats missing in summary

Full context and depth of Beau's analysis on anti-feminist memes and patriarchy.

# Tags

#Feminism #GenderEquality #Patriarchy #AntiFeministMemes #GenderRepresentation