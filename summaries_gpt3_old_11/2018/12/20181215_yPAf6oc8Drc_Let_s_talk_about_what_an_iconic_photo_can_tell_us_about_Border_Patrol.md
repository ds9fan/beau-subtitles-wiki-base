# Bits

Beau says:

- Recounts a story from the Vietnam War, specifically the Battle of Saigon, involving a Viet Cong trooper who continued fighting despite severe injuries.
- Mentions the iconic photo from the Vietnam War where American troops, instead of killing the injured Viet Cong trooper, offer him water.
- Shifts the focus to address the inhumane actions of Border Patrol agents who destroy water left by volunteers for migrants crossing dangerous routes.
- Compares Border Patrol agents' actions to videos of teenagers torturing others and questions their humanity.
- Argues against the idea that such actions are meant to deter "bad elements," pointing out the vulnerable groups affected by the lack of water.
- Asserts that no physical barriers can deter people's drive for safety, freedom, and opportunities, criticizing the futility of border walls.
- Urges Border Patrol agents to quit their jobs, warning them that they will be held accountable for their actions once the current administration leaves office.
- Condemns the Border Patrol agents' actions as "depraved indifference" and advises them to resign rather than hide behind orders.
- Cautions against using "just following orders" as a defense, drawing a historical parallel to the consequences faced by those who used that defense in the past.

# Oneliner

Border Patrol agents' inhumane actions contrast with humanity shown in war, urging accountability and resignation to avoid future repercussions.

# Audience

Advocates, Activists, Border Communities

# On-the-ground actions from transcript

- Quit your job at Border Patrol (suggested)
- Turn in your resignation (implied)
- Advocate for accountability within Border Patrol (exemplified)

# Quotes

- "You cannot deter the drive for safety."
- "Your badge is not going to protect you from that."
- "Just following orders isn't going to cut it."

# What's missing in summary

The full transcript provides a detailed and emotive exploration of the contrast between humanity and inhumanity in different contexts, urging accountability and action in response to unjust practices.

# Tags

#BorderPatrol #Humanity #Accountability #VietnamWar #Injustice