# Bits

Beau says:

- Explains the concept of globalism and how it's perceived by different groups.
- Mentions that some individuals associate globalism with Jewish conspiracy theories.
- Describes globalism as a new world order with a single government making decisions for everyone.
- Contrasts globalism with nationalism, stating they both serve the purpose of keeping people in their place.
- Questions the representation aspect of democracy and whether politicians truly represent the public.
- Argues that both globalism and nationalism are ways to divert attention from real issues.
- Points out the irony of blaming foreigners for job loss instead of focusing on corporate decisions.
- Expresses a belief in finding better ways to govern the world without relying on a select few.
- Defines a nationalist as a low-ambition globalist who only wants control over a small part of the world.
- Concludes by critiquing the misuse of the terms nationalist and globalist as buzzwords in politics.

# Oneliner

Globalism and nationalism both serve to maintain power structures by diverting attention from real issues through different means.

# Audience

Politically aware individuals

# On-the-ground actions from transcript

- Question the motives behind political rhetoric (implied)
- Challenge the misuse of terms like nationalist and globalist in political discourse (implied)

# Quotes

- "All a nationalist is, is a low-ambition globalist."
- "It's turned into a buzzword, and it's so funny to watch people with identical ideologies to the one they're criticizing, use it as an insult."

# What's missing in summary

The nuances and examples provided by Beau in explaining the concepts of globalism and nationalism can be best understood by watching the full transcript.