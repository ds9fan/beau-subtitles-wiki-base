# Bits

Beau says:

- Explains the origins of MS-13, tracing back to a coup in El Salvador in 1979.
- Notes that the US government backed the coup, leading to a decade-long civil war and the creation of refugees.
- Mentions that these refugees ended up in California and formed a minority among immigrants.
- Describes how the minority immigrant group eventually evolved into a street gang for protection.
- Talks about how MS-13 transitioned from a street gang to a transnational criminal enterprise.
- Reveals the influence of a man nicknamed Satan, who was trained by the US military and played a significant role in transforming MS-13.
- Questions the rationale behind the push for a border wall in response to MS-13, pointing out the role of American foreign policy in creating the gang.
- Suggests that instead of focusing on walls, curtailing American foreign policy might be a more effective approach.
- Raises concerns about the lack of scrutiny and vetting for individuals trained by the US military to carry out violent actions.
- Advocates for addressing the root of the issue rather than reacting impulsively.

# Oneliner

MS-13's origins tied to US-backed actions, urging reflection on American foreign policy over border walls and vetting practices.

# Audience

Policy Makers, Activists

# On-the-ground actions from transcript

- Advocate for policy changes to address the root causes of gang violence (suggested)
- Support organizations working towards dismantling gang violence through community intervention (exemplified)

# Quotes

- "MS-13 is an American creation, not just in the sense that it was founded here, but in the sense that every step of the way, it was American foreign policy that built it."
- "Maybe a better idea [than building a wall] might be to curtail our foreign policy."
- "You never hear them calling for strict scrutiny or vetting of those people that the U.S. military trains to murder, torture, assassinate, and dominate a community by fear."

# Whats missing in summary

In-depth analysis of the historical context and consequences of American foreign policy on the formation and growth of MS-13.

# Tags

#MS-13 #AmericanForeignPolicy #GangViolence #USMilitaryTraining #RootCauses