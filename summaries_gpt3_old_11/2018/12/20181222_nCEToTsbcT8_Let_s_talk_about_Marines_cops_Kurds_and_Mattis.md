# Bits

Beau says:

- The theme of duty is prevalent in recent news, particularly with the ruling from a federal court in Florida stating that law enforcement has no duty to protect individuals.
- General Mattis's resignation is believed to be rooted in his sense of duty, as evidenced by his honor, integrity, and sense of duty as described by fellow Marines.
- Despite some Marines behaving dishonorably, the Marine Corps as an institution values honor, integrity, and a sense of duty.
- Beau surprisingly agrees with President Trump's decision to withdraw from Syria, citing that the U.S. should not have been involved there in the first place.
- Mattis's sense of duty may stem from feeling that leaving allies in Syria behind goes against his principles.
- Kurdish people, spread across countries like Turkey, Syria, Iraq, and Iran, are seen as being abandoned by the U.S. and may now have to pursue independence independently.
- The U.S. leaving Syria may provide an opening for Kurdish independence without U.S. intervention.
- Beau suggests that many Kurdish fighters may view it as their duty to continue the fight for independence.
- The federal court ruling reaffirms that law enforcement has no obligation to protect individuals, even in extreme situations like mass shootings.
- Beau warns that government and law enforcement are not present to protect citizens but rather to control them, urging people to be cautious about legislation that could limit their ability to protect themselves.

# Oneliner

Law enforcement has no duty to protect individuals, and duty plays a significant role in recent news, from Mattis's resignation to Kurdish independence prospects.

# Audience

Advocates for individual safety and empowerment

# On-the-ground actions from transcript

- Support Kurdish independence efforts (implied)
- Advocate for legislation that empowers individuals to protect themselves (implied)

# Quotes

- "Law enforcement is not there to protect you. We do not live under a protectorment."
- "Do some cops save lives? Yes, but they do it in spite of the law, not because of it."
- "Government is there to control you, not protect you."

# Whats missing in summary

The full transcript delves deeper into the complexities of duty, government control versus protection, and the implications for individual safety and empowerment.