# Bits

Beau says:

- Analyzing the internet tough guy as a symptom and analogy for American foreign policy.
- Drawing parallels between the internet tough guy's behavior and American foreign policy's insulation from consequences.
- Exploring how Americans can ignore or back horrible foreign policy due to being insulated from repercussions.
- Addressing the disconnect between Americans and global issues, leading to invasion and lack of empathy.
- Challenging the notion that being born in America makes one more worthy of humanity.
- Exploring the impact of the wealthy and powerful on global injustice.
- Advocating for change starting with individuals and their consumer choices.
- Stating that change must begin with individuals, particularly those watching the video.
- Linking global income disparities to the responsibility for instigating change.
- Implying that any revolution must be global in today's interconnected world.

# Oneliner

Analyzing the internet tough guy as a symptom of American foreign policy's insulation from consequences, urging individuals to instigate global change through consumer choices.

# Audience

Global citizens

# On-the-ground actions from transcript

- Start influencing change with your consumer choices, as every purchase sends a message to corporations (implied).
- Recognize the power of individual actions in initiating global change (implied).

# Quotes

- "Any revolution that is not global, it's not a revolution."
- "We are telling those massive corporations what kind of world we want; we're voting with our debit cards."

# Whats missing in summary

The full transcript delves deeper into the interconnectedness of global issues and the necessity for a global revolution to drive meaningful change.

# Tags

#GlobalChange #ConsumerActivism #AmericanForeignPolicy #IncomeInequality #Revolution