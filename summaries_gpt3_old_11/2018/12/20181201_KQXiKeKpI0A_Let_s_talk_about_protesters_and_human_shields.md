# Bits

Beau says:
- Protesters are upset with the government for not upholding their rights and laws, leading to interference with life, liberty, and happiness.
- Militarized goons confront protesters who throw rocks, sticks, and snowballs, reminiscent of a historical event.
- The scenario described mirrors the Boston Massacre, where government forces opened fire on protesters, sparking a revolution.
- Beau points out that although no one was killed in this situation, the potential harm was disregarded, similar to historical events.
- Mention of using children as human shields and how it contradicts the belief in American honor and integrity.
- Reference to Saddam Hussein using human shields during the Gulf War, illustrating the military term's origin.
- Beau questions those supporting the use of force against civilians and asks how they felt about similar incidents in history, like Waco, Texas.
- Strong condemnation of opening fire on unarmed crowds and undermining the Constitution.
- Beau expresses confusion about why there is a debate on the immorality of such actions.
- The transcript concludes with a critical reflection on current actions betraying American values.

# Oneliner

Protesters face militarized response, evoking historical parallels and moral outrage at endangering civilians.

# Audience

Activists, protestors, community members

# On-the-ground actions from transcript

- Stand against immoral actions (exemplified)
- Uphold rights and laws (suggested)

# Quotes

- "You do not punish the child for the sins of the father."
- "You do not open fire on unarmed crowds."
- "That's America. That's how we make America great, betraying everything that this country stood for."

# Whats missing in summary

The full transcript provides a deeper exploration of historical parallels and moral implications of using force against civilians in protests.

# Tags

#Protest #HumanRights #Militarization #AmericanValues #History