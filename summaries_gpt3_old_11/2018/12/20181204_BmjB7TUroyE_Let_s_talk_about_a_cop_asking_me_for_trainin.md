# Bits

Beau says:

- Recounts an encounter with cops during a training session where one suggested hitting a suspect in the knee with a baton for effectiveness.
- Describes the dangerous legal, tactical, and medical implications of such actions.
- Expresses concern about cops being able to say anything in a police report, even if it's false.
- Shares his decision to stop teaching law enforcement classes due to this experience.
- Talks about an officer who learned about new concepts like time, distance, and cover from a video on police militarization.
- Explains the significance of time, distance, and cover in police work using real-life examples like the shootings of Tamir Rice and John Crawford.
- Mentions the importance of auditory exclusion in high-stress situations and how it affects perception and response.
- Advises on proper handling of situations where suspects may not immediately comply due to biological responses like fight, flight, or freeze.
- Warns against the dangers of positional asphyxiation and the importance of proper restraint techniques.
- Stresses the necessity of wearing protective vests and staying behind cover during shootouts to avoid unnecessary harm.
- Shares a personal story illustrating the importance of applying the spirit, not just the letter, of the law in volatile situations.
- Warns against enforcing unjust laws and dealing with ideologically motivated individuals who pose greater risks.
- Advises against seeking advanced training within one's department due to potential repercussions and suggests external advocacy for reform.

# Oneliner

Beau recounts encounters with law enforcement, stresses critical training gaps, and offers advice on applying the law with caution.

# Audience

Law enforcement personnel

# On-the-ground actions from transcript

- Seek advanced training on critical concepts like time, distance, and cover to improve policing practices (suggested).
- Advocate for reform and advanced training within police departments to enhance officers' skills (exemplified).

# Quotes

- "Time, distance, and cover, please start to use it."
- "The most dangerous people on the planet are true believers."
- "You need to stay away from ideologically motivated people."

# Whats missing in summary

Watching the full transcript provides a comprehensive understanding of Beau's insights on law enforcement training gaps and practical advice for officers.

# Tags

#LawEnforcement #TrainingGaps #PoliceReform #CriticalAdvice #UseOfForce