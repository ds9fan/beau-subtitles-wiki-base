# Bits

Beau says:

- Border Patrol uses border apprehensions as a gauge for the number of illegals entering the US.
- The number of people coming across the southern border is at a 40-year low.
- Trump's policy on illegal immigrants hasn't significantly impacted the numbers.
- People are being scared into supporting unnecessary measures like the border wall.
- The US is no longer seen as a beacon of freedom due to lack of critical thinking.
- Raising billions for a wall is misguided when there are more pressing issues.
- Beau suggests starting a GoFundMe to audit the money raised for the wall.
- Blending private business interests with government is a form of fascism.
- The wall won't address the root causes of illegal immigration.
- Fear tactics are used to manipulate public opinion and justify unnecessary actions.

# Oneliner
Border apprehensions are at a 40-year low, yet fear tactics are fueling support for an ineffective border wall.

# Audience
American citizens

# On-the-ground actions from transcript
- Start a GoFundMe to audit the money raised for the border wall (suggested)
- Challenge fear-based narratives and encourage critical thinking in political discourse (implied)

# Quotes
- "We are people who elect rulers instead of leaders."
- "We are no longer the independent people, the critical thinkers that we once were."
- "The fact that you're trying is pretty scary for two reasons."
- "How many people right now terrified of MS-13 coming to the United States?"
- "Nobody in DC is going to look out for you."

# Whats missing in summary
The full transcript provides in-depth analysis and examples of how fear tactics and misinformation are used to push for ineffective policies like the border wall.

# Tags
#Immigration #BorderWall #FearTactics #CriticalThinking #Fascism