# Bits

Beau says:

- Explains the origins of "rule 303" among military contractors and its transition to law enforcement.
- Describes a historical incident where a soldier invoked "rule 303" after shooting locals, believing he had the means and thus the responsibility to act.
- Points out that "rule 303" has evolved into "might makes right" in modern contexts, contrary to its original meaning.
- Gives examples of recent events where individuals acted based on having the means and therefore the responsibility to intervene.
- Draws parallels between the concept of "rule 303" and the duty of law enforcement officers to protect individuals.
- Addresses excuses made for an officer who did not intervene during the Parkland shooting, debunking arguments like salary concerns, split-second decisions, and weapon discrepancies.
- Challenges the notion that an officer's duty is bounded by risk and stresses the responsibility to act when having the means.
- Urges officers in schools to question if they are expendable and stresses the importance of prioritizing student safety over personal survival.

# Oneliner

Military contractors and law enforcement officers should understand the true meaning of "rule 303" - having the means carries the responsibility to act.

# Audience

Law enforcement, school resource officers

# On-the-ground actions from transcript

- Reassess your commitment to protecting those under your care (implied)
- Prioritize student safety over personal survival (implied)

# Quotes

- "Your duty is not bounded by risk."
- "You chose the profession of arms. Your duty is to act to protect those kids."
- "If you cannot truly envision yourself laying down your life to protect those kids, you need to get a different assignment."

# Whats missing in summary

The full transcript provides a detailed analysis of the concept of "rule 303" and its application in military and law enforcement contexts, urging officers to prioritize their duty to protect individuals over personal safety.

# Tags

#Rule303 #LawEnforcement #Responsibility #Duty #ProtectTheKids