# Bits

Beau says:

- Admits being wrong about asylum seekers applying for asylum in the first country they come to.
- Discovered a EU court ruling stating refugees must apply in the first member state they enter.
- Explains the Safe Third Country Agreement between the US and Canada.
- Clarifies that the Agreement only applies to the US and Canada, not to other countries.
- Mentions efforts in the US to expand the treaty's application and Canada's desire to scrap it due to US treatment of asylum seekers.
- Trump's attempt to designate Mexico as a safe third country was ridiculed worldwide.
- Canadians are considering scrapping the Agreement due to the US no longer meeting the safety requirements.
- Urges people to not believe everything they see in memes and to seek accurate legal information.
- Points out that the widespread belief about applying in the first country is incorrect.
- Stresses the importance of understanding immigration law and not spreading misinformation.

# Oneliner

Beau admits he was wrong about asylum seekers applying in the first country, explaining the Safe Third Country Agreement between the US and Canada while warning against misinformation from memes.

# Audience

Legal researchers, advocates

# On-the-ground actions from transcript

- Read and understand international treaties and agreements (suggested)
- Seek accurate legal information from reliable sources (suggested)
- Advocate for fair treatment of asylum seekers and refugees (implied)

# Quotes

- "Knowledge is power. Don't get your legal information from memes."
- "People that know immigration law were so dumbfounded by this widespread belief."
- "Don't believe everything you see in memes."

# Whats missing in summary

Deeper understanding of the implications of spreading misinformation about asylum seekers applying in the first country they come to.

# Tags

#Immigration #AsylumSeekers #SafeThirdCountryAgreement #Misinformation #LegalInformation