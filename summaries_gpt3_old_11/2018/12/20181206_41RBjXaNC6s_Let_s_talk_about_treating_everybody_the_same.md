# Bits

Beau says:

- Advocates for treating everybody fairly rather than the same to build a just society.
- Shares a personal anecdote about learning the difference between treating everyone the same and treating everyone fairly.
- Recounts a story of the director of photography for Olin Mills challenging the idea of treating everybody the same.
- Mentions the detrimental effects of McNamara's Project 100,000 during the Vietnam War when enlistment standards were waived.
- Points out that treating everybody the same doesn't always equate to fairness.
- Expresses the importance of understanding and learning about different cultures and religions to treat everybody fairly.
- Notes the American demonization of education and learning about other cultures as a barrier to treating everybody fairly.

# Oneliner

Treating everybody fairly, not the same, is key to building a just society and understanding different cultures.

# Audience

Community members, educators, policymakers.

# On-the-ground actions from transcript

- Learn about different cultures and religions to understand where others are coming from (suggested).
- Challenge the demonization of education and strive to learn more about other cultures (implied).

# Quotes

- "The goal shouldn't be to treat everybody the same. It should be to treat everybody fairly."
- "Treating everybody the same is not treating everybody fairly."
- "In order to treat everybody fairly, you have to know where they're coming from."

# Whats missing in summary

The full transcript provides more depth on the importance of education, understanding cultural differences, and the consequences of treating everyone the same without considering individual circumstances.

# Tags

#Fairness #CulturalUnderstanding #Education #SocialJustice #CommunityLeaders