# Bits

Beau says:

- Using Elf on the Shelf to address social issues has sparked anger and controversy, evident from Facebook shares and comments sections.
- Connects the criticism of feminists in comic books addressing social issues to his use of Elf on the Shelf for the same purpose.
- Recalls his limited exposure to comic books as a kid, primarily Punisher and G.I. Joe, which he perceives as masculine-themed without room for feminism or social issues.
- Analyzes Punisher's storyline beyond vengeance, suggesting it delves into trauma, PTSD, and psychological issues.
- Points out the strong feminist undercurrent in G.I. Joe, with competent female characters and the exploration of social issues throughout the series.
- Contrasts popular beliefs about the 80s G.I. Joe cartoon being Cold War propaganda with the writers' anti-establishment stance and addressing social issues through Cobra's actions.
- Mentions the X-Men's central theme of mutants' civil rights, drawing parallels to allegories for being gay or the American Civil Rights Movement.
- Examines Superman's narrative as a refugee alien symbolizing the pursuit of freedom and opportunities, reflecting themes of immigration and bravery.
- Argues that social issues have always been present in comics, challenging the audience's perception by suggesting they were oblivious to it before.
- Addresses the discomfort among the core audience of comic books, mainly white males, due to the current social issues being tackled within the industry.

# Oneliner

Using pop culture references, Beau dismantles the misconception that comics are devoid of social issues, challenging the audience to confront uncomfortable truths.

# Audience

Comic book enthusiasts

# On-the-ground actions from transcript

- Analyze and critically interpret the social issues addressed in various comic book series (suggested)
- Challenge stereotypes and biases within the comic book industry by promoting diverse representation (implied)

# Quotes

- "Social issues have always been in comics. You were just too dense to catch it."
- "If you have a privilege for so long, and you start to lose it, it feels like oppression."
- "Two chunks of plastic have had more of an impact than the actual stories of people who underwent some pretty horrible things."

# Whats missing in summary

The full transcript delves into the intersection of pop culture, social issues, and audience perceptions in comic books, urging a reevaluation of how narratives convey deeper societal commentary.

# Tags

#Comics #SocialIssues #Feminism #PopCulture #Representation #AudiencePerception