# Bits

Beau says:

- Recounts a story from college where he intervened in a potentially dangerous situation at a bar to help a girl he vaguely knew.
- Describes how he pretended to be the girl's brother to diffuse the situation and make the guy back off.
- Talks about how the guy respected Beau's property rights over the girl's autonomy, which he found troubling.
- Draws a parallel between the bar incident and the current discourse around the Kavanaugh hearing.
- Uses a math analogy to show the prevalence of sexual assault among women and how it may affect those around us.
- Mentions the emotional display of Kavanaugh during the hearing and questions his suitability for a judge based on his emotional outbursts.
- Points out the power dynamics involved in sexual assault, focusing on control rather than just sex.
- Talks about the motivation for women to come forward with allegations, especially when the accused may have power over millions of others.
- Compares the Kavanaugh situation to the Catholic priest cases where multiple victims came forward over time.
- Encourages reflection on the impact of not believing or supporting survivors of assault on real-life relationships.

# Oneliner

Beau recounts a bar incident to shed light on respecting autonomy, parallels it with the Kavanaugh hearing, and urges reflection on supporting assault survivors to preserve real-life relationships.

# Audience

Social justice advocates

# On-the-ground actions from transcript

- Reach out to survivors for support and validation (implied)
- Take time to understand the dynamics of power and control in sexual assault (implied)
- Challenge harmful comments that discourage survivors from coming forward (implied)

# Quotes

- "It's about power and control."
- "You're altering real-life relationships over bumper sticker politics."
- "Do the math and then think about it."

# Whats missing in summary

The full transcript provides a nuanced perspective on intervening in harmful situations, understanding power dynamics in assault cases, and the importance of supporting survivors in maintaining trust and relationships.

# Tags

#RespectAutonomy #SupportSurvivors #PowerDynamics #BelieveWomen #Reflection