# Bits

Beau says:

- Addresses the Me Too movement in light of the Kavanaugh nomination, focusing on sexual harassment and assault prevention.
- Offers advice on avoiding risky situations, using analogies like leaving a bicycle unlocked in front of a meth head's house.
- Stresses safeguarding reputation to attract the right kind of people.
- Suggests ending dates at the front door and avoiding taking anyone home if drinking.
- Warns about the importance of being mindful of words and signals sent, especially keeping hands to yourself.
- Addresses the "him-too" hashtag and men's fears of false accusations of sexual harassment or assault.
- Notes that false accusations happen about 2% of the time and usually allegations are true.
- Encourages men to be accountable for their actions and avoid situations where they could be falsely accused.

# Oneliner

Beau offers advice on preventing sexual harassment and assault, urging accountability and mindfulness in actions and signals sent.

# Audience

Men, women, allies

# On-the-ground actions from transcript

- Safeguard your reputation to attract the right kind of people (suggested)
- End dates at the front door and avoid taking anyone home if drinking (suggested)
- Be mindful of the words you use and signals you send (suggested)
- Keep your hands to yourself to avoid sending the wrong signals (suggested)
- Be accountable for your actions to prevent false accusations (suggested)

# Quotes

- "Gentlemen there is one cause of rape, and that's rapists."
- "Don't put yourself in a situation where you can be falsely accused 2% of the time."
- "The miniskirt said, am I really to blame? And the hijab said, no, it happened to me too."

# Whats missing in summary

Full context and nuances of Beau's advice and reflections on sexual harassment and assault prevention.

# Tags

#MeToo #SexualHarassment #Prevention #Accountability #GenderEquality