# Bits

Beau says:
- Analyzes the shooting in Dallas from a non-law enforcement perspective.
- Points out the significance of truth-telling behaviors in liars.
- Questions the credibility of the officer's story in the shooting incident.
- Raises the possibility of a hidden motive due to noise complaints filed by the officer against the victim.
- Suggests a different narrative where the officer's actions could be seen as a home invasion.
- Mentions the potential legal consequences for the officer if viewed as a home invasion in Texas.
- Recalls a past encounter discussing Black Lives Matter and All Lives Matter.
- Expresses skepticism about the investigation process and potential police corruption.
- Criticizes the lack of accountability within the Dallas Police Department.
- Condemns the attempt to vilify the victim posthumously.
- Concludes by reflecting on the tarnished image of law enforcement and the need for justice.

# Oneliner

The shooting in Dallas is scrutinized through a critical lens, questioning motives and justice while addressing systemic issues within law enforcement.

# Audience

Activists, Community Members

# On-the-ground actions from transcript

- Challenge police corruption and demand accountability within law enforcement (suggested).
- Support justice for victims of police violence by advocating for thorough investigations (suggested).

# Quotes

- "If this is the amount of justice they get, they don't."
- "Not one cop has crossed the thin blue line to say that's wrong."
- "It's a few bad apples spoils the bunch."

# Whats missing in summary

Full video provides in-depth analysis and commentary on the Dallas shooting, police accountability, and justice.

# Tags

#DallasShooting #PoliceAccountability #BlackLivesMatter #Justice #PoliceCorruption