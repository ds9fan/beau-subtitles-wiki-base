# Bits

Beau says:
- Gun control has failed due to being written by those unfamiliar with firearms, leading to ineffective measures.
- The term "assault weapon" is made up and lacks a concrete definition within firearms vocabulary.
- The criteria set by the assault weapons ban were arbitrary and did not address weapon lethality.
- Banning high-capacity magazines is ineffective as changing magazines is quick and rushing an attacker in that moment is dangerous.
- Banning specific firearms like the AR-15 could lead to the use of more powerful weapons like the Garand or M14.
- Banning semi-automatic rifles altogether is impractical and costly, as these designs are not technologically advanced and can be easily manufactured.
- Restrictions on firearms in other countries did not effectively reduce overall murder rates, just changed the method of killing.
- Raising the age limit for purchasing firearms from 18 to 21 could be a more effective solution, especially for preventing school shootings.
- Closing loopholes in existing laws regarding domestic violence and gun ownership is vital to prevent future violence.

# Oneliner

Gun control measures have largely failed due to a lack of understanding of firearms, with solutions like banning assault weapons or high-capacity magazines proving ineffective, while raising the age limit for firearm purchases could be a more practical step towards reducing gun violence.

# Audience

Legislators, policymakers, gun control advocates

# On-the-ground actions from transcript

- Raise the age limit for purchasing firearms from 18 to 21 (suggested)
- Close loopholes in existing laws regarding domestic violence and gun ownership (implied)

# Quotes

"Legislation is not the answer here in any way shape or form." 

"Raising the age from 18 to 21? Yeah, that's a great idea. That's brilliant, actually."

"When you make something illegal like that, you're just creating a black market."

# Whats missing in summary

The full transcript provides a detailed breakdown of why current gun control measures are ineffective and suggests practical steps like raising the age limit for firearm purchases and closing loopholes in existing laws to address gun violence comprehensively.

# Tags

#GunControl #Firearms #Legislation #ViolencePrevention #AgeLimit