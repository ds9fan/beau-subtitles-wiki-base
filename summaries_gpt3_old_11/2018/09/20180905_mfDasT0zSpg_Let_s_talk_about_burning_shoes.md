# Bits

Beau says:

- Beau explains his intention to burn a pair of Nikes due to his dislike for the company.
- He reveals his son's possession of Nikes and his plan to burn them, leading to his son's objection.
- Beau questions the significance of burning shoes and the association with freedom.
- He addresses the idea of donating the shoes to those in need instead of burning them.
- Beau draws parallels between burning shoes and burning the American flag as symbols.
- He criticizes those who focus on symbolic gestures without understanding the deeper issues.
- Beau challenges individuals who are quick to disassociate with a company over a commercial but ignore larger ethical concerns.
- He points out the hypocrisy of valuing symbols over genuine freedom.
- Beau encourages reflection on the true meaning of freedom and actions that support it.
- He concludes by urging people to prioritize true freedom over symbolic gestures.

# Oneliner

Beau questions the symbolism of burning Nikes, urging reflection on true freedom and meaningful actions over symbolic gestures.

# Audience

Consumers, activists, ethical individuals

# On-the-ground actions from transcript

- Donate unwanted items to shelters or thrift stores near military bases (suggested)

# Quotes

"Y'all have a good day."
"If you're loving that symbol of freedom more than you love freedom."

# Whats missing in summary

Full context and nuances of Beau's passionate argument on symbolism and true freedom.

# Tags

#Symbolism #Freedom #Ethics #ConsumerActivism #SocialResponsibility