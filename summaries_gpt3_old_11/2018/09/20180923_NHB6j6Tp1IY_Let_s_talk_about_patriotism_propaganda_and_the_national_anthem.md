# Bits

Beau says:

- Responding to an op-ed story shared by two people about a woman attacked for kneeling during the national anthem at a county fair.
- Expresses irritation and reads the author's bio, feeling compelled to respond and bring attention to the issue.
- Questions the author's perspective on patriotism and protest, particularly in relation to the purpose of a protest.
- Challenges the author's views on kneeling for the fallen and questions their understanding of the protest's meaning.
- Points out the difference between nationalism and patriotism, advocating for correcting the government when it is wrong.
- Shares a personal anecdote about a colonel who believed patriotism is displayed through actions, not symbols.
- Encourages a deeper understanding of patriotism and suggests seeking guidance from experienced individuals before giving lectures.

# Oneliner

Beau responds to an op-ed story, challenging perspectives on patriotism and protesting, advocating for a deeper understanding of true patriotism through actions.

# Audience

Americans

# On-the-ground actions from transcript

- Contact Beau for connections with experienced individuals to understand true patriotism (suggested)

# Quotes

- "Patriotism is correcting your government when it is wrong."
- "Patriotism was displayed through actions, not worship of symbols."
- "There is a very marked difference between nationalism and patriotism."

# Whats missing in summary

Full understanding of true patriotism through actions and correcting the government when necessary.

# Tags

#Patriotism #Protest #NationalAnthem #Community #Understanding