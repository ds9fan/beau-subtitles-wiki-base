# Bits

Beau says:

- Recalls his experience on September 11th, watching the attacks unfold on TV.
- Notes the casualty of that day that often goes unnamed - the erosion of freedom.
- Describes the gradual loss of freedoms post-9/11 due to government actions.
- Mentions the strategic nature of terrorism and its goal to provoke overreactions.
- Points out the danger of sacrificing freedoms in the name of security.
- Urges against normalizing constant surveillance and militarization of police.
- Advocates for teaching children about the importance of freedom.
- Stresses the need for self-reliance and community preparedness in disasters.
- Suggests counter-economic actions to reduce government influence.
- Recommends surrounding oneself with like-minded individuals to support freedom.
- Encourages helping those who have been marginalized or hit rock bottom.
- Talks about the power of community building and grassroots efforts in protecting freedoms.

# Oneliner

Beau urges for grassroots community action to safeguard freedom in the face of government overreach post-9/11.

# Audience

Community members

# On-the-ground actions from transcript

- Teach children about the importance of freedom (suggested)
- Be self-reliant and prepared for disasters (suggested)
- Practice counter-economic actions to reduce government influence (suggested)
- Surround yourself with like-minded individuals for support (suggested)
- Help marginalized individuals and those in need (suggested)
- Build strong community connections to protect freedoms (suggested)

# Quotes

"Y'all been giving freedom CPR, but we can't let this become normal."
"Defeat an overreaching government by ignoring it."
"The face of tyranny is always mild at first."

# Whats missing in summary

The full transcript provides a detailed analysis of the erosion of freedoms post-9/11, the strategic nature of terrorism, and the importance of grassroots community action in safeguarding freedom.

# Tags

#September11 #Freedom #Terrorism #CommunityAction #GrassrootsOrganizing