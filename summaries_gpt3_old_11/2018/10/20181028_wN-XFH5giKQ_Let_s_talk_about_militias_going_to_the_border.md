# Bits

Beau Ginn says:

- Beau is on the Mexican border with his militia commander to "protect America" but won't stop refugees on the Mexican side to avoid legal violations.
- They won't take action on the American side either, as it's against the law to send them back at gunpoint and violates core principles like upholding the Constitution.
- Beau mentions that individuals entering the U.S. have rights, including due process, and they wouldn't violate that unless they were traitors.
- They are essentially just backing up the Department of Homeland Security (DHS) and hanging out with them, despite the DHS labeling militia members as potential terrorists or extremists.
- Beau expresses concern about the optics of their actions, especially if they follow through with their rhetoric, as it could expose their identities and operations.
- He hints at his militia commanders playing "4D chess" to ensure they look good and maintain a positive image while being on the border ready to confront unarmed asylum seekers.

# Oneliner

Beau Ginn is on the Mexican border with his militia, not taking action against refugees to avoid legal violations, while expressing concerns about optics and potential consequences of their rhetoric.

# Audience

Community members near the border.

# On-the-ground actions from transcript

- Support local organizations aiding asylum seekers (suggested)
- Connect with border community groups to understand the situation better (implied)

# Quotes

- "We're down here on the border protecting America with our guns, ready to kill a bunch of unarmed asylum seekers who are following the law."
- "Hey, Colonel!"

# Whats missing in summary

The full transcript provides a deeper insight into the complexities of border protection and the internal conflicts within militia groups.

# Tags

#BorderProtection #Militia #LegalViolations #Optics #AsylumSeekers #HumanRights