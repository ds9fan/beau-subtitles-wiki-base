# Bits

Beau says:
- Updates viewers on surviving Hurricane Michael and the aftermath.
- Expresses gratitude for the support received and assures everyone's safety.
- Talks about the issue of looting post-natural disasters and its implications.
- Addresses law enforcement response to looting and its impact on relief efforts.
- Mentions media portrayal of looting after disasters like Katrina.
- Questions the priorities of law enforcement in closing down areas due to looting threats.
- Points out the inherent flaws in law enforcement assumptions about looters.
- Praises the Florida National Guard for their quick response and aid efforts.
- Stresses the importance of individual preparedness in emergencies.
- Encourages viewers to assemble emergency kits with essentials for survival.
- Offers practical tips and resources for creating emergency bags at different budget levels.
- Advises including medical supplies, especially for those dependent on daily medications, in emergency kits.
- Urges viewers to take action in preparing for emergencies to ensure survival.

# Oneliner

Law enforcement's response to looting post-natural disasters hinders relief efforts, prompting Beau to advocate for individual emergency preparedness.

# Audience

Community members

# On-the-ground actions from transcript

- Assemble emergency kits with essentials for survival (suggested)
- Include medical supplies and extra medications in emergency bags (suggested)
- Read resources and put together emergency bags (suggested)

# Quotes

- "Two van fulls of wet TVs are worth more than your life."
- "You can't count on government response."
- "If you're not going to read the article short version you need food water fire shelter medical supplies and a knife."

# Whats missing in summary

In-depth insights on the impact of law enforcement actions during natural disasters and the significance of individual preparedness in ensuring survival.

# Tags

#NaturalDisasters #EmergencyPreparedness #Looting #LawEnforcement #CommunitySafety