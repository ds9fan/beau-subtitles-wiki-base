# Bits

Beau says:
- Talks about the recurring issue of caravans coming to the US for the past 15 years.
- Mentions the US drug war policy and interventions in other countries as reasons for the caravans.
- Points out the violence in Honduras, where the current caravan originated, due to a high murder rate.
- Explains that tax dollars fund assistance for these migrants through the Office of Refugee Resettlement.
- Compares the cost of helping migrants get on their feet to the cost of detaining them, showing that it's cheaper to assist them.
- Addresses questions regarding why migrants travel in large groups, carry flags, wear military fatigues, and claim asylum legally.
- Urges people to address foreign policy and drug war impacts instead of blaming migrants.
- Expresses the moral dilemma of sending people back to dangerous situations and questions the justification for doing so.

# Oneliner

Caravans of migrants are a recurring issue due to US policies, and sending them back to danger lacks justification or humanity.

# Audience

Policy Makers

# On-the-ground actions from transcript

- Contact your representative to address foreign policy impacts on countries like Honduras (suggested).
- Advocate for changes in US drug war policies that affect violence in other countries (suggested).

# Quotes

- "It's cheaper to be a good person."
- "You can't preach freedom and then deny it, send people back to their death for no good reason."
- "These people will die. Anyway, they will die."

# Whats missing in summary

The full transcript provides a comprehensive understanding of the complexities and moral dilemmas surrounding the issue of migrant caravans and calls for a humane approach to addressing their needs and the root causes of migration.

# Tags

#MigrantCaravans #USPolicies #Immigration #Humanity #ForeignPolicy