# Bits

Beau says:

- Explains the importance of sound bites and context in a speech by Kennedy from 1963.
- Mentions the misinterpretation of Kennedy's speech due to focusing on sound bites rather than the full context.
- Urges the significance of understanding the entire speech to grasp the true meaning.
- Points out the danger of relying solely on sound bites in today's world driven by short attention spans and social media.
- Talks about the manipulation of information through visual sound bites like Twitter, lacking substance.
- Raises concerns about politicians using sound bites without defending or providing meaningful content.
- Reveals the U.S. and U.K.'s involvement in starting the Syrian Civil War as a pretext to oust Assad.
- Suggests that the real motivation behind the war might have been related to competing pipelines backed by different countries.
- Warns against falling for propaganda and encourages looking at the bigger picture to understand global events beyond sound bites.
- Comments on the rise of propaganda through dedicated news networks supporting specific political parties.

# Oneliner

Beau explains the dangers of relying on sound bites, urging a deeper understanding of context to unravel hidden truths in speeches and global events.

# Audience

Citizens, Information Seekers

# On-the-ground actions from transcript

- Analyze global events beyond sound bites (implied)
- Avoid relying solely on short, unsubstantiated information (implied)

# Quotes

- "Context, it's really  importan t."
- "If you want to understand what's going on in the world, you have to look beyond the sound bites."
- "Propaganda is coming into a golden age right now."

# Whats missing in summary

The full transcript delves into the dangers of misinformation through sound bites and urges a critical examination of broader contexts for a deeper understanding of global events. 

# Tags

#SoundBites #Context #Propaganda #GlobalEvents #CriticalThinking