# Bits

Beau says:

- Out for a drive to escape the house, chainsaw, and trees.
- Shocked to find Facebook and Twitter censoring independent news outlets opposed to government policies.
- Draws a clear distinction between these outlets and Alex Jones, who advocated violence and spread conspiracy theories.
- Knows many of the people affected personally, like Nick Burnaby from Anti-Media.
- Mentions how journalists like Carrie Wedler were banned in a mass purge.
- Points out that the outlets he works with were not targeted in this censorship.
- Urges not to compare these outlets to Alex Jones and stresses the importance of free discourse on social media platforms.
- Suggests looking for alternative social media networks like SteemIt and MeWe.
- Expresses concern over the chilling effect of this censorship on free speech.
- Warns that once censorship begins, it doesn't stop, and millions of subscribers were affected.
- Notes the high quality of reporting from these independent outlets compared to mainstream networks.

# Oneliner

Facebook and Twitter's censorship of independent news outlets threatens free discourse and public access to diverse perspectives online.

# Audience

Social media users

# On-the-ground actions from transcript

- Join alternative social media platforms like SteemIt and MeWe (suggested)
- Support independent news outlets by following and sharing their content online (exemplified)

# Quotes

- "This is kind of going to be the end. This is going to be the end of free
  discourse on Facebook."
- "You need to be looking for another social media network. You need to be
  getting ready to move."
- "Alex Jones was calling for innocent people to be killed. No, he can be
  censored. Simply disagreeing with the government policy is a whole other issue."

# Whats missing in summary

Importance of supporting independent journalism and seeking diverse perspectives outside mainstream platforms.

# Tags

#Censorship #SocialMedia #IndependentNews #FreeSpeech #AlternativePlatforms