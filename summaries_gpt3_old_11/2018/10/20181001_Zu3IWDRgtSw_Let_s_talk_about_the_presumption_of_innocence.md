# Bits

Beau says:

- Comments on the presumption of innocence in relation to Kavanaugh and the job interview.
- Mentions Kavanaugh's support for law enforcement's ability to stop individuals on the street.
- Talks about Kavanaugh's support for NSA collecting metadata without a warrant.
- Points out Kavanaugh's involvement in the indefinite detention of innocent people.
- Raises concerns about Kavanaugh's contradictory statements during confirmation hearings.
- Stresses the importance of the presumption of innocence in the American justice system.
- Expresses disbelief at Kavanaugh's sudden defense and supporters' lack of awareness about his rulings.
- Condemns party politics for undermining rights and overlooking individuals' beliefs in favor of power.
- Warns about the potential consequences of granting executive power to someone who undermines rights.
- Encourages critical thinking about supporting candidates based on party politics rather than principles.

# Oneliner

Beau comments on Kavanaugh's controversial stances, presumption of innocence, and the dangers of party politics in sacrificing rights for power.

# Audience

American citizens

# On-the-ground actions from transcript

- Question party politics and support candidates based on principles, not party allegiance (implied)
- Advocate for safeguarding constitutional rights and the presumption of innocence (implied)

# Quotes

- "You've bought into bumper sticker politics and you're trading away your country for a red hat."
- "It's almost like they aren't familiar with his rulings."
- "You can sit there and you can wave your flag and you can talk about making America great again."

# Whats missing in summary

The full transcript provides a detailed analysis of Kavanaugh's controversial stances, the importance of the presumption of innocence, and the detrimental effects of party politics on safeguarding rights.

# Tags

#Kavanaugh #PresumptionOfInnocence #PartyPolitics #Constitution #AmericanJustice