# Bits

Beau says:

- Explains the confusion around when it's appropriate to call the police.
- Points out that every law, no matter how insignificant, is backed up by the penalty of death.
- Gives an example of jaywalking to illustrate this point.
- Describes the escalation that can occur even for minor infractions.
- Warns about the potential consequences of involving the police unnecessarily.
- Advocates for only calling the police when death is an appropriate response.
- Stresses the importance of minding one's business when no harm is being done.
- Mentions the saying "strong fences make good neighbors" in relation to unnecessary police involvement.
- Talks about the patience required to deal with certain situations.
- Raises awareness of the different treatment people of color may face in interactions with law enforcement.

# Oneliner

Every law is backed by the possibility of death, so only involve the police if it's truly a matter of life and death.

# Audience

Community members

# On-the-ground actions from transcript

- Only involve the police if death is an appropriate response (implied)
- Mind your business when no harm is being done (implied)

# Quotes

- "Every law, no matter how insignificant, is backed up by penalty of death."
- "If death is an appropriate response to what's happening, that's it."
- "Don't call the law unless death is an appropriate response because it's a very real possibility every time you dial 9-11."

# Whats missing in summary

The full transcript provides additional context and examples to support the argument against unnecessary police involvement.