# Bits

Beau says:

- Received a message from a young lady who is alienated from her father due to his comments.
- Explains the impact of men's comments on relationships with daughters and sisters.
- Addresses the common arguments made about sexual assault victims coming forward.
- Challenges the perception and justifications used to doubt victims.
- Points out the damaging effects of not believing or supporting victims.
- Urges men to have honest and difficult talks with the women in their lives.
- Encourages apologizing instead of trying to justify harmful comments.
- Stresses the importance of not giving reasons for victims to not come forward.

# Oneliner

Men must confront harmful attitudes towards sexual assault victims to prevent alienation and enable support.

# Audience

Men

# On-the-ground actions from transcript

- Have honest and open talks with the women in your life about harmful comments and attitudes (suggested).
- Apologize sincerely for past comments and behaviors (implied).

# Quotes

- "You don't get to rape her."
- "You've given them all the reason in the world to never come forward."
- "Just apologize."

# Whats missing in summary

The emotional impact and urgency conveyed in Beau's message can be best understood by watching the full transcript.

# Tags

#SexualAssault #GenderRelations #SupportVictims #Apologize #BreakTheSilence