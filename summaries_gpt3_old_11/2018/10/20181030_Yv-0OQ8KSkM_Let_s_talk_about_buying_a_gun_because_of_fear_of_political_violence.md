# Bits

Beau says:

- A viewer asks for advice on purchasing a gun after a tragic event at a synagogue.
- Beau addresses sexism in gun advice, particularly for women.
- He explains the subtext behind the recommendation of a revolver for women.
- Beau delves into the misconceptions surrounding shotguns and their intended use.
- He advocates for understanding the purpose of owning a firearm before making a decision.
- Beau advises choosing a firearm based on one's environment and needs.
- He likens firearms to different types of shoes, each designed with a specific purpose in mind.
- Beau stresses the importance of training and understanding the responsibility of owning a firearm.
- He recommends seeking guidance from someone experienced in firearms for selection and training.
- Beau underscores the necessity of training to kill rather than merely shooting targets.

# Oneliner

A woman seeks advice on gun purchase post-synagogue incident as Beau dismantles sexist gun advice, advocates for responsible ownership, and stresses the importance of training to kill.

# Audience

Women seeking guidance on firearm selection and responsible ownership.

# On-the-ground actions from transcript

- Find someone experienced in firearms to help you select and train with a firearm (suggested).
- Train to use the firearm effectively and responsibly, understanding its purpose (implied).
- Practice shooting at ranges that allow realistic training scenarios (implied).

# Quotes

- "You're going to train to kill, not poke holes in paper."
- "Each one is designed with a specific purpose in mind. Your purpose, based on what you've said, is to kill a person."
- "You're going to train like you fight. You're going to train to kill, not poke holes in paper."
- "You want something easy to use, easy to maintain, and very reliable. That's what military firearms are."
- "Never protect human life. Only protect what matters."

# Whats missing in summary

The full transcript includes detailed advice on firearm selection, training, and tactical considerations for self-defense in potential violent situations.

# Tags

#FirearmSelection #Training #Responsibility #SelfDefense #GenderBias #ViolencePrevention