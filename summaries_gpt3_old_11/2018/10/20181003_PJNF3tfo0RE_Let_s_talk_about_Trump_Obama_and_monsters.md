# Bits

Beau says:

- President of the United States mocked sexual assault survivors on television, setting a shockingly low bar.
- The President's actions contribute to silencing sexual assault survivors who come forward.
- Beau questions the exoneration of the President based on the word "let" in a particular quote.
- He contrasts the leadership styles of Obama and Trump, criticizing Trump's entitlement and lack of responsibility.
- Beau challenges the notion that Trump embodies honor and integrity.
- He points out the hypocrisy in Trump discussing innocence and guilt, given his past actions.
- Beau criticizes the idea that Kavanaugh's character should overshadow allegations against him.
- He warns against dehumanizing perpetrators of sexual assault, stressing that they are people, not monsters.
- Beau draws a parallel between labeling Nazis as monsters and the dangerous dehumanization of perpetrators.
- He concludes by urging viewers to recognize that dangerous individuals can be ordinary people in their communities.

# Oneliner

President Trump's actions towards sexual assault survivors set a dangerous precedent, while Beau warns against dehumanizing perpetrators and draws chilling parallels to history.

# Audience

Advocates, Activists, Community Members

# On-the-ground actions from transcript

- Support survivors of sexual assault (suggested)
- Challenge dehumanization of perpetrators (implied)

# Quotes

- "The scariest Nazi wasn't a monster. He was your neighbor."
- "Scariest rapist or rape-apologist, well they're your neighbor too."

# Whats missing in summary

The full transcript provides a deeper exploration of the societal implications of dehumanizing perpetrators and the importance of supporting survivors.

# Tags

#SexualAssault #Dehumanization #Leadership #Responsibility #CommunitySafety