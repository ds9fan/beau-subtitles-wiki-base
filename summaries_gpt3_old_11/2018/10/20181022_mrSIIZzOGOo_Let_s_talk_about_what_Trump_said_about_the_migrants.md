# Bits

Beau says:
- President cutting off aid to Central American countries for not stopping people from coming to the US.
- President believes it's the head of state's duty to keep people in their country.
- Wall along the southern border can be used to keep Americans in.
- The slow creep of tyranny is starting to show its face.
- Beau finds the President's statement terrifying.
- Warning about the dangers of sacrificing freedom and liberty for a political figure.
- The wall is seen as a prison wall, not just a border wall.

# Oneliner

President's actions to cut off aid and control movement illustrate the potential dangers of sacrificing freedom for security, as the wall could also be used to keep Americans in.

# Audience

Americans

# On-the-ground actions from transcript

- Question political decisions and their implications (implied)
- Stay informed about political developments and their potential impact on freedom and liberty (implied)

# Quotes

- "That wall is a prison wall. It's not a border wall."
- "If you believe in the ideals of freedom and liberty, you can't if you've chosen to you've sold out your ideals and your principles and your country for a red hat and a cute slogan."

# Whats missing in summary

The full transcript provides more context on the potential consequences of prioritizing security over freedom and liberty. Viewing the full transcript can offer a deeper understanding of Beau's concerns about the President's actions.

# Tags

#Freedom #Tyranny #Politics #Security #BorderWall #CentralAmerica