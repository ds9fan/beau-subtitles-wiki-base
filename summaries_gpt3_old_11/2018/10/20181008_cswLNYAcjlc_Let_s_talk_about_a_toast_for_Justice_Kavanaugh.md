# Bits

Beau says:

- Beau sarcastically toasts Supreme Court Justice Kavanaugh's ascension to the throne, celebrating that his life wasn't ruined.
- He expresses relief that Kavanaugh won't have to go back to mining coal and praises his appointment to the Supreme Court for ensuring America's safety and freedom.
- Beau applauds Kavanaugh for not being tied down by concerns like jury trials, lawyers, or charges before indefinite detention, believing it will keep Americans free and safe.
- He humorously mentions the efficiency of the government in finding bad people and praises NSA for gathering information without warrants, in the name of freedom and safety.
- Beau commends local cops for having someone who supports stopping people for no reason, physically grabbing them, patting them down, asking for identification, all in the name of safety.
- He expresses faith in the government's good intentions and dismisses concerns about potential abuse of power.
- Beau concludes that sacrificing rights, selling out the country, and disregarding the Constitution were worth it to defeat feminists and leftists and get a "man's man" on the Supreme Court.

# Oneliner

Beau sarcastically celebrates Kavanaugh's Supreme Court appointment, praising the erosion of rights in exchange for perceived safety and freedom.

# Audience

Activists, advocates

# On-the-ground actions from transcript

- Organize protests against eroding rights and unchecked government power (suggested)
- Advocate for transparency and accountability in government actions (suggested)

# Quotes

- "It's almost like none of us even looked into this man's rulings."
- "We're gonna be free now."
- "We sure beat them me too, girls."

# Whats missing in summary

The full transcript provides a deeper understanding of the sarcastic criticism towards blind celebration of power at the cost of rights and freedoms.

# Tags

#SupremeCourt #Rights #Freedom #GovernmentPower #Activism