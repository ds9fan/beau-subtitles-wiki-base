# Bits

Beau says:

- Viewers question his identity and credibility due to a contradiction between what he says and what they expect.
- People want to know his backstory to determine how much trust to place in his words.
- Beau acknowledges having authority on certain topics but stresses that trust in authority figures is not a good thing.
- He warns against blind obedience to authority and the dangers of trusting sources without verification.
- Beau mentions how people often share false information on social media, prompting the need to verify sources.
- He advocates for checking facts, running information through filters, and questioning the morality and ethics behind actions.
- The Iraq war is cited as an example of the consequences of people not verifying information and blindly following authority.
- Reference is made to the Milgram experiments, where individuals showed a willingness to harm others under the influence of authority figures.
- Beau encourages viewers to think for themselves, not blindly follow authority, and trust their own judgment over external sources.
- He concludes by stressing the importance of critical thinking, independent analysis, and self-trust in evaluating information.

# Oneliner

Beau stresses the dangers of blind obedience to authority and advocates for critical thinking and self-trust in evaluating information.

# Audience

Viewers

# On-the-ground actions from transcript

- Verify sources before sharing information (suggested)
- Run information through filters (suggested)
- Question the morality and ethics behind actions (suggested)
- Think critically and independently (suggested)

# Quotes

- "Don't trust your sources, trust your facts, and trust yourself."
- "Ideas stand or fall on their own."
- "Trust yourself."
- "That obedience to authority, that submission to authority is deadly."
- "Y'all have a good night."

# Whats missing in summary

The full transcript provides a detailed exploration of the dangers of blind obedience to authority and the importance of critical thinking in evaluating information.

# Tags

#Authority #CriticalThinking #FactChecking #Trust #Ethics