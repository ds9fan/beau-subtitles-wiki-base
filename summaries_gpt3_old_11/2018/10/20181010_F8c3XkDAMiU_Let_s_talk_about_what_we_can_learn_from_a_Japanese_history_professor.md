# Bits

Beau says:

- Introduces a tale about a history professor in Japan before World War II, who predicts the war's outcome and moves his family to a remote island.
- The professor chooses to remain silent and not influence policy or people, despite being influential himself.
- Beau explains how remaining silent in the face of tyranny or oppression is not neutral but siding with the oppressor.
- Talks about the interconnected world today where remaining silent means siding with those in power.
- Beau questions the societal norm of avoiding topics like money, politics, and religion, suggesting that discussing these is necessary to combat tyranny.
- Urges for open, honest dialogues rather than debates, to prevent the slow march of tyranny.
- Stresses the importance of speaking out against tyranny as it threatens freedom everywhere.

# Oneliner

A history professor's silent choice in pre-WWII Japan prompts Beau to advocate for open dialogues against tyranny to safeguard freedom.

# Audience

Advocates for open dialogues.

# On-the-ground actions from transcript

- Start having open dialogues about sensitive topics within your community (suggested).
- Encourage honest and open communication rather than debates (suggested).
- Speak out against tyranny and oppression in your circles (implied).

# Quotes

- "When you remain silent in the face of tyranny or oppression, you have chosen a side."
- "Tyranny anywhere is a threat to freedom everywhere."

# Whats missing in summary

The full transcript delves into the historical narrative of a Japanese history professor, connecting it to the importance of speaking out against oppression and tyranny in modern society.

# Tags

#History #Tyranny #Freedom #OpenDialogues #Oppression