# Bits

Beau says:

- Son asked about Gillette ad, proud of his son's understanding.
- Message of ad: Don't sexually assault, belittle, bully; violence not always answer.
- Ad not attack on masculinity; praised by Beau as promoting real masculinity.
- Beau questions why some men didn't see themselves as the positive figure in the ad.
- Feminists upset at corporate feminism for sales; Beau sees ad as promoting masculinity, not feminism.
- Beau defends razor company's insertion into social issues, citing lack of rites of passage in the U.S.
- Mentions brutal rites of passage in history, like the agoghi, aimed at creating honorable warriors.
- Talks about Amazonian boys enduring painful rites of passage to become men.
- Contrasts American men's reaction to discomfort with Amazonian boys' endurance.
- Beau praises his son for not identifying with negative aspects in the ad.

# Oneliner

Beau praises Gillette ad for promoting real masculinity and questions reactions to its message.

# Audience

Men, feminists, corporate entities

# On-the-ground actions from transcript

- Support positive messages in media (exemplified)
- Advocate for healthy masculinity (exemplified)
- Encourage understanding and empathy in discussing social issues (exemplified)

# Quotes

- "If you felt attacked by that ad, it says more about you than it does Gillette."
- "My 12-year-old boy was more of a man than the people that are upset with this."
- "Any worthy goal is going to require you to suffer."

# Whats missing in summary

Full context and emotional depth

# Tags

#Masculinity #GilletteAd #ToxicMasculinity #RitesofPassage #MediaRepresentation