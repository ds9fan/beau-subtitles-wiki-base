# Bits

Beau says:

- Addresses toxic masculinity, its origins, and its impact on society.
- Explains the connection between toxic masculinity and men's movements.
- Talks about the mythopoetic men's movement of the '80s and '90s.
- Mentions how the term "toxic masculinity" originated from the first MRAs.
- Describes the fear of men becoming hyper-masculine or feminized.
- Comments on the glorification of violence and its impact on masculinity.
- Talks about the loss of ideals associated with masculinity.
- Points out the expectations of masculinity in society.
- Addresses the responsibility of men to act and the lack of ingrained ideals.
- Talks about the difference between assertiveness and aggression in men and women.

# Oneliner

Beau explains the origins and impacts of toxic masculinity, delving into men's movements and societal expectations around masculinity.

# Audience

Men's movement participants

# On-the-ground actions from transcript

- Challenge toxic masculinity by promoting healthy masculinity (suggested)
- Support organizations that address toxic masculinity and its effects (suggested)
- Advocate for gender equality and understanding between genders (suggested)

# Quotes

- "Masculinity is not a team sport."
- "Not saying hello can get a woman killed."

# Whats missing in summary

Full context and depth on toxic masculinity and its societal implications.

# Tags

#ToxicMasculinity #Men #GenderEquality #SocialExpectations #MythopoeticMovement