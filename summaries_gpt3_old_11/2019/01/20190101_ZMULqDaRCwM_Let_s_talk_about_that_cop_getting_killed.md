# Bits

Beau says:

- Mentions the names of cops killed in the last 60 days by gunfire, some heroes like Sergeant Helles and Corporal Singh.
- Talks about how headlines focus on an illegal immigrant killing a police officer rather than the officer's bravery in confronting a gang member.
- Compares the coverage of Corporal Singh's death to that of Molly Tibbets, pointing out the family's response of taking in an immigrant.
- Challenges the notion of holding entire demographics accountable for the actions of individuals, referencing the basis of "build the wall."
- Addresses the hypocrisy of not wanting to build a wall around cops despite unarmed people being killed.
- Calls out the selective application of laws and lack of understanding of immigration laws.
- Criticizes the use of biased studies from organizations like FAIR, CIS, and Numbers USA founded by John Tanton.
- Exposes John Tanton's belief in maintaining a white European majority and the bias in studies conducted by organizations associated with him.
- Asserts that the issue is about race and how people's biases are evident in their reactions to incidents involving brown individuals.
- Draws parallels between incidents like Benghazi and Western Africa where similar situations yielded different levels of public outrage based on political narratives.

# Oneliner

Mentions cops killed by gunfire, challenges biased headlines, and addresses selective accountability, immigration laws, and racial biases.

# Audience

Community members

# On-the-ground actions from transcript

- Challenge biased narratives in media (implied)
- Advocate for fair and just treatment of all individuals regardless of background (implied)

# Quotes

- "Stop standing on the graves, the corpses, the flag-covered coffins of people far more honorable than you to make a political point they might not agree with."
- "You're not outraged about a cop getting killed, you didn't know the names of the other ones."

# Whats missing in summary

The full transcript provides a deeper insight into the biases in media coverage, selective accountability, immigration laws, racial biases, and the impact of political narratives on public perception.

# Tags

#PoliceBrutality #MediaBias #SelectiveAccountability #RacialBias #ImmigrationLaws