# Bits

Beau says:

- Beau introduces the idea of bringing original concepts to the table in his videos.
- He chooses not to fact-check Trump's speech but focuses on a subtle aspect where Trump mentioned his oath to protect the country.
- Beau explains the significance of swearing an oath to the Constitution rather than the country.
- He stresses that the Constitution's ideals are more critical than any individual in power.
- Beau praises the revolutionary nature of the US Constitution, especially the concept of self-governance.
- He acknowledges the flaws of the Founding Fathers, including their stance on issues like slavery.
- Beau envisions a world where true freedom means the end of government control.
- He references Thomas Paine's view that government is a necessary evil and advocates for a society without government intervention.
- Beau urges people to use their voices to speak out against injustices and corruption in government.
- He concludes by suggesting that future generations may have to fight with bullets and lamp posts for their rights.

# Oneliner

Beau stresses the importance of upholding the ideals of the US Constitution over blind nationalism, advocating for a society where true freedom means the end of government control.

# Audience

Activists, Advocates, Citizens

# On-the-ground actions from transcript

- Speak out against injustices in your community (suggested)
- Advocate for transparency and accountability in government (suggested)
- Use social media platforms to raise awareness about critical issues (suggested)
- Prepare to defend democratic values through peaceful means (suggested)

# Quotes

- "When fascism comes to the United States, it'll be wrapped in a flag and carried across."
- "Today, we the people can ensure tranquility, provide for the common defense, and secure the blessings of liberty everywhere."
- "Getting rid of government is not destroying the Constitution. It's carrying it to its logical conclusion."
- "If ever the time shall come when vain and aspiring men possess the highest seats in government, our country will stand in need of its experienced patriots."
- "Today is the time you've got to start using your voice, often, to speak out against the injustices that you see."

# Whats missing in summary

The full transcript provides a detailed exploration of the importance of upholding the ideals of the US Constitution, advocating for a society where true freedom transcends government control.

# Tags

#USConstitution #Freedom #Activism #Government #Democracy