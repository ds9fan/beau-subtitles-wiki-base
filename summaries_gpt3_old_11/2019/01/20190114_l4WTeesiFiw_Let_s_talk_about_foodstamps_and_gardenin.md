# Bits

Beau says:

- Beau shares a positive experience at a garden section where he learned that SNAP benefits can be used to purchase items like fruit trees, berry bushes, seeds, and more.
- He addresses misconceptions about food stamps, mentioning that over 10% of Americans use them and the average household receives around $250 a month in benefits.
- Beau counters typical complaints about food stamp usage, explaining that the amount of food purchased is for a month and the choices are influenced by the need to stretch the budget.
- He debunks stereotypes about food stamp recipients, pointing out that many working families rely on this assistance and it's not about laziness.
- Beau criticizes the blame placed on those using food stamps, shifting the focus to systemic issues and the broken social support structure.
- He warns against cutting food stamp programs abruptly, as hunger can lead to unrest and advocates for addressing other government wastes first.
- Beau introduces the idea that SNAP benefits can be used to buy seeds, fruit trees, and bushes, sparking a solution for people living in cities who lack land for traditional gardening.
- He suggests creative urban gardening solutions like vertical gardening with pallets, using shoe organizers as planters, and even guerrilla gardening on unused land.
- Beau proposes community gardening as a way to involve others and build a sense of community which he feels is lacking in society.
- He encourages utilizing others' properties for gardening, mentioning the mutual benefits of increasing property value and providing stability for food stamp recipients.

# Oneliner

SNAP benefits can be used for growing food, debunking myths around food stamps and offering urban gardening solutions to build community.

# Audience

Community members

# On-the-ground actions from transcript

- Set up a community garden to involve and connect with others (exemplified)
- Utilize unused land for guerrilla gardening with permission (exemplified)
- Grow food on someone else's property with their consent (exemplified)

# Quotes

- "If you are doing well in this system, you might just want to eat this one."
- "It's not the people who actually broke the system. It's not the people making the decisions up in DC."
- "Every little bit helps in that regard."
- "It increases your food security, hopefully cuts down your dependence, and maybe can contribute to getting you out."
- "I throw seeds on the ground and spit on them and something grows."

# Whats missing in summary

The full transcript provides detailed insights into using SNAP benefits for gardening, dispelling myths about food stamps, and offering practical urban gardening solutions to enhance community resilience.

# Tags

#SNAP #FoodStamps #Gardening #CommunityBuilding #SystemicIssues