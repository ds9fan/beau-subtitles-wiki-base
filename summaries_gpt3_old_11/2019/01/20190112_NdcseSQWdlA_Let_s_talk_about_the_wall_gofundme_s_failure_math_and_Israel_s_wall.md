# Bits

Beau says:

- Explains the situation with the wall GoFundMe being refunded.
- Talks about the intention behind redirecting donations to a new non-profit.
- Expresses skepticism about non-profits set up with intentions to make the government do what they want.
- Breaks down the math behind the donations raised for the wall.
- Points out the minimal percentage of the total cost raised through donations.
- Mentions the estimates for the total cost of building the wall, ranging from 20 to 70 billion dollars.
- Stresses that the amount raised does not prove that Americans want the wall.
- Argues that Americans oppose the wall morally, ethically, legally, and economically.
- Comments on the potential future consequences of having a wall in the country.
- Compares the proposed US wall with Israel's wall and its purpose.
- Questions the effectiveness of Israel's wall in stopping terrorism.
- Raises concerns about the constitutional implications of implementing a similar wall in the US.

# Oneliner

The wall GoFundMe refunds money, revealing Americans' opposition to the wall both morally and economically, as Beau breaks down the math and questions the effectiveness of walls like Israel's.

# Audience

Americans

# On-the-ground actions from transcript

- Support organizations working towards humane immigration policies (suggested)
- Advocate for ethical and effective border security measures (suggested)

# Quotes

- "Americans don't want the wall. They know that it's morally wrong."
- "It's not what we want, most people, anyway."
- "And it wasn't designed to stop immigration. It was designed to stop terrorism."
- "The wall didn't work there either, guys."
- "We'd have to give all of that up and it still wouldn't work."

# Whats missing in summary

Full understanding of the implications of funding and building a border wall in the US.

# Tags

#BorderWall #GoFundMe #Immigration #Opposition #Ethics