# Bits

Beau says:

- Explains the concept of Stay Behind Organizations created by NATO during the Cold War to resist a Soviet invasion by setting up networks of people in various locations.
- Mentions that these networks included military, intelligence personnel, and civilians with the goal of running resistance operations and revolution from inside the country.
- Acknowledges the effectiveness of these networks despite some extreme cases like Gladio, which went off the rails.
- Encourages individuals to start community-based networks by assessing what skills or resources they can offer within their immediate circle.
- Gives examples of how people of all ages and backgrounds can contribute to such networks, even mentioning the value of life experience and energy.
- Suggests using social media to connect with others and recruit members for the network.
- Talks about the importance of structure within the organization, mentioning different approaches like a barter system or a more social commitment-based system.
- Shares the idea of engaging in community service to demonstrate the group's active involvement and attract like-minded individuals.

# Oneliner

Beau explains how to create community-based networks inspired by NATO's Stay Behind Organizations, stressing the importance of individual contributions and active participation.

# Audience

Community organizers, activists

# On-the-ground actions from transcript

- Connect with individuals in your immediate circle to start building a community-based network (suggested).
- Use social media to recruit members and expand the network (suggested).
- Participate in community service activities to demonstrate active involvement and attract like-minded individuals (exemplified).

# Quotes

- "Everybody has something to offer a network like this."
- "Everybody. So how do you recruit people to join your network?"
- "Those are the basics of how to set one up."

# Whats missing in summary

The full transcript provides detailed insights into the concept of community-based networks, offering practical advice on starting, structuring, and growing such networks with examples and encouragement.

# Tags

#CommunityNetworks #Activism #BuildingCommunity #NATO #ResistanceOrganizations