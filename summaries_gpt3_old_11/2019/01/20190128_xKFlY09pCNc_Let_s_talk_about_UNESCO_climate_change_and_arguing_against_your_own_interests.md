# Bits

Beau says:

- Expresses shock and disappointment at the United States' withdrawal from UNESCO, an organization that protects historical and cultural sites.
- Shares various reasons given by Trump supporters for the withdrawal, including claims of taxation as theft, anti-Semitism, and focus on climate change.
- Explains the significance of UNESCO's concern about climate change, particularly related to historical sites near waterways and rising sea levels.
- Addresses the debate on climate change, pointing out the lack of credible studies denying it and the influence of oil-funded think tanks.
- Advocates for sustainable practices like stopping deforestation, transitioning to renewable energy, and reducing pollution, regardless of beliefs about climate change.
- Criticizes the spread of propaganda that leads individuals to argue against their own interests and ignore environmental sustainability.
- Urges people to question why they oppose efforts to create a cleaner world and improve their own livelihoods.
- Links political resistance to environmental action to campaign contributors protecting their business interests at the expense of the planet.
- Condemns the withdrawal from UNESCO as a consequence of misguided arguments and propaganda clouding public understanding.

# Oneliner

The United States' exit from UNESCO exposes the impact of misinformation and political interests on environmental conservation and historical preservation.

# Audience

Climate advocates, environmentalists, history enthusiasts

# On-the-ground actions from transcript

- Advocate for sustainable practices in your community (implied)
- Support organizations working to protect historical and cultural sites (implied)

# Quotes

- "The propaganda is that thick."
- "You're arguing against your own interests."
- "Think about why you're dead set against making the world a cleaner and better place."

# Whats missing in summary

In-depth exploration of the detrimental effects of misinformation and political agendas on environmental conservation efforts and historical preservation. 

# Tags

#ClimateChange #HistoricalPreservation #Sustainability #Propaganda #EnvironmentalAdvocacy