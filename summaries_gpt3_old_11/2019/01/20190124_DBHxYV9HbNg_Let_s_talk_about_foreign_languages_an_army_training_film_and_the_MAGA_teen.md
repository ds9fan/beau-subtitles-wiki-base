# Bits

Beau says:

- Stresses the importance of speaking the same "linguistic" or "ideological" languages as those you're communicating with to ensure effective communication.
- Recounts an old army training film that showcases the importance of understanding your audience and speaking their "linguistic" or "ideological" languages.
- Describes a scenario where a new lieutenant attempts to explain the benefits of a new grenade launcher to a seasoned first sergeant, leading to a failed communication.
- Illustrates a successful communication scenario where the lieutenant engages the first sergeant by asking for his expertise on how to use the weapon based on his experience in Vietnam.
- Shows how using relatable messengers who speak the same "linguistic" or "ideological" languages as the audience can enhance communication effectiveness.
- Mentions the importance of presentation over information in shaping perceptions.
- Cautions against hastily jumping to conclusions based on initial presentations of events, citing the example of the incident involving the kid in the Make America Great hat.
- Points out the impact of messaging and perception in shaping reactions and responses to different situations.
- Urges vigilance against succumbing to spreading fake narratives and misinformation, especially in combating such phenomena.
- Encourages awareness of how messages are presented and perceived to navigate through the complexities of communication effectively.

# Oneliner

Understanding and speaking the same "linguistic" or "ideological" languages as your audience is key to effective communication, as Beau illustrates through an old army training film and reflections on present-day messaging and perception.

# Audience

Communicators, Activists, Academics

# On-the-ground actions from transcript

- Reach out to communities in their native languages for effective communication (exemplified).
- Use relatable messengers who speak the same "linguistic" or "ideological" languages as the audience to enhance communication (exemplified).
- Be cautious about spreading misinformation and fake narratives, ensuring messages are presented accurately and responsibly (exemplified).

# Quotes

- "Sometimes it may not be that you're constructing a poor argument. It may be that you're just not the person that can deliver it to the audience."
- "The way something is presented is often times more valuable than the information presented in it."
- "Messaging is vital and the way things are perceived can be more critical than the facts."

# Whats missing in summary

The full transcript provides a nuanced exploration of effective communication through the lens of understanding linguistic and ideological languages, using real-life examples to underscore the importance of relatability and mindful messaging.

# Tags

#Communication #EffectiveMessaging #UnderstandingAudience #Perception #CombattingMisinformation