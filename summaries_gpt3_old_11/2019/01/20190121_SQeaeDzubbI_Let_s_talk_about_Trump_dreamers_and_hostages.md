# Bits

Beau says:

- President Trump's negotiation tactics involve offering protection for Dreamers in exchange for $5.7 billion for his wall.
- Dreamers refers to DACA recipients who were brought to the US as kids and are more American in heart and mind.
- The president is willing to offer Dreamers three years of protection because he knows they are not a threat.
- Beau questions why there isn't a path to citizenship for Dreamers rather than holding them hostage for funding.
- Temporary Protected Status (TPS) recipients are also offered protection in exchange for funding.
- TPS recipients are legal, following the law, and have been in the US for years.
- President Trump cut six countries from the TPS program, threatening those individuals with deportation.
- Beau criticizes the president's actions as extortion and thuggery, using federal law enforcement as soldiers.
- Congress and the Senate are not calling out the president for his actions, which Beau finds embarrassing.
- Beau advocates for establishing a path to citizenship for Dreamers and TPS recipients, rather than punishing them.

# Oneliner

President Trump uses protection offers for Dreamers and TPS recipients as bargaining chips for wall funding, drawing criticism for his thuggery and extortion tactics.

# Audience

Legislators, policymakers, activists

# On-the-ground actions from transcript

- Advocate for establishing a path to citizenship for Dreamers and TPS recipients (suggested)
- Call out the president's actions as extortion and thuggery (implied)
- Support legislation that prioritizes protection for immigrants (implied)

# Quotes

- "The President of the United States is acting like a criminal, and he's using the guns of federal law enforcement as his soldiers in the street."
- "They're kids. They came here as kids. I don't think they should be punished for the sins of their father."

# Whats missing in summary

The full transcript provides a detailed analysis of President Trump's negotiation tactics regarding Dreamers and TPS recipients, urging for a path to citizenship and criticizing the president's actions as extortion and thuggery.

# Tags

#Immigration #Dreamers #TPS #Extortion #Thuggery #PathToCitizenship