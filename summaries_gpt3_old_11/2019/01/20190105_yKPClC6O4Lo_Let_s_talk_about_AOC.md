# Bits

Beau says:

- Beau introduces himself and mentions it's part three.
- Beau asks his dad about The Breakfast Club movie.
- Mention of watching a video online of a congressperson dancing.
- Beau talks about how people express themselves when they were younger compared to when they're older.
- Beau hints at wanting to see something about the First Lady when she was younger.
- Beau congratulates the Republican Party for changing the perception of the First Lady through a video.
- Beau jokingly mentions the reason behind people being angry about the First Lady dancing.
- Beau wraps up with a light-hearted comment and bids goodnight.

# Oneliner

Beau asks about a movie, comments on people's expressions, and jokes about the First Lady dancing, ending with a light-hearted farewell.

# Audience

Social media users

# On-the-ground actions from transcript

- Share positive content online (suggested)
- Spread positivity through humor (suggested)

# Quotes

- "With one video, she has been turned into the most adorable woman in the country."
- "Y'all have a nice night."

# Whats missing in summary

The tone and humor of Beau's delivery can be fully appreciated by watching the full video.

# Tags

#Humor #Family #Politics #SocialMedia #Expression