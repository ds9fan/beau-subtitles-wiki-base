# Bits

Beau says:

- Border apprehensions along the southern border are at a 40-year low.
- The Constitution of the United States outlines a process for funding projects.
- Trump ran on promises of prosperity, military supremacy, and international respect.
- The international community initially viewed Trump as a laughingstock.
- Trump's unpredictable nature garnered attention from other nations.
- Trump realized he lacked the power to fulfill his promises.
- Drawing parallels to Hitler, Beau identifies specific elements of fascism in Trump.
- Fascism includes blending government and economic power, disdain for human rights, and control of the media.
- Beau points out Trump's obsession with nationalism and military supremacy.
- He criticizes Trump's actions as betraying the Constitution and Congress.
- Congress serves as a vital checkpoint against tyranny and dictatorship.
- Beau predicts that a national emergency declaration might proceed due to lack of Democratic compromise.
- He urges individuals to choose between supporting the Constitution or Trump's actions.
- Beau warns against betraying the foundational ideas of the United States.
- He references historical figures like August Landmesser and Sophie Scholl as examples of standing against tyranny.

# Oneliner

Border apprehensions are low, but Beau warns against Trump's potential fascist tendencies and urges a choice between supporting the Constitution or betraying it.

# Audience

American citizens

# On-the-ground actions from transcript

- Support the Constitution by opposing actions that circumvent Congress (implied)
- Choose to stand against tyranny and betrayal of constitutional principles (implied)

# Quotes

- "Either you support the constitution of the United States or you betray it."
- "You're a patriot or a traitor."
- "Circumventing Congress, it's a betrayal of the very ideas this country was founded on."

# What's missing in summary

The full transcript delves deeper into the comparison between Trump and fascism, urging individuals to make a decisive choice between loyalty to the Constitution and enabling potential tyranny.

# Tags

#BorderApprehensions #Constitution #Fascism #Trump #NationalEmergency #Tyranny #SupportConstitution #Betrayal #AmericanCitizens