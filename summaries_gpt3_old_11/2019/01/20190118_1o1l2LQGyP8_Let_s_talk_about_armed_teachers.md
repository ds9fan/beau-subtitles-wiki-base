# Bits

Beau says:

- Beau addresses the topic of armed teachers in schools, describing it as a Band-Aid solution at best.
- He points out the lack of practicality in arming teachers due to the extensive training required.
- Beau explains the intense and high-pressure situations teachers could face during a school shooting.
- He stresses the importance of accuracy and the inability to miss a shot in such scenarios.
- Beau urges teachers to change their mindset from educators to warriors in order to protect students.
- He advises on creating mobile cover using filing cabinets with steel plates and frangible ammo.
- Beau mentions the need for teachers to become experts in combat due to inadequate policies from school boards.
- He warns about the challenges of engaging in close quarters combat in school hallways.
- Beau suggests that the burden of making any potential solution work falls on the individual teacher.
- He concludes by encouraging exploring alternative solutions rather than relying on arming teachers.

# Oneliner

Arming teachers as a solution to school shootings is impractical and requires extensive training and mindset shifts, urging individuals to seek alternative solutions.

# Audience

Teachers, educators

# On-the-ground actions from transcript

- Build mobile cover using filing cabinets with steel plates and frangible ammo (suggested)
- Become an expert in combat and close quarters combat (implied)
- Seek alternative solutions beyond arming teachers (implied)

# Quotes

- "When you're walking through the hallway, you're not a teacher anymore, you're a warrior."
- "You have to change your entire mindset."
- "If it works, it's because you made it work."
- "Liability and effectiveness don't go hand in hand."
- "This isn't going to solve the problem."

# Whats missing in summary

Detailed examples of the challenges and risks teachers face in potential school shooting scenarios.

# Tags

#SchoolSafety #ArmedTeachers #CombatTraining #AlternativeSolutions #RiskAssessment