# Bits

Beau says:

- Internet backlash towards a man who mocked kids at a Lego building competition.
- Man live-tweeted mocking comments about kids building Lego creations.
- Beau mentions his own son's interest in building, like creating a temporary castle.
- Beau admires the creativity and skill involved in building something from scratch.
- Beau contrasts the man's mockery with the pride he should have for his son building a working robot.
- Beau criticizes the man for trying to appear manly by making fun of kids' creative endeavors.
- He suggests the man should apologize for his behavior and acknowledge the value of the kids' skills.
- Beau encourages the man to recognize and appreciate the creative aspects of masculinity.
- He points out the harm in mocking children for their interests and skills.
- Beau concludes by urging the man to apologize to his son and the other kids affected by his comments.

# Oneliner

Internet backlash as Beau criticizes a man for mocking kids' creativity at a Lego competition, urging him to apologize and appreciate their skills.

# Audience

Parents, Adults

# On-the-ground actions from transcript

- Apologize to the kids for mocking their creativity (suggested)
- Acknowledge and appreciate the valuable skills kids develop through creative endeavors (suggested)

# Quotes

- "Your kid's got a pretty valuable skill there already developed, and you're mocking it."
- "You might want to get on Twitter and apologize to your son and all the other kids that got caught in a crossfire because you wanted to look tough and manly picking on kids."

# Whats missing in summary

The emotional impact of mocking children's creative efforts and the importance of supporting and encouraging their skills.

# Tags

#Creativity #Parenting #Apology #Support #Kids #Community