# Bits

Beau says:

- Introducing part one of how to change the world, similar to the gun video.
- Inspired by a genuine Facebook comment wanting to share information.
- Drawing parallels between society today and George Orwell's 1984.
- Explaining the need to become conscious before rebelling against societal norms.
- Sharing personal experiences of helping others post-Hurricane Michael.
- Stressing the importance of building a network for independence.
- Encouraging single parents to contribute to networks despite challenges.
- Emphasizing the value of self-evaluation in determining what one can offer.
- Describing networks as force multipliers that evolve into communities.
- Advocating for community building over top-down leadership.
- Acknowledging the uniqueness of each situation in network building.
- Quoting 1984 on hope lying with the proles.

# Oneliner

Beau introduces how to change the world by building networks, stressing community empowerment over top-down leadership, inspired by a genuine Facebook comment.

# Audience

Community members

# On-the-ground actions from transcript

- Build a network with diverse skills (suggested)
- Contribute to networks despite challenges (implied)
- Offer your skills and strengths (suggested)
- Start building a community for resilience (suggested)

# Quotes

- "If there is any hope, it must lie in the proles."
- "You're changing the world by building your community."

# Whats missing in summary

The full transcript delves into the importance of consciousness, rebellion, and community building for societal change, offering practical advice and personal anecdotes to inspire action.

# Tags

#CommunityBuilding #Networks #Empowerment #SocietalChange #BuildingResilience