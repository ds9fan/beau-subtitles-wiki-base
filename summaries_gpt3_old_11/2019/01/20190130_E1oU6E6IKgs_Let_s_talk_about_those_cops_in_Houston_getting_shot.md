# Bits

Beau says:

- Four cops were shot during a box drug raid in Houston.
- The reason for the shooting may be due to a lack of proper threat assessment or ignoring it.
- Instead of discussing the shooting, Beau focuses on the statement made by the police union president, Joe Jamali.
- Joe Jamali threatened those who criticize police officers, stating they will be held accountable.
- Beau mentions a group in Texas willing to train 15 officers for free if Joe Jamali resigns.
- Beau argues that freedom of speech and the ability to criticize the government are vital for a community's safety.
- He questions whether Joe Jamali cares enough about his officers to resign for their benefit.
- Beau concludes by leaving the decision to Joe Jamali and suggests dinner is ready.

# Oneliner

Four cops shot in Houston, Beau challenges police union president threatening critics, offers free training if he resigns, stressing importance of freedom of speech.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Contact the group in Texas for free officer training if Joe Jamali resigns (suggested)
- Hold accountable those who threaten freedom of speech and criticize police actions (implied)

# Quotes

- "If you don't want to be cast as the enemy, don't be the enemy of freedom."
- "Freedom of speech and the ability to criticize your government or stir the pot against your government is pretty
  - "You representing an armed branch of government do not get to tell people what they can and cannot talk about."

# Whats missing in summary

Full context of Beau's challenge and the importance of freedom of speech.

# Tags

#Houston #PoliceShooting #FreedomOfSpeech #Training #CommunitySafety