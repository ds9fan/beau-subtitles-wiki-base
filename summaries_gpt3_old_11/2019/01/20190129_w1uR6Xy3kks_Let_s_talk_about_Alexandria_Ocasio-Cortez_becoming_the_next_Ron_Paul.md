# Bits

Beau says:

- Made a controversial comparison between Alexandria Ocasio-Cortez and Ron Paul, causing backlash from libertarian friends.
- Discovered that a significant portion of his audience is overseas, viewing the U.S. situation akin to the fall of the Roman Empire.
- Ron Paul, a renowned figure in the libertarian community, was known for his strict adherence to the Constitution and opposition to government overreach.
- Contrasts Ron Paul’s views with AOC's beliefs on corporate greed and government role in oppressing the little guy.
- AOC attracts millennials who have faced economic challenges and view corporate greed as a significant issue.
- Beau points out that economic indicators should not be solely based on the success of the wealthy but also on the struggles of federal employees needing food banks during a paycheck lapse.
- Illustrates how Walmart could significantly increase employee pay by redistributing a portion of its profits.
- Suggests that the real divide may not be corporate vs. government but rather establishment vs. ordinary citizens.
- Raises the question of whether AOC could evolve similarly to Ron Paul and become a significant political figure.
- Speculates on whether the Democratic Party establishment will allow figures like AOC to rise within their ranks.

# Oneliner

Beau compares AOC to Ron Paul, challenges views on establishment, corporate greed, and government overreach, sparking reflections on evolving political landscapes.

# Audience

Political analysts, activists

# On-the-ground actions from transcript

- Challenge establishment narratives (implied)

# Quotes

- "What if it's not corporate establishment versus government establishment? What if it's just establishment versus you?"
- "With a little bit of refinement and a little bit of time, she'll grow just like everybody does."
- "I think that we're looking at a powerhouse in the making."

# Whats missing in summary

Exploration of the potential impact of emerging political figures like AOC and the resistance they may face within established political structures.

# Tags

#PoliticalAnalysis #Establishment #CorporateGreed #GovernmentOverreach #AOC #RonPaul