```

# Bits

- Beau expresses his dismay over the President of the United States mocking sexual assault survivors on television.
- He criticizes the efforts to silence sexual assault survivors led by the President.
- Beau questions the lack of honor and integrity in President Trump and compares him unfavorably to President Obama.
- He points out the danger in dehumanizing perpetrators of sexual assault and draws parallels to historical events.
- Beau ends by cautioning against being bystanders in the face of evil.

# Audience
Those concerned about sexual assault survivors and the impact of political leadership on societal values.

# On-the-ground actions from transcript
- Support organizations aiding sexual assault survivors (exemplified)
- Speak out against attempts to silence survivors (exemplified)
- Challenge dehumanization of perpetrators (generated)

# Oneliner
President’s actions set dangerous precedents; speak up, support survivors, and humanize all. Evil thrives when good people do nothing.

# Quotes
- "There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim, and it's being led by the President of the United States."
- "The scariest Nazi wasn't a monster. He was your neighbor."
- "Speak up, support survivors, and humanize all."

# What's missing in summary
The emotional impact and detailed examples of Beau's comparisons to historical events.

# Tags
#SexualAssault #Leadership #Dehumanization #SupportSurvivors #SocialResponsibility
```