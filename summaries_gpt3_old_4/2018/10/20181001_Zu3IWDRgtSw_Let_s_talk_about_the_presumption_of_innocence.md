```

# Bits

- Beau expresses surprise at the focus on presumption of innocence in relation to Kavanaugh.
- Beau points out Kavanaugh's support for law enforcement overreach.
- Beau questions the sincerity of those defending Kavanaugh based on presumption of innocence.
- Beau criticizes the impact of party politics on undermining rights.
- Beau warns of the consequences of supporting Kavanaugh solely based on party affiliation.

# Audience

Activists, voters, legal advocates

# On-the-ground actions from transcript

- Challenge party politics' influence on rights (implied)
- Stay informed on judicial nominees and their rulings (exemplified)
- Advocate for upholding constitutional rights in court appointments (suggested)

# Oneliner

Beau questions Kavanaugh defenders and warns against sacrificing rights for party politics.

# Quotes

- "You've bought into bumper sticker politics and you're trading away your country for a red hat."
- "Don't expect anybody who knows anything about this man's rulings or knows anything about the Constitution, knows anything about the Supreme Court to believe a word you say."

# Whats missing in summary

Deeper insights on the impact of prioritizing party politics over constitutional rights.

# Tags

#PresumptionOfInnocence #Kavanaugh #PartyPolitics #Constitution #JudicialNominees #RightsUndermined
```