```

# Bits

- Beau talks about the importance of understanding when it's appropriate to call the police, stressing that every law is backed up by the threat of death.
- He uses examples like jaywalking to illustrate how minor offenses can escalate to violence if police are involved.
- Beau mentions that calling the police on someone over trivial matters like Barbecue Becky or Permit Patty can put their lives in danger.
- He advises that the only time to involve the police is when death is a proportionate response to the situation.
- Beau advocates for minding your business unless someone is being harmed, stating that legality does not equate to immorality.
- He reflects on a situation where a white person remained calm while dealing with unreasonable behavior, acknowledging that the outcome could have been different for a black person.
- Beau warns about the potential escalation and danger faced by black individuals when confronted by law enforcement.
- He concludes by suggesting that one should only call the police when death is an appropriate outcome, as every 911 call carries the risk of serious consequences.

# Audience

Community members, bystanders, concerned citizens

# On-the-ground actions from transcript

- Refrain from involving law enforcement in non-violent situations unless there is a genuine threat of death (exemplified)
- Advocate for de-escalation techniques and conflict resolution strategies in community settings (suggested)

# Oneliner

Beau stresses the gravity of involving police, cautioning against unnecessary calls and advocating for intervention only when life is at risk.

# Quotes

- "Every law, no matter how insignificant, is backed up by penalty of death."
- "Nobody's being harmed by these actions. Nobody's being hurt."
- "If nobody's being harmed, it's not really a crime."
- "Don't call the law unless death is an appropriate response because it's a very real possibility every time you dial 9-11."
- "Strong fences make good neighbors."

# What's missing in summary

The full video provides a detailed analysis of the risks and consequences associated with involving law enforcement in non-violent situations.

# Tags

#Police #911 #CommunitySafety #DeEscalation #RacialJustice #LegalSystem #Responsibility #CommunityIntervention #SocialAwareness