```

# Bits

- Beau addresses the stigma surrounding dating sexual assault survivors by comparing their emotional responses to combat vets.
- He challenges the notion that survivors are "damaged goods" and unworthy of love, stating that trauma affects everyone in some way.
- Beau shares a story of a combat vet's experience during a mission, showcasing the impact of trauma on memory.
- He encourages reflection on the similarities in emotional responses between survivors and combat vets.
- Beau concludes by urging viewers to reconsider the narratives around trauma and self-worth.

# Audience
Survivors, allies, supporters

# On-the-ground actions from transcript
- Challenge stigma around dating sexual assault survivors (exemplified)
- Support survivors and combat vets (exemplified)
- Advocate for understanding trauma's effects (exemplified)

# Oneliner
Beau challenges stigma by comparing emotional responses of sexual assault survivors and combat vets, urging a shift in perceptions of trauma and self-worth.

# Quotes
- "You are not damaged goods."
- "Everybody's broke in some way."
- "It is amazing what trauma can do to your memory."

# Whats missing in summary
The emotional depth and impact of Beau's storytelling.

# Tags
#Stigma #Trauma #SelfWorth #Survivors #CombatVets

```