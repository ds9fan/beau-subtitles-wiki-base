```

# Bits

- Beau responds to a story about a woman attacked for kneeling during the national anthem.
- Beau questions the purpose of protest and lectures on patriotism.
- Beau criticizes a spokesman for law enforcement for not understanding the protest's meaning.
- Beau contrasts nationalism with patriotism, advocating for correcting the government when wrong.
- Beau shares a story about patriotism being displayed through actions, not symbols.

# Audience
Patriotic activists, Protest supporters.

# On-the-ground actions from transcript
- Contact Beau for connections to military veteran for insights on patriotism. (suggested)

# Oneliner
Beau challenges misconceptions on patriotism and protest, advocating for action over symbols.

# Quotes
- "Patriotism is correcting your government when it is wrong."
- "There is a very marked difference between nationalism and patriotism."
- "Patriotism was displayed through actions, not worship of symbols."

# Whats missing in summary
Full context and emotional depth.

# Tags
#Patriotism #Protest #NationalAnthem #LawEnforcement #Activism

```