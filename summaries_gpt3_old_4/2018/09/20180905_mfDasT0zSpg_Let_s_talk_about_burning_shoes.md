# Bits

- Beau questions burning shoes as a form of protest.
- Urges to donate shoes to those in need instead.
- Points out hypocrisy in outrage towards Nike commercials.
- Compares burning shoes to burning the American flag.
- Calls out hypocrisy in prioritizing symbols over actual freedom.

# Audience
Consumers concerned about ethical consumption and activism.

# On-the-ground actions from transcript
- Donate unwanted shoes to shelters or thrift stores near military bases (exemplified)
- Raise awareness about the impact of consumer choices on workers' rights (generated)

# Oneliner
Burning shoes to protest Nike? Donate them to those in need instead and prioritize real change over symbolic gestures. 

# Quotes
- "You're loving that symbol of freedom more than you love freedom."
- "Take them and drop them off at a shelter."

# Whats missing in summary
Deeper exploration of the impact of consumer choices on labor rights and freedom.

# Tags
#EthicalConsumption #Activism #Symbolism #ConsumerRights #Freedom