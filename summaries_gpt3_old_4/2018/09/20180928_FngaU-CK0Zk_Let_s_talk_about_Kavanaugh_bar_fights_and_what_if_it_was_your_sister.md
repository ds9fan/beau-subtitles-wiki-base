```

# Bits

- Beau shares a story from college where he steps in to protect a girl from a pushy guy at a bar.
- He reflects on the importance of respecting women's boundaries and property rights over their own bodies.
- Beau connects the story to the Kavanaugh hearing and challenges viewers to empathize with the women involved.
- He breaks down statistics on sexual assault among women, urging viewers to realize the impact of their comments on survivors.
- Beau contrasts the response to male survivors coming forward with the skepticism faced by female survivors.
- He underscores the power dynamics involved in sexual assault, particularly in the context of Supreme Court nominations.
- Beau concludes by warning against dismissing survivors and damaging real-life relationships over political stances.

# Audience

Men, women, allies.

# On-the-ground actions from transcript

- Have a serious, respectful, and empathetic dialogue with friends about sexual assault statistics and survivor experiences. (exemplified)
- Challenge harmful comments or attitudes towards survivors of sexual assault. (exemplified)
- Take the time to understand the power dynamics of sexual assault and its impact on survivors. (suggested)

# Oneliner

Beau shares a personal story to illustrate the importance of respecting women's boundaries and urges viewers to empathize with survivors of sexual assault, challenging harmful attitudes.

# Quotes

- "He respected my property rights over her than he respected hers as a person."
- "It's that same thing that property relationship."
- "How many of them talked about it with you? I don't think any of them will now because they're not gonna trust you."

# What's missing in summary

The emotional delivery and nuanced storytelling are best experienced in the full video.

# Tags

#Respect #Empathy #SexualAssault #GenderEquality #SocialAwareness #Friendship #PowerDynamics #RealLifeRelationships

```