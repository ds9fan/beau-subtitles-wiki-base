```

# Bits

- Beau talks about armed teachers not being a solution, but a Band-Aid on a bullet wound at best.
- Describes the intense training and psychological preparation required for armed teachers in schools.
- Suggests creating mobile cover using a modified filing cabinet with steel plates for protection.
- Advises using frangible ammo to reduce risks of ricochet in crowded school settings.
- Encourages teachers to realistically weigh the risks and challenges of being armed in a school shooting scenario.
- Calls for a realistic evaluation of the feasibility and effectiveness of arming teachers in schools.

# Audience

Teachers, educators, school staff.

# On-the-ground actions from transcript

- Modify a filing cabinet with wheels to create mobile cover (exemplified).
- Contact a local metal fabricator for steel plates for protection (exemplified).
- Use frangible ammo to lower the risk of ricochet in a school shooting scenario (suggested).

# Oneliner

Arming teachers is a Band-Aid solution needing intense training, mobile cover, and realistic evaluation in schools.

# Quotes

- "You have to change your entire mindset. When you're walking through the hallway, you're not a teacher anymore, you're a warrior."
- "You have to train and you have to do it all in secret. You can't tell anybody."
- "You alone as an individual, as a teacher, as an educator, you have to become the expert in combat."

# Whats missing in summary

Further insights on the complexity of arming teachers and the need for alternative solutions.

# Tags

#ArmedTeachers #SchoolSafety #Training #SchoolShooting #Security #EducationSafety
```