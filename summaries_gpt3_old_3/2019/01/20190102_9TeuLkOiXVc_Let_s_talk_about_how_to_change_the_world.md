```

# Bits

- Beau introduces the video as part one of "how to change the world," likening it to a previous series on guns.
- A comment on Facebook prompts Beau to address the need for consciousness, rebellion, and building networks to effect change.
- Beau draws parallels between today's society in the Western world and the control mechanisms in George Orwell's "1984."
- Being conscious and rebelling against societal norms is emphasized as a necessary step towards change.
- Beau shares a personal anecdote about needing help and building a network in the aftermath of Hurricane Michael.
- He stresses the importance of having a network to achieve independence and the freedom to choose how tasks get done.
- Single parents are encouraged to contribute to networks by leveraging their skills and involving their children.
- Building a network is presented as a path towards creating a stronger community that can care for itself regardless of political leadership.
- Beau expresses skepticism towards top-down leadership and suggests community building as a more effective approach to change.
- The need for tailored approaches in building networks based on individual circumstances is acknowledged.

# Audience
Community members seeking to create positive change.

# On-the-ground actions from transcript
- Build a network by reaching out to individuals with relevant skills and forming mutually beneficial relationships. (exemplified)
- Offer help and seek assistance within your community to foster interdependence and resource sharing. (exemplified)
- Contribute your skills and leverage them to strengthen community bonds. (exemplified)
- Conduct an honest self-evaluation of your abilities to determine how you can best contribute to network building. (exemplified)

# Oneliner
To change the world, rebel against societal norms, build networks, and empower your community through interdependence.

# Quotes
- "Being independent doesn't actually mean doing everything by yourself. It means having the freedom to choose how it gets done."
- "Everybody has something to offer a network like this. Everybody."
- "You're changing the world you live in. You're changing the world by building your community."

# What's missing in summary
The full transcript provides detailed examples and personal anecdotes that illustrate the importance of networks in effecting change.

# Tags
#Change #CommunityBuilding #Networks #Interdependence #Rebellion #Empowerment #SocietalChange #SelfEvaluation #BuildingCommunity #ResourceSharing

```