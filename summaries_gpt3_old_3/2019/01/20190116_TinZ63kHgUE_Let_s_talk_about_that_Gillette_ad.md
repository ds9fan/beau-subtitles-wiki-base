```

# Bits

- Beau talks about the Gillette ad and masculinity, where he mentions his son's positive understanding of the message.
- He explains that concepts like not sexually assaulting, belittling, or bullying are not issues for men.
- Beau sees the ad as promoting real masculinity and mocking toxic masculinity, not feminism.
- He defends the razor company's right to weigh in on social issues, pointing out the lack of national rites of passage from boyhood to manhood in the U.S.
- Beau contrasts brutal rites of passage with a story about Amazonian boys enduring painful ant bites to become men, criticizing American men's response to discomfort.

# Audience

Men, feminists, individuals reflecting on masculinity.

# On-the-ground actions from transcript

- Contact Gillette to express support for their message (exemplified)
- Research and support initiatives promoting positive masculinity (generated)
- Learn about and respect diverse cultural rites of passage (exemplified)

# Oneliner

Gillette ad sparks debate on masculinity, rites of passage, and endurance, challenging perceptions along the way.

# Quotes

- "If you felt attacked by that ad, it says more about you than it does Gillette."
- "My 12-year-old boy was more of a man than the people that are upset with this."
- "In the Amazon, you've got these young boys becoming men who endure some of the most painful bites known to man for an extended period of time."

# What's missing in summary

Deeper insights into cultural perceptions of masculinity and responses to societal messages.

# Tags

#Gillette #Masculinity #RitesOfPassage #Feminism #ToxicMasculinity #SocialIssues #CulturalNorms #AmazonCulture #PositiveMasculinity
```