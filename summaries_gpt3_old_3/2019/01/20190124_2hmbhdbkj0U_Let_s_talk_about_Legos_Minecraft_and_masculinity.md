```

# Bits

- Beau expresses disappointment in a man who mocked kids at a Lego building competition.
- Beau praises the creative and valuable skills developed by kids building robots.
- Addressing the man directly, Beau suggests apologizing to the children for mocking their creativity.
- Beau contrasts the act of building robots with the mockery on Twitter, questioning the man's behavior.
- Beau encourages embracing and celebrating the creative aspects of masculinity, rather than mocking them.
- Beau concludes by advising the man to apologize on Twitter and reflects on the importance of valuing children's skills.

# Audience

Parents, adults, educators.

# On-the-ground actions from transcript

- Apologize to your child and the other kids on Twitter for mocking their creativity. (exemplified)
- Celebrate and encourage creative skills in kids around you. (suggested)

# Oneliner

Don't mock creativity; celebrate it. Apologize for undermining valuable skills in children. #CreativeSkillsMatter

# Quotes

- "Your kid built a robot. That is awesome. You should be proud."
- "You might want to get on Twitter and apologize to your son and all the other kids that got caught in a crossfire because you wanted to look tough and manly picking on kids."

# What's missing in summary

The full video provides a deeper exploration of toxic masculinity and the importance of nurturing creative skills in children.

# Tags

#ToxicMasculinity #Creativity #Parenting #Apology #KidsSkills #SupportChildren