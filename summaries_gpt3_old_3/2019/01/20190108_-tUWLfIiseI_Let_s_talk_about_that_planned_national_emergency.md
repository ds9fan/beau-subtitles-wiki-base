```

# Bits

- Beau talks about the planned national emergency over the border wall and its implications.
- Border apprehensions are at a 40-year low along the southern border.
- The Constitution of the United States lays out a process for funding projects.
- Beau draws parallels between Trump and Hitler, pointing out specific elements of fascism.
- He stresses the importance of Congress as a part of the checks and balances system.
- Beau urges people to choose between supporting the Constitution or the President.
- He references historical figures as examples of choices people faced in difficult times.

# Audience

Citizens, activists, political analysts.

# On-the-ground actions from transcript

- Contact your representatives to express your stance on the border wall and national emergency (suggested).
- Stay informed about political decisions and their implications (exemplified).
- Advocate for upholding the Constitution and democratic principles (generated).

# Oneliner

Beau draws parallels between Trump and Hitler, urging people to choose between supporting the Constitution or the President in the face of a national emergency.

# Quotes

- "Either you support the constitution of the United States or you betray it."
- "Circumventing Congress, it's a betrayal of the very ideas this country was founded on."
- "You're a patriot or a traitor."

# Whats missing in summary

The emotional intensity and historical context can be best understood by watching the full video.

# Tags

#NationalEmergency #BorderWall #Fascism #Constitution #ChecksAndBalances #PoliticalChoice #HistoricalParallels #CitizenEngagement
```