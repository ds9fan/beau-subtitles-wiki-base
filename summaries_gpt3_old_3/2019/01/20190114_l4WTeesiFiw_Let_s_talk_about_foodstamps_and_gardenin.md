```

# Bits

- Beau talks about the ability to buy seeds and fruit trees with food stamps, providing a solution for some.
- Addressing misconceptions about food stamps, Beau shares that more than 10% of Americans are on food stamps, with an average household receiving about $250 a month in benefits.
- Beau dispels myths about food stamp recipients being lazy and explains that many working families rely on food stamps due to low wages.
- Beau criticizes the blame placed on food stamp recipients, redirecting the focus to systemic issues and government decisions.
- Beau suggests creative solutions for urban residents to grow their own food, such as vertical gardening, community gardens, or utilizing unused land with permission.
- Encouraging community building, Beau advises seeking permission to grow food on someone else's property, fostering networks and food security.

# Audience
Advocates, urban gardeners, community organizers.

# On-the-ground actions from transcript
- Start vertical gardening at home (exemplified).
- Set up or join a community garden (suggested).
- Seek permission to grow food on someone else's property (suggested).

# Oneliner
Beau debunks food stamp myths and advocates for growing your own food with innovative urban gardening solutions to build community and food security.

# Quotes
- "It's not the people who actually broke the system. It's not the people making the decisions up in DC."
- "Every little bit helps in that regard."
- "It's very hard to get out. In a couple of years, when apples start coming, you're probably going to need them still."

# What's missing in summary
The nuances of growing food in different climates and the potential challenges faced by individuals in areas less conducive to agriculture.

# Tags
#FoodStamps #CommunityGardening #UrbanAgriculture #FoodSecurity #SystemicIssues #CommunityBuilding
```