```

# Bits

- Beau addresses President Trump's negotiation tactics regarding the border wall funding.
- Trump offers $5.7 billion for the wall in exchange for protection for Dreamers and TPS recipients.
- Beau criticizes Trump for using immigrants as hostages to achieve his political goals.
- He questions why Trump won't establish a path to citizenship for Dreamers.
- Beau accuses Trump of acting like a criminal through his negotiation tactics.
- He calls out Congress and the Senate for not holding Trump accountable for his actions.

# Audience

Politically engaged citizens.

# On-the-ground actions from transcript

- Contact your representatives to demand a path to citizenship for Dreamers (suggested).
- Advocate for the protection of immigrants under Temporary Protected Status (suggested).
- Speak out against using immigrants as hostages for political gain (exemplified).
- Support organizations working towards immigrant rights and citizenship paths (exemplified).

# Oneliner

President Trump's negotiation tactics on border wall funding are criticized for using immigrants as pawns, with calls for establishing a path to citizenship for Dreamers.

# Quotes

- "The President of the United States is acting like a criminal."
- "They came here as kids. I don't think they should be punished for the sins of their father."
- "Not disrupt the lives of federal employees, of people in need, of everybody."

# What's missing in summary

Deeper analysis on the impact of political negotiations on immigrant lives and the importance of holding leaders accountable.

# Tags

#Immigration #Politics #BorderWall #Dreamers #TPS #Citizenship #Advocacy #Accountability #Congress #Senate

```