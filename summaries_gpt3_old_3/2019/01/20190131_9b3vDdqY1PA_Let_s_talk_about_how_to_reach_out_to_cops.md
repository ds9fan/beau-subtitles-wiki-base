```

# Bits

- Beau talks about the slogan ACAB and its philosophy behind it.
- He explains the potential issues with using slogans like ACAB in communicating with cops.
- Beau suggests focusing on harm reduction and self-interest to reach cops effectively.
- He points out the failures of the drug war and how it impacts law enforcement.
- Beau provides examples from Portugal to showcase the effectiveness of decriminalization.
- He delves into how cops perceive themselves and the importance of image and culture.
- Beau recommends discussing issues like the drug war and gun control to challenge cop perspectives.

# Audience
Individuals engaging with law enforcement

# On-the-ground actions from transcript
- Question cops about their feelings on the drug war's impact on their colleagues (exemplified)
- Advocate for harm reduction strategies when engaging with law enforcement (suggested)
- Raise awareness about the failures of the drug war to prompt critical thinking (generated)

# Oneliner
Engage law enforcement on harm reduction strategies and question the impacts of the failed drug war to foster dialogue and change perspectives.

# Quotes
- "The drug war is without a doubt the focal point of our problems with law enforcement."
- "Not waging the drug war is more effective than waging it if you actually believe in harm reduction."
- "That image may be more 
- "The drug war and gun control are probably the two best ways to reach out to cops and explain that what they are now isn't what they think they are."
- "If there's no harm, there's no crime. If there's no victim, there's no crime."

# Whats missing in summary
Detailed examples of harm reduction strategies in law enforcement interactions.

# Tags
#LawEnforcement #ACAB #DrugWar #HarmReduction #PoliceCulture #Decriminalization
```