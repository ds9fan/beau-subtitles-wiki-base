```

# Bits

- Beau introduces the topic of honor killings, specifically focusing on a case in Georgia involving Tanya Lynn.
- The Supreme Court of Georgia granted a retrial to a man who killed his wife based on the judge not allowing enough testimony about her alleged infidelity.
- Beau points out the alarming fact that a significant number of women in the US are killed by current or former intimate partners.
- He draws attention to the issue of toxic masculinity, where murder becomes a response to perceived betrayal by a woman.
- Beau stresses the importance of calling these acts what they are—honor killings stemming from toxic masculinity.

# Audience

Activists, advocates, policymakers.

# On-the-ground actions from transcript

- Raise awareness about intimate partner violence and honor killings. (suggested)
- Support organizations working to prevent domestic violence and support survivors. (exemplified)
- Advocate for legal reforms to address issues related to toxic masculinity and honor killings. (generated)

# Oneliner

Beau sheds light on honor killings in the US, urging a reexamination of toxic masculinity and intimate partner violence.

# Quotes

- "A woman cheats, engages in infidelity, has an affair. The response is not to leave, it's not divorce, it's not counseling, it's murder."
- "We need to call it what it is because this is toxic masculinity and that's the problem."

# What's missing in summary

The emotional impact of Beau's storytelling and the urgency of addressing toxic masculinity in cases of intimate partner violence.

# Tags

#HonorKillings #ToxicMasculinity #DomesticViolence #Awareness #Activism