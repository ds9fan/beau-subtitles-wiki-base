```

# Bits

- Beau talks about the failed wall GoFundMe and the money being refunded.
- He explains the unrealistic expectations of the GoFundMe and the math behind it.
- Beau expresses that Americans don't want the wall for moral, ethical, legal, and economic reasons.
- He contrasts the proposed US wall with Israel's wall, pointing out differences in design and purpose.

# Audience
People interested in understanding the ineffectiveness of the border wall GoFundMe and the reasons Americans oppose the wall.

# On-the-ground actions from transcript
- Contact local representatives to express opposition to the border wall (exemplified)
- Support organizations working towards immigration reform (exemplified)

# Oneliner
The failed border wall GoFundMe reflects Americans' opposition to the wall for moral, ethical, legal, and economic reasons, contrasting it with Israel's wall.

# Quotes
- "Americans don't want the wall. They know that it's morally wrong, ethically wrong, legally wrong, economically wrong, and mathematically wrong."
- "If just a third of the people in the United States wanted the wall and were willing to give a dollar, it could have raised more than a hundred million dollars but it didn't."

# What's missing in summary
Deeper insights into the complexities of border security and immigration policy.

# Tags
#BorderWall #GoFundMe #ImmigrationReform #USPolicy #IsraelWall #EthicalConcerns
```