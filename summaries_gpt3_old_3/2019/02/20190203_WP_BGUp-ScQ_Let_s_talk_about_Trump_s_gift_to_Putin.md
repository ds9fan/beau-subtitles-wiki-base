```

# Bits

- Beau explains how Trump's decision to withdraw from a treaty banning short and intermediate range land-based missiles was a gift to Putin.
- The treaty negotiated in 1987 favored the United States entirely, allowing them to walk away with everything while the Soviet Union walked away with nothing.
- By withdrawing from the treaty, the U.S. removed the only obstacle hindering Russia from developing weapon systems that could threaten European allies.
- Beau expresses concern about the possibility of an arms race and the repercussions of the U.S.'s decision to withdraw from the treaty.
- He criticizes the administration for giving up a strategic advantage without gaining anything in return and compares Obama's administration's approach to Putin with that of Trump's.

# Audience
Foreign policy analysts, activists

# On-the-ground actions from transcript
- Contact your representatives to express concern about the U.S. withdrawing from the treaty (implied)
- Stay informed about international treaties and their implications (exemplified)

# Oneliner
Trump's withdrawal from a treaty banning missiles was a gift to Putin, raising concerns about European security and the risk of an arms race.

# Quotes
- "The U.S. opened the door and got rid of the only obstacle that was standing in Russia's way of developing weapon systems that can annihilate entire cities in Europe."
- "Russia didn't pull out of this. We did."

# What's missing in summary
Details on the political implications and responses to the treaty withdrawal.

# Tags
#ForeignPolicy #Treaty #Security #ArmsRace #Putin #UnitedStates

```