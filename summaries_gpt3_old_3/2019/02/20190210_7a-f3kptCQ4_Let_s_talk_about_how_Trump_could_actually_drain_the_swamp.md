```

# Bits

- Beau tweets the president about draining the swamp of campaign finances, suggesting he could support real campaign finance reform legislation to make it unstoppable.
- Beau points out that the president's promise to drain the swamp may not have been genuine, as he himself is a businessman who benefits from the current system.
- Beau contrasts AOC, who is actively addressing campaign finance issues, with the president who is not reaching out to her but instead is surrounded by alligators.
- The president's focus on cutting out the middleman rather than genuinely tackling campaign finance issues is critiqued by Beau.

# Audience
Political activists and advocates

# On-the-ground actions from transcript
- Contact your representatives to support real campaign finance reform legislation (exemplified)

# Oneliner
Beau criticizes the president for not genuinely addressing campaign finance issues and suggests supporting real reform legislation.

# Quotes
- "He doesn't care about draining the swamp. He was just cutting out the middleman."
- "That bill [campaign finance reform legislation] ould be unstoppable if he actually wanted to drain the swamp."

# What's missing in summary
Context on AOC's stance and actions on campaign finance issues.

# Tags
#CampaignFinanceReform #GovernmentAccountability #PoliticalActivism #AOC #PresidentTrump #LegislationAction
```