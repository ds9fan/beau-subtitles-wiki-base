```

# Bits

- Beau talks about President Trump wanting to put the Bible back in schools.
- Christian kids at a school didn't like the idea, feeling they get enough on Sundays.
- Beau believes in teaching religious texts in schools, focusing on philosophy rather than enforcing moral codes.
- He mentions passages from the Quran, Torah, Buddhism, Hinduism, and Christianity showing common teachings.
- Beau criticizes the hypocrisy within Christianity, especially in politics.
- He mentions that public schools won't be turned into religious institutions.
- Beau talks about the Church of Satan showing up to demand their texts be taught too.
- He advocates for teaching philosophy in schools to teach how to think rather than what to think.

# Audience

Education policymakers, religious educators, community leaders.

# On-the-ground actions from transcript

- Contact local schools to advocate for teaching various religious texts (suggested).
- Research and advocate for the inclusion of philosophical teachings in school curriculums (suggested).

# Oneliner

Beau dissects the controversy around teaching religious texts in schools, advocating for philosophical education over indoctrination.

# Quotes

- "If you don't follow your own code of ethics, why on earth you expect someone else to?"
- "The ultimate irony is that today's Church of Satan embodies most of the ideals of Christianity better than most Christian churches."
- "The goal of education should not be to teach you what to think. It should be to teach you how to think."

# Whats missing in summary

Beau's engaging delivery and nuanced exploration of the intersection between religion, education, and philosophy.

# Tags

#Religion #Education #Philosophy #Christianity #Schools #Teaching #Hypocrisy #ChurchOfSatan #Ethics #CommunityLeadership
```