```

# Bits

- Beau explains the complexity of leaving a domestic violence situation, mentioning financial control, pet safety, and job security as barriers.
- Domestic violence affects everyone regardless of gender, race, or class.
- Beau shares personal connections to the cause, including a friend who was killed by her husband.
- Domestic violence shelters significantly lower the risk of harm and provide support for survivors to restart their lives.
- Shelter House of Northwest Florida is mentioned as a forward-thinking organization addressing the various challenges faced by survivors.
- Beau encourages donations, including money and old cell phones, to support organizations aiding domestic violence survivors.
- Beau dispels stigmas around domestic violence and encourages everyone to get involved in supporting survivors.

# Audience
Supporters of domestic violence survivors.

# On-the-ground actions from transcript
- Donate money or old cell phones to organizations like Shelter House of Northwest Florida (exemplified).
- Contact Shelter House of Northwest Florida to inquire about ways to support survivors (exemplified).
- Sponsor a family or provide support kits for rape victims at hospitals (exemplified).

# Oneliner
Leaving domestic violence is complex, support organizations like Shelter House of Northwest Florida to help survivors restart their lives and overcome barriers.

# Quotes
- "Domestic violence affects everybody, every class, but it's not as simple as just leaving."
- "Any obstacle that's standing in the way, they get rid of it."
- "Odds are you have an old cell phone sitting in a drunk drawer, mail it to them."

# Whats missing in summary
The emotional impact and personal stories shared by Beau can best be experienced by watching the full video.

# Tags
#DomesticViolence #SupportSurvivors #Donate #ShelterHouse #Awareness #CommunitySupport
```