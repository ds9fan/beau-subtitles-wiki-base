```

# Bits

- Beau talks about the political division in the United States, comparing it to the time of desegregation.
- He mentions the dominance of Trump's wall in headlines despite most Americans feeling it isn't a priority.
- Beau criticizes the government and large corporations for advocating conservation while harming the environment.
- He touches on lobbying and corruption with $3.4 billion spent on these activities in the country.
- Beau points out the misinformation spread by the news and the ongoing drug war leading to innocent people being imprisoned.
- The issue of fragile masculinity is discussed, with men reacting strongly to a razor ad.
- Systemic racism, hate crimes, and the presence of literal Nazis on the streets are also mentioned.
- Beau shifts to a more positive outlook, talking about Americans taking action to combat these issues.
- There is a mention of companies producing more pollution than the entire U.S. citizenry and the strengthening of communities from within.
- Beau applauds independent journalists for separating fact from fiction and praises those fighting against systemic racism.

# Audience

Activists, concerned citizens, community organizers.

# On-the-ground actions from transcript

- Join organizations fighting against systemic racism. (exemplified)
- Support independent journalists and media outlets. (exemplified)
- Donate to domestic violence shelters or other charitable causes. (exemplified)
- Take steps to combat environmental degradation in your community. (exemplified)
- Stand up against hate crimes and work towards unity in diversity. (exemplified)

# Oneliner

The United States faces deep divisions, but individuals can drive change by taking action against systemic issues and promoting unity in diversity.

# Quotes

- "We defeat tyranny and oppression by ignoring it while we quietly create our own systems to replace it."
- "The state of the Union? I don't have a clue. The state of those people who care? Well, they're in a state of defiance."

# Whats missing in summary

More context on the challenges faced by today's youth and the need for intergenerational solidarity in addressing societal issues.

# Tags

#PoliticalDivision #SystemicIssues #Unity #Activism #CommunityEmpowerment #Journalism