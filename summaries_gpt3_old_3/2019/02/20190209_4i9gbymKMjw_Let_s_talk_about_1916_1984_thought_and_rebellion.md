```

# Bits

- Beau talks about the Irish Movement for Independence starting with the takeover of the General Post Office in Dublin in 1916.
- The rebellion in 1984 by Winston, from George Orwell's novel, began with independent thought and writing a diary, not physical acts.
- Beau stresses the importance of independent thought as the true starting point of rebellion and change.
- He contrasts meaningful, thought-provoking Facebook posts with shallow memes that don't encourage true independent thought.
- Beau reflects on how ideas have the power to create change without the use of force or violence.
- He urges listeners to think for themselves, spark meaningful dialogues, defend their ideas, and create change through free thinking.

# Audience

Activists, critical thinkers, rebels

# On-the-ground actions from transcript

- Think critically and independently about societal issues (implied)
- Spark meaningful dialogues and defend your ideas (implied)
- Create change by sharing and discussing new ideas (implied)

# Oneliner

Independent thought sparks rebellion and change more effectively than physical acts in Beau's reflection on historical events and modern communication.

# Quotes

- "Real rebellion starts with thought, independent thought, free thought, looking around and analyzing things for yourself."
- "For the first time, ideas travel faster than bullets. Probably the route we should focus on."
- "As an act of rebellion, think. Think for yourself. About anything."

# What's missing in summary

Beau's engaging storytelling and emphasis on the power of independent thought are best experienced in the full transcript.

# Tags

#Rebellion #IndependentThought #Change #Ireland #Facebook #Ideas

```