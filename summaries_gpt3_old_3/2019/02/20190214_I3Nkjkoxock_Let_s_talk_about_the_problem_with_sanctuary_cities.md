```

# Bits

- Beau talks about a violent crime incident in Florida where an undocumented immigrant was victimized.
- The victim was threatened, beaten, tortured, and robbed by individuals who also threatened to have him deported.
- Sanctuary cities exist to protect undocumented immigrants from being afraid to go to the police.
- Beau criticizes the need for sanctuary cities and the impact on victims of violent crimes.
- There are concerns about the accuracy of ICE databases leading to wrongful detentions.
- Beau mentions the lower violent crime rates among illegal immigrants compared to native-born citizens.
- Private prisons may be pushing for a shift from the war on drugs to a war on immigration for financial gain.
- The problem with sanctuary cities is that they are not universal, leading to inconsistencies in law enforcement.

# Audience
Local residents, community leaders, advocates

# On-the-ground actions from transcript
- Advocate for the protection of undocumented immigrants in your community (exemplified)
- Support policies that prevent local law enforcement from enforcing immigration laws (exemplified)

# Oneliner
Sanctuary cities protect immigrant victims, but inconsistencies in enforcement pose challenges in ensuring their rights are upheld. 

# Quotes
- "Problem with sanctuary cities is that they even need to exist."
- "They're not real people. They can't go to the cops."
- "No local department should be enforcing laws outside of this jurisdiction."

# Whats missing in summary
Deeper insights into the impact of sanctuary city policies on community trust and safety.

# Tags
#SanctuaryCities #Immigration #ICE #ViolentCrime #CommunitySafety #PrivatePrisons #LawEnforcement

```