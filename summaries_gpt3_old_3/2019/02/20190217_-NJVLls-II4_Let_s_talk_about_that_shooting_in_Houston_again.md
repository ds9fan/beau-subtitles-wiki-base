```

# Bits

- Beau addresses the shooting in Houston and reveals that the warrant used was entirely fabricated.
- Houston police targeted an independent journalist in 2016, leading to journalists cultivating sources within the department.
- The public face of the police presents it as an isolated incident, while the private face suggests deeper issues within the department.
- Beau questions the origins of the heroin, ethics surrounding informants, and the need for an unbiased investigation.
- He calls for the involvement of the FBI and criticizes the demonization of the victim.

# Audience

Activists, journalists, community members.

# On-the-ground actions from transcript

- Contact the FBI to request their involvement in investigating the Houston Police Department (suggested).
- Support independent journalists by amplifying their work and seeking out alternative sources of information (exemplified).
- Advocate for transparency and accountability within law enforcement agencies by demanding unbiased investigations (generated).

# Oneliner

Houston police face scrutiny as Beau reveals a fabricated warrant, urging FBI involvement and challenging the demonization of victims.

# Quotes

- "Please stop demonizing the victim."
- "Bring in the FBI because there's going to be a whole bunch of questions you don't even want to ask."
- "Every investigative independent journalist in the country started cultivating sources and contacts within the Houston Police Department."

# What's missing in summary

The full video provides a detailed analysis of the fabricated warrant and deeper issues within the Houston Police Department, along with insights on accountability in law enforcement.

# Tags

#Houston #Police #FabricatedWarrant #FBI #Transparency #Accountability #VictimDemonization #IndependentJournalism
```