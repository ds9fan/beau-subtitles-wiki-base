```

# Bits

- Beau shares the story of a historical figure who inspired him.
- The historical figure engaged in passive resistance against the Nazi regime.
- The White Rose Society, led by the figure, wrote leaflets advocating resistance.
- The figure was executed at 21 years old, leaving behind powerful words.
- Beau reflects on the lessons we can learn from her life and actions.

# Audience
History enthusiasts, activists, educators.

# On-the-ground actions from transcript
- Research historical figures who inspired resistance (exemplified)
- Share stories of bravery and resistance with others (exemplified)
- Support causes promoting passive resistance (exemplified)

# Oneliner
Meet Sophie Scholl, a symbol of passive resistance against tyranny, inspiring courage and action.

# Quotes
- "How can we expect righteousness to prevail when there is hardly anybody willing to give himself up individually to a righteous cause?"
- "Somebody after all had to make a start. What we wrote and said is also believed by others. They just don't dare express themselves as we did."

# What's missing in summary
More depth on Sophie Scholl's trial and how her actions influenced others.

# Tags
#SophieScholl #PassiveResistance #WhiteRoseSociety #NaziRegime #Courage

```