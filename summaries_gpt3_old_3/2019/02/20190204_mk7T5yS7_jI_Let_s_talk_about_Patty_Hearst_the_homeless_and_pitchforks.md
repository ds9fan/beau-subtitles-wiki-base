```

# Bits

- Beau reflects on the kidnapping of Patty Hearst and the moral implications of extreme wealth.
- He uses Jeff Bezos as an example to illustrate the potential impact of billionaires on society.
- Beau suggests that one wealthy individual could end homelessness in the US with a fraction of their wealth.
- He questions the morality of extreme income inequality and proposes possible actions to address it.
- Beau warns that growing income inequality may lead to social unrest and advocates for change.

# Audience

Social activists, advocates, policymakers.

# On-the-ground actions from transcript

- Advocate for policies that address income inequality (exemplified).
- Support initiatives to end homelessness in the community (exemplified).
- Boycott companies that contribute to social inequality (suggested).

# Oneliner

45 years after Patty Hearst's kidnapping, Beau questions the morality of extreme wealth and the potential for billionaires to address societal issues like homelessness.

# Quotes

- "One guy can end homelessness and give every homeless person in the country $200 a week."
- "We're reaching that critical point where it's not going to be solved with taxes."
- "When people get hungry and people get desperate, they act."
- "The ultra-wealthy probably need to [...] know where the pitchforks are."
- "It's just a fault."

# What's missing in summary

Further insights on the impact of extreme income inequality and potential solutions.

# Tags

#IncomeInequality #WealthDistribution #Homelessness #SocialChange #Activism #Billionaires

```