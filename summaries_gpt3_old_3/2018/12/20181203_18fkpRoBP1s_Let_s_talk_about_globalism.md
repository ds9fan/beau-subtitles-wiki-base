```

# Bits

- Beau talks about globalism and nationalism, and how they are used as buzzwords.
- He explains that globalism is often associated with a new world order and control by an elite group.
- Nationalism, according to Beau, is similar to globalism but on a smaller scale, focusing on kicking down instead of punching up.
- Beau challenges the notion of representative democracy and questions whether senators truly represent the public.
- He criticizes the idea of blaming foreigners for job loss instead of holding CEOs and politicians accountable.
- Beau states that he is neither a nationalist nor a globalist, suggesting there are better ways to make decisions for everybody.
- He describes nationalists as low-ambition globalists who want control over their little corner of the world.

# Audience
Globalism and nationalism observers

# On-the-ground actions from transcript
- Challenge the use of buzzwords in political discourse (exemplified)
- Question the representation of politicians and policymakers (exemplified)
- Hold corporations and politicians accountable for decisions impacting jobs (exemplified)

# Oneliner
Beau breaks down globalism and nationalism as buzzwords used in political rhetoric, challenging the status quo.

# Quotes
- "All a nationalist is, is a low-ambition globalist."
- "Both globalism and nationalism, it's just a method of motivating people through different means to kick down instead of punching up."
- "There are some pretty smart folks out there. I think they can come up with something better than that."

# What's missing in summary
Deeper exploration on the impact of globalism and nationalism on socio-political dynamics.

# Tags
#Globalism #Nationalism #Buzzwords #PoliticalRhetoric #Accountability #Representation #DecisionsMaking #SocioPoliticalDynamics

```