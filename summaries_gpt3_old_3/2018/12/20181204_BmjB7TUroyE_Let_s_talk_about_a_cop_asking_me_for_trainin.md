```

# Bits

- Beau shares a story about a cop suggesting using excessive force and the lack of accountability in law enforcement training.
- He explains the importance of "time, distance, and cover" in police work to prevent unnecessary harm.
- Beau talks about auditory exclusion in high-stress situations and the misunderstanding of resistance.
- He warns against excessive use of force and stresses the importance of proper training and equipment.
- Beau recounts a personal anecdote about unjust laws and the discretion of law enforcement officers.
- He advises against seeking advanced training within one's department and suggests outside intervention for reform.

# Audience
Law enforcement professionals

# On-the-ground actions from transcript
- Implement "time, distance, and cover" protocol in police work to ensure safety (implied)
- Learn about auditory exclusion and its effects in high-stress situations (implied)
- Wear protective vests and stay behind cover during shootouts to minimize risks (implied)
- Exercise discretion in enforcing unjust laws, considering the spirit over the letter (implied)
- Seek reform in police training and practices through external advocacy (implied)

# Oneliner
Beau sheds light on police training, safety protocols, and the importance of discretion in law enforcement to prevent harm and ensure accountability.

# Quotes
- "Most bad shoots are because this was not applied. Time, distance, and cover, please start to use it."
- "When you run across somebody that is ideologically motivated, and they are breaking the law, but the crime has no victim, keep rolling."
- "There are a lot of unjust laws out there. When you run across somebody that is ideologically motivated, keep rolling."

# Whats missing in summary
The full transcript provides detailed insights into police training, safety measures, and the need for reform in law enforcement practices.

# Tags
#LawEnforcement #PoliceTraining #SafetyProtocols #Reform #AuditoryExclusion #Discretion

```