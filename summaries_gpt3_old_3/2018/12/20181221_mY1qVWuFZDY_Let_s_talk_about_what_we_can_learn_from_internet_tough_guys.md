```

# Bits

- Beau talks about the internet tough guy and what we can learn from them being a symptom and a perfect analogy.
- The internet tough guy is compared to American foreign policy, both being insulated from consequences behind a machine.
- Beau delves into the impact of American foreign policy and the disconnect Americans have due to their insulation.
- He addresses differing attitudes towards helping foreigners, suggesting the need for the wealthy to care to affect change.
- Beau points out that global change must start with individuals making conscious choices with their purchases.
- He stresses the importance of individuals taking action to drive change and how global revolutions are interconnected.

# Audience
Global citizens, Social Activists

# On-the-ground actions from transcript
- Examine your own actions and purchasing habits to support the kind of world you want (exemplified)
- Start making conscious choices with your purchases to influence corporations (exemplified)
- Recognize the need for global interconnectedness in driving revolutions (suggested)

# Oneliner
Beau reflects on the internet tough guy, American foreign policy, and the power of individuals in shaping a global revolution through conscious choices.

# Quotes
- "Every time we go to the Walmart or the grocery store or the gas station, we are telling those massive corporations what kind of world we want."
- "Money makes the world go round, right? Because of the abundance that this country has and the wealth that has accumulated here, It is the machinery for change."

# Whats missing in summary
Deeper insights on the impact of global interconnectedness in driving revolutions and the role of individuals in shaping a better world.

# Tags
#InternetToughGuy #AmericanForeignPolicy #GlobalChange #IndividualResponsibility #Revolutionaries #ConsciousConsumerism
```