```

# Bits

- Beau shares a story from the Vietnam War about humanity during conflict.
- He contrasts this with the inhumane actions of some Border Patrol agents.
- Beau criticizes Border Patrol for dumping out water left for migrants, endangering lives.
- He calls out the Border Patrol agents for their actions and urges them to quit.
- Beau stresses that deterrence measures like walls won't stop people seeking safety.

# Audience
Border Patrol agents

# On-the-ground actions from transcript
- Quit your job at Border Patrol (urged)
- Turn in your resignation (urged)

# Oneliner
In a powerful message, Beau contrasts acts of humanity in conflict with the inhumane actions of some Border Patrol agents, urging them to quit their jobs.

# Quotes
- "Just following orders isn't going to cut it."
- "Your badge is not going to protect you from that. You need to quit."
- "If you work for border patrol you need to quit because eventually this administration is going to leave office and you are going to be held accountable."

# Whats missing in summary
The full impact of Beau's emotional delivery and the historical context he provides.

# Tags
#Humanity #Conflict #BorderPatrol #Quit #Safety #Migration #Accountability #Deterrence #HumanSpirit
```