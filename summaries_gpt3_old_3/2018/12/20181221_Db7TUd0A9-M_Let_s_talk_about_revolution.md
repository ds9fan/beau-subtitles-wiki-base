```

# Bits

- Beau talks about revolution and the Yellow Vest Movement.
- He explains that riots become rebellions seeking reform.
- Beau differentiates between a revolution and regime change.
- He mentions the impact of foreign influence on revolutions.
- Beau describes the essence of a revolution as bringing new ideas to the table.
- He points out the trend in Western civilization to decentralize power through revolutions.
- Beau warns that a revolution in the US could lead to violent consequences.
- He explains the potential repercussions of a revolution on the country's infrastructure and supplies.
- Beau stresses that violent revolution is not the solution and suggests focusing on new ideas.
- He concludes by prompting a dialogue on governments that empower people.

# Audience

Activists, Americans, Revolutionaries

# On-the-ground actions from transcript

- Analyze and understand the characteristics of revolutions (exemplified)
- Foster dialogue on empowering government structures (generated)

# Oneliner

Beau warns of the violent consequences of a revolution in the US and advocates for dialogue on empowering government systems.

# Quotes

- "A revolution is bringing a new idea to the table."
- "People always think, 'Well, it can't happen here like that.'"
- "If the trucks stop rolling, those 18-wheelers, day one, there's shortages of some kinds of food."
- "You can put a bullet in it, or you can put a new idea in it."
- "In the United States, a violent revolution will be horrible."

# Whats missing in summary

The detailed examples and historical contexts discussed by Beau can provide a deeper understanding of the potential consequences of a revolution.

# Tags

#Revolution #YellowVestMovement #Violence #USGovernment #WesternCivilization

```