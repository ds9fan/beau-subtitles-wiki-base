```

# Bits

- Beau addresses the issue of protest and human shields in the U.S.
- Protesters upset with government violating rights are met by militarized goons.
- Beau references the Boston Massacre as a historical parallel to current events.
- Beau criticizes the idea of using human shields and harming civilians in conflicts.
- The importance of upholding moral standards and not betraying foundational values is emphasized.

# Audience

Protesters, activists, community members.

# On-the-ground actions from transcript

- Contact local representatives to express concerns about police violence (suggested).
- Join peaceful protests to advocate for human rights and equality (exemplified).
- Organize community forums to raise awareness about moral standards in conflict situations (generated).

# Oneliner

Protesters face militarized response, invoking historical parallels on rights and human shields, urging adherence to moral standards.

# Quotes

- "You do not punish the child for the sins of the father."
- "You do not open fire on unarmed crowds."
- "Betraying everything that this country stood for, at least pretended to stand for."

# What's missing in summary

The emotional weight and historical context can be fully grasped by watching the full video.

# Tags

#Protest #HumanRights #MoralStandards #PoliceViolence #AmericanValues #CommunityEngagement
```