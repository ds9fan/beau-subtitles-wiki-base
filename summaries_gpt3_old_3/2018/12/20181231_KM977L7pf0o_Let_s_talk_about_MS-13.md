```

# Bits

- Beau explains the origins of MS-13, tracing back to a coup in El Salvador in 1979 and the subsequent civil war.
- The gang grew in power when gang members were sent back to El Salvador, a country with no police.
- A key figure in transforming MS-13 into a transnational criminal enterprise was trained by the US military.
- Beau questions the need for a border wall given that American foreign policy played a significant role in the creation of MS-13.
- He suggests curbing American foreign policy instead of building walls as a solution.
- Beau criticizes the lack of scrutiny on individuals trained by the US military for violent acts compared to asylum seekers.
- The narrative underscores how American actions contributed to the rise of MS-13.

# Audience

Policy analysts, activists, community organizers.

# On-the-ground actions from transcript

- Contact policymakers to advocate for changes in American foreign policy regarding training individuals for violence. ( suggested )
- Join advocacy groups working to reform US military training programs to prevent the creation of violent groups. ( exemplified )

# Oneliner

MS-13's origins tied to American foreign policy raise questions about the effectiveness of border walls and the need for policy changes. 

# Quotes

- "MS-13 is an American creation, not just in the sense that it was founded here, but in the sense that every step of the way, it was American foreign policy that built it."
- "Maybe a better idea [than building a wall] is to curtail our foreign policy."
- "You never hear them calling for strict scrutiny or vetting of those people that the U.S. military trains to murder, torture, assassinate, and dominate a community by fear."

# What's missing in summary

The full video provides a deeper exploration of the impact of American foreign policy on the rise of MS-13.

# Tags

#MS-13 #AmericanForeignPolicy #BorderWall #USMilitaryTraining #Advocacy