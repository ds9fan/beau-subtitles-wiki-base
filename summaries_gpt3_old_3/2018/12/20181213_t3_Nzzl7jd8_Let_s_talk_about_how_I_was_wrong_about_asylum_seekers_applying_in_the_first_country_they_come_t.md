```

# Bits

- Beau admits he was wrong about asylum seekers applying for asylum in the first country they come to.
- He discovered the Safe Third Country Agreement between the United States and Canada, realizing its limited scope.
- The treaty states that asylum seekers must apply for asylum in the country they first reach along their journey.
- Beau warns against getting legal information from memes and stresses the importance of reading and understanding treaties and laws.
- He points out that the U.S. has fallen out of requirements to be a safe third country, according to Canadian standards.
- President Trump tried to get Mexico designated as a safe third country despite global skepticism due to safety concerns.
- Beau concludes by stressing the importance of accurate legal knowledge and the limitations of the Safe Third Country Agreement.

# Audience
Immigration law enthusiasts.

# On-the-ground actions from transcript
- Read and understand international treaties and laws (exemplified)
- Avoid relying on memes for legal information (exemplified)

# Oneliner
Beau admits his mistake, uncovers the limitations of the Safe Third Country Agreement, and warns against misinformation from memes in legal matters.

# Quotes
- "Knowledge is power."
- "Don't get your legal information from memes."

# Whats missing in summary
The full context of the treaty and its implications.

# Tags
#ImmigrationLaw #AsylumSeekers #SafeThirdCountry #LegalKnowledge #InternationalTreaties #BeauFromInternetPeople