```

# Bits

- Beau talks about duty in recent news events, including a court ruling in Florida and General Mattis's resignation.
- Marines praising Mattis's honor, integrity, and sense of duty as Marines value these qualities.
- Beau agrees with Trump's decision to withdraw from Syria but acknowledges the impact on Kurdish allies.
- The federal court ruling states that law enforcement has no duty to protect citizens, even in extreme circumstances.
- Beau reminds that government controls, not protects, and law enforcement is its enforcement arm.
- Americans need to keep in mind the lack of duty for law enforcement to protect them, especially when advocating for legislation.

# Audience
Citizens, activists, policymakers

# On-the-ground actions from transcript
- Pay attention to politicians' sense of duty (exemplified)
- Advocate for legislation that empowers citizens to protect themselves (exemplified)

# Oneliner
Beau delves into duty with Florida's court ruling, Mattis's resignation, and a reminder on law enforcement's role: control, not protection.

# Quotes
- "Law enforcement is not there to protect you. We do not live under a protectorment."
- "Government is there to control you, not protect you."
- "There is no duty for them to protect you."

# What's missing in summary
The nuances and detailed analysis provided by Beau in the full transcript.

# Tags
#Duty #LawEnforcement #Government #Protection #Mattis #KurdishIndependence #FederalCourtRuling

```