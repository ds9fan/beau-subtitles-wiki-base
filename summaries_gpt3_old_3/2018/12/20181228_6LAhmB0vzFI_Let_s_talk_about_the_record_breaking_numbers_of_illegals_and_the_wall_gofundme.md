```

# Bits

- Beau talks about the record-breaking number of immigrants coming into the US and the flawed methodology used by Border Patrol.
- He points out that the number of illegal immigrants is at a 40-year low and questions the effectiveness of certain policies.
- The issue of raising funds for a border wall on GoFundMe is discussed, with Beau criticizing the lack of critical thinking among supporters.
- Beau raises concerns about the influence of private interests in government actions and warns against the dangers of fascism.
- The inefficacy of a border wall is explored, with Beau detailing alternative methods that smugglers could use to bypass it.
- He concludes by criticizing the fear tactics used by politicians and the lack of critical thinking in current political discourse.

# Audience

Citizens, Activists, Voters

# On-the-ground actions from transcript

- Challenge misinformation spread about immigration (exemplified)
- Support organizations advocating for comprehensive immigration reform (exemplified)
- Advocate for policies based on data and expert opinions (generated)

# Oneliner

Beau debunks immigration myths, criticizes wall funding, and warns against political manipulation, urging critical thinking for effective solutions.

# Quotes

- "We elect rulers instead of leaders."
- "We don't critically think."
- "The wall won't work."
- "Nobody in DC is going to look out for you."
- "You're willing to give money that's left over from them taxing you to them to build a wall that won't work."

# Whats missing in summary

Deeper insights on the historical context of immigration policies and the impact on neighboring countries.

# Tags

#Immigration #BorderWall #CriticalThinking #Policy #Fascism #GovernmentInfluence #PoliticalManipulation
```