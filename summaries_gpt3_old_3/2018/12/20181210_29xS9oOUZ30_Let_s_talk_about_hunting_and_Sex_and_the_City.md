```

# Bits

- Beau points out the stereotypical association of hunting with certain demographics.
- Trophy hunting, especially of exotic animals, by wealthy individuals is criticized by Beau.
- He explains how canned hunts work and their impact on conservation efforts.
- Beau suggests investing the hunting money in the local community instead.
- He recommends volunteering with anti-poaching task forces as an alternative to hunting dangerous game.
- Following Kristen Davis as an advocate for animals in Africa is encouraged.

# Audience
Conservationists, animal rights advocates.

# On-the-ground actions from transcript
- Invest hunting money in local communities for sustainability (suggested).
- Volunteer with anti-poaching task forces in Africa (suggested).
- Follow Kristen Davis as an advocate for animals in Africa (suggested).

# Oneliner
Beau criticizes trophy hunting, suggesting investing hunting money in local communities for sustainability and volunteering with anti-poaching task forces as alternatives.

# Quotes
- "First take the twenty or thirty thousand dollars you're gonna spend on this hunt and just invest it in the community where you were gonna go."
- "It's about proving your masculinity in a completely canned hunt."
- "That may be a little bit militant for some people."
- "Follow Kristen Davis. That's Charlotte from Sex and the City."
- "Y'all have a good night."

# What's missing in summary
Beau's passionate delivery and detailed examples are best experienced by watching the full video.

# Tags
#Hunting #TrophyHunting #Conservation #CommunityInvestment #AntiPoaching #AnimalAdvocacy #AfricaWildlife