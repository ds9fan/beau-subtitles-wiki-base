```

# Bits

- Beau talks about the origins and significance of Rule 303 in military and law enforcement.
- Rule 303 stems from a soldier using it as a justification for summary execution based on having the means to act.
- The term has evolved to imply "might makes right," but its essence lies in taking responsibility to act when needed.
- Beau gives examples of situations where individuals with the means to help chose to act, even without a direct duty.
- He contrasts these examples with instances like the Parkland shooting, where an officer did not intervene despite having the means.
- Beau challenges the excuses made for the officer's inaction, stressing the duty of those in law enforcement to protect others.
- He questions the mentality of prioritizing personal safety over the lives of those in danger.
- Beau concludes by urging individuals in law enforcement to revaluate their commitment to protecting others, especially in critical situations.

# Audience

Law enforcement officers, school resource officers.

# On-the-ground actions from transcript

- Analyze your commitment to protecting others in critical situations. (implied)
- If you cannot envision laying down your life to protect those in danger, reconsider your assignment. (implied)

# Oneliner

Rule 303: Having the means to act comes with the responsibility to protect, especially in critical situations where lives are at stake.

# Quotes

- "Your duty is not bounded by risk."
- "The gun makes the man."
- "Are you expendable? If not, quit."

# What's missing in summary

In-depth analysis of philosophical aspects and excuses made by law enforcement officers.

# Tags

#Rule303 #LawEnforcement #Responsibility #ProtectAndServe #CriticalSituations #Commitment
```