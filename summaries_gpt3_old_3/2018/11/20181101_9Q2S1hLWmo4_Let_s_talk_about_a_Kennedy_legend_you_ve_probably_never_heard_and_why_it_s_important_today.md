```

# Bits

- Beau introduces a conspiracy theory about President Kennedy and the Green Berets.
- President Kennedy allegedly met with Green Berets in 196, and they took an oath to fight against a tyrannical government if needed.
- 46 Green Berets served as Kennedy's honor guard after his assassination, a unique occurrence.
- The legend includes a story about Green Berets placing their green berets on Kennedy's casket.
- A group of Green Berets still visit Kennedy's grave site annually.
- Beau connects the legend to current political events, warning about the suspension of habeas corpus by the current president.

# Audience
History enthusiasts, political activists.

# On-the-ground actions from transcript
- Contact local representatives to express opposition to the suspension of habeas corpus. (suggested)
- Research and understand habeas corpus and its importance in upholding the rule of law. (exemplified)

# Oneliner
President Kennedy's legend with Green Berets warns against the suspension of habeas corpus today, urging action for the rule of law.

# Quotes
- "The suspension of habeas corpus is though, that is a threat to national security. It's a threat to your very way of life."
- "You can support your country or you can support your president. Those are your options."

# Whats missing in summary
The full transcript provides detailed historical context and a deeper exploration of the implications of habeas corpus suspension.

# Tags
#ConspiracyTheory #HabeasCorpus #GreenBerets #JFK #PoliticalActivism
```