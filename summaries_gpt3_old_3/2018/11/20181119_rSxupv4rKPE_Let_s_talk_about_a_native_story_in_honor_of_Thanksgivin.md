```

# Bits

- Beau talks about the real story of a Native American girl named Matoaka, also known as Pocahontas, and how it differs from the widely known version.
- He sheds light on the dark reality of Matoaka's life, including being kidnapped, held hostage, forced into marriage, and dying at a young age.
- Beau criticizes the popular culture's portrayal of Native Americans, often involving a white savior narrative and misrepresentation of their stories.
- He mentions the lack of authentic Native American representation in mainstream media and the tendency to fabricate myths and stories about them.

# Audience
History enthusiasts, educators, activists.

# On-the-ground actions from transcript
- Research and share accurate Native American history with others. (suggested)
- Support and amplify Native voices and stories in media and literature. (exemplified)

# Oneliner
Beau reveals the grim truth behind the Pocahontas story, urging a reevaluation of popular narratives on Native Americans.

# Quotes
- "Most of what you know of Native stories is false. It's completely false, completely made up."
- "There's not a whole lot of stories about natives just being native."
- "It's pretty clear he probably didn't do, at least not to the extent they're saying."

# What's missing in summary
Deeper exploration of the impact of misrepresentation on Native American communities.

# Tags
#NativeAmerican #History #Misrepresentation #Pocahontas #Narratives

```