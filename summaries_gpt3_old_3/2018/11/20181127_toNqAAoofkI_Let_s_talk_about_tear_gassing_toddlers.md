```

# Bits

- Beau Ginn talks about tear gas and toddlers, questioning reactions to violence.
- Mention of violence in the United States and justifications found on social media.
- Beau poses the scenario of someone tear gassing a child and asks how one should respond.
- Reference to Border Patrol and violence, questioning the nation's response to fear.
- Commentary on the behavior of the government and potential reactions along the border.

# Audience
Social activists, concerned citizens.

# On-the-ground actions from transcript

- Contact local representatives to express concerns about government behavior. (suggested)
- Support organizations working to protect children and families at the border. (exemplified)

# Oneliner
Beau Ginn questions violence, fear, and government behavior, urging reflection on responses to injustice.

# Quotes
- "What are you going to do when it doesn't happen?"
- "Sometimes, we tear gassed kids."

# What's missing in summary
Insight into Beau's full analysis and perspectives on violence, fear, and government actions.

# Tags
#Violence #GovernmentBehavior #Injustice #BorderPatrol #SocialJustice #Fearmongering