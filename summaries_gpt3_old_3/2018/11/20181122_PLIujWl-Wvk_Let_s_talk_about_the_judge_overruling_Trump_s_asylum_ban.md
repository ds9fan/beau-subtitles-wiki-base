```

# Bits

- Beau explains the importance of a judge overruling Trump's asylum ban.
- Many people were misled to believe crossing the border and claiming asylum is illegal.
- Trump attempted to change asylum law through a proclamation, defying the Constitution.
- Beau criticizes Trump's actions as an attempt to act like a dictator.
- He praises the judicial branch for stopping Trump's attempt but criticizes Congress for not doing more.
- Beau questions the patriotism of those who support Trump's actions.

# Audience
American citizens, activists, legal advocates.

# On-the-ground actions from transcript
- Share accurate information about asylum laws with others. (exemplified)
- Contact elected representatives to hold them accountable. (exemplified)
- Support organizations working to protect constitutional rights. (exemplified)

# Oneliner
Beau criticizes Trump's attempt to change asylum laws, calling it dictatorial and a betrayal of the Constitution.

# Quotes
- "If you weren't appalled by this and want this man out of office because of it, you don't care about the Constitution."
- "He attempted to undermine it, attempted to circumvent it."
- "People who fashioned themselves patriots are mad at the judge who stopped him from betraying the Constitution."
- "If it's your party, they can set fire to the Constitution and you'll roast a marshmallow over it."
- "It's treason."

# What's missing in summary
The emotional impact of hearing Beau passionately criticize Trump's actions and question people's patriotism.

# Tags
#Trump #AsylumBan #Constitution #Dictatorship #Patriotism

```