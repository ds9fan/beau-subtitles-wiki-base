```

# Bits

- Beau talks about the issue of police militarization and the concept of the warrior cop wanting to mimic the military.
- He challenges the idea that warriors equate to killers, citing that only four percent of the military are actually killers.
- Beau points out the shift in law enforcement from protecting the public to strictly enforcing the law, leading to moral dilemmas for officers.
- He criticizes the propaganda used to justify the militarization of police forces and the misconceptions around the dangers faced by law enforcement.
- Beau delves into the misuse of military-grade equipment by law enforcement, such as flashbangs, MRAPs, and SWAT teams, due to lack of proper training.
- He stresses the importance of understanding the true role of law enforcement as public servants and protectors, rather than warriors.

# Audience
Law enforcement officers, policymakers, community advocates

# On-the-ground actions from transcript
- Advocate for comprehensive training programs for law enforcement officers (exemplified)
- Support policies that prioritize community service and protection over militarization (exemplified)
- Call for transparency and accountability in the use of military-grade equipment by law enforcement (exemplified)
- Advocate for a shift in law enforcement culture towards serving and protecting the public (exemplified)

# Oneliner
Beau challenges the warrior cop mentality and calls for a return to true public service in law enforcement to prevent unnecessary fatalities.

# Quotes
- "If you're a cop in an area where feeding the homeless is illegal, you're a bad cop."
- "There is no war on cops. There should be no warrior cops."
- "Being a cop, not really that dangerous. Not a lot of deaths really. It's not even in the top 10."
- "You're the weapon if you're a warrior."
- "The more they step away from their role as public servants and protectors of the public, there's going to be more and more fatalities."

# What's missing in summary
In-depth examples and analysis on the impact of militarization on community relations and the need for systemic reforms in law enforcement practices.

# Tags
#PoliceMilitarization #WarriorCop #LawEnforcement #CommunityRelations #PublicService #Training

```