```

# Bits

- Beau explains the legal framework around asylum seekers, noting that those claiming asylum are following the law while those trying to stop them are violating it.
- Beau calls out the argument about who will pay for asylum seekers, pointing out that it's cheaper to be compassionate.
- He addresses the notion that asylum seekers are coming for benefits, explaining that Mexico offered benefits but they still seek safety.
- Beau argues that the issues driving asylum seekers are connected to past actions by the US, making it their problem.
- He challenges the idea that people should stay in their own countries to fix them, pointing out the role of US interference in destabilizing regions.
- Beau uses a powerful analogy to illustrate the scale of the refugee crisis and criticizes armchair commentators who talk tough without understanding the realities of conflict.
- He ends by reflecting on the impact of his previous comments and the importance of approaching serious topics with respect.

# Audience
Advocates, policymakers, activists

# On-the-ground actions from transcript
- Contact elected officials to advocate for compassionate asylum policies (exemplified)
- Support organizations like the Office of Refugee Resettlement through donations or volunteering (exemplified)
- Challenge misconceptions about asylum seekers in your community through education and awareness campaigns (exemplified)

# Oneliner
Beau dismantles arguments against asylum seekers, urging compassion and understanding in a complex global issue.

# Quotes
- "It's cheaper to be a good person."
- "Stop projecting your greed onto them. Maybe they just want safety."
- "Accept some responsibility for your apathy and stay and fix it."

# What's missing in summary
The full impact of Beau's emotional appeal and storytelling can be best understood by watching the entire video.

# Tags
#AsylumSeekers #LegalFramework #Compassion #HumanitarianCrisis #RefugeeSupport #GlobalResponsibility #ConflictResolution #Advocacy #Awareness #SocialImpact
```