```

# Bits

- Beau responds to a story of a woman being attacked for kneeling during the national anthem.
- Beau questions the author's understanding of patriotism and protests.
- Beau criticizes the author's lack of knowledge regarding the purpose of the protest.
- Beau challenges the idea that patriotism is blindly following the government.
- Beau shares a story about patriotism being displayed through actions, not symbols.

# Audience

Activists, Patriots, Protesters

# On-the-ground actions from transcript

- Contact Beau to connect with the civilian who served the country for 30 years and understands patriotism (suggested)
- Challenge misconceptions about patriotism and protests in your community (exemplified)
- Stand up for individuals exercising their constitutionally protected rights (generated)

# Oneliner

Beau dismantles misconceptions about patriotism, urging action beyond blind allegiance to symbols.

# Quotes

- "There's a very marked difference between nationalism, which is what this gentleman is talking about, and patriotism."
- "Patriotism is correcting your government when it is wrong."
- "Patriotism is displayed through actions, not worship of symbols."

# Whats missing in summary

The emotional depth and personal anecdotes shared by Beau are best experienced in the full video.

# Tags

#Patriotism #Protests #Misconceptions #Activism #Constitution #CommunityAction
```