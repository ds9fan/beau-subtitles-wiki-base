```

# Bits

- Beau talks about sexual harassment and assault in light of the Kavanaugh nomination.
- He provides advice on avoiding such situations, including safeguarding reputation and being careful with signals.
- Beau addresses the issue of victim blaming and the importance of accountability for actions.
- He stresses the need for men to be cautious and avoid situations that could lead to false accusations.
- Beau concludes with a powerful statement that rape is solely caused by rapists.

# Audience

Men, women, individuals concerned about sexual harassment and assault.

# On-the-ground actions from transcript

- Safeguard your reputation to attract the right kind of people. (exemplified)
- End dates at the front door for safety and quality. (exemplified)
- Avoid taking or going home with anyone when drinking. (exemplified)
- Watch what you say and be mindful of signals sent. (exemplified)
- Keep your hands to yourself to avoid misunderstandings. (exemplified)
- Be accountable for your actions to prevent false accusations. (exemplified)

# Oneliner

Beau offers advice on avoiding sexual harassment and assault, stressing accountability and caution to prevent harm.

# Quotes

- "Be very careful with the signals you send."
- "Rape is solely caused by rapists."

# What's missing in summary

The full transcript provides a detailed exploration of societal attitudes towards sexual harassment and assault, as well as the importance of personal accountability in preventing harm.

# Tags

#SexualHarassment #AssaultPrevention #Accountability #VictimBlaming #RapeCulture #MenToo

```