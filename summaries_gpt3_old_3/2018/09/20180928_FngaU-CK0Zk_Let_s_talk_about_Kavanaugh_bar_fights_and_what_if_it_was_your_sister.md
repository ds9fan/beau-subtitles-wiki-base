```

# Bits

- Beau shares a story of stepping in to protect a girl from a pushy guy at a bar.
- He reflects on the importance of respecting women's autonomy and property rights.
- Beau uses a friend-focused approach to make people realize the prevalence of sexual assault among women.
- He points out the emotional responses and double standards in how sexual assault cases are perceived.
- Beau underscores the power dynamics involved in sexual assault and the motivation behind survivors coming forward.
- He challenges individuals to reconsider their stance on sexual assault allegations and the impact of their words on real-life relationships.

# Audience
Men reflecting on sexual assault.

# On-the-ground actions from transcript
- Calculate the statistics on sexual assault among your friends and acquaintances. (exemplified)
- Challenge your preconceptions about sexual assault and power dynamics. (exemplified)
- Refrain from making dismissive comments about survivors of sexual assault. (generated)

# Oneliner
Beau shares a powerful story to challenge perceptions of sexual assault and respect for survivors.

# Quotes
- "It's that same thing that property relationship."
- "So do the math and then think about it."
- "You already ruined that."

# Whats missing in summary
The emotional impact conveyed through Beau's storytelling can be best understood by watching the full video.

# Tags
#SexualAssault #Respect #GenderEquality #Friendship #ChallengingPerceptions #Empathy #RealLifeRelationships
```