```

# Bits

- Beau questions burning Nike shoes as a symbol of protest.
- He suggests donating the shoes to those in need instead of burning them.
- Beau challenges the idea of focusing on symbols over real issues like sweatshops.
- He points out the inconsistency of boycotting Nike over a commercial while ignoring their unethical practices.
- Beau criticizes those who prioritize symbols over true freedom.

# Audience
Consumers, activists, social justice advocates

# On-the-ground actions from transcript

- Donate unwanted items to shelters or thrift stores near military bases. (exemplified)
- Advocate for fair labor practices and ethical sourcing in consumer products. (generated)

# Oneliner
Don't burn Nike shoes, donate them to those in need. Symbols shouldn't overshadow real issues like sweatshops and slave labor.

# Quotes
- "You're loving that symbol of freedom more than you love freedom."
- "Take them and drop them off at a shelter. Maybe give them to a homeless vet."
- "You don't really get to have an opinion about freedom because you don't know what it is."

# Whats missing in summary
Nuances of the argument and the emotional impact of prioritizing symbols over real issues.

# Tags
#Nike #Symbolism #Ethics #ConsumerActivism #SocialJustice

```