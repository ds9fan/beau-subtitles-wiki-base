```

# Bits

- Beau expresses concern over Facebook and Twitter censoring independent news outlets opposed to government policies.
- Beau points out the importance of outlets, even sensationalist ones, in filling a role in informing people.
- Beau shares that some of the outlets he knows were impacted by the censorship, leading him to encourage exploring alternative social media platforms.
- Beau warns about the dangers of censorship and the potential end of free discourse on Facebook and Twitter.
- Beau advises the audience to be prepared to move to other social media networks to continue accessing valuable information.
- Beau stresses the need to avoid comparing the censored outlets to Alex Jones, as the offenses were different.
- Beau underlines the significance of independent outlets reporting on government abuses and the risks associated with their censorship.
- Beau recommends following journalist Carrie Wedler and mentions specific alternative platforms like SteemIt and MeWe.

# Audience
Alternative news seekers

# On-the-ground actions from transcript
- Look into alternative social media platforms like SteemIt and MeWe for accessing uncensored news. (suggested)
- Follow journalist Carrie Wedler for reliable news updates. (exemplified)
- Share information from independent news outlets on alternative platforms to counter censorship. (implied)
- Prepare to transition to other social media networks for continued access to diverse news sources. (generated)

# Oneliner
Facebook and Twitter's censorship of independent news outlets signals the end of free discourse online, urging a shift to alternative platforms for uncensored information.

# Quotes
- "This is going to be the end of free discourse on Facebook."
- "You need to be looking for another social media network."
- "It's gonna happen with Facebook. Because this type of censorship, once it starts, it doesn't stop."

# Whats missing in summary
Exploration of the impact of censorship on diverse voices in the digital space.

# Tags
#Censorship #IndependentNews #AlternativePlatforms #FreeSpeech #SocialMedia

```