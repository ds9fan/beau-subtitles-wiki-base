```

# Bits

- Beau shares his experience with Hurricane Michael hitting his area, causing trees to fall, power outages, and issues with looting in the aftermath.
- He criticizes law enforcement response to looting and the impact it has on preventing volunteers from providing relief efforts.
- Beau reflects on media portrayal of looting after disasters, pointing out racial biases in coverage.
- He questions the priorities of law enforcement in protecting property over human lives during natural disasters.
- Beau praises the Florida National Guard for their quick response and assistance in the aftermath of the hurricane.
- He stresses the importance of individuals preparing emergency kits to be self-reliant during disasters, providing practical tips for assembling them.

# Audience
People in disaster-prone areas.

# On-the-ground actions from transcript
- Prepare an emergency kit with food, water, fire supplies, shelter, medical supplies, and a knife. (suggested)
- Organize your emergency supplies in one spot to access easily during a disaster. (exemplified)

# Oneliner
Beau shares insights on looting, government response, and the importance of individual preparedness during disasters.

# Quotes
- "In the event of a natural disaster, protecting the inventory of some large corporation should pretty much be at the bottom of any law enforcement agencies list."
- "Putting together a kit, a bag, for emergencies is something that every family should do."
- "We're just talking about enough to survive a week or two until things normalize."

# What's missing in summary
The detailed examples and personal anecdotes from Beau's experience.

# Tags
#HurricaneMichael #DisasterResponse #IndividualPreparedness #Looting #EmergencyKit #CommunityResponse
```