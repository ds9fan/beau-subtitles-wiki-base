```

# Bits

- Beau expresses disappointment in the President mocking sexual assault survivors on television, setting a low bar.
- He criticizes the President for leading efforts to silence sexual assault claimants.
- Beau questions the excuse of "Let" in justifying inappropriate behavior towards women.
- He contrasts the perceived lack of honor and integrity in President Trump with his view of President Obama.
- Beau argues against appointing individuals based solely on not being sexual assault perpetrators.
- He warns against dehumanizing perpetrators, drawing a parallel to Nazi Germany.
- The transcript ends with a reminder that dangerous people, like rapists and rape apologists, can be ordinary individuals in society.

# Audience

Advocates for survivors of sexual assault.

# On-the-ground actions from transcript

- Contact local advocacy groups to support survivors (exemplified)
- Share resources on consent and bystander intervention on social media (exemplified)
- Challenge dehumanizing rhetoric towards perpetrators in online comments (exemplified)

# Oneliner

Beau criticizes the President for mocking sexual assault survivors, warns against dehumanizing perpetrators, and draws parallels to Nazi Germany's normalization of evil.

# Quotes

- "There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim, and it's being led by the President of the United States."
- "Can't say the same thing about President Trump."
- "The scariest Nazi wasn't a monster. He was your neighbor."

# Whats missing in summary

The emotional impact and detailed examples can be best understood by watching the full video.

# Tags

#SexualAssault #PresidentTrump #Dehumanization #SurvivorsAdvocacy #NaziGermany #BystanderIntervention