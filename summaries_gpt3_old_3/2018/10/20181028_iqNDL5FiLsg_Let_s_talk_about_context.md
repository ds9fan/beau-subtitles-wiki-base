```

# Bits

- Beau talks about the importance of context in understanding sound bites, using Kennedy's speech as an example.
- Sound bites can be misleading when taken out of context, like in the case of Kennedy's speech where the real message was about the Soviet Union, not conspiracy theories.
- Beau points out that in today's world driven by short attention spans, context is often overlooked in favor of quick, attention-grabbing quotes.
- He criticizes the prevalence of sound bites in social media and news, where substance is often sacrificed for brevity and wit.
- Beau warns about the dangers of blindly accepting sound bites without looking at the bigger picture or considering the context.
- He delves into the situation in Syria, suggesting that the US and UK were involved in starting the civil war to oust Assad, possibly driven by interests in competing pipelines.
- Propaganda is a key theme throughout Beau's talk, with a call to always look beyond the sound bites and understand the interconnected interests at play in global events.

# Audience

Media consumers, critical thinkers.

# On-the-ground actions from transcript

- Research historical events and speeches to understand context (exemplified)
- Verify information beyond sound bites (exemplified)
- Read beyond headlines to grasp the full picture (exemplified)

# Oneliner

Beware of sound bites: Beau's insight on the importance of context in understanding media messages and global events.

# Quotes

- "Context, it's really  [...]  because you have to look beyond the sound bites."
- "If you want to understand what's going on in the world, you have to look beyond the sound bites."
- "You have to look at that bigger picture at all times."
- "Propaganda is coming into a golden age right now."
- "Most times the sound bite, just like in Kennedy's speech, it's the exact opposite of what's really being said and really being done."

# What's missing in summary

Deeper insights into specific examples of media manipulation and propaganda tactics could be better understood by watching the full video.

# Tags

#Context #SoundBites #Propaganda #CriticalThinking #GlobalEvents #MediaConsumption #Syria #SocialMedia #InformationVerification
```