```

# Bits

- Beau talks about when it's appropriate to call the police, mentioning that every law is backed up by the penalty of death.
- He gives examples like jaywalking, where a seemingly trivial offense can escalate to violence.
- Beau stresses that calling the police should only happen when death is an appropriate response to the situation.
- He cautions about the potential dangers that calling the police can pose, particularly for marginalized communities.
- Beau advocates for minding your own business unless there is a real threat of harm that warrants police intervention.
- He touches on the disparities in how different individuals, especially based on race, are treated by law enforcement.
- Beau concludes by warning about the real possibility of escalation and harm every time someone calls 911.

# Audience

Concerned citizens, community members.

# On-the-ground actions from transcript

- Question the necessity of involving the police in non-violent situations (implied)
- Advocate for de-escalation techniques in potential conflicts (exemplified)
- Refrain from calling law enforcement unless there is a genuine threat of harm (suggested)

# Oneliner

Beau stresses the gravity of police involvement, urging restraint in calling 911 unless death is warranted, especially in non-violent situations.

# Quotes

- "There is no law so insignificant, so trivial, so stupid, that a cop will not put a bullet in somebody over."
- "Every law, no matter how insignificant, is backed up by penalty of death."
- "Nobody's being harmed by these actions. Nobody's being hurt."
- "If nobody's being harmed, it's not really a crime."
- "Don't call the law unless death is an appropriate response because it's a very real possibility every time you dial 9-11."

# What's missing in summary

Detailed examples of situations where calling the police can escalate to violence.

# Tags

#Police #LawEnforcement #Deescalation #CommunitySafety #RacialJustice #911Awareness