```

# Bits

- Beau talks about offensive Halloween costumes and consequences.
- Being offensive is not a substitute for having a sense of humor.
- Reminisces about the past where offensive jokes were considered acceptable.
- Calls out the anonymity of the internet leading to inappropriate behavior in real life.
- Encourages being a good person over being edgy or shocking.
- Addresses the PC culture and the need for changing flawed thinking.
- Warns that actions today will be remembered and have consequences.

# Audience

Individuals, especially those considering offensive behavior or costumes.

# On-the-ground actions from transcript

- Refrain from wearing offensive costumes ( suggested )
- Choose to be a good person over being edgy or shocking ( exemplified )
- Challenge and change flawed thinking and bigotry ( exemplified )
- Be mindful of the consequences of your actions ( exemplified )

# Oneliner

Offensive Halloween costumes have consequences, be a good person over being edgy.

# Quotes

- "Being offensive and edgy is not actually a substitute for having a sense of humor."
- "Bigotry is out. It's something that sane people have given up on."
- "Nobody's going back to that. Nobody's going to think that's OK."

# Whats missing in summary

The emotional delivery and intonation of Beau's message.

# Tags

#Halloween #OffensiveCostumes #Consequences #PCCulture #ChangeInThinking #AnonymityOfInternet #BeAGoodPerson #Bigotry #Accountability
```