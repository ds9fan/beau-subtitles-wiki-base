```

# Bits

- Beau talks about the importance of not blindly trusting authority figures and instead encourages people to fact-check and think for themselves.
- He mentions how subtle the obedience to authority can be, using the example of checking sources on social media.
- Beau references the Milgram experiments from the 60s to illustrate how people can be swayed by authority figures to do harmful things.
- He stresses the importance of trusting oneself and thinking critically rather than relying solely on authority figures.
- Beau concludes by stating that ideas stand or fall on their own, reinforcing the need to trust facts and oneself.

# Audience
Individuals seeking to cultivate critical thinking skills.

# On-the-ground actions from transcript
- Fact-check information before sharing or believing it. (exemplified)
- Question authority and think critically about the information presented. (exemplified)
- Trust your own judgment and avoid blindly following authority figures. (exemplified)

# Oneliner
Beau advocates for critical thinking over blind trust in authority, urging individuals to fact-check, question, and trust themselves.

# Quotes
- "Don't trust your sources, trust your facts, and trust yourself."
- "I want you to think for yourself."
- "Ideas stand or fall on their own."

# What's missing in summary
The impact of blindly following authority figures and the consequences of not thinking critically.

# Tags
#CriticalThinking #FactChecking #QuestionAuthority #TrustYourself #MilgramExperiments

```