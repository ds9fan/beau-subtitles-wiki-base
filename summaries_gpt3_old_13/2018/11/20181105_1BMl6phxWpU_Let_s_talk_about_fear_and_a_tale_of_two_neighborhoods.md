# Quotes

- "Today I am the baby formula man."
- "That's what it boils down to now. It's fear. It's fear."
- "We're Americans, the sons of the pioneers. Are we anymore?"
- "You can't tell a kid to be a good person. We've got to show them."
- "If you got a problem in your community, fix it, do it yourself."

# Bits

Beau says:

- Beau describes running supplies to Panama City's hurricane impact zone, including taking his kids with him.
- In a rough neighborhood, Beau encounters gang members who help him distribute supplies efficiently.
- Beau contrasts this experience with encountering fear and hostility in a nicer neighborhood.
- He criticizes the culture of fear that inhibits community action and self-reliance.
- Beau encourages taking initiative at the local level to make a difference without waiting for government intervention.
- He stresses the importance of leading by example, especially when teaching children about empathy and community involvement.

# Oneliner

In a tale of two neighborhoods, Beau showcases the power of community over fear and the importance of taking local initiative.

# Audience

Community members

# On-the-ground actions from transcript

- Contact local organizations like Project Hope Incorporated to find ways to help in your community (exemplified)
- Organize supply drives or aid distribution efforts in non-government locations (exemplified)
- Take the initiative to address community needs without waiting for government intervention (exemplified)

# What's missing in summary

The full transcript provides a detailed narrative on the impact of fear, the importance of community action, and the need for self-reliance in addressing local issues.