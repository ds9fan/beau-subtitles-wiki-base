# Quotes
- "The suspension of habeas corpus is the end of the rule of law."
- "The suspension of habeas corpus is though, that is a threat to national security. It's a threat to your very way of life."
- "This migrant caravan, like those that have come for the last 15 years, these asylum seekers, this isn't a national emergency."
- "At this point you can support your country or you can support your president. Those are your options."
- "The suspension of habeas corpus is something that can't be tolerated."

# Bits
Beau says:
- President Kennedy allegedly met with selected Green Berets in 1961 and made them take an oath to fight back against a tyrannical government.
- Kennedy's honor guard included Green Berets after his assassination, a unique occurrence.
- A legend suggests that Green Berets placed their green berets on Kennedy's casket.
- A group of active-duty Green Berets visit Kennedy's grave site annually.
- Beau warns about the potential suspension of habeas corpus by the current president, linking it to a Kennedy conspiracy theory.
- Beau challenges the notion of suspending habeas corpus in the context of migrants seeking asylum.
- Beau urges viewers to understand the significance of habeas corpus and its potential removal.
- The suspension of habeas corpus is portrayed as a threat to national security and the rule of law.
- Beau stresses the importance of choosing between supporting the country or the president in such a critical situation.

# Oneliner
President Kennedy's alleged oath to Green Berets ties into a current concern about the suspension of habeas corpus, posing a threat to national security and the rule of law.

# Audience
Citizens, activists, concerned individuals

# On-the-ground actions from transcript
- Contact local representatives to express opposition to the suspension of habeas corpus (suggested)
- Join community organizations advocating for civil liberties and the rule of law (exemplified)

# Whats missing in summary
Full context of Kennedy conspiracy theory and the potential implications of suspending habeas corpus

# Tags
#KennedyConspiracy #GreenBerets #HabeasCorpus #NationalSecurity #CivilLiberties #CurrentEvents