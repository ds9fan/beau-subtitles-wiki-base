# Quotes
- "I said, Sergeant, take that hill."
- "You do not know the meaning of the word blue falcon until you're talking about a senior commander trying to cover his rear and the collateral damage of a bunch of enlisted guys."
- "Honor, integrity, those things. It's bred into you, right?"
- "Don't do it. When that vague order comes down, you ask for clarification."
- "It's your uniform, your honor being used to legitimize them circumventing the law."

# Bits
Beau says:

- Recounts a story about a general during the Tet Offensive in 1968 who successfully takes a hill with no casualties by giving a vague order that gets more specific as it reaches the soldiers on the ground.
- Talks about the unique chance for soldiers at the border to defend American freedom by following legal and moral guidelines.
- Warns against mission creep, using Afghanistan as an example of how vague orders can lead to extended military presence without clear objectives.
- Advises soldiers to keep their weapons slung and on safe, follow the laws of war and real rules of engagement, especially when dealing with non-combatants.
- Urges soldiers to seek clarification on vague orders, avoid illegal actions, and not allow themselves to be used as political tools by superiors.

# Oneliner
Soldiers urged to uphold honor, integrity, and legality by seeking clarification on vague orders and avoiding illegal actions that could compromise their values.

# Audience
Soldiers

# On-the-ground actions from transcript
- Seek clarification on vague orders (suggested)
- Keep weapons slung and on safe (implied)
- Follow the laws of war and rules of engagement (implied)

# Whats missing in summary
The full transcript delves into the importance of legal and moral conduct for soldiers, warning against mission creep and being used as political tools.

# Tags
#Soldiers #MissionCreep #LegalConduct #Clarification #AmericanFreedom