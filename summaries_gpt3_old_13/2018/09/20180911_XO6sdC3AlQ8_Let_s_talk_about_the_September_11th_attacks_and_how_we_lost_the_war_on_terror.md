# Quotes
- "We can't let this become normal."
- "The more self-reliant you are, the less dependent on government you are."
- "You have to surround yourself with other people who are in it."
- "Your community is strong enough, what happens in DC doesn't matter because you can ignore it."
- "We're going to have to defeat a government that is very overreaching by ignoring it."

# Bits
Beau says:
- Recalls the events of September 11th, reflecting on the impact and aftermath of the tragic day.
- Criticizes the erosion of freedoms post-9/11 due to government actions and the public's response.
- Explains terrorism's strategic nature in provoking overreactions from governments and citizens.
- Advocates for reclaiming freedom through community-level actions and self-reliance.
- Urges viewers to focus on educating children, preparing for emergencies, and engaging in counter-economics.
- Stresses the importance of surrounding oneself with like-minded individuals and supporting those in need.
- Encourages viewers to emulate the U.S. Army Special Forces in becoming force multipliers for community building.
- Emphasizes the necessity of ignoring an overreaching government to combat tyranny effectively.

# Oneliner
We must reclaim freedom by strengthening communities, educating children, fostering self-reliance, and resisting government overreach.

# Audience
Community members

# On-the-ground actions from transcript
- Teach children about the importance of freedom and the dangers of government overreach (suggested).
- Prepare for emergencies and natural disasters by becoming self-reliant and capable of community aid (suggested).
- Engage in counter-economics by supporting local markets, bartering, and reducing reliance on government-backed currency (suggested).
- Surround yourself with like-minded individuals who prioritize freedom and mutual support (suggested).
- Support and uplift marginalized individuals who understand the loss of freedom and may be willing to get involved (suggested).
- Initiate community-building efforts to strengthen local networks and resilience against government overreach (suggested).

# What's missing in summary
The full transcript provides a detailed analysis of the erosion of freedoms post-9/11, the strategic nature of terrorism, and practical steps individuals can take to combat government overreach and reclaim freedom through community action.

# Tags
#September11 #Freedom #CommunityAction #SelfReliance #Terrorism #GovernmentOverreach