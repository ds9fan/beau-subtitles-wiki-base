# Quotes
- "Patriotism is correcting your government when it is wrong."
- "Patriotism is displayed through actions, not worship of symbols."
- "You can kiss my patriotic behind."

# Bits
Beau says:
- Responding to a story about a woman attacked for kneeling during the national anthem.
- Questions the author's understanding of patriotism and purpose of protest.
- Criticizes the author for attacking a woman exercising her constitutionally protected rights.
- Shares a story about patriotism being displayed through actions, not symbols.
- Encourages the author to learn from a civilian who served the country for 30 years.
- Draws a distinction between nationalism and patriotism.
- Concludes by asserting his own patriotic stance.

# Oneliner
Beau challenges misconceptions on patriotism and defends a woman's right to protest, advocating for actions over symbols.

# Audience
Activists, Patriots, Community Members

# On-the-ground actions from transcript
- Contact Beau to connect with the civilian who served the country for 30 years (suggested).
- Engage in actions that display patriotism through meaningful deeds rather than mere symbolism (exemplified).

# Whats missing in summary
Full understanding of the nuances between nationalism and patriotism, and the importance of actions over symbols in displaying true patriotism.

# Tags
#Patriotism #Protest #NationalAnthem #Community #Action #CivilRights #Misconceptions #Activism