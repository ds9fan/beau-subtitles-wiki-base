# Quotes
- "There's two kinds of liars in the world. There's a bad liar, and a bad liar you gotta ask him that question over and over and over again to finally get to the truth."
- "When she resorted to violence, assaulted him, it's a home invasion."
- "If you kill someone during a home invasion in Texas? You're facing capital murder. Capital murder."

# Bits
Beau says:

- Beau questions the official narrative of a shooting in Dallas, pointing out inconsistencies in the story.
- He suggests that the officer may have had a motive due to prior noise complaints filed against the victim.
- Beau raises concerns about the legality of the officer's actions, likening it to a home invasion.
- He mentions the lack of accountability within the police force and criticizes the lack of opposition to such actions.
- Beau expresses his disappointment in the current state of law enforcement and the erosion of public trust.

# Oneliner
Beau questions the official story of a shooting in Dallas, suggesting a possible motive, and criticizes the lack of accountability within law enforcement.

# Audience
Advocates for police accountability

# On-the-ground actions from transcript
- Challenge police corruption (implied)
- Advocate for justice for victims of police violence (implied)
- Support efforts to increase police accountability (implied)

# Whats missing in summary
Full context and emotional impact of Beau's analysis

# Tags
#PoliceAccountability #DallasShooting #HomeInvasion #Justice #BlackLivesMatter