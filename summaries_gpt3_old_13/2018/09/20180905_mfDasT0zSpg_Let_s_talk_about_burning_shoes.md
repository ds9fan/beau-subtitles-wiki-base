# Quotes

- "Take them and drop them off at a shelter. Maybe give them to a homeless vet, you know, those people you pretend you care about."
- "You're just mad at them. It's a symbol, right? You're just mad at them. You don't really care about the worker."
- "It's one of those things. You're loving that symbol of freedom more than you love freedom."

# Bits

Beau says:

- Beau wants to burn some shoes, but not because he hates the company. He questions the symbolic act of burning shoes and suggests donating them instead.
- He challenges those who are quick to disassociate with a company over a commercial but overlook the unethical practices the company has been built on.
- Beau points out the hypocrisy of valuing symbols over the actual causes they represent, like freedom and workers' rights.
- He draws parallels between burning shoes as a symbol of protest and burning the American flag for attention.
- Beau questions the sincerity of those who claim to care about certain issues but ignore larger systemic problems.

# Oneliner

Burning shoes as a symbol of protest raises questions about true intentions and actions towards real causes, not just symbols.

# Audience

Consumers, Activists, Advocates

# On-the-ground actions from transcript

- Donate unwanted shoes to shelters or homeless veterans (suggested)
- Support thrift stores near military bases by donating clothes and shoes (suggested)

# Whats missing in summary

The full transcript provides a deeper dive into the nuances of performative activism and challenges individuals to reexamine their actions and values beyond symbolic gestures.

# Tags

#Activism #PerformativeActivism #EthicalConsumption #ConsumerResponsibility #SocialJustice