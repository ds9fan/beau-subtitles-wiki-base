# Quotes

- "The intent of the Second Amendment is to strike back against government tyranny."
- "A Second Amendment supporter wants minorities to have firearms."
- "If everybody's armed, it breaks up the government's monopoly on violence."
- "The Second Amendment came into play to get the Civil Rights Act passed."
- "The Second Amendment is there to protect them."

# Bits

Beau says:

- Gun control and race are intertwined due to muddled talking points and misconceptions.
- Second Amendment supporters advocate for arming minorities to protect against government tyranny.
- There is a divide between Second Amendment supporters and gun enthusiasts.
- Violent rhetoric within the gun community often deals with race.
- The difference in philosophy between Second Amendment supporters and gun nuts is about decentralizing power vs. might makes right.
- Gun control measures can unintentionally target minority groups.
- Historical trends show a racial component in gun control, but it's less prevalent today.
- Second Amendment supporters aim to ensure minorities are armed to protect their rights.
- The intent of the Second Amendment is to arm minority groups against oppression.

# Oneliner

Gun control, race, and the Second Amendment intersect, with supporters advocating for minority firearm ownership to combat tyranny and oppression.

# Audience

Advocates, activists, educators

# On-the-ground actions from transcript

- Contact local gun stores for free training programs for minority and marginalized groups (exemplified)
- Advocate for affordable access to firearms for low-income families (exemplified)
- Join organizations promoting firearm ownership for minorities (exemplified)

# What's missing in summary

Deeper exploration of the historical connection between gun control and racism, and the importance of understanding the intent of the Second Amendment in protecting marginalized groups.

# Tags

#GunControl #Race #SecondAmendment #Minorities #Tyranny #CommunityPolicing