# Quotes
- "If you laughed at that, Probably the crazy cousin."
- "They're accountable. You can hold them accountable."
- "We've got to start talking to each other. We got the same problems."
- "It's unaccountable men with guns. We can work together and we can solve that."
- "Get involved, we got to start working together."

# Bits
Beau says:
- Explains how in rural areas, law enforcement, like the sheriff and deputies, are known, accountable, and part of the community.
- Contrasts this with minority communities lacking institutional power and facing unaccountable men with guns in law enforcement.
- Mentions the distrust and fear towards agencies like ATF and BLM due to their unaccountability and past actions.
- Urges people, especially in cities, to get involved, vote out corrupt officials, and demand accountability.
- Emphasizes the need for different communities to come together against the common issue of unaccountable men with guns.

# Oneliner
In rural areas, law enforcement is accountable and known, unlike the unaccountable men with guns in minority communities, urging unity to address common issues.

# Audience
Communities

# On-the-ground actions from transcript
- Vote out corrupt officials and demand accountability (exemplified)
- Start talking to different communities and work together against unaccountable men with guns (exemplified)

# Whats missing in summary
The full transcript delves deeper into the disparities between rural and minority communities' relationships with law enforcement, illustrating the importance of accountability and community unity in addressing systemic issues.

# Tags
#CommunityPolicing #Accountability #Unity #InstitutionalPower #Justice