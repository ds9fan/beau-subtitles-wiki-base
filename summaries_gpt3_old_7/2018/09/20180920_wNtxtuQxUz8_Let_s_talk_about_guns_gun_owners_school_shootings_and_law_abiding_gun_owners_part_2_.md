```

# Bits

Beau says:

- Explains the failure of gun control written by individuals without knowledge of firearms.
- Breaks down the flaws in banning assault weapons due to the vague definition of "assault weapon."
- Points out the ineffectiveness of banning high-capacity magazines in preventing mass shootings.
- Debunks the idea of banning AR-15s and semi-automatic rifles, citing the availability and ease of making such firearms.
- Compares the cost and accessibility of legal versus illegal acquisition of firearms in the context of bans.
- Advocates for raising the age to 21 for purchasing firearms as a practical solution to address school shootings.
- Addresses the loophole in gun laws related to domestic violence convictions and the need to close it.

# Audience

Gun policy advocates.

# On-the-ground actions from transcript

- Raise the age requirement for purchasing firearms from 18 to 21 (suggested).
- Close the loophole in domestic violence laws related to gun ownership (implied).

# Oneliner

Beau dismantles common gun control strategies and proposes a simple solution to address school shootings: raising the age to purchase firearms to 21.

# Quotes

- "A soldier can enlist in the Army, overseas fighting a war, come home and not be able to buy a gun. You got your priorities mixed up."
- "Legislation is not the answer here in any way shape or form."
- "When you make something illegal like that, you're just creating a black market."

# What's missing in summary

In-depth analysis on comprehensive solutions beyond legislative measures.

# Tags

#GunControl #Firearms #SchoolShootings #Policy #DomesticViolence

```