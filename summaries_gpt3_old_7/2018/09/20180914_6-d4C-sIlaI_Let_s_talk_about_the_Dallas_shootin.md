```

# Bits

Beau says:

- Questions the official narrative of the shooting in Dallas, discussing the inconsistencies in the story.
- Speculates on a possible motive for the shooting based on the officer's past noise complaints against the victim.
- Points out that even if the door was open, entering someone's home without permission and resorting to violence constitutes a home invasion.
- Raises concerns about the potential facing of capital murder charges due to the circumstances of the shooting.
- Shares a personal anecdote about a thought-provoking question on Black Lives Matter.
- Expresses disbelief at the police searching the victim's home post-shooting to find justification for the killing.
- Condemns the lack of accountability within the Dallas PD and criticizes the culture of covering up for fellow officers.
- Addresses the loss of public trust in law enforcement, attributing it to corruption and unjust actions within the system.

# Audience

Community members, activists, advocates.

# On-the-ground actions from transcript

- Challenge police misconduct by demanding accountability within your community (implied).
- Advocate for transparency and justice in cases of police violence (implied).
- Support movements that seek to address systemic issues within law enforcement (implied).

# Oneliner

Beau questions the official narrative of a shooting in Dallas, revealing potential motives and criticizing police actions, urging for justice and accountability.

# Quotes

- "If this is the amount of justice they get, they don't."
- "It's a few bad apples spoils the bunch and that certainly appears to have happened in Dallas."
- "You're trying to slander and villainize an unarmed man that was gunned down in his home."

# What's missing in summary

Deeper insights into police accountability and systemic issues in law enforcement.

# Tags

#PoliceViolence #BlackLivesMatter #Accountability #Justice #SystemicIssues #CommunityActivism
```