```

# Bits

Beau says:

- Beau expresses the importance of burning something due to rain, but not actually burning anything.
- He talks about not owning Nikes himself but his son having a pair.
- Beau questions the idea of burning Nikes in protest, suggesting donating them to those in need instead.
- He brings up the issue of sweatshops and slave labor, questioning the true motivation behind burning the shoes.
- Beau draws parallels between burning Nikes and burning the American flag, both being seen as symbols.
- He challenges those who are willing to disassociate with Nike over a commercial but overlook their history of using sweatshops and slave labor.
- Beau implies that some prioritize symbols of freedom over actual freedom itself.

# Audience

Consumers, activists.

# On-the-ground actions from transcript

- Donate unused items to shelters or homeless veterans (suggested).
- Give clothes to thrift stores near military bases for enlisted personnel (implied).

# Oneliner

Beau questions the true meaning behind burning Nikes in protest and challenges prioritizing symbols over real issues like sweatshops and freedom.

# Quotes

- "You're loving that symbol of freedom more than you love freedom."
- "Maybe give them to a homeless vet, you know, those people you pretend you care about."
- "It's a symbol. You're just mad at them."

# What's missing in summary

Deeper insights on consumer behavior and activism.

# Tags

#Nike #ConsumerActivism #Symbolism #Freedom #Sweatshops
```