```

# Bits

Beau says:

- Addresses the #MeToo movement and sexual harassment in light of the Kavanaugh nomination.
- Provides advice on how to avoid getting into situations of sexual harassment.
- Compares safeguarding one's reputation to locking a bicycle to prevent theft.
- Advises to end dates at the front door to prioritize quality over quantity.
- Warns against taking someone home or going home with someone when drinking at parties.
- Stresses the importance of watching what you say to avoid sending unintentional signals.
- Urges keeping hands to yourself to prevent misinterpretation of signals.
- Mentions the "him-too" hashtag and addresses men's fear of false accusations of sexual harassment.
- States that accusations of sexual harassment are true about 98% of the time.
- Argues against victim-blaming based on clothing or behavior in cases of assault.
- Concludes with a powerful statement that rapists are the sole cause of rape.

# Audience

Men concerned about avoiding accusations of sexual harassment.

# On-the-ground actions from transcript

- Be mindful of your reputation and the signals you send (implied).
- End dates at the front door for safety (implied).
- Avoid taking or going home with someone when drinking (implied).
- Watch what you say to prevent misunderstanding (implied).
- Keep hands to yourself to avoid sending wrong signals (implied).

# Oneliner

Beau gives advice on avoiding sexual harassment situations, comparing safeguarding reputation to locking a bicycle and stressing accountability for actions.

# Quotes

- "There is one cause of rape, and that's rapists."
- "Gentlemen, there is one cause of rape, and that's rapists."
- "If you're that worried about it, it's because you've done something."
- "The miniskirt said, am I really to blame? And the hijab said, no, it happened to me too."
- "Don't put yourself in a situation where you can be falsely accused 2% of the time."

# Whats missing in summary

In-depth analysis on the impact of reputation, behavior, and accountability in preventing sexual harassment.

# Tags

#MeToo #SexualHarassment #Accountability #VictimBlaming #RapeCulture
```