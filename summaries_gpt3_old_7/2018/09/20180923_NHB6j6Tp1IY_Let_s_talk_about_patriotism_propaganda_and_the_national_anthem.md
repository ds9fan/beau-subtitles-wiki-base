```

# Bits

Beau says:

- Beau responds to a story about a woman attacked for kneeling during the national anthem at a county fair.
- He questions the narrative presented by the author and challenges the notion of patriotism.
- Beau criticizes the author's lack of understanding of the purpose of protests and the principles of patriotism.
- He contrasts nationalism with patriotism, advocating for correcting the government when it's wrong.
- Beau shares a story about patriotism being displayed through actions, not symbols.
- He suggests that true patriotism involves loyalty to the community and defending constitutionally protected rights.
- Beau references a civilian who served the country for 30 years, indicating that true patriotism is more than just symbols.
- The video serves as a teaching tool to differentiate between nationalism and patriotism.
- Beau ends the response by asserting his own view on patriotism and expressing his defiance towards the author's stance.

# Audience

People interested in patriotism and social justice advocacy.

# On-the-ground actions from transcript

- Contact Beau to further understand his perspective on patriotism (suggested).
- Reach out to individuals with diverse experiences to gain a broader understanding of patriotism (exemplified).

# Oneliner

Beau challenges misconceptions about patriotism and nationalism, advocating for actions over symbols in a thought-provoking response.

# Quotes

- "Patriotism is correcting your government when it is wrong."
- "You know, why aren't you wearing an American flag lapel pin? Because he believed that patriotism was displayed through actions, not worship of symbols."
- "There is a very marked difference between nationalism and patriotism."
- "Sir, patriotism is not doing what the government tells you to."
- "You can kiss my patriotic behind."

# What's missing in summary

Beau's passionate delivery and nuanced explanations on the distinctions between nationalism and patriotism.

# Tags

#Patriotism #Nationalism #SocialJustice #Community #Activism

```