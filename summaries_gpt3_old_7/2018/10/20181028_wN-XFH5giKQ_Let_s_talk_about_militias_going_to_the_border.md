```

# Bits

Beau Ginn says:

- Called to the Mexican border by militia commander to "protect America."
- Not physically stopping refugees on Mexican side to avoid violating laws.
- Not taking action on American side to uphold U.S. laws and core principles.
- Backing up DHS, despite being labeled potential terrorists/extremists.
- Concerned about revealing militia operations if rhetoric turned into action.

# Audience

Border activists and individuals interested in militia activities.

# On-the-ground actions from transcript

- Coordinate with community organizations to support asylum seekers (suggested).
- Join or support local border advocacy groups (suggested).

# Oneliner

Militia member on Mexican border weighs legal and ethical dilemmas in "protecting America" with guns against unarmed asylum seekers.

# Quotes

- "We wouldn't do that unless we were traitors, so we're not going to do that."
- "They're trying to make sure that the optics are good, we look good, because we're down here on the border protecting America with our guns."
- "Ready to kill a bunch of unarmed asylum seekers who are following the law."

# What's missing in summary

Insights on the potential consequences of militia actions and the complexities of border protection.

# Tags

#Militia #BorderSecurity #Ethics #AsylumSeekers #DHS

```