```

# Bits

Beau says:

- Received a message from a young lady who can't look her father in the eye due to his comments on sexual assault.
- Explains the impact of men's comments and attitudes on their relationships with women.
- Addresses common arguments used to discredit sexual assault victims.
- Points out the damaging effects of justifying or excusing assault.
- Calls for men to have honest and critical dialogues with the women in their lives.
- Urges men to apologize for harmful comments rather than justify them.
- Condemns the culture that discredits assault victims and protects perpetrators.
- Stresses the importance of believing and supporting survivors unconditionally.
- Raises awareness about the prevalence of sexual assault and the need for accountability.
- Advocates for a shift in societal attitudes towards assault and victim blaming.

# Audience

Men, fathers, individuals reflecting on their attitudes towards sexual assault survivors.

# On-the-ground actions from transcript

- Have a candid and honest talk with the women in your life about your comments and justifications (suggested).
- Apologize sincerely for harmful remarks rather than trying to justify them (suggested).

# Oneliner

Men must confront harmful attitudes on sexual assault and apologize for damaging relationships, instead of justifying harmful comments.

# Quotes

- "One out of five, gentlemen, one out of five women that heard you talk have been sexually assaulted."
- "You don't get to rape her. I mean, this is a pretty simple concept."
- "I think a lot of men probably need to sit down and talk to the women in their lives and try to explain some of their comments and some of their justifications for it."

# Whats missing in summary

The emotional delivery and passion conveyed through Beau's message.

# Tags

#SexualAssault #Men #Relationships #Accountability #BelieveSurvivors #Apologize #SocialChange
```