```

# Bits

Beau says:

- Beau toasts Supreme Court Justice Kavanaugh's ascension, celebrating that his life wasn't ruined.
- He sarcastically mentions being worried Kavanaugh might have to mine coal if not appointed.
- Beau expresses relief that America will now be safe and free with Kavanaugh on the Supreme Court.
- He comments on Kavanaugh not being tied down by things like jury trials or lawyers before detaining someone.
- Beau sarcastically praises the government's efficiency and lack of mistakes, especially with the NSA gathering information without warrants.
- He humorously praises local cops for being able to stop people for no reason, physically grab them, and ask for identification without fear of abuse.
- Beau sarcastically downplays concerns about abuse by the government and asserts that giving up rights is worth it for safety.
- He mocks the lack of research into Kavanaugh's rulings and celebrates defeating feminists and leftists by appointing him to the Supreme Court.
- Beau acknowledges the sacrifices made in exchange for Kavanaugh's appointment, joking about selling out the country but feeling victorious over the #MeToo movement.

# Audience

Individuals concerned about civil rights and judicial appointments.

# On-the-ground actions from transcript

- Organize a community forum to critically analyze judicial appointments (suggested).
- Join or support organizations advocating for civil liberties and judicial accountability (exemplified).

# Oneliner

Beau sarcastically celebrates Kavanaugh's Supreme Court appointment, prioritizing safety and freedom over rights and due process.

# Quotes

- "Congratulations, I'm very glad that your life wasn't ruined."
- "It's gonna keep us free. It's gonna keep us safe."
- "We're gonna be free now. Okay, so yeah, we had to give up some rights, and sell out our country..."

# Whats missing in summary

Deeper insight into the implications of prioritizing safety over civil rights in judicial appointments.

# Tags

#SupremeCourt #JusticeKavanaugh #CivilRights #JudicialAccountability #Satire

```