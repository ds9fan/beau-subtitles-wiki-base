```

# Bits

Beau says:

- Beau addresses the comments on his video about sexual assault and Kavanaugh, particularly focusing on the presumption of innocence.
- He points out that the presumption of innocence is a cornerstone of the American justice system.
- Beau criticizes Kavanaugh's support for actions like stop-and-frisk, warrantless searches by the NSA, and indefinite detention without trial.
- He questions the sudden defense of Kavanaugh based on the presumption of innocence, especially by those unfamiliar with his previous rulings.
- Beau warns about the dangers of party politics leading to the erosion of rights and the potential consequences of granting excessive executive power.
- He cautions against blindly supporting political figures without considering their impact on constitutional principles and individual rights.
- Beau suggests that supporting Kavanaugh solely based on party affiliation is detrimental to the country's well-being and undermines the integrity of the Supreme Court.
- He encourages critical thinking and awareness of political rhetoric that may prioritize loyalty over principles and values.
- Beau challenges individuals to question their allegiance to certain political figures and ideologies that may compromise fundamental rights and freedoms.
- He concludes by urging viewers to be vigilant, informed, and thoughtful in their political beliefs and actions.

# Audience

Viewers, voters, political activists

# On-the-ground actions from transcript

- Challenge political rhetoric in your community (suggested)
- Stay informed about political candidates' stances on constitutional rights (implied)
- Advocate for judicial nominees who uphold fundamental rights (implied)

# Oneliner

Beau warns against blind party loyalty compromising constitutional values, urging critical thinking in political support decisions.

# Quotes

- "You've bought into bumper sticker politics and you're trading away your country for a red hat."
- "Don't expect anybody who knows anything about this man's rulings or knows anything about the Constitution, knows anything about the Supreme Court to believe a word you say."

# What's missing in summary

Full nuance and depth of Beau's commentary on the intersection of party politics, constitutional rights, and judicial integrity.

# Tags

#PresumptionOfInnocence #Kavanaugh #ConstitutionalRights #PartyPolitics #CriticalThinking #SupremeCourt
```