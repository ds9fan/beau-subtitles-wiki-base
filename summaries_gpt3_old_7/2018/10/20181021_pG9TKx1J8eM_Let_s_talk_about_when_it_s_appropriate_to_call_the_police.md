```

# Bits

Beau says:

- Every law, no matter how insignificant, is backed up by the penalty of death.
- Barbecue Becky, Permit Patty, or Keyfob Katie putting lives at risk by calling the police.
- When nobody is being harmed, it's not really a crime, even if it's illegal.
- Only call the cops when death is an appropriate response to the situation.
- Strong fences make good neighbors; mind your business if nobody is being harmed.
- Refusing to comply with the police can lead to violence, even over minor infractions.
- The risk of escalation and violence is high when police are called unnecessarily.
- Patience needed to deal with police differs greatly based on race.
- The potential danger of calling the police, especially for people of color.
- Compliance with the law doesn't necessarily equate to moral behavior.
- The importance of considering the potential consequences of involving law enforcement.
- Being aware of the disproportionate responses people of color may face from law enforcement.
- The reality that every call to 911 carries the risk of violence and death.
- Acknowledging personal limitations and understanding the implications of escalating situations.
- Cautioning against involving law enforcement unless absolutely necessary to prevent harm.

# Audience

Community members, bystanders, individuals.

# On-the-ground actions from transcript

- Be cautious about involving law enforcement unless there is a threat of harm (implied).
- Advocate for community-based solutions to conflicts and disputes (implied).
- Refrain from unnecessarily escalating situations by involving the police (implied).

# Oneliner

Every call to the cops carries the threat of violence; only dial 911 when death is an appropriate response.

# Quotes

- "Every law, no matter how insignificant, is backed up by the penalty of death."
- "Nobody's being harmed by these actions. Nobody's being hurt. And that's really the only time you should call the law."
- "Compliance with the law doesn't necessarily equate to moral behavior."

# What's missing in summary

Nuances of Beau's storytelling and delivery.

# Tags

#Police #LawEnforcement #CommunitySafety #RacialJustice #ConflictResolution
```