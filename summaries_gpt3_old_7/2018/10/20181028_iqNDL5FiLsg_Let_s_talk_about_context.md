```

# Bits

Beau says:

- Explains the importance of context in sound bites using Kennedy's speech as an example.
- Mentions the misinterpretation of Kennedy's speech due to focusing on sound bites without context.
- Talks about the dangerous impact of social media and shortened attention spans on understanding complex issues.
- Gives an example of U.S. involvement in Syria, starting from a Civil War and the hidden motives behind it.
- Warns about the rise of propaganda and the need to look beyond the surface to understand global events.
- Stresses the interconnectedness of the world through communication and business.

# Audience

Viewers interested in understanding the impact of sound bites and the importance of context in media and global events.

# On-the-ground actions from transcript

- Analyze news sources critically (suggested).
- Research beyond headlines (implied).

# Oneliner

Context matters: Beau explains the dangers of sound bites in media, urging viewers to look deeper for truth in a world dominated by quick information.

# Quotes

- "Context, it's really  importan‍t."
- "Propaganda is coming into a golden age right now."
- "You have to look beyond the sound bites."
- "Most times, the sound bite is the exact opposite of what's really being said."
- "Taking a whole bunch of information and getting down to what's actually  impor‍tant."

# What's missing in summary

Deeper insights on how propaganda influences public perception and the need for thorough analysis beyond surface-level information.

# Tags

#ContextMatters #MediaLiteracy #Propaganda #GlobalEvents #CriticalThinking
```