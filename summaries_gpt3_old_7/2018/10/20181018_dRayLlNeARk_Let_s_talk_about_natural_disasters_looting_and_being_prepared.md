```

# Bits

Beau says:

- Beau returns after being absent due to Hurricane Michael hitting at a strength of four unexpectedly.
- Comments on the aftermath of the hurricane with trees falling, power loss, but assures his safety and wellbeing of others.
- Addresses the issue of looting after natural disasters and the problematic law enforcement response.
- Mentions how media coverage of looting post-Katrina had racial biases in portraying different families looting vs. finding food.
- Questions the priorities of law enforcement in shutting down areas due to looting threat, prioritizing property over lives.
- Praises the Florida National Guard for their quick response and aid efforts during the disaster.
- Advocates for individual preparedness by putting together emergency kits with essentials like food, water, fire, shelter, medical supplies, and a knife.
- Urges viewers to take the time to assemble these emergency kits to be better prepared for unforeseen disasters.

# Audience
Community members in disaster-prone areas

# On-the-ground actions from transcript

- Put together an emergency kit with essentials like food, water, fire, shelter, medical supplies, and a knife (suggested).
- Ensure to have a couple of weeks' supply of daily medications in the emergency kit for family members who need them (suggested).

# Oneliner
After Hurricane Michael, Beau urges individual preparedness through assembling emergency kits with essentials like food, water, and medical supplies to counteract unreliable government responses in disasters.

# Quotes
- "In the event of a natural disaster, protecting the inventory of some large corporation should pretty much be at the bottom of any law enforcement agencies list."
- "It's not a big process. We're not talking about building a bunker and preparing for doomsday. We're just talking about enough to survive a week or two until things normalize."
- "Take the time. It doesn't take long. It's not a big process. We're not talking about building a bunker and preparing for doomsday."

# Whats missing in summary
The full transcript provides a detailed insight into the challenges faced post-natural disasters and the importance of individual preparedness through assembling emergency kits.

# Tags
#HurricaneMichael #NaturalDisasters #EmergencyPreparedness #Looting #CommunityResponse

```