```

# Bits

Beau says:

- President cutting off aid to Central America for not stopping people from coming to the US is scary.
- President believes it's his job to keep people in the US, hinting at potential tyranny.
- The wall along the southern border could be used to keep Americans in too.
- Warning about the slow creep of tyranny and loss of freedom.
- Describes the wall as a prison wall, not just a border wall.
- Calling out those who support the President's actions as betraying ideals of freedom and liberty.
- Expressing concern about the smiling face of tyranny in current events.
- This situation marks a betrayal of country for a mere slogan and symbol.
- Urges people to think about the implications of not being able to leave the US.
- Describing the situation as terrifying and a clear sign of potential danger.

# Audience

US citizens, patriots, activists.

# On-the-ground actions from transcript

- Contact local representatives to express concerns about presidential actions (implied).

# Oneliner

The President's actions hint at potential tyranny, with the border wall posing a threat to freedom by potentially keeping Americans in. Betraying ideals for slogans is a dangerous path.

# Quotes

- "That wall is a prison wall. It's gonna work both ways."
- "If you've chosen to, you've sold out your ideals and your principles and your country for a red hat and a cute slogan."

# Whats missing in summary

The full video provides a deeper exploration of the implications of the President's actions and the potential threats to freedom and liberty.

# Tags

#Tyranny #Freedom #BorderWall #CentralAmerica #AidCut #USPresident #Patriotism #Warning #Activism
```