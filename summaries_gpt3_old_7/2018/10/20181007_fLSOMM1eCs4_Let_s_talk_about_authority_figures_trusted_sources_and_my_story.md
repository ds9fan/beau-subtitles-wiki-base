```

# Bits

Beau says:

- People want to know his backstory to determine how much trust to put in what he says.
- Beau doesn't want people to blindly trust authority figures, including himself.
- He warns against blind obedience to authority and submission to authority.
- Mention of people sharing false information on social media and the importance of checking sources.
- Beau advises to run facts through filters - fact, logic, moral, and ethical.
- Reference to historical events like the Iraq war due to blind trust in authority without fact-checking.
- Beau mentions Milgram's experiments showing people's tendency to follow authority figures' instructions, even to harm others.
- Encouragement to trust oneself and not just rely on authority figures.
- Beau values independent thinking over blind agreement with his views.
- Ideas should stand or fall based on their own merit, not the authority behind them.

# Audience

Viewers of online content.

# On-the-ground actions from transcript

- Fact-check information before sharing (implied).
- Encourage critical thinking and independent research (implied).

# Oneliner

Beau warns against blind obedience to authority and encourages independent thinking and fact-checking in a world of misinformation.

# Quotes

- "Don't trust your sources, trust your facts, and trust yourself."
- "Ideas stand or fall on their own."

# Whats missing in summary

Deeper insights on the importance of critical thinking and questioning authority.

# Tags

#Trust #CriticalThinking #Authority #FactChecking #IndependentThinking #Misinformation

```