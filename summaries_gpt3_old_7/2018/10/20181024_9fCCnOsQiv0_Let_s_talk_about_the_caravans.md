```

# Bits

Beau says:

- Caravans have been coming for 15 years due to US drug war policy and interventions in other countries.
- The US played a role in legitimizing a coup in Honduras, contributing to the violence people are fleeing.
- The murder rate in San Pedro Sula, where the caravan originated, is much higher than in cities like Chicago.
- Tax dollars, specifically through the Office of Refugee Resettlement, fund assistance for these migrants.
- It's cheaper to help migrants get on their feet than to detain them, as seen in the comparison of costs.
- Migrants travel in large groups for safety, similar to kids trick-or-treating in numbers.
- Carrying other countries' flags and wearing military fatigues is about solidarity and practicality, not conspiracy.
- Seeking asylum at a port of entry is the legal way to claim asylum in the US.
- Blaming migrants for seeking asylum is misguided; addressing foreign policy and drug war impacts is key.
- Sending people back to dangerous situations goes against US values of freedom and humanity.

# Audience

Policy influencers, advocates, concerned citizens

# On-the-ground actions from transcript

- Advocate for changes in foreign policy and drug war strategies to address root causes of migration (suggested)
- Contact representatives to push for humane treatment of asylum seekers (exemplified)
- Raise awareness about the realities of migration and asylum processes (implied)

# Oneliner

Caravans have been coming for 15 years due to US policies; addressing root causes and treating asylum seekers humanely are imperative.

# Quotes

- "It's cheaper to be a good person."
- "Blaming the victim is not a solution."
- "You can't preach freedom and then deny it."

# Whats missing in summary

Deeper insights on the complexities of migration, asylum processes, and the impact of US policies on Central American countries.

# Tags

#USPolicies #Migration #AsylumSeekers #Humanitarianism #ForeignPolicy
```