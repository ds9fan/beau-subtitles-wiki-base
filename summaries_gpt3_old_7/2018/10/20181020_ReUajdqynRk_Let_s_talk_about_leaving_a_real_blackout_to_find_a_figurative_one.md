```

# Bits

Beau says:

- Beau expresses the need to get out of the house, away from noise, and mentions driving to escape.
- Upon gaining internet connection, Beau discovers that Facebook and Twitter have censored independent news outlets opposing government policies, finding it terrifying.
- Beau distinguishes these outlets from Alex Jones, noting the severity of Jones' content compared to the dissenting news outlets.
- He reveals personal connections with individuals working at the affected outlets, discussing the chilling effect of shutting down dissent online.
- Beau recommends following journalist Carrie Wedler and criticizes the ban on her personal Twitter account.
- The outlets Beau is associated with were not affected by the ban, but he stresses the potential threat to independent networks like theirs.
- He warns against comparing the banned outlets to Alex Jones and underscores that the issue is about disagreeing with government policies, not advocating violence.
- Beau points out that Facebook and Twitter are no longer free platforms for discourse, hinting at government influence in the censorship.
- He urges people to seek alternative social media networks like Steam It and MeWe for uncensored information as Facebook's censorship may escalate.
- The censorship on Facebook has resulted in the removal of tens of millions of subscribers from certain outlets, affecting the landscape of online news sharing.

# Audience

Social media users.

# On-the-ground actions from transcript

- Find alternative social media platforms like Steam It or MeWe to access uncensored information (suggested).
- Share news from independent outlets on alternative platforms to counter censorship (exemplified).

# Oneliner

Facebook and Twitter's censorship of independent news outlets opposed to government policies signals the end of free discourse online, urging users to migrate to alternative platforms for uncensored information.

# Quotes

- "Facebook and Twitter are no longer free platforms for discourse."
- "This is going to be the end of free discourse on Facebook."
- "Be ready. Once it starts, it doesn't stop."
- "You need to be looking for another social media network."
- "This is kind of terrifying."

# Whats missing in summary

The full transcript provides a deeper insight into the implications of online censorship and the potential consequences for free speech.

# Tags

#Censorship #SocialMedia #IndependentOutlets #GovernmentPolicies #OnlineNews

```