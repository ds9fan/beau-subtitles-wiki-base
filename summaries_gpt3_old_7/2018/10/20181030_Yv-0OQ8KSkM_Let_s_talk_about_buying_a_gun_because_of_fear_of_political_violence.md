```

# Bits

Beau says:

- Explains the sexism in gun advice for women, suggesting that the advice to get a revolver or shotgun is rooted in stereotypes.
- Advises on the importance of training with a firearm, especially for women, and understanding the subtext behind popular firearm choices.
- Urges women to choose a firearm based on their specific needs, environment, and purpose, rather than gender stereotypes.
- Recommends finding someone knowledgeable to help with selecting and testing firearms to find the right fit.
- Stresses the importance of training to kill and not just for self-defense, understanding the gravity of owning a firearm.
- Provides tips on firearm training, including choosing the right firearm, learning to shoot, and the mentality needed during a potential confrontation.
- Emphasizes the necessity of speed, surprise, and violence of action in a firefight situation.
- Advises on tactical approaches when facing a home intruder, focusing on protecting life rather than material possessions.
- Recommends using the layout of your home strategically in a defensive situation, such as creating a fatal funnel to catch intruders off guard.
- Suggests opting for military-used or military-based firearms for simplicity, reliability, and ease of use in high-pressure situations.

# Audience

Women considering purchasing a firearm.

# On-the-ground actions from transcript

- Find someone knowledgeable in firearms to help you select and test different options (suggested).
- Train with the chosen firearm extensively to understand its operation and capabilities (suggested).
- Practice shooting techniques, including speed reloads and engaging targets at different distances (suggested).

# Oneliner

Choosing a firearm should be based on purpose, not gender stereotypes. Extensive training and strategic thinking are key in firearm ownership.

# Quotes

- "Each one is designed with a specific purpose in mind. Your purpose, based on what you've said, is to kill a person."
- "You're going to train to kill, not poke holes in paper."
- "You want about two, three inches separating your rounds. Because when a bullet hits, it creates a wound channel, but it also kills tissue around the wound."

# What's missing in summary

In-depth insights on firearm selection, training, and tactical considerations for women preparing for potential confrontations.

# Tags

#FirearmTraining #SelfDefense #GenderStereotypes #TacticalApproaches #HomeDefense
```