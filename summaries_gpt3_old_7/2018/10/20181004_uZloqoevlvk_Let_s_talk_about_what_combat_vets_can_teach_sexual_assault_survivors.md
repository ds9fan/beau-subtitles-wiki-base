```

# Bits

Beau says:

- Beau addresses the messages he's received and expresses his difficulty in responding adequately to the shared experiences.
- He touches on the benefit of the shared experiences and transitions into discussing what women can learn from combat veterans.
- Beau warns against dating combat veterans due to their detachment, fear of intimacy, and emotional issues resulting from traumatic experiences.
- He draws parallels between the emotional responses of sexual assault survivors and combat veterans, normalizing the feelings experienced by both groups.
- Beau questions the double standard in stigmatizing dating sexual assault survivors while praising dating combat veterans.
- He challenges the notion of survivors being "damaged goods" and addresses the unfair stigma surrounding sexual assault survivors.
- Beau shares a story about a combat experience to illustrate the impact of trauma on memory and emotional responses.

# Audience

Survivors, supporters, combat veterans

# On-the-ground actions from transcript

- Support survivors (exemplified)
- Challenge stigmas (exemplified)

# Oneliner

Beau dismantles stigmas by drawing parallels between sexual assault survivors and combat veterans, challenging societal perceptions of trauma.

# Quotes

- "You are not damaged goods."
- "We are all damaged goods in some way."
- "There's nothing wrong with you."

# What's missing in summary

The full video provides deeper insights into trauma, memory, and societal perceptions of survivors and combat veterans.

# Tags

#Trauma #Stigma #Survivors #CombatVeterans #EmotionalSupport #SocietalPerceptions

```