```

# Bits

Beau says:

- Talks about Halloween costumes and the freedom to choose offensive costumes.
- Mentions the consequences of dressing up in offensive costumes.
- Addresses the lack of humor in being offensive and edgy.
- Talks about the changing societal views on offensive jokes.
- Points out the anonymity of the internet leading to bold actions.
- Encourages being a good person rather than just being shocking.
- Acknowledges the right to dress and act as one pleases, but also the judgment it may bring.
- Comments on the idea of political correctness trying to change flawed thinking.
- Asserts that bigotry is outdated and unacceptable.
- Warns that actions today will be remembered, despite any temporary social shifts.

# Audience

Individuals reflecting on their choice of Halloween costumes and their behavior towards others.

# On-the-ground actions from transcript

- Be a good person, not just shocking (implied).
- Choose costumes thoughtfully and avoid offensive portrayals (implied).
- Respect changing societal norms and avoid perpetuating bigotry (suggested).

# Oneliner

Beau talks about the consequences of offensive Halloween costumes, urging people to be good rather than shocking, and pointing out the evolving societal views on humor and bigotry.

# Quotes

- "Being offensive and edgy is not actually a substitute for having a sense of humor."
- "Bigotry is out. It's something that sane people have given up on."
- "You can do whatever you want, but the rest of us are going to look down on you."

# What's missing in summary

Beau's passionate delivery and emphasis on personal responsibility and societal accountability are best experienced by watching the full video.

# Tags

#Halloween #Costumes #SocietalNorms #Bigotry #Consequences #Humor

``` 