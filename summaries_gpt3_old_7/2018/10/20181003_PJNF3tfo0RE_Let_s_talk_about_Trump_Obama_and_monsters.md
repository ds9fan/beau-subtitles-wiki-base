```

# Bits

Beau says:

- President of the United States mocked sexual assault survivors on TV, setting the bar incredibly low.
- Trump's comments suggest a concerted effort to silence sexual assault survivors.
- Beau questions how to prevent individuals like Trump from committing assault.
- Trump is portrayed as entitled, irresponsible, and lacking honor and integrity.
- Contrasts between Obama and Trump are drawn regarding behavior in public settings.
- Beau criticizes the presumption of innocence argument used by Trump in the context of job interviews.
- The dangerous notion of dehumanizing sexual assault perpetrators as "monsters" is discussed.
- Drawing parallels to Nazis, Beau warns against overlooking evil in plain sight.
- The focus shifts to the complicity of individuals who stay silent in the face of evil.
- Beau concludes by underscoring the proximity of dangerous individuals to everyday life.

# Audience

Activists, advocates, voters.

# On-the-ground actions from transcript

- Contact local organizations supporting sexual assault survivors (implied).
- Join advocacy groups working to combat rape culture (implied).
- Organize community events to raise awareness about the importance of speaking out against sexual assault (generated).

# Oneliner

Trump's actions on sexual assault, contrasted with Obama, prompt reflections on dangerous dehumanization and the importance of speaking out against evil in plain sight.

# Quotes

- "He set the bar that low."
- "They're not monsters, guys. They're people."
- "The scariest Nazi wasn't a monster. He was your neighbor."

# What's missing in the summary

The full video provides a more in-depth exploration of the dangers of overlooking evil and the importance of speaking out against injustice.

# Tags

#SexualAssault #Trump #Obama #Dehumanization #Silence #EvilInPlainSight #RapeCulture #CommunityAction #Accountability #Nazis

```