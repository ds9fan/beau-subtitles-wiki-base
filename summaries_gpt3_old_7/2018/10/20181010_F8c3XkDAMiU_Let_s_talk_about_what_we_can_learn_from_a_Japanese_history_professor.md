```

# Bits

Beau says:

- Beau tells a tale of a history professor in Japan before World War II who predicts Tokyo's destruction and moves to a remote island.
- The history professor, despite his potential influence, chooses to remain silent instead of trying to prevent the war.
- Beau argues that remaining silent in the face of tyranny is choosing the side of the oppressor.
- He points out that power is centralized today and being silent only benefits those in power.
- Beau challenges the societal norms of avoiding discussing money, politics, and religion, suggesting that it's time to have open dialogues on these topics.
- He advocates for having meaningful debates and dialogues among ourselves rather than letting politicians drive the narrative.
- Beau warns that tyranny anywhere poses a threat to freedom everywhere, urging people to speak up against oppression.

# Audience

Activists, advocates, community members.

# On-the-ground actions from transcript

- Initiate open dialogues on topics like money, politics, and religion (suggested).
- Have meaningful debates and dialogues within your community (exemplified).
- Speak up against oppression and tyranny in any form (implied).

# Oneliner

History professor's silence in the face of tyranny prompts Beau to urge open dialogues on taboo topics to combat oppression.

# Quotes

- "When you remain silent in the face of tyranny or oppression, you have chosen a side."
- "Tyranny anywhere is a threat to freedom everywhere."

# What's missing in summary

The passion and conviction conveyed by Beau's storytelling and call to action can best be experienced by watching the full video.

# Tags

#Silence #Tyranny #Oppression #OpenDialogues #Activism
```