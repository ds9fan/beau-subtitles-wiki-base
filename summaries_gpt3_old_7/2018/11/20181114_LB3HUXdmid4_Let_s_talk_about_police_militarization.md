```

# Bits

Beau says:

- Explains the concept of the "warrior cop" and how it stems from a misguided idea of mimicking the military.
- Points out the misconception that equates killing with being a warrior, when in reality, only a small percentage of the military are actual killers.
- Raises concerns about the shift in law enforcement's focus from protecting the public to strictly enforcing the law.
- Argues that if a law enforcement officer enforces unjust laws, like those criminalizing feeding the homeless, they are not serving the public good.
- Challenges the notion that being a cop is an extremely dangerous job by providing statistics on other professions that have higher fatality rates.
- Criticizes the propaganda used to justify the militarization of police forces and the inflated statistics regarding law enforcement deaths.
- Questions the necessity of militarized equipment such as flashbangs, MRAPs, and SWAT teams in routine policing operations.
- Addresses the lack of proper training given to officers who operate with military-grade equipment, leading to dangerous situations and unjustified killings.
- Urges law enforcement to reconsider their approach, focusing more on serving the public rather than enforcing laws with excessive force.
- Concludes by stressing the importance of understanding the true meaning of being a warrior and avoiding unnecessary violence in law enforcement.

# Audience
Advocates for police reform.

# On-the-ground actions from transcript

- Rethink the approach to law enforcement (implied)
- Advocate for police accountability and proper training (implied)
- Support policies that prioritize community well-being over militarization (implied)

# Oneliner
Beau challenges the concept of the "warrior cop," questioning the militarization of police forces and urging a focus on public service over unnecessary force.

# Quotes
- "If you're no longer a public servant, your job is strictly to enforce the law, means you're personally responsible on a moral level for every law in the books."
- "If you're a cop in an area where feeding the homeless is illegal, you're a bad cop. Period. Full stop."
- "Being a cop, not really that dangerous. Not a lot of deaths really. It's not even in the top 10."
- "There is no war on cops. There should be no warrior cops."
- "You're the weapon if you're a warrior."

# Whats missing in summary
In-depth examples and analysis on specific cases or incidents related to police militarization.

# Tags
#PoliceMilitarization #LawEnforcement #Propaganda #PublicService #MilitarizedEquipment #CommunitySafety
```