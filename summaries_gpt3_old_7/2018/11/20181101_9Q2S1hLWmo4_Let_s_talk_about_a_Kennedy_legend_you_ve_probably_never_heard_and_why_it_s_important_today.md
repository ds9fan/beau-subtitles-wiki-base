```

# Bits

Beau says:

- Beau shares a conspiracy theory about President Kennedy meeting with Green Berets in 1960 to take an oath to fight against a tyrannical government.
- The legend suggests that Kennedy's honor guard included Green Berets, which was unusual and mysterious.
- A newsletter leak about a group called the Special Forces Underground prompted a DOD investigation that confirmed it wasn't a racist organization.
- Active-duty Green Berets still visit Kennedy's grave annually for a ceremony, showcasing a unique tribute.
- Beau connects Kennedy's criteria for fighting tyranny, specifically the suspension of habeas corpus, to current political considerations.
- He questions the need for suspending habeas corpus in dealing with migrants and asylum seekers.
- Beau argues that the suspension of habeas corpus poses a greater threat to national security than unarmed refugees seeking asylum.
- He warns against the erosion of the rule of law and the dangerous implications of suspending habeas corpus.

# Audience

History enthusiasts, conspiracy theory followers, political analysts.

# On-the-ground actions from transcript

- Visit JFK Presidential Library to see Command Sergeant Major Barretti's beret on display (implied).
- Research and understand the significance of habeas corpus and its implications (exemplified).

# Oneliner

President Kennedy's alleged oath to Green Berets and the potential suspension of habeas corpus raise concerns about tyranny and the rule of law in a gripping conspiracy narrative.

# Quotes

- "The suspension of habeas corpus is the end of the rule of law."
- "The suspension of habeas corpus is a threat to national security. It's a threat to your very way of life."
- "At this point you can support your country or you can support your president."
- "The rule of law will be erased completely."
- "You can watch this country turn into another two-bit dictatorship or you can kill this party politics that you're playing."

# Whats missing in summary

In-depth exploration of the historical context and potential implications of suspending habeas corpus.

# Tags

#ConspiracyTheory #JFK #GreenBerets #HabeasCorpus #Tyranny #NationalSecurity
```