```

# Bits

Beau says:

- Beau shares the true story of a Native girl named Matoaka, commonly known as Pocahontas, contrasting it with the fabricated version often portrayed.
- Matoaka learned English and was taken hostage by colonists, forced into marriage, and ultimately died young.
- The popularized narrative of Pocahontas as a romantic figure who saved John Smith is largely fictionalized.
- Native stories in popular culture often involve themes of the "good native" and the "white savior," perpetuating inaccurate stereotypes.
- Beau mentions a movie plot where a Native character leaves, becomes an FBI agent, and returns to the reservation as a hero, contrasting this with the harsh reality faced by Native communities with law enforcement.
- He brings attention to the distorted portrayal of Native culture and history in mainstream media, urging a more accurate understanding.

# Audience

History enthusiasts, Native rights advocates.

# On-the-ground actions from transcript

- Research accurate Native American histories (suggested).
- Support Native American-led initiatives and storytelling (implied).

# Oneliner

Beau reveals the grim truth behind the fabricated narrative of Pocahontas, shedding light on the harsh realities faced by Native communities in mainstream media storytelling.

# Quotes

- "Most of what you know of Native stories is false."
- "Not a whole lot of stories about natives just being native."
- "Most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."

# What's missing in summary

The full transcript provides deeper insights into the misrepresentation of Native American stories and the impact of these inaccuracies on mainstream perceptions.

# Tags

#NativeAmerican #Pocahontas #History #Colonialism #Misrepresentation #NativeCulture

```