```

# Bits

Beau says:

- Narrates a story from the Tet Offensive about a general who successfully ordered a lieutenant to take a hill with no casualties.
- Compares the vague orders coming down the chain of command to the situation at the border, where soldiers may face mission creep.
- Expresses concern about soldiers being used for illegal emergency law enforcement at the border by the Department of Homeland Security.
- Warns soldiers about the consequences of following vague orders that may lead to illegal actions and stresses the importance of abiding by the laws of war.
- Encourages soldiers at the border to keep their weapons safe, follow the rules of engagement, and not get involved in situations that could jeopardize their legal standing.
- Advises soldiers to familiarize themselves with the legal process of applying for asylum in the United States, pointing out that migrants in caravans are acting legally.
- Questions why Republicans, in control of the legislative and executive branches, haven't changed asylum laws if they disagree with current practices.
- Urges soldiers to protect their honor and integrity by not allowing themselves to be used as political tools and to seek clarification on vague orders.
- Condemns the use of soldiers for political gain before midterm elections and stresses the importance of following the law even in ambiguous situations.
- Ends with a warning to soldiers to keep their weapons slung and on safe, to ask for clarity on orders, and to prioritize following the law.

# Audience

Soldiers at the border

# On-the-ground actions from transcript

- Seek clarification on vague orders (implied)
- Familiarize yourself with the laws of war and rules of engagement (suggested)
- Understand the legal process of applying for asylum in the United States (suggested)

# Oneliner

Soldiers at the border urged to prioritize following the law, seeking clarity on orders, and protecting their honor amidst potential mission creep and political manipulation.

# Quotes

- "You keep that weapon on safe and you keep it slung."
- "If you allow that mission creep to happen, you're going to need it."
- "It's your uniform, your honor being used to legitimize them circumventing the law."

# What's missing in summary

Importance of maintaining integrity and following legal guidelines in challenging circumstances.

# Tags

#Soldiers #BorderSecurity #MissionCreep #AsylumSeekers #LawsOfWar
```