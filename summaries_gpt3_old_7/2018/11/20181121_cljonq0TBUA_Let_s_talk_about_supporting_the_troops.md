```

# Bits

Beau says:

- Dennis informs Terry about a group in need after a hurricane.
- Beau helps gather supplies and distribute them to the community in need.
- Veterans, including Dennis, Terry, David, a lady, a man with a pickup truck, and an 89-year-old man, come together to assist.
- Beau points out the lack of support for veterans facing issues like delayed GI bill payments and homelessness.
- There is a significant lack of outcry over veteran suicides and other challenges they face.
- Beau criticizes the use of veterans as political tools and the selective support they receive.
- He urges genuine care and involvement in supporting veterans by addressing root causes.
- Beau concludes by stressing the importance of supporting truth before claiming to support the troops.

# Audience

Community members, veterans, supporters.

# On-the-ground actions from transcript

- Contact local veterans' organizations to offer support (suggested).
- Volunteer at shelters or community centers assisting veterans in need (implied).
- Advocate for better policies and resources for veterans (exemplified).

# Oneliner

Beau shares a story of community support post-hurricane, raising awareness on the need for genuine care and involvement in supporting veterans.

# Quotes

- "If you really want to help veterans, stop turning them into combat veterans because some politician waved a flag and sold you a pack of lies that you didn't even bother to look into."
- "Before you can support the troops, you've got to support the truth."

# Whats missing in summary

The emotional depth and personal anecdotes shared by Beau.

# Tags

#CommunitySupport #Veterans #HurricaneRelief #SupportTheTruth #GetInvolved
```