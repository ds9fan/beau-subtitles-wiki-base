import argparse
import os
import time
import glob
import re

from stable_whisper import load_model, results_to_sentence_srt, finalize_segment_word_ts
from stable_whisper.text_output import clamp_segment_ts

parser = argparse.ArgumentParser()
parser.add_argument('-m', '--model', help='model - tiny, base, small, medium, large', default='medium')
parser.add_argument('paths', nargs='*', help='paths to audio files', default=[])
parser.add_argument('--force-max-len', type=int, help='limit a max number of characters per phrase', default=120)
parser.add_argument('--force', action='store_true', help='force overwrite')
parser.add_argument('--core', action='store_true', help='print text to console instead of saving')
parser.add_argument('--include-ts', action='store_true', help='(for --core only) include timestamps in output')
parser.add_argument('--beam-size', type=int, help='beam size', default=3)
parser.add_argument('--patience', type=float, help='patience', default=3)

def to_text(lines) -> str:
    """
    lines: List[dict]
        [{start:<start-timestamp-of-text>, end:<end-timestamp-of-text>, text:<str-of-text>}, ...]
    """

    def secs_format(secs: (float, int)):
        mm, ss = divmod(secs, 60)
        hh, mm = divmod(mm, 60)
        hh_maybe = f'{hh: >2.0f}:'
        return f'[{hh_maybe}{mm:0>2.0f}:{ss:0>2.0f}]'

    res_str = '\n'.join(
        f'{secs_format(sub["start"])} {sub["text"].strip()}'
        for sub in lines)

    return res_str

def results_to_sentence_text(res: dict, ts: bool,
                            end_at_last_word=False,
                            end_before_period=False,
                            start_at_first_word=False,
                            force_max_len: int = None):
    """
    from https://github.com/jianfch/stable-ts/blob/main/stable_whisper/text_output.py
    license: MIT
    (might as well make this whole file MIT)
    Parameters
    ----------
    res: dict
        results from modified model
    ts: bool
        include timestamp in output
    end_at_last_word: bool
        set end-of-segment to timestamp-of-last-token
    end_before_period: bool
        set end-of-segment to timestamp-of-last-non-period-token
    start_at_first_word: bool
        set start-of-segment to timestamp-of-first-token
    force_max_len: int
        limit a max number of characters per phrase. Ignored if None (Default: None)
        Note: character count is still allow to go under this number for stability reasons.
    strip: bool
        perform strip() on each segment
    """
    if force_max_len:
        segs = finalize_segment_word_ts(res,
                                        end_at_last_word=end_at_last_word,
                                        end_before_period=end_before_period,
                                        start_at_first_word=start_at_first_word,
                                        force_max_len=force_max_len,
                                        strip=True)
        segs = [dict(text=''.join(i), start=j[0]['start'], end=j[-1]['end']) for i, j in segs]
    else:
        segs = clamp_segment_ts(res,
                                end_at_last_word=end_at_last_word,
                                end_before_period=end_before_period,
                                start_at_first_word=start_at_first_word)
    if not ts:
        return "\n".join(seg["text"].strip() for seg in segs)

    return to_text(segs)

def transcribe_core(model, audio_path, include_ts=False, force_max_len=None, force=False, beam_size=None, patience=None):
    result = model.transcribe(audio_path, language="en", pbar=True, beam_size=beam_size, patience=patience, compression_ratio_threshold=2.0, suppress_silence=True, remove_background=True, condition_on_previous_text=False)
    text = results_to_sentence_text(result, ts=include_ts, force_max_len=force_max_len)
    with open(audio_path.rpartition(".")[0] + ".txt", "w") as writer:
        writer.write(text)


def transcribe(model, audio_path, force_max_len=None, force=False, beam_size=None, patience=None):
    basename = os.path.basename(audio_path).rstrip('.ogg')

    #date = basename.split("_")[0]
    #year = date[:4]
    #month = date[4:6]
    #day = date[6:8]
    # re version
    date_match = re.match(r'^(\d{4})(\d{2})(\d{2})_.*', basename)
    if date_match:
        year = date_match.group(1)
        month = date_match.group(2)
        #path_ts_txt = os.path.join("whisper_ts", year, month, f'{basename}.ts.txt')
        path_txt = os.path.join("whisper_txt", year, month, f'{basename}.txt')
        #path_srt = os.path.join("whisper_srt", year, month, f'{basename}.srt')
    else:
        #path_ts_txt = os.path.join("whisper_ts", f'{basename}.ts.txt')
        path_txt = os.path.join("whisper_txt", f'{basename}.txt')
        #path_srt = os.path.join("whisper_srt", f'{basename}.srt')
    if os.path.exists(path_txt) and not force:
        raise FileExistsError(f'{path_txt} already exists. Use --force to overwrite.')

    result = model.transcribe(audio_path, language="en", pbar=True, beam_size=beam_size, patience=patience, compression_ratio_threshold=2.0, suppress_silence=True, remove_background=True, condition_on_previous_text=False)
    text_ts = results_to_sentence_text(result, ts=True, force_max_len=force_max_len)
    text = results_to_sentence_text(result, ts=False, force_max_len=force_max_len)

    os.makedirs(os.path.dirname(path_txt), exist_ok=True)
    os.makedirs(os.path.dirname(path_ts_txt), exist_ok=True)
    os.makedirs(os.path.dirname(path_srt), exist_ok=True)

    results_to_sentence_srt(result, path_srt, force_max_len=force_max_len)

    with open(path_txt, 'w', encoding='utf-8') as f:
        f.write(text)
    print(f'Saved: {os.path.abspath(path_txt)}')
    with open(path_ts_txt, 'w', encoding='utf-8') as f:
        f.write(text_ts)
    print(f'Saved: {os.path.abspath(path_ts_txt)}')


def cli():
    args = parser.parse_args()
    if args.paths and len(args.paths):
        paths = args.paths
    else:
        paths = glob.glob("audio/**/*.ogg", recursive=True)
    model = load_model(args.model)
    if args.core:
        for audio_path in sorted(paths)[::-1]:
            transcribe_core(model, audio_path, include_ts=args.include_ts, force_max_len=args.force_max_len, force=args.force, beam_size=args.beam_size, patience=args.patience)
        return

    exist_count = 0
    print(f"Transcribing {len(paths)} files...")
    for audio_path in sorted(paths)[::-1]:
        time_start = time.time()
        try:
            transcribe(model, audio_path, force_max_len=args.force_max_len, force=args.force, beam_size=args.beam_size, patience=args.patience)
        except FileExistsError:
            print(f"Done. File exists: {audio_path}")
            exist_count += 1
            if exist_count > 10:
                print("10 files already exist. Exiting.")
                break
        time_end = time.time()
        if time_end - time_start > 1 or len(paths) < 10:
            print(f"{time_end - time_start:.2f} seconds")
    print("done")


if __name__ == '__main__':
    cli()
