# Bits

Beau says:

- Analyzing the shooting in Dallas, Beau questions the official narrative and introduces the concept of liars, categorizing them as bad liars and prepared liars.
- Beau questions the credibility of the officer's story, pointing out inconsistencies and potential motives.
- Expressing concern over the lack of justice and transparency in the case, Beau mentions the potential consequences of the officer's actions under Texas law.
- Recalling a past interaction with Kevin Crosby, Beau brings up the topic of Black Lives Matter and the importance of justice for black lives.
- Beau criticizes the police's actions post-shooting, particularly the search of the victim's home to justify the killing.
- Addressing police corruption and the lack of accountability within the Dallas PD, Beau condemns the culture of covering up wrongdoing.
- Beau challenges the notion of "a few bad apples" within law enforcement, asserting that tolerating misconduct tarnishes the entire institution's reputation.
- Concluding with a reflection on the loss of public trust in law enforcement and the need for accountability and justice in this case.

# Quotes

- "There's two kinds of liars in the world. There's a bad liar, and a bad liar you gotta ask him that question over and over and over again to finally get to the truth."
- "Because if this is the amount of justice they get, they don't. They don't."
- "It's not a few bad apples spoils the bunch and that certainly appears to have happened in Dallas because if you're wearing one of those uniforms and you let this slide, you're just as crooked as they are."

# Oneliner

Beau questions the official narrative of a Dallas shooting, exposes potential motives, and calls for justice and accountability in law enforcement.

# Audience

Advocates for justice

# On-the-ground actions from transcript

- Demand transparency and accountability from law enforcement (implied)
- Support initiatives that address police corruption and misconduct (implied)
- Advocate for systemic reforms to ensure justice for victims of police violence (implied)

# Whats missing in summary

Deeper insights into the systemic issues plaguing law enforcement and the impact of unchecked police misconduct on communities.

# Tags

#DallasShooting #PoliceAccountability #JusticeForVictims #SystemicReforms #BlackLivesMatter