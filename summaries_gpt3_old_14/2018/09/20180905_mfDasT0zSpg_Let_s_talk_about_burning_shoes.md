# Bits

Beau says:

- Beau wants to burn something inside because it's raining and he feels it's really important to burn it today.
- Beau talks about not owning Nikes due to his views on the company but mentions his son has a pair that he wants to burn.
- He questions the importance of burning the shoes in relation to the Skycloth creating liberty and freedom.
- Beau dismisses the idea of burning the shoes as foolish and immature, suggesting more meaningful actions.
- He suggests donating the shoes to a shelter or thrift store near military bases for those in need.
- Beau questions the motives behind burning the shoes, whether it's to insult workers or just express anger towards the company.
- He draws parallels between burning shoes and burning the American flag as symbols of protest.
- Beau criticizes those who are willing to disassociate with Nike over a commercial but ignore the issues of sweatshops and slave labor.
- He implies that some people prioritize symbols over true freedom and understanding.
- Beau concludes by pointing out the irony of loving symbols of freedom more than actual freedom.

# Quotes

- "Take them and drop them off at a shelter. Maybe give them to a homeless vet, you know, those people you pretend you care about."
- "You're loving that symbol of freedom more than you love freedom."

# Oneliner

Beau questions the symbolism of burning shoes, urging for meaningful actions over emotional reactions and criticizing prioritizing symbols over true freedom.

# Audience

Consumers, activists, parents

# On-the-ground actions from transcript

- Donate shoes to shelters or thrift stores near military bases (suggested)
- Raise awareness about sweatshops and slave labor issues (implied)

# Whats missing in summary

The full transcript dives into the irony of symbolic actions, urging for genuine care and actions towards those in need rather than performative gestures.

# Tags

#ConsumerActivism #Symbolism #Humanitarianism #SocialResponsibility #EthicalConsumption