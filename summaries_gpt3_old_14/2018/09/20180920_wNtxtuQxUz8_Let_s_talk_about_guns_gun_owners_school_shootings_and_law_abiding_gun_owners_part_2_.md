# Bits

Beau says:

- Gun control has failed due to being written by those unfamiliar with firearms, leading to ineffective measures like banning assault weapons based on arbitrary criteria.
- Proposed solutions like banning high-capacity magazines are flawed, as changing magazines is quick and rushing the shooter during that time is dangerous.
- Banning specific firearms like the AR-15 is not practical, as it may lead to the use of more powerful weapons like the Garand, escalating the lethality of potential attacks.
- Banning semi-automatic rifles entirely is problematic because modern rifles are designed this way, and such bans could create a black market, making firearms more accessible to those who shouldn't have them.
- Raising the minimum age to purchase firearms from 18 to 21, especially for long guns, could be a practical measure to prevent impulsive actions by younger individuals.
- Closing loopholes in laws regarding domestic violence offenders' access to firearms is necessary to prevent potential future violence.
- Beau argues that focusing solely on legislation won't solve the underlying issues causing gun violence and that a broader approach is needed.

# Quotes

- "The assault weapons ban was an utter failure because there's nothing called an assault weapon."
- "Legislation is not the answer here in any way shape or form."
- "Raising the age from 18 to 21 is a great idea. That's brilliant, actually."
- "It doesn't matter what your motives are, the reality is that [outlawing firearms] is going to happen."
- "Legislation is not the answer here in any way shape or form."

# Oneliner

Beau dismantles common gun control proposals, advocating for practical solutions like raising the minimum age to 21 and addressing domestic violence loopholes instead of ineffective bans.

# Audience

Advocates for sensible gun control.

# On-the-ground actions from transcript

- Raise the minimum age to purchase firearms from 18 to 21 (as exemplified).
- Close loopholes in laws regarding domestic violence offenders' access to firearms (as suggested).

# Whats missing in summary

Beau's detailed breakdown of gun control proposals and their shortcomings can provide valuable insights into the limitations of legislative solutions and the need for broader approaches to address gun violence effectively.

# Tags

#GunControl #Firearms #Legislation #DomesticViolence #CommunitySafety