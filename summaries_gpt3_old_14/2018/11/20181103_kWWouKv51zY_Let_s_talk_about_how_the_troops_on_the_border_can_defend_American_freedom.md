# Bits

Beau says:

- Tells an anecdote about a general during the Tet Offensive who successfully took a hill with minimal casualties by giving a vague order that was implemented effectively on the ground.
- Points out that soldiers at the border have a unique chance to defend American freedom by joining a select group in history who truly defended it.
- Warns about mission creep, using Afghanistan as an example, and the dangers of vague orders leading to illegal actions domestically.
- Emphasizes the importance of following the laws of war, UCMJ, and rules of engagement, especially when dealing with non-combatants.
- Urges soldiers to keep their weapons safe and slung, avoid illegal actions, and not get involved in questionable orders.
- Advises soldiers at the border to familiarize themselves with legal asylum procedures in the United States and points out that seeking asylum is legal.
- Criticizes the political use of soldiers before midterm elections and warns against blindly following vague or illegal orders.
- Encourages soldiers to seek clarification on orders, maintain a paper trail for defense, and prioritize following the law over mission creep.

# Quotes

- "Keep that weapon slung and you keep it on safe."
- "You wouldn't do anything illegal. You wouldn't circumvent the law, earn that trust."
- "If you allow that mission creep to happen, you're going to need it. You're going to need a defense."
- "Your honor being used to legitimize them circumventing the law."
- "It's bred into you, right? You wouldn't do anything illegal."

# Oneliner

Beau warns soldiers at the border to uphold their honor by following the law, avoiding mission creep, and seeking clarification on vague or illegal orders.

# Audience

Soldiers at the border

# On-the-ground actions from transcript

- Seek clarification on vague orders and maintain a paper trail for defense (implied).
- Familiarize yourself with legal asylum procedures in the United States (implied).
- Prioritize following the laws of war, UCMJ, and rules of engagement (implied).

# Whats missing in summary

Importance of upholding honor and integrity while serving at the border.

# Tags

#Soldiers #Border #MissionCreep #LegalAsylum #RulesOfEngagement