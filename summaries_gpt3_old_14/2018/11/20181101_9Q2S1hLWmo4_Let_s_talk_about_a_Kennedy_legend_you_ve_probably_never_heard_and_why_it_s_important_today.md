# Bits

Beau says:

- President Kennedy allegedly met with selected Green Berets in 1961 and made them take an oath to help Americans fight back against a tyrannical government.
- Kennedy was assassinated in 1963, and 46 Green Berets from Fort Bragg headed to DC, with 21 serving as his honor guard, a role unprecedented for Green Berets.
- There are various legends surrounding the activities of the Green Berets after Kennedy's assassination, including placing green berets on his casket.
- The JFK Presidential Library displays Command Sergeant Major Barretti's beret, the only permanent military display in the facility.
- A newsletter leaked information about a group called the Special Forces Underground, prompting a DOD investigation that concluded it was not a racist organization.
- Active-duty Green Berets visit Kennedy's grave site annually, performing a brief ceremony.
- Kennedy allegedly set out criteria for Green Berets to act if habeas corpus was suspended, a scenario currently being considered by the sitting president.
- Beau questions the necessity of suspending habeas corpus to address migrants, arguing that asylum seekers have rights and are following the law by seeking asylum.
- Beau warns against the suspension of habeas corpus as it threatens the rule of law and national security, urging viewers to understand the implications and stand against it.
- He presents the choice between supporting the country or the president in the face of potentially erasing the rule of law through the suspension of habeas corpus.

# Quotes

- "The suspension of habeas corpus is the end of the rule of law."
- "The suspension of habeas corpus is a threat to your very way of life."
- "At this point you can support your country or you can support your president."
- "The rule of law will be erased completely."
- "This is dangerous."

# Oneliner

President Kennedy allegedly initiated Green Berets to defend against tyranny, leading to legends and a warning against suspending habeas corpus by Beau.

# Audience

Americans

# On-the-ground actions from transcript

- Visit JFK Presidential Library to see Command Sergeant Major Barretti's beret on display (exemplified)
- Educate yourself on habeas corpus and its importance in upholding the rule of law (exemplified)
- Advocate for upholding the rule of law and resisting the suspension of habeas corpus (exemplified)

# Whats missing in summary

The full transcript provides historical context and a cautionary tale about the suspension of habeas corpus, urging viewers to understand its significance and take action to protect the rule of law.

# Tags

#PresidentKennedy #GreenBerets #HabeasCorpus #NationalSecurity #Tyranny