def summarize(transcript, author, model="gpt-3.5-turbo-0125"):
    logit_bias = logit_bias_for_transcript(transcript)
    enct = enc.encode(transcript)
    print(f"transcript len: {len(transcript)} chars, {len(enct)} tokens")
    tokenlimit = min(max(768, int((len(enct)**0.5)*20)), 1536)
    res2 = client.chat.completions.create(
        messages=[
            {
                "role": "system",
                "content": dedent("""
                    Include any deadlines or dates.

                    NO "REMEMBER,"
                    NO SPOTLIGHT METAPHORS.
                    NO PRO-POLICE ACTIONS. We stan community policing, not police. Remember the victims of police.

                    Let’s get a quick cheat sheet together. Serve up the the transcript like this:
                    ```
                    # Bits

                    <Name of speaker> says:

                    - Give an overview of the moments in the transcript.
                    - Use the original examples rather than generated abstract summaries.
                    - Prefer extraction over summary.
                    - Show don't tell: what happened? Why? The who, what, when, where, why, emotions, and dynamics.
                    - Ten to fifteen short and precise nuggets.
                    - Keep the flow of the point.
                    - Up to fifteen bullet points:
                    - ...
                    - ...
                    - ...
                    - ...
                    - ...
                    - ...
                    - ...

                    # Quotes

                    Provide the most memorable slogan-worthy quotes from the text to remember and share. Max five.

                    # Oneliner

                    Copy the most important Bits line, usually the first one, plus summary of the others. Ideally should cover beginning, middle, end, and MUST cover core takeaway. SHOULD include every point from Bits. THIS SECTION IS MOST IMPORTANT.

                    # Audience

                    In three to five words, who can take action?

                    # On-the-ground actions from transcript

                    <!-- Skip this section if there aren't physical actions described or suggested. -->
                    - Actionable, specific, completeable community, prep, aid stuff. Fewer is better unless author had many. Include ONLY actions described in the transcript. Focus do-now ones. On-the-ground actions MUST begin with direct, practical, concrete, physical, social, community network actions, concrete interactive verbs, examples: Contact people, Join organizations, Meet people, Build an object, Give someone an object, Go to your community, Organize an event, Create an org, Complain to your..., Ask each other, Bake neighbors a..., Coordinate neighbors to..., Distribute aid to..., Feed your people, ...; In parenthesis, end each "On-the-ground actions from transcript" line with whether transcript ( suggested, exemplified, implied ) the action, or if it was ( generated ).
                    - DON'T say to "educate yourself". DON'T include general, vague, ambient, always-on actions. NEVER USE "reflect on..." or "notice your..." or "discuss..." type actions! Those ain't actions. Actions are done with HANDS, not brain.
                    - Actions MUST have a clear completion point. DO say specifically what to do on the ground with other people in your community.

                    # Whats missing in summary

                    10-20 words on what can be best gotten from watching the full thing

                    # Tags

                    #UpToTen #Topic #Keywords #Someone #might #SearchFor
                    ```

                    Wordpart token limit {tokenlimit}
                """).format(tokenlimit=tokenlimit)
            },
            {
                "role": "user",
                "content": dedent("""

                    transcript
                    ```
                    {transcript}
                    ```
                    from {author}
                    transcript length: {tokens} wordpart tokens
                """).format(transcript=transcript, tokens=len(enct), author=author),
            }
        ],
        logit_bias=logit_bias,
        max_tokens=tokenlimit,
        presence_penalty=0.10,
        frequency_penalty=0.05,
        temperature=0.6,
        model=model,
        seed=1,
        top_p=0.99
    )
    costs.append((model, res2.usage))
    return res2.choices[0].message.content
