# Bits

Beau says:

- Explains the importance of a judge overruling Trump's asylum ban.
- Notes that some people were misled to believe crossing the border and claiming asylum was illegal due to misinformation from news outlets and the President.
- Clarifies the legality and morality of seeking asylum.
- Condemns Trump's attempt to change asylum laws through proclamation, labeling it as dictatorial behavior.
- Describes the attempt to circumvent the established process of lawmaking and the Constitution.
- Points out that while the judicial branch intervened, Congress failed in its duty to prevent such actions.
- Accuses Trump of attempting to undermine the Constitution and challenges the notion of patriotism among his supporters.
- Concludes by stressing the importance of upholding the Constitution.

# Oneliner

A judge's ruling on Trump's asylum ban exposes his dictatorial attempt to circumvent the Constitution, revealing a dangerous erosion of democratic principles.

# Audience

Americans concerned about democratic values.

# On-the-ground actions from transcript

- Challenge misinformation spread by news outlets (suggested)
- Support efforts to uphold the Constitution (exemplified)

# Quotes

- "If it's your party, they can set fire to the Constitution and you'll roast a marshmallow over it..."
- "His defenders will probably have photos of the Constitution or an American eagle or one of those stupid red hats..."
- "You don't care about the Constitution."

# Whats missing in summary

The full transcript provides a deeper insight into the implications of Trump's actions on democracy and the importance of upholding constitutional values.