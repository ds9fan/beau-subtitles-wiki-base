# Bits

Beau says:

- Beau delves into the issue of police militarization, sparked by viewer interest.
- He criticizes the concept of the "warrior cop" who aims to mimic the military, often equating warrior with killer.
- Beau points out that only four percent of the military are actually killers.
- He mentions a book recommendation, "On Killing" by Lieutenant Colonel Grossman, to delve deeper into the topic.
- Beau contrasts the importance of civil affairs and CBs in keeping bad guys off the battlefield through infrastructure development.
- He criticizes the shift in law enforcement's focus from protecting the public to solely enforcing the law.
- Beau argues that enforcing unjust laws makes one a "hired goon" rather than a public servant.
- He questions the justification for police militarization and the desire for weapons associated with being a "warrior cop."
- Beau challenges the inflated statistics used to portray law enforcement as being in constant danger, leading to quick trigger responses.
- He dissects a flawed study on unarmed killings by law enforcement, revealing skewed statistics and unjustified fatalities.

# Oneliner

Beau delves into police militarization, critiquing the "warrior cop" mentality and inflated danger perceptions leading to unjustified actions.

# Audience

Law enforcement officers, policymakers

# On-the-ground actions from transcript

- Challenge unjust laws (suggested)
- Advocate for community-focused policing (implied)

# Quotes

- "If your interest is no longer protecting the public and it's only enforcing the law, you're a hired goon."
- "Your duty is not bounded by risk, my friend."
- "There is no war on cops. There should be no warrior cops."

# Whats missing in summary

Deeper insights into the consequences of police militarization and the need for a shift towards community-focused policing.

# Tags

#PoliceMilitarization #LawEnforcement #CommunityPolicing #PublicServants #Injustice