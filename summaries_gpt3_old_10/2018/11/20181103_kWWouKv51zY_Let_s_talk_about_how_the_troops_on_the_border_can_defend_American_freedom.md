# Bits

Beau says:
- Recounts a story from the Tet Offensive in 1968 where a general orders a lieutenant to take a hill crawling with enemy forces, resulting in success without any casualties.
- Stresses the importance of vague orders becoming specific on the ground and the need for implementation.
- Talks about the situation at the border and the unique chance for war fighters to defend American freedom by joining a select group of individuals who have truly defended it throughout history.
- Mentions "Mission Creep" using Afghanistan as an example and warns against soldiers being used for illegal purposes domestically.
- Advises soldiers to stay within the law, keep their weapons safe, and avoid getting involved in potentially illegal orders.
- Urges soldiers to know the laws of war, the UCMJ (Uniform Code of Military Justice), and the real rules of engagement.
- Encourages soldiers to keep their weapons slung and safe to protect themselves legally.
- Recommends soldiers to visit the USCIS website to understand the legal process of applying for asylum in the United States.
- Points out that migrants seeking asylum at the border are acting legally and could do so because there is no law requiring them to apply in the first country they reach.
- Calls attention to the trust the American people have in the military uniform and urges soldiers to maintain that trust by following the law and not circumventing it.

# Oneliner

Beau stresses the importance of soldiers following legal orders, avoiding mission creep, and protecting themselves by staying within the law.

# Audience

Soldiers, Military Personnel

# On-the-ground actions from transcript

- Understand the legal process of applying for asylum in the United States (suggested)
- Follow legal orders and rules of engagement, keeping weapons safe and slung (implied)
- Avoid getting involved in potentially illegal orders, seek clarification if needed (implied)

# Quotes

- "You wouldn't do anything illegal. You wouldn't circumvent the law, earn that trust, keep that weapon slung and keep it on safe."
- "It's your uniform, your honor being used to legitimize them circumventing the law. That's what's going on."

# Whats missing in summary

The full transcript provides detailed insights into the importance of soldiers following legal orders, understanding mission creep, and protecting themselves by staying within the boundaries of the law.

# Tags

#Soldiers #Military #LegalOrders #MissionCreep #Asylum #USCIS #LawfulEngagement