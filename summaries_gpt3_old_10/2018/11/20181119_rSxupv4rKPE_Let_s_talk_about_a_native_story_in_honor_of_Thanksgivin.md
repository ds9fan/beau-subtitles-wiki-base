# Bits

Beau says:
- Shares a Native American story of Matoaka, not the typical Thanksgiving tale.
- Matoaka learned English, traded with colonists, and was kidnapped by them.
- Forced into marriage with a colonist who took her to England as an exhibit.
- Matoaka died young, theories suggest she was poisoned or fell ill.
- Clarifies the real story of Pocahontas, whose real name was Matoaka.
- Challenges the romanticized narratives around Native stories in popular culture.
- Points out the false portrayal of natives as either saviors or needing to be civilized.
- Mentions the common theme of a white savior in Native American storylines.
- Talks about a movie where a character leaves the tribe, becomes an FBI agent, and returns as a hero.
- Raises awareness about the misrepresentation of Native culture and stories in mainstream media.

# Oneliner

Beau shares the tragic real story of Matoaka (Pocahontas) and challenges the fabricated narratives of Native American tales in popular culture.

# Audience

History enthusiasts

# On-the-ground actions from transcript

- Challenge and correct misrepresentations of Native American history and culture (suggested).
- Support and amplify authentic Native voices and stories (exemplified).

# Quotes

- "Not a whole lot of it's true in popular."
- "Most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."

# Whats missing in summary

Beau's emotional delivery and passion for setting the record straight on Native American history and culture.

# Tags

#NativeAmerican #History #Misrepresentation #Culture #Storytelling