# Bits

Beau says:

- Tells a legend about President Kennedy meeting with selected Green Berets in 196
- Kennedy allegedly had Green Berets take an oath to fight back against a tyrannical government
- Green Berets were reportedly involved in Kennedy's funeral after his assassination
- A conspiracy theory suggests the existence of a secret organization within the Green Berets
- A group of active-duty Green Berets visit Kennedy's grave annually
- Beau warns about the suspension of habeas corpus and its implications
- Questions the necessity of suspending habeas corpus in dealing with migrants
- Argues that asylum seekers have rights the moment they set foot on American soil
- Beau criticizes the idea of suspending habeas corpus as a threat to national security and the rule of law
- Urges viewers to understand the significance of habeas corpus and its potential removal

# Oneliner

President Kennedy's alleged oath to Green Berets and the warning against suspending habeas corpus underscore the importance of upholding democratic principles.

# Audience

Citizens, Activists, Green Berets

# On-the-ground actions from transcript

- Research habeas corpus and its implications (suggested)
- Stay informed on government actions regarding civil liberties (suggested)
- Advocate for upholding democratic principles in your community (suggested)

# Quotes

- "The suspension of habeas corpus is though, that is a threat to national security. It's a threat to your very way of life."
- "The suspension of habeas corpus is something that can't be tolerated."

# Whats missing in summary

Watching the full transcript provides a deeper understanding of the potential consequences of suspending habeas corpus and the importance of upholding democratic values.

# Tags

#GreenBerets #ConspiracyTheory #HabeasCorpus #Kennedy #Democracy