# Bits

Beau says:

- Dennis heard about a group in need north of Panama City after the hurricane.
- Terry was informed by Dennis and reached out to Beau for help.
- Beau went to Hope Project to pick up supplies with a lady filling in for David.
- Beau loaded up his truck with supplies, including baby formula.
- On the way, they stopped at a community center in Fountain, Florida.
- The community center needed food, so Beau exchanged supplies.
- An 89-year-old man helped load food into another truck at the community center.
- Beau emphasized that Dennis, Terry, David, the lady, the truck owner, and the 89-year-old man were all veterans.
- Beau discussed the challenges veterans face, like delayed GI bill payments and benefits being reduced.
- He criticized the lack of outcry over veteran suicides and political use of veterans.
- Beau pointed out that veterans are often used as an excuse not to help others.
- He urged people to understand veterans better and stop turning them into combat veterans for political reasons.
- Beau emphasized the importance of supporting the truth before supporting the troops.
- He encouraged getting involved to truly help veterans.

# Oneliner

Veterans come together to aid hurricane victims, revealing societal misconceptions and urging genuine support for veterans.

# Audience

Community members, advocates.

# On-the-ground actions from transcript

- Reach out to local organizations aiding hurricane victims (implied).
- Support veterans by advocating for timely GI bill payments and maintaining benefits (implied).
- Get involved in initiatives that provide mental health support for veterans (implied).

# Quotes

- "If you really want to help veterans, stop turning them into combat veterans because some politician waved a flag and sold you a pack of lies that you didn't."
- "Before you can support the troops, you've got to support the truth."

# Whats missing in summary

The full transcript provides a deeper understanding of the challenges veterans face and the importance of genuine support beyond rhetoric.

# Tags

#Veterans #CommunityAid #Support #HurricaneRelief #Truth #SocialResponsibility