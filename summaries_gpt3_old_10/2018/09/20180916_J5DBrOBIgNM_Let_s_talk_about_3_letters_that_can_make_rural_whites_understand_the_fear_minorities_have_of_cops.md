# Bits

Beau says:

- Explains the distrust minorities have for law enforcement, contrasting it with the accountability of sheriffs in rural communities.
- In rural areas, law enforcement officials are usually known personally, making them more accountable to the community.
- Describes how in close-knit rural communities, law enforcement officials prioritize the spirit of the law over the letter of the law.
- Mentions the ability to hold accountable local law enforcement through either voting or community pressure.
- Points out that minority groups lack institutional power and therefore struggle to hold law enforcement accountable.
- Draws a parallel between the distrust felt by minorities towards law enforcement and how white country folk might relate through examples like the ATF and BLM.
- Raises concerns about unjust killings by unaccountable men with guns like ATF and BLM agents.
- Encourages action by leveraging institutional power to hold law enforcement accountable, such as electing police chiefs and mayors.
- Stresses the importance of caring enough to get involved in addressing issues of police accountability.
- Suggests addressing corruption within police departments as a preventive measure before officers join federal agencies like ATF.
- Advocates for unity among all individuals facing similar problems with unaccountable law enforcement.

# Oneliner

Understanding distrust of law enforcement: minorities lack institutional power, while rural communities hold accountable sheriffs directly. Unite to address unaccountable men with guns in law enforcement.

# Audience

Community members

# On-the-ground actions from transcript

- Elect local police chiefs and mayors for accountability (implied)
- Address corruption within local police departments to prevent issues in federal agencies (implied)
- Talk to individuals from different backgrounds to find common ground and solutions (implied)

# Quotes

- "It's unaccountable men with guns. We can work together and we can solve that."
- "We've got to start talking to each other. We got the same problems. It's just different agencies."
- "We can swing an election. And in these cities where the police chief hadn't elected, get rid of the mayor and make it clear why."

# Whats missing in summary

The full transcript includes detailed examples and explanations on how different communities perceive law enforcement accountability and suggests actionable steps to address issues of police accountability and corruption.

# Tags

#CommunityPolicing #PoliceAccountability #InstitutionalPower #Unity #ElectOfficials