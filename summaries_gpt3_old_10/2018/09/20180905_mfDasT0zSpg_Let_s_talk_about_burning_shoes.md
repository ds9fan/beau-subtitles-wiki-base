# Bits

Beau says:

- Inside due to rain, feeling the need to burn something.
- Expresses dislike for Nike but respects son's choice to wear them.
- Initially talks about burning Nikes but reveals it as a metaphor.
- Urges not to burn the shoes but donate them to those in need.
- Questions the intention behind burning the shoes - workers or company leadership.
- Compares burning shoes to burning the American flag for attention.
- Points out hypocrisy of denouncing Nike for a commercial but ignoring past labor abuses.
- Concludes with a statement about understanding true freedom.

# Oneliner

Beau debates symbolic shoe burning, advocating for meaningful action over emotional reactions, questioning true understanding of freedom.

# Audience

Consumers, activists, parents.

# On-the-ground actions from transcript

- Donate used shoes to shelters or homeless vets (implied).
- Give shoes to thrift stores near military bases (implied).

# Quotes

- "You're loving that symbol of freedom more than you love freedom."
- "It's what built that company. You don't really get to have an opinion about freedom because you don't know what it is."

# Whats missing in summary

The emotional depth and personal connection that Beau brings to the topic.

# Tags

#ConsumerActivism #Symbolism #SocialResponsibility #Nike #Freedom