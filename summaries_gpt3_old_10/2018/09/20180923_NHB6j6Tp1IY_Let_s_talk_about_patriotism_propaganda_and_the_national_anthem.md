# Bits

Beau says:

- Responding to an op-ed story about a woman attacked for kneeling during the national anthem.
- Expresses irritation and decides to respond after reading the author's bio.
- Questions the purpose of a protest and lectures on patriotism and the Constitution.
- Calls out the author for attacking the woman and not understanding the meaning behind the protest.
- Questions the competence or potential propaganda motives of a law enforcement spokesman.
- Defines patriotism as correcting the government when wrong and protecting constitutionally protected rights.
- Recalls a story about patriotism being displayed through actions, not symbols.
- Encourages seeking advice from someone with 30 years of service before giving lectures on patriotism.
- Differentiates between nationalism and patriotism.
- Ends with a strong statement asserting his own patriotism.

# Oneliner

Beau challenges misconceptions on patriotism, questions a law enforcement spokesman, and defends constitutionally protected rights.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact Beau for advice on patriotism (implied)
- Seek counsel from individuals with long service history before giving lectures on patriotism (implied)

# Quotes

- "Patriotism is correcting your government when it is wrong."
- "Sir, patriotism is not doing what the government tells you to."
- "That's kind of chilling, isn't it?"
- "Patriotism was displayed through actions, not worship of symbols."
- "You can kiss my patriotic behind, okay?"

# Whats missing in summary

The full transcript delves deeper into the complexities of patriotism, protest, and constitutional rights, providing valuable insights and challenges to societal norms.

# Tags

#Patriotism #Protest #Constitution #LawEnforcement #NationalAnthem