# Bits

Beau says:

- Analyzes a shooting in Dallas involving a cop and a civilian.
- Differentiates between bad liars and good liars, discussing their tendencies.
- Questions the official narrative of the shooting based on the officer's actions.
- Raises concerns about the officer's potential motive based on past noise complaints against the victim.
- Points out that even if the victim's door was open, the officer's actions could be considered a home invasion.
- Notes the legal consequences of killing someone during a home invasion in Texas.
- Recalls a past interaction regarding Black Lives Matter and All Lives Matter.
- Expresses skepticism about the justice system's treatment of black lives in Dallas.
- Condemns the post-shooting search of the victim's home for justifying the killing.
- Criticizes the lack of intervention from other police officers in the Dallas PD.
- Argues that the actions of a few bad officers tarnish the reputation of the entire force.
- Calls out the attempt to discredit the unarmed victim who was killed in his own home.
- States that unjust enforcement of laws and corruption contribute to public distrust of the police.

# Oneliner

Analyzing a shooting in Dallas, Beau questions the motive behind an officer's actions and criticizes police corruption, urging for justice and accountability in law enforcement.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Seek justice for victims of police violence (implied)
- Hold law enforcement accountable for unjust actions (implied)
- Advocate for transparency and integrity within police departments (implied)

# Quotes

- "If this is the amount of justice they get, they don't."
- "Not one cop has crossed the thin blue line to say that's wrong."
- "It's a few bad apples spoils the bunch."
- "That is why people don't back the blue anymore."
- "You're trying to slander and villainize an unarmed man that was gunned down in his home."

# Whats missing in summary

The emotional intensity and raw honesty conveyed by Beau can be fully experienced by watching the original video.

# Tags

#DallasShooting #PoliceViolence #BlackLivesMatter #Justice #Accountability #CommunityPolicing