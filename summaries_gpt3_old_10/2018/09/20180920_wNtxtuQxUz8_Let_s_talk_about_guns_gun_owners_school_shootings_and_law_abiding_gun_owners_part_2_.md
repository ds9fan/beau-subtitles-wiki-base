# Bits

Beau says:

- Differentiates between failed gun control measures written by those with little knowledge of firearms and potential effective solutions.
- Challenges the idea of banning assault weapons due to the vague definition of "assault weapon."
- Points out flaws in banning high-capacity magazines, as changing magazines is quick and rushing the shooter during that time is dangerous.
- Argues against banning specific firearms like the AR-15, as it may lead to more powerful weapons being used in shootings.
- Explains why banning semi-automatic rifles altogether might be ineffective, especially in a country with a high number of guns.
- Suggests raising the legal age from 18 to 21 to purchase firearms as a potential solution to prevent school shootings.
- Addresses loopholes in laws regarding domestic violence histories affecting gun ownership.
- Compares outlawing firearm designs to outlawing abortion, stressing that legislation alone cannot solve the issue.

# Oneliner
Misguided gun control measures fail due to lack of firearm knowledge, while raising the legal age for gun purchases could be a simple, effective solution to prevent school shootings.

# Audience
Legislators, policymakers, gun control advocates

# On-the-ground actions from transcript
- Raise the legal age for purchasing firearms from 18 to 21 (suggested)
- Close the loophole that allows individuals with domestic violence histories to avoid gun ownership restrictions (suggested)

# Quotes
- "Legislation is not the answer here in any way shape or form."
- "Legislation is not the answer here in any way shape or form."
- "Legislation is not the answer here in any way shape or form."
- "If you ban them they get cheaper, more accessible to the people you don't want to have them."
- "When you make something illegal like that, you're just creating a black market."

# Whats missing in summary
The full transcript provides detailed explanations on why certain gun control measures have failed and proposes alternative solutions such as raising the legal age for gun purchases and addressing loopholes in existing laws.

# Tags
#GunControl #Firearms #Legislation #SchoolShootings #DomesticViolence