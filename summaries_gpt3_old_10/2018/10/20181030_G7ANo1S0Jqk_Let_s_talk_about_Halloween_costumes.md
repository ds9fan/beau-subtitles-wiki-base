# Bits

Beau says:

- Addresses offensive Halloween costumes such as blackface, sexy Pocahontas, Trayvon Martin, and Adolf Hitler.
- Questions the shock when people face backlash for intentionally offensive actions.
- Points out that being offensive is not a substitute for having a sense of humor.
- Recalls a time when offensive jokes were more accepted but notes the evolution of societal norms.
- Calls out individuals who haven't grown up and still find humor in tormenting others.
- Mentions the anonymity of the internet contributing to people's offensive behavior.
- Encourages being a good person rather than just being edgy or shocking.
- Asserts that people have the right to dress and joke how they want but others will look down on them.
- Addresses the notion of political correctness and the need to change flawed thinking.
- Warns that actions today will be remembered and have consequences.

# Oneliner

Addressing offensive costumes, lack of humor, evolving norms, and consequences for actions in a thought-provoking Halloween message.

# Audience

Internet users

# On-the-ground actions from transcript

- Be a good person (implied)
- Change flawed thinking (implied)
- Avoid offensive behaviors and costumes (implied)

# Quotes

- "Be edgy, be shocking, here, be shocking, be a good person."
- "Nobody's going back to that."
- "People are going to look down on you."

# Whats missing in summary

The full transcript offers more context on the evolution of societal norms, the impact of internet anonymity, and a call to be a better person.

# Tags

#Halloween #OffensiveCostumes #Consequences #PoliticalCorrectness #InternetBehavior