```

# Bits

Beau says:
- President mocked sexual assault survivors on TV, setting a low bar.
- Effort to silence sexual assault survivors led by the President.
- Explains the dangerous implication of the word "let" in assault context.
- Contrasts Obama's character with Trump's, questioning integrity.
- Talks about the presumption of innocence and the Kavanaugh hearing.
- Warns against dehumanizing rapists, drawing a parallel to Nazis.

# Audience
Activists, voters, survivors

# On-the-ground actions from transcript
- Speak out against silencing of assault survivors (exemplified)
- Challenge dangerous rhetoric around assault (generated)
- Support policies that empower survivors (implied)

# Oneliner
President's actions set dangerous precedent, urging to challenge silence around sexual assault and avoid dehumanizing rhetoric.

# Quotes
- "There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim."
- "The scariest Nazi wasn't a monster. He was your neighbor."
- "Scariest rapist or rape-apologist, well they're your neighbor too."

# What's missing in summary
Nuances of Beau's delivery and emotional impact.

# Tags
#SexualAssault #President #Dehumanization #Nazis #SilencingSurvivors

```