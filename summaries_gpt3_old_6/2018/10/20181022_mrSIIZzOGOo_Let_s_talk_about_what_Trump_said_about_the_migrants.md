# Bits

Beau says:
- President cutting off aid to Central American countries for not stopping people from coming to the US is terrifying.
- President believes it's the head of state's job to keep people from leaving their country.
- The wall along the southern border can be used to keep Americans in.
- This is a slow creep of tyranny with a smiling face.
- Supporting these actions goes against the ideals of freedom and liberty.
- The wall is seen as a prison wall, not just a border wall.

# Audience

Concerned citizens, activists.

# On-the-ground actions from transcript

- Contact local representatives to advocate against cutting off aid to Central American countries (exemplified).
- Join or support organizations working to protect human rights and immigrant rights (suggested).
- Volunteer with organizations supporting vulnerable populations affected by restrictive immigration policies (suggested).

# Oneliner

President's actions to cut off aid and control borders are signs of tyranny, jeopardizing freedom and liberty for Americans as well.

# Quotes

- "That wall is a prison wall. It's gonna work both ways."
- "If you believe in the ideals of freedom and liberty, you can't if you've chosen to you've sold out your ideals and your principles and your country for a red hat and a cute slogan."

# Whats missing in summary

Nuances of Beau's passionate delivery. 

# Tags

#Tyranny #Freedom #Immigration #HumanRights #Activism