```

# Bits

Beau says:
- Hurricane Michael hit unexpectedly, causing damage and power outages.
- Acknowledges community support and safety, despite personal impact.
- Expresses concern for neighboring communities facing severe consequences.
- Comments on law enforcement response to looting during disasters.
- Law enforcement's shutdown during looting hampers volunteer relief efforts.
- Media portrayal of looting post-disasters creates bias and misinformation.
- Urges preparing for disasters independently to avoid reliance on government.
- Praises Florida National Guard's swift response during Hurricane Michael.
- Advocates for individuals to create emergency kits with essentials.
- Stresses the importance of having necessary supplies for survival.
- Offers to share resources and links to aid in assembling emergency kits.
- Encourages viewers to prioritize preparedness for short-term survival.

# Audience
Community members, disaster preppers.

# On-the-ground actions from transcript
- Prepare an emergency kit with food, water, fire supplies, shelter, medical necessities, and a knife. (suggested)
- Gather necessary supplies for at least a week or two to survive independently. (exemplified)
- Include extra medication for family members who require daily meds in the emergency kit. (generated)

# Oneliner
Hurricane aftermath insights: Be prepared, create emergency kits with essentials for short-term survival; government response may not always be reliable.

# Quotes
- "Protecting the inventory of some large corporation should pretty much be at the bottom of any law enforcement agencies list."
- "The end result is you can't count on government response."
- "Definitely take the time to do it if you're not going to read the article short version you need food water fire shelter medical supplies and a knife."
- "Putting together a kit, a bag, for emergencies is something that every family should do."
- "Just talking about enough to survive a week or two until things normalize and it's not hard and it's really not that expensive."

# What's missing in summary
Importance of community support and independent preparation for disaster resilience.

# Tags
#HurricaneMichael #CommunitySupport #IndependentPreparation #EmergencyKit #DisasterPreparedness #FloridaNationalGuard #SurvivalEssentials

```