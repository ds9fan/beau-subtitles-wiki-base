```

# Bits

Beau says:
- Halloween costumes: offensive choices have consequences.
- Edgy humor is not a replacement for a sense of humor.
- Anonymity online leads to offensive behavior in real life.
- Normalizing bigotry is unacceptable.
- PC culture aims to change flawed thinking.
- People won't tolerate regressive attitudes resurfacing.
- Actions have consequences; don't be surprised by backlash.

# Audience
Individuals, educators, event organizers

# On-the-ground actions from transcript
- Refrain from offensive Halloween costumes (exemplified)
- Cultivate a sense of humor without being offensive (exemplified)
- Act responsibly online to prevent harmful behavior offline (generated)

# Oneliner
Offensive Halloween costumes have consequences; edgy humor isn't a substitute for decency. Normalize respect, not bigotry.

# Quotes
- "Edgy humor is not actually a substitute for having a sense of humor."
- "Normal people do not want to associate with you."
- "Bigotry is out. It's something that sane people have given up on."

# What's missing in summary
Nuances of Beau's delivery and emphasis on consequences.

# Tags
#Halloween #Costumes #Consequences #Bigotry #PCculture #Responsibility