```

# Bits

Beau Ginn:
- Militia commander called him to the Mexican border to protect America without stopping refugees.
- Not stopping refugees to avoid violating U.S., Mexican, and international law, as well as losing gun rights.
- Won't send back asylum seekers at gunpoint, respecting rights and due process.
- Backing up DHS, despite being labeled potential terrorists or extremists.
- Concerned about DHS knowing militia tactics if they actually follow through with rhetoric.
- Speculates on militia commanders playing "4D chess" for good optics while armed at the border ready to kill unarmed asylum seekers.


# Audience
Border residents, concerned citizens


# On-the-ground actions from transcript
- Contact local authorities to report militia activity (exemplified)
- Advocate for peaceful and legal border protection measures (exemplified)


# Oneliner
Militia member at the border speculates on optics while ready to kill unarmed asylum seekers, raising concerns about legality and ethics.


# Quotes
- "We're down here on the border protecting America with our guns."
- "They're going to know exactly who we are and how we operate."
- "Ready to kill a bunch of unarmed asylum seekers who are following the law."


# Whats missing in summary
The full transcript provides more context on the complexities of border protection and the potential consequences of militia actions.


# Tags
#BorderSecurity #Militia #Ethics #Legalities #Refugees #DHS #Optics #AsylumSeekers #DueProcess
```