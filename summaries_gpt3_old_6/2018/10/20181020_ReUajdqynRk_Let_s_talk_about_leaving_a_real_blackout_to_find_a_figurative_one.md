```

# Bits

Beau says:
- Beau is out for a drive to escape the monotony at home.
- Facebook and Twitter have censored independent news outlets opposing government policies.
- Comparing the censorship to Alex Jones is not accurate.
- Many independent outlets were affected, some providing valuable information.
- Facebook and Twitter are no longer free platforms for open discourse.
- The censorship appears to be at the government's request.
- Users need to prepare to shift to alternative social media networks.
- Independent outlets are moving to platforms like Steam It and MeWe.
- Tens of millions of subscribers were removed from Facebook.
- Independent outlets offer better fact-checking and accountability than major networks.

# Audience
Social media users

# On-the-ground actions from transcript
- Prepare to shift to alternative social media networks like Steam It and MeWe (suggested).
- Follow independent journalists like Carrie Wedler (exemplified).
- Support independent news outlets by seeking out their information on alternative platforms (generated).

# Oneliner
Facebook and Twitter censor independent news outlets opposing government policies, prompting users to prepare for a shift to alternative platforms like Steam It and MeWe.

# Quotes
- "Comparing the censorship to Alex Jones is not accurate."
- "Tens of millions of subscribers were removed from Facebook."
- "Independent outlets offer better fact-checking than major networks."

# What's missing in summary
The full transcript provides a detailed exploration of the censorship of independent news outlets and the implications for free discourse on social media.

# Tags
#Censorship #SocialMedia #IndependentNews #AlternativePlatforms #FactChecking #Accountability
```