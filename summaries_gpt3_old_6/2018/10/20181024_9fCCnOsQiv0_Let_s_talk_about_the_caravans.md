```

# Bits

Beau says:
- Caravans have been coming for 15 years due to drug war policy and US interventions.
- The current caravan is a result of US involvement in legitimizing a coup in Honduras.
- San Pedro Sula, where the caravan originated, has a high murder rate driving people to flee.
- Tax dollars fund assistance for migrants through the Office of Refugee Resettlement.
- It's cheaper to help migrants get on their feet than detain them.
- Migrants travel in large groups for safety, not a conspiracy.
- Carrying other countries' flags fosters camaraderie among fellow nationals.
- Wearing military fatigues is practical for durability on a long journey.
- Claiming asylum is legal and must be done at a port of entry as per US law.
- Blame should be on US foreign policy and drug war funding gangs rather than migrants.

# Audience
Advocates, policymakers, community leaders.

# On-the-ground actions from transcript
- Contact representatives to address US foreign policy impact (suggested).
- Advocate for changes in drug war policies that impact Central American countries (suggested).

# Oneliner
Caravans are a consequence of US policies; it's legal for asylum seekers to claim asylum, advocate for policy change to address root causes of migration.

# Quotes
- "It's cheaper to be a good person."
- "If you have that big of a problem with them coming here, call DC."
- "Give me one good reason to send these people back to their deaths."

# Whats missing in summary
Deeper understanding of US foreign policy's impact on Central American countries.

# Tags
#Migration #AsylumSeekers #USPolicy #CentralAmerica #Advocacy #HumanRights
```