```

# Bits

Beau says:
- Addresses sexual harassment and assault in the context of the Kavanaugh nomination.
- Provides advice on avoiding such situations, focusing on personal accountability.
- Advises safeguarding reputation, ending dates at front door, not taking anyone home while drinking, and watching what is said and signals sent.
- Stresses keeping hands to oneself to avoid sending wrong signals.
- Mentions the fear of false accusations and stresses personal accountability for actions.
- Counters victim blaming arguments by focusing on the accountability of both parties.
- Challenges the notion that clothing choices impact rape, asserting that rapists are the sole cause.

# Audience
Men, people discussing sexual harassment.

# On-the-ground actions from transcript
- Safeguard your reputation to attract the right kind of people. (exemplified)
- End dates at the front door for safety. (exemplified)
- Avoid taking or going home with someone while drinking. (exemplified)
- Watch what you say to avoid sending the wrong signals. (exemplified)
- Keep your hands to yourself to prevent misunderstandings. (exemplified)
- Practice self-accountability to avoid false accusations. (exemplified)

# Oneliner
Beau gives advice on avoiding sexual harassment, focusing on personal accountability and challenging victim blaming narratives.

# Quotes
- "There is one cause of rape, and that's rapists."
- "If you're that worried about it, it's because you've done something."
- "Don't put yourself in a situation where you can be falsely accused 2% of the time."

# What's missing in summary
Detailed examples and anecdotes to support the advice given by Beau.

# Tags
#SexualHarassment #Accountability #VictimBlaming #RapeCulture #PersonalSafety #MenToo

```