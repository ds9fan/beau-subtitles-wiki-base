# Bits

Beau says:
- Beau is inside to burn something due to the rain and mentions the importance of burning it today.
- Beau talks about not owning Nikes but his son having a pair that he wants to burn.
- Beau questions the idea of burning shoes to protest against a company and suggests donating them instead.
- He asks whether burning shoes is meant to insult workers or those in charge.
- Beau compares burning shoes to burning the American flag for attention.
- He addresses the disconnect between being mad at a commercial and ignoring ethical issues within the company.
- Beau concludes by stating that loving a symbol of freedom more than actual freedom is contradictory.

# Audience
Consumers, activists, ethical shoppers.

# On-the-ground actions from transcript

- Donate unwanted items to shelters or homeless veterans (suggested).
- Give away clothing to thrift stores near military bases (suggested).
- Question and challenge the motivations behind symbolic actions (exemplified).

# Oneliner
Burning shoes in protest? Beau questions symbolic actions and urges ethical consideration over emotional reactions towards corporations.

# Quotes
- "Take them and drop them off at a shelter. Maybe give them to a homeless vet, you know, those people you pretend you care about."
- "You're loving that symbol of freedom more than you love freedom."

# Whats missing in summary
The full transcript provides a detailed exploration of the contradictions in symbolic protests and consumer activism.

# Tags
#ConsumerActivism #EthicalShopping #SymbolicProtests #CorporateResponsibility #Donations