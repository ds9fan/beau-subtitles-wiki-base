```

# Bits

Beau says:
- Responding to a story shared by people who knew a woman attacked for kneeling during the national anthem.
- Expresses irritation at the story and the author's bio, urging attention to the issue.
- Questions the purpose of a protest and lectures on patriotism, pointing out the importance of correcting the government when wrong.
- Challenges a Law Enforcement spokesman, questioning his understanding of the protest and patriotism.
- Recalls a powerful interaction post-9/11 where patriotism was defined through actions, not symbols.
- Encourages understanding the distinction between nationalism and patriotism, rejecting a nationalist viewpoint.

# Audience
Activists, Patriots, Community Leaders

# On-the-ground actions from transcript
- Contact Beau to connect with the civilian who defined patriotism through actions (generated)

# Oneliner
Beau calls out a Law Enforcement spokesman's misunderstanding of protest, defines patriotism through actions, not symbols, and distinguishes it from nationalism. #PatriotismThroughActions

# Quotes
- "Patriotism is correcting your government when it is wrong."
- "Patriotism is displayed through actions, not worship of symbols."
- "There is a very marked difference between nationalism and patriotism."

# Whats missing in summary
Deeper insights on the impact of distinguishing between nationalism and patriotism in today's society.

# Tags
#Activism #Patriotism #Protest #Community #LawEnforcement
```