```

# Bits

Beau says:
- Draws on experience with liars and Andy Griffith to analyze a shooting incident in Dallas.
- Differentiates between bad and good liars, pointing out the significance of truth in lies.
- Speculates on the shooting incident, questioning the officer's motives and actions.
- Raises concerns about the officer's potential motive based on past noise complaints against the victim.
- Contemplates the legal implications of the officer's actions, suggesting it could be considered a home invasion.
- Mentions the potential capital murder charge in Texas for killing someone during a home invasion.
- Recalls a powerful question from a past encounter regarding Black Lives Matter.
- Expresses doubt in the justice system's treatment of black lives based on the Dallas incident.
- Criticizes the Dallas PD for searching the victim's home post-shooting.
- Condemns police corruption and lack of accountability within Dallas PD.
- Concludes by urging reflection on the implications of unjust law enforcement actions.

# Audience
Activists, Community Members

# On-the-ground actions from transcript
- Contact local advocacy groups to demand accountability from law enforcement (suggested)
- Join community efforts to advocate for justice for victims of police violence (exemplified)

# Oneliner
Beau analyzes a shooting incident in Dallas, questioning motives and calling for justice while critiquing police corruption and lack of accountability.

# Quotes
- "When she resorted to violence, assaulted him, it's a home invasion."
- "If this is the amount of justice they get, they don't. They don't."
- "Not one cop has crossed the thin blue line to say that's wrong."

# What's missing in summary
Deeper insights on the implications of systemic racism in law enforcement and the impact on marginalized communities.

# Tags
#PoliceViolence #BlackLivesMatter #Justice #Accountability #CommunityAction #PoliceReform #SystemicRacism #Advocacy #Analysis #DallasShooting

```