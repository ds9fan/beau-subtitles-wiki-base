```

# Bits

Beau says:

- Recounts an anecdote about a general during the Tet Offensive in Vietnam who successfully orders a lieutenant to take a hill with no casualties.
- Explains the importance of vague orders becoming specific on the ground for implementation.
- Talks about the unique chance for service members at the border to defend American freedom.
- Warns about mission creep using the Afghanistan example and criticizes the potential use of soldiers as emergency law enforcement personnel domestically.
- Urges soldiers to stay within the law and not get involved in potentially unlawful actions.
- Advises those at the border to know the laws around asylum and how to legally apply for it in the United States.
- Points out that the migrants in the caravan are acting legally by seeking asylum and that the laws could be changed if desired.
- Encourages soldiers to uphold trust, honor, and integrity by avoiding illegal actions, especially when following vague or potentially illegal orders.
- Suggests seeking clarification and maintaining a paper trail for defense if faced with mission creep or unlawful orders.
- Stresses the importance of following the law and not being used as a political tool before the midterm elections.


# Audience

Soldiers, service members


# On-the-ground actions from transcript

- Contact USCIS website to understand legal asylum application process in the United States (suggested)
- Seek clarification via email or other means when vague orders are received (suggested)


# Oneliner

Following orders is more than words; it's about legality, honor, and avoiding being a political pawn – Beau urges soldiers to stay safe and legal. 


# Quotes

- "You better act like it. You keep that weapon on safe and you keep it slung."
- "You wouldn't do anything illegal. You wouldn't circumvent the law, earn that trust, keep that weapon slung and keep it on safe."
- "It's your uniform, your honor being used to legitimize them circumventing the law."


# What's missing in summary

In-depth context on historical examples and legal implications


# Tags

#Military #Soldiers #Border #Legalities #Orders #Asylum #MissionCreep #LawfulActions #PoliticalTool 

```