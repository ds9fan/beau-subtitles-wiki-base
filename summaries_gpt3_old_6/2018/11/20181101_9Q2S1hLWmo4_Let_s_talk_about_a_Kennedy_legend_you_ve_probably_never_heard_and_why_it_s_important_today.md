```

# Bits

Beau says:

- Introduces a legendary conspiracy theory involving President Kennedy and the Green Berets in 1962.
- Describes an oath taken by selected Green Berets to fight against a tyrannical U.S. government if criteria were met.
- Mentions President Kennedy's visit to Fort Bragg in 1962, meeting selected men in private.
- Points out the peculiar involvement of Green Berets in Kennedy's honor guard after his assassination.
- Raises questions around the secretive activities of Green Berets post-Kennedy's death.
- Suggests a mysterious connection between Green Berets and Jackie Kennedy post-assassination.
- Mentions the display of Command Sergeant Major Barretti's beret at the JFK Presidential Library.
- Talks about an investigation into a leaked newsletter about the Special Forces Underground.
- Contrasts the alleged racism claims with the inclusive history of the Green Berets.
- Raises awareness about the suspension of habeas corpus and its potential implications under the current president.

# Audience

History enthusiasts, conspiracy theory aficionados.

# On-the-ground actions from transcript

- Contact relevant historical societies or researchers to delve into the Kennedy conspiracy theory (suggested).
- Organize local events or talks to raise awareness about habeas corpus and the importance of the rule of law (suggested).

# Oneliner

President Kennedy's alleged pact with Green Berets and the looming threat of habeas corpus suspension prompt reflections on historical mysteries and current political implications.

# Quotes

- "The suspension of habeas corpus is the end of the rule of law."
- "The suspension of habeas corpus is a threat to national security. It's a threat to your very way of life."
- "At this point you can support your country or you can support your president."
- "The rule of law will be erased completely."
- "You need to realize exactly what it means for that to be gone."

# What's missing in summary

The full video provides a detailed exploration of the Kennedy conspiracy theory and delves deeper into the potential consequences of suspending habeas corpus.

# Tags

#KennedyConspiracy #GreenBerets #HabeasCorpus #History #PoliticalImplications #PresidentialActions

```