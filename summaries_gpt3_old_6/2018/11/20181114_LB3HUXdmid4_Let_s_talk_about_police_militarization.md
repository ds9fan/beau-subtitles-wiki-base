```

# Bits

Beau says:

- Police militarization leads to a mindset of the "warrior cop" who equates being a warrior with being a killer.
- Only four percent of the military are killers, but this mindset has been adopted by some law enforcement.
- Police have shifted from being public servants to strictly enforcing the law, leading to moral responsibility for every law.
- If a cop enforces unjust laws like making feeding the homeless illegal, they are considered a bad cop.
- Law enforcement has convinced themselves of a hyper-dangerous job, leading to quick trigger-pulling and unjustified killings.
- Propaganda about the dangers faced by law enforcement officers has led to a belief in a "war on cops."
- SWAT teams, meant for life-saving and rare use, are now overused for minor operations, lacking proper intelligence and training.
- Lack of proper training with advanced police tools like flashbangs leads to dangerous situations and mistakes.
- The perceived capability of SWAT teams often does not match the reality, leading to dangerous encounters with real resistance.
- There is no real need for the militarization and warrior mentality within law enforcement, as police violence is not a top dangerous job.

# Audience

Law enforcement officers, policymakers, community organizers.

# On-the-ground actions from transcript

- Contact local law enforcement agencies to advocate for de-escalation training and community policing practices (implied).
- Organize community forums to address concerns about police militarization and advocate for accountability (implied).
- Reach out to local government officials to push for oversight on the use of advanced police tools and tactics (implied).

# Oneliner

Police militarization and the "warrior cop" mindset lead to unjustified killings and a lack of accountability in law enforcement, urging a shift back to community protection rather than enforcement.

# Quotes

- "If you're a cop in an area where feeding the homeless is illegal, you're a bad cop. Period. Full stop."
- "Your duty is not bounded by risk, my friend. We know you want to go home at the end of your shift. Everybody does."
- "SWAT team's supposed to be a life-saving tool. It's why it was designed. It's also supposed to be rarely used."

# What's missing in summary

In-depth examples and analysis on the repercussions of police militarization and the "warrior cop" mentality.

# Tags

#PoliceMilitarization #WarriorCop #LawEnforcement #Accountability #CommunityPolicing

```