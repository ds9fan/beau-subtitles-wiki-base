```

# Bits

Beau says:

- Explains the significance of the judge overruling Trump's asylum ban.
- Points out that crossing the border to claim asylum is completely lawful and moral.
- Calls out news outlets for misleading the public by labeling asylum seekers as illegals.
- Addresses Trump's attempt to change asylum laws through a proclamation, likening it to dictatorship.
- Accuses Trump of directly challenging the Constitution by attempting to dictate law.
- Asserts that the judicial branch did its job by stopping Trump's actions, while Congress failed to act.
- Expresses disgust at those who defend Trump's actions, questioning their commitment to the Constitution.
- Condemns the erosion of democratic principles in the face of partisan loyalties.
- Stresses the importance of upholding the three branches of government as the foundation of the Constitution.
- Calls out blind loyalty to a political party at the expense of constitutional values.

# Audience

Activists, voters, concerned citizens.

# On-the-ground actions from transcript

- Contact your representatives to uphold the Constitution (exemplified).
- Join local advocacy groups promoting democratic values (suggested).

# Oneliner

Beau breaks down the implications of a judge overruling Trump's asylum ban, exposing attempts at dictatorship and the erosion of constitutional principles in US politics.

# Quotes

- "If it's your party, they can set fire to the Constitution and you'll roast a marshmallow over it."
- "You don't care about the Constitution."
- "That's how far this country has gone."

# What's missing in summary

Detailed examples and nuances of how blind party loyalty can erode democratic values.

# Tags

#Trump #AsylumBan #Constitution #Dictatorship #Partisanship #Democracy
```