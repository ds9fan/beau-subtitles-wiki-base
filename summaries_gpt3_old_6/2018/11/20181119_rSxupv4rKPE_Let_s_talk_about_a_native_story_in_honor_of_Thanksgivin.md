```

# Bits

Beau says:

- Tells the real story of Matoaka, also known as Pocahontas, contrasting it with the romanticized version.
- Describes how Matoaka was kidnapped, held hostage, forced into marriage, and died young - a stark contrast to the popular love story portrayal.
- Points out the fabricated nature of most Native stories in popular culture, often involving a white savior narrative.
- Mentions the lack of representation of natives just being native in popular narratives without the interjection of a white savior.
- Talks about a movie where a character becomes an FBI agent and returns as a hero to a reservation, contrasting this with the reality of Native experiences with the FBI.
- Raises awareness about Leonard Peltier's case and the discrepancy between his story and the portrayal by authorities.
- Reminds the audience to critically think about the narratives they encounter, especially during holidays like Thanksgiving.

# Audience
History enthusiasts, Native culture advocates.

# On-the-ground actions from transcript
- Research and learn about the real stories and experiences of Native Americans (suggested).
- Support Native activists and causes (exemplified).
- Write to Leonard Peltier at Coleman Prison to understand his perspective (suggested).

# Oneliner
Beau reveals the harsh reality behind the romanticized tale of Pocahontas, urging a critical look at fabricated Native narratives perpetuated in popular culture.

# Quotes
- "Most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."
- "There's not a whole lot of stories about natives just being native."

# What's missing in summary
Deeper insights into the impact of distorted historical narratives on Native communities.

# Tags
#NativeAmerican #Pocahontas #HistoricalNarratives #Colonialism #NativeCulture #FabricatedStories #CriticalThinking
```