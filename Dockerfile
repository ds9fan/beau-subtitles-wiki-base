FROM python:3.9

WORKDIR /app

# Install system dependencies
RUN apt-get update && apt-get install -y ffmpeg

# Install Python dependencies
RUN pip install --no-cache-dir \
    'numpy<2' \
    torch==1.12.1 \
    torchaudio==0.12.1 \
    torchvision==0.13.1

RUN pip install --no-cache-dir \
    atomicwrites \
    tqdm \
    rich \
    yt-dlp

# Install stable-whisper from git
RUN pip install git+https://github.com/jianfch/stable-ts.git@690aebb0358cf1ff2c2ae592b993ef610c60ff96

RUN pip install --no-cache-dir pyyaml
RUN pip install --no-cache-dir ffmpeg

# Copy your project files
COPY . .
# Set the default command
#CMD ["python", "your_main_script.py"]
