Well, howdy there, Internet people.
It's Bo again.
We talk about community networking, community building
on this channel a lot, a lot.
And when we talk about it, it's not a big deal.
It's really not.
It's not fraught with danger.
It's nothing heroic.
It's just networking for most of us, for most of us.
Because for most of us, we've won the geographic lottery.
not that dangerous to do that. It's not true for everybody. It's not true for
everybody. One of my personal heroes and somebody who was definitely a great
community builder in US history once said that the calling to speak is often
a vocation of agony. But we must speak." They killed him too. I want to talk about
a shooting that happened in Chicago. Two moms, and I want to make sure I use that term, moms,
were gunned down at a bus stop. Not at an anti-violence vigil. At a bus stop. They had
I had to get some air.
Found out about this because I saw a post calling out the press.
By the way, they handled it, because they did.
They painted it as if two anti-gun activists were killed at an
anti-gun vigil or something like that.
And the organization they belonged to was just like,
that's not what happened.
And that straightforwardness, it caught my attention, because
They said they weren't activists.
They were moms.
They lived in that community.
They were just trying to make it better.
Went to the organization's website.
And yeah, there's an anti-violence component,
of course.
I mean, it's Chicago.
If you want to make your community better,
that's going to be part of it, right?
Also talks about food security, professional development.
All this is going to stay real familiar, right?
If you've watched this channel for any length of time,
I don't know Andrea.
I don't know Chantel.
Couldn't identify him from a photo.
But I feel like I lost two of my own.
Kindred spirits, no doubt.
Yeah, we're obviously very different.
But same message, same idea.
Community building from the ground up.
Don't wait for somebody else to make it better, you do it.
They went out for a walk to get some air.
And now they're gone.
Now they are gone.
The organization they belong to is called M.A.S.K., moms
against senseless killing.
I guess moms and men against senseless killing.
Pretty heroic endeavor, trying to end violence through love
and more opportunity.
And they're gone.
Now that organization has put together GoFundMe to get a
reward together for information.
I'm going to put the link down below, because in a situation
like this, every dollar is going to help, because it
loosens lips.
This would especially be true if we could count on Chicago
PD to keep up the heat.
But we can't, because it's Chicago PD.
The GoFundMe is currently at time of filming,
about 13 grand, $13,000.
Anyway, it's just a thought.
have a good night.