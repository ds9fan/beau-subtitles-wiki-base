Well, howdy there, internet people, it's Bo again,
and it is the 4th of July.
So, I got a giant box of fireworks,
and what we're gonna do is we're gonna light it all
on fire at once, I soaked it in gasoline,
we're gonna see what happens, it's gonna be great.
Um, and we're doing this because it's the 4th of July,
and we got that important document that got released today,
Celebratin' America.
All men are created equal,
unless you're in a concentration camp,
Where the government is arguing that they don't have to provide you with soap, or toothpaste, or even sleep, adequate
food, bathing, none of that.  Because I guess those people, they're not real people.
Or maybe you're in one of those locations that there's thousands of sexual abuse complaints that get
filed, but they're not investigated because nobody cares what happens to kids if they're brown, right?
What are you celebrating? Freedom? No.  No, don't think so.
Another phrase that's in that document is the guarantee to the pursuit of happiness.
I didn't see anything about, you know, lines in the sand prohibiting that.
All men, endowed by their creator.
Anyway, y'all have a happy Fourth of July.
It's just a thought.