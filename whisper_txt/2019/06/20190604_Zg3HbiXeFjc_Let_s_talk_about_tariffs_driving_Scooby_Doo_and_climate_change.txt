Well, howdy there, internet people, it's Bo again.
So tonight we're gonna talk about tariffs,
driving, Scooby-Doo, and what they can teach us
about climate change.
The tariffs are starting to hit.
The effects are starting to be felt.
Costco, Walmart, Dollar General, Family Dollar, Dollar Tree,
they all said they're gonna have to raise prices.
And people all over the country are going, what?
The experts in the field all said this is what was going to happen and how it was going
to play out, but a lot of Americans chose to allow their chosen politician to interpret
the facts for them.
And those politicians, they don't care.
They made little jokes.
We can't know if the world was supposed to end in 1990.
No, that's not what it said.
said that we would pass the point of no return across that threshold in 1990 if we didn't
make changes.
And we did make changes.
So now that threshold has been moved back.
It's a lot like driving a car.
When the car in front of you passes that light pole, you want to be able to count 1001, 1002
before you pass that light pole.
Maybe it's time to stop, because cars don't stop on a dime, neither does climate change.
The new predictions say that by 2050 we will cross that point of no return, and that is
when we will start to see the beginning of the collapse of society.
The report referred to it as an existential national security threat, and that's what
the media is kind of going on. That's the terminology they're using. I like a
different quote in that report. It threatens the premature extinction
of Earth originating intelligent life. Premature extinction of Earth originating
intelligent life. Those are the stakes. The higher-end models put us quote on a
on a path to the end of human civilization.
It's important stuff.
Just two degrees, two degrees warmer,
a billion people get displaced.
It's hard to imagine a billion people.
It's hard to imagine a billion.
It's a huge number.
If you wanted to count a million seconds,
it would take about 12 days.
If you wanted to count a billion seconds, well it would take till about 2050, 31.7 years.
We're talking about a border crisis now.
Worrying about refugees now.
Wait until there's a billion of them.
It's a serious thing.
And these are the experts in the field.
Hope you trust them this time.
What we know is that this is the younger generation,
so this is their fight.
They're going to have to know more about this than we do,
certainly more about it than I do.
I mean, I can read the reports.
I can understand what they're saying.
But I don't know all the nuances.
And they're going to be told what they can do
to fight climate change.
And they're going to be told to plant a tree.
Yeah, it'll help.
Plant a tree.
Make sure it's a fruit tree.
It'll help recycle.
It'll help, but it's not going to solve the problem.
These small measures, well, that bought us a little time.
The report recommends a World War II-style mobilization.
Basically, it recommends the New Green Deal, really.
I mean, that's what it's talking about.
what it's saying is going to solve the problem. You're really gonna have to get
involved because it's multinational corporations and it's your government.
Those are the biggest polluters, not you, not you as an individual. So you've got
to vote with your dollar and you've got to rein in your government. And at the
end of the day, the youth, those meddling kids, when they finally get that villain,
and they pull the mask off of them just like scooby-doo they're gonna find out
it's some old dude trying to make some money anyway it's just a thought y'all
Well, have a good night.