Well, howdy there, internet people, it's Bo again.
You know, I've been accused of many things in my life.
Being inarticulate is not one of them.
Being unclear in my speech is not one of them.
Being unable to express my thoughts is not one of them.
So I was very surprised to see a certain set of comments
under those videos about the Bahamas.
But we can't help everybody, they're different.
What about our own people?
You can take those questions and comments over to somebody who's looking for an excuse
not to get involved, because those are the only people that resonates with.
Somebody that's already wrote it all off, given up the fight.
Means nothing to anybody else.
this is a point that I have been unable to convey, let me try again. I do not
believe that beyond America's borders live a lesser people. I don't think that
skin tone, language, place of birth, religion, sexual orientation, culture,
gender, or distance somehow means you don't need to help somebody who's in
need. And the funny part about it, if those men who made these comments, they
were all men, if they were the type of men they present themselves to be, they
like to pretend they are, that list, the language barriers, the distance, the
cultural differences, those are challenges to be overcome.
They're not obstacles that will defeat you.
Rugged individualist, right?
You ever think about the people telling you that,
oh, they're different?
Those people over there, don't worry about them.
They're not like you.
You ever think about those people and compare them to the
people, they're telling you we're different, I'm telling you right now I got more in common
with some poor guy that's sheltering in Nassau right now because his home was destroyed than
I am ever going to have in common with one of those empty suits up on Capitol Hill that's
telling me he's different.
Me and that guy from the Bahamas, oh, we sit down and have a drink.
We can relate on a meaningful level because we're kind of the same.
might even work the same job. I could probably get along with him. I've been on
an island before. Those on Capitol Hill, not so much. They're different. I've met a
lot of them. Can't relate to them. I know they try to cast the image that they are
relatable. You know, every once in a while there's that photo that surfaces.
They're wearing blue jeans and a flannel and a bar, right?
Man of the people.
Very few of them are.
We have to start thinking broader than a border.
These lines dividing up the world for no good reason.
When I hear somebody that has bought into that nationalist line
and really bought into it,
They sound like a gang member to me.
They really do.
They really do.
And tell me the difference.
Tell me the difference.
You got the turf.
Those ever precious borders.
You got your colors.
Only difference is, it's not a bandana, it's on a flagpole.
But you get real mad if somebody disrespects it, right?
You may not, if you're just in the neighborhood and not part of the gang, you may not agree
with everything they do.
But it takes a whole lot of courage to speak out against it, doesn't it?
And the thing is, this turf, this gang, more than 350 million people, we can help.
We can do a lot.
We've got the machinery for change here.
We can.
We just have to want to.
Sad.
Those colors.
So much of what you believe is a person and how you identify yourself, all hinges on what
colors are flying on that flagpole outside of the hospital you were born in.
Are you really telling me that somebody born in Brownsville, Texas is really that different
than somebody born up the road on the other side of the border?
It's an artificial line.
Means nothing.
Those people born in those two different hospitals, a few miles apart, they got a lot more in
common than they do with their representatives and their government.
They always will.
Your obligation to humanity does not end at the border.
Anyway it's just a thought.
Y'all have a good night.