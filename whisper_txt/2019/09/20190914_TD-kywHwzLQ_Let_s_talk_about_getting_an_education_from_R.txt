Well, howdy there, internet people, it's Beau again.
So tonight, we're going to talk about a wonderful educational resource
that shouldn't have to exist,
but it does, and I'm glad that it does.
It's an educational chat bot named Roo.
R-O-O.
If you don't know what a chat bot is, basically you send it a message online and it answers you.
But there's no person on the other end of it, it's all a computer program.
pretty cool.
This is an educational chatbot that provides education that apparently parents still don't
want to provide.
They still don't want to provide it.
There's that level of embarrassment about talking about this.
A lot of parents believe that these schools will take care of it.
The thing is, only 24 states actually require this kind of education, and only 13 of those
states require that the education provided be medically accurate.
Think about that for a while.
So this chat bot, what kind of questions will it answer?
All of them.
Anything I came up with, it had an answer for, for things as simple as what happens
during puberty to much more advanced questions about activities, orientations, emotional
questions, relationship advice.
It has it all.
It's pretty cool.
And I know what you're saying.
What kind of teen would need that?
Why don't they talk to their parents?
Apparently a lot of them.
This chat bot has been up for nine months.
It has had more than a million conversations.
That's a hundred thousand a month, thousands a day.
So it's needed.
It is needed.
Teens don't make the wisest decisions even when they have all the information.
If they don't have all the information, they're certainly not going to make the best decisions.
There's this idea that if you don't talk about it, it's not happening.
It's not true.
It still happens, it still happens.
And now this chatbot exists to pick up the slack where parents fail.
Or maybe I'm certain that there are people who legitimately don't have someone in their
life that they can talk to about this.
And it's wonderful that this exists for them, it really is.
Because this can cut down on a lot of problems.
This is not a topic in which you want to make a mistake, but it goes to show that we need
to talk to our kids more.
We need to make our children more comfortable when it comes to talking to us about anything,
about anything, because otherwise they're definitely going to get the information.
Now in this case, you have a medical organization that's providing it, but that may not be the
case with other topics, with other taboo topics.
We need to talk to our kids more.
And the funny part about this chatbot, to me, is that it is a project of a certain organization
that everybody always wants to defund because they don't agree with that kind of planning.
The reason I find that funny is because those people who are that dead set against this,
they're probably the type that don't talk to their kids.
Which means those who want to defund this organization, their children are turning to
it for advice.
That is hilarious to me.
That is a form of poetic justice that just cracks me up right there.
They do provide other services and this would be one of them.
A million.
A million in nine months.
It's wonderful that this organization put this chat bot out there.
It is also a blazing condemnation of our country.
Teens are turning to the internet, turning to a chat bot to get advice that they should
very, that they should feel comfortable getting from their parents.
if not from their parents, from the educational institutions that are around them, but less
than half actually provide it.
That's America today.
Education is important.
Learning is important.
It helps you make wise decisions.
This is not a topic that you want teens making decisions without all the information.
So hats off to the organization that put this out there.
Anyway, it's just a thought.
Y'all have a good night.