Well, howdy there, internet people, it's Bo again.
So today we're going to talk about money and politics,
because one tweet has just sent the world into a fit,
well, at least the US.
Representative Castro from Texas tweeted out
a list of people who had given the maximum donation, who
live in San Antonio, who gave the maximum donation
to President Trump's re-election campaign.
People are mad.
People are angry about this.
And I don't know why.
A, yeah, it's public information.
B, if you support a cause, why do you
care if it becomes public?
There are two reasons people donate to political campaigns.
A, they believe in the cause.
Or B, they're trying to create political favor.
If you believe in it, why do you care if it becomes public?
If you're just trying to create political favor because it's good for business, maybe
you need to understand that it can be bad for business too.
And people are angry.
Kevin McCarthy, he's the House Minority Leader, a guy who believes video games are responsible
for mass shootings.
He tweeted out, what happened to when they go low, we go high?
Concentration camps and hate crimes.
That's what happened.
If you are funding President Trump's reelection campaign, you're paying for him to go around
and call people invaders, to dehumanize people, to dehumanize our neighbors.
What you guys have been advocating for this whole time is that we need more vetting.
We need to know who our neighbors are.
Yeah, we do.
We need to know if they're going to pay to have that kind of rhetoric out there.
I think this is a great thing.
Personally, I think every candidate should have to tweet out who pays them.
Now when I said I was going to talk about this, somebody told me, you know, if you do
that somebody's going to release your contributions.
Okay.
Pencils of Promise, Hack the Hood, True Colors Fund,
Shelter House, Wounded Warrior Foundation,
David Sheldrick Wildlife Trust,
Yellowstone Wolf Project, Oxfam,
Habitat for Humanity Louisiana,
Girl Forward, Respiratory Therapists Without Borders,
Kaleidoscope Youth Center,
Gay and Lesbian Advocates and Defenders,
National Alliance to End Homelessness, ACLU,
Kurt Vonnegut Memorial Library, Planned Parenthood,
Americans for Immigrant Justice,
YMCA Project Guatemala, Honey Bee Conservancy,
National Coalition Against Domestic Violence and the Committee to Protect Journalists.
If you believe in what you're donating to, you don't care if it becomes public, especially
if they're doing good.
Why would you be ashamed of that?
This is what we've done.
By we, I mean you guys.
you buy one of our shirts money goes to one of these. We all are finding it. We all are
doing good. The only reason you would be worried about it becoming public is if you know it's
not doing good. So yeah, we need to know. In fact, that would be a great Twitter account
somebody to set up. You can get all the information off of OpenSecrets, by the way. Just tweet
it out. Let's see who's funding, who's paying who off. It'll be fantastic. Anyway, it's
It's just a fault.
Y'all have a good day.