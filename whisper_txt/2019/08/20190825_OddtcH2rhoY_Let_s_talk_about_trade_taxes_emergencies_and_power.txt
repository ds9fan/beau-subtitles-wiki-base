Well, howdy there, Internet people, it's Bo again.
So, we've got to talk about the trade war some more.
The President said that he had second thoughts
about everything.
That was interpreted to mean that he wished
he hadn't started the trade war.
That's not what he meant.
He does have second thoughts about everything
and third and fourth, judging by how often
his positions switch.
But what he meant was that he regrets not raising the tariffs higher.
He regrets not taxing you more.
And one of the things I've realized over the last few days is that most Americans do not
understand how tariffs work.
The president has no power to tax the Chinese.
That's not a thing.
He taxes American importers on Chinese goods as they come across the border.
Those importers pass that tax, that cost of that tax, onto the retailer, who then passes
it on to you? What the president said is that he regrets not taxing you more.
That's really what he said. That's really what it means. There's no other possible
meaning behind that. Another question that keeps coming in is can the
president use the International Emergency Economic Powers Act to simply
end trade with China? Our largest trading partner with a pen stroke. That has to be
unconstitutional. No, he can. He can. He does have that authority. These emergency
powers acts, these laws that came about in the 60s and 70s, they were designed
to deal with nuclear war. The entire eastern seaboard is gone. They grant the
presidency a massive amount of power. Now the idea was because Congress was gone
or he couldn't get in contact with them. He didn't have the ability to actually
legislate and use our constitutional system so they created an environs around it for the event
of massive catastrophes. That was the idea. This is kind of a byproduct of that thought process.
He does have that ability. What does that mean for us? The presidency has too much power. The
The president is too powerful, not just Trump, all of them.
We need to move to limit that.
That's why it seems like we're living in a dictatorship where a pen stroke can change
the lives of millions of people.
We're not living in a representative democracy because we subverted it.
There's never been a democracy in history that hasn't committed suicide, and right now
we've got the gun to our head.
We have to move to limit the power of the presidency, of the office, not just Trump.
During the next administration, it needs to be an overriding theme to rescind a lot of
the executive powers that the executive branch has.
We've got to get rid of them.
Otherwise, it's not going to be long before we go down that road.
somebody with Trump's attitude takes power and has the popular support to
enact this stuff. Doesn't have a resistance fighting against it. It can
happen and it will. We have to get rid of this legislation. I know people were
hoping for me to say no here's this bizarre constitutional no that's not in
In this case, yeah, he totally has this authority.
He can destroy the economy with a pen stroke.
He shouldn't, but he could.
Anyway, it's just a thought.
Y'all have a good day.