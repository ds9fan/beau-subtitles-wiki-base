Well, howdy there internet people, it's Beau again.
So tonight we're going to talk about grammar and pronouns and the history of the English
language.
Had a guy tell me today that he didn't think we should have to rewrite the entire English
language because of the personal preferences of one person.
Fair enough, fair enough.
But that got me thinking about something I knew but didn't really know.
So I started reading about the development of the English language.
So it started with Old English.
That's 450 to 1100.
It's when the Germanic people first came over mixed with the locals that were already there
And that's where that language came from.
We wouldn't really be able to read it today unless we were trained to do so.
Middle English was next, and that's 1100 to 1500, and that's the addition of French.
French came in because a guy named William the Conqueror from Normandy came over.
He brought French with him.
It became the language of the royal court.
There was a class divide.
The peasants used English and the ruling elites used French.
Then you get early modern English which is 1500 to 1800 and the vowels became shorter
and there was the introduction of more foreign words because of colonization.
Then you get late modern which is 1800 to now which is wider vocabulary because of the
industrial revolution and the technological revolution.
needed words to describe things like cell phones and then still more addition
of foreign words. So that's the history of the English language. Now in the
1700s, in the middle of 1700s, a fad developed called prescriptive grammar and basically
they were little books that told you the proper way to speak rather than descriptive grammar
which is the way people actually talk.
Now one of these books was written by a guy named Rob Lough, and he later became Bishop
of London, and he wrote a short intro to English grammar.
Shortly thereafter, a guy named Sir Charles Coutts expanded on this volume, and he included
his personal preference of using he and him and his as the generic pronoun.
In 1850, Parliament ratified this, codified it, turned it into law.
The rest of the time, for the overwhelming majority of the history of the English
language, we had a gender neutral pronoun, a singular gender neutral pronoun, they.
It's not actually rewriting the English language to suit one person's personal preference.
It's stopping that because that's what happened with the expansion because that book became
a textbook. So all of the ruling elites became trained in this manner and his personal preference
bled through the entire language. That's how it happened. So they, them, theirs, that's
actually the norm. That's the norm. For the overwhelming majority of the history of the
English language we had a singular gender neutral pronoun but I mean I guess
we can rewrite the entire language because of one person's personal
preference. Anyway, it's just a thought. Y'all have a good night.