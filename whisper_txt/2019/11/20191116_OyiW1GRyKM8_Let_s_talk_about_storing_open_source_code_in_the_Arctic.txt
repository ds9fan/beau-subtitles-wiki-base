Well, howdy there, internet people, it's Bo again.
So Microsoft is basically placing all of the world's open source code into a vault in the
Arctic.
You know, back in the 40s, at Oglethorpe University in Georgia, the crypt of civilization was built.
It's a vault, full of a whole bunch of microfilm.
kinds of stuff. It's slated to be opened in 8,113, 6,100 years from now. The reason
it was put together is because Dr. Thornwell Jacobs, when he was teaching
about ancient civilizations and researching them, he was just beside
himself at the lack of information so he wanted to put something together because the lack
of information stemmed from these civilizations falling.
Implicit in the project is the belief that one day this civilization will fall.
Now near the global seed vault in Norway is the Arctic World Archive.
The open source code isn't on disks or hard drives, it's on a microfilm-like substance
on giant reels.
Time capsules are unique in the idea that we're going to be sending information into
the future that hopefully they already know.
the reason we're doing it is because they may not. While some of the stores, some of the things that
are put into these vaults and capsules are useful, the open source code, encyclopedic knowledge,
stuff like that, useful, some of the care for humanity's future behind this dark acceptance
that we will one day fall, it gets lost with human arrogance. Right next to the open source
code in the Vatican archives is the secret sauce recipe for McDonald's.
The presumption that we would know what the future would want or need is the same arrogance that will
likely lead us to fall. Perhaps, given humanity's just seeming unwillingness to work together
to avoid a fall, maybe we should focus on building vaults all over the place, multiple
ones, that contain basic human information, the basic knowledge that humanity needs to
survive. If there is a collapse, the open source code on microfilming
doesn't do much good. But basic information about medicine, food
preparation, agriculture, that would. That would. There's a terrifying reality behind
these sites, and that is that world governments, large companies like
Microsoft, the Vatican, McDonald's foresee a world reset occurring within the next
750 years because that's how long that film lasts. Anyway, it's just a thought.
Y'all have a good night.