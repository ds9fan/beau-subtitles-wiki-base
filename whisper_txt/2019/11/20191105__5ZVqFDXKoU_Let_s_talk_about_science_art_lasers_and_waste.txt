Well, howdy there, internet people, it's Beau again.
So tonight, we're going to talk about human ingenuity,
science, art, waste, and lasers.
Something kind of interesting has happened.
I want to draw attention to it and how significant it is.
You know, nuclear power is one of those things
that if we could just harness it for real could alter everything.
Could alter everything.
And one of the big problems with it is the waste it generates.
And we've been dealing with this waste by not dealing with it.
Long-term storage facilities, just basically sticking it underground
and sacrificing that chunk of the earth for 10,000 years.
When we decided to go that route,
put panels together to try to figure out
how to convey a message of warning
10,000 years into the future, think about it.
10,000 years ago, we were painting in caves.
10,000 years from now, it could be an extremely advanced society that would know exactly what's
under there, or we could be painting in caves again, 10,000 years is a long time, a lot
can happen.
So these panels had to come up with ways to transmit the warning, don't come here, don't
go to this location and haven't understood 10,000 years in the future.
Some of the ideas were art, plain and simple, art, literal art.
One of the ideas I liked the most was to make the landscape just completely uninviting and
want to create a landscape of thorns, concrete thorns, giant ones, to make it unwelcoming,
scare people. Something out of a horror movie. They also agreed on a message. A rough idea
of what the message should be and they've put this message in various languages and
stuff like that. And this is the message. This place is a message and part of a system
of messages. Pay attention to it. Sending this message was important to us. We considered
ourselves to be a powerful culture. This place is not a place of honor. No highly esteemed
deed is commemorated here. Nothing valued is here.
What is here is dangerous and repulsive to us. This message is a warning about danger.
The danger is in a particular location. It increases toward a center. The center of danger
is here of a particular size and shape below us.
The danger is still present in your time as it was in ours.
The danger is to the body and it can kill.
The form of danger is an emanation of energy.
The danger is unleashed only if you substantially disturb this place physically.
place is best shunned and left uninhabited. That's the message we're
sending into the future. Ten thousand years from now that's what they'll get
unless a Nobel Prize winner is right. The physicist came up with this idea
idea, that you could use light, lasers, lasers, to take out a neutron, thereby completely
altering what it is.
Believes that 10 to 15 years from now they'll have it figured out.
Doing this would reduce the time nuclear waste is dangerous to like 30 minutes instead
of thousands of years.
It's amazing.
It is amazing.
It's one of those things we need to keep in mind.
As we see all the doom and gloom in the world and all the problems we face, understand human
ingenuity is amazing and we can come up with some pretty brilliant stuff given the resources
The problem is, we tend to waste our resources, we tend to use them for destruction rather
than creation.
Something like this, an idea like this is out there for every problem that faces humanity.
We just have to put the funding behind it, we have to give these people the resources
they need to make it happen.
trying to capitalize on everything. There's no profit in this. Well, in this
case there is. Some company can make a ton of cash rendering nuclear waste
inert. That's why it has funding. Think of all the things in the world that could
change it this significantly that there's no profit motivator for and
therefore there's no funding. Sometimes we might should research just to
research. Anyway, it's just a thought. Y'all have a good night.