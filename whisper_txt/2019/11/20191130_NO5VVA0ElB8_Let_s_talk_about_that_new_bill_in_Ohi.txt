Well howdy there internet people, it's Beau again.
We got some great news coming out of the state of Ohio tonight.
Amazing news.
Legislators in the House of Representatives in Ohio have figured out that they have a
new superpower.
They can introduce legislation that will literally change reality as we know it.
It's amazing.
I'm in awe.
Now admittedly, they're starting with something small.
They're just going to have doctors remove ectopic pregnancies from the fallopian tubes
and implant them in the uterus.
I don't know why doctors didn't think of this before, but I mean think about this.
Think about all those people trying to get pregnant that had to deal with this.
Well now it's solved.
It's great.
Now I know in a state with 1.7 million people dealing with hunger, 500,000 of them kids,
it seems like a weird place to start if you have a superpower, but I have no doubt that
very soon the House will legislate that unicorns will come down and drop food off to people.
In case you don't know, that's not a medical procedure.
That's not a thing.
It doesn't exist.
But a bill in Ohio suggests that doctors attempt it, and if they don't, well, they can be
arrested for a new crime called aggravated abortion murder, and it carries a possible
death penalty.
If we're going to propose legislation, I have something I'd like to propose.
Let's call it legislating medicine without a license and come up with a cute name for
the crime too, aggravated arrogance murder, because that's what this is, it's a combination
of arrogance and ignorance, otherwise this bill never would have existed.
And I know there's somebody out there right now, I don't know why you're getting all
worked up, so somebody stupid wrote a bill, introduced a bill, big deal, 21 sponsors,
A fifth of the House of Representatives in Ohio have signed on to it, 21.
Somebody else is saying, well, you know, I mean, maybe they just didn't read it.
It carries the death penalty.
I'm not sure I'd cop to not reading a bill that carries the death penalty.
Reading it and sponsoring it, well, that's ignorance and arrogance.
reading it, that's evil. I'm certain the people in the state of Ohio, there's a lot
of problems there. I'm certain they would rather legislators focus on the 1.7
million hungry people, 500,000 of them kids, than everybody seems to pretend
they care about. And the House would be a good place to start because we all know
the people in the House up there know how to feed themselves. That's what this
is about, right? Anti-choice campaign dollars flowing in. That's what this is, unless you
want to admit to writing it. Probably not. The sponsors of this bill. Candace R. Keller
from District 53, Ron Hood from District 78, the co-sponsors, Naraj Atani, John Becker,
Tom Brinkman, John Cross, Bill Dean, Timothy Ginter, Chris Jordan, Daryl Kick, Susan Manchester,
Riordan McClain, Derek Maron, Phil Plummer, Jenna Powell, Tracy Richardson, Craig Rydell,
Romanchuk, Todd Smith, A. Nino Vitale, and Paul Zeltwanger. This is your
representative. It does not matter if they are from your political party. You
got to campaign against them. You have a moral obligation to campaign against
them. These are people who sponsored a bill that will affect more than half the
population and they know nothing about the subject matter. Nothing. It carries the death
penalty if you don't do something that isn't possible. It doesn't matter if they're part
of your political party, you can't let this stand. You cannot let this stand. The state
of Ohio has a lot of problems. It's rotting from the head. This is everything that is
wrong with America's leadership. These are people that are willing to execute someone
over something they don't understand. They know nothing about. Literally willing to kill.
They know nothing of the subject matter and incidentally, this is the second time it's
come up.
You want to fix your state?
You got to get rid of these people.
You want to make America great again?
You need to get rid of everybody that has this level of arrogance, this level of ignorance,
much willingness to end a human life, while claiming to be pro-life, over something they
don't understand.
They know nothing about this, otherwise they wouldn't have sponsored it.
The alternative is that they were willing to put you at risk of execution without even
reading the bill.
anyway. It's just a thought. Y'all have a good night.