Well, howdy there internet people, it's Bo again.
So I got a question from a teacher and I'm going to read the question because it is fantastic.
It's amazing and there's a lot of sub-questions in it.
Bo for the boy and the pledge, as a teacher I am torn about whether it's okay that the
boy refused to leave the room with the principal such that the police had to be called.
To me, with the witnesses he had and the knowledge that his parents had spoken to the school,
wouldn't going have helped his case against the sub and been less disruptive?
Or am I wrong for thinking compliance at that point makes him look more respectful?
Given those teachable moments, I want to be ready to teach my kids to self-advocate the
right way. And right is in quotation marks, which I love. This is amazing, okay.
So the first thing that we have to assume is that he didn't have a
teacher like you. That's the first thing we have to assume. We have to
assume that he did not have a teacher who was interested in teaching her
students how to self-advocate for their rights. That's not common. And then let's say devil's
advocate that he did have a teacher that did this and trained him in how to self-advocate
correctly whatever that may be. What are the odds that he's going to remember his training?
I don't know that it's fair to think that an 11-year-old boy would remember his training.
It seems that there were three adults in the room that couldn't remember theirs.
These situations are tense, they're high pressure.
Now as far as what the right way to self-advocate is, I can throw you in a room with activists
who are on the same team believe in the same cause and you can watch them slaughter each
other over this question.
I personally don't think there is a right way.
I believe in diversity of tactics.
You do whatever is most effective.
There's no template.
The three main schools of thought are noncompliance until you've proven your point, which seems
to be what you're suggesting here, non-compliance until you force the arrest and therefore get
the lawsuit to change policy or direct action, which in this case would end up being violent.
The I have to assume I'm going to go out on a limb here and say you're in a public school.
I don't think this is your teachable moment.
Number one, it's too complex.
They're going to get this once.
It's not going to be reinforced.
It's not part of the curriculum.
So you've got to give them main ideas.
The other reason I really don't want you doing it is because we need you in the school system,
not the unemployment line.
I think your teachable moment is simply teaching them that they can self-advocate for their
rights that they can stand up for themselves and just teaching them what their rights are.
A lot of people don't know and if you don't assert your rights, you don't have any.
That's the reality of it.
But I want to thank you and oddly enough, I want to thank Lakeland PD.
Because this question made me realize something.
People like me, one of the things we preoccupy ourselves with is how do we create the next
generation of people that will look at an injustice and say, no, not today.
How do we create that?
It wasn't until this message that I realized we don't have to.
like Lakeland PD are going to do it for us.
Certainly this kid, his resolve is steeled now.
And every child in that class that watched this has now seen an over reactive government
That creates people that will get involved.
That creates defiance.
And I love that, I'm not going to say your name, don't worry, but you're not right next
door to this place.
This has turned into a national conversation.
Because of an injustice in Lakeland, Florida, students all over the country, assuming there
are more teachers of your caliber, are going to learn about their rights.
And they're going to learn about the kid in Florida who took a stand.
Now as far as appearing respectful, there's no way to do that.
There is no way to do that.
It doesn't matter how, what form of activism you choose.
You're going to make somebody mad.
The best example I can give is the movement from black Americans to have cops stop shooting
them while they're unarmed.
Seems like a pretty basic request, but don't stand in traffic, don't sit down during a
song, don't hold a sign, don't petition, don't give speeches.
There's always going to be somebody that's going to say whatever you did was wrong.
The question is, was it effective?
Most activism is about drawing attention.
Sometimes being a little disrespectful is the way to do that.
Sometimes it's not.
It's a real gray area.
Like many things of value, you determine your own level of involvement.
You determine how far you're willing to take it.
The individual does.
What those kids need from you is to just let them know that they can.
Just let them know that it is okay to advocate for yourself, to stand up for your rights.
That Big Daddy government isn't always correct.
And you owe your allegiance to nobody except those you want to give it to.
Anyway, it's just a thought.
y'all have a good night.