Well howdy there Internet people, it's Bo again. So it looks like we got to talk
about that shooting in Houston again. You know, the one where the police union
president came out afterward and said, don't you talk bad about our cops, you
know. Don't stir the pot against our officers. We got your number. Kind of
making veiled threats. Well now we know why they didn't want anybody talking
about it. Turns out that warrant, the affidavit used to get the warrant, well
Oh, it was entirely fabricated.
It's what the latest reports are saying.
The CIs, the confidential informants who were said
to go and buy the heroin from this house,
oh, that never happened, wow.
Oh, and the officer, one of the officers,
he had heroin in his car, crazy.
That's all public now.
Now I need to do a little bit of a disclosure notice and explain something before we get
to the next part.
In 2016, Houston Petey targeted an independent journalist, even had him under surveillance,
wound up bringing him up on trumped up charges, which of course were eventually wiped out,
but that made his life uncomfortable for a while.
Now I'm sure you're familiar with the concept of the thin blue line.
Many journalists work the same way, especially when it comes to a government agency targeting
someone.
When that happened, every investigative independent journalist in the country started cultivating
sources and contacts within the Houston Police Department.
So let me tell you what I've heard, but first we're going to talk about the public face.
The public face, the chief of police, is out there saying, don't paint us all with a broad
brush, isolated incident, a few bad apples, same song and dance we always hear.
And then the same song and dance we always hear about the victim, demonizing them.
Don't forget, you know, we didn't do this willy-nilly, there was a 9-1-1 call of somebody
who was maybe the mother of somebody who might have done heroin at this house maybe.
And they had drugs, a little bit of marijuana, a little bit of coke.
And they had guns in Texas, two shotguns, a Winchester and a Remington, not the weapons
of a drug dealer.
That's the public face.
Now, from what I understand, maybe they were, the private face is a little different.
the private face is telling people to tear their mother cases apart and that's
what should be done but they're not just doing it to the officers involved in the
raid they're doing it to the entire division 200 people doesn't seem like
they believe it's an isolated incident the same time as I'm criticizing and
keeping a private I want to give the chief of police credit for at least
attempting to look into it. That is the right thing to do, but you don't need to
do it. You need to bring in the FBI because there's going to be a whole
bunch of questions you don't even want to ask, much less know the answer to. From
what I understand in this division, you are going to find out that you have a
lot of professional lawful investigators. I'm not going to say ethical because I
don't believe in the war on drugs, but they're operating within the law. You've
You've got a lot of officers who have seen one too many episodes of The Shield.
If you actually do an investigation, that's what you're going to find out.
Let me give you some questions that you're not going to want to ask.
Where did the heroin come from?
Did it come out of the evidence locker?
Or are those officers out there shaking people down?
What about the CIs?
Are they paid?
informants are they paid? The ones that were mentioned in this, they didn't do
anything though. Where'd the money go? Certainly this guy didn't pay him to do
nothing. If he did they probably would have said they did it. There's probably
gonna be a whole slew of charges come out of this if you actually want to look
into it, but you need to bring somebody in that doesn't have loyalty, that doesn't want
to cover for their buddy, that isn't part of that thin blue line.
And please stop demonizing the victim.
nilly, you and I both know that that 911 call was not enough to get a warrant for a no-knock
raid.
That's where that marijuana and coke, that's where that evidence came from.
In court that would be suppressed.
You have no problem talking to the press about it.
Something that wouldn't be allowed in a courtroom.
using to slander dead people? Really? And then, there is one other question I have.
One of these officers had heroin in his car. What makes you think he didn't have pot and
coke.
Anyway, it's just a fault.
Y'all have a good night.