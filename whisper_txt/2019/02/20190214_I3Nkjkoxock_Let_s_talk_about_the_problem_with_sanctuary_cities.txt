Well howdy there internet people, it's Bo again.
So today we're going to talk about the problem with sanctuary cities because something happened
here in Florida that just has me fuming.
And just so you know, this is what the cops say happened, not me.
This guy, he's driving along and a couple ask him for a ride and at first he says no
because they're shady and eventually he lets them in or they get in and then he agrees
to give him a ride. They stick a gun to the back of his head and make him drive to this
trailer where of course there's a whole bunch of them at. They take him inside, tie him
to this chair, blindfold him, beat him, torture him, steal his money, steal the tools out
of his truck, take his keys, threaten to kill him. I guess a female with him apparently
told him that if he said anything that she'd say that he raped her.
But thank God they decided to let him go.
They don't kill him.
But before they let him leave, they make sure to tell him that he can't go to the cops because
he's illegal.
Yeah it was one of the white people that did that to him.
That story didn't go the way you thought it was going to, did you?
And that's why sanctuary cities exist.
Because if the cops, local cops, have to run everybody that they won't run into through
ICE's database, I mean let's just set aside the whole fact of local law enforcement walking
around like the Gestapo asking for papers.
It means that victims of violent crime can't go to the cops without fear of being deported.
Makes them less than human.
Removes all of their protections.
Beat them, rape them, rob them, doesn't matter.
They're not real people.
They can't go to the cops.
Problem with sanctuary cities is that they even need to exist.
Now a lot of local departments, they don't want to do it because of the liability involved
with it, because ICE's database is not entirely accurate.
We just had a case here in Florida where a 50-year-old man violated probation because
he failed your analysis.
Now being the violent criminal that he is, he went and turned himself in.
Where he finds out he has an ICE detainer and he's slated to be deported to Jamaica.
And he tells them, you know, I'm a U.S. citizen, I've only been to Jamaica once on a cruise.
And they hold him for three weeks before they find out ICE's database was wrong.
Three weeks, papers please.
Then we just had a veteran get picked up by ICE, wearing his dog tags, carrying his military
ID with his passport on his person, and they held him anyway.
Also a U.S. citizen.
Local departments don't want to be exposed to that kind of liability.
The smart ones anyway, the ones ran by non-racists.
And I know, I know, you're saying that if we have these sanctuary cities, well then
all these undocumented workers will come there and they'll set up these little communities
and they won't assimilate.
And I can understand that argument, because one county over there's a trailer park.
And it is full of these people.
They barely speak English.
They all work under the table, while collecting disability, selling drugs, they got that flag
flying in the middle of it, it's not the U.S. flag, it's the Confederate flag, also
a bunch of white people.
And it doesn't raise violent crime either, that's the other thing.
statistically proven, illegal immigrants commit less violent crime than native-born.
But that also, that right there, has a lot to do with why there's a war on immigration
all of a sudden.
You've got these private prisons that know marijuana legalization is coming.
Because it certainly seemed like they're pushing to have that war on drugs shift to
a war on immigration.
Because let's be honest, they're in it for money.
They don't want to house violent people.
If you can't get the money for housing non-violent drug users who never harmed anyone, non-violent
immigration violations who never harmed anyone seems like a good place to make that money.
The problem with sanctuary cities is that they're not every city.
That's the problem with them.
No local department should be enforcing laws outside of this jurisdiction.
The feds took control of immigration.
They decided it was their power, and I'm not even going to get into that, that they took
that power from the states.
They took that power, they took it over, it's their responsibility, let them deal with it.
There's no reason for your local deputy or your local cop to walk around like the Gestapo
and further enforce the idea that undocumented workers are subhuman and have no rights.
Anyway, it's just a thought.
Y'all have a good day.