Well howdy there internet people, it's Bob again.
Yeah, we're gonna talk about national emergencies tonight.
I've tried to make this video a few times throughout the day.
First time I got mad, and it was not fit for public consumption.
Second time I was like, well I'll just make a satire video.
Make a joke out of it.
And I got my vest and my gas mask, my helmet,
reporting live from the national emergency.
But this ain't funny.
This isn't funny at all.
at all. This is dangerous. It's really dangerous. I'm not sure people grasp it. This is how
every democracy, every Republican history committed suicide. It's how it happened.
Some made up, trumped up, pardon the pun, emergency. This is made up. Border apprehensions
are at a 41-year low, there is no crisis, there is no emergency.
If Trump was smart, he would have just taken credit for that, but he didn't.
He wants a monument to himself.
And I know there's memes floating around talking about the precedent it's setting
and the different things that could be done when the Democrats take power.
But they're not really addressing the real issue because a lot of things they're pointing
out, most Americans are like, yeah, well, we kind of need to do something about that.
The real problem is that this is a direct challenge to the legislative branch.
There's a reason our government is set up the way it is with three equal branches of
government. No one person should have this much power. This is dictatorial rule. This
was sent to Congress. It was rejected. And he's doing it anyway with a pen stroke. It's
unnerving. It is unnerving. And yeah, there's memes that say, well, we could do this or
this. That will explain how, though, and I think that's an important piece of this.
National emergency, as other presidents have found out when they've declared
them, and other presidents have declared national emergencies, there's some still
active. They're not quite like this, though. See, a national emergency can't
create legislation. It can't create law. It can only modify. It can only help enforce.
Let's take guns for example, because that's going to be near and dear to most of the people
wearing red hat. It's going to be near to what's left of their heart. If they wanted
come after guns, they'd have to find a law. They'd have to find some way to justify it, right?
And you got a right. You got a second member right to own a firearm. You do, but that right
isn't absolute. The same statute that prohibits felons from owning a firearm also prohibits drug
addicts. Well, it's kind of like immigration law, isn't it? Real hard to enforce. So they need a
wall they need a metaphorical wall filter people out your analysis sounds
like a good plan so that just take a little money from DoD start randomly
testing people you don't show up you lose your guns and since you know there
are some drugs that can be out of your system in three days they'll only give
you two days notice hope you don't have to work but see they don't even have to
do that. They don't have to come after the guns. They can just come after the ammo. Seize
the factories. Nationalize them. We have an ammunition shortage, but there's not one.
There's not a border crisis either. All they have to do is say it and convince enough people
that it exists. So they just seize the factories. I hope you have been saving your brass and
know how to reload. Well, they can't just seize property. Do you think the government
owns all the land along the border? With that pen stroke, he's stealing land from Americans
at the point of a government gun. They're just going to take it.
Climate change, they could do something with that. Have to frame it in a national security
setting though. You know, most of our military bases, they're on the coast.
Rising sea levels buddy. We need some new land. Your land. Give it up. You're
supporting it now. Forced vaccinations. That's easy to frame. Let's say
hypothetically some idiot pulled us out of a treaty dealing with short and intermediate
range missiles.
Well, we're at a greater risk for biological warfare, forced vaccinations.
Anything can be framed in a national security setting.
And right now, there's probably a bunch of Democrats out there going, well, I'm kind
OK with a lot of this.
OK.
You know, the education gap between us
and other countries, that's becoming a defense issue.
We don't have smart enough people.
So we're going to take all the schools
and put them under DOD control.
Not such a good idea.
There's a reason no one person is
supposed to have this much power.
And I know, I know, let's just go ahead and save the comment section on this.
If they came after our guns it would be civil war.
Well Sparky, if the only time you're willing to fight back and you're willing to resist
is if they come after your guns, they don't need to come after your guns.
They can take everything worth fighting for away, and you'll still be there clutching
your AR.
And that's the thing that bothers me the most.
The people who ideologically are supposed to be just losing their minds right now.
The three percenters, the oath keepers, the strict constitutionalists.
There should be a march on Washington from you guys.
is y'all are all wearing red hats. You're so caught up in partisan politics, you don't
see what's happening. Pandora's box is being opened and it's extremely dangerous. It is
extremely dangerous. The average American needs to start leading themselves. Forget
about what the government is doing. We don't have leaders anymore. We certainly don't
have one in the Oval Office. We have a ruler. This is how every democracy in history is
committed suicide. If the Senate and House of Representatives, the Supreme Court, do
Do not put an end to this immediately.
That's it.
It's over.
The precedent is set.
And you will see more and more power transfer to the executive branch until we are just
another 2-bit dictatorship.
MAGA.
Y'all have a good night.
It's just a thought.