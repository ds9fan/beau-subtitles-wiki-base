Well, howdy there, internet people, it's Bo again.
I'm a little under the weather, so I was sitting around
watching YouTube videos, and even though I wasn't gonna
make a video today, and looks like I'm going to,
because I found a video from Pure TV, had my picture on it.
I immediately was like, a Nazi wants to talk, let's talk.
So I'm ready to jump in the comments section,
I get there and find out it's not that,
it's a black guy critiquing the reparations video.
And he broke it up into two parts.
I watched them.
And I love the way he did it because he did it
conversation, let's talk about it.
And I think doing it via video, my response like this,
will help other people jump in on the conversation.
Um, so, the first thing brought up the $100 billion.
Yeah, that's not the value of slavery.
The value of slavery is definitely way more than that.
That's just the largest number I've ever seen
tossed out in a plan by a politician.
So again, I'm one of those people.
I want results, and if this is the biggest number that's
been thrown out, we need to start there.
And when people threw out the $100 billion number,
white people freaked out.
However, the tax exemption thing, I like it.
But as you said in your video, maybe a tweak.
So I want to propose my tweak, because there's
issue with it that I see. The median black household income is right at 30
grand, okay, and at 30 grand that's the point where you hit zero federal income
tax, your effective tax rate after your deductions and everything is nothing. So
you end up paying nothing or getting money back and the less income you have
the more money you get back. You actually will end up getting more back
than you paid in. So the idea of tax exemption just pulling you guys out of
of the system, it's going to hurt the people at the bottom, it's going to hurt the people
that need it most, it's really going to help people that are making money already, but
those people at the bottom, it's going to hurt them.
So rather than tax exemption, what about a tax credit, a refundable tax credit more importantly?
Doing it that way, if they're making 200 grand a year, yeah, it's benefiting them.
If they're at the point where they're already paying nothing in taxes, that income tax refund
is even bigger and they would get it year after year.
Now, the other part, I mean aside from it actually creating benefit, the other part
I like about this is that it's doable, you know, if we want to create results, the first
thing we have to do is get the legislation enacted, okay?
dollar reparations funds, that's going to be really hard to sell politically.
Tax credits?
Did white people hate taxes?
You would find allies in all kinds of weird places.
There are whole groups of people that are predominantly white that would be opposed
to any form of reparations, but if you phrase it as tax credits, taxation is theft, man.
Of course they need to, of course do that.
You will find people that would march against you in the street supporting this just based
on the idea that it's cutting down on government extortion, which is how it's viewed.
So that from a grassroots level, that's going to be something that can be done.
I mean, aside from the effectiveness of it, which I do believe that that would be pretty
effective. It's something that can be accomplished and that's one of those
things that a lot of the reparation plans I've seen I'm like that's gonna be
really hard to sell any time in the next 60 years. This is something that if with
the right amount of organizing could be done pretty quickly. So it again it's
just a thought. I appreciate the way you critiqued it and turned it into a conversation, which
is the whole point of my channel. So I really do appreciate that. Anyway, just a thought.
Y'all have a good night.