Well, howdy there, internet people, it's Beau again.
You know, all over the southwestern United States,
you can find these spirals that are carved into the side of cliffs or painted on the side of stones or whatever.
Some are clockwise and some are counterclockwise.
They're marks of this ancient hero's journey, quest.
And it's a journey that is very, very relevant today.
And while I love reading Old Wisdom, I heard this story about a decade ago around a campfire
not entirely unlike this one, and everyone had had a little homemade wine in their system.
So if I get some of these details wrong, forgive me.
It's the Hopi creation story.
And the way it goes, after they emerged from the earth because their world was destroyed,
They found themselves here, and I know that sounds crazy, but understand it is widely
accepted today that the Hopi are descendant from the ancient Pueblo, you know, people
who carved their civilization into the earth on cliffs.
So if that civilization collapsed, how would you describe it?
Once they emerged, they met the caretaker, and he said they could live here, but they
had to protect it and make sure the things that destroyed the last world, like greed,
didn't occur here.
They had to find balance, and he sent them on this quest to find center.
And at center, well, that's where they should build their society.
So they set out in different directions, swearing to get to know the earth with their toes.
And they marked their path with these spirals as they went.
Some clans made clockwise spirals and some made counterclockwise spirals.
I love that story.
I love that as a creation story.
From the very beginning, man was seeking center, trying to find balance.
Today every wise person is seeking the same thing.
Every relationship, every society, every civilization is still seeking that.
Given my views, it is not entirely lost on me that the migration ended in the southwestern
United States.
I think of future people on a hero's journey because their world was destroyed who at some
date in the near future will probably emerge from the earth out of a tunnel and meet the
caretaker.
I wonder if the caretaker of tomorrow will be as hospitable.
Wonder if he's only going to ask that they protect the earth and try to find balance.
Anyway, it's just a thought.
have a good night.