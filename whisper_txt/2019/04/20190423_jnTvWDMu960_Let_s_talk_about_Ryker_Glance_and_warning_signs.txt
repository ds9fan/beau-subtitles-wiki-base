Well howdy there internet people, it's Bo again.
So tonight we're going to talk about a young boy named Ryker Glantz.
He's two and according to reports, his mom and dad got into an argument.
In that argument, his dad took a pistol, stuck it to his head and pulled the trigger.
weapon malfunctioned. Mom grabs him, runs outside, sticks him in a car seat. About
the time Dad comes out with a shotgun and fires. It hits him, according to reports,
took off half his face, and he survived. He's currently in critical condition, but
But he has survived.
Reason I'm bringing this up is because in a lot of domestic abuse situations it comes
out of nowhere.
We don't know what happened.
I don't know what happened in this situation.
But a lot of times there are warning signs, but they seem mundane.
They can handle it.
And then all of a sudden it hits a boiling point.
Key is to get out before then.
There's always a way out.
There are shelters.
There are organizations that will help you get in touch with them.
There are ways.
What are the warning signs?
Jealousy, possessiveness, unpredictability, bad temper, obviously, punching walls, that
kind of thing.
animal cruelty, verbal abuse, extreme controlling behavior, and this goes down to the way you
present yourself to the outside world, the way you dress, all of this, antiquated beliefs
about the role of women in the world, victim blaming, it's always your fault, sabotaging
Limiting the victim's ability to work, earn money, get an education, these types of things.
Sabotaging birth control.
Complete disregard for the other's drives, whether they want it or not, it doesn't matter.
Controlling all the finances.
Limiting your access to funds.
Accusations of infidelity and of course harassment at work.
These are the warning signs and left unchecked, they always end bad, it always ends bad.
Now we don't know what happened in this situation, but regardless of how this went down, it's
the perfect opportunity for everybody to kind of take stock of these warning signs, make
sure you know what they are, and when you start to see them get out early, get out early.
ago fund me for the for the mom so she can be with Ryker and Ryker son shotgun
blast to the face and you survived you win every tough guy competition forever
All right, that is amazing anyway, it's just a thought y'all have a good night