Well, howdy there internet people, it's Bo again. So tonight, I want you to close your eyes
I'm going to describe the situation and I want you to picture yourself in it
You're in a country and the graduates coming out of school, they're worried they're not going to be able to get a job
The economy's changed so much. They're not portrayed very well by the media
There's been attacks on freedom of the press, assembly, speech
Intellectuals are demonized. The government is blaming foreign interference for everything.
Of course, predictably, a protest happens. Picture this protest at whatever large meeting area there is in your country.
If you're in the US, picture it at the National Mall, you know, where the Washington Monument and the wall and
where Forrest Gump gave his speech.  speech. You join, you show up, because somebody has got to hear this. Somebody's
got to do something.  A weird thing happens, that protest, it grows exponentially and it doesn't disperse. It's there
day after day. Smaller protests all around the country. Eventually there's a million people
there. Logistical problems start to set in. Food, water, hygiene starts to stink.
There's infighting among the leadership of the movement, you know. People vying
for power. Some of it is just egos. Some of it is the work of intelligence
agencies, your own trying to disrupt the movement and foreign intelligence
agencies because they smell an opportunity. Government response happens, tanks,
armored personnel carriers, quarter million troops, and they're making their
way to you. Suddenly this amazing thing happens. The people in the surrounding
suburbs, they stop them from reaching you, mostly through nonviolent means, they block
the roads, they stop traffic, they do whatever they can.
The tanks don't reach you.
You've got popular support, but that infighting is still happening.
So that window of opportunity is missed.
You're still there, surrounded by troops.
You know what's coming, you're not too worried because you're begging for reform, not
revolution, but you know they're out there, so you're looking to that leadership, looking
for somebody to tell you what to do.
Then the government comes back, and this time they make it to you, and you're facing Type
59 tanks, Type 56 rifles, they kill hundreds, maybe thousands, we'll never know.
And the weird thing happens, 30 years later, somebody on the other side of the world tells
this story in hopes of making a point.
This happened, Tiananmen Square.
Now the part that Americans think of as Tiananmen Square, that's still a few days from now.
But the protest went on a really long time, the occupation.
We don't really think of it that way because we have a very narrow scope of what went down.
I really wanted to talk about this, but there's so much that parallels what happens in the
U.S., I didn't really know where to take it, to be honest.
If you are politically active in the United States, you need to read about this in depth.
So I thought to the one question I always get asked by young activists, what do I do?
How do I get started?
And I have kind of a canned answer of, you know, you do what you can, where you can,
when you can, for as long as you can.
And then I thought about the most iconic image that came out of Tiananmen Square, Tank Man.
I actually had this picture on the wall in my office for a long time, had a high-pressure
job, and any time that pressure got to me, I could look up, and there's that guy standing
in front of that tank.
That's pressure.
There he is, standing in front of that tank.
There's more tanks behind it.
Again, narrow scope that we know.
In the photo that is most popular, there's four tanks.
out, tanks were running all the way down the street, and he stopped them all, one guy.
You could tell by the way he was dressed and what he was doing, he wasn't there to be a
revolutionary that day, he just saw something, he's like, somebody's got to do something.
And then somebody did, he did, he stood out there in front of that tank.
If there's ever a metaphor for the power of the state and the implied violence of the
state versus the individual. It's that image. It is that image. And he's standing there
when the tank, you know, tries to go left, well he steps right, gets in its way, stops
that tyranny. If it goes right, he goes left. It's probably a metaphor there too. Eventually
Eventually he gets that tank stopped, and the other tank's behind it, and he climbs
up on it, starts yelling at the people inside, telling them what they're doing is wrong.
He gets off of it, and they wind that tank back up, and he steps back out in front of
it again.
That is, that's courage.
And he stops it again.
Now eventually two people run out, they grab him and drag him off.
Now for the longest time, we thought that was Chinese secret police who grabbed this
guy who was doing, well, what he could, where he could, when he could for as long as he
could.
Turns out Chinese records kind of indicate that they got no clue who he is.
certainly didn't catch him. There there was one guy who said that well we think
we got him but we're not sure. There's a bunch of theories. One is that he made it
out of the country and that he's an archaeologist. Another is that he's still
in mainland China doing what he can when he can where he can. Obviously that's a
version I like. But the thing to take away from this is that a million people with popular
support, they were all waiting for somebody. They were all waiting for somebody. You are
They were all waiting for somebody.
You are somebody.
Anyway, it's just a thought.
Y'all have a good night.