Well, howdy there, internet people.
It's Bo again.
We've got to talk about something serious tonight.
Apparently, the US government is planning on teaching
American kids Arabic numerals.
I want to know why.
All over Facebook, there's polls going on about it.
The good thing is American people are starting to wake
up, because it's 60% to 85% saying, no, we don't need to
learn this. And I agree with that. I don't know why we can't just use the numbers we
use today. I mean, when you really think about this, why would we need to do that? I mean,
that's kind of scary. This is obviously, you know, it's a plot. It's got to be. It's got
to be a plot. I mean, and then you hear them talking about it saying that, you know, it's
the basis of mathematics and science everywhere, that's just that
taquita that they use when they're talking. I mean this has got to be a plot
from that Al Jaber group. It's how they gonna bring about Shakira Law. If you
don't know that the numbers on your keyboard are Arabic numerals, that's okay.
There's nothing wrong with that. There's nothing wrong with not knowing
something however basing your opinion off of one word and not actually knowing
the context of what is being asked that that's that that is bad especially when
it is supposedly something that is going to influence policy the numbers we use
are Arabic numerals. That's what they are. But because it has that word Arabic in it,
the immediate reaction from people was no, no, it's got to be bad. Because the
propaganda has gotten so thick since 9-11 that everything associated with the
Middle East is seen as horrible, it has to be evil.
We need to walk that back.
We need to walk that back and we need to do it quick.
Generally speaking, any time the question is, should Americans learn X?
The answer is yes, always, always.
The fact that we have become a nation where knowledge is looked down upon, where gaining
knowledge of any kind is looked down upon, it's a bad sign.
It is a bad sign.
The Middle East has contributed a lot to civilization as a whole, and disregarding something simply
And because it's Arab in origin, it's ridiculous.
It's a genetic fallacy.
It is how we end up with a society where 60 to 85 percent of people answer a poll they
know nothing about.
That's how we got here, was by saying, no, we don't need to learn that.
good is that going to do us? Probably a lot. Education is never wasted. Knowledge
is never wasted. So anytime this topic comes up, the answer should pretty much
always be yes. Yes, Americans do need to learn this. Anyway, it's just a thought.
Y'all have a good night.