Well howdy there internet people, it's Bo Ginn.
So tonight I want to talk about tear gas and toddlers.
Cause that's a thing now.
You know, we're the United States.
We're no stranger to violence.
You go on social media, you find people justifying violence over anything.
Step on my flag, I'll step on your face.
Don't stand for the national anthem. I'll break you knee and give you reason not to stand.
What would you do if somebody tear gassed your kid?
Seriously, what would you do if while you were trying to get
somewhere safe, somewhere better, some thug used a weapon
banned in war on your child?
What would you do?
How would you respond?
So I guess if we don't have a pile of Border Patrol bodies by the end of the week, and
I mean a pile, mass funerals of those flag-covered coffins, I guess they are pretty peaceful people,
aren't they?
More peaceful than us.
You know, I know everybody likes to pretend they understand that part of the world, but
the areas that these people are currently in, they're not dominated, but the cartels
have a pretty heavy presence in those areas.
You don't think they're walking around right now?
Here, have a gun, protect yourself next time, because every dead Border Patrol agent is
a win for them.
So they got the means, they have the opportunity, and they certainly have a motive.
My question is what are you going to do when it doesn't happen?
Are you going to admit to yourself that that fear monger in the Oval Office has turned
this entire nation into cowards?
afraid of toddlers. We tear gassed kids like any other two-bit dictatorship.
You know, people are wondering right now, well, why are the president's poll numbers falling
among the troops? Because they don't like seeing their government behave like Saddam's.
Sometimes, we tear gassed kids.
If there is not a massive upsurge in violence along the border, it says a whole lot about
those people.
Says they're better than us because I can't say we didn't earn it, anyway, it's just
a thought.
Good night.