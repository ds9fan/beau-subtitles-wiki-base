Well, howdy there, internet people, it's Bo again.
We're inside today because it's raining
and I gotta burn something.
It's really important I get a burn today.
Now, I don't own Nikes
because I don't like the company for a whole bunch of reasons,
but I don't force my views on my son.
So he's got some Nikes and we're gonna burn his.
Dad, no!
Boy, you know how important it is that I burn these today?
If I don't burn these shoes,
then the Skycloth ain't gonna create any liberty
next time the magic song plays like you don't know anything about freedom or
even basic witchcraft. I taught you better than that. In case you can't tell
I'm not really gonna burn any shoes cuz that's stupid. That's that's that those
are the actions of some man child who's emotional and entitled and too stupid to
really think about things. If you're that mad and you feel like you can't wear a
pair of Nikes because of the fact that some multinational company did a
commercial you don't like. Take them and drop them off at a shelter. Maybe give them to
a homeless vet, you know, those people you pretend you care about. Take them and put
them in a thrift store near a post, you know, near a military base so those enlisted guys
who were on food stamps so maybe their kids can get some decent shoes. Just a thought.
Now I got some questions though. When you're burning these shoes, are you trying to insult
the kids in those sweatshops that made them all those years,
the slave laborers in Uzbekistan picking the cotton,
or you're just mad at the people on top.
It's a symbol, right?
You're just mad at them.
You don't really care about the worker.
So you're gonna burn a symbol with them.
You get where I'm going with this?
Sounds a whole lot like what all those boys
that have been burning the American flag say.
It's a symbol.
way to draw attention to another cause. Man. So maybe you should think about that just
a little bit. But I do have another question. It's really more of a statement. If you're
mad about this and you're willing to disassociate with Nike because of their commercial with
the guy who milled during the magic song, but you don't have a problem with sweatshops,
slave labor that they've used all those years, it's what built that company. You
don't really get to have an opinion about freedom because you don't know what
it is. It's one of those things. You're loving that symbol of freedom more than
you love freedom. Y'all have a good day.