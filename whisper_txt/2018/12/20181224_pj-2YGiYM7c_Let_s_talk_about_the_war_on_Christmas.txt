Well, howdy there, internet people, it's Bo again.
So today, we're gonna talk about the war on Christmas.
I know a lot of people are gonna think this is a joke video.
It's not, there is a war on Christmas.
What is Christmas?
This is the day we celebrate the guy who said,
love thy neighbors, thyself.
And then, he was asked for clarification.
And he told this story.
And the story was there's this guy traveling from one place
to the next, and he gets overtaken by robbers.
He's stripped naked, beaten, left for dead.
And along walks a priest.
And the priest sees him, averts his eyes, crosses the road,
gets away from him, because he didn't
want to have to deal with it.
Kind of the way a lot of folks do homeless people today.
And then along comes a Levite.
Levi does the same thing and then along comes a Samaritan and Samaritan picks
him up bandages him puts him on the donkey takes him into town to a hotel
where he pays for the guy's room and he tells the innkeeper that he'll be back
and that if this guy needs anything get it for him I'll pay you back for it when
to get back. Love thy neighbor as thyself. How did a whole lot of people who call themselves
Christians spend the run-up to Christmas? Raising money to pay for a wall to make sure
that that traveler never gets here and that we certainly don't need to avert our eyes
is because we don't have to see him.
There is definitely a war on Christmas.
He also said that you're supposed
to love in truth and deed, not word and talk.
So when that cashier looks at you and says, happy holidays,
and you say, it's Merry Christmas,
Be loving in truth and deed, or word and talk.
Seems like word and talk to me.
The idea is that a Christian is just
supposed to be pouring out with so much love
that people who aren't Christian look at them and like, man,
I want to be like that person.
That's the idea.
So when you take some cashier task,
because he or she wished you a happy time.
Do you think people look at you and say, man,
I want to be like them?
Or do you think they want to avoid you?
There's definitely a war on Christmas,
but it doesn't have anything to do with holiday
cups at the coffee shop.
Anyway, it's just a thought.
Now, there are a lot of people in this country and there are a lot of different religions.
A lot of them have holidays right around now.
So from my family to yours, happy holidays.
Y'all have a good night.