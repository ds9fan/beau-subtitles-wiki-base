Well, howdy there, internet people, it's Bo again.
Happy Halloween, almost.
So let's talk about Halloween costumes.
Yeah, you can.
If you want to, you can dress up in blackface.
You can dress up like sexy Pocahontas.
You can dress up like Trayvon Martin or Adolf Hitler
or any other offensive thing that you can think of.
You can do that.
but it doesn't mean that anybody has to associate you with you afterward.
It doesn't mean that anybody has to employ you afterward.
Doesn't mean that your school can't throw you out.
You know, I don't understand the shock when people do something like this and
then there's a backlash and they get fired.
You intentionally are setting out to do something offensive.
And then you cry when people get offended.
Man.
Anyway, seems a little odd.
Here's the thing, being offensive and edgy is not actually a substitute for having a
sense of humor.
It's really not.
And see, and that's the thing.
And it's normally guys my age.
Back in my day, people could take a joke.
Right?
See, here's the thing.
You can pull that with the young kids.
But see, I was alive back then.
I remember what it was like.
Jokes back then, making fun of the slow kid.
Yeah, man, we were fantastic people.
Tormenting the gay kid till he killed himself.
Just a joke, you haven't grown up at all.
You haven't realized that was wrong.
There's a reason that your ilk has had to set up dating
websites specifically for you guys, because normal people
want to be around you. Normal people do not want to associate with you. And again, it's
that anonymity of the internet. That's what brought all this about. It used to be just
a screen name and people could say whatever they want. And then, now that social media
become more real and you're normally dealing with a real person, with a real name.
People still think they have that anonymity though, that it's over the internet so it
doesn't matter.
And then they carry that attitude out in the real life.
Be edgy, be shocking, here, be shocking, be a good person.
Try that.
Like I said, you can do whatever you want.
You can dress however you want.
And you can think it's funny.
You have that right.
But the rest of us are going to look down on you.
And you know, it's the same people, you know, it's this PC thing running amok.
They're trying to change the way we think.
Yes, that's exactly what it is.
You know, there's a lot of people
that don't want to admit that.
No, that's exactly what it is. We are trying to change the way you think
because the way you think is critically flawed.
Bigotry is out.
It's something that sane people have given up on.
Now I understand that
the current political climate,
The pendulum has swung over into that direction.
But I wouldn't mistake a fluke for a change in culture.
Nobody's going back to that.
Nobody's going to think that's OK.
The stuff you do today, when you think that it's coming back
the style so you start showing your true colors, people are going to remember it.
People are going to remember it.
So again, you can do what you want, it's just a joke.
But you can't be mad when you suffer the consequences of your actions.
Anyway, it's just a thought, y'all have a good night.