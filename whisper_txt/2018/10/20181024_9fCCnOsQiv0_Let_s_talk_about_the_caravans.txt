Well, howdy there, internet people, it's Bo again.
So, I guess we gotta talk about these caravans.
There's a lot of Trump supporters out there right now
going, see, we told you,
if we weren't tough on that last caravan,
there'd be another, and now look, there's another.
Of course, there's another.
They've been coming for 15 years.
This isn't new.
15 years this has been going on.
This is just the first president to pick on them.
These caravans have been coming for 15 years, and they will continue to come as long as our drug war policy remains the
same and as long as we keep intervening in these countries.
If you actually look back in 2009, see what happened in Honduras, which is where this caravan is coming from,
take a look and see what the U.S. had to do with legitimizing that coup in that country,
which is a big source of the violence, they're fleeing.
This is a blowback.
This is consequences.
We're not actually facing the real consequences, though.
This particular caravan headed out from San Pedro Sula
in Honduras.
San Pedro Sula has a murder rate of 159 per 100,000.
Now we're Americans, we don't know what that means, not really, we need something to
compare it to, right?
Chicago.
Chicago's murder rate fluctuates between 15 and 20 per 100,000 versus 159 per 100,000.
It's a very violent place.
That is why they are fleeing.
Now we have a whole bunch of questions, keep getting asked.
One of them is, who's going to pay for them?
Us.
Tax dollars.
That's what's going to pay for them.
It's been going on for a long time.
Office of Refugee Resettlement.
They spend about half a billion dollars a year doing this and have for a very long time.
Now, the office has a bigger budget, but that's what actually gets spent doing.
They help them get on their feet, and then they're done.
There's a lot of data on this.
But again, we're Americans.
Half a billion dollars, we need something to compare it to, right?
Remember that family detention fiasco Trump pulled down at the border?
That cost two billion in just that short amount of time.
And this is half a billion for a whole year to help them get on their feet.
We're actually getting them on their feet rather than just warehousing them.
So it's cheaper to be a good person.
Another question that keeps popping up,
why do they keep coming in these big groups?
It's Halloween, right?
When you tell your kids about trick-or-treating,
you say, hey, go off out there alone,
there's safety in numbers, right?
Same principle applies.
No big conspiracy there.
Why are they carrying other countries' flags?
Well, as the caravan travels, it goes through other countries.
They carry the flag so Hondurans can hang out with Hondurans.
Nicaraguans can hang out with Nicaraguans, and so on and so
forth.
Just kind of share the travel with your fellow countrymen,
I guess.
Well, why are they wearing military fatigues?
You know, I'm cleaning up after a hurricane.
I've been wearing BTU pants for about a week.
Why?
Because it's extremely durable.
I mean, the material's called ripstop.
In fact, if I was going to take a 1,000 mile journey on foot,
it's probably what I'd wear.
If I could pick anything, that's probably what I'd wear.
very durable material. Again, no great conspiracy there. Well, why don't they do it legally?
They are. They are. That is how you claim asylum. You show up and you say, I'm claiming
asylum. That's how it works. You cannot claim asylum at an embassy or at a consulate. You
have to be at a port of entry, which is at the border, or within the country. That's
That's how US law works.
If you go to the USCIS website, they'll tell you the same thing.
In fact, it even says that your immigration status, whether you came legally or illegally,
is irrelevant.
They are doing it legally.
That's how the law works.
If you have that big of a problem with them coming here, I would suggest you call DC and
to get a hold of your representative.
Maybe talk to them about how our foreign policy
impacts these countries.
Talk to them about how our drug war
just funds the gangs down there.
Maybe do that instead of blaming the victim.
15 years has been going on.
Now, I did my best to answer most questions in the comments,
those that were common.
I got a question for y'all, for everybody saying to turn them away.
Give me one good reason to send these people back to their deaths.
One.
I can't think of one.
It's the U.S.
You can't preach freedom and then deny it, send people back to their death for no good
reason other than you've got a president who is relying on your fear of the
unknown, your fear of somebody you don't know anything about, your ignorance of
the situation down there. That's the only reason there is. These people will die.
Anyway, they will die.
Just a thought.