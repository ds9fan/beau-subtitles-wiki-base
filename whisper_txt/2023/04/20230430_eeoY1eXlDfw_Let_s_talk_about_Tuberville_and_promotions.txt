Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Senator Tuberville
and promotions in the military being delayed
and readiness and what this little misadventure
can tell us about things that are likely to come up in 2024
and the Republican party as a whole.
Okay, so if you have no idea what's going on, Senator Tuberville is holding up the
confirmation of promotions. Holding up promotions for high-ranking people in the military.
Higher-end promotions require the Senate, alright, and he is holding up 184 of them
I think at the moment and he has indicated that he will continue to hold
them up until the Department of Defense does what the American people don't want
them to do and they do what he says.
So, what he wants is for the military to not give leave for family planning.
Right now if you are sent to a red state and you have to leave that state to go obtain
family planning, DOD is going to help you out. He wants that to stop and he is
holding up these promotions of people that have absolutely nothing to do with
that policy to try to get it. The thing is even Republicans are uncomfortable
with it. Susan Collins said that you know normally if you're going to hold up
confirmations for political appointees, not for apolitical professionals.
Susan Collins is absolutely right, not a sentence I ever thought I'd say to be honest.
There are rumors that McConnell is, his patience is wearing thin and that he may step in soon.
DOD shows zero willingness to change its policy.
That seems really unlikely because if they do, they're going to lose recruits.
If they change this policy, they are going to lose recruits.
People will not join the military because of it.
All of this would impact readiness, which as we have talked about before on the channel,
is a huge thing in the military.
The thing is, the 184 promotions that he is holding up, it doesn't just stop them from
getting a star on their shoulder.
It doesn't just stop their pay raise.
In some cases, it stops them from going to their next posting, which is already impacting
readiness in a huge way.
My understanding is that one of the people affected by this is the soon-to-be, or incoming
commander of cyber command, an entire command. That's troublesome and it's
also Senator Tuberville can rule instead of represent. Let's be clear on the
numbers here. When it comes to abortion, 64% of Americans believe it should be
legal in all or most cases. Only 34% believe it should be illegal in all or
most cases. It's not even close. We were making fun of MAGA being 20 points
underwater. This is 30. It isn't even close. This isn't a desire to represent.
It's not a desire to enact the will of the American people. It is a desire to
rule them, and here's the important part, this is the entire Republican Party.
There's no real pressure on him to stop.
I mean, when the pressure that is coming is coming from Susan Collins, I mean, you know
how effective that is.
I'm sure he'll learn his lesson.
Even if Republicans don't publicly endorse it, or maybe even privately disagree with
it, they're not stopping it.
This is the entire Republican Party.
The Republican Party is the dog who caught the car.
They got Roe overturned and now they have no idea what to do.
When it comes to 2024, this issue's going to weigh pretty heavily.
The Republican Party has no way to stake out a position that is tenable, because you can't
appeal to the extremist base that they have created by a half measure, and any kind of
measure is going to upset 64%.
I have no idea what they're going to do.
They may just attempt to continue to double down and hope that a Supreme Court that's
legitimacy is coming more and more into question is just going to continue to back them.
I would expect this to be one of the top three issues in the 2024 election.
The Republican Party will do everything they can to try to downplay it, but they can't.
They can't just ignore it, they have to say something because they have to appease the
extremist base that they have developed over the years.
In the meantime, at a time when the Republican party is talking about how weak the U.S. military
is, the Republican party is intentionally weakening it further, I guess, from their
perspective.
damaging readiness on purpose because they don't want people to get leave.
Anyway, it's just a thought, y'all have a good day.