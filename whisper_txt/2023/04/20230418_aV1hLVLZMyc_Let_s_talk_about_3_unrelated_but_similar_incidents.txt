Well, howdy there, internet people.
It's Bell again.
So today we are going to talk about three separate events,
three seemingly unrelated stories, one in Kansas City,
one in New Mexico, one in New York.
And although these incidents are unconnected,
they share a common element.
And there's a similarity that when people started
sending messages about the different events,
it got confusing because they are very similar.
We'll start with the one in Kansas City.
A 16-year-old black kid is going to pick up their siblings,
goes to the wrong house, knocks on the door,
rings the doorbell. The homeowner, an 84 year old white man, opened the door and
fired through the storm door, the glass door, striking the kid once in the arm, I
think, and once in the head. The kid is alive, which is nothing short of a
miracle. The officials there, law enforcement there, says they do believe
there is a racial component to it. The shooter has been charged with assault
and armed criminal action. There have been questions about the charges and why
those charges were leveled instead of other things. One, the laws there are
really weird. And two, I don't think this is necessarily going to be the final
charges. I think there's a pretty good possibility that the charges change as
time goes on on this one. The next event is in New York. A group of friends are
driving around in a very rural area, they get lost pulling to a driveway and they're
turning around, they're leaving, and the homeowner comes out and pops off two
rounds. One of them strikes the car and kills a 20-year-old woman. The car
flees, the people flee. Law enforcement shows up, takes about an hour to get the
shooter out of the house, has been charged with second-degree murder. Then
there is a situation in Farmington, New Mexico where law enforcement went to the
wrong house and they knock on the door, ring the doorbell, they don't get an
answer. They're standing there for a little bit and right about the time that
they start to realize they may not be in the right spot, the homeowner opens the
door and is holding a gun and law enforcement opens fire and kills him.
They also exchange fire with the wife before it is discovered what is
happening. People have asked about that and about potential charges. I've looked
at the footage. I don't know. I don't know. This is very different than other
situations where law enforcement goes to the wrong house. In this case, they
didn't force entry and, you know, they didn't lie to get a warrant or anything
like that. It looks like a mistake, in which case it will probably be deemed a
harmless error, even though it was definitely not harmless. Which means
they're allowed to be there. Them being there wasn't illegal. Therefore, when the
homeowner had a weapon, the shoot would be deemed justified. That's your most
likely scenario. I'd be very surprised if they filed charges on that one, but we'll
have to wait and see. Three incidents all over the country. Maybe it's not a good
idea to, maybe it's not a good idea to constantly fear monger and keep people
on edge. You have a lot of innocent people being lost because people feel like they live
in a combat zone and it's becoming a self-fulfilling prophecy. Anyway, it's just a thought. Y'all
So have a good day.