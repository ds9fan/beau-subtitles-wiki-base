Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about Wisconsin and some
results and the Republican Party taking notes.
At least, they probably should be.
Last week, we talked about a race up in Wisconsin.
Talked about how important it was.
It was for Supreme Court and how this race was going to
determine the makeup of the court there, how it was important nationally, because
Wisconsin is a swing state and come 2024, there's likely to be election
challenges, because if the Republican party can't win at the polls, well,
they'll try to litigate their way to victory in Wisconsin.
And it's important for redistricting, reproductive rights, a whole bunch of reasons.
But nationally, this was probably the most important election to occur this year.
So that race was yesterday.
I want to say it was around 730 when I first started getting messages from people saying
she won, protestants say which won.
This is the more progressive judge, and I'm sitting there thinking, there's no way they
called that that early.
That's not happening.
I was wrong.
I didn't look at it for like another 45 minutes or so.
I didn't actually pull up the numbers.
Yeah, it was a blowout, total landslide.
The voters in Wisconsin understood the assignment.
About 10 points.
About 10 points.
Now the Republican candidate, the more conservative judge, gave some unique remarks, said that
it didn't have a quality person to concede to, or something like that.
Yeah, well, that's the funny thing about elections, you don't actually have to concede
if you lost by that much.
Now, what does this mean beyond this?
Because most people thought this was going to be close, at least within five points.
With it not being even remotely close, the Republican Party is probably looking at their
math.
This was a candidate that should have appealed to the MAGA America First base.
It's almost like there's not enough of them.
And it's almost like moderate Republicans and independents want absolutely nothing to
do with that messaging.
It's almost like going after women's rights, scapegoating different groups.
like that's not a winning strategy because it's not. The Republican Party has
to change course and do it quick or I would imagine they are going to, well I
don't know that they're going to lose by 10 points, but they're going to have
problems in 2024. Smart Republican strategists would be re-evaluating right
now. Are they going to? Probably not because they're probably too tied up and
trying to figure out what to do with Trump's indictment and trying to figure
out how to spin that and create conspiracy theories or whatever. It's not
going to work. The last three elections the Republican Party has underperformed
and all they are doing is doubling down on what has caused them to underperform.
That election was a sign. What happened in Wisconsin was a sign.
They're not going to read the signs, but it is relatively safe to assume that if the Republican
party continues on the path that it is on, it's going to have severe issues in 2024.
And Lindsey Graham was right.
Anyway, it's just a thought.
Have a good day.