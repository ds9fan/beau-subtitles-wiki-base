Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about whether or not
I still think New York is the weakest case.
Something I have said for a while
is that out of the cases and the various legal entanglements
that the former president finds himself in,
that New York seemed to be the weakest case.
As soon as all of the information came out
and it was available for review,
questions started coming in, do I still think that? I still think it's the
weakest case, but I no longer think it's a weak case, if that makes any sense.
Initially, I thought they were going to have a hard time demonstrating some of
what they were alleging. Now, if they can demonstrate half of what they are
claiming they can in that indictment, I would imagine that something, one, of those charges
is going to stick.
And when it comes to sentencing, realistically, one is almost as good as thirty.
It probably wouldn't make a huge difference in the sentence.
So it's no longer about them actually being able to demonstrate what they're saying.
I think the weak part in that case is the mechanism used to turn it from misdemeanor
to felony.
And I don't necessarily know that it's weak, more that it's unproven, at least by what
I can find out.
I am certain that once they're done explaining the indictment language, the arraignment process
and everything, LawTube is going to be all over that.
And they will go through it.
Because as near as I can tell, the mechanism they're using to do this, it kind of relies
on allegations that aren't really proven or it relies on crimes that are more the
Fed's business and I it doesn't seem like it's been done before so it's
unproven. Now again the Law Tube people I'm certain they're gonna be all over
this and I will yield to whatever their determination is they they know a whole
lot more about this than I do. So I still don't think it is the strongest case. I
think out of the four main ones that we're looking at, it's probably the
weakest. But at this point it seems more weak based on procedure stuff than them
actually being able to demonstrate it. Now, one of the other questions that has
come in is why is it going to take so long to get to the next part of this?
The wills of justice turn slowly and all of that stuff. Say with me, our justice
system is broken. Now, at the same time, I wouldn't worry about it too much. I think
there are going to be more developments in other cases
between now and then.
I see that as incredibly likely.
When it comes to the documents case,
the more the information comes out about that,
the more little tidbits I hear, the more I
become convinced that that is probably the strongest case,
to the point where, right now, I don't think they're determining whether or not
they have a case or whether or not to indict.
I think they're trying to determine what to indict him for
and how hard they really want to hit him with charges.
The shift in some of the questioning
that we have heard about, I can tell you this.
If I was in a situation where somehow I wound up
with a bunch of wild classified documents
and didn't return them, and all of a sudden the questioning
was less about what did I know and when did I know it,
And it started to turn to, well, did
I have a specific interest in this topic?
I would be super concerned.
Because that moves it from something
that might just be the behavior of somebody
who is entitled to somebody who might have been up
to something that was bad.
beyond just endangering something through keeping
documents in the way that he was,
there might have been another motive.
So that case is definitely proceeding.
And I feel like there's a pretty good likelihood
that between now and the next major set of developments
in the New York case, there will probably
be movement on the documents case.
Anyway, it's just a thought.
Y'all have a good day.