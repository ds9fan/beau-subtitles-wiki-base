Well, howdy there, internet people, it's Bo again.
So we're going to talk about Wisconsin again
and some of the ideas that are being tossed around
by the Republican party up there and math.
So we have talked about the election up there.
There was a Supreme Court election, very important,
And the more progressive judge won.
Big.
Double digits.
Big, big win.
But the legislature there, it's a Republican controlled
legislature.
And they picked up some seats.
Enough seats to where, in theory, they
could override a governor's veto.
Or, as rumors suggest, something that might be entertaining,
impeach a judge.
I mean, I get it.
The numbers are there on a party line vote.
I understand that.
That math checks out.
Theoretically, it's something that they could do.
I would like to point out that most of the comments that
has spurred these rumors were actually
made talking about a lower court position.
But the Republican Party isn't going out of their way to shut these rumors down.
So let's go through it a little bit.
She, the Supreme Court Judge, Protasewicz, won by double digits.
Massive margin.
Did the seats that they pick up win like that?
They didn't, right?
They didn't.
Her massive margin, her massive win, means that people that voted for them crossed party
lines to vote for her.
If Republicans decide that they want to impeach a judge, throw away the will of the voters,
undermine democracy, the republic, constitution, all that stuff, if they decide they want to
that route, they have the votes to do it on a party line vote. But what happens next? What happens
in 2024? They will have angered voters. They will have canceled out the will of voters who voted for
them and crossed party lines to vote for her. They will be upsetting voters who have already shown
they are willing to cross party lines. I'm fairly certain that those voters might cross party lines
in other races, like in the legislature. Republicans here need to do the math.
unless they are in a very, very red district where they have a 10-point
lead, a double-digit lead, if they do this they're in trouble. They're going to have
a much harder race because they'll be upsetting voters who have already
determined that the authoritarian push from the Republican Party isn't
something they want. They demonstrated that in this election. They showed by
double digits that that's not what they want. If they go against the will of
those voters and they try to engage in authoritarian measures from the legislature, they'll pay for it
in 2024. Me, personally? Do it. Do it. I'm fairly certain that you'll end up handing the Democratic
party a trifecta. If they discount the will of the voters of Wisconsin to that
degree, those voters will not vote for them again. A massive amount of them. She
won by double digits. You should probably just leave that judge alone. Anyway, it's
It's just a thought.
Y'all have a good day.