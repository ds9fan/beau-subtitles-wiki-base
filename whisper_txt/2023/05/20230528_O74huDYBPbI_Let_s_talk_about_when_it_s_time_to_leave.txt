Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk about when it's time to leave,
when it's time to get out.
Because due to a lot of legislation all over the country,
it's a question that's coming in more and more often.
How do you know when it's time to leave the state you're in
and go to a freer state?
Generally speaking, the answer is when you start to ask yourself if it's time, but the
reality is there is no hard answer to this because everybody's situation is different.
Your resources determine the amount of forethought you need to put into this.
So we're going to take two extremes.
say you have a decent tech job, you work remotely, you rent your apartment and you have four
or five grand in savings. Yeah, you don't need to worry about much because you could
be gone in a week, especially if you know somebody in another area. So there's not a
lot of pre-planning that needs to go into that other than figuring out where you would
want to go. The flip side of that is you have two kids, you own your home, and
you're at paycheck to paycheck. Don't have savings. You got to start planning
now. As soon as you start thinking about it, you need to start really planning.
You know, every state, every person is going to be different in this regard and there is
no hard and fast advice other than when did Noah build the ark before the flood?
If you're considering it, if this is something that has worried you, if there is legislation
that is specifically targeting you,
now's the time to start making a plan.
You may not need to put it into action right now,
depending on your resources.
But the lower your resources are, the sooner
you have to start to enact the plan.
If you're somebody that has tons of cash laying around,
you don't have to think about it too much,
Because you can move somewhere else and clean up what you're leaving behind in the other state with those resources.
If you don't have a lot of resources, you have to start thinking ahead now, where you're going to go, where you're
going to work.  Do you have friends in an area that can help you get a job, get acclimated to the area?
Is there a particular state you want to be in?
What you're going to do about your kid's schooling?
like there's a whole lot more to it. So if you are asking yourself that, if you
are part of a demographic that is being targeted, you should probably start
thinking about it now, like right now, this minute, and trying to piece together
what you would do and how long it would take, you know, figure out what it's
going to take for first and last and security deposit in this state you're
planning on moving in? How long is it going to take you to get that kind of
cash? Maybe go ahead and start saving up for it. You have to plan and you have to
put the pieces of the plan in motion that you can. Now, if this is a legitimate
concern of yours, then you should probably be thinking about it
immediately because let's assume that things go well in the next election in your state.
That doesn't mean it's going to disappear right away.
There are a lot of areas that are hostile to a lot of demographics right now.
And you have to make the move that's right for you, your circumstances, your resources,
your risk level.
And it varies.
The irony is there's going to be a lot of people who aren't really being targeted that
are going to want to move too.
And that's going to make it harder.
Because if you get a flood of people headed to a particular state, it's going to drive
up rent prices, it's going to make the job market more difficult, stuff like that.
So if you are somebody who's on that list, who they've decided to other, I would start
planning as soon as possible.
And doing that, you'll relieve the pressure later.
know where you're going to go. You'll have saved up a little bit to get there
or you'll have made the connections in the resources so you don't need as much
saved. But if you're on that list you need to start thinking about it now.
Anyway, it's just a thought, y'all have a good day.