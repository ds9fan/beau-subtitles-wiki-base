Well, howdy there internet people, let's bow again.
So today we're going to talk about timelines
and the debt ceiling and the legislation
and those people who are opposed to it.
You know, there's a lot of people saying
that they're not happy with the bill
and they may vote against it and so on and so forth.
I don't know that they understand that they're late.
they're late, they're late for an important date. They don't have time to
vote against it. There's no time for renegotiation. Okay, so the House rules
say you need 72 hours to review legislation. The current bill, the one
that people are less than happy with, that's going to be voted on on Wednesday,
Hopefully. That's the earliest. Let's say people vote against it and it fails.
Renegotiation starts. Let's say it's a world of make-believe and magic where
everything gets hammered out in 10 minutes. The whole negotiation is
redone and everybody has something that they'll vote for in 10 minutes. 72 hours
starts. It gets, you know, turned into legislation and it gets distributed to
people, okay, on Thursday. 72 hours starts. That means the earliest voting day for
the House is Monday. Guess what? We default that day. Still has to go through
the Senate. The time for negotiation is over. This is the deal. Either they're
going to vote for it or they are going to actively vote to default even if they
were to do away with the rules okay and say okay well we don't need 72 hours to
to review the legislation we'll just vote on it now that's fine but it still
has to go to the Senate it's still there's a whole lot at play here and they
don't have time. The people who are saying that they are going to vote
against this, you better hope that you have your vote count math right because
if you vote against this and it fails and we default, I'm fairly certain you
won't get re-elected. It's really that simple. So if you're
to vote against it for some kind of show for your base to show that you're a true hardliner
or whatever, I mean that's fine, but you better be right on the money on your vote count and
you better hope that nobody hits the wrong button or changes their mind at the last minute.
Because if this fails, if this doesn't go through, the US is in for a whole lot of economic
turmoil. And I don't think that the American people will be very forgiving
when it comes to to casting their votes for the people who voluntarily decided
to cause a default when there's a deal on the table. Seems super unlikely that
there's going to be a whole lot of campaign contributions coming your way
from, you know, the major companies who are all going to take a massive hit. In
fact, they might donate to your opposition. It just seems very unlikely
that this fails because if it does, there's a whole lot of problems. The
other alternatives include withdrawing it and sending up a clean debt
ceiling, which I don't think Republicans are going to go for because that's going
to be even more upsetting to their base, to the people they promised that
they would fight forever for and all of that stuff. And keep in mind, that
timeline that I gave, that includes getting the negotiation and turning it
into legislation done in a night. Doesn't really seem likely. This is the deal. It
It is what it is.
Anyway, it's just a thought.
Y'all have a good day.