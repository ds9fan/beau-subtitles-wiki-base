Well, howdy there internet people, it's Bo again.
So today we are going to talk about North Carolina
and that veto override and what that override means
for the people of North Carolina,
but specifically what it means for Republicans
in North Carolina.
And I'm not talking about the people in office,
I'm talking about the Republican voter
because the state legislature just told you something
I really hope that you heard it. In poll after poll, the people of North Carolina
told the state legislature that they did not support that legislation, that they
didn't want it. An overwhelming majority. Republicans in the state legislature
passed it anyway, and then when it got vetoed, they overrode the veto to enact
legislation that was wildly unpopular. They put the special interest groups
above the people of North Carolina to include Republicans, to include their
Republican voters. They told you flat out, you don't matter. Their loyalty is
not to you, the loyalty is to the party and the special interests that that
party represents. That's what they said, and it's every single one of them because
any of them had the ability to stop this. Any single Republican in the state
legislature could have stopped this. They could have stepped forward and said, no, I
actually support the people of North Carolina, not the special interest groups
that the leadership of my party is telling me to obey.
But none of them did.
They all decided that it was better for them to do what the party says.
By extension, they're telling you, you need to obey.
You need to do what you're told because they're not there to represent you.
They're there to rule you.
They made it super clear, any of them could have stopped this on their own.
But none of them put the people of North Carolina, the will of the people of North Carolina over
their own political ambitions or over the ambitions of the leadership or over the ambitions
of the special interest groups that want to control you.
If you don't vote them out, they're going to keep doing it.
going to see it as a mandate. They are going to come after everything because
you're telling them that they can regulate anything and you'll still vote
for them. I'm not telling Republicans in North Carolina to vote Democrat, primary
them. You need to set the tone right now that that's not acceptable, that they
don't own you, that you will vote for an independent, you will primary them.
them, that they don't have your undying loyalty simply because they put an R after their name.
Any of them could have stopped this, but none of them did.
None of them thought the people of North Carolina was more important than obeying the leadership,
overriding a veto to enact legislation that the majority of people in North
Carolina did not want. None of them saw that as a priority. Their priority was
doing what they're told. They don't sound like leaders to me. They don't sound like
representatives. Anyway, it's just a thought. Y'all have a good day.