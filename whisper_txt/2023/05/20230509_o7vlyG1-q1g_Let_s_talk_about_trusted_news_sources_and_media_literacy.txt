Well, howdy there internet people, Ledzebo again. So today we are going to talk about
media literacy
and trust in media organizations because some interesting polling came out from
YouGov
and it shows some trends that we might want to pay attention to.
Now what they did was they asked a whole bunch of people
how much they trusted various media organizations
and then they displayed the results by
were broken down by party, Republicans and Democrats,
and then a net trust score, okay?
So what are the top 10 most trusted media organizations?
Number one, the Weather Channel.
Number two, PBS.
Number three, BBC.
Number four, Wall Street Journal.
Number five, Forbes.
Number six, AP.
Number seven, ABC.
number 8 USA Today, number 9 CBS,
number 10 Reuters. I know that may be surprising to some people
but it all kind of tracks and I know that there's going to be a group of
people wondering
where one outlet in particular is, Fox,
below Newsmax and OAN
which we'll come back to. Okay so what does the top 10 tell us?
people care about things that impact them directly.
That tracks, that's pretty standard. The Weather Channel and PBS.
BBC, why is that rated so highly? The accent.
Americans, generally speaking, and this may surprise people in the UK,
they trust the accent. It is sophisticated.
This probably doesn't have a whole lot to do with actual reporting.
this has to do with presentation. Wall Street Journal and Forbes. People trust
money. People care about money. The fact that Wall Street Journal and Forbes are
rated higher than AP is something I will never forget or forgive. And then the
bottom five, they are not all straight news outlets. They definitely have their
own biases and sometimes it's pretty obvious, but for the most part they are
more straight news than a lot of other options. So what does that tell us?
People care about themselves and people can tell the difference between objective
news and opinion if they want to. The top 10 they show that pretty clearly. I mean
with the exception of like the money but anyway. So what does it tell us that Fox
is below Newsmax and OAN? There are probably a whole lot of people who are
not familiar with OAN and Newsmax's record of accuracy, let's just say. I think I'm pretty
well established in saying that I don't like Fox. I think that's a known fact and that
I do question some of their practices, obviously, the fact that Newsmax and OAN are above them
says that there are a lot of people who are unfamiliar with the networks themselves and
are basing their answers on a perception on what they've heard, which I get it, but it's
not great for media literacy.
You can also tell from this poll that with the exception of the top five, the ones that
are the Weather Channel, PBS, BBC, Wall Street Journal, and Forbes, me, money, and sophisticated,
Republicans distrust pretty much everything.
Unless it is obviously partisan, unless it is something that is just clearly not objective,
distrust it. They, I hate to be the one to say it, they don't care about facts, they
care about feelings. They want their opinions reinforced. That's what the
polling shows. They distrust literally almost everything. They have a net
distrust of almost every media organization with the exception of those
that specifically tell them what they want to hear. So what can we do with this
information? Even Republicans, even those who pretend like they don't know the
difference between opinion and objective news. They do. They do. That's what this
polling shows. Maybe it is time to stop humoring them, to stop pretending like
their talking points are real. There are a lot of outlets, especially on cable
news that both sides issues that don't have two sides and they do that because
they believe that one political party actually believes their own rhetoric.
They don't. Their own polling shows this. They know the difference between
objective fact and opinion. They just reject the facts. Stop treating their
opinions as facts. It's it's pretty clear if you if you really sit down and look
at this. That's one of the big takeaways. The other thing is please understand
that the Wall Street Journal and Forbes they are written with a very specific
leaning. They are written
in many cases to cater to a very specific
group. It's a big club and you're probably not in it.
That is one of the more
surprising things to me
is that the majority of Americans that these outlets have a net trust
when compared to the AP even.
That is something that we're gonna have to work on if you ever want to get
towards a more cooperative rather than competitive society because the way
these outlets are written, the way the overall slant is at them, it is very pro
competitive society. Anyway it's just a thought. Y'all have a good day.