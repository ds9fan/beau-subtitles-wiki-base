Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Senator McConnell
and Senator Manchin and some friction that has developed
in the Senate over what might happen in West Virginia.
So to remind everybody, Senator Manchin
is kind of a Democrat.
He is a member of the Democratic Party
And he has done pretty well in West Virginia, which is a
predominantly red state.
However, now he has an expected opponent in the next
election who is incredibly popular.
Now, McConnell, he is, you know, McConnell.
Somebody who, for whatever reason, people continually
underestimate how shrewd he is.
Um, he's a Republican and he has indicated his support for the
Republican candidate, shock.
This has apparently really bothered Manchin who said, this is what's
wrong with this place, okay, and I have, I've said this, I have never ever
campaigned against a sitting colleague. And
if you want to know what's wrong with the process,
go talk to McConnell. That tells you everything that's wrong
with the Congress and the United States Senate, especially under his leadership.
Man,
I guess siding with the Republican Party
all those times really didn't benefit you very much.
I never thought the Leopards would come for my face, right? Yeah, so if McConnell
throws his weight behind Justice, who is the at this point the presumptive
opponent of Manchin, I mean I don't want to say it's a certainty that Justice is
going to win because Manchin has squeaked out wins in a very red state, but it is going
to be much harder for Manchin if McConnell throws his weight behind justice, which does
appear to be the case.
The Republican party is about power, and it doesn't matter if you compromise with
authoritarians.
They want power.
That never changes.
It doesn't matter if y'all are friends and have dinner and have lunch together all the
time.
At the end of the day, from McConnell's perspective, that's a seat.
seat is a seat, and he wants a majority of them so he can be Senate Majority Leader.
Or so at least the Republican Party can control the Senate.
The compromise that a lot of people think is so important, it just sets you up to be
surprised when people who are very politically shrewd and thirst for power decide to use
you to get that power.
Anyway, it's just a thought, y'all have a good day.