Well, howdy there Internet people, it's Beau again.
So today we are going to talk about Michigan
and two stories coming out of Michigan and Trump.
They both relate to Trump.
The first and probably the biggest headline
is that the Michigan Supreme Court,
they decided against removing Trump from the ballot.
They upheld that decision.
The interesting thing to note about it is they didn't make a determination about whether
or not he engaged in insurrection or anything along those lines.
They rejected it on procedural grounds.
Basically it doesn't appear that there is a similar clause in Michigan election law
like there is in Colorado, and that was their basis for rejecting it.
The other news is one of the electors, one of the fake electors for Michigan.
Renner, who is 77, cut a deal with the investigators in the case and is cooperating.
He's also talked a little bit about it now, and basically he described how he was ushered
through the process by people who he thought knew what they were doing, that he wasn't
really aware of what he was a part of until much later.
In fact, the January 6 hearings were kind of a moment for him.
And describing how he felt, betrayed is an understatement, that's all I can say.
It's worth noting that out of those, out of the electorate, he's the only one that's
made a deal thus far, and it's worth remembering that the investigation is still ongoing.
It hasn't ended.
There may be more people who get wrapped up in it.
Based on the public-facing statements, I would imagine that Renner provided information that
might lead authorities in Michigan to consider looking at some of the other people, those
who guided him through the process, those people who he believed knew what they were
doing.
They might end up looking at some of them a little bit closer.
Depending on the information those people provide, if they are spoken to, it might go
even higher.
This may be a situation where we end up with another Georgia style case, but we'll have
to wait and see how it plays out. Anyway, it's just a thought. Y'all have a good day.