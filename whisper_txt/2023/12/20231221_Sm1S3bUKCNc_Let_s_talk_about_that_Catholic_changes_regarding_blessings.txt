Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the news,
talk about what it means, where it goes from here,
talk about why people are acting like it's a big deal,
even though in some ways it may seem kind of underwhelming
and even contradictory.
But we'll go over what it means and why people are excited.
This is one of many questions about this.
Can you talk about what the Pope said?
You've been covering this for a while, and people are acting like this is huge,
but it still doesn't allow gay marriage, question mark.
Yeah, it doesn't.
So, if you have no idea what I'm talking about, this is what occurred, short version, too late.
late is the church has decided that priests can bless people who are in same
sex couples. They can't marry them. They can't perform a marriage. They can't
even provide that blessing at the time a civil union is taking place, but they can
bless those same people later. What was said in relevant part, when people ask
for a blessing, an exhaustive moral analysis should not be placed as a
precondition for conferring it. Right, it doesn't allow for same-sex marriage, okay?
But what it does is it changes the fundamental way it's viewed. Being
in a same-sex couple is no longer, that's a sin, and it's one of those sins that means
you need to get away from this church, you don't show up here, we don't want to talk
to you, you're not getting a blessing, you're not getting anything.
And it turns it, it transitions it into, that's a sin, like a whole bunch of other sins come
on inside.
That's what it really boils down to.
The Pope has been slowly working to make the Catholic Church more welcoming to LGBTQ people.
This is huge.
This is a major development because the Church is a very traditional organization and change
takes time.
It occurs incrementally and this is a giant step.
And you may not see that if you're not Catholic, if you're not familiar with Catholicism.
As an example, mixed marriages, up until the late 1950s, they couldn't even be done in
the church.
They had to be done, like, in a different part of the facility, but not like actually
in the church itself.
And right now, me saying that, people are thinking mixed marriages like white and black.
Now, I'm talking like Baptist and Catholic.
That's how traditional they are.
That's how old some of these rules are, and it takes a long time to change.
A step like this is big, and it's going to matter to a whole lot of people.
matter to Catholics in the United States, but this is, it's bigger news in countries
where Catholicism makes up the fabric of the social world, and in many cases the political
world.
Again, it's not a complete win, but it's one of those incremental steps along the way,
And it is going to matter to millions.
So it may not seem groundbreaking, but inside the way the Catholic Church works, it's pretty
big news.
And it is.
It's another sign that on a long enough timeline, we win.
Anyway, it's just a thought.
Y'all have a good day.
day.