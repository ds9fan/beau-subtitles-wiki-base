Well, howdy there, internet people.
It's Bo again.
Merry Christmas.
So today, we are going to talk about phone calls
and making one or accepting one today,
whether you celebrate it or not for a whole lot of people
in the United States.
This is a day that is really happy or really sad for people who are isolated or for whatever
reason a personality conflict or whatever has left you not talking to somebody.
Remember that today is a day that you could text or call, and it not seem out of place.
It not be a big deal.
You aren't going to interrupt people's holidays.
Most people expect phone calls and texts from people today.
So if you're in a situation where there's somebody you want to reach out to, but for
whatever reason you haven't, today might be the day to do it.
Because the pressure is a little bit less.
The flip side to that is if you are somebody who's having a happy day, you know, you've
got your family around you, think of those who may not.
Think of those who are a little bit more isolated.
Maybe make the call to them and check up on them.
It can be a happy day or it can be a very, very lonely day.
And a few-minute phone call can make all the difference for some people.
So maybe work that into your plans if you're in that situation.
If you know somebody who maybe doesn't have everything going on and maybe they're new
to town, maybe they're isolated for whatever reason.
It might be a good day to reach out, just see how they're doing.
And again, it doesn't have to seem like you're checking up on them.
You can just call and talk to them because of the holiday.
It provides that cover whether you're trying to reconnect with somebody or you're trying
to trying to make sure a friend is in the right place so it's a simple task
it won't take long and it might mean the world of difference to someone. Anyway
It's just a thought. Y'all have a good day.