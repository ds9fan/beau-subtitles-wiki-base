Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Jack Smith
and the Grinch.
And just a reminder for people who are involved
or watching certain developments
that are occurring in the United States
that are of, I mean, kind of historic proportions.
Because I guess some people have forgotten
the gravity of the situation here.
Okay, if you missed the news,
the legal team for former President Trump, they had an issue with Smith's request for
an expedited schedule when it came to looking at Trump's claim of presidential immunity
before the appeals court there.
When they're filing, basically asking for it not to be expedited, they said that the
schedule that was being proposed, well, it, quote, would make President Trump's opening
brief due the day after Christmas.
This proposed schedule would require attorneys and support staff to work round the clock
the holidays, inevitably disrupting family and travel plans. It is as if special counsel growled
with his grinch fingers nervously drumming, I must find some way to keep Christmas from coming, but
how? I mean, you know, I like metaphors, so I mean, I think that's cool, but I would like to point out a
couple of things. First, this didn't work. This argumentation did not appeal to the appeals court.
But I think it might be worth reminding everybody that this is potentially the most consequential
criminal case in American history. Yeah, you might have to work overtime. I mean, that kind of goes
with it. It's also worth noting that that's the last day for that filing.
Maybe you could do it before. You don't have to work on Christmas. You could get
it done ahead of time. It is baffling to me that there are people who are
still viewing this as something that is just of normal political substance. It's not.
This is a criminal trial of the former president of the United States and the crux of it, what
is going to be decided in that courtroom is whether or not Trump sought to basically engage
in a self-coup to interfere with the election to the point that it would
alter the outcome. That's pretty important.
I'd also like it noted that there are a whole bunch of people who believed the
lies about the election that are going to miss Christmas for a while.
Anyway, it's just a thought, y'all have a good day.