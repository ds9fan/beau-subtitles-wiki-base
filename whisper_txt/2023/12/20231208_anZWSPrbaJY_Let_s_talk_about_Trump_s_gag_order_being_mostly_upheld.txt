Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump
and the appellate decision, what it means,
what was changed, where it goes from here,
dealing with the order that was telling him to be quiet.
Of course, this is in the DC case.
I know that it's hard to keep track,
but this particular order is talking about the DC case.
So, the short version of this is that the appeals court has decided that the overwhelming
majority of the gag order stays in effect.
There were some slight changes, but overall, it was upheld.
The biggest change is that while Trump cannot talk about judges or prosecutors or potential
witnesses, their families, so on and so forth, he can specifically talk about special counsel
Jack Smith.
And even though no questions have come in on this so far because the news is breaking
right now. I imagine the question is going to be why. The answer is simple.
Winsmith took that position, became a very public figure, and the bar is much
higher when it comes to that. So that's the reason. The important part to note
here is the appeals court saying, Mr. Trump's documented pattern of speech and
and it's demonstrated real-time, real-world consequences
pose a significant and imminent threat
to the functioning of the criminal trial process
in this case.
Okay, so, this is a lot like the other gag order.
It may not have been smart for Trump's team to appeal it.
appeal it. The violations that occur prior to the appeal, the judge has to
wonder whether or not they will hold up on appeal. Now, this has already been
reinforced by the appeals court, and the appeals court is saying that poses a
significant and imminent threat. So the judge taking action on any violation of
the gag order it becomes more likely now. And the court did go on to say that they
don't, you know, allow such an order lightly because he is a former
president and he's running for president and all of that stuff but even though
there's a strong public interest in what he has to say, there is a strong public
interest in the trial proceedings being protected. So that's the short version of
this. So Trump is back under a gag order for the DC federal case, the election
case.
Anyway, it's just a thought, y'all have a good day.