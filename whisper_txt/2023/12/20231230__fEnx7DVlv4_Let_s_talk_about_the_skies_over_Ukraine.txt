Well, howdy there, internet people.
Let's above again.
So today we are going to talk about Ukraine and Russia
and what occurred and just run through the numbers
on it real quick and provide the information
we have a time of filming.
Okay, so it appears that Russia launched
the largest aerial barrage that has taken place in the war over the last
almost two years. The rough count was 122 missiles of varying types and 36
drones. It looks like Ukrainian air defense took out 87 of the missiles and
27 of the drones, which is, I mean, that's a decent ratio given what they're working
with right now, but it's not so good if you're on the receiving end of what made it through.
It appears that Russia decided to target energy and social infrastructure.
Damaged buildings include a maternity hospital and schools.
The energy infrastructure is designed to make it really cold.
We talked about this being a likelihood a couple months ago.
My guess is that this is occurring now in such volume because of the success of the
strike against the warships.
So Russia's response to that was an aerial barrage targeting critical and civilian infrastructure.
The thing about this is a lot of commentators are pointing to the numbers and saying they
can't have much more left.
We don't know that.
It does look, by the variety that was used, that short of stuff coming off of subs, they
pretty much threw everything but the kitchen sink at them.
But I wouldn't say that this was like a final volley or something like that.
They probably have more coming, particularly when it comes to the drones.
Eventually if they're using them at anywhere near this rate, eventually they will start
to run low on the missiles.
But the drones, they're a little bit easier to come by.
So I would imagine we're going to see more of this, maybe not to this degree because
this was big.
There's not an accurate count of the loss yet, so we'll hold off on that, but I would
expect more of this.
is why the the rushing of trying to get the Patriots back over there and stuff
like that trying to get more of the missiles over there that's why it's
being rushed because there's gonna be more of this anyway it's just a thought
Y'all have a good day.