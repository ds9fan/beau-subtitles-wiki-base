Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about pirates
and dreams and voyages.
We'll provide a little update on Captain Cory.
For those who may not have been around,
don't remember, or missed it, there
will be videos down below for context.
But the short version is, about six months ago,
a young man who was very enthusiastic about pirates
needed a little bit of help fulfilling a bucket list item.
Like all good pirates, he wanted silver.
He wanted a YouTube play button.
A bunch of people from this channel
and a whole bunch of others jumped in and made that happen, made that happen.
So Cory has set sail.
He is no longer with us.
It is important to remember for the last six months of his life, he lived his dream.
He lived his dream.
Now understandably his family seems to want a little bit of space at the moment.
His mother has indicated that she definitely intends on keeping the channel up and running
kind of as a memorial to him.
So I'm sure that while you may not get a response immediately, I'm sure that they would appreciate
any message that you may want to leave over on his channel, which is a crack in the box.
There'll be a link down below.
And I am certain that he would appreciate those who stopped by to wish him a good voyage.
I know that this is not the news everybody wanted to hear, but it was the news that I
think on some level everybody knew was coming and just take take a little bit
of solace in the fact that he really did get his dream for six months and in large
part, it was due to the people who are going to be pretty upset by this news.
Anyway, it's just a thaw, y'all have a good day.