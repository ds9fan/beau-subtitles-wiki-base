Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the most amazing two
minutes to ever occur on Fox News.
OK, so if you somehow missed it, Tucker,
you know, no longer with the network.
Jesse Waters is taking over his slot, his time slot, and on his first show his
mother called, his mother called in, and he took the call live and it started out
the way you would expect. A proud mother congratulating her son and everything
you know that you would picture as you know some kind of feel-good thing that
they were gonna put on a on a show to kind of open it up. And then she
explained how he could best keep his job by not you know going down conspiracy
rabbit holes and she said that he he shouldn't go down conspiracy rabbit
because they don't want to lose him, and they don't want any lawsuits.
An obvious shot at other Fox News hosts.
She then went on to say that he needed to do no harm and be kind, which, I mean, that's great advice.
Said that he needed to use his voice responsibly, and then said to stop the Biden-bashing
bashing and forget about the laptop and stop worrying about other people's
bodies. It was glorious. She closed by advising him to tell Trump to give up on
politics and go back to TV. I want to see a show with Jesse's mom. That's
what I would like to see. I would watch that. It was very entertaining. The host
did his best to not appear bothered, although he did seem to mumble at one
point that I knew this was a bad idea. I will try to find a clip and put it down
below because it was it was pretty humorous. I got a big chuckle out of it.
Now here's the the question we have to ask. How much foreknowledge did
Fox have about the topics being discussed? This is a time slot that has
gotten them in a lot of trouble and they may be trying to shake it up and this
This may be them trying to set the tone.
If she was improvising what she said, that's not the case, but if this was a little bit
more structured, this might be a signal from Fox.
We have to wait and see, but yeah, I'll try to find a clip of it.
Anyway, it's just a thought.
have a good day and be kind.