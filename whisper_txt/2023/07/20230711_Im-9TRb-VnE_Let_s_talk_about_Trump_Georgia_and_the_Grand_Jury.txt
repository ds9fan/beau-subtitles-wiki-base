Well, howdy there, internet people.
Let's vote again.
So today, we are going to talk about Georgia and Trump
and the possibility that things are going to start moving
along in Georgia, okay.
So tomorrow, in the area that matters,
a grand jury will be being selected.
They're going to pick 23 jurors and three alternates.
This grand jury will consider a number of cases.
One of the cases it is likely to take a look at
based on all available information
is the election interference case
involving former president Donald J. Trump.
This would be the one with the perfect phone call
asking for eleven thousand seven hundred and eighty votes.
So,
how long is this going to take?
The signaling suggests that it might take till August.
At the same time, if
it was me, I would probably try to keep people off balance
not be entirely forthcoming in my plans. This grand jury is likely to determine
whether or not former President Trump will become a thrice indicted former
president. There are a lot of questions about whether or not this is going to
move forward because of Smith's case and what happens if it does move forward
and then Trump is indicted federally for a similar scheme. Based on just general
statements and what we've seen so far, I have a feeling that the prosecution in
Georgia intends on moving forward pretty much no matter what Smith does. I'm not
sure, but that is definitely how it appears. So it's important to remember
this grand jury does not determine guilt or innocence. It determines whether or not
an indictment should be forthcoming. That's what it's determining. This
This isn't the beginning of a trial, it's the grand jury process.
So it's the start of yet another set of potential criminal proceedings for the former president,
which is getting to be a lengthy list.
I'm running out of space on my whiteboard.
Anyway, it's just a thought.
Y'all have a good day.