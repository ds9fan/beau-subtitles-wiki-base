Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the Arizona Republican Party and money.
We're going to talk about cash and the Arizona GOP and what their current
situation may tell us about 2024 out in Arizona.
Okay.
If you have missed the reporting, apparently the state level Republican
party in Arizona has about 50 grand, $50,000 to the party, like that's what
they have. If you're not familiar with how much state-level parties typically
have, for comparison, this weekend, this last weekend, the Democratic Party in
Florida, an incredibly red state, at least it seems to be, raised about half a
million dollars over the weekend. $50,000 is not a lot of money when you are
talking about a political party. It is worth remembering the party spent a
whole bunch of money defending Trump claims and stuff like that. They also
spent about half a million dollars on projects that they probably didn't need
to, such as like a victory party for 2022, which was an election in which they
lost, well, all of the statewide races. But even with that in mind, the party
spending a bunch of money that it didn't need to that's that's not
entirely uncommon. The thing is it's not being replaced. The money being spent in
weird ways that happens but generally speaking donors come in and replace that
funding. This isn't happening out in Arizona for some reason. It might have to
do with the fringe takeover of the Republican Party out there. If the party
can't win, people don't want to donate to it. You have to remember that many
donors view it as an investment. They're not really saying, hey I really support
this person's opinions. Their positions, their policy, what they're really doing
is buying access. If the candidates can't win there's no reason to access them. The
fact that the GOP out there is having such a hard time raising money right now,
This could spell huge problems for Republicans come 2024. You already have conservative
commentators basically saying nobody wants to donate to a crazy party, quote.
Um, there is a push to have the party adopt positions that are, quote, more sane,
but we'll have to wait and see how that plays out. At the moment Arizona is kind
of up for grabs. The Democratic Party should probably be doing everything they
can to cement and solidify any support they have because it's it seems as though
So, right now, the Republican Party in Arizona is in utter disarray, and it looks like it's
time for them to make a move.
Anyway, it's just a thought, y'all have a good day.