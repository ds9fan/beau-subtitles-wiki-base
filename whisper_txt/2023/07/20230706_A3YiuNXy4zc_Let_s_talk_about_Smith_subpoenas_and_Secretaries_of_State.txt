Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Arizona again
because there is some more information coming out of there.
A lot of eyes have been on Arizona
because recently information surfaced
suggesting there was a phone call,
very similar to the phone call in Georgia
from Team Trump trying to apply pressure
regards the 2020 election, trying to alter the outcome, to find votes, so on and so forth.
When that story broke there were a whole bunch of people asking if, you know, this was going to
prompt the Department of Justice to get involved and I said the real question is why do people
feel comfortable talking about it all of a sudden after all of this time and suggested they may have
already talked to the feds. Reporting out of Arizona today says that as recently as
May, remember that phrasing, the special counsel's office had subpoenaed the
Secretary of State's office in Arizona and they talked to lawmakers. They've
already talked to the feds. Smith already has the information. Again, it is important
to remember that the order in which news breaks is not necessarily the order in
which it occurred and that certainly seems to be the the case here. Now, that
phrasing, as recently as May, that would seem to indicate that the Arizona
Secretary of State's office received subpoenas about more than one thing. We
know from reporting that at least part of what the special counsel's office is
looking into deals with some lawsuits that were filed in the aftermath of the
2020 election. It does seem like the election interference slash January 6
probe that it is far more expansive than we currently know. There's
a lot there is a lot happening with that that isn't making the news. So again, they
this subpoena in these conversations occurred in May. We got a little hint
about it a few days ago and now there's confirmation of it. So remember when
When little elements start to surface in the media, that the real reason they may start
to surface and become public is because Smith's office already has the information.
Maybe had it for a month or more before we ever find out about it.
That's a wild way to run a witch hunt, I guess.
Anyway, it's just a thought.
y'all have a good day.