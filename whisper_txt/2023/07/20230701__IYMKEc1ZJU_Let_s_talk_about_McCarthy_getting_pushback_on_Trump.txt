Well, howdy there, internet people, it's Bob McGunn.
That didn't take long, did it?
Okay, so McCarthy recently suggested
that Trump may not be the strongest candidate
to run in 2024.
We talked about it, said he could expect a lot of pushback,
and said that was a pretty big step in his campaign
to distance himself and the rest of the Republican Party
from Trump.
Almost immediately he started trying to walk it back. He realized how big a step
he had taken. He has since said that Trump is stronger now than he was in 2016.
I mean you can believe that if you want to. It's worth remembering that the
person they're talking about is a twice-impeached, twice-indicted person who
lost their last election. They're not a political powerhouse anymore, and it's
worth remembering that he lost his previous election before the actions
related to the Sixth, before the indictments, before all of that. They are
looking at the same polls that led them to believe there was going to be a red
wave, the thing that did not materialize. If they had a red wave, McCarthy wouldn't
Trump anymore because he wouldn't need the votes from the Trump-aligned far
right Republicans. But social media, they're viewing social media clicks and
likes as votes. Same thing that we've been talking about, same thing that led
to their less than stellar performance in the midterms. They haven't learned
to anything. So he's trying to walk it back. That's fine. At some point, the
Republican Party is going to have to understand. They fed him after midnight.
They let him get that taste of power. They created that appetite in Trump, then
they spilled a glass of water on him, and they created a whole bunch of little
many Trumps that are going to be just as mischievous if not more so, that are
going to be just as problematic if not more so, and are going to continue to
erode the amount of faith that the average American has not just in the
Republican Party but in conservatives in general. The Republican Party has to get
rid of Trump. They have to distance themselves. It has to occur if they expect
to remain viable. Yeah, it's gonna be annoying. It's going to be chaotic.
Probably gonna have a bad outcome, but the longer this is allowed
to continue, the longer they all placate him and do whatever he wants, the more
damage he is going to cause. McCarthy needs to continue the attempt to
distance himself. That's the best thing for the Republican Party and more
importantly to McCarthy, it's the best thing for McCarthy. Anyway, it's just a
thought, y'all have a good day.