Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the conclusion
of our four-part Halloween special.
And we're going to go over the one thing that
can tie it all together.
And at the same time, we will be able to talk about the one
franchise that realistically, deep down, I think we all know
is the scariest of all of them, Scooby-Doo.
So Scooby-Doo, at some point in time,
fought zombies and werewolves and even a guy
with a pumpkin for a head.
Over the course of this, it demonstrated
that the animals, well, a lot of times they're better than the people.
Scooby and his crew did it as a tightly knit group working to make communities that had
been impacted better.
And I think that deep down, I think we all know that Shaggy burned his draft guard.
that doesn't explain why it's the scariest because it's the one that has
the most elements of truth in it. This time of year we talk about ghosts and
goblins and ghouls and we look for the supernatural and we try to scare
ourselves. It's part of the fun of the holiday, is being scared. And we use all
of these inorganic means to do it. Legends, myths, stories. What sets Scooby
apart is that, at least originally, it showed the way the world really worked.
worked. That if you did enough research, if you followed the clues, jinkies, you looked
for the evidence and you looked for the patterns. Eventually, you would find out that any time
monsters are set upon the earth, it's generally the fault of some human trying to make some
money. Most times a rich old white guy. And the reason that Scooby-Doo is
important is because they win. They win. A lot of people who are idealistic, they
want to change the world and they look at big picture stuff and that's good and
it's really important. But Scooby and his team, generally speaking, they picked
battles that were big enough to matter and small enough to win. That is how they
built a legacy that has lasted generations. That's how they changed the
world. There are a lot of huge issues in the world. If they are solved, they will
be solved by small groups of people working towards the same goal
independently of each other, trying to make the world a better place, doing
what they can, when they can, how they can, for as long as they can.
The change that this country and this world needs, it's not starting at the top.
Nobody's coming.
Nobody is coming to save us.
There is no savior politician on the horizon.
Sometimes there's justice and sometimes there's just us.
All of the problems facing the world, they were caused by people.
They can be solved the same way.
But it takes small actions, building up over time.
It's not about getting the right person in the right spot because it's a system error.
Even if you get that perfect, idealistic person that would never sell out no matter what they're
offered, you get them into the highest office in the land, it doesn't do anything because
they can't get legislation through Congress.
That's a system-wide error, and it needs a systemic response.
It's got to be us.
Anyway, it's just a thought.
Happy Halloween!