Well, howdy there, internet people, it's Bo again.
So today, we're gonna talk about Hunter Biden
deciding to play a little game.
And the Republican response to it
being exactly what you might imagine.
Okay, so if you don't know what's going on,
for quite some time, months,
The Republican Party has set out to find something that involves Hunter Biden and President
Biden and tie it together and say that it is somehow, in some way, corrupt.
That has been their goal.
And they have repeatedly said that they have evidence.
This is the, you know, this right here, this proves it.
have to believe this. And their base has eaten it up. They've believed it 100%.
So, Hunter Biden agreed to talk to the committee in public, answer their questions.
The Republican Party said no. They want it done behind closed doors. Why? Because
their Twitter followings will not get what they expect. The evidence that they
claim they have. All of the allegations that they've made, that they've said, they have
proof of. If they had Hunter Biden in a public hearing, they would be expected to confront
him with this evidence. And they don't want to do that because odds are based on what's
available and what can be fact-checked, they don't have anything. But they keep
telling their base that they do. So if they were to do it in public it would be
completely embarrassing and their base would see through it. So instead they are
demanding that it be done in private on December 13th. And no, you don't get to
know what's what was said. All of those people that were talking about
transparency and how important it was for their base to know everything. Now
you don't get to know. No. You don't actually get to hear any of
that or see any of that. But don't worry. They'll selectively release little bits
of it, I'm sure. Stuff that's out of context to continue to play on the
gullibility of their base. Hunter Biden called. The Republican Party has been
bluffing their base and saying, oh we have this, we have this, oh here's this
check, here's this, here's that, and every time there's even the slightest bit of
investigation. The whole thing falls apart. They've left their base and Hunter
Biden has called. And now they've decided, well, they don't want to play
anymore. They want to take it behind closed doors so they can continue to
manipulate their base. Because what they will do is they will almost assuredly
take little bits of it out of context and release it and claim that that is
the smoking gun. That's the that's the final piece and don't worry eventually
they'll do something about it. It's Charlie Brown and the football and the
Republican base. They just keep trying to kick it. I would hope that the Republican
base sees this and understands that Hunter Biden agreed to come in, talk to
committee in public answer their questions, and they said no.
The only other reason they would say no is if it was literally just a fishing expedition.
Either they plan on releasing stuff out of context, or it's a fishing expedition, or
they'll call him in there, they'll ask him a bunch of questions, and most of them won't
go anywhere.
So it'll make them look silly.
Either way, the idea that, oh, they've got him, that's kind of disproven by this.
Because if they had him, oh man, that would be great TV.
You have him sitting there and you confront him with the evidence.
Seems like they don't have anything to confront him with.
Anyway, it's just a thought.
have a good day.