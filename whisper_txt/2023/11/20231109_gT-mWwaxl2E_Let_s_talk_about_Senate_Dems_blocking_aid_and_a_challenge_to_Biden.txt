Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the democratic party challenging Biden.
Maybe we're going to talk about a letter we're going to talk about a
request for assurances.
We're going to talk about the way it's being covered and then we're going to
talk about the alternative to the way it is widely being covered.
Okay, so if you missed it, 24 Democratic senators and two independents who tend to caucus with
the Democrats, they kind of said no to aid for Israel.
They've asked for assurances, more information, so to speak.
They want to know whether or not it'll be in compliance with international law.
They want to know about the steps taken for the mitigation of civilian harm.
And they want to know whether or not Israel has a plan that allows for achievable victory
conditions.
And they're kind of indicating that they may try to hold up that aid if they don't get
answers.
The way this is being covered, the way it's being framed, is that this is a challenge
to Biden from within his own party.
And maybe it is, maybe it is, I mean, that's definitely one way to read it.
But there's also another way.
There's another way to look at it.
Recently, we talked about how diplomacy works.
I think we were talking about how the Saudi Defense Minister was willing to have direct
conversations and how that was a really good sign for de-escalation because they weren't
using an envoy.
They weren't using a middleman to kind of slow down the process.
It was the person who actually made the decisions.
See in diplomacy the reason you use an envoy or somebody like that is because they can
walk in and be like, I love y'all. I love you guys. I love your culture. I believe in your cause,
whatever it is. I'm going to try to get my country's credit card and, you know, let y'all
use it to buy ray bands and berets or whatever you need. And while I want to do that, the thing
is I gotta talk to my boss first. So maybe you could help me out here. Give me something to
tell them because I'm on your side but but my boss he's different. That's how
you get leverage. That's how you get leverage and that's how you can get
concessions. The aid that is in question here. When we talked about the aid before
talked about how there's no leverage. When you're talking about the yearly aid
that everybody talks about. There's no real leverage there. And if you don't
know, that's actually already like designated out through a memorandum of
understanding. I want to say out until 2028. But the bulk aid, I separated the
two. The bulk aid, there's a little bit of leverage there. You know how you might
get it? If you were able to say, hey I love you guys. I love everything about you.
I believe in your cause, whatever it is, but the Senate?
I don't know.
I don't know that they're going to let this go through.
They want some assurances.
They want some information.
Maybe you can help me out, and that will help me sway them.
It's a great big poker game and everybody's cheating.
This is how you get leverage, and that's probably what's happening.
That would be my guess.
I don't think it's actually a big challenge from within the Democratic Party to Biden.
I think it's more about maybe some senators understanding, maybe they even talked to Biden,
and helping to create a boss, somebody that can be used to say, I got to go talk to this
other person and this is what they need. It seems more likely. My real question is
not whether or not it's a challenge to Biden. It's foreign policy. Foreign policy
it's about power and it's always going to be about power. So that's not my real
question. My real question is whether or not you think they engaged in a little
bit of like light editing or they just copied and pasted the request for
assurances and information straight out of the State Department's descent
channel because the language is super similar it's weird anyway it's just a
thought, y'all have a good day.