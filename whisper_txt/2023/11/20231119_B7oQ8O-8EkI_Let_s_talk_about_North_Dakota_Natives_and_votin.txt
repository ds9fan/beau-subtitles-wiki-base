Well, howdy there, internet people.
Led Zebo again.
So today we are going to talk about North Dakota
and natives and voting,
and a story that is going to sound incredibly familiar
to a whole lot of people.
But hey, at least this time it's not in the South.
Okay, so a federal judge has decided
that the 2021 redistricting map in North Dakota violated the Voting Rights Act of 1965.
The basic allegation in the suit, and it definitely appears that the judge agreed with
this on some level was that the legislature basically packed one tribe and cracked another
to dilute the voting power.
So the judge gave them until December 22nd to remedy this situation.
Now, North Dakota has a unique legislative schedule, so they would have to call in a
special session to do that.
The Republicans have indicated that they will be meeting with various people here shortly
to determine whether or not they want to try to appeal or get the special session underway.
There is certainly a pattern that is occurring right now with the maps that were done off
of the last census and Republicans gerrymandering their states to try to protect their power.
And the judges continue to strike the maps down and then there's continued legal battles
over it.
But at this point, it appears that the natives have won and they'll get a new map.
Looking at this, even if there is an appeal, I feel like the natives are still going to
come out on top and a new map is going to be ordered.
quite as bad a violation as we have seen in a lot of southern states
recently, but it appears to be intentional. So we'll continue to follow
this just like we follow all of the other ones and we'll find out what
happens, but hopefully this will get remedied pretty quickly. Anyway, it's just
a thought, y'all have a good day.