Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about McCarthy.
We're gonna talk about some things that he has said.
We're gonna talk about the big question,
will he or won't he continue to remain in Congress?
We're gonna talk about a message
he might be trying to send and where it goes from here.
Okay, so as a quick recap,
McCarthy, former Speaker of the House, he really wanted that position.
To get that position, he made a lot of deals and a lot of promises with a lot of different
Republicans of different factions.
So many promises that he couldn't keep them all.
When he was Speaker, he was not the leader of the party.
He was beholden to so many different factions that he couldn't lead because he had to make
good on these deals.
These deals, some of them in direct contradiction to each other, eventually led to enough turmoil
and that led to him being ousted.
Caused a whole bunch of chaos and discord within the Republican Party.
And now we have a new speaker.
Short abbreviated recap there.
Okay.
So the big question he keeps getting asked, is he going to run for Congress again?
And his answer is basically, hey, I've got the holidays, I'm going to talk to my family
and find out what's right for us.
I mean, that sounds good.
At the same time, he's also saying that he believes that the Republican party needs,
I believe he's using the word accountability for the eight people who, from his perspective,
all of the discord, cause all of the infighting, the chaos, the situation that
they're in. He wants accountability. And those things come up together a lot.
Almost as if he was saying that if he doesn't see that accountability, well
he's not gonna run again. And I know a lot of people are thinking, okay well
then don't run again. But we have to talk about the elephant in the room, McCarthy
He was not a great speaker.
I don't think there's a whole lot of people who would objectively look at McCarthy and
be like, he did a great job as speaker.
He truly advanced the Republican agenda.
I don't think that's something you're going to hear a lot of people say.
But McCarthy, while he may not be a great speaker, he is really good at something else.
Money, power coupons, fundraising.
He's really good at it.
The Republicans need that money, especially with Trump getting all of those small dollar
donations.
The Republicans in the House, they need that money.
If he doesn't run and he's not in Congress, well, he may not fundraise for them.
Now, he said that he's doing the right thing.
He's following the custom.
He is making introductions between fundraisers and the new speaker.
introduction is not the same thing as a relationship. So it seems that McCarthy
may be trying to send a message. If you want me to stay in the house, there needs
to be quote accountability for those troublemakers over there. That's what it
seems like. And if you don't want me in the house, well you better start picking
of the phone and calling a whole lot of people because you're going to need a lot of little
checks to make them for the big ones that I can get.
We don't know that, but if that's the case, eventually that will be stated bluntly in
private and the Republican Party will have a choice to make.
They'll have to decide whether or not that cash, that fundraising money, old school politics,
Whether that's more important, or whether social media engagement and culture war clout
is more important, and which one wins elections.
Given their previous performance, I mean, I know the answer to this, but I don't know
that they know yet.
I have no idea how they will decide if that does really become the ultimate question.
The end result here is that the Republican Party is in the exact same position it was
in prior to McCarthy being ousted.
They're still at an impasse.
They're still kind of a laughing stock.
There hasn't been any forward movement and they're still just sitting there and it's
creating a very negative image even among their voters.
So you have that and you have a situation where McCarthy may have more power not being
speaker than he did as speaker.
No matter what happens over the next couple of days when it comes to the budget and everything
like that. The issues within the Republican Party in the House? Oh, they're not over.
Anyway, it's just a thought.
Y'all have a good day.