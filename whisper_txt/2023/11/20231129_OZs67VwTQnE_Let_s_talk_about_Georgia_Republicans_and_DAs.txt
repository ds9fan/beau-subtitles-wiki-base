Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Georgia
and a law that the state legislature pushed through
to grant themselves a little bit of power
and it was something that a lot of people
were worried about and why maybe they shouldn't
really be that worried about it.
Okay, so we talked about it at the time.
The Georgia State Legislature pushed through this bill,
created this law that created a commission
that would go after, quote, rogue prosecutors.
It seemed incredibly obvious that this was something
they were putting together to go after the Fulton County DA.
Now, when they put the law together,
They said that the Supreme Court had to approve the rules.
Well, it went to the Supreme Court, the Supreme Court of Georgia.
And the Supreme Court of Georgia was basically like, no.
They said that they didn't feel they had the ability to regulate prosecutors
in that way because of the way the law is written,
commission can't do anything without the rules. The rules can't be given to the
commission until the Supreme Court approves them and the Supreme Court will
not approve them. So what does this mean? For the time being the commission can't
do anything. There is a there's a possibility because there have been a
a couple of Republicans saying, you know, this is a, this is something we can fix
real quick. We'll just overhaul it. Everything will be fine. Don't worry
about it. We'll do that. Yeah, I don't know about that. This was not
incredibly popular and it was more popular at the time they did it than it
is now. The other thing that's at play is because of the way the schedule works
in Georgia and the other stuff they have going on like redistricting and all of that stuff,
it seems unlikely they'll be able to get to this anytime soon, especially with the holidays coming
up. I feel like they may not want to do this once the number in the year changes and it officially
becomes an election year. That seems like something that might alter people's
opinions on it. So currently this is not a thing. It can't do anything. They do
have the ability to try to overhaul it and fix it and get it somewhat in line
Uh, but given the schedule, they may not, in which case this may just kind of be
forgotten about, which is probably a really smart idea, I feel like during an
election year, this would become incredibly unpopular, the, uh, the
likelihood of it becoming a national topic that draws a lot of attention to
the legislators involved, it grows. I wasn't incredibly worried about this to begin with
because I felt like once they tried to use it, it was going to prompt a pretty big political
backlash. But given the current situation and the way it's developed, I'm not sure they'll ever use
it because I don't know that they're going to come back after the new year
and try to quote fix it not when it's met with the opposition it has. Anyway
It's just a thought. Y'all have a good day.