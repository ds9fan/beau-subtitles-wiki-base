Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump and a recording and how he could have
stopped it, how he believes he could have stopped it and why they went, and just
kind of run through how we'll probably end up seeing this material again if you
missed it a recording of an interview that took place I want to say like two
months after January 6th. It has surfaced and in it Trump is kind of
talking about how he wanted to go down there and stop what was happening at the
Capitol. He wanted to go but the Secret Service wouldn't let him and he would
have gone down there and fixed everything.
And that's what people are focusing on, the idea that he could have stopped it but didn't.
And he says, and I could have done that.
And you know what?
I would have been very well received.
Don't forget, the people that went to Washington that day, in my opinion, they went because
they thought the election was rigged.
That's why they went.
And he could have stopped it.
believed that he could have calmed everything down and all of that stuff.
And that certainly appears to be the part that he was focusing on, was that he wanted
to stop it, but the Secret Service wouldn't let it.
And that's okay.
I feel like there's probably going to be testimony that might, you know, kind of contradict that.
However, there's something else here.
Don't forget, the people that went to Washington that day, in my opinion, they went because
they thought the election was rigged.
That's why they went.
Is that why they went?
Who told them that?
Who told them something was wrong with the election?
I feel like this is going to show up again.
this you have Trump saying that the reason it happened was because they
believed that the election was stolen. That's what he's saying. It's kind of
been demonstrated now that that's not what happened. The election wasn't
Stalin. And who told them that it was? So if they were there because of something
that wasn't true, who told them the thing that wasn't true? That's probably gonna
come up more than once, I feel like. The other thing that's worth noting is that
you have people, and I believe Trump has even echoed this himself at times, that
people put out the idea that well no it was that's not what was going on that
was that was the government the government did that a bunch of theories
about this Trump believes that they were there because they thought the election
was stolen. Doesn't sound like a fabricated narrative. If all of this
happened because people spread something that wasn't true, I feel like over time
some of those people might be, they might end up losing their love of the former
president. The support that he enjoys is based on an image, an image that is
slowly crumbling. And I don't believe that this audio is going to help him
very much. Anyway, it's just a thought. Y'all have a good day.