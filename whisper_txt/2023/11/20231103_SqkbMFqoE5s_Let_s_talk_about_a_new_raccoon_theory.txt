Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about a theory that has come out.
I have not seen this in the wild yet,
but had two of y'all send me messages
because one of your relatives asked you about it.
And so we will go through where this is probably coming from.
And it's funny because the coverage of this is recent.
This has been going on since the 90s.
But some articles just came out talking about it,
so that probably sparked the spread of the theories.
So we will talk about raccoons and vaccines and airplanes,
I guess.
Okay, so what's the theory?
That they, them, the powers that be,
the scary people that control everything
and do nefarious things in secret,
they are using airplanes to drop packages
that have vaccines in them.
So it gets into the water supply or something.
The thing is, there's actually a pretty massive
element of truth to this. The government is, in fact, using airplanes to dump
little pellets. They're not pellets. They kind of look like granola bars. All over
the Eastern United States, basically. And these pellets, they have vaccines in them
for rabies. They're for raccoons. They're for raccoons. This has been going on
easily 20 years and it just got coverage I want to say it was NPR and it I guess
it it sparked a theory that they're trying to force vaccination by dumping
this and getting it into the water supply of course that is not happening
what's occurring is they do this in areas like where I live like out in the
middle of nowhere. They will use an airplane and it has kind of like a PVC
pipe on it and it just periodically drops these little pellets that raccoons
find and eat and then they don't get rabies. And that's important because
they all mammals can get rabies but there are there a reservoir they store
it so that's where it comes from there there's no big secret about this there
has been coverage of this in the news for a quarter of a century now it's
worth noting because it is certainly going to come up now that I've seen how
this progressed. They are experimenting with something for bats. So your big
things when it comes to rabies are skunks, bats, and raccoons. For bats it's
hard to use the pellet thing because I mean they fly you know. So they are
looking into doing an aerosol that they can use to vaccinate the bats, and I am...
I cannot wait to see the theories that are going to arise out of that. But this
is all about stopping the spread of rabies. It's actually been super
successful. It's... there's a reason it's been going on for 20 years. In fact,
In fact, there's a lot of talk about expanding what they're doing and how they're doing
it.
If you hear this, that the government is dropping vaccines from the sky, I mean, it's true,
but it's for rabies and trash pandas.
Anyway, it's just a thought.
thought, y'all have a good day.