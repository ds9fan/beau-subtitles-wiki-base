Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the House Republicans
and campaign promises, promises made and promises,
well, promises made.
And we're gonna talk about gas prices
and how everything is shaping up there.
So during the midterms,
the House Republicans put a lot of talk behind gas prices.
That was their focus,
talking about how Biden wasn't doing enough
and so on and so forth.
So fresh on the news that oil companies made so much
that some of them are gonna be doing buybacks,
they've decided that they're gonna impose
some new regulations and they passed something in the House.
And of course, it's like to increase refining capacity,
require these oil companies that made all this money
to fix some infrastructure,
so maybe gas prices won't go up for the average person.
No, of course not.
It's to prohibit Biden or the president
from doing draws from the strategic reserve.
Remember when Biden put the oil on the global market
in an attempt to get prices down, remember that?
They wanna stop him from being able to do that.
Now, to be honest, I know prices at the pump dropped,
but I do not know that there's a causal link.
I didn't look into it because that's not really the point.
The point is they don't care about gas prices.
They obviously don't.
They're not doing anything in an attempt
to make it to where they don't go back up.
In fact, they're attempting to limit something
that might've helped
because those were just the lines
that they fed to their base.
That was just something that they used
to trick them and motivate them.
They don't actually care about the commoners.
Silly.
Okay, so what does this tell us?
One, promises made are definitely not promises kept.
They do not care about gas prices for the average person.
Their focus is not on helping them.
Their focus may be on hurting them
in hopes of it helping the Republican Party in 2024.
And it shows that the House Republicans are rudderless.
They've passed this in the House.
They think it's gonna get through the Senate?
They think Biden's gonna sign it?
It's more action for social media engagement.
It's more talk designed to manipulate those
who don't actually follow through,
who believe that they're gonna win.
Follow through, who believe whatever their bettors tell them.
It's designed to trick those people.
That's what it's there for.
This isn't going anywhere.
But while they gain absolutely nothing
because there's no way the legislation advances,
they broadcast very clearly
that they didn't care about gas prices,
that that was all talk
because they're not actually trying to regulate
the companies who are engaged in something
that until I think the 80s was flat out illegal.
They're just trying to limit Biden's power to help,
which is a bold strategy, I guess.
This might be something to bring up
with your relatives at some point.
Anyway, it's just a thought.
Y'all have a good day.