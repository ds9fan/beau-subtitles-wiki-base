Well, howdy there, internet people.
It's Beau again.
So today we're going to talk a little bit about Ukraine
and the apparent US shift when it comes to strategic goals.
I had a couple of questions about why you have major
figures in the United States suddenly kind of indicating
that they really hope that Ukraine doesn't try to go into Crimea.
Okay, so, and it's not just going to be the US on this.
It's going to be the West in general.
Leaders in the West are very aware of the fact that all wars are popular for the first
90 days, and then support begins to erode.
The West, they're saying, yeah, we're going to support Ukraine forever.
And yeah, I mean, that sounds good, but the reality is, nations don't have friends.
They have interests.
And there's a limit.
I would imagine that the reason for this being put out there is that retaking what
was taken in the most recent invasion is probably going to be easier and it puts Ukraine back
to the position it was in before the West started getting involved.
And there's probably a fear of moving into Crimea and widening things, making it last
longer which will require more Western support.
That is, that's your most likely answer.
There are other ones though.
It is worth remembering that from the outset, the Biden administration was like strategically
nothing about Ukraine without Ukraine.
They get to make the calls.
And that is actually the right take.
This new shift, it deviates from that a little bit.
The United States and the West haven't been dictating strategic goals this entire time.
I would note that if I was facing an opponent that believed in their own propaganda, that
believed what they put out and the rhetoric that they used, and that opponent believed
that Ukraine was just a puppet of the West.
If I wanted to go after Crimea, I would probably have a high-ranking general, maybe the Secretary
of State, advise Ukraine publicly against doing it.
if the opposition takes the bait, well, they won't be ready for it.
At the same time, if you're dealing with a country that does historically have a pretty
good intelligence service despite recent developments, you might put that out there and then create
some chatter to make it seem as though you are going to go after Crimea when you're not.
This is messaging.
It's messaging on the international level.
It's sending signals.
If the West wanted to tell the government in Ukraine that they didn't think that they
would benefit by going into Crimea, they would do it via phone call.
They wouldn't do it via interviews, this is messaging, it's trying to signal something.
Remember all sides use propaganda, all sides conduct information operations, and those
of us watching, we get caught up in them.
There are a lot of possibilities with this.
One is that, yeah, the West really doesn't want them to go into Crimea.
They would prefer they go after the territories that were recently taken.
The other is they're engaging in an information operation.
The other is they're engaging in a different kind of information operation.
I would suggest that given the fact that support for continuing to back up Ukraine with supplies,
it isn't just dropping like a rock.
It's declining because it always does, but it's not dropping in a drastic fashion.
I would guess that there might be signaling going on that is really the opposite of what's
being said, and it may be kind of a wave to Putin to say they're at the point where they're
considering retaking Crimea.
it's time to go home and cut your losses but that's that's just one more option
It so anyway, it's just a thought y'all. Have a good day