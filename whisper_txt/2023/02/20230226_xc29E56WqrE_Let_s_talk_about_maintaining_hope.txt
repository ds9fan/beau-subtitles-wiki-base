Well, howdy there, internet people.
It's Bo Gown.
So today, we're going to talk about hope.
We're going to talk about hope and how to maintain it.
Because over the last couple of months,
it's been a recurring theme in a lot of messages.
People asking how to maintain hope,
how to keep their spirits up, and what methods you
can use to stay positive when, you know, when things aren't going well.
And the messages came in from just a wide variety of people.
Some people have economic issues, some it's romantic, some it's their situation and the
changes that they're going through.
Some of them are people who are part of a community that is being actively targeted
by legislation.
But the question is always the same at its root.
How do you maintain hope?
Ready for a silly answer, silly, lame, cliche, whatever you want to call it?
Be the hope for somebody else.
Be the change you want to see in the world.
And I know it's cliche.
It sounds cliche, but it's true.
It is true.
And I think the reason the saying has been around for so long is because it is true.
And it's true on two different levels.
There's the observational side of it.
of the people you know that are just Mary Poppins types, always full of hope.
Are they typically those who don't help?
Are they typically those who don't try to encourage others?
They're not, right?
They're overwhelmingly, sometimes annoyingly, positive.
And then you think about the actual act, the action-based side of it.
If you're the person who is helping somebody who believes they're helpless, who is providing
Hope to the Hopeless.
What do you get to witness with your own eyes?
You get to see that hope restored.
You get to see that help pay off.
And sure, it helps them, but it helps you too.
It keeps you motivated.
And staying in the fight, so to speak, it keeps your hope alive.
I was venting to somebody who has worked on a similar cause with me, the same cause.
And I was just talking about how because of the pandemic, like there was this long period
where I didn't get to do anything, like hands-on.
I had the channel and we could help that way.
But as far as me going somewhere and doing something, I didn't get that.
And I need it.
I know I need it.
down after Ian, after the hurricane, that may have helped me more than the people there
because it restores that feeling.
It restores that hope.
You get to see it, not just in what you do, but you see other people helping too.
Mr. Rogers type stuff, look for the helpers.
They're always there.
That is a big part of it, at least for me.
The way I can stay positive, the way I can continue no matter what, because I do what
I can, when I can, where I can, for as long as I can.
If you find yourself lacking in hope, there are people who are in a worse situation who
could use your encouragement, your assistance, and it doesn't even have to be big.
Sometimes it can be something small, and you can see that small action pay off in them
and how it helps them.
And when you see that little change, it can give you hope.
Because you understand and you witness things getting better.
Yeah, with some of the problems that people are facing, it's going to take a whole lot
of people to be the change they want to see in the world.
And it's probably going to get uncomfortable along the way.
But as long as there are people out there who are still in the fight, hope's not lost.
It's still there.
And if you want to see it, get close to it.
Get close to it, help.
Get in the fight.
Put yourself in a position where you're around those people who are actively trying to make
things better.
Hope's contagious.
Anyway it's just a thought.
Have a good day.