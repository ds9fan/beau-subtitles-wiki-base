Well, howdy there, internet people, it's Beau again.
So today we are going to talk a little bit more
about the balloon situation
and everything that is developing there
and the balloon gap between the US and China.
To start off, it's worth noting that the Chinese government
has now kind of indicated that over the last few years,
I want to say too, but can't remember the exact number of years, they have picked up
like 10 balloons that they believe were US surveillance balloons.
Had a lot of questions about that.
Is it true?
Is that possible?
I am not aware of a US high altitude balloon surveillance program.
I would not be surprised if one existed, and if it did exist, it would certainly want to
take a look at China.
Is their claim plausible?
Yeah, of course.
Surveillance flights happen all the time.
Even if the U.S. is not using balloons over their territory, it is certainly engaging
in other forms of surveillance flights.
happens a lot. Okay, next piece of information. The U.S. did recover what
appears to be the main sensors off of the initial balloon, that first balloon they
were able to pull that out of the water. They will Humpty Dumpty it and try to
put that thing back together. It will lead to them developing more counter
measures and understanding more about how it works and what information they
were trying to gather. It might also confirm the Chinese claim that it was a
research balloon that just got away from them and whatever. My best guess would
would say that what they're going to find out is that it is information that
could plausibly be for civilian uses but also has a military application. My
guess. The United States has said now they are picking up more aircraft, more
unidentified objects, because they have dialed in their equipment. They have
removed the filters that would filter out smaller and slower moving things
in the sky. So it's picking up the balloons now. It is worth noting that at
least one of the ones that has been taken down recently, it appears to be a
balloon but did not have a payload of any kind. So that very well may have just
been some random research project that has been floating around up there for
however long. So there is that. Over the weekend, Congress will receive a classified briefing about the balloon race and
everything that is going on with it. The briefing is almost certainly tailored with what I am about to say in mind.
Congress often asks, like why they aren't told everything
thing from the Defense Department.
My guess is that shortly after this briefing you will find out why on Twitter or some like
social media outlet where the people who receive the briefing disclose things they probably
shouldn't have.
This is why a lot of those briefings are nowhere near as detailed as they might want them.
in Congress tend to not understand what is fit for public consumption.
So those are the big pieces of news about the balloons that have come out.
Again, the one thing that I want to stress, surveillance flights are normal.
They happen all the time.
It's not anything to be afraid of.
It's not anything to allow that should unnerve you.
Other countries do it to the US, the US does it to other countries.
Recently I said every country does it and some people in the comments were like, I live
in Ireland or I live in Austria.
Okay, not every country, but most.
have a program aimed at using aerial craft to surveil other countries. Not all
of them go over the other's airspace, but most have something like that, and those
that don't often rely on intelligence shared from countries that do. It's an
incredibly common thing.
It should not be generating the amount of fear
that it is generating.
Anyway, it's just a thought.
I hope you all have a good day.