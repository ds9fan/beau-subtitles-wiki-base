Well, howdy there, internet people, it's Bo again.
So today we are going to talk a little bit more
about Senators McConnell and Scott
and the situation that has been developing there.
Little bit of background in case you missed it.
Scott challenged McConnell
for Republican leadership in the Senate.
He did not succeed.
McConnell, being McConnell, didn't really do anything about it and then not too
long ago he pulled Scott and another person who assisted in challenging him
off the Commerce Committee. It's a pretty big punishment. That's a very powerful
committee and most people kind of thought that was going to be the end of
it. But apparently the feud between Scott and McConnell is a little bit bigger than we knew.
McConnell is somebody who waits for the opportune time to get even, so to speak.
With all of the trouble that Scott is in right now over sunsetting federal legislation and that
plan and how it would impact, you know, social security and Medicare and Medicaid and all, everything.
McConnell just kind of put it on out there. Most Republicans are going on to radio shows and TV
shows and tweeting about how that isn't, that was not the plan. That is not what was going on. Biden
is making that up that's not true even though they're literally video clips of
some of them like actually saying it and the plan was spelled out a lot of
Republicans don't care about that because they know they're not going to
be fact-checked by conservative outlets McConnell got on a radio show was like
that's not a Republican plan that's a Scott plan and just I mean put it
put it on out there. He talked about what a bad idea it was and how he and McCarthy
had already talked and nobody was going to touch Social Security or Medicare.
It wasn't gonna happen. He then went on to talk about how this was just, you know,
such a bad idea and it might cost Scott when it comes time for re-election
because Scott's from Florida and there's a lot of elderly people in Florida. Social
Security and Medicare, that's super important. And what this tells us is that
this feud is way bigger than anybody knew. Something happened beyond what we
know. McConnell is somebody who generally follows the 11th commandment as set
forth by Ronald Reagan, you know, do not speak ill of another Republican, and his
main priority has pretty much always been to maintain Republican power.
Pushing Scott out there like this, close to an election, and even talking about the election consequences of it, that's
unique, that's cold even for McConnell.  I would imagine that Scott is going to have to try to dazzle.
I would expect some big news from Scott because I think he understands he's in trouble.
He has McConnell now signaling to Florida, to Republicans in Florida,
you might want to pick somebody else.
So, I would expect Scott to either do some grandstanding or propose some wild legislation
to get some headlines, do something to get the attention off of this plan, which according
to McConnell, leader of the Republican party in the Senate, that's a Scott plan.
Rick Scott plan. Put it all on him. And to be fair, I didn't see McConnell at any
point in time support that plan when it was put out there. And I don't know that
that plan had significant Republican support. So everything that's being said
may be completely accurate, but the fact that it's McConnell saying it while many
other Republicans are trying to do what they can to help Scott out signals that
McConnell is done and he is ready for Rick Scott to go home. It is certainly an
interesting development and it is not going to cool the tensions between the
MAGA faction and like the normal Republican faction. It may elevate them
so you may see a lot of wild stuff over the coming weeks. Anyway, it's just a
thought, y'all have a good day.