Well, howdy there, internet people, Lidsbell again.
So today, we're going to talk about Russia and coffee makers
and hair dryers and refrigerators.
The United States has announced its latest round
of diplomatic efforts against Russia,
and it has prompted a whole lot of questions
And we're just going to go through and explain
why these items are on the list and what the United States is
hoping to accomplish and whether or not
it's going to really be that successful.
If you missed it, the US announced its new round
of sanctions against Russia.
Some of the items on the list, that's a little surprising.
It is.
It's coffee makers, high-end coffee makers,
refrigerators, hairdryers, microwaves, stuff like that.
And people are asking, why?
It's not the product.
It's what's inside of them.
We have talked on this channel for a while
about Russia's lack of precision-guided munitions
and how a lot of their higher-tech equipment
they can't make because of previous rounds of sanctions.
They're having a really hard time getting the components
to make some of their weapons.
Because of that, they started finding the components
in other items to include all of the household goods
you see on that sanction list.
Yeah, dual use coffee makers are apparently now a thing.
So the Russian military, they have
been taking the chips and semiconductors, stuff
like that, the components, out of these electronic devices
and repurposing them, using them to create weapons.
That does say a whole lot about the current state
of the Russian defense industry.
It does, in fact, show that, yeah, the sanctions are working.
At the same time,
the Russian defense industry found a way around it
through coffee makers.
Now, the United States
is sending out these new sanctions.
Are they really going to be that effective? I am skeptical.
I'm very skeptical of how effective
this round is going to be, mainly because the United States doesn't actually make a
lot of this stuff.
It's not produced here, therefore it's not shipped from here.
For this round of sanctions to be effective, the U.S. has to get other countries to play
along with it, to participate and to truly participate.
These are not items that, you know, customs in any country is really, you know, traditionally
looking for.
That's probably not going to change much.
So even small-scale operations over time could send a lot of this stuff through.
I have serious doubts about the effectiveness of this.
It seems to be more symbolic in nature and also it might be a little bit of poking fun
at the same time.
Drawing attention to this deficiency in the Russian defense industry, there's a propaganda
win there that they might be angling for. I mean imagine another country barring
the United States from getting this kind of stuff and in the message it would
send you know Raytheon or Northrop or whoever is dependent on coffee makers to
get their equipment to get the components they need. There's a huge
propaganda element there. I don't know that this is going to be incredibly
effective but that is that's what they're at least attempting to do is
stop the flow of components that are inside of these devices that they're
they're sanctioning. Yeah we'll just have to wait and see how this one plays out.
I'm skeptical, but the international community may come together on this one.
At the same time, this is one of those things where the sanctions, if effective, would be
felt by the average person.
This is something that would be noticed, and it would be hard for the Russian government
to kind of sweep this under the rug or wash it,
especially without washing machines.
Anyway, it's just a thought.
Y'all have a good day.