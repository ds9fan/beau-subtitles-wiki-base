Well, howdy there internet people, let's bow again.
So today we are going to talk about Trump
and terminology and some political news
that is just straight out of South Park.
Okay, so I'm gonna start by reading you a quote
and then we're gonna talk about the quote
and why it was said.
Okay, I don't like the term woke
Because I hear the term woke, woke, woke.
It's just a term they use.
Half the people can't define it.
They don't know what it is.
I mean, that sounds like
something a pretty progressive person would say.
That was Trump.
Trump said that.
You have to wonder why, right?
It's the one thing that Trump's good at.
Trump is good at branding.
He's really good at that.
It is his, it's his one real skill.
It's why he can have so many failures
and still continue as a brand.
He really understands how it works.
He also understands how to destroy a brand apparently.
I guess that knowledge goes both ways.
His leading opponent in the Republican primary
has branded himself as the anti-woke culture warrior.
If Trump is successful at turning the Republican base
against the term woke, he leaves his opponent
with no real credibility.
He doesn't have a brand anymore.
In fact, without the word woke, I don't know how you would describe him if you
were supportive of him, I would imagine that Trump is going to be on this kick
and he's going to try to get people to be more descriptive about what
they're, they're upset with this is good.
And it's bad, it's good because if he's successful, it is severely
to damage another person that is very intent on kicking down. It's bad because
it's going to alter the rhetoric. One of the things that Trump did over his time
in politics was slowly reduce the need for dog whistles. Woke is a dog whistle.
If you don't know, pretty much any time the word woke is put into a sentence, it could
be substituted with a description of a marginalized community, and the sentence would still make
sense.
Anti-woke could mean anti-black.
It could be anti-LGBTQ.
It depends on the circumstances.
But woke can pretty much always be substituted out for a targeted demographic.
If they get rid of the word woke, they may just start saying what they mean.
So we have a number of things to keep an eye out for.
continuation of Trump trying to steer the Republican Party away from this term
because he knows it will destroy another primary candidate's brand. We have to
watch for that candidate to rebrand themselves and we have to watch and see
how the Republican base is going to respond to that term no longer being in
favor with dear leader and it's probably, I have a feeling it's going to make the
rhetoric more direct, more aggressive, which isn't great, but at the same time
there may be benefit to that too in the long term. It's one of those things that
causes the mask to slip, and the people in the center, they don't mind voting for
people who are kind of flashy, but they want that mask on. If it starts to slip,
they may lose support. Anyway, it's just a thought. Y'all have a good day.