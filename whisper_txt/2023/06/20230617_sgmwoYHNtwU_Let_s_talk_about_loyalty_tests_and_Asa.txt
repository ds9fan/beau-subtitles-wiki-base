Well, howdy there, Internet people. Let's bow again.
So today we are going to talk about loyalty,
pledges, and oaths, and Asa Hutchinson
and the Republican Party. Because
Hutchinson had some questions. You know, the Republican Party,
they want any candidate
who is going to be on the debate stage
running for president, they want them to sign a pledge, take an oath that says they are going
to support the eventual nominee. Given one candidate's legal entanglements, well, Hutchinson
had questions. And according to the reporting, the Republican Party was just like, tough.
If you want to be on the debate stage you have to take this oath. You have to
sign this pledge to remain loyal to whoever the nominee is. I mean I
understand where Hutchinson's coming from on this one. It makes a whole lot of
sense, he was seeking some kind of clarification, an amendment, because he
believed it to just be inconceivable that the Republican Party would demand
that he remain loyal to somebody who might be convicted under the Espionage
Act. He said, I'm not going to vote for him if he's a convicted felon. I'm not
going to vote for him if he's convicted of espionage, and I'm not going to vote
for him if he's convicted of other serious crimes, and I'm not going to
support him. They need to put a little rationality to what is said in that oath
or that pledge. I mean it makes sense, right? The ultimate irony being, of course,
that if pledges, oaths, and loyalty meant anything to Trump, this conversation
wouldn't be taking place. They're demanding people who apparently take an
oath seriously, take a pledge seriously. They're demanding that they extend that
oath to somebody who would never abide by it. When Trump assumed that office,
when he walked into the Oval Office, millions of lives became his
responsibility. A whole lot of people live and die based on whether or not
certain information remains secret and he took that information and put it in a
bathroom next to what looks like a copying machine. I don't know that loyalty
and oaths are something that are really going to be honestly extended to the
former president. I have to admit that on some level Hutchinson actually
questioning this and saying hey we need to change the wording of this. I mean
And that's almost quaint, right?
A politician who apparently cares enough about an oath or a pledge to make sure it's something
that they can live up to.
That's unique.
I mean, I don't particularly like Hutchinson, but I'll give him that.
I have a feeling that this is really going to come back to bite the Republican party.
That those making this demand of candidates are going to regret it.
Because if Trump is convicted, they have a whole slate of nominees who will have pledged
to have supported him if he won a Republican popularity contest.
And they also have to wonder what's going to happen if he's convicted and he still wins
the nomination because it's possible. Then you have a whole bunch of Republican
candidates who will have attack ads for the rest of their careers because they
took a loyalty oath to somebody convicted under the Espionage Act. That
That doesn't seem to be good long-term planning to me.
Anyway, it's just a thought.
y'all have a good day.