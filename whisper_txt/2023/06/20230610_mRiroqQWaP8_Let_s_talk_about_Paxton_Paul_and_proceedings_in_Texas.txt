Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Paxton,
Paul, proceedings, and Texas.
OK, so the attorney general of Texas, Ken Paxton,
the impeachment has gone through,
looking at the trial sometime soon.
A person who is central to a lot of the allegations related
the impeachment is a man named Nate Paul. Nate Paul was just picked up by the
feds. Eight counts of making false statements, I think while seeking loans,
it appears from the allegations that basically he's alleged to have
overstated assets and understated liabilities. This occurred in 2017 and
2018. Now it's worth noting that the the whistleblowers that came out of Paxton's
office kind of allege that Paxton attempted to use his office to benefit
Paul. It's also worth noting that in the reporting it seems as though Paxton is
also still currently under investigation by the FBI. So when the
impeachment proceedings started and they just kind of came out of nowhere, we
talked generally about how it was incredibly likely that the Republican
party in Texas was aware of things that us commoners were unaware of. That
certainly seems to be the case. My guess is that there is going to be more
information that comes out prior to the trial. There will probably be
more developments that under normal circumstances would be front-page news,
but while it is likely that it'll still make the news, it might be overshadowed
by something else. I mean maybe there will be another high-profile case
involving a politician that, you know, kind of gets more coverage. So while
everybody is going to be focused on Trump, it's important not to lose sight
of the other things that are going on. As far as the proceedings with Paxton, it is
important to remember that there's actually links between Paxton and
Trump's attempt to alter the outcome of the election in 2020. We don't know if
that has anything to do with this. But Paxton assisted in a lot of ways.
There's probably a whole bunch more that is going to come out and it's probably
not going to be pretty for somebody who was very much an up-and-coming star in
the Republican Party, not just in Texas, but nationwide.
This is also something that will likely impact elections in Texas in the future.
So we can't lose sight of it just because something a little bit more historic is ongoing
as well.
Anyway, it's just a thought, y'all have a good day.