Well, howdy there internet people, Lidsabow again.
So today, we are going to talk about Trump and Smith
and the little game they are playing
because at this point in time it,
well, it does appear that Smith
is holding at least a pair of queens.
Reporting now suggests that Mike Ronan,
who was a campaign official for Trump in 2020,
has entered into a proper agreement with the feds.
Okay, so this is not a person
that has a high public profile, okay?
But this is a person that many believe had a lot of access
when it comes to alternate electors and stuff of that. He also is reported or
believed, I should say, believed to have worked closely with Rudy Giuliani. In
fact, during the House Committee, during those hearings, he was asked specifically
about his conversations with Giuliani after the election. He pled the fifth.
So, something else to know is that this proffer agreement was entered into reportedly once
the Department of Justice was like, hey, by the way, you're gonna talk to the grand jury.
it's also worth noting that a while back they took his phone. Okay, so all of this
would be related to the election interference, January 6th, maybe the
fundraising stuff. Keep in mind that if the Department of Justice wanted to, all
All three of those could be completely separate cases.
The walls that have protected Trump for so long, there's a lot of cracks in them right now.
The cracks are starting to show.
I am of the opinion that Smith actually has another queen up his sleeve that
that hasn't been publicly confirmed yet.
The rate at which this information is now coming out leads me to believe that by the
time this actually goes for, it reaches the point where they're actually seeking the indictment,
going to have boxes of evidence. So much so that Smith might have to store some of
in his bathroom. Anyway, it's just a thought. Y'all have a good day.