Well, howdy there, internet people, it's Bo again.
So today we are going to provide your yearly heads up.
This is primarily for anybody who is in the islands,
along the coast, the eastern coast, the Gulf Coast,
pretty much anywhere in the southeastern United States,
so on and so forth.
It is hurricane season.
There is a tropical depression down in the Gulf right now
that is expected to strengthen into a hurricane.
Where's it going?
Nobody knows yet.
But what we do know is that it's that time of year,
which means it's time for you to get your stuff together.
Do it now.
Don't wait.
Don't wait until you realize you're in the cone, okay?
Go ahead and anything that you wish you had
during a previous storm, make sure you have it.
This is a fact of life now.
It's going to continue to happen.
It needs to become routine.
As soon as that first storm develops,
I actually think this is the third.
But you need to start getting ready right then.
You need to start putting your plan into action,
or at least figuring out what it is.
So you have it ready for the rest of the year.
So you're looking for non-perishable food,
the water you might need, if you want propane to cook,
if you plan on staying.
your meds, all of that stuff, because you know,
once that cone moves towards your direction
and your city or your county is inside the expected path,
everything is gonna sell out
and you're not gonna be able to get it.
If you get it now, it's a lot like putting your mask on
before you try to help somebody else on an airplane.
If you have your stuff,
you are more capable of helping other people.
Now is the time to plan your evacuation route, talk to the people who you may be going to
stay with, something that people tend to forget, pet carriers or kennels if you are going somewhere
that isn't set up to house your dog, your animal, whatever it is.
Make sure you have a way to transport your furry family members.
Now's the time to do that.
Get it all set up, get it all ready, put thought into it now.
And this is especially true if you don't have a bunch of resources, you can start doing
it piece by piece.
Get 24 pack of water the next time you go to the store and just build it up.
Canned goods are pretty cheap and then you don't have to worry about it.
One of the questions that prompted this was somebody asking me about what tools they need
for recovery.
Reminder, I have an entire second channel and the question that came in is pretty much
the exact duplicate of a question from over there asking about a very specific brand of
tool because it is really inexpensive.
went and bought those last year got like a whole set and used them. If you don't
want to watch the video, short version, hard tools, pretty decent. They are not
like DeWalt or anything that you're gonna pay several hundred dollars for
an individual tool for, but they work. They are not horrible in any way shape
perform. So now's the time to make your preparations, that way you don't get
caught off guard. Remember those cones shift, now's the time. It makes it easier
for people who come in and do relief if more people are set up to begin with and
it also enables you to help your neighbor if you have, if you end up in
that situation. So put the thought into it now. Put the thought into it this
weekend. If you have time, just figure out what you're going to do. Make sure
you have everything extra that you're going to need for a couple of weeks,
laid on, put it in the pantry, the back of the closet, whatever, and leave it
there. If you normally keep your stuff and you stay stocked up, now is the time
to kind of go through and check. Make sure everything is still good. Make sure
you have everything you need. I would strongly recommend a battery-powered
radio. This is something that people have less and less. Remember, sometimes that's
the only way to find out where those National Guard resupply points are. So
maybe lay one of those on if you don't have one. Anyway, it's just a thought.
Y'all have a good day.