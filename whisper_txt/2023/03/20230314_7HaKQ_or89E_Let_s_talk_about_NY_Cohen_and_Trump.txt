Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Michael Cohen
and Trump and New York and everything
that's going on up there.
A lot of questions have come in, and there
is a lot of excitement around the situation that's
developing up there.
And there are a lot of people who are setting themselves up
to have their hopes very, very high on these proceedings because there are a lot of indications
that New York is going to move forward with a criminal case against former President Donald
J. Trump.
And people are pointing to Cohen.
I haven't been too excited about this case because we don't know what they have other
than Cohen, really.
We don't know what's there.
When you hear Cohen speak, this is a person who can speak very convincingly.
Here's the thing, he testified, grand jury, all that stuff.
a grand jury testimony, depositions beforehand, all of this, this is different than doing
it at trial.
Testimony beforehand, that's not cross-examined.
That's not something that is subject to intense scrutiny from the defense.
Trump's defense is going to go after Cohen pretty heavily, I would think.
You're talking about a person that has a long history of public statements that show him
to be very adversarial towards Trump.
A good cross can sow doubt with that, and then you have Cohen's own past.
There's a lot that is up in the air.
If New York doesn't have some heavy documentation and something other than Cohen to frame it,
I think there's a good chance that the case falls apart during cross.
That Trump's defense can put up a good defense.
This is not one I'm getting my hopes up about, and it has nothing to do with my personal
beliefs about what happened.
We're talking about the legal system here, and once it progresses to the trial phase,
his statements are going to be cross-examined, and the defense is very likely to try to paint
him as somebody who is out for revenge.
Even though Cohen says that that's not what it's about, that may not be what comes
across during the trial.
So while, yeah, absolutely, the indications certainly look like New York is moving ahead
with seeking an indictment.
That's what it looks like.
I am not going to get my hopes up about it because we don't know what they have, really.
And until we have a clear picture on the documentation or supporting evidence to back up Cohen, I
can't bring myself to think that this is going to be a case that moves forward well for the
prosecution.
Now that being said, there are a lot of people, a lot of analysts who follow this stuff, and
this case in particular, who've been following it very closely, who seem to believe that
they have way more than they're letting on.
And that may be the case, but as far as what's widely reported, I have reservations about
saying that this is the one that's going to move forward on it.
It might be, but keep in mind, Trump has had time to build a very strong defense for this.
If a criminal indictment does occur, even once that happens, manage your own expectations
until trial.
Anyway, it's just a thought.
So have a good day.