Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about a conspicuous absence
on Fox News and why it might be happening.
A very prominent figure on the Republican side of the aisle
hasn't really been around.
And there are now indications of at least what
people in his orbit believe is the reason.
If you don't know, Trump hasn't been on Fox News since September when he was on with Hannity.
The reporting now has a quote from somebody close to Trump.
Everybody knows that there's this soft ban or silent ban.
It certainly, however you want to say it, quiet band, soft band, whatever it is, is
indicative of how the Murdochs feel about Trump in this particular moment.
Now a bunch of news agencies have reached out to Fox for comments and they haven't
gotten any, at least not at time of filming.
The suggestion that Trump has been placed on a do-not-let-him-on-the-air list is, it's
not unbelievable.
It's not something that's out of the realm of possibility.
Given Fox's current situation with the depositions and all the cases that are going on, they
might be trying to limit liability.
The former president coming on at a time when he continues to echo the claims about the
election and all of that stuff is probably not exposure that Fox wants right now.
So while there is no confirmation of this beyond what appears to be a suspicion from
within the Trump circle, it's not as far-fetched as it might seem.
You know, a lot of the things that come out from Team Trump are less than reliable.
But this seems plausible.
If I was running stuff at Fox, I don't think that I would want the former president on,
Especially not right now.
But also given the fact that it does appear that Murdoch is looking for another candidate
to back and raising the profile of Trump's opposition in the primary, it really does
seem to track.
There are candidates that are just low-level, long shot, know they're not going to make
it, that are getting time on Fox.
But Trump, he's nowhere to be found.
So with a lot of the upstart right-wing news outlets losing viewership and losing deals
to get into homes, Trump's lack of presence on Fox, that might be something that truly
hinders his ability to move forward with his desire to return to the White House.
Given everything that we're seeing and the lack of support coming from high ranking members
and big funders.
Trump is becoming more and more relegated to a position where his rhetoric influences
the base that then influences policy makers.
The odds of him holding a position again seem to be ever decreasing.
Anyway, it's just a thought, y'all have a good day.