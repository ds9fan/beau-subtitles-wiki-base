Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about Fox and Zoom
and the New York Times.
And I'm going to do something I pretty much never
do on this channel.
I'm going to give you links to articles, quite a few,
actually.
In a recent video, I tried to manage expectations
and talked about how these cases, like the one that is going forward against Fox, they
normally don't win.
It's incredibly hard to prove defamation.
It's hard to demonstrate what you need to demonstrate to show that a media company has
gone that far off the rails.
The New York Times has reportedly obtained a Zoom call of Fox higher-ups.
The call is from November 16, 2020, according to their reporting.
The call itself and the communications that kind of stemmed from that are nothing short
of amazing.
are very telling. There's a lot of quotes in it worth reading. I will link the original
article and some other summaries of it. It's worth going through, particularly if you ever
watch Fox, because this takes you behind the scenes and you get to see some of the things
in the decision making process that generally you might not be able to see.
Here is one of the more telling quotes, listen, it's one of the sad realities.
If we hadn't called Arizona those three or four days following election day, our ratings
would have been bigger.
The mystery would have still been hanging out there.
There was an entire discussion over this.
Now, if you don't remember, during the election, Fox called Arizona, correctly, mind you, for
Biden, and they did it first.
At most news agencies, that's a good thing.
Like that's something you should be proud of.
Not just were you right, you were also first.
Being right should be most important, it often isn't, depending on the outlet, but they were
were right and they were first. That should be a win. But they were actually upset because
their audience was mad about it. And there was even reportedly, depending on the articles
you're going to look at, some of them will tell you that there was actually discussion
about maybe taking it back or maybe not making calls so soon because they don't want to upset
their viewers with the truth.
That's pretty wild.
Here's another one that I just found amazing.
But I think we're living in a new world, in a sense,
where half of the voting population
doesn't believe in big corporations, big tech,
big media.
There's a lack of trust.
when they feel like things are being done behind closed doors and rooms that they can't understand it
Exacerbates the emotion and how they feel about the process
Fox is a
It is is a big media company. They're they're a big corporation and I
mean, I
Don't think they're they qualifies big tech
Necessarily, but I mean two out of three isn't bad, right?
And as a news organization, it would be their job to inform them. But when you
look through these conversations, that isn't what I take away from it. That's
not what I take away as them believing their mission is. It seems like their
mission is more to protect the brand, which is a term that comes up. Again, don't
often do this. There are going to be links down below. Read the articles. If you
want to understand the decision-making process and some of the stuff that
happened at Fox, it's definitely worth your time. I'll even put some summary
videos that go through some of the quotes or something like that down there
as well. It's wild. It is wild to me. All of that stuff I said about managing
expectations and how it's gonna be really hard for the machine companies
to actually get ahead on this and move forward. Yeah, if there's more of this
type of stuff in what they have to use during this case, yeah I take all that
back. I take all that back. It may be a lot easier. Again, all of this stuff has
to be tested in court and all of that, but I mean, well you'll see when you read
it. Anyway, it's just a thought. Y'all have a good day.