Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Bison and Biden
and the Inflation Reduction Act and the Secretary of Interior
and what all of this has to do with each other.
And we're going to answer a question that I found funny
just because of the way it was phrased.
So what's going on?
As we talked about at the time, the Inflation Reduction Act
had a lot of money for the environment in it.
Some of that money is going to be
used to help lift the populations of bison.
The Secretary of the Interior has
decided a good way to do this would
be to transfer some of the bison from federal land
to tribal land and allow the tribal governments and entities
to kind of take the lead on bringing this animal back
and helping to further rebound the populations.
The bison is coming back.
The numbers are measured in the tens of thousands.
That's good.
It's worth remembering at one point in time
they were measured in the tens of millions.
They were hunted to the brink of extinction in the 1800s.
They are a keystone species.
What this means is as they move, as the populations on federal land and tribal land, as they rebound
and there's more and more of them, and hopefully as they're allowed to move from area to area,
they fix the environment along the way.
They have such a marked impact on the area around it that it alters the entire ecosystem
for the better.
There are a number of animals like this.
Elephants in Africa is a good one where there's a lot of research on it.
Bison are in the same category.
So this is something that if allowed and successful, this is something that would help every
out there. So it's a great idea, it's a unique method of doing it. And then we
have this question, just wondering if you really think the native governments will
be better at protecting the buffalo than the US has done overall, or if this is
just some weird tokenism. Do I think the tribal governments will do better at
protecting the bison than the US government overall? I mean you
understand what you're comparing it to, right? I mean the answer is yes. I mean as
long as the tribal governments don't hunt it to the brink of extinction,
they're gonna do a better job. I happen to think they're gonna do a far, far
better job. And it's not just some weird image that people like to play into. It
has to do with the land, it has to do with the balance, it has to do with
conservation. There's also a very strong economic motivator. This is something
that the tribal governments will absolutely handle in a better way.
They're far more vested in all ways, in any way that you can think of, in any way
you can categorize, the tribal governments are going to be more
interested in a positive outcome than the US government. So yeah, I think
they'll probably do a better job. And historically, again, it's not like they
can do worse. I mean by default they're gonna do better. It's one of those things
that's buried in the Inflation Reduction Act that is it's going to have a
positive impact. Might be a while because normally stuff like this takes some
time but this is going to be one of those things that when it when the
outcomes are all counted and everything is looked at afterward. This is going to
be a win. It's just getting started, it's too early to call it that, and all of
that stuff, but this is one of those programs I have a lot of faith in. I
think this is going to go pretty well. Anyway, it's just a thought. Y'all have a
Have a good day.