Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about something
that is a little bit out there.
But a number of questions have come in about it.
And I have a feeling as this information and these quotes
become a little bit more widespread,
it will become more and more of a topic of conversation.
So it's worth looking at prior to being everywhere.
The quote in question is, an artificial interstellar object
could potentially be a parent craft that
releases many small probes during its close passage
to Earth, an operational construct not too
dissimilar from NASA missions.
with proper design, these tiny probes would reach the Earth or other solar system planets
for exploration as the parent craft passes by within a fraction of the Earth-Sun separation."
Okay, yeah, so that's talking about a literal alien mothership.
It is from Sean Kirkpatrick, who is the director of the Pentagon's All Domain Anomaly Resolution
Office.
It's the Pentagon's X-Files.
Their job is to look into weird stuff and kind of figure out what it is.
So that's the quote that is probably going to show up in a lot of places.
It is worth noting that this is a part of a research paper, a draft, hasn't been peer
reviewed, all of the normal stuff.
The general idea behind it is that this could be happening, not that it is.
Basically, this office, they have found a number of things
that could be explained by optical sensors being off
normal run-of-the-mill errors that could explain
what was witnessed.
If everything else is accounted for, the things that
were seen, the phenomena, were operating outside of the laws
physics. So given that their job in this office is to come up with plausible
scenarios for what this stuff is, this is what they came up with. And the reason
our survey scopes don't see the probes as they head to Earth is that they don't
reflect enough light. The survey scopes just aren't good enough to pick them up.
So, while this is definitely interesting, it's worth noting that they are not saying
this is something that is occurring.
They're trying to explain things that are unexplainable.
This office is relatively new.
The whole idea behind it was to get rid of the stigma of looking into stuff like this.
There are going to be a lot of things coming out of this office in the future that are
just wild because their job is to try to explain the unexplainable.
So they're going to come up with pretty wild theories from time to time.
It's worth remembering that this office is not going to make a definitive statement on
something like that without a giant press conference.
Research papers, positions, theories, little notes scribbled into margins, stuff like that
should not be taken as evidence that the Pentagon believes this is occurring.
This is them looking into incredibly strange things, and contrary to the way it's been
done since World War II, they're doing it pretty transparently.
So the public is going to see a lot of just wild and weird statements, and everybody should
probably be ready for that.
And again, it's not out of the realm of possibility that what they're suggesting here is true.
It's an explanation for some of the things that have been observed.
But it is not the only explanation and they are not saying this definitely happened.
Those are the key things to keep in mind.
So yeah, be ready for a lot of just weird stuff to come out of that office over the
next probably three to five years.
There's going to be a whole lot of it.
Anyway, it's just a thought.
Have a good day.