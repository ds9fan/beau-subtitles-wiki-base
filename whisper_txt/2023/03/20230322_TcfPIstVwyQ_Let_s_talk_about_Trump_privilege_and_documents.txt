Well, howdy there internet people, it's Beau again.
So today we are going to talk a little bit about the Trump document case.
While all eyes are on New York, remember there are other proceedings moving forward.
This one involves the special counsel's office and what's happening there.
According to reporting from ABC and what sources reportedly told them, the special counsel's
office sought to pierce attorney-client privilege with at least one of Trump's lawyers and
to compel that lawyer, Corcoran, to testify before the grand jury about things that would
normally be covered by attorney-client privilege.
The judge agreed.
The judge agreed and found prima facie evidence of this criminal scheme that would allow DOJ
to compel that testimony.
It's worth noting, prima facie evidence is a very low standard.
Literally, on its face, this is what it looks like.
It's not a very high bar, but in this case it's enough to get this testimony.
It seems as though what was presented to the judge was the belief that Trump deliberately
misled his own attorneys, who then were put in a position of misleading the feds through
no real fault of their own, is the way it looks at this point.
That seems like it would be something pretty difficult for the feds to prove.
How do you demonstrate that the former president deliberately misled the attorneys?
It's not actually as hard as you might imagine, not in this case, not when there's a clear
timeline of when they were asked for the documents back, when a meeting between the attorneys
and Trump took place, when the feds were informed that there was a diligent search and that
everything had been returned and all of this.
That combined with other pieces of evidence, say hypothetically the security footage from
the club that we know the feds got, that could establish that Trump was aware of the documents
after that point, but continued to tell his attorneys something else.
Or it could show that Trump moved them beforehand.
There's a whole lot of scenarios involving basically a calendar of events and the footage.
And then there's a whole bunch of other ways they could get to that information.
While it sounds hard to prove or to demonstrate, it's really not.
So according to the reporting, the lawyer will end up going back before the grand jury
and have to talk about six separate lines of inquiry that were previously covered by
attorney-client privilege and didn't have to talk about it.
That really does indicate a genuine, a very genuine effort on the part of DOJ to get to
this.
It's also worth noting that in the conversations, it does appear that that phrase retention
comes up.
As we have talked about from the beginning, accidentally ending up with a bunch of documents,
that's one thing.
willfully retaining them, that's something else.
If the feds, and if this reporting is accurate, it certainly seems to be the case.
If the feds believe that he deliberately misled his own attorneys about the existence of those
documents, that goes a long way to showing willful retention.
So while everybody is focused on New York, and awaiting news from there, and then eyes
are likely to turn to Georgia, just remember there are other proceedings happening, and
in this case, definitely seems like big news that might get overshadowed.
Anyway, it's just a thought, y'all have a good day.