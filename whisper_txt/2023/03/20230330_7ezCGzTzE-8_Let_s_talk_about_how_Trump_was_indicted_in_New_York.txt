Well, howdy there, internet people.
Let's bow again.
Former president Donald J.
Trump, leading contender for the Republican nomination for president in 2024, has been
indicted, has been indicted, um, reporting at this point in time, information is, uh,
sparse, but the reporting suggests he was indicted in New York and, uh,
While the exact charges are not known, early information suggests one of them is a felony.
The charges are reported to be related to the whole money that changed hands and the
falsification of business records dealing with it, as far as the hush money payments.
In related news, the grand jury up there is reportedly looking into a second payment.
We talked about it the other day and talked about how there were a number of options on
why things were moving the way they were, moving as slowly as they were, and one of
them was that they had uncovered another potential criminal violation and that they were looking
into that.
That does appear, according to reporting, to be true at this point.
In more related news, Allen Weisselberg, the former CFO of Trump Organization, has changed
lawyers.
Up until very recently, Weisselberg was being represented by Trump lawyers, lawyers from
Trump World.
That is no longer the case.
Generally speaking, while there's no evidence of this at this point, but generally speaking,
when this occurs and the person changes lawyers and it's moving away from a potential defendant,
it's an indication that that person may have decided to cooperate.
So those are the developments as we know them right now.
There is undoubtedly going to be a whole lot more information coming out, but I figured
I would put this out as soon as possible.
Get ready for a bumpy week because there will be a lot of speculation.
There will be a lot of fake and real outrage.
will be a lot of people cheering. But most importantly, there's going to be a
lot of speculation. Temperate. Go off of what is reported as fact and nothing
more. Don't let your emotions get away from you on here. Keep in mind, there are
multiple other criminal investigations going on and this might just be the
first of many future developments when it comes to this. So everybody just go
off of what is known, not what is speculated. So anyway, it's just a
thought, y'all have a good day.