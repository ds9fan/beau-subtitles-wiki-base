Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Putin's heart, I guess,
because that is a pretty big conversation right now.
And there's a lot of coverage on it from various outlets,
and it's all over social media.
So we're going to kind of run through it
because questions have started coming in.
And we'll just kind of go through the report,
And then we'll talk about it.
So what does the report say?
The report says that Putin suffered a cardiac event
and had to be resuscitated.
That basically his guards found him and brought in a doctor
and they brought him back and all of that stuff.
Here's the thing about this.
Is it possible?
Sure, it is.
It's possible at any time, but as far as the report itself
the coverage that it has gained, it is important to note this is from one
source. It's from a telegram channel that is kind of believed to have some
connections to the Kremlin, but it's anonymous. And the report came from there
and was based off of two anonymous guards. So it's two guards telling an
anonymous channel that this occurred. I have a lot of questions about that
because it seems unlikely that it's something the guards would discuss. Not
not if they brought Putin back around because there were only so many guards.
I mean that that that sounds like a really quick way to have a fall of your
own disclosing that kind of information especially with the lengths he goes to
to keep his health issues secret. So with the lack of sourcing why did this catch
on so quickly because Putin is not actually in great health. That much is
known. There are a lot of rumors about it and it is worth noting there are a lot
of rumors about it on the channel, on the telegram channel that released
this information. But what's confirmed is a different story. So is this possible?
sure sure medical issues can happen at any time but I wouldn't go mapping out
you know who's who's next to to take charge I wouldn't start playing Swan
Lake just yet. It is interesting though that you're starting to see kind of kind
to see the veil that's been up, get pierced. No, people don't look at him as invincible
anymore. And that's a really good sign. Anyway, it's just a thought. Y'all have a
Have a good day.