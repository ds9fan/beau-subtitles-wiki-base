Well, howdy there, internet people.
Let's bowl again.
So today we are going to talk about Biden's trip because he's reportedly
going on one and it's a big one.
Um, and we're going to talk about the trip, where he's going and
what is likely to be discussed before we get into that though, I want to, I
want to kind of acknowledge something that I think is important. This trip, a
trip to the Middle East at this point in time, this is politically risky, big. If
it goes bad, he's going to take the blame.
Politically, it is very risky. Morally, it's an imperative because if it goes
well, it could save a whole lot of lives. I think it's important to acknowledge
that this is not something he has to do, politically speaking. Okay, so we'll talk
about Israel first. I don't actually know the order of the trip, but he will be
going to Israel, while he is there, there will be three major points that are being
discussed, three things that are supposed to get out and on the table.
The first is the United States is behind Israel.
Biden has to get that out there.
Shouldn't be too hard.
Him and Netanyahu have known each other forever, so that should be an easy part of this.
While he's doing that and saying the United States is totally behind Israel, he's supposed
to convince them to do everything they can to limit civilian loss.
That's the second part.
The third part is coming up to some kind of agreement that allows NGOs, humanitarian organizations,
multinational teams maybe into Gaza to help.
Maybe during this conversation they pitch them on the idea of handling this in a much
more subtle way as far as realigning the organization there.
So the next stops, as if that's not enough, will be to go to Jordan and Egypt.
My guess is those are both going to be the same conversation, more or less.
Stay out of it militarily.
Can you help?
Those will be the conversations.
And then after that, he'll be meeting with the president of the Palestinian Authority.
Again, I'm saying that as if that's the chronological order.
I don't know the order in which the meetings are taking place.
But those will be the conversations.
As far as the conversation with the Palestinian Authority, it's either going to be immediately
after Israel or it's going to be absolutely last.
I know foreign policy people right now, you're like, why are they talking to them?
Foreign policy is about power, right?
So foreign policy people are kind of like, what is this?
To have a series of meetings about the fate of the Palestinian people and not meet the
Palestinian Authority president, that is not a good look.
why. It's, the Palestinian Authority president has a whole lot of limitations
on what, what they can actually do diplomatically here unless there is some
kind of major breakthrough with Israel. So, but that's the, that's the trip. If it
goes well, people will probably forget about it. If it goes poorly, it's going to hurt
him politically. If it goes well, a whole bunch of lives will be saved, though. That's
That's the math.
Anyway, it's just a thought.
Have a good day.