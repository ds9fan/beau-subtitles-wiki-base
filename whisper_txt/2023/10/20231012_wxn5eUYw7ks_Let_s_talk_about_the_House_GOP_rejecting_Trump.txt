Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Trump and how the house GOP just told America
what they really think about Trump and his leadership capabilities.
We talked about it when he made the endorsement.
It was a very risky proposition for Trump because if he wins, well, okay, so we all
know that House Republicans are kind of loyal to Trump, but if he loses shows that they're not.
Trump endorsed Jordan for Speaker of the House.
Jordan did not get it.
Behind closed doors with only Republicans voting, Jordan didn't get it.
Republicans in the US House of Representatives just told America that Trump's decision-making
capabilities, his ability to pick a leader, is not one to follow.
That's what just happened.
They chose Scalise.
It's worth noting we still don't know whether or not Scalise has the 217 votes necessary.
But Trump is the bigger loser there.
Jordan sure, Jordan didn't get the nomination, but Jordan is still Jordan in the House of
Representatives.
Trump threw his weight behind Jordan and it did not matter.
anything, it very well may have given the nomination to Scalise. The Republican
voters, the rank-and-file, they definitely need to acknowledge what just happened.
They need to understand what just happened. The House GOP rejected Trump's
leadership. They're saying he's not fit to make those decisions. They didn't follow
them. For them, Trump was just somebody they could use to rile up their base,
ride his coattails. For Trump, his dream of heading back to the White House is
definitely turning into a nightmare because as more and more adds up, as
there are more obstructions, as there are more revelations about the number of
people who actually believed in him. Things aren't looking so good. This was
just Republicans voting with Trump saying pick Jordan and Jordan already
having a significant base. Trump's endorsement was not enough to get
Jordan over the line just among Republicans. This is a prime example of
why judging some of these political capabilities and how much people truly
support them based off of social media engagement is a really bad idea because
behind closed doors, just like a whole lot of people are going to be in the voting booth.
Well, it's a little different than on Facebook.
This was a really bad sign for Trump and Trumpism.
I hope that the rank-and-file Republicans get the message that the House GOP was...
I mean, I don't see how they could have unintentionally done this.
They're sending a message.
They're rejecting Trump.
Anyway, it's just a thought.
Y'all have a good day.