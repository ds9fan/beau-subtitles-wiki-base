Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about some news
that for a whole lot of people,
probably isn't gonna matter much.
But for some, it's going to be big, big news,
very surprising.
And it stems from some questions that were asked
and the answer.
The questions weren't asked of me.
this wasn't something that came into the channel. Some cardinals, they sent some
questions to the Pope, and the response is definitely going to alter the way the
Catholic Church deals with certain communities. Okay, so if you don't know,
Catholic Church marriage is a man and a woman, okay, and there's no getting
around that, basically. However, the current pope in the past had supported
the idea of legal benefits for spouses, civil unions, basically. I don't think
that term was ever used. It might have been, but that was the position. Some
Cardinals wanted some clarification on this, and the response was basically,
priests can't become judges who only deny, reject, and exclude, who only deny, reject, and exclude.
That's a quote. It goes on to say, for this reason, pastoral prudence must adequately discern whether
there are forms of benediction requested by one or more persons that do not transmit a mistaken
conception of marriage." To translate that and make it consumable by people who aren't Catholic,
they can be blessed is what it boils down to. It doesn't allow for same-sex
Catholic marriages. That's not what this is. But same-sex couples can be blessed. I
I think for a lot of people, this is one of those things that's like, well, I mean, who
cares?
You know, not really that big of a deal.
For a lot of people, particularly those who are from countries, cultures, communities
that are heavily Catholic, this is huge.
This is huge.
it will alter the way they engage in their social life in a whole bunch of
different ways. I know there are going to be some people who say, well, this isn't
really enough and all of that, and I get it. You're talking about the Catholic
Church. When we, you know, talk about that phrase on a long enough timeline, we
win, all of that stuff. When it comes to stuff like this, the Catholic Church
they're gonna be the last people across the finish line. I know that for a whole
lot of people it's not gonna seem like a lot. This is going to be life-altering
for some and it's a step in a direction that I mean I didn't see happening I'll
be honest, certainly there's going to be pushback on this, and I would imagine
that it's going to start at kind of the local level within the church, and
eventually there will be questions raised about it, but this is one of the
most conservative, traditional organizations in the world, with a whole
a lot of influence. It's easier to other and marginalize people if your God says
that it's okay, if they can't be blessed, if they can't be part of that community
because while in the United States that community is shrinking in a lot of other
countries and a lot of places, that is the social fabric still.
This will go a long way to integrating the LGBTQ community into that social life, which
over time there will be more exposure.
People are afraid and therefore easily angered at and driven to marginalize things they don't
understand.
People fear what they don't know.
This is going to be a big step in exposing a whole lot of people to a group that they
only viewed as people so bad they couldn't even be blessed.
So if you don't see the significance, ask any Catholic friends you have.
I would imagine that it will be less than a week before the Catholic Church went woke
or Pope Francis the Woke or something like that hit social media because as much as most
of the commentators like to pretend they're plugged into this community, they're really
not so it might take some time to get there but that's definitely on the
horizon.
Anyway, it's just a thought.
Have a good day.