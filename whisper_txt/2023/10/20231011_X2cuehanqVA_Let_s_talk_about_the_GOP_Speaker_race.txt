Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the race for the speakership in the
U S house of representatives and how all of that is working out because
it's going great.
Um, so one of the more interesting developments was a, uh, a number of
Republicans that would like to have McCarthy back. McCarthy back. So right now
you have Jordan and Scalise. Those are the two top contenders for it. Jordan
has shored up support with 47 votes. Scalise has 31 at time of filming. These
numbers will probably rise a bit by the time y'all watch this video. McCarthy has
60. It's worth noting that McCarthy was like, yeah, no, I don't want to go. You
can't make me take the speakership again. He seems to have absolutely no interest
in it. However, according to the sources, he has the most votes. But what is
incredibly interesting is we are talking about a range of support between 31 and
60, 31 and 47 between people who actually want the position. If anybody
doesn't know, they need 217. Neither one of these candidates are inspiring a lot
of enthusiasm. There's not a lot of people coming out saying, yes, that's the
person I want. One of the major dividing points is what to do about the 8. There
There are a large number of Republicans who would like, quote, accountability for the
eight people who triggered this giant mess.
And it seems unlikely that Jordan would provide it.
Scalise hasn't said much, but I don't know.
My guess is they'll pick one person to be accountable.
In the background, there is a movement, and I believe they have won so far, to get a few
Republicans to flip to vote for Jeffries and get Jeffries to be Speaker of the
House. They don't have to flip many to do that and there is an active effort
underway to make it happen. I think they need five and they have one, so they need
four more, which makes it a lot closer than any of the Republican candidates at
this point. But we'll have to wait and see how it plays out. The Republican
Party is afraid of another giant show, so it looks like they're gonna try to do
most of this behind closed doors. But we'll be told by our betters who who's
in control once they decide for us. Anyway, it's just a thought. Y'all have a
Good day.