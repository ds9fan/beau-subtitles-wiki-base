Well, howdy there internet people, it's Beau again.
So today we are going to talk about Arizona
and some numbers that came in for a race out there
that I feel like is going to be incredibly important.
And it's also being covered in a unique way.
Okay, so in Arizona,
when it comes to the senatorial race out there,
It looks, at this point, it looks like it is going to be a three-way race with Lake
as the Republican, Sinema as an Independent, and Gallego as the Democrat.
So the polling on this, in a three-way race, Sinema gets 15%, Lake gets 36%, Gallego gets
41. We talked about it earlier. Lake and cinema are so polarizing and they are
going to be such characters in this. Gallego just has to be normal. That's it.
Just just be normal. Don't do anything fun. Just just run a normal campaign. The
numbers are already showing that. The numbers are already showing the the
intuition, the gut feeling that I had, the polling is lining up with it, and in
addition there's 8% that are unsure. Okay, so 41, 36, 15 in a three-way race. When it
is Lake versus Gallego, it is being covered as he only has a 48 to 43 lead.
Gallego still winning 48, Lake getting 43, 9% unsure. It's that only part that I
find really humorous. I know that 41 to 36 sounds like more because 40 and 30
but do the math. It's five points either way. The lead
doesn't actually shrink, but it's interesting to see it covered in that
way. What I find even more interesting is that what this suggests is that
Sinema's base is half Republican. That's an interesting note, or at
least half conservative and I shouldn't say Republican because the the numbers
just indicate that they they split pretty evenly. Okay so that's where we're
at. This undoubtedly is going to be a consequential race. So at this
point the Democratic Party has a pretty good lead. Odds are that Lake and Sinema
are going to go after each other.
Let them just be normal.
Don't do anything weird.
That's like, if you know anybody on that campaign,
just tell them, don't do anything weird.
I have a feeling that Arizona is just fed up with it
and they would like a normal, non-controversial,
a non, uh, somebody who doesn't have a social media controversy every other week.
I think if Gallego can show that that's, that's him, that's all it's going to take.
Anyway, it's just a thought, y'all have a good day.