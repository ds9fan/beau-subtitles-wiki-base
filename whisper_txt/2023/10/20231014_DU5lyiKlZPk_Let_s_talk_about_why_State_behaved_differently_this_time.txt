Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk about State Department
and where State Department has been, what happened,
why aren't they doing what they normally do?
And we're gonna do this because of a question,
because somebody sent one in saying,
hey, normally, State Department's out there
calling for diplomacy, calling for everybody to stop
and to start talking, why aren't they doing it this time?
They started initially.
That was what they were going to do.
In fact, if I'm not mistaken, the very first tweet from the Secretary of State was one
that was echoing Turkey's call for a ceasefire.
And then news broke about the captives.
That tweet was deleted, and it was replaced with, Israel has the rights to defend itself,
rescue any hostages, and protect its citizens.
The Office of Palestinian Affairs,
they actually had a tweet up, and it was their position.
The route they were going was to say, hey, no retaliatory strikes.
Everybody chill out, come to the table, talk.
News of the captives broke.
That tweet was deleted.
And it was replaced with one that unequivocally condemned the attack.
There was a formula when it comes to the cycle that's over there.
And yeah, under normal circumstances, the Palestinian side could absolutely count on
the US to try to broker a ceasefire or to call for restraint.
That's the normal formula, yeah, sure, however, once captives were taken from a whole bunch
of different countries, many of them US allies, that wasn't going to happen.
talked about this early on. A non-state actor taking a U.S. citizen, that changes
the formula for the U.S., for State Department, because then it's no longer
about trying to calm everything down, trying to pursue diplomacy through that
route. It's about getting people back and nothing else. That's what happened.
There's a lot of people who have very extreme opinions when it comes to stuff like this.
Just understand those opinions don't exist in a vacuum. And what sounds good ideologically
When it hits that table and the news breaks, it changes opinions.
Yes, if this had been limited to military targets, let's start there, the US absolutely
would have been calling for restraint.
What happened and the fact that there were Americans lost and Americans captive?
The U.S. is not going to call for restraint there.
It's one of those things where I would imagine the U.S. would kind of look the other way
on just about anything right now.
The United States has never been super effective at pursuing Palestinian interests, but oftentimes
they did call for restraint, and sometimes they actually got it.
That's what happened.
I know a lot of people aren't going to like that answer, but this question came in repeatedly.
State Department at. State Department started to do what they always do and
then they found out they took US people captive and it changed the
formula. So that's the question, that's the answer. They had a false start on it.
I mean they started off doing what they always do, but as more information came
out and news of the captives came out, it changed everything. Anyway, it's just a
thought. You all have a good day.