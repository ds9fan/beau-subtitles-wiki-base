Well, howdy there, Internet people, it's Bo again.
So today we are going to talk about
what's gonna be happening in the
House of Representatives this week.
Because there's gonna be a lot of movement
and a whole lot of talking points
are already starting to come out
and they're feeding these talking points
to their base via social media and via interviews.
And we just need to add some context to them.
Because the talking points they're giving, their base,
they're the reason that this is happening to begin with.
They have used a certain type of rhetoric,
and it has led to the Republican Party
being completely dysfunctional.
So we're going to kind of run through it.
Now, if you don't know what's going on,
McCarthy, the Republican Speaker of the House,
he got a deal to get 45 more days,
keep the lights on, the government not disrupt the economic stability of millions of Americans.
And the far-right Republicans, well, they didn't like that.
That was bad.
The shutdown, I guess, should have occurred.
Why?
Could be any number of reasons.
But at the end of the day, it doesn't matter.
They didn't like that.
So the plan is to motion to vacate.
vacate the chair. And that would put McCarthy in a position to where he might
be ousted. And those far-right Republicans, they're already out there
saying, you know, if McCarthy, if he gets to stay as speaker, it's only because the
Democrats allowed him to stay. I mean, that's true. That is true. But let's
be real clear about something. If he is ousted, it's only because the Democrats
allowed him to be ousted. Those far-right Republicans who are super mad
because McCarthy worked out something that was bipartisan, in order to oust
McCarthy they have to do it in a bipartisan fashion. They need the
Democratic votes. They can't do it on their own, and if they tell you otherwise
they're lying. So how did the Republican Party get in this position? The answer
is simple. The rhetoric they have used for years is finally coming home. The
polarization that they have caused in this country where Republicans see
Democrats as enemies, as people who are out to get them, rather than other
Americans, that's why this is happening. When a political movement starts
othering people, it doesn't stop. It doesn't end. It just continues. Once the
Republican Party successfully othered the Democratic Party and made the
Republican base think of them as enemies rather than as their neighbors, well
guess what happens? Well, you have to have an enemy. So you look within. So the
far-right Republicans, they started casting any normal conservative as
their opposition, as the enemy, and it divided the Republican Party. A divide
that you see right now. You want to know who is to blame for this? It's the people
who let Trump get that kind of hold on the Republican Party because that's when it really
took root.
It existed before, don't get me wrong.
Talk radio throughout the early 2000s, the Tea Party, all of this stuff, it helped move
in that direction, but it was Trump that capitalized on it.
Trump destroyed the Republican Party.
Those who mimic him, those who use his rhetoric, those who continue to other, other Republicans
as well as more than half the population, they're just continuing that tradition.
Make no mistake about it, you're next.
Even if you're a Republican, you're not going to be extreme enough.
It doesn't stop, ever.
This is why this kind of rhetoric is so dangerous.
It's why you had people warning about this in 2016 and before, saying this is where it
was going to lead.
And here you are.
The reason the Republican Party is the way it is, is because Republicans were convinced
to see Democrats as their mortal enemy rather than their neighbors.
It's that simple.
That's where it started.
Why did those in power do that?
Because they wanted to rule you by fear rather than represent you.
They wanted to make you afraid.
Those people, they're not like you, they're coming to get you.
They wanted you scared, so you would do whatever they said.
Basic civics would explain this, would explain what's happening right now.
That's why they love the uneducated.
The Republican Party pushed this.
They scared you so they didn't have to represent you, which is their job.
It's in their title.
They're your representative.
They're supposed to be representing your interests, not making somebody else's life worse.
They're supposed to be making your life better.
Are they doing that?
I'm willing to bet that most people are going to say no.
In fact, the most Republican of the Republicans, you know, the purest Republicans, those who
are above reproach, their answer, what they want is to shut down the government and jeopardize
the economic stability of millions of Americans.
Why?
Because they have to keep you scared.
If you keep falling for it, they will never represent you.
They will never make your life better because they don't need to.
They just have to keep you scared of your neighbor.
That's it.
That's their game plan.
And so far it's worked.
Jeffries, who leads the Democratic Party in the House if you don't know, in a position
nobody has ever been in. Literally, throughout all of American history, nobody in his position
has ever had this situation. Because there's never been a party as dysfunctional, as broken
as the current Republican party is. He has a lot of leverage, he has a lot of power.
And realistically, this shouldn't be an issue, but in the quest for social media clicks
and engagement, in the quest to be more extreme than the next Republican, it has created
a situation where nothing that they're pushing through will go anywhere.
So it doesn't even get voted through.
in the Senate, they they are a little bit more insulated from this because they
have to win statewide. It's a different race for them. It's why they're more
deliberate. It's why they're less extreme. It's why they're more functional.
If a candidate is telling you who to be afraid of, and that's their entire pitch, understand
they're never going to represent you, they're never going to make your life better because
they never planned on it.
They told you what they were when they campaigned.
Anyway, it's just a thought.
Y'all have a good day.