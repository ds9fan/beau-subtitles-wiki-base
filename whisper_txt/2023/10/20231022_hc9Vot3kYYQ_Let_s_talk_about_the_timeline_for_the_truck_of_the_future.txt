Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Elon Musk and the
Cybertruck and the truck of the future being literally a
truck sometime in the future.
The company missed the third quarter deadline, self-imposed,
they wanted to begin delivery. And it's not looking good when it comes to it being a speedy process to
get to scaling it up to production. Musk himself is reported to have said, we dug our own grave
with the Cybertruck. This is a project that has kind of fallen out of media's view because of well
the whole social media thing going on. But this this vehicle has been well it's had issues ever
since the glass broke that day. Currently they are hoping to get production up to 250,000 units per
year, sometime in 2025. That's what they're looking for. Which is, I mean, that's a ways off.
That number is even more disheartening if you are on the wait list for the vehicle, which,
which, according to reporting, has about 2 million people on it.
So it might be a while before the waitlist is fulfilled, I guess.
This news did not go over well with investors, obviously, and I would imagine it's giving
Musk himself quite a headache.
The issues that have arisen have, I think a lot of it has to do with the body, how the
body is being put together, but we'll have to wait and see how this progresses.
a 2025 target date is I would imagine that that's probably also a little bit
flexible as far as whether or not it's you know going to start right in January
or at some other point in time but right now the Cybertruck is not it's not
moving along very well. Doesn't appear to be. But it is admittedly. It's a large
task. The other issue for Musk is that while there is a waitlist of a couple
million people, it really hasn't been determined if there's a market for this
yet. This isn't something that's going to likely be global when it comes to
people who want it. When you think about China or Europe, this is probably not
something that's really going to go over well there. This is an American thing, so
we'll have to wait and see how this plays out, but there is a growing concern that
this may end up like another stainless steel vehicle back in the past, back to
the future.
Anyway, it's just a thought.
Y'all have a good day.