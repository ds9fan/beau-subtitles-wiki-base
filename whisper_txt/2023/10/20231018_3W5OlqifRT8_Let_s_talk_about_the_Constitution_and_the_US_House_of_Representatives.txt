Well, howdy there, Internet people, it's Beau again.
So today we're going to talk about
the United States House of Representatives
and the United States Constitution
and some talking points that are coming out
and how all of them interplay
as things move along to choose a Speaker of the House.
Well, I guess as things don't move along
to choose a Speaker of the House.
So, the first talking point that we're going to go over is the repeated claim that the
Democratic Party is blocking the Republicans from choosing a speaker.
The reason the Republicans are saying this is because they believe their base is too
ignorant of the Constitution to understand how it works.
There is no vote to block.
That's not an option.
The Democratic Party is voting for their candidate.
The Republican Party is unable to put up a candidate that Republicans can unite behind.
That's how that works.
That's what's occurring.
It's worth noting that the Democratic candidate is getting more votes.
That's not the Democratic Party voting no on a chosen Republican speaker.
That's not how it works.
They made that up.
They're lying to you.
The other one that has come out, and this was broadcast on national TV, is that the
Democratic Party should help the Republicans choose their speaker.
Help the Republicans choose their speaker.
Weird.
Because I'm pretty sure in the Constitution, it doesn't say that the speaker is a member
of the majority party. It says the US House of Representatives, the whole House
of Representatives, shall choose a speaker. The Republican Party is so
entitled that they feel the Democratic Party has to come to their rescue to
help them choose a speaker. That's not really a message that I would want to
send out to people this party is so broken right now it can't even govern
the house much less govern the country. The US Constitution is pretty clear
about this. The House of Representatives chooses the speaker not the majority
party. Yeah, generally speaking, the speaker is from the majority
party. Why? Because normally the majority party isn't broke. They
are interested in actually fulfilling the purpose of
government, not getting clicks on Twitter. That's what's going
on. The talking points that are going out from Republicans trying
to shift blame for this failure.
The only reason they can make them is because they believe their base is too ignorant to
understand the process, too ignorant to understand the Constitution.
There's no other way you could make those.
And as far as Democrats crossing over to help Republicans choose a speaker, I'd remind
everybody that it is Jeffries that's been getting the most votes.
So maybe Republicans should cross over and end this.
Anyway, it's just a thought.
y'all have a good day.