Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about some news
concerning George Santos.
And it is a bad sign for his future political aspirations.
George Santos, if you don't remember,
is the embattled representative.
There have been numerous calls for him to resign.
So far, those calls have been resisted,
even though he is currently facing,
I want to say a 13-count federal indictment.
His former treasurer, Nancy Marks,
has entered into a plea agreement, pleaded guilty
in federal court.
There is a sentencing recommendation of,
I want to say, somewhere between maybe 3 and 1
half to 4 years, something like that.
And basically, as part of the agreement,
it certainly appears that she is implicating Santos.
Her attorney said something really interesting.
When asked about it, he kind of indicated
that he believed that his client had been, quote,
mentally seduced by Santos, and indicated
that he believed there were some mental games played.
We'll have to wait and see how all of that plays out.
But generally things are focusing on a loan that was reported but wasn't real.
It was a fake loan in the amount of half a million dollars.
My understanding, and this part is kind of fuzzy at time of filming, is that the idea
was to show Republicans that, look, he can fundraise, he's got this cash.
There were also fake donors, even though apparently the donors' names were real, they weren't
aware of the situation.
So there's that as well.
This definitely complicates the situation for Santos as it tries to retain his seat,
I guess, and it makes it much harder to deal with the indictment.
This is one of those stories that, again, in a normal political climate, this would
be front-page news every day, but we live in a, you know, post-Trump era or a Trump
era, and yeah, I mean, normally this doesn't even get covered, but that's
what's going on with that. It seems unlikely that Santos is just going to be
able to ride this out and hope it goes away. There's definitely more to come on
this front and eventually the Republican Party is going to have to is going to
have to do something in regards to Santos. The longer this drags on with no
response from the GOP, it certainly appears that they are condoning that
behavior. Anyway, it's just a thought. Y'all have a good day.