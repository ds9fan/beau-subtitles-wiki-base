Well howdy there internet people, it's Beau again. So today we're going to talk about the
hunt for blue lightning. Okay so an F-35 lightning had what the Navy is calling a landing mishap
on the USS Coral Vinson. Landing mishap is the apparently the naval terminology for crashing and
then falling off the deck of the carrier into the water and sinking. The F-35 is the cutting edge of
aviation technology. This happened in the South China Sea. So the United States is currently
rushing to try to get this aircraft back off the ocean floor because if it falls into Chinese hands
they will be able to reverse engineer it, gain a pretty significant edge. They will learn a lot
that they will be able to apply on their own for their own aircraft and they will also have an
inside look at its capabilities and probably figure out some kind of electronic warfare way
to counter a lot of it. This aircraft plugs in to a lot of other systems. It's kind of networked.
Now for their part, the Chinese say they have no interest in that aircraft. They have said this
publicly and not to put too fine a point on it, they are lying. They absolutely do. I would be
completely shocked if they didn't have teams out trying to find it right now. Now to add another
element to this story, apparently the Navy's closest salvage ship is so far away that by the
time it gets there, I want to say eight days from now, the black box that is giving a signal of
where the aircraft is, its battery will have run dry. Well I mean not dry, it's underwater, but
you know what I mean. So this is one more thing that we can expect to see a lot of in near-peer
contests. This would happen anyway. World powers would definitely be interested in this aircraft
if this had happened at any point in time, but given the fact that it happened so close to China
at a time of relatively increased tensions and at the start of the near-peer contest, they're
definitely going to want to take a look at it regardless of their public statements. And the
United States seems to be pretty far behind in being able to get to it. This may prove to be
an incredibly costly mistake for the US Navy. Anyway, it's just a thought. Y'all have a good day.