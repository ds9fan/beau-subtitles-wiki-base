Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about those text messages from Sean Hannity.
If you don't know, the committee, looking into what happened,
they released a letter that included excerpts from Hannity's text messages.
And those texts, they can be spun with just the slightest bit of effort into,
well, what was Sean Hannity worried about?
He was obviously in on it.
How did he know all of this?
Why is he so plugged in?
There's a whole bunch of questioning that could paint a narrative that way.
The reality is he's so plugged in because he's Sean Hannity.
But the other side to that is everything that was released,
all of those excerpts, with a little bit of effort,
they could be spun the other way, too.
Sean Hannity could walk in and say, hey, I was trying to be the adult in the room.
Look, I told him I was worried.
I didn't think this was a good idea.
They're going to lose counsel.
Trump should never talk about the election again.
He needs to go down to Florida.
I was trying to be the good guy.
And he could frame it that way with just the slightest bit of effort.
I don't think that's an accident.
I don't think that excerpts were released rather than everything they have.
It seems to me the way I'm reading this, the committee is offering Sean Hannity
a golden opportunity.
They're looking at him and saying, hey, why don't you come in from the cold,
get on the winning team here?
Why don't you tell us what you know?
And in the process, you can paint yourself however you'd like and save your career.
That's how it seems.
It looks like they're trying to give him an opportunity
because they know he's plugged in, because he's Sean Hannity.
Now, there are probably people wondering, well, why would he do that?
Why would he do that when everybody else is staying silent?
Everybody else isn't staying silent.
You have a whole bunch of high profile people
who are trying to obstruct this process.
But when you talk to the committee, when you look at what they say,
they say that like nine out of 10 people they've talked to have cooperated.
Who are these other people?
They're people that aren't in the public eye.
And this is something that Sean Hannity will understand
probably better than the rest of them,
because Hannity has been in the infotainment industry a long time.
In the cartoon Frozen, there's that scene where Elsa's out on the balcony
right before her coronation.
And I don't remember exactly what happened,
but her magical powers surface.
She freezes something right as one of the servants from the castle
opens the door and they see it.
But they don't say anything.
Yeah, we don't live in castles anymore.
And the assistants, they don't behave like that anymore.
They talk.
The people that the committee has talked to,
most of them are people who are in the public eye.
Most of them are going to be people who aren't in the public eye,
who we've never heard of.
They might be personal assistants, legal advisors,
security professionals, secretaries, chauffeurs,
people like that, communications people, PR people,
people who were around conversations and overheard stuff,
people who were asked to do stuff,
minor things in furtherance of whatever was going on.
And they're talking.
Hannity, more so than the others, probably understands
that that's kind of all they need.
Him going down with the ship isn't really going to do anybody any good.
So he's being presented with an opportunity to save his skin
and paint that narrative however he would like.
If he is half as smart as his PR people have made him out to be,
he'll probably take this.
I don't see any benefit to Sean Hannity for not answering this.
I don't see any way that that helps him.
Now, at the same time, you have to wonder
whether or not a cooperation from Hannity
might change the other people's tunes.
Or even the worry that Hannity is going to talk
might spur other people to remember stuff.
Because Hannity is plugged in.
He knows a lot of people.
A lot of people talk to him.
He probably has details, information,
that other people may not really have a clear picture on what he knows.
If he's smart, he'll probably go and talk to the committee.
If he wants to roll the dice,
he'll probably talk about some kind of journalistic freedom or something like that.
But it all depends on how deeply he's really involved
because we can't really tell from the excerpts.
When he says, I'm worried about what's going to happen over the next 48 hours,
the day before it happened,
he could mean that Trump have severe political ramifications from Pence,
not following Trump's guidance.
It could be something that simple.
Or it could mean he was fully aware of what was about to happen
and he didn't want it to.
Or he was worried about the fallout to those people who directed it
if it didn't go well.
Without context, we don't know.
And I don't believe that's an accident.
You know, what the American public doesn't know,
well, that's what makes us the American public.
And it seems like at this moment,
they plan on keeping a lot of context to themselves
until they can get people to start telling the story from the inside,
the high profile people.
But I could be wrong about that.
That's just the way I'm reading it.
Anyway, it's just a thought.
Y'all have a good day.