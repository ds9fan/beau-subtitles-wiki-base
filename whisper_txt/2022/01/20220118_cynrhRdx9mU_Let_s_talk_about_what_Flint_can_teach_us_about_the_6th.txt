Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about
two seemingly unrelated events
involving government officials
and how there's some parallels
and how there's a lesson that people in Congress
really need to pay attention to
because I can totally see the same thing happening again.
It starts with looking at the new reporting
that is coming out about what happened in Flint, Michigan.
Now, I'm not going to go through all of the details of it, but I will put links down below
because I find this interesting.
The reporting suggests that there was an initial team of prosecutors who were building a RICO
case, a pretty expansive RICO case that was going to implicate a whole lot of government
officials.
But see, then this weird thing happened.
There was an election and there was a new attorney general.
That initial team of prosecutors, well, they got taken apart and a new team put into place.
That new team did eventually bring some charges, but nowhere near the scope and the size of
what the initial team was looking at.
They didn't use RICO.
They didn't build that expansive case, bringing everybody into it that may have been involved.
I think people in Congress need to understand that that's a very real risk.
When they're looking into the events of the 6th, they need to understand that they're
on a clock, and I understand that these investigations take time, and it does seem to be moving along,
And I am also certain that there are people advising those in Congress saying that, hey,
you know what?
It'd be really good if this information came out like right before the election.
That would be fantastic.
You know, that would get us a point or two.
And that's probably true.
However, if that investigation is not complete and that final report is not out and the referrals
not made, what happens if the election doesn't go your way?
That's a pretty big gamble.
And I am certain that there are some who would push back against that.
Push back against the idea of holding it.
It needs to be everybody.
This needs to be something that gets resolved quickly.
The longer they wait, the longer they drag this out, while it may seem good politically,
The worst it is for everything else.
And with the likelihood of Republicans retaking the House, what happens if that final report
isn't out?
What happens if those referrals aren't made?
This isn't something that they should play politics with.
I know that's going to be hard to convince people in Congress of, but there are some
things that are just duty, and given the fact that a lot of this investigation revolves
around people not fulfilling their duty, not acting when they should, it's really important
that those on the committee pursue this as quickly as possible and get that report out,
explain to everybody what happened and how wide-ranging it was, because I think most
people who have been paying attention understand that what occurred on the sixth, at the Capitol,
that that was cover.
That that was just the public-facing part of something that was much more wide-ranging.
The committee has to pursue that and has to inform the American people as soon as possible.
This isn't something they can play political games with.
And believe me, the fallout from it, from everything that we can see, the fallout from
it is going to be large enough to still swing those points.
I know that's what really matters to a lot of people.
I wouldn't play with the timing on this.
Let's get it done and go from there.
Anyway, it's just a thought.
Y'all have a good day.