Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about processing information
again and understanding methodology.
And we're going to go through three lists that I have here.
I'm going to go through them very quickly.
The lists are from different years.
And we're not doing this to actually establish
a fact with this.
We're showing a pattern.
And we're going to do this because I got this message.
I'd love to believe what you said about crime rates,
but every list I've ever seen says the most dangerous cities
are the ones you'd expect, Chicago, Baltimore, New
Orleans, et cetera.
When I read this, I couldn't help
but think of this scene in a movie.
And I can't remember the movie.
But the guy gets out of his car at a gas station.
He's got a map in his hand.
And he goes and he asks the guy working at the gas station,
he's like, hey, where is any town USA?
I can't remember the name of the town.
And the gas station attendant, he's
like, it's eight or nine miles down the road.
And the guy's like, well, that's weird,
because you say it's eight or nine miles away,
but it's not on my map.
The guy's like, well, that's because that's a map of Illinois
and you're in Indiana.
You need a new map.
Same thing is happening here.
With that in mind, let me reread this.
I'd love to believe what you said about crime rates,
but every list I've ever seen says the most dangerous cities
are the ones you'd expect.
It's a list of cities.
Of course, rural areas aren't in it.
And I'd be willing to bet most of the lists you see
are actually major cities.
And they have a very high population count
to even be included.
So of course, it appears that it's the same cities.
So what I have, the first list I'm
going to run through real quick, it is a list of cities,
but it has a lower population threshold to be included.
And it really changes the results.
It shifts what people expect to find.
Because when you think violent crime,
there are a couple of cities, and one in particular,
that always gets mentioned.
Let's see where it's at on the list.
Number one, Detroit, Michigan.
Number two, St. Louis, Missouri.
Number three, Memphis, Tennessee.
It's already starting to look way different, right?
Four, Baltimore, Maryland.
Five, Springfield, Missouri.
Little Rock, Arkansas.
Cleveland, Ohio.
Stockton, California.
Albuquerque, New Mexico.
Milwaukee, Wisconsin.
San Bernardino, California.
Oakland, California.
Anchorage, Alaska.
Rockford, Illinois.
New Orleans.
That's at 15.
We're at 15, and one city still hasn't been mentioned.
Wichita, Kansas.
Lansing, Michigan.
Nashville, Tennessee.
Houston, Texas.
Chattanooga, Tennessee.
Beaumont, Texas.
Peoria, Illinois.
Odessa, Texas.
Lubbock, Texas.
Buffalo, New York.
We're at 25.
Still no Chicago.
26 is Oklahoma, Tulsa, Oklahoma.
If you want to get to Chicago, you've got to go to 31.
Just because the entry level, the city
had to have a lower population to be included in this list
than most of the list people see.
It shifts your perception.
So the next one we're going to go to
is the one you should be looking at.
This is a good one.
It's at the county level, not cities.
It's looking at the counties, and it's age adjusted,
meaning if you have two areas and one of them
is made up of mostly older people,
then the other one, which is younger people,
the one with the younger people will always
have a higher per capita violent crime rate.
This adjusts for that to give you a better read.
So let's see what the counties are.
Phillips County, Arkansas, Waddenness County, Alabama,
City of St. Louis, Missouri.
That's a county and a city.
It functions as both.
Macon County, Alabama, City of Petersburg, Virginia.
Functions as both.
Leflore County, Mississippi, City of Baltimore, Maryland,
Dallas County, Alabama, Dillon County, South Carolina,
Washington County, Mississippi, Jefferson County, Arkansas,
Orleans Parish, Louisiana.
That's one you'd recognize off the list.
Holmes County, Mississippi, Hines County, Mississippi,
Adams County, Mississippi, Mississippi County, Arkansas,
Sunflower County, Mississippi, Cahoma County, Mississippi,
Vance County, North Carolina, and City of Danville, Virginia.
Now those are gun homicide rates for 2016 to 2020.
Four year period.
When you're looking at something like this, it's better to take a few years.
That way one mass incident doesn't throw things off.
Now that being said, this is from Center for American Progress.
Pretty liberal organization.
So let's flip.
Let's go to one put out by cops.
See what they say.
This isn't age adjusted.
Number one, Orleans Parish.
Number two, Cahoma County, Mississippi, Phillips County, Arkansas,
St. Louis City, Missouri, Baltimore City, Maryland,
Petersburg City, Virginia, Macon County, Alabama,
District of Columbia, Washington County, Mississippi,
Dallas County, Alabama.
Pretty similar.
These are from different years as well.
It's worth noting that even the cops, this is Police 1,
including the cops, there is a clear regional divide.
St. Louis is the westernmost city to make the list,
and Baltimore is further north.
In other words, it's the south.
It's the south.
That's where this happens.
They also go on to point out that it's low income areas
that have the highest violent crime rates.
So when you're looking at those lists, most dangerous cities,
understand what matters is the methodology.
What population does the city need to have to even be considered for the list?
So when you see most dangerous major city or most dangerous metropolitan areas,
by default, they're not including rural areas.
If you do it by the county level, you see some pretty interesting things.
Here in Florida, as an example, the most violent county,
it's not Miami-Dade, it's Levy.
I would imagine that other states are the same.
I'll go ahead and tell you, in Louisiana, it's not Orleans Parish,
currently this year.
You can normally find this information on health department websites.
The thing that throws it off is using raw numbers,
which is what outlets like Fox News like to do.
They use raw numbers.
There were X number of shootings, right?
And the number sounds huge, because rural people live in an area where if that happened,
I mean, that's a tenth of the population.
You need to look at per capita rates, and you need to look at it at the county level.
Age-adjusted is best, but you get the same results pretty much if they're not age-adjusted.
It's also better to look at a grouping of years, rather than just one year in particular.
You will find that rural areas or areas with small towns often have higher violent crime rates.
Anyway, it's just a thought. Y'all have a good day.