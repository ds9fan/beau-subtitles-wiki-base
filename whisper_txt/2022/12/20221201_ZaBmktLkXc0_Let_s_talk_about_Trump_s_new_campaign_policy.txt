Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about some policy changes
that have occurred at the Trump campaign
in response to the dinner
that prompted a lot of discussion.
The general assumption at this point
is that Trump is unable to make his own decisions about who
he can talk to and who he shouldn't.
I'm just going to read this straight from the AP.
So I'm not accused of reframing this.
Trump's campaign is putting new protocols in place
to ensure that those who meet with him
are approved and fully vetted, according
to people familiar with the plans who requested anonymity
to share internal strategy.
The changes will include expediting a system borrowed
from Trump's White House in which a senior campaign
official will be present with him at all times.
Apparently, the former president needs someone
to hold his hand and tell him whether or not
it's OK to talk to someone, because he apparently
lacks the judgment to make that determination himself.
One of the more important things to note
is that, according to this reporting, at this point,
anybody who Trump meets with has been vetted.
This is something they're putting into place.
They're going to fully vet anybody who
meets with him from this point.
So in a few weeks, maybe a month,
when something similar occurs, remember
that they were fully vetted.
This news is just a bad sign for his campaign.
Trump's draw for a lot of people was
that he was seen as a maverick.
He was seen as somebody who would do what he wanted.
That's how he owned Libs and all that stuff.
It becoming public knowledge that he
is being handled in this manner is probably
going to contribute to his continuing
to decline in the polls.
He will continue to lose more and more support
as people begin to see through the facade that existed.
That excitement that occurred during that first campaign
and the shielding that was put up around him
when he was in the White House, it's all kind
of tumbling down around him.
And people are seeing things and noticing things that they may
not have noticed before.
A statement going out in the AP saying that somebody else
is determining who Trump can talk to
and that a senior campaign official will be with him
at all times suggests that his campaign is now running him,
not the other way around.
He's not running the campaign.
The campaign is running him.
And this isn't going to play well
with what's left of his base.
There's going to be another little letdown
as they realize the person they framed as almost a deity
is, according to his campaign staff,
somebody who can't even decide who
to have a meal with properly.
Anyway, it's just a thought.
Y'all have a good day.