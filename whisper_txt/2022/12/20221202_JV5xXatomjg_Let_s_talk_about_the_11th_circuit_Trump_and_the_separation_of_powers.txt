Well, howdy there, internet people.
It's Bill again.
So today we're going to talk about the 11th Circuit,
Trump and a ruling about the special master.
The 11th Circuit, a three-person panel there made up entirely
of GOP appointees decided that, well, basically everything
was wrong with Judge Cannon's order.
The special master is unnecessary.
It violates the separation of powers
as outlined in the Constitution.
It's a pretty stinging rebuke in relevant parts.
We cannot write a rule that allows
any subject of a search warrant to block government
investigations after the execution of the warrant,
nor can we write a rule that allows only former presidents
to do so.
It goes on, it is indeed extraordinary for a warrant
to be executed at the home of the former president,
but not in a way that affects our legal analysis
or otherwise gives the judiciary license
to interfere in an ongoing investigation.
The key elements here are that the investigation
is being conducted by the executive branch.
The judiciary stepping in and trying
to create oversight early on before they're really
supposed to be there is a violation
of the separation of powers.
That's the way the 11th Circuit is looking at it.
So basically, Judge Cannon's order
that granted the special master was vacated.
When this order was given, we talked
about how it was a delay tactic, wasn't really going to hold up.
It was just part of Trump's normal process
when dealing with legal issues, which is just delay,
delay, delay, delay, delay.
We are at the point where it is no longer holding up.
However, it was successful in delaying the process.
So at this point, the FBI will no longer be encumbered,
or the Department of Justice will no longer be encumbered
by a lot of the stipulations put forth by Judge Cannon.
And Cannon really shouldn't have much more to do with this.
This is a huge loss for Trump in the documents case, which,
I mean, if you're being objective,
there are a limited number of ways
in which this can end up turning out favorably
for the former president now.
And those limited ways are dwindling by the minute.
It is not going well for him at all on this case.
So I expect to see a slight acceleration.
We've already seen it pick up after the midterms.
We will probably see it start to move even faster now,
just because DOJ is starting to feel the momentum,
not really having anything to do with this ruling
and the effects of the ruling,
but more having to do with things being out of their way,
and they're just starting to kind of feel
like they're moving in the right direction.
So this is definitely not the last
we're going to hear about this.
Anyway, it's just a thought.
Y'all have a good day.