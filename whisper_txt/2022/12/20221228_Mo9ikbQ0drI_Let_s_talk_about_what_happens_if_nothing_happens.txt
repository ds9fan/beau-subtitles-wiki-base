Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about what
happens if nothing happens.
If DOJ doesn't move forward with anything.
Because of the referrals, there are
a lot of people who seem to be more
expectant of action from DOJ.
I would point out again, the referrals,
I don't think the referrals are going to have much to do
with DOJ's decision-making process.
If anything, I think it may actually cause them to delay.
If they were slated to go forward and move out there
and indict tomorrow, they would probably
wait a couple weeks now to let the political aspects kind
of fade out a little bit.
But, there are questions about what happens if nothing happens, if they don't move forward.
And the big one seems to be, is it going to make it more likely that the next Republican
president attempt the same thing and be more successful with it?
No, not immediately.
There is a risk associated with inaction.
Don't get me wrong, but it's not that.
I'm not sure of the order of the videos that are going to be coming out, so either we've
already had one coming out or that came out that explained how difficult it would actually
be to do what Trump was attempting in the United States, or one will be coming soon.
But the real danger here isn't somebody attempting to duplicate what Trump did.
Because what Trump did was kind of doomed from the beginning.
It wasn't going to get, it wasn't going to 100% keep him in office no matter what happened.
That wasn't, that was not an outcome on the table.
The real danger here isn't at the federal level.
It's at the local and state level.
If there is no action whatsoever, you will see local and state officials start to be
a little bit more emboldened.
You will have lawmakers become lawless and start doing the same thing with the elections
and casting doubt on it.
You're already seeing it at the state level.
You've already seen it occur at the state level.
will get down to the local level. Once they're successful there, where it's a
whole lot easier, the network at the local level will then help it occur at
the state level. And then the networks at the state level, well then they really
could do it at the federal level. It would be a whole lot easier and it
wouldn't require storming a building. That's your real danger. It's not an
immediate thing. Honestly, it's 10 or 15 years away, but that's the real
risk. A newer crop of politicians emboldened by a total lack of
accountability that start doing it themselves at a local level where it's
It's easier to get away with it.
And then they establish those networks and they carry those networks with them as they
move up the political ladder in the United States.
And then by the time they get to D.C., there might be 15 or 20 of them that are like-minded
that actually have the networks in place to do what Trump wanted to do.
Because the United States is not coup-proof.
It just can't be amateur hour, which is what the sixth was.
That's your risk.
That's the worry.
It's not something you're going to see in two years or four years.
You get out to eight, then it starts becoming more of a concern.
When it comes to the downstream effects from a lack of accountability, it might be so far
removed that those who are tasked with providing accountability today, don't see it.
Anyway, it's just a thought, y'all have a good day.