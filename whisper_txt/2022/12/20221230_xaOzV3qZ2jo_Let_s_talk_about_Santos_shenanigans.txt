Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about some shenanigans
going on in New York and in DC, originating in New York.
I know earlier this week, we talked about Santos.
And if you aren't aware of the story,
a person ran for Congress and they won their election.
After they won, a lot of questions have been raised
about their biographical data in pretty much every regard.
It's just everywhere, from education to work experience
to heritage, I mean everything is in question.
There definitely seems to have been some embellishments
along the way.
Now there are a lot of people calling for him to step down,
obviously, and there have been statements saying
that prosecutors are looking into it.
The thing is, as far as the misrepresentations,
I don't know that it's actually illegal.
But Congressperson Richie Torres is introducing an act
to fix that.
The act is called the Stop Another Non-Truthful Office
Seeker Act.
Stop Another Non-Truthful Office Seeker.
So the Santos Act, that's got to hurt.
It would require people to provide information
about their educational background, military service,
and employment history.
And it would be included with their filing
a statement of candidacy, which means it would be under oath.
So if you're going to run for Congress,
you have to fill out this form.
This form, if this was to pass, would now
include basically a work history and an educational history.
And you would have to fill it out accurately.
If you lied then, it would be a violation of federal law.
So that is something that is currently underway.
I don't know whether or not it's going to pass, to be honest.
It seems like something that should.
But at the same time, you're limiting Congresspeople's
ability to lie to the American public.
So I don't know if they would vote in favor of that.
But the proposal is being made.
As far as what's going on with the Santos situation,
I would imagine that the prosecutors might
be looking into something other than just
the misrepresentations about his work history and education
and stuff like that.
There's probably more to it.
We'll have to wait and see.
My guess is that this may have something
to do with campaign finance or something like that.
So anyway, it's just a thought.
Have a good day.