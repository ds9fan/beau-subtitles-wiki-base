Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the possibility
of President Biden running for re-election in 2024.
And the idea that he's just too old
because that's a concern.
It is being voiced a lot.
I got a rather entertaining message about it.
So we're gonna kind of go through it.
I would point out that a lot of this is based
on the idea that he should announce now
that he's not going to run and go ahead and clear the field.
And he should announce now because Trump announced now.
Please remember, Trump announced very, very early,
traditionally speaking, like nobody announces this early.
It's probably not actually about running.
I don't know.
Maybe he thinks that if he's running for president,
it would provide him some kind of insulation
if he was in some legal situation
that nobody's heard of or anything.
Maybe it's that.
Okay, so here's the message.
Biden's too old for re-election in 2024.
The grim reaper is right around the corner.
Shouldn't he say he's not running and open up the field?
Okay, so first, I don't know that Biden's too old.
Okay, that's neither here nor there to me.
Two years is a long time.
Currently, he's not an incredibly popular president,
but two years is a long time.
And look at the midterms.
He's not so unpopular that he's a drag on the party.
We don't know.
We don't know whether or not he's gonna run.
But let's assume he's not going to run for a second.
Let's assume he's not gonna run
and that he already knows he's not going to run.
He still shouldn't say that he's not gonna run.
Right now, the Republican Party is devoting
a lot of attention and a lot of energy
and a lot of resources to attacking Biden.
And they're doing that because they're worried
about running against him in 2024.
All of the stuff about the Democratic Party
being afraid of Trump, that is very much projection.
You didn't see the Republican Party out there
funding Democratic candidates that were aligned with Biden.
You didn't see that happen.
You didn't see them out there trying to get
Biden-linked candidates through their primaries
because they know they'll be easier to beat.
That didn't occur.
The reverse did.
Okay, so if he stays, doesn't announce
that he's not gonna run,
the Republican Party is still very focused on him.
They're coming after him.
They're not going after the other candidates.
They're wasting their resources.
They're building that rhetoric.
They're building that expectation.
The other thing is that if he says he's not running again,
he immediately becomes a lame duck.
He becomes powerless. He can't push his agenda through.
And whether people want to admit it or not,
he is getting a lot of his agenda through piece by piece,
little bit here, a little bit there,
little bit here, a little bit there.
And in two years, the political situation
may look very, very different.
And a lot of it may be attributed to the fact
that he's not running.
It may be attributed to his agenda,
which means maybe suddenly people don't care that he's old,
or it gives his endorsement a lot of weight,
gives his endorsement a whole lot of weight
if he manages to turn everything around from here.
So I don't think that his age
really has much to do with this.
I don't think that that's gonna be a deciding factor.
I really don't.
I think his performance is what's gonna matter.
And I think that's what he would base his decision on.
But no matter what the situation,
even if he's already decided
that he's definitely not running,
he shouldn't announce, he shouldn't say that.
He shouldn't give the Republican Party time
to hit those other potential candidates way in advance.
If he knows he's not gonna run,
he can stand there and act as the lightning rod
and keep the Republican Party focused on him.
Keep the Fox News talking points focused on him.
And it spares the eventual real candidate
all of that negative attention.
And he's smart enough to know that.
Biden is not new to politics.
So I wouldn't expect him to announce he's not going to run,
even if he decided he wasn't going to run
on day one of his presidency.
If he decided that first day he wasn't gonna run
for re-election, he wouldn't tell anybody.
I'd be super surprised if he did.
So anyway, Grim Reaper around the corner or not,
I don't expect this announcement to occur.
Anyway, it's just a thought. Y'all have a good day.