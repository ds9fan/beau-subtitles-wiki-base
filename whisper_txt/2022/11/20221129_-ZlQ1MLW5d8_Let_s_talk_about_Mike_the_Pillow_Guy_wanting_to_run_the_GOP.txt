Well howdy there internet people, it's Bill again.
So today we are going to talk about the selection of the new chair of the Republican Party.
An interesting figure has decided to throw their hat into the ring.
The pillow guy.
The pillow guy.
Mike Lindell has said that he will be attempting to get the votes to be the new chair of the
Republican Party.
A person who will be making all the decisions for the Republican Party.
This is a position that requires a lot of knowledge and a lot of wisdom.
Often times, knowledge gets summed up as knowing that Frankenstein wasn't the monster, he was
the doctor.
Now Lindell has set out to do his due diligence and reach out personally to all 168 voting
members in an attempt to address their concerns, which he does admit that some of them have.
And he has pointed out that one of the conversations took four hours.
Now realistically, I'm fairly certain that McDaniel, the incumbent, actually already
has the votes to keep the position.
But you never know.
But I think a more important question is whether or not Lindell is going to accept the results.
For anybody who doesn't know who we're talking about, this is the same Mike Lindell who had
the FBI want to take a look at his phone and constantly spread false information about
the elections and always promised to have the evidence of the malfeasance two weeks
from Thursday or whatever for the last two years.
This is a person who, while unlikely, could become the next chair of the GOP.
If knowledge is knowing that Frankenstein wasn't the monster but was the doctor, wisdom
is knowing that the doctor was the monster.
And that's the situation the GOP is in.
They have allowed all of these bogus claims to circulate.
They have leaned into them when they thought it would be useful.
They didn't correct them.
They didn't come down forcefully as elements within the Republican Party attempted to subvert
basic democratic institutions within the United States.
Because of that, a lot of people have found their home in the Republican Party.
And again, while it's unlikely, it's not impossible that Lindell becomes the chair.
More importantly, this shows that this isn't over.
They will still have problems with their primaries.
They will still have people refusing to accept very clear results because they haven't came
down on this forcefully.
They haven't pushed back enough.
As this process continues and more and more of their base becomes disillusioned with the
electoral process, they're going to stop voting.
The Republican Party has created a situation in which they will end up suppressing their
own vote.
And it's all because they lack the courage to stand up to a tweet.
This will probably become a pretty well-covered story.
Again, I stress, it is incredibly unlikely that Lindell gets this slot.
But it isn't impossible.
Meet your monster.
Anyway, it's just a thought. Y'all have a good day.