Well, howdy there, internet people.
It's Beau again.
So today we are going to provide some context
and clear up some information that has spread
that isn't entirely accurate.
And it's about something that I'm not covering in depth,
but a whole bunch of questions came in about this
because of, well, you'll see.
So we're gonna kind of go over this,
but this isn't something I'm covering deeply.
Frankly, I just don't have the plates to cover everything.
And I wouldn't want to make a mistake in reporting,
which brings us to what we're going to talk about.
There is reporting that suggests 15,000 people in Iran
have been sentenced to death.
Didn't happen.
That has not occurred.
As far as any reporting that can be verified,
there has been one sentence like that.
And this person, according to the allegations,
torched a government building.
The walls are very different there.
That basically amounts to an act of war against God.
The sentence for that is obviously pretty severe.
So there is context to this claim though.
The 15,000, that number comes from a UN estimate
of the total number of people detained
in the demonstrations in Iran.
To my knowledge, only one to 2,000 of those
were ever actually even charged.
I don't even know that all of those people
are still in custody.
But again, I'm not covering this deeply.
So fact check that as more information comes out.
The letter that is being referenced
it's real, it is real.
It came from parliament to the judicial branch.
Iran has a separation of governments,
not unlike the US.
But the letter itself doesn't actually specify
what the punishment they're saying should be.
Just saying, hey, it should be severe
and teach the lesson quickly.
It's also not like from all of parliament.
It's, I wanna say 220, which is a decent amount
of their parliament.
But this is more akin to, let's say,
Boebert and Holly and the Space Laser Lady
putting out a letter to judges in Portland
saying, hey, be extra tough on those demonstrators.
That's kind of what this is.
So there have not been 15,000 sentences
handed down like that.
As far as anybody knows, there's been one.
That being said, the people I know
that actually are covering this deeply,
they do expect more.
And there is a significant worry
about a number of people who were charged with crimes
that that is a possible punishment.
So while it hasn't happened,
that doesn't mean that the Iranian government
isn't going to use that penalty
to deter further demonstrations.
But at this moment, it hasn't occurred.
It appears as though this is journalist telephone
where one outlet reported something
and then another outlet got a piece of information
and then somebody put the two together.
And then as it moved along, the story kept changing.
But at this point in time,
there are not 15,000 people sentenced that way.
And there is a significant concern
from human rights advocates
about those who have been charged
with the more severe crimes.
But there's also not an expectation of 15,000.
That's nobody that I talk to.
Again, I'm not covering this deeply,
but nobody I talk to is expecting numbers
anywhere near that high, like not even close.
But they do expect more, which any is a concern.
I would wait for a fact check from AP
or something like that
before getting in too much of an uproar over this,
because the information that is available tonight,
it doesn't look like this is happening
the way most of the reporting is saying it is.
So I would wait for somebody who is really covering this
to talk about it or a fact check
from a straight up news organization
that doesn't have a political agenda here
that is just saying this is what's occurring.
Because there's a lot of information that is inaccurate,
but most of it looks like it's inaccurate
due to it being out of context.
Not that somebody is intentionally misreporting,
but more they're synthesizing pieces of information
that don't necessarily go together.
So I would be real careful with this one.
Anyway, it's just a thought.
Y'all have a good day.