Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about whether or not Republicans have it figured out yet.
I'm going to answer a question and I'm going to use a quote to answer the question
and then use that to illustrate a point.
Because I was asked if I really believed that they just didn't get it yet.
That they don't understand what is separating them from their base
and what is causing their very long streak of underwhelming performance at the ballot box.
And the answer is I don't think they have it figured out yet.
In fact, I think that there is a faction within the Republican Party
that actually intends to double down on what they've been doing.
They truly believe that honing the libs on social media somehow will translate to electoral success.
They're truly invested in this idea because they think that's what got them Trump.
And again, when it comes to Trump, when he initially won,
there were a whole lot of people who weren't voting for Trump.
They were voting against Hillary Clinton.
But they think it's his rhetoric that got him into office that first time.
They think it's that social media edginess that put him in power and they want to duplicate it.
As evidenced by the space laser lady who is for whatever reason very prominent within the Republican Party.
Now at time of filming, the House hasn't been decided yet, but we have this.
Republicans in the majority solidly united with our, all caps,
Speaker of the House means we have subpoena power for those that don't understand.
And actually says that.
That demonstrates pretty clearly that there is a segment of the Republican Party
that believes their path to victory involves subpoenas.
It involves looking into Biden's kid or rehashing baseless theories about the election, stuff like that.
They're doing this so they can talk about it on social media and in theory own the libs.
It's not going to work. It will not work.
This is a whole lot like the idea of making Trump Speaker of the House.
No, no, don't throw me in the briar patch.
It's the same type of thing.
When they double down on this, the American people are going to see the Republican Party burning money
and wasting time and focusing on trying to re-litigate an old election and spread conspiracy theories
or going after Biden's kid, something like that.
This isn't anything that's going to resonate with the voters they're trying to reach,
because again, they still don't understand that it's their policies.
It's their rhetoric.
It is the fact that at this point, they are all edge and no point.
They don't have any policy and the American voters will see that.
They'll see them playing games on social media rather than trying to steer the country.
And it will lead to another failure at the ballot box.
I don't, I know that a segment of the Republican Party hasn't learned their lesson yet.
I don't know if that segment is going to end up on top and they're going to be able to call the shots.
But there's definitely a large portion of the Republican Party that equates retweets to votes.
And that's not real.
That's not how our system works.
I think they'll continue to make a lot of the same mistakes.
I think they will continue to scapegoat and alienate people.
You know, it's a lot like when they put that, when they put that recording out from Biden to his son
and believed that that was going to somehow be damaging to it.
And it literally made him the most human president ever.
They haven't learned their lesson.
They will keep making the same mistakes.
Anyway, it's just a thought.
Y'all have a good day.