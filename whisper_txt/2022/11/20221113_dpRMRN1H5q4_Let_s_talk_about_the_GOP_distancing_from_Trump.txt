Well howdy there internet people, it's Beau again.
So today we're going to talk about the developments in conservative media
and the almost certain developments within the Republican Party.
We have seen the New York Post, the Wall Street Journal,
Washington Examiner, even Fox News hint or directly state that it is time
for the Republican Party to part ways with former President Donald Trump.
The question is why?
I mean most people watching this channel, you're going to be like, it's about time.
But why are they saying it now?
It's not that they're different. It's not that they're morally opposed to his
authoritarian brand. It's nothing like that.
Those who supported him and mimic him, they are him. They enabled him.
They brought that rise.
What changed? Was it his habit of lying to America and to his own constituents?
Is that what caused them to want to separate?
No, of course not. The xenophobic rhetoric wasn't that either.
Was it when he sold out our allies? Nope.
When he downplayed a public health issue?
Undermined US foreign policy?
Undermined the working class?
Through attack after attack on marginalized people inside the United States?
Scapegoated them? Wasn't that either.
Jeopardized national security?
Directed people to the Capitol?
It wasn't any of that. Why do they now advise to break ties with him?
Because he lost. It's that simple.
It's not a moral position. It's not an ethical one. It's not a philosophical one.
He lost. His brand is fading.
His ability to rally people to the polls is fading.
They don't have a use for him anymore. He lost.
But that's it. There's no attack on the behavior that led Americans to reject him.
It's just that he lost because those people that want to maintain power for the far right in this country,
they don't care how they do it. It's power for power's sake.
They don't care about the rhetoric. They don't care about who gets harmed in the process.
They care about power and Trump can't deliver anymore.
So they're done with him. The question you have to ask yourself is whether or not that's the country you want.
If you want to follow people who will accept everything that Trump did as long as they're winning,
if those are the people you think should be directing this country,
or if you think that it may be at some point you could lead yourself,
you could bring about a better world that doesn't rely on kicking down.
They didn't disagree with anything related to Trump or Trumpism.
He's just not their ticket to power anymore.
Anyway, it's just a thought. You know, have a good day.