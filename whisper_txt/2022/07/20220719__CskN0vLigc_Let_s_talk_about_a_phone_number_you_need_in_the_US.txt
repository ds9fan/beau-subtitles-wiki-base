Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about a new number,
a number everybody in the United States needs to memorize.
Even though right now, as it's being rolled out,
it is not yet what it will soon be, it's a first step.
And it's a good first step.
And eventually, this is going to be something
that is going to save a lot of lives.
The number is 988.
And it will be 911, but for mental health.
As it rolls out, I actually think it's active now,
you will be connected to counselors.
And this is, at least for the moment,
heavily aimed at suicide prevention.
But as time goes on, they appear to be gearing up
to add other services.
Right now, it's a counselor on the phone.
They have plans for mobile care units.
So rather than dialing 911 and sending the cops
to somebody's house, well, you can send to counselors.
This is a good thing.
This is something that's been needed.
There was a lot of campaigning around the idea
of taking some responsibilities away from law enforcement,
lightening their load, so to speak,
and putting it into the hands of people
who might be a little bit more trained
or educated on that topic.
This is the first step to doing it.
The United States government is devoting,
I want to say, like a quarter billion dollars to this
to kind of build up these capabilities.
The idea is to eventually also have what amounts
to like the little county clinics that exist,
but for mental health.
So there's an in-person place that you can go to.
In a country that has a very long history
of just ignoring mental health, this is a very hopeful sign.
The number is 988.
So file that away, 988.
And you will probably, depending on the state you live in,
you'll probably eventually hear about legislation
that's designed to fund some aspects of it,
because it's going to be a lot like 911.
The state is going to bear some of the cost.
So when that comes up, when they start talking about funding 988,
this is what it's for.
It is to create a 911 for mental health.
So anyway, it's just a thought.
Y'all have a good day.