Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about how Trump lost Idaho.
The Republican Party there, the state level,
they just had their convention,
as many states are doing right now,
and they're setting up their party platform.
The Republican Party in Idaho decided
that they had no problem with being seen as,
let's just say an outlier,
being seen as a bit more unusual.
Some might use the word extreme.
They adopted a lot of unique pieces to their platform.
One is that, according to their platform,
there will be no exemptions for the life of the mother.
If mom's life is in jeopardy, well, it doesn't matter.
You know, no treatment.
You still have to have the birth.
Now, how they expect the pregnancy to be completed
if mom goes, I will never know,
but this is the sort of thing they adopted.
It's extreme by anybody's measure, right?
They also adopted language suggesting that the 16th Amendment
to the US Constitution be repealed.
That is taxation at the federal level,
which is unique coming from Idaho,
because I think they're rated the sixth most dependent state
on the federal government.
So that seems odd to me, but whatever,
they're willing to be seen this way.
They're willing to adopt these just wild party planks,
and that's fine.
It's their party.
They can do what they want.
You know what didn't get adopted?
Rejection of the 2020 election.
Other states passed that.
Other states passed language that said,
you know, hey, Biden's illegitimate or whatever.
Trump really won.
The Republican Party put that into their platform
in other states, but not in Idaho.
Idaho, a state that by a four to one margin
said that mom's life doesn't matter.
They were like, no, no.
Believe in Trump's baseless claims?
No, that's a bridge too far for us.
Even we're not that out there.
That is really bad news for Trump.
It's going to be incredibly hard for the former president
to galvanize any support, to galvanize that base,
when one of the more extreme state parties
left him out in the cold.
It didn't even make it out of committee.
That's got to hurt.
When it comes to the Republican Party,
I mean, Idaho, that's a pretty safe area.
For a state platform that was willing to be seen
as incredibly extreme to not adopt that language,
that is a slap in the face.
That is them saying, yeah, I mean, we might be nuts,
but I mean, hey, we can count.
Not going to fool us again.
That's bad news for Trump.
I imagine there will be some Idaho potatoes and ketchup
on the wall when he hears the news.
So while the platform that was adopted in Idaho
is obviously not ideal by any stretch of the measure,
that is an incredibly unique development.
Even with all of the extreme language,
they're not backing Trump anymore.
Anyway, it's just a thought.
Y'all have a good day.