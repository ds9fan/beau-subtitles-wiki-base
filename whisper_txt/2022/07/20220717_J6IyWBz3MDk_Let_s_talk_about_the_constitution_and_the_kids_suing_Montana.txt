Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Montana
and something that is going on there with the kids,
because the kids are all right.
Youth in Montana have launched a lawsuit
against the state of Montana,
alleging that Montana fostering fossil fuels
the way that they are,
well, it's contributing to environmental degradation
and that it's just going to disrupt
and destroy their future via climate change.
And the first time I talked to somebody about this,
they're like, yeah, they've got a constitutional right.
And I'm like, look, I love the idea
and I'm very sympathetic,
but they do not have a constitutional right.
That's not there.
And they sent me the Montana State Constitution,
which says, the state and each person
shall maintain and improve a clean
and healthful environment in Montana
for present and future generations.
The legislature shall provide for the administration
and enforcement of this duty.
The legislature shall provide adequate remedies
for the protection of the environmental life support system
from degradation and provide adequate remedies
to prevent unreasonable depletion
and degradation of natural resources.
All right, you got me.
They do. It's there.
So, kids have been fighting this for a while.
The state, of course, has been fighting back.
They went to the judge and were like,
yeah, you got to get rid of this case.
The judge is like, no.
They took it to the Montana Supreme Court
and the Supreme Court there is allowing it to go forward.
Currently, the trial date is set in early February 2023.
The way things normally work, though,
you're probably looking at early summer.
There's going to be delays
because this is going to be a big case.
A lot of depositions.
It's interesting because if this succeeds,
it will probably cause a ripple effect in a lot of places.
I would note that a very brief search found similar cases
in Hawaii, Virginia, and Utah.
It's an interesting tactic.
I can't wait to see how it plays out,
but I'm pretty impressed, to be honest.
The name of the case, if you're curious,
is Held v. State of Montana.
Anyway, it's just a thought.
Y'all have a good day.