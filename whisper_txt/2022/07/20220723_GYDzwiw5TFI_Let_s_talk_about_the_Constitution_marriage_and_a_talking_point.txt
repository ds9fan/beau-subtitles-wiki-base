Well, howdy there internet people. It's Beau again. So today we're going to talk about the
Constitution of the United States and rights and marriage and rights that seem
as though they should be self-evident. We're going to do this because I have
encountered the talking point that the Republican Party, the right in general,
is going to try to use to undermine gay marriage. Because shortly after that
video I encountered it multiple times from different people. So I'm assuming
somebody said this recently and it's now out there and this is what they're all
going to parrot. The right to gay marriage isn't in the Constitution!
I'm an originalist. Sure you are. Show me on the Constitution where the
right to get married exists. Gay marriage isn't in the Constitution. It was
overreach by the court. The court declaring rights not in the Constitution
is unconstitutional. Okay, so this is how we're going to go with this. This is
going to be the talking point. That's fine. Let me just start with this. Whoever
told y'all this, whoever put this talking point out there, they have either never
read, didn't understand, or more likely don't support the U.S. Constitution. And
they're feeding you this in hopes of you also failing to support the U.S.
Constitution. That's their goal, Mr. Originalist. Okay, so my fellow
constitutional scholars, I would like you to turn in your pocket Constitution to
the Ninth Amendment. The enumeration in the Constitution of certain rights shall
not be construed to deny or disparage others retained by the people.
Enumeration means list. They put this at the end of the Bill of Rights and they
said, hey, just because we listed these, that doesn't mean there aren't other
rights. So the idea that a right has to be specifically listed to exist is wrong.
It is not constitutionally based. The Constitution itself tells you that. This
is a bad talking point by people who don't support the Constitution or don't
understand it. See, the people who put the Bill of Rights together, they were
concerned, they were worried, that at some point some disingenuous authoritarian
goons would say, hey, you didn't specifically list this right, that this
thing that is clearly self-evident, you didn't write this down, therefore that
right doesn't exist. So we can just, you know, take that away.
Where in the Constitution does it say that you have a right to raise your own
children? Where in the Constitution does it say that straight people have a right
to get married, right to travel? There's actually a whole bunch that aren't
listed. So this talking point that they have y'all out there parroting, all it
does is make you look like somebody who, I don't know, couldn't uphold your oath.
Because you never actually read it. It's the Ninth Amendment. The founders, the
people who put the Bill of Rights together, wrote a specific amendment to
counter the kind of people you're following right now. They knew they'd
exist. They knew at some point there would be authoritarians, more
authoritarian than they were, who wanted to chisel away at the freedoms in this
country. So they wrote that in there. If you have parroted this, stop. It doesn't
send the message you think. Originalist. I just, I want to hammer that point home
again. The people you're following, the people you're getting these talking
points from, are the sort of people that the people who wrote the Constitution
viewed as their enemy. They viewed them as the danger to freedom that they wrote
about in all of their other writings. And those are the people you're following
in your parroting. That should, if you, considering three of you have the word
patriot in your screen names, that should trigger a little bit of self-reflection.
Anyway, it's just a thought. Y'all have a good day.