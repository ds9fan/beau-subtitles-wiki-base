Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about inflation
in the United States.
Bidenflation, right?
It's at a 40-year high in the United States.
Okay.
So something we've talked about on the channel
is that it's a global thing.
It's not just confined to the United States.
The idea of pinning it on Biden seems a little silly.
And I've said this, but now we have like really hard data
to back it up from Deutsche Bank.
They looked at 111 countries.
Where's the United States?
About middle, about middle.
We're running around 8.6%,
which means we're doing better than the Netherlands,
worse than Germany, okay, as far as inflation goes.
The only places that really seem to be immune to this
are Japan and China.
Japan historically has low inflation, like all the time.
In fact, they have trouble hitting
the percentage of inflation they want most times.
China has price controls.
So that helps a whole lot.
Turkey has the worst inflation in the G20 at 74%.
Most of the large economies to the south of our border
or in Africa are running in double digits.
So here's the thing.
If the US is just doing average, as far as inflation goes,
it's running in the middle of the pack, so to speak,
how's it Biden's fault?
It's a global phenomenon.
It's happening everywhere.
More importantly, if you are watching a channel
or a pundit who is continuously trying to pin this on Biden,
why do you watch somebody who thinks so little of you,
who thinks you're that easy to manipulate,
that you can't find this out on your own,
that you are just incapable of understanding basic numbers,
or do they just believe you won't look it up yourself?
A lot of echo chambers, information silos have developed,
and they have become so confident in their control over you
that, well, they don't think you'll look elsewhere
for information.
They think that they have to spoon feed you
and that you'll believe whatever they tell you.
If you have information sources
that are constantly referring to this
as something that Biden did, here are your facts.
Here's the data.
You can look it up.
Why would you continue to be informed by an organization
or a person who you know is intentionally misinforming you,
intentionally framing something in a way that is untrue,
and that, well, it's detrimental to you?
Anyway, it's just a thought.
Y'all have a good day.