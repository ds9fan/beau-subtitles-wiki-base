Well howdy there internet people, it's Beau again. So today we're going to talk about
the power of the screen and sharks and how the power of the screen might be used for
good. You know, when I was a kid there was a series of movies that came out. The first
ones came out before I was born. But there was a bunch of them. And they made people
avoid the water. Made them avoid the water. People didn't even live near the coast. There
were people who didn't want to get in lakes because of these movies. They didn't want
to end up snack food for great white sharks. In fact, there are studies that suggest your
opinion of sharks today, almost 50 years after the first movie came out, might still be influenced
by it. And that the decline of sharks in the water might have had a lot to do with the
Jaws. Not a great story there. Not a good use of the screen, right? But it could be
used for good. When I put out that video talking about a comprehensive strategy for gun control,
one of the things I said that would be really useful is if somebody reached out to Hollywood
and got them to alter the way they portray firearms in movies. And it was one that got
kind of the most laughs as far as people thinking it just wasn't going to happen. And I get
it. It seems far-fetched. But man, could you imagine that kind of impact? Removing that
macho toxicity that has become associated. Changing the whole perception of it. It's
possible that it was sharks. They just need the will, right? So, 200 directors and producers
and Hollywood types, they put out a letter. As America's storytellers, our goal is primarily
to entertain, but we also acknowledge that stories have the power to affect change. Cultural
attitudes toward smoking, drunk driving, seat belts, and marriage equality have all evolved
due in large part to movies and TV's influence. It's time to take on gun safety. That's
promising. The letter goes on and it basically outlines that they're not just talking about
gun safety on the set. They're talking about being more critical in the way the firearm
gets used in storytelling. And they're hoping to shift perception. Now, what the perception
they're going to try to shift it to, well, we don't really know that yet. And how effective
it's going to be. We don't know that yet. But what we do know is that historically,
Hollywood can shift thought without a law. They can change the way people think. And
if they do it right, they can drastically alter cultural perceptions. They've done
it over and over again. Sometimes for good, sometimes for bad. But it's definitely possible.
And this letter, it's just a letter, you know. But it marks a shift if this becomes
a standard and they reduce the perception that the gun makes the man. They reduce the
idea that violence is always the answer. It will have a marked effect. If they can portray
that enough, if they can get those stories out there, give us more MacGyver characters,
the impacts will be pronounced. It'll save lives. This is the kind of out-of-the-box
thinking that we need. Anyway, it's just a thought. Y'all have a good day.