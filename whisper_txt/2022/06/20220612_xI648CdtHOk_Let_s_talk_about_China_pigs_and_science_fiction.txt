Well howdy there internet people, it's Beau again.
So today we're going to talk about a science fiction plot line that is
apparently not fiction and it blends a lot of things together that people have
ethical questions about on their own. It blends a lot of issues together all at
once. So let's just get started. Scientists in China have decided to
clone pigs en masse using artificial intelligence with no human involvement
so they can eat them. Yeah I mean if you were to find a way for one of those
little robot dogs to deliver the pigs to market that would pretty much be all of
my science fiction nightmares all rolled up into one. So the obvious question is
why did this happen right? Okay so pork is an incredibly important meat in China
and they never have enough supply to meet demand under the best of conditions.
Recently a lot of their pigs got sick and that further curtailed their own
production and would lead to more imports. Food security, super important to
the leadership there. This was apparently the route they chose to to achieve that.
So that's what's happening. I want to be clear about
something. This isn't lab-grown meat. This is whole pigs being cloned by
artificial intelligence to then be turned into meat. So it's like lab-grown
meat with extra steps. There's a whole lot of ethical questions involved in
this all at once and a lot of concerns that people who would consider themselves
futurists are probably having right now. The interesting thing to me is not only
has this apparently been pretty successful but the artificial
intelligence is actually better at cloning than people are. The process of
cloning is complicated and the short version is cells often get damaged and
the whole process kind of goes, you know, it doesn't go anywhere when people do it.
And the AI is much better at producing the desired result. I'm gonna be honest, I
don't really have a closing thought on this one. This was just some information
that I happened to come across that I thought, wow, you know, with everything
else going on right now this is probably worth sharing. We know that we're going
to have to come up with some innovative ways to deal with food security and we
know that we're going to have to mitigate in a lot of ways to deal with
climate change. I'm not sure that this is the way to go about it, to be honest.
Call me old-fashioned, but this is definitely a story I'll be following. As
it progresses I will let you know what happens. Anyway, it's just a thought. Y'all have a good day.