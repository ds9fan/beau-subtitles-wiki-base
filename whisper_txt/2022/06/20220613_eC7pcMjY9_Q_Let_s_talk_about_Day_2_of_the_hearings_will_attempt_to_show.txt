Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about what to expect
from the hearings today.
So far, it looks like they're following the template.
Tell them what you're going to tell them.
Tell them.
Tell them what you've told them.
Day one was definitely, tell them
what you're going to tell them.
Day two looks like the start of that second part, the tell them
phase, right?
OK.
So what it looks like they're laying out
is that Trump knew that all of his claims about the election
were false.
He knew they weren't true when he was making them.
That appears to be what they're going to try to demonstrate.
That he had been told, that everybody told him this.
He knew he was lying is their position.
And that's what they're going to try to show.
You will hear from Trump's former campaign manager,
a former Fox News political director.
If I'm not mistaken, it's the one
who got canned after correctly calling the election, which
I guess that's something you don't want on a news outlet.
Anyway, you'll hear from an election attorney who is
probably just going to provide context.
You will hear from a former US attorney, BJ Pock,
US attorney for the Northern District of Georgia, I think.
Might be middle.
The interesting thing is that Pock resigned,
I want to say the day after, maybe two days after,
that phone call where Trump was calling down to Georgia
wanting to find the votes.
I've said that I think there are going to be some things
that the American people really don't know,
most Americans aren't aware of, that get brought up
in these hearings.
If I had to guess, I have a feeling
BJ has some surprises for us.
And that's just a hunch.
But if I was going to be looking for new information,
I'd be paying attention there.
And then a former Philadelphia city commissioner.
Honestly, I have no idea why.
That one I'm not sure about.
But it looks like they're going to try to establish
that Trump knew he was lying when he made these claims.
And then through the rest of the hearings,
they'll be able to point back to that
and say he said this, which caused this action,
and he knew this to be untrue.
That seems to be their route.
Now, keep in mind, these hearings
are going to go on a while.
So you're going to have to kind of keep
a lot of these little bits and pieces in mind.
It seems to be what they're going
to try to demonstrate today.
I can't say that for sure.
But that looks like where it's headed.
Now, whether or not they're able to demonstrate that,
that's kind of up to you.
You've got to watch and find out.
Anyway, it's just a thought.
Y'all have a good day.