Well howdy there internet people, it's Beau again.
So today we are going to talk about Russia and Ukraine, costs, grain, and Africa.
When this started, when Russia invaded Ukraine, we talked about how it was going to disrupt
the world's grain supply.
And we talked about how it was going to hit Africa first, it was going to hit poor countries
first.
That has started.
It's beginning in the horn of Africa, Somalia, in that area.
And there was a drought.
Now a drought is a drought, but there's not a lot of aid because the grain supply has
been disrupted.
They've already lost hundreds to famine and it will get worse.
When this really starts catching coverage everywhere as it spreads, this is Putin's
fault.
This is the cost of imperialism.
It's a cost that never gets talked about when wealthy countries, when powerful countries
play their little masters of the universe games.
The cost that is paid by those people who live in countries that have nothing to do
with it, who don't really have any chips in that international poker game where everybody's
cheating, those costs are almost always ignored.
And because they're ignored, we continue to make the same mistakes and we continue
to just forget about it.
Not a big deal.
We don't really have to worry about that because that's somewhere over there, those
far away places, those places that don't matter because they're not economically
powerful.
The coverage that is going to come out of Africa is directly tied to Putin's invasion
of Ukraine.
There's no other way to spin that.
The disruption of the grain supply is causing this.
The drought is the reason that locally they're not capable of producing their own, but without
the disruption caused by that invasion, aid would be there.
You'd have a lot more aid coming in.
But right now, countries are focused on more important things, you know, money, as hundreds
are lost because they don't have food.
That is a sad state of affairs and it is the reality of the imperialist nature of large
powers.
This is the cost.
It happens all the time.
These games that get played on the foreign policy scene, they impact countries that are
seldom thought of and never visited.
Anyway, it's just a thought.
Have a good day.