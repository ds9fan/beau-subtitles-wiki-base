Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Trump's bad week.
And it's not about the hearings.
While the hearings are spreading his dirty laundry all over the country,
there's another issue that he's dealing with.
The first is his social media platform, Truth Social,
the platform that he suggested was going to be the free speech,
you know, social media platform.
There are reports suggesting it is now banning anybody
who is talking about the hearings.
That's not good for marketing.
But it's his company. He can do whatever he wants with it.
A much more important development is happening in Alabama.
Trump initially endorsed Mo Brooks there.
And then when Brooks was not willing to play along with the, you know,
whole election thing that Trump keeps pushing,
when he wasn't willing to play along with those claims
and told people they needed to put it behind him, well, Trump got mad.
The endorsement got yanked and it went to Katie Britt.
Now, one of the more interesting developments is that Trump loyalists,
his own influencers, are telling their followings to disregard Trump's endorsement
and that they need to vote for Mo Brooks anyway.
They are painting Katie Britt as like a McConnell bot.
And it seems to be having some effect.
We're not talking about minor influencers here.
We're talking about some of the bigger names.
Now, this puts Trump in a position where people are beginning to realize
that his endorsements are motivated mainly by pettiness
or by a desire to endorse somebody that he believes is going to win anyway
so he can ride their coattails.
It's having the overall impact of showing a lot of the Trumpist base
that the emperor has no clothes.
For his part, Brooks said,
this is weird, last time Donald Trump talked about Katie Britt,
he said she was unqualified for the Senate.
Donald Trump is the only man in American politics who could get conned
by Mitch McConnell twice in an Alabama Senate race.
The overall rejection of Trump's endorsements demonstrate his dwindling influence.
The media has put a lot of ink behind the idea that Trump's endorsements really matter.
But in most cases, they don't.
His endorsement follows the lead.
When it comes to those that were important to him, you have to look to Georgia.
And his endorsements didn't matter.
If Mo Brooks pulls this off, Trump is even weaker.
But what's more important is that this particular development is showing the base
that this authoritarian nonsense, this strongman leader,
actually really isn't that effective.
Because it's showing that there isn't an agenda.
It's pettiness.
It's rhetoric.
But there's not an agenda for them to get behind.
One of the things about Trumpism is that Trump is the raw prototype.
The real worry when it comes to the slide into authoritarianism
is the slick, polished version of Trump that comes afterward.
If that base begins to see through it now, we might be able to avoid that polished version
that is typically far more dangerous.
Anyway, it's just a thought.
Y'all have a good day.