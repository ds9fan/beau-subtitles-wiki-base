Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about, well, Trump
and the possibility of him being indicted and then acquitted
because rhetoric is starting to shift on this.
Initially, when all of this came up,
there were a lot of voices saying it doesn't matter.
Nobody's going to hold him accountable anyway.
And believe me, I understand that sentiment.
But as events have unfolded and it appears to become more likely
that an indictment might be on the horizon,
that rhetoric is changing to, well, even if you indict him,
you're not going to convict him.
So you shouldn't indict him.
Because if he's acquitted, well, then he'll be exonerated
and he'll be more of a challenge to defeat in the next election.
I think people should probably remember
that decisions like this are supposed
to be about pursuit of justice.
Is it in the interest of justice?
And does the evidence support it?
Not about what's best in the next election.
It would be important to remember that aspect of it.
But for a lot of people, that may not matter.
I'm also going to suggest that your logic is off.
I don't think that Trump would be more emboldened
by being indicted and then acquitted
than he would by nobody even attempting to hold him
accountable for his actions.
That doesn't add up to me.
Aside from that, I think people are
missing the larger picture when you're looking at it
through the partisan lens.
Look at Texas.
Look at Missouri.
Look at the recent events there.
A whole lot of Republicans have decided
that it is a winning election strategy
to attempt to out-Trump Trump, to be more Trump than Trump
himself.
Right?
What does that mean for the 2024 election?
What message are they being sent if nobody attempts
to seek accountability?
Do you think that they'll be more emboldened
if he's indicted or less?
You're talking about one person, and I
think the logic is off there, but you're
talking about one person who might be emboldened
by being acquitted.
Or you're talking about hundreds of politicians
who want to be Trump who see no accountability.
Stop looking at it through the partisan lens
and look at it as the existential threat
to the republic that it actually is.
There are a lot of establishment voices
that are going to want to protect the institution
of the presidency.
They don't need backup from outside.
There's going to be a lot of voices saying,
hey, just let this go.
They don't need outside assistance on that.
Sometimes you can become so accustomed to a two-tiered
system of justice that you might actually unintentionally
start advocating for it, which is kind of what's happening.
There are people who are saying, yeah, he definitely did this,
but you shouldn't indict him because he's not
going to be found guilty anyway.
I mean, that's a pretty defeatist attitude.
My take on this is the same as it's always been.
It needs to be in pursuit of justice,
and it needs to go where the evidence goes.
That's how this should happen.
As far as the political aspects of it,
it is far more dangerous to have a clear indication
that behavior like this will go completely unpunished,
that nobody's even going to attempt
to hold you accountable, than it would be for an indictment
to end in an acquittal.
I don't think that advocating to just look the other way
on this one because it's best short-term politically,
I don't think that's an opinion that comes from a clear picture
of what could happen for eight or 12 years down the road.
Stop thinking solely about the next election.
There's a whole lot more at stake here.
Anyway, it's just a thought.
Y'all have a good day.