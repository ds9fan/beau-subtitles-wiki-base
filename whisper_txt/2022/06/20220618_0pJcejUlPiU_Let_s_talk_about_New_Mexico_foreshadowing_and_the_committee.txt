Well, howdy there, Internet people.
Let's bow again.
So today we're going to talk about something that happened in New Mexico
and what it has to do with the committee and the midterms and the 2024 election.
Now, the situation in New Mexico, in Otero County, has been resolved, more or
less, but it's foreshadowing for what we can expect down the road, unless the
committee in the Department of Justice provide a clear resolution here. The
Commission in Otero County said they weren't going to certify the election.
They did what I guess some people wanted Pence to do. They weren't going to
certified. Now, legal mechanisms in New Mexico, let's just say they changed their
mind, and at the end of it two of the three voted to certify. The thing is the
the primary election there, there was no evidence of anything, but it's become a
political stance now. It's not even just a conspiracy theory. It's something that
has evolved into a talking point. And we've discussed it on the channel before
how sometimes, especially in relation to foreign policy, something will be said
and it makes sense at the time or maybe it doesn't make sense at the time. But
it's said enough that eventually people believe it's the right move and then
they start expecting politicians to behave in that manner and that's kind of
what happened. It didn't make sense when the claims first came out but it was
repeated so often that politicians caved to that pressure. Now the Supreme Court
there basically did force their hand and they begrudgingly went ahead and
certify it, but we can expect this to play out in the midterms and in the next
election. People wanting to grandstand, some of them who may genuinely believe
the conspiracy theories and some of them just doing it for their own political
ends, but on a wide enough scale that is something that can actually undermine
the United States. This cast a great deal of importance on what's happening
up on the hill, what's happening during these hearings, and why it's important
for Americans to understand what actually occurred in the run-up to the
sixth and how all of those people were tricked because it's still happening.
There are still people around the country who are operating under that
same assumption and to this day there's still no evidence. We're in a
situation where the committee and the DOJ have to act. There's no other way to
look at it. This issue has evolved beyond just Trump. It's now a political
position and it's one that is far more dangerous than I think most people are
giving it credit for. Anyway, it's just a thought. Y'all have a good day.