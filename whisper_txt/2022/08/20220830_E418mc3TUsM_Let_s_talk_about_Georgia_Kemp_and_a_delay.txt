Well, howdy there, internet people. It's Bo again. So today we are going to talk about Governor Kemp, the Governor of
Georgia. We're going to talk about a delay that is probably going to be pretty lengthy, and subpoenas, and the special
grand jury and everything that just happened.
So, if you don't know what was going on, the special grand jury in Georgia that was
looking into the allegations of election interference by Trumpworld was hoping to hear directly
from the governor.
The governor tried to get out of the subpoena.
The judge was like, no, you definitely need to go talk to him.
The judge was worried about the political influence that could happen if Kemp
testified now or anytime leading up to the midterms. And as much as I dislike
the the ruling, it makes sense because the reality is every candidate could
use this, his appearance politically in some way, either to cast themselves as somebody
who would never betray Trump or to show that, you know, Kemp was somehow subpoenaed and
therefore involved, even though that's not really what's going on.
You know, there's a lot to it.
Democratic candidates could use it to suggest, you know, corruption on the part of the Republican
party. There's a whole lot of different ways it could be spun. And yeah, that's true. So the judge
says that the governor has to talk to the special grand jury promptly after midterms. The problem
with this is that after the midterms comes the holidays. It seems unlikely that there will be
some formal resolution from the special grand jury in Georgia before the new year now because
of this.
Now that's assuming that the grand jury absolutely needs Kemp's testimony.
They may not, but if they were really hinging anything on that, you're probably not going
to get any kind of resolution whatsoever, any kind of decision, until after the new
year.
At which point, Trump will probably have made his decision about announcing, and that just
increases the political stakes of the entire investigation.
So in the judge's attempt to reduce the political nature of the investigation within Georgia,
kind of just got punted up to the national stage, is really what's going on.
So anyway, it's just a thought, y'all have a good day.