Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Ukraine and Russia
and a message that the Department of Defense
just sent to Russia.
And I mean the US Department of Defense,
because they named it.
They gave it a name.
Up until this point, the US effort
to assist, train, and arm Ukrainian defenses
has been done piecemeal.
A little bit here, a little bit there.
No centralized structure.
DoD has decided to give it an operation name.
We don't know what the name's going to be yet.
For the purposes of this video, we
will call it Operation Ukrainian Shield.
Let's hope it doesn't turn into Ukrainian Storm.
So what does this mean?
Step one, there will be a one to three star general
who is given command.
And that person will set up command and control
and a logistical network to make things happen.
Up until now, everything's been done
in a haphazard civilian manner.
Now, it will be done in a haphazardly structured
military manner.
People involved in it, they'll be
eligible for ribbons and stuff like that.
But here's the important part.
Why don't ranchers name their livestock?
It's harder to do something once something has a name, right?
Same is true of military operations.
Once that structure is established,
it's easier to do something.
And it's easier to do something in the military operations.
Once that structure is in place, it is in place.
And it will continue to function until that situation resolves
itself.
The message that was sent to Moscow
is that you're not going to be able to just slow roll this.
If you want Ukraine, you're going to have to fight for it.
So where everybody's cheating, Ukraine and Russia,
they're sitting at the table.
The United States just walked up behind Ukraine
and figuratively and literally just like, yeah,
you've got a line of credit.
So Russia now has to decide how badly they actually
want to commit to this.
The United States, and since DOD is doing it,
I'm going to assume that other allied countries are
going to do it as well.
They will structure it.
That's going to clear up a lot of the delays.
When it comes to equipment flowing in that Ukraine needs,
this structure will clear the way for that
and make sure that those delays are kept to a minimum.
So it may not seem like a big deal,
but establishing that bureaucracy,
it being given an operational name, means it's here to stay.
The United States, at least for the time being,
doesn't look like they're going to walk.
They're kind of committing to assist Ukraine
as long as it takes.
Now, what we're going to do once the command and control is
put together, we're going to take another look.
We'll revisit this topic, and we'll
see what they brought in.
Because based on that, we should be
able to get a really clear picture of what
DOD actually expects to happen and how much of the strength
and how much of the strategy is based
on going on the offensive, going on the defensive,
or establishing resistance efforts.
But either way, Moscow has to decide what to do,
because they just kind of indicated
that they were going to basically
raise the size of the military a little bit,
but not really enough to alter much.
Looks like they're just trying to buy time.
The message is, you've got the watch.
Ukraine has the time.
Anyway, it's just a thought. Y'all have a good day.