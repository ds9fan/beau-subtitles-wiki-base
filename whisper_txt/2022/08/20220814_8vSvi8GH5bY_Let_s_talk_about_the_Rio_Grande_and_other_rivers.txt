Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about rivers.
That's a subject that has come up on the channel a lot,
but we've primarily been focused on the Colorado,
even though recently we did bring up Rhine.
We're going to talk about some other rivers,
because that same issue of less water
is occurring in a lot of places.
And I think it might be a good idea to show the scope of it.
Here in the US, on top of the Colorado, the Rio Grande,
that section, Albuquerque, the little northern part there,
that went dry, first time in 40 years.
It went dry.
Now, my understanding is that some rain
brought a little of it back, but it is not in a good way.
Now, on the other side of the Atlantic, you have the Thames.
You know, the one that London, you know,
really important one.
Its head has reportedly moved two to five miles.
The source of it has moved two to five miles
because it dried up.
That seems noteworthy.
They're saying that it's the first time that's happened.
The Euphrates in Iraq, they're having issues with that.
They're going to have to work out a deal with Turkey,
or they will be a country with no rivers.
This isn't localized to the Colorado.
It's not localized to the southwestern United States.
This is a big deal.
We need to start paying attention to this.
This needs to become a campaign issue.
This needs to be something that is on the ballot
because we have to get real action.
This needs to be a kitchen table issue,
as the political strategists like to call it.
For those in the southwestern United States,
it should be already.
You're just starting to get measures.
I don't want to call them solutions.
You're starting to get measures, like in Vegas with the pools.
You're starting to see stuff like that.
They're going to become more and more pronounced.
This should be something you're talking about
if you live there, but they're not solutions.
We need big change, lots of change,
better water management, better planning, better research.
That's going to have to come from the federal level.
This issue isn't going to go away.
It's something that will continue to return,
and every time it comes back, it's
going to get worse and worse.
This needs attention, and we need to start now
because this isn't something that stops on a dime.
This needs pretty immediate action.
Environmental issues need to get on the ballot and fast.
Anyway, it's just a thought.
Y'all have a good day.