Well howdy there, Internet people. It's Beau again.
So today we're going to talk a little bit more about Arizona and the primary there.
See, that last video, that was talking about Republican primary voters.
And the primary went about the way I thought it would.
A whole bunch of mini-Trumps won.
Now it's time to talk about the GOP elephant in the room.
Those are the people that are going to be on the ballot.
Those are the people that the citizens of Arizona as a whole are going to vote on.
On some level, putting up a bunch of Trumpy candidates makes sense.
Because Arizona is Trump country when it comes to the Republican Party.
Right?
I mean, they're pretty loyal to him.
In 2016, he won that state by three and a half points.
But see, then in 2020, he lost.
Trump lost Arizona.
Now the Republican Party is running a bunch of carbon copies of Trump.
That presents an issue for the Republican Party.
It's important to remember that the citizens of Arizona rejected Trump before January 6th.
Before the hearing disclosed everything it's disclosed.
Before Fox News started turning on him.
Before Roe was overturned.
Before the AG there in Arizona debunked the claims from that audit.
If you missed that, one of the big claims that stuck out was they said that 282 deceased
people might have voted.
Well, the AG's office looked into it.
A lot of those people were super surprised to find out they weren't living anymore.
And all was said and done, won.
Won.
A lot of the candidates who just won the Republican primary, they ran on securing the election.
Now they have to spin the appearance that they were duped, that they were tricked, that
they couldn't see through that.
The Republican Party has a problem in Arizona.
It's worth noting when the people of Arizona decided that Trump was a loser, it was the
first time that state had gone to a Democratic candidate for president since 1996.
Before then, 1948, Trump drove negative voter turnout.
It's possible that many Trumps, carbon copy Trumps, will do the same thing.
People showing up not to vote for any particular candidate, but to vote against Trumpism.
We've talked about it for a while.
The Republican Party has put itself in a position where you can't win a primary without Trump,
but you can't win a general with him.
Anyway, it's just a thought. Have a good day.