Well, howdy there, internet people. It's Beau again.
So today we are going to talk about Trump and the tax man.
We're gonna talk about the other Trump case.
No, not the other one in Georgia
and not the other one in DC, the other one in New York.
This case is against Trump Organization
and the CFO, Allen Weisselberg.
Allen Weisselberg.
It's a tax case and I wanna say it's over about 1.7 mil,
something like that.
The Trump team tried to get this all dismissed.
The judge heard the arguments
about it being politically charged,
you know, the DA's a Democrat, all of this stuff,
and was basically like, yeah, it went to the grand jury.
So jury selection begins October 24th and scheduled it.
These charges, some of them are pretty significant.
There's one where the sentence range
appears to be five to 15 years.
Keep in mind, Weisselberg's like 75 years old.
Now, cases like this, tax cases,
they tend to drag on and on.
A trial can take months
because there's a lot of educating the jury
as the case proceeds.
They can take a really long time.
It is worth noting from a political standpoint
that if jury selection begins October 24th,
it's a safe bet that the trial will be going on
during the midterms,
assuming that this schedule is adhered to.
I don't know how much significance it's gonna have,
if any, because Trump himself is not a defendant in this.
But this is yet another case.
It's another legal worry for Trump world.
There are cases springing up,
there are investigations springing up,
there are searches springing up,
people inside his circle
are just constantly being hit now.
This may seem to be one of the least significant
to the American people.
However, just remember, when cases go forward,
they're not always what you think they would be for.
I think most Americans,
when they think of accountability and Trump,
they're looking for something related to the sixth.
That may not be the way it shakes out.
There is this case and there's the document case,
which is, that appears to be a very strong case.
So it's possible that it ends up being taxes and documents
that actually really starts to chip away
at Trump's Teflon coating.
Anyway, it's just a thought. Y'all have a good day.