Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about some news out of Louisville
and out of the Department of Justice.
And it's news that I am sure a lot of people
have been waiting on.
A lot of people have probably given up hope
that they were ever going to hear this news.
But today, the Department of Justice
announced that four former or current cops there
in Louisville have been charged.
The charges stem from the death of Breonna Tyler
back in March of 2020, I think.
The charges are civil rights offenses, obstruction, use
of force and unlawful conspiracy.
Now, it's kind of hard to pin it down at the moment
because it's over a couple of indictments
and an additional information.
But it's pretty comprehensive.
The federal theory, the allegation,
is that they falsified, that some of them
falsified, the initial search warrant.
From that point forward, everything is bad.
This probe began in, I want to say, April of 2021.
We're just getting the results of it now.
This is how the Garland Justice Department works.
You don't know it's coming.
I'm willing to bet that there are people who watch this channel,
who watch channels that are very plugged in to what's happening in the Justice Department.
As far as I know, there weren't a lot of hints that this was coming.
Now anytime something like this comes up, the question is, okay, they were charged,
are they going to be convicted?
The feds are different than the state.
They have a much higher rate of gaining convictions.
I would also suggest that if the feds have half what they kind
of suggested they have, you might even see people
plea and not go to trial.
Generally speaking, when the feds are like, oh, yeah,
y'all had a conversation in a garage,
that's a really bad sign for the defense.
And that happened here.
Now, the other thing to note, and the other thing
that's important is that there is a separate ongoing probe
into the department as a whole, looking
into patterns of racial discrimination
and excessive force.
This probe has been going on a really, really long time.
Generally, if they don't find anything, the probe stops.
It, to my understanding, has not stopped.
So I would expect more.
Anyway, it's just a thought.
y'all have a good day.