Well, howdy there, internet people. It's Beau again.
So today we are going to talk about Republicans
becoming Democrats in Colorado
because a state senator there just did that, Kevin Priola.
Now, there are a lot of questions about this.
Why did it happen? Is it a trend?
We're gonna kind of go through all of them.
So as far as why he did it, I mean,
the easiest thing to do is look at his stated reasons,
which basically has to do with the denying of election results
and the violent rhetoric.
And apparently that's just become a bridge too far for him.
That's the stated reasons.
Another reason is that he might have realized
that you can't win a primary without Trump.
You can't win a general with him.
He might have come to the conclusion
that it might be a whole lot easier to win in Colorado
as a conservative Democrat rather than a moderate Republican.
So that may be the angle he's playing.
Now, is he super right-wing?
No, not really.
He's generally been bipartisan.
And one thing that he explicitly said
is that basically there's too much at stake
for the Republicans to be in control.
And that's a paraphrase, but not by much.
He cited climate change denial as one of his reasons
for changing as well.
Does that mean that he's in line
with the Democratic Party platform?
No, no, no.
He is a pro-life, pro-Second Amendment candidate
and has been for a long time.
I don't see those positions changing simply
because he changed parties.
So does this matter in Colorado?
Yes.
It just became a lot harder for the Republican Party
to take control of the state Senate there.
One seat doesn't seem like a lot,
but when there's already a gap, that one seat,
that makes it even harder.
Now, is this the start of a wider trend?
I mean, it depends on what you mean by trend.
If you're talking about, are there
going to be a few other candidates who maybe switch
parties?
Yeah, because legitimately it will be easier
to win as a conservative Democrat
than a moderate Republican in a lot of places.
So I can see that there's a lot of potential
and I can see that happening.
Is this a sign that the Republican Party is actually
starting to fall apart?
Not yet, no.
I don't see a whole lot of this happening,
especially on the national stage.
You might see it a couple of times,
but I don't see this becoming a widespread trend.
I think it might be more likely for people to maybe leave
office and then come back later as a member of the other party.
I don't see them switching.
That's a unique thing.
Obviously, Republicans are super mad about this
and throwing out some language that's entertaining,
threatening a recall, and all of that stuff.
Good luck with all that.
So it's happening.
You may see it a little bit more,
but I don't think it's going to be a widespread trend.
Just because somebody changes parties
doesn't mean they change positions.
It's political affiliation, not policy decisions.
I don't expect some of his more controversial views,
as far as the Democratic Party is concerned,
I don't expect those to change any time soon.
He may view that as a way to appeal to independents
and show that he's not beholden to any party platform,
because I want to say his new district is like 25% Republican,
25% Democrat, and everybody else is unaffiliated.
Probably gives him a lot of room.
So there are legitimate reasons for somebody to do this,
but it's politics, so you always have to look at
the political considerations for that as well
when somebody switches sides like this.
So anyway, it's just a thought. Y'all have a good day.