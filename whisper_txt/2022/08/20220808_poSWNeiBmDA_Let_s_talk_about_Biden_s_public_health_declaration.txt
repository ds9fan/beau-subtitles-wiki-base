Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
the Biden administration's public health declaration.
If you don't know, the Biden administration
declared a public health emergency.
What a public health emergency does,
what this declaration does,
is that it allows access to emergency funding.
It allows accelerated production of vaccines.
And it makes it easier to collect and share data.
Now, the declaration was made in relation to monkeypox.
In the United States,
there are currently around 6,600 confirmed cases.
There are assuredly more than that.
Those are the confirmed cases.
Now, the states of California, New York, and Illinois,
they have issued their own state-level declaration.
The World Health Organization has called it
a public health emergency of international concern.
So there has been a lot of response
when it comes to the declarations
and the legal mechanisms to get the ball rolling,
to get resources.
Okay?
Now, one thing that I have noticed
is that the messaging on this
seems very targeted to a specific demographic.
Personally, I have learned a lot since the 1980s,
and I feel like I've already seen this movie.
So this is what everyone should know
to protect themselves and those they care about.
First, no skin-to-skin contact with people
who have a rash that looks like monkeypox.
That's a quote from the CDC website.
One thing that I do want to point out
is that the image you see in all of the articles,
that is the typical appearance of the rash.
If you look, there's a bunch of atypical variations.
And after looking at all of them,
I wouldn't be able to pick one out of a lineup.
So be aware of that.
Avoid kissing or more with people who have the rash.
You also don't want to share bedding, utensils,
cups, clothing, towels,
anything that might be used that might get fluid on it.
You want to wash your hands often with soap and water
or alcohol-based sanitizer.
There is a belief that it can be spread
through respiratory secretions.
And it also notes that it's still being researched
whether or not it can be spread
by people who aren't exhibiting symptoms.
All of that is from the CDC website.
So those are your steps to protect yourself.
I know that a lot of the messaging
is going out to a specific demographic.
I feel like we should know by now
that these things don't stay confined.
They rarely do that.
So I don't think it's a good idea to think,
oh, this isn't my problem.
I don't need to know anything about it
if you're not in that demographic.
So be aware of this.
And when we find out more about vaccine rollout
and stuff like that, I'll let y'all know.
Anyway, it's just a thought.
Y'all have a good day.