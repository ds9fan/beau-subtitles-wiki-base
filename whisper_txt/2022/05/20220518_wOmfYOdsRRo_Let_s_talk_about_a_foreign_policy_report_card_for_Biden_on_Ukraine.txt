Well, howdy there, Internet people.
It's Beau again.
So today we're going to be talking about a Biden foreign policy report card.
We're going to do this because I had somebody send me a message saying,
hey, you know, under Trump, you would critique pretty much every foreign policy decision
and we haven't seen you do that with Ukraine. Are your biases showing?
No, no, that's definitely not it.
I haven't done it because I actually don't like cheerleading for Biden.
He's not my guy. I've said that repeatedly.
He is nowhere near progressive enough for me.
He's very status quo. He isn't somebody that I like cheering for.
So that's why I haven't done it.
But you want one? Okay. So we'll talk about Ukraine.
What was the situation he was presented with? A nuclear-armed,
presumably near-peer country that invaded a non-aligned country,
a country the United States did not have an obligation to protect.
But it presented a foreign policy opportunity, right?
And what happened? Biden led a charge to unify behind Ukraine.
In the process, undid all of the damage that Trump did to NATO during his administration,
reunited it, and strengthened it. Galvanized a coalition to fund and arm Ukraine,
giving Russia the biggest black eye it's had since the Soviet Union fell apart.
It does appear that we're going to see a Ukrainian victory.
He got Americans to cheer and the world to unite around the idea of economically isolating a country
that at the beginning of his administration was seen as a near-peer competitor.
It was going to be a three-way, multipolar, near-peer contest.
Remember, Russia, China, the United States.
And he did all of this without directly involving the United States in a war.
What more do you want from him? I mean, I have my critiques.
I have issues with the speed at which some of this was done.
I don't think that the piecemeal delivery of equipment was the right way to go about it.
I understand it from a foreign policy perspective,
but I think that it probably would have saved a lot of lives had it just been a total flood.
But we'll never know. That's my biggest critique, that he didn't do what he did faster.
Now, I do want to be super clear about one thing.
This isn't actually Biden. Biden isn't making these decisions.
When he took office, we talked about the foreign policy team that he brought on.
It's an all-star team. They're the people who made these decisions.
They're the people who put this into action.
But you have to give the Biden administration credit for bringing all of those people together.
He set up a second State Department before he ever took office,
so he would be prepared for stuff like this. At the end of this, what do you have?
When he took office, the high-stakes international poker game where everybody's cheating,
the big table, the no-limit table, was going to be China, Russia,
and the United States sitting at that table.
On the first hand, the Biden administration sent Russia back down to a lower-stakes table.
I don't necessarily like it because I have issues with how American foreign policy works,
but when you're talking about what American foreign policy currently is,
it really couldn't have gone better for the U.S.
And a lot of that has to do with the people the Biden administration brought on early on.
They were ready for it.
Anyway, it's just a thought. Y'all have a good day.