Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the Republican primary
in Georgia, specifically the governor's race
and the results.
Now I'm gonna go ahead and be honest,
voting hasn't ended yet.
Okay, voting hasn't started.
It's a couple of days ago by the time y'all watch this.
But those polls, wow.
So if I'm wrong about this,
feel free to go ahead and Photoshop my face
onto that Dewey defeats Truman photo.
But I'm going to assume that Kemp has won.
Now it gets interesting though.
I mean, it's funny, yeah, Trump's candidate lost,
probably going off the polls by a pretty wide margin.
So what happens next for Trump?
What's he gonna do when it comes to Georgia?
There's an interesting dynamic that's being set up
throughout the GOP right now.
There's Trump candidates
and then there's like normal Republicans.
Those are who's vying for control in these primaries.
In states where Trump's candidate loses,
what does he do?
Does he not show up to help the candidate
who in this case totally embarrassed him, Kemp?
Does he just write off the Republican Party
in states where his candidate doesn't win?
And if so, what does that do for Republican voter turnout
in the general elections?
Trump's combative nature combined with his
just historic levels of pettiness
leads me to believe that in states
where his candidate doesn't win,
he won't back the Republican Party.
In fact, he may actually talk bad about the people
who beat his candidates.
Talk bad about the people who beat his candidates
or he'll say nothing
and drive down Republican voter turnout in those states.
I mean, Kemp is clearly at this point
when I'm filming, clearly the forecasted winner.
But without an energized Republican base,
are they gonna show up when he squares off against Abrams?
That is something that is probably weighing pretty heavily
on the minds of normal Republicans
because the MAGA faction with their level of pettiness,
there's no other word, the vindictive nature of this group,
they may just stay home
and it may cost Republicans elections around the country
all because they still refuse to do the one thing
they have to do with Trump, which is get him out of the party.
Anyway, it's just a thought. Y'all have a good day.