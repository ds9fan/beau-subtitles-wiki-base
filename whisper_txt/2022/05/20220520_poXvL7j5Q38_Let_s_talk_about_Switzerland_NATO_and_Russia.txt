Well, howdy there, internet people, Lidsbo again.
So today, we're going to talk about the stuff of legends.
We're going to talk about something
that I don't think many people really would have considered,
something that is of mythical proportions,
for lack of a better term.
Due to Russia's invasion of Ukraine,
we have talked a lot about non-aligned countries,
and countries that were traditionally neutral, Finland,
Sweden. But we haven't talked about just the gold standard of neutrality. Switzerland.
It hasn't really come up because I mean it's Switzerland. It's not like they
would ever choose a side. And if they were to ever be in a situation where
they did choose a side, I mean that would say a whole lot about the actions
that led them to that point. We're talking about a country that didn't join
the United Nations until 2002. It's fiercely independent. It hasn't had an
international war in two centuries. It managed to stay out of World War I and
World War II. They discussed NATO membership. Because of Russia's actions,
they discussed NATO membership. If you want to know whether or not Russia's
actions were in any way acceptable, were in any way justified, that's a
pretty strong signal right there. A country that has stayed out of every war
for 200 years discussed NATO membership and currently the Ministry of Defense in
Switzerland is drawing together options and coming up with a plan on how to
cooperate with NATO but not officially join it because the neutrality is
actually enshrined in their Constitution. So they're talking about how they can
backfill munitions. So if Poland sends Ukraine 500,000 rounds of ammunition,
well Switzerland can then send Poland 500,000 rounds of ammunition. So they're
not actively participating, but they're increasing the ability of other
countries to help. They're talking about joint exercises, regular leadership
meetings between their brass and NATO brass. That's uh, I mean, that's remarkable.
That's wild when you really think about it. There is an online debate that exists over
whether or not Russia's actions were in some way justified. A country with a 200-year
commitment to neutrality is reviewing it because of Russian aggression. They're
looking for ways to come right up to the edge of what they're allowed to do by
their own Constitution. When it comes to the international poker table where
everybody's sitting around, the entrance of Switzerland and them just watching
the game and maybe loaning a player a little bit of money, that's a huge deal.
That is a huge deal.
The argument over any justification for this war is over.
There is no justification for the Russian aggression, none.
It doesn't exist.
It's manufactured.
It is so offensive that Switzerland considered joining NATO so it could participate in a
response.
Anyway, it's just a thought.
y'all have a good day.