Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about the 80s and Grenada
and a bizarre little historical quirk
in the way the United States remembers history.
In a recent video, I made an offhand comment
about there being a military alliance in the Caribbean.
And it prompted way more questions
than I ever would have imagined, people
asking what it was, why didn't it stop the U.S. invasion of Grenada, so on and so forth.
So we're going to kind of go through that.
We're going to talk about the regional security system, that's the Alliance, and we're going
to talk about Grenada because in the U.S. the perception of Grenada, the collective
memory of what happened there is widely shaped by a movie called Heartbreak Ridge.
Why did the US invade Grenada?
Right now, you're probably saying medical students, because that's what most of the
country believes, and that's based from that movie.
The Marines go ashore, rescue the students, fight troops from Grenada, and I want to say
some from Cuba, and that's the end of it.
That's not a good summary of what occurred.
So there was a, I don't want to call it a coup, because I legitimately believe that
large portions of what happened there was an actual accident.
They didn't mean for it to.
So let's say there was a forcible change of government in the country, and some of the
leadership did not make it, that didn't survive that change of leadership.
So the Organization of Eastern Caribbean States reached out to the United States, was like,
hey, we could use some help with this.
I mean, imagine if you were the leadership of another island nation and you saw that
happen.
You might want to make sure that it doesn't happen to you.
It is worth noting that there are a lot of people who believe that US intelligence kind
of had this organization ask the US for help to give the US legal cover to intervene.
I have seen no evidence to suggest that's true.
That being said, if the evidence surfaced tomorrow, I would not be surprised.
That's totally plausible.
Okay, so, they ask for help.
The US says yes, why?
Partially because the administration is just absolutely terrified of having another situation
develop similar to what had happened in Iran with a bunch of Americans getting captured.
On top of that, that week, I think, the US had gotten a pretty big black eye in Beirut.
This allowed Reagan to change the story, change the news cycle.
So the US invades.
The operation is spearheaded by US Army Special Operations and the SILs.
The Marines come ashore shortly thereafter.
And this is where it gets kind of bizarre for a lot of Americans.
Within a few hours, there are troops from Barbados, Jamaica, Antigua, Dominica, St.
Lucia, St. Vincent, St. Kitts, and Nevis.
The RSS didn't stop the U.S. invasion of Grenada because they participated in it.
It's not in the collective memory of the United States.
It's not how we remember this, but there was a coalition that went in.
Now the U.S. troops were gone by December of 83.
The Caribbean troops stayed until 85.
Another thing that gets kind of overlooked is that it wasn't just troops from Grenada
and Cuba that were there.
There were also Soviet troops, I want to say some from Libya, East Germany, maybe even
a few from Bulgaria.
Now not all of them got involved in the fighting, but they were all on the opposition side.
And in another bizarre twist, because the operation went relatively smoothly, I don't
know that the Caribbean forces that showed up, I don't know that they ever actually
got involved in the combat.
So that's kind of an overview of Operation Urgent Fury.
That's what it was called, the invasion of Grenada.
Now, the obvious question here with everything that we've been talking about this week,
was this US imperialism?
I mean, there were a lot of legitimate reasons for it, right?
At the same time, I am fairly certain that if there were recordings of the conversations
that took place during the decision-making, making sure that we didn't end up with another
Cuba down there, another communist island country, that factored into the math.
That had something to do with the equation, which means it was about maintaining a sphere
of influence, which would make an imperialism.
Maybe slightly more justified, but it doesn't change the fact that that definitely had something
to do with it. The regional security system, by the way, that alliance, was explicitly
anti-communist. In fact, there are some who, much like the earlier statement, who believe
US intelligence actually encouraged them to form this alliance to kind of stop domino
theory-type stuff from occurring. Again, I don't have any evidence to back that up,
but yeah, that probably happened. So there's an overview of it and how it went down, and it's
just interesting to note the way the United States remembers that war because of a movie. Anyway,
It's just a thought. Y'all have a good day.