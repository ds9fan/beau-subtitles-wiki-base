Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about power lines.
We're gonna talk about power lines
and why a certain power line that's going up
is much cooler than most others
and is a really good sign for the future
for a really weird, cynical reason.
Okay, the Biden administration has approved
something called the Energy Gateway South Transmission Line.
What is it?
It's a 416 mile power line.
Starts in Wyoming, goes through Colorado, it ends in Utah.
It will bring 2000 megawatts of renewable energy, wind,
into an existing power grid to increase reliability.
This is paving the way to get 25 gigawatts of clean energy
off of public lands by 2025.
So it's bringing in renewable energy
and good paying union jobs.
Construction begins on this in June
and there is a gateway west
that will hopefully start construction in August
and both will be completed in 2024
as long as they stay on schedule.
So why is this particular power line different?
I mean, other than it bringing in green energy.
Who do you think is gonna make money off of it?
Warren Buffett's Pacific Corp.
That's a good sign in a very, very cynical way.
Big money is now behind green energy.
So Pacific Corp's plan,
they will have 2000 miles of new transmission line
going in over the next few years
and they're bringing on clean energy
and they're dumping dirty energy.
They will be shutting down 22 coal plants
and replacing it with all renewable stuff.
The big money, the people that have influence in DC
are going green.
Something like this,
when you have people with this much money
get behind stuff like this,
it tends to move things a lot faster
because they have those power coupons.
They can call up senators and get stuff done.
It's weird because when you normally think
about these companies,
you generally don't have a good feeling,
but it has become profitable now.
So now that clean energy is becoming more profitable,
you're going to get the big money behind it,
which means they're going to accelerate the switch
because they're going to make money on it.
It's great for the environment.
I'm sure that's not why they're doing it,
but it's great for the environment.
This is one of those things where
I don't really want to cheer for a billionaire,
but it's something that needs to be done.
And these are the people that have the resources to do it.
They're going to make a dump truck full of cash on it,
I'm sure, but we're finally getting movement
in the direction we need to.
Anyway, it's just a thought. Y'all have a good day.