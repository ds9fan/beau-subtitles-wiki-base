Well howdy there internet people, it's Beau again. So today we are going to talk about
Colorado and the Mesa County clerk there, Tina Peters, who is known for appearing with the pillow
guy and is known for repeating Trump's, well, less than accurate claims about the election,
who is also under investigation for alleged election security breaches,
who was also barred by a judge from overseeing the election in her county,
who was also arrested last week for, on an obstruction charge,
who just announced her candidacy for Secretary of State of all of Colorado,
putting the entire state's election under her control if she were to win.
It's worth noting that she made this announcement on Bannon's podcast.
That's quite the resume and this is a symptom of something that is occurring all over the country.
There are people who have echoed Trump's claims about the election, running to control the elections
in Georgia, Michigan, Nevada, and Arizona. You can safely assume that this is going to happen
in any swing state, that they will try to get loyalists into those positions.
Now the current Secretary of State in Colorado released a statement about this development.
It says, Tina Peters is unfit to be Secretary of State and a danger to Colorado elections.
Peters compromised voting equipment to try to prove conspiracies, costing Mesa County taxpayers
nearly $1 million. She works with election deniers, spreads lies about the elections,
was removed from overseeing the 2021 Mesa County election, and is under criminal investigation by
a grand jury. Colorado needs a Secretary of State to be the Secretary of State.
Colorado needs a Secretary of State who will uphold the will of the people, not one who embraces conspiracies
and risks Coloradans' right to vote. That is from Jenna Griswold, current Secretary of State there.
Normally the Secretary of State position is one that it's not really hotly contested.
It isn't viewed as a must-win election. The current strategy of Trumpism requires
those elections be won. If you are in a swing state, really if you're in any state,
if you've never paid attention to this election before, this position now warrants your attention.
Making sure that Trump loyalists do not control the elections at the state level is an imperative.
It is something that will help defeat Trumpism as a whole, because since they know they're not going to win
at the ballot under conventional circumstances, they may attempt to find a way to alter those outcomes.
Anyway, it's just a thought. Y'all have a good day.