Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about fear and thirst
in Las Vegas, the other Las Vegas, the one in New Mexico,
not the one in Nevada.
This is a city of 10,000 to 15,000 people,
something like that.
And they have a little less than three weeks of water left.
OK, so what happened?
There's a drought.
There's a drought.
And then there's a fire.
The fire charred, I want to say, 340,000 acres,
something like that, big fire.
Then there's a monsoon, which is good because of the drought.
However, have you ever put out a campfire with water?
That gray sludge that's created, same thing happened.
That flowed heavily into one of the city's two reservoirs,
rendering one just apparently completely useless.
It can't filter it.
So this limited their water supply heavily.
They've been trying to conserve and acting measures even
beyond the normal drought restrictions.
But that only goes so far.
Now, on top of the issue they have with being down a reservoir
and it being unable to filter this water,
there's also the issue of when you're
trying to disinfect water, you use a lot of chlorine.
Chlorine and carbon, in high quantities,
mixing chlorine with carbon, tends
to create a carcinogenic byproduct, which,
generally speaking, is not something
you want in your water.
The city has a unique plan as a temporary measure.
There's a nearby lake, and they are
trying to come up with the right balance
to disinfect that water without creating a cancer-causing
byproduct in the water.
They're doing those tests over the next couple of days,
but there's no guarantee that they'll be able to get it.
And this is just the beginning of this measure.
If they can figure out how to do it,
then they have to work out the logistics for distributing it
and getting it where it needs to be.
The solution here, as is the case
in a whole lot of places in the United States,
is upgraded infrastructure, particularly with water.
This is going to be an ongoing problem in the United States,
and it's going to show up in a bunch of weird ways.
The infrastructure in the US, when
it comes to distributing water, it isn't up to the task.
So anything that stresses it will
cause it to fail, which is what's happening here.
If they can make this work with the lake,
it'll buy them a couple of months.
And hopefully, during that period,
they can come up with some more permanent solution.
If this doesn't work, it's probably
going to end with the National Guard trucking in water
from somewhere.
When you hear politicians push back
on infrastructure spending, this is the result.
This is what happens.
There is a lot of basic infrastructure
in the United States that is crumbling.
Any stress will cause it to fail.
We're seeing it all over the country.
And it's probably time for citizens
to understand that sometimes the biggest hurdle to overcome
are the local politicians or the state-level politicians who
don't want to invest in the communities.
Anyway, it's just a thought.
Have a good day.