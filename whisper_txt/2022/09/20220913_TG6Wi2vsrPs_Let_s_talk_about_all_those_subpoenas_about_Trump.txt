Well, howdy there, internet people.
It's Bo again.
So today we're gonna talk about
what the Department of Justice was up to last week
because apparently they had a busy week.
And depending on the reporting,
it deals with 30 or 40 people, somewhere in that range.
So we will talk about what we know and what we don't know
about the 30 to 40 subpoenas
that the Department of Justice sent out.
What we know is that the recipients range
from high profile people to staffers
you never have heard of.
The subpoenas themselves are requesting information
that is pretty broad in scope,
dealing with the post-election behavior
from the Trump administration.
And the subpoenas that have been described
are long enough to be divided into sections and subsections,
requesting information about fundraising activities,
the alternate electors,
delaying the election certification,
the rally, so on and so forth.
We know that at least some of them
compelled the recipient to appear before the grand jury
on September 23rd.
We know that a couple of people, at least two,
had their phones taken so they could be searched.
The subpoenas requested copies of communications
between the recipient of the subpoena
and the Trump allies you would expect,
Giuliani, Powell, Eastman, people like that.
To me, the most interesting part is the suggestion
that the recipient needed to provide any information
they had about members of the executive
or legislative branches that helped kind of plan
anything that might obstruct, influence,
impede, or delay the certification
of the presidential election.
I have a feeling like a whole bunch of Senate staffers
just got super nervous.
And they probably have good reason to.
So why are they casting such a wide net?
Because it's broad.
There's a lot of moving parts.
And the grand jury that is tasked with looking into this,
it seems like every time they think
they've gotten to the bottom of it,
there's a sort of a,
there's a seditious underground parking garage
that they need to go through next.
So it just keeps expanding.
Some of these people, they're staffers,
and they are gonna have access to a lot more information
than people are gonna give them credit for.
The help, as we saw during the hearings,
they often have the best notes
because they're the people that actually run things.
So we can expect there to be
probably some pretty detailed information
that comes out of this.
Taking the phones, that's pretty significant.
A lot of times that's reserved for
incidents when the Department of Justice believes
the person wouldn't provide it voluntarily.
So it's big news, but it's another one of those steps.
This takes us to the next level
where they are kind of building a case
that appears now to be actively targeting
not just people that are seen as Trump's inner circle,
but possibly other political figures
who may have assisted in attempting to
produce a different outcome to the election
than what the votes said.
So that's where we're at.
We're gonna have to wait.
There will probably be more information
flowing out pretty regularly
between now and September 23rd.
Anyway, it's just a thought.
I hope you all have a good day.