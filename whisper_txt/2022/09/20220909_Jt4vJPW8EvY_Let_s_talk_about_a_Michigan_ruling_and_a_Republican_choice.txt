Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about a ruling in Michigan
and how that ruling combined with a promise
from the Attorney General in Michigan
has led to the ball being in the Grand Ole Party's court.
The Republican Party has a choice to make in Michigan.
We're about to see if they are OK with putting their face
on the decision to strip half the state of its rights
or if they're just going to hide behind the Supreme Court.
A judge in Michigan has ruled that the 1931 law prohibiting
abortion forces a pregnant woman to forego
her reproductive choice and to instead serve
as an involuntary vessel entitled to no more respect
than other forms of collectively owned property.
And it prevents her from determining
the shape of her present and future life.
Basically, the judge said, hey, this
violates due process and equal protection.
The Attorney General in the state
has promised not to appeal it.
So that law being struck down is going to stand.
The governor of the state is asking the Supreme Court
to step in and rule on a lawsuit that the governor filed
trying to protect the right to engage in family
planning in the state.
And the Supreme Court is also expected
to rule, the Supreme Court of Michigan,
is also expected to rule on a constitutional amendment
about whether or not that can be on the ballot.
And that's supposed to happen this week.
So now that the courts in Michigan
have made their decision, it's up to the GOP.
Politicians, Republican politicians in the state
can appeal.
They can fight.
But then they have to put their names on it.
And they have to say, yes, we are
going to fight in court to take away half the state's rights.
One of the things that the Republican Party kind of
counted on was being insulated from this decision
because, well, it was something the Supreme Court did.
So it's not going to impact their re-election chances.
That's not the way it's shaping up.
And you're seeing a lot of Republican politicians
start to walk back their rhetoric on this topic.
And we're going to see how far they're
willing to go, whether or not all of that rage
that they manufactured, all of that anger,
all of that kicking down at people,
whether or not they're willing to do it themselves
or they just want their constituents to do it.
Anyway, it's just a thought.
Y'all have a good day.