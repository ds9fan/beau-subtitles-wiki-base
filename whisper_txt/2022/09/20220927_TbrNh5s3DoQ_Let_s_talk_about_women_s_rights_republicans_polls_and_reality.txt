Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about women
and rights and freedoms and polling
and what polling can teach us about objective reality
and where to focus your efforts.
CBS News conducted a poll.
And there were a couple of interesting pieces in it.
Two of them we're going to focus on right now.
One of the questions was, what's at stake in the midterms?
And 68% of people said your rights and freedoms.
And I think that that's probably a very accurate depiction.
Then there was another question.
And it said, if Republicans control Congress,
women will have.
And then it had three options, fewer rights and freedoms,
more rights and freedoms, or things won't change.
43% said fewer rights and freedoms.
39% said no change.
And 18% said more rights and freedoms.
So this is why, when we talk about people both sides
and things that don't have two issues,
this is one of those moments.
This 18% of people, they have chosen, one way or another,
to disregard reality.
You are never going to reach these people.
This 18% of people, they have literally,
I reject that reality and substitute my own.
If you are a Republican and you believe
that women will have more rights and freedoms,
I will have a pinned comment down below on YouTube.
Please list them.
I can think of a couple of things
that you might be able to try to frame that way.
But there's no way that those small examples would
overshadow everything else.
These people have just rejected reality.
But because it makes up 18%, people
will try to both sides this.
You'll see it in the media because of this poll, probably.
But what does this tell the Democratic Party?
While 68% of people believe that rights and freedoms
are at stake, only 43% believe that women
will have fewer rights and freedoms
under a Republican-controlled Congress.
That 39% that think things won't change,
that should be the group of people
that the Democratic Party is reaching out to and saying,
hey, yeah, there's the obvious family planning thing
that everybody's talking about.
But then you also have normal birth control.
You have family planning designed to increase births.
You have candidates for the Republican Party
suggesting that allowing women to vote was a mistake.
The general culture tone and the rhetoric
supports the furtherance of a patriarchal society.
That 39% of people can be reached.
And that's where the Democratic Party
needs to be focusing their efforts.
There is no objective reality in which
a Republican-controlled Congress leads to more rights
and freedoms for women.
That's not a thing.
There's nothing in their platforms
that suggests that's even something they want to attempt.
In fact, most of the rhetoric,
most of their overtly stated positions
suggest they want to limit women's rights and freedoms,
with the possible exception of having the right and freedom
to do what you're told to by some mediocre man
who thinks that by the virtue of his birth,
he should have a say-so.
Polls like this, where they actually ask questions
that relate to objective reality,
can be really important and should guide policy
and should guide Democratic messaging.
39% of people are up for grabs.
Anyway, it's just a thought.
Y'all have a good day.