Well, howdy there internet people. It's Beau again. So today we are going to talk about
Greenland and ice and sea levels and how some things are just inevitable. They are going
to occur no matter what actions we take. Because there's a really interesting study and it
had a cool name in the concept so it's gotten a lot of attention. And it's about ice that
is in Greenland that is basically cut off from the process that keeps it ice. Even though
right now it's still ice, there's more than 100 trillion tons of ice that won't stay ice.
This is what they are calling zombie ice. This study was designed to provide a conservative
estimate of how much sea level rise we will see just from the ice in Greenland. And the
answer is 10 inches. 10 inches no matter what. No matter what actions are taken right now
today, no matter how we cut our emissions right now, we're going to see this. Now there's
no timeline for it, but this is going to definitely occur. And the reason it's going to occur
is because this area has risen in average temperature, I want to say 1.5 degrees per
decade for like the last 40 years. This has already been set in motion. And when you look
at it and you compare it to Noah's estimate as an example, Noah is saying 10 to 12 inches
over the next 30 years. We're going to see it. It's going to occur. Now one thing that
is really important when it comes to talking about sea level rise is understanding that
10 inches doesn't mean 10 inches. You can't walk out to the coast where you're at and
say okay in 30 years it'll be a foot higher than this because that's not how it works.
In some areas if it goes up by a foot, well at 6 inches it's cresting over into a new
area that opens up a wider area that will be flooded. So that area may become much wider,
may become a bay. And somewhere on the other side of the world you may even see water levels
recede. Now one thing that's important to note about this study is that it's new. There
hasn't been a lot of debate about it. But that's not why I'm bringing it up. The exact
prediction of 10 inches no matter what, that's going to be debated, no doubt. But the reason
I think this is notable is because it's talking about the inevitability of some of this. Because
you know we hear stop climate change, avoid climate change. That's not happening anymore.
We are past that point. We're at mitigate climate change. We're trying to lessen the
effects not avoid it altogether because that's not possible anymore. And the longer we wait
the more effects are going to be locked in. The more effects are going to become inevitable
because this stuff doesn't happen immediately. It takes time for the effects to become apparent.
And that's one of the reasons I think that this is important to note. This study is important
to note and studies in a similar fashion. Because it's talking about stuff that we can
see it. We see that train coming but we also know it doesn't have any brakes. This ice
is still there at the moment but it is going to melt. And it is going to raise our sea
levels. That's what makes this notable. And I think that if more research is conducted
in this fashion, there are more studies and those studies and that research gets publicity,
we might be able to encourage more people to join the fight to mitigate it. Anyway,
that's just a thought. Y'all have a good day.