Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Red Wolf Week
and red wolves in general and reintroduction
and what you can do if you want to.
And some of y'all already have
and also why you can't stop once you start winning.
Okay.
So the red wolf at one point in time
it was all over the Southeast.
It was everywhere.
But by around 1970, it was in real trouble, real trouble.
It basically existed in little pockets
in Louisiana and Texas.
So Fish and Wildlife got this novel idea
and they decided to step in.
And they started catching them
with the very bizarre idea at the time
of putting them into a breeding program
to eventually reintroduce them to the wild.
And the red wolf became functionally extinct in the wild.
Then in 1987, if I'm not mistaken, 35 years ago today,
wolves started being re-released,
reintroduced in North Carolina.
And it was so successful that it became
a model for other reintroduction efforts.
Think Yellowstone.
But Fish and Wildlife was like, hey, we did it.
And then they just stopped.
They stopped protecting the animal.
So numbers started to decline in large part due to poaching.
You know, a whole bunch of guys
who want to show how tough they are
by taking a wolf from 100 yards away with a scoped rifle.
Real tough guys.
Anyway, they were able to get the wolf
and then they took it and put it in a breeding program.
Real tough guys.
Anyway, so the numbers started to decline
and then legal battles started to ensue.
And eventually, the federal courts
kind of told Fish and Wildlife that they
had to get back in the game.
And they have.
And they have.
And now we are fighting to bring this animal back
to the wild.
Now, fun fact.
Recently, we did a live stream.
And we sent some money up to a wolf conservation
center in New York.
If you want to look into it, it's nywolf.org.
They actually participate in the program.
One of their wolves, Devon, was reintroduced.
You see the little stuffed animal, the pictures,
and all of that stuff.
You can, if you want, you can head over to that website,
nywolf.org.
And you can symbolically adopt a wolf.
Or you can make a donation.
Or you can check out their gift shop.
And it will help these kinds of efforts.
Because if there's two things that we've learned,
one, we can't stop.
Because people will always devolve
if they're not encouraged to protect these animals.
And that you can't just count on the government
to do what's right for animals that need protection.
Anyway, it's just a thought.
Y'all have a good day.