Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about what, well, you,
and pretty much anybody else who is going to participate
in electoral politics, can learn
from a situation happening in Arkansas.
There's a group in Arkansas called Get Loud Arkansas,
and it is ran by state senator Joyce Elliott.
They will be calling, sending emails, snail mail,
going door to door, and trying to make sure
that people are registered, but not in the normal way.
They're going to be focusing on up to 104,000 people
who might have been improperly removed from the rolls.
So come election time, they won't be able to vote.
This is a thing that happens in a lot of states.
It occurs, at least seems to occur more often,
in red states, where voters get taken off the rolls
for various reasons.
If you plan on participating in the electoral system,
if you plan on voting in those midterms,
if you think you might be one of those unlikely voters who,
in some cases we already know, is going to be the deciding
factor in who wins, it's probably a good idea
to check on your voter registration,
because not every state has an organization like Get Loud
Arkansas that is going to go out and harass you
into making sure that your voter registration is up to date.
This would be especially true if you are part of a demographic
that the ruling party of your state wouldn't want to vote,
or you happen to live in an area that typically
votes against the predominant party.
Not saying that it's intentional in any way,
because that might actually be a violation of the law.
But coincidences do happen.
So if you plan on voting, this is
something you might want to just make sure
that everything's still in order, because with a lot
of the rhetoric surrounding the last election,
it has given cover for a lot of people to,
in the name of election integrity,
subvert people's voice.
So if you're in a state, especially in the South,
you might want to check up on that.
Just stop by and do it with enough time
to make sure that your new registration, if you need one,
has time to process.
Check the registration laws in your state.
Anyway, it's just a thought.
Y'all have a good day.