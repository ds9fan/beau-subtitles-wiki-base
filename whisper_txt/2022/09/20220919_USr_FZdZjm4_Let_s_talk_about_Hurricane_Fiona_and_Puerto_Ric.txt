Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Hurricane Fiona and Puerto Rico.
Now, it's worth noting that I'm going to specifically talk about Puerto Rico in this,
but the Dominican Republic is also being hit.
Okay, so in this instance, it's not so much the wind.
It isn't so much the wind. It's the rain.
12 to 18 inches on average, some places up to 30 inches.
This is causing flooding, and some of it is major.
There's flooding in urban areas, and there is flooding with rivers.
I want to say one of the rivers topped 20 feet over where it should be
and was going up at the time I saw it.
This has caused landslides and mudslides, power outages.
At one point, it appeared as though the entire island was without power.
I want to say that's one, almost one and a half million homes.
The grid there has still not really recovered from Maria.
Now, Biden has issued the declaration.
He did that Sunday morning, but given the fact that it's right around five years since Maria
and there are still problems left over from that,
there are probably a lot of people in Puerto Rico right now that are less than optimistic
about the federal government's response because it was less than adequate last time.
We don't know how it's going to play out this time.
It does appear that there are people there to help set up like command and control functions
and get the power up and running.
It appears they're already there, but sometimes past performance does predict future results.
So this may be something we need to watch in case there needs to be a widespread effort
to try to help out down there because we don't know what's going to happen
and there hasn't been a great track record.
So as everything clears up over the next couple of days and we get a picture of the scope of what's happened,
we'll know a little bit more and we'll know what we as individuals can do to help out.
And I will try to keep everybody posted on that.
Anyway, it's just a thought. Y'all have a good day.