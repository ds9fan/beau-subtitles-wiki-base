Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about
one of the Biden administration's loftier goals,
something that they're actually setting as a goal.
And I don't think it's getting the coverage that it should
because it's kind of a big deal.
On September 28th, there's going to be a conference
hosted by the Biden administration.
The goal of this conference is to end hunger
in the United States by 2030.
Eight years.
That's big.
That's big.
And it doesn't seem to be getting the coverage it should.
Now, this was announced back in May,
and it draws its inspiration from a conference
that occurred back in 1969.
Three-day conference.
And at that conference, a lot of the systems
that you're familiar with today that
are used to limit hunger in the United States,
I mean, that's where they kind of originated,
at least in the federal government's view.
That's where they came from.
Another way of saying that is that a lot of the systems
in use today are a half century old.
Might be time to update them.
And that's what the Biden administration
seems to be doing.
Now, there is a little bit of pushback
because of the timeline.
A lot of people are pointing out that it seems rushed
and that they feel like they need more time to come up
with better ideas.
They had more lead up time to the 1969 conference.
OK, fine.
You had more lead up time back then.
They also didn't have instant communications.
This is a big deal.
This is an opportunity.
Get on the ball.
This is your shot.
Take it.
Now, one of the interesting things to me about it
is that it's not just about feeding people.
It's about feeding people healthy food.
You know, how many times have you seen a post on Facebook
or on Twitter that says something to the effect of,
you know, I was behind somebody with an EVT card
and all they did was buy junk food?
Yeah, because it's cheap.
That's why they bought all those frozen pizzas,
because it's cheap.
It's also not healthy.
In the talking points about this,
they're talking about getting healthy food.
Biden mentioned empty chairs at tables and kitchens
because of people who were lost to heart disease, diabetes,
or diet-oriented diseases, his term.
I mean, I think that'd be a great change.
We haven't updated these systems.
We haven't overhauled them in a very, very long time,
and it's probably time to do so.
Now, as far as looking at this through a political lens
rather than, hey, this is really good for the United States,
I think it's probably a smart move by the Democratic Party.
This is quite literally a kitchen table issue.
Anyway, it's just a thought.
Y'all have a good day.