Well, howdy there internet people. It's Beau again. So today we are going to talk about
what's going on in Virginia, the schools in Virginia, and what happened. I was
going to do a video about this anyway. I was certainly going to talk about this,
but I got a message that I feel is important to respond to, and it's a nice
little lead-in to the conversation. If you don't know what's going on, students
all over Virginia left class today. They were upset because the governor there
has indicated that the government intends to target certain members of the
student body, namely LGBTQ people. And the students, well, they didn't like that. So
here's the message. I was wondering if you would be willing to say something
about the student walkouts in Virginia. I know you're very supportive of the
community is what we're going to say here, and we appreciate it, but I've also
seen your videos on education and know you put a lot of value on education. I
think you would agree that school was not the place for such activities. Yes,
school is a location for studying, learning, writing on chalkboards, and
sitting in rows, and everybody conforming, and this is something I truly do not
believe. What was this? An attempt at a Jedi mind trick? This is not the civic
engagement you're looking for. Now, I think these students should get extra credit
for this. And just as an aside, if you are going to pose as a member of the LGBTQ
community, perhaps referring to trans people by a term that I would use to
describe a trans mission is probably not the best route. That might be a way that
somebody could see through your little gambit here. But it leads into the
whole cartoon depiction thing. So this is something the Republican strategists
need to pay attention to, because the Republican Party has decided to
scapegoat this demographic. They've decided to go after them, paint them as
evil, and they feel okay doing it because they don't know them. To the Republican
Party, to the people making these decisions and targeting this group of people,
trans people are mythical creatures. They've never seen them before. They know
nothing about them. They have this cartoon depiction of them. And so it
makes it easier to other them, to pander to a bigoted base by targeting them.
The thing is, for the students, they're not mythical. They're their friends.
Roughly 16% of Gen Z, those are people born in 97 to 2012, are LGBTQ. You're targeting
people that you don't think exists. For them, those are their friends. Those are
people they see every single day. You are pandering to a bigoted base. They are
fighting for their friends. Who do you think has greater resolve? Who do you
think's gonna win? The other thing Republican strategists need to pay
attention to is those crowds outside those high schools. Those students that
walked out, those aren't small crowds. And understand, about half of them are gonna
be eligible to vote in 2024. But you think they're gonna stay home? They won't
because they're better than you. You're going after their friends. You found a
group that was small enough so you believed it wouldn't matter. It wouldn't
energize people to stand up for them. It's not the way it works. They're better
than you. They're going to win. As far as them walking out, one of the big problems
in this country is a lack of civic engagement. And demonstrations like that,
advocacy like that, that's important and it's missing. They shouldn't be
penalized for it. They're setting the example for the rest of the country. If
you aren't just pandering for votes, you're probably pretty religious. You
probably have a religious motivation for this set of beliefs. May God grant you
the serenity to accept the things you cannot change because on a long enough
timeline, we win. Anyway, it's just a thought. Y'all have a good day.