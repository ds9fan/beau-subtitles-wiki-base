Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about an idea
that has kind of floated up
and entered public consciousness in a way
that for a lot of people is probably unnerving.
And it's the idea that the Republican Party
has another paper plot underway,
another scheme with which to subvert the US Constitution,
try to use a loophole, a technicality,
to get away from what it really meant,
and force amendments to the Constitution.
Now, the idea was to use a bunch of states
to call for a constitutional convention,
and then from there push the amendments through.
Okay, sure, let's say they do it.
Then what?
Well, I mean, if you actually believe in the Constitution
and you're not just some authoritarian goon out for power,
well, then the states have to ratify it,
three quarters of them.
So 38 states would have to ratify these amendments.
That's not gonna happen.
That's not realistic.
So they would have to scheme
and find some way to subvert the Constitution there
and get around that process.
So they can force their will on the people, right?
Okay, so what does this really tell us?
Better yet, a question.
Why don't they try to achieve these legislative goals
through normal legislation?
They can't.
They don't have the power.
They're too weak.
That's why they don't do it that way.
So they're trying to use some technicality
because they know they don't have the power.
They don't have the support.
And this is a way to try to cling to power
rather than change their positions.
That's what it's telling you.
The Republican Party fixating
on all of these quasi-legal theories,
these ideas of trying to exercise power
by skirting the Constitution,
it shows that they're weak
and that they know they're weak
and that they know they're not getting stronger.
Because if they actually had support,
they'd be able to do it through normal legislation.
But they don't have it.
So let's say they do it.
They subvert the Constitution.
They change the way things are ratified.
They push this stuff through,
through some quasi-legal means,
but they don't have the support.
What happens?
Nothing.
Nothing.
Nothing happens.
They don't have consent of the governed.
It's not like this concept
hasn't been tried before in the United States.
I'm pretty sure there was like this period called Prohibition
that was brought about by an amendment
that didn't have public support.
What occurred?
It went away.
That's what would happen here.
This push shouldn't be something
that scares progressives.
I mean, be aware of it, be ready to counter it,
but it shouldn't be scary
because it's a sign that you're winning.
They know they don't have the support
to push through legislation through normal means.
So they have to try to wiggle their way through.
And they don't have any hope of gaining that power back
because they're that out of touch
with the majority of Americans.
They don't understand where the country has gone
and they're confused.
So they're lashing out.
They call themselves patriots
while trying to find some way to skirt the constitution,
which undoubtedly is their profile picture.
It's not a scary prospect.
It's a sign that progressives are winning.
Every time they try something like this,
they alienate more and more people
who see them for what they are.
People who don't want to represent,
but people who want to rule,
who want to force their ideological,
their political, and their religious beliefs
on the rest of the country,
even though they don't have consent of the governed.
It's not a scary thing.
It's a sign that progressives are winning.
Anyway, it's just a thought. Y'all have a good day.