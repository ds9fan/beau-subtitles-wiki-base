Well, howdy there internet people.
It's Beau again.
So today we are going to talk about civics and races
to watch during the midterms from a civics teacher's
point of view.
We're going to do this because I got a request from a teacher.
By the way, because this is kind of in here,
if you ever want to, if you're a teacher
and you want to use any of these videos in a class,
go right ahead.
You don't need anything from me to do that.
Just don't get in trouble.
OK, so the actual question here, which
races in the upcoming midterms do you feel
would be good ones to watch?
I'm hoping to put together a list of Senate, House,
and Governor races that my students can
monitor in November.
OK, so if I was doing it for a civics class,
I would focus on two House, two gubernatorial ones,
and then a whole bunch of Senate ones
because it provides unique opportunities that way.
For the House, I would look at North Dakota
because it gives you an independent.
You know, Karamund, we've talked about her on the channel.
And you can explain the dynamics of running
as the opposing party in a incredibly partisan state,
a state that leans heavily one way or the other.
I would look at Colorado, 8, I think,
because it's a new seat, which gives you the opportunity
to talk about how seats and districts and all of that stuff
come together.
Then for governors, I would look at Florida
because that's one that if your students are watching
the news at all, they'll catch bits and pieces of that one.
And Maryland because it gives you the opportunity
to talk about the tactic that the Democratic Party used
there.
The Democratic nominee is Wes Moore,
and they intentionally elevated a Republican candidate
during the primary because they thought
he would be easy to beat.
And then for the Senate, this is where I would give them a bunch.
Arizona, Florida, Georgia, Nevada, New Hampshire,
North Carolina, Ohio, Pennsylvania, and Wisconsin.
And the reason is this is relatively easy to track
how either party could either retain or obtain
control of the Senate.
And it gives you that opening to talk about how
it is the more deliberative body.
The paths to victory for the Republican Party
is pretty simple.
They hold Pennsylvania and Wisconsin,
and then they flip one of Arizona, Nevada, Georgia,
and New Hampshire unless they lose either Pennsylvania
or Wisconsin.
If they lose, then they have to flip either one more or two
more in addition to that.
The Democratic Party has a pretty simple route too.
All they have to do is hold the seats they have,
that would be one, or flip as many as they lose.
So it gives them an opportunity to track
how control of one of the houses occurs and kind of plot it out
without it being overly complex.
Because if you tried to do that with the House,
there's like 500 different options
on how things could break down.
But with the Senate, it's pretty easy.
So that's kind of how I would go.
I'd also look for one that is just ridiculously gerrymandered
outside of your state so you don't incur the wrath
of any elected officials.
But I would do that as well.
And then maybe, I don't know how you would even
try to find this, but if you could find one
where you knew there would be a lot of early voting,
especially if the early voting was heavily one party,
so people could watch the mirage and watch it flip.
Because typically, the mail-in ballots
are counted at a different time.
So it either starts off one way and then could lean the other,
or it starts off heavily based on the in-person voting
and then flips later when they count the mail-in votes.
So that would be another good one.
But as far as getting them involved and interested,
I mean, I would try to provoke them
to talk about various issues and maybe pick
a race in each category to have them vote on.
But I would be reluctant to do that if I was a public school
teacher, because I'm sure somebody would complain
and it would turn into a giant thing
and you'd end up on CNN.
But breaking it down that way, it
gives them different views of the different tactics,
gives them an independent, lets them chart the path,
and it should be a fun learning experience.
Anyway, it's just a thought.
Have a good day.