Well, howdy there, internet people, it's Bo again.
So today we're going to talk about somebody
who's likely to become the most sought after man
on the planet.
There is reporting that suggests Russian General Grissomov
is headed to Izyum to take personal command of that advance.
Now, earlier in the conflict,
there was another general who shared the same last name.
And people got him confused with this Gerasimov.
This is the Gerasimov, the Gerasimov of the Gerasimov
doctrine.
If you're not familiar with what that is,
it is a Russian military doctrine
that, not to overstate it, is brilliant.
It really is.
It's not combined arms.
It's combining all arms of the state for the functions of war.
It's brilliant.
However, it had a critical flaw.
It's a lot like somebody coming to you asking for investment advice.
And you say, well, you take your first million and put it into real estate.
Never explain how to get that first million.
That's the trouble with this doctrine.
It was written for a military that doesn't exist.
It was written for the Russian military that he had reports on, what the generals under
him said they were capable of, what they listed as their assets.
That's what it was written for.
And we have been shown repeatedly over the last two months that is not the Russian military
that exists.
So the doctrine would be very successful if it had a professional military to execute
it.
Russia just doesn't have that.
Okay, so before we get into why he's about to become the most wanted man in the world,
let's talk about what this means.
Vorosimov is kind of the equivalent to the chairman of the Joint Chiefs, going to take
personal command of a battle.
This tells us a couple of things.
One, this line of advance is incredibly important to Russia.
Two, the offensive is not going well.
This is him saying, I'm going to go do it myself.
And three, their chain of command is broke.
That's what it tells us.
If this reporting is accurate and this is actually happening, that's what it says.
Now let's say he shows up.
What does Ukraine do?
No matter what, doesn't matter how insignificant the fighting that is occurring at that time
is, if he's in command of it, he has to lose.
They have to throw everything they have at defeating him because it would be a huge demoralizing
psychological operation for him to go back with no real changes.
That would be Ukraine's smartest move would be to counter him with anything and everything
they could.
Now, Ukraine has been very successful at forcing Russian generals to remove themselves from
the battlefield rapidly.
And they will probably try to do that.
And they will most certainly be encouraged to do that by NATO.
Because here's the thing, critical flaw in that doctrine or not, this guy's a genius
you can't underestimate that. From a long-term NATO perspective, he can't be allowed to leave.
He will have first-hand knowledge and see how the Russian military actually operates,
the world that they actually inhabit, what it's truly like in the field. If he leaves the
the battlefield with that knowledge, he'll probably go back and write a
doctrine that would work really well. So I would want to be anywhere near
this guy because there are going to be a whole lot of different agencies,
governments, and forces that are going to be looking to drop something on his head.
This is a pretty surprising development. This isn't something that really happens in real life.
In movies, sure, the general goes to the front and rallies the troops and all of that.
In real life, that doesn't happen. And this is a really bad sign for how things are progressing for Russia.
for Russia. This is them sending, and it doesn't really get more high level than
this, to the front to take command of this. This is the reporting. Now it's a
bizarre move. It's something that I would, I have a hard time believing that this
is actually going to happen, but there's been a whole lot of moves they've made
that just didn't make any sense. So this would just be another one in that chain.
Sending somebody that is this important not just to military doctrine and the
military itself but to the propaganda effort to the front is not smart. This is
a person who will often be in the room with Putin. He's one of the stars of the
Russian military. Him being defeated or him not returning home would be a huge
loss, not just from the strategic side and how it impacts the
military, loss of command and control and all of that, but from the propaganda side,
the morale side, people will notice him gone. They can't just ignore his removal.
So, I would imagine that the amount of resources that are currently being devoted to A, confirming
that this is actually happening, B, finding him, and C, making sure that he doesn't go
home are immense.
This also signals that the aid that is coming into Ukraine is definitely working.
They're throwing their star into play to try to get this, to try to break the stalemate
because those lines, yeah, they gained a little bit over the last day or so, but they're not
really moving.
And that's bad news for Russia, because as we've talked about from the beginning, all
Ukraine has to do is keep fighting.
They don't have to win the battles.
They don't have to drive Russia out militarily.
All they have to do is wear them down.
And at the moment, it certainly appears that they're succeeding in that.
Anyway, it's just a thought.
Y'all have a good day.