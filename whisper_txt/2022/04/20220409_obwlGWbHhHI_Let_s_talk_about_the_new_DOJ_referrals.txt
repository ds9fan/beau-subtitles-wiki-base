Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the new referrals for contempt
that have come out of the January 6th committee.
And there are a lot of people who are getting really excited about this,
and I'm going to do a little bit of managing expectations today.
The first thing to know is that the committee can't indict anybody.
That's why they're called referrals.
They can refer them to the Department of Justice for indictment.
That means they go before a judge.
That's when the criminal proceedings actually start.
Up to this point, the committee has referred four people.
You have Steve Bannon, who's a podcaster.
He was referred, indicted, and is going to trial this summer.
You have Mark Meadows, who was the chief of staff.
He was referred. No indictment yet.
That was months ago.
You have Dan Scavino, one of the new ones,
communications advisor, has been referred.
Peter Navarro has been referred.
Peter Navarro is a weird case,
because while he's refusing to talk to the January 6th committee about it,
he's on TV seeming to admit to very large parts of it
when he talks about the Green Bay Sweep.
So out of the four people that have been referred,
only one's been indicted.
And at this point, if indictments were issued today,
tomorrow, Monday, whatever,
these trials would occur after the midterms.
A more important part of this is that if the indictments
don't come down before the midterms and the Republicans retake the House,
they theoretically could squash all of this but Bannon's.
I don't hold a whole lot of hope that this is going to lead to much.
We don't know what the Department of Justice is doing.
There have been some signs that they're actually investigating,
running a parallel investigation of what happened on January 6th
and moving up the chain.
That's actually how it's supposed to work.
Garland has never been one for press releases
or telling people what's going on.
But at this point in time, things have been dragging on so long,
there's a whole lot of people who don't see accountability on the horizon.
The longer these indictments don't exist,
the longer DOJ just doesn't follow up on the referral,
doesn't issue a statement as to why they're not going to indict
or a statement that they're going to indict something,
the more people lose faith in that system
and the idea that there's going to be accountability of any sort
beyond those people who were misled and duped.
That seems like the end of the accountability at this point.
Signs from DOJ suggesting they're investigating beyond that,
yeah, they're there and there are higher level people
who have been indicted for activities on the 6th.
Now, there are explanations.
Maybe DOJ doesn't want to get involved in this
while they're in the middle of running their own investigation,
their own criminal investigation.
Maybe they don't want to make announcements
because they don't want to politicize it.
There's a whole bunch of, well, it could be this,
but we don't know and there isn't enough information out there
to really make an educated guess as to what's going on.
The only thing that I can say that might be a reason to maintain hope
is that this is kind of Garland's style.
This is kind of the way he's always been.
There's never been a drive from him to keep the press informed
about cases that are ongoing.
It's never been part of his way of doing things.
The American people have grown accustomed to that.
The American people have grown accustomed to being informed
as to what's happening as things move along.
It's worth noting that that's not the way things have been done
throughout most of American history.
So it might just be that he's being very traditional,
but there's always the well-founded belief
that those at the top aren't held accountable for their actions
and the longer things draw out,
the longer this goes on without developments,
the more Americans start to see that as what's happening.
So there are two more referrals,
but it doesn't necessarily mean anything unless DOJ indicts.
In fact, if DOJ doesn't indict,
it might actually serve to embolden those
who are trying to obstruct the committee's work.
So anyway, it's just a thought.
Y'all have a good day.