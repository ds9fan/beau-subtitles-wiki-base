Well howdy there internet people, it's Beau again.
So today we're going to talk about the Moskva.
This is a ship, was a ship, that now
lives in a pineapple under the sea off the coast of Ukraine.
It was sunk.
It was a Russian warship, the flagship of the Black Sea Fleet,
and it sank.
There are varying explanations for this.
The Ukrainian military claims that they sank it.
The Russian military claims it spontaneously combusted.
I don't know which is worse for the Russian military,
to be honest.
I don't know if it's worse to have it sank by the Ukrainians
or to admit that your crew is so inept that a small fire sank
your flagship.
Either way, it's all bad for the Russian side.
Quick overview, the ship was originally
built in 1979 in Ukraine.
Commissioned in 1983 as the Slava.
In 2000, it was refitted, a very costly refit,
and recommissioned, renamed the Moskva.
And then in 2022, it was renamed again,
the Snake Island Memorial Reef.
All jokes aside, because there's a whole bunch of them
to make about this, I'm going to go ahead and get
about this.
We can't overlook the fact that naval history was probably
just made.
This is the first time in modern naval history
that I'm aware of that the flagship of a fleet
was sank by a country without an operational Navy.
This is kind of like getting fired on your day off.
But people want to know the value of it.
That's really hard to determine.
This isn't a disposable asset.
This isn't like a tank or a plane.
This is a little bit different.
The cost isn't just the cost to have it built.
The refits on these things can run tens of millions
or hundreds of millions of dollars.
And this was refit, I want to say twice.
It underwent substantial refits.
So it's not just the money you think about.
It's also the fact that these things
don't go obsolete as quickly.
They're not supposed to.
The Moskva was supposed to be in service until 2040,
meaning somebody born in Moscow today could have served on it.
It's last year in service.
This was supposed to be around a while.
It's an incredibly valuable ship.
It's going to take a long time to replace.
Ships like this take a while to build.
They're not easy to just come up with another one.
So it's time.
It's money.
It's morale.
And people are making a big deal about that as far as how
it's going to impact Russian morale.
And yeah, I mean, that's definitely
going to factor into it.
I would also point out while we, it's
impossible to not make jokes about this,
but it is worth noting that there were 500 sailors
on this thing.
And it is incredibly unlikely that all of them
were currently above water.
So what does it change on the ground?
Honestly, not a whole lot.
Not really.
Russian ships are going to be leery of coming
close to the Ukrainian shores now,
lest they also spontaneously combust.
So that may provide a little bit of breathing room,
but it's not going to keep them from operating.
I think the biggest loss here, the biggest thing that's
going to impact the future of the war
is the fact that a leader who is trying his best to save face
and posture and talk about his country's military might just
lost their flagship to a country without an operational Navy.
That may be the thing that kind of brings all this into focus,
or it may be something that he feels
that he has to double down on and prove that the Russian Navy
isn't incompetent.
But I have a feeling the loss of this ship
will weigh heavily in the mind of Putin,
because this isn't something that they can cover up.
It's not something they can ignore.
They can come up with some story to explain it missing,
but I don't think the Russian people are going to buy
the story that they're putting out.
It was in a war zone, and it sank.
So that's an overview of it.
There are tons of PR opportunities
for Ukraine in this one.
It is certainly going to be a morale boost
to the Ukrainian side.
This is the ship that was involved in the Snake Island
thing, the ship that was told to go have fun by itself.
So there's going to be a morale boost on that side,
because that has become kind of a rally cry for the resistance.
And it's going to hit Russian morale,
but I don't know that that's going
to be as big a deal as it's being made out
to be in the press.
I think its impact on Putin is going
to be more important than its impact on the average soldier.
Anyway, it's just a thought.
Y'all have a good day.