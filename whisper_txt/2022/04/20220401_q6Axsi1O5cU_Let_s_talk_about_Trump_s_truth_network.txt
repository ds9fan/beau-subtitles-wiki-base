Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Truth Social,
which is former President Trump's social media network.
And it's been kind of a surprise development.
You know, as most of you probably know,
this was billed as it was going to be like a competitor to Twitter and Facebook
and all of this stuff.
And most people, myself included, made fun of it.
And as most of you probably know,
scheduled to get off the ground in March.
So here we are.
What happened?
It has millions of active users.
It's basically a Twitter clone.
And there are millions of people who have signed up
for this conservative echo chamber.
Companies, conservative companies,
are paying a fortune to have their ads on this network.
So the politicians who invested a bunch of money in it,
everybody who invested money in this,
they're making out.
We misread it.
It's actually doing really, really well.
New installs of the app are just through the roof.
It's kind of surprising.
I mean, given Trump's past business record,
I don't think anybody really expected this to happen.
Those of us who have been following this really closely
understand that it's April Fool's Day.
No, it's still a dumpster fire.
The new installs have dropped, I want to say,
93% or something like that.
I did see a bunch of people talking about it today,
but they were talking about it on Twitter
because they couldn't get on to the new social media outlet.
It doesn't appear as though the deadline
for getting it up and running in March is really met.
It doesn't appear to be able to handle any real sort of load of users.
So it seems as though enthusiasm for this project is slowly fading.
I'm not aware of any major advertisers going over there or anything like that.
If you didn't find this joke funny,
I mean, just remember that there were a whole bunch of politicians
who invested tons of cash in this who are probably losing tons of money.
I mean, that should make you laugh because that's always funny.
Yeah, I don't think anybody's surprised by these developments,
except for maybe the person who left Congress to go work for the company.
Anyway, it's just a thought. Y'all have a good day.