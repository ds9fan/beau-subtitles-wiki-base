Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about Russia
sanctioning the United States in a way of sorts.
There aren't a lot of ways for Russia
to respond as far as sanctions go.
They have quickly become the most sanctioned country
on the planet, surpassing even Iran.
Last count, I want to say it was 5,500 different sanctions
on Russia.
The chief of the Russian space agency
has announced that they will no longer be delivering rocket
engines to US customers for space
and said that Americans could fly up there on their brooms
if need be.
I am sure that SpaceX is heartbroken by this news.
In related news, when it comes to space exploration,
NASA has a program called Artemis.
And it is the program that is slated
to return us to the moon.
Right?
With everything going on, there's
probably room for some hope, some diversion
from all of the constant issues here on Earth.
And Artemis has a unique little thing.
You can have your name flown around the moon.
You can sign up on the website.
I'll try to remember to put the link down below,
but it's easy to find.
Basically, they put your name on a flash drive.
They're calling it a boarding pass.
It seems like a really interesting way
to get young people, get kids involved in the curiosity
aspects of it, kind of pique that interest
in reaching for the stars.
So if you are currently just overwhelmed with everything
that's going on in the world, there are slivers of hope.
There are things that can cheer you up
and can divert your attention from the constant news cycle.
So if you have kids, it might be something
worth doing with them, getting them a boarding pass for it.
And if you're just interested in it,
it might be something worth looking into.
But as far as the sanctions go from Russia,
I'm fairly certain that the United States will be just fine.
And this won't present any major issues for NASA's endeavors.
Anyway, it's just a thought.
Y'all have a good day.