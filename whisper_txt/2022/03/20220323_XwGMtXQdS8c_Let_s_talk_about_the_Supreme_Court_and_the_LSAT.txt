Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the LSAT and Supreme Court nominations.
For those overseas, the United States is in the process of putting a new justice on the
Supreme Court.
The person nominated will be the first black woman ever.
Because of that, a whole lot of folks have come out of the woodwork trying to find a
reason for her not to be there because it's still the United States.
I have a whole video going over her background, and I'll put it down below.
But suffice it to say, there is literally nobody sitting on the Supreme Court that has
her wide range of legal experience.
Not a single person.
Period.
Full stop.
Since they can't go after her for a lack of experience, they have decided to ask for
her LSAT scores.
And they have made this super important.
A whole bunch of people who got bored pretending to understand vaccines now are experts in
the legal system.
So what is the LSAT?
It's the test you take to get into law school.
To get into it.
It's on it.
Analytical reasoning.
Logical reasoning.
A writing sample and reading comprehension.
Weird, right?
I imagine, since they're holding this up as something that would be a requirement for
somebody to sit on the Supreme Court, you would think that it would have something to
do with the law.
It's not.
It's a placement test to see if you'll be able to handle the courses.
That's what it is.
Now I was going to try to sneakily find out what her LSAT score was, but in the process
of looking I found out that I would have to get into Harvard's records to find out.
Because that's where she got into.
Given the fact that she got into Harvard Law with this LSAT score, I'm going to assume
that it's at least a little above average.
But it really doesn't matter because this is the pre-test.
This is what you take to get into law school.
So what you would really need to know is her GPA.
When she left.
And again, I was going to try to find that out, but when I looked at her Wikipedia biography,
I realized she graduated cum laude.
I'm going to guess it was probably pretty high.
The reality is they don't have a reason to oppose her.
She has more experience, a wider range of experience, than anybody on the court.
She graduated from Harvard Law.
She probably knows what she's doing.
I mean, it seems as though she's an incredibly qualified candidate.
But you know, she is a black woman.
So there's a whole bunch of people who don't want to see that happen.
This is exactly what it looks like.
There's no reason to be questioning her LSAT score.
It was good enough to get her into Harvard.
And there is no doubt that she is qualified.
There's no doubt that she is experienced.
It's just manufacturing an issue to turn people against someone that they feel they can other.
Anyway, it's just a thought. Y'all have a good day.