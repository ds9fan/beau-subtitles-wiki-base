Well howdy there internet people, it's Bo again.
So today we're going to talk about Vice President Pence.
He's been in the background throughout this entire administration,
but all eyes are certainly on him today.
It is expected that the House will ask the Vice President to invoke the 25th
Amendment and to take the reins of the executive branch.
According to CNN, reporting from sources close to the Vice President,
Pence is hoping to spend his remaining days in office telegraphing to our
allies and adversaries that we have a fully functioning government.
That's an interesting thought right there, that the Vice President's primary
concern is convincing the rest of the world that we still have a government.
That's interesting.
I'm going to suggest that if in the middle of a massive public health issue
and all of this, the Vice President's primary concern is signaling that we
have a functioning government, we don't.
Because the executive branch seems pretty preoccupied with things other
than governing.
It appears that the chief executive has just signed out already, he's done,
and the Vice President is concerned with appearances.
Congress is preoccupied with attempting to get some semblance of a functioning
executive branch.
This statement, this idea, really indicates that we don't have a fully
functioning government.
You know who can change that?
Pence.
Pence can.
You know, I don't know if it's going to become a big story later, it might,
but there's a rumor that Vice President Pence displayed an immense amount of
personal courage during the recent unpleasantness at the Capitol, and that
it was in fact Pence who invoked Rule 303 in the truest meaning of the term.
If that rumor is true, if it was in fact Vice President Pence who overrode the
president and released the National Guard, in that moment, at that exact second,
Vice President Pence acknowledged that the president wasn't fit to lead.
He already invoked it.
If that occurred, he's already done it.
I don't know that a formal acknowledgement of that is such a drastic step,
such a big deal, but it would certainly move things along.
I understand the political realities of it.
I'm not sure that politics should come above the country, though.
I don't know that having our elected officials spend their time signaling
that the government is functioning is more important than the government
actually functioning.
All eyes are on the vice president.
Anyway, it's just a thought. Y'all have a good day.