Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about what
we can learn from security details, from bodyguards.
Because there's a lesson there that can actually
be applied to quite a few things going on in the United States
right now, but one thing in particular.
So if you don't know, a long, long time ago in a land far
away, I used to be a security consultant.
When I was doing that, I was a part of a few protective
details.
And there's this interesting thing that happens.
Now, when you're setting up a detail,
you have the inner ring that's closest to the client.
And then depending on the threat level, you have more rings.
If you're talking about a celebrity,
you generally have an outer ring that is window dressing.
It's people who literally do not know what they're doing.
They're most times cops, local cops that are just hired.
And their job is to stand there and look at the crowd.
It's a deterrent.
It's window dressing.
But the deterrent is pretty effective at keeping people
away.
This weird thing happens.
And to be honest, I can't remember a single time
it didn't occur.
It pretty much always happens.
There's the event.
Whatever the event is, it could be a speech, a concert,
a book signing, it doesn't matter.
After that event, there's like a meet and greet.
The client, whoever it is, is shaking hands and meeting
people and stuff like that.
That outer ring, during the build event,
the ticketed event, the speech, the concert, whatever it is,
they do exactly what they're supposed to do.
When the meet and greet starts, that outer ring,
it breaks down completely.
It just ceases to be.
It ceases to function.
And it starts off with them just backing up.
Because to them, the event's over.
And it is, but the job isn't.
So they start backing up and getting closer.
And eventually, they end up turning around and looking
at the client, which is not really good security.
If you're doing that, all that happens
is you get to see whatever occurs occur.
You don't get to intervene.
So that inner ring, when this occurs,
and most people expect it to, they
raise their security posture a little bit.
They're a little bit more alert because they
know that deterrent is gone.
That outer chain, it broke.
And it starts with one link and then spreads,
just like anything else.
So it puts more stress on the rings on the inside.
They have to work a little bit harder
because they're still paying attention.
Right now, I think a whole lot of the country
is looking at the client.
They have turned around.
The main event is over.
So they're no longer paying attention.
You could apply this to a lot of things.
Apply this to quite a few things in this country right now.
The thing that I think that you, as individuals,
can really act on is applying it to the public health issue.
The main event is over to most people.
And a whole lot of people have stopped paying attention.
They have turned around.
That means you should probably elevate your security posture
a little bit when it comes to this.
The guidance that comes out, generally speaking,
it's written as if everybody's doing what they're supposed to.
They're not.
So my advice to you is whatever the guidance is,
do that and a little bit more.
If they say, hey, it might be a good idea
to wear masks inside small buildings,
wear masks inside all buildings.
Go a little bit beyond because there
are people out there who are no longer paying attention.
That chain is broke.
It's something that you can actually do to help this.
Because I have a feeling if things don't turn around
and turn around fast, we are in for a very, very rough fall.
The job's not over just because the main event is.
The job isn't over until the client is secure.
And right now, we're in the meet and greet phase.
A whole lot of people aren't paying attention.
And many believe that they're off the clock,
that they've done everything they need to.
So I would continue to go a little bit above the guidance
until things are back under control.
Anyway, it's just a thought.
Y'all have a good day.