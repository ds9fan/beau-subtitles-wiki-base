Well howdy there internet people, it's Beau again.
So today we're going to talk about a proposed piece of legislation.
And it is certain to spark a lot of conversation in the coming days.
It's pretty unique. It's called the Love America Act, and with a name like that
you know it's going to be interesting, and add to that it was introduced by none
other than Senator Hawley.
You know, legislation often has a statement of reasons, like a preamble.
Something to explain why the legislation is needed, why it's happening, right?
In my entire life, I have never seen a piece of legislation where the stated
reasons for it, and the legislation itself, are in direct contradiction
quite like this. It's really unique.
Okay, so why does he want it? Let's start there. Why does he want this bill?
The idea behind it is to make certain that American students don't believe the
US was inherently racist when it was founded. That's his goal. He doesn't want
that to be taught. To accomplish this, he has a four-step plan. The first is that
students in the first grade read and are able to recite the Pledge of Allegiance.
Okay, that doesn't do anything one way or the other. That's just, what's it called,
indoctrination. Step two, students in the fourth grade read the Constitution of
the United States and are able to recite its preamble. Step three, students in the
eighth grade read the Declaration of Independence and are able to recite its
preamble. Step four, students in the tenth grade read and are able to identify the
Bill of Rights, which is a little confusing to me because I don't know if
Senator Hawley wants additional emphasis put on the Bill of Rights or he doesn't
understand that the Bill of Rights are part of the Constitution. They're the
first ten amendments to it and when you study the Constitution, you kind of have
to study the amendments. Okay, so his goal is to make sure that American students
don't believe the US was racist when it was founded. To accomplish this, he is
going to have them start by reading the Constitution and memorizing the preamble.
So they're going to get this glorious, idealistic preamble, right, and they're
going to know it because they've got to memorize it. And they're going to hear about
we the people. And then they're going to learn about all the rights and everything
that are supposed to go along with that. And then they're going to realize that we
the people didn't include a certain group of people because the founders
kind of didn't see them as people, at least not whole people, maybe just three-fifths
of a person, right? And then they're going to go on and they'll get down to the
15th Amendment or so and at that point they're going to realize that, wow, the
country was really racist in the beginning because in 1870, the 15th
Amendment, that's the one that prohibited barring people from voting
based on their race. So up until then it was okay and it needed a constitutional
amendment to stop it. Golly gee whiz, that seems awfully racist to me, Senator. And
then when you study the Constitution, you often need to kind of look at how it was
interpreted. So they'll read things like the Dred Scott decision. I would imagine
people would walk away from that thinking that the United States was kind
of racist when it was founded because it was. Then they'll go to the Declaration
of Independence, which by the way that seems backwards to me. It seems like you
should do this in chronological order, but whatever. So the same thing happens.
They get this glorious idealistic preamble and then they get to read about
merciless Indian savages. That's in the Declaration of Independence. And they'd
probably learn that Jefferson wanted to include in the list of grievances that
the king allowed slavery. He wrote it in, but the founders, they had him take it
out because they wanted to continue the incredibly racist institution of slavery
because it was building this country. You might say that it was integral to the
United States when it was founded. There is no way to read the founding documents
of this country and walk away with any other conclusion than it was racist when
it was founded. And then as you go through time and you look at the
amendments, you can see as more and more people got included in people and you
get kind of like a timestamps for when different things happened. And then if
you really get into it, then you start looking at the federal laws that went
along with it to expand on the ideas in the Constitution, like the Civil Rights
Act, which didn't occur until the 1960s. It's not a matter of whether or not the
country was founded and and was racist back then. We're talking about how racist
it is now. There is no way you can read these documents and actually understand
them and believe anything other than the U.S. was inherently racist when it was
founded because it was dependent on the institution of slavery. The idea here is
to make students love America. You want them to love America? Give them an
America worth loving. You want them to respect the flag? Make it the symbol of
something worthy of respect. But when you have politicians lie about things that
are plain truth, these are historical realities, you can't pretend this stuff
didn't happen. It casts doubt on whether or not the United States is ever going
to learn from it, is ever going to level the playing field, is ever going to
fulfill the promises that are in those glorious and idealistic preambles. You
want them to memorize. They should memorize them because that's the goal. It's
not where we're at. That's where we should be headed. I actually think this
is a great idea because it's not going to work to accomplish his goal, but I
think if more people actually sat down and read these documents, they might have
a better understanding of why the United States is so lopsided. And when you look
at this on a timeline and you understand that from the beginning until the 1960s
it was legalized racism, it's a whole lot easier to accept that there are,
let's just say, after effects that aren't explicitly racist legislation, but they're
racist institutions that continue to exist and permeate our society today. So
yes, I think it's a great idea. Students should absolutely read these things. Not
because they won't show that the US was racist when it was founded. It's
definitely going to show that because it was, but because it gives them a scope of
where we started and where we should be. And we're not there yet. The United
States isn't perfect. It's not a finished project. There are a whole lot more
people that need to be included under that umbrella of freedom that is
promised in those preambles that you want them to memorize. Anyway, it's just
a thought. Y'all have a good day.