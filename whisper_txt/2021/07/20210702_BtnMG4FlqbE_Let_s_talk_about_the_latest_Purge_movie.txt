Well howdy there internet people, it's Beau again.
So today we're going to talk about the Purge
and how everything is political.
We're going to do this because we're going to try to head something off
that happens anytime something like this occurs.
So, the latest installment
of this movie franchise.
It's pretty political.
If you're not familiar with the series of movies,
basically for a set period each year,
well, all crime is legal. Everything.
And people go wild.
That's the general storyline.
The latest installment
takes place along the southern border of the United States.
And it
highlights a group of people who, well, they just
want to take America back.
They want to make it great again. They want to purify it.
A bunch of bigots.
And they,
well, they're not ready for it to be over.
So they mount their own little insurrection
and keep it going. And it's coordinated.
The parallels are
pretty obvious.
And people are describing it as ham-fisted
because it is
not subtle.
It is definitely a shot at Trumpism.
Now, anytime something like this happens,
you have that group of people that comes out and says,
great, they have to make everything political.
In times like this, everything does
become political, but I would point out
that
horror movies in general explore our darkest fears.
That's what they're about.
And
they often contain sociopolitical commentary. This is especially true of
the Purge franchise.
Throughout it,
there is definitely
commentary on economic disparity
between the haves and the have-nots.
It exists. It's there throughout the entire franchise of movies.
It is very political.
And it's not limited to this franchise.
So when you hear people say, oh, horror movies are woke now.
They're politically aware.
Just go ahead and point out that
pretty much forever
they have been.
Not all, but most.
There's commentary about the environment,
toxic sludge,
commentary about greedy politicians who don't want to close the beaches,
commentary about the military-industrial complex,
and how when it finds something that's a danger to humanity, rather than eliminate it,
it tries to weaponize it.
It's there
throughout everything.
The
Umbrella Corporation
would be a good example.
Obviously, commentary exists.
It always has been.
Those who don't like the commentary,
who don't want to see it, who want to reject
that commentary,
they
just
refuse to see it, or they pretend like they don't.
They pretend like it's a joke.
All art
is political in some way.
Pretty much everything today is political.
If you can consider
horror films art.
But because of the nature
of this film
and
the political commentary it is providing,
I would imagine there's going to be a whole lot of upset people on social media.
So, it may be worth remembering
that the entire franchise
is political in nature.
It provided political commentary from the beginning.
And
the commentary that is in the
latest installment
is certainly not an accident.
It's a little too
on the nose, to be honest.
The parallels to the sixth
are really easy to see.
But the interesting part
is that it was filmed
before the sixth ever happened.
Because,
I guess life imitates art.
Anyway, it's just a thought. Y'all have a good day.