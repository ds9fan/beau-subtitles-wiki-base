Well howdy there internet people, it's Beau again.
So today we're going to talk about wolves and Wisconsin and a romantic image.
Eight months ago I put out a video when the gray wolf came off the Endangered Species
Act list.
Was asked if I thought it was a good idea.
I said flat out no.
My concern was that once the federal protections were gone, the duty to protect the animal
would revert to the states.
The drive to bring this animal back from the brink of extinction has been going on 40,
50 years.
Forty or fifty years.
And I was concerned that there could be a setback because despite the lobbying efforts,
we hadn't crossed the finish line.
We had not crossed the finish line yet.
The populations were not stable.
Not really.
That was eight months ago that video went out.
Now there's this image.
Man versus nature, right?
One of the great themes in literature.
And because the wolf is an apex predator, there's a romantic image that says that taking
out a wolf, I mean that's the pinnacle of tough guy.
You aren't on foot carrying a flintlock anymore in a wild and untamed area.
You are on four wheelers using drones and advanced equipment.
It is not man versus nature.
It isn't some contest.
It's just killing something to capture an image.
You want to shoot a wolf and get a trophy?
There are other ways.
So what has happened?
University of Wisconsin has released a study suggesting that the great wolf population
in Wisconsin has been decimated by a third.
By a third.
Taking the numbers back down to 2008-2009 levels.
It's pretty clear that the state cannot protect this animal.
A decade up in smoke.
So a bunch of guys can feel tough.
So they can pretend they are something they are not.
And losing a third of the population, it's not like it's just going to take ten years
to recoup this.
Because the packs are going to have been disrupted and it's going to take longer than it did
the first time.
Because at that point, back in 2008-2009, kind of on a roll.
Steady growth.
Because the packs were stable.
Probably not going to be the case now.
The state up there has said that they will let science determine whether or not to have
another hunt this year.
After reducing the population by a third in eight months.
I'll go ahead and let you know, the science is in.
You can't have one.
You can't have one.
Not if you expect to maintain this animal.
This could have been one of the great success stories of American conservation.
All it took was for the states to stay the course and to protect the animal.
Instead economic interests and the desire to feel like a tough guy has set things back
a decade.
And less than a year.
I'm going to suggest that there are a lot of guys out there that need to get a new hobby.
And understand that the masculine instinct, there's also a creative side to that as well.
It's not all about destruction.
And I would also point out that it isn't sending the image you think it is.
It's not the same.
You're not doing this with a flint knife, a flint lock, and a bow and arrow.
The days of that man versus nature contest, man versus wolf, it's over.
And advances in ammunition have changed that.
It's not the same.
All it is now is a game of make believe.
Pretending you're a mountain man.
Anyway, it's just a thought.
Y'all have a good day.