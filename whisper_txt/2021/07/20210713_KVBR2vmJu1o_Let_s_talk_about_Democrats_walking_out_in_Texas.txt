Well howdy there internet people, it's Beau again.
So today we're going to talk about what's going on in Texas.
If you don't know what's happening, the Republican Party is pushing forward a voting restrictions
bill.
I don't know what they're calling it, but that's what it is.
It's a bill to make it harder to vote, to make it more inconvenient to vote, discouraging
voting.
That's what it's about.
They're framing it as some kind of election security thing, but realistically nothing
in the bill actually improves security, so that seems unlikely.
Democrats fled the state.
Democratic politicians, they left Texas.
Rumer Mill says they went up to DC to hide out.
By doing this, they denied Republicans having two-thirds of legislators there.
Means they can't move forward.
The governor of Texas can, in theory, send Texas law enforcement to go round up these
legislators and drag them back to the statehouse.
That can happen.
I would be surprised if the governor of Texas is that short-sighted, because if that happens,
all that does is strengthen the Democrats' position.
It gives them a lot of street cred.
So I would be surprised if that occurs, but it's possible.
So what is this at the end of the day?
It's a filibuster.
It's a filibuster.
It's a procedural trick to slow down legislation.
That's all we're looking at.
Even the numbers are roughly the same.
Two-thirds, right, versus 60 votes.
It's the same thing.
So if you're looking at this and you're screaming, it's obstructionist, do you feel that way
about the filibuster?
Conversely, if you're looking at it right now and saying, this is preserving democracy,
do you feel that way about the filibuster?
This is a procedural trick.
It's politics.
That's all it is.
Sure, it's higher drama.
It's a bigger thing, because everything is bigger in Texas.
But when you really just sum it up, it's a procedural trick to slow down legislation.
So what are the Democrats in Texas hoping to accomplish?
Do they think they're going to stop this bill?
I doubt it.
I doubt it.
I don't think that's the goal.
I don't believe that they really think that this is going to completely stop this bill,
because the governor can just call for another session, and they'll be right back where they
started.
I mean, sure, maybe they'll flee the state again.
This could go on for a while if they wanted to.
But when it's all said and done, odds are Republicans will be able to push through this
bill.
So what are they doing?
Why are they expending all of this political capital in hopes of gaining more?
This is an Alamo moment.
They know they're not going to win.
They know they're not going to win.
It's a holding action.
They're hoping to give their allies, the federal government, time to push through a voting
rights act, some kind of legislation to kind of nullify what states are doing in their
attempt to discourage people from voting.
That's what's happening.
I wouldn't count on a win from Democrats in Texas, but it may buy enough time to get something
done at the federal level.
That's probably what we're watching.
But while it is entertaining and there is lots of high drama, just remember there isn't
a whole lot of difference other than travel expenses between this and the filibuster.
Kind of works the same way.
Anyway, it's just a thought.
Y'all have a good day.