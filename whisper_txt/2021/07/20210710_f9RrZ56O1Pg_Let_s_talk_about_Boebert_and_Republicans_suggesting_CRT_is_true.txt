Well howdy there, Internet people. It's Beau again. So today we're going to talk about
Lauren Boebert, Representative Lauren Boebert from Colorado, and the discussion that she
has sparked. To catch everybody up if you don't know what's going on, the Zen Education
Project put out a pledge for teachers to take. Republican outlets, conservative outlets,
they saw this pledge and they ran with it. Headlines, teachers defy state legislators,
CRT will be taught, da da da, stuff like that. Republican politicians then went on the offensive.
Representative Boebert suggested that 5,000 teachers be fired over this. Mass firings
of teachers is not normally a good idea. But anyway, from there, Democrats generally pointed
out that Boebert has a GED. Yesterday when I talked about Democrats being elitist, I
got a bunch of questions. What do you mean? This. Exactly this. I'm not saying that I
would choose Representative Boebert to be on my team during a trivia contest. What
I'm saying is that equating intelligence or education level even to credentialing isn't
a good idea. I know a whole lot of people don't even have a GED that are way, way smarter
than Boebert. And when this comes out and it hits areas where a lot of people leave
school early and go to work on the farm because they know that's going to be their job, it
comes across as very elitist. Now, if you are going to criticize her for being a high
school dropout, I would strongly suggest you invest in grammatic. I'm going to read you
something from Raw Story. Representative Lauren Boebert, Republican Colorado, called for mass
firings which brought out the critics who pointed out her own educational history which
included being a high school dropout. Educational credentials and intelligence or how smart
you are, they don't go together. Not really. Not only is this line of attack very elitist
and alienating to a lot of people who could align with the Democratic Party for a little
bit of effort, it's also not the most important line of attack. It doesn't even make any sense.
I would strongly suggest the fact that Republicans are kind of suggesting that CRT is true. That
seems like it would be more important to focus on because the pledge was not, I'm going to
teach CRT. The pledge was, we the undersigned educators refuse to lie to young people about
US history and current events regardless of the law. So for Republicans to read this and
say, hey, they're going to teach CRT, they are saying CRT is true. That seems like it
might be worth pointing out. I'd also suggest that a sitting congressperson calling for
people to be fired because they exercise their right to free speech, that might be
getting really close to a First Amendment issue, especially when the pledge is structured
the way it is. Because immediately before the part I read, it says, two state legislators.
This is a petition. A petition for a redress of grievances. Something that is explicitly
authorized in the US Constitution. So those would make better lines of attack than saying,
ha, she got her GED. People asked for an example, and here's a really good one. Stuff like this
will alienate rural Americans and probably other demographics. I just know that it's
something that really bothers rural Americans. So I would be cautious of equating any educational
credential with intelligence, actual education level, experience, anything like that. And
it goes both ways. I know people who do not have a GED who are incredibly intelligent.
I know people with a PhD who couldn't find their way out of a mall. Anyway, it's just
a thought. Y'all have a good day.