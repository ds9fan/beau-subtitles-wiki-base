Well, howdy there, internet people.
It's Beau again.
So masks are back in style.
I mean, that's cool.
Mandates are back as well.
I know I think Nevada started this morning.
There are a few other places,
and there are more on the way without a doubt.
So I think that the messaging that is going out with this
is a little off.
So I'm gonna try to add my own.
Most people watching this channel,
you're fully vaccinated.
And yeah, it's true.
The risk to you, pretty small.
It is pretty small.
The vaccines are incredibly effective.
They're doing what they're supposed to.
The odds of you having a negative outcome are pretty slim.
Okay?
Admittedly.
But because of that, I think there are a lot of people
who may be a little lax.
Because after a year,
I think that there are more than a few people
who are just like, you know what?
Welcome to the consequences of your own actions.
You chose not to get vaccinated.
You chose not to wear a mask.
It's on you.
And I get that.
I get that.
And all the precautions that you're gonna take
if you're fully vaccinated,
at this point, it's really to protect them.
Fact.
It's to protect them.
Because while it is incredibly effective at protecting you,
there is the worry that you still may be able to transmit.
So given the fact that many people are just fed up with it
and are ready to let people deal with the consequences
of their own actions,
I'm gonna give you a motivation to pay attention,
to do all of the stuff you're supposed to,
to not get lax just because you know you're protected.
There are people out there who have a medical condition
or have some other valid reason
as to why they can't get vaccinated.
If you don't want to do it for the people
who were really annoying over the last year,
do it for them.
Do it for those people who don't have a choice.
They're at risk and they're gonna stay at risk
through no fault of their own.
The reality is, yeah, at this point,
your actions are there to protect the people
who refuse to protect themselves.
That's why you're wearing a mask.
But there are some who really don't have that ability.
So if you get to the point where you're just like,
you know what, it's on them,
remember those people who legitimately can't
take all the precautions that we were able to take.
This is typically how it goes historically
with events like this.
There are peaks and valleys
because there are always those people
who do not want to listen to people
who know what they're talking about.
And they end up paying most of the price for it.
Just go back to the way it was back when it all started.
Wash your hands, don't touch your face, wear a mask.
I would also point out that while the guidance
isn't coming out saying this,
it would probably still be a good idea
to socially distance, to stay apart a little bit.
When they're saying wear a mask,
I don't see that advice being given.
It probably should be
because that's what the data indicates.
So I know it's annoying
and I know at this point there is a reluctance
to go out of your way to protect people
who absolutely refuse to do the bare minimum
to protect themselves.
But just do it for those who can't.
Stay on your guard to protect those around you.
Anyway, it's just a thought, y'all have a good day.