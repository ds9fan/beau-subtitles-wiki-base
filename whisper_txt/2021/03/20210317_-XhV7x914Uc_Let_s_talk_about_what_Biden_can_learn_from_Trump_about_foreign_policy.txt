Well howdy there internet people it's Beau again. So today we're going to talk about what
President Biden can learn from former President Trump about foreign policy
it's not a joke. I know people probably thought that was a typo in the title.
So Trump's foreign policy was not good. It was bad. It was really bad. It was basically an
unmitigated failure in pretty much every regard. However he attempted to do something and
although it failed because he didn't have the skill set or the base of knowledge to make it work
it was a good idea. High-level talks with North Korea. Now I don't think that Biden actually has
the base of knowledge or skill set by himself to make it work either. However Biden has a really
good foreign policy team behind him so he might be able to. For a very long time the United States
has insisted on a precondition getting rid of that program. That program that North Koreans have.
It has to go away before anything can happen. That's bad. That's not a good that's not a good
stance and we have held it for a very long time and it has produced absolutely no positive results.
North Korea isn't going to do it. They have no reason to do it. If you look at countries that
went that route with the United States, most of them got toppled shortly after they gave up their
programs or the U.S. just pulled out of the deal. They have no reason to believe the U.S. is acting
in good faith because the United States is probably not acting in good faith. Now before we go further
we should get rid of a little bit of fear that constantly gets put out. The United States is
not going to invade North Korea. Has no desire to. North Korea is not going to attack the United
States. That drama, that's not real. That's all posturing. The U.S. won't invade North Korea
because for a little more than half a century they have been preparing for it. They're ready for it.
It's not a position the U.S. wants to be in. Aside from that, they have China as an ally.
North Korea doesn't want to attack the United States because although they know we won't invade,
we do have a lot of aircraft and if they start it, well China may decide that they need to
exert just a little bit more control over North Korea and they don't want that.
It's a stalemate and it has been a stalemate for a very long time. That's not a realistic flashpoint.
Not really. So with it being kind of in gridlock, there's no real likelihood of it moving towards
conflict, which means there's a lot of free area to move into moving towards peace.
The first step is the United States dropping the idea that getting rid of that program has
to be on the table. It doesn't. We have plenty of time because however long we continue to insist
on getting rid of the program as being some kind of precondition, that's just however long it's
going to take until there's any real progress even started. They aren't going to agree to it.
It's not going to happen. They have no reason to believe we're acting in good faith because
we're probably not. So what do we do? What's the best move? Especially now that the United States
is retooling to face near peers, one of which is China. North Korea knows this. North Korea
is aware that the United States isn't going to want to create a flashpoint where none exists
with China. So they have an even freer hand. Talks, high-level talks. Now you don't have to
salute North Korean generals or offer them a ride in Air Force One or any of that stuff,
but high-level talks with the North Korean government over nothing, over minor stuff,
fishing rights. Like it doesn't have to be anything related to the military. Just to show
that dialogue can produce results because so far they haven't had a lot of signs that it can
because we've insisted on something that they're not going to do.
Kim Yo-jong, who is Kim Jong-un's sister, is a very powerful woman in our country.
She's the de facto number two, really. She just made a statement directed to the United States.
It wasn't very complimentary, but it doesn't matter. It opened the door to conversation.
Now it shouldn't be Biden, just like it won't be Kim Jong-un, because he's met with a president now.
So it's going to have to be a head of state who talks to him. However,
their number two talking to our vice president very openly would probably produce a lot of results
if getting rid of that program wasn't the main focus of it. There's a whole lot of other issues
in the area and it could start with that. And yeah, this isn't going to result in getting
rid of nukes anytime soon. It's going to be a long process, but at least it would start the
process, because right now we're not even remotely moving in that direction.
So that would be a move I'd watch for is the start of high-level talks. Now we don't know.
Biden's team is currently reviewing Trump's policy, or lack thereof, when it comes to North Korea,
and they're trying to come up with an official stance. There's already been some little setbacks
in that road, but they're hoping to come up with some kind of centralized policy. I would hope
that it includes talks that don't have the precondition of getting rid of that program,
because if they want to have any real progress, that's what they're going to have to do.
Anyway, it's just a thought. Y'all have a good day. A couple of y'all have commented saying that
I seem down. I'm actually a little under the weather. I'm not like emotionally distraught
or anything. I just don't feel well. Anyway, y'all have a good day.