Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about whether life imitates art
or art imitates life in Muncie Central High School
and some classwork that got assigned there
and the completely predictable chain of events
that arose from that classwork.
A teacher there was teaching the story of V for Vendetta.
Assigned classwork, assigned a project.
This led to students producing posters
and these posters drew parallels
between modern social movements
and the plot from V for Vendetta,
which I mean, yeah, that makes sense.
Kind of the point.
These posters then went up in the hallway
where the establishment had an issue with it.
It hurt their feelings.
According to reporting,
school resource officers didn't like it.
That led to a discussion.
That discussion led to the teacher being asked
to take the posters out of the hallway.
That predictably prompted a student-led demonstration,
which then led to the establishment
putting the students on remote learning, e-learning.
If only there was some book that could have predicted
this exact chain of events.
I don't actually expect the people
who are in opposition to this to read.
So watch the movie.
And when you realize one of the most poignant scenes
in the movie is the part where somebody says something
about the establishment
and the establishment gets his feelings hurt,
and then a bunch of militarized goons in fatigue
show up and corner them,
maybe you'll start to see some parallels.
I guess we're thankful nobody got black bagged.
Once you're done processing that scene,
review the footage from the hallway again.
Nice pants, by the way.
You look like real warriors.
Act like it too, because when I think warriors,
I think picking on teenagers and teachers.
Of course the students see similarities.
You're cosplaying it in the hallways for them.
The opposition to this being taught suggests,
oh, well, see, they're being indoctrinated.
No, the students are drawing their own conclusions
based off of what they see and what they hear,
and you are reinforcing it every step of the way.
You're following it like it's a script.
Of course they see parallels.
They exist.
They see the parallels because they're not indoctrinated.
You don't see them because you are indoctrinated.
You are playing the role of the bad guy.
Stuff in the hallway isn't the only similarity,
because see, in this analogy, the students,
well, they would be the citizens.
And in the story, the students have this protest,
or the citizens have this protest,
and the establishment puts them on remote learning,
puts out a curfew.
It's the exact same thing.
It's the same thing.
Of course they see similarities.
Of course they do.
You're playing it out for them.
I'm sure the goal of this was to cast doubt
on what they have seen with their own eyes
to try to indoctrinate them.
It's not working, because to the contrary,
you're confirming everything they believe.
This might be the time for the school there
to try to diffuse this, to acknowledge, hey, you know what?
You're right.
We were wrong.
We were using V for Vendetta as a how-to guide
instead of a warning.
They see what exists.
They see what's around them.
And yes, there are parallels.
They're not wrong.
You are.
The school board is.
I would point out that this type of move
to keep students ignorant is occurring in school districts
across the country.
You might want to get involved.
You might want to speak out, because this isn't isolated.
V for Vendetta is on that list of 850 books
that circulated in Texas.
When our schools are concerned about educating,
concerned about critical thought,
we have an issue that will last for generations.
Trying to stop students from thinking critically
about what they see and what they hear
and their own experiences is indoctrination.
And it leads to a society that can be led astray very easily.
Anyway, it's just a thought. Y'all have a good day.