Well, howdy there, internet people.
It's Beau again.
So today, we're going to go over what we know
and what we don't.
When I was growing up, my favorite cartoon
always told me that knowing was half the battle.
We do not have half the battle won here.
Now we're talking about the Omicron variant.
The list of information we know is pretty short.
What we don't know will fill volumes right now.
It's still very early.
So what do we know?
We know it was discovered in South Africa.
There's no evidence to suggest it originated there.
South Africa raised the alarm, said, hey,
here's this new variant.
Once that went out, countries all over the world
were like, oh, wow, we have that here too.
At time of filming, the only places
where I hadn't seen it officially confirmed
that they already had cases were Australia, South America,
Antarctica.
That's it.
Everywhere else, it's already there.
Specific countries, Germany, Italy, Hong Kong, Israel,
Canada, the United Kingdom, the Netherlands, the list.
You get the point.
With that in mind, the flight restrictions
are probably kind of pointless.
It's already spread.
It's kind of like trying to blow air into a popped balloon
at this point.
So the real question is, will it out-compete Delta or not?
That's really what's going to be the deciding factor.
The overwhelming majority of cases currently are Delta.
So the reason Omicron is a variant of concern
is because they notice mutations that they believe
might make it more transmissible.
Do they know this yet?
No.
No, they don't.
But it's kind of an educated guess.
They're acting out of an abundance of caution
as they should.
The way this chain of events is playing out
is how it should have played out when
it was originally discovered.
This is what should have happened.
Now, they don't know that it's more transmissible yet,
but they think it is.
I haven't seen anybody state a real hard opinion
on whether or not it is going to be more severe or less severe,
which is also a possibility.
So what we have right now is there's a new variant.
It's probably, based on an educated guess,
more easily transmitted.
And it's kind of already everywhere.
They don't know anything about the severity yet.
That's at time of filming.
The docs are saying it'll take two weeks
to get a really clear picture of this.
So until then, nations are going to act out
of an abundance of caution.
So you're going to see them do things,
like the flight restrictions, that
aren't going to make any sense.
At this point, it doesn't seem as though the US restricting
flights from South Africa is really going to matter,
because we're probably not going to restrict them
from all over the world.
Now, that's what nations are going to do,
act out of an abundance of caution.
What should you do?
I can't tell you that.
I'm not a doctor.
But I'll tell you what I'm going to do,
act out of an abundance of caution.
If you have spent this entire time since this began,
engaging in mitigation efforts that have worked, keep them up.
If you've relaxed, maybe you don't wear your mask as much
as you used to or something like that,
it's probably time to bring that back up to where it was.
Do all of the stuff that you can to mitigate risk,
because there's a lot that isn't known now.
It could be that this is less severe, but we don't know that.
It could be more severe.
So you want to mitigate the risks.
So wash your hands.
Don't touch your face.
Stay at home as much as you can.
If you have to go out, wear a mask and go get vaccinated.
Anyway, it's just a thought.
Y'all have a good day.