Well howdy there internet people. It's Bo again. So today we're going to talk about coats and hats
because I noticed something. I was on Instagram yesterday and I noticed something I think y'all
should be aware of. Everybody who was outside wearing coats and hats. And then since I thought
that was a little odd, I went to YouTube and looked at all the new videos. Anybody who was outside
wearing coats and hats. Like they're trying to tell us something. Doing it all at once like that.
It's odd, right? But they're not fooling me. They're not getting me. I did my own research.
I saw a meme on Facebook. I know that normally doesn't count as research, but this was different.
It had like citations down at the bottom. I didn't read them, but while I was doing my meme research,
what I found out was that only 1,601 people on average die every year from hypothermia.
1,601 people. That's not a lot of people. You got a 99.99923% success rate with that.
That's probably not going to happen. You're not going to infringe on my freedom and make me wear
a coat over that. No. Uh-uh. And here's the weird part. If you look into that 1,601 number,
if you look into that, what you're going to find out, most of those people, they were wearing coats.
Coats don't even work. It's a myth. Something they made up to sell you stuff. And if you tell
people that and you're like, hey, I'm wearing a coat, but I'm still cold, what do they tell you?
Dress in layers. Get a booster for your coat when the coat didn't work to begin with. Again,
trying to sell you something. See, they're not getting me. I'm no sheep. I don't need a wool
coat and feel all warm and snugly. Something else you may not know is if you rearrange
the letters in the word coat, it'll spell taco and tacos will keep you warm. See, it's all a game.
It is all a game. Out to sell you stuff. All these people wearing coats and hats on Instagram
and YouTube, shills for big jacket. All of them. So one of y'all, thank you to whoever it was,
posted my last video on the new variant on a forum full of some very interesting people who
inundated me with messages. Yeah, so there's that. To answer the questions that were posed in a few
of them, rather than just show you how silly you look by saying some of this stuff. No, there isn't
a giant conspiracy for everybody to start talking about the new variant at once. It's kind of big
news and we're in the middle of a global pandemic. That's why it became a topic of conversation.
As far as when I will stop promoting vaccines and masks and general hygiene, because apparently
that's an issue too. I don't know when the lethal virus is kind of under control. Maybe then,
but probably not until then. This isn't actually a complicated subject when it comes to masks
working. You've known since you were a kid and your parents kept telling you to cover your mouth
when you cough or sneeze, that putting something in front of your mouth and nose when you cough
or sneeze reduces spread. You don't need a PhD or a Facebook meme to tell you this. This is
common sense, much like wearing a coat. Anyway, wash your hands. Don't touch your face. Stay at
home as much as you can. Wear a mask if you have to go out. Get vaccinated. Get a booster and don't
forget to bundle up. Anyway, it's just a thought. Y'all have a good day.