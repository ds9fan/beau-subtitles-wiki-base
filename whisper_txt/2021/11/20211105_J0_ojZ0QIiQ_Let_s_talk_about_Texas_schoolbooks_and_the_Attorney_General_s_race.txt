Well howdy there internet people, it's Beau again. So today we are going to talk about
Texas and school books and the attorney general's race because they all kind of
blend together in this story. You have a state representative there who sent out
letters to schools basically asking, hey do you have these books? What are you
doing with them? How much did they cost? So on and so forth. It's a list of eight
hundred and fifty books. Now the reason, stated reason, is he's worried that they
might make students feel discomfort, guilt, anguish, or any other form of
psychological distress because of their race or sex. When you hear that, I mean it
sounds like it would be a list of books that are bigoted. And then you look at
the list of books. What is white privilege? Hood feminism. Jane against
the world. That's about a pretty famous court case. The new Jim Crow. V for
vendetta. How to be an anti-racist. The Indian Removal Act and Trail of Tears. So
you want to talk about race, identity, and gender. Pink is a girl color and other
silly stuff people say. This book is gay. Separate is never equal. And my two
favorites, Cider House Rules and We Are All Born Free. If you don't know that
last one, it's a book about the Universal Declaration of Human Rights and it's put
out by Amnesty International. This is a pretty good sample from the list of
eight hundred and fifty books. They are all books that would give a voice to
students who maybe feel a little marginalized. Maybe feel like they don't
belong, are concerned about who they are, who they're going to be. Stuff like that.
So what's this move about? I mean does it make sense politically? Only in the most
cynical of ways. When you're talking about policy, I don't think this is
designed to go anywhere. I don't think that this state representative, I can't
remember his name, actually intends on getting rid of these books. I think it's
just designed to get his name out there. To create headlines, to create buzz. I
mean this isn't a demographic that's huge. People that would support this, if
you really think about it, who would be behind this move? The only people I can
think of that would support this would be bigots who also happen to be bad
parents. Seems like a pretty narrow demographic to cater to. But if you
create a bunch of headlines, well maybe you can coast into that Attorney
General's office just on name recognition. That's what it seems like.
I can't remember his name. And I say bigots who also happen to be bad parents, because to
support this, getting rid of these books, yeah you'd have to be pretty bigoted.
And to be concerned about your kids being exposed to, I mean some pretty
basic stuff in most cases, you'd have to be concerned about your ability to
impart your bigoted values to your children. I mean that's really what it
seems like. I encourage my children not to be bigots. However, if they're exposed
to a racist character in a book, or they see a racist on TV because they walk
into the room while I'm watching C-SPAN, I don't panic. I don't think that
exposure is going to alter their moral values, because I try to instill my
values in my kids. I mean this honestly seems like a political move designed to
appeal to people who can't even teach their children to bigot right. Doesn't
seem like a large voting bloc. That's why I say it's about getting name
recognition. It's about getting his name out there. I still can't remember what it is. But he does
want to be Attorney General. He wants to be Attorney General. You know the funny
thing is, while I can't remember his name, I happen to have run into somebody also
running for Attorney General in Texas. A Democrat. His name's Lee Merritt. Two R's,
two T's. Literally walking down the road, and me and some other people, and we wound
up meeting him. Prompted a 15 to 20 minute conversation. Very impressive man. Very
impressive man. So impressive that I followed him on social media, and because
of that I happen to know that on November 10th in Austin he's holding a
meet-and-greet. So if you live in Texas and you want to get to know a candidate
who I can assure you would not support this, there's one you can meet. When we're
reviewing stuff like this, and we're reviewing the moves of the
Republican Party as they just lockstep into the 14 characteristics here, we need
to remember that in today's age they don't have to burn the books. They just
remove them. Now if you are a student in Texas and by some weird chain of events
this actually goes somewhere, and these books are removed from your school, you
can't find them in the school library, whatever, remember banned books are the
best books and they will certainly be available at your public library. If they
don't have it on the shelf, they can reach out to another library and get it.
Can't remember his name. Anyway, it's just a thought. Y'all have a good day.