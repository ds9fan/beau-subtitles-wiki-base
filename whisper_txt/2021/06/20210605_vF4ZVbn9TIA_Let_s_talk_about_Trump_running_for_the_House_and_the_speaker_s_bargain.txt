Well, howdy there, internet people, it's Beau again.
So today we are going to talk about an idea that apparently former
president Trump is kicking around.
It's the idea of running for the house, running for a house seat, and
then becoming speaker of the house.
That's a concern more so than you might think.
Not too long ago, one of y'all sent me some information on a pretty counterintuitive theory.
And the theory is basically that transparency, when it comes to how your representative votes,
well that's actually bad. You shouldn't know how they vote.
And nobody else should either, because it eases corruption
if lobbyist or the speaker knows how the different
representatives vote.
It's an interesting theory.
I'm not completely sold on it, but it's an interesting
theory, as put out a person I saw talking about it was a
Harvard-based researcher, James D'Angelo.
low. So he provides this example and he calls it the Speaker's Corrupt Bargain.
Let's say you're a newly minted representative. You just get to
Congress. You show up and you are there to do what your constituents want you to
do. You are there to be the idealistic representative and all your constituents
care about is getting more pizza. I don't remember what the exact example he
used was but let's just say it's getting more pizza. So day two you walk into the
speaker's office and you're like hey I need to get this bill about getting more
pizza I need to get that you know voted on and the speaker's like you know my
calendar is over there and it's full why don't you come back in a couple months
and we'll see what we can do.
Until then, just go be a congressperson, go vote.
Speaker encourages them to read all the laws
and vote their conscience.
Couple months later, that newly-minted representative
comes back.
OK, well, I did all that, you know.
let's get my bill voted on.
The speaker's like, you know, see, here's this weird thing.
You voted against me, voted against my interest 62%
of the time.
Now, this representative from Wyoming over here,
they voted with me 86% of the time.
Hm.
Why don't you come back another couple months?
We'll see if we can find a spot for your bell then.
And this is how the concept that all politics is a local went by the
wayside because all politics became national.
Imagine a former president Trump with that kind of power that undoubtedly
exists today, that type of behavior exists today.
Imagine that kind of power in Trump's hands.
This might be even more concerning than a second presidential run.
Because if he becomes Speaker of the House, he has the ability to bring all
Republicans in line, has the ability to measure their votes and to purity test
those he doesn't like out. Incidentally, this is probably why your favorite
progressive representative becomes less progressive once they get up there
because they have to make compromises to get the bills they really care about to
the floor. That's how it works. It's interesting and it's something that we
should definitely pay attention to especially if somebody like Trump is
looking to further their brand by getting into the speaker's chair. Anyway
It's just a thought. Y'all have a good day.