Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about Gwen Berry.
If you don't know, person qualified
for the US Olympic team, national anthem.
And she did something completely unprecedented,
something no athlete has ever done before.
She took that moment to draw attention
to systemic issues in this country.
And she did it for the first time on Monday.
Now, this didn't happen today.
But many of our politicians have turned into 1990 shock jocks.
So they held it for Monday so they
could generate the most outrage.
Because that's what they're about now.
They're about generating outrage,
not about generating policy.
I would like to clear a few things up.
First and foremost, that flag, it's a piece of cloth.
It is a piece of cloth.
In and of itself, it means nothing.
It's a symbol.
It's a symbol of the ideals this country
is supposed to represent.
It's the symbol of a representative democracy.
If you have spent the last year attempting
to undermine the ideal of a representative
democracy in this country, you don't
get to pretend to be mad about this.
You've already shown us that you don't care about those ideals.
To you, it's not a symbol of those ideals.
It's a bumper sticker.
It's something you can rally your base behind.
That's it.
Nobody cares about your fake outrage.
This is doubly true if you are a politician in the United States,
if you are somebody that has the ability to influence policy,
if you are a member of Congress.
You don't get to pretend to be mad about this,
because this isn't new.
These protests, these events have been going on for years.
You're their audience.
If you're a politician in the United States,
they are aimed at you.
They are trying to get your attention
so you will address some of the systemic issues
in this country.
And you have done nothing.
You don't get to complain about it.
You don't get to pretend to be upset,
because you have done nothing.
Instead of realizing that there are issues in this country that
need to be addressed by you, you would rather
go on some pundit show and stoke outrage.
Rather than unite the country, you
would divide it, because it's better for you,
because you don't care about the country.
You care about your own political career.
Now, I had somebody ask me point blank
if I think the tactics, the way she did it, if I liked that,
or if I liked kneeling.
I think it's better to kneel.
Personally, I think it makes better imagery.
But I would point out a number of things about that.
One is that, well, she didn't ask me.
She worked very hard over the course of her life.
And I would imagine that she knew
what she was going to do when put in this situation.
I'm not going to second guess that, really.
I would also suggest that maybe she
doesn't want to get on the ground around a bunch of people
who might become angry at her exercising the principles
enshrined in the Constitution.
They might decide to beat her with the flag.
If you said nothing about a person being beaten
with the flag at the Capitol, if you've
attempted to downplay that event,
you're going to pardon me for not taking
your advice on flag etiquette.
Because again, you don't actually care.
It's not something that bothers you.
It's just something you can use to make Americans angry,
to make them afraid.
Rather than addressing the issues,
you are making them worse.
Anyway, it's just a thought.
Y'all have a good day.