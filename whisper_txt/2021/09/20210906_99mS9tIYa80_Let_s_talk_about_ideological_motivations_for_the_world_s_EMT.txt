Well, howdy there Internet people. It's Beau again. So today we are going to talk about the
ideological motivations
for people who are
looking at that world EMT idea and saying, that's a good idea.
Because the examples I chose that I used in the other parts,
these are not people you would expect to agree on a foreign policy issue.
They're very different.
If you look at the
love and peace will conquer all crowd,
they support this because
this is a way to avoid war.
That's always good.
So the Marianne Williamson, right?
This avoids war. It avoids unnecessary loss. That's a win.
So that's why they support it.
Then you look at people like Thomas Barnett, right?
Thomas Barnett is probably somebody who views war as another tool in the foreign policy toolbox.
Why does he support it?
Because it's effective.
It's a way to pursue American national interests
without war
and to lessen the cost of war
to the United States if it does happen.
These are both
motivations that deal with what is.
The way the world is right now.
However, if you're watching this channel, odds are you don't like the way it is. That's why you're watching it, right?
I don't either.
If we want to
change the world and we want to
get to that world where everybody's going to get a fair shake,
we have to acknowledge it's a global game.
We can't just do it in the United States.
We talk about income inequality in the United States all the time.
And yeah, it absolutely needs to be addressed, no doubt.
But there's
economic inequalities
the world over.
This is a way to repair that. This is a way to fix it.
There are a lot of countries all over the world
that are less than stable,
that have low standards of living,
that are, for lack of a better word,
poor countries, right?
But they have no reason to be poor.
You have countries in Africa, right?
They're wealthy countries.
They are wealthy countries. They should be,
but they're not.
Africa is going to be the new Middle East.
We're going to spend a lot of time there. The United States, the West in general,
and the East too.
Africa's going to be there a lot
because of the
resource wealth that exists, right?
If we don't change our doctrine,
it's going to look exactly like the Middle East.
We're going to be there constantly,
and there's going to be a bunch of loss, and at the end of the day,
the people there are going to be no better off.
However,
if we shift our doctrine,
and the idea is to build these countries up
so they are stable on their own,
and they're pursuing their own interests,
they don't get their wealth extracted anymore.
We engage,
the West
and the East now.
We engage in this soft form of colonialism.
We don't go in and plant our national flag and say, this is ours now.
No, we show up with corporate logos on business cards and work out deals,
but the same thing occurs.
We extract the wealth,
which leaves those countries with a low standard of living
when they don't need to have one.
Their standard of living can be raised.
The global income inequality can be addressed,
and it puts us on that path
to getting to a point
where
we can
get to where everybody's getting a fair shake.
You know,
there are people who would say, you know, this kind of sounds
like you're not really wanting to
consider the opinions of those in the country.
Now, I do.
I think that
when we would go in to
help a nation, we would actually be going in to help it.
They would determine their system of government. They would determine their
economic policies.
Initiatives like this,
they're always going to carry
U.S. national interests with them
because they're always going to get co-opted.
Because of the state of the world today,
if you want to get
to that
world where everybody's getting a fair shake, it has to start in the United
States.
It has to start here.
If it starts anywhere else
where people start talking
in the idea of
getting to a world where everybody's equal,
the United States would invade them.
It has to start here.
This is a way.
It's incremental, sure.
It's not super radical,
sure, but it'll work and it could be implemented like tomorrow.
And it would better the standard of living
for millions upon millions of people
and reduce war.
I will take
that
over the promise of something
more radical occurring twenty years from now.
I would prefer to have that today or tomorrow
than some dream.
This is a
doctrinal shift that could occur in American foreign policy and in
international foreign policy as a whole
that unless you are just
an overt, ardent nationalist
that truly believes exploiting everybody all over the world is a good thing,
you can get behind it.
There's not an ideological
system
that would oppose this
with the exception of those who really are colonialists.
It would work.
That's the other thing.
The reason you have so many people from so many different backgrounds and
different ways of looking at the world coming to the same conclusion
is because it's pragmatic.
It would work.
And I think that's
probably the biggest selling point right there, is that this would actually
function instead of creating
twenty-year
missions that
don't end well.
Anyway, it's just a thought. Y'all have a good day.