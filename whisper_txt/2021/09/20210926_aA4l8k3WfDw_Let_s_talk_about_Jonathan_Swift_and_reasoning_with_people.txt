Well howdy there internet people, it's Beau again. So today we're going to talk about
Jonathan Swift and reason. We're going to do this because there's a quote of his
that is very popular right now because it suits the times and it allows people
a shield. And we're going to talk about that because somebody sent me a message
saying, hey I see a lot of Jonathan Swift in your work. I'm just wondering how
you can justify ignoring his number one rule, which is it is useless to attempt
to reason a man out of a thing he was never reasoned into. Meaning that if
somebody is believing something without reason, they didn't put any thought in it
to it to begin with, they didn't apply logic in the beginning, you can't get
them out of it with reason and logic. Okay so how do I justify not going by
this rule? It's wrong. It's that simple. I don't believe that. I don't believe it's
true. He was, he engaged in a lot of biting humor and this was probably more
of a statement that indicated that he believed that fiction can get to truth,
which is something I believe. But the idea that reason and facts can't
counteract blind belief, well that's just not true. In fact I'm fairly certain
there are a lot of things you may have believed as a kid that you don't believe
now because you applied reason to them. I think that this is a popular quote
right now because it is so hard to reason with certain demographics in the
United States right now. And you can throw this up as a shield and just say,
yeah you know what, there's no point in even trying. I don't believe that. There's
no evidence to suggest that that's true. People can be reasoned with, most, not all.
Sure there are some that are just stubborn and they will never let go of
an unreasonable belief, but I would say that for most people they can be
reached with reason. You just have to find the right combination of reason and
fact and truth and maybe a little fiction to throw in there to get the
point across. Historically that's been proven to be true. Anybody who has ever
had a phobia that they lost can say that reason can get you out of a belief you
weren't reasoned into. And if you have any doubt about whether or not this
quote is accurate, whether or not this rule is something that should be
applied, if suddenly you're like, well okay maybe it's not true, you are
evidence that you can be reasoned out of a position you were never reasoned into.
Because you heard the quote, it made sense, you applied it, you used it, and now
you're doubting it through the application of reason. It's hard when
you're talking about certain demographics in the United States right
now, no doubt, and it can be frustrating. But we have to try. We don't have
an option because we're not going to get them out of their
position by appealing to their unreasonable nature because that's just
going to drive them further into salad dressing or whatever. So anyway, it's just
a thought. Y'all have a good day.