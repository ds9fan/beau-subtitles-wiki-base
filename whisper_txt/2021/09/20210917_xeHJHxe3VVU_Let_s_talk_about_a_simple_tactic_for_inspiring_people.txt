Well, howdy there, internet people.
It's Beau again.
So today, I'm going to tell you a story.
I'm going to tell you about a guy.
He was an instructor.
He was a teacher.
And every single person that I have ever met
who was instructed by this guy said
that he was just inspiring, inspiring.
He was somebody who was foundational to them changing
and to them really seeking to strive to hit that next level.
And I tell you the incredibly simple tactic
he used to instill that in people.
He was an instructor at Fort Benning.
And it didn't matter who you were.
Didn't matter who you were at all.
He called everybody Ranger.
Everybody.
Somebody walked up, needed something.
What you need, Ranger?
Everybody.
Now, keep in mind, a lot of these guys,
they're not even soldiers yet.
They haven't even been through the training
that would allow them to legitimately
call themselves soldiers.
And he's calling them Ranger.
Now, if you don't know, becoming a Ranger is not an easy task.
And at Fort Benning, oh, it's a big deal.
I mean, it's a big deal anywhere,
but especially at Fort Benning.
Because all these new recruits, they know.
They know that they're there.
And that expectation gets set.
He puts it out there that he believes that every single one
of them can make it.
Every single one of them has that capability.
He set that expectation just by using that word,
by treating it as if that's what they wanted.
And not just was that what they wanted,
they could achieve it, so much so that he was already
calling them that.
And I'm not joking when I say that every single person I know
who was instructed by this guy mentioned it at some point,
to the point where I don't even know this guy's name.
But I know a whole bunch of people
who have talked about, you know, the guy that
called everybody Ranger.
It's incredibly effective, and it doesn't take a whole lot.
The reason I'm telling you this is because somebody asked me.
They're like, how do you expect people to understand
that what Millie did was right, when a whole lot of them
won't even admit that what Trump did was wrong?
This is why.
This is why when I make videos like that,
I come from a place of assuming that that group of people who
is tuning into this channel to get an alternate viewpoint
are doing so because they want to get better.
They want to better themselves.
They want to hit that next level.
Coming from that assumption, just like that instructor,
I know statistically speaking, most of them
aren't going to make it.
But I want to set the expectation
that that is what they want to do and that they can.
So that's the reason I phrase stuff the way I do.
When it comes to trying to be encouraging and just operate
under the assumption that the people who will never admit
that they were wrong are just misinformed
and that they want to get better,
they want to dig themselves out of the information
silo that they're in, I want to set that expectation.
I understand that statistically speaking, most of them won't.
But it doesn't cost me anything to come from the point of view
that they can and that they want to.
Yeah, sure, I will be let down a lot.
But I'll also be surprised.
And I'm OK with that trade.
When he was doing that, calling a whole bunch of people,
Ranger, constantly, he knew that the overwhelming majority of
them would never get that tab.
But he wanted to inspire them.
He wanted to set the expectation that they could
and that they should.
Anyway, it's just a thought.
Y'all have a good day.