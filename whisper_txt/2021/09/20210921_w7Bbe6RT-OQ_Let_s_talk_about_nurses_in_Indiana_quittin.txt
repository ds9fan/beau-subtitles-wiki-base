Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about numbers and nurses
and how things may not be what they seem.
And we're going to discuss whether or not
there is any actual debate among doctors and nurses
about whether or not the vaccine is the right thing to do.
And we're gonna do this because I got a message.
Beau, you always make it seem as though there is no debate
among doctors and nurses about whether or not
people should take the vaccine.
You do this intentionally.
I would love to see you talk about the 120 nurses
who just quit in Indiana
because they refused to get the vaccine.
Let's talk about that.
Okay, let's talk about it.
Let's start with, that's not what happened.
That's not actually what occurred.
Indiana Health University, okay,
they have 125 part-time employees
who refused to provide documentation
saying they had been vaccinated.
That's what we know, okay.
There is absolutely no indication
that these 125 employees are nurses, none.
That's not in any of the reporting.
They could be housekeeping.
They could be working in the kitchen.
We don't know, okay.
There is also zero indication that the reason
they didn't provide proof that they've been vaccinated
had something to do with them believing it didn't work.
That's also not in the reporting.
That's a huge jump.
Okay, so now that we have that part out of the way,
the quick fact check on the claim,
let's just pretend it's true.
125 part-time nurses, they said, nope, I'm not taking it.
And they're doing it because they don't think it works
or they have questions or whatever.
125, when you say 125, that sounds like a really big deal.
And if you're talking about like a hospital,
like the one that serves me and the counties,
plural, around me, 125 nurses,
yeah, that would be a big deal.
I mean, that's like a third of the staff.
There'd be pandemonium in the streets.
However, if you are talking about the outfit
you are talking about that has 34,000 employees,
all of a sudden 125, that doesn't seem like a big deal.
In fact, it kind of makes the exact opposite point.
Even if you go with the claim that these were nurses
and they made this decision based on some kind of question
about the vaccine, it shows that 99.6 something,
more than 99.5% do agree with it.
That's a pretty overwhelming majority.
There isn't real debate about this.
This is an example of the media both sides in something
that they don't need to.
Realistically, you're talking about 125 people
that may or may not have actual questions about this
and may or may not actually be medical professionals
versus literally everyone else.
There isn't debate over this.
There's really not.
The implied debate is kind of manufactured.
You're talking about 125 employees.
Some of them may have just taken a suspension with pay
on their way to another job where they got vaccinated.
They could have decided to become traveling nurses
and come down to Florida where hospitals down here
are paying astronomical amounts for nurses
because we desperately need the help.
There could be a whole bunch of reasons for this.
Or they could just be non-medical people
who have questions because people keep saying
that people have questions.
Contrary to this showing that there is real debate
in the medical community about this,
it shows that there's not.
More than 99.5% are vaccinated.
The example you gave at that location, 34,000 employees.
Anyway, it's just a thought.
Y'all have a good day.