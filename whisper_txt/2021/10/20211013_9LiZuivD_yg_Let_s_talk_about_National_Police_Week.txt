Well, howdy there, internet people, it's Beau again.
So today we're going to talk about National Police Week.
If you don't know, that started today.
And during this time, people reflect on the numbers.
And they talk about the things that officers and deputies
face on a daily basis.
And they try to come to terms with it.
And this is normally a time when there are calls to action,
And when there is something that is posing a significant risk to the people who wear
a badge in this country, normally there's an outcry saying, hey, we've got to do something
about this.
You have people with those flags, have the little thin blue line down the middle of them,
and they want to back the blue.
I don't know where that's at though right now because the numbers, they're pretty staggering.
You know, last year, 264 cops were lost, 264, and 145 of them come from the same culprit,
the same thing spreading across this country, and they're not doing anything about it.
Now, under normal circumstances, you would have the unions, you'd have people out there
screaming.
More than half of our officers have been lost to this.
We need to have a war on whatever it is, right?
We got to find these entities and eradicate them, get rid of them.
They're bad for society.
The thing is, that was 2020, 264 loss, 145 from one thing.
That thing is the leading cause again so far this year, but there's no outcry.
The law and order party is nowhere to be found.
In fact, they seem to be siding with the opposition.
It's COVID.
COVID is the leading cause of officers being lost.
And governors who like to fashion themselves as law and order governors who back the blue
are doing everything within their power to make sure that the top law enforcement officer
in a jurisdiction cannot require their their officers, their deputies, to take
the precautions necessary. If it was gunfire, I don't think anybody would
object to a chief of police or a sheriff mandating that you have to wear a vest.
I'm pretty sure that in pretty much every department, even the little ones
around here, it's a requirement that you wear your vest at all times.
preventative measure. But not with this, right? This is different because it
became a loyalty test. Became a loyalty test and they pushed the idea of not
allowing mandates, which is fine, but they did it to such an extreme in such a way
that it undercut efforts to get people vaccinated. Led to 145 flags being in the
or being handed to somebody's family. Maybe back the blue isn't quite what they
really mean. Maybe it's just a slogan. Maybe it's just something that they say
to evoke an emotional response in people and make them think that those
politicians, those law and order governors, those are the only thing, the
only people standing between the average citizen and chaos. In fact, it kind of
feels like it's the only thing it could be because they certainly don't seem to
support law enforcement. Anyway, it's just a thought. Y'all have a good day.