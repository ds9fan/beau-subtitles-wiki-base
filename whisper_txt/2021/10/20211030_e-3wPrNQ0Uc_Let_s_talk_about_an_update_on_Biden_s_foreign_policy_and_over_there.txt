Well, howdy there internet people, it's Beau again.
So today, we're gonna talk about over there.
We're gonna talk about over there.
We haven't checked in on what's happening there in a while.
So we're gonna take a look at it, because something happened this week that
was kind of unexpected.
If you remember the coverage when all of this was fresh and new, and
everybody was paying attention to it because the United States was involved.
Two things that I kept repeating so often that I'm certain people got tired
of hearing it was that the Biden foreign policy, what he wanted was to create
three poles of power in the region and that Iran coming out was critical to
that. Iran becoming a regional player and being accepted by the international
community, that had to happen in order for his foreign policy to succeed.
And from the U.S. standpoint, that really hinged on the nuclear deal.
The other thing that I repeated myself on frequently was that the only thing that was
going to stop Afghanistan from devolving into the situation it devolved into was a token
security force to take over after the United States withdrew.
Most times I said a regional token security force.
So this week in Tehran, representatives from countries all over the region, all over the
bloc immediately surrounding Afghanistan, they all met in Iran under Iran's auspices,
that regional player. So important that President Raisi himself was going to be there and then they
had the cyber things so the vice president showed up instead. And they had Russia and China on the
phone. Well, on the webcam. That's a big deal. That is Iran becoming that regional player.
It's not under US auspices. But it doesn't really matter from the foreign policy standpoint in
In order for it to succeed and create a more stable Middle East that would allow the United
States to deprioritize and get out, and for there to be less war, it doesn't really matter
if Iran is coming out under the United States, if they're coming out with their help.
It would be nice for U.S. interests, but it's not a requirement for the overall scheme to
work.
And the subject of this meeting was to figure out what to do with Afghanistan.
Now at this point, it doesn't seem as though they're looking to put in a regional token
security force, but the goal is to establish a more inclusive government in Afghanistan.
The goal is to kind of pressure the new government into becoming more inclusive.
Now if you are part of a Western human rights group or this is a cause that's important
to you, don't get your hopes up.
It's not going to be anything near what you're hoping for.
slightly better than what exists, and if you don't know, it's not going well there.
It's not getting a lot of coverage because the US is gone, and people tend to forget
about things when the US isn't directly involved here.
So it appears that Iran is coming out in its own way, more under the auspices of China
and Russia, and one of their first tasks is trying to stabilize Afghanistan.
This is good.
Now, I'm sure that there are some people who are going to be upset because they're coming
out under Russia and China is how it appears.
When you're talking about foreign policy and you're talking about international affairs,
When you're talking about trying to keep the peace and trying to avoid war, nationalism
is politics for basic people.
Just forget about that.
It doesn't matter who they come out under.
All that matters is that they do.
Now I'm sure somebody is going to try to give Biden credit for this.
No.
This isn't a case of the Biden administration playing that international poker game where
everybody is cheating.
It's not them playing it well.
It's them going bust, getting up from the table, walking away, tripping, and finding
a few extra chips.
This wasn't a success from the Biden administration.
It just turns out that it may accomplish the same things.
It's too early to tell how successful they're going to be, either in stabilizing Afghanistan
or bringing Iran out and hopefully
stabilizing a lot of the Middle East. However,
this appears to be pretty organic
which is much better than a major power
kind of forcing it to happen. I mean not just from the whole self-determination
and idealism standpoint,
but also from
the idea that it's more likely to work if they're not being
pressured into it and it doesn't appear that they are being pressured into it.
It appears that they see the vacuum and they realize that it is good for them too.
Without the US presence there, without the presence of the same entity responsible for
a 20 year long occupation, it may be easier for a lot of this stuff to happen.
It's not going to happen under the US, but it doesn't matter, not really.
Not if the goal is to avoid conflict.
It's too early to tell for certain what's going to happen, but the moves that are being
made make it worth watching, make it worth paying attention to, because this is the kind
of information that can help you get that crystal ball thing going.
If this occurs, if this is successful, if Iran attempts to lead the way in stabilizing
Afghanistan and succeeds, they're going to be much more likely to pursue that stronger
regional role that is accepted by the international community.
So again, this is more of a sign of things that might come rather than a sign of things
that will, but it's interesting enough to mention and it's definitely something we
we should keep an eye on.
Anyway, it's just a thought.
Y'all have a good day.