Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about a situation
that is so farcical, that is so just odd,
that I don't even think you can make it up.
Even in the weirdest of shows,
I don't think they could find a way
to create a parody of this.
We're gonna talk about how former President Trump
has become the semi-reasonable person in the room in a way when it comes to the overall MAGA
movement. The story begins with a tweet from Marjorie Taylor Greene. That's the space laser
lady. I have President Trump's permission to tell you all that he is 100% against all capital
letters, the mandates, but he still encourages everyone to get the vaccine and booster. That
is his position. He also said if he was president, he would never mandate the vaccines and no one
would be fired. If you don't know, the former president is, well, let's just say getting some
pushback from his base when it comes to encouraging people to get vaccinated.
You know, I've talked about it before, he's doing it, in my opinion, for very self-serving
reasons.
He understands that it's now taking out his base.
However, the people who put their faith in him, they got pretty militant in their beliefs
So, they see this as a betrayal.
They see this as him rolling over.
The response to this tweet, if you look at the replies and the quote tweets, was basically
nothing but condemnation from his supporters, talking about how he sold him out.
He gave up.
Actions speak louder than words.
at your hotel, it's not going well.
It isn't going well.
The base that was created through the rhetoric that Trump used to gain power, well that base
is moving on without it.
And they are still very much invested in that rhetoric, very much invested in that system
of beliefs in the denial of reality, something he helped foster, something the the space laser lady
helped foster. And now they're trying to be reasonable. I mean, they're they're trying to
be self-serving in my opinion, but what they're saying is reasonable for once. Get vaccinated get
get a booster. You shouldn't need a mandate to tell you to do that. Okay, yeah, I can
get behind that. And they're meeting stiff resistance from their base. Now, the other
part about this that I find very humorous is that objectively, this is like the one
time former President Trump is actually attempting to do something that would benefit the country,
might actually help restore the United States to a better position.
One might say that as make America great again, and that's what's causing his base to turn
on him.
There's a level of poetic justice in that that I just can't wrap my head around.
Now the problem with this is that you can see two of the main figures in the MAGO movement
getting pushback from their base after taking this stance.
Those who are less concerned about the well-being of their base and would probably just like
to rely on making sure the other team doesn't vote or their votes don't count, they're
They're not going to push for the vaccinations.
They will probably continue on with the rhetoric that they have had, the rhetoric they've used,
which will leave people unprotected, will leave people without the protection they could
have if the leaders would lead.
This may be the way that somebody within the MAGA universe surpasses Trump and kind of
takes over that movement.
This is the moment where it's possible he's weak, he's in a weakened state, his base doesn't
have the faith in him that they once had, he can't sell out the seats, there's a bunch
of bad stuff going on at once.
All it takes is for somebody to capitalize on that.
Somebody who is even more self-serving than Trump to come along and say, you know what?
Medical freedom.
It's your freedom.
And sway that base over to themselves.
I can see that happening.
I would expect it to happen.
I have a couple of people I imagine are going to do it, or at least make the attempt.
Just understand that those who would do that, if you are on the Republican side of the aisle,
if you are part of the MAGA world, just understand those who would attempt to surpass him in
that way, they're copies of him.
They are copies of him.
And a copy of a copy of a copy in some cases.
I personally don't think Trump was of the best quality to begin with, but those who
are just duplicating his style and pandering back to you, they're going to be even worse.
They're going to be even worse.
They'll tell you one thing, they'll do something else, just the way Trump did.
And then they'll tell you that whatever they did, well, that's what they said they were
going to do, even though you know it wasn't.
They will continue to lie and manipulate, and it's going to have even more serious
effects on the country.
Those who might take that position, who might try to do that, are going to be even more
disingenuous than the former president, if you can believe that.
And once they have power, just like he did, he'll say the right things.
But he's going to do whatever he wants.
The most important thing for this group of people to understand is that the vaccines
work.
They provide protection.
They save lives.
They're good.
Go get vaccinated.
Anyway, it's just a thought.
Y'all have a good day.