Well howdy there internet people, it's Beau again.
So today we're going to
talk about some good news for a change.
I love it
when something good
comes from something bad.
I actually like that more than when something good
comes from something good.
Something that's good creating
good offspring.
Well I mean that's just
the status quo.
But something
good coming from something bad,
that's real change, that's real progress.
This is one of those things that came up recently
because somebody was like,
you know I just found out the origin
of the term rule 303.
That's got kind of a messed up origin. And I'm over there like, kinda? No, it's got a
really messed up origin.
And it became a good thing
because of y'all.
When y'all heard the term,
y'all made it that.
Positive change in the world.
So
to me,
I've always enjoyed
watching something transform
from something bad to something good.
Now, the
culture war
that has been going on
over statues in this country,
there's been a couple of developments.
One
is that truly offensive statue up there in Nashville. And by offensive I mean
not just the subject matter,
but also the way it looks. It was just
horrible.
Of Nathan Bedford Forrest,
yeah, that's been removed.
And it's worth noting that it was covered in graffiti
at the time it was removed.
Now,
when it comes to the culture war over statues,
the big one
is that statue of Robert E. Lee
in Charlottesville.
That was the one that created the most tension,
caused the most problems.
It was taken down as well.
That happened a while back.
But the
government just decided what they're going to do with it.
It's going to be melted down
and turned into a piece of public art
by the Jefferson School
African-American Heritage Center.
That seems incredibly fitting.
And it's under their
swords to
plowshares proposal.
Something good from something bad.
I think it's fantastic.
And I'm sure there are going to be some people
that have an issue with this.
I don't. I think it's beautiful. I think it's a wonderful idea.
It's another small step
in
the transformation of the southern United States.
This isn't just removing it
out of political pressure,
transforming it
in this way.
It sends a pretty direct message
that change is coming.
Anyway, it's just a thought.
Have a good day.