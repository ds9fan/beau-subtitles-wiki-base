Well howdy there internet people, it's Beau again.
So today we're going to talk about something I read.
I get asked a lot what I read and how I stay up to date.
I read a lot of
reports, studies, assessments, stuff like that.
I don't talk about them very often because most times they are incredibly boring.
uh...
But I read a really interesting one.
I don't know that I have ever recommended people
casually read an intelligence product before, but there's a first time for
everything.
It was put out by the National Intelligence Council, part of a series.
I think they publish them every four years.
It's mid to long term.
It's a mid to long term assessment.
This one is called Global Trends 2040,
a more contested world.
And there's some incredibly interesting stuff in it.
If you want to know
the bigger picture
behind foreign policy and domestic decisions,
you should probably read this.
Before we get into a brief summation
of some of it,
I'll go through how these are laid out because
if you've never read one before it can be kind of odd.
So they normally start off with themes
and they list off things that are going to be present
throughout
the entire document.
Common items that are going to be just throughout the whole thing.
Then it goes into structural forces.
These are
events
that are incredibly likely,
given the information we have today.
And then you have the social dynamics,
which
they have a new buzzword for it, but this is
how people respond
to these incredibly likely events.
And then they present scenarios
based on
varying degrees of how people respond and which events become most important.
This one lays out five different scenarios.
It also has five themes.
The themes
are global challenges.
That's climate change,
public health issues,
water insecurity, financial collapse, technological disruption, stuff like that.
The next theme is fragmentation
and this is occurring
at the local, nation-state, and international level
where people kind of divide off into
information silos.
Here on the internet we call them echo chambers.
Entire nations
or
security blocks
can succumb to this.
Another theme is
disequilibrium,
imbalance.
And it's not just the typical imbalance that comes along with foreign policy
or the major powers in those countries that are typically exploited
or the imbalance between international organizations and nation-states,
but also
an imbalance between
what the people of countries want
versus what
those governments can provide.
Then it has contestation.
This is
the affiliations
in this case.
This is
the way you view yourself
and what in-group you belong to. Sometimes it can be based on the way you look,
sometimes the way you think,
sometimes just the region you're in.
These affiliations are going to become more pronounced
according to this assessment.
And then the last one
is adaptation.
If there's one big takeaway here,
any nation-state that does not adapt
in the next twenty years
isn't going to make it.
The structural forces at play
are a slowing population growth,
increased migration,
climate change,
the burdens of
climate change being distributed very unevenly throughout the world,
digital currencies,
new economic systems,
and advancing tech, automation, artificial intelligence, stuff like that.
The social dynamics,
so how people respond to these events,
well, that's pessimism,
distrust,
breaking off in information silos,
and it all serves to undermine civic nationalism.
Nation-states will be dealing with these issues as well,
and that imbalance
of what the people want versus what the government can provide.
The assessment suggests
that non-state actors
will begin taking over
government functions
pretty much everywhere,
and that there will be new
and shifting
models of governance.
It's a pretty big assessment right there.
That's a pretty bold prediction.
On the international scene,
we still see China and the United States as the dominant powers in most scenarios,
but there are several other poles of power as well.
All of this information
gets compiled,
and they take it, and they use it to create five different scenarios.
I'm going to give you the names of the scenarios, because I'm assuming if you've watched this far into it,
you're probably going to read it.
It's like a hundred fifty pages, but
most of it's graphs.
The first scenario is called the Renaissance of Democracies.
This is Cold War 2.0.
The next one is
a World Adrift,
and this is the international order just isn't working,
and it's just kind of a mess.
The third is competitive coexistence.
This is what the United States and China had
prior to
Trump trying to get a better deal.
The next is separate silos.
The poles of power in that are the EU, China, Russia, and the United States.
And the last one is tragedy and mobilization,
and that's as bad as it sounds.
The real key takeaways here
is that over the next two decades,
we have to be prepared for change,
and if we're smart,
we're going to have to direct it,
because really when you're talking about shifting modes of governance
at this level,
it seems to me,
reading between the lines in this assessment,
is that we're going to move to social democracy,
or we're going to move to something that is
incredibly authoritarian.
It is worth reading.
If you are somebody who is active
and likes to fit into the bigger picture,
likes to understand what's going on,
why decisions are being made, why some things are being ignored,
this is worth reading.
It's called Global Trends 2040,
A More Contested World.
You can find it using a search engine. I will try to put a link down below in the comments.
I'd really like to know what you think.
Anyway, it's just a thought.
Y'all have a good day.