Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about
an interesting political dynamic
that's shaped up over the weekend
and what it means for the future of the Republican Party
and how Democrats may use it to their advantage
in the future.
Over the weekend, 100 CEOs had a conference call on Zoom,
some of them from pretty big companies.
The purpose of this call was to discuss how they might best flex their corporate muscles
in opposition to the latest Republican push to limit access to voting.
They feel that this whole push is based on a lie.
Now the main reason they feel that way is because it is.
But they discussed options on how to counter this push.
One of them was freezing campaign contributions to any politician who supports it.
Another would be freezing or postponing, limiting, ending investments in states that pass it.
Those are pretty big moves.
Those are the kind of moves that will certainly get the Republican Party's attention and
probably change minds.
I would imagine just the suggestion of them doing this has already changed minds.
Aside from that, former President Trump had a little get-together with a lot of big Republican
donors, and he gave a speech and they walked away, let's just say a little disheartened because
this speech did not talk about how they were going to retake the Senate or take the House or win in
2022 or 2024. He used this speech to talk about his personal grievances and lash out at his political
opposition. And by political opposition I mean other Republicans, Pence, McConnell, people like
that. It reinforced the idea that supporting Trump would lead to infighting in the Republican
Party. Large donors don't like that because that means their money gets used to battle other
Republicans. Now, for the Republican Party excluding Trump, this puts them in a very
unenviable position because they can't control Frankenstein's monster. They have
no idea what Trump is going to do. He's a hindrance when it comes to gathering
campaign funds and he's still an asset when it comes to motivating the base, but
it's also led to corporate America turning against them. Realistically the
only move that the Republican Party has is to try to recast themselves as the
party of the working class. The problem with that is they would have to start
supporting policies that actually help the working class, that help those on the bottom.
That's not something the Republican Party is really known for.
It's not something that a lot of current Republicans will want to do because they'll have to use
the opposite talking points and they'll have to debate their past selves.
It's not a good position.
Now, for Democrats, they may end up being able to get more funding from corporate donors
while continuing to motivate the base by being in opposition to Trump, who is really good
at generating negative voter turnout, people who are showing up not necessarily to vote
for a candidate, but to vote against him and his candidate.
And all they really have to do is make good on Biden's promises.
There may be a significant political realignment coming, something we haven't seen in the
United States since the Southern Strategy, because the Republican Party can't really
survive on its current platform, what's left of it.
They're going to have to adapt.
They're going to have to move in a different direction.
How they accomplish that is anybody's guess, but until they figure it out, the Democratic
Party has a pretty big opening to make some substantial gains.
Anyway, it's just a thought.
Y'all have a good day