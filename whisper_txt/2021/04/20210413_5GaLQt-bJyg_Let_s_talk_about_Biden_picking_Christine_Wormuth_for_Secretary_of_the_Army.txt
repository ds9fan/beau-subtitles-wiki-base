Well, howdy there, internet people, it's Beau again.
So today we are going to talk about President Biden's pick for Secretary of the Army.
Now, the big headline here is that if confirmed, Christine
Wormuth will be the first woman Secretary of the Army.
The Secretary of the Army is a civilian position, reports to the Secretary of Defense.
It is part of that whole concept of civilian control of the military.
There's going to be somebody, I'm sure, that says, well, she was never in the Army.
She's not supposed to have been, not really.
That's not what this position is about.
It's about policy, overall guidance.
an administrative position. She's not going to be out there drawing up battle plans.
Okay, so what does her resume look like? Back in 95 she was an intern, then she was a civil
servant with DOD for six or seven years, then a senior fellow at the Center for Strategic
and International Studies, then under Secretary of Defense for Policy under Obama, then she
went to work for Rand Corporation. If you're not familiar with Rand, it's a
think tank. If you want to know anything about the military, look to Rand. They've
done a study on it and they did it right. Because of that, lucky me, I didn't have
to do a whole lot of research on this one. I'm pretty familiar with their views.
One thing that stands out in contrast to the previous administration is that she
She totally understands the value of local partners.
She is also real big on international alliances.
She has spoken about this quite a few times.
Overall, her resume is exactly what you would expect for a Secretary of the Army.
But to me, what's interesting about her resume is not what's on it, it's what's not on it.
She never worked for Raytheon, Lockheed Martin, nothing like that.
The Secretary of the Army exerts a lot of influence over how money is spent.
This is a nice change.
In the past, she has spoken a lot about how money is spent.
And one thing I can remember is she was talking about our NATO partners.
Our partners are supposed to spend 2% of their GDP on defense.
And she went out of her way to basically say, yes, you know, they're supposed to spend that
and we want them to spend that.
But I'm much more concerned about whether or not the money being spent is producing
results.
getting the capabilities needed.
That would also be a nice change in DOD to actually have some kind of interest in the
money being spent well.
Because she worked for Rand, she has a good idea of how to handle near peers.
She won't be attempting to prepare for the next engagement by preparing for the last,
which is something that happens a lot,
and it's pretty ineffective.
Overall, she's a good pick.
She's a good pick.
I'm pretty familiar with her.
I don't know of anything controversial in her history.
I would imagine she is going to sale
through Senate confirmation.
I think the only real surprise here
is that she was tapped for Secretary of the Army
rather than Secretary of the Air Force.
Other than that, yeah, I mean that's the only real surprise here.
So, anyway, it's just a thought.
Y'all have a good day.