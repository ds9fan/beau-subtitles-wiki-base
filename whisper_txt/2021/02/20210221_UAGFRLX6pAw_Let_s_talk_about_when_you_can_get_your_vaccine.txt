Well howdy there internet people, it's Beau again.
So today we're going to do a little update on the timeline. The timeline everybody is
wondering about.
We have some more information.
So when they first
released the timeline they thought they would have it all in hand
by April.
I was, to use my favorite comment
from that video, I was visually skeptical.
Yeah.
I think the term I used was
they were being ambitious.
They were going off of best case scenarios for production and
distribution and everything else.
They have now encountered
the absolute logistical nightmare
that comes along with trying to deliver a product to three hundred million
people twice.
The timeline has shifted a bit. It has been altered.
Okay, so as of now
they are saying
May, early June
will get us through all of the priority people.
These are
older people, people in nursing homes, frontline health care workers,
people who are truly, truly, truly at risk.
Those are first.
They're saying that they will have six hundred million doses
by July.
They'll have it in hand, but then they've got to get it in our arm.
So they're saying it's going to take to the end of summer.
Um...
I honestly,
to be one hundred percent
transparent here,
I was skeptical
the first time.
I think they've gone the other way with it. I think they're being overly cautious.
I think it could be done a little sooner than this.
Not by much.
But uh...
that's where we're at.
According to this timeline, your best case scenario for
open season, anybody who wants one can get one,
is July.
Good news is it does look like it will be wrapped up before school starts back.
So there is that.
Uh...
I know
it's been a long time
and I know it's got to be rough because I'm very much a hermit
and it's even starting to bother me now.
But we can't really succumb to the fatigue that goes along with it.
So until this happens,
we have to
wash our hands, don't touch our face, stay at home. If you have to go out, wear a
mask, do everything you're supposed to.
Because the reality is
if we relax,
every transmission is another chance for mutation. If there are enough mutations,
eventually one of them
is going to be one that can get past these vaccines.
And then we have a whole new mess of problems.
So hang in there.
And eventually we will get through this.
Anyway, it's just a thought. I hope you all have a good day.