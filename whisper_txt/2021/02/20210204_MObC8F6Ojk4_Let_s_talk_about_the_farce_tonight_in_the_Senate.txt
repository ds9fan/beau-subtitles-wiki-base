Well howdy there internet people, it's Beau again.
So today we're going to talk about what's going to happen tonight in the Senate.
Earlier this week we talked about budget reconciliation and how Bernie was willing to use his new
position to muscle through some major pieces of Biden's relief plan.
The Democratic leadership gave him the green light.
That's going to happen.
They're going to attempt to use this process.
A quick recap on the process.
It is a procedural trick that allows the majority party, in this case the Democrats, to push
through budget related items without risking a filibuster.
They only need 51 votes to get stuff through.
So it allows things like a relief bill to get through without going through the normal
process.
The downside to this is the period in which people can introduce amendments.
That's going to happen tonight.
It's a joke.
It's a farce.
It is one of the moments in the Senate that you can watch and see how ridiculously broken
the Senate rules are.
What's going to happen tonight is Republicans introducing amendment after amendment after
amendment.
The rumor says they have 400 prepared.
They probably won't introduce all those.
And people have to vote on them.
The goal here is not really to get any of these passed.
That's not the goal.
The goal is to trick your political opposition into voting the wrong way on something.
That's really what it's about.
It is absurd.
Normally the party that is in power doesn't really want their supporters watching because
it's typically them that makes the mistakes.
This year Democrats have actually said don't watch.
I disagree.
It's enlightening.
These amendments can have anything to do with building a border wall or pork projects in
home states.
Anything at all that has to do with spending money.
The goal though is to get your political opposition to vote against something everybody in their
home state would want.
When you see ads around election time that say Senator Smith voted against a bill that
would have given everybody $500 or whatever.
That normally happened during one of these voteramas, that's what they're called, because
even the Senators know it's a joke.
It's a sham.
It's a farce.
That's going to happen tonight, but it is the first phase in pushing through the relief
bill.
Just so you, if you happen to hear about it, that's what they're talking about.
If you hear tomorrow that a Senator voted in some ridiculous manner, that's what happened.
Both parties engage in this behavior.
It's not just Republicans.
Democrats do it too.
It is the Senate.
Anyway, it's just a thought.
Y'all have a good day.