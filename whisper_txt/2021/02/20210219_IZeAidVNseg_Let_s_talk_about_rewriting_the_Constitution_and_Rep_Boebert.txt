Well howdy there internet people, it's Beau again.
So today we're going to talk about rewriting the Constitution, the parts you don't like.
A tweet today went out by a representative, this is somebody in Congress, Lauren Boebert.
She says, protecting and defending the Constitution doesn't mean trying to rewrite the parts you
don't like.
That's a weird statement.
That's odd.
I would assume that if you want to support, protect, and defend the Constitution of the
United States, you would want to support, protect, and defend Article 5, which outlines
the ways to rewrite the parts of the Constitution you don't like.
There are four of them.
The first is a two-thirds vote in both houses of the U.S. Congress, which is then ratified
by three-fourths of the state legislatures.
The second is a two-thirds vote in both houses of the U.S. Congress and then ratified by
ratification conventions in three-fourths of the states.
The third is a national constitutional convention called by two thirds of the state legislatures
and then ratified by three-fourths of the state legislatures.
The fourth is a national convention called by two-thirds of the state legislatures and
then ratified by ratification conventions in three-fourths of the states.
The Constitution has been rewritten or amended several times throughout history.
I would note that as far as I know, all of the attempts at amending the Constitution
that have gone anywhere originated in Congress.
I don't know that the convention method has actually ever been successfully used.
But this whole tweet goes to an idea that is not true and is also kind of dangerous.
The Constitution of the United States is not infallible.
It was designed to be changed because unlike many politicians today, those who wrote it
understood they weren't perfect.
They didn't try to pretend that they had all of the answers to every topic.
They deferred to subject matter experts a lot.
At this point, it's probably something we need to remember.
The United States Constitution was designed to be a living document.
It was designed to be changed because the views of the people are going to change.
The Constitution was never meant to be a document that held the United States back.
It's not what it was.
The idea that somebody in Congress is apparently unaware of the fact that the Constitution
can be changed is disheartening, especially considering this is the representative who
recently did that Zoom call with all of the firearms behind her.
A right protected by the Second Amendment.
The second rewrite of the Constitution.
I would also point out that if it wasn't for rewrites of the parts we didn't like, she
wouldn't be in office.
She couldn't vote.
This concept, this mythologizing of American history and of the founding and of the Constitution
is incredibly dangerous.
It's a document.
It's a contract with a process that allows it to be amended.
I would point out that anybody who likes to argue from constitutionalist viewpoints, they
have a problem with the way the government currently operates.
Everybody in the United States pretty much has issues with how the federal government
currently operates.
I would point out, as far as the idea that the Constitution is infallible, the Constitution
either explicitly allowed what is happening today or was powerless to prevent it.
The Constitution isn't infallible and the people who wrote it knew that.
Anyway, it's just a thought.
Y'all have a good day.