Well howdy there internet people, it's Beau again.
So today
we're going to talk a little philosophy
because I got a question from a student who wants to debate something with their
teacher
and it's uh...
right up my alley.
So, the thing that the student would like to discuss is a closely held belief of a
whole lot of people.
And if you're going to discuss something and debate something,
challenge a closely held belief that is held by the majority of the population
the world over,
you need good argumentation.
And
the student wants assistance with that.
So we're going to help.
The
statement
that the teacher
is making is that
governmental laws
are the only way
to achieve societal order.
That is something that a whole lot of people believe.
So
it's... there is the assumption
that it is the only way to do it.
It's not even a discussion
about whether it can do it.
It's the only way.
But is that true?
Do governmental laws
in fact achieve societal order?
No, they don't.
Not at all.
It's not the laws.
Not in the system that we have today.
It's not laws. It's not words in a book.
If that was true, there'd be nobody in prison.
If those in government
could just write things down
and people wouldn't do them anymore,
nobody would
be in jail.
It's not laws.
It's a monopoly on violence.
That's what actually achieves
the societal order that we have today, what we believe to be societal order.
It's the fact that if you
don't abide by the rules that they write down,
people with badges
and blue uniforms are going to show up and put you in a cage.
It's the monopoly on violence
that achieves the societal order, but people don't like to say it that way
because I mean that sounds bad.
But in reality
that's what it is.
That is what it is.
This idea also
throws out
another premise
that somehow government
is separate
from society,
which is something that
I have an issue with a civics
professor in the United States saying,
us being a
government of the people, by the people,
for the people, all that stuff,
government is separate
from societal order,
from society.
If it was part of it,
the order would come from society itself, right?
Because that's where
the people in government come from.
The statement itself is false.
The laws do not in fact create societal order.
The monopoly on violence that society grants to the government
is what creates societal order.
Okay, so
now that we have
the initial premise,
kind of a little bit of doubt cast on it,
now let's look at the word only.
Is there any other way
to achieve societal order?
One that doesn't require
governmental laws
or the monopoly on violence, which is what actually
creates the order that we're used to today.
So, if you want to make this case,
you have to
come up with an idea
of what this other society would look like.
And basically what you want
is everybody abiding by the golden rule.
That's what that society would be.
So, I'm going to go to the idea store.
When I'm at the idea store, I'm going to take the idea off the shelf, I'm going to put it in my cart.
I'm going to go to the cashier.
There's somebody else in line in front of me.
Even though I'm in a hurry,
I'm not going to cut in line.
There's no law stopping me from doing that.
But there's a custom,
there's an ethic
of doing things a certain way.
No law,
no violence,
but there is societal order.
After I pay for it, I'm going to go out to the parking lot, I'm going to throw my idea in the back of the truck,
and I'm going to return my shopping cart.
No law requiring me to do that.
It's just me,
golden rule.
It is societal order.
So,
we have cast doubt
on the opposition's premise.
And we have shown
pretty clearly that societal order can exist
without governmental law
and without violence.
Now comes
the hard part.
Can you scale this?
Can you scale this up?
Now, I believe you can.
I think it would take a lot of work and a lot of education and a lot
of instilling
the golden rule into people
because it would take a society where everybody was looking out for everybody's interest rather
than just being selfish.
I think that that's possible.
I think it would probably take generations to achieve, but I believe it's possible.
The student also said
that
the teacher
sums up their argument.
This quote he uses
summarizes his argument.
It's a James Madison quote.
If men were angels,
no government would be necessary.
Yeah, I mean that makes sense.
That's why you would want to give
a bunch of men in government a monopoly on violence
because they're not angels.
This is not James Madison's best quote.
It's very contradictory.
Okay, so
can it be scaled up?
Despite my beliefs,
there is a strong argument
against it.
Let's say today
all borders are gone.
Nation states cease to exist the world over.
What happens?
In small areas like where I live, nothing,
like literally nothing changes.
In
some areas like
tightly knit
urban areas,
people would probably cooperate
and
they'd be fine.
There wouldn't be much difference
between yesterday
and today.
In some areas
it would probably devolve
into a situation where you have somebody who is
using
violence
to get what they want and eventually
that person would probably
achieve a monopoly on violence over a geographic area.
That would probably happen.
So
those are kind of the three outcomes.
Now, what happens
if these little geographic areas are near each other?
The strong argument says
that those who are willing to use violence would overpower those who
won't.
And I mean, yeah, that's a strong argument.
But
maybe not as strong as people think.
The strongest argument against self-organization
is the fact that if it's attempted,
well, one day
it might devolve
into the exact system we have today.
Anyway,
it's just a thought. Have a good day.