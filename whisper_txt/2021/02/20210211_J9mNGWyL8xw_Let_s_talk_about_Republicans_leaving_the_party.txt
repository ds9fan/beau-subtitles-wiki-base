Well howdy there internet people, it's Beau again.
So today we're going to talk about the news coming out of the Republican Party.
There was a conference call.
Had more than a hundred Republicans on it.
And the purpose of this call was to discuss forming a third party.
What's the difference between this potential third party and all the other ones that we
have?
Names.
There were people on this call whose name you might recognize.
They had representatives from all four of the recent Republican administrations.
These are insiders.
These are people who know how the game is played.
These are people who have their own base, have their own donors, have their own connections.
They want to capitalize on the anti-Trump sentiment that is sweeping the nation.
The goal is to move the conservative movement back to center.
So much so they want to call their party the center-right party.
It makes sense.
They're reading the room.
They understand the way the wind is blowing.
There is one small problem.
The United States already has a center-right party.
You might have heard of them.
They're called Democrats.
They control the House, the Senate, the Oval Office right now, those people.
Republicans have bought into their own propaganda.
On the international spectrum, Democrats are center-right.
They're not leftists.
They're certainly not radical.
For the U.S., sure, they're left-ish.
But that's not saying much.
We just had a fascist in the White House.
If you talk to most Marx-reading leftists, they will tell you Democrats are not their
people.
There are a few social Democrats within the Democratic Party who I consider to be left.
But most of the leftists from the Republican means, yeah, they don't really claim them
either.
They're not leftists.
They're center-right.
Aside from that, though, it makes sense.
It's a good plan.
I actually think that they would have the ability to seat people in the House and the
Senate their first time out because the current Republican Party is not reading the room.
They do not know which way the wind is blowing.
It does appear that they're going to acquit Trump.
And then from that point forward, every new revelation looks bad on them.
Every time one of the accused says, well, I did it because Trump wanted me to, it demonstrates
that those in the Senate were incapable of saying the truth.
Or they didn't care.
Or they're corrupt.
Either way, it bodes well for the center-right party.
They're not reading the room.
Odds are they would underestimate a new third party the same way they underestimated Trump.
So I think it has a chance.
Now as far as I'm concerned, if the conservative movement does come back to center, I think
it's a good thing because realistically, at that point, if Democrats want to differentiate
themselves, well, they really only have one way to go now, don't they?
Anyway, it's just a thought.
Y'all have a good day.