Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about numbers, records, statistics, predictions, kids, mistakes,
and something it may be time to do.
Over the weekend, the director of the National Institutes of Health
said that Delta shows no signs of peaking.
They're saying it is within the realm of possibility to see 200,000 cases a day again.
Over the weekend, the U.S. set a record with almost 2,000 children in hospitals due to this.
At this point, unless you have a bona fide reason,
it's probably a mistake to not have been vaccinated, but it's a mistake you can fix.
It's an error. There's no reason. It has to be a fatal error.
The director of NIH described the unvaccinated as sitting ducks.
The numbers are still very high for people who haven't taken their shots.
So while certainly encourage your friends and family to take the precautions they need to,
it may also be time for you to prepare. With these numbers, if they parallel last year,
we can expect a similar response, which means it may be time to stock up on things that you may need
if you're unable to leave the house for a while.
Because unless there is an increase in vaccination rates, I would imagine there are going to be lockdowns again.
So as a lot of politicians have staked their political futures on denying what's occurring,
it may be one of those things that it waits until it's just so bad it can't be ignored.
So there won't be a lot of warning. If you have the ability, now might be the time to stock up on essentials,
because if it does come in a rush, there will be a rush on the stores.
There will be disruptions in the supply chain, just like last year.
So it's something you might want to be ready for, might be prepared for it,
because it does seem like it is definitely within the realm of possibility again.
If you haven't gotten your vaccine, I understand that there's hesitancy.
I understand there's fear, and I understand there are some people that have real reasons.
What I would suggest is not listening to Facebook, not listening to Twitter,
not listening to a politician, not listening to some person on YouTube.
Call your doctor, your doctor, not a YouTube doctor, not a doctor on Twitter or Facebook.
Call your doctor who knows your medical history and ask them what they think you should do.
Maybe that's the best route, because there's a lot of misinformation out there.
Those who have treated you before probably have the best picture.
I can understand a reluctance to believe the government or whatever.
I get it.
Your local doc, you probably trust them.
They wouldn't be your doc otherwise, right?
Maybe reach out to them, get their advice, and go from there.
Anyway, it's just a thought.
Y'all have a good day.