Well, howdy there, internet people, it's Beau again.
So today we are going to talk about President Biden,
$6.1 billion and 315,000 or so additional people
along with something called the Art Institutes.
Okay, so what's going on?
More relief, more relief, more student relief.
OK, so how does this one work?
If you attended something called the Art Institutes
between January 1, 2004 and October 16, 2017,
and you have debt, it looks like if you qualify, it's gone.
It's automatic.
As I understand it, you don't have to do anything.
this is just going to happen.
Why is it happening?
According to Biden's statement on this,
this institution falsified data,
knowingly misled students and cheated borrowers
into taking on mountains of debt
without leading to promising career prospects
at the end of their studies.
We will never stop fighting to deliver relief to borrowers."
And it goes on from there.
So, this is under, this was an option that was existing.
This isn't something that was newly created.
This is a bar defense to repayment.
It is automatic.
I know that some of you have already been notified and you already have questions.
So to answer all of the questions because it was the same one, I have no idea.
The question that came in over and over again is with this being forgiven in this way, does
it count as taxable income?
The answer is I don't know.
I do not know.
I would call a tax person because that is going to vary state to state, I'm sure, because
you know some states have income tax.
I have no idea how that works and there's no way we're going to be able to basically
give advice on that.
I would call a tax person in your area.
When you break this down, it's just under $20,000 a person.
I would make sure that that aspect of it, you get that right, you know?
Okay, so for those who are keeping track of the progress here, so far this is, what, 160
billion or so forgiven and 4.6 million people impacted.
So that's it.
Now in this case, this is something having to do with a specific institution.
My understanding is that there's something else similar coming about another institution,
but it's rumor and I don't know anything more about it than that.
I have no clue what it is.
So I would definitely keep your keep your eyes open if you're waiting for if you're
waiting for something similar because of a similar situation at a different institution.
Just keep watching.
Anyway, it's just a thought, y'all have a good day.