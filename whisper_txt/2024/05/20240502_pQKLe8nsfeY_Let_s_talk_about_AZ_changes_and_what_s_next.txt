Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Arizona and mistakes
and changing things and undoing that and delays
and where everything will go from here
and how this will look come November.
OK, so in case you missed it, there
was a small issue in Arizona.
A court ruling put a law that really kind of originated
back in 1864 back on the books dealing
with reproductive rights.
And by that, by dealing with, I mean, like, just basically
totally banning it.
So, the House and the Senate in Arizona have repealed this, okay.
It will go to the governor.
The governor is expected to sign it, I actually think tomorrow.
It doesn't seem like they're going to waste a lot of time on this one.
Now due to a unique structure, even though this has happened and it's been repealed,
it will still end up going into effect for a period of time.
Because I want to say it's 90 days after the session ends, something like that it takes
before it gets repealed.
So this law will actually be in effect for a little bit.
People's lives will be disrupted because of it.
And then it will go away once that time passes, it will go away and a 15 week ban will take
its place.
Now one of the things that is interesting in some of the coverage as Republicans try
to just run away from the consequences of their actions and not take any personal responsibility
for what they advocated for for all of those years, a lot of the coverage is saying that
Republican Senate in the state and the Republican House in the state. Well, they
repealed it. I mean, that's a true statement. It is in the sense that the
Republicans control both of those institutions. Just to be clear, it was the
Democratic Party with two Republican votes in the Senate and three in the
House. The overwhelming majority of Republicans voted to make sure that you
didn't have these rights, that they voted against it. Let's be super clear on that
regardless of what the coverage says and how it's framed. You did not have
Republican support to repeal this. You are to be ruled, not represented. Okay, so
where does that leave us for November? Odds are there's going to be at least
one ballot initiative dealing with reproductive rights on the ballot then
to basically enshrine it in the state constitution out there. So even though
Even though this has been resolved, it is still incredibly likely to end up being a
major issue and a deciding factor in the 2024 election.
Anyway, it's just a thought, y'all have a good day.