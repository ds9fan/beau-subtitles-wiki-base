Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about animals.
We're going to talk about animals,
primarily focusing on a news item
that I feel almost obligated to cover
because so many of y'all sent me little bits of information
about it, knowing that I would find it
incredibly interesting story. Okay, so we'll start there. According to reporting,
a 40 year old man visiting Yellowstone, well, he suffered minor injuries after
allegedly kicking a bison in the leg. As somebody whose last words are very
likely to be, I'm a pettit, if you are ever up in that part of the country, just
some quick tips. Do not feed the giant hamsters, those are bears, and do not
kick the fluffy cows. The fact that these are minor injuries is nothing short of a
miracle. These things weigh like 2,000 pounds. They can hurt you like badly. So
just just something to bear in mind if you are up that way. The animals, they're
they're not domesticated at all, doing that is not a good idea.
Okay, so moving on, there's been a whole bunch of questions about another animal-related
incident from that general part of the country that also crosses over into politics and dealing
with the governor of South Dakota.
Yeah, I'm going to just say not really something I want to talk about at length, just a couple
of key things here.
First, you cannot expect any dog to be trained as a puppy.
particular breed is a puppy until I think 24 months. Aside from that, something
that occurs a lot when people get dogs that are working dogs or they are dogs
or pointers, they are often dogs that if you want them to perform in that
roll, you have to spend a lot of time training them. And it is worth noting that
those dogs were trained to do those tasks long before the invention of an
electric collar. Oftentimes when people have a dog that does not behave the
way they expect it to, they blame the wrong end of the leash. If you ever find
yourself in that situation, it would probably be best to rehome the dog or if
necessary take it to a shelter instead of taking it to a gravel pit. Anyway, it's
It's just a thought.
Y'all have a good day.