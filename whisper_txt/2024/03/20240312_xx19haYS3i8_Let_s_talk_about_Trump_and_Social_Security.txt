Well, howdy there, internet people, it's Bo again.
So today we are going to talk a little bit about Trump
and Social Security, and something that he said
that is undoubtedly going to impact the rest of the year.
One of the smartest things that Trump world has done
since it came into existence
when you're talking about politics
is stay away from the topic of Social Security.
they have nothing that can help them there.
It's only going to hurt them.
And for the most part,
it's not something they've talked about.
So Trump said something.
He was asked a question in the context of national debt
and what could be done when it comes to Social Security,
Medicaid, Medicare, those types of things.
And he said, there is a lot you can do in terms of entitlements, in terms of cutting,
and in terms of also the theft and the bad management of entitlements.
Okay.
So cutting was in that sentence about Social Security.
That was the topic.
Obviously, the Biden administration jumped on this.
They have seized on this.
They're putting out little ads to respond to on social media.
It's a thing.
Trump's coming after your social security.
They're going to lean into this heavy.
Why?
Because this is an age bracket that normally Trump does pretty well with.
The people who care about this topic deeply are people that generally Trump does well
with.
So they're going to go after it and hit him where it hurts.
What is Trump World saying?
Trump World is saying clearly, Trump was quote, clearly talking about cutting waste, not entitlements.
That's a spokesperson for the Trump campaign.
Clearly talking about cutting waste, not entitlements.
I will read that segment again.
There is a lot you can do in terms of entitlements, in terms of cutting and in terms of also the
theft and the bad management of entitlements.
That also, that's going to hurt.
Okay, so this isn't going to go away.
This is going to run throughout the rest of the election.
Biden is going to try to make this a campaign issue because Trump walked into it.
Trump is going to stay on the defensive.
This is one of those things that is something that is not going to go away.
Because as people talk about this, it will continue to resonate, it'll continue to motivate
more people, and it might sway their vote.
This is something that this demographic really cares about, so we'll see how it plays out
from here.
Anyway, it's just a thought y'all have a good day