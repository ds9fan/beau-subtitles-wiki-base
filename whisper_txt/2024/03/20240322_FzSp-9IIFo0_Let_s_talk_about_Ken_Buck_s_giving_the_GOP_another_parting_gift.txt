Well, howdy there internet people, it's Bo again.
So today we are once again going to talk about Ken Buck
because he demonstrated today
that he is Ken off to take
one more little dig at
the Republican Party before he leaves.
As a quick reminder Ken Buck is the representative from Colorado
that is leaving in the middle of his term and
has thrown quite a few wrenches
along the way. His latest and probably his last
is that he signed the Democratic Party's
Foreign Aid Discharge Petition. That would
force a vote to come to the floor
when it comes to that giant aid package
that the Democratic Party wants to push through, that the Republican Party is
less than willing to push through.
My understanding, I think he's the first Republican to sign it.
It brings it up to a hundred and eighty-eight signatures.
It needs two hundred and eighteen.
Because of how expansive it is,
there are different groups that take
issue with it. There are progressives who won't sign it.
There are a whole bunch of Republicans who won't sign it.
Now realistically
is this going to tip the scales?
Probably not. Probably not in and of itself.
It might prompt other Republicans to sign on to it
as well. Buck
has been, Buck has been pretty supportive
of Ukraine and the US
efforts to supply them and that is at odds with most of the Republican Party.
Now the other thing to keep in mind
is that his signature is good
until I think June 25th,
whenever the special election there in Colorado is,
when his inevitable replacement
is elected, his signature doesn't count anymore.
So if the Democratic Party wants to capitalize on this,
they have to get those other signatures between now and then.
Whether or not that's going to happen,
it doesn't seem
impossible, but I don't know that I would call it likely.
Again, this really seems to be just Buck reinforcing his statement that he had
had Knaf and just taking one more dig at a Republican party that he believes is
completely dysfunctional and is no longer serving conservative ideals, their
constituents or the country which is kind of what he has laid out. I don't
know that he's put that fine a point on it but he is definitely unhappy
with the dysfunction that is occurring within the Republican Party in the House
and this is just another very clear signal of that. Again, yeah he's leaving
the house, I don't think that this is the last time we're gonna see this guy.
Anyway, it's just a thought, y'all have a good day.