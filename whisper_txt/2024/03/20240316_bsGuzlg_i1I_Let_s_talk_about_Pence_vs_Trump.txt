Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Pence and Trump
and the move that Pence just made
and kind of put it into context because the headlines,
they're kind of leaving out an important piece of this.
If you haven't heard yet,
Pence, the former vice president,
has refused to endorse Trump,
his former running mate.
There are a lot of people who are not coming out
to endorse Trump. And that's what it sounds like. It sounds like Pence
just isn't going to endorse him. That's not what happened.
Pence went on Fox to announce that he couldn't endorse him.
And
He said, Donald Trump is pursuing and articulating an agenda that is at odds with the conservative
agenda that we governed on during our four years.
And he goes on to list the different ways that he sees Trump as, well, being a rhino,
for lack of a better word, and giving up on Republican ideas, on conservative ideas.
He says, as I have watched his candidacy unfold,
I've seen him walking away from our commitment
to confronting the national debt.
I've seen him starting to shy away from a commitment
to the sanctity of human life.
And last week, his reversal on getting tough on China.
This isn't just Pence saying,
I'm not endorsing him.
This is coming out and listing the reasons why.
This is coming out and providing a counter endorsement in a way, saying if you actually
believe these things, you can't vote for Trump.
That is worth noting.
He said he was not going to vote for Biden.
But coming out like that and announcing that he's not going to endorse him, that's a little
different than simply refusing to endorse him.
It's an open statement that he no longer believes that the presumptive nominee of the Republican
Party is really fit to be there.
It's not a little thing.
It's not as simple as, say, if McConnell had withheld an endorsement.
This is a change in tone and it was announced.
Pence is trying to sway people away from voting for Trump.
That's what's occurring because apparently Pence believes that Trump has abandoned conservative
values, Republican ideals, and is engaging in that populism that he warned about when
he was running.
It's probably worth noting, especially if you are somebody who considers yourself a
principled conservative.
Anyway, it's just a thought.
Y'all have a good day.