Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Michigan
and nuclear power.
We're gonna talk about nuclear power again
because there's been a development recently
within the last few days, we were talking
about a pledge that the United States had signed
and said, hey, we're gonna devote more attention
to building out nuclear power within the United States.
At the time, we were like, hey,
I mean, they signed the pledge, but we don't know if they're actually going to do anything
with it.
Okay.
So the Biden administration is going to provide a $1.5 billion loan that according to reporting
will be paid back with interest to a company to bring an 800 megawatt facility back online.
It's in Michigan, I believe it was initially built in 1971, and was shut down in 2022.
I believe the plan was to actually dismantle it, but now they want it back up and running
by 2025, and it appears that they're going to try to keep the license and try to keep
it running through 2051.
reporting says that there are already two electric co-ops up there for rural
areas that have agreed to buy electricity from the plant over the long
term. So that's the news. It certainly does appear that they're going to put
some effort into that pledge. Now, obviously, this plan has its critics,
and they have requested a hearing with the Nuclear Regulatory Commission. My
guess is that the NRC is going to take their concerns very seriously because
Because it's the NRC that will be responsible for the safety and bringing it back online.
I don't foresee them just blowing off any concerns.
So we'll wait and see how that plays out.
Now this is a unique shift and it is apparently occurring very quickly where it appears the
Biden administration is incredibly interested in building out the nuclear power capability
of the US.
The pledge may have been a formality.
It may have been something that was discussed privately between a whole bunch of countries
and now they're just going to actively pursue it.
It's worth noting nuclear power is more expensive than solar or wind or a lot of the other greener
energies that are being pursued.
areas up north, well, solar gets hard to maintain at times.
So we'll wait and see how it plays out.
My guess is that this is not the end of this.
There's going to be more news on this front.
Anyway, it's just a thought.
Y'all have a good day.