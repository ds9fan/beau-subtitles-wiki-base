Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about New York and wind,
and South Fork wind, and a first, something that occurred.
And it's worth noting.
It's worth marking that moment.
The first US offshore wind farm that
is operating at utility grade and delivering
electricity is open. It's called South Fork Wind and it's up in New York. There
was a whole lot of fanfare when it came online. It will provide power to 70,000
homes, which in the grand scheme of things, it's not a lot, but it is part of
a much wider campaign to build a whole bunch of these. The Biden
has approved a number, I don't even know how many, as part of a long-reaching
strategy to get this number up to 10 million homes by 2030. It's all part of
a transition away from carbon and to get to a much cleaner way of getting
energy. The 70,000 homes, I think I saw something that said that it reduces
emissions by what 60,000 cars would emit. When you look at it in that light, it
seems a little bit more significant, but that transition is underway. It's one
One that we have been waiting for and waiting for, campaigning for, calling for, for years
and years and it's starting to occur.
There are a number of other large projects that are slated to come online relatively
soon.
I know Revolution Wind is one, but there are other projects in other places as part of
that wider campaign to redo the infrastructure here, it's something that has to happen.
This isn't one of those, maybe we should do this.
This isn't something that should be politicized in any way.
This is something that needs to occur from an environmental level, from a political level,
from a national security level.
level says that the United States needs to transition and get to cleaner, greener energies.
And that's what our infrastructure needs to be based on to be, well, competitive, but
also just to be here in the future.
So this is one of those small steps that can lead to something much bigger.
Anyway, it's just a thought.
Have a good day.