Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the nomination
and delegates and the presumptive nominee,
what all of that means.
Because Biden is one now.
After Georgia, Biden is the presumptive nominee.
What does that mean?
It means that he has enough delegates
to win the big convention.
He has 1968 and he has 2015.
Trump is not the presumptive nominee yet.
He does not have the delegates yet, but it's yet.
He's almost certainly going to get them.
It would be shocking if he didn't.
So that's where all of that rests after today.
Biden is the presumptive nominee now.
The only real question that arose in any great number out of this was, does this mean that
the uncommitted delegates and the uncommitted voters, that they're done?
No.
I mean, keep in mind, I don't think anybody believed that the uncommitted movement was
going to stop Biden from getting the nomination.
It was about signaling and messaging.
what it was. Can they continue to do that through August which is when the big
convention is I think? Maybe. The answer to that is maybe. Delegates, when you vote
in a primary, okay, you create the spots. There are spots for uncommitted
delegates. The people who fill those spots are also called delegates and
and they're chosen at conventions, so it depends on the person who fills the spot.
It clears mud, right? In Minnesota,
as an example, they have a convention
to choose the people who will be
the delegates. If they choose people
who are uncommitted and part of that movement,
then that movement could continue to signal through
the big convention. If they choose people who are riding with Biden, well then no, they can't.
There's a whole process involved here and it's not very straightforward,
it all depends on who the people are who become the actual delegates as to whether or not they
can continue to message if need be. They seem pretty organized in most places. I would imagine
that at least some of those delegates would be part of the uncommitted movement and continue to
message. But again, I don't believe anybody thought that the uncommitted vote was going to
stop Biden from becoming the presumptive nominee, and the Democratic Party is not going to want
delegates that are uncommitted or not really showing their support. They're going to want
that unity. So yeah, there's still an opportunity to message and signal. So that's the end
result of today. Biden became the presumptive nominee. Trump has not yet, but he will. Anyway,
It's just a thought, y'all have a good day.