Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Ecuador and Mexico
and Vienna and just run through some information
that has apparently been forgot by a number of countries.
And we'll go over what happened
and just kind of run through everything.
Okay, so we'll do a little recap first.
The former vice president of Ecuador, he was the vice president 2013 to 2017-ish.
He was inside the Mexican embassy in Ecuador.
Officials in Ecuador wanted him.
He's been there, he was there, I believe, since December and I think he was either about
to get granted asylum or had just been granted asylum.
But officials in Ecuador wanted him over bribery, graft, something like that.
Some people are undoubtedly going to try to look at that information and decide, is he
a good guy? Is he a bad guy? Were the charges real? Fake? Did he do it? Did he not? You
know, were they politically motivated? They're going to look at all of this stuff. Here's
the thing, from a foreign policy perspective, it does not matter. Like, it is an afterthought.
None of that is important to the actual situation because officials in Ecuador raided the Mexican
embassy in Ecuador. For anybody who didn't watch all of the cheesy 1980s
action movies and TV shows that needed some strange plot device to make their
bad guy untouchable, you can't do this. It's against the Vienna
a convention. I believe in technical legal jargon, they refer to it in that convention
as totally uncool. There are international norms and there are protocols about this.
For all intents and purposes, that's Mexican soil. And an armed force entered it, took
someone.
When you start looking at whether or not he actually did what they said he did and all
of that stuff, this is a lot like the situation that just occurred with Iran.
They say, well, that facility was used mostly for intelligence gathering.
Okay, even if it was used entirely for intelligence gathering, if it is part of their diplomatic
facility, you can't do this. And so in response to the raid on the Mexican
embassy, Mexico has severed diplomatic ties, which is, I mean, that's a pretty
big step, but in some ways this could be viewed as an act of war. So it's a big
step, but not the biggest they could have taken. The other thing to remember about this is that
Ecuador is not unaware of the rules and how this is supposed to work. You might remember a guy
named Julian hanging out at an embassy in the United Kingdom for a while. That was Ecuador's
embassy. They know how this works. I don't believe the diplomatic fallout
from this is over with. There's probably going to be more. The key point to take
away from the beginning is there are a number of countries now that have
apparently decided that Vienna doesn't matter anymore. That's the way it seems.
that opens up a whole lot of just multiple cans of worms. The reason those
protocols exist and they're in place the way they are is to avoid war primarily.
Undermining them is bad. I feel like that should go without saying. And I'm sure
Or there are going to be some people who say, you know, this guy, he really didn't do this
or this guy definitely did this.
Again, from a foreign policy standpoint, it doesn't matter.
That's completely irrelevant to the question.
As far as international norms go, this is way out of line.
I believe that a lot of the other countries in the region have expressed their disapproval.
My guess is there will be some kind of joint statement about it.
And again, I don't think the diplomatic fallout from this is over.
There's going to be more.
This is one of those things, much like the situation with Iran, Mexico can't just let
it go.
This is a big deal.
This is another developing story that is probably going to continue for a while.
Anyway, it's just a thought.
y'all have a good day.