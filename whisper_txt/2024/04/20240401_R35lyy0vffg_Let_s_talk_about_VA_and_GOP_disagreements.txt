Well, howdy there, internet people, it's Beau again.
So today, we are once again going to talk about Republicans
in the US House of Representatives
and the discomfort that exists between various factions
within the Republican Party in the House.
And this is playing out in Virginia, okay.
So there is a representative, Bob Good.
Bob Good is the leader of the House Freedom Caucus.
In many ways, people see him as the de facto leader
of the Twitter faction, even though he's not
the biggest name, he's not the biggest person out there
engaging in that kind of social media stuff
because he's the leader of the House Freedom Caucus.
He is viewed as their boss, kind of.
He is facing a primary in June from John McGuire.
John McGuire is, see, he's in exile.
He campaigned for Trump, and he is being backed by,
I don't want to call them moderate Republicans,
because they're still pretty right-wing,
like even for the Republican Party,
but they're not Twitter faction, okay?
So you have sitting members in the House
going to help McGuire with his primary campaign.
We talked about this back when the Republican Party
had its little retreat,
and they were supposed to put all this to bed,
and everybody was supposed to stop.
Apparently, that's not going to happen.
The infighting within the Republican Party in the House definitely appears like it's
going to continue for some time, at least through June.
Now, that in and of itself is an issue for good.
Having Republicans come out to support a primary opponent, that's not a good situation to
be in.
The other issue that is bad for Good is that during the early part of the primary, he didn't
support Trump.
He didn't back Trump wholeheartedly.
Now today, when he's asked about this, he's like, get with the present.
We have to 100% get behind Trump and all of this stuff.
That is not stopping the ads that are apparently being run up there that say, Bob Good has
no faith in Trump.
So, Good is in a situation where he is being hit from, again, not moderate, but slightly
less right-wing, and the hardcore Trump side.
They're both going at him.
This is certainly going to lead to uncomfortable dynamics in the House for the rest of the
term, and it will probably impact how things play out in June in the primaries.
Given that this is occurring and this is happening to the leader of the House Freedom Caucus,
I would expect a response from the Twitter faction.
I would expect them to do it to other people in return.
And you're going to have Republicans just kind of duking it out amongst themselves in
a way that for a very long time was kind of a breach of protocol, for lack of a better
term, when it comes to sitting Republicans.
So we'll see how it continues to play out.
Anyway, it's just a thought.
y'all have a good day.