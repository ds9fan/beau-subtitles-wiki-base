Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump, Vice President Harris, and the
United States Secret Service, because there were developments involving both
details, we will start with the Trump stuff first, so his Secret Service
detail is reportedly figuring out contingency plans to deal with a situation in which he
is maybe found in contempt and ordered to go to jail.
The planning for this reportedly started sometime after the ADA up there said that they were
quote, not yet seeking gel time. So that's as far as the contingency planning has gone
in that regard. Realistically, this isn't going to be very hard. The main concern they're
going to have is not really securing a particular area. It would be figuring out how exactly
to deal with movement.
They're not going to be incredibly comfortable with not being able to move him, especially
if people know where he is.
So I'm not sure if they will bring somebody on from the New York court staff or maybe
bring in the US Marshals, but they will have to come up with some way to move him in the
event of an emergency. And then from there they'll also make sure his medical
needs and all of that stuff are taken care of if this eventuality was to
occur. Now moving on to the Harris detail, apparently they had what at this point
sounds like a medical incident. One of the, as I understand it, one of the
uniformed Secret Service officers was behaving erratically. I think somebody
said that they were they were speaking gibberish, I think is the quote. It led to
an altercation with the lead agent on that detail, a physical altercation. After
that the uniformed officer was cuffed and after an evaluation they wound up in the
hospital. So it does appear to be a medical thing. Some of the reporting
is mixed saying that the uniformed officer was armed and then some are
saying that they were unarmed. My understanding was that the uniformed
officer was armed, meaning they had their weapon, but the altercation did not
involve a weapon. And I think that's where the confusion is coming in when it
comes to the reporting. So yeah that's that's what the Secret Service has been
up to. I'm sure they miss the days of you know just like having to hide
girlfriends or whatever. Anyway, it's just a thought. Y'all have a good day.