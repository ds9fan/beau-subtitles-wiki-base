Well, howdy there, internet people, it's Beau again. So today we are going to talk about another
message from outside of the United States. And I have to admit I'm loving these right now
because there is nothing that is more effective at making you realize exactly how bizarre your own
internal politics are in your country than commentary from outside of it. And in this
In this case, it's just a person pointing something out that they believe the Democratic
Party is missing, and in any other country, it would make complete sense.
But we're not any other country, we're the United States.
Okay, so, here's the message.
G'day, Beau.
I don't know if you read these, but as an Aussie watching on, I can't help but wonder
how the Dems are missing a huge attack vector in this abortion showdown, men.
There are going to be a lot of young men becoming fathers who had no intention of doing so and
will see their intended futures jeopardized, particularly if Democrats start making noise
about paternal tests so the state is not responsible for children that fathers should be paying
for.
I imagine a lot of Republican daddies have paid for quiet abortions over the years as
their golden boys got some unfortunate girl pregnant and put the family name
in jeopardy. Dems need to get their stuff together. The world is in enough trouble
without four more years of Trump and a truly insane Republican Party. That is a
completely logical argument. It's a completely normal thought process and
something that would be important to bring up anywhere else. The United
States was founded by religious fanatics. Because of that, there is still a lot of
I don't know somebody's gonna say pilgrims, potato potato. Because of that
there's still a lot of holdover. Here's something that I don't know that a lot
of people know, particularly those outside of the United States. Roe was
decided in 1973. In 1965 and 1972, just a year before, those are the two cases
that protect birth control. At the time Roe was decided, the United States still
viewed that as an incredibly controversial topic, and in many places, it still is.
The pill, birth control.
Because of that, and because of those two debates kind of kicking off in earnest at
the same time, around the same time anyway.
The idea of using abortion for birth control has been the subject of a lot of rhetoric
for a very long time.
If the Democratic Party was to suggest that, that you know, well if you do this, then it
can be used for birth control, you know, and that's, it will help, you know, little Jimmy.
They would be demonized in the United States because people would fall back on a rhetoric
that they have heard for most of their life and, yeah, they would succumb to peer pressure
from dead people, talking points from the 70s.
Because the United States, when it comes to social progress, moves very, very, very slowly.
This argument would absolutely work on anybody under 30, maybe 40.
They would see the logic there, but the older demographics, that's taboo.
Using that procedure for birth control is taboo, and I know that doesn't make any sense,
But that's the reason.
Because the talking points of the Republican Party are so dated that you have to think
half a century behind to really be able to understand what the Republican Party is going
to say.
And for whatever reason, that rhetoric, it sticks.
I do believe that if laws got suggested saying that in any state where family planning is
limited, a woman can demand paternity tests, that might have some influence but
you can't you can't say why. You can't say why because the those people who
often talk about family values, yeah they they don't necessarily want that child
support payment.
That might work, but you can't link the two politically because, again, it's peer pressure
from dead people.
Anyway, it's just a thought.
Y'all have a good day.