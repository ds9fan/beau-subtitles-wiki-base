Well, howdy there internet people, it's Bo again.
So today we are going to talk about Ohio
and a small split that appears to be developing
within the Republican Party up there,
why it's occurring, a move by some Republicans up there
that kind of sparked this, and Schoolhouse Rock,
because apparently there are some people
who need to watch a few episodes.
Okay, so short version here.
Recently in Ohio, voters decided,
hey, we like reproductive rights.
Reproductive rights are good.
Some Republicans in the state legislature there
have decided that the voters really don't know
what they want, they're just, you know,
those people down there, we don't need to listen to them.
We want to rule, not represent.
and they have decided to introduce a legislation that takes the authority from the courts and
puts it into the assembly's hands.
That the assembly would decide everything about how this is implemented and remove that
power from the courts.
This gained some support from Republican lawmakers in Ohio.
It is worth noting that the Republican Speaker of the House did not have a favorable opinion
of this, saying, quote, this is schoolhouse rock type stuff.
We need to make sure we have three branches of government.
I mean, good on him for knowing that, I guess.
It is not really that surprising that Republicans are going this far in trying to undermine
the Constitution, basic principles of the Republic, because their key issue has been
taken away and they don't know how to respond to it.
They're the dog who caught the car.
Removing a wedge issue like reproductive rights is only good as long as it's a wedge issue.
Once it becomes resolved, well, it doesn't go well, especially if your party is on the
losing side, and removing reproductive rights is wildly unpopular.
So rather than admit that they held a position for so long and did not evolve on it, that
they have lost touch with the rest of the country and their voters, they have just decided
that their voters, while they're not smart enough to know what they really want, those
commoners, they need to be told what to do.
It is refreshing to see somebody in the leadership of the Republican Party be like, no, that's
not how any of this works.
But the fact that this gained traction in Ohio should really be a warning to the citizens
of Ohio.
Because they don't view you as their constituents.
They don't view themselves as your representatives.
They view themselves as your rulers, and you need to do what you're told.
Anyway, it's just a thought.
Y'all have a good day.