Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
the US House of Representatives, the Republican Party,
and their ever-shrinking majority in the House,
because a recent development has brought their majority
to even lower numbers.
Okay, so what has occurred?
Representative Bill Johnson, Republican from Ohio,
has decided not just to see if he's gonna resign,
He's going to resign a little bit early, the 21st of January, right around the corner.
In the U.S. House of Representatives, there are 435 seats.
To get a majority, you need 218.
The Republican Party started with 222, but then they lost Santos, and then they lost
McCarthy, and now they're losing Johnson, which will bring them down to 219.
They lose two votes, they can't get it through.
This is going to become incredibly important as everything starts to move forward with
the budget, hopefully move forward with the budget, because now those people who would
like to obstruct and punish Americans for them not being in power, I guess, it's not
going to take as many to vote against something to derail it. The Republican
Party's dysfunction is becoming more and more apparent. In fact, one of the
reasons Johnson is apparently leaving is that he feels the upper echelons of DC,
well, they just don't care about average working people. That's what his statement
said. Now on February 13th there's going to be a special election to determine
who takes Santos seat. That could make things interesting because right now
there's no clear leader between the Democratic and the Republican candidate.
it could go either way. So the House continues to be in disarray as it has
been since the Republicans took the majority, and you can expect that to
continue until the next election at this point. And keep in mind there are other
things going on that might lead to other Republicans leaving. The Republican
Party might, it is theoretically possible for the Republican Party to lose their majority
before the next election. And all of this can be laid at the feet of Trumpism. The dysfunction
that the Republican Party is seeing right now is a direct result of Trump's brand of leadership,
and the rhetoric that came about with Trumpism, because the voters are now
asking some of their representatives to deliver on these promises that Trump
never had any intention of keeping, but that rhetoric went out there, they think
it's something that can actually occur, so they're demanding it, and some of the
representatives are trying to at least appear like they're attempting to
deliver it. This dysfunction, we need to get used to it because it's going to
continue for quite some time. Anyway, it's just a thought. Y'all have a good day.