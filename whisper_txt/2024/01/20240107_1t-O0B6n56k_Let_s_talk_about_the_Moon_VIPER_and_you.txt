Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about, well, the moon,
and NASA, and Viper, and how you can be involved,
in a bizarre way, with one of NASA's projects.
Okay, so, towards the end of the year,
I wanna say November is when it's supposed to be
on the moon, but NASA is sending something up called Viper.
VIPER stands for Volatiles Investigative Polar Exploration Robot, or rover.
And its purpose is to get up there, go around the lunar south pole, and look for water ice.
This is to pave the way for the Artemis missions, which will eventually lead to
manned bases on the moon. So they have to find the water.
And it's going to investigate in an area that is less than ideal.
The rover is going to try to survive for I want to say around 100 days in some pretty
inhospitable climates and lighting situations.
So that's the overall mission and what's going on.
So how can you be involved in it?
if you get them your name by 1159 p.m. Eastern Time on the Ides of March, March
15th, they will take that information and attach it to the rover so your name
will go into space. If you want to do it it's www.nasa.gov
slash send-your-name-with-viper. Yeah, they could have. I'll put the link down below.
Couldn't have made that easy. And the idea behind doing this is obviously to get
people interested in space travel. That's why NASA's doing it. This is something
you might want to involve your kids with. If you're trying to find something to
create a spark, to give them another interest, to get them to look towards the
stars or whatever, this is something that's unique. Your name will go into
space. And as far as NASA's part and their PR part what they do is if you go
this link and put in the information. It's really just asking your name. They
give you a little digital boarding pass and there's a QR code that you can go
and get more information, sign up for more stuff if you want to, but it's
definitely a way to kind of kickstart an interest in space and space travel. So
it's something interesting that I ran across I thought y'all might enjoy. Anyway
It's just a thought, y'all have a good day.