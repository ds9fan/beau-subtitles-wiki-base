Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Hunter Biden and proceedings and how
everything is shaping up and Hunter Biden's little surprise today.
So if you missed it and you don't really know what happened, the Republican
party in the House, they are trying to get to the point where they can hold Hunter Biden
in contempt of Congress for not showing up to testify and defying a subpoena.
The problem with this is that he's totally willing to testify.
He just wants to do it publicly and not in some back room.
And they're still pushing on the idea that, well, that's still contempt.
I want to start before we get into this and get into his little surprise, I want to start
by pointing out that there is no precedent for the House to hold a private citizen, which
is what Hunter Biden is, in contempt when they've agreed to show up and testify publicly.
That's actually not really a thing.
They can try to make it a thing, but that's not how this is supposed to work.
Okay, so obviously he showed up today to this hearing where they were talking about it.
Little surprise visit.
And it threw the Republican Party into disarray.
And a lot of their time was spent grandstanding, you know, telling him play stupid games, win
stupid prizes, do what we say, you know, because we're your betters, we're your rulers type
of thing.
Again, he's a private citizen.
Now the question that Republicans should be asking themselves is why don't the Republican
party? Why doesn't the committee want him to testify publicly? You have to figure
out why they have the position they do because it's important because when you
really think about it it's one of two things. Either they don't believe that
you, the Republican base, are smart enough to process what you're going to
hear and they need to like handle it for you and tell you what to think because
you're just not that bright, or they intend on taking pieces of that deposition and leaking it
selectively to influence your opinion and tell you what to think. Now that last one, that's what
Hunter Biden's team believes it is, so they want to testify publicly. Why would Hunter Biden's team
think that? I mean, I don't know. It's not like that's what they did, you know, concerning like
all the allegations of voter fraud, the fake electors, the 2020 election in general, COVID,
the insurrection. I mean, okay, so maybe they do have a little bit of a history of doing stuff like
that. But it's interesting to me to watch somebody who the Republican Party has just generally
characterized as some incompetent, shot-out, drug-addled loser. Outplay them politically
at every turn. That has to be embarrassing. And you see that it's embarrassing because
when Hunter Biden does the smallest thing, like today, he got up and left. Once his point
had been made, him and his team, they got up and left. And it was right before the space
laser lady was supposed to start talking and she straight up lost it. She started
you know calling him a coward and all of this stuff. He's leaving right when it's
my turn to talk. I mean he's a private citizen he's allowed to do that. It is
it's unique from my perspective because you know years ago when the allegations
were first made, we did a video, we went through the evidence. There's evidence of
stuff that's shady, but nothing that amounts to illegal. And we went through it,
went through the whole timeline. Since then, I really haven't personally cared
much about this. Like it's not something I've been incredibly interested in, but
after this? I have to admit I kind of want to hear what he has to say and that
should worry the Republican Party. I am watching the Republican Party
rehabilitate his image in real time and turn him into some anti-establishment
folk hero. He's a Biden and you are turning him into an anti-establishment
folk hero. That is wild. At the end of it, you have people who have defied
congressional subpoenas super mad that he's willing to testify in public.
That's how it breaks down. You have members of Congress who have defied
congressional subpoenas trying to make a big deal about the fact that he wants to
testify in public so everybody can see it. And at the end of this, you have to
remember that according to what the Republican Party has told their base,
well they have all the evidence. They know what he did, so why not confront him
with it? Unless of course that evidence is just selectively leaked
stuff designed to tell the base what to think, and they don't actually have the evidence
they think they do.
I would be unsurprised if by the time this is all said and done, Hunter Biden, a person
who had very much the reputation of the black sheep of the family, like even among people
who are sympathetic is turned into somebody that has a more prominent and powerful political
profile.
And it will be because the Republican Party is being outplayed.
I believe the saying today is, well, play stupid games, win stupid prizes.
Anyway, it's just a thought, y'all have a good day.