Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Nikki Haley,
altering her rhetoric a little bit,
making it a little bit more pointed.
And her saying something, or I guess really not saying
something, that at this point for most Republicans,
for most Republican candidates, has been a given.
And she's not just giving that away, and I think you're probably going to see more of
this from other Republican politicians.
So she was asked if she's going to support Donald Trump if he gets the nomination.
Now for a long time, the standard view in the Republican Party is to bend the knee and
and say, yes, dear leader, he's our man, he's the guy, and yes, if he gets the nomination,
well, I'll support him.
She didn't say that.
I'm running against him because I don't think he should be president.
The last thing on my mind is who I'm going to support.
The only thing on my mind is how we're going to win this.
first sentence, I'm running against him because I don't think he should be
president. There's a whole lot of Republicans who have been voting for
Nikki Ailey in these primaries who also believe that. Then the person tried to
kind of ask it again and make her commit one way or another and she said I'm gonna
run and I'm gonna win and y'all can talk about support later. Right now you can
ask him if he's going to support me when I'm the nominee is what she said.
Now let's be real.
If Haley was to win, would Trump support her?
No, of course not.
He would say that it was somehow tricked and rigged and everything was wrong because he
can't lose because nothing is ever his fault.
You know, the real attributes of a leader, right?
You're going to start seeing more Republican politicians do this.
You're going to see more Republican politicians try to put a little bit of distance between
them and Trump because while the polling says one thing and he still has an energized base,
the results in courtrooms, they haven't been super favorable to the former president.
And there's a lot of politicians who are starting to maybe see something else in the
future and maybe see that the Republican Party needs to go a different way if it plans on
winning.
So it's still going to be background noise at the moment, but it's going to get more
and more pronounced.
Keep in mind, it's not I'm running against him because I think I'm the better candidate
or I think I would do a better job. I'm running against him because I don't
think he should be president. There's a whole lot of Republicans that feel that
way. Anyway, it's just a thought. Y'all have a good day.