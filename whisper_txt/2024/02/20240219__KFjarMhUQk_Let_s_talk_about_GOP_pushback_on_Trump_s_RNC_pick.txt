Well, howdy there, internet people, let's bow again.
So today we are going to talk a little bit
about Trump's drive to have his chosen people
as the top people in the RNC,
in the Republican National Committee.
And we're gonna talk about how some Republicans
are starting to realize that Team Trump,
well, they're about Trump and Trump only.
They're not actually about the Republican Party.
If you missed it, Laura Trump, who is one of the people that Donald Trump would like
to install, well, she said that she would basically spend every penny to get Trump re-elected,
to get Trump back in the White House, and that getting Trump elected, well, that was
the one and only priority.
Michael Steele, who is a former national chairman of the RNC, offered some advice, a little
bit of information about the job, perhaps some training, I believe might be how he phrased
it.
And we're just going to run through what he said.
I will start by saying wrong answer.
That is not the number one responsibility of the RNC to elect Trump.
The number one responsibility is to elect every candidate on the ballot on behalf of
the party.
It is to raise money for those candidates who are on the ballot.
It is to place the infrastructure every candidate will need.
You are required, so let me give you a little bit of training here, Laura, since you want
the job, that's actually in the quote, you are required to organize and coordinate every
state party and the territories of the United States who are in that Republican family and
you are to provide them with a platform from which you will launch a platform to talk about
what the party believes in.
As we talked about when this information came out and when that quote first came out, being
at the top of the RNC, that's not actually like a do-nothing job, and it's not actually
a job where the sole goal is to elect a presidential candidate.
In fact, it's not even like one of the top jobs.
There's way more to it.
There are bigger priorities.
There are a lot of Republicans who are starting to realize that Trump has been using the Republican
party is his own personal piggy bank, and that the Trump world isn't actually Republican,
not actually conservative.
They have a different set of goals.
You're starting to see more and more prominent, influential Republicans come out and say that.
But that occurring more often, it might carry some sway with a lot of the more moderate
Republicans, those who didn't fully buy into the Trump rhetoric, those people that at one
point in time would have been called small government conservatives, those people who
are actually conservatives and not just authoritarians.
So I would expect to see way more of this over the next year or so.
You're going to see a lot more Republicans come out and call out the Trump machine in
various ways.
Anyway, it's just a thought.
Have a good day.