Well, howdy there, internet people.
Led Zebo again.
So today we're going to talk about, I guess, Utah and girls
basketball and something that occurred
and what the remedies are and whether or not
they're going to be employed and how everything is shaping up.
So if you have no idea what I'm talking about,
don't know what happened, and you miss this story,
A person with the Utah State School Board named Natalie Klein put out a social media
post with an image of a 16-year-old girl.
And that image, well, I'll just read what the girl's father said, here's a person that
supposed to be in a position of leadership that advocates for our children's safety,
well-being, their privacy, and she's the one who has instigated this post that has led to all this
hate. The girl had to go into hiding with police protection because the post led people to believe
that she was trans.
She's not. She's not.
Now, the governor and lieutenant governor, they have said that Klein's going to be held
accountable.
I mean, sure, that sounds good, but unless the legislature is going to impeach her,
she has to resign or lose at the polls.
So I don't know how that's going to happen really.
But here's the thing.
This whole moral panic, the way they have convinced people to other, and attack children,
and bully children, is by saying that, oh, well, we're protecting the girls.
a trans girl being on a sports team ever led to a cis girl needing police
protection going into hiding? Because if the answer to that is no, you're not the
good guys in this story. The comments that were on the post, it's not just all
of the hate. It was the whole idea of taking this child and making assumptions
about her body. Comments that I promise will end up being in her mind for years.
Why? Because you thought it was okay to bully a child.
She wasn't the type of child it's okay to bully and that's why it's a problem.
them. If your moral panic has you bullying children on the internet, you're
not the good guys. You might want to re-examine a whole lot of things about
your life.
A child, a teen girl, wound up needing police protection because of one of these posts.
So somebody could score some cheap points for re-election.
Anyway, it's just a thought.
Y'all have a good day.