Well howdy there internet people, Ledzebo again.
So today we are going to talk about Michigan
and something that occurred there
that is probably going to be the start of a trend
and it might help mitigate some things.
And it's something that has been talked about
for quite some time but it never really got,
it never really got a full push the way it should.
People talked about it and said, hey this might help
But, for the most part, it got ignored.
And we're going to talk about how that might be changing.
One of the things that has been suggested as a method of mitigating incidents at schools,
mass incidents at schools, is making sure that parents are more responsible when it
comes to a child accessing a firearm. The first time I remember this really
getting any significant push was after Parkland when there was an idea to bump
the age to purchase a rifle from 18 to 21. The subtext of that is if you raise
the age then you know it'll be the parents who are responsible for managing
access to a firearm. That fell out of favor the way it always does because
people reverted to their normal talking points rather than talk talking about it.
So again nothing happened. A jury in Michigan is holding a mother responsible
for basically allowing access to a firearm
that was used in a mass incident at a school, used in a mass shooting.
She was found guilty of
four counts of involuntary manslaughter. That's probably going to become a trend.
And people are going to say that that's not right.
But when you compare it to anything else, it's the way it's always been.
If you were to provide access to alcohol, to minors, and then they got on the road,
what would happen?
It's the way it's always been with other stuff.
This is probably going to be a trend that is going to spread across the country.
The firearm, if it is not locked up, if it is not secured, and it was a parent who granted
access to that firearm and it gets used, I'm going to suggest there's going to be a high
probability that that parent is now going to be on the case. They're gonna be on the
case with the kid. It is something for people to note because there are a lot
of people who have the idea that, well, not my kid. Okay, maybe that's true, but
you know what? You should still secure the firearm. Just saying that that'd
That would probably be a good idea.
And now there is a legal precedent that is being set that will spread.
Those firearms need to be secured.
And I would remind everybody that should happen anyway.
That should be something that was already occurring.
So we can watch this, but I'm fairly certain you're going to see it in other locations.
Anyway, it's just a thought, y'all have a good day.