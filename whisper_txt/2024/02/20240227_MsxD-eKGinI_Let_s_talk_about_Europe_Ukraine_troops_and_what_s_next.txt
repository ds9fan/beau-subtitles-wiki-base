Well, howdy there internet people, it's Beau again. So today we are going to talk about Ukraine
and Europe and things not being off the table and what it means for the United States.
Because we have talked about something for probably a year and a half on the channel.
And it's been one of those things that was a hypothetical and it was viewed in the,
oh, well, this is something that might happen one day if everything went wrong.
Well, Republicans happened. They played with the aid going to Ukraine.
Now you have a situation where European nations are saying that they will not take sending troops
to Ukraine off the table. European nations, meaning NATO allies.
From the beginning it was clear that as long as Ukraine got a steady supply of aid they would win.
The Republican Party didn't like this because it made Biden look good.
Now they have interrupted that supply of aid.
European security is tied to Ukrainian security. European nations might
intervene. If they do, just the way European security is tied to Ukrainian
security, American national security, the national security of the United States,
is tied to European security. Republicans in Congress who did everything they
could to disrupt this aid for a political talking point are on the verge
of expanding the war. As they currently sit, not approving the aid, not doing the
bare minimum of their job. It runs the risk of expanding because Ukraine will
need help because they don't have the supplies. If they get help from NATO
allies, what do you think happens?
The Republican Party's quest for social media engagement and Twitter talking points and
being obstructionist when it comes to the national security of the United States, it
It is on course to cause a major issue for the U.S.
They need to approve that aid and they need to do it quickly.
Anyway, it's just a thought, y'all have a good day.