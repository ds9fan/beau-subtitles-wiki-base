Well, howdy there internet people, let's bow again.
So today we are going to talk about Iran,
learning a lesson the United States has had to learn
over and over and over again.
Because the reporting now suggests,
and the information coming out of Iran now suggests
that Iran is, well, they're concerned about the actions
of some of the groups they have historically supported, feeling that
maybe they're going a little bit too far and that Iran itself might end up paying
the price for that. So if you don't know what's going on, there are a lot of
non-state actors all over the Middle East that Iran has supported and they
have they have pumped them up. They're like, yeah, we're gonna we're gonna fight
the West in every way shape and form. The thing is Iran, the Iranian government,
doesn't actually mean that. That's just something to pump up the non-state
actors and get them hyped. The non-state actors do believe that, as is often the
case in situations like this. So some of the groups that they have been
supporting, some of the groups that the Western media calls their proxies, well
they're trying to do what they've always talked about and Iran's like whoa whoa
whoa hang on a second you understand you're you're upsetting the Chinese and
the Indian governments right you do that those are our allies looking at you
hooties so it's causing an issue there now in Iraq the groups that they have
supported there well they want to go after the US because Iran's always been
like yeah you know blah blah blah and now they're realizing that that's not
really what the Iranian government wants. It is important to remember that rhetoric
and foreign policy don't actually line up. So here's the thing.
Right now, the U.S. is looking at Iran, and it's that meme with the guy with his head
in the noose first time, because Iran is having to deal with something the U.S. has had to
deal with numerous times, where the U.S. trains a group, arms it, organizes it, uses it for
a little bit, or completely uses it to accomplish its goal, and then later there's an issue
with something they have decided to do, which more than likely was actually in line with
the rhetoric that caused the U.S. to support them to begin with.
But things happen.
Iran is dealing with this now.
There is a strong indication that Iran has now told the Houthis and the groups in Iraq
and north of Israel to chill out.
The problem is those groups, they actually believe all of the rhetoric that Iran has
espoused.
So this could lead to a lot of very unique situations, especially if Iran wants to ensure
that Iran itself is not punished for the actions of the non-state actors.
I mean one of the easy ways to make sure that doesn't happen is to make sure that when the
West does respond, it knows exactly where those non-state actors are.
Things are about to get real dirty in the Middle East.
It is important to remember that there were a lot of countries that told the Palestinians
for years.
If you generate that public outcry, if you get the international community on your side,
we will be there to back you up.
And now that's happened, and none of those countries are showing up.
You have to ask why.
There are a bunch of reasons you can go to the video on the second channel, let's talk
about the roads to foreign policy dynamics.
the short version is none of those countries are going to try to pose an
existential threat to Israel, a nuclear power. They're not going to try to take
territory from it. It was all a lie to keep the Palestinians hyped up. The same
way it was all a lie to keep the Houthis hyped up, to keep the groups in Iraq
hyped up. Iran doesn't actually mean any of that, but it has said it, it has armed
them and now it's in a situation where it very well may end up paying the cost
for the actions of its proxies. Something that there are strong indications that
Iran wants to avoid. There are probably going to be some pretty unique
developments over the next week maybe two anyway it's just a thought y'all
Have a good day.