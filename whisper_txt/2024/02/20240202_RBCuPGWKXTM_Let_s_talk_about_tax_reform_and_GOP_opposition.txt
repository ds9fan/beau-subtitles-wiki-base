Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about that bipartisan tax bill
and why there's opposition to it.
Because it's kind of a mystery when you really think about it.
When you really kind of look at the bill with something
that's just going to overwhelmingly
help most Americans, you have to try to figure out
why there are some people who are supposed
to be representing the interest of Americans who
would be opposed to it.
It would be strange, right?
But when you get down to it at the end of it,
just like most Scooby-Doo mysteries,
the answer is pretty much always the same.
It is unique.
It is strange to have a politician just admit it.
And that kind of happened.
definitely acknowledged one of the reasons that a lot of Republicans are having issues
with this bill.
It's not ideological.
It's not anything like that.
It's that, I mean, frankly, they need to hurt you.
They have to hurt you.
They can't help you.
That would be bad for them.
So Senator Chuck Grassley said, I think passing a tax bill that makes the president look good,
mailing out checks before the election, means he could be reelected.
Okay, first, there's no checks in this, but yeah, it would help most Americans.
And for a lot of Americans, they perceive it as a check, because it would help them
that much.
But the reason they're opposed to it isn't some policy issue with it.
It's that if you get that check before the election, well, you might credit the person
who signs the bill and the law.
You might think that government is working.
You might think that, like, they're representing you, and you can't have that.
That's bad.
the American people are doing well, it is bad for the Republican Party. So they
have to actively try to sabotage you and hurt you financially. So you will learn
your lesson and vote for them because they obviously have your best interest
at heart. Look at what you made me do. Yeah, every every Scooby-Doo mystery they
all pretty much in the same way. When you take the mask off, some rich powerful
person, typically an old white guy, that is trying to maintain that power and
wealth. That's the issue. That's the end of every mystery, and it certainly
appears to be the reason for the opposition to this. Because they don't
want you helped. They don't want you economically in a better position. They want you hurt. They
want you broke. So you blame Biden, even though it's them actively trying to sabotage your
economic well-being. And kind of admitting it, that's new. Anyway, it's just a thought.
y'all have a good day.