Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump in New York
and how things are expected to proceed from here,
where James is at and her position
and what Trump's next steps are,
because it's Trump, there's always, you know,
the next move, okay.
So, Trump has stated that he feels he was denied his ability to provide a counter-judgment,
to propose a counter-judgment, basically negotiate how things move forward.
It is worth noting that in a lot of cases, there's a tendency to allow the defendant
to structure, at least have some say in how it moves forward.
So there's that.
We'll see how that plays out.
A letter was sent in to the judge
asking for the ability to do that.
I don't know how that's going to move forward.
But either way, that would not be a large delay.
The other thing is that Trump is expected to appeal.
The general consensus is that Trump will appeal on the grounds that, well, unlike all the
things he gets super mad about, like people walking across a line without a permission
slip and stuff like that, that in his thing, there's no victims.
That's apparently what people believe he's going to argue.
It's also worth noting that most of the attorneys that I have talked to are like, yeah, that's
not going to work.
And these are the attorneys that were correct about how everything would play out thus far.
So there's that.
It's worth noting, if Trump does appeal and is less than successful in those endeavors,
he will owe both pre-judgment and post-judgment interest.
Interest is racking up on this.
I mean, it's a couple million dollars a month.
I want to say 600,000 a week is what they finally worked out as a good estimation number.
So there's that.
There is speculation that Trump may have some issues in getting the actual cash to deal
with this. James, the attorney general, where's she at? She's ready to take his
buildings. She is not, she is not very sympathetic in any way shape or form
right now. She said, if he does not have the funds to pay off the judgment, then
we will seek judgment enforcement mechanisms in court, and we will ask the
judge to seize his assets. Yeah, I feel like she is not in the mood to to
entertain a lot of delays. So that's where everything is sitting right now. My
guess is we'll start to see some of this form up and have a clearer picture about
how things will move forward mid-March. Anyway, it's just a thought. Y'all have a good day.