Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about comic books, I guess.
We're going to talk about X-Men and a rumor
about how a character is going to be portrayed
in an upcoming series involving that franchise there.
As you might imagine, there are people who are unhappy.
And it's one of those videos.
So there are people who are unhappy that Morph, a character
who can shapeshift, that character is rumored, I guess,
to be non-binary in the upcoming series.
And this has people upset, screaming, woke, and so on
and so forth.
So, yeah, let's just go ahead and go through it.
Let's start with this.
The only things we don't have room for are hatred, intolerance, and bigotry.
That's Stan Lee.
Let's just start there, okay?
Let's also remind ourselves that the X-Men were about civil rights.
That's what it was about.
X-Men were woke from the beginning. That was their whole thing. 1963 they came
out, they introduced at that point in time, and they were they were a metaphor because
not just did you have the the good and bad, you know, you also had the normal
humans who didn't like the mutants. And that hatred, that bigotry against them,
Well, it changed things. It not only made them different, but it was a great
metaphor for what was happening with the civil rights movement in the country at
the time. Now if you have a problem with that last sentence, that was also Stan
Lee a quarter of a century ago. He said that in 2001. He was talking about the
series. It's what it was always about. You know, the Sentinels, go back and really
look at it and compare the time period that those things came out with what was
going on in the United States and realize those are a giant metaphor for
white racist cops beating black people. The X-Men have always been woke. They've
always been about civil rights. 1966, Black Panther came out. It was all part
of that attempt to bring it to the forefront. Using X-Men to advance
LGBTQ rights is totally on-brand using X-Men to help a marginalized group
become more accepted. It is totally on-brand. That's what it was there
for from the beginning. It's just how it started. It was always woke. It was
always woke. The thing is, when you look at anything like this, anything
that's good, that's a lasting franchise, you're gonna find out it
addressed social issues. It's what made it resonate. If you are somebody who is
truly upset about this, not just upset because that doesn't respect the source
material or or something like that but you have a problem not with you know it
being changed at all but the fact that the shape-shifting person who would go
back and forth I'm assuming being non-binary if that's your issue just
remember that if you were in an issue of X-Men you would be one of the bigoted
normal people holding a sign. That's your character. That's who you are. You are not
being the hero that Stan Lee knew you could be. Anyway, it's just a thought.
Y'all have a good day.