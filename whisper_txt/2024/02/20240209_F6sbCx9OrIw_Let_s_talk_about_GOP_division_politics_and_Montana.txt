Well, howdy there, Internet people.
It's Bo again.
So today, we are going to take a look behind the curtain,
see how politics might actually happen.
We're going to talk about an unusual set of circumstances,
Montana, Louisiana, the House and the Senate,
and how they all came together to almost do something, maybe,
because we don't really know.
But there were a lot of unique events that kind of lined up,
and there's a lot of rumors about it.
So let's start off with this.
All of this, except for what you can see in the newspapers,
this is all rumor.
Of course, this didn't actually occur.
This is just the discussion.
OK, so Speaker Johnson is rumored
to have been interested in endorsing Rosendell for a Senate seat, which is weird because
Speaker Johnson, of course, is in the House.
And the seat is in Montana.
Johnson is from Louisiana.
So there's no direct connection here.
It's really strange.
It's also really strange because in the Senate, they have their eye on Montana.
The GOP leadership in the Senate feels like Montana is a really good spot for them to
pick up a seat.
They want Tester's seat and they want it bad.
So they have already lined up their person, a person named Sheehee.
So Johnson throwing his weight behind somebody else?
That's bad form.
Why would he do that?
I don't know, but maybe if Rosendell was going to give him a vote for that stand-alone aid
package for Israel, that might do it.
Because Rosendell was against that before he was for it.
And all of this happened around the same time.
And then the Senate, allegedly, kind of got ahold of Johnson and was like, hey, you need
to stay on your side of Capitol Hill, son.
And that endorsement didn't go anywhere.
And there's probably a lot of hurt feelings over this because it appears that deals got
made that, well, couldn't be honored.
Of course, again, this is all a rumor.
Nobody knows this for sure, except for anybody who might have been involved with it.
But it certainly appears that a deal was made in the House, kind of exchanging a vote for
an endorsement to run for a Senate seat.
And then the Senate got super mad about that interference and kind of pushed the whole
thing to the side. There are a couple of things that are important to note about
this. One is that no politician likes primaries. You don't want to waste
resources fighting over the same seed, especially if it's going to be one
that's hotly contested and one that you really think you can pick up. So that's
one of the reasons the Senate might have been upset by this. The other thing to
remember is that Johnson, he is definitely flexing a little bit, showing
how he can render the MAGA faction irrelevant and all of that stuff.
Getting involved in the Senate's activities, that might be a step too far.
That might be something that causes a little bit more internal division, and it
is important to remember that generally speaking the Senate wins when these
little arguments occur. The Senate tends to come out on top. So that kind of
political wheeling and dealing, we don't often get to see any of it and sure we
didn't really see it this time, but there's enough people talking about
various aspects of the rumor for it to be put together. Again it's a rumor,
allegations. At this point everybody is walking back their part in it if they
played one at all and I would imagine that, I don't know, maybe by the middle of
next week I doubt they'll even admit they're gonna run a race in Montana
with the way they're walking stuff back. Anyway, it's just a thought. Y'all have a
Good day.