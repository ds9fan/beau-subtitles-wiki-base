Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Wisconsin maps one more time because it is wrapped up.
That storyline is kind of done for now.
The governor signed the new maps into law.
So what does this mean?
It means that the hold the Republican Party held over the state through gerrymandered
maps is broken.
As far as my idea of this being an opportunity for the Democratic governor to play hardball,
when he signed the maps into law, which are pretty much even, he said something to the
effect of, you know, when I ran I said I wanted fair maps and I meant fair maps,
not maps for my party. And that's what he decided when he neglected to play hard
ball and exercise the power that he had. So what happens now? These maps will be
in effect for the next election, for the elections in November. It is
pretty evenly matched with a number of competitive districts in both the House
and the Senate there. So what does this mean for you? What does it mean for
people in Wisconsin? It means that these elections are now entirely about
turnout. They are about turnout. You have some districts that are, they lean red,
you have some districts that lean blue, but they are much more evenly matched
and then there are a bunch of very very competitive districts that are just
straight toss-ups. In those districts in particular, it's all about turnout. So the
various political machines, the political parties up there, their job is now about
creating a platform that energizes people to show up. That's what it's about.
It's weird that when you, you know, have districts that aren't gerrymandered and
you have competitive districts, it turns into a situation where the representatives,
I mean, they kind of have to represent you because they actually need your vote.
It's not a situation where that district is guaranteed to a certain party.
There are still some of those on the map, but they've become a lot more competitive.
They would be more competitive, by that I mean they'd lean blue, if the governor had
elected to play hardball and just let it go to the courts.
And he decided not to do that and decided to be fair and all of that stuff.
So that's where it's at.
This storyline is kind of wrapping up.
And just for all time's sake, we'll say Justice Protasewicz one more time because she's the
reason this happened.
the reason that Wisconsin is no longer gerrymandered.
Anyway, it's just a thought, y'all have a good day.