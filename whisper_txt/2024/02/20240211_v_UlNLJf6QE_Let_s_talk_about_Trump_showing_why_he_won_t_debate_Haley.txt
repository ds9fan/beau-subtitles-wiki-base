Well, howdy there internet people, it's Bo again. So today we are going to talk about Donald J
Trump and Nikki Haley and how things are getting a little bit more heated between the two and
how Trump being forgetful
says things that leaves him open to pretty pointed responses from her and
how
This scenario, the type of thing that we're about to talk about and what occurred, this is why Trump doesn't want to
debate her.  It's why he won't get on stage with her, because he's afraid of this happening live.
Okay, so what occurred?
Trump was telling one of his stories, you know, all the stories that he tells about people.
He's like, oh, and this person came to me and said whatever.
We've heard them about a hundred people and if you believe them, you believe them, I guess.
But he's telling one about Nikki Haley and how he says that she said that she would never
run against him.
And how her husband was there with her.
And then he says, where's her husband?
Where is he?
He's gone.
He knew, he knew.
that yeah I mean basically kind of drawing attention to the fact that her
husband has not been joining her on the campaign trail and I get it I can
understand how somebody might think that that would be a I don't know an
indication that there was some kind of trouble in their marriage and how that
might be a reason to go after it and maybe draw up some moral outrage or
something, right? If you don't know, her husband is an officer in the National
Guard who's been activated. He's in Africa. That's why he's not on the
campaign trail with her. So Haley says, Michael is deployed serving our country.
Something you know nothing about. Then goes on, someone who continually
disrespects the sacrifices of military families has no business being commander
in chief. Can you imagine this occurring on a debate stage? Can you imagine Trump
with the way he's taken to rambling and just kind of saying whatever comes to his
mind, stream of consciousness stuff, can you imagine how she would respond to it?
pointed remarks that just expose him for who he is. He's supposed to be somebody
worthy of being president of the United States. That's why he's running. I would
suggest attacking somebody because their husband wasn't joining them on the
campaign troll, for any reason, would be below that office.
Just me saying that.
And I would suggest that that should preclude them.
But even if you don't believe that, what does it say to have
your research so bad that you make a statement like this and
not know the answer and not know that her husband has been called up and is deployed.
And from what I understand, I don't even think this is the first time it's happened to it.
It seems like maybe somebody that can't even get basic information like that right, maybe
they don't have any business in the Oval Office.
as she said what was it she said maybe she had that right has no business being
commander-in-chief I mean I don't know might make sense but regardless of how
you feel about her response her response is why Trump is terrified to get on a
on a debate stage with her.
Anyway, it's just a thought.
Have a good day.