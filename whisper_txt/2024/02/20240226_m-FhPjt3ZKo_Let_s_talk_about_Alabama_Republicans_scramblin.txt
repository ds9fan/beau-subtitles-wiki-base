Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Alabama
and how Republicans in Alabama are currently scrambling,
trying to distance themselves
from the totally predictable outcomes
that have occurred ever since that recent ruling.
And we're going to just kind of run through
some of the things that are happening
and some of the things that have been said.
and just catch everybody up. Now, if you have no idea what's going on, in Alabama
the Supreme Court there in the state basically decided that, oh, fertilized
frozen embryos? Yeah, those are children now. Those are children, legally speaking.
So, obviously, people and entities providing IVF treatments, they have a lot
exposure now that they didn't have before. The Republican Party is to blame
for all of this. Their rhetoric, their moves, their appointees, everything
tracks back to the Republican Party, so they're trying to distance themselves
from something that is wildly unpopular and again was completely predictable. It
It appears that their current move is to try to blame the fertility clinics that are no
longer providing the services that they were providing before, as if the fertility clinics
somehow have a problem providing those services that they were providing until Republicans
stepped in and stopped them until that ruling came down. Now what has occurred
and the way they are trying to shift the blame to the fertility clinics is the
Attorney General was like, oh don't worry we have no intention of going after y'all.
I mean that sounds good and because of that you have statements like this. These
clinics should start providing these services again. It is not fair to patients
to have their embryos held hostage and procedures canceled." This is a Republican
representative in the state legislature there trying to try to make it seem as
though the fertility clinics don't want to provide the treatments that they had
been providing all along until, you know, Republicans in Alabama decided to give everybody
freedom.
There have been some very colorful responses to this.
I think the best response came from UAB.
We very much appreciate Attorney General Steve Marshall's support of IVF.
Because Attorney General opinions are not binding on Alabama courts, we are required
to follow the Alabama Supreme Court's decision unless and until it reconsiders its opinion,
or the Alabama Legislature addresses it through legislation."
Yeah, they're right.
We talked about it already.
The opinion of the Attorney General, sure, it's comforting.
It means absolutely nothing in a courtroom.
It does not help provide any real insulation for the clinics.
They can't go off that.
The people who would have to fix it are the people currently apparently blaming the hospitals,
blaming the clinics.
So why don't they just fix it?
Because doing so undermines their own rhetoric.
If they go in and they fix this through legislation, they have to admit a couple of things.
I mean, I guess they don't have to, but the voting population is going to know a couple
of things right away.
A, it's their fault.
B, all of the rhetoric they put out, well, that's what led to this.
And all of the rhetoric they have put out since the ruling is also let's just say lacking.
So the Republican Party in Alabama has an issue.
They have to acknowledge that they messed up in a huge way, in a way that is wildly
unpopular, in a way I would like to point out is going to happen again with other issues,
to be clear because all of this was completely predicted when the US Supreme Court handed
down its ruling and states all over the country decided to act on it based on the wildest
rhetoric they could find that would give them the most amount of social media clicks.
The bad policy that Alabama Republicans enacted, it was predictable.
The outcomes were predictable.
They're going to have to acknowledge that.
It's up to them to fix it.
The people trying to blame the clinics right now, it is up to them to fix it.
Nobody else.
The hospitals, the fertility clinics, they were providing these services.
It's not like they suddenly decided they didn't want to.
It's bad law, bad policy.
All in the name of freedom.
That apparently just constantly restricts freedom because nobody knows how it works
anymore.
Anyway, it's just a thought.
Y'all have a good day.