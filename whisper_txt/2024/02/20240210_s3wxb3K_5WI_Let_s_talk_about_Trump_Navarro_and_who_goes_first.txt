Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump's crew
and who goes first, because it looks like we may finally
have that answer.
Throughout all of the entanglements,
one of the big questions that has popped up
is who is actually going to end up going behind bars first.
And it appears that Peter Navarro might be that person.
So to refresh everybody's memory,
Navarro, Trump advisor, January 6th hearing going on, subpoena.
He's like, yeah, whatever, not exactly cooperative
when it came to the committee, the hearings,
and all of that stuff, didn't provide documents and testimony.
Contempt, then it went to court, found guilty.
Sentenced to four months.
Has to go do four months in confinement.
Been appealing it.
And was basically asking to stay out of confinement
while the appeal process played out.
The judge said no.
The judge has said that Navarro needs to report to the designated facility at the time the
Bureau of Prisons says he needs to report there.
When that is, I actually can't find out.
That information wasn't easy to come by.
Now Navarro is saying, of course, that this whole thing is a giant political witch hunt,
political bias, so on and so forth.
The judge didn't really go for that
and said that there was, quote, no actual proof
that it occurred, and went on to say
that the, quote, defendant's cynical,
self-serving claim of political bias
poses no question at all, let alone a substantial one.
So that argument was not going anywhere.
Okay, so what happens here?
Navarro can go do his four months,
or there's still one more possible appeal.
It seems unlikely that that appeal would be successful
when it comes to keeping him out
while further appeals play out.
It does look like Navarro has exhausted the options.
So, although we don't know when, currently it seems like the most likely outcome from this point
is that Navarro goes and does his four months.
There is still a slim possibility that something could interrupt that,
but based on everything we've seen so far it looks like he's first. Anyway, it's
It's just a thought.
Y'all have a good day.