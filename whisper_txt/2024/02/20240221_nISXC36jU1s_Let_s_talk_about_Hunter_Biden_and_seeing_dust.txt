Well, howdy there internet people, let's bow again.
So today we are going to talk a little bit about Hunter Biden and the
developments there, and how some of the things that have been said about
him are going up in dust, um, and we're going to run through something
that was very amusing that occurred.
And then we're going to talk about something even funnier, which
is the reaction of certain commentators to the news.
OK, so if you missed the initial news,
Hunter Biden's attorneys have called into question
some of the government's evidence,
namely a photo that the government was saying
showed a powdery substance.
They said it was coke.
And then Biden's attorneys, Hunter's attorneys,
they pointed out that A, it wasn't a photo
that Hunter had taken, it was a photo
of a photo that was sent to him.
And more importantly, that that wasn't coke,
It was sawdust.
That's amusing.
I mean, making that mistake on evidence is funny.
It is worth noting that this is not
like a death blow to the case against Hunter.
But it is an amusing situation.
And it definitely could be used to show
that maybe the prosecution is going about this a little haphazardly.
But what's even funnier are certain commentators on the right who are just so angry at Hunter
that they are out there trying to suggest that he's lying about it being sawdust.
Or they're just asking questions about it.
And we're not talking about small commentators either.
We're talking about some that are on Fox.
And one that I found very entertaining was when they flashed the photo up on screen.
And they're like, look at it lined up in three little perfect lines like that.
Does that look like sawdust to you?
You decide.
Well, I mean, yeah, Sean, it does.
exactly what sawdust looks like. Incidentally, Mr. Man of the People, it's
still on a table saw. So what we get to find out here is whether or not the
audience that likes to always portray themselves as like the blue-collar
workers really are or I guess the other option would be if they said that no
they thought that it was coke that they were just you know intentionally lying
or deluding themselves but it's a it's definitely telling that commentators are
choosing to cast doubt on this explanation, especially considering what
they like to claim their audience is, who they claim their audience is. And for
those people who are people who at some point in time have been in a woodworking
shop, just remember they questioned whether or not that was sawdust. They
asked you, does that really look like sawdust? You might want to pay attention
when they use that tactic, that technique at other times because if you've been in
a woodworking shop, you've been around a table saw, you know that's sawdust. What
are some other things that they just ask questions about? Anyway, it's just a
thought. Y'all have a good day.