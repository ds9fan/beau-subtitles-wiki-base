Well howdy there internet people, it's Beau again. So today we're going to talk about the
Postal Service and why it just can't remain solvent, why it always needs money, why it's
always going to Congress with its handout. I don't know of any other business that goes to Congress
with their handout as often as the Postal Service does, with the exception of those companies that
constantly give campaign contributions to Congress people. I think the main reason that this business,
why the Postal Service keeps going to Congress asking for money, is because it's not a business,
it's a government service, one that is so essential it's explicitly authorized in the
Constitution, Article 1, Section 8. It's Congress's job to fund the Postal Service.
The Postal Service isn't asking for a bailout, it's asking for funding, it's asking Congress
to do its job. Now one of the biggest roadblocks to this is one, the idea that the Postal Service
is a business. Two, the, I don't know, hundred thousand dollars or so, according to Open Secrets,
that Mitch McConnell got from people affiliated with UPS and FedEx, that might have something to
do with it as well. Generally speaking, government services are not expected to turn a profit.
You know what doesn't turn a profit? The VA, Veterans Affairs, doesn't turn a profit.
They're not explicitly authorized in the Constitution either. Let's get rid of them.
No? Don't want to do that. That would be politically untenable.
You want to sacrifice services mainly for rural people, I'm going to suggest to the
Republican Party that's probably a bad idea. If you ever look at one of the maps where
your votes come from, they're generally rural people.
Aside from that, I would suggest that while fiscal responsibility is important, it's incredibly
important, putting too much emphasis on cost saving when it comes to government services
can lead to some bad things. I would go ahead and bring up in completely unrelated news,
it looks like the settlement for those in Flint is going to be around 600 million dollars,
and to be honest, I don't think that's enough.
Not for the long term damage that's going to be caused. I can't remember exactly why
that happened either. They had something to do with the government prioritizing money
while they were supposed to be prioritizing an essential service.
Aside from that, right now, given everything that's going on,
the post office is incredibly important to maintain the vote, and that's really what
this is about. And that's not, you know, it's not really some kind of bizarre talking point,
not when the president himself has said that that's kind of the reason, you know, when
he said if they don't get those two items, that means you can't have universal mailing
voting because they're not equipped to have it. So while facing an issue that might impact
the vote, his response is to, well, go after the postal service and make it even harder
to process the votes because he wants to suppress the vote. So, you know,
go after the postal service and make it even harder to process the votes because he wants
to suppress the vote because he doesn't care about the Constitution. He doesn't care about
democracy. He doesn't care about the republic, and he never has.
The postal service is not a business. It's a government service. If we're going to have
essential services, then it's not any good. Maybe we should just replace everybody up
there. Would make sense to me. Anyway, it's just a thought. Y'all have a good day.