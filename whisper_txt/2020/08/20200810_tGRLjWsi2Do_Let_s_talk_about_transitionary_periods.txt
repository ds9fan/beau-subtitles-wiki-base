Well howdy there internet people, it's Beau again. So today we're going to talk about
transitionary periods. You know we we know we have them historically speaking.
They occur all the time when we're talking about countries. They uh the US
for the last few years has certainly been in a transitionary period but they
happen in our personal lives as well. You know over the last few years a lot of my
friends have gone through changes and because I'm their friend I want to
celebrate it. I had a friend that got promoted, he was in the army, could not
wait to call him top. Another friend she finished her doctorate, couldn't wait to
call her doctor. Now partially because her last name is Jones and her field is
archaeology and come on. Another friend, couple actually, they've been trying to
have a kid for years, had to go to a specialist, finally took, could not wait
to call her mom. Another friend got married and first thing I said to her
afterward was addressing her by her new name.
Friend got promoted at work, been at that company forever, couldn't wait to call
him director. Another friend in the military went from enlisted to officer,
couldn't wait to call him sir. Another friend, not such a great scenario, she got
divorced, could not wait to call her miss and use her maiden name because in this
case that person she was married to didn't really let her be who she was,
really kind of kept her down. So that new title and that other name gave her a lot
of freedom, really symbolized that for her. It's a good thing. I know some people
change their entire names. If somebody goes through a transitionary period and
you're their friend, you want to celebrate it with them. You want to take
part in that and if it comes with a new title, a new way of addressing them, you
look forward to using it if you're their friend. If somebody goes through
something, goes through some form of transition and you refuse to use the new
way of addressing them, you're probably not their friend. You're probably not even
a good person. Anyway, it's just a thought. Y'all have a good day.