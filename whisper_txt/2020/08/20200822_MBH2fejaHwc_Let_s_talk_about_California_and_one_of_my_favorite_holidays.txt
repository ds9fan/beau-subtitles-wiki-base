Well howdy there internet people, it's Bo again.
So today we're going to talk about California and everything going on there.
And we're going to talk about one of my favorite holidays.
A very unconventional, unorthodox, and certainly unofficial holiday that's been around for like
five years and has maybe celebrated about like 500 people. And we're going to talk about a bizarre
situation that has brought that whole thing full circle. But before we do that,
I want you to picture some of your heroes. People who have truly changed the world.
Get them in your mind. Because years ago, a friend of mine, a guy named Brian,
he was a veteran who had gone over and definitely done his part.
He said something and it caught everybody's attention.
He said that while it may be somebody in his profession, a soldier,
who at times protects freedom or protects your rights,
that more often than not, those freedoms or rights were obtained by a criminal.
Interesting concept, right? But it's true. Obviously it doesn't apply to all criminals.
However, from the heretics of old who brought about modern science,
to those who just would not stop doing what they had always done, and therefore today,
you can have the beverage of your choice, to those who might have bent the rules
to make sure that you have the right to vote, to those who stood against some of the greatest
injustices the world has ever known.
They might be heroes today, but then they were criminals.
Therefore, August 22nd is Think a Criminal Day.
What does this have to do with California, right?
California is currently battling more than two dozen major wildfires.
And 300 smaller ones. Under normal circumstances, they have a resource that they can tap into.
Inmates. Inmates getting paid two to five dollars a day and a dollar an hour while they're actually
out there working against the fire. However, because you can't just go out and do nothing,
however, because you can't really socially distance inside, half of those teams, they're down.
They're not out there working. And that put the state of California in a pretty tight spot.
So much so, that when the inmate heroes were unavailable,
the governor called out soldiers, the National Guard, to pick up the slack.
It all kind of came full circle.
So, think back to those heroes that you pictured in the beginning.
How many of them had mug shots?
Anyway, it's just a thought. Y'all have a good day.