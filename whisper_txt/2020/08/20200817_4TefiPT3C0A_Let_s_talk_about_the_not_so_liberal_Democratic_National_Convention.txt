Well howdy there internet people, it's Beau again. So today we're going to talk about the
Democratic National Convention. We're going to do this because we have a whole bunch of questions.
They're not even questions, it's statements. This doesn't seem very liberal. It's not. It's not.
But we talked about this. It's not supposed to be. The strategy is to show that a Biden-Harris
administration is going to be center-right. That's what they're trying to show. They know
that they have the Democratic Party vote. They're counting on that. This convention is to reach out
to moderate Republicans, to those who are less than approving of Trump's performance.
That's the goal. This is why they snubbed the squad in favor of literal Republicans.
If you're looking at this and you're on the left or you're a progressive liberal and you're saying,
I can't believe they're doing this and you're just mad, it's probably working. Their strategy
is probably working. Because while you're doing that, there is some moderate Republican going,
man, I don't feel so bad about crossing party lines right now. And you'll compare this,
a show of bipartisan unity, a show of putting America first, all of this stuff,
this giant pageant of blandness, compare that to the Republican convention,
which is probably going to be a whole bunch of Trump loyalists shouting four more years
and we're doing everything great. While most of the country has a very disapproving view
of President Trump's performance, they're going to be doing it.
Disapproving view of President Trump's performance because he's low performing.
Not what I want. It's not what 90% of you want. But the strategy is probably going to be effective.
It does reiterate the point that this is going to be a very center-right administration.
Because if this works for them now, they're going to want what every first-term administration
wants, which is a second term. And they're not going to change it up. This will be a very bland
administration. It will be an administration that is just stopping Trump. That's the goal.
Maybe repairing some of the damage that he's caused. You're not going to get that pendulum
swing back towards liberal progressive policies. Certainly not left policies, not with this crew.
But that's not their goal. Their goal is to deny Trump a re-election, period. That's it.
For that, it's probably going to work. Judging by the amount of animosity that the current lineup
and the current rhetoric is generating from the actual left, it's probably really appealing
to the center-right Republicans, to the moderate Republicans. That's their goal. Whether or not
that strategy translates into votes, we won't find out for a while. But from the social media gauge,
it's probably working. So as you watch this and discuss, just remember it's not for you.
You're not supposed to like it. Your neighbor who at one time had a Trump bumper sticker on his car,
but now it's gone. That's who this is supposed to appeal to. Anyway, it's just a thought. Y'all have a good day.