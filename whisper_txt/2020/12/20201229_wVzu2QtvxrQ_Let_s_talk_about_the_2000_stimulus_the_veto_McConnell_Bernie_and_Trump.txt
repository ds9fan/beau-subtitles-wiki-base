Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about what's
going on up on Capitol Hill, because I
have a whole bunch of questions about it.
And it makes sense, because outside looking in,
it's kind of odd.
You have people who are not really political allies that
appear to be trying to do the same thing.
They look like they're on the same team.
It doesn't make sense unless you view all of it
through the lens of the Trump-McConnell power struggle.
That's why people are making the decisions they are.
OK, so to catch you up, in the House,
the veto override for the NDAA, the defense budget, passed.
A separate measure for creating $2,000 stimulus checks passed.
Now it goes to the Senate.
What is best for McConnell is for the veto override
to go ahead and the stimulus checks to fail,
because then senators broke with Trump.
They overrode his veto, and they denied him his stimulus checks
that he popped up at the last moment.
That's what's best for McConnell.
That brings more Republican senators firmly into his camp,
if he can get that to happen.
So his goal is going to be to get the veto override to go
through quickly, and then hopefully just
forget about that other stuff and not worry about it.
Who is standing in his way?
Bernie Sanders and Rand Paul.
That's two names you don't often hear together.
So they're doing it for very different reasons.
They're not actually on the same team.
Bernie Sanders is going to try to delay the veto override vote
in hopes of getting a vote on the stimulus checks.
Rand Paul wants to delay the veto override vote just
to make it look like it wasn't easy to override the president.
And he kind of actually said that, which was surprising.
I mean, he did follow it up with,
a policy facade, but he did kind of make it clear
that that's really what it was about,
because Paul is very much in Trump's little camp.
So as far as how this is going to play out, we don't know.
There's no way to really guess.
There's a lot of political maneuvering
that can occur still.
But that's where it is as of this moment.
The key thing to remember when you're looking at the decisions
being made right now, other than Sanders and his buddy,
the person that's helping him, they're not really
acting out for policy.
It's about power.
It's about consolidating power under McConnell
or allowing Trump to keep power after he leaves office.
It really is about who gets to be president.
Who gets to be president after he leaves office.
It really is about who gets to be the party
lead for the Republican Party.
That's why it doesn't make sense.
That's why looking at it, it just
seems bizarre, because it's not about the issues at hand
right now.
All of the maneuvering has to do with consolidating power,
getting them into McConnell's camp or Trump's camp.
Doesn't have to do with the defense budget.
Doesn't have to do with the defense budget.
So as we watch it unfold, just keep that in mind.
And all of this is playing out against the backdrop
of Georgia's runoff elections and the vote
that will occur on the 6th for the electoral votes.
So there you go.
Anyway, it's just a thought.
Y'all have a good day.