Well howdy there internet people, it's Beau again.
So we're going to start your weekend off with some good news.
Nice change, right?
Okay, so a little bit of back story on all of this.
In 2012,
President Obama created a program called Deferred Action for Childhood
Arrivals.
DACA, D-A-C-A,
probably seen it in headlines.
What this program does
is it protects people who were brought here without authorization
as children.
These are people who grew up
as Americans.
Many of them, they may not even speak the language of their home country.
These are Americans.
They just don't have papers.
It protected
a couple hundred thousand people, I think.
I'm not actually sure on the exact number there.
But in 2017,
Trump tried to end it.
In June,
the Supreme Court ruled that the way he tried to end it was arbitrary and
violated federal law.
So DACA was back.
In July,
the acting head of DHS, Chad Wolf,
signed a memo
saying not to accept new applicants to the program while he reconsidered it.
He also cut work authorization down to a year.
So in November,
a federal judge
decided that Chad Wolf
had been appointed to that post unlawfully.
Yesterday,
the same federal judge
ruled that since he was appointed to that post unlawfully,
his memo means nothing.
DHS was ordered to begin accepting new applications and it bumped the work
authorization back up to two years.
This is a win.
This is a win.
Given the fact
that Biden is supposed to take office in a month and a half,
it's pretty unlikely
that the Trump administration is going to be able to place this program back in
jeopardy.
Biden is an open supporter of this program.
He has publicly stated his intentions to revitalize it.
Given his
statements on asylum seekers,
it's not unreasonable
to think that he may seek to find a pathway to citizenship
for the people protected by this program because as it stands,
they can stay here,
but they're not citizens.
And there's no real way for them to go from where they are
to becoming a citizen.
They're stuck in limbo.
You wouldn't be unreasonable
if you held out hope
that he was going to try to find a pathway to citizenship for them,
which would be a great thing because they have been stuck in limbo a long, long time.
At the end of the day, the Trump administration may try
some appeals or something to try to run out the clock on this,
but it seems pretty unlikely
that they're actually going to be able to end it.
And with Biden's stances on this, it does seem likely that we may actually see some
forward movement on this
rather than just trying to hang on to it.
So there you go.
Start off your weekend with some good news.
Anyway, it's just a thought.
Y'all have a good day.