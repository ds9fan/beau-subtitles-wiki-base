Well howdy there internet people, it's Beau again.
So today we're going to talk about President Trump's declaration, his statement, that he
is going to veto the defense spending bill because he's mad at Twitter.
It's really what it boils down to.
He is demanding that something be inserted into the defense spending bill that repels
Section 230 of the Communications Decency Act.
Section 230 basically protects companies like Twitter from lawsuit because they use user
generated content.
He's mad because Twitter fact checked him.
It's really what it boils down to.
And because of this he is throwing a temper tantrum and is willing to veto a defense spending
bill.
I'm not sure if he still plans on vetoing it over renaming bases that are named after
Confederate generals.
I don't know if that's on the table or not.
He made this little declaration last night and politicians don't know what to do.
There's a lot of talk about it.
Oh no, call his bluff.
That's what you do.
Call his bluff.
Or let him go out with a bang.
Let Mr. I support the military veto a defense spending bill that I'm pretty sure includes
a pay raise for troops right before Christmas.
Yeah let him do it.
I'm sure that that's not going to influence the way anybody thinks in Georgia, particularly
around Columbus where Fort Benning is.
Let him do it.
Make him do it.
Even if he does do it, what happens next?
It's the NDAA.
This bill has been passed every year for like the last 58, 59 years with massive bipartisan
support.
It's veto proof.
His veto will get overridden.
That would be the perfect end to the Trump administration.
Call his bluff.
Stop playing his games.
This is the Republican Party's chance to distance themselves from President Trump.
This is their opportunity.
And as far as Democrats go, it would be a massive, massive mistake to not send this
up and have him veto it.
Let him do it.
Call his bluff.
I don't think that he will.
I don't think he'll veto it.
I don't think that he's going to want one of his last acts as president to be something
that is mocked forever.
The bill's going to get through.
It's the NDAA.
It always gets through.
There's no reason to cave to President-reject Trump.
He's on his way out.
He's done.
Let him upset the people of Georgia.
Anyway, it's just a thought.
Y'all have a good day.