Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump, the Republican Party, and options.
Because Trump has kind of indicated that he's going to run again in 2024.
And he's going to start campaigning now.
See a lot of the Republican Party, they've just been waiting him out, knowing he was
going to go away, thinking he was going to go away.
But see if Trump decides to start campaigning now, well he leads the party for the next
four years.
Congress people and Senators in the Republican Party will be beholden to him, even though
he's not in office.
See this whole time he was Frankenstein's monster.
That's how they looked at him.
They breathed life into him.
He got too powerful.
They needed his base for re-election.
It was politically untenable for them to go against him because they knew he would turn
on them over Twitter.
See now we get to find out whether the GOP has knowledge or wisdom or either.
Knowledge is knowing that Frankenstein wasn't the monster, he was the doctor.
Wisdom is knowing that Dr. Frankenstein was the monster.
See they created him.
They enabled him.
They breathed life into him.
They kept him moving.
They let all of this happen all because they needed that base.
They needed those votes.
Didn't want to upset him.
Even now with Barr, a Trump loyalist, saying that there was nothing wrong with the election,
nothing that would have mattered, they still haven't found their courage to speak out,
cut ties with him, even though some of their own party members are getting threats from
his base.
They can't cut him off.
They had the chance.
They had the chance.
They voted against it because it wouldn't help them politically.
Since their legacy, integrity, democracy itself isn't enough of a motivating factor, let's
talk about some political realities for a second.
Give you the real options.
Because just like you didn't really think Trump through, you're probably not thinking
it through now.
If you cut ties with him now, yeah, the Republican Party is going to pay for it in two years.
That base, oh, they're going to be mad.
But by four years from now, they'll forget about it.
They will.
Something else will happen.
It'll be old news.
But if they don't cut ties, if they let him stay in the public eye by continuing to entertain
him, humor him, well, you know what's going to happen, right?
If he stays in the public eye and people are still interested in him, all those people
that retired, those public officials who left public life, you know what they're going to
do two years from now and then four years from now?
They're going to release their books.
And they're going to do it right around election time because it's good for sales.
Everybody's out for themselves.
You know how it works.
Then all those conversations become public.
Ones that happened behind closed doors, ones where you talked about how incompetent he
was and then went out there and sang his praises to the camera.
The ones where you talked about his base, his supporters, and how gullible they were.
All that's going to come out.
People do not like being tricked.
Do not like being conned.
You will pay for it two years from now, four years from now, six years from now, as long
as he is still in the public eye and people can still make money writing books.
He will destroy the Republican Party and a whole lot of politicians along with him because
you wouldn't stand up to your own creation.
You'll let it get too powerful.
You have an opportunity now, just like you did before.
Depends on whether or not you're going to take it.
Your choice.
But if it makes you feel any better, most of the country looks at you the same way you
look at him.
We're Dr. Frankenstein too.
We let our own creation get too much power.
Run amok.
Destroy everything.
Anyway it's just a thought.
Y'all have a good day.