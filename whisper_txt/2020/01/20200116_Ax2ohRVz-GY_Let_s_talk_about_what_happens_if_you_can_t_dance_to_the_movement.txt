Well, howdy there, internet people, it's Beau again.
And tonight we are going to talk about none of the heavy news.
It's a heavy news day.
I specifically chose a heavy news day to talk about this,
because it's a question I get asked a lot.
How do you keep going?
How do you not get wore down?
You know, when you immerse yourself in the news all the time,
You get to see the worst that humanity has to offer all the time.
And it's in pursuit of a greater good, at least it should be, that's the idea.
What is that greater good?
A better world, right?
You want to build a better world.
That future world is going to be a reflection of us.
If we are worn down and bitter and denying the slightest joys, that's what it's going
to be.
You have to find happiness, you have to find joy.
It can be a lot of different things.
It can be listening to raindrops on a tin roof, that's that noise by the way.
It could be music, it's one of mine.
I tend to listen to music that reflects the mood I want to be in rather than the mood
I'm in.
helps could be anything anything you find joy you find happiness in a lot of
times if you surround yourself with other political people and you're having
fun somebody will find a way to remind you that there are horrible things going
on and you shouldn't be shouldn't be having fun for those people I'm gonna
to read a passage by Emma Goldman. If you're not familiar with her, I strongly
suggest reading some of her work. I did not believe that a cause which stood for
a beautiful ideal for release and freedom from convention and prejudice
should demand the denial of life and joy. I insisted that our cause could not
expect me to become a nun and that the movement would not be turned into a
cloister if it meant that I did not want it I want freedom the right to
self-expression everybody's right to beautiful radiant things this is kind of
where that idea if I can't dance to it it's not my revolution it's where it
comes from. It's where it comes from. Whatever attitude we have as we shape the future is
going to be the attitude of the future. We have to remember that. So when you see somebody
that is politically active and they are doing their part and more often than not the people
They get kind of told, well, you know, there are horrible things happening and you're doing
whatever.
You're going to see a movie.
It doesn't matter.
There are more important things you could be doing.
There's always something else you could be doing.
The world's pretty messed up.
But you have to take care of yourself.
Otherwise you can't stay in the fight.
It's really important.
for your mindset and if you experience joy and happiness every once in a while
go out of your way to do that it reminds you what you're fighting for it reminds
you that this is what you want for everybody and it keeps you moving you
can't live in a constant state of angst you'll burn out you'll burn out you'll
become completely ineffective. Something else that Emma Goldman said, she said the greatest
fallacy is that the aims and purposes were one thing and the methods and tactics were
something else. I agree with that. It's the same principle. It's the same
principle. The methods and tactics you use to achieve your aims are going to be
reflected in those aims. How something comes into being is an essential part of
its nature. If something comes into being full of anger, it's going to be angry.
Now as far as the heavy news today, or what, you're probably watching this tomorrow, there
will be a part two to the Maddow interview. We'll talk about it then. As far as Russia,
Don't panic, it's not that big of a deal.
It's just restructuring.
It's normal, he's done it before.
Anyway, today, prior to the interview,
have some fun, do something that makes you happy,
because I got a feeling we're in for a long road.
You're going to need your batteries charged.
Anyway, it's just a thought, y'all have a good night.
you happy because I got a feeling we're in for a long road. You're gonna need
your battery is charged anyway it's just a thought y'all have a good night