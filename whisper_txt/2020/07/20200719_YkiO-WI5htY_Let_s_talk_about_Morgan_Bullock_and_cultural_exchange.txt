Well howdy there internet people, it's Beau again.
So today we're going to talk about a young woman named Morgan Bullock
and we're going to talk about cultural appreciation,
appropriation,
and exchange. We're going to talk about what the differences are. We're going to do this because
she gave us the perfect window
to kind of look at this
subject through
unintentionally.
If you don't know,
Morgan Bullock is an Irish dancer, traditional Irish dance, river dance.
She has studied it half her life.
She recently caused a stir
by performing traditional Irish steps
to modern music
and putting it on TikTok
while black.
That's really what this boils down to.
Somehow this was a...
this upset people. So it didn't take long for people to just crawl out of the woodwork
and begin to accuse her
of cultural appropriation.
But was it?
Was it? That doesn't just mean that
you're not a part of this culture and you
did something
with it. That's not what that means and I would like to take this moment to point
out
that given Irish-Americans
practices when they first arrived in this country,
she very well could be Irish. Skin tone is not really a good measure of that.
Um...
So, cultural appropriation is where you take something from another culture with
no understanding, no respect for it, you tokenize it,
and maybe use it for your own ends.
That's what cultural appropriation is.
That's not what she did.
She spent half her life
studying Irish dance.
I would have to assume
that at some point during this she's heard Irish music.
If you don't know, Irish folk are storytellers. It's like our thing.
Um...
The music reflects that.
If you listen to enough Irish music you will learn the culture and history of
Ireland dating back to
forever.
It is what it is.
Um...
She did this for ten years.
Ten years she studied this and yes,
the dancing that she does is primarily associated
with music that is mostly instrumental.
However, I find it hard to believe that that's all she listened to. That's all
she heard
after
ten years, half her life,
and going to Ireland,
which I would imagine most people complaining about this haven't.
Um...
So that's cultural appreciation.
She understands it. She respects it. She participates in it.
Nothing wrong with that.
But see, then she took that next step.
She took something from that culture
and she transformed it
and made it her own
in a non-mocking, non-offensive way while still respecting it and
understanding the culture.
That's cultural exchange,
otherwise known as
the story of humanity
and how civilization progresses. This is why we have Arabic numerals in the
United States and somebody right now is going to say, well, they're not really Arabic. I know
they got them through cultural exchange.
This is how humanity moves forward.
The world would be a whole lot better off if we had more cultural exchange.
So why were people calling it
cultural appropriation?
Because she's black.
It wasn't actually Irish folk complaining about it, by the way. I know a whole lot of Irish folk.
Nobody had an issue with it.
The Irish Embassy
even tweeted out support for it.
It was white folk in the United States looking to
play the victim and whine.
There's no issue here. There's no scandal here.
The world would be better off
if there
were more people like
Morgan Bullock
who took the time to understand another culture.
But I do understand the concern
because the last time black folk and Irish folk got together and started
dancing, it spawned a whole new form of dance.
That's where tap dancing came from.
The world
can continue to move forward.
All it takes is a little bit of understanding.
Anyway, it's just a thought. Y'all have a good day.