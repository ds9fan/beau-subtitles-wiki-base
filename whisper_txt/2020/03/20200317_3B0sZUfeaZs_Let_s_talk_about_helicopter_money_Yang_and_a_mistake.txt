Well howdy there internet people, it's Beau again.
So today we're going to talk about helicopter money.
The administration is considering mulling over the idea of basically just cutting everybody
a check.
One grand, two grand, three grand, the number hasn't been decided.
Give everybody a couple thousand dollars.
The Republicans are interested in doing what seems a whole lot like Andrew Yang's plan
on a short term basis to stimulate the economy.
That's their reason for doing it.
Democrats seem, many seem to be opposing it.
Some quietly, some a little more openly.
Their reason for doing so is pretty cynical.
I think their motivations for opposing it really have to do with the belief that if
the administration cuts a check to everybody in the country, well he's going to get a bump
in the polls.
And that may be true.
That may be true.
I would like to think that the average American's memory is a little better than that and they
will remember the lackluster response from the administration when all of this started.
At the same time, from the perspective of the average worker, from the perspective of
controlling what's happening right now, why do most people go out at the moment to go
to work?
Because they need the money.
Again, my concern is not the economy.
My concern is people.
If people can stay home and not lose their home, they're probably more likely to do so.
This seems like a pretty good way to make that happen.
I think the Democratic Party is making a mistake by opposing this.
Money in the hands of the average person will not only help stimulate the economy, which
I literally do not care about.
That is not high on my priority list.
But it will enable people to stay home.
It will enable people to stay home.
And that's what we need to be focused on.
We need to be focused on flattening the curve.
We need to be focused on controlling what is controllable.
And this is controllable.
The federal government can do this.
It's not without precedent.
It's happened before.
This is, I mean, the reality is it's dangerous out there.
Take this.
Take that money.
I'm not a fan of UBI in general terms because I believe it creates dependence on the government.
From a philosophical standpoint, I think that's bad.
However, if we're going to have an overreaching government that is capable of doing anything
and has this power, this seems like a pretty good moment to use it.
I would rather that money go into the hands of the average working person than into Wall
Street.
They're going to have to stimulate the economy somehow.
Why not keep people in their homes at the same time?
It seems like a good idea.
I am not as concerned about the outcome of the election.
I understand the Democratic Party.
That's what they do.
That's what they're worried about.
I think they're making a critical error.
I don't think it's necessary for the average American to suffer to understand that Trump's
not a great president.
If you cannot defeat Trump without making the average American lose their home or have
to choose between losing their home and paying their bills or spreading what's going around
right now, you probably need a different campaign strategy.
We need to focus on controlling what's happening, controlling the elements that we can.
Keeping people in their homes is the best way to do that.
Anyway, it's just a thought.
Y'all have a good night.