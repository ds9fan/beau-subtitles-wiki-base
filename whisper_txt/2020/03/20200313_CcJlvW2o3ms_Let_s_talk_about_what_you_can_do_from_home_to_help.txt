Well howdy there internet people, it's Beau again.
So today we're going to talk about what you can do.
Specifically, what you can do from home.
I got a message and it basically said, you know, I'm not a scientist, I'm at risk, I
can't go anywhere and volunteer, I don't know what I can do to help, I feel like I should
be helping and frankly I'm staying at home and I'm going out of my mind.
It's not exactly what it said, but it was roughly that sentiment and that was the way
I read it.
I'm sure there are a lot of people that feel like that.
As I'm reading it, I had no idea how to respond, but somebody tweeted an article with the hashtag
Rule 303 literally 30 seconds later.
So if you are in this situation, you can be folding at home.
The word folding, the symbol at home.
It is a project ran by Stanford and what it does is it basically takes over, borrows your
computing power.
Anything you're not using, it's using for research.
There's a network of these computers, they're distributed and all of the processing power
is kind of added together.
And the research is folding proteins, I guess.
I don't really know how this works exactly.
And then the information, the research is shared with all the labs who are trying to
come up with ways to counteract it.
So this could actually be critical, life-saving information.
And you can do it from the comfort of your own home.
It looks pretty easy to set up.
I haven't done it yet, I'll be honest, but I probably will.
There will always be some way for everybody to contribute.
Everybody can do something in any situation.
You just have to figure out what it is and how it is.
And there you go.
So there it is.
I'm going to go ahead and say that over the next couple of weeks as we're going through
this, I would imagine there's probably going to be a lot, well not a lot, but a few videos
like this that are just short, getting information out that I think should be out.
I know that's not exactly what people signed up for on this channel, but I think everybody
will forgive me for interrupting this broadcast.
You know, the power of the internet is disseminating information.
So I would feel bad not using it for that purpose.
So folding at home.
You should be able to find it really easy through whatever your favorite search engine
is.
Anyway, it's just a thought.
Y'all have a good night.