Well howdy there internet people, it's Beau again.
So today we're going to talk a little about qualified immunity and how people are responding
to the idea of taking it away.
If you don't know, qualified immunity is basically a shield.
It's what limits the liability of individual officers so their actions are borne more by
the taxpayer, by us, than them as individuals.
A concept that is gaining momentum right now is to remove this, make them carry insurance
or something like that on their own.
Now on social media right now you can see a bunch of officers responding and giving
their thoughts on this.
I'm here to defend their thoughts because I think they're right.
I think they're right.
Basically what they're saying is that if that happens and qualified immunity does get stripped
away and they become personally liable for their actions, that they're only going to
show up if it's like a real emergency or there's a victim.
Yes please do that, that's actually what we want.
Now they're saying this out of spite, to them it's an edgy comment, but the reality is they're
laying down a pretty solid foundation for real police reform.
They're doing it by accident, but yes, that's a great idea.
Think about what this threat actually says.
They're worried that they may get sued because they used force in a situation in which there
was no victim.
There was nobody being harmed until they arrived.
Yeah, you should stop that.
That's a great idea.
If officers actually stuck to this, it would limit a lot of situations from happening.
It really would.
I think this is a fantastic idea.
Yes, only respond if it's an actual emergency, if there's a victim, if somebody's at risk.
Otherwise, sit in your car, eat your donuts, flip through Ranger Joe's catalogs or whatever
it is you do when you're waiting.
The American people will be just fine with that.
They're really okay with it.
We don't want to be over-policed.
Yeah, respond if there's an actual emergency, if people's lives are at risk, if there's
a victim.
Other than that, chill.
It's going to make your life a whole lot easier.
It really will.
You're going to have less work.
The people are going to feel more safe.
Everybody wins.
Can we please just give freedom a chance?
Anyway, it's just a thought.
Have a good night.