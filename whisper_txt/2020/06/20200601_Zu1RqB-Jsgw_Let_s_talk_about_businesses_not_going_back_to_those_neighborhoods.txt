Well howdy there internet people, it's Beau again.
So today we're going to talk about a slogan, a statement, a talking point, I don't know
what to call it.
One of those things that gets said over and over again when people really don't know what
to say.
And like many other talking points, when it gets used over and over again, people forget
the subtext.
They forget the quiet part, the part that's not said.
And I keep hearing this statement over and over and over again and it doesn't seem like
anybody is acknowledging the part that comes before.
You know they've got this great point, but they're not acknowledging what has to happen
before in order for this to be true.
And I think it's really important that we take a look at that, because it's the most
important part of the statement.
Tell me if you've heard this.
Well these businesses, they're not going to go back to those neighborhoods because they
know it's going to happen again.
They know it's going to happen again.
Which part, which part are we talking about here?
Are we talking about the fires covered by insurance?
Is that what we're talking about?
They know that's going to happen again?
And it might, yeah.
But what you're saying is, that is such a certainty that they're making economic decisions
based on it.
Okay.
What about the stuff that comes before?
In order for the fires to happen again, all that other stuff has to happen again.
So when you make this argument, be very aware that what you're really saying deep down is
that hey, a pile of unarmed people, that's just the cost of doing business in those neighborhoods.
And let's be real clear, what are those neighborhoods?
They full of people that look like me?
If it was people that look like me, would you maybe have a different attitude towards
it?
That is a horrible argument.
You are saying with certainty that it's going to happen again.
So much so that you don't blame businesses for making financial decisions based on that
idea.
And that the concern is the money.
Not the people, but the money.
We might really want to think about that.
Maybe that's something we should be demanding change.
We don't have to accept this.
It doesn't have to be this way.
And let's be real honest.
These big companies like that, what do they do in these neighborhoods?
Pay people just above poverty of wages and extract all the wealth out of them they can.
Maybe it's better if they don't go back.
Maybe it's better if all of those people who are so concerned about the shopping choices
of those people in those neighborhoods, maybe a fund could be put together.
Help some investments.
Help the people in those neighborhoods get some means.
Because I bet if they had some money, well, maybe it wouldn't be such a certainty that
bad things are going to happen to them and nobody's going to be held accountable so often
that it comes to the breaking point again.
Maybe if they had some money, maybe they could get some representation.
Maybe it wouldn't be so likely.
It's going to happen again.
It's a certainty.
It is a certainty.
When you make that statement, understand you're completely acknowledging the problem.
You can no longer pretend that it doesn't have to do with race because it's those neighborhoods,
even though this stuff happened all over town, it's those neighborhoods.
Those are the ones they're not going to go back to because they don't want to be close
to where it happens.
We should probably think about this a lot.
It really might be time for the angel investors of the world to help out the people in those
neighborhoods.
Put them in a situation where they have the economic means to demand change.
Because it's very clear that money's what matters, not a pile of people.
Money.
That's the talking point.
Not that it's so certain that it's going to happen again, and that it's a tragedy that
it's going to happen again, and that it's something we need to fix because it's going
to happen again, but because there's going to be a loss of money.
Anyway, it's just a thought. Y'all have a good night.