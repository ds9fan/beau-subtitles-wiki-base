Well howdy there internet people, it's Beau again.
So today we're going to talk about Juneteenth.
This is going to be part of an ongoing series discussing historical events that either aren't
in history books at all or they don't get the exposure they should and we all know why.
Juneteenth is in the news today because the President of the United States has chosen
this date to hold a rally in Tulsa, which is either incredibly tone deaf or he completely
understands the message he's sending and it's just for a few people.
Either way, that's not what we're here to talk about.
We are here to talk about what Juneteenth is, how it happened, and we're going to dispel
a couple of myths that kind of grew out of it or are still being circulated as truth.
Maybe they shouldn't be.
Okay, so January 1st, 1863, the Emancipation Proclamation, right?
Lincoln did it, freed the slaves.
So everywhere the slaves are free.
No, of course not.
News took time to travel, it's not like he could send a mass text or email or anything
like that.
Even if he did in Galveston, the city we're going to be talking about, they wouldn't have
time to read it because they were busy fighting the Battle of Galveston.
It wasn't until June 19th, 1865, two and a half years later, that federal troops showed
up and enforced it.
Two and a half years.
Two and a half years.
That's a long time.
Now there's a bunch of reasons, a bunch of theories as to why it took so long for news
to reach Galveston.
One of them is that, well, they sent a messenger, but they were shot.
Another is that the Union really didn't want that because they needed one more good harvest.
Whole bunch of theories involving it.
We're not going to go through all of them because they're all based on a false premise
that we're going to get to in a minute.
Once the federal troops began enforcement, it altered the owner-slave relationship and
turned it into employer-employee relationship.
And well, this gives birth to another myth.
Freedom is scary.
Freedom is scary, especially if you've never experienced it before.
So some of the former slaves chose to stay where they were and continue working where
they were.
This gave rise to the idea that, well, you know, my ancestors, well, they were good to
their slaves.
So much so that even after slavery was over, they stayed.
No, that's not what happened.
They didn't have means.
They didn't know where to go.
They chose the devil they knew.
Nobody was good to their slaves.
That wasn't the thing.
Yeah, sure, maybe they were less evil than their neighbor, but being less evil than the
institution of slavery inside the United States, that's not saying much.
That is not something to brag about or try to whitewash your ancestors' actions.
Nobody was good to their slaves.
If they were, they would have freed them.
And that brings us to the next thing, those theories, why it took so long.
The theories don't matter.
None of them do.
None of them.
Because the reality is they did know.
News did reach there.
Galveston was a pretty big port.
It rivaled New Orleans.
It had a cathedral, a Roman Catholic cathedral, functionaries traveling in and out.
It had like a dozen newspapers.
You mean to tell me in two and a half years, nobody traveling through there mentioned the
end of slavery?
That seems like it would probably come up.
Seems like pretty big news.
All of these theories are just serving to lessen the culpability of those who tried
to ignore it, who ignored that proclamation, who chose to stay with the status quo even
though they knew things were changing.
They could see it on the horizon.
Yeah, maybe they weren't being forced to do it yet, but they knew it was coming.
They knew it had happened.
But they chose to maintain the status quo.
Juneteenth got celebrated.
And it continued to be celebrated.
It kind of fell out of favor during the Great Depression.
Then during the Civil Rights Movement, it saw a resurgence.
And it's still, it's becoming more and more widely celebrated today.
It's become a symbol of those first scary steps into freedom.
I actually think in Texas, it's a holiday.
But one of the most important lessons from Juneteenth, and one of the biggest things
to be learned, it's not for the people who celebrate it.
It's for the people who are choosing to ignore what's on the horizon, choosing to
ignore the signs that things need to change, choosing to stay silent and maintain the status
quo.
Right now, white folk can learn a whole lot more from the events surrounding Juneteenth
than black people can.
Anyway it's just a thought.
Y'all have a good day.