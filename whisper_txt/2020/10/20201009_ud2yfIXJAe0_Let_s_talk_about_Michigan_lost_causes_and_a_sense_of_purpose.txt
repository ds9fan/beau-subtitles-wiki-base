Well howdy there internet people, it's Beau again.
So today we are going to talk about wanting to belong, a sense of purpose, insurmountable
odds, lost causes, and that thing up in Michigan.
We're going to do this because last night I was doing what a whole lot of the country
was doing.
I was hanging out on Twitter, laughing, joking, just doom scrolling, looking at the mug shots.
Dozens of people in that thread, hundreds, everybody having a good time.
And then somebody came along and just stomped on the vibe.
They did that by asking a question, simple question.
How many of these guys you think are in this situation now wound up there simply because
they wanted to belong to something?
They wanted a sense of purpose.
Yeah, yeah, that's probably true.
That is probably true for some, not all, but for some of them.
Doesn't matter, it's too late for them, they've made their bed, they're going to have to deal
with the consequences and those consequences appear to be pretty severe.
However, there may be other people on that road with that desire for a sense of purpose
and it is that sense of purpose that turned into that plan because here in the US we have
romanticized that hopeless battle, going up against those insurmountable odds.
Believe me, I get it, I get it.
But somewhere that got equated with pulling a trigger and the imagery became, you know,
being pinned down and outnumbered and all that.
I do, I do understand that desire, that rush you get from facing those insurmountable odds
and maybe coming out on the other side of it and setting things right.
I get it, I do.
Thing is, if you're on that road long enough, you will, if you make it, you'll figure out
that destroying, fighting, dying, it's easy, it's easy.
Building, living is harder, much more fulfilling too.
You're also less likely to get co-opted and that's what happened here.
Bunch of working class, working poor got co-opted, got energized by rhetoric from the establishment,
from those in power.
They wound up doing their betters bidding without realizing it, doing something that
benefits them politically or they thought it would.
And those at the top, those they were serving, I don't care about them, their biggest concern
is the fact that some of their friends that make seven, eight, nine, ten figures a year
may have to pay just a little bit more in taxes.
Somebody exploited that desire for a sense of purpose because our society doesn't always
give people that.
You know, the thing is, if you want a sense of purpose, you want to fight those hopeless
battles, you want insurmountable odds, start trying to look out for those on the bottom.
Start trying to build something for them.
Build, you don't have to destroy anything.
Build something for them.
Work to build a just society.
Believe me, when you start trying to build something for those on the bottom, you're
facing those insurmountable odds.
You wake up every day outnumbered, outgunned, outmanned, outfinanced, and behind enemy lines.
If that is the rush you seek, it doesn't get better than that.
It is never ending.
Aside from that, you get to build.
You get to do something that leaves a positive mark rather than becoming the butt of a joke,
an internet meme.
If you do it right, if you succeed, if you are successful, and you build that better
world, you don't have to destroy anything, those other systems, those other establishments,
they will decay on their own from disuse, and you will have completed your revolution
without firing a shot.
You don't have to hurt anybody.
You just have to build something better.
If you are one of those who is looking for a sense of purpose, that's it, and if you
do it, you're building something for somebody like you.
Because if you want to engage in a hopeless battle, at some point in time, on some level,
you lost hope.
There are people out there who are there now who have no hope, and you can help give them
to them.
You can help bring them there.
Isn't that better?
Anyway, it's just a thought. Y'all have a good day.