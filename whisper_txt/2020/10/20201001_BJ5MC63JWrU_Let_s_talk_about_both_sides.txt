Well howdy there internet people, it's Beau again.
So today we're going to talk about both sides.
Talk about bias.
We're going to do this because I noticed a few comments and one question.
Comments suggesting that I have shown my bias in regards to Trump and Biden.
And the question was when am I going to go after Biden the way I do Trump?
Fair enough.
Fair enough.
But first, what's bias?
Unfair favoritism.
Shown to a person or idea or something like that.
Real simple definition.
But the key phrase there, key part, is unfair.
Double standards.
That's bias.
Being unbiased does not require that if I say something mean about Trump today, I have
to say something mean about Biden tomorrow.
That's not what it means.
What is my job on this channel?
It's in the about section.
I want to apply common sense, common sense to the situations we face in the world and
the country today.
Pretty simple.
If Trump says it's raining and Biden says it isn't, my job isn't to say, hey, this is
what Trump said.
This is what Biden said.
Now my job is to look out the window, apply some common sense.
And when you do that and you apply even base minimum standards fairly, it's really easy
to make an objective determination.
Trump openly aligns himself with the worst people the country has to offer.
He openly aligns with the most divisive, most hateful people in the nation.
Biden doesn't do that objectively.
By that incredibly low standard, Biden is the better man.
When am I going to go after Biden the way I do Trump?
Hopefully January 22nd.
Biden is not my man.
I've said it a dozen times on this channel.
I am not a fan, to be honest.
However, he's objectively less harmful.
I don't have high expectations for a Biden administration.
I don't think he's going to usher in a golden age, bring Camelot back.
It's not what I foresee.
I foresee having to go after him the same way I do Trump pretty quickly because there
is a cause that's very personal to me that he's not great on, to be completely honest.
He's better than Trump, but everybody is, so that doesn't really matter.
I would imagine that by March, there will be a video from me asking why something hasn't
been done yet, why some of Trump's policies down on the border haven't been reversed,
and probably another one a month or two later, because Biden is better, but he's not up to
my standards.
So if I was to apply that fairly, I would have to do that.
But see, Biden is a private citizen right now, and as a private citizen, he has shown
more leadership than Trump if I apply the standard fairly.
Objectively, that makes Biden the better man.
This isn't really hard.
I don't expect much from Biden.
I just want him to stop the decline into being a failed state, and I believe he can do that.
This can actually pretty much all be summed up by something that happened at the convenience
store yesterday.
Guy who knows I'm pretty political asked me what I thought about the debates.
I said I'm no fan of Biden, but I don't believe the country can survive another four years
of Trump.
Pretty low standard right there, isn't it?
But that's the standard that Trump has set.
See people say that a lot.
Candidate X is going to destroy the country.
I've never said that before in my life, and I mean it quite literally when it comes to
Trump.
I do not believe that the semblance of a representative democracy that we have in the United States
will survive another four years of Trump.
That is an objective determination based on the last four years.
It's not bias.
Looking at what he's done, what he's failed to do, what he's attempted to do.
He's an authoritarian in the making, objectively.
That's not bias.
That's applying the same standard.
I think what people want when they say this is neutrality.
I think people are confusing neutrality and bias.
I'm not neutral.
I've never claimed to be neutral.
I don't want to be neutral.
There is a lot of injustice in the world.
I am not taking the side of the oppressor.
Anyway, it's just a thought.
Y'all have a good day.