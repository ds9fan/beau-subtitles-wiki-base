Well, howdy there, Internet people.
It's Beau again.
So we have some amazing news today,
some news that's going to make every American proud.
Everybody's going to be beaming with the red, white, and blue.
Our president, Donald J.
Trump, has been awarded the 2020 Nobel Prize
for his outstanding work at uniting this country
and helping to bring peace and justice and remove the —
no, of course not. That didn't happen.
I mean, well, I mean, he might have won the Nobel Prize,
but the Nobel Peace Prize,
that went to the UN's World Food Program,
an organization established in 1961
with the lofty goal of ending food insecurity and hunger.
This year in particular, they have been tasked pretty heavily.
They have operated in 88 countries
and assisted almost 100 million people.
Pretty cool stuff.
Overall, their model is to move towards sustainability,
but they also have to deal with immediate needs.
This year in particular, it was a little more difficult,
not just because of, you know,
the whole world public health thing that's going on,
but there were also a lot of conflicts,
and conflicts do tend to interrupt supply change,
which increases food insecurity.
Aside from that, in some areas,
food, hunger gets used as a weapon,
and they attempt to alleviate that.
They also try to model themselves
in a way that stops problems before it starts.
They want to create a model
in which the areas are sustainable on their own,
because in a lot of places,
the food insecurity causes the conflict,
or at least contributes to it, and it creates the cycle,
because people are fighting over resources.
If they can create a sustainable way
in which everybody is being fed, it eliminates that,
and in a way, stops wars before they start.
It seems pretty fitting for a peace prize, right?
I know that there are criticisms of this organization.
I have some myself.
However, I would point out it's a UN organization.
Most of the criticisms that are leveled against it
are ideologically based.
Yeah, I mean, they work primarily through governments.
It's the UN. That's kind of what they do, you know?
I don't think anybody can take away from their mission
and how important their mission is
and how effective they are at it.
Yeah, there's always room for improvement,
but 100 million people got to eat.
That's pretty amazing.
Aside from that, I think one of the more important things
that we can take away from this organization
in the United States is looking at them as a template
for a revamping of our foreign policy,
you know, moving more towards being the world's EMT
than the world's policeman, trying to alleviate the problems.
And even better in their case,
if we follow the model closely enough,
maybe we can help solve problems before anybody's calling 911.
That would be ideal.
So that is the winner of this year's Peace Prize.
I know I shouldn't feel this way,
but honestly, I cannot wait to see how the president responds
because we all know he was really hoping for this,
felt very entitled to it.
I'm not sure how he will find a way to insult or disparage
or attack an organization
that feeds 100 million people over Twitter,
but I'm sure he'll surprise us.
I know it'll be hard, but I'm sure he'll figure something out.
Anyway, it's just a thought. Y'all have a good day.