Well howdy there internet people, it's Beau again.
So today we're going to talk about the whole mask thing.
There's a discussion about whether or not it's okay for governments or private entities
to tell you to wear a mask when you go to certain places.
The organizations that are demanding, in some cases, asking others for you to wear a mask,
their logic is that, yeah it was okay before, but now you have something in your body, maybe
you have something in your body, that could prove harmful to other people.
Therefore you have to alter your behavior.
Seems pretty simple.
This isn't a new concept.
A few months ago, you can get up in the morning, grab your keys, get in your car, drive to
the grocery store, walk inside, make your purchase and walk out.
Things have changed a little bit.
You may have something in your body that poses a risk to other people, so now you grab your
keys, get in your car, drive to the grocery store, put your mask on, walk inside, make
your purchase and walk out.
Okay.
For some reason that's a problem.
Like it's new that the idea of having something in your body requires your behavior to change.
A couple of months ago, if I had decided that I was going to wake up in the morning and
down a fifth, then grab my keys and go to the grocery store, I'm willing to bet most
people would say that was wrong.
Why?
You can't prove that I would hurt somebody.
I mean, even if I was over the limit, you just have likelihoods, statistics, stuff like
that.
There's no way you could prove that me, I was going to hurt anybody.
Especially if I won't take the test.
It's not a new concept.
Not wearing a mask is driving while impaired.
If you have the ability, if you have a mask available, if you're in an area that is exposed,
that's pretty much everywhere now.
Yeah you should.
And you shouldn't need an order to tell you that.
That's common sense, it's common decency to want to protect your neighbors.
Now the idea behind this is, you know, I have a constitutional right to not wear a mask.
That's not a thing.
That's just completely made up.
That's not real.
The other thing is that it's the beginning of tyranny.
And forcing us to do this, it's tyranny, because there's a long history of authoritarian, tyrannical
regimes that want their citizens to be unidentifiable.
It doesn't even make sense.
It doesn't even make sense.
That's a whole lot like, you know, the Trump crew coming out and saying, well it's going
to magically disappear, you know, after the election.
But in the meantime, I'm going to go ahead and take this magical cure I've been hawking,
because I'm a snake oil salesman.
Which is it?
Is it overblown and not a threat and we should reopen?
Is it a hoax?
Or should he be taking the pills?
Which is it?
See anytime an ideology requires you, and Trumpism is now an ideology, you're not Republicans
anymore.
Anytime an ideology requires you to embrace two conflicting things and accept them both
as truth, simply because dear leader said so, that's the road to authoritarianism.
That's the road to tyranny.
Because if they can get you to do that, and they have with a lot of people, you can justify
anything.
Yeah this is morally and ethically wrong, but we have to do it.
No, that's not how it works.
You need to take a hard look at the many different ways that Trump induces his base, his followers,
to embrace ideas that are in direct contradiction to each other.
It seems almost intentional, methodical even, that he's trying to condition them to accept
whatever he says.
If you could pose a risk to others, yeah, wear a mask.
You don't need an order to tell you that.
You know it's the right thing to do, just do it.
Exercise some social responsibility.
And as far as the other stuff goes, really think about the logic of the ideas that the
Trump crew is presenting.
There's a reason it doesn't make any sense.
Because what they're saying isn't true.
Anyway, it's just a thought. Have a good night.