Well, howdy there, internet people, it's Beau again.
So tonight, we're going to talk about putting yourself
in that position, and beliefs that may be widely held
that are just plain wrong.
And then we're going to go over some statistics to show
that they're just plain wrong.
And then we're going to go into what those widely held
beliefs create.
So, there was an interview in which a lawyer in a very high profile case right now said
that she had never had that issue because she'd never put herself in that position.
There's a whole lot of things you can say that about and it'd be true.
You could say, I never went into the ocean, therefore I never put myself in the position
to be bitten by a shark.
Fair enough.
That's true.
That's a true statement.
That makes sense.
It doesn't make sense in the case of what we're talking about.
Donna Oratuno, probably butchering the last name, she's one of the lawyers on the Weinstein
case.
She was asked in an interview if she had ever been the victim of an assault.
I have not because I would never put myself in that position.
I've always made choices from college age on
where I never drank too much.
I never went home with someone I didn't know.
I just never put myself in any vulnerable circumstance ever.
I would suggest that this comment put you
in a pretty vulnerable circumstance.
But does any of this ring true?
Let's just take the idea that if you stay away
people that you don't know. Well, that's a good security measure. That will stop you
from becoming a victim. At least help. A year ago, there was a study of almost 1,000 survivors.
23% of women were assaulted by a partner or ex-partner. 24% were assaulted by a family
remember, 44% were assaulted by another known person, 9% were assaulted by someone they
don't know.
So really, you're actually safer hanging out with people you don't know.
That's the idea that if you're drunk and with somebody you don't know, well then you kind
of had it coming.
you know those drunk promiscuous women,
that's the image that's created here.
There are a couple of other findings
that relate to some recent cases that are high profile.
Go over those two, even though they don't directly relate.
32% of these incidents were reported to the police
more than two years later.
20% were reported to the police more than 10 years later.
22% had not reported them at all.
Wow.
So that kind of dispels another myth about why
people wait so long.
That's not rare.
It's incredibly common.
So what does this tell us?
I mean, I think everybody knows that what she said was false.
This is like asking what somebody's wearing when it happens.
Doesn't matter if it's a miniskirt or a diaper, that had nothing to do with it.
The reality is that this is occurring with the backdrop of men all over the
United States saying that, well, I'm scared to be alone in a room with a
woman now because they're just going to make something up. That's the idea. It
creates this impression when this type of rhetoric is spouted off that it's not
really the man's fault because she was drunk and went back to the hotel with
somebody she didn't know maybe. What it also shows is a lot of successful
people's habit of prioritizing their own success, their own luck, under this system.
Over the concept of stopping the victimization and the things that we know are occurring
in this system.
This is a widespread thing.
Sexual assault is common, statistically it's common.
are worried about coronavirus compared to this. What's more likely to happen to
one of your loved ones? But it doesn't get reported. Why? Because it's not actually
about what it seems like it's about. It's about power. So who does it most often?
often, the powerful, who are protected by the media.
We don't want this to seem like an issue.
This idea of blaming the victim, of saying, well, it was her fault because she drank something
or wore something or she was alone with someone. Apply it to anything else. Would
you say that about any other kind of violent crime? Of course not. Of course
not. But happens here all the time. Lawyers and high-profile cases say stuff
like this publicly, because they know it's a widely held belief.
Widely held beliefs are often wrong.
Anyway, it's just a thought, y'all have a good night.