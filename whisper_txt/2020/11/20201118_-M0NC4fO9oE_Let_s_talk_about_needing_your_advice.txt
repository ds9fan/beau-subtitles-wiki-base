Well howdy there internet people, it's Bo again.
So today I kind of need your advice.
I need your advice.
You know, as some of you know, I used to be a consultant.
Kind of give advice on security issues.
Now years ago, I worked for this guy.
Worked for the owner of this company.
And he wanted to bring this person on.
And I do what I always did, you know.
I went and kind of looked into the guy.
Now, his resume was inflated.
But everybody lies during the interview.
They try to present themselves in the best light.
That in and of itself means nothing.
But I don't have a good feeling about this guy,
from the very beginning.
I talked to a couple people who used to work for him.
And they're like, yeah, he's no good.
He is no good.
He just makes stuff up.
He's a con artist.
He's a gun-a-fleece boss.
And I tell my boss this.
But this guy is basically telling him
that he can fix all the problems.
He can fix the company, turn the company around,
and everything's going to be great.
The boss was hearing what he wanted to hear.
It's what was happening.
And it didn't matter what I said.
I still wanted to hire this guy.
At the end of the day, I'm an advisor.
I get paid to give advice.
What they do with it is up to them.
So he brings the guy on as a troubleshooter.
Now, the guy isn't from the same field.
So when the first couple of projects
this guy's involved with turn out to be failures,
the boss is like, well, there's a learning curve.
The guy starts making personnel changes, firing people.
And I go to the boss.
I'm like, you know, some of the people you're losing
are good people who have been around a while.
They kind of know what they're doing.
And the boss is like, you know, they have been around a while.
And we've got issues.
We have problems.
We hired this guy to troubleshoot.
Maybe the trouble is the personnel.
Fine, whatever.
I start digging into the guy a little bit more,
find out some more stuff about him,
even find a family member who's like, no, this guy's no good.
You need to stay away from him.
And find out that some of his claims on his resume
were even more inflated than initially thought.
The thing that really bothered me about him
was that any time something went wrong,
it was always somebody else's fault.
He's the guy in charge.
But when something turned up missing,
when a project went south, whatever,
he would blame somebody underneath him, always,
without fail.
And I go back to the boss.
I'm like, look, the guy is no good.
I'm telling you this.
I've been telling you this.
Now you can see it.
He's blaming people who have done a decent job.
And he's like, well, we thought they did a decent job.
They didn't, really.
Again, I'm just an advisor.
I got nothing to say about this.
And then it comes into the situation
where he may have broke some laws.
And I find out about it.
And I go to the boss.
And he's like, look, I don't really
think that this is what happened.
And you can't really prove this beyond a shadow of a doubt.
Like, no, I can't.
But by the time I can, there's going
to be a whole lot more issues.
And you're not going to have a company left.
Boss doesn't listen to me.
Owner of the company does not listen to me.
He keeps this guy on for years, causing problems.
And then he voted for him again.
If you take this situation with President Trump
and you put it in any other context,
whether it be an employee, which he is,
the president is an employee of the boss, the people.
Put it in the context of somebody
who wants to date your friend.
Put it into any other context.
Anybody who continued to follow him
would look like the boss in that story,
look like somebody who'd been conned,
look like somebody who wasn't accepting reality,
look like somebody who had fallen
into a cult of personality.
At the end of the day, the real boss here, the people,
have decided it is time for a personnel change.
At the end of the day, that's what happened.
Yeah, the boss was conflicted about it,
about half-hearted in this measure.
But at the end of the day, it was decided it is time
to get rid of this troubleshooter
because he couldn't fulfill the things he said
he was going to do, because he couldn't accept responsibility,
because he ran the company into the ground,
spoiled the company's relationships
with other companies.
I'm doing my best to come up with ways
to reach out to those people who still believe
that Trump walks on water.
If you have somebody in your life
that is still under his sway, maybe this
is a video that can help reach them.
Anyway, it's just a thought.
Y'all have a good day.