Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about what's going on in Texas.
The Republican Party there is attempting
to invalidate the votes of more than 100,000 people
who voted in accordance with what they were told
by government employees, by government officials.
One of the counties there set up little drive-thrus.
So you could pull in, get your identity verified,
get your ballot, fill it out in private, and hand it back.
OK?
Now, right before the election, the Republican Party
is taking it before a federal judge
in the attempts of invalidating all of these votes,
making sure that their voice isn't heard.
They're doing this because the Republican Party
is at risk of losing Texas.
Perhaps if the Republican Party has strayed so far
from its ideas that it's at risk of losing Texas,
maybe straying further isn't really the answer.
Because at the end of the day, they
can wave their flag all they want.
But if they seek to deny people representation,
deny people their voice, well, they're not
really honoring that flag.
What was the battle cry?
No taxation without representation.
I'm assuming the GOP is going to make sure that these 100,000
people aren't taxed, right?
Or is it really just about stealing the election?
Is it about interfering with the democratic process
because they're worried they're going to lose Texas?
The Republican Party is worried they're going to lose Texas.
If they weren't worried, they wouldn't be doing this.
This type of behavior, suppression on this scale,
100,000 people, this is why we have election monitors turning
their eyes to the United States.
This country used to have elections that were, well,
they went relatively smooth.
In fact, we were kind of the example held up
to the rest of the world.
But after four years of whatever the Republican Party has turned
into, now election monitors are looking to us for problems.
Maybe that's the issue.
Maybe that's why you had 100,000 people in Texas
ready to cast their vote against the Republican Party.
And that's what it's about.
They've picked an area that is very
likely to turn out for Biden.
And they're trying to invalidate those votes because they
want to undermine the election.
They're trying to steal the election
because they know they can't win unless they cheat.
All of that talk about election security and voter fraud
and all of that, none of it was true.
None of it was true.
They didn't care about that.
Their worry is that the people's voice is going to be heard
because they aren't for the people.
They aren't for the working class.
They are for the establishment.
Donald Trump is not anti-establishment.
He is the establishment.
He's a billionaire who hung out with all,
all of the political establishment.
The Republican Party has lost its way.
It's going to continue to stray until Trumpism is
removed from their platform.
This move should encourage even more people
to switch their vote from Republican to Democrat
because it doesn't matter about policy at this point.
Now you're talking about the basic functions of democracy
that the Republican Party is attempting
to undermine in Texas because they're
afraid they're going to lose one of their strongholds
because they keep undermining the Constitution,
keep undermining the founding principles of this country.
That's their fear.
That's their worry.
And it's a valid one.
And every time they try to take it another step,
there are more people who turn away from the Republican Party.
And they should because at this point,
they are trying to turn this country into a nation that
doesn't have a representative democracy because it
doesn't have representation.
Anyway, it's just a thought.
Y'all have a good day.