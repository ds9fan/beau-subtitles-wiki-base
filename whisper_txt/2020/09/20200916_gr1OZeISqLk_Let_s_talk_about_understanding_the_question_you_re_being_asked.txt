Well howdy there internet people, it's Bo again.
So we're going to be recording here for the next few days.
I'm on the edge of the storm.
Nothing to worry about, but a lot of rain.
Can't record in the shop with a tin roof.
OK, so tonight we're going to talk
about making sure you understand the question you're being asked.
Because for years I didn't.
Certain I have given less than satisfactory answers now.
So for years I have helped people build community networks.
And every so often after holding a workshop or whatever,
actually talking to people, helping them do it,
I have somebody come up to me, normally a younger person,
and ask, well how do you start?
I'm sitting there, it's like I just spent an hour talking
about this, what do you?
And I'm sure I have given less than satisfactory answers,
because I didn't understand the question.
If you are an old hand at building community networks,
pay attention.
That question doesn't mean what we think it means.
They're asking how to get that core group that you start with,
that to us is a given.
Younger people are so technology based
that they don't have that.
It doesn't exist for them.
And they're less likely to interact
with people they don't know.
Figured this out after stumbling upon a video
by Working Stiff USA, new YouTube channel.
And by new, I mean I was the ninth subscriber.
Very new channel.
They go through and really address this issue
when we're talking about younger people not really knowing
how to step out and build their network.
I don't want to remake the video.
I'm going to put it below.
But I want to add a couple of things.
First is, it's literally anybody.
Anybody.
The waitress at your favorite restaurant,
the person working at the gas station,
your friend from school that you haven't talked to in forever,
especially those who are involved
in extracurricular activities.
They tend to be more active.
People at work.
Anywhere.
Literally anybody.
And you don't have to click with them on a personal level
right away.
It's not even remotely important.
All that matters is that commitment to that goal.
And then as your network grows, it's going to split.
When it gets large, it'll divide into two little networks.
Those that you click with and you
resonate with on a personal level, they'll stick with you.
Those that don't, they'll form their own.
That's how it works.
So don't feel like you have to become best friends initially.
You probably won't.
There's going to be a whole bunch of people
that cycle in and out.
And that's OK.
That's OK.
It's expected.
You don't really need to worry about that.
Now the video I'm going to put below,
definitely coming from the left, but goes out of their way
to point out that it isn't ideologically based.
Not initially, anyway.
That it's just about doing good.
And granted, if you do this long enough,
there will be certain ideological things that occur.
It happens.
And that's addressed.
I also want to point out, it's a brand new YouTube channel.
The audio is not great.
Very quiet.
You're going to turn it up all the way.
But at least it's coming out of both speakers.
If you weren't here when this channel first started,
in the beginning, I literally had people leaning left.
So don't focus too much on the production value,
but focus on the content.
Especially if this is a question you have.
If you want to know how to get that first group of people.
This video specifically focuses on getting that first one.
And that makes sense to me.
And then from there, it'll grow.
The content's great.
It's definitely worth checking out.
And as current events have shown,
these community networks are incredibly important.
Yeah, all of the day-to-day problems
that exist in the world can be helped with one of these.
But also the emergencies that are going on right now.
The fires, the hurricanes, whatever.
They become very important in not only dealing with them
when they occur, but helping to recover afterward.
Anyway, it's just a thought. Y'all have a good night.