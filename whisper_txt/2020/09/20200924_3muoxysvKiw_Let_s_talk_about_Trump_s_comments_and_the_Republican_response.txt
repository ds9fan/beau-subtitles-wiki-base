Well howdy there internet people, it's Beau again.
So today we're going to talk about the President's comments again and the Republican response
to them.
And by response I mean complete lack of a response.
Republicans normally do not comment on the President's statements because they're worried
he'll make fun of them on Twitter.
But they commented on this because they understand the severity of what he said.
You do not know the President of the United States was asked very directly if he would
commit to a peaceful transferal of power and he wouldn't.
The President would not commit to a peaceful transferal of power.
And the Republican response has been GOP leader in the House McCarthy saying let me be clear
it'll be peaceful.
And McConnell saying oh don't worry, you know whoever wins the election they'll be inaugurated.
That's cool, fantastic, except you guys don't get to decide that, he does.
Your man has to be held accountable for his words.
And you're not doing that.
Right now you're not doing that.
You're not pushing him to make this commitment.
You're defending him.
You're downplaying it.
You will forgive me if I do not believe you will act when the time comes, if the time
comes.
Because right now you're refusing to act and your biggest worry is an angry tweet.
You're telling me that you're going to act when he hasn't taken violence off the table?
Yeah I don't believe that.
Not at all.
And I get it.
You guys don't believe it.
That's fine.
It's not like you guys have a really long history of underestimating this man.
That's how he wound up in the Oval Office because you didn't take him seriously in the
primaries.
That's how he wound up there.
And now you're all held captive by his Twitter account.
The president needs to commit to this and you need to make him.
That's your job.
Your job in the legislative branch is to act as a hedge against executive tyranny.
That's why you exist and you are failing in that obligation.
The most interesting defense came from the senator from Nebraska who just said, well,
the president says crazy stuff.
I'm going to suggest that that's a reason he shouldn't be president.
A man whose words can start a war.
A man whose words can take the economy.
A man whose lack of words can take out 200,000 Americans.
He dies at four years.
You've never held him accountable for anything.
Why would you expect the American people to believe you would do it when the chips are
down?
You won't.
You're proving it now.
The Republican Party has a moral obligation, a legal obligation, and an ethical obligation
to put pressure on the president to make him commit to this.
The names McCarthy and McConnell may go down in history, not in a favorable light, but
as the people who enabled the run-up.
You need to stop underestimating this man because just as he has held you captive, he
can do it to the country.
It is your obligation, your duty to stop it.
It is just a thought. Have a good day.