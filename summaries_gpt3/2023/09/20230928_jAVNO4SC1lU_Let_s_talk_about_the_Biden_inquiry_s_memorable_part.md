# Bits

Beau says:

- House Republicans initiated an inquiry to determine whether to impeach the current president.
- Their witness stated that the current evidence does not support articles of impeachment.
- Despite this, House Republicans are pursuing further investigation with 12,000 pages of bank records.
- The inquiry into impeachment has taken precedence over preventing a government shutdown.
- In roughly 61 hours, a government shutdown may occur, impacting millions of Americans who could stop receiving paychecks.
- Some individuals may not even be aware of how they will be affected by the government shutdown.
- House Republicans have prioritized the impeachment inquiry over helping working-class Americans.
- Beau criticizes the Republican Party for losing sight of the real issues and focusing on impeachment for social media engagement.
- He points out that preventing financial hardship for Americans should take precedence over impeachment proceedings.
- Beau urges people to pay attention to the fact that millions of Americans may suffer due to the government shutdown while Congress prioritizes impeachment.

# Quotes

- "Playing impeachment is the priority as the clock winds down and millions of working-class Americans, many of whom are their constituents, get hurt."
- "Keeping the lights on, stopping millions of working-class Americans from suffering financial hardship, that's not important."
- "This was the priority."
- "Doing something to prevent this, you're right, had everything gone according to the way things should work, nobody would have noticed."
- "They will notice when their checks stop coming, and there will be anger generated."

# Oneliner

House Republicans prioritize impeachment over preventing a government shutdown that could harm millions of working-class Americans financially.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact your representatives to urge them to prioritize preventing a government shutdown and supporting working-class Americans (implied).
- Stay informed about the government shutdown situation and how it may impact you and your community (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how House Republicans are prioritizing impeachment over addressing critical issues like preventing a government shutdown and supporting working-class Americans. Viewing the full transcript can offer a deeper understanding of Beau's perspective on these political priorities.

# Tags

#HouseRepublicans #ImpeachmentInquiry #GovernmentShutdown #WorkingClassAmericans #Priorities