# Bits
Beau says:

- Bernie showing support for Biden by making a video is being misconstrued by some as him selling out to corporate Democrats.
- Biden's appearance at a UAW picket line with workers is seen as historic by Bernie, not a sign of selling out.
- Beau believes Bernie's video was an attempt to draw attention to the importance of Biden's support at the picket line.
- The President showing up at a picket line was a significant move that could be used to advocate for striking workers.
- Beau argues that Biden supporting some progressive ideas does not mean he is entirely progressive and rejects the narrative of good guys versus bad guys.
- Biden's appearance at the picket line was seen as a show, which is a common practice to garner attention and media coverage.
- Bernie's video used the President's appearance to shed light on CEO pay packages versus worker wages and the struggles faced by workers.
- The decline in union strength has impacted starting wages and job status in sectors like automotive manufacturing.
- Beau defends Bernie's actions as a way to bring attention to workers' issues and believes Biden's support was a strategic move for gaining media coverage.
- The situation is viewed as a blend of social media narratives and pessimism, but Beau encourages considering the positive impact of the President's appearance at the picket line.

# Quotes
- "A sitting president of the United States showed up at a picket line of an active strike and threw the weight of the Oval Office behind the workers."
- "I think Bernie was just trying to acknowledge that and use it. Use it. Use the power of the president showing up to get that message out there."
- "Biden showing up was for show. Absolutely. Absolutely. That part is true."

# Oneliner
Beau explains why Bernie's support for Biden and Biden's appearance at a picket line are significant moments often misunderstood and misrepresented, shedding light on the importance of advocating for workers' rights.

# Audience
Activists, Political Observers, Voters

# On-the-ground actions from transcript
- Support workers' rights and unions by actively participating in strikes and picket lines (exemplified)
- Advocate for fair wages and better working conditions in your community (implied)
- Educate others about the importance of unions and the struggles faced by workers (implied)

# Whats missing in summary
The full transcript provides a detailed analysis of the significance of Bernie's support for Biden and Biden's appearance at a picket line, shedding light on the importance of advocating for workers' rights and the impact of declining union strength on starting wages and job status in various sectors.

# Tags
#BernieSanders #JoeBiden #WorkersRights #UnionSupport #PoliticalAnalysis #SocialJustice