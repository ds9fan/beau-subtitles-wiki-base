# Bits

Beau says:

- Republicans are using social media to provoke outrage and elevate their profile.
- Marjorie Taylor Greene introduced a provision to reduce the Defense Secretary's salary to $1 a year.
- Greene believes the current Defense Secretary is destroying the military despite his 40+ years of service.
- Legislation like Greene's provision has zero chance of becoming law but is used for social media engagement.
- High-ranking military officials are paid well to make them harder to buy and prevent financial motives.
- While Americans face financial difficulties, politicians are focused on social media antics instead of real issues.

# Quotes

- "They're confusing social media engagement with votes."
- "What's the message? Marjorie Taylor Greene thinks she knows more about the military than the 40-year-plus veteran."
- "Legislation like this has zero chance of ever actually going anywhere."
- "This is what they're doing for social media clicks."
- "Rather than getting a budget together, this is what they're playing with."

# Oneliner

Republicans use social media antics like reducing the Defense Secretary's salary to $1 to provoke outrage and gain attention, while real issues like Americans facing financial difficulties are sidelined.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Contact local representatives to prioritize real issues over social media antics (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of how Republicans are leveraging social media for attention while neglecting pressing issues.

# Tags

#Republicans #SocialMedia #MarjorieTaylorGreene #DefenseSecretary #PoliticalAntics