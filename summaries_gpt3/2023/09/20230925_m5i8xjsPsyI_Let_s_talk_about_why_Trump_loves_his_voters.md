# Bits

Beau says:

- Trump professes his love for his base, and it's true; he loves them deeply.
- Trump understands that a government shutdown will hurt Americans but reassures his base that they won't be blamed; he'll pin it on Biden.
- He openly commands a government shutdown while assuring his base that they won't face the blame.
- Trump counts on the ignorance of his base to manipulate them into believing his narrative.
- Trump loves his base because they are easy to manipulate and trick into supporting him, even when he's ordering actions that harm Americans.

# Quotes

- "Did you catch what he actually said there?"
- "He just doesn't think they're smart enough to figure it out."
- "That's why Trump loves the uneducated."

# Oneliner

Trump openly commands a government shutdown, reassuring his base they won't face blame, banking on their ignorance, and love for manipulation.

# Audience

Voters, activists, citizens

# On-the-ground actions from transcript

- Call out manipulation and misinformation in political leaders (exemplified)
- Educate and inform others about political tactics (exemplified)
- Stay informed and critically analyze political statements (exemplified)

# Whats missing in summary

The full video provides in-depth analysis on Trump's manipulation tactics and the dangers of blind loyalty to political figures.