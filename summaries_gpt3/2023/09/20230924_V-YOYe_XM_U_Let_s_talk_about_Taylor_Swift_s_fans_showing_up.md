# Bits

Beau says:

- Taylor Swift's social media post directing people to vote.org led to tens of thousands registering to vote, averaging 13,000 users every 30 minutes.
- This resulted in a 23% increase for National Voter Registration Day, all from an Instagram post.
- The post also caused a 115% increase in 18-year-olds registering to vote, signifying a substantial impact.
- Many younger people seem unhappy with the current state of things and are motivated to make a change.
- Beau stresses that registering to vote is just the first step, and it's vital to show up and vote when it really matters.
- He warns against letting the momentum fade and not following through with voting after registering.
- Beau urges people to understand the importance of showing up to vote to bring about the desired change.
- He mentions that sustained engagement and messaging from Taylor Swift could have a significant impact on certain races.
- Younger people, particularly those affected by current legislation, might have a higher turnout due to their friends being targeted.
- Beau expresses hope for increased youth participation in voting, given the circumstances and motivations.

# Quotes

- "It led to tens of thousands of people registering to vote."
- "Not showing up when it actually matters, when it's time to vote."
- "You have to show up. You have to do it."
- "In some places her activity alone will swing races."
- "I think that they understand that shade never made anybody."

# Oneliner

Taylor Swift's social media post led to a significant increase in voter registration, especially among 18-year-olds, stressing the importance of showing up to vote for actual change.

# Audience

Young voters

# On-the-ground actions from transcript

- Register to vote and encourage others to do so (exemplified)
- Show up and vote in elections (implied)

# Whats missing in summary

The full transcript provides additional context on the impact of social media posts by influential figures like Taylor Swift in driving voter registration and youth engagement in the political process.

# Tags

#VoterRegistration #YouthEngagement #TaylorSwift #Influence #Voting