# Bits

Beau says:

- Senator Bob Menendez from New Jersey has been indicted on bribery charges, leading to calls for his resignation from the Democratic Party.
- Menendez has decided to step down from the Senate Foreign Relations Committee but not resign from the Senate itself.
- This situation echoes a new tradition in Congress where politicians facing indictments give up committee assignments rather than resigning.
- Previously, the tradition demanded resignation upon indictment, but now it seems to have shifted towards relinquishing committee roles.
- The change in this tradition may have occurred in the last six years during an administration that openly disregarded laws and norms.
- The current leniency towards indicted politicians suggests they are not held to the same standards as the general public.
- The Democratic Party is likely to intensify calls for Menendez's resignation as legal proceedings progress.
- Politicians being held to lower standards than the average citizen historically leads to negative outcomes for countries.
- Accepting that elites are above the law can result in rapid deterioration of a nation.
- The lack of accountability for politicians sets a dangerous precedent for the country's future.

# Quotes

- "They're almost not expected to be. They're not expected to be held to the same standards."
- "Those countries that openly accept the idea that their betters truly are better and they can go by a different set of laws, it doesn't go well for that country."

# Oneliner

Senator Menendez indicted on bribery charges sparks calls for resignation, unveiling a new trend in Congress where indicted politicians relinquish committees instead of resigning, raising concerns about accountability and a dangerous precedent for the nation.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Advocate for stricter accountability measures for politicians (implied)
- Stay informed about politicians' actions and hold them accountable (implied)

# Whats missing in summary

Further insights on the potential consequences of allowing politicians to operate under different standards than the general public.

# Tags

#SenatorMenendez #BriberyCharges #PoliticalAccountability #Congress #Indictments