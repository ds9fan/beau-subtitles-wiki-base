# Bits

Beau says:

- Explains the potential impact of a government shutdown on federal workers and services.
- Points out the ripple effects of federal workers not getting paid, affecting the local economy.
- Breaks down the proposed Republican continuing resolution and its potential consequences, like an 8% cut leading to severe impacts on various services and programs.
- Raises concerns about vulnerable populations bearing the brunt of budget cuts.
- Suggests prioritizing cuts from sources like companies not paying taxes rather than affecting the most vulnerable.
- Encourages seeking detailed information and context rather than relying solely on labels like "extreme budget."

# Quotes

- "each thing impacts the next."
- "you're going to lose roughly 800 Border Patrol people."
- "there's something on that list they don't want to give up."
- "maybe there's some money there that could be used just saying."
- "Just because somebody says, well, it's an extreme budget, yeah, good call. Don't take their word for it."

# Oneliner

Beau explains the potential impacts of a government shutdown and the proposed Republican budget cuts, urging people to seek detailed information rather than relying on labels like "extreme budget."

# Audience

Budget-conscious citizens

# On-the-ground actions from transcript

- Contact local representatives to express concerns about the potential impact of budget cuts on vulnerable populations (implied).
- Join advocacy groups that provide detailed information on proposed budget cuts and their consequences (implied).

# Whats missing in summary

The full transcript provides a comprehensive breakdown of the potential consequences of a government shutdown and proposed budget cuts, urging individuals to seek detailed information to understand the real impact.

# Tags

#GovernmentShutdown #RepublicanBudgetCuts #BudgetPrioritization #CommunityAction