# Bits

Beau says:

- Beau introduces episode six of "The Road's Not Taken," a series where he covers under-reported stories and provides context.
- He starts off discussing foreign policy, mentioning the F-35 aircraft and NATO's tactics to decentralize aircraft for survivability.
- Beau criticizes the missed opportunities of the current Speaker of the House, McCarthy, regarding Zelensky's visit to the US.
- He talks about indigenous land rights in Brazil being enshrined by the high court, anticipating future conflicts due to resource interests.
- Beau analyzes confusion surrounding open-source data on Russian conscript loss rates, noting Russia's use of less trained personnel in combat.
- He mentions a situation in South Korea where US military installations were raided, leading to investigations of US service members for involvement in a drug ring.
- Beau reports on Bulgarians accused of spying for Russia in the UK, hinting at significant espionage activities.
- He comments on Russia deploying elite paratroopers as regular infantry, showcasing a lack of appropriate personnel.
- Beau shifts focus to the United States, mentioning an investigation into misconduct allegations against Baton Rouge cops by the Civil Rights Division.
- He notes the expansion of the United Auto Workers Strike and Biden's potential visit to support the union workers.
- Beau touches on Trump's promise of mass deportations if re-elected and the importance of not blaming those with less power for problems.
  
# Quotes

- "He lost the opportunity to have Republicans standing beside Zelensky as he talked about how vital that relationship with the United States is."
- "Nobody who has less institutional power than you is the source of your problem."
- "Shovels can be incredibly significant and useful, especially in situations you might not think about it."

# Oneliner

Beau covers foreign policy, missed opportunities in US politics, indigenous land rights, Russian conscript confusion, and South Korean investigations.

# Audience

Policy influencers, activists

# On-the-ground actions from transcript

- Contact local representatives to advocate for indigenous land rights protections (suggested)
- Support unions and workers' rights by joining local labor movements (implied)

# Whats missing in summary

The full transcript provides in-depth insights into global events, geopolitical strategies, and domestic political dynamics, offering a comprehensive analysis of underreported stories and missed opportunities in foreign policy and US politics.

# Tags

#ForeignPolicy #UnderreportedStories #PoliticalAnalysis #LaborRights #Geopolitics