# Bits

Beau says:

- FTC rule eliminates non-compete clauses, a win for labor.
- Expect legal challenges to narrow the rule's scope.
- Less than 1% chance the rule will stand as is.
- Possible argument: FTC overstepped its authority by creating a new law.
- Administration likely has a response to the separation of powers issue.
- Predicts large portions of the rule will stand, still a win for labor.
- Deep-pocketed interests will challenge the rule in court.
- Anticipates a lot of news coverage on challenges to the rule.

# Quotes

- "Non-compete clauses have never made sense to me."
- "I don't think an employer should be able to limit where you work in the future."
- "Expect to hear a lot of news about challenges to this."

# Oneliner

FTC's rule eliminating non-compete clauses faces legal challenges and questions of authority, with predictions of narrowing but still a win for labor.

# Audience

Labor advocates, policymakers.

# On-the-ground actions from transcript

- Stay informed on developments regarding the FTC rule and legal challenges (implied).
- Support organizations advocating for labor rights and fair employment practices (implied).

# Whats missing in summary

Deeper analysis on the potential impacts of the FTC rule changes and the long-term effects on labor rights and employment practices. 

# Tags

#FTC #NonCompeteClauses #LaborRights #LegalChallenges #EmploymentPractices