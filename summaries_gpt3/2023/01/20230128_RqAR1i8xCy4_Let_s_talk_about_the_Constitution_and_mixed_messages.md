# Bits

Beau says:

- Explains the mixed messages about the Constitution he's been accused of giving.
- Talks about how he loves the Constitution's concise and precise nature, mentioning that it's the reason why he might make a short video just reading it.
- Points out that the Constitution repeats only one command, which is about not depriving any person of life, liberty, or property without due process, emphasized in both the fifth and 14th amendments.
- States that this command is the foundation of everything in the Constitution, making it the glue that holds the nation together as a nation of laws.
- Argues that supporting the Constitution means following this command twice given, and failure to do so means not truly supporting any part of it.
- Stresses the importance of evaluating behavior through the lens of necessity to prevent deprivation of life, liberty, or property without due process.
- Concludes by reiterating that failure to support this fundamental command means not genuinely supporting the Constitution in any aspect.

# Quotes

- "No person shall be deprived of life, liberty, or property without due process."
- "If you do not support the one command that the Constitution gave the governments twice, you cannot pretend that you support the Constitution."

# Oneliner

Beau explains mixed messages about the Constitution, focusing on the fundamental command of due process and its critical role in upholding the nation of laws.

# Audience

Constitutional enthusiasts

# On-the-ground actions from transcript

- Ensure understanding and support for the fundamental command of due process (emphasized)
- Advocate for the necessity of due process in all actions (implied)

# Whats missing in summary

The full video provides a deep dive into the importance of upholding the fundamental command of due process in the Constitution and its impact on supporting the nation of laws.

# Tags

#Constitution #DueProcess #Support #FundamentalCommand #NationOfLaws