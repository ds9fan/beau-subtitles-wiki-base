# Bits

Beau says:

- Talks about a person on YouTube who feels like they are "preaching to the choir" due to constant attacks if they express self-doubt.
- Beau shares his experience of receiving hate mail to show that the person is not only reaching like-minded people.
- Questions the notion that preaching to the choir is pointless, mentioning pastors preaching to choirs even though they are already aware of the information.
- Emphasizes that sometimes the goal is not to convince others but to let those who think similarly know they are not alone.
- Suggests that reinforcing beliefs and keeping people engaged has value in moving towards a better world.
- Encourages the idea that even if you are speaking to like-minded individuals, your words can still inspire growth and progress.

# Quotes

- "I don't say this to convince somebody, I say it so those who already think like me know they're not alone."
- "It keeps people thinking about the larger issues. It keeps people engaged. It makes them push further, hopefully beyond you."
- "Maybe your sermon helps them to keep the faith. Helps keep them moving forward and spurs their ideological growth."

# Oneliner

Beau shares insights on the value of speaking to like-minded individuals and how it can contribute to progress and growth, challenging the notion of "preaching to the choir."

# Audience

Creators, Activists, Influencers

# On-the-ground actions from transcript

- Start a community group centered around shared beliefs and growth (suggested)
- Encourage ideological growth through engaging content creation (implied)

# Whats missing in summary

Exploration of the importance of solidarity, community building, and empowerment in shared beliefs.