# Bits

Beau says:

- Beau addresses the concepts of philosophy, consistency, utopias, and contradictions, sparked by a message he received.
- The message questions advocating for certain government interventions while maintaining a philosophy of anti-authoritarianism.
- Beau shares his vision of a cooperative society where people live harmoniously without a president.
- He presents two answers to the question posed, a Twitter answer and a real answer.
- The Twitter answer focuses on being a leftist for harm reduction and helping people through advocating for egalitarian society.
- The real answer involves a scene from the movie Platoon that impacted Beau, showcasing the idea of fairness in combat roles for rich and poor individuals.
- Beau contrasts the lifestyle of those in gated communities with homeowners associations to those in trailer parks and government housing, who already practice cooperation out of necessity.
- He recounts an experience in government housing where individuals exemplified community support despite societal stereotypes.
- Beau stresses the importance of providing resources and support to individuals in poverty to expand the reach of anti-authoritarian philosophies.
- He concludes by suggesting that to achieve a utopia, one must be willing to adapt and be pragmatic in their approach.

# Quotes

- "You're not going to get very far telling people who are barely getting by that they have to go further."
- "When you're thinking about your ideologically pure utopia that's going to emerge generations from now, you can be pretty much anything in service of that except for ideologically pure."

# Oneliner

Beau delves into philosophy, utopias, and the challenges of advocating for societal change while navigating contradictions and realities of poverty.

# Audience

Philosophy enthusiasts, activists

# On-the-ground actions from transcript

- Support community initiatives to aid individuals living in poverty (implied)
- Advocate for resources and assistance for those struggling financially (implied)

# Whats missing in summary

Beau's engaging storytelling and nuanced perspective on societal issues can best be appreciated by watching the full video. 

# Tags

#Philosophy #Consistency #Utopias #Contradictions #AntiAuthoritarianism #CommunitySupport #Poverty #Advocacy #SocietalChange