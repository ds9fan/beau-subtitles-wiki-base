# Bits

Beau says:

- Criticizes Trump's campaign promise to use the US military to go after organizations in Mexico without questioning the cause and effect or looking at the consequences.
- Trump plans to utilize the US military against organizations in Mexico, similar to how he took down IS, despite ongoing hostilities with IS and recent incidents in the US.
- Trump's promise involves ordering the Department of Defense to utilize special forces, cyber warfare, and covert actions to target cartel leadership, infrastructure, and operations.
- Beau points out the potential mission creep and spread of military operations if cartels are targeted, leading to a situation spiraling out of control and triggering a civil war.
- The promise is not to defeat the cartels but rather to create refugees and unnecessary destruction.
- Criticizes the media for not questioning campaign promises and focusing on the potential negative outcomes of such actions.
- Mentions past US military interventions leading to refugees and destabilization in other countries, suggesting a similar outcome if Trump's promise is fulfilled.
- Emphasizes the cycle of mistakes in US foreign interventions and the inability to fix other countries while causing harm.
- Concludes that Trump's promise will result in unnecessary destruction, chaos, and more refugees without achieving victory.

# Quotes

- "His promise is not to win. His promise is to create a bunch of refugees."
- "It might be time for the media to stop accepting campaign promises just as talking points."
- "Talk about what is actually going to occur if this campaign promise is fulfilled."
- "A whole bunch of unnecessary destruction and chaos, a whole bunch more refugees."
- "He won't win, because this war, the plants won."

# Oneliner

Beau criticizes Trump's promise to use the US military in Mexico, foreseeing unnecessary destruction, chaos, and more refugees due to flawed foreign interventions.

# Audience

Activists, Policy Makers

# On-the-ground actions from transcript

- Advocate against harmful foreign interventions (implied)
- Support policies that prioritize diplomacy over military actions (implied)

# Whats missing in summary

Full understanding of the negative impact of Trump's proposed military actions and the cyclical nature of US interventions leading to destabilization in other countries.

# Tags

#Trump #USMilitary #ForeignIntervention #Refugees #MediaCoverage