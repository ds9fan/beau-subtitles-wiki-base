# Bits

Beau says:

- Explains the request for basic courtesy and respect in addressing individuals based on their title, name, and pronouns.
- Describes the common response of "I just don't get it" when discussing gender identity and pronouns.
- Notes a shift in social conservatives' response towards Pete Buttigieg and his husband regarding the term "husband."
- Questions the inconsistency in social conservatives' argument for using gender-neutral terms like "partner" instead of "husband."
- Points out the underlying motivation of bigotry and discrimination rather than genuine lack of understanding.
- Emphasizes the celebration of title changes as reflective of personal growth and identity.

# Quotes

- "They want to be addressed in a way that reflects them."
- "It's not that you didn't understand. It's that you didn't want to."
- "People go through title changes all the time, and it normally marks the growth of that person."
- "You'll feel better about your lot in life if you can kick down at somebody."
- "It's that somebody had told them, you can other somebody this way."

# Oneliner

Beau explains the resistance to using gender-neutral terms and addresses the underlying bigotry in social conservatives' response to Pete Buttigieg's marriage.

# Audience

All individuals

# On-the-ground actions from transcript

- Support and respect individuals by addressing them using their preferred title, name, and pronouns (implied)
- Challenge discriminatory attitudes and behaviors towards marginalized communities (implied)

# Whats missing in summary

The full transcript expands on the importance of respecting individuals' identities and the negative impact of bigotry on personal growth.

# Tags

#GenderIdentity #Respect #Bigotry #SocialConservatives #TitleChanges