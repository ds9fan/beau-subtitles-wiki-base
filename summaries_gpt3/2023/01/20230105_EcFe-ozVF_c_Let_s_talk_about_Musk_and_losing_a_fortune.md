# Bits

Beau says:

- Elon Musk has reportedly lost a couple hundred billion dollars, surpassing others as the biggest loser of wealth.
- Musk's alignment with the far right since acquiring Twitter has led to questions about the impact of "go-fash-no-cash" on his wealth.
- The majority of Musk's wealth loss is attributed to Tesla's valuation and marketing rather than his actions on Twitter.
- Musk's marketing genius led to Tesla being valued at around a trillion dollars, surpassing all other major car manufacturers combined.
- Doubts about Tesla's valuation and Musk's selling of Tesla stock prior to buying Twitter contributed to the decline in stock prices.
- Musk's alignment with the far right through pushing conspiracy theories on Twitter has alienated many, including his core demographic of left-wing supporters.
- The perception that Musk is now associating with the right wing has led to alienation of customers and potential customers.
- Musk's failure to maintain Tesla's marketing momentum and valuation has affected the company's stock performance.
- Current Tesla customers are being lost due to Musk's actions and marketing strategies.
- The decline in Tesla's stock prices is primarily due to the company's inability to sustain the valuation driven by Musk's marketing efforts.

# Quotes

- "Somebody can lose a couple hundred billion dollars and still be one of the richest people on the planet."
- "It's at this point, I would say most of it is just the marketing exceeding the performance."
- "He is a genius. Just not the kind that people think."
- "Is this go-fash, no cash? Yes, but also no."
- "Elon Musk becoming the biggest loser of wealth ever."

# Oneliner

Elon Musk's wealth loss is primarily tied to Tesla's valuation and marketing, not just his actions on Twitter, alienating customers along the way.

# Audience

Investors, Tesla enthusiasts

# On-the-ground actions from transcript

- Support alternative electric vehicle companies to reduce reliance on Tesla (implied).
- Engage in critical analysis of company valuations and marketing strategies to make informed investment decisions (implied).
- Advocate for ethical business practices and alignment with values when supporting companies (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of the factors contributing to Elon Musk's wealth loss, including his marketing strategies, Tesla's valuation, and alienation of customers.

# Tags

#ElonMusk #Tesla #WealthLoss #MarketingGenius #FarRight