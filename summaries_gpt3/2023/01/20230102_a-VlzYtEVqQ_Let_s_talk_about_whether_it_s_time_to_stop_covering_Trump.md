# Bits

Beau says:

- Addressing whether 2023 is the year to stop covering Trump due to recent events, including the coup attempt.
- Asserting that historic events like the coup attempt should continue to be talked about and not forgotten.
- Noting that Trump's attempts to hold a press conference with no cable news outlets showing up indicates his dwindling influence.
- Drawing a parallel to a 1924 article about Hitler being "tamed by prison" and the importance of continuing coverage until there's a conclusion.
- Emphasizing that those who enabled Trump and similar figures are still in power, indicating a need to keep covering them.
- Stating that Trumpism will outlive Trump and continue under new names or figures if coverage stops prematurely.

# Quotes

- "The coverage has to continue until there's a conclusion."
- "They may rehabilitate their image, but they showed you who they are."
- "Trumpism will outlive Trump."
- "It's not over. Make no mistake about it."
- "If the coverage stops, Trump becomes this bygone thing."

# Oneliner

Addressing the need to continue covering Trump and those who enabled him, as Trumpism will outlive him if coverage stops prematurely.

# Audience

Journalists, activists, citizens

# On-the-ground actions from transcript

- Continue staying informed about political events and figures (implied)
- Stay engaged in political discourse and hold those in power accountable (implied)

# Whats missing in summary

The full transcript includes historical parallels and a call to action to stay vigilant against authoritarian figures and their enablers.

# Tags

#Trump #Coverage #PoliticalAnalysis #Authoritarianism #MediaCoverage