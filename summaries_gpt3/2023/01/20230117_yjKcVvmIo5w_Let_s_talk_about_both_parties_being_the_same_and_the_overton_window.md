# Bits

Beau says:

- Explains the concept of the political spectrum with left, right, authoritarian, and anti-authoritarian.
- Addresses the statement that both political parties in the US are the same and where it originates.
- Points out that both parties use violence to maintain order but are not necessarily identical.
- Contrasts the Democratic and Republican parties regarding consent-based policing, demilitarizing the police, and responses to maintaining order.
- Analyzes the perspectives of anti-authoritarian right, authoritarian right, authoritarian left, and anti-authoritarian left on corporate and capitalist influence in politics.
- Emphasizes that while the criticisms are valid, there are distinctions between the parties in terms of policy and ideology.
- Acknowledges the challenges of debating with the extreme authoritarian right and anti-authoritarian left.
- Argues that anti-authoritarian left individuals have deeply rooted beliefs, are well-informed, and challenging to debate due to their strong convictions.
- Compares historical contexts to illustrate the evolution of political ideologies in the US.
- Encourages engaging in political discourse and mentions the Tea Party's impact on shaping Republican Party policies.

# Quotes

- "Both parties do, in fact, use the power of the state, the violence of the state, to maintain order."
- "The United States is authoritarian right."
- "People who are anti-authoritarian left, they didn't get to that position through bumper sticker mentality."

# Oneliner

Beau explains the nuances between political parties in the US, addressing criticisms of similarity and underlying ideological differences.

# Audience

Politically engaged individuals.

# On-the-ground actions from transcript

- Debunk anti-Semitic conspiracy theories through education and awareness (implied).
- Engage in civil discourse with individuals holding different political beliefs to foster understanding and constructive debate (implied).

# Whats missing in summary

In-depth analysis of the historical context shaping political ideologies and the significance of engaging in political discourse for societal progress.

# Tags

#PoliticalParties #USPolitics #IdeologicalDifferences #Debate #Authoritarianism