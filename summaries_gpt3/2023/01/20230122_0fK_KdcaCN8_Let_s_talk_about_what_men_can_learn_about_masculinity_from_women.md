# Bits

Beau says:

- Addresses the concept of masculinity and what women from the pre-women's liberation era can teach modern men about it.
- Dismisses the idea of "alpha" and "traditional masculinity," stating they are not real concepts.
- Describes the advice given to women in the past on how to attract a man, including posture, topics to talk about, and appearance.
- Talks about the shifting views of masculinity and the confusion it causes for young men.
- Criticizes the advice given to men today on how to be masculine, likening it to teaching them to be women in the 1800s.
- Mentions the importance of heroes exhibiting universal qualities like problem-solving and community protection.
- Shares a Japanese concept of masculinity related to achieving perfection without effort and helping the community.
- Criticizes those pushing consumerism as a form of masculinity and encourages helping the community instead.
- Emphasizes that masculinity is not unified and individuals should focus on genuine actions to make the world better.
- Urges men to think for themselves and to prioritize helping others as a true representation of masculinity.

# Quotes

- "They're not teaching you to be a man. They're teaching you to be a woman in the 1800s."
- "Your masculinity will be perfect if you just get out there and help."
- "If you want to achieve that good masculinity, that kind that doesn't get made fun of in razor commercials, you have to think for yourself."

# Oneliner

Beau dismantles traditional ideas of masculinity, urging men to prioritize community service over superficial traits to embody true manhood.

# Audience

Men, Young Adults

# On-the-ground actions from transcript

- Help your community by volunteering, supporting others, and making the world better (implied).

# Whats missing in summary

In-depth exploration of the impact of consumerism on modern masculinity and the importance of critical thinking in defining true manhood.

# Tags

#Masculinity #CommunityService #GenderRoles #TraditionalIdeals #CriticalThinking