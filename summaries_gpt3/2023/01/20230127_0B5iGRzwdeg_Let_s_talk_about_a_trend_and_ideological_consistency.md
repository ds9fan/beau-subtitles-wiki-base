# Bits

Beau says:

- Explains how Switzerland and Finland can teach us about Ukraine and Russia.
- Points out the trend of people believing in something off and not liking the answers when exploring the roots of their beliefs.
- Addresses the idea of Russia having a national interest in invading Ukraine due to NATO's influence.
- Talks about the impact of Russia's aggression on countries like Finland and Switzerland.
- Mentions the issue of Russia's actions not being within international law.
- Challenges the pretext of NATO's presence on Russia's borders being a valid reason for Russia's invasion of Ukraine.
- Criticizes arguments defending Russia's imperialism based on historical factors and racial hierarchies.
- Raises questions about defending imperialism and supporting Russian influence over sovereign countries.
- Dismisses claims of Red Scare propaganda as a justification for opposing Russia.
- Condemns the idea of justifying military action based on another country's approval as condoning imperialism.

# Quotes

- "NATO cannot exist on Russia's borders."
- "Just because something is in a nation's national interest doesn't mean that it's the correct moral or lawful thing to do."
- "If you are saying that one country's interests do not matter unless they can be approved by another country, you're condoning imperialism."

# Oneliner

Beau explains how Switzerland and Finland's actions reveal insights on Ukraine and Russia, challenging beliefs and justifications for Russian aggression and imperialism.

# Audience

Global citizens

# On-the-ground actions from transcript

- Support efforts to counter misinformation about international relations (suggested).
- Advocate for diplomacy and respect for sovereign countries' interests (suggested).

# Whats missing in summary

In-depth analysis of Russia's wider geopolitical losses beyond the provided pretext.

# Tags

#Ukraine #Russia #NATO #Imperialism #InternationalRelations