# Bits

Beau says:

- Introduces an interactive video discussing journalism practices and a specific case to illustrate a point.
- Mentions the intersection of Rains Road and Ross Road as a focal point for the demonstration.
- Instructs viewers to search for a specific phrase related to an incident at the intersection.
- Reads a police department press release regarding an incident at the mentioned intersection.
- Criticizes journalists for accepting police press releases as factual accounts without questioning.
- Raises concerns about police departments using ambiguous and misleading language in press releases.
- Urges journalists to scrutinize police narratives and compare them with actual footage.
- Questions the motives behind police press releases and the lack of accountability in reporting.
- Encourages journalists to stop treating police press releases as unquestionable truth.
- Calls for a critical examination of police narratives by journalists and a shift away from blind acceptance.

# Quotes

- "Stop taking police department press releases as gospel."
- "Make 2023 the year that journalists stop accepting police press releases as if they're fact."

# Oneliner

Beau introduces an interactive lesson on journalism, urging reporters to scrutinize police narratives instead of accepting them blindly as factual accounts. He criticizes the use of ambiguous and misleading police press releases.

# Audience

Journalists, News Outlets

# On-the-ground actions from transcript

- Scrutinize police narratives for accuracy and question ambiguous or misleading statements (implied).
- Stop accepting police department press releases as unquestionable truth (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how journalists should approach and verify information from police press releases, advocating for critical examination and transparency in reporting.

# Tags

#Journalism #PoliceNarratives #MediaAccountability #PressReleases #CommunityPolicing