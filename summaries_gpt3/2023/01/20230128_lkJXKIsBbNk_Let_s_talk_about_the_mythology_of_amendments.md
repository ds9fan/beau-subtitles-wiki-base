# Bits

Beau says:

- Analyzing the mythology surrounding the amendments, particularly the Second Amendment and its historical context.
- Emphasizing the founders' original intent behind the amendments and how it differs from modern interpretations.
- Exploring the idea that being pro-Second Amendment requires being alert to injustice and using peaceful civic actions first.
- Drawing parallels between the spirit of the First Amendment (freedom of speech) and the Second Amendment (right to bear arms).
- Arguing that being anti-Woke contradicts the spirit of the Second Amendment, as being "woke" involves being alert to injustice.
- Questioning why some leaders advocate for supporting the Second Amendment while being anti-Woke, which goes against the founding principles.
- Encouraging individuals to fulfill their duty under the Second Amendment by remaining vigilant against injustice and utilizing civic actions.

# Quotes

- "It's like the founders put up a piece of glass, you know, break glass only in the event of tyranny."
- "Tyranny is too late in the game. Things don't just flip and become totalitarian overnight."
- "By definition, you have to be alert to injustice."
- "You have to fulfill it. And you have to be alert to injustice, because that's what's going to precede tyranny."
- "It is a duty. You have to wonder why they don't want you alert to injustice."

# Oneliner

Beau questions the modern interpretation of the Second Amendment, advocating for vigilance against injustice as a core duty underlying its spirit.

# Audience

Citizens, Activists, Gun Rights Advocates

# On-the-ground actions from transcript

- Talk to your second amendment friends about the true spirit and duty behind it (suggested).
- Be alert to injustice in your community and surroundings (implied).

# Whats missing in summary

The full transcript provides a nuanced perspective on the historical context and original intent behind the Second Amendment, urging individuals to embody civic responsibility and vigilance against injustice.

# Tags

#SecondAmendment #FoundersIntent #Injustice #CivicDuty #Vigilance