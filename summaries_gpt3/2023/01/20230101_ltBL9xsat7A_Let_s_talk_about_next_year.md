# Bits

Beau says:

- Plans for next year's content on the channels are being discussed.
- The main channel will not see significant changes, just a workflow change and a new building for a more productive workspace.
- There will be a surprise rebuild of the shop in the new building.
- On the second channel, expect a community garden video, Jeep build phases, and content related to hurricane relief.
- The summer might bring travel content on the channel "The Roads with Bo."
- Two books are almost ready for release this year.
- Expect at least one live stream a month, possibly two.
- More interviews are planned on the second channel, including in-person interviews for deeper insights.
- Beau hints at bigger projects in the works but isn't ready to announce them yet.

# Quotes

- "So on the main channel, not much is going to change."
- "Expect at least one live stream a month, maybe two."
- "There are a couple of other bigger projects that we've been working on that I'm not quite ready to announce yet."

# Oneliner

Beau plans for upcoming content, including workspace upgrades, books, live streams, and in-depth interviews, with hints of bigger projects on the horizon.

# Audience

Content Creators

# On-the-ground actions from transcript

- Start planning and creating content for your channels (implied)
- Stay updated with community garden video, Jeep build phases, and hurricane relief content (implied)
- Tune in for live streams and interact with the content (implied)
- Support upcoming book releases and participate in possible motivational events (implied)
- Stay engaged for future announcements of bigger projects (implied)

# Whats missing in summary

Details on the specific topics of the two upcoming books and further elaboration on the surprise rebuild in the new building.

# Tags

#ContentCreation #ChannelUpdates #LiveStreams #Interviews #FutureProjects