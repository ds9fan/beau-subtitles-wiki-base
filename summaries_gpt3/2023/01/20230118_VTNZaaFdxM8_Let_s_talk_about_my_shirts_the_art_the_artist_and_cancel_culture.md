# Bits

Beau says:

- Explains the concept of separating the art from the artist and applies it to his choice of shirts.
- Mentions wearing shirts from Rick and Morty, but decides to stop wearing them due to allegations against a key player in the show.
- Talks about wearing Hunter S. Thompson shirts and how it wouldn't be a good fit for his channel if it routinely raised money for AA or NA.
- Provides an example of a Harry Potter shirt he couldn't wear on his channel due to the artist using her platform to attack people supportive of causes he raises money for.
- Describes how cancel culture isn't about targeting conservatives but about companies protecting their brand image and assets.
- Explains that cancel culture is essentially capitalism in action, removing things that might upset certain demographics that use the product.
- Concludes by stating that just separating the art from the artist doesn't guarantee acceptance everywhere, especially if the ideas associated with them cause harm to communities.

# Quotes

- "Just because you separate the art from the artist, that doesn't mean that the art is still going to be welcomed everywhere."
- "Cancel culture is literally capitalism in action."
- "It's not about being a conservative, it's about the ideas that are so aggressive that they're causing active harm to the communities that these companies are catering to."

# Oneliner

Beau explains the nuances of separating art from the artist and why it matters in the context of cancel culture and brand image protection.

# Audience

Content creators, Conservatives

# On-the-ground actions from transcript

- Support domestic violence shelters (implied)
- Be mindful of the impact of the content you support or associate with (implied)

# Whats missing in summary

Deeper insights into the impact of cancel culture on brand image and the importance of considering the associated ideas rather than just separating art from the artist.

# Tags

#CancelCulture #Artists #BrandImage #Conservatives #CommunitySupport