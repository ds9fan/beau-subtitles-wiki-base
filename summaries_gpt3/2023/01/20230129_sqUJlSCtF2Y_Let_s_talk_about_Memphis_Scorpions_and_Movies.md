# Bits

Beau says:

- Memphis has decided to disband the Scorpion team responsible for troubling actions.
- The strategy behind the Scorpion team, with plainclothes officers having a lot of leeway, raises concerns.
- Despite the team's disbandment, there hasn't been a disavowal of the problematic strategy.
- A significant percentage of officers from the team are currently facing legal consequences.
- The family's attorney has been uncovering more troubling information about the team's actions.
- The community gets tragedy while the department boasts seized assets.
- Hollywood movies and TV shows often draw inspiration from real-life events involving unchecked police authority.
- Officer Rafael Perez, associated with the Rampart scandal, was mimicked in Denzel Washington's character in "Training Day."
- Perez cooperated and exposed misconduct involving over 50 cops.
- Beau expresses concern about potentially losing records after the Scorpion team's closure.

# Quotes

- "The community gets tragedy while the department boasts seized assets."
- "Hollywood understands what happens when you provide that much unchecked authority."
- "Teams like this lead to problems."
- "Do I believe it's in good faith when they're not disavowing that strategy?"
- "They knew it was a Scorpion when they picked it up."

# Oneliner

Memphis disbands the problematic Scorpion team, but the concerning strategy remains unaddressed, reflecting the broader issue of unchecked police authority in popular culture.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Contact local representatives to advocate for the disavowal of problematic police strategies (suggested)
- Join community organizations focused on police accountability and reform (implied)

# Whats missing in summary

Exploration of the long-standing impact of unchecked police authority in real-life events and its portrayal in popular culture.

# Tags

#PoliceReform #PoliceAccountability #CommunityAction #MemphisPD #UncheckedAuthority