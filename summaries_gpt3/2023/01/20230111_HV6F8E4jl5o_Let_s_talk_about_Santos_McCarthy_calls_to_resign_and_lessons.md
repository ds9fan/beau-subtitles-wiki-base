# Bits

Beau says:

- Addressing ongoing developments involving Santos and the new representative.
- Local GOP calls for the representative to resign and not be seated on committees.
- Outrage emerged after news that the representative or his aide impersonated McCarthy's chief of staff for funds.
- Republican Party leaders and committees showed they can tolerate lies, except when it comes to money.
- Lesson learned: Republicans can lie to constituents, voters, and supporters as long as they don't "mess with the money."

# Quotes

- "They can totally lie to you. They can lie to their constituents. They can lie to their voters. They can lie to the people who support them. Just don't mess with the money."
- "It's totally okay with them if your representative, your senator, your elected official lies to you and misrepresents himself to you, but they better not mess with the money."

# Oneliner

The Republican Party tolerates lies as long as money isn't compromised, revealing a stark truth about GOP priorities.

# Audience

Republicans

# On-the-ground actions from transcript

- Confront establishment Republicans about their tolerance for lies from representatives (implied).

# Whats missing in summary

The full transcript provides a detailed insight into the Republican Party's priorities and the consequences of lying, especially in relation to financial interests.