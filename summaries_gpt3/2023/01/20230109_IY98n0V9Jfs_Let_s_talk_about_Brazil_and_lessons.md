# Bits
Beau says:

- Drawing parallels between recent events in Brazil and the United States where supporters of a far-right authoritarian leader stormed key buildings.
- Noting the transferability of nationalist rhetoric and imagery used to incite far-right supporters.
- Emphasizing that the threat is not over, cautioning against underestimating the persistence of this style of leadership.
- Warning that without a resounding defeat, this authoritarian playbook will continue to be replicated with potential success.
- Stating that the fight against far-right authoritarianism is ongoing and needs to be acknowledged and addressed.
- Stressing the importance of recognizing the pattern and not dismissing current events as a thing of the past.
- Mentioning the ease with which false patriotism can be manufactured to manipulate aggressive individuals reminiscing about a mythic past.
- Noting that this leadership style can be easily mimicked and that the fight against it must persist.
- Calling attention to the need to take seriously the lessons from recent events and not downplay the ongoing threat.
- Urging vigilance and acknowledging that until there is a clear defeat, this cycle may continue.

# Quotes
- "It's not over. It's not over in the United States. It's not over in Brazil. And it can happen anywhere."
- "That style of leadership, it will transfer to the next Xeroxed copy of Trump because there wasn't that resounding defeat."
- "We can't just pretend that it's stopped."
- "This far right authoritarian style of leadership is easily mimicked."
- "This is one of those times when the polls matter."

# Oneliner
Recent events in Brazil mirror past events in the United States, warning against underestimating the ongoing threat of far-right authoritarianism and stressing the need for continued vigilance.

# Audience
Activists, concerned citizens

# On-the-ground actions from transcript
- Stay informed and engaged with current events (suggested)
- Advocate for policies and leaders that prioritize democracy and human rights (implied)

# Whats missing in summary
The full transcript provides a detailed analysis of the parallels between recent events in Brazil and the United States, urging vigilance and action against the ongoing threat of far-right authoritarianism.

# Tags
#FarRight #Authoritarianism #Nationalism #Brazil #UnitedStates