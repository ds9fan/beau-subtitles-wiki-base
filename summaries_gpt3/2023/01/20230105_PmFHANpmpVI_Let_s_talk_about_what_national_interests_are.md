# Bits

Beau says:

- Defines national interests as the pursuit of power in foreign policy.
- National interests involve safety and wealth to achieve power.
- Foreign policy decisions are not transactional but part of an international poker game.
- Gives an example of releasing oil to control global oil prices.
- Explains how countries like China strategize based on their national interests.
- US foreign policy often aims to maintain the status quo as the world's top power.
- Foreign policy decisions are ultimately about power, safety, and wealth for the country as a whole.
- Criticizes the current foreign policy system, suggesting a shift from being the world's policeman to the world's EMT.
- Stresses that anything benefiting a country in terms of safety, wealth, and power is considered a national interest.
- Urges to view foreign policy decisions through the lens of power, safety, and wealth.

# Quotes

- "Foreign policy is the pursuit of power, plain and simple."
- "If you want to get more in depth, it is safety and wealth being used to achieve power."
- "It's about achieving, building, storing power, and then using it theoretically to benefit the population."
- "Foreign policy decisions are always going to lead you to why that decision was made."
- "Anything that benefits the country in the realm of safety, wealth, and ultimately power, is a national interest."

# Oneliner

Beau defines national interests as the pursuit of power in foreign policy, involving safety and wealth to achieve power, and criticizes the current system, advocating for a shift towards being the world's EMT.

# Audience
Foreign policy analysts

# On-the-ground actions from transcript

- Analyze foreign policy decisions through the lens of power, safety, and wealth (suggested)
- Advocate for a shift in foreign policy towards prioritizing emergency response over policing (implied)

# Whats missing in summary

In-depth examples and analysis of how national interests influence foreign policy decisions.

# Tags
#ForeignPolicy #NationalInterests #Power #Safety #Wealth