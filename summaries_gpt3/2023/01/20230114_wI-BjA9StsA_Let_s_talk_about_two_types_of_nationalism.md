# Bits

Beau says:

- Talks about nationalism and the different kinds of nationalism.
- Mentions how people have pointed to Ireland as an example of nationalism used for good.
- Explains the distinction between nationalism as a tool and nationalism as an ideology.
- Describes how nationalism was used by Connolly and Pearse in Ireland in 1916.
- Points out that nationalism becomes an ideology when the nation already exists.
- Addresses the negative impacts of nationalism on the economy, environment, diplomacy, and domestic situations.
- Emphasizes that nationalism is good for motivating people for war.
- Explains how nationalism can morph into fascism when internal enemies are sought.
- Criticizes nationalism as politics for people who require a leader to tell them what to do.
- Warns about the dangers of nationalism as an ideology leading to perpetual war.
- Talks about the prevalence of nationalism in the US and its potential dangers.
- Connects the push for nationalism in the US to political movements like Trump's.

# Quotes

- "Nationalism is good for war, which is useful if you're trying to achieve a nation."
- "Nationalism is politics for basic people. People who really require a leader."
- "Nationalism in service of throwing off a colonial yoke. Yeah, I get it. It's a tool."

# Oneliner

Beau explains the dangers of nationalism as an ideology leading to perpetual war and the prevalence of nationalism in the US.

# Audience

Activists, policymakers, educators.

# On-the-ground actions from transcript

- Analyze and challenge nationalist rhetoric in your community (suggested).
- Educate others on the dangers of nationalism turning into fascism (suggested).

# Whats missing in summary

In-depth analysis of the historical and contemporary implications of nationalism and its potential for both positive and negative outcomes.

# Tags

#Nationalism #Ireland #Fascism #US #PoliticalMovements