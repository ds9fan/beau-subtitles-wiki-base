# Bits

Beau says:

- Explains the addition of 87,000 new employees at the IRS over 10 years.
- Clarifies that more than 50,000 of the new hires are to replace retiring IRS employees.
- Notes a net gain of around 27,000 employees, mainly customer service and IT staff.
- Debunks the myth of hiring 87,000 armed agents, stating they are specifically targeting tax evaders.
- Mentions that individuals making under $400,000 annually won't face increased audit chances.
- Points out that despite the bill passing in the house, it's unlikely to advance in the Senate.
- Criticizes the creation of false issues by certain political factions for perceived victories.
- Emphasizes that the reported changes are still set to occur with increased hiring and audits.
- Condemns the misleading narrative created around the IRS issue for political gains.
- Concludes that the core situation remains unchanged, despite the political spectacle.

# Quotes

- "87,000 armed agents, okay? That was never going to happen."
- "They just made that up and it got reported on by outlets like Fox."
- "They will hire 87,000 more people and there will be more audits of people who make more than $400,000 a year."
- "Literally nothing changed, except they got a cool little vote and talking point in."
- "So all of this is still happening, just so everybody's clear on that."

# Oneliner

Beau clarifies the hiring of 87,000 IRS employees and debunks myths surrounding the bill, exposing political manipulations for false victories while asserting that the core changes are still set to happen.

# Audience

Taxpayers, Political Activists

# On-the-ground actions from transcript

- Contact your representatives to stay informed and hold them accountable (exemplified)
- Stay updated on political developments related to IRS funding and hiring (suggested)

# Whats missing in summary

Detailed analysis of the potential implications of increased IRS staffing on tax enforcement and compliance.

# Tags

#IRS #Taxation #PoliticalManipulation #GovernmentFunding #Accountability