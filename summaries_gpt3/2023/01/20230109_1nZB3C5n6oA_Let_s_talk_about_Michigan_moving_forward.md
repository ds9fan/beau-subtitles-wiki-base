# Bits

Beau says:

- Michigan Attorney General Dana Nessel reopens an investigation into individuals involved in the electors' plot after a year of inaction.
- Nessel's decision to proceed with charges is met with criticism from the GOP, who label it a political stunt.
- Despite GOP claims, the Attorney General's office indicates a swift move towards charging decisions.
- Nessel asserts there is clear evidence supporting charges against the false electors.
- State-level investigations, like the one in Michigan, showcase frustration with the slow pace of federal investigations.
- The Attorney General's office signals a non-political intent and a rapid progression towards charges.
- Michigan has laws addressing the behavior in question, indicating a quick movement towards legal action.
- The GOP's dismissive attitude may backfire as the investigation progresses swiftly.
- Nessel's actions challenge the narrative of a broken justice system by demonstrating proactive state-level prosecution.
- State attorneys general are growing impatient with the federal government's pace, leading to independent investigations and prosecutions.

# Quotes

- "There is clear evidence to support charges against those 16 false electors."
- "I don't think that this is going to be a political show."
- "There are a whole lot of people saying that the justice system is broken."

# Oneliner

Michigan Attorney General Dana Nessel reopens an investigation into the electors' plot, signaling swift legal action against those involved, challenging the narrative of a broken justice system.

# Audience

Legal activists and concerned citizens

# On-the-ground actions from transcript

- Contact local representatives to advocate for efficient and fair legal processes (implied)
- Support efforts by state-level attorneys general pushing for timely investigations and prosecutions (implied)

# Whats missing in summary

Insights on the potential implications of state-level prosecutions and their impact on the justice system.

# Tags

#Michigan #AttorneyGeneral #StateInvestigations #JusticeSystem #LegalAction