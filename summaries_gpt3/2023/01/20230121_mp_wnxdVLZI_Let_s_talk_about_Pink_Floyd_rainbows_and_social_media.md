# Bits

Beau says:

- Pink Floyd changed their social media profile pictures to commemorate the 50th anniversary of Dark Side of the Moon, featuring a design related to the album's iconic cover.
- Some individuals reacted negatively to the rainbow element in the profile picture, questioning its relevance and accusing Pink Floyd of being "woke."
- The album cover of Dark Side of the Moon features light hitting a prism, creating a rainbow, unrelated to the current association with LGBTQ+ pride.
- Beau notes Pink Floyd's history of being politically engaged and mentions the album's 1973 release date, five years before the rainbow flag became a symbol of Pride.
- The backlash against the rainbow in Pink Floyd's profile picture stems from a desire for "equal representation," but Beau argues that default representation already favors the majority.
- Beau criticizes the insecurity and anger fueling such comments, suggesting they are manipulated by those in power to maintain control through fear and division.
- Authoritarians historically use scapegoating and fear-mongering to control populations, directing anger towards specific groups to manipulate behavior.
- Beau warns against falling into a pattern of blaming and fearing others, stressing the importance of breaking free from manipulation to avoid generational consequences.
- Not addressing the manipulation and fear tactics perpetuated by certain groups could lead to a perpetuation of ignorance and timidity among future generations, creating an underclass.
- Beau urges listeners to resist being controlled by divisive narratives and avoid becoming complicit in perpetuating harmful systems.

# Quotes

- "You're making yourself look stupid. What is that, Pink Floyd? What a disgrace. From this moment, I don't listen to this band."
- "If you don't stop letting these people control your every thought by giving you somebody to blame, you're gonna end up another brick in the wall."

# Oneliner

Pink Floyd's social media tribute sparks backlash, revealing deeper fears of manipulation and division rooted in societal power dynamics.

# Audience

Social Media Users

# On-the-ground actions from transcript

- Resist manipulation and fear tactics by critically analyzing information and narratives spread on social media (implied).
- Foster understanding and empathy towards diverse perspectives to combat divisive narratives (implied).

# Whats missing in summary

The full transcript provides a comprehensive examination of how societal power dynamics, manipulation, fear, and division intersect in responses to seemingly innocuous events like Pink Floyd's social media tribute.

# Tags

#PinkFloyd #SocialMedia #Manipulation #Fear #Division