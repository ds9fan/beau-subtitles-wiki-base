# Bits

Beau says:

- The Department of Justice declined to provide the House Judiciary Committee with information on ongoing investigations, citing long-standing policy. 
- The DOJ stated that they will not confirm or deny the existence of pending investigations or provide non-public information in response to congressional requests. 
- This decision means that upcoming House Judiciary hearings may lack substance, as many topics are related to active investigations. 
- The separation of powers between the executive branch, where the DOJ resides, and Congress is emphasized as the reason behind this practice.
- Members of the House Judiciary Committee may face challenges in fulfilling their claims and promises made on social media due to the lack of progress in investigations.
- The DOJ's stance prevents the committee from obtaining non-public information about ongoing criminal investigations.
- Concerns raised about this issue are deemed unrealistic because DOJ policy restricts most of what people are worried about.
- Beau was criticized for not discussing this topic earlier, but he explains that the DOJ's policy makes it unlikely for the information sought by the committee to be provided.

# Quotes

- "The Department of Justice does not comment on active investigations when it comes to congressional requests."
- "It's not a thing where it's the Democrats stonewalling the Republican Party."
- "Concerns expressed about ongoing investigations are unrealistic because department policy prohibits 99% of what people are worried about."

# Oneliner

The Department of Justice's refusal to disclose ongoing investigations to the House Judiciary Committee may hinder the effectiveness of future hearings, rooted in a long-standing policy of non-disclosure.

# Audience

Legislative aides, policymakers

# On-the-ground actions from transcript

- Contact your representatives to advocate for transparency and accountability in government practices. (implied)

# Whats missing in summary

The nuances and detailed explanations provided by Beau in the full transcript. 

# Tags

#DepartmentOfJustice #HouseJudiciaryCommittee #OngoingInvestigations #SeparationOfPowers #Accountability