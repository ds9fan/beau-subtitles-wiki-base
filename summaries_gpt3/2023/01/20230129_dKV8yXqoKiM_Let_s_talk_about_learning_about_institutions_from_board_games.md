# Bits

Beau says:

- A woman in her twenties shares a tradition of playing board games with her parents every Tuesday night, but it has recently become difficult for her to attend.
- The woman's parents, who recently retired, have become consumed by watching Fox News, leading to negative and hateful behavior during their game nights.
- An incident occurred where the woman's parents reacted negatively to learning that her black friend received a full scholarship, assuming it was solely based on her skin color.
- Despite the woman's efforts to explain her friend's achievements, her parents refused to believe her, leading to a heated argument.
- The woman is struggling with the decision to continue attending these game nights due to the toxicity and hate that has tainted the once enjoyable tradition.
- Beau suggests introducing new rules for Monopoly that simulate systemic disadvantages to help the woman's parents understand the concept of generational wealth.
- He acknowledges the difficulty of the situation and the common issue of Fox News influencing destructive behavior in families.
- Beau encourages the woman to try educating her parents using the Monopoly analogy or by showing them his video to help them understand the impact of their behavior on her.

# Quotes

- "You did it fine in the message."
- "Connect all of the dots for them."
- "If you want to make the effort and try to educate them, go this route."
- "It's put you in a situation where this is how you feel."
- "Realistically though, from what you've described, you're going to end up going to dinner."

# Oneliner

A woman struggles with attending weekly game nights with her parents due to their consumption of Fox News leading to hateful behavior, prompting Beau to suggest using Monopoly to explain generational wealth.

# Audience

Children of radicalized parents

# On-the-ground actions from transcript

- Show the video to parents and explain the impact of their behavior (suggested)
- Introduce new Monopoly rules to simulate systemic disadvantages (suggested)

# Whats missing in summary

The full transcript provides additional details on the woman's struggle with her parents' behavior, offering a creative solution through a Monopoly analogy to address generational wealth issues and systemic disadvantages. 

# Tags

#FamilyConflict #GenerationalWealth #Radicalization #BoardGames #FoxNews