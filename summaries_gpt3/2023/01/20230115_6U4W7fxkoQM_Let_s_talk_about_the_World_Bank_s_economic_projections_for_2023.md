# Bits

Beau says:

- Overview of the projections for the global economy in 2023 by the World Bank.
- Global economy expected to expand by 1.7%, barely dodging recession.
- Lowest projections in 20-30 years, except for the 2008 financial crisis and the worst of the 2020 global pandemic.
- The US economy also expected to barely dodge recession with a growth projection of around 0.5%.
- Potential threats to the economy include a resurgence of global public health issues impacting supply chains and Russia's invasion of Ukraine.
- If supply chains are affected, it could erase the projected growth in the US.
- Russia's victory in Ukraine could lead to economic consequences, such as increased costs, especially for food.
- Despite potential risks, current projections indicate that the US and global economy will avoid recession for now.
- Uncertainty remains due to various unpredictable factors.
- The projections are based on the assumption that things continue as they are now.

# Quotes

- "The global economy will expand by 1.7%, that's really low."
- "The US will grow by roughly half a percent, that's low."
- "If Russia wins, your pocketbook, your checking account, it takes a hit."
- "Y'all have a good day."

# Oneliner

Beau provides an overview of the World Bank's projections for the global economy in 2023, showing minimal growth but potential risks ahead.

# Audience

Economic analysts, policymakers

# On-the-ground actions from transcript

- Monitor global public health issues and their impact on supply chains (implied)
- Stay informed about the situation between Russia and Ukraine (implied)

# Whats missing in summary

Insights on potential strategies for mitigating economic risks and preparing for possible downturns.

# Tags

#WorldBank #Economy #GlobalOutlook #Projections #RiskFactors