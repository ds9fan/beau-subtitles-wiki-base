# Bits

Beau says:

- Elvis faced a significant backlash when he first emerged, with demonstrations and laws attempting to ban his performances.
- The backlash against Elvis was due to his provocative dance moves and challenging of societal roles, particularly as a white Southern man.
- People were upset not because Elvis may have been profiting off black music, but because he was introducing it to white kids, challenging existing roles.
- Despite the backlash, society did not collapse, and instead evolved and moved forward.
- The opposition to Elvis was rooted in perception, tradition, and resistance to change.
- Beau questions if there are modern performers challenging societal roles, and warns against standing against them based on outdated traditions.
- He suggests that those opposing current performers may be ridiculed in the future, just like those who protested Elvis.
- Beau ends with a message inspired by Elvis and Jesus: "Don't be cruel."

# Quotes

- "Elvis, Elvis, leave me be. Keep that pelvis far from me."
- "Your concern is based on tradition. Your concern is based on peer pressure from dead people."
- "At some point in the future, there will probably be one of those people on a stamp."
- "Don't be cruel."
- "Y'all have a good day."

# Oneliner

Elvis faced backlash for challenging societal roles, warning against opposing modern performers based on tradition.

# Audience

Cultural critics, music enthusiasts

# On-the-ground actions from transcript

- Support and appreciate modern performers who challenge societal norms (exemplified)
- Embrace change and evolution in society (exemplified)

# Whats missing in summary

The full transcript delves into the historical backlash against Elvis and draws parallels with modern-day resistance to change and challenging of traditional roles.

# Tags

#Elvis #SocietalChange #CulturalEvolution #ChallengingNorms #MusicHistory