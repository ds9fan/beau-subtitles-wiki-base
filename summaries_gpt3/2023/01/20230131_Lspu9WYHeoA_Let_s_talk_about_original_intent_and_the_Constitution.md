# Bits

Beau says:

- Originalists push for following the original intent of the Constitution while admitting they don't know what the original intent was, creating a paradox.
- Some believe the Constitution can't be changed, but it has amendments, which are changes themselves.
- The Constitution is not set in stone and was never intended to be unchangeable.
- The US Constitution has seven articles, with Article 5 covering amendments, indicating the founders' intent for it to be changed.
- Article 5 gives clues on how frequently and quickly changes were expected.
- Certain parts of the Constitution were protected from changes only until 1808, showing the expectation for frequent changes.
- People believing the Constitution shouldn't be changed have been misled to maintain the status quo.
- The founders understood that society and thoughts change over time, necessitating a mechanism to update the Constitution.
- Removing the ability to change the Constitution removes its beauty and original intent.
- The machinery for change in the Constitution is one of its most revolutionary aspects.

# Quotes

- "The Constitution is not set in stone and was never intended to be."
- "They expected it to be changed pretty often."
- "One of the most revolutionary parts of the Constitution is the part that those people who call themselves patriots, call themselves constitutionalists, are trying to make sure you forget."
- "When you remove that section, the beauty of the document disappears."
- "The machinery for change in the Constitution is one of its most revolutionary aspects."

# Oneliner

Originalists paradoxically push for the original intent of the Constitution while not knowing it, failing to recognize its revolutionary machinery for change.

# Audience

Constitution advocates

# On-the-ground actions from transcript

- Educate others on the original intent and purpose of the US Constitution (implied)
- Advocate for the importance of being able to change the Constitution when necessary (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of why the US Constitution was designed to be changed and adapted over time based on the founders' intentions. Watching the full transcript can provide a deeper understanding of the importance of flexibility in constitutional interpretation.

# Tags

#USConstitution #OriginalIntent #Amendments #Change #Founders