# Bits

Beau says:

- M&Ms updated their cartoon candies, sparking controversy like Tucker Carlson upset about the candy not being "hot" anymore.
- M&Ms made moves to be more inclusive, leading to backlash from the right due to their support for causes considered left-leaning.
- A recent campaign focused on women's charities upset conservative thought leaders, resulting in a known alpha male figure rejecting the feminine M&Ms.
- The company announced an indefinite pause on the spokes candies and introduced Maya Rudolph as a spokesperson, seemingly using satire.
- Conservatives misunderstood the indefinite pause, declaring victory over the candies, despite indications that they will return, possibly for the Super Bowl.
- The situation led to confusion and backlash, with conservatives struggling to understand the satire behind M&Ms' actions.
- Beau suggests that the war against M&Ms may be unwinnable and predicts further conservative backlash that could ironically boost M&Ms sales.
- He humorously suggests targeting Hershey's next due to the pronouns "her" and "she," poking fun at conservative outrage.

# Quotes

- "The war against M&Ms may be unwinnable."
- "Pronouns are bad. Y'all should go after them."
- "They're trying to train your kids early and stuff."

# Oneliner

M&Ms' inclusive moves spark conservative backlash, leading to misunderstandings and declarations of victory, but the war against the candies may be unwinnable.

# Audience

Social media users

# On-the-ground actions from transcript

- Support women's charities (implied)
- Stay informed on corporate actions and responses (suggested)

# Whats missing in summary

The full transcript provides more context and humor regarding the M&M controversy, enhancing understanding and entertainment.

# Tags

#M&Ms #ConservativeBacklash #Inclusivity #Satire #CorporateActions