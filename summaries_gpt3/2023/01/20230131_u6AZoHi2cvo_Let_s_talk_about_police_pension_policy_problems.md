# Bits

Beau says:

- Qualified immunity is not what many people think it is; it's a civil protection, not a shield against criminal charges.
- There are calls to alter or end qualified immunity, but it's not a cure-all solution.
- The slogan "end qualified immunity and take the payments out of the police pension fund" is not a policy suggestion but a statement to show accountability.
- Shifting payments to the police pension fund could create unintended consequences by providing a financial incentive for officers to cover up misconduct.
- Changing statutes for criminal liability may be more effective in changing the culture within law enforcement than altering the pension fund system.
- Confusing slogans with solutions can have serious consequences, especially when lives are at stake.

# Quotes

- "Slogans are not solutions."
- "We can't confuse slogans with solutions, especially with something like this."
- "Lives are literally on the line."
- "It's a great slogan and it's a great way to draw attention to the fact that officers aren't held accountable enough."
- "Anyway, it's just a thought."

# Oneliner

Slogans like "end qualified immunity and take payments from the police pension fund" draw attention to accountability but may have unintended consequences, confusing slogans with solutions when lives are at stake.

# Audience

Advocates, policymakers, activists

# On-the-ground actions from transcript

- Advocate for altering statutes for criminal liability to incentivize reporting misconduct (implied)

# Whats missing in summary

The full transcript provides a comprehensive breakdown of the slogan "end qualified immunity and take payments from the police pension fund" and its potential implications, urging caution in confusing slogans with actual policy solutions.

# Tags

#QualifiedImmunity #PoliceReform #Accountability #PolicySlogans #CommunityPolicing