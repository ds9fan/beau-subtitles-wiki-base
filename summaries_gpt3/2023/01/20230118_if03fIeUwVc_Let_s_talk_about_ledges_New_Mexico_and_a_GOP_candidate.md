# Bits

Beau says:

- Analyzing recent events in New Mexico involving drive-by shootings at homes of Democratic Party members.
- Law enforcement linking ballistics evidence to a Republican candidate, Solomon Pina, in the attacks.
- Pina believed baseless election claims and allegedly paid people to carry out the attacks.
- Reports suggest Pina may be advised to seek a plea deal due to incriminating evidence.
- The media focusing on Pina's landslide loss in the election and his motivations.
- One in four voters supported Pina, raising concerns about electorate judgment.
- Beau criticizes the Republican Party's shift and warns of increasing political violence.
- Mention of tweets indicating support for Pina and Trump, hinting at ongoing divisive rhetoric.
- Beau stresses the importance of countering divisive rhetoric and not underestimating its impact.
- The need for continued vigilance to prevent further violence and address underlying issues.

# Quotes

- "In one of them, rounds wound up in the bedroom of a child."
- "It is becoming more common. It is happening with greater frequency."
- "Ignoring his statements that later end up as Republican policy."
- "The echo chambers that brought about what occurred on the 6th, they still exist."
- "We're headed to our own troubles."

# Oneliner

Analyzing political violence in New Mexico, linking a Republican candidate to drive-by shootings, Beau warns of increasing frequency and the dangers of divisive rhetoric.

# Audience

Community members, voters

# On-the-ground actions from transcript

- Contact local representatives or party officials to express concerns about political violence (suggested)
- Attend community forums or events focused on promoting unity and understanding (implied)

# Whats missing in summary

The full transcript provides a comprehensive analysis of recent political violence, urging vigilance and action to address the underlying issues fueling such incidents.

# Tags

#NewMexico #PoliticalViolence #RepublicanParty #DivisiveRhetoric #CommunitySafety