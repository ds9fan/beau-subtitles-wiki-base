# Bits

Beau says:

- New York has recently allowed a unique form of composting where individuals can be composted after they pass away.
- The process involves being placed in a still box with organic material to help compost the body, taking about a month.
- Family members then collect the resulting dirt in an urn-like container.
- This new form of composting has faced backlash from certain religious communities and traditionalists.
- Despite the initial resistance, Beau predicts that rural people, particularly those who value land passed down through generations, may find this method appealing.
- As more states adopt this disposal method, there may be increased opposition from religious groups and cemeteries, as it challenges established practices and business interests.
- Beau believes that ultimately, the demand for this composting method may prevail due to its appeal to those seeking a return to older burial practices.
- He envisions a broad demographic, including rural and conservative individuals, embracing this alternative to traditional burials.
- Beau points out the potential conflict between personal choices after death and the interests of big business and government.
- He concludes by suggesting that this composting method could spark debates over individual rights even posthumously.

# Quotes

- "I mean, I wouldn't mind literally becoming part of the ranch."
- "And this is definitely going to cut into some business interests, because it's fundamentally altering the way things are done."
- "But make no mistake about it, the big business and government will argue over what you can do with yourself, even once you're gone."

# Oneliner

New York's new composting law sparks backlash and potential shifts in burial traditions, challenging business interests and individual rights posthumously.

# Audience

Environment enthusiasts, rural communities

# On-the-ground actions from transcript

- Advocate for eco-friendly burial options in your community (exemplified)
- Research and support legislation promoting alternative burial practices (exemplified)

# Whats missing in summary

The full transcript provides a deeper exploration of the societal implications and potential conflicts arising from the introduction of composting as a burial alternative.