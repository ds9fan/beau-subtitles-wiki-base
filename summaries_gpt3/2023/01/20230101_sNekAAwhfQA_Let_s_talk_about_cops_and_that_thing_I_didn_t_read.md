# Bits

Beau says:

- Beau received a message questioning why he didn't read a specific piece of information on a police website related to high crime rates in the South, particularly affecting poor and Black individuals.
- The information not read by Beau revealed that all the 10 most dangerous counties listed had an African American plurality or majority.
- Beau questions the causal link between skin tone and high crime rates, pointing out the lack of evidence and providing examples like all locations having a Walmart, majority with brown eyes, and a river – none of which directly lead to crime.
- Institutional racism in policing is brought to light through the inclusion of irrelevant information that perpetuates harmful stereotypes.
- Beau stresses that treating demographic identifiers as causal factors without evidence is a form of prevalent racism within law enforcement, leading to dangerous assumptions and actions.
- The danger lies in perpetuating the idea that Black people are more dangerous, ultimately contributing to excessive force by law enforcement officers.
- The misinformation included in crime statistics can lead to viewing skin tone as a threat, despite it not correlating with an increased propensity for violent crime.

# Quotes

- "Skin tone does not increase somebody's propensity to engage in violent crime."
- "Treating demographic identifiers as causal things without information to back it up is a type of racism."
- "Institutional racism in policing continues to persist through the inclusion of irrelevant information that perpetuates harmful stereotypes."

# Oneliner

Beau received criticism for not discussing high crime rates affecting poor and Black individuals, pointing out the lack of evidence linking skin tone to crime and the dangers of perpetuating harmful stereotypes.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Challenge institutional racism in policing through advocacy and awareness (implied)
- Educate others on the dangers of perpetuating harmful stereotypes based on demographic identifiers (implied)

# What's missing in summary

The full transcript provides a detailed insight into the prevalence of institutional racism in law enforcement and the dangers of perpetuating harmful stereotypes based on skin tone in crime statistics.