# Bits

Beau says:

- Exploring proposed laws at the state level aimed at detrumpification, preventing those involved in the Sixth from holding public office or positions of public trust.
- Laws are surfacing in places like Connecticut, New York, and Virginia, designed around words like insurrection, rebellion, and sedition, which aren't commonly charged.
- Concerns about the vague definitions within the laws, particularly regarding crimes related to the specified offenses and how they could be misused in the future.
- Beau expresses a lack of opposition to the idea behind the laws but is worried about their potential selective application and unforeseen consequences.
- Acknowledging the uniqueness of the events of the Sixth and the need for specific legislation that doesn't overly restrict future employment opportunities based on vague criteria.
- Emphasizing the importance of ensuring that the laws are well-defined to prevent potential misuse or unjust applications down the line.

# Quotes

- "The laws appear to be structured with the intent of detrumpification, of making sure that those people who participated in the Sixth can't hold public office."
- "We have to acknowledge that what happened on the 6th was special. It was different."
- "Creating a statute that basically bars them from future employment is a little much when it's this vague."
- "While I like the idea in theory, the application seems like it needs a little work to me."
- "Y'all have a good day."

# Oneliner

Beau examines state-level laws aimed at detrumpification but raises concerns about their vague definitions and potential misuse, stressing the need for more specific legislation.

# Audience

Legislative observers

# On-the-ground actions from transcript

- Contact your state representatives to express concerns about the vague definitions in proposed laws (suggested).
- Advocate for clear and specific legislation to prevent potential misuse and unjust applications (implied).

# Whats missing in summary

Deeper insights into the potential consequences of vaguely defined laws and how they could impact individuals in the future.

# Tags

#StateLaws #Detrumpification #Legislation #VagueDefinitions #PotentialMisuse