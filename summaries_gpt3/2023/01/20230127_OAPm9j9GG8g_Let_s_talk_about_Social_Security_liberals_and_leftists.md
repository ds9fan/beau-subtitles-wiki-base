# Bits

Beau says:

- Republicans will inevitably target Social Security, wanting to make changes like cutting it or raising the age, as part of a long process to chip away at it.
- Liberals and the left must be unified when defending Social Security, despite potential disagreements over phrasing and motivations.
- Leftists may be upset with liberals not for what they want to do, but for why they want to do it.
- Liberals tend to argue that Social Security is not welfare, but something they paid into and are entitled to, while leftists stress that everyone deserves a decent life, regardless of contributions.
- Leftists aim to prevent the conditioning of viewing a difference between welfare and earned benefits, advocating for the idea that everybody deserves a decent life.
- Social Security is not solely for those who worked their entire lives; it serves broader purposes, reinforcing the notion that everybody is entitled to a decent life.
- Leftists urge for unity in defending all social safety nets and promoting the concept that everyone deserves a decent life.
- Advocates should approach the defense of Social Security positively, focusing on the idea that regardless of circumstances, everyone is entitled to a decent life.
- Referencing the Constitution's goal to "promote the general welfare" supports the argument for protecting Social Security as a vital safety net.
- Collaboration between liberals and leftists is vital in facing potential Republican attacks on Social Security.

# Quotes

- "Leftists will be upset with liberals."
- "Everybody deserves a decent life."
- "Everybody's entitled to that."
- "Say yes, and everybody, regardless of situation, is entitled to a decent life."
- "This is a moment where liberals and leftists, you have to work together."

# Oneliner

Republicans target Social Security, urging liberals and leftists to unite in defending its importance and promoting that everyone deserves a decent life.

# Audience

Advocates, activists, citizens

# On-the-ground actions from transcript

- Unite liberals and leftists in defending Social Security (implied)
- Advocate for the idea that everyone deserves a decent life (exemplified)

# Whats missing in summary

In-depth examples and analysis on the potential consequences of failing to defend Social Security fully.

# Tags

#SocialSecurity #Unity #Liberals #Leftists #GeneralWelfare