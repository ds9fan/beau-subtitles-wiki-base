# Bits

Beau says:

- Shares insights from a Census Bureau survey on natural disaster displacement.
- 3.3 million Americans were displaced last year, with one out of six never returning home.
- The survey provides perspective on scope and time regarding natural disasters.
- Points out that these numbers are expected to increase with climate change.
- Indiana, Maine, North Dakota, Ohio, and Oklahoma are the least likely to experience displacement, while Florida is the most likely.
- Emphasizes the importance of emergency preparedness, especially considering the increasing frequency of natural disasters.
- Mentions the vastness and diversity of the United States in accepting displaced people.
- Encourages individuals to take steps to prepare for natural disasters.
- Raises awareness about the commonality of natural disaster displacement.
- Urges families to create emergency kits and plans.

# Quotes

- "At some point in your life, you will be displaced by a natural disaster."
- "The most important takeaway here is for those who need some information to get their family members to put together a kit or a plan."
- "Y'all have a good day."

# Oneliner

Beau shares insights from a Census Bureau survey on natural disaster displacement, urging individuals to prepare as such occurrences are becoming more common.

# Audience

Community members

# On-the-ground actions from transcript

- Prepare an emergency kit for your family members to deal with natural disasters (suggested).
- Create a plan to address potential natural disaster situations in your area (suggested).

# Whats missing in summary

The full transcript provides detailed statistics and insights on natural disaster displacement, reinforcing the importance of preparedness in the face of increasing occurrences.

# Tags

#NaturalDisasters #EmergencyPreparedness #ClimateChange #CommunityAction #Awareness