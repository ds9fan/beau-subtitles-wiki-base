# Bits

Beau says:

- Explains the Inflation Reduction Act, pointing out significant wins.
- Criticizes Biden administration for not adequately promoting the biggest win in healthcare.
- Emphasizes that the major healthcare win involves Medicare negotiating with pharmaceutical companies.
- Compares Medicare negotiation to dealing with Vito Corleone from "The Godfather."
- Describes the process where Health and Human Services sets prices, and pharmaceutical companies can counteroffer but may face hefty taxes if they don't comply.
- Addresses concerns about pharmaceutical companies shifting costs onto others.
- Analyzes the strategy behind changing societal thought regarding Medicare for all.
- Comments on the lack of legislative priority for expansive healthcare despite widespread support.
- Speculates on the potential shift in public perception if pharmaceutical companies raise prices.
- Advises not to negotiate with "Dark Brandon" and makes a reference to 4D chess in politics.

# Quotes

- "But to me, that's not the biggest."
- "I mean, yeah, it's a negotiation, the same way you negotiate with Vito Corleone."
- "Oh no, don't throw me into the briar patch."
- "To change society, you have to change thought first, not the law."
- "This is what it actually looks like."

# Oneliner

Beau explains the key healthcare win in the Inflation Reduction Act involving Medicare's negotiation power with pharmaceutical companies, likening it to dealing with organized crime.

# Audience

Healthcare advocates

# On-the-ground actions from transcript

- Support legislative efforts for Medicare negotiation with pharmaceutical companies (implied)
- Advocate for healthcare reform and increased support for Medicare for all (implied)

# Whats missing in summary

Detailed analysis of the potential impact of Medicare negotiation on healthcare costs and access.

# Tags

#Healthcare #Medicare #Negotiation #PharmaceuticalCompanies #InflationReductionAct