# Bits

Beau says:

- Half the country is facing rough weather, be prepared for tornadoes.
- Areas in California experiencing flooding should prepare for more with another atmospheric river coming.
- Concerns about flooding around the Russian River are high.
- Avoid going to your attic during flooding as it's not a safe place to seek shelter.
- Have a plan in place and know what to do if your area gets flooded.
- If you must go to the attic, ensure you have a way out by creating an exit on the roof.
- Don't be someone's nightmare in the aftermath of a disaster, make sure you have a way out.
- People often end up in the attic due to someone in the home unable to achieve an alternative.
- Ensure you have necessary supplies like batteries and water ready for severe weather conditions.
- Utilize advanced weather warnings to be prepared and not caught off guard.

# Quotes

- "Your attic isn't a good place to go."
- "Don't be somebody's nightmares for the next five years."
- "Make sure you have a way out."

# Oneliner

Beau stresses the importance of being prepared for severe weather conditions, advising against seeking shelter in the attic and ensuring you have a way out to avoid being trapped.

# Audience

Community members in areas prone to severe weather.

# On-the-ground actions from transcript

- Prepare an emergency kit with batteries, water, and essentials in case of severe weather (implied).
- Familiarize yourself with your area's evacuation routes and shelters (implied).
- Stay informed about weather updates and warnings to take necessary precautions (implied).

# Whats missing in summary

Detailed instructions on creating an exit from the attic in case of flooding.

# Tags

#WeatherPreparedness #EmergencyPreparedness #FloodSafety #SevereWeather #CommunitySafety