# Bits

Beau says:

- There are impersonators on YouTube leaving comments claiming you won a prize or asking for personal information and money.
- Impersonators try to gather personal information and trick people into sending them money.
- If a YouTuber has something special to share, they will make a video about it on their platform.
- Impersonators create fake accounts that look similar to the real channel but have small differences in the name and profile picture.
- Always verify the profile sending you a message by checking if it redirects you to the actual channel.
- Pay attention to web addresses in emails or messages to detect scams. 
- Most YouTubers do not direct people off-platform or ask for personal information.
- Scams are becoming more common as the internet expands, targeting a wider audience.
- Beau expected older individuals to fall for scams but found that younger people were also targeted.
- Use links in the about section to reach out to YouTubers on their official social media accounts.

# Quotes

- "If a person on YouTube has some kind of special deal or something like that or some kind of information or whatever, they're going to make a video about it."
- "Most of us, we don't even allow that. It will direct you to Patreon or something like that if you're wanting to support the channel."
- "So just kind of be on guard."
- "Y'all have a good day."
- "Thanks for watching!"

# Oneliner

Be on guard against YouTube impersonators asking for personal information and money; verify profiles, avoid off-platform interactions, and use official channels to reach out.

# Audience

YouTube Viewers

# On-the-ground actions from transcript

- Verify the profile of anyone reaching out to you on YouTube (suggested)
- Use official social media links to contact YouTubers (implied)

# Whats missing in summary

The full transcript provides detailed insights into how YouTube impersonators operate and offers practical tips to avoid falling for scams online.

# Tags

#YouTube #Scams #Impersonation #OnlineSafety #FactChecking