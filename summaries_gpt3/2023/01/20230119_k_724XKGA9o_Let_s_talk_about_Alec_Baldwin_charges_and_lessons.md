# Bits

Beau says:

- Providing an overview of the Alec Baldwin case, pointing out that both Baldwin and the armorer from the set of Rust are facing charges.
- Contrasting the differences in how involuntary manslaughter charges are applied in New Mexico versus Georgia, leading to different perceptions.
- Explaining that involuntary manslaughter charges in New Mexico carry a maximum sentence of 18 months, while in Georgia, it could result in a decade of imprisonment.
- Mentioning that the prosecution believes Baldwin and the armorer didn't exercise enough caution during a lawful act, resulting in a death, which will be decided by a jury.
- Not forming an opinion on guilt or innocence until more details emerge and leaving it up to the jury to decide.
- Anticipating that the gun crowd will heavily criticize Baldwin and stress the importance of negligence over accidents in such incidents.
- Emphasizing that unintentional shootings resulting in death are unfortunately common in the United States due to negligence and lack of caution.
- Calling for the gun crowd to take proactive steps in teaching gun safety to prevent such incidents from occurring.
- Expressing doubt in the gun crowd's willingness to take action but hoping for a change in response to such incidents.
- Concluding with a thought on the dangers of assuming a gun is unloaded, urging caution and awareness.

# Quotes

- "There are no accidents, only negligence."
- "The most dangerous gun in the world is the one that you're pretty sure is unloaded."

# Oneliner

Beau clarifies differences in involuntary manslaughter charges, raises concerns about negligence in unintentional shootings, and urges the gun crowd to take proactive steps in teaching gun safety.

# Audience

Gun owners, activists, advocates

# On-the-ground actions from transcript

- Advocate for proactive gun safety education within your community (implied)
- Take steps to ensure firearms are handled with caution and proper safety measures (implied)

# Whats missing in summary

Deeper insights into the implications of negligence in firearm handling and the importance of proactive gun safety education.

# Tags

#AlecBaldwin #InvoluntaryManslaughter #GunSafety #Negligence #CommunityPolicing