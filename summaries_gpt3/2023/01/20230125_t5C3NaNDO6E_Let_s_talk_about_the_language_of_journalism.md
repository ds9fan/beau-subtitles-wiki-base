# Bits

Beau says:

- Explains why the word "alleged" is frequently used in journalism to protect outlets from being sued.
- Mentions that the absence of certain words like "alleged" can signify commentary rather than straight reporting.
- Shares a personal experience of using the term "alleged" in reporting and the editor's explanation on its necessity.
- Talks about passive language in journalism, such as using "death of" instead of "murder of," until legally confirmed.
- Recounts a friend getting criticized for using "death of" in reporting due to legal and journalistic considerations.
- Advises to think from a legal perspective when critiquing journalists' choice of terminology like "murder" or "killed."
- Mentions a case in Memphis that will likely stir strong emotions once the footage is released, warning viewers to brace themselves.

# Quotes

- "If somewhere else in that passage of that article, whatever, blame seems to be implied, oh, that's alleged."
- "That's why you hear that word so much."
- "Until they're convicted, they're an alleged armed robber."
- "Be ready for it. When the footage comes out, just kind of brace yourself."
- "The city government up there is already trying to distance itself from the media."

# Oneliner

The importance of journalistic terminology like "alleged" and the impact of legal perspective on reporting, with a warning about an upcoming disturbing footage.

# Audience

Journalists, Reporters, Activists

# On-the-ground actions from transcript

- Brace yourself for the disturbing footage release (implied)

# Whats missing in summary

Importance of understanding journalistic terminology and legal perspectives in reporting.

# Tags

#Journalism #Media #LegalPerspective #Ethics #Memphis