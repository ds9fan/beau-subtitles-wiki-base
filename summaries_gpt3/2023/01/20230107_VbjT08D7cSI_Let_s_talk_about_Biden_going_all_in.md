# Bits

Beau says:

- The Biden administration has launched the "All in Federal Strategies for Reducing and Ending Homelessness" program with an ambitious goal of reducing homelessness by 25% in the next two years.
- The program involves input from people who have experienced homelessness, providers, advocates, developers, and multiple federal agencies.
- Lessons learned from successful initiatives like tackling veteran homelessness are being applied to this program.
- The approach includes a focus on housing first and engaging state and local governments to set equally ambitious goals.
- Web-based seminars for this program start on January 10th, indicating a fast-moving initiative.
- While achieving a 25% reduction in homelessness in two years seems challenging, setting such an ambitious goal is more impactful than settling for minimal targets.
- Even if the program falls slightly short of the 25% reduction, any progress will be a significant win.
- The timeline for achieving these goals is tight, with 2025 approaching quickly.
- The resources and planning put behind this initiative make even a partial success beneficial.
- The outcome of this program will be interesting to observe given its ambitious objectives and comprehensive planning.

# Quotes

- "The Biden administration has launched the 'All in Federal Strategies for Reducing and Ending Homelessness' program with an ambitious goal of reducing homelessness by 25% in the next two years."
- "Even if the program falls slightly short of the 25% reduction, any progress will be a significant win."
- "The outcome of this program will be interesting to observe given its ambitious objectives and comprehensive planning."

# Oneliner

The Biden administration's ambitious program aims to reduce homelessness by 25% in two years, drawing on past successes and involving various stakeholders and agencies.

# Audience

Advocates, policymakers, communities

# On-the-ground actions from transcript

- Attend the web-based seminars starting on January 10th to learn more about the program and how to get involved (suggested).
- Support local initiatives to reduce homelessness by engaging with state and local governments (implied).

# What's missing in summary

The full transcript provides additional insights into the detailed planning and stakeholder involvement in the program, offering a comprehensive view of the initiative's potential impact.

# Tags

#Homelessness #BidenAdministration #FederalProgram #CommunityAction #AmbitiousGoals