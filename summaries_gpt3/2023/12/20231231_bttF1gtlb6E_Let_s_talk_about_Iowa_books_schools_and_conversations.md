# Bits

Beau says:

- Iowa legislature passed a law curbing school activities, but a federal judge issued a temporary injunction against it.
- The three main components of the law are banning books, banning LGBTQ+ content, and requiring parental notification for pronoun changes.
- The ban on books, including history books and those aiding students, has been halted temporarily.
- The ban on LGBTQ+ content was described as overly broad and is also temporarily halted.
- The parental notification requirement stands because the plaintiffs lacked standing to challenge it.
- The law is seen as overly broad and unlikely to fully stand in its current form.
- Educators have a temporary reprieve, but the situation may change based on the progress of the case.

# Quotes

- "The ban on books, including history books and those aiding students, has been halted temporarily."
- "The law is seen as overly broad and unlikely to fully stand in its current form."
- "Educators have a temporary reprieve, but the situation may change based on the progress of the case."

# Oneliner

Iowa faces a controversial law curbing school activities, with bans on books and LGBTQ+ content temporarily halted, while a parental notification requirement remains standing.

# Audience

Educators, Activists, Community Members

# On-the-ground actions from transcript

- Monitor the progress of the case and advocate for inclusive policies in schools (implied).
- Support educators in navigating the legal challenges and standing up for diverse educational materials (implied).

# Whats missing in summary

Details on the potential impacts of the law on students and the educational environment.

# Tags

#Iowa #Schools #BanningBooks #LGBTQ+ #InclusiveEducation