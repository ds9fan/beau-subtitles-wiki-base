# Bits

Beau says:
- Biden administration compared Trump's rhetoric to Hitler, sparking debate.
- Reference to Godwin's Law: likelihood of Nazi comparisons in online debates.
- Godwin, creator of the law, was reached out to for his opinion.
- Godwin believes Trump intentionally uses inflammatory rhetoric with similarities to Hitler.
- Godwin implies Trump's rhetoric is not accidental, citing specific remarks.
- Emphasizes the importance of not equating Biden and Trump as the same.
- Trump may continue using such rhetoric to garner attention.
- Stresses the non-coincidental nature of Trump's rhetoric.

# Quotes
- "I think it [comparing Trump's rhetoric to Hitler] would be fair to say that Trump knows what he's doing."
- "One of them is your basic status quo centrist American president, which is exactly what everybody said he was going to be. The other is a far-right extreme authoritarian."
- "That rhetoric, it's not an accident. It is not an accident."

# Oneliner
Biden administration compares Trump's rhetoric to Hitler, with Godwin affirming intentional parallels, stressing the importance of not equating them.

# Audience
Online Political Observers

# On-the-ground actions from transcript
- Read the full interview with Godwin for deeper insight (suggested)
- Stay informed about political rhetoric and its implications (implied)

# Whats missing in summary
Importance of remaining vigilant against dangerous rhetoric to safeguard against authoritarian tendencies.

# Tags
#Trump #Biden #Rhetoric #GodwinsLaw #Authoritarianism