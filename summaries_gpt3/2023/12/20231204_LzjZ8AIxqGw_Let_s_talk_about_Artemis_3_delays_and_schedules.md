# Bits

Beau says:

- NASA's Artemis project aims to put somebody back on the moon, with Artemis III being the first crewed craft scheduled for late 2025.
- A recent GAO report indicates a potential delay, with Artemis III likely to occur in early 2027 instead of the initially planned late 2025.
- The main delay is attributed to the Human Landing System (HLS) by SpaceX, moving slower than expected in its development.
- The report suggests that if HLS development follows the average timeline of NASA's major projects, Artemis III will be pushed back to 2027.
- The HLS system didn't meet several key milestones, leading to delays in the Artemis mission.
- Issues also arise with the development of suits by a company called Axiom, with NASA requesting additional emergency life support integration.
- The delay in the Artemis mission means it will take longer before another person steps foot on the moon.

# Quotes

- "Artemis III will be the first crewed craft going where, well, actually somebody has gone before."
- "It looks like it's going to be a wee bit longer before we actually see another person on the moon."

# Oneliner

NASA's Artemis project faces delays due to setbacks in the Human Landing System (HLS) development, potentially pushing the mission to early 2027, prolonging the wait for a return to the moon.

# Audience
Space enthusiasts

# On-the-ground actions from transcript

- Stay updated on NASA's Artemis project progress and advocacy efforts to support timely developments (implied).
- Engage with space-related communities to stay informed and potentially contribute to advancements in space exploration (implied).

# Whats missing in summary

Details on specific HLS milestones and potential solutions to expedite development.

# Tags

#NASA #Artemis #SpaceExploration #MoonMission #HLS