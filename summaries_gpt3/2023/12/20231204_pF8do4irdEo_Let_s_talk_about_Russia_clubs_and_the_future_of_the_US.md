# Bits

Beau says:

- Russia's Supreme Court labeled the LGBTQ+ community as an extremist movement, leading to security services raiding LGBTQ+ establishments in Moscow.
- Putin has long promoted "traditional family values" and passed anti-LGBTQ laws like the gay propaganda law in 2013.
- The quick progression from anti-LGBTQ laws to security crackdowns in Moscow serves as a warning for Americans, as we are about 10 years behind Russia in this regard.
- The targeting of the LGBTQ+ community is often a testing ground for authoritarian regimes that may eventually impact other groups.
- Americans cheering on authoritarian actions are unknowingly setting themselves up to be the next target of oppressive measures.

# Quotes

- "When masked, security services are photographing your documents, generally speaking, that's a bad sign for your future."
- "It's not gonna stop with our community. They're just the test group."
- "If you're cheering this on because you are not part of this group, you have to understand that once a government gets power like this, it's really hard to get it back."

# Oneliner

Russia's anti-LGBTQ progression warns Americans of authoritarian measures and the dangerous implications of supporting such actions.

# Audience

Aware citizens

# On-the-ground actions from transcript

- Reach out to LGBTQ+ organizations for support and guidance (suggested)
- Advocate for LGBTQ+ rights and protections in your community (implied)
- Stand in solidarity with marginalized groups facing oppression (implied)

# Whats missing in summary

The emotional impact and urgency of standing against oppressive actions and protecting marginalized communities.

# Tags

#Russia #LGBTQ+ #Authoritarianism #Warning #HumanRights