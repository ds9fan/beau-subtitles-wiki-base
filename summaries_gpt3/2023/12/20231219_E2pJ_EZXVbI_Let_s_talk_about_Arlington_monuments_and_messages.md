# Bits

Beau says:

- Explains the questions around the Reconciliation Monument at Arlington and the history behind Confederate monuments.
- Points out that the monument isn't actually called the Reconciliation Monument and that it's referred to as the Confederate Memorial on the Arlington Cemetery website.
- Talks about the historical context of Confederate monuments being built during periods of civil rights activity with messages of racial oppression.
- Notes that the Confederate Memorial at Arlington was built in the early 1900s with a tone of reconciliation, different from many other Confederate monuments.
- Mentions Congress's involvement in the decision to potentially remove the Confederate Memorial through the NDAA of 2021.
- Addresses the impossibility of completely removing the connection to the Confederacy from Arlington Cemetery due to Confederate soldiers buried there.
- Provides historical context on how Arlington was initially taken during the Civil War for strategic military reasons, not due to Union losses.
- Mentions Mary Custis, who owned Arlington before it was taken by the US Army, and her connection to Robert E. Lee.
- Comments on the current situation where the Department of Defense is trying to remove the Confederate monument, authorized by Congress, with some members now expressing outrage.
- Talks about a temporary restraining order in place while the issue is being sorted out and criticizes Congress for potentially not addressing the situation they initiated.

# Quotes

- "You won't find that name. Click on where it says monuments and look for something called the Reconciliation Monument. You won't find that there."
- "The one at Arlington, it really did have a different purpose. It was built in the early 1900s and it really did have a tone of reconciliation."
- "Congress actually made this happen, to be clear."
- "That is impossible, and more importantly, I think you know it's impossible."
- "I don't think that there's ever going to be a point in time where people do not understand the connection between Arlington National cemetery, the Civil War, or the Confederacy."

# Oneliner

Beau breaks down misconceptions around Confederate monuments at Arlington Cemetery, pointing out the historical context and Congress's role in potential removal.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Contact local historical societies to learn more about the history of Arlington Cemetery (suggested)
- Support community efforts to contextualize historical monuments accurately (exemplified)

# Whats missing in summary

Deeper insights into the historical significance of Arlington Cemetery and the ongoing debate around Confederate monuments.

# Tags

#Arlington #ConfederateMonuments #Congress #History #Community #Removal #ReconciliationMonument