# Bits

Beau says:

- The U.S. released declassified numbers on Russia's losses in Ukraine, revealing high casualty rates.
- Russia lost a significant amount of tanks, IFVs, and APCs during the invasion.
- Russia aims to boost its active duty troops to 1.5 million by recruiting individuals deemed undesirable by Putin.
- The U.S. releasing these numbers is a strategic move to showcase their intelligence accuracy and influence.
- Some U.S. politicians question where the money is going, which is towards supporting Ukraine.
- There is reluctance from certain politicians to support aid due to their far-right ideology alignment with Putin.
- Despite the release of these numbers, it may not sway those opposed to aiding Ukraine.
- The Democratic Party may need to compromise to ensure aid is approved, potentially impacting future elections.
- Those against aid post-number release may be driven by ideology rather than corruption concerns.
- Civilian casualties in conflicts often surpass combatant losses.

# Quotes

- "There are certainly people watching this video who are like I want US foreign policy to fail."
- "They have chosen a far right-wing ideology over US national interest."
- "They have to make the U.S. lose so they can feel better about their Twitter talking points."

# Oneliner

The U.S. released revealing numbers on Russia's losses in Ukraine, but some politicians' reluctance to support aid stems from a far-right ideology alignment with Putin.

# Audience

Politically aware viewers.

# On-the-ground actions from transcript

- Support aid efforts for Ukraine (suggested).
- Advocate for policies that prioritize humanitarian support (implied).

# Whats missing in summary

The full transcript provides a nuanced understanding of the political motivations behind aid support and opposition.

# Tags

#Russia #Ukraine #US #Politics #AidSupport #ForeignPolicy