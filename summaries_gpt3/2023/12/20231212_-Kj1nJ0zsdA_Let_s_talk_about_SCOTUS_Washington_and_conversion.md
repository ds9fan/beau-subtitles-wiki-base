# Bits

Beau says:

- The Supreme Court of the United States did not take up a case regarding Washington State's ban on conversion therapy.
- Washington State banned conversion therapy, a discredited practice aiming to change someone's orientation or gender.
- Lower courts are divided on whether such bans can exist.
- The Supreme Court's decision to not hear the case means the ban stands, but it could have been a nationwide decision.
- The core issue isn't about free speech but whether states can regulate conduct under professional licenses.
- Allowing such speech under professional licenses could weaken state licensing systems.
- Beau believes the Supreme Court should have taken up the case to affirm state regulations.
- The conservative justices likely avoided the case due to the inevitable ruling in favor of state regulation.
- The Supreme Court's decision maintained the ban on conversion therapy but missed an chance for a broader impact.
- Allowing such speech could lead to harmful practices without legal repercussions.

# Quotes

- "The core of this case is whether or not the state has the ability to regulate conduct that occurs under a state issued professional license."
- "It's a win but it was an opportunity for it to be an even bigger win."
- "The Supreme Court's decision to not hear the case means the ban stands, but it could have been a nationwide decision."

# Oneliner

Beau explains why the Supreme Court's decision on Washington's conversion therapy ban is more significant than just a win, revealing the core issue of state regulation over professional conduct.

# Audience

Activists, policymakers

# On-the-ground actions from transcript

- Advocate for policies that protect vulnerable communities from harmful practices (implied)
- Support organizations working to uphold state regulations on professional conduct (implied)

# Whats missing in summary

More context on the potential impact of allowing harmful practices under professional licenses.

# Tags

#SupremeCourt #ConversionTherapy #StateRegulation #LegalJustice #ProfessionalEthics