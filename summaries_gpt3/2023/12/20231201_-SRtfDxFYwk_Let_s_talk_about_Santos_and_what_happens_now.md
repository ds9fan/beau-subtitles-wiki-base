# Bits

Beau says:

- Representative George Santos was expelled from Congress by a relatively decisive vote, shrinking the Republican majority.
- Santos was not convicted of any crime, sparking concerns about setting a new precedent for expelling members without a conviction.
- Despite lacking defenders, Santos faced opposition for his alleged activities, particularly upsetting Republicans by mishandling donors.
- The fear now is that more members could face expulsion for lesser offenses without the need for a conviction.
- The concern about potential future expulsions is shared by many people.
- The allegations against Santos and the knowledge of other Congress members played a significant role in his expulsion.
- This event in Congressional history may end up being overshadowed by other behaviors that have yet to face formal action.
- While currently significant, the expulsion of Santos may diminish in importance over the next few years.
- The situation serves as a reminder of the dynamics and consequences within Congress.

# Quotes

- "This event in Congressional history may end up being overshadowed by other behaviors."
- "The concern now is that there are going to be more attempts to expel people based on less serious behavior."
- "He messed with the money. When he engaged with donors the way he did, that upset a whole lot of Republicans."
- "The concern about potential future expulsions is shared by many people."
- "I think this little portion of Congressional history is going to end up being a footnote."

# Oneliner

Representative George Santos's expulsion from Congress sparks concerns about setting new expulsion precedents without convictions, potentially leading to future expulsions for lesser offenses.

# Audience

Congressional observers

# On-the-ground actions from transcript

- Contact your representatives to voice concerns about expelling members without convictions (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the expulsion of Representative George Santos and the potential implications of setting new precedents for expelling members from Congress without a conviction. Viewing the full transcript will give a comprehensive understanding of the situation and its broader impacts in the political landscape.

# Tags

#Congress #Expulsion #GeorgeSantos #Precedent #PoliticalEthics