# Bits

Beau says:

- Department of Defense instituted a program to keep extremists out of the military, leading to 183 extremists identified in the past year.
- In addition to extremists, 58 service members were identified with gang affiliations.
- Out of the 183 identified extremists, 68 cases were cleared or unsubstantiated.
- Chain of command acted in every substantiated case, ranging from discharge to court-martial, demonstrating seriousness in addressing extremism.
- Weak link found in recruitment screening process, where steps were not always followed, potentially due to laxity from struggling to meet recruitment goals.
- Despite recruitment screening issues, the program appears to be functioning well overall.
- Programs initiated by the Secretariat of Defense, which some deemed unnecessary, have resulted in high numbers of extremists being identified.
- Army had the highest numbers of identified extremists, while Space Force had the lowest.
- Screening process may be improved by rejecting candidates without proper screening in place, potentially increasing recruiter diligence.
- Overwhelming majority of substantiated cases resulted in significant punishments, with only three receiving counseling.

# Quotes

- "Department of Defense instituted a program to keep extremists out of the military."
- "Every single instance where it was substantiated, it was acted upon."
- "The Army had the most numbers, raw numbers, but Space Force had the lowest."

# Oneliner

Department of Defense identifies extremists and gang affiliations in the military, with chain of command taking serious actions on substantiated cases while recruitment screening remains a weak link.

# Audience

Military personnel, policymakers

# On-the-ground actions from transcript

- Reject candidates without proper screening (implied)
- Increase recruiter diligence in screening processes (implied)

# Whats missing in summary

The potential long-term impacts and effectiveness of the program in keeping extremists out of the military.

# Tags

#DepartmentofDefense #MilitaryExtremism #RecruitmentScreening #ChainOfCommand #ProgramEffectiveness