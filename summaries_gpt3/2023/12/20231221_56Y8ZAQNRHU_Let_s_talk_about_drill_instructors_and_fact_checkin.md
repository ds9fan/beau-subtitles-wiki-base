# Bits

Beau says:

- Explains the reasons behind drill instructors yelling, including to establish authority, create urgency, and provide stress inoculation.
- Reveals a lesser-known reason for yelling, which is to turn a teachable moment for one person into a lesson for everyone.
- Shares the importance of repeating information in videos to combat misinformation and reach a wider audience.
- Emphasizes the effectiveness of multiple exposures to debunk false beliefs and encourage critical thinking.
- Advocates for using teaching techniques to help people learn how to fact-check and be less susceptible to misinformation.

# Quotes

- "If you suffer enough shark attacks, which is where a whole bunch of instructors come up and yell at you all at once and normally yell conflicting things, and anyway, you can become very unbothered by a lot of things that might happen later if you're in the military."
- "It turns a teachable moment for one person into a teachable moment for everybody."
- "Battling that kind of bad information is something that I think is really important."
- "It's a teaching technique, and it's one that works."
- "Y'all have a good day."

# Oneliner

Beau explains the reasons behind drill instructors yelling and the importance of repeating information to combat misinformation effectively.

# Audience

Educators, Critical Thinkers

# On-the-ground actions from transcript

- Share educational videos multiple times to combat misinformation effectively (suggested).
- Encourage critical thinking through repeated exposure to debunk false beliefs (suggested).
- Use teaching techniques to help others learn how to fact-check and be less susceptible to misinformation (suggested).

# Whats missing in summary

The detailed examples and explanations provided by Beau in the full transcript are missing from this summary.

# Tags

#DrillInstructors #Yelling #CombatMisinformation #TeachingTechniques #CriticalThinking