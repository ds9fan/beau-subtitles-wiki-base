# Bits

Beau says:

- The Satanic Temple set up a religious display in the Iowa State Capitol, sparking controversy among politicians and religious leaders.
- The display was vandalized by someone from Mississippi, charged with criminal mischief.
- An after-school Satan Club linked to the Satanic Temple is forming in Tennessee.
- The Temple clarified they don't advocate for introducing religion into public schools unless other religious groups are present.
- Right-wing politicians often push for religious freedom but mainly mean their religion.
- Separation between church and state exists to allow all religions, not just one, access to state facilities.
- Changes in rules may occur where religious groups are no longer allowed to use state or county facilities or funds.
- There's a possibility that this situation is a form of protest, and changes may or may not happen.
- The desire to display religious beliefs on state property can lead to issues and undermine religious freedom.
- Beau reminds that the US was founded as a non-theocratic country, and that principle has served well.

# Quotes

- "If you open the door, well, all religions get to walk through."
- "The desire to try to convert people on state property or to signal your allegiance to a particular voting bloc tends to just lead to issues."
- "It's just a thought."

# Oneliner

The Satanic Temple's display in Iowa sparks controversy, while an after-school Satan Club forms in Tennessee, raising questions about religious freedom and separation of church and state.

# Audience

Activists, educators, policymakers

# On-the-ground actions from transcript

- Support inclusive policies in schools by advocating for the presence of diverse religious groups on campus (implied)
- Advocate for the separation of church and state to ensure religious freedom for all (implied)

# What's missing in summary

The full transcript provides more context on the potential outcomes of the situation and the historical significance of the separation between church and state in the US.

# Tags

#ReligiousFreedom #SeparationOfChurchAndState #Protest #SatanicTemple #Education