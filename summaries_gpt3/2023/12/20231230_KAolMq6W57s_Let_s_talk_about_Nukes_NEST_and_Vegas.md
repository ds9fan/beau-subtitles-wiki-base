# Bits

Beau says:

- Explains a theory circulating about the National Nuclear Security Administration looking for scary things on New Year's.
- Confirms the rumor that they will be looking for a dirty bomb, which they do annually.
- Describes how the Nuclear Emergency Support Team (NEST) will be involved in scanning for radiation.
- Mentions the use of a high-tech helicopter (Huey) with pods hanging off the side for the scanning.
- Assures that this activity is routine and not due to any specific threat, happening at major events with large crowds.
- Talks about the presence of NEST in places like Vegas and New York for New Year's celebrations.
- Emphasizes not to panic if you see the scanning helicopter labeled as NEST.

# Quotes

- "You will see a Huey."
- "If you happen to see those, don't panic, it's pretty normal as weird as that is to say."
- "If you see anything that's labeled NEST, don't panic."

# Oneliner

Beau explains the routine scanning for dirty bombs by NEST on New Year's, reassuring people not to panic if they see the high-tech helicopter flying low.

# Audience

Event Attendees

# On-the-ground actions from transcript

- Be aware and informed about the routine scanning by NEST for dirty bombs during major events like New Year's (implied).
- Stay calm and don't panic if you witness the scanning helicopter labeled as NEST (implied).

# Whats missing in summary

The full transcript provides detailed information about the routine scanning for dirty bombs by NEST during major events like New Year's celebrations.

# Tags

#Security #NEST #NewYear #RoutineScanning #DirtyBomb