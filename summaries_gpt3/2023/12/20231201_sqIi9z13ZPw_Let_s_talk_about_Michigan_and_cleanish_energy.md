# Bits

Beau says:

- Explains the controversy around Michigan's move to 100% clean energy by 2040.
- Friends who are environmentalists are upset about the redefinition of clean energy in the bill.
- The bill mandates that 100% of electricity comes from clean energy, with 60% from renewables.
- The remaining 40% can be from hydrogen, nuclear, or natural gas with carbon capture.
- People are particularly upset about natural gas with carbon capture being included.
- Beau sees the bill as a positive step, but acknowledges it's not perfect.
- Carbon capture is seen as expensive, inefficient, and still promoting fossil fuel use.
- Beau questions whether advancements in carbon capture technology will make it more efficient.
- Believes new electricity plants won't be built due to costs, existing ones may add carbon capture.
- Acknowledges that despite the flaws, Michigan's move is a significant step in the right direction.

# Quotes

- "Everybody I know that had an issue with this, it had to do with the natural gas."
- "That's what they're mad about, not the overall bill."
- "It's a big step."

# Oneliner

Beau explains the controversy around Michigan's 100% clean energy bill, particularly focusing on the redefinition of clean energy and the inclusion of natural gas with carbon capture.

# Audience

Michigan residents, environmental activists

# On-the-ground actions from transcript

- Contact local representatives to advocate for stricter definitions of clean energy (implied)
- Join local environmental groups to stay informed and involved in energy policy (implied)

# Whats missing in summary

In-depth analysis of the potential impact of including natural gas with carbon capture in Michigan's clean energy transition.

# Tags

#Michigan #CleanEnergy #EnvironmentalPolicy #CarbonCapture #Renewables