# Bits

Beau says:

- Raises the issue of Iowa's decision to not participate in a summer program for EBT, impacting low-income families.
- Explains the program provides $40 per month per child to help with food costs during school breaks.
- Points out the state's cost for this program is only 50% of administrative costs, around $2 million.
- Criticizes the governor's choice to prioritize a publicity stunt over funding for children's meals.
- Calls out politicians for using vulnerable groups as scapegoats and distracting from real issues.
- Dismisses the argument that tax dollars are being wasted on providing food for kids who may not be deemed "needy."
- Argues against denying food assistance to those in need based on the worst-case scenario of a few who may not qualify benefiting.
- Expresses concern over the politicization of children's basic needs like food.

# Quotes

- "When you have a country that is trying to politicize and debate whether or not kids should eat, I suggest that's a problem."
- "For the nine billionth time on this channel, a group of people who have less institutional power than you do are never the source of your problems."
- "Those in power just get you looking down at them, so you're not paying attention to what those in power are doing."
- "Assuming that's occurring, the worst-case scenario here is that somebody using this framing paid their taxes and that money went back to their kid."
- "Anyway, y'all enjoy your holiday meals. It's just a thought. Have a good night."

# Oneliner

Iowa's refusal to participate in a food assistance program for low-income families raises concerns about prioritizing publicity stunts over children's meals, prompting a critical look at politicizing basic needs.

# Audience

Advocates for child welfare

# On-the-ground actions from transcript

- Contact local representatives to express support for programs providing food assistance to low-income families (suggested).
- Donate to local food banks or organizations supporting children's nutrition (exemplified).

# Whats missing in summary

The emotional impact of prioritizing politics over children's well-being and the importance of community support for vulnerable families. 

# Tags

#ChildWelfare #FoodAssistance #Politicization #CommunitySupport #LowIncomeFamilies