# Bits

Beau says:

- Introduces the topic of Justice Thomas, the Supreme Court, pay, finances, timelines, and ProPublica.
- Mentions ProPublica as a reputable outlet known for breaking major stories.
- Talks about a report from ProPublica regarding Justice Thomas facing financial issues in January 2000.
- Justice Thomas discussed financial problems with a GOP lawmaker on a flight back from a conservative conference.
- Lawmaker attempted to get justices a raise but it didn't happen.
- Panic in the GOP was triggered by Thomas mentioning the possibility of resigning over pay, which could lead to Clinton nominating replacements.
- Justice Thomas started receiving gifts shortly after, connecting to previous reporting by ProPublica.
- ProPublica's reporting doesn't imply anything nefarious but raises questions about the timeline of events.
- Urges the audience to read the reporting to understand the situation better.
- Emphasizes that the issue will come up and be politically significant, regardless of evidence surfacing.

# Quotes

- "This is going to be big."
- "Politically, it is going to be used whether or not evidence ever surfaces."
- "Y'all have a good day."

# Oneliner

Beau explains a report from ProPublica on Justice Thomas facing financial issues and a timeline of events that could have political implications.

# Audience

Advocates and activists.

# On-the-ground actions from transcript

- Read the report from ProPublica to understand the situation better (suggested).
- Stay informed and prepared for this issue to be a significant talking point (implied).

# What's missing in summary

Context on Justice Thomas's financial issues and potential implications for the Supreme Court.