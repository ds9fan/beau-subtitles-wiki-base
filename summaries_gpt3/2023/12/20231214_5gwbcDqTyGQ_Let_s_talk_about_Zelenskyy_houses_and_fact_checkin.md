# Bits

Beau says:

- Explains the importance of fact-checking information yourself, especially in light of current stories circulating about Zelensky.
- Responds to a viewer's claim about Zelensky using aid money to buy a yacht, which led to the viewer sending a link to a tweet stating Zelensky bought a mansion in Florida.
- Advises on the initial step of fact-checking by searching for the claim on Google and checking for news outlets reporting on it.
- Suggests waiting for a major outlet to pick up the news if it initially doesn't show up in search results.
- Encourages looking at non-news results and websites dedicated to debunking social media rumors to verify information.
- Guides viewers on using a reverse image search tool like TinEye to verify the authenticity of images being shared.
- Analyzes the edited image from the tweet and proves its falsity through comparison with original images.
- Dissects the fake naturalization certificate image and clarifies that the house in question is not in Vero Beach and not worth $20 million.
- Emphasizes the importance of being a critical consumer of information to avoid being misled by sensationalized claims.
- Commends the viewer for questioning and seeking accurate information, stressing the need to develop skills to discern misinformation.

# Quotes

- "Now download a couple images of the house. Doesn't matter which ones."
- "It's just made up for engagement and clicks."
- "Our biggest sign should have been that it wasn't property records being shown."
- "There is nothing in that tweet that's true."
- "Y'all have a good day."

# Oneliner

Beau explains how to fact-check claims like Zelensky buying a mansion, revealing the importance of independent verification and critical information consumption to combat misinformation.

# Audience

Information consumers

# On-the-ground actions from transcript

- Use reverse image search tools like TinEye to verify images shared online (implied).
- Verify sensational claims by checking multiple sources and using critical thinking skills (implied).
- Develop the ability to discern misinformation and avoid spreading false information for engagement (implied).

# Whats missing in summary

The full transcript provides a detailed guide on fact-checking and verifying information, promoting critical thinking skills and independent verification to combat misinformation effectively.

# Tags

#FactChecking #Misinformation #CriticalThinking #InformationConsumers #Verification