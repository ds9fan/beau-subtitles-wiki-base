# Bits

Beau says:

- Annual fundraiser for domestic violence shelters providing gifts for teens
- Tablets, cases, earbuds, Google Play, Hulu cards given to teens in shelters
- Excess funds used for other shelter needs like holiday meals
- Tablets for teens provide a sense of control and ownership in a chaotic situation
- Importance of communication to understand shelter needs for effective donations
- Need to call shelters to determine current needs as they vary each year
- Example of past needs like cleaning supplies and bras at shelters
- Main channel fundraiser may set up an endowment for long-term impact
- Ways for individuals with limited resources to contribute to activism locally
- Importance of acknowledging wins and taking breaks when overwhelmed
- Tips on decentralizing activist groups and avoiding centralization pitfalls
- Beau's interest in Jeep Wranglers and desire to try the environmentally friendly 4XE

# Quotes

"Tablets for teens provide a sense of control and ownership in a chaotic situation."
"Call shelters to determine current needs as they vary each year."
"Importance of acknowledging wins and taking breaks when overwhelmed."

# Oneliner

Beau shares insights on fundraising for domestic violence shelters, the importance of communication with shelters, and tips for effective activism locally.

# Audience

Community members

# On-the-ground actions from transcript

- Contact local shelters to inquire about their current needs (suggested)
- Volunteer to run errands or help with maintenance at organizations in need (suggested)
- Support causes by offering specific skills or assistance as per their requirements (exemplified)

# Whats missing in summary

The full transcript provides detailed guidance on fundraising for domestic violence shelters, effective communication with shelters, and practical tips for local activism.

# Tags

#Fundraising #DomesticViolence #CommunityNetworking #Activism #ShelterSupport #LocalChange #Decentralization #JeepWrangler #Environmentalism