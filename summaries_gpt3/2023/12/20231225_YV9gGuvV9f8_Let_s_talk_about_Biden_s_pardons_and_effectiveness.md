# Bits

Beau says:

- The Biden administration recently expanded pardons for simple possession of marijuana.
- The impact is more about finding housing or employment rather than widespread.
- To receive benefits from the pardon, individuals must apply for a certificate from the DOJ.
- Most marijuana charges occur at the state level, not federal, limiting Biden's impact.
- Biden's goal is to set an example for governors to follow suit in pardoning similar offenses.
- This move signifies a potential shift in how federal charges are treated.
- The main aim is to encourage Democratic governors to take similar actions.
- The focus is on setting a precedent rather than directly impacting a large number of individuals.
- The majority of marijuana charges are state-level, limiting federal impact.
- The Biden administration is aiming to lead by example and encourage other governors to act similarly.

# Quotes

- "No one should be in a federal prison solely due to the use or possession of marijuana."
- "The real goal probably wasn't to just help the few thousand that are going to be impacted directly."
- "The overwhelming majority of those charges occur at the state level."
- "It's a thought. Y'all have a good day."

# Oneliner

The Biden administration's pardons for marijuana possession set an example for governors, aiming to lead by example rather than create a widespread impact.

# Audience

Policy advocates, activists, governors.

# On-the-ground actions from transcript

- Reach out to local governors to advocate for similar pardoning actions (implied).
- Support policies that prioritize pardons for simple possession of marijuana (implied).

# Whats missing in summary

Further analysis on the potential implications of the Biden administration's actions and the broader impact on criminal justice reform efforts.

# Tags

#Biden #Pardons #Marijuana #Governors #CriminalJustice #Reform