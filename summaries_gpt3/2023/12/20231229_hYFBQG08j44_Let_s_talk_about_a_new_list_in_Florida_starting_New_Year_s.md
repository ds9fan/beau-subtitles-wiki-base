# Bits

Beau says:

- New registry in Florida starting January 1st created by the Protect Our Loved Ones Act - the Special Persons Registry.
- Voluntary registry for individuals with specific conditions like nonverbal, Alzheimer's, or deaf.
- Aimed at providing law enforcement with vital information to prevent tragic outcomes.
- Information on the registry can be critical in cases of wandering or emergencies.
- Requires documentation of the condition, like a birth certificate.
- Registry setup by sheriff's departments, cross-referenced between counties.
- Aims to prevent misunderstandings and potential tragedies during encounters with law enforcement.
- Acknowledges the need for systemic change within law enforcement culture.
- Despite mixed feelings, recognizes the potential to save lives through this registry.
- Implementation of the database starts on January 1st, likely to happen swiftly.

# Quotes

- "This registry is voluntary."
- "The fact that it needs to is a total indictment of law enforcement culture."
- "Until that occurs, this will save lives."

# Oneliner

New voluntary registry in Florida aims to provide vital information to law enforcement starting January 1st, potentially saving lives despite exposing systemic issues.

# Audience

Floridians, caregivers, advocates.

# On-the-ground actions from transcript

- Register yourself or a loved one on the Special Persons Registry (exemplified).
- Ensure documentation of the condition is available for registration (exemplified).
- Support initiatives for systemic changes within law enforcement culture (implied).

# Whats missing in summary

The emotional impact of the registry on individuals and families. 

# Tags

#Florida #SpecialPersonsRegistry #LawEnforcement #SystemicChange #CommunitySafety