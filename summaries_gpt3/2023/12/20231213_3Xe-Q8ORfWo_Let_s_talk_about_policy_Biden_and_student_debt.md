# Bits

Beau says:

- Policy shifts take time, and it's vital to mark small wins even in setbacks of big policy changes.
- Explains the importance of nuance in discussing big policy changes in the current political climate.
- Illustrates an example of legislation in the Senate concerning hedge funds not owning single-family housing.
- Emphasizes the need for a 10-year period for hedge funds to sell off their inventory to prevent a housing crash.
- Talks about the challenges of understanding large financial numbers like $400 billion for student loan forgiveness.
- Reveals that the Biden administration has forgiven over $100 billion in student debt, impacting millions.
- Notes the lack of recognition for these small wins and the disconnect between commentary and actual events.
- Stresses the significance of acknowledging small wins to understand and appreciate ongoing progress.
- Mentions the motivation and fight sustainability gained from recognizing and celebrating incremental changes.
- Encourages taking a moment to appreciate progress, combat cynicism, and stay engaged in pushing for change.

# Quotes

- "Policy shifts take time, and it's vital to mark small wins."
- "You have to take the time to acknowledge those small wins."
- "Those little changes add up real quick."
- "More than $100 billion has been forgiven."
- "Acknowledging the wins helps motivate you and keep you in the fight."

# Oneliner

Policy shifts take time, marking small wins is vital; embracing nuance in discussing big changes, especially in forgiving student debt over $100 billion.

# Audience

Advocates, Activists, Students

# On-the-ground actions from transcript

- Acknowledge and celebrate small wins in your community's progress (implied).
- Stay informed about ongoing policy changes and their impacts (implied).
- Support initiatives that address financial issues and student debt forgiveness (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of the significance of recognizing incremental progress and the impact of policy changes on various sectors. Viewing the full transcript can offer a deeper understanding of the nuanced approach required in discussing and advocating for social change.

# Tags

#PolicyShifts #SmallWins #StudentDebt #Progress #Nuance