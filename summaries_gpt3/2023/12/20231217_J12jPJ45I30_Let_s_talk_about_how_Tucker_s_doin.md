# Bits

Beau says:

- Checking in on Tucker Carlson after some time.
- Carlson's openness to the idea of the Earth being flat.
- Mention of deception and lack of trust in preconceptions.
- Carlson's surprising pushback on the flat Earth theory.
- Speculation on Carlson hanging out with Jones soon.
- Suggesting sharing this information with Fox News-watching relatives.

# Quotes

- "Tucker Carlson was once one of the highest rated people on a news network, is open to the idea that the earth is indeed not round."
- "It seems like that might be something that can get through some pretty heavy defenses."
- "He's on his way to, yeah, he's going to be hanging out with Jones soon I guess."

# Oneliner

Beau checks in on Tucker Carlson's openness to the flat Earth theory, suggesting sharing with Fox News-watching relatives.

# Audience

Fox News viewers

# On-the-ground actions from transcript

- Share clips of Tucker Carlson's comments on the flat Earth theory online (suggested).

# Whats missing in summary

Context on the history between Tucker Carlson and Jones

# Tags

#TuckerCarlson #FlatEarthTheory #Deception #FoxNews #Jones