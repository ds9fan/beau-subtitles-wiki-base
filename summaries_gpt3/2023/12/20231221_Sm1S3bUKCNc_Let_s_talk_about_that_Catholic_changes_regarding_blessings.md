# Bits

Beau says:

- Explains the recent news about priests blessing same-sex couples but not allowing gay marriage, showing a significant shift in the Catholic Church's stance.
- Emphasizes that this change is a big deal because it alters the way same-sex couples are viewed within the church, moving from exclusion to acceptance.
- Points out that although the decision may not allow for same-sex marriage, it signifies a major step towards LGBTQ inclusion within the Catholic Church.
- Describes how this change is part of a broader effort by the Pope to make the Church more welcoming to LGBTQ individuals.
- Mentions historical examples of slow changes in Church practices, illustrating the significance of the recent development.

# Quotes

- "That's what it really boils down to."
- "This is huge. This is a major development because the Church is a very traditional organization and change takes time."
- "It's another sign that on a long enough timeline, we win."

# Oneliner

Beau explains the significant shift in the Catholic Church's stance towards blessing same-sex couples, marking a major step towards LGBTQ inclusion.

# Audience

Catholics, LGBTQ community

# On-the-ground actions from transcript

- Support LGBTQ Catholics in their journey of faith and acceptance (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of the recent changes in the Catholic Church's approach towards blessing same-sex couples, offering insights into the significance of this development and its implications for LGBTQ inclusion.

# Tags

#CatholicChurch #LGBTQ #Inclusion #Change #Acceptance