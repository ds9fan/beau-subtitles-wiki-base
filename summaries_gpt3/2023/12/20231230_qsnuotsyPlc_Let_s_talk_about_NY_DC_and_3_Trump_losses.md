# Bits

Beau says:

- Explaining two cases against Trump in New York and DC, with significant losses for Trump.
- Mentioning a DC case brought by cops impacted by January 6th against Trump where his immunity claims failed.
- Noting two losses in an E. Jean Carroll case in New York where Trump's attempt to delay the case based on immunity was unsuccessful.
- Detailing Trump's team's attempt to remove an expert witness in the E. Jean Carroll case who calculates damages, which the judge denied due to being too late in the game.
- Speculating that damages in the E. Jean Carroll case are expected to be significant, possibly leading to a large payout.
- Anticipating more movement in both cases soon, especially the E. Jean Carroll case starting on January 16th.

# Quotes

- "He tried to use his immunity claims there, And the judges said that the argument, quote, fails."
- "The expectation is that it is going to be a pretty big number."
- "You didn't turn in your homework on time, basically."

# Oneliner

Trump faces significant losses in two cases in DC and New York, with immunity claims failing and the expectation of a substantial payout looming.

# Audience

Legal observers

# On-the-ground actions from transcript

- Follow the developments in the legal cases against Trump, especially the E. Jean Carroll case starting on January 16th (suggested).
- Stay informed about legal proceedings and outcomes related to accountability for public figures (suggested).

# Whats missing in summary

Insights on the potential implications of these legal battles for future accountability and implications for other similar cases.

# Tags

#Trump #LegalCases #Accountability #EJeanCarroll #DC #NewYork