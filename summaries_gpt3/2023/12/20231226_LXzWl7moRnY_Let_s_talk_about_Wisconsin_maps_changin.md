# Bits

Beau says:

- Wisconsin is considered the most gerrymandered state in the country.
- Justice Protasewicz correctly identified this fact during her campaign, causing discomfort among Republicans who created the contentious maps.
- Republicans threatened to impeach Justice Protasewicz if she took a case on the gerrymandered maps, but she proceeded with it.
- The court ruled that the maps in Wisconsin are indeed gerrymandered.
- New maps are expected to be ready for use by 2024, with the legislature having the first chance to submit them by August.
- If the submitted maps are deemed partisan, the court will step in and select a map.
- Due to widespread gerrymandering, the Wisconsin legislature heavily leans Republican, raising concerns about their map submission.
- The redistricting will likely impact the makeup of the legislature, with predictions that Republicans may retain control of the assembly but lose the Senate.
- This redistricting issue in Wisconsin is seen as highly consequential and is nearing conclusion.
- The resolution and new maps are expected before the next election, providing fair representation after a decade of partisan maps suppressing voter voices.

# Quotes

- "Wisconsin is widely viewed as the most gerrymandered state in the country."
- "They threatened to impeach her if she took a case. And she was like, yeah, no, I'm taking the case."
- "The odds are it's going to go to the court."
- "It is likely going to alter the makeup of the legislature in Wisconsin."
- "Having their voice and their vote diluted by people who wanted to rule rather than represent."

# Oneliner

Wisconsin faces consequential redistricting issues, with new maps expected before the next election after a decade of partisan suppression of voter voices.

# Audience

Wisconsin residents, political activists

# On-the-ground actions from transcript

- Contact local representatives to advocate for fair and non-partisan redistricting (suggested)
- Stay informed about the redistricting process and attend relevant community meetings (implied)

# Whats missing in summary

Insights on the potential impacts of the new maps and the importance of fair representation in elections.

# Tags

#Wisconsin #Redistricting #Gerrymandering #JusticeProtasewicz #Legislature