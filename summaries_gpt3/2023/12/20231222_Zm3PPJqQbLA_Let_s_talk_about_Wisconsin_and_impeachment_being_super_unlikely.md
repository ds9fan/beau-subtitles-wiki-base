# Bits

Beau says:

- Justice Protozeewicz identified Wisconsin as gerrymandered while seeking office on the Wisconsin Supreme Court.
- Republicans were nervous about her involvement in a redistricting case and wanted her to step down, but she refused.
- Republicans threatened impeachment over her refusal to step down.
- The Republican Party, specifically the House Assembly, heavily pursued the idea of impeaching her for a while.
- The leader of the GOP in Wisconsin stated that impeachment is now "super unlikely."
- It seems that the Republicans understand the potential backlash of impeaching Justice Protozeewicz.
- Justice Protozeewicz is currently not in danger of being impeached, and the long-running story may be coming to a close.

# Quotes

- "Impeachment was, quote, super unlikely."
- "If Republicans hold that position and then try to stop her from correcting it, they're creating a situation where you have even more upset people about it."

# Oneliner

Republicans back down from threatening impeachment against Justice Protozeewicz over redistricting concerns in Wisconsin.

# Audience

Wisconsin voters

# On-the-ground actions from transcript

- Contact local representatives to express support for fair redistricting (implied)

# Whats missing in summary

The full transcript provides a detailed account of the political saga surrounding Justice Protozeewicz and the Republican Party's stance on redistricting in Wisconsin.