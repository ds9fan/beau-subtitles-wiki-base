# Bits

Beau says:

- Two men are in trouble for allegedly killing 3,600 birds, including bald and golden eagles, from January 2019 to March 2021.
- They face charges for violating the Bald and Golden Eagle Protection Act, with text messages allegedly admitting to their felonies.
- The birds were killed and likely sold for their feathers, but it's unclear how many were protected.
- Despite not being arrested, the men have been ordered to appear, indicating a strong case against them.
- The illegal wildlife trade for feathers continues due to its appeal as a status symbol.
- This case may draw attention to the ongoing issue of wildlife trafficking, potentially becoming a significant trial.

# Quotes

- "Only elephants need ivory, only rhinos need horns, and only birds need feathers."
- "There's probably going to be a lot more said and if this goes to trial it will probably turn into a really big thing."

# Oneliner

Two men face charges for illegally killing thousands of birds, shedding light on the dark world of wildlife trafficking.

# Audience

Wildlife enthusiasts, conservationists

# On-the-ground actions from transcript

- Join wildlife protection organizations to support efforts against wildlife trafficking (implied)
- Stay informed about wildlife protection laws and report any suspicious activities involving protected species (implied)

# Whats missing in summary

The emotional impact of the environmental harm caused by the illegal killing of thousands of birds and the importance of raising awareness about wildlife conservation efforts.

# Tags

#WildlifeTrafficking #BirdConservation #IllegalKilling #EnvironmentalProtection #ConservationEfforts