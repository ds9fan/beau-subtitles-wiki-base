# Bits

Beau says:

- Addresses the issue of groups left out in the United States who get a raw deal and how they are perceived by others.
- Reads a message from a conservative individual questioning why minority groups seem to dislike America and American values.
- Responds by pointing out that these groups have been marginalized and mistreated by society, making it hard for them to support the country.
- Advises the conservative individual to support these groups on the issues where they acknowledge they are mistreated, even if they don't agree on everything.
- Emphasizes the need for the system to change to be inclusive of those who have been historically marginalized.
- Uses the analogy of a child not embraced by the village burning it down for warmth to explain the anger and desire for change among marginalized groups.
- Encourages advocating for change in the system to earn respect rather than expecting support for an unjust system.
- Suggests that actively supporting marginalized groups will lead to a deeper understanding of their struggles and potentially reveal more injustices.

# Quotes

- "A child that is not embraced by the village will burn it down to fill its warmth."
- "If you want them to respect a system, it has to be a system worthy of respect."
- "You have identified things that you will say is them getting a raw deal."

# Oneliner

Beau addresses the disconnect between marginalized groups and societal perceptions, urging support for change in a system that currently excludes and mistreats them.

# Audience

Conservative individuals, Allies, Advocates

# On-the-ground actions from transcript

- Support marginalized groups on the issues where they are mistreated (implied)
- Advocate for systemic change to be inclusive of marginalized communities (implied)
- Listen to and understand the struggles of marginalized groups by actively engaging with them (implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on understanding and supporting marginalized groups in the United States, urging a shift in societal values and systems towards inclusivity and respect.

# Tags

#MarginalizedGroups #SystemicChange #Support #Inclusivity #Respect