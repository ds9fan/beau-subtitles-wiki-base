# Bits

Beau says:

- The Biden administration announced a $8.2 billion investment in 10 passenger rail projects in the United States, the largest since Amtrak's creation in 1971.
- The goal of this investment is not just job creation and infrastructure update but also to make Americans more comfortable with passenger rail travel.
- The flagship project among these is a high-speed rail from Vegas to LA, reaching speeds of 220 miles per hour and reducing travel time to about two hours by 2028.
- This initiative is part of Biden's efforts to boost the economy, create jobs, and modernize American infrastructure.
- The focus on the LA to Vegas route is strategic because it's a frequently taken trip, allowing people to become more accustomed to using passenger rail.
- Biden's visit to Vegas aimed to promote these infrastructure projects and the economic benefits they bring, although overshadowed by local events.
- Passenger rail could be a sustainable way to transport people in the US, especially as technology advances and interest in rail travel grows.
- Increasing familiarity with passenger rail may lead to more sustainable projects that utilize technologies common in other countries.

# Quotes

- "The goal is to get the American people more comfortable with rail moving passengers."
- "Passenger rail is something the US probably needs to become super enthusiastic about."
- "This is an attempt to alter that dynamic."

# Oneliner

Biden's $8.2 billion investment in passenger rail aims to boost the economy, create jobs, and familiarize Americans with rail travel for a sustainable future.

# Audience

Americans

# On-the-ground actions from transcript

- Support and advocate for the development of passenger rail projects in your local area (implied).
- Use passenger rail services whenever possible to familiarize yourself with this mode of transportation and support its growth (implied).

# Whats missing in summary

The full transcript provides more insight into the importance of passenger rail for sustainable transportation and economic growth.