# Bits

Beau says:

- Explains the recent actions taken by the Senate Judiciary Committee regarding subpoenas for Harlan Crowe and Leonard Leo.
- Mentions the partisan nature of the vote on the committee, with Democrats wanting to hear from them.
- Notes that if Crowe and Leo refuse to comply with the subpoenas, it will have to go to the full Senate and require 60 votes for enforcement.
- Questions the Senate Judiciary Committee's motivation for looking into the issue of ethics surrounding the Supreme Court.
- Emphasizes the importance of the Supreme Court's legitimacy and the need for public faith in its ethical operations.
- Suggests that if Congress wants to implement an ethics code for the Supreme Court, they should simply do so without creating a spectacle that undermines the institution.
- Advocates for a straightforward approach of putting forth legislation for an ethics code instead of engaging in lengthy hearings that may further damage the Supreme Court's legitimacy.

# Quotes

- "The legitimacy of the Supreme Court is paramount. If the American people do not believe that the Supreme Court is operating in an ethical and good faith manner, eventually the rulings will be ignored."
- "If Congress wants to put forth ethics, an ethics code that the Supreme Court is supposed to follow, they need to just do it."
- "The show, it doesn't matter. Put forth the code, make sure it is applied from here on out, and go from there."

# Oneliner

Beau delves into the Senate Judiciary Committee's subpoenas for individuals linked to Supreme Court justice gifts, stressing the importance of public faith in the Court's ethics and advocating for a direct approach to implementing an ethics code.

# Audience

Congress Members

# On-the-ground actions from transcript

- Put forth legislation for an ethics code for the Supreme Court (suggested)
- Ensure the ethics code is applied consistently from now on (suggested)

# Whats missing in summary

Insights on the potential repercussions of undermining the legitimacy of the Supreme Court and the importance of public trust in its ethical operations.

# Tags

#SupremeCourt #Ethics #SenateJudiciaryCommittee #Legislation #PublicTrust