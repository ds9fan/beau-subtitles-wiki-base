# Bits

Beau says:

- Explaining two separate incidents in Trump World that happened recently, involving Rudy Giuliani and the former president.
- Giuliani is in the middle of dealing with a defamation case related to election workers and is determining how much money he will have to pay for his comments.
- Despite being ruled out of bounds by the judge, Giuliani insists that his comments were true and does not regret them.
- Giuliani repeats his controversial statement during the proceedings, potentially leading to increased monetary penalties.
- Moving on to Trump, who claims presidential immunity and asked the Supreme Court to decide on it quickly, which the Court agreed to expedite consideration of.
- Trump's team released a statement criticizing the actions as an attempt to prevent him from retaking the Oval Office in 2024.
- Beau points out that if Trump truly believed in his presidential immunity claim, taking it to the Supreme Court quickly should benefit him.
- The statements from Giuliani and Trump's team indicate their strategies to drag out legal proceedings and cater to their base rather than a strong legal basis.
- Beau observes that both instances show Trump World trying to influence public opinion through statements, not realizing that the judicial system is impartial.
- Concluding with a reflection on the ill-advised nature of both statements and how they reveal Trump World's tactics.

# Quotes

- "I told the truth. They were engaged in changing votes."
- "If Giuliani is engaged in that behavior while the dollar amount is being figured out, it probably means the dollar amount goes up."
- "If there was any basis to the president's claims, it seems like they would want it to go to the Supreme Court."
- "You have two instances last night that were Trump world acting like Trump world."
- "The judicial system literally does not care about the court of public opinion."

# Oneliner

Beau explains recent incidents involving Giuliani and Trump in Trump World, showing their tactics to influence public opinion and drag out legal proceedings.

# Audience

Political observers, activists

# On-the-ground actions from transcript

- Contact legal aid organizations to support efforts combating misinformation and defamation cases (suggested)
- Stay informed on legal proceedings related to political figures and hold them accountable (implied)

# Whats missing in summary

Analysis of the potential implications of these incidents on future legal battles and public trust in political figures.

# Tags

#TrumpWorld #Giuliani #PresidentialImmunity #SupremeCourt #LegalProceedings