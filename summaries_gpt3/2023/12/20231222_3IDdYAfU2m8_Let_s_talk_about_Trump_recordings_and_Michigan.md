# Bits

Beau says:

- News broke about a reported recording of a phone call on November 17th, 2020 involving Trump, McDaniel, and Michigan officials.
- Trump and McDaniel were allegedly pressuring officials in Michigan not to certify the election results.
- Key phrases from the call include Trump saying officials "They would look terrible if they signed" and McDaniel urging not to sign, saying they will provide attorneys.
- The call aimed to prevent the certification of Wayne County votes, potentially affecting around 880,000 people.
- This was another attempt by Trump to alter the election outcome in Michigan, where Biden won by 154,000 votes.
- The recording may play a significant role in legal cases and could come up in federal and Georgia cases.
- While Beau hasn't heard the recording personally, its contents are widely reported.
- This recording adds to the evidence that may strengthen legal cases against Trump's interference in the election.

# Quotes

- "They will look terrible if they signed."
- "We'll take care of that."

# Oneliner

News broke about a recording of Trump pressuring Michigan officials not to certify election results, potentially affecting 880,000 voters, adding to legal evidence.

# Audience

Legal activists, concerned citizens

# On-the-ground actions from transcript

- Contact legal advocacy organizations to stay informed and support efforts against election interference (suggested)

# Whats missing in summary

Full context and emotions conveyed in the actual recording.