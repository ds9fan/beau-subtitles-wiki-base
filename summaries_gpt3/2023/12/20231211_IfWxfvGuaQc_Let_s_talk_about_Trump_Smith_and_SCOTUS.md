# Bits

Beau says:

- Explains the concept of presidential immunity being claimed by Trump in the context of the January 6th case in DC.
- Smith has asked the Supreme Court to decide immediately on the question of whether a former president is immune from federal prosecution for crimes committed while in office.
- Reads a section from Article 2 of the Constitution to show that presidential immunity doesn't exist.
- Notes that the Constitution allows for criminal trial even if a president has been impeached and removed from office.
- States that the idea of presidential immunity as described by Trump is fabricated.
- Criticizes the Republican Party for choosing to support Trump over the country and Constitution.
- Predicts that the Supreme Court may not find the need to hear Trump's appeal and might leave it to the appeals court.
- Doubts that even the current Supreme Court might side with Trump due to the clear constitutional provisions.
- Concludes by expressing uncertainty about the Supreme Court's decision.

# Quotes

- "The idea of presidential immunity and the way Trump is describing it, it's made up. That's not a thing."
- "You can support Trump or you can support the country. You can support the Constitution or you can support the real estate developer."

# Oneliner

Beau explains why presidential immunity does not exist and questions whether the Supreme Court will side with Trump over the Constitution.

# Audience

Legal scholars, political activists

# On-the-ground actions from transcript

- Contact legal experts to understand the implications of presidential immunity (suggested)
- Share this information to raise awareness about the Constitution's stance on presidential immunity (implied)

# Whats missing in summary

Full context and depth of analysis on presidential immunity and its implications in ongoing legal proceedings.

# Tags

#PresidentialImmunity #Trump #Constitution #SupremeCourt #LegalIssues