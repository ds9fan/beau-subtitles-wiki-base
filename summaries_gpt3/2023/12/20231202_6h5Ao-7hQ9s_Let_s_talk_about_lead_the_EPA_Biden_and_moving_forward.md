# Bits

Beau says:

- The EPA has proposed a new rule that will require most cities in the United States to replace all lead pipes within 10 years, regardless of lead levels in the water.
- The rule also aims to lower the amount of allowable lead in water systems, with a requirement to provide filters if levels exceed the limit.
- Nearly a hundred thousand people in Flint were affected by lead contamination, underscoring the urgency of addressing this issue.
- The ban on installing lead pipes began in the 1980s, revealing decades of neglect in fixing the problem despite knowing that no level of lead is safe.
- Funding for this pipe replacement initiative is available through the bipartisan infrastructure law passed in 2021.
- Public comments will be accepted for 60 days, followed by a hearing in January before the rule takes effect.
- While some cities may resist the rule, the EPA and Biden administration are pushing for it, backed by available funding.
- The key challenge may revolve around the financial responsibility for replacing lead pipes.
- Beau speculates that unless influential politicians advocate against it, the rule is likely to move forward smoothly.
- The focus should be on prioritizing public health and preventing lead poisoning, rather than financial considerations.

# Quotes

- "No acceptable safe level of lead."
- "It's better for me politically if your kids get lead poisoning."
- "Y'all have a good day."

# Oneliner

The EPA proposes a rule mandating lead pipe replacement in cities, prioritizing public health over financial concerns, with funding available from the bipartisan infrastructure law.

# Audience

Citizens, legislators, activists

# On-the-ground actions from transcript

- Contact local officials to advocate for prioritizing public health over financial interests (implied)
- Stay informed about the progress of the rule and actively participate in public comments and hearings (implied)

# Whats missing in summary

The full transcript provides additional context on the history of lead pipe issues, the urgency of addressing them, and the potential resistance from certain cities.

# Tags

#LeadPipes #EPA #Biden #PublicHealth #Infrastructure #Advocacy