# Bits

Beau says:

- Explains the importance of looking beyond administrative salaries when supporting non-profits and charities.
- Shares a personal story of a marketing fundraising professional who significantly increased donations for a struggling organization.
- Emphasizes the value of hiring skilled individuals, even if it means higher administrative salaries.
- Compares the skills of those running large logistics for nonprofits to Fortune 500 company executives.
- Suggests evaluating administrative salaries as a percentage of revenue and program expenses to determine effectiveness.
- Advises talking to individuals who have used a charity's services to gauge its impact.
- Acknowledges that high administrative salaries may seem concerning but could be justified if they contribute to the organization's goals.
- Acknowledges the necessity of resources in addressing societal issues and supporting non-profits.

# Quotes

- "Fights like that, they take resources."
- "High administrative salaries may seem concerning but could be justified if they contribute to the organization's goals."
- "Look at it in the context of the overall amount of funding."
- "You're going to see what they do firsthand."
- "It is a fight and fights like that they take resources."

# Oneliner

Beau explains the nuances of evaluating administrative salaries in non-profits and charities, stressing the importance of impact over numbers and resources in addressing societal issues.

# Audience

Supporters of non-profits

# On-the-ground actions from transcript

- Talk to individuals who have used a charity's services to understand its impact (suggested).
- Analyze administrative salaries as a percentage of revenue and program expenses to gauge effectiveness (implied).

# Whats missing in summary

The full transcript provides a nuanced perspective on evaluating administrative salaries in non-profits and charities, focusing on impact assessment beyond numbers and acknowledging the resource requirements in addressing societal challenges.

# Tags

#Nonprofits #Charities #AdministrativeSalaries #ImpactAssessment #ResourceAllocation