# Bits

Beau says:

- Representative Scott Perry's phone was seized by the federal government in connection with the January 6th probe in 2022.
- Perry claimed protection under the speech and debate clause in the Constitution to prevent access to his text messages and emails.
- A legal battle ensued, with a judge ruling against Perry's claim of protection.
- An appeals court has ordered Perry to disclose 1,659 out of 2,055 documents, withholding access to 396.
- The court found that most of the information was not protected by speech debate, but some were.
- The documents are believed to contain information on the vice president's role during certain events in Congress and election claims.
- Perry's attorney mentioned his undecided stance on appealing the decision.
- Investigators are now poised to access a significant amount of text messages and emails for the investigation.
- Despite some documents being protected, the majority will be available for scrutiny.
- The outcome may provide valuable context to the investigation.

# Quotes
- "Representative Scott Perry's phone was seized by the federal government in connection with the January 6th probe."
- "The court found that most of the information was not protected by speech debate, but some were."

# Oneliner
Representative Scott Perry's phone seized in January 6 probe, court orders disclosure of majority of documents, potentially revealing significant information.

# Audience
Legal observers, political analysts

# On-the-ground actions from transcript
- Stay informed on the updates regarding the investigation (implied).

# Whats missing in summary
Legal nuances and detailed courtroom proceedings

# Tags
#ScottPerry #LegalBattle #SpeechAndDebate #Investigation #Transparency