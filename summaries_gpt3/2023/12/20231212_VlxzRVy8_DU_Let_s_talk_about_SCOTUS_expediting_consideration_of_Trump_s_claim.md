# Bits

Beau says:

- Supreme Court considering Trump's claim of presidential immunity on an expedited schedule.
- Appeals court reviewing the case quickly as well.
- Trump given until the 20th to respond, and until Wednesday by the appeals court.
- Beau previously argued that presidential immunity as claimed by Trump is not supported by the Constitution.
- Beau received a message asserting that presidential immunity is established in case law and referenced previous investigations.
- Beau refutes the argument by clarifying that there has never been a ruling on criminal immunity for presidents.
- The Office of Legal Counsel memo cited by the message does not carry the force of law and can be changed.
- Beau mentions a precedent in Nixon v. Fitzgerald where the president was immune in civil liability cases within their official duties.
- However, this immunity does not extend to pre-presidency matters or criminal cases.
- Beau questions the idea of full immunity for presidents and how it could potentially lead to abuse of power.
- He points out the absurdity of suggesting that presidents have total immunity for any action.
- Beau underscores the importance of not granting unlimited power based on partisan support.
- He expresses skepticism about the Supreme Court taking up the case due to the weak argument presented.
- Beau concludes by discussing the ongoing legal proceedings and speculates on the Supreme Court's decision.

# Quotes

- "Presidents are not kings."
- "There's no case law, there's no precedent, there's nothing like that."
- "If you're saying they can literally do anything they want because they sold you a hat and a bunch of rhetoric you might want to revaluate."
- "Presidents don't have full immunity for anything that they do."
- "Y'all have a good day."

# Oneliner

Supreme Court fast-tracking consideration of Trump's presidential immunity claim, prompting critical analysis from Beau on the limits of presidential power.

# Audience

Legal scholars, political analysts, concerned citizens.

# On-the-ground actions from transcript

- Contact legal advocacy organizations to stay informed on updates regarding the Supreme Court and appeals court decisions (implied).

# Whats missing in summary

Deeper insights into the potential implications of granting presidents full immunity for their actions.

# Tags

#Trump #PresidentialImmunity #SupremeCourt #LegalAnalysis #ChecksAndBalances