# Bits

Beau says:

- Reports a 12% decline in the murder rate, which is significant and might be a record.
- Mentions that despite surveys showing 77% of Americans believe crime is increasing, the numbers indicate otherwise.
- Points out the influence of media coverage in shaping perceptions of reality.
- Addresses statistics claiming 13% of a certain demographic (Black people) are responsible for 52% of murders.
- Disputes the accuracy of these statistics, explaining that they only represent cleared murder cases, not all murders.
- Raises concerns about missing data from unsolved cases and individuals who simply go missing.
- Challenges the notion that skin tone is linked to likelihood of committing murder, citing economic factors and poverty as more relevant indicators.
- Suggests a correlation between income inequality, poverty, and crime rates leading to higher murder rates.
- Poses questions about the impact of economic issues in 2020 and subsequent government relief efforts on crime rates.
- Encourages viewers to think critically about causation versus correlation in data interpretation.

# Quotes

- "It's weird."
- "The only interesting thing about it, when you actually get down and start going through the information, the real disparity I see is that people with brown eyes are far more likely to commit a murder."
- "100% of people who do not know the difference between correlation and causation will die."

# Oneliner

Beau breaks down misleading statistics on murder rates, challenges preconceptions on demographics and crime, and underscores the importance of understanding causation versus correlation.

# Audience

Data Consumers, Critical Thinkers

# On-the-ground actions from transcript

- Fact-check statistics before accepting and sharing them (suggested).
- Advocate for accurate representation of data in media coverage (implied).
- Support initiatives addressing income inequality and poverty to reduce crime rates (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how statistics on murder rates can be misleading and the importance of considering socioeconomic factors in understanding crime trends.

# Tags

#Statistics #Crime #Causation #Correlation #MediaCoverage #IncomeInequality