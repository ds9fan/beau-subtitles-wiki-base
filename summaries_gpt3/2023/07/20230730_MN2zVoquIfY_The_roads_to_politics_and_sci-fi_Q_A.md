# Bits

Beau says:

- Beau is doing another Rhodes Q&A, which will now likely occur weekly due to the influx of questions.
- Questions are being organized thematically, with a mix of political and science fiction-related queries.
- Topics discussed include frozen roundworms coming back to life, Twitter's rebranding, the idea of a monument to Emmett Till, and the parallels between Star Trek episodes and current events.
- Beau shares insights on Ukraine's troop strength, Twitter rebranding, and the potential for a general strike.
- Beau also addresses the ICC's limitations in arresting political figures like George W. Bush or Dick Cheney.
- Beau expresses surprise at Trump's 2016 win, discussing voter turnout and the impact of negative sentiments towards Hillary Clinton.
- Beau touches on the challenges of off-grid living in Colorado based on a tragic incident involving a group's failed attempt.
- Beau shares thoughts on Mitch McConnell's handling of Trump within the Republican Party and Hulu's staggered release of Futurama episodes.

# Quotes

- "I think that a monument to Till would do better at reminding people of the history rather than the mythology."
- "Stuff on YouTube is edited. When you're talking about anything that is permanent, you have to actually try this stuff in a more controlled setting."
- "If he can't resolve this problem before he finishes his time in office, his legacy is going to be that of the Senate majority leader who enabled Trump."

# Oneliner

Beau delves into various topics, from frozen roundworms to Trump's presidency, providing insights on current events and community networks.

# Audience

Viewers interested in diverse perspectives on current events and community engagement.

# On-the-ground actions from transcript

- Research and support local community networks (suggested)
- Advocate for accurate and comprehensive survival education (implied)
- Stay informed about political figures' actions and their implications (implied)

# Whats missing in summary

Insights into Beau's perspectives on Trump's presidency and Mitch McConnell's handling of the Republican Party.

# Tags

#CommunityEngagement #CurrentEvents #YouTube #PoliticalInsights #SurvivalEducation