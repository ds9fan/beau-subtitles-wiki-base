# Bits

Beau says:

- Explains the role of third-party candidates in the presidential election process.
- Stresses that third-party candidates are mainly about messaging and not actually winning.
- Envisions a scenario where a third-party candidate miraculously wins the presidency and poses the question of what happens next.
- Analyzes the limitations and challenges a third-party president might face within the existing government structure.
- Argues that real change comes from building power structures outside of traditional political parties at the local level.
- Advocates for getting involved in the Democratic Party at the grassroots level to push for progressive change.
- Points out the success of movements like the Tea Party in influencing party dynamics over time.
- Emphasizes the need for long-term, grassroots efforts to bring about meaningful systemic change.
- Encourages focusing on community networks to influence electoral outcomes effectively.
- Concludes by stressing the importance of building sustainable power structures rather than relying on a presidential candidate to bring about significant change.

# Quotes

- "A savior is not coming to fix the United States."
- "The real solution is to build your own power structure that isn't linked to a political party."
- "You will not usher in some glorious revolution, you will not get deep systemic change by recreating the exact same system that brought us here."
- "The system that exists wants to maintain the system that exists because it benefits them."
- "You have to start at the bottom and you have to build it up."

# Oneliner

Beau explains the limitations of third-party presidential candidates, advocating for grassroots power-building outside traditional political structures for meaningful change.

# Audience

Activists, Political Reformers

# On-the-ground actions from transcript

- Start from local levels: County Commission, City Council, School Board, State Representatives, House of Representatives, Senate (suggested)
- Build community networks to influence electoral outcomes (implied)
- Get involved in Democratic Party at local levels to push for progressive change (exemplified)

# Whats missing in summary

Beau's detailed breakdown and analysis of the limitations and challenges faced by third-party presidential candidates, urging for a focus on grassroots power-building for impactful change.

# Tags

#ThirdParty #PresidentialElection #Grassroots #PoliticalReform #CommunityBuilding