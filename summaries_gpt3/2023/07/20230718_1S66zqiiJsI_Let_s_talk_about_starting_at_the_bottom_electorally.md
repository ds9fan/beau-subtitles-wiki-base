# Bits

Beau says:

- Addressing electoral politics and the need for work to achieve change.
- Starting at the bottom is key to creating real progress in the political system.
- Progressives moving up through the Democratic Party can lead to a more progressive establishment.
- Emphasizing the importance of starting at the bottom rather than jumping straight to the top.
- The power lies in Congress, making Senate and House positions vital for progressive change.
- Advocating for a strategic approach to implementing change through political careers at local and state levels.
- Acknowledging the need for reforms in the US electoral system.
- Stating that building one's power structure is necessary to combat existing political machines.
- Explaining the significance of building a power structure outside of traditional political parties.
- Stressing the importance of more than just voting for a third-party candidate to bring about systemic change.

# Quotes

- "Starting at the bottom is key to creating real progress in the political system."
- "You have to build your own power structure."
- "A dream without a plan, it's never going to happen."
- "Building a power structure outside of a political party [...] That's the way forward."
- "It requires a lot more work."

# Oneliner

Beau explains the importance of starting at the bottom in electoral politics to create real change and build a power structure outside traditional parties.

# Audience

Activists, Political Organizers

# On-the-ground actions from transcript

- Join organizations working towards systemic change (implied)
- Build a power structure outside traditional political parties (implied)

# Whats missing in summary

Beau's detailed insights on the necessity of starting at the bottom in politics are best experienced in the full transcript.

# Tags

#ElectoralPolitics #ProgressiveChange #PowerStructure #SystemicChange #PoliticalActivism