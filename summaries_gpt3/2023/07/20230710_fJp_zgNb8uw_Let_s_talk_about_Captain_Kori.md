# Bits

Beau says:

- Updates on Captain Cory's journey are being shared.
- Captain Cory, a young man passionate about pirates, received help to fulfill a bucket list item.
- He wanted a YouTube play button and support poured in from various sources to make it happen.
- Captain Cory has passed away, but he lived his dream in the last six months of his life.
- His family is requesting space at the moment to cope with the loss.
- His mother intends to keep the channel running as a memorial to him.
- Messages of support can be left on his channel, "a crack in the box."
- The news might not be what everyone wanted to hear, but it was somewhat expected.
- Despite the sad news, there is solace in knowing that Captain Cory lived his dream.
- The community's support played a significant role in fulfilling Captain Cory's dream.

# Quotes

- "He really did get his dream for six months."
- "I know that this is not the news everybody wanted to hear."
- "Take a little bit of solace in the fact that he really did get his dream for six months."
- "It was due to the people who are going to be pretty upset by this news."

# Oneliner

Updates on Captain Cory's dream fulfillment and passing, with a call for messages of support on his memorial channel.

# Audience

Online community members

# On-the-ground actions from transcript

- Leave a message on Captain Cory's channel, "a crack in the box" (suggested)
- Visit his channel to wish him a good voyage (suggested)

# Whats missing in summary

The full transcript gives a more detailed background on Captain Cory's dream fulfillment and the impact of community support.

# Tags

#CaptainCory #Pirates #Dreams #Support #Community