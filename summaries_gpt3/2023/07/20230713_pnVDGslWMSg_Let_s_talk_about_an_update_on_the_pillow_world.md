# Bits

Beau says:

- Beau provides an update on the aftermath of the 2020 election, focusing on the MyPillow CEO, Mike Lindell, and his claims.
- MyPillow's financial situation seems dire as they are subleasing manufacturing space and selling surplus equipment.
- Major retailers dropped MyPillow products post-January 6th, impacting the company's revenue.
- Lindell believes that retailers like Bed Bath and Beyond made decisions based on not carrying MyPillow products.
- Lindell remains confident in the lawsuits against Dominion and Smartmatic, claiming he has evidence that will vindicate him.
- He compares his situation to the movie "My Cousin Vinny" in terms of evidence presentation.
- Beau suggests keeping track of this community as they might reenter the news cycle due to Lindell's financial situation.
- Voting machine companies might be willing to settle for less due to Lindell's financial struggles.
- Despite overwhelming evidence against the claims, a community continues to believe in the election fraud allegations.
- Beau hints at a potential resurgence of this topic in the news cycle soon.

# Quotes

- "He compares his situation to the movie 'My Cousin Vinny.'"
- "There's still a pretty active community looking into the voting claims."
- "It's the kind of stuff that when you read through it just makes your mind melt."

# Oneliner

Beau gives an update on MyPillow's financial struggles post-election, Lindell's confidence in lawsuits, and the persistent community still promoting election fraud claims.

# Audience

News consumers

# On-the-ground actions from transcript

- Stay informed on the developments in the aftermath of the 2020 election (implied).
- Be critical of misinformation and false claims regarding election fraud (implied).

# Whats missing in summary

Detailed analysis and context on the aftermath of the 2020 election and its impact on MyPillow and Mike Lindell.

# Tags

#ElectionFraud #MyPillow #MikeLindell #NewsUpdate #CommunityBeliefs