# Bits

Beau says:

- Beau introduces the topic of discussing the 10 worst states to work and live based on empirical data from CNBC.
- The ranking grades states based on their welcoming environment for workers and families, indicating potential future business developments.
- States like Florida, Arkansas, Tennessee, and Indiana received low grades, with Texas surprisingly ranking as the worst state.
- The methodology behind the ranking considers inclusiveness as a key factor in attracting workers to a state.
- Beau points out the recent changes in inclusiveness levels in many states, impacting their rankings.
- He hints at a common factor among these poorly ranked states that might explain their low scores.

# Quotes

- "And number one will surprise you."
- "Inclusiveness is an important part of whether or not workers want to come to your state."
- "There's an interesting thing about it if you really look."
- "Y'all have a good day."

# Oneliner

Beau reveals the 10 worst states to work and live, with Texas surprisingly ranking at the bottom due to lacking inclusiveness for workers.

# Audience

Employed individuals seeking welcoming work environments.

# On-the-ground actions from transcript

- Research and understand the metrics used by CNBC to rank states (suggested).
- Advocate for inclusivity and diversity in your workplace and community (implied).

# Whats missing in summary

In the full transcript, Beau provides insights into the importance of inclusiveness in attracting workers and families to states, shedding light on the factors contributing to the rankings.

# Tags

#States #WorkEnvironment #Inclusiveness #BusinessDevelopment #Ranking