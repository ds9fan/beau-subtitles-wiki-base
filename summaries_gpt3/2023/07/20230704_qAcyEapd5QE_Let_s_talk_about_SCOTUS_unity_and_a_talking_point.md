# Bits

Beau says:

- Explains a talking point emerging about separating the T-Q from L-G-B within the LGBTQ community.
- Notes that this idea is being pushed by those sympathetic to the right, suggesting that distancing from the T is better for the LGB.
- Warns against falling for this tactic of divide and conquer used by authoritarian regimes throughout history.
- Mentions a Supreme Court decision that wasn't just about the T, and urges not to be convinced that the targeting won't extend to others in the LGBTQ community.
- Emphasizes that authoritarian groups always move on to target the next group, so division within the community is not a solution.
- Stresses the importance of not being misled and manipulated by such tactics, referencing the Supreme Court ruling as evidence.
- Concludes by warning against trying to appease authoritarian figures by sacrificing parts of the community.

# Quotes

- "Don't let them convince you that they're not coming for you too."
- "There's always a next group for authoritarians, for people of that mindset, and you will not turn that authoritarian tiger into a vegetarian by feeding it your friends."

# Oneliner

Authoritarian tactics of divide and conquer within the LGBTQ community must be recognized and resisted, as unity is vital against oppressive forces.

# Audience

Advocates for LGBTQ rights.

# On-the-ground actions from transcript

- Resist attempts to divide the LGBTQ community (implied).
- Stay united and support all members of the LGBTQ community (implied).

# Whats missing in summary

The full transcript provides a deeper dive into the dangerous tactic of dividing the LGBTQ community and the importance of unity in resisting oppressive forces.