# Bits

Beau Young says:

- Senator Tuberville has been holding up high-level military promotions, including those of the United States Marines.
- The senator's actions have been undermining US military readiness because of his opposition to the branches of the military caring for their personnel's reproductive rights.
- Effective tomorrow, the United States Marines will not have a confirmed commandant due to Senator Tuberville's actions.
- This situation shifts from undermining military readiness to undermining separation of powers and foundational constitutional principles.
- A vacant leadership position in the Marines will result in someone becoming the acting commandant, with significant authority despite lacking confirmation.
- Senator Tuberville's refusal to confirm nominees for high-profile military positions jeopardizes civilian control of the military and undermines checks and balances.
- By not consenting to nominations, Senator Tuberville establishes a precedent where positions are filled by acting officials, diminishing the Senate's role.
- This situation ultimately hands control of the military to President Biden, bypassing the Senate's advice and consent.
- The precedent set by acting appointments weakens the Senate's authority and consolidates power with the President and Secretary of Defense.
- Senator Tuberville's actions may risk U.S. military readiness and the lives of service members and civilians worldwide.

# Quotes

- "His little stunt is jeopardizing civilian control of the military."
- "It's going to show that the Senate has no power here."
- "Effective tomorrow, Senator Tuberville has handed all control of the military to Biden."
- "I'm sure they don't really care about that, but I bet they care about the fact that Tuberville is handing Biden all the power."
- "Anyway, it's just a thought, y'all have a good day."

# Oneliner

Senator Tuberville's obstruction of military promotions jeopardizes civilian control and hands power to the President, undermining constitutional principles and risking military readiness.

# Audience

Congressional constituents

# On-the-ground actions from transcript

- Contact Senator Tuberville's office to express concerns about his obstruction of military promotions and its implications for civilian control (suggested).
- Support organizations advocating for upholding constitutional principles and the importance of Senate oversight in military appointments (exemplified).

# Whats missing in summary

The full transcript provides a detailed breakdown of how Senator Tuberville's actions impact military leadership, civilian control, and constitutional principles, offering a comprehensive understanding of the situation.

# Tags

#SenatorTuberville #MilitaryPromotions #CivilianControl #ConstitutionalPrinciples #USMarines