# Bits

Beau says:

- Introduces a debate between American leftists and liberals about civic engagement.
- Describes a situation where a leftist friend is trying to convince a liberal friend to participate in mutual aid.
- The liberal friend believes in electoral politics and doesn't see the effectiveness of mutual aid.
- Beau suggests engaging in the other's preferred form of civic engagement to bridge the gap.
- Emphasizes the importance of experiencing mutual aid firsthand to understand its effectiveness.
- Encourages trying to change the liberal friend's perspective by involving them in mutual aid activities.
- Advises removing barriers to understanding by allowing the liberal friend to see the impact of mutual aid.
- Acknowledges the different perspectives between leftists and liberals on civic engagement.
- Urges experimentation and understanding between friends with differing political views.
- Recommends taking actions to bridge the gap and show the effectiveness of mutual aid.

# Quotes

- "If she shows up, she will see that mutual aid is absolutely effective. It does work."
- "You won't do the easiest thing, the thing that to her is effective and easy."
- "But oftentimes people need to see that for themselves."
- "Because if she sees it with her own eyes, she'll believe that too, right?"
- "Remove it. that would be my suggestion."

# Oneliner

Beau suggests bridging the gap between leftists and liberals by experiencing each other's preferred forms of civic engagement, aiming to show the effectiveness of mutual aid.

# Audience

Friends with differing political views

# On-the-ground actions from transcript

- Experiment with each other's preferred form of civic engagement (suggested)
- Invite the liberal friend to participate in mutual aid activities (implied)
- Allow the liberal friend to see the impact of mutual aid firsthand (implied)

# Whats missing in summary

The full transcript provides a detailed guide on how to navigate differing political perspectives within friendships and advocates for practical experiences to understand the effectiveness of mutual aid.

# Tags

#CivicEngagement #Leftists #Liberals #MutualAid #PoliticalPerspectives