# Bits

Beau says:

- Beau talks about the attorney general in Arizona, the Supreme Court, and the spirit of Andrew Jackson, which is not typically desirable.
- Andrew Jackson is quoted as saying, "John Marshall has made his decision. Now let him enforce it," after disagreeing with a Supreme Court decision.
- The current attorney general in Arizona, Mays, a former Republican, seems to embody Jackson's statement by encouraging complaints of discrimination in public accommodations.
- Beau suggests that many viewers may not disagree with the attorney general's position on enforcing Arizona's public accommodation law.
- Beau doubts that businesses, especially creative ones, hold bigoted opinions to the extent of discriminating against individuals based on race, gender, or other factors.
- He believes that it is unlikely for this issue to escalate because he doesn't think many businesses share such extreme opinions.
- If future complaints arise, Beau predicts that they may end up back in the Supreme Court due to Attorney General Mays' stance on enforcement.
- The tone suggests that the Supreme Court lacks the ability to enforce decisions beyond bringing matters back to court, requiring broader federal government efforts for enforcement.
- Beau hints at deeper philosophical considerations regarding the rule of law and constitutionality but leaves them for another day.
- In closing, Beau shares his thoughts and wishes everyone a good day.

# Quotes

- "John Marshall has made his decision. Now let him enforce it."
- "If any Arizonan believes that they have been the victim of discrimination, they should file a complaint with my office."
- "It appears that Attorney General Mays has said the Supreme Court has made their decision, now let them enforce it."

# Oneliner

Beau talks about Attorney General Mays embodying Andrew Jackson's spirit against a Supreme Court decision on discrimination complaints in Arizona, doubting businesses' extreme opinions.

# Audience

Legal activists

# On-the-ground actions from transcript

- File complaints with the Attorney General's office in Arizona if you believe you have faced discrimination (suggested).
- Stay informed about developments regarding discrimination complaints and legal actions in Arizona (exemplified).

# Whats missing in summary

Deeper exploration of philosophical aspects related to the rule of law and constitutionality discussed by Beau. 

# Tags

#AttorneyGeneral #Arizona #SupremeCourt #Discrimination #Enforcement