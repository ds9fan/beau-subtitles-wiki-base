# Bits

Beau says:

- The US Navy is under scrutiny for its historical names following the lead of other military branches.
- Generals who joined the Confederacy are being stripped of their names from military installations.
- Concerns arise as black fathers question ships named after segregationists where their sons serve.
- Unlikely for the Navy to change ship names due to tradition, despite ongoing movement and discourse.
- The renaming of army bases took years of debate before action was taken.
- Possibility that ship names won't be passed on to new ships upon retirement.
- Some sailors serving on these ships may retire before the ship itself.
- Navy's tradition and historical importance in naming ships make change unlikely.
- Naming after historical figures due to their significance in Navy's history might deter name changes.
- Beau suggests starting with senators for any petition or action towards change.

# Quotes

- "I don't think you're wrong."
- "Everything's impossible until it's not."
- "It seems really unlikely that they change the name of a major ship."

# Oneliner

The US Navy faces scrutiny for historical names, unlikely to change despite ongoing movement and discourse.

# Audience

Families of service members

# On-the-ground actions from transcript

- Petition senators for ship name changes (suggested)
- Join the movement to change ship names (implied)
- Advocate within the Navy for renaming ships (exemplified)

# Whats missing in summary

The full transcript provides deeper insights into the historical context and challenges faced in renaming Navy ships named after segregationists.