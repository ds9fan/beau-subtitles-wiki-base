# Bits

Beau says:

- Russia is strengthening its National Guard, a move commonly made by authoritarian leaders to ensure loyalty.
- Western focus is on the impact of strengthening the National Guard on the Russian war effort.
- Overlooked is the transfer of special units, with skills in undercover operations and dynamic entries, to the National Guard.
- These special units possess skills directly transferable to targeting critics of Putin and undermining opposition.
- Putin may be planning to target individuals less loyal to him using these special units.
- The special units selected are likely chosen for their ability to keep quiet and avoid leaking information.
- The potential use of these units to target critics and opposition should not be ignored by both Russians and the West.

# Quotes

- "Russia is strengthening its National Guard, a move commonly made by authoritarian leaders to ensure loyalty."
- "The potential use of these units to target critics and opposition should not be ignored by both Russians and the West."

# Oneliner

Russia's strengthening of the National Guard and transfer of special units may indicate plans to target critics of Putin, a concerning development for both Russians and the West.

# Audience

Russians, Western observers

# On-the-ground actions from transcript

- Stay informed about developments in Russia's National Guard and special unit transfers (implied)
- Raise awareness about potential targeting of critics and opposition by Putin's regime (implied)

# Whats missing in summary

Insight into the potential implications for critics of Putin and opposition movements in Russia, as well as the importance of vigilance and preparedness in response to these developments.

# Tags

#Russia #Putin #NationalGuard #Authoritarianism #Critics