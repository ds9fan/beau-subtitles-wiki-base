# Bits

Beau says:

- Explains the connection between the Supreme Court and electoral politics, addressing a message expressing hopelessness towards electoral politics and the Supreme Court's actions.
- Points out that the average retirement age for Supreme Court justices is around 80, with Thomas and Alito being among the oldest.
- Mentions the possibility of Thomas and Alito retiring soon, potentially impacting the court's decisions.
- Emphasizes that the Supreme Court justices are products of electoral politics, even though individuals do not directly vote for them.
- Notes that electoral politics influence the makeup of the court and that future retirements could shift the court's dynamics.

# Quotes

- "But you can't separate what the Supreme Court does from electoral politics."
- "The Supreme Court, the makeup of the court, is a product of electoral politics."
- "The average retirement age for a Supreme Court justice is 80 I think."
- "You can't say that electoral politics has nothing to do with the Supreme Court."
- "It's worth remembering that the two eldest justices are members of the block that are making the decisions most people watching this channel really disagree with."

# Oneliner

Beau explains how electoral politics and the Supreme Court are intertwined, showcasing the potential impact of future retirements on court decisions and underscoring the influence of electoral processes.

# Audience

Voters, Activists, Political enthusiasts

# On-the-ground actions from transcript

- Stay informed about the ages and potential retirements of Supreme Court justices (implied).
- Engage in electoral politics to influence future appointments to the Supreme Court (implied).
- Support candidates who prioritize judicial appointments that resonate with your values (implied).

# Whats missing in summary

The full transcript provides detailed insights into the relationship between electoral politics and the Supreme Court, offering a nuanced perspective on how future retirements could shape the court's decisions and the importance of understanding this connection.

# Tags

#SupremeCourt #ElectoralPolitics #Retirement #Influence #Voting