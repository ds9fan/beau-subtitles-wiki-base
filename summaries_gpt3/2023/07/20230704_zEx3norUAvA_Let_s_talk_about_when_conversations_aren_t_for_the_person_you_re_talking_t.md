# Bits

Beau says:

- Returned home to a semi-rural conservative town in Norcal after several years, attending church with his mom.
- Mom's pastor was homophobic, transphobic, and misinformed about homelessness, blaming substance use and laziness.
- Had a lengthy talk with mom about the pastor's sermon and was asked to speak with him directly.
- Views engaging with someone promoting violence and hatred as challenging within an ultra-conservative religious context.
- Recognizes the unlikelihood of changing the pastor's mindset but encourages the attempt regardless.
- Suggests having the difficult talk not to succeed but to show the importance of the issue to his mom.
- Emphasizes that the goal isn't to change the pastor but to influence his mom's church choice.
- Warns of the pastor potentially leading his mom down a harmful path if not addressed early.
- Advocates for having the tough and possibly ineffective talk as a way to protect his mom's beliefs.
- Compares conversing with the pastor to responding to comments online: impact may not be immediate but can influence others.

# Quotes

- "The sad fact is that preaching hatred and violence and bigotry in Jesus' name, that is a moneymaker."
- "Not because you'll succeed, but because you'll fail."
- "A conversation is not for the pastor. A conversation is for your mom."
- "It's probably worth it, probably worth the time."

# Oneliner

Beau returns home to confront a homophobic pastor, urging a difficult talk with mom not to change the pastor's mind but her church choice.

# Audience

Family members, allies

# On-the-ground actions from transcript

- Talk to your family directly about harmful beliefs in their community (suggested)
- Guide family towards more inclusive and welcoming environments (suggested)

# Whats missing in summary

The emotional weight and importance of standing up against harmful beliefs within close-knit communities.

# Tags

#Family #Community #Homophobia #ConservativeTown #Religion