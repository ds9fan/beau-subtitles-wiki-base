# Bits

Beau says:

- Governor Tony Evers used his super veto power in Wisconsin in a unique and humorous way.
- He slashed a GOP tax cut for the rich from 3.5 billion down to 175 million.
- The wealthy in Wisconsin are not getting the tax cuts that other people are receiving.
- Evers vetoed the cuts for the highest tax brackets, showcasing an interesting use of power.
- He creatively manipulated a part of the legislation that allocated extra revenue for schools.
- By vetoing specific characters in the legislation, Evers extended the funding until the year 2425.
- Some may view Evers' actions as an abuse of executive power, but Beau finds it impressive.
- Republicans in Wisconsin are likely unhappy with Evers' innovative use of the veto.
- Previous rulings have gone against similar veto maneuvers, but Evers' creativity stands out.
- With a new liberal majority in the Supreme Court, there may be a chance for Evers' actions to hold despite expected legal challenges.

# Quotes
- "400 years of increased revenue."
- "I'm not even mad. That's impressive."
- "I'm sure that some would suggest that that is an abuse of executive power."
- "Definitely some interesting news coming out of Wisconsin."
- "It's just a thought."

# Oneliner
Governor Tony Evers creatively manipulates legislation in Wisconsin, cutting tax breaks for the rich and extending school funding to the year 2425, sparking legal battles and Republican discontent.

# Audience
Wisconsin residents

# On-the-ground actions from transcript
- Support legal battles against any abuse of power (implied)

# Whats missing in summary
The full transcript provides detailed insights into Governor Tony Evers' unique use of veto power and the potential legal ramifications, offering a deeper understanding of the political landscape in Wisconsin.