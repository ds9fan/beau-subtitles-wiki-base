# Bits

Beau says:

- Supreme Court struck down affirmative action for college admissions, sparking talk about legacy admissions.
- Legacy admissions offer preferential treatment to students whose parents attended the university.
- Harvard is currently being challenged on legacy admissions.
- Ivy League schools have a high percentage of legacy admissions, ranging from 25-35%.
- Legacy admissions make students 45% more likely to be admitted compared to those who are not legacies.
- Beau references a meme with a wealthy business owner, a white worker, and a black worker at a table with cookies to explain the concept.
- Wealthier individuals, not marginalized groups, benefit from legacy admissions and make admissions less competitive.
- Beau criticizes rich white individuals for misleading conservative white workers into believing that marginalized groups are taking opportunities away from them.
- He urges conservatives to understand that rich individuals are often the ones manipulating biases and bigotry for their advantage.
- Beau encourages people to see beyond these manipulations and recognize commonalities with others rather than being pawns in a cycle of manipulation.

# Quotes

- "They're using your bias, your bigotry, to manipulate you so they can stay up on top."
- "People who have less institutional power than you do are never the source of your institutional issues."
- "Learn from this."
- "You have more in common with the black guy down the road than you are ever going to have with your representative in DC."
- "They're playing you. They're using your bias, your bigotry, to manipulate you so they can stay up on top."

# Oneliner

Supreme Court's affirmative action ruling led to scrutiny of legacy admissions, revealing how biases are manipulated to maintain privilege.

# Audience

Conservative America

# On-the-ground actions from transcript

- Challenge legacy admissions policies (implied)
- Educate others on the manipulation of biases by the privileged (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of how biases and manipulation play out in societal systems, urging individuals to critically analyze power dynamics and commonalities among different groups.

# Tags

#LegacyAdmissions #AffirmativeAction #BiasManipulation #InstitutionalIssues #Privilege