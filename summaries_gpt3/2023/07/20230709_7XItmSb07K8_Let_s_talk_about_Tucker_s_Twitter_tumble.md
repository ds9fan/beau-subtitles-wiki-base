# Bits

Beau says:

- Tucker moved from Fox to Twitter, sparking a lot of speculation.
- Tucker's impact on Twitter is less extreme compared to TV, as Twitter is already inflammatory.
- Musk's moves on Twitter may have incentivized more inflammatory behavior.
- Numbers show a decline in viewership for Tucker's Twitter episodes.
- Views steadily declined: announcement - 137 million, episode one - 120 million, episode two - 60 million, and so on.
- Another set of numbers indicates a decline in people watching Tucker's videos for more than two seconds.
- Concerns about Tucker becoming more extreme without Fox seem unfounded due to the declining viewership.
- The decline could signal a wider issue within the conservative movement.
- Viewers of Fox, considered moderate Republicans, may be moving away from extreme factions.
- People exposed to extreme content on Fox may not actively seek it out, a potentially positive sign.

# Quotes

- "Twitter is Thunderdome."
- "Just because somebody viewed the tweet doesn't mean they watched any of the video."
- "There are a lot of people who really express a lot of concern that without Fox holding him back, that he was going to be even more of a right-wing cultural juggernaut."
- "It definitely appears that there's a decline and it's pretty substantial."
- "This could be a sign that people are moving away from the more extreme factions of the Republican Party."

# Oneliner

Tucker's move to Twitter shows declining viewership, potentially signaling a shift away from extreme conservatism.

# Audience

Political analysts, social media users

# On-the-ground actions from transcript

- Analyze viewership trends to understand political shifts within the conservative movement (implied).
- Encourage critical consumption of media content and its potential impact on political ideologies (suggested).

# Whats missing in summary

Insights on the overall impact of Tucker's declining viewership on conservative media landscape.

# Tags

#ConservativeMovement #TuckerCarlson #TwitterImpact #PoliticalShifts #MediaConsumption