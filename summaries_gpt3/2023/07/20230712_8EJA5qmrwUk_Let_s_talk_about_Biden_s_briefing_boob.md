# Bits

Beau says:

- President Biden's use of profanity and shouting has become a topic of interest, particularly among right-wing media.
- Fox News claims Biden's profanity undermines his "well curated image," despite instances of him using profanity publicly in the past.
- Biden's go-to phrases when frustrated with his staffers include "how the heck did you not know that, man?" and "don't hecking BS me."
- Beau defends Biden's demanding approach to accurate information, suggesting it's a positive change from the previous administration's penchant for inaccurate information.
- Biden prefers briefings in plain, everyday language, and he requests translations of technical jargon into understandable terms.
- Beau humorously points out the absence of reported ketchup incidents in the White House amidst all the focus on Biden's use of profanity.

# Quotes

- "How the heck did you not know that, man?"
- "Don't hecking BS me."
- "Welcome to a White House that expresses urgency about things other than golf tee times, taking documents and cooing."
- "I personally think this is a nice change."
- "Y'all have a good heckin' day."

# Oneliner

President Biden's use of profanity sparks controversy, but demanding accurate information and translating technical jargon show a refreshing approach in the White House.

# Audience

Media consumers

# On-the-ground actions from transcript

- Translate technical jargon into everyday language (exemplified)

# What's missing in summary

The full transcript provides a detailed and humorous analysis of the controversy surrounding President Biden's use of profanity, showcasing a contrasting approach to demanding accurate information in the White House.

# Tags

#PresidentBiden #Profanity #Accuracy #Translation #WhiteHouse