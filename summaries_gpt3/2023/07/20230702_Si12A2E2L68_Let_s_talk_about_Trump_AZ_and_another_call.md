# Bits

Beau says:

- Talking about Arizona, Trump, and a breaking story similar to the famous phone call in Georgia.
- Allegations that Trump or his surrogates reached out to the Arizona governor asking for votes.
- Questions arise about criminal investigation and potential election interference.
- Speculation on why the story is surfacing now after a long time of silence.
- Possibility that media got wind of the story because the feds were already aware and involved.
- Uncertainty on whether the individuals involved have already spoken to investigators.
- Likelihood of investigators getting involved due to the potential pattern of reaching out to state officials to influence outcomes.
- Mention of a recent interview with Raffensperger relating to election interference.
- Implication that if not already, individuals will soon be discussing the matter with investigators.
- Concluding with a thought on the unfolding situation.

# Quotes

- "Hey, find me some, find me some votes."
- "Maybe something happened to make them feel comfortable talking about it in public."
- "Do we know that? No, no we don't."
- "They will be discussing it with them soon."
- "Anyway, it's just a thought."

# Oneliner

Beau talks about allegations of Trump or his surrogates reaching out to the Arizona governor for votes, raising questions of criminal investigation and potential election interference, speculating on the timing and involvement of investigators.

# Audience

News consumers

# On-the-ground actions from transcript

- Contact local representatives for updates on the situation (implied)
- Stay informed about developments in the story (implied)
- Support transparency and accountability in political processes (implied)

# Whats missing in summary

Insights on the possible implications of the emerging story and its impact on electoral integrity.

# Tags

#Arizona #Trump #ElectionInterference #Investigation #MediaCoverage