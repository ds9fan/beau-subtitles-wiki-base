# Bits

Beau says:

- Talks about confirmation bias and how it can lead people into information silos.
- Shares a personal experience related to confirmation bias and hiring practices.
- Mentions how confirmation bias can make people believe objectively false things.
- Explains how the belief system about gender norms can impact hiring practices.
- Mentions a report showing that people with they/them pronouns on their resumes were 8% less likely to get a call back.
- Raises questions about whether this bias is specific to non-binary people or just about listing pronouns.
- Urges deeper investigation beyond headlines to avoid falling for incomplete or misleading information.
- Suggests that putting pronouns on resumes may induce fear in hiring managers leading to potential bias.
- Advises against completely omitting pronouns from resumes but acknowledges the slight impact it may have on call-back rates.
- Encourages putting pronouns on resumes even for those who fit neatly into traditional gender norms to reduce fear and unknown factors in hiring decisions.

# Quotes

- "Everybody wants to confirm what they already believe."
- "It confirmed what I believe, but that's not really what the information shows."
- "The key element to avoiding confirmation bias and falling for bad information is to basically assume it's always going to be wrong and continue to look further into it."
- "Read the source material; it's a very good guard against falling for and consuming bad information."
- "It just links it to the presence of pronouns."

# Oneliner

Beau explains confirmation bias in hiring practices, urging deeper investigation beyond headlines to avoid falling for incomplete information, and encourages putting pronouns on resumes to reduce unknown factors in hiring decisions.

# Audience

Job seekers, HR professionals.

# On-the-ground actions from transcript

- Read source material (guard against bad information) (suggested).
- Put pronouns on resumes (reduce unknown factors in hiring decisions) (suggested).

# Whats missing in summary

The full transcript provides a detailed exploration of confirmation bias in hiring practices and the importance of thorough investigation beyond headlines to avoid incomplete or misleading information.

# Tags

#ConfirmationBias #HiringPractices #Pronouns #Inclusivity #GenderNorms