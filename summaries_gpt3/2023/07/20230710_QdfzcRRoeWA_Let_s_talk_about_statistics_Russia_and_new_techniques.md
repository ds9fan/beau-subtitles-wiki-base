# Bits

Beau says:

- Addressing statistical analysis, Russia, Ukraine, and new techniques in obtaining accurate numbers.
- Questioning the accuracy of reported numbers due to countries having incentives to manipulate data.
- Exploring the origin of the numbers and the methodologies used to determine casualties.
- Contrasting Russia's claim of 6,000 losses with Ukraine's estimate of six figures.
- Describing the analysis of military funerals' photos and excess inheritance cases to ascertain casualty figures.
- Emphasizing that the numbers obtained represent the minimum, not the total count.
- Pointing out that various factors could make the actual casualty count higher than reported.
- Expressing disappointment and disapproval towards unnecessary conflict and resulting casualties.
- Acknowledging viewer support for Ukraine while expressing concern over the human cost.
- Commenting on the effectiveness of the innovative techniques used to estimate casualties.

# Quotes

- "It's not an accurate depiction of the whole picture."
- "None of this was necessary. This was elective."
- "I hope you all have a good day."

# Oneliner

Beau dives into the techniques and challenges of obtaining accurate casualty numbers, revealing the gruesome reality behind statistics in conflicts.

# Audience

Analytical viewers

# On-the-ground actions from transcript

- Advocate for transparency and accountability in reporting casualty numbers (implied).
- Support organizations working towards peace and conflict resolution (implied).
- Raise awareness about the human cost of conflicts through education and activism (implied).

# Whats missing in summary

The emotional impact and detailed analysis provided in Beau's full commentary.

# Tags

#Statistics #Conflict #Casualties #Russia #Ukraine #Transparency