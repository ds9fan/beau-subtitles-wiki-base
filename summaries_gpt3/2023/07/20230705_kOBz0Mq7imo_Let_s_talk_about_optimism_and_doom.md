# Bits

Beau says:

- Beau received a message questioning his optimism after discussing the Supreme Court not being as right-wing as perceived.
- The message mentions recent rulings pushing the US closer to discriminatory practices against LGBTQ individuals and expanding Christian rights.
- The message also criticizes Beau's mindset, suggesting his optimism may be due to his identity as a white Cishet man compared to the writer, who is trans and living through difficult times.
- Beau clarifies that he considers himself a realist, not an optimist.
- He dislikes the doomer mindset that accepts a bleak reality without seeking change.
- Beau stresses the importance of not succumbing to doomerism, which he sees as surrendering to the status quo.
- He encourages adopting confidence to make a difference and change the outcome.
- Beau urges against expecting catastrophes constantly and advocates for long-term thinking when aiming for societal change.
- He points out the need to challenge the doomer philosophy that can lead to inaction or mere talk without action.
- Beau reminds the listener not to give up and to believe in their ability to effect change.


# Quotes

- "I'm not an optimist. I'm a realist."
- "Being a doomer is giving up. It's surrender."
- "If you want to change the world, I need you to adopt the absolutely unearned self-confidence of every middle-class white dude on Twitter."
- "Being a doomer to the point that the world and the U.S. should have you at is just surrender to the status quo."
- "Don't give up."

# Oneliner

Beau clarifies he's a realist, not an optimist, rejecting doomerism and urging confidence to make a difference in challenging the status quo.

# Audience

Activists, Change-makers

# On-the-ground actions from transcript

- Challenge the doomer mindset by actively seeking ways to make a difference (implied).
- Refuse to accept the status quo and believe in your ability to effect change (implied).

# Whats missing in summary

The emotional connection and depth of the speaker's perspective can be best understood by watching the full transcript.

# Tags

#Realist #Doomerism #Optimism #Change #Activism