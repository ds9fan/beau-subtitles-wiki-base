# Bits

Beau says:

- Senator Graham, once an off-and-on supporter of former President Trump, is currently supporting him and making campaign appearances.
- Despite endorsing Trump and appearing in his support in South Carolina, Graham was booed off the stage.
- Beau warns the Republican Party to take note that Trump's faction is no longer about policy and principle but solely about Trump's personality.
- Courting Trump's faction poses a danger to the Republican Party as they have shifted from traditional Republican values to following Trump exclusively.
- The authoritarian embrace of Trump's faction since 2015 has the potential to take down long-standing Republican members, like Lindsey Graham in South Carolina.
- Trump is seen as a liability to the Republican Party, but many refuse to acknowledge this reality.
- Those deeply entrenched in supporting Trump are unlikely to change their views, even if indictments are successful against him.
- These individuals are now considered more Trumpist than Republican, supporting Trump alone rather than the party's policies.
- Supporting Trump may not guarantee votes from his faction, and adopting his far-right agenda could alienate moderates.
- Beau warns against mistaking social media support for actual votes and urges for a separation of Trump from the Republican Party.

# Quotes

- "Courting them is a danger to the Republican Party because eventually it will be Trump who is calling all of the shots."
- "Trump is a liability to the Republican Party. They keep trying to ignore it."
- "Even if these indictments are successful, it's not going to change their opinion of him."
- "Supporting Trump will not necessarily get you their vote and supporting Trump and his policies in that far-right agenda will absolutely lose you the moderates."
- "Don't confuse social media clicks with votes. They only support one person and it's not you."

# Oneliner

Senator Graham's booing incident in South Carolina underscores the danger of the Republican Party's shift towards Trump's personality over policy, urging a separation for those aspiring for more than blind allegiance.

# Audience

Republican Party members

# On-the-ground actions from transcript

- Recognize the shift towards personality over policy within the Republican Party and advocate for a return to core values (implied).
- Take steps to separate Trump from the Republican Party to avoid further alienation of moderates (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of Senator Graham's support for Trump, the risks of catering to Trump's faction, and the potential consequences for the Republican Party. Watching the full transcript can offer a deeper understanding of these dynamics. 

# Tags

#RepublicanParty #Trump #ShiftInPolitics #PartyPolicies #Moderates