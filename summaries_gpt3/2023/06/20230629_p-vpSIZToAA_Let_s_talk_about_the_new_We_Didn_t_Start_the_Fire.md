# Bits

Beau says:

- Fallout Boy updated the lyrics of "We Didn't Start the Fire" to reference recent events, sparking questions about its impact.
- Teachers previously used Beau's videos referencing historical events alongside the original version of the song as a teaching tool.
- The original song's significance lies in its chronological order of historical events, making it effective for teaching.
- Beau won't be analyzing the new version like he did with the original due to its lack of chronological order and pop culture references.
- The original version, despite attempting to reference significant historical events, also faced issues in standing the test of time.
- Beau predicts that the new version's focus on pop culture events will not age well compared to the original's historical references.
- The future relevance of pop culture references like Kanye and Taylor Swift in the new version is questioned.
- The original song faced similar issues in predicting future relevance, as seen with its mention of "terror on the airline" now seeming outdated.
- Beau concludes that the new version may not be as impactful as a teaching tool due to its lack of chronological order and focus on pop culture.

# Quotes

- "The reason history teachers use it is because it's a timeline."
- "The new lyrics won't stand the test of time the way the original ones did."
- "It's not in order. So it can't be used the same way."

# Oneliner

Beau explains why Fallout Boy's updated "We Didn't Start the Fire" may not have the same impact as the original for teaching due to its lack of chronological order and focus on pop culture.

# Audience

History teachers, music enthusiasts

# On-the-ground actions from transcript

- Analyze historical events in chronological order with students (implied)
- Encourage critical thinking about the relevance of cultural references in music (implied)

# Whats missing in summary

Insights into the potential educational value of discussing music's cultural references with students.

# Tags

#Teaching #Music #HistoricalEvents #PopCulture #Impact #ChronologicalOrder #Education