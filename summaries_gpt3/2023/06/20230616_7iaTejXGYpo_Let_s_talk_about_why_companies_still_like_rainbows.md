# Bits

Beau says:

- Explains the prevalence of marketing rainbows and why they will likely continue despite efforts to stop them.
- Mentions a survey where 70% of non-LGBTQ+ individuals believe companies should show support through advertising, sponsorship, and hiring practices.
- Points out that there are an estimated 17 million LGBTQ+ individuals in the U.S. who are supportive and hold significant economic power.
- Companies are interested in tapping into this trillion-dollar market and are more likely to support a community with immutable characteristics for long-term gains.
- Notes the risk of losing customers permanently by alienating a community versus temporarily upsetting others who may eventually return.
- States that companies prioritize profit over social responsibility, akin to countries pursuing power in foreign policy.
- Attributes the widespread appearance of rainbows to capitalism and the profitability of being accepting and inclusive.
- Suggests that there are not enough individuals invested in culture wars to significantly impact the prevalent display of rainbows.
- Mentions examples like Target moving displays but also investing in pride celebrations, showing that acceptance can be profitable.
- Concludes that being a good person and accepting leads to a wider market and ultimately more profit.

# Quotes

- "It is more profitable to be a good person. It is more profitable to be accepting."
- "The reason you see rainbows everywhere is because of, well, the right wing. Capitalism."
- "The money is always an acceptance."

# Oneliner

Despite resistance, marketing rainbows persists due to capitalist interests and the economic power of supportive LGBTQ+ individuals.

# Audience

Marketers, LGBTQ+ allies

# On-the-ground actions from transcript

- Support LGBTQ+ businesses and initiatives (exemplified)
- Attend and participate in pride celebrations and events (exemplified)

# Whats missing in summary

The full transcript provides more insight into the economic motivations behind companies' support for LGBTQ+ communities and the profitability of inclusivity in marketing.