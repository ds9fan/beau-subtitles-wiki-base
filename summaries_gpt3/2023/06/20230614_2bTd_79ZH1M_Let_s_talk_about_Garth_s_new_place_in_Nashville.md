# Bits

Beau says:

- Taking a break from national news to address a question about Garth Brooks' new place in Nashville.
- Clarifying that Garth Brooks is not opening a gay bar but a place where good manners are expected.
- Brooks wants the place to be safe and where people love and respect one another.
- Despite being in Tennessee, there seems to be some resistance to this concept.
- Brooks mentioned serving every brand of beer and promoting love and respect for one another.
- He doesn't seem concerned about how this might affect his brand, as he has always been advocating for inclusivity.
- Brooks has a history of being woke and advocating for equality, even before it became a popular term.
- He believes that as society progresses, the majority accepting stance outweighs the bigoted views.
- Brooks isn't fazed by potential backlash from those who do not share his values.
- His actions are seen as good capitalism as he appeals to a growing demographic that values kindness and respect.

# Quotes

- "If you are let into our house, love one another. Again, love your neighbor."
- "30 years this man has been telling you who he is. He is super woke."
- "As society moves forward, the population of people that are accepting far outweigh those that are bigoted."
- "He is one of the biggest brands in country music."
- "I don't think he needs assistance from people who outrage farm on Twitter."

# Oneliner

Beau clarifies Garth Brooks isn't opening a gay bar but a place promoting good manners and respect, standing by his inclusive brand amidst potential backlash.

# Audience

Country music fans

# On-the-ground actions from transcript

- Support inclusive businesses like Garth Brooks' new place (exemplified)
- Promote love and respect in your community (exemplified)

# Whats missing in summary

The full transcript provides a detailed look at Garth Brooks' inclusive values and the potential impact on his brand amidst societal progress.

# Tags

#GarthBrooks #Inclusivity #CountryMusic #Tolerance #Respect