# Bits

Beau says:

- Chick-fil-A is being called "woke" because of their Diversity, Equity, and Inclusion (DEI) section, which has been in place for years.
- Right-wing commentators are riling up their audience by labeling companies as "woke" to generate clicks and views.
- DEI is not a symbol of being "woke" but rather a part of capitalism to widen a company's market.
- Beau personally chooses not to support Chick-fil-A due to past donations, but acknowledges the company's attempt to appeal to a broader audience through DEI.
- The outrage over Chick-fil-A being labeled as "woke" is fueled by buzzwords and manipulated by commentators for profit.
- Beau challenges the idea that having a DEI office goes against Christianity and urges viewers not to be misled by manufactured outrage.
- The focus on Chick-fil-A's DEI office is a distraction from the reality of capitalism driving businesses to expand their consumer base.
- Being woke and aware of social injustices should be seen as positive, and companies like Chick-fil-A implementing DEI initiatives should be appreciated.
- Despite establishing a DEI office, Chick-fil-A is not likely to actively participate in events like sponsoring pride parades.
- The narrative around Chick-fil-A's "wokeness" is a ploy to keep viewers engaged for ad revenue, ultimately benefiting the commentators.

# Quotes

- "DEI isn't woke. It's capitalism."
- "Woke is good, actually. Being alert to injustice is good."
- "Chick-fil-A establishing a DEI office. Yeah, I mean, that's cool."
- "They're playing you."
- "Your commentators, they're playing you."

# Oneliner

Chick-fil-A's "wokeness" is a marketing strategy, not a political statement, revealing the manipulation behind outrage-driven narratives.

# Audience

Conservative viewers

# On-the-ground actions from transcript

- Question manufactured outrage and look beyond buzzwords (implied)
- Support companies implementing Diversity, Equity, and Inclusion initiatives (implied)
- Challenge the profit-driven manipulation by commentators (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of how companies navigate capitalism and social issues, urging critical thinking and awareness of media manipulation.

# Tags

#ChickfilA #Wokeness #Capitalism #DEI #MediaManipulation