# Bits
Beau says:

- Addressing the possible expungement of former President Trump's impeachments.
- McCarthy's support for the idea of expunging the impeachments.
- Legal scholars doubting the feasibility of expunging impeachments since it's not a criminal process.
- Mentioning the precedent of Andrew Jackson having his Senate censure reversed in 1837.
- Emphasizing that even if the impeachments are expunged, they will still remain in the historical record.
- Describing the expungement as a symbolic gesture for the base, adding an asterisk to Trump's record.
- Pointing out that Trump's focus may be more on his legal troubles rather than the impeachments.
- Criticizing the House GOP for using such actions as a show for social media engagement without real impact.
- Stating that the expungement won't change public perception of Trump but will tie House GOP members to him historically.
- Concluding that the expungement is merely a performative act with no substantial effect.

# Quotes
- "It's a show with nothing more."
- "It doesn't actually go away."
- "They might go on to do something, but they will forever be tied to Trump."

# Oneliner
Beau addresses the symbolic expungement of Trump's impeachments, outlining its historical impact and criticizing it as a performative act with no real consequence.

# Audience
Politically engaged individuals

# On-the-ground actions from transcript
- Educate on the implications of performative political acts (implied)

# Whats missing in summary
The detailed nuances of historical precedents and legal scholars' perspectives on expunging impeachments.

# Tags
#Trump #Impeachments #HouseGOP #HistoricalRecord #PoliticalSymbolism