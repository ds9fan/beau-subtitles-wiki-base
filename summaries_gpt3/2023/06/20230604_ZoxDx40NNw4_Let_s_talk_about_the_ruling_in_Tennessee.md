# Bits

Beau says:

- A federal judge in Tennessee deemed a ban on certain shows unconstitutionally vague and substantially overbroad.
- Most bans on these types of shows are boilerplate, with the same goal of being unconstitutional.
- States must provide an incredibly compelling reason to ban such shows, tailored narrowly to that reason.
- Over 500 bills targeting the LGBTQ community have been introduced, but most are likely to be struck down in court.
- The government must show a significant reason to intervene in matters of speech, including LGBTQ expression.
- Despite the ruling in Tennessee, the fight against discrimination is far from over.
- The state's lack of compelling interest in these bans suggests they will continue to be struck down in court.
- Efforts to scapegoat the LGBTQ community are likely to persist to divert attention from other issues.
- Blaming marginalized communities for institutional issues is a diversion tactic by those in power.
- The fight for equality continues, even after legal victories.

# Quotes

- "States must provide an incredibly compelling reason to ban such shows, tailored narrowly to that reason."
- "The fight against discrimination is far from over."
- "Blaming marginalized communities for institutional issues is a diversion tactic by those in power."

# Oneliner

A federal judge in Tennessee strikes down a ban on certain shows, revealing the unconstitutional nature of most bans targeting LGBTQ communities, but the fight against discrimination continues.

# Audience

Legal advocates, LGBTQ activists

# On-the-ground actions from transcript

- Support organizations advocating for LGBTQ rights (suggested)
- Stay informed and engaged in legal battles against discriminatory legislation (implied)

# Whats missing in summary

Detailed examples of discriminatory legislation and the potential impacts on LGBTQ communities.

# Tags

#LGBTQ #Discrimination #LegalRights #Equality #Activism