# Bits

Beau says:

- Provides an update on a story involving the expulsion of two black men and a white woman from the Tennessee state legislature.
- The local areas responsible for the representatives sent them right back despite the expulsion.
- Both Pearson and Jones advanced through the Democratic primary.
- Pearson will face John Johnston, an independent, while Jones will face Laura Nelson, a Republican in the special election on August 3rd.
- The special election is costing Tennessee close to a million dollars.
- Beau believes the special election is just a political stunt by Republicans to show racism.
- He predicts that both Pearson and Jones will win the special election, rendering the entire process pointless.
- Criticizes the state legislature for not believing in the will of the people and sending a message of young black men needing to stay in their place.

# Quotes

- "This election is costing the people of Tennessee probably close to a million dollars, just so the state legislature could prove that, well, they're still pretty racist."
- "All of this was a political stunt by Republicans and all it showed was that the majority of the state legislature in Tennessee does not believe in the will of the people."
- "I have a feeling they're gonna stay in their place and their place is apparently the Tennessee State Legislature."

# Oneliner

Beau provides an update on the expulsion of two black men and a white woman from the Tennessee state legislature, criticizing it as a political stunt showing institutional racism.

# Audience

Advocates for social justice.

# On-the-ground actions from transcript

- Support Pearson and Jones in the special election (suggested).

# Whats missing in summary

More details on the initial situation that led to the expulsion of the representatives.

# Tags

#Tennessee #StateLegislature #Racism #SpecialElection #PoliticalStunt