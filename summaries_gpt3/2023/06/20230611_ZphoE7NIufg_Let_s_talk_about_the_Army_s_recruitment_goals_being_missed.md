# Bits

Beau says:

- The U.S. Army missed its recruitment goal, leading to speculation about a "woke military."
- Recruitment numbers have been declining since 2018, contrary to the perception of recent events causing the decline.
- In 2018, the Army set a goal of recruiting 80,000 troops but fell short at 70,000.
- The trend continued in subsequent years, with goals decreasing and recruitment numbers also dropping.
- Factors contributing to the recruitment decline include generational shifts, awareness of military realities, and job availability.
- Veterans' influence on younger generations and evolving perceptions of the military impact recruitment.
- The idea that recent events or a "woke military" are to blame for recruitment challenges is debunked.
- Recruitment issues date back to 2018, marking the first time the Army missed its goals since 2005.
- The decline in recruitment numbers coincides with generational changes influenced by past military engagements.

# Quotes

- "Recruitment issues date back to 2018, marking the first time the Army missed its goals since 2005."
- "The decline in recruitment numbers coincides with generational changes influenced by past military engagements."
- "The idea that recent events or a 'woke military' are to blame for recruitment challenges is debunked."

# Oneliner

The declining U.S. Army recruitment numbers since 2018 are influenced by generational shifts and awareness, refuting claims of a "woke military."

# Audience

Military analysts, policymakers, citizens

# On-the-ground actions from transcript

- Inform young people about military realities (implied)
- Advocate for progressive military reforms (implied)
- Support veterans in sharing their experiences (implied)

# Whats missing in summary

The full transcript provides detailed insights into the factors affecting U.S. Army recruitment numbers and challenges, offering a comprehensive view of the situation.

# Tags

#USArmy #Recruitment #GenerationalShifts #MilitaryPerceptions #WokeMilitary