# Bits

Beau says:

- Explains cultural shifts, unconscious bias, and discomfort surrounding LGBTQ issues.
- Responds to a viewer who struggles with acceptance of trans people in the public eye.
- Analyzes the viewer's discomfort and unconscious bias towards trans individuals.
- Points out the viewer's "passing privilege" and the societal acceptance of those who fit neatly into traditional gender norms.
- Encourages self-reflection and understanding of discomfort to facilitate change and acceptance.
- Provides historical examples of gender representation in media to illustrate the evolution over time.
- Suggests that discomfort may stem from unfamiliarity with non-traditional gender presentations.
- Emphasizes the importance of acknowledging changing societal norms and embracing diversity.
- Validates the viewer's willingness to learn and change, despite their initial discomfort.
- Offers advice on addressing discomfort and adapting to societal progress regarding gender identity.

# Quotes

- "It's not a new thing. It's been around."
- "Sometimes once you acknowledge something like this, it goes away."
- "You're probably not hateful."
- "You're not too old to change. You are changing."
- "Just acknowledge that things are changing."

# Oneliner

Beau addresses discomfort with LGBTQ issues, urging self-reflection and acceptance of evolving gender norms to facilitate change and understanding.

# Audience

Individuals grappling with unconscious bias.

# On-the-ground actions from transcript

- Analyze moments of discomfort around unfamiliar gender presentations (suggested).
- Acknowledge societal changes in gender norms and embrace diversity (implied).

# Whats missing in summary

The full transcript provides a comprehensive exploration of unconscious bias and discomfort towards LGBTQ individuals, offering valuable insights for personal growth and acceptance.