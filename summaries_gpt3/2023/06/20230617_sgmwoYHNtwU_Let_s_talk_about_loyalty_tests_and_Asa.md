# Bits

Beau says:

- Asa Hutchinson and the Republican Party require candidates to sign a pledge to support the eventual nominee for president, regardless of legal issues.
- Hutchinson sought clarification on whether he should remain loyal to a candidate facing legal troubles.
- Hutchinson refuses to support a candidate convicted under the Espionage Act or serious crimes.
- The demand for loyalty to Trump seems ironic given his disregard for pledges and oaths.
- Trump's handling of classified information raises doubts about extending loyalty and oaths to him.
- Hutchinson's call to amend the pledge shows a rare concern for honoring oaths in politics.
- The Republican Party may face consequences for demanding loyalty to Trump if he faces conviction.
- Candidates pledging loyalty to Trump could suffer from attack ads if he is convicted and still wins the nomination.
- Taking a loyalty oath to a potentially convicted individual doesn't seem like a wise long-term strategy for the Republican Party.

# Quotes

- "They're demanding people who apparently take an oath seriously, take a pledge seriously."
- "A politician who apparently cares enough about an oath or a pledge to make sure it's something that they can live up to."
- "That doesn't seem to be good long-term planning to me."

# Oneliner

Asa Hutchinson questions loyalty oaths in politics, raising concerns about supporting a potentially convicted nominee like Trump. 

# Audience

Political observers

# On-the-ground actions from transcript

- Question the loyalty demands placed on political candidates (suggested)
- Advocate for ethical and accountable leadership in political parties (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of the implications of loyalty pledges in politics and the potential consequences of supporting candidates with legal issues.

# Tags

#Politics #Loyalty #RepublicanParty #Trump #AsaHutchinson