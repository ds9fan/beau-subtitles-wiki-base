# Bits

Beau says:

- A two-year probe in Minneapolis revealed systemic discrimination in the police department, including excessive force and unjustified use of lethal force.
- Marginalized groups in the city faced the brunt of the department's behavior, particularly Black Americans, Native Americans, and those with behavioral health issues.
- The report uncovered that the restraint used on George Floyd was applied hundreds of times, with over 40 instances where an arrest wasn't necessary.
- The city and police department have now entered into a consent decree, with federal oversight and mandated reforms.
- The success of consent decrees depends on the willingness of department leadership to remove problematic officers, which can have a greater impact on culture than policy changes.
- Addressing illegal behavior during the reviewed period varies, sometimes resulting in charges depending on the strength of the case.
- The fallout from George Floyd's death is ongoing, with the need for long-term corrections to address the systemic issues uncovered.

# Quotes

- "A two-year probe in Minneapolis revealed systemic discrimination in the police department."
- "Throwing out the bad apples as quickly as possible is what matters."
- "The fallout from George Floyd's death is ongoing."

# Oneliner

A two-year probe in Minneapolis uncovered systemic discrimination and excessive force within the police department, leading to a consent decree and ongoing efforts to address deep-rooted issues.

# Audience

Reform advocates, community members

# On-the-ground actions from transcript

- Join or support local organizations advocating for police reform (suggested)
- Stay informed about the progress of mandated reforms in the Minneapolis police department (exemplified)

# Whats missing in summary

Detailed examples and nuances from the full transcript can provide a deeper understanding of the systemic issues within the Minneapolis police department.

# Tags

#Minneapolis #PoliceReform #SystemicDiscrimination #ConsentDecree #GeorgeFloyd #CommunityAction