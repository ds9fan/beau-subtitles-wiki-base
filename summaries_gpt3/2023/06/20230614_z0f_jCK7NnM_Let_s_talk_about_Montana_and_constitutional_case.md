# Bits

Beau says:

- Talking about a case in Montana called Held vs. Montana where 16 kids are suing the state for supporting dirty energy and contributing to climate change.
- Montana's constitution guarantees the right to a clean and healthful environment since 1972, which the plaintiffs believe the state is not upholding.
- The state's defense seems to be downplaying their contribution to climate change by arguing it's a global issue and their impact is minimal.
- Beau compares this defense to denying other constitutional rights like free speech or due process based on global standards.
- The case has entered trial, and Beau anticipates a lengthy legal process before a resolution is reached.
- Despite potential appeals and a prolonged legal battle, Beau sees the case as a significant step towards holding the government accountable for environmental protection.
- Beau believes that protecting the environment should be a top priority for the government, falling under promoting the general welfare.
- He questions the value of a massive defense budget when the country's own actions are harming it.

# Quotes

- "You're going to have a government that has the authority that the one we have does, I think that protecting the environment should be pretty high upon that list."
- "It doesn't really do a whole lot of good to have an 800 billion dollar a year defense budget if you're destroying the country yourself."

# Oneliner

Kids in Montana sue the state for supporting dirty energy, questioning the government's role in protecting the environment and promoting general welfare.

# Audience

Environmental activists, advocates

# On-the-ground actions from transcript

- Support environmental organizations in Montana to amplify the message of protecting the environment (implied).
- Join local climate action groups to advocate for sustainable practices in your community (implied).

# Whats missing in summary

The full transcript provides detailed insights into a legal case challenging the government's environmental responsibilities and the importance of holding authorities accountable for protecting the environment.