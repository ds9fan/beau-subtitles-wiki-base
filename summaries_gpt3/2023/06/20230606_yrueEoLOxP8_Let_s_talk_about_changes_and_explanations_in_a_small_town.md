# Bits

Beau says:

- Addressing change in a small town and managing obstacles that arise.
- A message about being part of the LGBTQ+ community and distancing from friends.
- Feeling terrified of confrontation in a right-wing rural town.
- Questioning the right decisions and seeking advice on handling the situation.
- Being accommodating and conflict avoidant, struggling to express discomfort.
- Exploring whether it's right to distance oneself and how to handle explanations.
- Emphasizing that those who are not accepting do not deserve an explanation.
- People are not entitled to know the reasons behind positive changes in your life.
- Small towns often resist change, making it challenging for individuals evolving.
- Encouraging personal growth and being true to oneself without needing approval.
- Advocating for surrounding oneself with supportive and accepting people.
- Asserting that explanations are only owed to those who truly care and support.
- Advising caution in revealing full explanations in small towns due to gossip.
- Urging individuals not to let anyone hinder their personal growth and progress.

# Quotes

- "Those who don't want you to change, those who wouldn't be accepting of that, they don't matter."
- "You don't owe them anything, nothing you have to worry about, only you and those people who are accepting."
- "Don't let anybody stand in that way."
- "If you don't look back on the person you were five years ago and cringe, you wasted five years."
- "People will change, they should change."

# Oneliner

Beau addresses managing change in a small town, advocating for personal growth amidst resistance and the importance of surrounding oneself with supportive individuals.

# Audience

Community members seeking guidance on navigating personal growth in challenging environments.

# On-the-ground actions from transcript

- Surround yourself with supportive and accepting individuals (implied).

# Whats missing in summary

Beau's insightful perspective on embracing personal growth and change while navigating resistance in a small town.

# Tags

#Change #Acceptance #PersonalGrowth #SmallTown #LGBTQ+ #Support