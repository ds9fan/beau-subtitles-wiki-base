# Bits

Beau says:

- Examines the concept of individuals viewed as bad doing good things and vice versa.
- Talks about motivations and how people rationalize committing evil acts for a greater good.
- Gives examples of Americans justifying heinous actions in the name of national security.
- Explores the idea of individuals fighting not against what's in front of them, but for what they believe they're protecting.
- Illustrates how loyalty and the belief in fighting for a greater good can lead individuals to participate in actions they may find questionable.
- Touches on the importance of perceived waste and the illusion of doing the right thing in conflicts.
- Concludes by reflecting on the universal nature of justifying actions in war and the influence of societal norms.

# Quotes

- "Just remember before you can commit a great evil you have to be convinced that it's for a greater good."
- "Soldiers do not fight against what's in front of them. They fight for what's behind them, what they believe they're protecting."
- "You have to believe that. If they don't, well then they have to deal with everything that they did."
- "Because at the end of it, you have to remember, everything that occurs in war is horrible."
- "We identify ourselves by lines on maps and flags, the little things that separate us."

# Oneliner

Beau examines how individuals justify heinous acts for a greater good, delving into loyalty, rationalization, and the universal nature of war justifications.

# Audience

Advocates for Peace

# On-the-ground actions from transcript

- Question societal norms that perpetuate the justification of violence (implied)
- Educate others on the dangers of blindly following authority figures (implied)

# Whats missing in summary

The full transcript dives deep into the psychology behind justifying immoral actions in the name of a greater cause, urging viewers to critically analyze motives beyond blind loyalty.

# Tags

#War #Ethics #Loyalty #NationalInterest #HumanPsychology