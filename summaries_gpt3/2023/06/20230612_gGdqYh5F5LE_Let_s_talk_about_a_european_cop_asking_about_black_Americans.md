# Bits

Beau says:

- A cop in Europe reached out for advice on dealing with black Americans in a good way.
- The cop in Denmark handled a situation involving a black American couple on vacation arguing over insulin left at home.
- The European cop was bothered by the reaction of the black American man, throwing his hands up in compliance due to fear from U.S. law enforcement behavior.
- Beau suggests starting interactions with "It's cool, we're not American cops."
- European cops practice consent-based policing compared to the domination-based policing in the U.S.
- The European cop aims to make people more at ease when interacting with law enforcement.
- Suggestions are welcomed from black Americans on what can set them at ease in such situations.
- American law enforcement behavior is seen as out of line compared to the rest of the world.
- The European cop hopes that calling Danish police won't be associated with a death sentence for Americans.
- Beau encourages sharing suggestions to improve interactions with law enforcement.

# Quotes

- "It's cool, we're not American cops."
- "American law enforcement behavior is so out of line with the rest of the world."
- "Calling Danish police does not come with the chance of a death sentence."
- "The behavior exhibited by a large portion of American law enforcement is so out of line."

# Oneliner

A cop in Europe seeks advice on handling black Americans, focusing on calming interactions and understanding the fear caused by U.S. law enforcement behavior.

# Audience

European Law Enforcement 

# On-the-ground actions from transcript

- Share suggestions on how law enforcement can make interactions more comfortable (suggested).
- Practice consent-based policing techniques to create safer interactions (exemplified).

# Whats missing in summary

The full transcript provides a nuanced look at the differences in policing approaches between the U.S. and Europe, aiming to improve interactions and ease fears caused by American law enforcement behavior.

# Tags

#LawEnforcement #BlackAmericans #CommunityPolicing #PoliceReform #Fear