# Bits

Beau says:

- Biden administration intervened in a labor dispute on railroads regarding paid sick days.
- Unions were pushing for paid sick days and short notice sick days, but negotiations weren't progressing.
- Biden tried to help the unions but didn't fully meet their demands, risking a rail strike.
- Biden considered using Congress's power to break the strike, angering union supporters.
- Beau critiqued this as the first major mistake of Biden's administration.
- Despite promises for paid sick days, no concrete action or legislation was seen initially.
- Eventually, Biden's administration quietly worked on securing paid sick days for railroad workers.
- Biden and Bernie Sanders played key roles in ensuring agreements with major freight carriers for sick days.
- Unions, including IBEW, now have significant benefits like paid short notice sick days and more.
- Beau acknowledges his initial skepticism but admits he was incorrect in doubting Biden's commitment.
- Biden and Bernie continue to work behind the scenes to assist other unions in achieving their goals.

# Quotes

- "I believe in paid sick days and I think railroad workers should get that, blah, blah, blah."
- "Biden and Bernie appear to have really, really came through."
- "If I'm reading between the lines on all of this correctly, Biden and Bernie appear to have really, really came through."
- "I was openly skeptical and I made kind of a very direct statement that I did not think that the Biden administration was actually going to continue to work on this."
- "It's just a thought."

# Oneliner

Biden's administration quietly secures paid sick days for railroad workers, proving skeptics wrong and aiding unions behind the scenes.

# Audience

Railroad workers, union supporters

# On-the-ground actions from transcript

- Support and advocate for unions in your community (implied)
- Stay informed about labor disputes and negotiations (implied)

# Whats missing in summary

The full transcript gives a detailed account of how behind-the-scenes efforts by Biden and Bernie helped secure significant benefits for railroad workers, showcasing a positive outcome despite initial skepticism. 

# Tags

#Biden #RailroadWorkers #UnionSupport #PaidSickDays #LaborDispute