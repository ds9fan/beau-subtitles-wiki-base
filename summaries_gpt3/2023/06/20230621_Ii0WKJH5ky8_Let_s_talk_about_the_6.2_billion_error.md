# Bits

Beau says:

- Explains a $6.2 billion accounting error made by the Pentagon in relation to money given to Ukraine.
- Clarifies that the Pentagon doesn't hand cash to Ukraine but provides equipment.
- Mentions the concept of depreciation and how it affected the accounting error.
- Gives a detailed example of vehicle depreciation to illustrate the issue.
- Points out that the equipment provided to Ukraine is not brand new but includes older stock.
- Suggests visiting GovPlanet, an auction website, to see military Humvees for sale at low prices.
- Notes that the missing $6.2 billion is not where all the money that disappears from the Pentagon goes.
- Emphasizes that $6.2 billion is significant to normal people but a small fraction of the Department of Defense's budget.
- Acknowledges the potential for audits of the Pentagon's finances but indicates that the accounting error is not the primary concern.

# Quotes

- "To us, $6.2 billion is a lot of money. To the Department of Defense, that's a rounding error."
- "If you are one of those people calling for an audit of the Pentagon's finances, oh no, you're still good."
- "The military did not account for depreciation."
- "The equipment provided to Ukraine was not made this year."

# Oneliner

Beau breaks down a $6.2 billion Pentagon accounting error regarding equipment for Ukraine, revealing the impact of depreciation and the insignificance of the sum in the defense budget.

# Audience

Budget analysts

# On-the-ground actions from transcript

- Visit GovPlanet to see military equipment for sale (suggested)
- Conduct audits of Pentagon finances (exemplified)

# Whats missing in summary

The full transcript provides a detailed breakdown of the accounting error, depreciation impact, and insignificance of $6.2 billion in the Pentagon's budget.

# Tags

#Pentagon #AccountingError #Depreciation #MilitaryEquipment #Audit