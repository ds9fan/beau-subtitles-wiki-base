# Bits

Beau says:

- The approval of an online religious charter school in Oklahoma by the Statewide Virtual Charter School Board.
- Governor's support for the school based on religious liberty, contrasting with the Oklahoma Attorney General's view of it being a violation of the state Constitution.
- Potential legal challenges at the state level due to state constitutions closely resembling the U.S. Constitution.
- Predictions that the program will likely be short-lived and face opposition either at the state or federal level.
- Criticism towards those claiming to defend the Constitution while being ignorant of its contents.

# Quotes

- "This is probably going to get ground down in the state courts because a whole lot of states have state constitutions that closely align with the U.S. Constitution."
- "This isn't going to fly, I wouldn't worry about it too much."

# Oneliner

Approval of an online religious charter school in Oklahoma sparks legal concerns as state officials clash over its constitutionality, likely facing swift opposition.

# Audience

Legal advocates, activists

# On-the-ground actions from transcript

- Challenge the approval of the online religious charter school through legal action (implied)
- Stay informed about the developments regarding this issue and support efforts to oppose it (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the approval of an online religious charter school in Oklahoma and the legal challenges it may face at both the state and federal levels.

# Tags

#Oklahoma #ReligiousCharterSchool #Constitutionality #LegalChallenge #StateVsFederal