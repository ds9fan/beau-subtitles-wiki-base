# Bits

Beau says:

- Eastman is facing 11 disciplinary charges in California that could lead to the loss of his license due to his involvement in trying to alter the outcome of the 2020 election.
- His actions were aimed at obstructing the electoral count on January 6th to prevent Vice President Pence from certifying Joe Biden as the winner.
- The proceedings against Eastman are expected to last about a week with multiple witnesses being called.
- Testimonies from these proceedings could have implications for investigations related to January 6th and Trump in Georgia.
- Many in the legal profession believe that the proceedings against Eastman are necessary for ethical reasons, as his actions were not grounded in the law.
- Witnesses called by Eastman might have direct knowledge of any potential plan.
- The judge in the proceedings does not seem receptive to Eastman's arguments or style, similar to the situation with Meadows.
- Information from Eastman could be critical for potential charges in Georgia and related to January 6th.
- Testimonies from these proceedings might lead to further cooperation from Eastman with federal investigations.
- Despite seeming unimportant now, the proceedings against Eastman could have significant future implications.

# Quotes

- "His actions were aimed at obstructing the electoral count on January 6th."
- "Testimonies from these proceedings could have implications for investigations related to January 6th and Trump in Georgia."
- "Many in the legal profession believe that the proceedings against Eastman are necessary for ethical reasons."

# Oneliner

Eastman's disciplinary proceedings in California, involving attempts to alter the 2020 election outcome, could have significant implications for future investigations and ethical considerations in the legal profession.

# Audience

Legal professionals

# On-the-ground actions from transcript

- Attend or follow updates on Eastman's disciplinary proceedings (exemplified)
- Stay informed about the implications of these proceedings for future investigations (implied)
- Support ethical considerations in legal practice (implied)

# Whats missing in summary

Insights on the potential impact of Eastman's testimony and cooperation on federal investigations and legal ethics.

# Tags

#Eastman #DisciplinaryProceedings #2020Election #LegalEthics #FutureImplications