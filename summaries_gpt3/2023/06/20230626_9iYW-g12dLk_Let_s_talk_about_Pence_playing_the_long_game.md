# Bits

Beau says:

- Critiques former Vice President Pence's campaign strategy for not capitalizing on a unique tool he possesses.
- Expresses dislike for Pence's policies and philosophy but acknowledges his actions on January 6th in not contesting the electors.
- Questions why Pence is not leveraging his role in protecting American democracy on January 6th in his campaign.
- Suggests two possible reasons for Pence's strategy: either he has undisclosed wrongdoing or he is playing a long game, waiting for Trump's downfall to claim credit for preserving democracy.
- Speculates that Pence might be waiting for certain proceedings related to January 6th to come to light before using this information in his campaign.
- Points out that Pence's campaign is currently inactive, not utilizing the potentially advantageous information he has.
- Raises the possibility that Pence's advisors are aware of the political value of his actions on January 6th and are strategically waiting to reveal it at a more opportune time.

# Quotes

- "I don't understand why he's not using that and this is the only reason I can come up with other than he's super guilty of something and we don't know it."
- "He was the one person who really stood in the way that day and he's not bringing it up."
- "I think Pence might be playing the long game here."
- "Politically, that's gold and they're not using it yet."
- "He's waiting for somebody else to do it and then he can come out and explain."

# Oneliner

Beau questions former Vice President Pence's campaign strategy and speculates if he is playing a long game by waiting for Trump's downfall to claim credit for preserving democracy.

# Audience

Campaign strategists, political analysts, supporters of democracy

# On-the-ground actions from transcript

- Analyze Pence's actions and statements critically to understand his political strategy (implied)

# Whats missing in summary

Insights on Pence's potential political strategy and the impact of waiting for a more opportune time to reveal significant information.

# Tags

#CampaignStrategy #Pence #PreservingDemocracy #PoliticalSpeculation #January6th