# Bits

Beau says:

- Explaining the concept of standing orders that Trump claimed to have.
- Recalling Trump's assertion of having a standing order to declassify any file at his discretion.
- Mentioning his initial skepticism about the existence of such standing orders.
- Noting that a judge compelled the Department of Justice and the Office of the Director of National Intelligence to respond to a Freedom of Information Act request.
- Revealing that the DOJ and ODNI confirmed possessing no records responsive to the request.
- Speculating on the reason behind their reluctance to provide an answer initially.
- Stating that the idea of these standing orders goes against tradition, practice, and common sense.
- Describing this confirmation as one more defense tactic of Trump's that has been debunked.
- Implying that Trump's defense strategy around standing orders has been unsuccessful.
- Signing off by wishing a good day to the viewers.

# Quotes

- "Those kind of standing orders are not real, don't pretend like it is."
- "That's them saying that this doesn't exist."
- "This isn't something that could really disrupt that."
- "It's one more defense that Trump tried to float that, well, nah, that's not going to work."
- "Anyway, it's just a thought."

# Oneliner

Beau reveals the non-existence of Trump's claimed standing order for declassification, debunking a defense tactic.

# Audience

Media consumers

# On-the-ground actions from transcript

- File Freedom of Information Act requests to seek transparency from authorities (suggested)
  
# Whats missing in summary

Full context and detailed analysis of Trump's defense strategies regarding classified information and standing orders.

# Tags

#Trump #Declassification #StandingOrders #FreedomOfInformationAct #Transparency