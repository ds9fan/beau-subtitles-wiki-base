# Bits

Beau says:

- In June 2022, Randy Cox was paralyzed from the chest down after being thrown forward in a police transport van due to sudden braking.
- The settlement for Randy Cox's case in New Haven, Connecticut is $45 million, the largest police misconduct settlement in U.S. history.
- The city's insurance only covers $30 million of the settlement, leaving the city to pay the remaining $15 million out of pocket.
- Smaller settlements have not effectively changed law enforcement behavior, leading to larger settlements to send a message.
- The five officers involved in Randy Cox's case have been charged with reckless endangerment and cruelty to persons, but have not gone to trial yet.
- Some officers have left the force, been fired, or retired following the incident.
- Attorney Ben Crump, known for handling cases of police misconduct, represented Randy Cox in this case.
- The magnitude of this settlement and its implications are likely to set a standard for future cases involving police misconduct.

# Quotes

- "A $45 million settlement, again, largest reported police misconduct settlement in U.S. history."
- "Smaller settlements don't alter law enforcement behavior; they view it as a cost of doing business."
- "The city's insurance only covers $30 million of the settlement."

# Oneliner

In June 2022, Randy Cox was paralyzed in a police van leading to a $45 million settlement, the largest police misconduct settlement in U.S. history, revealing systemic issues in law enforcement accountability.

# Audience

Legal advocates, activists

# On-the-ground actions from transcript

- Contact legal advocates for support (suggested)
- Stay informed about cases of police misconduct (exemplified)

# Whats missing in summary

Detailed information on the specific changes needed in law enforcement practices to prevent similar incidents in the future.

# Tags

#PoliceMisconduct #Settlement #Accountability #LawEnforcement #BenCrump