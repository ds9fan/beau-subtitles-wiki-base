# Bits

Beau says:

- Addressing the spread of two things, one real and one not, particularly in Florida and Texas.
- Confirming at least five cases of malaria in Florida and Texas, sparking fear and misinformation.
- Explaining that climate change has expanded the range of insects, leading to the spread of diseases like malaria.
- Mentioning a Department of Defense study predicting the rise of diseases due to climate change, including malaria.
- Warning about the increasing prevalence of diseases due to climate change and profit-driven motives to maintain dirty energy.
- Encouraging viewers to watch a video discussing the national security threat posed by climate change and disease spread.

# Quotes

- "Climate change has increased the range of insects."
- "We're going to have to deal with diseases that we normally didn't."
- "It's not because of some conspiracy other than one that is profit-driven to keep you using dirty energy."

# Oneliner

Beau explains the spread of diseases like malaria in Florida and Texas due to climate change, debunking conspiracy theories and exposing profit-driven motives behind dirty energy use.

# Audience

Climate activists, public health advocates.

# On-the-ground actions from transcript

- Watch the video discussing the national security threat posed by climate change and disease spread (suggested).

# Whats missing in summary

The tone and delivery style of Beau's message, as well as the additional context provided in the full transcript.

# Tags

#ClimateChange #DiseaseSpread #Malaria #ConspiracyTheories #DirtyEnergy