# Bits

Beau says:

- Outlines issues with supply and drawdowns in Ukraine, warning those in the military reserves and National Guard to pay attention.
- Reveals that equipment being sent to Ukraine is not newly purchased but part of existing U.S. military material from pre-positioned stock worldwide.
- Details concerning failures in maintenance: none of the M777 howitzers were mission ready, and only three out of 29 Humvees were.
- Points out a contracting company responsible for maintenance and the army supply group failed in their duties.
- Warns military personnel and maintenance providers worldwide to expect increased scrutiny after this failure.
- Predicts delays in equipment reaching Ukraine due to necessary fixes, but most issues can be quickly resolved.
- Anticipates thorough inspections of all pre-positioned stocks, especially those managed by contracting companies.

# Quotes

- "A 100% failure rate is just, I mean that is just out of, that should be out of the realm of possibility."
- "Be ready for the world to fall in on you."
- "It was just all bad."

# Oneliner

Issues with equipment readiness in pre-positioned stock may delay supplies to Ukraine, prompting global military maintenance scrutiny.

# Audience

Military personnel, maintenance providers

# On-the-ground actions from transcript

- Inspect and maintain equipment thoroughly to ensure mission readiness (implied)
- Stay prepared for heightened accountability and scrutiny in maintenance practices (implied)

# Whats missing in summary

Importance of maintaining military equipment for global operations. 

# Tags

#Ukraine #Military #SupplyChain #Maintenance #GlobalSecurity