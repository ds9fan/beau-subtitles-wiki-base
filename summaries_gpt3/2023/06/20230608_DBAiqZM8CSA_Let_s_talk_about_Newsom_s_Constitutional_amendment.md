# Bits

Beau says:

- Governor Newsom from California proposed a constitutional amendment focusing on firearms regulations at the national level.
- The proposed amendment includes raising the age to purchase a firearm to 21, universal background checks, creating a waiting period, and instituting an assault weapons ban.
- To become part of the U.S. Constitution, the amendment needs to pass a two-thirds vote in the House and Senate and be ratified by three-fourths of the states.
- The inclusion of an assault weapons ban is a significant barrier to the amendment's progress, especially at the national level.
- States have the authority to implement assault weapons bans individually, and 10 states have already done so.
- Despite the potential for a constitutional convention called by state legislatures, none of the amendments to the U.S. Constitution have taken that route.
- Beau speculates that Governor Newsom's proposal may be a strategic move to boost his presidential candidacy in 2028 when demographics might be more favorable towards such regulations.
- Newsom's inclusion of the assault weapons ban in the proposal may hinder its success, as it faces significant opposition.
- Beau suggests that a version of the amendment without the assault weapons ban might have had a better chance of progressing through the legislative process.
- Changing the U.S. Constitution is intentionally challenging, and the difficulty in amending it serves a purpose in maintaining stability and consistency in governance.

# Quotes

- "It's supposed to be hard to change the US Constitution."
- "Realistically, if this amendment had just the first three elements… that might have a chance."
- "Newsome, he's not an unintelligent person. He knows this, so why is he doing it?"
- "The odds of this going anywhere now are slim to none."
- "Y'all have a good day."

# Oneliner

Governor Newsom's proposed constitutional amendment on firearms faces hurdles due to including an assault weapons ban, hindering its potential progress towards becoming part of the U.S. Constitution.

# Audience

Legislators, Activists, Voters

# On-the-ground actions from transcript

- Advocate for firearms regulations at the state level (implied)
- Stay informed about proposed amendments and their potential impact on gun laws (implied)

# Whats missing in summary

Further insights on Governor Newsom's potential motivations and strategies behind proposing the constitutional amendment.

# Tags

#GovernorNewsom #ConstitutionalAmendment #FirearmsRegulations #AssaultWeaponsBan #LegislativeProcess