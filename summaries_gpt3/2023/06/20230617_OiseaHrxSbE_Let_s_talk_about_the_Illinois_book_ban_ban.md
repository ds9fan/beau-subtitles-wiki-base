# Bits

Beau says:

- Explains the Illinois book ban ban, clarifying that it doesn't actually ban books but restricts funding for libraries.
- Outlines what libraries in Illinois must do to comply with the legislation, including adopting the American Library Association's Library Bill of Rights.
- Emphasizes the importance of not excluding materials based on origin, background, or views, and avoiding removal due to partisan or doctrinal disapproval.
- Argues that libraries should provide books based on public demand, even if some individuals disagree, as their role is to serve the community.
- Commends Illinois for taking a positive step and hopes other states will follow suit to allow public libraries to uphold their ethical standards.

# Quotes

- "The states that are forcing libraries to remove books because they hurt somebody's feelings or you know they disagree with somebody's personal religious beliefs, they are forcing libraries to go against their own code of ethics."
- "A library isn't there to be an archive. It's there to provide services to the public. It's there to provide books that people want."
- "This legislation is a good move."
- "I really hope that other states follow suit and create a situation where public libraries can operate under the ethics that they've set for themselves."
- "They're trying to fulfill the promises they've made to the public, let them do it."

# Oneliner

Beau outlines Illinois' book ban ban, requiring libraries to uphold ethical standards and provide books based on public demand.

# Audience

Library advocates, book lovers

# On-the-ground actions from transcript

- Support your local library by advocating for their autonomy and adherence to ethical standards (exemplified)

# Whats missing in summary

Importance of supporting public libraries and ensuring access to diverse materials for all community members.

# Tags

#LibraryEthics #BookBans #PublicLibraries #CommunitySupport #Illinois