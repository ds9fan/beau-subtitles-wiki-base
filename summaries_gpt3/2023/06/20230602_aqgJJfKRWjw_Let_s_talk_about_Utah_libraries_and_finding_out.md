# Bits

Beau says:

- Utah's Davis School District is removing Bibles from elementary and middle school libraries due to vulgarity and violence in the King James Version.
- This action is a result of a 2022 law targeting marginalized groups and free speech.
- The law aims to have parents flag objectionable content in books.
- Beau criticizes the law for attempting to marginalize groups and appeal to bigoted sentiments.
- The removal of Bibles from libraries is seen as a form of malicious compliance.
- Parents likely already monitor their children's reading choices from the library.
- Beau suggests that similar actions may occur in other locations with similar laws.
- The situation is likened to reaping what you sow, even for those who don't read the Bible.
- The underlying message is about the consequences of targeting specific groups through legislation.

# Quotes

- "For whatsoever a man soweth, that shall he also reap."
- "You're not allowed to say other people can't read that book."
- "Expect this to occur in pretty much every location a law like this was passed."

# Oneliner

Utah's Davis School District removes Bibles due to objectionable content, a consequence of legislation targeting marginalized groups.

# Audience

Educators, parents, activists

# On-the-ground actions from transcript

- Contact local school boards to advocate for diverse and inclusive library collections (implied)
- Monitor and guide children's reading choices from school libraries (implied)

# Whats missing in summary

The full transcript provides deeper insights into the implications of legislative actions on marginalized communities and the importance of vigilance in upholding free speech and inclusivity.