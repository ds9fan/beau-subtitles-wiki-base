# Bits

Beau says:

- Hunter Biden, the sitting president's son, was charged with two counts of willful failure to pay taxes and a pretrial diversion on a gun charge related to a substance issue.
- The charges could lead to anywhere from probation to up to a year in jail, but Beau believes jail time is unlikely.
- Beau has a rule not to talk about private citizens, but he's breaking it because Republicans made false claims about Hunter Biden receiving a sweetheart deal from the Biden DOJ.
- The prosecutor responsible for Hunter Biden's deal is actually a Republican and a Trump appointee.
- President Biden did not interfere with the Department of Justice (DOJ) and could have pardoned his son but chose not to.
- Beau contrasts Biden's hands-off approach with what he believes Trump might have done if one of his children were in a similar situation.
- Beau mentions comparisons to Kushner's actions but cautions against seeking retribution based on partisanship.
- Beau expresses surprise at learning Hunter Biden's full name is Robert.

# Quotes

- "President Biden did not interfere with the Department of Justice (DOJ) and could have pardoned his son but chose not to."
- "Can you imagine one of Trump's kids getting arrested while he was president? Of course not!"
- "The crime becomes evident and then you look for the person. You don't look at a person and try to find a crime."
- "It seems pretty clear that isn't how DOJ works."
- "Did y'all know his name was Robert?"

# Oneliner

Beau breaks his rule to debunk false claims about Hunter Biden's case and praises President Biden's hands-off approach towards DOJ.

# Audience

Those interested in clarifying misinformation and understanding political dynamics.

# On-the-ground actions from transcript

- Fact-check misinformation spread about political figures (implied)

# Whats missing in summary

Context on the importance of separating political interference from legal proceedings.

# Tags

#HunterBiden #DOJ #Misinformation #PoliticalDynamics #FactChecking