# Bits

Beau says:

- Raskin, from the Democratic Party, is reaching out to the FBI for more information on allegations against President Biden.
- Raskin is likely seeking an assessment memo from the FBI regarding unverified information on the FD-1023.
- The assessment memo may reveal that the FBI found insufficient evidence to escalate the probe on Giuliani's allegation.
- Republicans were aware of the FBI's assessment but still chose to mislead their base with conspiracy theories.
- Politicians, particularly the Republican Party, continue to mislead their supporters with baseless claims.
- The pursuit of the allegations against President Biden is deemed as a distraction tactic by the Republican Party.
- Republicans intentionally focused on certain unverified information to fuel a scandal.
- Beau suggests that the Republican Party needed a scandal to divert attention from their failed policies.
- Beau hints at the possibility of damaging search results if people were to google "President Admits to Crime on Tape" related to Biden.


# Quotes

- "Who could have seen this coming? So my guess here is that he is looking for what I called the determination memo in that first video."
- "It's not surprising at this point that politicians are doing this, surprising that the same people keep falling for it over and over and over again."
- "They needed a scandal. They needed something to distract from their failed policies or candidates getting in trouble."


# Oneliner

Raskin seeks FBI information on Biden allegations; Republicans knowingly mislead base with unverified claims to distract from failures.


# Audience

Political activists, Democratic supporters


# On-the-ground actions from transcript

- Contact your representatives to demand transparency and accountability in political dealings (implied).
- Share verified information to combat baseless conspiracy theories within your community (implied).


# Whats missing in summary

The full transcript provides detailed insights into the manipulation of information by politicians and the detrimental impact of baseless claims on public perception. It also hints at the need for accountability and transparency in political actions.


# Tags

#Politics #BidenTapes #Republicans #ConspiracyTheories #Transparency