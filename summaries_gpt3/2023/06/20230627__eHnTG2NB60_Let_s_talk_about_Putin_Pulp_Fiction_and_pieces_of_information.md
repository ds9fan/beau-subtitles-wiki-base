# Bits

Beau says:
- Explains how movies can teach us about processing information out of chronological order.
- Uses examples like "The Usual Suspects" and "Pulp Fiction" to illustrate the importance of critical pieces of information.
- Analyzes the Wagner coup attempt and how different pieces of information changed the narrative.
- Emphasizes the need to re-evaluate timelines and not base opinions on initial information.
- Warns against forming incorrect assessments by not considering all the pieces together.

# Quotes

- "Don't base your opinion on the first information you get."
- "Understand the order in which the story breaks is not necessarily the order in which it occurred."
- "If you don't plug that in and acknowledge that that changes the order of events and it changes the reason for Russia's response, your view of it is going to be off."

# Oneliner

Beau explains how movies like "The Usual Suspects" and "Pulp Fiction" teach us to re-evaluate timelines and not base opinions on initial information, using the Wagner coup attempt as an example.

# Audience

Information Consumers

# On-the-ground actions from transcript

- Re-evaluate timelines based on new information (implied)

# Whats missing in summary

Beau's engaging storytelling and detailed analysis.

# Tags

#InformationProcessing #CriticalThinking #WagnerCoupAttempt #ReevaluatingInformation #MoviesAndLearning