# Bits

Beau says:

- Explains the differences between Pence and Trump regarding charging decisions by the Department of Justice.
- Mentions the key element in these types of crimes is intent, specifically willful retention.
- Describes how Team Pence found documents and cooperated fully with the feds to return them.
- Points out Pence's voluntary actions and his lack of intent to retain the documents illegally.
- Speculates that Pence might use this example in his run for presidency to show contrast with Trump.
- Suggests that Pence's actions demonstrate that these standards are consistent and not politically motivated.
- Indicates that Trump may play the victim and cast Pence in a negative light, feeling singled out.
- Concludes by mentioning the unlikelihood of Trump reflecting on his behavior leading to the different outcome.

# Quotes

- "I returned the papers."
- "They had documents they weren't supposed to have."
- "One person returned them, did everything they were supposed to, by the book."
- "It seems unlikely that Trump is going to engage in any introspection."
- "Anyway, it's just a thought, y'all have a good day."

# Oneliner

Beau explains the differences between Pence and Trump's charging decisions by the Department of Justice, focusing on intent and cooperation, potentially impacting Pence's presidential run and shedding light on consistent standards. Trump may play the victim, unlikely to introspect.

# Audience

Political observers

# On-the-ground actions from transcript

- Follow updates on political developments (implied)

# Whats missing in summary

Insight into the potential implications of these charging decisions for future political scenarios.

# Tags

#Pence #Trump #DepartmentOfJustice #Intent #Cooperation #PresidentialRun