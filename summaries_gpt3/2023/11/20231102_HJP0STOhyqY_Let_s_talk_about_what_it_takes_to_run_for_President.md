# Bits

Beau says:

- Explains the impact of money on running for president in the United States.
- Mentions a hedge fund billionaire and GOP mega donor, Cooperman, having issues with the current Republican Party and preferring progressives over Trump.
- Points out the significant money problem Trump might face due to lack of support from big GOP donors.
- Talks about Kennedy, a member of the Kennedy family, running on a third-party ticket against political elites.
- Notes that Kennedy is receiving more money from previous Republican donors, possibly aiming to attract both disaffected Democratic and Republican bases.
- Indicates a divide within the Republican Party with fundraising news not looking good for them.

# Quotes

- "He is a divisive human being who belongs in jail."
- "Trump has an issue, a big one, a big mega donor issue."
- "Words don't mean anything anymore in the United States."
- "None of them are good for the Republican party."
- "Republican fundraising is in disarray."

# Oneliner

Beau explains the impact of money on presidential campaigns, from a GOP mega donor's issues with Trump to Kennedy's unique fundraising approach, signaling disarray in Republican fundraising.

# Audience

Political observers

# On-the-ground actions from transcript

- Contact local political organizations to stay informed about fundraising dynamics (suggested).
- Join fundraising efforts for candidates that represent your views (implied).
- Organize community events to raise awareness about campaign finance issues (exemplified).

# Whats missing in summary

Insights on the importance of grassroots support and donor diversity in political campaigns.

# Tags

#PresidentialCampaigns #PoliticalFundraising #GOPDonors #ThirdPartyCandidates #CampaignFinance