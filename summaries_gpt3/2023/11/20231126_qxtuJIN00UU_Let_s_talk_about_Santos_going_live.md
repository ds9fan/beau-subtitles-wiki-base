# Bits

Beau says:

- Talking about George Santos and his recent online rant.
- Santos believes he will be expelled from Congress.
- Santos called out colleagues for engaging in inappropriate activities, such as being not entirely sober in their duties.
- Santos believes his colleagues are hypocritical and gross.
- Allegations of unethical behavior in Congress are not new.
- Santos indicated that he is not running for reelection.
- Santos mentioned that he dislikes being around his colleagues.
- The animosity and mudslinging in Congress seem significant.
- Santos' expulsion from Congress might happen soon.
- Santos' rant lasted for three hours and included profanity.

# Quotes

- "He's super angry and does not like the people he is in Congress with."
- "Yeah, so that's gonna happen."
- "It was something else. It was it was something else."

# Oneliner

Beau talks about George Santos' explosive online rant, revealing allegations and animosity within Congress, leading to Santos possibly being expelled soon.

# Audience

Political observers

# On-the-ground actions from transcript

- Contact your representatives to address unethical behavior in Congress (implied).

# Whats missing in summary

Context on who George Santos is and further details on the specific allegations against his colleagues.

# Tags

#GeorgeSantos #Congress #Expulsion #PoliticalDrama