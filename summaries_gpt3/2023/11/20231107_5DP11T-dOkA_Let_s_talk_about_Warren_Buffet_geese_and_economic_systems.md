# Bits

Beau says:

- Talking about the economic system in the United States and the concentration of benefits at the very top, exemplified by Warren Buffett.
- Describing Warren Buffett as one of the wealthiest individuals who has greatly profited from the current economic system.
- Criticizing the American myth of the "American Dream" and how it no longer functions as it once did.
- Addressing the defense and support from working-class individuals towards billionaires like Warren Buffett.
- Explaining the exorbitant wealth of billionaires, such as needing to earn a million dollars every year since 1023 to become a billionaire.
- Noting Warren Buffett's shift towards discussing wealth distribution and recognizing the need for adjustments in the system.
- Pointing out that even the billionaire class is starting to acknowledge the inequities in the economic system.
- Anticipating potential reactions from the right wing towards Warren Buffett's views on wealth distribution.
- Comparing Warren Buffett's criticism of the economic system to progressive figures in Congress and the Senate.
- Concluding with a reflection on the evolving perceptions of the economic system.

# Quotes

- "You want to keep a system where the goose lays more golden eggs every year. We've got that. Now the question is, how do those eggs get distributed?"
- "Warren Buffett has a more scathing view of the current economic system than a lot of people that we view as progressives in Congress and in the Senate."

# Oneliner

Beau dives into the skewed economic system, spotlighting Warren Buffett's stance on wealth distribution and the shifting perceptions around it.

# Audience

Economic justice advocates

# On-the-ground actions from transcript

- Question and challenge wealth distribution models (implied)

# Whats missing in summary

Deeper insights into the evolving dynamics of economic inequality and perceptions among different societal classes. 

# Tags

#EconomicJustice #WealthDistribution #WarrenBuffett #AmericanDream #WorkingClass