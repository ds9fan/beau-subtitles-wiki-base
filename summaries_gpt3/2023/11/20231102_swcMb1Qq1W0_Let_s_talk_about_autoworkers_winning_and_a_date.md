# Bits

Beau says:

- The big three automakers were in a dispute with the United Auto Workers.
- The United Auto Workers employed unique tactics during the strike.
- All three automakers eventually came around to the Union's way of thinking.
- Unions and labor organizing set the standard for fair pay and benefits.
- Toyota has reevaluated how they will be paying their employees.
- Toyota's decision may be related to the success of the UAW strike.
- The UAW invited other unions to synchronize contract expiration dates.
- This alignment of contract dates enhances solidarity among unions.
- The UAW renegotiated their contract end date before extending the invitation.
- The UAW's new contract will end on April 30th, 2028, potentially leading to a strike on May 1st.
- Coincidences like this require deliberate planning and coordination.
- Synchronizing contract dates can empower organized labor significantly.
- This strategic move can strengthen unions and their ability to negotiate.
- The power of solidarity among unions can lead to better outcomes for workers.

# Quotes

- "Unions and labor organizing set the standard for what is considered good."
- "With all contracts expiring around the same time, it puts a lot more power into the hands of organized labor."
- "It takes a lot of work to make coincidences like that happen."

# Oneliner

Beau reveals how unions' strategic organizing and solidarity can shift power dynamics in the workforce, impacting fair pay and benefits for all workers.

# Audience

Labor Activists, Union Members

# On-the-ground actions from transcript

- Organize with your union to synchronize contract expiration dates with other unions for increased solidarity and negotiating power. (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the recent auto workers' union strike, showcasing the power of labor organizing and strategic planning in influencing fair pay and benefits for workers.

# Tags

#LaborRights #UnionOrganizing #FairPay #Solidarity #WorkerEmpowerment