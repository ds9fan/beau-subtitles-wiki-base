# Bits

Beau says:

- Explains a theory about the government using airplanes to drop packages with vaccines, sparking from recent coverage despite being a practice since the 90s.
- Mentions the truth behind the theory, where the government indeed drops pellets containing vaccines for rabies, specifically targeting raccoons in the Eastern United States.
- Clarifies that the purpose of dropping these vaccine-containing pellets is to prevent the spread of rabies among raccoons, not to force vaccinations through water supply contamination.
- Notes that the practice has been ongoing for about 20 years, mainly in remote areas, using airplanes equipped with PVC pipes to drop pellets that raccoons consume to prevent rabies.
- Points out that the government is also experimenting with vaccination methods for bats, considering an aerosol approach due to the nature of bats flying.
- Emphasizes the success of the program in curbing the spread of rabies and hints at potential expansions in the future.

# Quotes

- "If you hear this, that the government is dropping vaccines from the sky, I mean, it's true, but it's for rabies and trash pandas."
- "They are using airplanes to dump little pellets. They're not pellets. They kind of look like granola bars."

# Oneliner

Beau explains the truth behind the theory of vaccines being dropped from airplanes, revealing the government's efforts to prevent rabies among raccoons and bats.

# Audience

Science enthusiasts, animal lovers

# On-the-ground actions from transcript

- Contact local wildlife organizations to learn more about rabies prevention efforts for animals like raccoons and bats (suggested).
- Join community initiatives focused on wildlife conservation and disease prevention (implied).

# Whats missing in summary

The full transcript provides additional insights into the government's long-standing practice of dropping vaccine-containing pellets to prevent rabies among wildlife, shedding light on misconceptions and conspiracy theories surrounding the topic.

# Tags

#Vaccines #RabiesPrevention #GovernmentInitiatives #WildlifeConservation #ConspiracyTheories