# Bits

Beau says:

- Meadows facing a lawsuit from the company that published his book.
- The company claims Meadows breached warranties about the truthfulness of the book.
- The company wants $350,000 back for an advance, $600,000 for expenses, and more.
- Meadows warned Trump against claiming election fraud but allegedly said something else under oath.
- Eastman facing disciplinary action for breaching professional ethics.
- Eastman's license might be jeopardized, impacting his decision on a plea deal.
- The organization involved in the suit is called States United.
- The senior VP of legal at States United believes Eastman had no legal basis for his actions.
- These legal actions stem from fallout due to Trump's attempts to cling to power post-election.

# Quotes

- "Y'all have a good day."
- "That's a pretty hefty sum."
- "It's just a thought."

# Oneliner

Meadows faces a lawsuit over breached book warranties, while Eastman's disciplinary actions may impact his plea deal decision amid fallout from Trump's post-election power struggle.

# Audience

Legal analysts

# On-the-ground actions from transcript

- Stay informed on legal proceedings and ethical breaches (suggested)
- Advocate for accountability in legal and ethical matters (exemplified)

# Whats missing in summary

Insights on the potential ripple effects of these legal cases on future political landscapes.

# Tags

#LegalProceedings #Ethics #Trump #Accountability #Fallout