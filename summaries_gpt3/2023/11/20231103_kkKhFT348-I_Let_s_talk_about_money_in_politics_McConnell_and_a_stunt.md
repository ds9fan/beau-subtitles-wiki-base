# Bits

Beau says:

- Senator Howley aims to introduce a bill to limit corporate funding in elections, seemingly targeting Citizens United.
- Senate Majority Leader McConnell warned Republicans not to support the bill, indicating it lacks party backing.
- Howley's bill may not have the support to pass, as it challenges a Supreme Court ruling based on constitutional grounds.
- Democrats are unlikely to support the bill, not due to opposing money in politics, but because it may not hold up in court and could provide Howley with a political win.
- Howley's move appears more symbolic for his base rather than a practical solution to money in politics.
- The bill seems more about signaling an attempt rather than achieving tangible results.
- Howley's tactic may enhance his credibility with his supporters, even if the bill is bound to fail.
- The core idea behind the bill isn't necessarily bad, but its feasibility and understanding of the political landscape are questionable.

# Quotes

- "We need to get money out of politics, that type of thing."
- "It's all about signaling. It's not about getting anything done."
- "This senator doesn't actually understand how things work."
- "Look, I tried to do something, but those people, the swamp or whoever, they wouldn't let it happen."
- "Maybe he just doesn't understand it, but that seems unlikely."

# Oneliner

Senator Howley's bill to limit corporate election funding appears more symbolic than effective, lacking support and legal feasibility, ultimately serving as a political gesture rather than a substantial change in money in politics.

# Audience

Politically active individuals

# On-the-ground actions from transcript

- Address the need to get money out of politics in real, tangible ways by supporting or engaging with organizations working towards campaign finance reform (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of Senator Howley's proposed bill, offering insights into the political dynamics and feasibility of the legislation.

# Tags

#MoneyInPolitics #CampaignFinance #SenatorHowley #CitizensUnited #PoliticalAnalysis