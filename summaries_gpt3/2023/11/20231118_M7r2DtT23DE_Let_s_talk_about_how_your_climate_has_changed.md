# Bits

Beau says:

- Climate change has already occurred, not just a future event.
- USDA's hardiness zone map has been updated, showing an average increase of two and a half degrees across the country.
- The change in hardiness zones will affect what plants can be grown in different areas.
- The two and a half degrees increase doesn't mean it happened in the last 10 years but suggests a trend towards it.
- Use the updated hardiness zone map to show skeptics the impact of climate change.
- People who garden will have to adjust what they grow due to the changing climate.
- The shift in hardiness zones is something familiar even to conservatives and skeptics.
- In some areas, the change in hardiness zones is dramatic, allowing for the growth of tropical plants in unexpected places.
- Show last year's map of hardiness zones alongside this year's to demonstrate the difference.
- Climate change is no longer a future threat but is already impacting everyone, regardless of their beliefs or feelings.

# Quotes

- "Climate change has already occurred, not just a future event."
- "Use the updated hardiness zone map to show skeptics the impact of climate change."
- "Climate change is no longer a future threat but is already impacting everyone."

# Oneliner

Beau explains how climate change has already occurred, using the updated hardiness zone map to show skeptics its impact, stressing that it's no longer a future threat but a present reality affecting everyone.

# Audience

Gardeners, climate activists, skeptics

# On-the-ground actions from transcript

- Show skeptics the updated hardiness zone map to demonstrate the impact of climate change (suggested).

# Whats missing in summary

The full transcript provides detailed insights into how climate change has already impacted gardening practices through the updated hardiness zone map and serves as a tool to convince skeptics of the reality of climate change.

# Tags

#ClimateChange #HardinessZones #Gardening #Skepticism #Conservatives