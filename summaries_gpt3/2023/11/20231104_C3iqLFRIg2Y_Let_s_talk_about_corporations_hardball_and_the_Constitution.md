# Bits

Beau says:

- Addressing questions about Senator Hawley's bill and its constitutionality.
- Explaining the need for a constitutional amendment or different Supreme Court judges for change.
- Critiquing the notion of "activist judges" in the context of Citizens United.
- Challenging the idea of basing arguments on what the founders intended regarding corporate personhood.
- Advocating against viewing corporations as people due to historical context.
- Suggesting that supporting Hawley's bill could be a strategic political move for Democrats.
- Emphasizing the importance of continuing the money in politics debate through supporting the bill.
- Speculating on the potential outcomes and political strategies related to the bill.
- Noting the Democratic Party's historical approach to playing politics.
- Acknowledging the potential repercussions of supporting Hawley's bill on his re-election.
- Commenting on the unlikely scenario of Democrats playing hardball politics.
- Sharing a personal perspective on how he might handle the situation differently.

# Quotes

- "I'm fairly certain the founders would not have viewed corporations as people when they didn't view all people as people."
- "You carry that forward. It will almost certainly be defeated. It won't stand when it gets to the courts."
- "Democratic Party does not really do hardball well."
- "I just don't see Schumer doing that."
- "I'm fairly certain the founders wouldn't view corporations as people when they didn't view all people as people."

# Oneliner

Beau breaks down the constitutionality of Senator Hawley's bill, questions activist judges, and strategizes on Democratic Party moves regarding money in politics.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Rally support for strategic political moves within your community (implied)
- Continue advocating for critical political discourse and actions (implied)

# Whats missing in summary

Insight into Beau's unique perspective and analysis

# Tags

#SenatorHawley #Constitutionality #DemocraticParty #MoneyInPolitics #ActivistJudges