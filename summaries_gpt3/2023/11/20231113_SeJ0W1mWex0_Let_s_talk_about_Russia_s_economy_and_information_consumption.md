# Bits

Beau says:

- Explains a scenario where a meme claims the ruble is outperforming the dollar, leading to confusion.
- Advises on how to be better consumers of information by looking at additional context.
- Illustrates how cherry-picking data can create a misleading narrative about the Russian economy.
- Mentions a deliberate effort by Putin to boost the ruble by ordering companies to sell foreign cash.
- Suggests that the reported surge in the ruble might be an intentional attempt to downplay the impact of economic sanctions.
- Points out that countries under sanctions won't admit they're not working, even if they are, to avoid strengthening their opposition.
- Encourages looking at a broader picture and considering events surrounding a situation for better context.

# Quotes

- "So if you look at this whole chunk of information, you see something that might very well be intentional."
- "You don't know it. It could just be happenstance that it got reported the way that it did, but it could be intentional."
- "It's a good way to kind of train yourself to look for more context, broader picture."
- "If it is true, why did it happen? When did it start? Look for events at that time."
- "Y'all have a good day."

# Oneliner

Beau explains the danger of cherry-picking data and deliberate efforts to spin narratives about the Russian economy, urging for a broader context in information consumption.

# Audience

Consumers of news

# On-the-ground actions from transcript

- Investigate and verify information before drawing conclusions (implied).
- Look for additional context and broader picture when consuming information (implied).

# Whats missing in summary

The detailed breakdown of the specific events surrounding the surge in the ruble and the implications of economic sanctions.

# Tags

#Currency #Russia #InformationConsumption #EconomicSanctions #Putin