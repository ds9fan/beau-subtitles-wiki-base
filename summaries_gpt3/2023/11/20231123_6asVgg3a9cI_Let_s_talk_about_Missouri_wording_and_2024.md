# Bits

Beau says:

- Missouri Supreme Court declined to hear a case about partisan wording on a reproductive rights ballot initiative.
- Secretary of State proposed phrasing the question in a partisan manner.
- The current Secretary of State plans to run for governor in 2024, making reproductive rights a cornerstone of the campaign.
- Seven states have protected access to reproductive rights when the question was put to voters.
- Despite Republican push, reproductive rights isn't a divisive issue when voters have a say.
- Democrats might leverage ballot initiatives to drive voter turnout.
- Republicans risk alienating younger demographics by continuing to push on this losing issue.
- Nonpartisan approach is being maintained in Missouri Supreme Court regarding ballot wording.
- Having reproductive rights on the ballot is likely to influence Democratic voter turnout in Missouri.

# Quotes

- "There have been seven states that have put this question to the voters in one way or another."
- "It's been a position of the Republican Party for a really long time, so they're slow to change on this."
- "Even in Missouri, the Supreme Court is still trying to maintain at least some form of nonpartisan language when it comes to this topic being on the ballot."

# Oneliner

Missouri's Supreme Court declines partisan wording on reproductive rights ballot initiatives, potentially influencing Democratic voter turnout in 2024.

# Audience

Voters, activists, residents

# On-the-ground actions from transcript

- Contact local organizations advocating for reproductive rights to get involved in ballot initiatives (implied)
- Support and volunteer for political campaigns focusing on protecting reproductive rights (implied)

# Whats missing in summary

Context on potential impacts of voter turnout and the significance of nonpartisan approaches in ballot initiatives.

# Tags

#ReproductiveRights #Missouri #SupremeCourt #DemocraticParty #RepublicanParty