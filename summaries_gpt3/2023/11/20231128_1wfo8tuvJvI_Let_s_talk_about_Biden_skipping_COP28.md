# Bits

Beau says:

- Biden is skipping COP28, the climate conference hosted in the UAE, a massive oil producing country.
- The UAE planned to use the conference to work out new oil and gas deals.
- Despite being the 28th conference, there's still no plan to phase out fossil fuels.
- Some believe that COP conferences have become getaways for heads of state rather than focusing on climate.
- Biden's absence may send a message that the conference has deviated from its purpose.
- Climate change is a global issue, and every country has a stake in it.
- Biden's decision to skip the conference may not be solely due to the UAE's hosting or the oil and gas deals.
- The President is currently dealing with multiple pressing issues and a trip to the UAE may not fit in his schedule.
- Regardless of Biden's intention, his absence could still send a strong message.
- Beau supports the idea of sending a message by skipping the conference.

# Quotes

- "Biden is skipping COP28, the climate conference hosted in the UAE, a massive oil producing country."
- "Despite being the 28th conference, there's still no plan to phase out fossil fuels."
- "Some believe that COP conferences have become getaways for heads of state rather than focusing on climate."

# Oneliner

Biden skipping COP28 in the UAE sends a message about the conference's deviation from climate focus amid pressing global issues.

# Audience

Climate activists, policymakers

# On-the-ground actions from transcript

- Advocate for stronger climate commitments at local and national levels (implied)

# Whats missing in summary

More details on Biden's complex schedule and global challenges he is currently facing.

# Tags

#Biden #COP28 #ClimateChange #UAE #FossilFuels