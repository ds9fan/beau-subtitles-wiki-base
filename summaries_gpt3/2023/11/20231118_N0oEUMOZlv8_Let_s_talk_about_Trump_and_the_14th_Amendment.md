# Bits

Beau says:

- Trump being banned from the ballot is being attempted in multiple states using the 14th Amendment due to his involvement in an insurrection.
- The court in Colorado believes Trump engaged in insurrection but should still be on the ballot because his oath doesn't include supporting the Constitution.
- The 14th Amendment disqualifies individuals who engaged in insurrection, but the presidential oath doesn't explicitly include support.
- There may be other cases where judges rule against Trump being on the ballot, eventually leading to a Supreme Court decision.
- The Supreme Court might find an alternative reason to allow Trump on the ballot despite the automatic disqualification suggested by the 14th Amendment.
- There could be a need for a unified finding for the Supreme Court to make a decision regarding Trump's eligibility based on the 14th Amendment.
- The Supreme Court might stick with the current ruling or interpret it differently to support Trump's candidacy based on the wording of the 14th Amendment.
- Changing the presidential oath to include support might be a suggestion, but it's unlikely to happen easily through Congress.
- Beau doesn't see this legal mechanism as the one that will ultimately prevent Trump from being on the ballot.

# Quotes

- "Trump engaged in an insurrection, he can't be on the ballot because the 14th Amendment prohibits those."
- "There may be other cases where judges rule against Trump being on the ballot."
- "It's probably not going to be what happens."
- "I do not necessarily believe that was the intent of the people who drafted it."
- "I knew this argument was being made. I honestly did not expect it to go anywhere."

# Oneliner

Colorado court believes Trump incited insurrection but should remain on ballot; Supreme Court likely to decide differently.

# Audience

Legal Analysts

# On-the-ground actions from transcript

- Challenge legal decisions through proper channels (implied)

# Whats missing in summary

Context on the potential implications of these legal battles for future elections.

# Tags

#Trump #14thAmendment #LegalChallenge #SupremeCourt #PresidentialOath