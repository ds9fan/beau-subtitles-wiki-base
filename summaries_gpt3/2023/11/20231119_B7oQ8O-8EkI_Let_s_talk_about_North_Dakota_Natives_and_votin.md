# Bits

Beau says:

- A federal judge in North Dakota found the 2021 redistricting map violated the Voting Rights Act by packing one tribe and cracking another to dilute voting power.
- The judge gave North Dakota until December 22nd to remedy the situation, requiring a special session due to the state's unique legislative schedule.
- Republicans are considering whether to appeal the judge's decision or call a special session.
- There is a pattern of Republicans gerrymandering to protect their power, leading to legal battles and judges striking down maps.
- Despite potential appeals, it seems likely that the natives will come out on top and a new map will be ordered.
- The violation in North Dakota may not be as severe as in some southern states, but it appears intentional.
- The situation is being closely monitored to see how it unfolds, with hopes for a quick resolution.

# Quotes

- "A federal judge in North Dakota found the 2021 redistricting map violated the Voting Rights Act."
- "Republicans are considering whether to appeal the judge's decision or call a special session."
- "It appears likely that the natives will come out on top and a new map will be ordered."

# Oneliner

A federal judge in North Dakota found the 2021 redistricting map violated the Voting Rights Act, leading to a potential shift in favor of natives despite Republican gerrymandering patterns.

# Audience

Advocates, Voters, Activists

# On-the-ground actions from transcript

- Contact local representatives to advocate for fair redistricting (suggested).
- Stay informed about updates on the situation and share with others (implied).

# Whats missing in summary

The full transcript provides more details on the legal battles surrounding gerrymandering and the impact on voting rights in North Dakota.