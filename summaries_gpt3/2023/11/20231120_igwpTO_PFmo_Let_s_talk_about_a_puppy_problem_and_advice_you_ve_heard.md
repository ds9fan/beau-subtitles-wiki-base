# Bits

Beau says:

- A lasting respiratory disease affecting dogs, causing pneumonia, has been noticed in Oregon, Colorado, and New Hampshire.
- The disease, possibly antibiotic-resistant, has shown an uptick in cases over the past year.
- Despite over 200 identified cases in Oregon, experts advise not to panic as there hasn't been a significant increase in dog fatalities.
- The recommended precaution for dog owners is to practice social distancing for their pets by reducing contact with other dogs.
- Researchers are actively working on identifying and combating the disease, but no concrete solutions have been found yet.
- The lack of a centralized system for tracking and addressing the disease presents a challenge.
- It is vital for dog owners in the affected areas to stay informed, especially if they frequent dog parks.
- The disease manifests with symptoms like coughing and sneezing, resembling common respiratory issues.
- Updating vaccines, particularly those targeting respiratory illnesses, is advised as a precautionary measure.
- The disease is currently unnamed, described only as a lasting respiratory illness causing pneumonia in dogs.

# Quotes

- "First, don't panic. They're saying that it's bad, it's rough, but they haven't noticed a large uptick in the number of dogs lost to this."
- "Dogs need to socially distance."
- "It's more of a coalition of the willing than a well-thought-out system so far."

# Oneliner

A respiratory disease affecting dogs in Oregon, Colorado, and New Hampshire prompts caution and social distancing for pets, with ongoing research to identify and combat the illness.

# Audience

Dog Owners

# On-the-ground actions from transcript

- Stay informed about the disease and its developments, especially if you live in areas like Oregon, Colorado, and New Hampshire (suggested).
- Practice social distancing for your dogs by reducing contact with other dogs (suggested).
- Update your dog's vaccines, especially those targeting respiratory issues (suggested).

# Whats missing in summary

The full transcript provides additional details on the ongoing research efforts and the lack of a centralized system to address the disease.

# Tags

#RespiratoryDisease #Dogs #PetHealth #Precautions #Research