# Bits

Beau says:
- Valley Labor Report is starting a live stream supporting union activity in Tennessee and Florida, lasting for 32 hours to draw attention to the idea of a 32-hour work week.
- Before the 40-hour work week, the average work week was 61 hours, showing that change is possible.
- Moving to a 32-hour work week aims to create more leisure time, increase productivity, and improve work-life balance.
- Companies with brand loyalty use a pricing method called "whatever the market will bear" to charge the maximum customers will pay.
- Corporations are profit-driven, and their main goal is to provide value to shareholders, not create social change.
- Organizing is key to making dreams, like a shorter work week, a reality.
- People once thought a 40-hour work week was impossible, but now it's the norm.
- Many dreams become reality when people work towards them.
- The Valley Labor Report's live stream will feature guest appearances from other YouTube channels over the next 32 hours.
- Beau encourages viewers to join the live stream and have a good day.

# Quotes

- "Before it happens, everything was just something in somebody's imagination. It was their dream."
- "Most companies charge the absolute maximum that people will pay before they start losing customers."
- "A whole lot of dreams become reality if people work for it."

# Oneliner

Valley Labor Report's 32-hour live stream supports a shorter work week, challenging norms and advocating for change in labor practices and corporate values.

# Audience

Labor advocates, workers

# On-the-ground actions from transcript

- Join the Valley Labor Report's live stream to support union activity and the idea of a 32-hour work week (suggested).
- Participate in organized efforts to advocate for better work-life balance and increased leisure time (implied).

# Whats missing in summary

The full transcript contains insights on labor practices, corporate behavior, and the power of collective action.