# Bits

Beau says:

- NASA used lasers to communicate with a spacecraft headed to an asteroid called Psyche.
- The beams of light traveled 10 million miles, enabling the transmission of a lot more information compared to traditional methods.
- The communication system used is DSOC, which stands for Deep Space Optical Communications.
- This development marks a significant advancement in communication technology in deep space.
- The asteroid Psyche is unique as it is believed to be formed from the core of a planet.
- It is a metal asteroid that has the potential to provide valuable insights into space objects humanity has not closely observed before.
- The DSOC test conducted was part of a larger mission to study the asteroid up close.
- This technology could become more common in future deep space missions, improving information transmission speed.
- NASA's use of lasers for communication showcases a new way to send information back from deep space.
- The success of this communication test is a promising step towards more efficient deep space exploration.

# Quotes

- "NASA used lasers to communicate with a spacecraft headed to an asteroid called Psyche."
- "This development marks a significant advancement in communication technology in deep space."
- "The asteroid Psyche is unique as it is believed to be formed from the core of a planet."

# Oneliner

NASA's use of lasers for deep space communication with the asteroid Psyche marks a significant advancement in technology and exploration, paving the way for faster information transmission.

# Audience

Space enthusiasts, technology innovators.

# On-the-ground actions from transcript

- Learn more about deep space communication technology and its implications (suggested).
- Stay updated on advancements in space exploration and technology (suggested).

# Whats missing in summary

The full transcript provides detailed insights into NASA's innovative use of lasers for deep space communication and the unique characteristics of the asteroid Psyche.

# Tags

#NASA #SpaceExploration #DeepSpaceCommunication #AsteroidPsyche #Technology #Innovation