# Bits

Beau says:

- Addressing a recent incident in the United States Senate involving a Republican Senator from Oklahoma, Bernie Sanders, and a union member named Sean O'Brien.
- During a hearing about economic issues, the senator confronted O'Brien for past social media comments, leading to a tense exchange.
- The senator appeared ready to physically fight O'Brien during the hearing, showcasing a lack of maturity and professionalism.
- Beau warns against picking fights with Teamsters, citing their reputation for toughness and solidarity.
- Beau contrasts the current behavior in the Senate with past experiences where physical confrontations were more common but not celebrated.
- He questions the display of aggression in a legislative setting and suggests that allowing Bernie Sanders to calm the situation didn't convey the intended message.
- O'Brien's response to the senator's challenge with a smile indicated a non-intimidated demeanor, contrasting with the senator's attempt to appear tough.
- Beau criticizes the trend of valuing politicians' tough-guy behavior, equating it to immature displays from high school.
- He expresses disappointment in the lack of maturity and adult behavior in the Senate, suggesting that such conduct doesn't truly represent aggressive individuals.
- Beau likens the incident to a senator trying to portray a cowboy image inadequately, pointing out the transparency of such attempts.

# Quotes

- "First things first, you never fight a teamster."
- "If your intent is to show that you are just a super aggressive man and you're ready to go anywhere, even in the middle of this Senate hearing, maybe it's best that you don't allow Bernie Sanders to strong-arm you back into your chair with words."
- "We're comparing the behavior of today to the behavior of my friends in high school."
- "Acting like my friends from high school? Probably not. Probably not the image you should be trying to cast."
- "It's supposed to be the most deliberative body in the United States."

# Oneliner

Beau delves into a Senate confrontation, warning against aggression, questioning maturity, and critiquing tough-guy politics in a legislative setting.

# Audience

Legislators, political commentators.

# On-the-ground actions from transcript

- Contact your representatives to advocate for professionalism and maturity in legislative conduct. (implied)
- Organize community dialogues on respectful communication and conflict resolution in political settings. (implied)

# Whats missing in summary

The full transcript provides additional context on the incident in the Senate and Beau's reflections on political behavior and maturity.

# Tags

#Senate #PoliticalConduct #Professionalism #Teamsters #BernieSanders