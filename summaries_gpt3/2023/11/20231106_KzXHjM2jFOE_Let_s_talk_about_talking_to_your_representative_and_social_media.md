# Bits

Beau says:

- Explains the importance of how you phrase your demands when contacting representatives.
- Differentiates between rhetoric on social media and effective communication with representatives.
- Advises on appealing to representatives' self-interest to influence their decisions.
- Emphasizes the impact of threatening to primary politicians as a more effective strategy than merely stating you won't vote for them.
- Advocates for supporting candidates who share your views as a proactive approach.
- Emphasizes that active involvement in politics is more impactful than occasional voting.

# Quotes

- "Voting once every four years, once every two years, it's not enough."
- "Getting involved in running a super-progressive candidate in the primary, that's hard."
- "The representative democracy that exists in the United States, it is advanced citizenship."
- "It might be more effective just to try to support somebody that you do want to vote for."
- "If you do that and they win, and they get up there, when you call them and you say, hey, you need to re-evaluate your position here, or we may have to run another candidate, they're going to know you will do it."

# Oneliner

Be mindful of how you phrase demands to representatives, appeal to their self-interest, and understand that supporting candidates who share your views is more effective than mere threats of not voting.

# Audience

Citizens, Activists, Voters

# On-the-ground actions from transcript

- Support a candidate who shares your views and actively back them (suggested)
- Threaten to primary politicians who do not support your interests (suggested)

# Whats missing in summary

The full transcript provides detailed insights into the nuances of effectively communicating with representatives and influencing political decisions through strategic actions like supporting candidates and threatening to primary politicians.