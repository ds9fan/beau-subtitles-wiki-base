# Bits

Beau says:

- Acknowledges a generational gap in reading habits, with kids preferring movies over books due to easy access to information on their phones.
- Recommends adapting to modern technology by using e-books or the LibriVox app to make reading more appealing to children.
- Shares a personal story about how a chance encounter with an astronaut on a plane inspired his love for reading.
- Emphasizes the importance of reading for increasing knowledge and suggests sharing the astronaut story with kids to encourage reading.
- Advises making reading fun rather than a chore for kids, recognizing the need to adapt to changing times and technologies.

# Quotes

- "No knowledge is wasted."
- "People read what they want to for entertainment, but all books increase knowledge."
- "We shouldn't turn into the people screaming, get off my lawn."

# Oneliner

Beau addresses the generational gap in reading habits, suggests adapting to modern technology to make reading fun, and shares a story to inspire a love for reading. 

# Audience

Parents, caregivers, educators

# On-the-ground actions from transcript

- Share the story of the astronaut encounter to inspire a love for reading among kids (suggested).
- Introduce kids to e-books or the LibriVox app to make reading more engaging (suggested).
- Make reading a fun activity rather than a chore for children (implied).

# Whats missing in summary

The full transcript provides a deeper insight into Beau's personal connection to reading and offers practical tips on how to encourage children to read in a technology-driven world.

# Tags

#Reading #Children #Adaptation #Knowledge #Inspiration