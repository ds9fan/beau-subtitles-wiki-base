# Bits

Beau says:

- President Biden expressed doubt about the numbers put out by the Ministry of Health in Gaza regarding the number of people lost.
- Beau acknowledges his history of doubting official numbers in conflicts and disasters, explaining his skepticism towards numbers that end in zero due to them being estimates.
- He points out that Biden's public perception was affected negatively when he made similar comments about estimates, especially concerning civilian or children lost.
- Beau mentions that Biden privately thinking of numbers as estimates is different from having a public record of doubting official figures.
- Despite Biden apologizing and meeting with Muslim American community leaders to address the issue, Beau believes questioning numbers immediately may not always be appropriate.
- Beau underscores the importance of waiting for more accurate numbers before drawing conclusions, especially in situations involving a large loss of civilians.
- He notes that minimizing civilian losses politically might not be advisable and can lead to unnecessary confusion and misinterpretation.
- Beau mentions that high emotions during times of loss can be exacerbated by careless comments, like the one made by President Biden.
- He praises Biden for acknowledging his mistake, apologizing, and spending extra time with those affected, indicating a positive step towards rectifying the situation.
- Beau concludes by reflecting on the incident and suggesting that acknowledging and rectifying mistakes is more significant than defending them.

# Quotes

- "I doubt any number that ends in zero, not just in conflict, but in natural disaster or mass incident, because they're estimates."
- "The public perception of that, yeah that's not cool."
- "When you have that kind of loss, emotions are high and there's no reason to add any additional confusion to it."
- "But it was a mistake."
- "Anyway, it's just a thought y'all have a good day."

# Oneliner

President Biden's public doubt about official numbers triggers scrutiny from Beau, who stresses the importance of waiting for accurate figures and handling sensitive topics with care and empathy.

# Audience

Media consumers

# On-the-ground actions from transcript

- Wait for more accurate numbers before drawing conclusions (implied)
- Handle sensitive topics with care and empathy (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of President Biden's public doubt regarding official figures and how Beau views such skepticism, stressing the importance of handling sensitive topics with empathy and waiting for accurate information before forming opinions.

# Tags

#PresidentBiden #OfficialNumbers #Skepticism #Empathy #Accuracy