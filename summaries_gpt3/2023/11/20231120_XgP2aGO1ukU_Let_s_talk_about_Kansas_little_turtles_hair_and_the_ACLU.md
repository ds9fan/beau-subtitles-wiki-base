# Bits

Beau says:

- The American Civil Liberties Union is involved in a case in Kansas where an eight-year-old boy was allegedly forced to cut his hair.
- The boy, who is Native American, decided to grow his hair after attending a cultural event called "little turtles."
- The school has a policy on hair length that only applies to boys, which the ACLU argues violates religious freedom and involves sex-based discrimination.
- The boy's mother tried to get an exemption from the school, but none was granted.
- The ACLU has given the school until December 1st to address the issue, or it may proceed to a courtroom setting.
- Hair length is a significant issue for those with closely held religious beliefs.
- The ACLU's involvement suggests that the case may escalate beyond a mere request for change.
- Beau anticipates that the ACLU is likely to win on the federal side of this issue.
- The importance of this case lies in the intersection of religious beliefs and discriminatory policies.
- Beau will continue to follow this case closely and provide updates.

# Quotes

- "The school has a policy about hair length and it only applies to boys."
- "You really don't get more closely held religious belief than this."
- "I'm curious to see what the school does and whether or not this proceeds to levels above just hey you need to change this."

# Oneliner

The ACLU challenges a Kansas school's policy on hair length, citing religious freedom and potential discrimination, giving a deadline of December 1st for resolution.

# Audience

School Administrators, Civil Rights Activists

# On-the-ground actions from transcript

- Contact the school to express support for the boy and urge them to address the discriminatory policy (suggested).
- Stay informed about updates on the case and be ready to take action in solidarity with the affected family (exemplified).

# Whats missing in summary

Importance of standing up against discriminatory policies and supporting individuals asserting their religious freedom rights.

# Tags

#ACLU #ReligiousFreedom #Discrimination #KansasSchool #CivilRights