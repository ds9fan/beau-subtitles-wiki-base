# Bits

Beau says:

- Mayor of New York, Eric Adams, had his phones and electronic devices taken by the FBI, reportedly for looking into campaign finance and money from overseas.
- Reports differ on whether Mayor Adams willingly gave his phones to the FBI or if they were seized with a warrant.
- The mayor's office insists he is cooperating and has nothing to hide, implying voluntary surrender of the phones.
- If the phones were seized with a warrant, it suggests suspicion of wrongdoing, requiring legal representation.
- Conflicting reports create uncertainty, with no clarification from the FBI.
- Forming a definite opinion before clarity on the situation is cautioned against due to the stark contrast between scenarios.
- Lack of sufficient information at the time of filming to determine the actual sequence of events.
- Regardless of how the situation unfolds, the FBI is currently examining the Mayor of New York's phone.
- The ongoing saga surrounding this incident indicates it will not be resolved swiftly or quietly.
- Further developments are awaited to understand the true nature of the situation, indicating that this issue will persist.

# Quotes

- "There's just a massive amount of difference between those two scenarios."
- "This is not a story that's going to go away anytime soon."

# Oneliner

Mayor of New York's phones seized by FBI; conflicting reports create uncertainty, stressing the importance of awaiting clarification before forming opinions.

# Audience

Concerned Citizens

# On-the-ground actions from transcript

- Contact local officials or organizations for updates on the situation (suggested)
- Stay informed through reliable news sources to understand the evolving narrative (implied)

# Whats missing in summary

The full transcript provides detailed insights into the conflicting reports surrounding the seizure of Mayor Eric Adams' phones by the FBI, urging viewers to withhold judgment until further clarity emerges.

# Tags

#NewYork #FBI #Mayor #EricAdams #Conflict #Investigation