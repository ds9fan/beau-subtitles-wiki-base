# Bits

Beau says:

- Louisiana facing a Voting Rights Act issue due to underrepresentation of Black population in congressional districts.
- Federal court determined the current maps likely violate the Voting Rights Act.
- Deadline for enacting a new map is January 15th.
- Outgoing governor can call a special session before January 8th when the new governor takes office.
- Potential for delay in enacting new maps due to the holiday season.
- Appeals court provided wiggle room for a small extension, but a prolonged delay could lead to a trial.
- Republican Party may not want a trial as their arguments for current maps may not fare well in public scrutiny.
- Likely scenario: new governor calls for a session, gets a small extension, and enacts a new map.
- There might still be attempts to delay or manipulate the map-drawing process in favor of Republicans.
- Federal courts agree that the current map violates the Voting Rights Act.

# Quotes

- "The question for Republicans is it worth trying to hold off until the next election when they probably won't be able to, in all the ways they have left, put them in the position of openly saying, well, we don't want black people's votes to count."
- "There's an outgoing governor. The incoming governor doesn't come in until January 8th."
- "It seems like something that they [Republican Party] would want to avoid and not just openly say, no, we want a gerrymander."
- "The order from the appeals court is pretty clear and there is a consensus among the federal courts that this map, the current map, likely violates the Voting Rights Act."
- "So we'll have to wait and see though."

# Oneliner

Louisiana faces Voting Rights Act violation due to underrepresentation of Black population in congressional districts; deadline to redraw maps by January 15th with potential political maneuvering.

# Audience

Louisiana residents, activists

# On-the-ground actions from transcript

- Contact local representatives to ensure fair redistricting (implied)
- Stay informed about the map-drawing process and advocate for equitable representation (implied)

# Whats missing in summary

Insights on the potential impact of delayed map redrawing on fair representation and voter rights in Louisiana.

# Tags

#Louisiana #VotingRightsAct #Redistricting #RepublicanParty #FairRepresentation