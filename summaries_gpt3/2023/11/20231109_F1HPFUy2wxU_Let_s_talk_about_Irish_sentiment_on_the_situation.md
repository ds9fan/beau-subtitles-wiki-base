# Bits

Beau says:

- Explains the vested interest of Irish and Irish-Americans in the Middle East conflict.
- Points out the misconception that the Irish were once supportive of the US but now support Palestinians due to becoming leftist, clarifying that Irish Republicans were always leftists.
- Attributes the change in Irish support to the partition of Ireland mirroring the partition of Israel, leading to a shift in identification from Israelis to Palestinians.
- Mentions the lasting impact on Irish politics, with overt connections drawn during the Troubles in Ireland.
- Describes the strong connection between Irish Americans and the Palestinian cause, influenced by historical dynamics.
- Addresses the coverage of conflicts and how they elicit reactions based on the interests of different groups in the United States.
- Emphasizes that countries don't have friends, only interests, which can change over time.
- Stresses that war and conflict are inherently bad, showcasing the importance of understanding different perspectives and interests.

# Quotes

- "Countries don't have friends. They have interests."
- "War is bad. It's always horrible."
- "It's all based on the perceptions of the time and the interests at the time."

# Oneliner

Beau explains the historical roots behind the strong interest of Irish and Irish-Americans in the Middle East conflict, clarifying misconceptions and shedding light on the shifting dynamics that influence perspectives and reactions.

# Audience

Irish Americans, Middle East conflict observers

# On-the-ground actions from transcript

- Understand the historical perspectives and dynamics that shape different communities' interests in conflicts (implied).
- Educate oneself on the connections between historical events and current sentiments in communities affected by conflicts (implied).

# Whats missing in summary

In-depth exploration of the complex historical and political factors influencing Irish and Irish-American perspectives on the Middle East conflict. 

# Tags

#Irish #IrishAmericans #MiddleEastConflict #HistoricalPerspectives #PoliticalDynamics