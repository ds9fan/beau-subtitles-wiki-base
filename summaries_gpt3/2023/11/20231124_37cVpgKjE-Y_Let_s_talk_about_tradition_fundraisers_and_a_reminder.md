# Bits

Beau says:

- Heading into the holiday season, it can be a tough time for many who may not want to be alone but have no choice.
- Invites viewers to think about inviting someone who came to mind over during this season.
- Conducts a yearly fundraiser through a live stream to benefit people in domestic violence shelters.
- Focuses on fulfilling immediate needs and providing gifts for older kids in shelters.
- The fundraiser tradition ensures that older kids receive tablets and items to call their own.
- Accidentally did the live stream fundraiser recently and raised close to $10,000.
- Plans to round it up to $10,000 to cover all needs and potentially provide excess funds.
- Explains there won't be an Amazon registry like in the past for miners on strike, but will provide addresses and lists for those willing to send in donations.
- Expresses gratitude for the unexpected success of the fundraiser.
- Assures viewers they will be updated on the fundraiser's progress and ways to contribute.

# Quotes

- "It's become a tradition on the channel."
- "We didn't actually plan on doing it until after Thanksgiving."

# Oneliner

Beau reminds viewers of the tough holiday season ahead, shares about the accidental live stream fundraiser success, and plans for future ways to contribute.

# Audience

Community members, supporters

# On-the-ground actions from transcript

- Send in donations to the shelter addresses provided (suggested)
- Stay updated on the fundraiser progress and ways to contribute (implied)

# Whats missing in summary

The full transcript provides insight into Beau's compassionate efforts during the holiday season, showcasing a successful fundraiser for domestic violence shelters and plans for future contributions.

# Tags

#HolidaySeason #Fundraiser #DomesticViolenceShelters #CommunitySupport #AccidentalSuccess