# Bits

Beau says:

- Memphis Police Department's Scorpion team and the killing of Mr. Nichols are under federal investigation.
- The first former officer involved in the case entered a plea agreement for 15 years in exchange for cooperation.
- Federal authorities seem to believe there is more to uncover in Memphis, hinting at potential future allegations.
- Those opting for trial rather than cooperation could face substantially higher sentences.
- Expect more individuals to cooperate as the investigation progresses, following the first officer's deal.
- The cooperation deal for 15 years sets a precedent for potential future deals and outcomes in the case.
- The situation in Memphis could mirror past cases where multiple allegations emerged.

# Quotes

- "The cooperation deal for 15 years. That's the recommended sentence."
- "The feds are under the impression that there is much more to discover in Memphis."
- "Once you have that first domino that falls, there will be more."
- "If the deal is for 15 years, they're going to understand the message that's being sent."
- "We'll continue to follow it until we get a resolution on all of the individuals involved."

# Oneliner

Memphis Police Department's Scorpion team faces federal investigation after the killing of Mr. Nichols, with the first officer entering a plea deal for 15 years, hinting at more revelations to come and higher sentences for those opting for trial.

# Audience

Legal observers, community members.

# On-the-ground actions from transcript

- Follow updates on the investigation and outcomes (implied).
- Stay informed about developments in the case (implied).

# Whats missing in summary

Details on how the community is reacting to these developments.

# Tags

#Memphis #FederalInvestigation #PoliceMisconduct #PleaDeal #Cooperation