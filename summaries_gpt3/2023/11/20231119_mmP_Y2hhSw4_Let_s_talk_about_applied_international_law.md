# Bits

Beau says:

- Explains the application of international norms and how it varies due to power and connections.
- Compares international law to the law in the United States regarding equality in application.
- Mentions how military power plays a significant role in the enforcement of international laws.
- Talks about the influence of connections, like knowing influential people or holding veto power.
- Expresses that power and connections often override international norms and laws.
- Suggests that citizens of a country hold the key to curtail violations of international norms.
- Gives examples where domestic discontent helped shape policy and curtail violations.
- Emphasizes that powerful countries may not view themselves as bound by international standards.
- Stresses that change in policy comes from the people inside a country, not international organizations.
- Concludes by stating that power and connections determine the application of international law.

# Quotes

- "Power and connections overrides the international norms, the international laws, and that's how it works."
- "It's not at the UN. the more powerful a country, the less it views itself as a member of the international community."
- "The only people who can make their voice be heard to the point where it shifts policy."
- "There's no immediate transformation of this because it's a process that takes time."
- "Power and connections."

# Oneliner

Beau explains how power and connections often override international norms and laws, urging citizens to hold their countries accountable for policy shifts.

# Audience

Global citizens

# On-the-ground actions from transcript

- Hold your country's leaders accountable for violating international norms by making your voice heard (implied).

# Whats missing in summary

The full transcript provides a detailed understanding of how power dynamics and connections influence the enforcement of international norms and laws, stressing the importance of citizen action in shaping policy shifts.

# Tags

#InternationalLaw #PowerDynamics #CitizenAction #Accountability #GlobalRelations