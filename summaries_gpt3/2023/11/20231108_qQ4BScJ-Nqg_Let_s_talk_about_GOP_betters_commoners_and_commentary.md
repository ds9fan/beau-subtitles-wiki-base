# Bits

Bob McKeown says:

- Commentary on Republican response to a disappointing performance, particularly focusing on former Senator Rick Santorum's statement.
- Santorum expressed relief that most states do not allow everything to be put up for a direct vote, implying that pure democracies are not ideal for running a country.
- The underlying belief seems to be that common people are not smart enough to make decisions for themselves and need representatives to decide for them.
- McKeown mentions how the term "our betters" and "commoners" are used sarcastically but actually reveal the true mindset of some individuals.
- There is a notion that citizens are not educated enough to make their own choices, leading to the preference for representative democracy over direct democracy.
- The idea of manipulation and control is brought up, suggesting that certain entities believe they can easily trick and influence the masses.
- The concept of giving the masses someone to be mad at in order to maintain control is discussed.
- The issue of wanting control over their own bodies in Ohio is mentioned, indicating a clash of beliefs between different groups.
- There is a warning to not overlook such revealing statements as they represent a deeper attitude that persists beyond the initial comment.
- The theme of authoritarianism and the need to create a common enemy to justify certain actions is emphasized.
- The distinction between wanting to represent versus wanting to rule is pointed out.
- The concept of citizens being viewed as commoners by certain entities is reiterated.
- The call to keep in mind the revealed mindset that discredits the ability of individuals to govern themselves.
- The idea that certain forces aim to create divisions and distractions to maintain power is discussed.
- The reminder to recall the quoted statement about not allowing people to govern themselves due to perceived lack of intelligence.

# Quotes

- "Thank goodness that most of the states in this country don't allow you to put everything on the ballot because pure democracies are not the way to run a country."
- "They need their betters to decide. They need to vote for somebody to represent their interests because they don't really know what their interests are."
- "They think you're easily manipulated. They think that you can be tricked, you can be fooled."
- "They have to give you somebody to look down at because otherwise you would never accept what's going on."
- "It's a good thing that most states don't allow people to govern themselves. Just not smart enough."

# Oneliner

Bob McKeown reveals the condescending attitude towards citizens in political discourse, exposing the belief that they are not capable of making informed decisions for themselves.

# Audience

Voters, activists, citizens

# On-the-ground actions from transcript

- Challenge condescending attitudes towards citizens and demand respect for individual decision-making abilities (implied)
- Stay informed about political rhetoric and call out manipulative tactics aimed at controlling public opinion (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the condescending attitude towards citizens in political discourse, urging viewers to remain vigilant against manipulation and control tactics.

# Tags

#RepublicanResponse #PoliticalAttitudes #Democracy #Manipulation #Representation