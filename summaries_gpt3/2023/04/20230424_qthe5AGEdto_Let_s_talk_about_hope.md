# Bits

Beau says:

- Shares a story about growing up in the South and encountering racism and homophobia.
- Recounts incidents from his childhood where interracial relationships and LGBTQ+ individuals faced discrimination.
- Describes encountering a gay classmate from high school at a bar as an adult.
- Talks about the progress made in acceptance and equality over his lifetime.
- Acknowledges the ongoing fight for equality and the challenges faced by marginalized communities.
- Expresses optimism about the world changing for the better in the long run.
- Emphasizes the link between hope and motivation, believing in the possibility of a better future.
- Encourages individuals to continue fighting for progress, knowing that future generations are relying on them.

# Quotes

- "Hope is dangerous. It's a cliche but it's true because if you have hope for a better world that means you can imagine it."
- "No matter what group you're a part of, there will be somebody like you after you. And they're counting on you."

# Oneliner

Beau shares personal experiences of discrimination, progress, and the interplay between hope and motivation in fighting for a better future.

# Audience

Activists, marginalized communities

# On-the-ground actions from transcript

- Support LGBTQ+ youth in high schools by creating safe spaces and standing up against bullying (implied)
- Advocate for racial equality and challenge discriminatory behavior in public spaces (implied)
- Educate others about the importance of acceptance and equality to create a more inclusive society (implied)

# Whats missing in summary

Beau's personal anecdotes and reflections provide a powerful reminder of the progress made in the fight for equality and the importance of maintaining hope amidst ongoing challenges.

# Tags

#Equality #Hope #Motivation #Activism #Progress