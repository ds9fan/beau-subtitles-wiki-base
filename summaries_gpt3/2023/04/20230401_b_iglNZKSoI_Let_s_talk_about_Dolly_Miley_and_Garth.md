# Bits

Beau says:

- School in Wisconsin stops students from performing "Rainbowland" duet by Dolly Parton and Miley Cyrus, citing potential controversy around acceptance and being a good person.
- Initially, the school also planned to stop the performance of "Rainbow Connection" by the Muppets for similar reasons but later reversed that decision.
- Beau questions the attempt to cancel performances by Dolly Parton and the Muppets, suggesting a political angle.
- The Academy of Country Music Awards will be hosted by Dolly Parton and Garth Brooks, known for their acceptance and support, especially in controversial areas like marriage equality.
- Garth Brooks, in the early '90s, expressed that traditional family values include encouraging children to be the best they can be, regardless of their parents' race or sexual orientation.
- Both Dolly Parton and Garth Brooks have been allies before it became mainstream, openly supporting causes like marriage equality.
- Beau anticipates a message being sent by selecting Dolly Parton and Garth Brooks as hosts for the Academy of Country Music Awards.
- Garth Brooks has a history of advocating for marriage equality, even when it was a controversial topic.
- The selection of Dolly Parton and Garth Brooks as hosts is seen as a significant move in the country music scene, indicating a message about acceptance and equality.
- Beau wonders if the choice of hosts for the awards show and the school's decision to halt performances are related or just a coincidence.

# Quotes

- "Traditional family values was encouraging children to be the best they can be. If your parents are black and white, if your parents are the same sex, that's still traditional family values to me." - Garth Brooks
- "Both of them were allies before most of us knew what an ally was, and they have been very open about their support." - Beau

# Oneliner

A school stops students from performing songs by Dolly Parton and Miley Cyrus, while the Academy of Country Music Awards feature hosts known for their acceptance and support, sending a message of inclusivity.

# Audience

Country music fans

# On-the-ground actions from transcript

- Support and amplify artists who advocate for acceptance and equality (implied).

# Whats missing in summary

The importance of recognizing and celebrating artists who use their platform to advocate for acceptance and equality.

# Tags

#DollyParton #MileyCyrus #GarthBrooks #Acceptance #Equality #CountryMusic