# Bits

Beau says:

- Asa Hutchinson, seeking the Republican Party's presidential nomination, faces questions about his background and beliefs.
- Hutchinson, a former governor of Arkansas, has extensive executive branch experience and is considered to the right of a normal conservative.
- Despite calling on Trump to drop out, Hutchinson may not stand a chance due to his history of prosecuting far-right groups.
- His past actions against far-right groups may alienate the energized base of the Republican Party.
- Candidates openly opposing Trumpism are starting to emerge, and as Trump's legal problems mount, more may take a stance against him.

# Quotes

- "He's to the right of normal, but still within the normal spectrum."
- "One of the problems that he is definitely going to run into is that while most of the groups that he went up against back then, they're gone."
- "This is one of those candidates that is coming forward and openly opposing Trumpism."

# Oneliner

Asa Hutchinson, seeking the Republican presidential nomination, faces challenges due to his past actions against far-right groups, potentially hindering his chances in a crowded field where candidates are openly opposing Trumpism.

# Audience

Political analysts, voters

# On-the-ground actions from transcript

- Support candidates openly opposing harmful ideologies (implied)

# Whats missing in summary

Insight into how the political landscape may shift as more candidates openly oppose Trumpism and as Trump's legal troubles increase.

# Tags

#AsaHutchinson #RepublicanParty #PresidentialNomination #Trumpism #FarRightGroups