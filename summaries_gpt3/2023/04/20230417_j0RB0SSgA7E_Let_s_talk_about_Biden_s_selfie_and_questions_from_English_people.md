# Bits

Beau says:

- Explains the outrage of American Republicans over a selfie of Biden with Jerry Adams in Ireland.
- American Republicans were told to be upset about the image without knowing the full context.
- Jerry Adams has a controversial past associated with violent activities in the Republican movement.
- Talks about the ambiguity surrounding Adams' past and his rise within the movement.
- Describes Adams as the public face and PR person for the Republican movement in the 80s and 90s.
- Mentions a shift towards peace by Adams after the Good Friday Agreement.
- Points out a significant moment when Adams condemned a violent act unequivocally, showing a visible change.
- Emphasizes Adams' evolution into a mainstream political figure advocating for peaceful means.
- Acknowledges that some may struggle to forgive Adams for his past actions despite his visible transformation.
- Urges American Republicans to understand the context and reconciliation efforts in the UK before criticizing the selfie incident.

# Quotes

- "He became more and more a mainstream political figure and wound up in office."
- "The idea here is that somebody who was sympathetic or was the public face of a violent anti-government movement should be ostracized."
- "If you are an American who is upset about this, please understand there are English people wondering why you're mad."

# Oneliner

American Republicans' outrage over Biden's selfie with Jerry Adams lacks context, ignoring Adams' visible transformation towards peace in Ireland.

# Audience

American Republicans

# On-the-ground actions from transcript

- Contact organizations promoting reconciliation efforts in Ireland (implied)
- Educate oneself on the history and progress of peace in Ireland (implied)

# Whats missing in summary

The full transcript provides a comprehensive background on Jerry Adams' transformation towards peace in Ireland, urging a nuanced understanding of complex political histories for American Republicans. 

# Tags

#Biden #JerryAdams #AmericanRepublicans #UK #Reconciliation