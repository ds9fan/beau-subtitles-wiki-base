# Bits

Beau says:

- Received a message about two instances where people went to the wrong house and ended up getting shot, sparking comparisons.
- Discovered that many people were making comparisons between the two situations.
- Two incidents: one involving a black kid who survived being shot, and another where a white woman was killed.
- Differences in the way the two cases were treated raised questions of bias.
- The black kid received more attention, support, and coverage compared to the white woman.
- Detailed the specific events of each incident and the aftermath.
- Pointed out disparities in media coverage, GoFundMe amounts, and reactions from authorities.
- Raised skepticism about the validity of some comparisons made between the two cases.
- Criticized the search for grievances and false equivalencies in trying to compare these vastly different situations.
- Encouraged reevaluating priorities when focusing on skin color in such tragedies.

# Quotes

- "The eternal question of why seems important."
- "If your first reaction to people accidentally going to the wrong house and getting shot is to look at their skin tone and try to figure out how you, by a similar pigment, were somehow damaged, your priorities are a little mixed up."
- "One of these things listed completely makes sense."
- "You're looking for something to view as oppression, it's not real."
- "It's not real."

# Oneliner

Beau questions bias and false equivalencies in comparing two instances of people being shot after going to the wrong house based on their race.

# Audience

Community members, social justice advocates.

# On-the-ground actions from transcript

- Reexamine priorities when faced with tragic events based on skin tone (implied).

# Whats missing in summary

Deeper insights into the impact of biases and false comparisons on justice and media coverage.

# Tags

#Bias #SocialJustice #RacialEquality #CommunityPolicing #Priorities