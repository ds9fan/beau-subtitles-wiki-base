# Bits

Beau says:

- Recounts a personal experience from middle school where he was scared by high schoolers in the woods, reflecting on a cave incident.
- Explains his familiarity with Tennessee, mentioning attending the same yearly field trip to the Hermitage as others.
- Criticizes the Tennessee state legislature for expelling three targeted individuals, sending clear messages with their actions.
- Points out the racial bias in the legislature's actions, sparing a white woman while booting two black men, implying skin color played a role.
- Asserts that the legislature's message isn't just about racism but also about asserting power and control over all Tennesseans.
- Analyzes the underlying message as one that diminishes the importance of individual voices and representation in favor of obedience to authority.
- Debunks the legislature's framing of their actions as defending the Second Amendment, revealing it as a tactic to distract and control the population.
- Calls out the manipulation around the topic of gun control, used to divert attention and generate fear to ensure compliance.
- Emphasizes the goal of the legislature: to enforce obedience and loyalty to the party above all else, disregarding democracy and individual rights.
- Encourages Tennesseans, especially young people, to resist this control by voting and sending a clear message of defiance against authoritarian tactics.

# Quotes

- "You better stay in your place, do what you're told, listen to your betters, and just accept your fate because you don't get a voice."
- "They're telling you, you will obey the party, period."
- "The Republican Party in Tennessee succeeded in creating tens of thousands of lifelong Democrats when they did this."
- "The Tennessee State Legislature showed its hand. They're not representatives, they're rulers."
- "If you put them back in office, it's going to send the message that it's unacceptable."

# Oneliner

Beau shines a light on Tennessee's legislature, exposing their messages of control and urging defiance through voting to reclaim power and representation.

# Audience

Tennessee residents

# On-the-ground actions from transcript

- Vote to re-elect the expelled individuals if they run again (suggested)
- Use your power at the ballot box to reinstate those who were removed from office (exemplified)
- Ensure that the voices of constituents are not silenced by voting against authoritarian tactics (implied)

# Whats missing in summary

Full context and emotional depth of Beau's impassioned plea for Tennesseans to reclaim their power through voting and resist authoritarian control.

# Tags

#Tennessee #StateLegislature #Authoritarianism #Voting #Representation #Power