# Bits

Beau says:

- Polling helps gauge the popularity of different entities and provides insights into political currents in the country.
- Most terms and entities in the United States do not have majority support, with the real metric being how many people view them positively versus negatively.
- Black Lives Matter (BLM) is viewed positively by 38% of Americans, while MAGA is viewed positively by 24%.
- BLM is two points underwater, while MAGA is 21 points underwater, meaning more people view MAGA negatively.
- Attacks against BLM and similar movements do not have the intended effect and can drive negative voter turnout.
- When candidates identify as MAGA, they are eliminating 45% of the population who view it negatively.
- The Republican Party may drive their own negative voter turnout by associating with MAGA.
- Campaigning against movements like BLM can lead to more people voting against the candidate.

# Quotes

- "Black Lives Matter is very, very much more popular than MAGA."
- "There are far more people who view MAGA in a negative light than view it in a positive light."
- "The Republican Party is going to drive their own negative voter turnout."

# Oneliner

Polling reveals that while Black Lives Matter is more popular than MAGA, attacks against movements like BLM may drive negative voter turnout.

# Audience

Political analysts, campaign strategists, voters

# On-the-ground actions from transcript

- Analyze polling data on different entities and movements to gauge public sentiment (implied)
- Be mindful of how attacks on certain movements can affect voter turnout (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of polling data and the potential impact of attacks on movements like BLM on voter behavior.

# Tags

#Polling #PoliticalCurrents #PublicSentiment #NegativeVoterTurnout #CampaignStrategy