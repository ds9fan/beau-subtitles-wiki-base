# Bits

Beau says:

- Texas is experiencing shifting narratives, promises, and new developments regarding a BLM demonstration incident in 2020.
- The shooter in the incident was convicted, and the Texas governor hinted at pardoning him.
- Details from the shooter's phone revealed racist content, posing questions about the governor's stance.
- The framing of the incident paints the victim as a leftist BLM protester killed by a right-wing army sergeant.
- Despite revelations of racist content, Beau suggests that being racist isn't a deal-breaker for many Republicans.
- The victim, Foster, was a right-wing veteran and libertarian, not a leftist as initially portrayed.
- Correcting the narrative to show Foster's true political beliefs might impact the governor's decision on the pardon.
- Foster believed in protecting the first amendment with the second and understood state violence against marginalized people.
- Beau believes it's vital to correct the narrative and acknowledge Foster's true political identity.
- The importance lies in revealing that Foster was a right-wing veteran, challenging the initial portrayal of him as a leftist BLM protester.

# Quotes

- "He was right-wing. He just wasn't a bigot."
- "The framing is that the shooter, an army sergeant, killed a leftist communist socialist Obama-loving BLM protester."
- "Foster wasn't a leftist. He was a right-wing veteran. That's who was killed."

# Oneliner

Texas faces shifting narratives and promises regarding a BLM demonstration incident, where revealing the victim's true political identity challenges the governor's pardon stance.

# Audience

Texans, Activists, BLM Supporters

# On-the-ground actions from transcript

- Correct the narrative about Foster's political beliefs and honor his true identity (implied).
- Advocate for justice for Foster and ensure his story is accurately portrayed (implied).

# Whats missing in summary

Full insight into the nuances of the shifting narratives and the potential impact on the governor's pardon decision.

# Tags

#Texas #BLM #ShiftingNarratives #Pardon #RightWingVeteran