# Bits

Beau says:

- Three separate events in Kansas City, New York, and New Mexico share a common element.
- In Kansas City, an 84-year-old white man shot a 16-year-old black kid who knocked on his door by mistake.
- The shooter in Kansas City has been charged with assault and armed criminal action, with questions raised about the charges.
- In New York, a homeowner shot at a group of friends who mistakenly pulled into their driveway, killing a 20-year-old woman.
- The shooter in New York has been charged with second-degree murder after an hour-long standoff with law enforcement.
- In Farmington, New Mexico, law enforcement went to the wrong house, resulting in the homeowner being shot and killed by officers.
- Law enforcement in Farmington did not force entry or use deceit to enter the property, leading to questions about potential charges.
- The incident in Farmington may be deemed a harmless error, allowing for a justified shoot based on the mistaken presence of law enforcement.
- Constant fear-mongering and keeping people on edge can lead to innocent lives being lost due to heightened tensions.
- Beau questions the impact of creating an environment where individuals constantly live in fear, likening it to a self-fulfilling prophecy.

# Quotes

- "Maybe it's not a good idea to constantly fear monger and keep people on edge."
- "You have a lot of innocent people being lost because people feel like they live in a combat zone."
- "It's becoming a self-fulfilling prophecy."

# Oneliner

Three separate incidents across the country reveal the dangers of fear-mongering and living in constant fear, leading to tragic outcomes.

# Audience

Communities, Activists, Bystanders

# On-the-ground actions from transcript

- Advocate for community policing and de-escalation training (implied)
- Support initiatives that address racial biases in law enforcement (implied)
- Join local organizations working towards police accountability (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the consequences of fear-mongering and the need for community-driven solutions to prevent tragic incidents.

# Tags

#Crime #RacialJustice #CommunityPolicing #FearMongering #Accountability