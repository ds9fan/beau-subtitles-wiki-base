# Bits

Beau says:

- Explains the connection between Taiwan, can openers, a perceived shortage, and parenting.
- Responds to a viewer's question about the implications of going to war over Taiwan.
- Shares a personal story about asking his dad, a gunnery sergeant, about the potential conflict.
- Mentions that China's invasion of Taiwan in war games usually fails.
- Describes the high cost projected for both Taiwan and the United States in the event of conflict.
- Points out the difficulty of China invading Taiwan due to its challenging nature.
- Emphasizes that the US strategy in such a scenario focuses on air and sea defense.
- Notes that the US doctrine involves being able to fight against two nearest competitors simultaneously.
- Concludes that a perceived shortage of a specific type of ammunition wouldn't change the overall strategy.

# Quotes

- "If your kids ask a question, even adult children, take the time to answer, if you can."
- "The reason China hasn't invaded and just taken Taiwan back is because it's really hard."
- "US doctrine is to be able to fight our two nearest competitors at the same time."

# Oneliner

Beau explains the implications of a conflict over Taiwan and the US strategy, focusing on air and sea defense.

# Audience

Viewers interested in understanding the potential implications of conflicts involving Taiwan and US military strategies.

# On-the-ground actions from transcript

- Educate yourself on international relations and military strategies (implied).

# Whats missing in summary

In-depth analysis and additional context on the geopolitical implications of potential conflicts involving Taiwan and the US military strategy. 

# Tags

#Taiwan #USMilitary #Conflict #Parenting #Geopolitics