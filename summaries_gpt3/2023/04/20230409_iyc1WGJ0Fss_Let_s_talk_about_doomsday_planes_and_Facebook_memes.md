# Bits

Beau says:

- Explains the concern about a meme on Facebook regarding the Navy building a new doomsday plane for nuclear war with China.
- Describes doomsday planes as airborne command and control centers paired with the US strategic arsenal.
- Mentions the Navy is indeed acquiring new doomsday planes, each costing around $250 million.
- Clarifies that the purchase of these planes is not a sign of imminent nuclear war with China.
- States that the current Navy planes in this role are old and need to be replaced with newer airframes.
- Compares the replacement of the airframe to upgrading an old food truck with a new truck to ensure mission success.
- Notes that the Navy plans to have three test planes first before acquiring nine more in the future.
- Emphasizes that this modernization is a normal process as equipment ages, not a preparation for immediate conflict.
- Addresses the accuracy of the Facebook meme regarding the cost and acquisition of the doomsday planes.
- Concludes by advising against getting news from Facebook and suggesting it's just a routine equipment upgrade.

# Quotes

- "Don't get your news from Facebook."
- "That part [about doomsday planes] would be scary."
- "It's just normal modernization as time goes along."

# Oneliner

Beau explains the truth behind a Facebook meme about the Navy acquiring doomsday planes, clarifying it's routine modernization rather than preparation for war with China.

# Audience

Internet users

# On-the-ground actions from transcript

- Verify information from reliable sources before believing or sharing content online (implied)
- Stay informed through credible news outlets rather than social media platforms (implied)

# Whats missing in summary

The detailed nuances and explanations provided by Beau in the full transcript can offer a deeper understanding of military modernization processes and dispel misinformation online.

# Tags

#Navy #DoomsdayPlanes #FacebookMemes #MilitaryModernization #Misinformation