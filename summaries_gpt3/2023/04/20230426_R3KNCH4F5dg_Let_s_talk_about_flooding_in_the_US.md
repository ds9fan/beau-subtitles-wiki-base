# Bits

Beau says:

- Addressing internet people about snow melt and flooding in California and along the Mississippi.
- Snowmelt from the Northern part heading down towards the rivers, causing flooding warnings.
- 20 gauges along the river already in major flood stage, with more expected.
- Urging people to have their flood plans ready and be cautious.
- La Crosse in Minnesota expecting a crest at 16.1 feet, with a possibility of further rise.
- Yosemite Park closing until at least May 3rd due to snowmelt, with warnings for hikers about the wet and muddy conditions.
- Extreme weather becoming the norm and increasing with climate change.
- Mitigation is necessary for the future as extreme weather events will worsen.
- Alerting to the need for action to prevent worsening situations in the future.
- Urging awareness and preparation for the increasing frequency and severity of extreme weather events.

# Quotes

- "The United States is going to have to come to terms with the fact that extreme weather is no longer extreme."
- "We're going to start seeing it more and it's gonna get worse if we don't do something."
- "It's gonna be wet, it's gonna be muddy, it's gonna be nasty."
- "We can mitigate in the future."
- "Just understand it's gonna be the norm."

# Oneliner

Beau addresses internet people about the increasing frequency and severity of extreme weather events, urging preparedness and mitigation for the future.

# Audience

Residents in flood-prone areas

# On-the-ground actions from transcript

- Prepare a flood plan and ensure readiness for potential flooding (implied)
- Stay cautious and aware of flood warnings in your area (implied)
- Stay informed about the weather conditions in regions prone to extreme weather events (implied)
- Support and advocate for climate change mitigation efforts in your community (implied)

# Whats missing in summary

Beau's engaging delivery and emphasis on the need for immediate action to address climate change impacts. 

# Tags

#ExtremeWeather #ClimateChange #Flooding #Preparedness #Mitigation