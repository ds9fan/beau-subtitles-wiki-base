# Bits

Beau says:

- News out of Texas: a Texas county board decided to remove a bunch of books from a library, leading to a lawsuit by patrons.
- Federal judge ruled that removal of books from libraries based on viewpoint content discrimination violates the First Amendment.
- Many jurisdictions across the country are using libraries to remove LGBTQ-friendly content, regardless of the viewpoint.
- People making these decisions often lack understanding of how the Constitution and First Amendment work.
- Judge ordered the books to be returned to the shelves within 24 hours, showing certainty in the outcome.
- Ongoing culture war involves unconstitutional actions by individuals.
- Supreme Court likely to uphold decisions against content discrimination to avoid favoring one side of the country.
- Overall, the issue revolves around the unconstitutional removal of books from libraries based on viewpoint discrimination.

# Quotes

- "The federal judge decided that the plaintiffs are likely to succeed on their viewpoint discrimination claim."
- "In most cases, people making these decisions do not understand how the Constitution works."
- "Supreme Court understands that if content discrimination is allowed, it is unlikely that the conservative side comes out ahead."
- "It's an ongoing thing."
- "Judge ordered the books to be returned to the shelves within 24 hours."

# Oneliner

A federal judge ruled against viewpoint discrimination in removing books from libraries, showcasing the ongoing unconstitutional culture war on LGBTQ-friendly content.

# Audience

Librarians, Book Lovers, Advocates

# On-the-ground actions from transcript

- Contact local libraries and offer support in defending against viewpoint discrimination (suggested).
- Join community advocacy groups working to protect LGBTQ-friendly content in libraries (suggested).

# Whats missing in summary

Importance of supporting libraries in upholding the First Amendment and protecting diverse content.

# Tags

#Libraries #FirstAmendment #LGBTQ #ViewpointDiscrimination #SupremeCourt