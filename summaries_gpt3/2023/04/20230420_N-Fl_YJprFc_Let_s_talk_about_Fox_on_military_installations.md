# Bits

Beau says:

- Explains the issue of having Fox News on military installations, particularly in common areas and offices.
- Mentions the First Amendment concerns in relation to military installations.
- Describes how on military installations, there are restrictions on what content can be shown, like cable news in food court areas.
- Talks about the push to remove Fox from bases by groups like Vote Vets due to concerns about content undermining chain of command, readiness, cohesion, and morale.
- Provides scenarios where commanders express controversial views that, if aired on Fox, could lead to their dismissal.
- Points out that comments made on Fox can be detrimental to military readiness and could lead to consequences if expressed by a commander.
- Raises the issue of taxpayers unknowingly funding content that undermines military readiness.
- Concludes by summarizing the importance of pushing to remove Fox News from military bases.

# Quotes

- "US taxpayers are paying to undermine readiness."
- "A lot of the comments that are made on Fox are so detrimental to those concepts of readiness."
- "The reason that there is a push to get Fox off the bases… is because a lot of the content on Fox… undermines the chain of command, undermines readiness, undermines unit cohesion and morale."

# Oneliner

Beau explains the push to remove Fox News from military bases due to concerns about its content undermining readiness and cohesion.

# Audience

Military personnel, activists

# On-the-ground actions from transcript

- Contact military installations to advocate for the removal of Fox News (suggested)
  
# Whats missing in summary

Details on the specific actions individuals can take to support the push to remove Fox News from military bases.

# Tags

#FoxNews #MilitaryBases #FirstAmendment #Readiness #VoteVets