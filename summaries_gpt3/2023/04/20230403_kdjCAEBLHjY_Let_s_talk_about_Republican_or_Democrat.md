# Bits

Billy Young says:

- Emphasizes the importance of tailoring arguments to reach people effectively, beyond using the strongest arguments.
- Talks about the narrow perception of party loyalty versus ideological consistency when identifying as Republican or Democrat.
- Advocates for supporting policy over party or personality to encourage moving beyond party lines.
- Suggests expanding the Overton window to encourage critical thinking and moving beyond talking points.
- Recommends adjusting messaging to appeal to the person you're speaking with, like promoting a cooperative society over competition.
- Shares an example of using anecdotal evidence effectively to challenge preconceived notions, especially for those who rely on personal observations.
- Stresses the need to customize arguments based on the individual to shift thoughts effectively.
- Encourages tailoring arguments to resonate with the listener for successful communication and thought-shifting.

# Quotes

- "Support policy over party or personality."
- "You have to tailor your argument to the person that you're talking to."
- "If you're trying to use those conversations to shift thought, you have to tailor your argument to the person that you're talking to."

# Oneliner

Billy Young stresses the importance of tailoring arguments to individuals for effective communication and thought-shifting.

# Audience

Communicators, Activists, Advocates

# On-the-ground actions from transcript

- Tailor your arguments to individuals to effectively communicate and shift thoughts (suggested).
- Use anecdotal evidence when appropriate to challenge preconceived notions (implied).

# Whats missing in summary

The full transcript provides in-depth insights on the significance of personalizing arguments to effectively communicate and shift perspectives.

# Tags

#Communication #TailoringArguments #Persuasion #ThoughtShifting #CriticalThinking