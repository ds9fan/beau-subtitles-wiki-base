# Bits

Beau says:

- Senator Ron Johnson of Wisconsin focused on the idea that climate change benefits his state because it is cold, disregarding the global impact.
- Republican Party's stance on climate change has evolved from denial to claiming it is happening but beneficial.
- The expert tried to explain the global impact of climate change to Senator Johnson, who seemed fixated on the benefits to his state.
- Converting excess deaths per 100,000 into real numbers reveals around 6.8 million extra deaths per year globally due to climate change.
- Climate change affects everyone, not just specific regions, and can lead to mass migrations, straining resources.
- Politicians' stance on climate change should be a key consideration for voters.
- Climate change is a pressing global issue that requires immediate action and cannot be ignored based on short-term benefits.
- Both Republican and Democratic Parties need to take significant steps to address climate change urgently.

# Quotes

- "Climate change isn't something that respects state lines or international borders."
- "Climate change is a global game. Everybody has to get involved."
- "Politicians' position on climate and actually doing something to mitigate should probably be a defining characteristic."

# Oneliner

Senator Ron Johnson fixates on Wisconsin's benefit from climate change, ignoring global impacts, as Republican Party's stance on climate evolves to deem it beneficial, disregarding the 6.8 million extra deaths yearly globally due to climate change.

# Audience

Voters

# On-the-ground actions from transcript

- Contact politicians to prioritize climate change mitigation efforts (implied).
- Get involved in local climate action groups or initiatives (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how politicians' stances on climate change can have significant implications globally and urges immediate action to mitigate its effects.

# Tags

#ClimateChange #RepublicanParty #GlobalImpact #Voters #ClimateAction