# Bits

Beau says:

- Addressing the importance of change, jokes, memes, and how change is a gradual process.
- Sharing a scenario where a family member sends bigoted jokes about the LGBTQ community.
- Explaining how to approach and help someone who is showing interest in learning and changing.
- Suggesting tactics like making the person explain the jokes they share to understand their impact.
- Emphasizing the recycled nature of offensive jokes as a signal of belonging to a specific group.
- Encouraging starting with educating family members about the hurtful nature of such jokes.
- Advising on taking baby steps in educating and understanding complex topics like gender identity.
- Stating the significance of preserving family values and using that as a starting point for change.
- Recommending exposing the person to family members from marginalized communities to reduce fear and prejudice.
- Stressing the gradual nature of the process and the importance of patience and persistence in fostering change.

# Quotes

- "Change takes time. Baby steps."
- "Exposure kills fear. Fear is the root of most of this."
- "Your family is your family and nothing comes between that type of stuff."
- "Give it time to digest before you give them something new."
- "Start by framing it around you want to keep your family."

# Oneliner

Beau provides tactics to help someone change their bigoted views by starting with family education, understanding offensive jokes, and gradually introducing new perspectives, stressing the importance of patience and persistence.

# Audience

All community members

# On-the-ground actions from transcript

- Start educating family members about the impact of offensive jokes (implied)
- Encourage the person to explain offensive jokes to understand their impact (implied)
- Slowly introduce new perspectives to the person, starting with small government conservatism (implied)

# Whats missing in summary

The full transcript provides detailed insights on initiating difficult but necessary dialogues to foster understanding and change in individuals holding prejudiced views.

# Tags

#Change #Education #Family #Understanding #Patience