# Bits

Beau says:

- Beau addresses the topic that everyone wanted him to talk about, a boycott involving Bud Light, from both conservatives and liberals.
- He stayed quiet because revealing his open secret was necessary to talk about it.
- Conservatives critiqued him for not discussing their successful boycott, while liberals wondered why he didn't make fun of them for not knowing the parent company behind the brand they boycotted.
- The people at the center of the issue reached out to Beau, questioning his silence on the matter.
- Beau explains that he refrains from interrupting his opposition when they're making a mistake, which is why he didn't initially address the situation.
- Bud Light faced backlash for a semi-inclusive marketing campaign that some perceived as going "woke," leading conservatives to initiate a boycott.
- Beau notes that the boycott by conservatives against Bud Light was somewhat successful, causing damage to the company but not as much as claimed.
- He mentions a website called OpenSecrets that reveals the political contributions made by companies like Anheuser-Busch, Bud Light's parent company.
- Beau elaborates on the political donations made by Anheuser-Busch in 2022, showing a significant portion going to Republican committees and candidates.
- The transcript ends with Beau talking about Trump's child calling for an end to the boycott of the conservative-leaning company.

# Quotes

- "I don't like to interrupt my opposition when they're making a mistake."
- "Y'all boycotted one. Y'all boycotted a big donor is what this boiled down to."
- "Maybe they decide to make amends and give an even bigger donation to the people who trashed them."

# Oneliner

Beau addresses the Bud Light boycott, revealing political donations and potential consequences.

# Audience

Consumers, activists, voters

# On-the-ground actions from transcript

- Contact your representatives to inquire about their campaign contributions and affiliations (suggested)
- Stay informed about the political affiliations of companies and their donations (exemplified)

# Whats missing in summary

Analysis of the potential impact of political donations on corporate decisions.

# Tags

#Boycott #PoliticalDonations #Conservatives #Liberals #OpenSecrets