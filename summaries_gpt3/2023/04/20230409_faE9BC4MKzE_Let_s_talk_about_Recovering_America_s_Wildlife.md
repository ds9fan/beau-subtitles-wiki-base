# Bits

Beau says:

- Introduces the Recovering America's Wildlife Act, which has been reintroduced with bipartisan support.
- About one-third of America's wildlife faces an elevated risk of extinction, impacting the environment and other species.
- State-level plans exist to assist in wildlife recovery, but federal funding of $65 million/year is insufficient.
- The Act aims to provide $1.4 billion annually to states and tribal governments to address wildlife conservation.
- The funding will help mitigate disease among species, reestablish migration routes, and control invasive species.
- Urges action now to prevent irreversible damage, drawing a parallel to the water crisis out west.
- Emphasizes the importance of acting before the situation worsens and becomes irreversible.
- Points out the successful conservation efforts in the US when adequate funding and resources are present.
- Stresses the need for collective will to address wildlife conservation effectively.
- Concludes with a call to seize the current moment and work towards a sustainable future.

# Quotes

- "If we don't take the opportunity, if we don't start to address this now, it will be regretted later."
- "The warning signs are there and we have the opportunity to do something about it before it is out of control."
- "For years and years and years, you had people talking about how bad the water situation was going to be out west, and nobody wanted to do anything about it."
- "The US has actually proven itself capable of doing it if the funding is there, if the resources are there, if the will is there."
- "Do we have the will to actually work to solve it before it becomes a headline-grabbing issue?"

# Oneliner

Beau presents the urgent need for the Recovering America's Wildlife Act, stressing the critical role of adequate funding in preventing irreversible wildlife loss and environmental impact.

# Audience

Conservationists, Wildlife Enthusiasts

# On-the-ground actions from transcript

- Advocate for the passing of the Recovering America's Wildlife Act by contacting your senators and representatives (suggested).
- Join local conservation efforts or wildlife preservation organizations to contribute to positive change in your community (implied).

# Whats missing in summary

The full transcript provides detailed insights into the importance of the Recovering America's Wildlife Act and the critical need for immediate action to prevent irreversible wildlife loss and environmental damage.

# Tags

#WildlifeConservation #RecoveringAmericasWildlifeAct #Funding #EnvironmentalImpact #UrgentAction