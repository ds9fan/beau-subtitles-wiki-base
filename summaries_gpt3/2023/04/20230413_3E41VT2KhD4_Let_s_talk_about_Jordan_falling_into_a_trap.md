# Bits
Beau says:

- Beau mentions Jordan and an upcoming hearing in Congress focusing on rates in New York.
- He talks about the perception created by connecting high crime rates with Democrat-run cities.
- Beau explains how setting a minimum population requirement skews lists towards Democrat-run cities.
- He points out that rural areas or medium-sized cities often have higher crime rates than large cities like Chicago or New York.
- Beau hints that Republicans in the hearing may be misled by right-wing outlets' misinformation.
- He advises understanding the methodology behind such lists to counteract misinformation effectively.
- Beau suggests watching a video to comprehend how rural areas often have higher crime rates.
- He mentions that the Republicans may not be aware of this discrepancy and could be in for a surprise during the hearing.

# Quotes
- "It feels like there's going to be some funny moments coming up..."
- "They believe their own propaganda."
- "It's going to be funny."

# Oneliner
Beau dives into the misleading perception of crime rates in Democrat-run cities and rural areas, hinting at potential surprises in an upcoming hearing.

# Audience
Viewers, Congressional staff

# On-the-ground actions from transcript
- Watch the video mentioned to understand how crime rates are misrepresented (suggested).
- Educate others on the methodology behind crime rate lists to combat misinformation (implied).

# What's missing in summary
Beau provides a detailed analysis of how crime rates are perceived and misrepresented, challenging viewers to think critically about data manipulation and misinformation.

# Tags
#CrimeRates #Perception #Misinformation #CongressionalHearing #RuralAreas