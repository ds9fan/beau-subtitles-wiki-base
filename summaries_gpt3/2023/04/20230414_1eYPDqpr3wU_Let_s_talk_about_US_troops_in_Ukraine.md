# Bits

Beau says:

- Reveals the presence of US personnel in Ukraine has recently surfaced, surprising many.
- Describes a hypothetical scenario where concerns about US equipment being misused in Ukraine led to the deployment of personnel attached to the US embassy.
- Mentions a sarcastic video made in November 2022 discussing the presence of these personnel as a response to prior outrage.
- Notes that the personnel are focused on ensuring the proper use of equipment and are not involved in combat.
- Suggests that while officially their role is accountability, they may also be providing advice.
- Speculates about the presence of former US military members working for contracting companies in Ukraine, despite lack of evidence.
- Emphasizes the role of manufactured outrage in shaping narratives and policies.
- Points out a US Congress member justifying the leak about the troops, potentially involved in creating the situation.
- Indicates that the deployment of personnel was likely already happening but became public due to manufactured outrage.
- Encourages critical thinking when evaluating reporting, suggesting that if a topic falls within someone's expertise, they may have covered it before.

# Quotes

- "I want you to picture a world where, let's say five or six months ago, Russia and people who are of like minds in the U.S. Congress."
- "When you create an outrage of the day, like they did, that's the only possible response."
- "The people who are super mad about this, in fact, there's one person who sits in the US Congress, who's kind of justifying the leak."
- "It's just a thought, y'all have a good day."

# Oneliner

Beau reveals the presence of US personnel in Ukraine amidst manufactured outrage, shedding light on accountability and potential covert operations.

# Audience

Journalists, policymakers, concerned citizens

# On-the-ground actions from transcript

- Contact journalists or news outlets to demand further investigation into the presence of US personnel in Ukraine (suggested).
- Reach out to policymakers to question the narrative and accountability surrounding the deployment of personnel (suggested).
- Engage in critical thinking and fact-checking to understand the nuances behind manufactured outrage and its impact on policies (exemplified).

# Whats missing in summary

Detailed analysis of the geopolitical implications and potential consequences of the revealed US personnel presence in Ukraine.

# Tags

#USpersonnel #Ukraine #ManufacturedOutrage #Geopolitics #Accountability