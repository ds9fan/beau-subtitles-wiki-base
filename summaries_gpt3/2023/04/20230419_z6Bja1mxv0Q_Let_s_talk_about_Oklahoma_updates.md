# Bits

Beau says:

- Updating on the events in McCurtain County, Oklahoma since a recording surfaced involving county officials having disturbing and inappropriate conversations.
- Topics in the recording included off-color commentary, dealing with a burn victim, talk about removing a journalist permanently, and expressing desires to beat and hang black people.
- Governor has called for resignations, with local people staging demonstrations for the same.
- Surprisingly, the Sheriff's Association in Oklahoma voted to expel the members involved in the recording, showing a departure from the usual closing of ranks to protect law enforcement officials.
- People on the recording have not publicly addressed its content, claiming it was obtained illegally.
- Media will likely continue coverage due to the perceived threat against a journalist.
- Expectations of resignations in the future due to continued coverage, local outcry, lack of law enforcement solidarity, and the governor's stance.
- Real investigation initiated at the state level.
- Beau will monitor and provide updates on any new developments.

# Quotes

- "The governor has called for their resignations but beyond that there have been local people staging demonstrations calling for their resignations."
- "I think they did that unanimously, I'm not sure."
- "There will continue to be coverage."
- "I will continue to monitor this and keep y'all updated on any new developments."
- "Y'all have a good day."

# Oneliner

Beau provides an update on the disturbing recording involving Oklahoma county officials, with calls for resignations, a surprising lack of law enforcement support, and ongoing media coverage indicating potential future resignations.

# Audience

Local Residents, Activists

# On-the-ground actions from transcript

- Join local demonstrations calling for resignations (exemplified)
- Stay informed through media coverage and continue to demand accountability (implied)

# Whats missing in summary

Full context and depth of the events and reactions in McCurtain County, Oklahoma.