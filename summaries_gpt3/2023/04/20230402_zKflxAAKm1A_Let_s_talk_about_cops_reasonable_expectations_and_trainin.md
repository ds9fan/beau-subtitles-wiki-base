# Bits

Beau Young says:

- Explains why cops couldn't perform at a top tier level in a specific situation in Nashville.
- Mentions the unrealistic comparison made between cops and top tier U.S. military.
- Emphasizes the extensive training required to reach a certain skill level.
- Points out that constant training and mindset changes might affect cops' roles.
- Argues that pushing cops to meet unrealistic standards may hinder their primary duties.
- Suggests that departments should focus on getting cops good enough to complete missions.
- States that aiming for perfection in every cop may not be practical or beneficial.
- Raises concerns about the impact of intense training on cops' roles as patrol officers.
- Acknowledges the importance of training but questions the need for perfection in all situations.
- Concludes by suggesting that the unrealistic expectations placed on cops may be unreasonable.

# Quotes

- "Unless you've gone through a door yourself, shut the hole under your nose."
- "The goal of departments should be to get them good enough to do it and complete the mission."
- "It's not that people are opposed to them training. It's that getting them to the level that those critiques were based on is going to prohibit them from being first on the scene."
- "The level of training it takes to get to the level that people apparently wanted them to be at, it prohibits them from also being patrol officers."
- "I think that's kind of unreasonable."

# Oneliner

Beau Young explains why cops couldn't meet unrealistic top-tier expectations and why it's impractical for them to strive for perfection in every situation.

# Audience

Police reform advocates

# On-the-ground actions from transcript

- Contact local police departments for community policing initiatives (implied)
- Attend community meetings to address concerns about police training and expectations (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the challenges and limitations faced by police officers in meeting unrealistic standards set by some critics, ultimately questioning the practicality and necessity of expecting perfection in every situation.

# Tags

#PoliceReform #TrainingStandards #CommunityPolicing #RealisticExpectations #LawEnforcement