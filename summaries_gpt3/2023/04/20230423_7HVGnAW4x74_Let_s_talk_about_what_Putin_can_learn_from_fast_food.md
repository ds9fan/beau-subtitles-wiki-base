# Bits

Beau says:

- Putin's reliance on reports is resulting in bad decisions as he views external information as propaganda.
- The focus on metrics and measurements in authoritarian countries may not be the root cause of the issue.
- Beau draws a parallel from his experience at a fast food joint where an emphasis on car wait times led to counterproductive actions.
- The pressure on a specific metric caused delays in serving customers efficiently.
- The obsession with numbers led to impractical solutions like creating parking spaces for waiting customers.
- Companies often prioritize reports and numbers over the actual goals they are meant to achieve.
- Russia's military also fell into the trap of prioritizing arbitrary numbers in reports, giving a false impression of efficiency.
- Beau argues that this issue is not exclusive to authoritarian states but is a result of poor leadership and a focus on irrelevant metrics.
- He warns about the dangers of institutionalizing this mindset as it can lead to flawed policy decisions.
- Beau advocates for understanding the purpose behind reports and metrics to avoid losing sight of the actual goals.

# Quotes

- "Focus on a good example here. Focus on street price."
- "When you're looking at reports, always remember why that report was generated and what the goal was."
- "It's a symptom of bad leadership."
- "They became so focused on meeting arbitrary numbers that they just started putting it in the report."
- "The obsession with numbers led to impractical solutions."

# Oneliner

Putin's reliance on reports leads to bad decisions, reflecting a broader issue of prioritizing metrics over goals, seen in fast food and military contexts.

# Audience

Leaders, decision-makers

# On-the-ground actions from transcript

- Question the purpose behind metrics and reports (implied)

# Whats missing in summary

The importance of focusing on goals rather than getting lost in metrics and reports.

# Tags

#Leadership #Metrics #DecisionMaking #Putin #FastFood #Military