# Bits

Beau says:

- Senator Tuberville is holding up 184 promotions for high-ranking people in the military until the Department of Defense stops providing leave for family planning services in red states.
- This action is causing a significant impact on readiness within the military.
- Even Republicans, like Susan Collins, are uncomfortable with Tuberville's actions, as they are holding up promotions for apolitical professionals.
- McConnell may step in soon as there is pressure building up against Tuberville's actions.
- Changing the policy as Tuberville desires could result in a loss of recruits for the military, affecting readiness even further.
- Holding up promotions not only delays pay raises and promotions but also impacts individuals' ability to go to their next postings, further affecting readiness.
- One of the affected individuals is the incoming commander of cyber command, which could have serious implications.
- Tuberville's actions showcase a desire to rule rather than represent the will of the American people.
- Despite public or private disagreements within the Republican Party, there is no significant effort to stop Tuberville's actions.
- The Republican Party is facing a dilemma post-Roe v. Wade, struggling to navigate the extremist base they have created.
- The issue of military readiness and family planning policies is likely to be a significant factor in the 2024 election.
- The Republican Party may struggle to find a tenable position that appeals to both extremists and the majority of Americans who support family planning services.
- The legitimacy of the Supreme Court is coming into question, adding another layer of uncertainty for the Republican Party's stance on family planning issues.
- The intentional weakening of the U.S. military's readiness by the Republican Party contradicts their narrative of a strong military.

# Quotes

- "It's not a desire to represent. It's not a desire to enact the will of the American people. It is a desire to rule them."
- "The Republican Party is the dog who caught the car."
- "The Republican Party will do everything they can to try to downplay it, but they can't."

# Oneliner

Senator Tuberville's actions delay military promotions for family planning policy changes, impacting readiness and revealing Republican Party struggles post-Roe v. Wade.

# Audience

Voters, Activists, Military Personnel

# On-the-ground actions from transcript

- Contact local representatives to express support for military readiness and access to family planning services (suggested)
- Join advocacy groups working to protect military professionals and their rights (exemplified)
- Organize community events or campaigns to raise awareness about the impact of political interference on military operations (implied)

# Whats missing in summary

Insights on the potential long-term consequences of undermining military readiness and the importance of upholding professionals' rights in the armed forces.

# Tags

#Military #Readiness #RepublicanParty #FamilyPlanning #2024Election