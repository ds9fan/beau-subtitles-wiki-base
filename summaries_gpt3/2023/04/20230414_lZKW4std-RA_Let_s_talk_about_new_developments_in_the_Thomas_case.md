# Bits

Beau says:

- Supreme Court Justice Thomas and Republican mega-donor Harlan Crow are involved in a situation with new developments related to real estate transactions.
- Crow bought properties owned by Thomas in Savannah in 2014 without disclosure from Thomas.
- Crow claims he bought the properties to preserve them to illustrate the life of Thomas as a Supreme Court Justice.
- Improvements were made to Thomas's mother's house, where he grew up, after Crow purchased it, and Thomas's mother still lives there post-purchase.
- Reports suggest connections between Thomas and Crow are becoming more concerning and less justifiable politically.
- Impeachment is being considered by some representatives regarding Thomas.
- ProPublica's investigation revealed this information, praising them for their thorough journalism.
- The situation involves a long-term undisclosed relationship that raises eyebrows, especially if Thomas's mother still lives in a property bought by Crow in 2014.
- The Chief Justice may be concerned, particularly if this information was not disclosed to him initially.

# Quotes

- "The plot has thickened through real estate plots."
- "Thomas's mother still lives there, even though Crowe has purchased property in 2014."
- "The vacations, honestly, they would have been waved away."

# Oneliner

Supreme Court Justice Thomas faces scrutiny over undisclosed real estate transactions with Republican mega-donor Crow, raising concerns of a long-term relationship. Impeachment is considered.

# Audience

Legal scholars, activists

# On-the-ground actions from transcript

- Contact representatives to express support for reform or impeachment (implied)
- Follow and support ProPublica for thorough investigative journalism (exemplified)

# Whats missing in summary

Detailed explanation of the specific real estate transactions and their implications.

# Tags

#SupremeCourt #Thomas #RealEstate #Impeachment #ProPublica