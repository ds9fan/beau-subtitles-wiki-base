# Bits

Beau says:

- Kellyanne Conway acknowledges the Republican Party's struggle to attract younger voters and singles waiting to get married to become conservative.
- Younger voters are turning out in increasing numbers and leaning towards the Democratic Party due to GOP's stance on family planning, climate change, and guns.
- Republican strategists are out of touch as they believe they have won policy arguments on the economy and education.
- The GOP angered many by denying relief for educational loans, making the American dream unattainable for young Americans.
- Conway's statement about waiting for people to get older or married shows a misconception that people become more conservative with age.
- Values and opinions are formed early in life and evolve with society; people don't suddenly become conservative as they age.
- The Republican Party needs to alter its policy platform as younger voters will not become more conservative over time.
- Conservatives are slow to adapt to change and are being left behind by younger generations moving towards progressive ideals.
- Republicans need to become more progressive to appeal to younger voters who will continue trending towards the Democratic Party.
- The belief that people become more conservative with age goes against the principles of representative democracy and the republic.

# Quotes

- "They didn't become more conservative. Society moved forward and therefore they're more conservative in comparison to the more progressive standards of the day."
- "The Republican Party has to become more progressive. There's no way around that."
- "It is against the ideas of representative democracy. It is against the ideas of the republic."

# Oneliner

Kellyanne Conway acknowledges the Republican Party's struggle to attract younger voters as societal progress moves forward, showing the need for the GOP to become more progressive.

# Audience

Young voters

# On-the-ground actions from transcript

- Advocate for progressive policies within the Republican Party (exemplified)
- Engage younger voters in political discourse and encourage participation (exemplified)
- Support candidates who represent progressive values (exemplified)

# Whats missing in summary

The full transcript provides additional context on the evolving political landscape and the necessity for political parties to adapt to changing societal values and beliefs. Watching the full transcript can offer a deeper understanding of the Republican Party's challenges with attracting younger voters.

# Tags

#RepublicanParty #YoungVoters #ProgressivePolicies #SocietalChange #Adaptation