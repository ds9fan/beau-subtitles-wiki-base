# Bits

Beau says:

- Chief Justice Roberts declined the Senate Judiciary Committee's request to talk, prompting questions about why the Committee couldn't subpoena him.
- The Senate Judiciary Committee is facing obstacles in addressing concerns about ethics at the Supreme Court due to the "Feinstein situation."
- Senator Feinstein's absence and the Republican Party's block on a temporary replacement prevent the Committee from issuing a subpoena.
- The only way to get someone new on the Committee is for Feinstein to resign, and pressure for her resignation may increase.
- Without Feinstein's replacement or return, the Committee lacks the votes needed to issue a subpoena.
- Congressional subpoenas can be difficult to enforce and may be especially challenging for a Supreme Court Justice.
- Feinstein's absence hinders the Democratic Party's ability to push judges through without the House.
- With the combination of slow judge approvals and lack of subpoena power, there may be mounting pressure for Feinstein to resign.
- The Senate Judiciary Committee is currently unable to take significant action due to the lack of votes and Republican opposition.
- Beau expresses disbelief at the lack of interest from some Republicans on the Committee in addressing ethics concerns at the Supreme Court.

# Quotes

- "They don't have the votes to get a subpoena."
- "There may be renewed pressure on Feinstein to actually resign."
- "It kind of stops the Democratic Party from doing one of the few things they can actually do without the House."
- "I kind of do expect there to be a little bit more pressure for her to just resign."
- "That to me just kind of boggles the mind that those Republicans on the Senate Judiciary Committee are comfortable, which is being like, yeah, we don't care."

# Oneliner

Chief Justice Roberts declined to talk, Senate Judiciary Committee faces hurdles due to Feinstein's absence; pressure for resignation may increase, hindering Democratic Party actions.

# Audience

Political Activists

# On-the-ground actions from transcript

- Contact Governor Newsom to urge him to appoint an interim replacement for Senator Feinstein (suggested).
- Reach out to Senator Feinstein's office to express support for her resignation (implied).

# Whats missing in summary

The full transcript provides a detailed explanation of the obstacles faced by the Senate Judiciary Committee in addressing ethics concerns at the Supreme Court and the potential impact of Senator Feinstein's resignation on their ability to take action effectively.

# Tags

#SupremeCourt #SenateJudiciaryCommittee #EthicsConcerns #SenatorFeinstein #ResignationPressure