# Bits

Beau says:

- Beau introduces the topic of two friends, Supreme Court Justice Clarence Thomas and Republican mega donor Harlan Crowe.
- Crowe allegedly took Thomas on extravagant vacations worth tens or even hundreds of thousands of dollars, including cruising on a mega yacht and trips to Indonesia.
- The focus is on the appearance of a Supreme Court justice vacationing with a GOP mega donor, which raises concerns about gift disclosures.
- Thomas failed to disclose these gifts properly, which may lead to civil or criminal penalties.
- Despite Crowe being generous with vacations for many people, this does not excuse the lack of disclosure by Thomas.
- The vacations might not be the most significant issue; the use of a jet without Crowe could pose a bigger problem and may trigger an investigation.
- The jet use, potentially costing 20 to 70 grand per trip, could be harder to dismiss compared to the vacations.
- Calls for Thomas to resign have emerged, and there may be an ethics investigation into his actions and the accuracy of his disclosure forms.
- The situation involving Thomas and Crowe appears to be escalating into a scandal that won't easily fade away.
- Beau acknowledges the unprecedented nature of the situation and suggests following the reporting from ProPublica for more details.

# Quotes

- "Vacations that if you were to assign a dollar amount to them would be tens of thousands or hundreds of thousands of dollars."
- "The use of the Jet that's going to be an issue if there is an investigation."
- "This is definitely not a story that's going to go away."

# Oneliner

Beau talks about Supreme Court Justice Clarence Thomas and Republican mega donor Harlan Crowe's extravagant vacations, gift disclosures, and the potential for an investigation and calls for resignation.

# Audience

Journalists, Activists, Citizens

# On-the-ground actions from transcript

- Read and share the reporting from ProPublica to stay informed (suggested)
- Follow any updates on the situation to understand its implications (suggested)

# Whats missing in summary

The full transcript provides detailed insights into the friendship between Clarence Thomas and Harlan Crowe, the controversies surrounding undisclosed gifts, and the potential consequences for Thomas. Viewing the entire transcript helps in grasping the nuances and developments in this unfolding story.

# Tags

#SupremeCourt #ClarenceThomas #HarlanCrowe #GiftDisclosure #Investigation #ProPublica