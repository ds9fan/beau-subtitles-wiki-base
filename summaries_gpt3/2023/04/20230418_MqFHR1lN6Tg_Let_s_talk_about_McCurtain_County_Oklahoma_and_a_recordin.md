# Bits

Beau says:

- Introduction to discussing an incident in McCurtain County, Oklahoma.
- Mention of a recording that surfaced involving county officials with inappropriate dark humor.
- Focus on the main concern: county officials expressing upset over not being allowed to beat or hang black people.
- Mention of the official reactions, including calls for resignations of specific county officials.
- Involvement of state police force, Oklahoma State Bureau of Investigation, and the attorney general's office in looking into the matter.
- The FBI now has a copy of the recording, prompting questions about their interest in it.
- Speculation on the Civil Rights Division of the FBI potentially investigating the County Sheriff's Department due to the disturbing content of the recording.
- Clarification that the FBI possessing the recording doesn't confirm an active investigation.
- Mention of the possibility of civil rights violations against black Americans in that location.
- Uncertainty about whether the Civil Rights Division will launch an investigation based on the recording's content.

# Quotes

- "Hearing top law enforcement officials in a county say that they're upset that they can't
  engage in extrajudicial killing, that might be something that the Civil Rights Division
  views as an indicator."
- "It is appalling. It is appalling. It is not something you want from public officials."

# Oneliner

Beau delves into a recording from McCurtain County, Oklahoma, revealing disturbing sentiments from county officials and potential civil rights violations being investigated by the FBI.

# Audience

Community members

# On-the-ground actions from transcript

- Contact local advocacy organizations to support potential civil rights investigations by federal agencies (implied).
- Stay updated on developments and spread awareness within your community about the importance of accountability and transparency in law enforcement (exemplified).

# Whats missing in summary

The full transcript provides a detailed account of the concerning recording involving county officials in McCurtain County, Oklahoma, shedding light on potential civil rights violations and federal investigations. 

# Tags

#McCurtainCounty #Oklahoma #CountyOfficials #FBI #CivilRights #Investigation