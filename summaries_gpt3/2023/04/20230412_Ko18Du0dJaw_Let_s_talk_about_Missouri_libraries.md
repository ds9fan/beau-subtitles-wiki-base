# Bits

Beau says:

- Missouri Republicans have decided to defund all public libraries in the state, reflecting an authoritarian desire to control and suppress education and knowledge.
- This decision is about ensuring the people of Missouri follow orders, stay ignorant, and are easily manipulated by those in power.
- The move to defund public libraries goes against the Missouri state constitution, which clearly states support for the establishment and development of free public libraries.
- The Missouri House will need a constitutional amendment to proceed with defunding the libraries, as the constitution mandates support for public libraries.
- Some members of the Missouri Senate oppose this move and have indicated that the funding will likely be reinstated, showing a divide between the House and Senate.
- The attempt to defund libraries reveals a deeper agenda of keeping constituents ignorant and easily controlled, even if it means going against the state constitution.

# Quotes

- "Education is power. Knowledge is power. Denying people education and denying people knowledge is denying them power."
- "That's what this is about. It's about controlling people."
- "In order to do what they want to do here, they have to get a constitutional amendment to the state constitution."
- "Not just do they want you in your place, not just do they want you and your kids as ignorant as possible so they can be manipulated, they will do it in direct contradiction to the Constitution."
- "Y'all have a good day."

# Oneliner

Missouri Republicans aim to defund public libraries to control education and knowledge, going against the state constitution, revealing a deeper desire for manipulation and ignorance.

# Audience

Missouri residents

# On-the-ground actions from transcript

- Contact your Missouri state legislators to express support for public libraries and urge them to uphold the state constitution (suggested)
- Join local library advocacy groups to help protect public libraries in Missouri (implied)

# Whats missing in summary

The full transcript provides more context on the political dynamics in Missouri and the potential implications of defunding public libraries, offering a comprehensive understanding of the situation.

# Tags

#Missouri #PublicLibraries #Education #Knowledge #StateConstitution