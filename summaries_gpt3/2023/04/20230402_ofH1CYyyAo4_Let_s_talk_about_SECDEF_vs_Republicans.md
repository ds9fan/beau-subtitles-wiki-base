# Bits

Beau says:

- The Secretary of Defense appealed to Republicans in Congress to stop blocking promotions for higher ranking members of the US military, impacting around 160 officers.
- Republicans are blocking promotions because they are upset that the Department of Defense is supporting women in the military with leave time for family planning.
- Women make up about 20% of the military force, and the DOD wants to ensure their readiness.
- The holdup in promotions is causing ripple effects and downstream issues within the military.
- Troops moving around installations and promotions being associated with different commands are being hindered by the promotion block.
- The promotion block affects retirements, readiness, and the ability of the US military to respond to challenges.
- Republicans are being critiqued for focusing on controlling women rather than considering the broader implications of their actions.
- The Secretary of Defense emphasized how the promotion block is impeding readiness as the US faces complex geopolitical situations.
- The impact on retirement for high-ranking officers will worsen if the promotion block continues.
- The Republican Party's actions are seen as grandstanding and prioritizing political points over military readiness and personnel well-being.

# Quotes

- "It is about, once again, the Republican Party wanting to control women."
- "Republicans attempting to grandstand and score political points while jeopardizing U.S. readiness."
- "The legal mechanism that they used to do this is holding up."

# Oneliner

The Secretary of Defense appeals to Republicans to stop blocking military promotions, citing impacts on readiness and retirements, revealing Republican focus on controlling women rather than prioritizing national security.

# Audience

Congressional constituents

# On-the-ground actions from transcript

- Contact your Congressional representatives to express support for ending the promotion block (suggested)
- Join advocacy groups supporting gender equality in the military (implied)
- Organize community dialogues on the importance of military readiness and gender equality in the armed forces (implied)

# Whats missing in summary

The full transcript provides additional context on the impact of the promotion block on military operations and personnel well-being, along with a deeper dive into the political dynamics at play.

# Tags

#Military #Promotions #GenderEquality #Republicans #Readiness