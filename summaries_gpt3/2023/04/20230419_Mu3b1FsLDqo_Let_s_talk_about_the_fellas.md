# Bits

Beau says:

- Explains the concept of "the fellas" on Twitter, a group that counters Russian propaganda and supports Ukraine.
- Responds to a message claiming that the fellas are not real people but a botnet controlled by US intelligence.
- Clarifies that the fellas are real human beings, not a botnet, and that while Western intelligence may amplify their message, the core group is genuine.
- Disputes rumors linking the fellas to CENTCOM or CIA control, providing insights into the geographic nature of US commands and debunking the Virginia headquarters claim.
- Shares the humorous origin of the CIA rumor, where some changed their location on Twitter to Langley as a joke.
- Encourages critical examination of rumors, especially regarding jargon like CENTCOM, which is often mistakenly associated with various global events.
- Asserts that the majority of information shared by the fellas is organic and accurate, supporting Ukraine without significant disinformation.

# Quotes

- "They are real human beings."
- "That's just not true."
- "It is definitely more akin to the USO than some super secret operation."

# Oneliner

Beau explains the authenticity of "the fellas" countering Russian propaganda on Twitter and debunks claims of them being a botnet controlled by US intelligence, affirming their real human existence and organic information dissemination.

# Audience

Social media users

# On-the-ground actions from transcript

- Support the efforts of groups countering disinformation by sharing accurate information and debunking false claims (implied).
- Engage in critical thinking when evaluating rumors and online information (implied).

# What's missing in summary

Beau provides valuable insights into how to discern rumors and misinformation online and showcases the authentic efforts of individuals countering propaganda with accurate information.

# Tags

#Twitter #RussianPropaganda #Ukraine #Disinformation #USIntelligence