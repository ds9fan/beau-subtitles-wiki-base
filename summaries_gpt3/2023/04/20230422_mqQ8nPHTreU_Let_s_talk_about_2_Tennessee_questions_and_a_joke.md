# Bits

Beau says:

- Overview of recent events in Tennessee, including the resignation of Scotty Campbell, vice chair of the House Republican Caucus, amid allegations of inappropriate behavior or harassment.
- Speculation on the possibility of more resignations in light of increased media coverage and scrutiny of ethics within the legislative body.
- Responds to a question about comedian Drew Morgan, mentioning his use of the word "fascist" to describe Tennessee's government in a recent act.
- Beau shares his perspective on Drew's comedic approach, noting that different tactics work for different audiences, and he appreciates Drew's ability to energize progressive individuals through comedy.
- Beau contrasts his own cautious use of strong language on YouTube with Drew's more liberal use, based on their different approaches and target audiences.
- Emphasizes the importance of recognizing diverse tactics in reaching varied audiences, acknowledging that what works for one person may not work for another.
- Beau concludes by posing the question of whether Drew's approach is wrong, leaving room for individual interpretation and reflection.

# Quotes

- "I don't think the media is going to let this go."
- "People aren't the same. They won't be reached by using the same tactics."
- "It's a diversity of tactics thing because everybody's different."
- "The real question you have to ask is, is he wrong?"
- "Anyway, it's just a thought, y'all have a good day."

# Oneliner

Beau delves into recent Tennessee events, potential resignations, and comedian Drew Morgan's approach, advocating for diverse tactics in reaching varied audiences.

# Audience

Progressive individuals

# On-the-ground actions from transcript

- Watch Drew Morgan's comedy clips about Tennessee to understand his approach and messaging (suggested).
- Embrace diverse tactics in communication and advocacy to reach different audiences (implied).

# Whats missing in summary

Further insights on the importance of tailoring communication strategies to diverse audiences based on their preferences and receptivity.

# Tags

#Tennessee #Resignations #DrewMorgan #Comedy #Progressive