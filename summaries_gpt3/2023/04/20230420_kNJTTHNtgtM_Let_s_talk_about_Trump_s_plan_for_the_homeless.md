# Bits

Beau says:

- Trump's plan to address homelessness includes making it illegal to be homeless, banning urban camping, and establishing tent cities.
- Trump lacks an understanding of the limits of federal power over local city ordinances.
- Homeless individuals under Trump's plan are given the option to go to jail or a tent city for psychiatric and substance abuse help.
- Beau questions the effectiveness of arresting people for being poor and the logistics of getting to work from a tent city.
- A significant portion of homeless individuals actually have jobs, challenging the stereotype of homelessness being solely due to a lack of character.
- Beau criticizes the punitive approach of turning housing for homeless individuals into tent cities, suggesting it's more about creating a class divide.
- He questions the logic of punishing individuals who may need treatment for mental health issues by placing them in a tent city.
- Trump's focus on punishment rather than genuine help for the homeless population is criticized by Beau.
- While Beau acknowledges the importance of providing housing and social services, he points out the flaws in Trump's approach.
- Beau concludes by expressing skepticism towards Trump's plan and its underlying motives.

# Quotes

- "You ban being homeless. You make it illegal."
- "Being homeless is not a lack of character, it's a lack of money, or it's a lack of being able to find housing."
- "It's not actually about helping. It's about giving their base somebody else to look down on."
- "Why do you want to punish them?"
- "His whole gimmick is finding a group of people to punish."

# Oneliner

Trump's misguided plan to address homelessness focuses on punishment rather than genuine help, showcasing a lack of understanding and compassion for those in need.

# Audience

Advocates, activists, policymakers

# On-the-ground actions from transcript

- Advocate for compassionate and effective solutions for homelessness (implied)
- Support organizations providing social services and housing to the homeless (implied)
- Educate others about the realities of homelessness and challenge stereotypes (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's plan to address homelessness and criticizes its punitive nature, lack of understanding, and potential harm to vulnerable populations.

# Tags

#Homelessness #Trump #PunitiveApproach #SocialServices #Compassion