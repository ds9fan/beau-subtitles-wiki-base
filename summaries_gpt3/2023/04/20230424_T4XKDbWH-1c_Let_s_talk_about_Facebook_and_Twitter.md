# Bits

Beau says:

- Facebook might owe money to US users between May 24th, 2007, and December 22nd, 2022, due to privacy claims about data sharing.
- Facebook agreed to create a fund of $725 million, but users must file a claim by August 25, 2023, at Facebook user privacy settlement.com.
- Claims may not yield significant payments due to fund allocation for administration and number of claimants.
- Twitter recently removed verification marks (blue checks) from all users, aiming to make it part of Twitter Blue.
- Elon Musk jokingly gifted verification back to certain celebrities who were critical of his service.
- Clicking on the blue checks now reveals that the person subscribed to Twitter Blue, leading to backlash from non-subscribers.
- Some celebrities denounced Twitter Blue publicly, possibly harming its marketing strategy.
- Concerns have been raised about potential legal issues regarding using celebrity likeness for endorsements.
- Some major Twitter users are changing names to manipulate the blue checks, raising marketing concerns.
- Beau left Twitter but finds the social media drama entertaining while staying off the platform.

# Quotes

- "You actually have celebrities on Twitter who have realized if they change their name the blue check goes away."
- "It's the exact opposite of an endorsement."
- "Twitter Blue. You had a number come out and say that they wouldn't pay for it."
- "Everything's fine. No, it's a mess."
- "I don't want to be on it."

# Oneliner

Facebook may owe US users money due to privacy claims, while Twitter's verification mark changes lead to backlash and marketing concerns.

# Audience

Social media users

# On-the-ground actions from transcript

- File a claim at Facebook user privacy settlement.com by August 25, 2023 (suggested)
- Monitor developments regarding Twitter's verification changes and potential legal issues (suggested)

# Whats missing in summary

Details on the specifics of the potential legal issues surrounding the use of celebrity likeness for endorsements.

# Tags

#Facebook #Twitter #Privacy #Verification #SocialMedia