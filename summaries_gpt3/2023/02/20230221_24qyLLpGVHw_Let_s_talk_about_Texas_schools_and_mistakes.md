# Bits

Beau says:

- Story about superintendent leaving weapon in school restroom.
- Weapon found by third-grader, reported to teacher.
- Superintendent resigned after the incident.
- Acknowledgment of danger of unsecured firearms in schools.
- Children shouldn't be expected to be vigilant about firearms.
- Arming teachers is a bad idea.
- Near misses with firearms should be learning opportunities.
- Solution lies in changing the culture around firearms.

# Quotes

- "Acknowledge that they are dangerous, they're not a prop, and the good guy doesn't always win."
- "Near misses should be teachable moments."
- "The solution is a change in the culture that surrounds firearms."

# Oneliner

Beau talks about a superintendent leaving a weapon in a school restroom, the dangers of unsecured firearms in schools, and the need for a cultural shift around firearms safety.

# Audience

Parents, educators, community members

# On-the-ground actions from transcript

- Advocate for stricter gun safety measures in schools (implied)
- Educate children about the dangers of unsecured firearms (implied)
- Support initiatives promoting responsible firearm ownership (implied)

# Whats missing in summary

The emotional impact of near misses with firearms and the importance of prioritizing firearm safety education.

# Tags

#FirearmSafety #Schools #CommunityPolicing #CulturalShift #GunControl