# Bits

Beau says:

- Former Vice President Mike Pence has been given a subpoena from Smith in relation to the January 6 investigation.
- Nobody has officially commented on this, and it's currently a leak.
- Assuming the leak is true, Pence might be one of the last people Smith wants to talk to.
- If Pence claims he didn't know anything, it might lead to more interviews with others.
- The leak probably came from Pence's team, with possible reasons being to assert executive privilege or to give Trump a heads up.
- It is likely that Trump will try to assert executive privilege but may not succeed.
- Pence's testimony will play a significant role in shaping possible charges against the former president.
- This development marks progress in the accountability process but may not immediately lead to indictments.
- The outcome of Pence's testimony will greatly impact how charges are framed.
- The process is supposed to be secret, so the leaked information is significant.

# Quotes

- "Former Vice President Mike Pence has been given a subpoena from Smith."
- "Pence's testimony is going to matter when it comes to how the charges get framed and shaped."
- "This is a big step in that direction."
- "It's going to be mostly speculation."
- "It's just a thought."

# Oneliner

Former Vice President Mike Pence receives a subpoena in relation to the January 6 investigation, marking progress in accountability, with his testimony playing a vital role in shaping potential charges.

# Audience

Political enthusiasts, accountability advocates.

# On-the-ground actions from transcript

- Stay informed about the developments surrounding the January 6 investigation (suggested).
- Pay attention to the potential implications of Pence's testimony on accountability (suggested).

# Whats missing in summary

Insights into the potential consequences of Pence's testimony and how it may impact the ongoing investigation.

# Tags

#MikePence #January6Investigation #Accountability #Subpoena #ExecutivePrivilege