# Bits

Beau says:

- Recent developments in Memphis regarding texts, photos, videos, reports, and certification processes are confirmed.
- Footage shows officers taking photos of Mr. Nichols, which were then allegedly shared with others, including a female acquaintance.
- The department is moving forward with decertifying the officers involved and prohibiting them from being cops again.
- Decertification paperwork reveals scathing allegations about inaccurate reports and unprofessional behavior captured on body cameras.
- There will be 20 additional hours of footage to establish the officers' mindsets and capture their unprofessional comments.
- Expect more developments and major shake-ups within Memphis as this story progresses through the justice system.
- Decertification charges paint a bleak picture, with the possibility of more officers being implicated in the future.

# Quotes

- "A lot of that was speculated about. It's now confirmed."
- "This is not a story that's going to fade from the news."
- "The decertification charges paint a very bleak picture."
- "I see the possibility of other officers being implicated in this and in other things."
- "Y'all have a good day."

# Oneliner

Recent developments in Memphis confirm officers taking photos and sharing them, leading to decertification and scathing allegations of unprofessional behavior captured on body cameras.

# Audience

Community members

# On-the-ground actions from transcript

- Support initiatives advocating for police accountability (implied)
- Stay informed about the developments in Memphis and advocate for justice (implied)

# Whats missing in summary

The full transcript provides detailed insights into the confirmed developments in Memphis and the potential implications for other officers involved. Viewing the full transcript can offer a deeper understanding of the situation and its impact on the community.

# Tags

#Memphis #PoliceAccountability #JusticeSystem #Decertification #CommunityPolicing