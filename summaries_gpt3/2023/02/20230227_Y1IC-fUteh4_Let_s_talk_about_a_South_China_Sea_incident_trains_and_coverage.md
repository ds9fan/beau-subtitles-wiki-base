# Bits

Beau says:

- The media coverage of events like the South China Sea incident can create a false perception of reality by focusing on dramatic moments and not the overall context.
- Surveillance flights like the one in international airspace happen frequently, but they only become news when there is footage, like the CNN crew on board.
- Coverage of specific incidents, like train derailments, can lead to public misconceptions about the frequency and severity of such events.
- Media tends to amplify certain events based on public interest, creating a distorted view of reality for viewers.
- The focus should be on making changes and improvements based on incidents rather than solely on sensationalizing news for views and revenue.

# Quotes

- "Something being covered repeatedly doesn't actually mean that it's at an increase."
- "It's a really bad idea to shape your beliefs and your perception of the world based on frequency of coverage."
- "There should be a whole group of people trying to figure out how to make sure this never happens again."

# Oneliner

The media's focus on sensational events creates a false reality, shaping perceptions based on frequency rather than facts, hindering progress.

# Audience

News Consumers

# On-the-ground actions from transcript

- Advocate for responsible and accurate news coverage (implied)
- Support efforts to improve safety regulations and standards for incidents like train derailments (implied)

# Whats missing in summary

The full transcript provides a comprehensive analysis of how media coverage can distort reality and the importance of focusing on facts over sensationalism.