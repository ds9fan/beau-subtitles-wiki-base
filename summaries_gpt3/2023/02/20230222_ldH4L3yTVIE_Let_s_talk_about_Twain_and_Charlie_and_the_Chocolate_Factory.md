# Bits

Beau says:

- Charlie and the Chocolate Factory and Mark Twain's works are being edited to use more inclusive languages, sparking debates and questions.
- An older copy of Huckleberry Finn by Mark Twain is examined, revealing instances of potentially offensive content.
- Twain's context as a satirist and his relationships with black individuals are discussed to counter accusations of racism.
- Twain's advocacy for issues like reparations and his views on race as a social construct are pointed out.
- Misunderstandings of Twain's satirical elements, particularly in relation to race, are addressed.
- The possibility of editing Twain's work for younger audiences is discussed, focusing on maintaining the original message.
- Beau compares the editing of Mark Twain's work in 2011 to the current edits in children's books like Charlie and the Chocolate Factory.
- Editing children's books for inclusivity is seen as a commercial decision to widen the audience rather than censorship.
- Beau explains that the changes in these books are driven by capitalism, aimed at appealing to a broader market.
- The impact of capitalism on art is mentioned, suggesting that it can sometimes lead to less impactful content due to the pursuit of profit.

# Quotes

- "It's not censorship. It's not woke nonsense either, it's capitalism."
- "It's just capitalism."
- "And capitalism does have a habit of making art less impactful."
- "It's really that simple."
- "Anyway, it's just a thought."

# Oneliner

Beau examines the editing of classic literature for inclusivity through a lens of capitalism, challenging notions of censorship and oppression.

# Audience

Educators, Book Lovers, Parents

# On-the-ground actions from transcript

- Support initiatives that teach critical thinking and provide context for literature (implied).
- Encourage open-mindedness and understanding of satire in educational settings (implied).

# Whats missing in summary

The detailed analysis and examples provided by Beau in the full transcript are missing from this summary.

# Tags

#Inclusivity #ClassicLiterature #Capitalism #Education #Satire