# Bits

Beau says:

- Providing a PSA for people in Palestine, Ohio, with information on environmental safety.
- Officials claim everything is fine with the air, soil, and municipal water in Palestine.
- Residents skeptical of official statement can schedule testing for air and well water by calling 330-849-3919.
- NTSB will be investigating an accident in the area.
- Charges against a reporter arrested for covering the accident have been dropped.
- A town hall meeting was held where environmental agencies reassured residents.
- Concerns about animal health are dismissed as anecdotal.
- Lack of trust in environmental regulatory agencies due to past inaccuracies and overlooking issues involving big money.
- Beau suggests getting well water tested for peace of mind.
- Non-government environmental agencies have not conducted testing yet, raising concerns.
- Americans' skepticism towards official statements stems from past government failures in environmental matters.
- Beau encourages residents in Palestine to prioritize testing their well water and air.

# Quotes

- "Americans' skepticism towards official statements stems from past government failures in environmental matters."
- "Get your well water tested."
- "Non-government environmental agencies have not conducted testing yet, raising concerns."

# Oneliner

Providing environmental safety information for Palestine, Ohio residents, urging skepticism towards official statements and recommending testing of well water and air.

# Audience

Residents of Palestine, Ohio

# On-the-ground actions from transcript

- Schedule air and well water testing by calling 330-849-3919 (suggested)
- Get your well water tested (suggested)

# Whats missing in summary

Importance of independent testing for environmental safety in Palestine, Ohio.

# Tags

#Ohio #EnvironmentalSafety #CommunityHealth #Skepticism #Testing