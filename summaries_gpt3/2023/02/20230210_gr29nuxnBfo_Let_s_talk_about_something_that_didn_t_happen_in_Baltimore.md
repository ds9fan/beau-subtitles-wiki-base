# Bits

Beau says:

- Talks about a plan to knock out the power in Baltimore using an anti-Semitic group's template.
- Mentions that the plan was disrupted but warns that similar incidents may occur in the future.
- Notes the high expectation on authorities to prevent every attack and the inevitability of success for those attempting it.
- Encourages viewers to develop their own response and prepare for potential power outages and infrastructure loss.
- Emphasizes the importance of communication, mobility, and having a plan in place.
- Urges people to talk to their network and be ready in case such incidents happen.
- Stresses the need for individual preparedness since authorities can't stop every attempt.
- Advises taking a few minutes to plan for potential power outages to ensure safety and comfort.
- Points out that most viewers likely already have what they need and just need to work out a game plan.
- Suggests setting aside time now to plan for potential future incidents.

# Quotes

- "You just have to work out the game plan."
- "Put a little bit of thought into it."
- "You want to be able to maintain a level of safety and comfort if something like this happens."

# Oneliner

Be prepared for potential power outages by developing a response plan to ensure safety and comfort.

# Audience

Community members

# On-the-ground actions from transcript

- Develop your own response plan in case of power outages (suggested)
- Communicate with your network about emergency plans (suggested)
- Ensure you have necessary supplies for potential infrastructure loss (suggested)

# Whats missing in summary

The full transcript provides additional context on the importance of personal preparedness and the potential risks associated with infrastructure attacks.

# Tags

#Preparedness #Infrastructure #Safety #Community #ResponsePlan