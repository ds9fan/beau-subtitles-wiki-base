# Bits

Beau says:

- Explains the intricacies of federal sentencing in the U.S. system, shedding light on common misconceptions.
- Emphasizes the discrepancy between maximum sentence and actual sentencing guidelines in federal courts.
- Mentions that journalists often focus on maximum potential sentences, creating unrealistic expectations for the public.
- Talks about the impact of criminal history and offense levels on sentencing outcomes.
- Provides examples of how different factors like criminal history and offense levels can affect sentences for individuals charged with the same crime.
- Points out the importance of understanding federal sentencing guidelines to accurately anticipate potential sentences.
- Raises awareness about journalists sometimes inaccurately predicting sentences based on maximum penalties rather than sentencing guidelines.
- Illustrates how sentences can drastically change based on new information during criminal proceedings.
- Clarifies that sentences in the federal system typically run concurrently for multiple charges, not consecutively.
- Stresses the need for accurate information dissemination, especially in cases involving law enforcement and civil rights violations.

# Quotes

- "Journalists end up setting expectations way too high when they talk about the maximums rather than the sentencing guidelines."
- "Because of a court case called Booker, I think, they pay attention to something called the Sentencing Guidelines."
- "The downside is in cases where, let's say it's a cop who violated somebody's civil rights, and the people in the street, they hear that he's looking at 10 years, maximum of 10 years, but then the judge sentences them to three. It's because that's what the guidelines said."
- "Generally speaking, the person has to have basically caught all of the enhancements and have a huge criminal history to get to the maximum."
- "You can't accurately guess what the sentence will be. You can't determine the range until the criminal proceedings are done."

# Oneliner

Beau clarifies federal sentencing guidelines, stressing the importance of understanding them to set accurate expectations, especially in cases involving law enforcement and civil rights.

# Audience

Journalists, Legal Advocates

# On-the-ground actions from transcript

- Understand federal sentencing guidelines and accurately convey information to the public (implied).

# Whats missing in summary

Detailed examples and explanations of how criminal history and offense levels impact federal sentencing outcomes.

# Tags

#FederalSentencing #Journalism #LegalSystem #CriminalJustice #CivilRights