# Bits

Beau says:

- Idaho legislation proposed to ban mRNA vaccines.
- Aimed at catering to anti-vaxxers from recent public health crisis.
- Proposed legislation could impact vaccines for RSV, HIV, and cancer.
- Undercuts the concept of medical freedom.
- Those in power shift from representing to ruling.
- Legislation targets bodily autonomy.
- Potential future step to prevent out-of-state vaccine access.
- Beau questions the legislative reasoning and state interest.
- Criticizes the authoritarian nature of the proposed legislation.
- Warns about the dangers of empowering authoritarians in governance.

# Quotes

- "It's amazing how the concept of freedom disappears the second those people who pretend to champion..."
- "They want to rule you. They want to tell you what you can do, what you can do with your own body."
- "You get to deny them the right to seek protection."
- "If you put an authoritarian in charge, they will behave as an authoritarian."
- "They will believe that they can run your life better than you can."

# Oneliner

Idaho legislation threatens vaccine access, targeting medical freedom and bodily autonomy, warning of authoritarian rule.

# Audience

Vaccine advocates

# On-the-ground actions from transcript

- Advocate for equitable vaccine access in Idaho (suggested)
- Support organizations fighting for medical freedom and bodily autonomy (implied)

# Whats missing in summary

Importance of resisting authoritarian legislation and protecting individual rights.

# Tags

#Idaho #Legislation #Vaccines #MedicalFreedom #Authoritarianism