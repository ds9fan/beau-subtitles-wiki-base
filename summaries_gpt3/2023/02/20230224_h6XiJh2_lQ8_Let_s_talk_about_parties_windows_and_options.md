# Bits

Beau says:

- Historically, the lack of a left-wing party in the US can be attributed to the Cold War era propaganda against left-leaning ideologies.
- As demographics shift and younger generations grow up without Cold War indoctrination, there is more acceptance of left-leaning ideas.
- The two main parties in the US are the Democratic Party, considered center-right or centrist, and the Republican Party, which has shifted to the extreme right.
- Third parties in the US struggle to gain traction because they are outside the established range of acceptable political thought, mainly center to center-right.
- Lack of focus on local races, extreme candidate selection, and platforms outside the acceptable range contribute to the failure of third parties to succeed electorally.
- A party centered around social democracy, left by US standards but not truly leftist, could potentially gain ground in the future.
- Fear of far-right authoritarianism in the US leads many to vote for the Democratic Party out of self-defense, even if they do not fully support it.
- Until the threat of far-right authoritarianism diminishes, efforts to establish a social democratic or left-leaning party face significant hurdles.
- Building a social democratic movement within the Democratic Party may be a more feasible approach than starting a new party from scratch.
- Overcoming the challenges of propagandized views, historical traditions, and misconceptions about party ideologies is key to establishing effective left-wing representation in the US.

# Quotes

- "Their main issue is that they're outside the Overton window."
- "They're not what people think really have the power, but they do."
- "It's a fight for its life against far-right authoritarianism."
- "They're voting for the lesser of two evils."
- "A lot of hurdles for a left-wing party in the United States."

# Oneliner

The lack of an effective leftist party in the US stems from Cold War propaganda, fear of far-right authoritarianism, and challenges with third-party positioning and representation.

# Audience

Political activists and organizers.

# On-the-ground actions from transcript

- Mobilize at local levels within existing political parties to shift ideologies towards social democracy (suggested).
- Focus on building infrastructure and winning local elections over a 10-15 year period to establish effective left-wing representation (implied).

# Whats missing in summary

The full transcript provides a comprehensive analysis of the obstacles facing the establishment of a successful left-wing party in the US and offers insights into potential strategies for overcoming these challenges.

# Tags

#USPolitics #LeftWingParty #SocialDemocracy #ColdWar #OvertonWindow