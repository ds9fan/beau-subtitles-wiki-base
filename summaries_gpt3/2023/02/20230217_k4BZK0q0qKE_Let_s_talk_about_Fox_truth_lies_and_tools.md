# Bits

Beau says:

- Addresses the topic of Fox News, truth, lies, and tools to combat misinformation.
- Mentions a filing revealing private messages from major figures at Fox News discussing baseless claims and lies about the 2020 election.
- Emphasizes that while Dominion may not win their case against Fox, the filing provides a tool to reach out to people who believe everything from Fox News.
- Suggests using the quotes from the filing to show family members or loved ones the reality of what was going on behind the scenes at Fox News.
- Encourages finding alternative news sources that family members may accept and using those to break them away from solely relying on Fox News.
- Stresses the importance of going through the quotes with family members to help them widen their information sources and not blindly accept everything from Fox News.
- Acknowledges that convincing as the filing may be, it does not guarantee Dominion's win due to the high bar for defamation cases in the United States.
- Proposes using the filing as a tool to help bring family members back to reality and prompt them to seek information from diverse sources.

# Quotes

- "Dominion may not win their case, but it's not worthless because it gives you a tool."
- "It might help them move to a point where they start to widen their information sources."
- "Will that guarantee Dominion win? No, no. It's a very high bar that they're going to have to overcome."
- "It might help you get a family member back."
- "It's a tool that you can use to reach out to people."

# Oneliner

Beau addresses using revelations about Fox News lies as a tool to combat misinformation and prompt loved ones to seek information from diverse sources.

# Audience

Family members and loved ones

# On-the-ground actions from transcript

- Reach out to family members using the revealed quotes from major figures at Fox News (suggested).
- Find out alternative news sources your family members might accept and share information from those sources (exemplified).

# Whats missing in summary

The full transcript provides detailed guidance on using revelations about Fox News to prompt loved ones to seek information from diverse sources, enhancing critical thinking and combating misinformation.

# Tags

#FoxNews #Misinformation #Dominion #Family #InformationSources