# Bits

Beau says:

- Companies have been using rainbows on their packaging as a performative act to capture a wider audience, especially the LGBTQ+ community.
- The performative act of supporting marginalized communities through marketing tactics is shifting thought and capturing a growing market.
- However, the LGBTQ+ community and their allies are becoming more aware of companies' true intentions through their political affiliations and actions.
- Companies that solely rely on superficial gestures like rainbow packaging will soon face consequences as the targeted community becomes more informed.
- To truly support marginalized communities and secure future markets, companies need to take tangible actions like offering employee transfers and covering relocation costs.
- Large corporations prioritize profit over morals or ideology, and failing to support marginalized communities could result in losing more than 20% of the market share.
- This moment challenges companies to decide between genuine support for marginalized communities or facing consequences as the market becomes more socially conscious.
- Companies need to involve their marketing divisions in decision-making processes to ensure alignment with shifting demographics and values.
- Neglecting to do the right thing now may result in abandoning a significant portion of the market that includes Millennials, Gen Z, and their social circles.
- It is a critical juncture for companies to choose between staying viable in the future by authentically supporting marginalized communities or facing market backlash due to inaction.

# Quotes

- "For years, for years, you've had a lot of companies slap a rainbow on the packaging of their product."
- "A lot of people call it rainbow capitalism."
- "The cost of doing the right thing. It's not high."
- "Make no mistake about it."
- "It's just a thought, y'all have a good day."

# Oneliner

Companies using performative gestures like rainbow packaging face the risk of losing over 20% of the market share if they fail to authentically support marginalized communities.

# Audience

Business Leaders

# On-the-ground actions from transcript

- Offer employees in targeted jurisdictions the option to transfer within the company and cover relocation costs (implied).
- Companies need to refrain from supporting politicians who introduce legislation targeting marginalized communities (implied).
- Involve marketing divisions in decision-making processes to ensure alignment with shifting demographics (implied).

# Whats missing in summary

The full transcript provides a detailed explanation of how companies can transition from performative gestures to genuine support for marginalized communities to secure their future market share.

# Tags

#RainbowCapitalism #PerformativeAllyship #CorporateResponsibility #MarketStrategy #InclusionInitiatives