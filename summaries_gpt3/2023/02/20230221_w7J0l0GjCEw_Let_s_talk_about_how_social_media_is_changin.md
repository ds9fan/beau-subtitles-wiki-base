# Bits

Beau says:

- Major social media platforms are starting to shift their standards regarding verification, offering paid verifications for accounts.
- Twitter has experimented with paid verifications, and Facebook appears to be planning something similar.
- Verified accounts may gain increased visibility and reach, which raises concerns about potential misuse by bad actors.
- There is a worry that verified accounts with increased reach could shape narratives or spread harmful information.
- The shift towards a pay-to-play scenario on platforms may make it easier for bad non-state actors to run information operations.
- Verified accounts may not necessarily be trustworthy or accurate; they could simply have paid for verification.
- Beau personally decided to take a step back from larger platforms due to concerns about authenticity and profit-driven content.
- He suggests fact-checking information from verified accounts and not blindly trusting them.
- Beau advocates for being cautious about narratives from verified accounts, as they could be manipulated by bad actors.
- There is a need to be vigilant and aware of the potential misuse of increased reach and visibility on social media platforms.

# Quotes

- "There's a big risk to that and it's something that everybody should be on guard for."
- "The verified label indicated a level of authenticity and accuracy. Whereas now it may not be the case."
- "I'm going to be kind of backing off the larger platforms and continuing to look for something that is more open, that is more organic."
- "To me, there seems like there's going to be a lot of room for it to be misused."
- "Y'all have a good day."

# Oneliner

Major platforms are shifting towards paid verifications, raising concerns about the misuse of increased reach and visibility by bad actors; be cautious and fact-check information.

# Audience

Social media users

# On-the-ground actions from transcript

- Fact-check information from social media platforms (suggested)
- Be cautious about narratives from verified accounts (implied)

# Whats missing in summary

Importance of fact-checking and being cautious about narratives from verified accounts to combat potential misinformation on social media.

# Tags

#SocialMedia #Verification #Misinformation #FactChecking #OnlineSafety