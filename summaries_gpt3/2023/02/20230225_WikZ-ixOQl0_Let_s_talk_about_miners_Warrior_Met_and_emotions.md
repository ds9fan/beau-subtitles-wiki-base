# Bits

Beau says:

- Addressing the situation with workers at Warrior Met, a coal mine in Alabama, and the push to return to work after a strike.
- Disappointment expressed towards the company, politicians in Alabama, and national politicians for not supporting the workers adequately.
- Acknowledging the disappointment felt by viewers who supported the workers during the strike.
- Clarifying the role of offering support and solidarity rather than making policy decisions.
- Expressing trust in the workers' decisions, whether to strike or move to a new phase.
- Emphasizing the importance of supporting causes worth fighting for, regardless of the outcome.
- Recognizing the families of the workers involved in the labor movement and their ongoing needs.
- Commitment to continue helping individual families if necessary.
- Stressing the significance of the American labor movement story and the continuous nature of fighting for workers' rights.

# Quotes

- "It's not a Hollywood story. It's an American labor movement story."
- "You put support towards something because it's a cause worth supporting."
- "You're there to back their play."
- "We trust the workers when they say it's time to strike."
- "It's an American labor movement story. There's not always a happy ending."

# Oneliner

Beau addresses the return-to-work push at Warrior Met, expressing disappointment and stressing the ongoing support for workers in the American labor movement.

# Audience

Supporters of workers' rights

# On-the-ground actions from transcript

- Support individual families if needed (implied)
- Continue offering assistance and solidarity to workers (implied)

# Whats missing in summary

The full transcript provides a deep dive into the dynamics of supporting workers in the American labor movement and the ongoing struggles faced by workers and their families.

# Tags

#WorkersRights #AmericanLaborMovement #Support #Solidarity #Disappointment