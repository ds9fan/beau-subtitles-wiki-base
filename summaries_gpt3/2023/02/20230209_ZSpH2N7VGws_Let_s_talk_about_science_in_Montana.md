# Bits

Beau says:

- Addressing science education in Montana, focusing on scientific theory and fact.
- Explaining the impact of Senate Bill 235, aiming to change how science is taught in schools.
- Pointing out the importance of teaching scientific theory alongside scientific fact.
- Emphasizing that science evolves and isn't just a static collection of facts.
- Warning about the consequences if theoretical science is eliminated from education.
- Criticizing politicians for prioritizing culture wars over proper education.
- Expressing concern for the future of Montana's students and their ability to compete.
- Questioning the motives behind the bill and its potential negative outcomes.
- Advocating for careful consideration of the implications of altering science education.
- Urging Montana to prioritize proper science education for the benefit of its children.

# Quotes

- "Teaching science without teaching theoretical science is pointless."
- "Montana is going to be looked at as that place where we definitely can't hire somebody from there."
- "The casualties of that culture war will be the children of Montana."
- "Be ready for drastic outcomes because the students won't be able to pass an HCT."
- "They want to force willful ignorance on the children of Montana."

# Oneliner

Beau warns Montana about the detrimental effects of eliminating scientific theory from education, potentially hindering students' future opportunities and competitiveness.

# Audience

Montana residents, educators, parents

# On-the-ground actions from transcript

- Challenge Senate Bill 235 through advocacy and education (implied)
- Support comprehensive science education for Montana's students (implied)
- Stay informed about legislative changes affecting education (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the implications of altering science education in Montana and the potential negative outcomes for students' future opportunities and competitiveness.

# Tags

#ScienceEducation #Montana #SenateBill235 #STEM #CultureWar