# Bits

Beau says:

- Public service announcement about changes in Medicaid eligibility
- Anticipated loss of coverage for 10 to 15 million people
- Eligibility checks paused during public health issue, now resuming
- Children's coverage through Medicaid or CHIP may still be valid even if adult coverage is lost
- People making slightly more may become ineligible for Medicaid
- Affordable Care Act's marketplace may provide options for those losing Medicaid coverage
- Timeline for eligibility checks varies by state
- Red states likely to move quickly in checking eligibility
- Urges people to gather information and prepare, as changes could start as soon as April
- Emphasizes importance of checking children's coverage separately

# Quotes

- "Just because you lose your coverage through Medicaid doesn't mean that your children do."
- "For those people, it's probably a little bit more important to gain as much information as you can now."
- "But most importantly, and this addresses 80% of the questions coming in, just because you lose your coverage doesn't mean that your kids do."

# Oneliner

Beau issues a public service announcement regarding upcoming Medicaid eligibility changes, warning of potential coverage loss for millions and advising on checking children's coverage separately.

# Audience

Medicaid beneficiaries

# On-the-ground actions from transcript

- Research Medicaid eligibility changes in your state and prepare for possible coverage loss (implied)
- Verify children's Medicaid or CHIP coverage separately if your own Medicaid eligibility changes (implied)

# Whats missing in summary

Importance of taking proactive steps and staying informed to navigate potential Medicaid coverage changes effectively.

# Tags

#Medicaid #Healthcare #EligibilityChanges #PublicServiceAnnouncement #AffordableCareAct