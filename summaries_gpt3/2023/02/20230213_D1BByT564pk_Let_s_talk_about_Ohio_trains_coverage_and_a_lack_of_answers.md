# Bits

Beau says:

- Explains a train derailment in a small Ohio town that led to a fire due to chemicals on the train.
- Mentions the lack of in-depth media coverage on the environmental impacts of the incident.
- Points out uncertainty about the future environmental impacts on soil, water, and air.
- States that the scale and scope of the impacts are still unknown.
- Notes the potential for long-term issues like cancer clusters and contaminated well water.
- Talks about different jurisdictions handling the situation differently, with West Virginia being proactive in water supply testing.
- Suggests that more testing needs to be done to understand the situation better.
- Raises questions about why the incident occurred and what safety measures can be taken to prevent future accidents.
- Emphasizes the need for clear reporting, understanding the environmental impact, and identifying the root cause of the derailment.
- Calls for accountability from companies to invest in safety measures.

# Quotes

- "We nuked a town with chemicals over this train."
- "We need a clear picture. We need real models about the environmental impact of this."
- "The problems need to be fixed. It needs to be mitigated so this doesn't happen to some other small town."

# Oneliner

Beau delves into a train derailment in Ohio, addressing the lack of media coverage on environmental impacts, uncertainties about future consequences, and the necessity for accountability and prevention measures.

# Audience

Environmental activists, concerned citizens

# On-the-ground actions from transcript

- Contact local authorities to inquire about safety procedures and prevention measures (implied)
- Support proactive water testing initiatives like those in West Virginia (exemplified)
- Advocate for thorough testing and reporting on environmental impacts (implied)

# Whats missing in summary

The emotional impact on the affected community and potential long-term consequences beyond environmental damage.

# Tags

#Ohio #TrainDerailment #EnvironmentalImpact #Accountability #Prevention