# Bits

Beau says:

- Shelby County prosecutor indicated 20 more hours of police footage in Memphis involving Mr. Nichols.
- Ben Crump, representing the family, anticipates receiving the footage soon, suggesting it may reveal critical information.
- Beau expected there to be more footage than what was initially released due to missing commentary post-incident.
- Unreleased footage likely includes statements from other officers present, shedding light on their mindset and actions.
- The additional footage may implicate or exonerate the officers involved based on their commentary.
- The focus of the released footage was on the incident, but body cam footage typically includes more context.
- The 20 hours of footage may contain irrelevant content like dash cam footage or officers' idle moments.
- The importance of releasing all footage to provide a complete picture and connect the dots, as stated by Crump.
- Beau believes the additional footage is unlikely to exonerate the officers who engaged in violence.
- Seeking justifications or rationalizations for police violence is not productive; acknowledging the systemic issue is necessary.

# Quotes

- "Stop looking for a way to make this okay."
- "In some situations, if you look hard enough, you can create a rationalization for it. But even a rationalization doesn't mean that it's justified."
- "At some point, the American people have to look at what law enforcement has become in this country and face it and understand what's happening."
- "We just didn't get the footage."

# Oneliner

Shelby County prosecutor revealed 20 additional hours of police footage in Memphis; unreleased commentary may implicate or exonerate officers, urging to stop seeking justifications for police violence.

# Audience

Advocates for police accountability

# On-the-ground actions from transcript

- Contact local officials to demand transparency and accountability in the release of all relevant footage (suggested).
- Support organizations working towards police reform and accountability in your community (exemplified).

# Whats missing in summary

The full transcript provides a detailed analysis of the importance of complete footage in understanding incidents of police engagement and the need to address systemic issues within law enforcement.

# Tags

#PoliceFootage #Transparency #PoliceAccountability #SystemicIssues #CommunityAction