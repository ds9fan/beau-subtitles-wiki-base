# Bits

Beau says:

- Major figures in the US suddenly hope Ukraine doesn't try to go into Crimea.
- Leaders in the West are cautious about wars losing popular support after 90 days.
- Nations prioritize interests over friendships.
- The West's vocal support for Ukraine may have limits.
- Retaking recently invaded territories might be easier and less resource-intensive.
- Biden administration initially prioritized Ukraine's strategic autonomy.
- Recent shift in US-West stance on Ukraine deviates from previous approach.
- Messaging and signals are being used on the international level regarding Ukraine.
- All sides use propaganda and information operations in conflicts.
- Support for backing Ukraine with supplies is declining but not drastically.
- Possibilities include Western disinterest in Crimea, information operations, and signaling to Putin.
- Western signaling may indicate considering retaking Crimea, potentially prompting Putin to reconsider.
- Uncertainty surrounds the true intentions behind Western messaging.
- Potential wave of Western signaling to prompt Putin to leave Crimea and cut losses.

# Quotes

- "Nations don't have friends. They have interests."
- "Support begins to erode."
- "Messaging on the international level."
- "All sides use propaganda."
- "It's just a thought y'all."

# Oneliner

Major figures express hope Ukraine avoids Crimea, West cautious on wars, signaling in Ukraine stirs uncertainty.

# Audience

Foreign policy analysts

# On-the-ground actions from transcript

- Analyze and stay informed about international messaging and signals (implied).
- Monitor developments and shifts in US and Western strategies towards Ukraine (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the US-West shift regarding Ukraine and potential messaging strategies employed in international relations.

# Tags

#Ukraine #US #InternationalRelations #Messaging #Propaganda