# Bits

Beau says:

- Exploring why people disregard the Doomsday Clock despite it being a gauge of nuclear war risk.
- Americans tend to focus on the DEFCON rather than the Doomsday Clock.
- Reasons for this include disliking the methodology behind the clock and the belief that it's not updated frequently.
- The DEFCON provides immediate information, but its updates are not public, leading to lack of preparation time.
- The core issue is people's tendency to ignore vital information due to a sense of safety associated with military involvement.
- Anti-intellectualism in the US contributes to this phenomenon.
- The discomfort from looking at the Doomsday Clock mirrors confronting climate change models – both depict scary realities.
- Beau encourages younger viewers to value education as an asset that can't be taken away, despite societal perceptions.
- Investing in education is emphasized as a lifelong benefit that should not be undermined by societal stigmas.

# Quotes

- "Americans tend to focus on the DEFCON rather than the Doomsday Clock."
- "Your education is pretty much the only thing that can't be taken away from you."
- "Being smart, being educated, being informed is somehow bad."

# Oneliner

Exploring why Americans prioritize DEFCON over the Doomsday Clock and the impact of anti-intellectualism on societal perceptions of education.

# Audience

Young individuals, Americans

# On-the-ground actions from transcript

- Value education as an asset (suggested)
- Challenge societal stigmas around intellect and education (suggested)
- Encourage the importance of being informed and educated (suggested)

# Whats missing in summary

Exploration of societal stigmas surrounding education and the impact on critical decision-making and preparation.

# Tags

#Education #Anti-Intellectualism #SocietalPerceptions #DoomsdayClock #DEFCON