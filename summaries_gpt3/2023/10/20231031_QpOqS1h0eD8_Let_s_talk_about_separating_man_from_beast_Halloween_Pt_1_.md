# Bits

Beau says:

- Exploring the concept of the separation between man and beast, and questioning if it truly exists or if it's a construct.
- The historical context of the werewolf myth as a punishment for doing something bad, turning humans into wolves.
- Over time, the werewolf narrative changes from a punishment to a condition representing the beast within.
- The idea that humans may have created monsters like the werewolf to distance themselves from beastly acts.
- An example from 1521 where a story about Frenchmen turning into wolves was possibly a rationalization for their inhuman actions.
- Challenging the notion of what separates humanity from other animals, discussing our ability to transmit complex ideas.
- The observation that while humans have the ability to share complex ideas, it is often used to justify beastly actions towards others.
- The recurring rhetoric of dehumanizing others by labeling them as animals to justify mistreatment.
- Those who dehumanize others are often the ones responsible for committing the most inhumane acts throughout history.
- Posing questions about the evolution of myths and whether good humans allow inhumane acts to persist.

# Quotes

- "Those who dehumanize others are often the ones responsible for committing the most inhumane acts."
- "There's an evolution, you have to wonder if those original myths really meant the person was actually turned into an animal."
- "Humans doing something so beastly that nobody wanted to believe that it was a human that did it."

# Oneliner

Exploring the origins of the separation between man and beast, questioning humanity's treatment of others as less than human.

# Audience

Activists, Philosophers, Movie Buffs

# On-the-ground actions from transcript

- Challenge dehumanizing rhetoric towards marginalized groups by actively promoting empathy and understanding (implied).

# Whats missing in summary

A deeper dive into the historical origins of myths and their impact on societal views today.

# Tags

#Mythology #Dehumanization #Humanity #Werewolf #History