# Bits

Beau says:

- Receives a message criticizing his video for explaining the economy and blaming Trump, forecasting a recession caused by Biden.
- Message predicts a century's worst economic collapse under Biden, warning Beau about affording his trailer and heating bill.
- Beau received the message on October 18th, 2022, but still has lights.
- One year ago, forecasts predicted a recession with 100% certainty due to be a major blow to Biden, which did not occur.
- GDP numbers for the third quarter are out, up by 4.9%, indicating no recession.
- Beau reminds viewers that presidents don't control the economy; individual beliefs and faith in the economic system play a significant role.
- Urges people to stop listening to fear-mongering sources and recalls the failed predictions of the worst economic collapse in a century.
- Encourages viewers not to believe headlines that aim to scare them and to be cautious about alarming forecasts.

# Quotes

- "Stop listening to people who get their paycheck by scaring you."
- "100% is what they said. It didn't happen."
- "When you see headlines that are just 'Oh, unbelievable,' don't believe them."

# Oneliner

Beau debunks fear-mongering economic forecasts, reminding viewers of individual control over economic belief systems and urging skepticism towards alarming headlines.

# Audience

Viewers

# On-the-ground actions from transcript

- Stop listening to fear-mongering sources (implied)
- Be cautious about alarming forecasts (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of failed economic forecasts, urging viewers to question fear-mongering narratives and take control of their economic beliefs.

# Tags

#Economy #Forecasts #FearMongering #IndividualControl #Skepticism