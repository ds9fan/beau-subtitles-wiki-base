# Bits

Beau says:

- Georgia's maps are being redrawn following a federal judge's ruling that Republican legislators diluted the voting power of Black Americans after the 2020 census.
- The judge ordered the creation of new maps to be in use for the next election, including additional majority Black districts.
- The governor of Georgia surprisingly scheduled a special session to draw up new maps on November 29th, complying with the law.
- Republicans in Georgia seem to be accepting the need for new maps, with the situation potentially coming to a close.
- The judge specified that a new congressional district will be in the western part of Atlanta, being very meticulous in the order.
- The resolution of this situation depends on avoiding Republican shenanigans, with hopes for everything to be settled before the next election.

# Quotes

- "Republicans comply with the law that it's kind of surprising and noteworthy when they do."
- "The judge actually said that the new congressional district was going to be in the western part of Atlanta."
- "It looks like the Republicans in Georgia are just like, well, okay, I guess we have to draw new maps."

# Oneliner

Georgia's maps are being redrawn to address the dilution of Black voters' power, with Republicans surprisingly complying, potentially resolving the issue before the next election.

# Audience

Georgia residents, Voting Rights Advocates

# On-the-ground actions from transcript

- Contact local voting rights organizations to stay informed and involved in monitoring the redrawing of maps (implied).
- Attend local community meetings or town halls to ensure fair representation during the map redrawing process (implied).

# Whats missing in summary

The full transcript may provide more details on the specific requirements outlined by the federal judge and potential challenges that could arise during the map redrawing process.