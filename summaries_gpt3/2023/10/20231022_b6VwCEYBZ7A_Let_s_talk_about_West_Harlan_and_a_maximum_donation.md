# Bits

Beau says:

- GOP mega donor Harlan Crow gave the max donation to noted progressive Cornell West for his independent third-party presidential campaign.
- The $3,300 donation is being sensationalized in the news, implying that West may compromise his values for money.
- West taking the money is not about selling out but about the necessity of funding his campaign.
- Crow likely provided the donation because he wants Biden to lose, believing West could draw votes away from him.
- Beau believes that the impact of West running on Biden's votes is overstated and that it might actually encourage more people to vote.
- The coverage of the donation is heavily influenced by partisanship, but Beau doesn't think $3,300 is enough to buy someone like West.
- Even a GOP mega-donor like Crow can support a progressive if it benefits his cause, showing a crossing of ideological lines.
- Beau suggests looking beyond headlines and sensationalism, considering the actual dollar amount involved in the donation.

# Quotes

- "The coverage of this is over 3,300 bucks and in the grand scheme, I don't think that's enough to buy somebody like West."
- "West running might get people to show up to vote who lean towards Biden, but don't like him enough to actually show up and vote for him."
- "Even a GOP mega-donor can reach across party and ideological lines if he thinks it's going to benefit him and his cause."

# Oneliner

GOP mega donor supports noted progressive with max donation for presidential campaign amidst sensationalized news coverage and partisan influence.

# Audience

Voters, Media Consumers

# On-the-ground actions from transcript

- Support third-party candidates in elections (implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on political donations, partisan influences, and the impact of third-party candidates beyond sensationalized headlines.

# Tags

#PoliticalDonations #ThirdPartyCandidates #PartisanInfluence #Sensationalism #ElectionImpact