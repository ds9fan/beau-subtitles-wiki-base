# Bits

Beau says:

- Biden is not on the ballot in New Hampshire for the primary, but this does not mean he is not running.
- Conflicting rule sets in New Hampshire and South Carolina are causing this situation.
- New Hampshire's primary must be the first in the nation, but the DNC wants the first primary to be more representative, so they chose South Carolina.
- South Carolina is chosen because it is more representative of the diversity in the Democratic Party.
- Biden is following DNC rules by not filing to have his name on the ballot in New Hampshire.
- Democrats in New Hampshire are being encouraged to write Biden's name in.
- Election officials are figuring out how to handle the influx of write-in ballots.

# Quotes

- "The answer is simple."
- "No great mystery here."
- "There are two rule sets and one of them he has to follow if he wants delegates."

# Oneliner

Biden faces conflicting rule sets in NH and SC, where DNC's push for diversity led to his absence from NH ballot, urging write-ins instead.

# Audience

Primary voters

# On-the-ground actions from transcript

- Write Biden's name in on the ballot (suggested)
- Election officials should prepare for handling a significant number of write-in ballots (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of the conflicting rule sets impacting Biden's presence on the primary ballot and encourages understanding of the situation.

# Tags

#Biden #PrimaryElections #DNC #RuleSets #Voters