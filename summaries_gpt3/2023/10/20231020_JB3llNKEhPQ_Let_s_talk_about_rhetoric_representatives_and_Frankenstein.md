# Bits

Beau says:

- Marionette Miller Meeks, a U.S. House representative, faced death threats and harassment after changing her vote for Speaker of the House.
- The Republican Party's inflammatory rhetoric and labeling of Americans as enemies has contributed to the normalization of threatening behavior.
- The rhetoric has escalated to the point of discussing Civil War and targeting those who don't follow the MAGA ideology.
- Constantly pushing for division and conflict within the party keeps the base on edge and drives voter turnout.
- Beau calls out the Republican Party for years of spreading lies and fueling anger through inflammatory rhetoric.
- The analogy of Frankenstein is used to illustrate that sometimes the creator, not the creation, is the true monster.

# Quotes

- "One thing I cannot stomach or support is a bully."
- "The rhetoric needs to stop."
- "Frankenstein isn't the monster in that story."
- "A wise person knows that Frankenstein is the monster."
- "Y'all have a good day."

# Oneliner

Marionette Miller Meeks faced threats, revealing how Republican rhetoric fuels division and anger, akin to Frankenstein's tale.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact authorities if you witness threats or harassment (suggested)
- Read and share information to counter lies and misinformation (suggested)

# Whats missing in summary

The detailed analysis and examples provided by Beau in the full transcript are missing in this summary.

# Tags

#USPolitics #RepublicanParty #InflammatoryRhetoric #Unity #CivilWar