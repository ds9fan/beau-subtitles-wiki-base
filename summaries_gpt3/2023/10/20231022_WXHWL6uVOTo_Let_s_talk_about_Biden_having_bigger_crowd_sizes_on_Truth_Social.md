# Bits

Beau says:

- Biden campaign set up an account on TruthSocial, Trump's social media site, to exploit Republican infighting.
- Majority of TruthSocial users seek anger and outrage as a release, making them likely to follow the Biden campaign for that content.
- Biden-Harris HQ has 32,100 followers on TruthSocial, while the Trump campaign has 26,600.
- Strategy involves letting Republicans attack each other, eliminating the need for Democratic attack ads.
- Biden's larger crowd sizes on TruthSocial are noteworthy.
- The approach appears to be effective in exploiting the disarray within the Republican Party.
- Democratic Party benefits from Republicans constantly attacking each other on TruthSocial.

# Quotes

- "People on Truth Social, the majority of them, they want to be mad."
- "The anger, that's a release for their anger."
- "I definitely thought they were going to get a decent amount of subscribers just for the outrage."
- "I talked about this, I think last week, maybe earlier this week."
- "Biden has bigger crowd sizes."

# Oneliner

Biden's campaign strategically engages in TruthSocial to exploit Republican infighting and anger for effective engagement.

# Audience

Political strategists

# On-the-ground actions from transcript

- Join and follow political campaigns on social media to stay informed (exemplified)
- Monitor and analyze social media strategies of political parties for insights (exemplified)

# Whats missing in summary

Analysis of the implications of political campaigns leveraging social media platforms for strategic advantages.

# Tags

#PoliticalStrategy #TruthSocial #RepublicanParty #DemocraticParty #SocialMedia