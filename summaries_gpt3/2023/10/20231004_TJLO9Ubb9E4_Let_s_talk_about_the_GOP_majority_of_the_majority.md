# Bits

Beau says:

- Explains the Hastert Rule, an informal rule among House Republicans where a majority of Republicans must support bringing something to the floor.
- Points out that because of polarization, this rule means just 26% of the House can prevent something from getting a vote.
- Criticizes the rule as undermining democratic principles, giving minority rule power over majority support.
- Emphasizes that Republicans are prioritizing ruling over representing, wanting obedience rather than true democracy.
- Notes that the current Republican Party differs from the past, becoming more authoritarian and less about small government conservatism.

# Quotes

- "They want 26 percent of the House of Representatives to be able to determine everything."
- "It's not representation. It's an example of minority rule."
- "They're authoritarians who want to tell you what to do, tell you what to be afraid of, so they can do whatever they want."

# Oneliner

Beau explains the Hastert Rule, revealing how a minority of Republicans can prevent majority-supported measures in the House, undermining democratic principles.

# Audience

House Republicans

# On-the-ground actions from transcript

- Read up on the Hastert Rule and its implications (suggested)
- Educate others on the potential consequences of minority rule in the House (suggested)

# Whats missing in summary

The full transcript provides a detailed breakdown of the Hastert Rule and its impact on democracy, offering insights into Republican Party shifts towards authoritarianism.

# Tags

#HastertRule #HouseRepublicans #MinorityRule #Authoritarianism #Democracy