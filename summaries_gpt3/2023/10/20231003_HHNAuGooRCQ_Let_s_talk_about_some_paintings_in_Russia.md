# Bits

Beau says:

- Russia has been painting bombers on the ground at their air bases as decoys, revealed by satellite imagery.
- Back in the day, this tactic might have been effective, but in today's world, it's easy to tell that these are just paintings.
- The purpose behind painting fake bombers on the ground is likely to disrupt targeting of actual bombers that have been expensive targets.
- Russia's resolution with their satellites and other airborne imagery isn't great, leading them to believe this tactic will work.
- One suggestion is that Russia may plan on parking their actual bombers on top of the painted ones, hoping they will be marked as "do not hit" targets.
- There's doubt about the effectiveness of this strategy, especially considering the advanced technology available today.
- Russia might aim to outsmart mission planners by hiding their aircraft in the open on top of the painted targets.
- The hope is that the real bombers will be missed due to being flagged as decoys.
- While this tactic may have worked in the past, it is unlikely to be successful in 2023.
- It remains to be seen whether Ukraine will fall for this strategy.

# Quotes

- "They may later plan on parking their actual bombers on the painted ones."
- "I think they're trying to outsmart the mission planners."
- "It's just a thought, y'all have a good day."

# Oneliner

Russia's tactic of painting bombers as decoys may not be as silly as it seems, with potential plans to outsmart mission planners.

# Audience

Military Analysts

# On-the-ground actions from transcript

- Monitor and analyze Russia's tactics in the region (implied).
- Stay informed about developments in military strategies and technologies (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of Russia's unconventional military tactics, offering insights into potential strategies and their implications for modern warfare.

# Tags

#Russia #MilitaryTactics #Decoys #Bombers #SatelliteImagery