# Bits

Beau says:

- Addressing three seemingly unrelated news stories that converge.
- Mentioning a social media rumor about turning off cell phones on October 4th to avoid activating the Marburg virus and turning people into zombies.
- Clarifying that the Emergency Broadcast System (EBS) doesn't exist anymore, but the emergency alert system will be tested the following day between 2:20 pm and 2:50 pm Eastern.
- Advising those with multiple phones for safety reasons to power off any secondary phones during the test to avoid issues.
- Emphasizing the importance of turning off phones for safety in specific situations even if the sound might come through on silent mode.
- Noting the misinformation about vaccines and the importance of protecting oneself.
- Mentioning two scientists receiving the Nobel Prize for their work on mRNA and saving thousands of lives.
- Stating that despite conspiracy theories being useless, they help spread the message to turn off phones to avoid potential attacks.

# Quotes

- "Turn off your phone tomorrow, not because zombies, but for safety."
- "Two scientists awarded the Nobel Prize for saving lives."
- "Conspiracy theories may be useless, but they spread the message of safety."

# Oneliner

Beau addresses rumors of a zombie apocalypse through a social media post, warns about misinformation on vaccines, and advises turning off secondary phones during an emergency alert system test for safety.

# Audience

Phone users

# On-the-ground actions from transcript

- Power off secondary phones during emergency alert system test (implied)
- Spread the message of phone safety during the test (implied)

# Whats missing in summary

Importance of staying informed and cautious during times of misinformation.

# Tags

#EmergencyAlertSystem #ZombieApocalypse #PhoneSafety #Vaccines #Misinformation