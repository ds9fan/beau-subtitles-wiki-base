# Bits

Beau says:

- Receives screenshots with stories of unwarranted confidence in surviving extreme situations.
- Examples include a man believing he could survive on a sunken sub, another thinking he could fight a grizzly bear with a pocket knife, and someone feeling they could have survived the Titanic sinking.
- Expresses admiration for the unearned confidence that some have, particularly middle-class white American men.
- Wishes people worldwide had such confidence to chase their dreams and make the world a better place.
- States the importance of confidence in pursuing achievable dreams rather than surviving the unsurvivable.
- Notes that this level of confidence is necessary to pursue actual dreams.
- Acknowledges the deep-seated confidence found in some individuals but deems it necessary for achieving goals.
- Encourages individuals who doubt themselves to adopt the confidence of a middle-class white American man on Twitter to go after their dreams and change the world.
- Emphasizes that sometimes all it takes to make a difference is the confidence to start.
- Concludes by suggesting that despite the confidence boost, one wouldn't have survived on the sub mentioned in the screenshots.

# Quotes

- "Pretend that you are a middle-class white American man on Twitter and just let that confidence wash over you."
- "Go change the world because a lot of times it just takes the confidence to start."
- "You wouldn't have survived on that sub."

# Oneliner

Beau encourages adopting unwarranted confidence to pursue dreams and change the world, contrasting it with survival odds in extreme situations.

# Audience

Dreamers and world-changers.

# On-the-ground actions from transcript

- Embrace unwarranted confidence in pursuing dreams (suggested).
- Start with the confidence needed to make a difference in the world (suggested).

# Whats missing in summary

The full transcript dives deeper into the concept of unwarranted confidence and its role in pursuing achievable dreams compared to surviving extreme situations.

# Tags

#Confidence #Dreams #MiddleClass #UnachievableSurvival #ChangeTheWorld