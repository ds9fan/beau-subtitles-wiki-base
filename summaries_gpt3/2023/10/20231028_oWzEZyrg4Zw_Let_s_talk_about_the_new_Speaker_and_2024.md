# Bits

Beau says:

- Addresses concerns about the U.S. House of Representatives and the new speaker in relation to the 2024 election.
- Mentions worries about trusting the new speaker with American democracy due to fears of another attempted coup.
- Credits Joe Manchin and Susan Collins for passing reforms at the end of 2022 that have made it more difficult to challenge state election results.
- Explains that the process to challenge election results now requires a larger number of House representatives, making it harder to attempt a coup.
- Points out that while the possibility of another attempted coup exists, it is now much less likely due to increased preparedness within government institutions.
- Emphasizes that the motivations behind the previous coup attempt, to keep someone in power, are not present this time.
- Concludes by expressing optimism about the decreased odds of a successful disruption compared to the past.

# Quotes

- "Can they do what they did last time, only this time be more committed?"
- "There's always a chance, you know, never say it can't happen here because it absolutely can."
- "The odds of it being successful are even lower."
- "It's also worth remembering that last time the reason they did it was to keep somebody in power."
- "Y'all have a good day."

# Oneliner

Beau addresses concerns about the U.S. House, the new speaker, and the 2024 election, expressing optimism but acknowledging the possibility of another attempted coup.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Stay informed and engaged with political developments (exemplified)
- Support and advocate for electoral reforms to strengthen democracy (exemplified)
  
# Whats missing in summary

Insights on specific reforms passed by Joe Manchin and Susan Collins in 2022. 

# Tags

#HouseOfRepresentatives #Speaker #2024Election #CoupAttempt #Democracy