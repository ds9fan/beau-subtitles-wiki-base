# Bits

Beau says:

- Addressing independents and moderate Republicans who are feeling alienated by the current state of the Republican Party.
- Pointing out that the moderate Republicans have lost power within the party and the party is moving further towards authoritarianism.
- Mentioning that moderate Republicans either endorsed the new Speaker's views or were powerless to stop them.
- Describing how the Republican Party has shifted right to the point of overt authoritarianism.
- Emphasizing that there is no place for moderates within the current Republican Party.
- Noting that moderate Republicans have compromised so much that there seems to be no way back for them.
- Comparing moderates in the party to a tiger that cannot be turned vegetarian by feeding it steak.
- Warning independents and moderate conservatives that the Republican Party does not represent them anymore.
- Urging individuals to realize that Trumpism has infected the Republican Party and that the far-right faction is still in control.
- Stating that the purpose of the events of January 6th was to ensure that the voices and votes of moderates and independents do not matter in the party's decisions.

# Quotes

- "The Republican Party has no place for you anymore."
- "The inability of moderate Republicans to alter the conversation shows that the Republican party as you knew it is gone."
- "They don't represent you and it will probably be a long time before they will."
- "You're a rhino. You're not conservative enough."
- "They want to rule you, and they've shown that they'll use just about any means to do it."

# Oneliner

Beau warns independents and moderate Republicans that the Republican Party has shifted too far right, leaving no space for them, as Trumpism continues to control and suppress their voices and votes.

# Audience

Independents, Moderate Republicans

# On-the-ground actions from transcript

- Join or support political movements or parties that better represent your beliefs (implied).
- Vote strategically in elections to support candidates who uphold moderate and independent values (implied).

# Whats missing in summary

The emotional impact and urgency conveyed by Beau's message is best experienced by watching the full transcript.

# Tags

#RepublicanParty #Moderates #Independents #Trumpism #Authoritarianism