# Bits

Beau says:

- Talks about recordings of an Australian billionaire named Pratt discussing Trump.
- Pratt shared classified information from Trump.
- Questions about potential indictment for Trump and using the recordings.
- Indicates that there isn't a law against sharing information as president.
- Notes that anything shared by Trump while president is within his rights.
- Suggests the potential use of the recordings for credibility.
- Emphasizes the irresponsibility but lack of legal consequences.
- Stresses the importance of electing someone who can maintain confidentiality.

# Quotes

- "Well, howdy there, internet people, it's Beau again."
- "The safeguard against this is supposed to be the American people understanding that they should probably not elect somebody who doesn't know how to keep their mouth shut."

# Oneliner

Beau explains recordings of Pratt sharing classified info from Trump, indicating no legal consequences but stressing the importance of electing individuals who can maintain confidentiality.

# Audience

Political observers

# On-the-ground actions from transcript

- Elect individuals who value confidentiality (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the implications of recordings where classified information was shared, focusing on the lack of legal consequences for Trump and the importance of choosing leaders who prioritize confidentiality.

# Tags

#Trump #Pratt #ClassifiedInformation #Indictment #Confidentiality