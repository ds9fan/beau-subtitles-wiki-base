# Bits

Beau says:

- Explains the strategy behind irregular conflict and unconventional war.
- Addresses the questions of what the small force aims to achieve and why the big force should show restraint.
- Notes that the small force seeks to provoke an overreaction from the big force to generate sympathy and support.
- Emphasizes that in this type of conflict, violence is part of a PR campaign.
- Argues that politically realigning the small force is more effective than using massive force.
- Provides historical examples like the Boston Massacre and Bloody Sunday to illustrate the effectiveness of restraint.
- Urges giving peace a chance in resolving such conflicts.
- Advocates for bringing opposing forces to the table instead of crushing them.
- Mentions the existence of literal manuals outlining these strategies.
- Points out the cyclical nature of conflict escalation and the ineffectiveness of continual back-and-forth responses.

# Quotes

- "Give peace a chance when you're talking about this kind of conflict, that's how you win."
- "The goal should be to bring them to the table, to bring them out. That does work."
- "Those who made that decision, those who decided to deploy that strategy, I'm willing to bet they're somewhere safe."
- "It's a PR campaign with violence. Nothing more."
- "When they were brought to the table, is when things calm down."

# Oneliner

In irregular conflict, provoke sympathy through overreaction, and prioritize peace talks over crushing opposition.

# Audience

Conflict analysts, peace advocates.

# On-the-ground actions from transcript

- Study and understand the strategies behind irregular conflict (implied).
- Advocate for peaceful resolution and diplomacy in conflicts within your community (implied).

# Whats missing in summary

In-depth analysis of historical examples and the importance of restraint in conflict resolution.

# Tags

#IrregularConflict #Peacebuilding #Diplomacy #ConflictResolution #Strategy