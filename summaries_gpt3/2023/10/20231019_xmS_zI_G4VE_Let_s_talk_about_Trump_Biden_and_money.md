# Bits

Beau says:

- Analyzing third quarter fundraising numbers for Biden and Trump from July 1st to September 30th.
- Biden raised $71 million, while Trump raised $45.5 million during this period.
- Speculation arises on whether Biden's fundraising advantage indicates his potential victory.
- Biden's $71 million and Trump's $45.5 million should not be directly compared due to different contexts.
- Trump is still in an active primary, unlike Biden who is backed by the DNC.
- Trump's fundraising is not heavily influenced by controversial issues like witch hunts.
- The focus should shift from how much they raised to cash on hand.
- Biden has $91 million cash on hand, while Trump has $37.5 million.
- Having more cash on hand positions Biden better for the future but doesn't guarantee victory.
- Cash on hand is a more critical number for predicting campaign sustainability and success.

# Quotes

- "You need to look at what's going out as well."
- "That's the number that can tell you a little bit more about what's going on."
- "Cash on hand is the number that matters."
- "Biden is off to a pretty healthy lead."
- "A lot of things can change."

# Oneliner

Analyzing third quarter fundraising numbers: Biden raised $71 million, Trump $45.5 million, but cash on hand is the key indicator for campaign sustainability and success.

# Audience

Campaign strategists

# On-the-ground actions from transcript

- Compare cash on hand for political campaigns (suggested)
- Stay informed about fundraising numbers and cash on hand of political candidates (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Biden's and Trump's fundraising numbers and the significance of cash on hand in predicting campaign success.