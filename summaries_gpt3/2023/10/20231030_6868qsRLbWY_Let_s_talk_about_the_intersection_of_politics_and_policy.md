# Bits

Beau says:

- Explains the analogy of foreign policy as a poker game where everyone cheats, representing the power dynamics between countries.
- Addresses the intersection of foreign policy and domestic politics, focusing on how politicians prioritize issues based on electability rather than public interest.
- Points out the inverted nature of domestic politics where politicians dictate what's significant rather than listening to constituents.
- Emphasizes that politicians prioritize foreign policy moves that can secure votes, resulting in a disconnect between public awareness and actual decisions made.
- Criticizes the expectation for politicians to have pure motivations in foreign policy, stating that their actions are self-serving and aimed at re-election.
- Urges viewers to understand the pragmatic, power-driven nature of foreign policy instead of seeking moral absolutes.
- Mentions specific examples like Iran to illustrate how historical events and propaganda shape current foreign policy decisions.
- Concludes by stressing the importance of grasping the realpolitik behind foreign policy to enact meaningful change.

# Quotes

- "Foreign policy, it's about power. How do they get it? It's a poker table. Countries are the players and they slide cards to each other. Everybody's cheating."
- "Politicians prioritize foreign policy moves that can secure votes, resulting in a disconnect between public awareness and actual decisions made."
- "For some reason, when it goes to foreign policy, we expect their motivations to be pure. They're not. They're self-serving."
- "It's about power coupons. When it crosses over into domestic politics, it's still about power."
- "It's never about morals."

# Oneliner

Beau breaks down the power dynamics of foreign policy, revealing how politicians prioritize issues for votes rather than public interest, urging viewers to understand the pragmatic nature to drive real change.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Understand the pragmatic, power-driven nature of foreign policy (implied)
- Advocate for transparency and accountability in foreign policy decisions (implied)
- Educate others on the intricacies of how foreign policy intersects with domestic politics (implied)

# Whats missing in summary

The full transcript provides a comprehensive breakdown of the intersection between foreign policy and domestic politics, urging viewers to critically analyze the motivations behind political decisions and advocating for a deeper understanding of realpolitik in shaping global dynamics.

# Tags

#ForeignPolicy #Politics #PowerDynamics #Realpolitik #PublicInterest