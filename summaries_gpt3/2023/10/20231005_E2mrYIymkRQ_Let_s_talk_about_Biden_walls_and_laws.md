# Bits

Beau says:

- Biden administration used executive power to clear the way for more border wall construction in a specific area.
- Biden did not reverse course on the wall, but the appropriations from Congress in 2019 mandated the continuation.
- Two political arguments exist for why Biden delayed action on the wall: wanting to rescind appropriations and creating distance from campaign promises.
- Actions, not statements, should be considered when determining intent, such as the auctioning off of unused border wall segments by the Biden administration in August.
- The border wall expansion under Biden is concerning due to its negative impact on the environment and wildlife migration.
- There are calls for real immigration reform and addressing the root causes leading people to flee their countries.
- The focus should be on helping countries address internal issues rather than intervening in their affairs.
- The need for immigration reform is urgent, and simply building walls is not a sustainable solution.
- It's vital to understand the reasons behind migration and work towards solutions that address the root causes effectively.
- Public awareness and acknowledgment of ineffective border policies and the importance of real reform are necessary.

# Quotes

- "Cowering behind a wall is not the answer."
- "There needs to be immigration reform, real immigration reform."
- "The real answer here is to help them address the issues."
- "You need to think about what it would take, how bad things would have to be for you to give up everything that you know."
- "The easiest way to do that is to stop intervening in their countries when it comes to their votes."

# Oneliner

Biden's delayed action on the border wall raises questions about intent, pointing towards the need for real immigration reform and addressing root causes of migration.

# Audience

Policy Advocates, Activists

# On-the-ground actions from transcript

- Advocate for real immigration reform by contacting policymakers and participating in advocacy groups (implied).
- Support organizations working towards addressing the root causes of migration in countries of origin (implied).

# Whats missing in summary

The full transcript dives deeper into the political maneuvering behind Biden's actions regarding the border wall and the potential impact on migration and the environment.

# Tags

#Biden #BorderWall #ImmigrationReform #PoliticalManeuvering #RootCauses #Migration