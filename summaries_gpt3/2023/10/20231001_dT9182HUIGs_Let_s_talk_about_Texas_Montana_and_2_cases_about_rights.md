# Bits

Beau says:

- Texas and Montana cases aren't linked, but the legislation topics and tactics are similar.
- Texas legislature passed a bill under the guise of protecting kids from drag shows.
- The goal was to provoke outrage towards a marginalized group to manipulate voters.
- The court found that the Texas law could infringe on constitutionally protected activities like cheerleading and dancing.
- Authoritarian governments trick people by blaming out groups, passing laws that restrict rights.
- Montana passed a law banning gender-affirming care, claiming it's to protect kids.
- The judge in Montana called out the legislature for being disingenuous in their intentions.
- Legislatures use fear tactics to pass laws that other people and restrict rights.
- If authoritarian legislatures target marginalized groups, they're also eroding everyone's rights.
- It's vital for people in states with such legislatures to understand the impact on their own rights.

# Quotes

- "When you take away people's rights, understand you're signing away your own."
- "If authoritarian legislatures target marginalized groups, they're also eroding everyone's rights."

# Oneliner

Texas and Montana cases show how legislatures manipulate fear to pass laws that erode rights for all.

# Audience

Voters, Community Members

# On-the-ground actions from transcript

- Question legislative intentions and demand transparency (implied)
- Support organizations working to protect marginalized communities' rights (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of how fear tactics and manipulation are used to pass laws that erode rights for all individuals.

# Tags

#Legislation #Rights #Authoritarianism #FearTactics #Community