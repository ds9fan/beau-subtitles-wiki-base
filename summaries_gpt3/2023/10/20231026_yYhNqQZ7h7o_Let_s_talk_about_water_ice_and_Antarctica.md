# Bits

Beau says:

- Talking about West Antarctica, water, and ice, with recent information suggesting a loss of control over the West Antarctic ice shelf.
- Regardless of efforts to control or reduce emissions, rapid decline is expected for the rest of the century.
- Even ambitious climate goals won't prevent the decline, leading to inevitable consequences.
- When the ice melts, the water goes into the ocean, raising concerns about sea level rise.
- The potential impact could be up to 5.3 meters, equivalent to around 17 feet for Americans.
- Recommends using a sea level rise viewer by Noah to visualize the potential effects of rising sea levels.
- Moving the slider to simulate rising sea levels shows the potential impact on cities like Miami, Charleston, San Francisco, and parts of New York.
- Infrastructure to accommodate rising sea levels, even at just 10 feet, will be a significant challenge.
- The process of rising sea levels is uncertain, but preparing for a 10-foot rise could take around 80 years.
- Climate change is a matter of national security and economic viability, not something that can be ignored.

# Quotes

- "Climate change is a matter of national security."
- "This should be a campaign issue in every election, everywhere, forever."
- "This is bad news."
- "It's just a thought, y'all have a good day."

# Oneliner

Beau talks about the loss of control over the West Antarctic ice shelf, leading to inevitable consequences like rising sea levels and the urgent need to address climate change as a matter of national security and economic viability.

# Audience

Climate advocates, policymakers

# On-the-ground actions from transcript

- Use the sea level rise viewer by Noah to understand the potential impact of rising sea levels (suggested).
- Recognize climate change as a matter of national security and economic viability, advocating for it to be a campaign issue in every election (implied).

# Whats missing in summary

Visuals and interactivity from the sea level rise viewer by Noah are missing in the summary.

# Tags

#Antarctica #ClimateChange #SeaLevelRise #NationalSecurity #CampaignIssue