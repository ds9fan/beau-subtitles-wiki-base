# Bits

Beau says:

- Beau introduces the topic of foreign policy intrigue between the United States and China, focusing on a former army sergeant named Joseph Schmidt.
- Schmidt allegedly went to great lengths to try to get himself recruited by Chinese intelligence after separating from the army in early 2020.
- Schmidt's unconventional approach involved sending an email expressing his desire to move to China, share information as an interrogator, and meet in person to talk about it.
- Despite concerns about discussing sensitive information over email, Schmidt continued his attempts, including searching for information on treason extradition and creating a document for the Chinese government stored in the cloud.
- The recruitment attempt lasted for some time, but it remains unclear if Schmidt ever made contact with Chinese intelligence or had a meeting.
- Upon returning to San Francisco, Schmidt was arrested by the federal government on charges of retention and attempting to deliver, suggesting he may not have successfully made contact with Chinese intelligence.
- Beau humorously speculates on potential future books and a spy comedy made in China about Schmidt's failed recruitment attempts.
- Beau questions the effectiveness of the US Army's intelligence training program, suggesting that more guidance could have prevented Schmidt's actions.

# Quotes

- "I am a United States citizen looking to move to China."
- "I have a current top secret clearance and would like to talk to someone from the government to share this information with you if that is possible."
- "Please contact me at your earliest convenience. I can set up a time to meet with you."

# Oneliner

Former army sergeant attempts unconventional recruitment by Chinese intelligence, leading to his arrest upon returning to the US, raising questions about intelligence training programs.

# Audience

Foreign policy observers

# On-the-ground actions from transcript

- Contact relevant authorities if you suspect any suspicious activities related to espionage (implied)

# Whats missing in summary

Insight into the potential consequences of failed espionage attempts.

# Tags

#ForeignPolicy #Espionage #USChinaRelations #IntelligenceTraining #Arrest