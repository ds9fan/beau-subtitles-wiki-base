# Bits

Beau says:

- Calls out the lack of coverage on disturbing claims made against Trump during his presidency.
- Mentions that Trump's derogatory statements towards military personnel barely registered in the news.
- Quotes attributed to Trump about military POWs, wounded, and fallen soldiers are brought up.
- John Kelly, Trump's longest-serving Chief of Staff, speaks out about Trump's disrespectful remarks towards military personnel.
- Kelly's statements could have been a career-ender in any other political climate.
- Despite the gravity of Kelly's claims, it received minimal media attention.
- Kelly's acknowledgment of these events on the record may help sway Trump supporters who are still under his spell.
- Beau suggests that Kelly's confirmation of Trump's statements may help people see the former president's true character.
- Emphasizes the importance of acknowledging and addressing these concerning statements made by a former high-ranking official.
- Encourages viewers to seek out videos or information regarding Kelly's statements for further understanding and awareness.

# Quotes

- "A person that thinks those who defend their country in uniform or are shot down or seriously wounded in combat or spend years being tortured as POWs are all suckers because there is nothing in it for them."
- "God help us."
- "To my knowledge, this is the first time Kelly has acknowledged any of this on the record."
- "For those people who are in your circles, who are still under the spell of Trump, this might be something that would help."
- "It might be useful to show that that loyalty is not returned."

# Oneliner

Despite minimal coverage, Trump's disrespectful remarks towards military personnel are confirmed by his longest-serving Chief of Staff, potentially changing perspectives on the former president.

# Audience

Trump supporters, concerned citizens

# On-the-ground actions from transcript

- Share videos or information about John Kelly's statements with Trump supporters (suggested)
- Use Kelly's confirmation to help sway individuals still supportive of Trump (suggested)

# Whats missing in summary

The full transcript provides a comprehensive breakdown of statements made against Trump regarding military personnel, urging further awareness and action.

# Tags

#Trump #Military #JohnKelly #Statements #Awareness