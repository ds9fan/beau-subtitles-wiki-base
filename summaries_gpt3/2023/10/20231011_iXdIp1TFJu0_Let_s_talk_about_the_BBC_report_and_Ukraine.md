# Bits

Beau says:

- Addressing the audience as "internet people" and discussing the BBC report and its credibility.
- Mentioning a report circulating on social media citing Bellingcat, an investigative journalism outfit.
- Emphasizing the clout of Bellingcat's reports regardless of personal opinions about them.
- Clarifying that Bellingcat did not make the specific claims attributed to them in the report.
- Exposing the manufactured nature of the BBC video and labeling it as an information operation.
- Pointing out that the misinformation spread was designed to provoke reactions and push a Russian talking point.
- Warning about the sophistication of misinformation tactics in the information space.
- Urging people to be cautious with news sources, especially on social media, and to seek confirmation before believing in a report.
- Stressing the potential harm misinformation can cause, not only in conflicts but also in elections and policymaking.
- Mentioning the European Commission's communication with Twitter regarding content moderation.

# Quotes

- "If you don't read the news, you're uninformed. If you get your news from Twitter, you're probably misinformed."
- "Assume it's not true at first. Wait until you get more confirmation."
- "We have to train ourselves for this."
- "It's something we need to get ready for."
- "It's just a thought, y'all have a good day."

# Oneliner

Beau warns about sophisticated misinformation tactics, urging caution with news sources, especially on social media, to prevent harm and misinformation.

# Audience

Information Consumers

# On-the-ground actions from transcript

- Verify news from multiple sources before believing or sharing (suggested)
- Be cautious with information on social media platforms (suggested)

# Whats missing in summary

The full transcript provides detailed insights into the spread of misinformation, the impact on societal beliefs and policies, and the need for critical thinking and vigilance in consuming news.

# Tags

#Misinformation #SocialMedia #NewsConsumption #Verification #CriticalThinking