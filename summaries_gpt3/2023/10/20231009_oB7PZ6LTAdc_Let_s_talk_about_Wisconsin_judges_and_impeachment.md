# Bits

Beau says:
- Justice Protasewicz's election shifted the Wisconsin Supreme Court to a more liberal majority.
- There is a challenge to the unfair voting maps in Wisconsin, with a 4-3 Supreme Court decision to hear the case.
- Republicans want Protasewicz to recuse herself, alleging bias due to her campaign stance on unfair maps.
- Despite pressure to recuse, Protasewicz has refused, citing that her decisions are bound by law, not personal preference.
- Republicans have threatened to impeach Protasewicz for not recusing herself, fearing fair maps will shift power balance.
- The extreme measures by Republicans to impeach a Supreme Court Justice showcase their authoritarian tactics.
- Fair maps threaten Republican control, leading them to politicize impeachment and disregard the will of the people.
- Republicans are willing to impeach to maintain power, showing a lack of concern for the democratic process in Wisconsin.
- Protasewicz's identification of unfair maps could lead to their striking down, a move feared by the Republican Party.
- The situation in Wisconsin exemplifies the high stakes involved in gerrymandering and power dynamics within the state.

# Quotes
- "During the next election, the voters of Wisconsin will make them pay for it."
- "If you have fair maps, that judge, she might give the state fair maps. We got to impeach her."
- "They're not hiding the reason they want to impeach her either."

# Oneliner
Justice Protasewicz's refusal to recuse herself from a case involving unfair voting maps in Wisconsin sparks Republican threats of impeachment and exposes authoritarian tactics to maintain power.

# Audience
Wisconsin voters

# On-the-ground actions from transcript
- Pay attention to local elections and hold representatives accountable for their actions (implied)

# Whats missing in summary
The full transcript gives a detailed insight into the power struggles and authoritarian tactics surrounding voting maps in Wisconsin.

# Tags
#Wisconsin #SupremeCourt #Gerrymandering #PowerStruggle #Impeachment