# Bits

Beau says:

- Trump endorsing Jordan for Speaker of the House is different and more consequential than his previous endorsements.
- Endorsing someone for Speaker of the House is a significant political move.
- Beau shares a story about an old man and a young guy, illustrating the consequences of a confrontation.
- If Jordan wins, Trump's support doesn't really gain him anything noteworthy.
- However, if Jordan loses, it signifies the Republican conference rejecting Trump's leadership.
- Trump has set himself up for a major loss by endorsing Jordan.
- There was no need for Trump to take this risk; he could have avoided it by staying silent.
- Trump's endorsement could potentially lead to negative consequences for him politically.
- The ads against Trump practically write themselves if Jordan loses.
- By endorsing Jordan, Trump has made a public statement to Republican members of the House that they need to pick Jordan.

# Quotes

- "Never fight an old man. If you win, you don't gain anything."
- "If he loses, it's a big deal. Nobody will let him forget it."
- "He gets nothing if he wins."
- "It's a huge, huge loss politically for Trump."
- "It's one of those things where you don't want to root for one side or the other."

# Oneliner

Trump's endorsement of Jordan for Speaker of the House could lead to significant political repercussions if Jordan loses, showcasing potential rejection of Trump's leadership within the Republican Party.

# Audience

Political observers

# On-the-ground actions from transcript

- Analyze and understand the potential political consequences of Trump's endorsement (implied).

# Whats missing in summary

The tone and nuances of Beau's storytelling and analysis can be best experienced by watching the full transcript. 

# Tags

#Trump #Endorsement #Consequences #SpeakerOfTheHouse #RepublicanParty