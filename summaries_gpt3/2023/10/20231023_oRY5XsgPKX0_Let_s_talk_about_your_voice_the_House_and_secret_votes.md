# Bits

Beau says:

- Talks about the dynamics of influence in the US House of Representatives.
- Mentions how representatives prioritize opinions based on financial contributions.
- Explains the impact of multiple voices versus individual engagement.
- Provides an example of a public versus secret vote scenario.
- Emphasizes the significance of public perception on representatives' decisions.
- Encourages individuals to recognize the power of collective action.
- Challenges the notion that contacting representatives is ineffective.
- Urges people to mobilize together to counterbalance wealthy influences.
- Raises awareness about the difference in representatives' votes in public versus secret settings.
- Stresses the importance of public engagement in influencing elected officials.

# Quotes

- "Your voice does matter."
- "You just need more of you."
- "It's not meaningless."
- "Your voice, it's not meaningless."
- "But it's not meaningless."

# Oneliner

Your voice matters, but collective action is needed to counter wealthy influences in politics.

# Audience

US constituents

# On-the-ground actions from transcript

- Mobilize with others to collectively reach out to representatives (exemplified)
- Encourage friends and community members to join in engaging with elected officials (exemplified)

# Whats missing in summary

The importance of public engagement in shaping political decisions.

# Tags

#USPolitics #Representatives #CollectiveAction #PublicEngagement #Influence