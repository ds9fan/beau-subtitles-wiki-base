# Bits

Beau says:

- American myth collides with individualism, creating a truly American monster.
- American zombie's origin roots in equatorial Africa and Haiti, where it was a victim, not a monster.
- Zombie myth evolved in the U.S., reflecting the American psyche's desire for individualism.
- The myth showcases the false notion of rugged individualism; survival requires teaming up with others.
- Reframe the zombie myth as a way to encourage emergency preparedness and community building.
- Small dedicated groups are the key to creating real change in the world.

# Quotes

- "One person can't fight off a zombie apocalypse. A network, a group can."
- "The myth showcases that the myth is wrong."
- "It shows that dedicated people teaming up well."

# Oneliner

American myth collides with individualism, creating the truly American monster of the zombie, reflecting the false notion of rugged individualism and the necessity of small dedicated groups for real change.

# Audience

Horror enthusiasts, community builders

# On-the-ground actions from transcript

- Prepare for emergencies like a zombie apocalypse to teach emergency preparedness and build community networks (suggested)
- Work in small dedicated groups to bring about real change (implied)

# Whats missing in summary

The full transcript delves into the origins of the American zombie myth and how it symbolizes the American psyche's desire for individualism, juxtaposed with the reality that true survival and change require collaboration and community building.

# Tags

#AmericanMyth #Zombie #Individualism #CommunityBuilding #EmergencyPreparedness