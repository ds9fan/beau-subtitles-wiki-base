# Bits

Beau says:

- Explains how fiction, especially science fiction, aims to create the suspension of disbelief to draw readers or viewers into the story.
- Notes the importance of getting details right in fiction to maintain the immersion and avoid jarring experiences that pull people out of the narrative.
- Science fiction writers face the challenge of balancing scientific accuracy with speculative elements that may become outdated.
- Mentions how authors use creative devices like magic or advanced alien technology to explain scientific concepts in fiction without risking factual inaccuracies.
- References the use of dark matter as fuel in the show Futurama as an example of choosing a fuel source that is not well understood to avoid disputes.
- Draws parallels between how fiction writers manipulate unknown concepts and how authoritarian figures exploit fear of the unknown to control and manipulate audiences.
- Suggests that conservative fearmongering often targets concepts or groups that are not well understood to spread misinformation and maintain control through fear.
- Points out that ignorance leads to fear, and when people are scared, they seek safety and are more easily manipulated by those who understand and exploit their fears.
- Observes that conservative leaders capitalize on topics like Critical Race Theory (CRT) and marginalized communities, which are often unfamiliar to their audience, to stoke fear and maintain control.
- Contrasts the forward-thinking nature of science fiction, which encourages looking ahead, with conservative fearmongering that aims to keep people in the past and resistant to change.

# Quotes

- "Ignorance really does lead to fear, especially if you have people motivated to keep people afraid because people who are scared, well, they want safety."
- "Suspension of disbelief, that willing avoidance of critical thinking. It can be used to help people see the future and what could exist."
- "Science fiction, it's about getting rid of fear. It's about looking ahead, not behind."

# Oneliner

Beau explains how fiction's suspension of disbelief can either inspire or control by manipulating the unknown, reflecting on conservative fearmongering tactics that exploit ignorance and unfamiliarity to maintain influence and spread misinformation.

# Audience

Storytellers, activists, educators

# On-the-ground actions from transcript

- Educate others on the tactics used in fearmongering to empower them to critically analyze information and resist manipulation (implied).
- Encourage community engagement and communication to foster understanding and combat fear of the unknown (implied).

# Whats missing in summary

The full transcript delves into the power dynamics of manipulation through fear and ignorance, exploring how storytelling techniques can be used positively or negatively to shape perceptions and influence behavior. Viewing the entire text provides a comprehensive understanding of how fear can be both a tool for control and a catalyst for change.

# Tags

#Fiction #ScienceFiction #Fearmongering #Conservatism #Education