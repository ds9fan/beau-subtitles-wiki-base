# Bits

Beau says:

- Describes a realization about social media, history, and future history sparked by a diner incident and a Twitter trend.
- Recounts a confusing interaction with his child about learning about lunch counters in school.
- Mentions the lack of consequences for those who poured food during lunch counter protests due to police indifference and lack of social accountability.
- Points out how future generations will have more detailed historical records due to social media documentation.
- Talks about the potential for social media to be a valuable tool for future historians in recording social movements.
- Raises the idea of future history books containing memes and detailed records of online posts.
- Emphasizes the generational impact of social media posts and the importance of being on the right side of history.
- Speculates on the challenges of creating an American mythology in the future with detailed historical records.
- Concludes by reflecting on the public nature of online posts and the scrutiny they will face over time.

# Quotes

- "What you post on Facebook echoes in eternity."
- "Every person is a public figure in that regard."
- "It's wild when you think about it."

# Oneliner

Beau talks about the intersection of social media, history, and future historical documentation, reflecting on the generational impact of online posts and the detailed records they create.

# Audience

History enthusiasts, social media users.

# On-the-ground actions from transcript

- Preserve digital content for future generations by documenting significant social movements online (exemplified).
- Encourage responsible online posting to ensure a positive generational legacy (implied).

# Whats missing in summary

The emotional weight of realizing the long-lasting impact of online posts and the detailed scrutiny they may face in the future.

# Tags

#SocialMedia #History #GenerationalImpact #DocumentingHistory #OnlineLegacy