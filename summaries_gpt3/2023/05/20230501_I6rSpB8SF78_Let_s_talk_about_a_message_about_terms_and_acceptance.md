# Bits

Beau says:

- Receives a message thanking him for supporting a community in Georgia, mostly by city folks.
- Urged to address terminology struggles among parents with trans kids, particularly older generations.
- Requested to convey that it's okay to not be perfect with terminology immediately.
- Acknowledged for being supportive despite past awkward moments and mistakes.
- Emphasized the importance of love and basic respect for the trans community.
- Encouraged to share the journey of improvement with terminology and support.
- Stressed the need for loved ones to continue showing support and respect.
- Expressed hope for a clear delivery of the message on understanding and acceptance.
- Mentioned the idea of finding a way to spread the message effectively.
- Ends with a message of positivity and a wish for a good day.

# Quotes

- "All we really want is for the people who loved us yesterday to love us today and to get the basic amount of respect anybody else gets."
- "People shouldn't be terrified at getting some term wrong. If you're trying, it's okay and we'll see it."
- "You were always supportive even when you were awkward and you have a huge trans following."
- "I hope you can come up with some way to deliver this well because I don't really know how to make this clear."
- "Y'all have a good day."

# Oneliner

Beau receives a message urging him to address terminology struggles among parents with trans kids, stressing the importance of support and respect, even through initial awkwardness.

# Audience

Supportive individuals.

# On-the-ground actions from transcript

- Find ways to effectively deliver messages of understanding and acceptance (suggested).

# Whats missing in summary

The emotional journey of learning and growing in support and understanding. 

# Tags

#Support #TransgenderCommunity #Acceptance #Terminology #Respect