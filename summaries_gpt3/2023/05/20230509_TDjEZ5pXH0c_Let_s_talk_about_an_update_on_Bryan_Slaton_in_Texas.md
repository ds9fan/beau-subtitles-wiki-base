# Bits

Beau says:

- Providing an update on Texas State Representative Brian Slaton and the findings of the House Committee regarding his activities and behavior.
- Slaton provided drinks to underage workers, leading to one needing Plan B with questions about consent.
- Slaton resigned two days after the findings were released, citing the need for a new representative to meet expectations.
- Despite the resignation, Representative Murr plans to call for a vote to expel Slaton from the Texas House of Representatives.
- Slaton is considered an officer of the state until a successor is elected and takes office, with the expulsion vote scheduled for that day.
- The expulsion vote is likely to succeed as Slaton has already resigned, making it the first expulsion in almost a hundred years in the Texas legislature.
- This situation is unique as it involves a Republican majority body holding another Republican accountable, a rare occurrence.
- Rumors suggest that Slaton was offered the chance to resign earlier but declined, leading to the investigation and subsequent events.
- There are questions about potential criminal activities as some findings by the committee were misdemeanors, possibly leading to law enforcement involvement.
- The outcome of the expulsion may cover these potential legal implications, but the exact course of action remains uncertain.

# Quotes

- "It's one of those cases that don't happen very often where you have a Republican majority body actually moving forward to hold another Republican accountable for their actions and their behavior."
- "There are questions about some of the activities because some of the things that the committee found were, in fact, crimes."
- "For those who were wondering if anything was going to come of it, yes, something absolutely came of it, and apparently came of it pretty quickly."

# Oneliner

Texas State Representative Brian Slaton resigns after misconduct findings, faces potential expulsion from a Republican-majority body for providing drinks to underage workers.

# Audience

Legislative observers, accountability advocates

# On-the-ground actions from transcript

- Contact Texas legislators to express support for holding accountable those who violate ethical standards (implied)
- Stay informed about local political developments and hold elected officials accountable for their actions (implied)

# Whats missing in summary

Details on the specific findings of the House Committee's investigation into Brian Slaton's activities and behavior.