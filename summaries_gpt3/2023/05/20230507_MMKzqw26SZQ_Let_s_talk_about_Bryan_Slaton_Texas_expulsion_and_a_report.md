# Bits

Beau says:

- Texas State House committee is investigating Representative Brian Slayton.
- The report likely recommends his expulsion due to assisting young women under 21 with alcohol.
- Rumors suggest Slayton was encouraged to resign but did not.
- The committee found the young woman could not effectively consent to the alcohol consumption.
- Slayton allegedly tried to prevent his young staff from disclosing information.
- The House is expected to vote on his expulsion soon, a rare occurrence since 1927.
- The issue will likely dominate Texas headlines until resolved.

# Quotes

- "The report is apparently going to recommend his expulsion."
- "On top of all of that you know there's the alcohol thing, there's the abusive capacity, there's the official oppression."
- "It's worth noting that the last time somebody was expelled was 1927, almost a hundred years ago."

# Oneliner

Texas State House committee investigates Rep. Slayton, likely to recommend expulsion for providing alcohol to young women.

# Audience

Texans, lawmakers

# On-the-ground actions from transcript

- Contact local representatives or organizations to voice opinions on the expulsion (implied)

# Whats missing in summary

Full details on the specific allegations and evidence presented in the report.

# Tags

#Texas #StateHouse #BrianSlayton #Investigation #Expulsion