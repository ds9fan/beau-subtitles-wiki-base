# Bits

Beau says:

- Democratic lawmakers reintroduced legislation to expand the Supreme Court by four seats, bringing the total number of justices up to 13, the same as the number of federal appellate courts.
- Motivations for expansion include ethical questions, unique rulings not in line with history, and concerns about the current far-right majority retaining control until 2065.
- Recent polling shows low confidence in the Supreme Court, with only 18% having a great deal of confidence, and a third of the country having hardly any confidence.
- Altering the institution might be necessary to protect it, especially with repeated attacks on institutions by one political party.
- While Beau doesn't foresee the expansion passing soon, he believes many running in 2024 will have to address how they plan to vote on it.
- Beau suggests that candidates supporting expanding the Supreme Court may receive a positive response, especially considering the low confidence levels in the court.
- Despite generally disliking messaging votes, Beau sees expanding the Supreme Court as something that needs to be done and likely wanted by the majority of Americans.
- The issue could become a unifying campaign topic for the Democratic Party, energizing various demographics like supporters of gun control, reproductive rights, conservationists, and younger people whose rights are under attack.
- Beau believes that while the proposal may fail initially, it could motivate people to give the Democratic Party a majority in the House and Senate to eventually pass it.
- Beau concludes by mentioning the broken justice system and leaves with a thought for the day.

# Quotes

- "The justice system is broke."
- "This is something that honestly the Democratic Party needs."
- "A third of the country has hardly any confidence."
- "Those people who want reproductive rights would get behind that."
- "The odd thing is I don't think that's why it's being proposed."

# Oneliner

Democratic lawmakers reintroduce legislation to expand the Supreme Court by four seats amidst low public confidence and concerns about the far-right majority's control until 2065, aiming to address ethical issues and unique rulings.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Mobilize support for expanding the Supreme Court through community outreach and education (implied)
- Advocate for reforming the justice system to address issues of legitimacy and public confidence (implied)

# Whats missing in summary

The full transcript provides a nuanced understanding of the motivations behind expanding the Supreme Court and its potential implications for American politics.