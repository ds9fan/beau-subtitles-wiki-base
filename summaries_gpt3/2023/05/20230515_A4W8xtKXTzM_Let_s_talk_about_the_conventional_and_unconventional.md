# Bits

Beau says:

- Explains the difference between conventional and unconventional forces in the context of the US military.
- Points out that unconventional forces are more capable of degrading US capabilities in conflict.
- Attributes the increased capability of unconventional forces to advancements in technology.
- Gives an example of drone technology and its impact on battlefield awareness for unconventional forces.
- Mentions the abundance of open-source intelligence available to the average person.
- Stresses that technology has provided unconventional forces with tools for surprise attacks.
- Talks about the increased lethality of individual soldiers, regardless of being conventional or unconventional.
- States that unconventional forces can keep fighting until the political resolve of the major power breaks.
- Notes that major powers often fail to adapt their military doctrine to effectively combat unconventional forces.
- Concludes that the US military may struggle more against unconventional forces than against conventional forces like China or Russia.

# Quotes

- "It's really, it kind of boils down to two things."
- "The one thing that is a determining factor."
- "The United States has a better chance of going toe-to-toe with China or Russia in achieving a decisive victory."
- "The technology, the information awareness that the unconventional force has access to today."
- "It's really hard for a major power to combat that."

# Oneliner

Beau explains why unconventional forces pose a greater challenge for major powers due to technological advancements and information access, making them harder to combat effectively.

# Audience

Military analysts, policymakers

# On-the-ground actions from transcript

- Analyze and adapt military strategies to effectively combat unconventional forces (implied)
- Utilize open-source intelligence for informed decision-making in conflict situations (implied)
- Build relationships with local populations to prevent sympathy towards unconventional forces (implied)

# Whats missing in summary

In-depth analysis and examples showcasing the impact of technological advancements on the capabilities of unconventional forces.

# Tags

#Military #Technology #UnconventionalWarfare #Intelligence #Conflict