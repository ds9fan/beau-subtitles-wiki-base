# Bits

Beau says:

- Claims have surfaced about Rudy Giuliani, overshadowed by a recent report and media frenzy.
- Allegations from a 70-page complaint filed by a former employee include being promised a million dollars annually but receiving significantly less.
- An allegation suggests Giuliani and Trump were selling pardons for two million dollars.
- A whistleblower named John Kariakou reached out through Giuliani to obtain a pardon, with an associate quoting a $2 million cost.
- The complaint includes allegations of audio recordings of Giuliani using sexist, racist, and anti-Semitic language.
- The suit filed against Giuliani is for $10 million.
- Giuliani has denied all allegations and plans to seek legal remedy.
- The wide range of allegations makes this case particularly interesting.
- Media attention may resurface once there is movement on the case.
- Beau hints at the need to be prepared for the allegations and suggests reading up on them rather than just listening.


# Quotes

- "Allegations include being promised a million dollars annually but receiving significantly less."
- "The suit filed against Giuliani is for $10 million."
- "Media attention may resurface once there is movement on the case."


# Oneliner

Claims against Rudy Giuliani emerge, including selling pardons, with a lawsuit for $10 million, sparking potential media interest.


# Audience

Media consumers


# On-the-ground actions from transcript

- Read up on the allegations against Giuliani (suggested)
- Stay informed about the case developments (suggested)


# Whats missing in summary

Full details and nuances of the allegations against Rudy Giuliani can be best understood by watching the full video.


# Tags

#RudyGiuliani #Allegations #MediaAttention #LegalRemedy #Whistleblower