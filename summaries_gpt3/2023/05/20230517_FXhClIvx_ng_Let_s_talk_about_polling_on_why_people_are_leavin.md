# Bits

Beau says:

- Explains about polling data indicating a significant increase in the percentage of people changing their religious affiliation.
- Notes that 24% of individuals in the United States have changed their beliefs over their lifetime, a 50% increase from 2020.
- States that the primary reason for this change is losing faith, accounting for 56% of cases.
- Points out that negative teachings or treatment of the LGBTQ community is the second most common reason, at 30%.
- Mentions that 17% switched churches or congregations due to becoming too political.
- Stresses the importance of the separation of church and state for both institutions.
- Observes that many are leaving churches because they find it easier to preach hate than love.
- Suggests that the decline in church attendance and contributions is linked to this shift.
- Raises the idea that preaching hate rather than love is driving people away from religious institutions.
- Concludes by hinting that being a good person is ultimately more cost-effective than spreading hate.

# Quotes

- "The reason the pews are becoming more and more empty is because a lot of people have decided that it is easier to preach hate rather than preach love."
- "There's numbers to back it up."
- "It's cheaper to be a good person."

# Oneliner

Beau analyzes polling data on changing religious beliefs, attributing shifts to losing faith and negative treatment of the LGBTQ community, revealing a significant increase in affiliation changes.

# Audience

Religious communities, LGBTQ advocates

# On-the-ground actions from transcript

- Reassess church teachings and treatment of marginalized groups (suggested)
- Support inclusive religious communities (implied)

# Whats missing in summary

A deeper exploration of the emotional impact on individuals leaving faith communities. 

# Tags

#ReligiousBeliefs #PollingData #LGBTQCommunity #ChurchAndState #CommunitySupport