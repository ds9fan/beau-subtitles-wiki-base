# Bits

Beau says:
- Explains the legislation HB 1147 in Texas that requires schools to provide bleeding control station training annually.
- Criticizes the legislation for aiming to turn students into combat medics rather than teaching basic first aid.
- Argues that the training proposed for third graders is unrealistic and too intense.
- Shares his personal experience of teaching his son first aid skills due to living far from a hospital.
- Challenges the effectiveness of the proposed training and suggests it cannot be the best solution.
- Emphasizes the glorification of violence in society and how it contributes to the issues being faced.
- Condemns the idea of teaching third graders combat medic skills while not addressing underlying societal problems.
- Calls out politicians for glorifying violence and contributing to the issue.

# Quotes
- "You're asking third graders to become combat medics. That's what this is for."
- "If you vote for this, you never get to talk about a book in a library again."
- "If you're a politician and you stand up there showing off your AR, you're a part of the issue even if you vote to teach them how to plug the holes."

# Oneliner
Proposed legislation in Texas aims to train third graders as combat medics, reflecting deeper societal issues and glorification of violence.

# Audience
Educators, policymakers, parents

# On-the-ground actions from transcript
- Advocate for realistic and age-appropriate emergency training in schools (implied)
- Challenge legislation that may not address the root causes of societal issues (implied)
- Prioritize nurturing creativity and understanding over glorification of violence (implied)

# What's missing in summary
The emotional weight and personal anecdotes from Beau's experience teaching his son first aid are best understood by watching the full transcript.

# Tags
#Texas #Legislation #CombatMedics #ViolenceGlorification #SocietalIssues