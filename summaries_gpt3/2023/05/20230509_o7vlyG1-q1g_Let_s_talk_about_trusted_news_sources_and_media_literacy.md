# Bits

Beau says:

- Shares insights from a YouGov poll on media organization trust levels, broken down by party affiliation.
- Top 10 most trusted media organizations include The Weather Channel, PBS, BBC, Wall Street Journal, Forbes, AP, ABC, USA Today, CBS, and Reuters.
- People trust media outlets based on how they impact them directly, such as money or accent.
- Fox ranks below Newsmax and OAN, indicating unfamiliarity with the latter two's accuracy records.
- Republicans generally distrust most media organizations unless they are clearly partisan or reinforcing their opinions.
- Republicans value feelings over facts, as shown by their widespread net distrust of media outlets.
- Beau suggests that even Republicans know the difference between opinion and objective news and should stop pretending otherwise.
- Beau encourages not treating opinions as facts, especially on issues where there is no real debate.
- Points out that Wall Street Journal and Forbes cater to specific groups and have a particular slant towards a competitive society.
- Beau calls for understanding and working towards a more cooperative society rather than a competitive one based on media biases.

# Quotes

- "People care about themselves and people can tell the difference between objective news and opinion if they want to."
- "Even Republicans, even those who pretend like they don't know the difference between opinion and objective news. They do."
- "Stop treating their opinions as facts."
- "It's pretty clear if you really sit down and look at this."
- "The majority of Americans trust these outlets more than the AP."

# Oneliner

Insights from a poll on media trust levels show Republicans' distrust, preference for opinions, and the need to differentiate facts from feelings.

# Audience

Media consumers

# On-the-ground actions from transcript

- Verify information from multiple sources before forming opinions (implied)
- Encourage critical thinking and fact-checking among peers (implied)
- Support media literacy programs in schools and communities (implied)

# Whats missing in summary

Exploration of how media biases impact societal cooperation and competition.

# Tags

#MediaLiteracy #TrustInMedia #Biases #FactsVsOpinions #SocietalImpact