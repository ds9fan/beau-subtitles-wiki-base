# Bits

Beau says:

- Exploring defenses of Trump after a decision in New York led to shocking revelations.
- Sought out right-wing social media reactions to understand their defenses.
- Shocked by two particular defenses focusing on Trump's ability to have any woman he wanted.
- Emphasizes that the behavior is not about attractiveness but about power.
- Both defenders happened to be fathers, which added a disturbing layer to their comments.
- Expresses horror at the thought of daughters not being believed based on looks or the man's wealth.
- Notes the impact of social media comments on relationships, especially with one's children.
- Criticizes the idea of valuing money as a reason to be with someone, especially for right-wing individuals.
- Acknowledges Trump's lingering influence even after leaving office.
- Warns about the permanence and consequences of words spoken online.

# Quotes

- "That's not why that occurs."
- "It's pathetic."
- "Trump, even out of office, even with as little power as he has, he still has a hold over people."
- "Not just your daughters, but other people in your life, they will never look at you the same way again."

# Oneliner

Exploring defenses of Trump reveals disturbing attitudes towards power and relationships, even among fathers, with a caution about the impact of online comments.

# Audience

Parents, online commentators

# On-the-ground actions from transcript

- Monitor and reconsider the impact of one's comments on social media (implied).
- Educate children on healthy values and relationships (implied).
- Foster open communication with children to address potentially harmful beliefs (implied).

# Whats missing in summary

The full transcript delves into the complex dynamics of defending Trump and the lasting effects of online commentary on personal relationships.