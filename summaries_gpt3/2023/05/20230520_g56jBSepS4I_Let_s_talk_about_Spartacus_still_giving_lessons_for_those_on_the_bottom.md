# Bits

Beau says:

- Addresses unity, economy, and Spartacus in response to a message received.
- Mentions the relationship between losing money and attracting investment.
- Talks about diverse workforce discouraging unions, referencing Jeff Bezos.
- Draws parallels between Spartacus' slave revolt and modern-day diversity.
- Argues that bigotry is bad for the economy in a capitalist society.
- Emphasizes that diversity does not stop unification, but bigotry does.
- Criticizes the exploitation of bigotry by those in power to keep people divided.
- Encourages unity across diverse backgrounds rather than falling for divisive tactics.
- Challenges the idea of having more in common with people of similar backgrounds rather than those in positions of power.
- Concludes with a message against falling for tactics that exploit bigotry.

# Quotes

- "Bigotry is bad for the economy. Period. Full stop. It always is."
- "Diversity doesn't stop people from unifying. Bigotry does."
- "Unions are diverse. Spartacus's army was diverse."
- "Those at the top will try to exploit bigotry to keep people divided."
- "You have more in common with the black guy down the road than with your white representative up in DC."

# Oneliner

Beau talks about unity, economy, and Spartacus, showcasing how diversity doesn't hinder unification, but bigotry does, warning against falling for divisive tactics.

# Audience

People seeking to understand the importance of unity and diversity in society.

# On-the-ground actions from transcript

- Challenge divisive rhetoric and encourage unity in your community (implied).
- Support diverse workplaces and advocate against discrimination (implied).

# Whats missing in summary

Importance of recognizing and rejecting attempts to exploit bigotry for political or social control.

# Tags

#Unity #Diversity #Bigotry #Economy #Exploitation