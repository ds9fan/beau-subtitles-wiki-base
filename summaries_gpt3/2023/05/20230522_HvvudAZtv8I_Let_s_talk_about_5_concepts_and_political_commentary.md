# Bits

Beau says:

- Exploring the concept of objectivity in journalism and commentary, acknowledging the challenge of true objectivity due to unconscious biases.
- Proposing five concepts to ponder on whether they are objectively good or bad, such as bigotry and authoritarianism.
- Asserting that denying people equal protection under the law, curtailing free speech, and favoring one religion over another are objectively bad.
- Questioning if objectivity means treating both political parties equally when one has embraced objectively bad principles.
- Arguing that objectivity does not equate to treating objectively bad actions as equal and showing favoritism to something detrimental.
- Emphasizing the importance of recognizing when one party's actions are objectively bad and not treating both sides as equal for the sake of false impartiality.

# Quotes

- "Bigotry is objectively bad."
- "Denying people equal protection under the law is bad."
- "Objectivity is not both sides in something that doesn't have two sides."
- "Treating it as an equal when it's not."
- "One party adopted a political platform and demonstrated through their actions that their route is objectively bad."

# Oneliner

Exploring objectivity in journalism and commentary, Beau challenges the notion of treating both parties equally when one adopts objectively bad principles.

# Audience

Journalists, commentators, readers

# On-the-ground actions from transcript

- Recognize and challenge objectively bad actions in political platforms (implied)
- Advocate for fairness and truth in reporting (suggested)

# Whats missing in summary

The full transcript delves into the nuances of objectivity in journalism and commentary, providing a critical perspective on treating objectively bad actions equally.

# Tags

#Journalism #Objectivity #Bias #PoliticalParties #Authoritarianism