# Bits

Beau says:

- The Harris Poll rates companies based on their reputation, with Tesla dropping from 11th to 62nd place.
- Speculation arises that Elon Musk's Twitter behavior may have influenced Tesla's reputation.
- Neuralink, a company Musk is associated with, has received FDA approval for human studies on brain chips.
- Despite controversy, Neuralink's technology has potential applications in healthcare and commercial sectors.
- Concerns are raised about Musk's personal perception impacting companies he's associated with.
- Musk's shift from being seen as a humanitarian to endorsing wild conspiracies may harm his reputation and affiliated companies.

# Quotes

- "There was a time not too long ago where he was kind of portrayed as somebody out to really better humanity."
- "And now he has tweets that dabble in the wilder conspiracies."

# Oneliner

The Harris Poll rates Tesla's reputation drop, prompting speculation on Elon Musk's influence and Neuralink's FDA approval for brain chips.

# Audience

Business analysts, tech enthusiasts

# On-the-ground actions from transcript

- Research Neuralink's developments and controversies (suggested)
- Stay informed about how public perception impacts companies (implied)

# Whats missing in summary

The full transcript provides more insight into Elon Musk's shifting public image and the potential consequences for companies he's associated with.