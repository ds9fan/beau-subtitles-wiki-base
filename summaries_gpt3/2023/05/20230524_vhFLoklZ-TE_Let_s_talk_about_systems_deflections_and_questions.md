# Bits

Beau says:

- Addressing a question that arises frequently: why do individuals with darker skin tones support fascist groups by displaying symbols like flags or tattoos?
- Drawing parallels between the acceptance of patriarchy and the acceptance of systems of oppression like white supremacy.
- Explaining how victims of oppressive systems can sometimes support and defend those systems due to fear of change and adherence to the status quo.
- Mentioning individuals like Enrique Tario and Nick Fuentes as names associated with fascist movements in the US.
- Noting that the concept of "whiteness" in the US is fluid and has evolved over time to include different ethnic groups for the benefit of maintaining power structures.
- Emphasizing that systems of oppression are a means for certain groups to gain and maintain control by dividing people into in-groups and out-groups.
- Stressing that the ultimate goal of oppressive systems is not to keep a specific group down but to consolidate power and authority for those in control.
- Pointing out the link between individuals supporting oppressive systems and the perpetuation of authoritarianism.
- Encouraging further exploration of racial hierarchies and systems of oppression beyond the US borders.
- Concluding with the idea that individuals supporting oppressive systems, regardless of their background, are not anomalies but rather common occurrences.

# Quotes

- "The system itself isn't about the system. The system is a means to an end, power, authority, authoritarianism."
- "When it comes to systems of oppression, the goal isn't actually to keep a certain group of people down. That's not the end."
- "It's not some historical anomaly. It occurs, and it occurs in other systems as well, all the time."

# Oneliner

Addressing the question of why individuals with darker skin tones support fascist groups, Beau draws parallels between the acceptance of patriarchy and systems of oppression like white supremacy, illustrating how victims of oppressive systems can perpetuate them.

# Audience

Social justice advocates

# On-the-ground actions from transcript

- Research racial hierarchies and systems of oppression beyond the US borders (suggested)
- Have open, honest dialogues with individuals who may unknowingly support oppressive systems (implied)

# Whats missing in summary

Exploration of how societal norms and the fear of change contribute to individuals supporting oppressive systems despite being victims of those systems.

# Tags

#Oppression #Fascism #Patriarchy #WhiteSupremacy #Authoritarianism