# Bits

Beau says:

- Provides techniques on avoiding falling for bad information and fact-checking oneself.
- Advises asking if information confirms existing beliefs and if it is sensationalized.
- Talks about using reverse image search to verify images in memes.
- Mentions the impact of AI on misinformation and fact-checking.
- Emphasizes verifying quotes and their context through Google.
- Warns about cherry-picking quotes and nut-picking to misrepresent groups.
- Advises checking statistics for accuracy, methodology, and context.
- Talks about linking unrelated events to create false narratives.
- Mentions the difference between correlation and causation.
- Encourages viewing memes as propaganda and retraining algorithms for quality information.

# Quotes

- "Figures don't lie, but liars certainly figure."
- "View every meme as propaganda."
- "Make sure there's something down below saying, hey this is bad."
- "Retrain your algorithm."
- "A string of coincidences. Nothing to see here."

# Oneliner

Beau provides techniques to avoid falling for misinformation, including verifying quotes, checking statistics, and viewing memes as propaganda.

# Audience

Social media users

# On-the-ground actions from transcript

- Comment on misinformation to slow its spread (suggested)
- Block outlets that consistently provide false information (exemplified)

# Whats missing in summary

In-depth series on fact-checking techniques and safeguarding against misinformation.

# Tags

#FactChecking #Misinformation #Propaganda #AvoidingFakeNews #DigitalLiteracy