# Bits

Beau says:

- North Carolina's governor vetoed a bill curbing reproductive rights, prompting Republicans to attempt an override.
- Republicans in North Carolina need every vote for the override; losing one vote could sway the decision.
- The bill in question is convoluted, with varying restrictions on reproductive rights at different stages of pregnancy.
- Only 35% of North Carolinians support the bill, indicating widespread opposition to additional restrictions.
- Majority in North Carolina either support expanding reproductive rights or maintaining the current status quo.
- Republicans are accused of prioritizing rule over representation, going against the will of the people.
- Despite low public support, Republicans plan to override the veto, disregarding constituents' desires.
- People in North Carolina are urged to convince just one Republican representative to prevent the override.
- Pressure from Republican Party leadership may influence representatives to vote against constituents' wishes.
- Civic engagement is encouraged to influence representatives and uphold the will of the people.

# Quotes

- "One vote. People in North Carolina need to convince one to break ranks, one to honor their campaign promise, one to do what polling suggests they should do."
- "Republicans deciding that they're going to rule rather than represent."
- "Don't actually listen to the people who put you in office. Don't enact their will."

# Oneliner

Republicans in North Carolina push to override governor's veto on a bill curbing reproductive rights despite low public support, urging civic engagement for change.

# Audience

North Carolinians

# On-the-ground actions from transcript

- Convince one Republican representative to prevent the override (implied)
- Engage in civic activities to influence representatives (implied)

# Whats missing in summary

The full transcript provides detailed insight into the political dynamics and public sentiment surrounding the vetoed bill in North Carolina.