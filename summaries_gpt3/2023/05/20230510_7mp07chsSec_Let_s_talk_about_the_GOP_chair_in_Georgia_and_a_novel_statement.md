# Bits

Beau says:

- Georgia's GOP chair, David Schaefer, is using a novel defense to avoid accountability for his actions related to the alternate electors in Georgia.
- Schaefer's lawyers claim he shouldn't be charged because he was following detailed legal advice, including advice from former President Trump's attorneys.
- The defense argues that Schaefer's actions were taken in conformity with legal counsel, eliminating the possibility of criminal intent or liability.
- While such a defense might work in some cases, Beau questions its effectiveness in this specific situation of far-right power grabs.
- The defense relies on the premise that intent is a key element in proving criminal actions, suggesting that if intent is lacking, it could be challenging to establish guilt.
- Beau speculates on potential complications if evidence surfaces suggesting knowledge of wrongdoing despite legal advice.
- Despite the defense's argument, Beau expresses skepticism about its ironclad nature due to the secretive nature of the events.
- The effectiveness of the defense hinges on whether there is documented reassurance from legal counsel that Schaefer's actions were lawful.
- Beau concludes by acknowledging that the defense might hold up under scrutiny if backed by substantial evidence but suggests awaiting further developments.

# Quotes

- "I'm just not sure that it's ironclad."
- "When it comes to far-right power grabs, generally speaking, 'I was just following orders' doesn't go over well."
- "It's just a thought. Y'all have a good day."

# Oneliner

Georgia's GOP chair attempts to evade accountability by claiming reliance on legal advice, but the defense's efficacy remains uncertain amid skepticism and potential complications.

# Audience

Legal observers

# On-the-ground actions from transcript

- Question legal defenses (implied)
- Await further developments (implied)

# Whats missing in summary

Context on the potential legal implications and developments that may arise from the defense strategy.

# Tags

#Georgia #GOP #LegalDefense #CriminalIntent #Accountability