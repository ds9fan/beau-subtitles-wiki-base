# Bits

Beau says:

- Analyzing Donald Trump's promise to sign an executive order on his first day in office to deny automatic US citizenship to the children of illegal aliens.
- Trump's plan involves altering the US Constitution, specifically the 14th Amendment, through an executive order.
- The oath of office Trump took to uphold and defend the Constitution seems contradictory to his plan.
- Trump's disregard for the Constitution is evident in his promise to undermine it for his own power.
- Trump's focus is on scapegoating people and gaining power for himself, rather than upholding constitutional values.

# Quotes

- "He's promising to undo the US Constitution with an executive order on day one."
- "He absolutely would try because he doesn't care about the Constitution. He doesn't care about the country."
- "He has promised on day one to undermine the U.S. Constitution."

# Oneliner

Donald Trump plans to undermine the US Constitution by signing an executive order on his first day in office, showing his disregard for the country's foundational values and his pursuit of power through scapegoating.

# Audience

Voters, Constitution defenders

# On-the-ground actions from transcript

- Defend the US Constitution by advocating for its values and principles (implied).

# Whats missing in summary

The full transcript provides a deeper analysis of Trump's intentions and how they conflict with constitutional values, which can be better understood by watching the entire video.

# Tags

#DonaldTrump #USConstitution #Citizenship #ExecutiveOrder #Presidency