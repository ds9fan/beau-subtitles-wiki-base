# Bits

Beau says:

- Explains the message he received regarding the Texas Constitution and Satanist literature in classrooms.
- Clarifies that he did not insinuate but outright stated that if Texas allowed the Ten Commandments in class, Satanist literature will follow.
- Disputes the claim that the United States was founded as a Christian nation, referencing the 1797 Treaty of Tripoli.
- Addresses the misunderstanding about the Constitution protecting only Christian religions, outlining that it safeguards all religions.
- Distinguishes between the law of unintended consequences and cause and effect regarding religion in schools.
- Mentions a federal court decision in Pennsylvania allowing the after-school Satan Club alongside the Christian evangelical club.
- Notes similar cases in Virginia where the Satanic Temple prevailed in gaining access to schools for their club.

# Quotes

- "If that legislation goes forward, they put the Ten Commandments into the classroom, I assure you Satanist literature will be in there alongside it."
- "The United States was not founded as a Christian nation."
- "The Constitution protects the free exercise of all religions, any religion, not just yours."
- "Your law of unintended consequences is just a slippery slope fallacy. No, it's cause and effect."
- "Y'all have a good day. Thank you."

# Oneliner

Beau dismantles misconceptions about religion in schools, citing historical treaties and legal precedents while addressing fear-mongering around Satanist literature.

# Audience

Students, educators, activists.

# On-the-ground actions from transcript

- Support and advocate for inclusive religious education in schools (suggested).
- Stay informed about legal battles surrounding religious clubs in schools (suggested).

# Whats missing in summary

Detailed analysis of the historical context and legal implications surrounding religion in American schools.

# Tags

#Religion #Education #Constitution #Texas #Satanism