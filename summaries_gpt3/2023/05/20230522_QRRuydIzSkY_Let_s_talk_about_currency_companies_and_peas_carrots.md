# Bits

Beau says:

- Explains the backing of the US dollar and terms like soft colonialism.
- Addresses the supremacy of US corporations and how they outcompete others.
- Talks about legal mechanisms and incentives that benefit US corporations.
- Mentions the extraction of wealth through corporate colonization.
- Provides insights into the US defense industry's role in maintaining the dollar's backing.
- Acknowledges the not-so-ideal practices in the system and the need for understanding to bring change.
- Emphasizes the prevalence of such practices in the West.
- Calls for a shift towards cooperation over competition in economic systems.
- Offers a critical perspective on global economic structures and their implications.
- Encourages reflection on the current state of affairs and the potential for improvement.

# Quotes

- "We don't colonize with flags anymore. We colonize with corporate logos."
- "The US dollar is backed up by that 800 billion dollar a year defense industry."
- "It's dirty right? It's not really how we would want things."
- "The wealth is extracted from the states that we colonize with corporate logos."
- "It will stay this way until we move to a more cooperative rather than competitive."

# Oneliner

Beau explains the backing of the US dollar, supremacy of corporations, and soft colonialism, calling for a shift towards cooperation in economic systems.

# Audience

Economic Justice Advocates

# On-the-ground actions from transcript

- Challenge exploitative economic systems (implied)
- Advocate for fair trade practices (implied)
- Support policies that prioritize cooperation over competition (implied)

# Whats missing in summary

In-depth analysis of specific examples and historical contexts shaping current economic structures.

# Tags

#USdollar #CorporateSupremacy #SoftColonialism #EconomicJustice #Cooperation