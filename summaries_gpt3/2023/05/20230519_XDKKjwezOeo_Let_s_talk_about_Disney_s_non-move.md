# Bits

Beau says:

- Disney decided not to move forward with a project that involved putting 2,000 Imagineers in Florida, leading to significant economic implications.
- Speculation surrounds whether Disney's decision was influenced by political factors in Florida, but there is no concrete evidence to support this.
- The project cancellation will result in a loss of $200 million annually from the economy due to the high salaries of the Imagineers.
- The economic impact goes beyond just salaries, including the money spent by the company in the area and the ripple effects on local businesses.
- The decision not only affects the employees but also has repercussions on various other aspects like landscapers, truckers, and overall economic activity in the region.
- The long-lasting effects of this decision will result in billions of dollars not being pumped into Florida's economy.
- Similar situations, with less attention, occur frequently, impacting regions without making headlines.
- Beau mentions a personal connection to a situation where a project was diverted from Florida to Georgia, Minnesota, or Maine based on future political conditions.
- These decisions have substantial long-term impacts on economically disadvantaged areas, causing significant financial losses.
- Companies make decisions based on future-oriented policies, and regions that do not adapt risk losing out on profitable ventures.

# Quotes

- "It is cheaper to be a good person."
- "When Disney puts something somewhere, it stays."
- "The amount of money that is lost because of it is huge."
- "These decisions have long-term impacts for an area that is economically disadvantaged."
- "Companies that look towards the future, institutions that look towards the future, they're not going to want to be there."

# Oneliner

Disney's decision not to proceed with a project in Florida showcases the massive economic repercussions and long-lasting impacts on regions, reflecting the broader consequences of political climates on business decisions.

# Audience

Economic policymakers

# On-the-ground actions from transcript

- Advocate for policies that support future-oriented companies and attract profitable ventures (implied)
- Support economically disadvantaged areas facing substantial financial losses (implied)
- Stay informed about the economic impacts of business decisions influenced by political climates (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of how business decisions are influenced by political climates, resulting in significant economic repercussions for regions and underscoring the importance of future-oriented policies.

# Tags

#EconomicImpact #BusinessDecisions #PoliticalClimate #FuturePolicies #CommunityImpact