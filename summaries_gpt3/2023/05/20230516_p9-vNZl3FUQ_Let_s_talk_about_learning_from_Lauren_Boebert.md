# Bits

Beau says:

- Lauren Boebert misinterpreted a graphic on CNN showing US border encounters since Title 42 ended.
- Boebert criticized CNN for showing a surge on the graphic when the numbers actually dropped.
- The graphic was in reverse chronological order, causing confusion for viewers used to reading left to right, top to bottom.
- Beau suggests that the graphic may have been intentionally misleading by not following expected chronological order.
- He mentions how truncated bar graphs can manipulate perception by showing only a portion of the data.
- Beau points out that media outlets sometimes use graphics to stir up trouble or reinforce preconceived notions.
- Despite Boebert's criticism, Beau acknowledges that misinterpreting such graphics is common.
- He urges viewers to take the time to process information from graphics and not jump to conclusions.
- Beau advises checking if the information is presented in context and reading the details carefully.
- He warns about the danger of graphics being used to reinforce preconceived notions rather than present facts accurately.

# Quotes

- "How stupid do they think the American people are?"
- "When you are looking at graphics, there's a bunch of things that can be done to alter the perception of something."
- "Make sure you actually take the time to process the information."
- "It's being done to reinforce a preconceived notion and that is the real danger."
- "When you see the graphics as the talking heads are up there stating their position, make sure that you take the time to process it and read the days of the week."

# Oneliner

Beau points out how a misleading graphic on US border encounters sparked criticism from Lauren Boebert, shedding light on the importance of processing information from graphics carefully to avoid falling into traps of misinformation.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Verify the information presented in graphics by checking multiple sources (implied).
- Take time to process information from graphics and avoid jumping to immediate conclusions (implied).
- Read details carefully and ensure the context is accurate before forming opinions (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how graphics in media can manipulate perception and reinforce preconceived notions, urging viewers to be vigilant in processing information for accurate understanding.

# Tags

#Graphics #MediaLiteracy #Misinformation #CriticalThinking #USPolitics