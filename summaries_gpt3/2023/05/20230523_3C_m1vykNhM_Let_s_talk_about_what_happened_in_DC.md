# Bits

Beau says:

- A U-Haul crashed into the barricades in Lafayette Park near the White House around 10 p.m.
- Secret Service investigated the scene, and a robot was used to ensure the vehicle's safety.
- Authorities found a flag in the U-Haul during the search.
- Beau researched the incident but found right-wing commentators on Twitter spreading conspiracy theories.
- Beau questions the shift in the right-wing's stance towards certain symbols and ideologies.
- He recalls a time when politicians like George Bush Sr. distanced themselves from negative symbols and rhetoric.
- The Republican Party's defense of certain symbols and ideologies has drastically changed over time.
- Beau urges people to critically analyze their beliefs if they echo dangerous ideologies.
- He points out the evolving nature of political ideologies, especially the right-wing becoming more authoritarian.
- Beau concludes by noting that those defending certain symbols haven't changed amid political shifts.

# Quotes

- "If you ever find yourself in a situation where the behavior of somebody who has one of those flags reflects poorly on you, there were a lot of poor decisions that were made along the way."
- "The right wing in the United States has changed, and you can say that the left wing has changed too, that they've moved to more and more progressive positions."
- "The people who fly that flag haven't changed."

# Oneliner

Beau delves into the U-Haul incident near the White House, exposing right-wing deflection and the changing nature of political ideologies, urging critical self-reflection.

# Audience

Activists, Political Observers

# On-the-ground actions from transcript

- Examine beliefs and ideologies to ensure alignment with values and ethics (implied).

# Whats missing in summary

A deep dive into shifting political ideologies and how they impact societal perceptions and behaviors.

# Tags

#DC #RightWing #PoliticalIdeologies #ConspiracyTheories #SelfReflection