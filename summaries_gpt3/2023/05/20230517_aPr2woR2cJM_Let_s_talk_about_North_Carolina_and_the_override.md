# Bits

Beau says:

- Explains the significance of the North Carolina veto override and its impact on the people and Republicans in the state.
- Points out that despite overwhelming opposition from North Carolinians in polls, Republican legislators passed unpopular legislation.
- Emphasizes that Republican lawmakers prioritized party loyalty and special interest groups over the will of the people.
- Criticizes Republican legislators for choosing to obey party leadership instead of representing their constituents.
- Urges Republican voters in North Carolina to take action by voting out those who prioritize party interests over the people's will.
- Encourages Republican voters to primary their representatives and show that blind loyalty to the party is unacceptable.
- Conveys the message that if voters don't hold their representatives accountable, they will continue to prioritize special interests over public opinion.

# Quotes

- "They put the special interest groups above the people of North Carolina to include Republicans, to include their Republican voters."
- "You need to set the tone right now that that's not acceptable, that they don't own you."
- "None of them thought the people of North Carolina was more important than obeying the leadership."
- "Their priority was doing what they're told. They don't sound like leaders to me."
- "If you don't vote them out, they're going to keep doing it."

# Oneliner

Beau calls out North Carolina Republicans for prioritizing party interests over constituents, urging voters to take action and not tolerate blind loyalty.

# Audience

North Carolina voters

# On-the-ground actions from transcript

- Primary Republican representatives (suggested)
- Vote out representatives prioritizing party interests over constituents (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of how North Carolina Republicans prioritized party loyalty over the will of their constituents, urging voters to take a stand and hold their representatives accountable.

# Tags

#NorthCarolina #Republicans #VetoOverride #PartyLoyalty #Accountability