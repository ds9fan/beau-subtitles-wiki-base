# Bits

Beau says:

- Explains a message received from someone in Montana criticizing his views on freedom of speech regarding TikTok.
- Quotes the 14th Amendment to the US Constitution to support his argument that states are bound by the Bill of Rights.
- Mentions Montana's state constitution also includes a freedom of speech clause.
- Talks about a freedom of speech lawsuit filed by five TikTok creators against the state.
- The creators argue that Montana lacks the authority to dictate foreign policy or national security interests.
- The attorney general in Montana is ready to defend the legislation in court.
- Beau predicts that federal government intervention might preempt some issues but court cases will still proceed.
- States that state governments cannot limit freedom of speech.
- Overall, the issue revolves around the conflict between state laws and individual freedom of speech rights.

# Quotes

- "No law shall be passed impairing the freedom of speech or expression."
- "States that state governments cannot abridge your freedom of speech."

# Oneliner

Beau explains freedom of speech rights in relation to TikTok, citing constitutional amendments and a lawsuit brought by creators against Montana's state laws.

# Audience

Constitution enthusiasts

# On-the-ground actions from transcript

- Contact legal aid organizations for guidance on constitutional rights and freedom of speech (implied).

# Whats missing in summary

Explanation on the potential impact of federal government intervention on the state's freedom of speech laws.

# Tags

#FreedomOfSpeech #Constitution #TikTok #Montana #Lawsuits