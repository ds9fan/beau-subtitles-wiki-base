# Bits

Beau says:

- Explaining the hesitation in discussing a patch linked to a specific group and the shooter in Texas.
- Describing the commercial availability and widespread use of patches once they leave an in-group.
- Noting hidden meanings of patches and how they can be misunderstood by many who wear them.
- Mentioning the discovery of social media profiles that indicate far-right and white supremacist views of the shooter.
- Speculating on the lack of direct links between the shooter and the group associated with the patch.
- Pointing out the recent conviction of the leadership of a group associated with the patch, which could deter any potential connection to the shooter.
- Anticipating intense scrutiny from domestic counter-terror efforts on the organization due to the proximity of the patch to the convicted leadership.
- Stating the preference to wait for concrete evidence before discussing such sensitive topics to ensure accuracy.

# Quotes

- "The patch, you know, it is what it is."
- "The socials, which do appear to be his, that pretty much confirms his ideological makeup."
- "I like to be right, I don't like to jump the gun."

# Oneliner

Beau explains his cautious approach in discussing a patch linked to a specific group and the shooter in Texas, along with the discovery of social media profiles indicating far-right views but no direct connection to the group, anticipating increased scrutiny on the organization.

# Audience

Community members

# On-the-ground actions from transcript

- Monitor and report extremist activity in your community (implied)
- Support efforts combatting far-right and white supremacist groups (implied)

# Whats missing in summary

The nuances of the dynamics of patches and their hidden meanings in different contexts.

# Tags

#Texas #Patches #SocialMedia #FarRight #WhiteSupremacy #CommunityPolicing