# Bits

Beau says:

- Explains the significance of the numbers 22% and 1% in Republican and Democratic math regarding the budget.
- Republicans focus on a 1% increase cap annually, not the 22% cut in non-defense spending.
- Breaks down how the 22% cut in non-defense spending is derived from the budget plan.
- Illustrates how a 1% increase can actually result in a 5% reduction in purchasing power due to inflation.
- Emphasizes the real-life impact of budget cuts on services like Border Patrol with a reduction in purchasing power.
- Criticizes the Republican budget plan as detrimental to the economy and unsustainable.

# Quotes

- "The Republican budget plan, it's not a plan for a healthy economy."
- "The 1% increase is still an effective budget cut."
- "It's huge. It's huge."
- "So the 22% reduction across the board. That's where the number comes from."
- "It's not even close to a reasonable starting point for a negotiation."

# Oneliner

Beau explains the implications of the 22% cut and 1% increase in the Republican budget plan, critiquing its impact on the economy and services.

# Audience

Budget analysts, policymakers

# On-the-ground actions from transcript

- Analyze and understand the implications of budget cuts on services in your community (implied)
- Advocate for sustainable budget plans that prioritize economic stability and public services (implied)

# Whats missing in summary

Detailed breakdown of economic impacts and comparisons to everyday purchasing power.

# Tags

#Budget #Republican #Democratic #Math #Economy