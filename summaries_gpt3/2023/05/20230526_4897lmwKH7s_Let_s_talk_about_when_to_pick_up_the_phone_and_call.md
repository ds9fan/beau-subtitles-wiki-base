# Bits

Beau says:

- Explains the appropriate time to contact law enforcement, especially in violent situations where force may be necessary.
- Points out that calling 911 summons lethal force and there might be better alternatives in non-violent situations.
- Illustrates how every law is backed by the potential for lethal force, not necessarily the death penalty, using examples like littering.
- Emphasizes that non-violent actions can escalate to violence if not complied with by law enforcement.
- Shares a real-life incident where a native man calling Border Patrol about trespassing led to his death, showcasing the risks of involving armed individuals who may not understand the situation.
- Questions the effectiveness of law enforcement de-escalation when calling them might actually escalate the situation.
- Encourages considering alternative ways to handle situations rather than immediately involving armed authorities.
- Raises awareness about the potential consequences of involving law enforcement and urges for a more thoughtful approach to seeking help.

# Quotes

- "When you dial 911, you're summoning a lethal force."
- "Every law is backed up by that. It's an inherent part of it in the US because of how we justify things."
- "Using armed people who don't know what's going on, it's not normally a good idea."
- "Not just might you save somebody's life, stop somebody from being killed who didn't need to be killed. The life you save might be your own."
- "There are normally better ways to deal with things."

# Oneliner

Beau explains the risks of summoning lethal force by calling 911 and urges for thoughtful alternatives in non-violent situations to prevent unnecessary escalations involving armed authorities.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Question the necessity of involving law enforcement in non-violent situations (implied)
- Advocate for exploring alternative ways to handle conflicts before resorting to calling authorities (implied)

# Whats missing in summary

Beau's emotional delivery and real-life examples provide a compelling argument against immediately involving armed authorities in non-violent situations, urging for a more thoughtful and cautious approach.

# Tags

#LawEnforcement #LethalForce #De-escalation #CommunityPolicing #ArmedAuthorities