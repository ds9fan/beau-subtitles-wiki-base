# Bits

Beau says:

- Beau questions the ongoing investigation into Biden by Republicans and expresses skepticism due to lack of evidence.
- Republicans are accused of rebranding similar claims with less evidence of influence peddling by Hunter and James.
- The Fox summary acknowledges no illegal activity by Joe Biden and lack of profits for him.
- A secret informant in the investigation has gone missing, raising concerns about their credibility.
- Beau questions the lack of urgency in finding the missing informant if the allegations were believed.
- International consulting firms linked to Biden's family received payments, which Beau sees as standard practice.
- Beau recalls his past investigation into claims against Biden and how they fell apart with a timeline analysis.
- Despite bank records not showing evidence and the missing informant, Beau predicts Republicans will drag out the investigation without producing substantial evidence.
- He suggests oversight of immediate family members' finances in high-ranking positions to prevent conflicts of interest.
- Beau proposes a bipartisan oversight committee including IRS and DOJ representatives to ensure financial transparency.

# Quotes

- "An informant in a Congressional investigation disappeared. They can't find him. They don't know what happened to him. Just gone with the wind."
- "If you're actually concerned about this type of stuff, create some oversight. You're the legislative branch of government. legislate."

# Oneliner

Beau questions the lack of evidence in the Republican investigation into Biden while advocating for financial oversight of high-ranking officials' family members.

# Audience

Legislators, government officials

# On-the-ground actions from transcript

- Advocate for bipartisan oversight of immediate family members' finances in high-ranking positions (suggested)
- Push for the creation of a committee including IRS and DOJ representatives to ensure financial transparency (suggested)

# Whats missing in summary

Detailed analysis and examples from Beau's past investigation into similar claims against Biden.

# Tags

#Biden #Republican #Investigation #FinancialOversight #Transparency