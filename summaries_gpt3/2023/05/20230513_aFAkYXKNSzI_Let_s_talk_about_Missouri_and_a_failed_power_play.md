# Bits

Beau says:

- Missouri Republicans tried to make it harder for citizens to override legislature through the Initiative Petition by increasing the required support from a simple majority to 57%.
- Their attempt to change the process was driven by their fear that citizens wanted reproductive rights, which goes against the restrictive ban they passed.
- Despite knowing their ban was unpopular and against the will of the people, Republicans prioritized ruling over representing the citizens.
- The Republican Party's attempt to limit citizens' ability to represent themselves failed, allowing for the potential restoration of reproductive rights in Missouri through an initiative petition.
- Republicans displayed a clear desire to rule over the citizens rather than truly represent them, showing a disregard for the will of the people.

# Quotes

- "Republicans, they don't care about the Republic. They don't care about being your representative. They don't want to represent you. They want to rule you."
- "They know their ban is unpopular. That they know it goes against the will of the people of Missouri. They just don't care because they're your betters."
- "They were not able to change it so that process is still available and there will probably be an initiative petition put forward that will restore reproductive rights in Missouri."
- "Rather than represent the will of the people, they want to override it."
- "Republicans in Missouri, well, they caught the car."

# Oneliner

Missouri Republicans attempt to restrict citizens' ability to override legislature through Initiative Petition due to fear of reproductive rights, prioritizing ruling over representing the will of the people.

# Audience

Activists, Missouri residents

# On-the-ground actions from transcript

- Support and participate in initiative petitions to restore reproductive rights in Missouri (implied).
- Stay informed about legislative changes and initiatives in Missouri (implied).

# Whats missing in summary

The full transcript provides more context on the power struggle between Missouri Republicans and citizens advocating for reproductive rights.