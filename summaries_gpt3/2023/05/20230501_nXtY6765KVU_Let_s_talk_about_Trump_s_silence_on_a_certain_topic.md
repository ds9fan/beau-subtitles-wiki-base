# Bits

Beau says:

- Outlines a recent conservative uproar over a beverage company's novelty can.
- Explains why Trump remained quiet during the uproar and why his son spoke out.
- Mentions that Trump's financial disclosure revealed a significant investment in Anheuser-Busch InBev.
- Notes that the Trumps did not actively participate in the culture war despite their investment in the company.
- Suggests that culture wars are used to keep supporters busy, angry, and energized.
- Emphasizes that those at the top do not genuinely care about the culture wars.
- Points out the hypocrisy of Trump's investment in the company while calling for the boycott to stop.
- Concludes that money always wins over culture war rhetoric for the Republican base.

# Quotes

- "They don't believe what they sell you."
- "If it's between that culture war nonsense that has the Republican base super angry, yelling, screaming and running over cans and money, the money always is going to win."
- "Those at the top, they don't actually care about that."

# Oneliner

Beau reveals how the Trumps' investment interests outweighed their supposed belief in culture wars, exposing the hypocrisy within conservative circles.

# Audience

Political enthusiasts

# On-the-ground actions from transcript

- Divest from companies contradicting stated beliefs (suggested)
- Stay informed on political motivations behind certain actions (suggested)
- Support businesses that uphold values you believe in (suggested)

# Whats missing in summary

Exploration of the manipulation tactics used by political figures in perpetuating culture wars for personal gain. 

# Tags

#ConservativeUproar #Trump #CultureWars #Hypocrisy #Investments