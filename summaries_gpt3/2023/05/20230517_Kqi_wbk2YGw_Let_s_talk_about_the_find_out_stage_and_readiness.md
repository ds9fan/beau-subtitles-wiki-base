# Bits

Beau says:

- Legislation in red states negatively impacts military readiness by hindering the force's ability to do its job.
- Bases in red states are affected when the state targets certain demographics, leading to closures and economic losses.
- Space Force headquarters was supposed to be at Redstone in Alabama but is now likely to be in Colorado.
- The restrictive laws in Alabama regarding reproductive rights and targeting the LGBTQ community may have influenced the decision.
- Installations are selected based on a checklist that includes factors like room for growth, proximity to airports, schools, and healthcare access.
- Laws that were not in place when Huntsville was selected are now impacting decisions regarding military installations.
- Bases in states with restrictive laws may never be selected again due to the negative impact on readiness.
- Lack of investment in states with such restrictions will continue until the laws change, as it affects recruitment, retention, and quality of life.
- Politicians using wedge issues like these laws have consequences that hurt the military in the long run.
- The economic activity around military installations is at risk when bases close due to legislative impacts.

# Quotes

- "Legislation in red states negatively impacts military readiness."
- "Y'all did this. You're literally hurting the military."
- "It's not a political thing. It's because you're literally hurting the military."

# Oneliner

Legislation in red states negatively impacts military readiness, risking closures of bases and economic losses, hurting the military.

# Audience

Military advocates, policymakers

# On-the-ground actions from transcript

- Advocate for changes in restrictive laws impacting military installations (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of how legislation in red states affects military readiness and the implications for bases and communities.

# Tags

#Military #Legislation #Readiness #RedStates #Alabama #SpaceForce #EconomicImpact