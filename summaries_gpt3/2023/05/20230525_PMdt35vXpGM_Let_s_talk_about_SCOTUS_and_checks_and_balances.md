# Bits

Beau says:

- Addressing the Supreme Court, branches of government, and Congress's power to legislate about other branches.
- Chief Justice Roberts considering new ethical standards and Harlan Crowe dismissing Congress's legislative interest in his friendships.
- Congress's authority to legislate and its impact on the court's independence.
- Pelosi's subpoena power to call Justice Roberts for a hearing investigation.
- The necessity for Congress to show a legislative interest to conduct a hearing investigation.
- The concept of independent but not immune branches of government with checks and balances.
- Comparing Congress enforcing ethics on the Supreme Court to the Presidential Records Act enforced after Nixon.
- Congress's power to impose ethical standards and the need for Roberts to self-regulate to keep Congress out of the Supreme Court's internal affairs.
- Congress's ability to alter aspects of the Supreme Court except those outlined in the U.S. Constitution.
- Speculation on Roberts' intention to self-regulate and the ongoing saga regarding ethical standards and the relationship between Harlan Crow and Clarence Thomas.

# Quotes

- "Independent doesn't mean immune from, the system was set up to have checks and balances."
- "The whole idea is to have each branch checks the others."
- "They absolutely have the power to impose ethical standards they have in the past, and I'm sure they will in the future."
- "Congress can actually alter a lot of things about the Supreme Court."
- "I definitely don't think this saga is over."

# Oneliner

Beau dives into the Supreme Court, Congress's authority, ethical standards, and the need for checks and balances in a system of independent branches.

# Audience

Civics enthusiasts

# On-the-ground actions from transcript

- Stay informed on the developments regarding Congress's legislative interest in ethical standards for the Supreme Court (implied).

# Whats missing in summary

Insight into the potential future implications of Congress's actions and the Supreme Court's response.

# Tags

#SupremeCourt #Congress #ChecksAndBalances #EthicalStandards #Legislation