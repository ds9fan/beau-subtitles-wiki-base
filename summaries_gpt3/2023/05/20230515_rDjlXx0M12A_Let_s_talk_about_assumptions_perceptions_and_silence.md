# Bits

Beau says:

- Received a message prompting him to address perceptions and assumptions.
- Clarifies his stance on not discussing certain incidents and why.
- Differentiates between law enforcement versus private citizen incidents.
- Explains his knowledge of a lateral vascular neck restraint (LVNR).
- Stresses the importance of intent but doubts its relevance in a specific incident.
- Expresses disapproval of cheering for a homeless person in distress being taken out on the subway.
- Encourages critical thinking about what content creators cover.

# Quotes

- "I give everybody the benefit of the doubt on intent. That's super important."
- "It doesn't matter what you think happened in your breakdown of it. It's not good."
- "There's no way where this is good."

# Oneliner

Beau clarifies his stance on discussing incidents, explains the LVNR, and stresses the importance of intent while criticizing cheering for disturbing events.

# Audience

Content creators, viewers

# On-the-ground actions from transcript

- Re-examine beliefs on what content creators cover (implied).

# Whats missing in summary

Beau's personal reactions and emotional responses to the message and the incidents discussed.

# Tags

#Perceptions #Assumptions #LVNR #Intent #CriticalThinking