# Bits

Beau says:

- Provides an update on additional information following his last video on Biden's budget.
- Mentions a potential proposal by the Biden administration for a minimum tax rate on income for billionaires at 25%.
- Acknowledges the pushback expected from billionaires and their allies in Congress against this proposal.
- Emphasizes the need for Congress to approve such proposals, not just the president.
- Expresses skepticism about the likelihood of the billionaire tax proposal passing due to the influence of wealthy interests in Congress.
- Suggests that bipartisan support from average voters might be necessary to push such a proposal through.
- Notes the symbolic nature of the proposal in setting the tone for economic debates and portraying the Biden administration as progressive.
- Concludes by mentioning this proposal as part of Biden's economic plan that will likely receive significant attention and debate.

# Quotes

- "It's being suggested that the Biden administration is going to suggest a billionaires tax."
- "The idea that a bunch of billionaires are going to allow a billionaire tax to go through seems pretty slim."
- "You're talking about people who are, well, billionaires. They have a lot of money, they have a lot of influence."
- "The only way to overcome that would be sheer numbers."
- "It's definitely setting the tone for an economic debate and it is casting the Biden administration in a more progressive light."

# Oneliner

Beau provides insights on the potential billionaire tax proposal in Biden's budget, facing challenges from wealthy interests and needing bipartisan support for approval.

# Audience

Policy analysts

# On-the-ground actions from transcript

- Contact your representatives to express support for the billionaire tax proposal (suggested).

# Whats missing in summary

Deeper analysis and implications of the billionaire tax proposal within Biden's economic plan.

# Tags

#Biden #TaxProposal #WealthInfluence #Congress #EconomicDebate