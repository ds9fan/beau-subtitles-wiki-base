# Bits

Beau says:

- United States Navy is renaming ships named after Confederacy-related people or events, like the USNS Murray being renamed the Marie Tharp.
- The USS Chancellorsville is being renamed for Robert Smalls, a former slave who became the first black person to command a U.S. naval ship after learning to sail as a child.
- In 1862, Smalls, while being forced to serve on a Confederate steamer, commandeered the ship, guided it past five Confederate forts, and handed it over to the Union Navy.
- Smalls had a remarkable life beyond this act, becoming the first black commander of a U.S. naval vessel and influencing Lincoln's decision to allow black troops.
- Robert Smalls' story is a truly American and uplifting tale that deserves recognition and remembrance.

# Quotes

- "Robert Smalls' story is a truly American and uplifting tale."
- "If you are looking for a story that is just purely American and kind of uplifting, Robert Smalls is definitely one to look into."

# Oneliner

The United States Navy is renaming ships named after Confederacy-related figures, like the USNS Murray becoming the Marie Tharp, and the USS Chancellorsville being renamed for Robert Smalls, a former slave turned hero and influential leader.

# Audience

History enthusiasts

# On-the-ground actions from transcript

- Research the remarkable life of Robert Smalls to learn about his contributions and impact on American history (suggested).
- Share Robert Smalls' story with others to spread awareness of this uplifting tale (exemplified).

# Whats missing in summary

Exploring the full transcript can provide a deeper understanding of the significance of renaming ships and recognizing impactful historical figures like Robert Smalls. 

# Tags

#USNavy #ShipRenaming #RobertSmalls #AmericanHistory #CivilWar #InspirationalLeadership