# Bits

Beau says:

- Talking about Missouri's HB 700 introduced by Representative Hardwick, focusing on curtailing government response to public health issues.
- Mentioning a particular prohibition in the legislation on introducing microchips under the skin via vaccines.
- Explaining the potential harmful implications of banning something that doesn't exist, like microchips, leading to conspiracies.
- Addressing how politicians playing into an echo chamber can create real problems later.
- Noting that while some parts of the legislation may cause immediate concerns, the ban on microchips might lead to more vaccine hesitancy.
- Providing a glimmer of hope by mentioning the lack of support for the legislation from lobbying groups.
- Stressing the importance of scientific literacy, education, and critical thinking in the US.
- Expressing concern over the existence and persistence of such legislation despite past failures.
- Concluding with a call for better education and critical thinking skills in the country.

# Quotes

- "Now this is funny on some level. It's humorous."
- "This is one of the real issues when politicians play into an echo chamber."
- "It shows the desperate, desperate need for a lot of scientific literacy in the United States."
- "That vaccine hesitancy, the loss that will be caused by it, it came from show legislation."
- "We need better schools."

# Oneliner

Beau addresses Missouri's HB 700, focusing on the prohibition of microchips through vaccines, illustrating the dangers of show legislation and the urgent need for scientific literacy and critical thinking in the US.

# Audience

Activists, Educators, Citizens

# On-the-ground actions from transcript

- Advocate for improved scientific literacy in educational institutions (implied)
- Promote critical thinking skills in media consumption (implied)
- Support lobbying groups opposing harmful legislation (implied)

# Whats missing in summary

The full transcript provides a comprehensive analysis of the potential implications of legislation like Missouri's HB 700, stressing the importance of education and critical thinking to combat harmful narratives and actions.

# Tags

#Missouri #HB700 #Legislation #Microchips #VaccineHesitancy