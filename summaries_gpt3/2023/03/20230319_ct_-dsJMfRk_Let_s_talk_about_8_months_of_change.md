# Bits

Billy Young says:

- A person shares their journey of change over eight months, prompted by meeting a trans woman at work.
- They were initially conservative and uncomfortable with the LGBTQ+ community, but their perspectives have evolved.
- The person describes their positive interactions with the trans woman, who they find feminine and kind.
- Despite this progress, they struggle to view the trans woman's friends in the same light.
- The person seeks advice on how to overcome this mental block and continue growing.
- Billy Young reassures them that change is a process and commends their progress.
- He encourages continued learning and self-reflection to combat internal biases.
- The person is reminded that their transformation signifies growth and should be embraced.
- Billy Young explains that passing privilege may influence the ease of accepting the trans woman they know well.
- He acknowledges the person's journey from holding prejudiced beliefs to actively seeking understanding.
- Growth involves realizing that a person's appearance does not solely define their gender.
- Billy Young praises the person's willingness to learn and adapt, showcasing their commitment to change.
- The person is assured that their struggle is normal and indicative of ongoing progress.
- Despite feeling challenged by accepting others, the person is encouraged to keep moving forward in their journey.
- Billy Young suggests that their relationship with the trans woman influences their ability to accept her friends.

# Quotes

- "If you are the same person you were five years ago, you've wasted five years of your life."
- "You're not a horrible person. You are a person mid-change."
- "You went from kind of a bigot to being so concerned about something you thought, not even something you did or said, but something you thought that you sent this message."
- "Change takes time. You're not going to get everything right, right away, but you'll keep moving forward."
- "Just the fact that you want to learn more is a pretty good indication that you're going to be successful."

# Oneliner

A journey of personal growth from prejudice to acceptance, guided by interactions with a trans woman and her friends.

# Audience

Individuals seeking guidance on personal growth and overcoming biases.

# On-the-ground actions from transcript

- Keep learning and allowing yourself to be educated (implied).
- Continue broadening your perspectives and challenging internal biases (implied).
- Embrace change as a gradual process and be patient with yourself (implied).

# Whats missing in summary

The full transcript delves into the nuances of personal growth and acceptance, offering valuable insights into overcoming biases through genuine interactions and self-reflection.

# Tags

#PersonalGrowth #Acceptance #LGBTQ+ #Learning #Bias #Community