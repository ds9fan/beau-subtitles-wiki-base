# Bits

Beau says:

- Wisconsin is facing a consequential judicial election in 2023, with one Supreme Court seat up for grabs.
- The current political landscape in Wisconsin includes a Democratic executive branch, a Republican-controlled legislature, and a conservative-leaning Supreme Court.
- The candidates vying for the seat are Janet Protasewicz (liberal) and Daniel Kelly (conservative).
- The outcome of this election is significant due to Wisconsin being a swing state, with potential implications for future elections, especially in 2024.
- Potential issues at stake include family planning laws dating back to 1849.
- The amount of money being spent on this judicial race underscores its importance and the high expectations from both parties.
- The election on April 4th is likely to have national implications, given Wisconsin's swing state status.
- The judicial rulings in this race could impact various issues like redistricting, voting rights, and family planning.
- Despite being an off-year election, this race is expected to be highly consequential and closely watched.
- Beau does not endorse candidates but distinguishes Protasewicz as the liberal and Kelly as the conservative.

# Quotes

- "Wisconsin is a swing state."
- "The candidates are Janet Protasewicz, if you live up there you find that funny because of the ads, and the other is Daniel Kelly."
- "This is a race that the entire nation is going to end up kind of waiting on rulings from, I would imagine, come 2024."

# Oneliner

Wisconsin's pivotal judicial election in 2023 could sway national implications in a swing state, impacting issues from family planning to voting rights.

# Audience

Wisconsin voters

# On-the-ground actions from transcript

- Pay attention to the upcoming judicial election in Wisconsin (suggested)
- Stay informed about the candidates and their stances on key issues (suggested)
- Engage in voter education and outreach efforts within your community (implied)

# Whats missing in summary

Deeper insights into the specific policy differences between the candidates and the potential consequences of the election beyond 2024.

# Tags

#Wisconsin #JudicialElection #SwingState #VotingRights #FamilyPlanning