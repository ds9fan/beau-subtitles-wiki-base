# Bits

Beau says:

- Reporting on accounts identified on Twitter trying to influence the narrative following a train derailment in Ohio.
- Accounts linked to pro-Moscow voices spreading misinformation like inaccurate maps and false cancer rate charts.
- Some accounts appeared to be Moscow-controlled, while others were American accounts amplifying the message.
- Beau finds it strange that an intelligence service like Moscow's or even Republicans are spreading such misinformation.
- Speculation that these accounts were using Twitter's paid verification service to increase reach and steer the narrative.
- If it was an information operation, the goal might have been to test the effectiveness of using the paid service to influence American politics.
- Potential consequences like congressional hearings if foreign intelligence services use paid services to influence internal politics.
- Beau expects more in-depth analysis on the incident in the future to understand better what happened.
- The time lag between the event and control of the narrative is something to watch for.
- The incident may be a precursor to similar events in the future.

# Quotes

- "Some of the accounts appeared to be Moscow controlled, and some were just American accounts amplifying that message."
- "It's not actionable. There's nothing that really can come of spreading disinformation about this."
- "If it turns out that the paid service is going to be used as a microphone for foreign intelligence services to try to influence internal American politics, it might lead to congressional hearings."
- "Right now the reporting is kind of light, but it's interesting and it's something that is probably one of those things we're going to see again."
- "It may not have been successful but it was just an interesting little tidbit I saw."

# Oneliner

Beau reports on suspicious Twitter accounts linked to pro-Moscow voices spreading misinformation after a train derailment, raising concerns about potential foreign influence on American politics.

# Audience

Internet users

# On-the-ground actions from transcript

- Keep an eye out for misinformation and disinformation spread on social media platforms (implied).

# Whats missing in summary

The full transcript provides more detailed insights into potential foreign influence operations using social media platforms.

# Tags

#Twitter #Misinformation #ForeignInfluence #Politics #SocialMedia