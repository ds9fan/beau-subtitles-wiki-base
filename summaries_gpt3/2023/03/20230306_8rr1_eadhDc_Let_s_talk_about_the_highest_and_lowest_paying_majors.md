# Bits

Beau says:

- Analyzing college majors and their corresponding salaries within five years of graduation to understand societal values and trends.
- Top 10 highest paying majors include engineering and business analytics, ranging from $65,000 to $75,000 annually.
- Lowest paying majors consist of education, social services, and performing arts, ranging from $36,000 to $40,000 yearly.
- Signifies a societal issue in undervaluing professions like education, mental health, and social work.
- Implies a lack of critical thinking and information processing skills in society.
- Educators are not adequately compensated for their vital role in society.
- Points out the discrepancy in values between high-paying and low-paying majors.
- Expresses concern about the devaluation of critical professions like education and mental health.
- Criticizes the lack of financial support and respect given to educators.
- Raises awareness about ongoing attacks on the education sector by legislators.

# Quotes

- "We do not value the thing we need the most."
- "Educators don't go into it for money, but there very well might be people who don't go into it because of money."
- "We have an extended problem in the United States with a whole lot of people having issue with processing information, the ability to critically think."
- "Social work? Nah. Mental health? Nah. Education? Nah. None of that stuff's important."
- "This is just identifying a symptom of a disease."

# Oneliner

Analyzing college majors and their salaries reveals societal values, showing undervaluation of critical professions like education and mental health.

# Audience

Students, educators, policymakers

# On-the-ground actions from transcript

- Advocate for fair compensation for educators and professionals in critical fields (implied)
- Support initiatives that prioritize education and mental health funding (implied)

# Whats missing in summary

The full transcript provides a deeper dive into the societal implications of salary discrepancies among college majors. Watching the full video can offer more insight into the speaker's perspective on valuing critical professions.

# Tags

#CollegeMajors #SalaryDiscrepancy #Education #SocietalValues #Undervaluation