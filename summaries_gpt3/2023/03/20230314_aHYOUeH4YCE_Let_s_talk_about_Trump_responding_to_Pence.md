# Bits

Beau says:

- Former President Trump is responding to former Vice President Pence's recent statements, indicating that Trump believes Pence is to blame for January 6th.
- Trump claims that if Pence had sent the votes back to legislators, there wouldn't have been a problem with January 6th, despite this not being within Pence's power.
- Trump seems to be trying to shift the blame onto Pence, portraying him as responsible for the events of January 6th.
- The attempt to make Pence accountable for the insurrection demonstrates Trump's mindset of deflecting responsibility and expecting obedience without taking any blame.
- Trump's statement reveals his belief that any negative outcomes are solely due to people not obeying him and that he is absolved of any responsibility.
- Despite not being a fan of Pence, Beau finds Trump's attempt to blame him as concerning and questions whether this is the kind of representation Republicans want.
- The thought process behind Trump's statement raises critical questions for Republicans about the type of leadership they are endorsing.

# Quotes

- "Trying to paint Pence as the person responsible in any way for what happened on the 6th is just wild."
- "And for Republicans, you really need to think about the thought process that has to be behind a statement like this."

# Oneliner

Former President Trump blames former Vice President Pence for January 6th, showcasing his refusal to take responsibility and demanding unwavering obedience.

# Audience

Republicans, political observers

# On-the-ground actions from transcript

- Analyze the thought process behind statements from political figures (implied)
- Question the type of leadership endorsed within political parties (implied)

# Whats missing in summary

Deeper insights into the implications of political leaders deflecting blame and demanding absolute loyalty.

# Tags

#Trump #Pence #Politics #Responsibility #Leadership