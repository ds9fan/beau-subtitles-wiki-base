# Bits

Beau says:

- Explains a viral image comparing General Eisenhower and General Milley based on their medals and the implication that Milley has undeserved medals.
- Clarifies that the number of ribbons displayed by generals changes over time but doesn't impact the comparison in the image.
- Counters the notion that Eisenhower was "unwoke" by detailing his actions supporting civil rights, desegregation, and other progressive causes.
- Notes that Eisenhower received numerous medals from different countries but was modest and didn't display them all.
- Warns against drawing historical conclusions from inaccurate memes and the dangers of spreading misinformation for specific agendas.
- Urges viewers not to rely on memes for historical accuracy and to question the motives behind such misleading information.

# Quotes

- "Don't get your history information from memes."
- "Your propaganda is bad and you should feel bad."

# Oneliner

Be cautious of historical inaccuracies in memes and understand the motives behind spreading misinformation.

# Audience

History enthusiasts

# On-the-ground actions from transcript

- Fact-check historical information before sharing (implied)
- Question the motives behind spreading inaccurate information (implied)

# Whats missing in summary

The detailed examples of Eisenhower's progressive actions and his modesty in receiving and displaying medals.

# Tags

#History #Memes #Propaganda #FactChecking #CivilRights