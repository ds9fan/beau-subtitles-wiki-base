# Bits

Beau says:

- Republican governor of North Dakota vetoed a bill to stop schools from using pronouns.
- Governor's image is that of a small government conservative.
- Governor's veto was based on principle of limiting state overreach.
- Vetoed a bill in 2021 regarding trans students playing sports in school.
- Concerning news: state legislature wants to override the veto.
- Faction of small government conservatives or anti-authoritarian right supporting trans people.
- Split on the right regarding trans people owning guns.
- Some Republicans standing by their principles amidst the authoritarian sweep in the party.
- Expect this principled group to become louder as MAGA and Trump influence wanes.
- Anticipates this group to regain lost power within the Republican Party.

# Quotes

- "It's not inconceivable, it's just not something you see very often."
- "Those who actually stood by their principles, I have a feeling you're going to see them get really loud soon."
- "Expect to see more of this in various ways."

# Oneliner

Republican governor of North Dakota vetoed bill on pronouns, revealing a principled stand against state overreach amidst a split within the conservative movement supporting trans rights.

# Audience

Conservative activists

# On-the-ground actions from transcript

- Contact local representatives to voice support for the governor's veto (suggested).
- Join small government conservative or anti-authoritarian groups advocating for limited state overreach (implied).
- Organize events to raise awareness on the importance of standing by principles within political movements (exemplified).

# Whats missing in summary

The full transcript provides deeper insights into the evolving dynamics of conservative factions and their stances on issues like trans rights and state overreach.