# Bits

Beau says:

- Nebraska legislature attempting a government takeover of healthcare, starting with banning gender-affirming care for trans minors.
- Senators vowing to filibuster the session to stop this legislation from moving forward.
- Four senators committed to filibuster, possibly joined by a fifth.
- Filibustering the entire session can grind the legislature to a halt but will need support to withstand the pressure.
- Urges people to show support by dropping a line to these senators to encourage and appreciate their efforts.

# Quotes

- "There's a whole lot of people who want to consider themselves allies right up until it's time to do ally stuff."
- "If you're doing it alone, it's hard."
- "People who are trying to take a stand could probably benefit from an email or a social media message."
- "This is definitely a time when some people who are trying to take a stand could probably benefit from an email or a social media message."
- "Y'all have a good day."

# Oneliner

Nebraska senators filibuster against healthcare takeover, needing support to grind legislative halt and hold the line; drop them a line to show appreciation and encouragement.

# Audience

Supporters of trans rights

# On-the-ground actions from transcript

- Contact Nebraska senators to show support (suggested)
- Drop them an email or a social media message (suggested)

# Whats missing in summary

The emotional impact and urgency of showing support for those fighting against the healthcare takeover in Nebraska.

# Tags

#Nebraska #Healthcare #Allyship #Filibuster #SupportTransRights