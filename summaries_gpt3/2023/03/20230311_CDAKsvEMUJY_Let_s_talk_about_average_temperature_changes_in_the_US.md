# Bits

Beau says:

- Average winter temperature in the United States has risen by 3.7 degrees Fahrenheit from 1970 to 2023.
- The average summer temperature has risen by 2.3 degrees Fahrenheit, impacting about 80% of the country.
- This warming trend is noticeably affecting agriculture, as some crops that need chill time to grow may not thrive.
- The impacts of climate change extend beyond temperature changes, affecting water resources as well.
- Warmer winters may lead to reduced snowpack, impacting freshwater sources.
- There is a reluctance to make necessary changes to address climate change.
- Many regions that will be severely affected by climate change are not in the United States and have contributed less to the issue.
- Climate change will have global impacts, including on the US, through factors like water scarcity and food production.
- The transition away from fossil fuels and dirty energy is inevitable and must happen for the well-being of future generations.
- Leaders who prioritize the security and well-being of future citizens are needed to drive these necessary changes.

# Quotes

- "It is occurring, and it's going to have impacts."
- "If you want your kids, your grandkids, to have some semblance of the society that we enjoy, these changes have to occur."
- "The transition away from fossil fuels, away from dirty energy, they're going to have to occur and they will occur one way or another."

# Oneliner

Average temperatures in the United States are rising, impacting agriculture and water resources, requiring a transition away from fossil fuels for a sustainable future.

# Audience

Climate activists, policymakers, citizens

# On-the-ground actions from transcript

- Advocate for leaders who prioritize sustainable energy transitions and climate action (implied)
- Support policies and initiatives that address climate change impacts on agriculture and water resources (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the impacts of rising temperatures in the United States and the necessity of transitioning away from fossil fuels for a sustainable future.

# Tags

#ClimateChange #TemperatureRise #FossilFuels #Sustainability #FutureGeneration