# Bits

Beau says:

- Explains the International Criminal Court issuing a warrant for Putin.
- Compares Putin to either an emperor or a pirate, discussing the consequences of each.
- Mentions the concept that emperors often behave like criminals but are protected by state power.
- Points out that Putin, as the head of a major power, is more like an emperor.
- States that Putin facing prosecution by the ICC is unlikely without a significant mistake or Russian people's decision.
- Mentions the US legislation authorizing using all means necessary to bring back detained personnel from the ICC.
- Notes that the US, like Russia, is also considered an emperor and has legislation to protect its personnel.
- Suggests that major powers, not just Russia, have mechanisms to avoid facing consequences for actions.
- Talks about the importance of size when it comes to enforcing legal mechanisms.
- Concludes by stating that there won't likely be a US President going to the ICC and refers to the concept as just a thought.

# Quotes

- "Is Putin more like an emperor or a pirate?"
- "The head of state of any major pole of power is an emperor."
- "The United States is an emperor as well."
- "Sometimes when it comes to not the morality of it, but the legality and the ability to enforce those legal mechanisms, size matters."
- "It's just a thought."

# Oneliner

Beau explains the ICC warrant for Putin, compares him to an emperor, and touches on US legislation protecting personnel from the court.

# Audience

World citizens

# On-the-ground actions from transcript

- Contact your representatives to advocate for accountability for leaders regardless of their power (implied).

# Whats missing in summary

The full transcript provides a deeper understanding of the implications of international law and power dynamics.

# Tags

#Putin #InternationalCriminalCourt #EmperorVsPirate #USLegislation #PowerDynamics