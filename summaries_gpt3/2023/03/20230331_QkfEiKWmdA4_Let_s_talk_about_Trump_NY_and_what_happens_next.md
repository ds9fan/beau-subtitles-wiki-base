# Bits

Beau says:

- Explains the uncertainty surrounding Trump's processing in New York and the need to re-evaluate expectations of knowing things ahead of time.
- Trump is expected to be processed in New York in the next few days under Secret Service protection, but the media's Tuesday expectation may not be accurate.
- Due to security concerns, both inaccurate information from the media and the DA's office may lead to uncertainties about timelines.
- The DA's office may have reasons to withhold accurate information about when Trump's indictment is expected.
- The traditional habit of knowing things ahead of time may no longer be reliable for security reasons.
- There is a possibility that Team Trump was informed about the situation in New York either out of courtesy or through a leak, leading to a lack of courtesy in other areas' prosecutors notifying Trump before indictment.
- The shift from political to legal commentary underscores the changing expectations of knowing things ahead of time regarding Trump's legal matters.
- The Secret Service and investigating jurisdictions are not obligated to inform the public in advance of Trump's legal proceedings, leading to increased speculation rather than certainty about the timeline.

# Quotes

- "The habit of knowing what's going to happen ahead of time - I wouldn't count on knowing that anymore."
- "It may come as a total surprise."
- "The expectations of knowing things ahead of time, those need to change."
- "That needs to start to fall under speculation rather than what we know."
- "Anyway, it's just a thought. Y'all So have a good day."

# Oneliner

Beau explains the uncertainties surrounding Trump's processing in New York and the need to shift expectations from knowing things ahead of time to speculation for security reasons.

# Audience

Legal commentators, political analysts

# On-the-ground actions from transcript

- Stay informed on legal proceedings concerning public figures (exemplified)
- Understand the shifting dynamics of legal commentary and the implications for public knowledge (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the changing landscape regarding public knowledge of legal proceedings involving public figures.

# Tags

#Trump #LegalProcess #PublicKnowledge #SecurityConcerns #Expectations