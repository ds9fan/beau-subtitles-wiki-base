# Bits

Beau says:

- Monkeys in Thailand use rocks as tools to smash open nuts, inadvertently creating stone tool-like flakes during the process.
- This observation challenges the long-held belief that early humans exclusively made stone tools intentionally during the Stone Age.
- Scientists are now reconsidering whether early humans accidentally created tools in a similar way to the monkeys, using stones as hammer and anvil.
- There is also a possibility that other primates, not humans, might have been the original toolmakers, with early humans simply utilizing the found stone splinters.
- The implications of this observation are significant, leading to a reevaluation of early human history and potentially rewriting parts of it.
- Toolmaking has been a key factor in understanding the advancement and worldview of early humans.
- If early humans did not intentionally make these tools but found and utilized them, it challenges previous notions about their capabilities and achievements.
- Scientists will need to analyze the locations where these tools were found to determine if they were accidental or not human-made.
- The evolution of human history is constantly evolving, with new observations prompting revisions in our understanding.
- The story of humanity is always in flux, with each new discovery reshaping our perception of the past.

# Quotes

- "Monkeys in Thailand use rocks as tools to smash open nuts."
- "The story of humanity is always being rewritten."
- "Our story, the story of humanity, it's always being rewritten."

# Oneliner

Monkeys inadvertently creating stone tool-like flakes challenge the belief that early humans exclusively made tools intentionally, prompting a reevaluation of human history.

# Audience

History enthusiasts, scientists, curious minds

# On-the-ground actions from transcript

- Analyze stone tool findings to determine accidental or human origin (implied)
- Stay updated on new scientific discoveries challenging historical narratives (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of how a simple observation about monkeys using stones as tools has profound implications for understanding early human history.