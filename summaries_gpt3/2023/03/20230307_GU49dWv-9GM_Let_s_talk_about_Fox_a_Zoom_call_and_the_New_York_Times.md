# Bits

Beau says:

- Explains the difficulty in winning cases like the one against Fox for defamation.
- Mentions the New York Times obtaining a Zoom call of Fox higher-ups from November 16, 2020.
- Describes the call and communications as "nothing short of amazing" and "very telling."
- Shares that the call reveals insights into decision-making processes behind the scenes at Fox.
- Quotes a Fox higher-up expressing regret for calling Arizona for Biden, affecting ratings.
- Reveals internal debates at Fox about upsetting their audience with the truth during the election.
- Talks about the lack of trust in big corporations, big tech, and big media among half of the voting population.
- Questions Fox's mission, suggesting it's more about protecting the brand than informing the public.

# Quotes

- "If we hadn't called Arizona those three or four days following election day, our ratings would have been bigger."
- "But I think we're living in a new world, in a sense, where half of the voting population doesn't believe in big corporations, big tech, big media."
- "It seems like their mission is more to protect the brand."

# Oneliner

Beau dives into a revealing Zoom call obtained by the New York Times, shedding light on Fox's decision-making processes and the struggle between truth and audience satisfaction.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Read the original article and summaries linked by Beau (suggested).
- Dive into the insights about Fox's decision-making process and internal dynamics (suggested).

# Whats missing in summary

Insights into Fox's internal dynamics and decision-making processes.

# Tags

#Fox #Media #DecisionMaking #Trust #Transparency