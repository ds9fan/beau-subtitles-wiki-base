# Bits

Beau says:

- Some suggest New York should have waited to pursue Trump after bigger cases.
- Beau disagrees and believes there's no coordination among prosecutors.
- He thinks starting with New York is appropriate due to the relatively minor charges.
- Trump's calls for support aren't gaining much traction possibly due to the perceived minor nature of the charges.
- Beau sees a slow progression in legal actions as beneficial for supporters to come to terms with reality.
- He believes starting with smaller cases may prevent supporters from making regretful decisions.
- Beau underscores the lack of coordination among prosecutors and the varying legal systems across jurisdictions.
- Legal processes' speed is dependent on filed motions and judicial decisions.
- Beau suggests New York starting with minor charges is advantageous for all parties involved.
- He expresses that Trump might have a better chance at mounting a defense in New York compared to other jurisdictions.

# Quotes

- "I think a slow kind of descent into reality is probably a good thing."
- "Starting with the relatively minor stuff in New York is probably a really good thing for his supporters."
- "I don't think New York is doing it wrong."
- "If they have the evidence, they pursue it."
- "It's just a thought."

# Oneliner

Beau believes starting with minor charges in New York regarding Trump is beneficial for supporters, allowing a gradual acceptance of reality and potentially preventing regretful actions.

# Audience

Legal analysts and interested individuals.

# On-the-ground actions from transcript

- Keep informed about legal proceedings in different jurisdictions (suggested).
- Stay updated on developments regarding the legal cases involving Trump (suggested).

# Whats missing in summary

Insights on the potential impact of legal proceedings on Trump's supporters and the importance of a gradual legal process.

# Tags

#LegalSystem #Prosecution #Trump #NewYork #Coordination