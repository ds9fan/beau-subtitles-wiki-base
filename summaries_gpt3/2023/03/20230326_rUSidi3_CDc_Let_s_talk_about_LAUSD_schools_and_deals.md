# Bits

Beau says:

- Los Angeles Unified School District workers, specifically support workers, felt undervalued, leading to negotiations and a strike.
- The average salary for workers in Los Angeles was around $25,000 a year, requiring many to have second jobs.
- The support workers' strike was backed by the teachers' union, with members encouraged to join picket lines.
- A tentative agreement has been reached after a three-day strike, with still pending ratification.
- Details of the agreement include a $1000 bonus for certain employees, a new minimum wage of $22.52, retroactive salary increases, and health benefits for part-time employees.
- There will be a $2 per hour increase for employees effective January of next year.
- The agreement also includes a $3 million investment in education and professional development for SEIU members.
- The negotiations likely involved intense back-and-forth, evident in the specific wage amount of $22.52.
- The union members seem confident that the agreement will be accepted by the membership.
- The teachers' union is also in separate contract negotiations, potentially leading to more developments in the future.

# Quotes

- "Los Angeles support workers felt undervalued, prompting negotiations and a strike."
- "A tentative agreement includes wage increases and benefits for workers."

# Oneliner

Los Angeles support workers strike for fair treatment, leading to a tentative agreement with wage increases and benefits, while teachers' union negotiations continue.

# Audience

Workers, unions, advocates

# On-the-ground actions from transcript

- Support the workers by staying informed about the progress of the agreement and potential future actions (implied).

# Whats missing in summary

Full details of the specific terms and conditions in the tentative agreement.

# Tags

#LosAngeles #WorkersRights #UnionNegotiations #SupportWorkers #TentativeAgreement