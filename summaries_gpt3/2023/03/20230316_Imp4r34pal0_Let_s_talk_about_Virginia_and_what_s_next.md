# Bits

Beau says:

- Seven deputies in Henrico County, Virginia have been charged with second degree murder for the death of Ervo Otenyo, a 28-year-old man.
- Ervo Otenyo was being admitted to a hospital and allegedly became combative.
- The prosecutor stated that Otenyo died of asphyxia and indicated that the seven deputies were involved.
- Documentation suggests Otenyo was on an emergency custody order related to mental health, which may be a significant factor.
- There are reports of video footage of the incident, described as brutal and inhumane.
- Despite the quick arrests of the deputies, there still needs to be a grand jury process before further progress.
- The deputies are set to appear in court on the 21st, where more information about the process may emerge.
- Statements from the prosecution, family attorney, and local FOP suggest a tense and uncertain situation.
- Many questions remain unanswered regarding mental health issues and the sequence of events leading to Otenyo's death.
- The unfolding events are likely to attract national attention, with more revelations expected in the future.

# Quotes

- "The public and experienced mental health professionals alike will be appalled when the facts of this case are fully made known."
- "It certainly seems like this is going to be national news."
- "There's a whole lot of questions as far as the mental health issues."
- "We're probably in for another long haul."
- "It's just a thought, y'all have a good day."

# Oneliner

Seven deputies charged with second degree murder in the death of Ervo Otenyo, raising questions about mental health and promising a lengthy and impactful legal process.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Support mental health advocacy groups (implied)
- Stay informed about the case developments and outcomes (implied)
- Attend court proceedings if possible to show solidarity and demand justice (implied)

# Whats missing in summary

Details on the potential impact of community activism and advocacy efforts in seeking accountability and justice.

# Tags

#PoliceViolence #JusticeForErvoOtenyo #MentalHealthAwareness #Accountability #CommunityAdvocacy