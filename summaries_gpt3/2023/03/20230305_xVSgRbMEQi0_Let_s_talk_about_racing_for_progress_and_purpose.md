# Bits

Beau says:

- Received a message from someone in the car scene who wants to use their hobby and skill to shift people politically.
- The person is left-leaning but has an in with predominantly right-wing individuals through their interest in cars.
- Concerned about being labeled a hypocrite for loving cars and racing while also caring about the environment.
- Advises focusing on the purpose of using the car scene to bring people to a more progressive place rather than worrying about criticism.
- Notes the difference in the political spectrum where the right is better at building a road to move people to their side compared to the left.
- Emphasizes the importance of using available tools, like one's interest in cars, to shift thought and bring about change.
- Acknowledges that taking heat is inevitable, especially from the left, but staying focused on the purpose is key.
- Mentions that helping someone move away from a bigoted ideology is worth any criticism faced.
- Encourages using one's skills and interests, like racing cars, as a means to influence thought and progress.
- Suggests finding like-minded individuals in the left-leaning car scene to create a unified front for positive change.

# Quotes

- "Is it worth a tank of gas to get somebody away from a bigoted ideology? Yeah, absolutely it is."
- "An activist can't be a perfectionist. They have to be a realist."
- "In war, you use whatever tools you have at your disposal."
- "If you think that you can use that to shift thought, you kind of have an obligation to do it."
- "But if that road is built, you can't abandon it."

# Oneliner

Beau advises using one's interests and skills, like racing cars, to shift thought towards progress and purpose, despite facing criticism, especially from the left.

# Audience

Left-leaning activists

# On-the-ground actions from transcript

- Connect with other left-leaning individuals in the car scene to create a unified front (suggested).
- Use your interest in cars as a tool to influence thought and progress (implied).

# Whats missing in summary

The full transcript delves into the importance of utilizing one's skills and interests for activism, even if facing criticism, to drive positive change in political ideologies.

# Tags

#Activism #Progress #PoliticalShift #CommunityBuilding #SkillUtilization