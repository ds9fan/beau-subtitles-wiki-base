# Bits

Beau says:

- Addresses the question of whether Russia's nukes still work and provides context on the maintenance process.
- Mentions the substance "fog bank" used in nuclear weapons, its importance, and the challenges faced in obtaining it.
- Shares insights on the difficulties of maintaining and modernizing nuclear arsenals, including the issue of part replacement.
- Raises concerns about the maintenance standards of Russia's nuclear weapons compared to Western countries.
- Explains the significance of even a fraction of the arsenal functioning for maintaining deterrence.
- Acknowledges the likelihood of some Russian nukes not working due to maintenance issues and corruption.
- Emphasizes that the concept of deterrence remains unchanged despite potential failures in some weapons.
- Comments on the misconception that a nuclear exchange can be won, stressing the catastrophic nature of such an event.

# Quotes

- "A small fraction of it functioning as intended is enough to cause untold amounts of devastation."
- "It is all bad. Please go watch War Games."

# Oneliner

Beau addresses Russia's nuclear arsenal maintenance, revealing concerns about functionality while stressing the catastrophic impact even a fraction can have.

# Audience

Global citizens, policymakers

# On-the-ground actions from transcript

- Monitor and advocate for transparency in nuclear arsenal maintenance and modernization (implied)
- Support anti-corruption measures in military spending and equipment upkeep (implied)

# Whats missing in summary

Beau's detailed analysis and warnings about the potential risks and consequences of assuming all Russian nukes are functional.

# Tags

#Russia #NuclearWeapons #Maintenance #Deterrence #Corruption