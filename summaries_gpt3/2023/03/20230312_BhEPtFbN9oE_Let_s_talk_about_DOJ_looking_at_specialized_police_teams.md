# Bits

Beau says:

- US Department of Justice announced a upcoming "historical" and "current" **"paper"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"**" **"**"**"**"**"**"**"** **"**"**"**"**"**"** **"**"**"**"**"**" investigation of specialized law enforcement teams, not an investigation. 
- Plainclothes teams with little oversight historically lead to misconduct, prompting a Department of Justice "paper" **"**" investigation on their actions.
- The Department of Justice is expected to compare the actions of these specialized teams to the normal rate of incident among regular law enforcement and determine effectiveness and misconduct rates.
- The purpose of this "paper" investigation is likely to change how funding is allocated, potentially resulting in defunding these teams due to findings of ineffectiveness and high misconduct rates.
- The risk of losing funding is more likely to end these programs than body cam footage or past misconduct.
- This "paper" investigation may be a method for the Civil Rights Division to address the issue without individually investigating multiple departments.
- Beau believes that this investigation is a way to stop future misconduct rather than address past wrongdoings.
- The long-term benefit of this investigation is the potential elimination of historically problematic law enforcement teams.
- Beau suggests that people should not expect immediate arrests of cops in their city due to this investigation; the more likely outcome is a loss of funding for these teams.
- The investigation aims to gauge effectiveness versus misconduct to determine if these specialized teams are truly worth the funding.
- Beau acknowledges that while this is a step towards addressing issues, it may not lead to immediate consequences for cops engaged in misconduct.

# Quotes

- "The risk of losing the funding will do more to end those programs than all of the body cam footage in the world."
- "This is a way to stop the problem rather than engaging with those who have already overstepped."
- "The more likely outcome is that two years from now that team doesn't have any funding."

# Oneliner

The US Department of Justice's upcoming "paper" investigation on specialized law enforcement teams aims to gauge effectiveness, potentially leading to defunding these teams due to high misconduct rates, rather than immediate arrests for cops engaged in misconduct.

# Audience

Policy advocates

# On-the-ground actions from transcript

- Contact policy advocacy organizations for updates and actions related to law enforcement reforms (implied)
- Join community forums or campaigns advocating for police accountability and effective allocation of funding (implied)

# Whats missing in summary

Detailed analysis of specific historical incidents involving plainclothes law enforcement teams and their impact on communities. 

# Tags

#DepartmentOfJustice #LawEnforcementTeams #Misconduct #FundingAllocation #PoliceAccountability