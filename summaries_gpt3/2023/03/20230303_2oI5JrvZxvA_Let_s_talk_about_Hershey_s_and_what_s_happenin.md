# Bits

Beau says:

- Talks about a subset of Americans upset with a candy company due to a marketing move involving pronouns.
- Mentions how conservative thought leaders are now upset with Hershey's and asking followers not to buy their products.
- Explains that Hershey put a picture of a woman on the wrapper with "her" and "she" for International Women's Day.
- Comments on the absurdity of conservatives being upset over pronouns in Hershey's name and marketing.
- Suggests that conservative brands, built on manufacturing outrage, are running out of things to be upset about and are now battling candy companies.
- Points out that most candy companies have legitimate reasons for criticism, but it generally does not involve their wrappers or marketing strategies.
- Speculates that marketing companies working with candy companies intentionally provoke a small group of vocal conservatives for free advertising.
- Notes that the demographic appreciating inclusive marketing is expanding while those against it are shrinking.
- Expresses disbelief at conservatives focusing on pronouns in candy wrappers to manufacture outrage among their base.
- Humorously suggests that conservatives should be mad at Mike and Ike's candy due to the names implying two guys alone together.

# Quotes

- "There's not. It's at the point now that conservative brands who have built themselves on manufacturing outrage are running out of things to manufacture outrage about."
- "It is just mind-boggling to me that this is what conservatives have latched onto in an attempt to manufacture outrage among their base."
- "Let's see if we can do this again. I mean, think about the message that that's sending. Two guys, alone together. Obviously, that's the candy that y'all should be mad at next."

# Oneliner

Conservatives upset over pronouns in Hershey's marketing reveal a trend of manufactured outrage losing traction.

# Audience

Social media users

# On-the-ground actions from transcript

- Support candy companies promoting inclusivity (implied).

# Whats missing in summary

Cultural commentary on manufactured outrage and shifting demographics influencing marketing strategies.

# Tags

#Candy #Conservatives #Marketing #Inclusivity #ManufacturedOutrage