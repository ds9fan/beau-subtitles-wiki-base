# Bits

Beau says:

- Contrasts critical thinking skills in the U.S. with what children in other countries are learning, particularly in relation to assembling and disassembling weapons.
- Mentions a science kit his kids received to learn about mechanics, but clarifies that it doesn't prepare them for specific careers like mechanics or race car driving.
- Addresses the circulating clips of children in foreign countries learning to field strip weapons in class, contrasting it with what U.S. kids are taught.
- Explains the simplicity of field stripping a weapon and its lack of relevance to becoming a warrior.
- Emphasizes the importance of critical thinking over mechanical skills for warfare and gives examples of critical thinking in combat scenarios.
- Talks about how understanding systemic injustice is vital for creating effective warriors and establishing rapport in foreign countries.
- Advocates for teaching children critical thinking about systems of injustice, technology, and the environment, rather than focusing on weapon-related skills.

# Quotes

- "If you want to create warriors, you have to teach people to critically think."
- "Nationalism is politics for basic people."
- "Sending people who think they're warriors because they know how to field strip a weapon is appalling."

# Oneliner

Beau contrasts critical thinking with mechanical skills, advocating for teaching children to understand systemic injustice over weapon-related training.

# Audience

Parents, educators, policymakers

# On-the-ground actions from transcript

- Teach children critical thinking skills (implied)
- Encourage understanding of systemic injustice, technology, and the environment (implied)
- Advocate for cooperative systems over competitive ones (implied)

# Whats missing in summary

The full transcript provides more context on the importance of critical thinking in preparing individuals for various challenges and the dangers of focusing solely on mechanical skills related to weaponry. Watching the full video can provide a deeper understanding of Beau's perspectives and examples. 

# Tags

#CriticalThinking #Education #SystemicInjustice #Warfare #Cooperation