# Bits

Beau says:

- Explains the role of the FDIC in covering failed banks and how it is essentially insurance for bank accounts.
- Mentions that recent bank failures have cost the FDIC around $23 billion, prompting the need to shore up their fund.
- Notes that the FDIC determines the assessment rates that banks have to pay, with the current plan being to have larger banks contribute more.
- Indicates that the goal is to ensure that smaller community banks are not heavily impacted by the fund shoring up efforts.
- Suggests that larger banks may face higher assessment rates compared to smaller banks, potentially shifting the burden away from community banks.
- Emphasizes that the FDIC seems to be aiming for larger banks to bail themselves out rather than passing the cost to Americans.
- Mentions the pressure on the FDIC to ensure that smaller banks do not bear the brunt of the fund restructuring.
- Points out that larger banks may see an increase in their insurance payments to cover the fund's stabilization.
- Concludes with the idea that industry leaders, i.e., big banks, are likely to be the ones primarily responsible for covering the costs of shoring up the FDIC fund.

# Quotes

- "The FDIC is insured by banks."
- "They're going to stick it to the big banks."
- "The FDIC is basically going to really make the banks bail themselves out."

# Oneliner

Beau explains how the FDIC is ensuring larger banks, not Americans, cover the costs of shoring up its fund, protecting smaller community banks from adverse impacts.

# Audience

Financial enthusiasts, bank customers.

# On-the-ground actions from transcript

- Contact your local community bank to understand how they may be affected by the FDIC's fund stabilization efforts (implied).
- Monitor news updates on the FDIC's assessments and fund restructuring to stay informed about potential impacts on different banks (implied).

# Whats missing in summary

Details on the potential consequences for larger banks and the broader banking industry from the FDIC's plan to have them bear the costs of fund stabilization. 

# Tags

#FDIC #Banks #Insurance #FinancialSecurity #CommunityBanks