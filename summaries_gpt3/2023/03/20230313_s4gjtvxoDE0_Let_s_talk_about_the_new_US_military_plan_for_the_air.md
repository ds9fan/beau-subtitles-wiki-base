# Bits

Beau says:

- Introduction to discussing the United States Air Force and its future.
- Mention of a new concept that was once seen as science fiction but is now on the brink of implementation.
- Possibility of purchasing around 1000 pilotless aircraft in the near future.
- Description of these aircraft as wingmen that will fly alongside fighter planes.
- Control of the drones either through AI, the fighter pilot, or from a distant observation aircraft.
- Purpose of the concept is to assign riskier tasks to drones, allowing human pilots to focus on primary objectives.
- Aim of these collaborative combat aircraft is to function effectively as wingmen while being cost-effective enough to be expendable.
- Reduced risk for pilots leads to potentially fewer mistakes and better focus on mission objectives.
- Plans for purchases to commence in the coming years, with a program named the Loyal Wingman program.
- The development could impact military strategies and make engagements with rival nations less costly.

# Quotes

- "More dog fights, less people."
- "The overall idea is to have these aircraft be capable enough to actually function as the wingman, but inexpensive enough to be something that can be lost."
- "If the pilot is not at risk, they are less likely to make mistakes."

# Oneliner

Beau talks about the potential future of the United States Air Force, including the use of pilotless aircraft as wingmen, aiming to enhance mission focus and reduce risks for pilots.

# Audience

Military enthusiasts, policymakers

# On-the-ground actions from transcript

- Research and stay informed about the developments in the United States Air Force's use of pilotless aircraft (implied).

# Whats missing in summary

Detailed insights on the potential ethical and strategic implications of shifting towards pilotless combat aircraft. 

# Tags

#UnitedStatesAirForce #FutureOfMilitary #PilotlessAircraft #CombatTechnology