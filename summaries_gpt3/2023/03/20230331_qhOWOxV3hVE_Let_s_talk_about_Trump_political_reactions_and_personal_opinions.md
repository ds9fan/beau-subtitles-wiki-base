# Bits

Beau Young says:

- Talks about his personal opinion of the Trump developments and addresses a question from a conservative that prompted the discussion.
- Appreciates the objective reporting on Trump's indictment and the calm demeanor despite being a never-Trump-er.
- States that based on publicly available information, the New York case against Trump seems to be the weakest among all he is facing.
- Mentions a high likelihood of two more indictments, which he believes are stronger cases than the one in New York.
- Expresses opposition to Trump beyond just being a Never Trump-er.
- Urges for objectivity when discussing legal matters and moving away from the theoretical and political commentary.
- Criticizes Republicans rushing to Trump's defense, stating that their words may backfire in the future.
- Points out that the indictment of a former President is a significant event that will have lasting political implications.
- Comments on Democrats potentially overplaying their hand and warns against assuming guilt before legal proceedings conclude.
- Stresses the importance of maintaining professionalism and presumption of innocence in legal matters.

# Quotes

- "I appreciate you immediately changing track and not dancing around celebrating Trump's indictment."
- "A former President of the United States was indicted. That's a big deal."
- "Assuming that he is going to be found guilty is not something that I think they should do."

# Oneliner

Beau Young addresses Trump's developments, criticizes Republicans rushing to his defense, and warns against premature assumptions of guilt by Democrats in a legal matter.

# Audience

Observers and commentators.

# On-the-ground actions from transcript

- Contact your representatives to urge them to prioritize professionalism and presumption of innocence in legal matters (implied).

# Whats missing in summary

Insights into the potential long-term effects of Trump's indictment on political discourse and the justice system.

# Tags

#Trump #Indictment #Politics #PresumptionOfInnocence #LegalSystem