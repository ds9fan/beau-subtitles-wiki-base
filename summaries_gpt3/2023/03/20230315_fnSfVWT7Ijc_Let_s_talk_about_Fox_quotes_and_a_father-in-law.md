# Bits

Beau says:

- Giving an overview of reaching out techniques and managing expectations when discussing sensitive topics, like politics.
- Describing a situation where someone attempted to talk to their father-in-law about news sources and misinformation.
- Explaining how the father-in-law reacted defensively, pivoting the topic and expressing distrust in all news sources.
- Expressing frustration at the father-in-law's reluctance to accept new information and change opinions.
- Asking for advice on how to break through the defensive rhetoric and make someone acknowledge credible reporting.
- Emphasizing that forcing someone to accept new information is not possible, but multiple gentle, informative interactions can lead to change.
- Suggesting the strategy of slowly introducing alternative news sources like the Associated Press (AP) to encourage critical thinking.
- Recommending showing multiple news outlets' coverage of a story to demonstrate credibility and encourage independent evaluation.
- Mentioning the importance of patience, multiple small dialogues, and allowing time for individuals to process information and change their views gradually.
- Stating that even small movements in changing someone's entrenched beliefs are considered progress in the right direction.

# Quotes

- "The truth is never told, it's realized."
- "It's not going to happen in one conversation."
- "Change is a process. It takes time."
- "If you're serious about it, you have to invest a lot of time in."
- "Just because you can get them to understand the baseless nature of a lot of the claims about the election, doesn't mean that they're going to become progressive."

# Oneliner

Beau shares techniques for gently guiding individuals away from misinformation by introducing diverse news sources gradually, acknowledging that change takes time and patience.

# Audience

Family members, community members

# On-the-ground actions from transcript

- Have multiple gentle and informative dialogues to help individuals transition away from misinformation (suggested).
- Introduce diverse news sources like the Associated Press (AP) to encourage critical thinking (suggested).

# Whats missing in summary

The emotional journey of navigating difficult political and misinformation-related conversations with loved ones is best experienced in full.

# Tags

#Family #Misinformation #Politics #CriticalThinking #Change #Community