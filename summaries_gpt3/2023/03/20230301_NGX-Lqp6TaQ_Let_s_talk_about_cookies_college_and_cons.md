# Bits

Beau says:

- Girl Scouts started Troop 6000 in 2017 to cater to children in New York City shelters, providing activities and support during their stay.
- Troop 6000 helps children transition to permanent housing, with the average stay in NYC temporary shelters being about 18 months.
- Support Troop 6000 by buying Girl Scout Cookies online; a link will be provided in the description to order at least four boxes.
- Project Rebound at California State University, Northridge, supports formerly incarcerated individuals in pursuing higher education.
- From 2016 to 2020, participants in Project Rebound had an impressive 3.0 GPA and a 0% recidivism rate.
- Donate to Project Rebound through a link provided below to contribute to college scholarships for program participants.
- The scholarship fund at Project Rebound requires 50 donors to unlock around $5,000 for college scholarships.
- Both programs, Troop 6000 and Project Rebound, are doing commendable work despite being geographically apart.
- Project Rebound's 0% recidivism rate is particularly noteworthy and speaks to the program's success in supporting formerly incarcerated individuals.
- Despite any reservations one may have about the Girl Scouts organization, supporting Troop 6000 through cookie purchases can make a significant difference to the children in shelters.

# Quotes

- "Support Troop 6000 by buying Girl Scout Cookies online."
- "Project Rebound boasts a 0% recidivism rate, a remarkable achievement."

# Oneliner

Support Troop 6000 and Project Rebound by buying Girl Scout Cookies online and donating to support formerly incarcerated individuals' college scholarships.

# Audience

Supporters and donors

# On-the-ground actions from transcript

- Buy Girl Scout Cookies online to support Troop 6000 (suggested)
- Donate to Project Rebound for college scholarships (suggested)

# Whats missing in summary

The full transcript provides additional details on the impactful work of Troop 6000 and Project Rebound, encouraging support through specific actions like purchasing cookies and making donations.

# Tags

#GirlScouts #Troop6000 #ProjectRebound #Support #Donations