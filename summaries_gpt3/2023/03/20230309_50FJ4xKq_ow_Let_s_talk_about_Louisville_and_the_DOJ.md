# Bits

Beau says:

- Louisville agreed to make corrections to their law enforcement based on DOJ Civil Rights Division recommendations.
- The DOJ found a culture within the department violating rights through invalid warrants, excessive force, and discriminatory behavior.
- Recommendations include constant body camera footage reviews, more oversight, and outside assistance for decision-making.
- The hope is for reforms, retraining or termination of individuals, and implementation of all recommendations.
- The Department of Justice can step in if reforms are not made, with extreme measures like taking over the department.
- Similar agreements have taken place in other cities like Ferguson, with Memphis next in line.
- The catalyst for these actions was Breonna Taylor's death, revealing a pattern of rights violations.
- This process aims for serious reform if acted upon in good faith, otherwise risking departmental overhaul.
- It signifies the start of change in law enforcement culture, aiming to address major issues.
- Despite not being a perfect solution, it is a step towards reform and has the potential to save lives.

# Quotes

- "This is a start. And it shouldn't be ignored because it's not the perfect solution."
- "It's a move in the right direction."
- "Those are kind of the two extremes when it comes to the outcomes."
- "This is a case where the bad apples spoiled the bunch."
- "It's good news, but it's not the end of the story."

# Oneliner

Louisville agrees to DOJ recommendations for law enforcement reform after rights violations, signaling the start of serious change with Memphis next in line.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Support and monitor the implementation of recommended reforms by law enforcement in Louisville and upcoming in Memphis (implied)
- Advocate for continued oversight and accountability in law enforcement practices (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the agreement between Louisville and the DOJ, the need for reform in law enforcement practices, and the potential for significant change through good faith action.

# Tags

#Louisville #DOJ #lawenforcement #reform #civilrights #Memphis