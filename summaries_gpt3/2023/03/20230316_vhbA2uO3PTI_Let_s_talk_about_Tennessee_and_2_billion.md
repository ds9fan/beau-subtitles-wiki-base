# Bits

Beau says:

- Tennessee is considering legislation that will make it impossible to change gender markers on official documents, essentially locking people into their birth sex.
- The bill's goal seems to be to prevent certain groups, like trans people, from claiming discrimination by removing them as a protected class.
- If the bill becomes law, Tennessee could lose billions in federal funding due to creating a mechanism for discrimination that goes against federal policies.
- While legally Tennessee might get away with this move, it could have significant financial consequences that its supporters might not fully grasp.
- Beau questions the motives of politicians in Tennessee and whether they are willing to risk losing substantial federal funding just to create a false enemy for political gain.
- The potential loss of two billion dollars in federal money due to this legislation is significant.
- Beau points out the moral wrongness of the bill but acknowledges that in the United States, financial considerations often carry more weight than morality.
- Beau underscores the importance of monitoring the situation closely to see how it unfolds.

# Quotes

- "This is something that the state can probably legally do and actually get away with."
- "Two billion dollars, it's a lot of money and that's really kind of what they're looking at losing."
- "But I think in this case, it's the United States. It runs on money."
- "The loss of funding may be a stronger argument in Tennessee than morality."
- "Anyway, it's just a thought, y'all have a good day."

# Oneliner

Tennessee's legislation to lock gender markers may cost billions in federal funding, prioritizing money over morality.

# Audience

Tennessee residents, Activists

# On-the-ground actions from transcript

- Monitor the progress of the legislation in Tennessee and stay informed about its potential impact (implied).
- Advocate against discriminatory legislation by raising awareness and mobilizing support (implied).
- Support organizations and groups working to protect the rights of transgender individuals in Tennessee (implied).

# Whats missing in summary

The full transcript provides more context on the financial and moral implications of Tennessee's proposed legislation, offering a comprehensive view of the potential consequences beyond just federal funding.

# Tags

#Tennessee #Legislation #TransgenderRights #FederalFunding #Discrimination