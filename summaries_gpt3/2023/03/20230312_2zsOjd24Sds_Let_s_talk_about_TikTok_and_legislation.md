# Bits

Beau says:

- Explains how the U.S. government is addressing concerns about TikTok through the RESTRICT Act.
- Congress and the White House are creating new powers within the Commerce Department to regulate technologies from foreign countries that pose security risks.
- The mechanism is designed to avoid directly targeting TikTok to prevent political backlash.
- The Commerce Department will be granted the authority to make decisions independently to shield elected officials from scrutiny.
- This approach mirrors how unpopular decisions are handled, like military base closures, through committees to avoid upsetting the public.
- Acknowledges that tech experts have raised concerns about TikTok's data security issues, which could potentially lead to the development of user profiles.
- The RESTRICT Act has significant support and is likely to pass, signaling potential trouble for TikTok in the future.
- Once the Commerce Department gains authority, they may impose restrictions on TikTok, affecting its future operations.

# Quotes

- "The idea is to give the Commerce Department the capability to regulate and do anything up to including ban technologies from foreign countries that may pose a security risk."
- "This is a way to implement unpopular decisions."
- "It's probably something that is going to go through so it doesn't look good for TikTok in the long term."
- "Once Commerce, the Department of Commerce actually has the authority, it's no longer in Congress's hands."
- "Odds are Biden will sign it."

# Oneliner

The U.S. government aims to address TikTok concerns through the RESTRICT Act, granting the Commerce Department power to regulate technologies and potentially impacting TikTok's future operations.

# Audience

Tech Users, Policymakers

# On-the-ground actions from transcript

- Monitor updates on the RESTRICT Act and its implications for TikTok (suggested).
- Stay informed about potential regulatory changes affecting technology platforms (implied).

# Whats missing in summary

Insights on potential implications for user data protection and privacy rights.

# Tags

#TikTok #USGovernment #CommerceDepartment #DataSecurity #Regulation