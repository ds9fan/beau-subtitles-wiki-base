# Bits

Beau says:

- Introduces the topic of water and the EPA's new regulations on PFAS chemicals.
- PFAS chemicals are described as forever chemicals due to their slow degradation and water-repellent properties.
- These chemicals are contaminating the nation's water supply, impacting around 200 million people in the U.S.
- The EPA is focusing on six specific PFAS chemicals, implementing new regulations and treatment procedures.
- Four out of five states took no effective action against these harmful chemicals, prompting EPA intervention.
- Questions arise about funding for addressing the issue, with the Biden infrastructure bill allocating some billions for it.
- While the dedicated funds will help, more resources will be required over time to combat water contamination effectively.
- Emphasizes the daily necessity of water and the importance of immediate action rather than delays.
- Suggests that investing in solving water contamination issues is vital and not just a matter of throwing money at the problem.
- Anticipates ongoing news coverage as different regions decide on implementation and potential legal challenges.
- Encourages staying informed through local and state news outlets for updates on this significant issue.

# Quotes

- "They're called that because they don't degrade quickly, and they repel water. It's just all bad."
- "This may not be something that we want to delay and delay and delay."
- "When the problem is a shortage of funding, throwing money at it is like super helpful."
- "So look to state coverage if this is a topic you're really interested in."
- "Y'all have a good day."

# Oneliner

Beau addresses water contamination by PFAS chemicals, urging immediate action due to their harmful effects on millions of Americans and the importance of ongoing funding for long-term solutions.

# Audience
Environmental activists, concerned citizens

# On-the-ground actions from transcript

- Stay informed through local and state news coverage (suggested)
- Advocate for sufficient funding and resources to combat water contamination (implied)

# Whats missing in summary

The full transcript provides more in-depth information on PFAS chemicals, water contamination, and the EPA's regulations, offering a comprehensive understanding of the issue.