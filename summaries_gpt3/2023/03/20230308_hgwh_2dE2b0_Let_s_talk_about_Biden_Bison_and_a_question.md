# Bits

Beau says:

- The Inflation Reduction Act includes funds for the environment, specifically to lift bison populations.
- The Secretary of the Interior plans to transfer bison from federal to tribal land for tribal leadership in population restoration.
- Bison were once nearly extinct due to hunting, but their numbers are now in the tens of thousands.
- Bison are a keystone species and can positively impact ecosystems as they move and multiply.
- Tribal governments are expected to play a key role in protecting bison populations better than the US government historically has.
- Tribal governments are more invested economically and environmentally in ensuring positive outcomes.
- The Inflation Reduction Act's initiative involving bison is predicted to have a positive impact in the long run.
- Beau expresses confidence in the program's success and believes it will be a significant win once fully realized.

# Quotes

- "Tribal governments will do a better job at protecting the bison than the US government overall."
- "This is going to be a win."
- "It's just getting started, it's too early to call it that."

# Oneliner

The Inflation Reduction Act supports tribal-led efforts to restore bison populations, likely leading to positive environmental impacts and a win in the long run.

# Audience

Environmental advocates, tribal communities

# On-the-ground actions from transcript

- Support tribal-led conservation efforts (implied)
- Stay informed about environmental initiatives involving keystone species like bison (implied)

# Whats missing in summary

The full transcript provides deeper insights into the importance of tribal involvement in conservation efforts and the potential positive outcomes for bison populations and ecosystems.