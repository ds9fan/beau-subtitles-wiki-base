# Bits

Beau says:

- Reflections on lessons learned and moving forward from 2016, as similar dynamics are playing out in the lead-up to 2024.
- Far-right targeting specific demographics through legislation, rhetoric, and actions, with rhetoric playing a critical role in legitimizing their agenda.
- A technique used by the far-right is saying something extreme, then walking it back to a slightly less extreme position to make critics seem unreasonable.
- Normalization of extreme positions through this technique, making it critical to challenge and question their rhetoric.
- A strategy to deal with racist jokes or extreme rhetoric is to pretend not to understand, forcing the person to explain and reveal the true nature of their statement.
- By questioning and getting details on extreme positions, support can be eroded as people become less interested and supportive.
- Connecting extreme statements to potential violations of the Constitution can be a powerful way to challenge and counter their narrative.
- Emphasizing the impact on constitutional rights, especially the First and Second Amendments, can help appeal to conservatives who value constitutional principles.
- Continuously questioning and dragging out extreme rhetoric is key to exposing the true intentions behind seemingly toned-down statements.
- The importance of pushing back, challenging, and unraveling extreme rhetoric to prevent its normalization and gain broader support against such tactics.

# Quotes

"Pretend like you don't get it."
"If you can't be trusted with the First Amendment, why should we trust you with the Second?"
"People don't want anything to do with it."

# Oneliner

Lessons from 2016: Challenge extreme rhetoric by continuously questioning and exposing the true intentions behind seemingly toned-down statements.

# Audience

Activists, Progressives, Concerned Citizens

# On-the-ground actions from transcript
- Challenge extreme rhetoric by continuously questioning and exposing the true intentions (suggested)
- Connect extreme statements to potential violations of the Constitution to counter the narrative (implied)
- Appeal to conservatives by emphasizing the impact on constitutional rights, especially the First and Second Amendments (implied)

# Whats missing in summary

In-depth analysis and examples from the full transcript can provide a more comprehensive understanding of how to effectively counter extreme rhetoric and tactics.

# Tags

#LessonsLearned #ExtremeRhetoric #ConstitutionalRights #Challenge #FarRight