# Bits

Beau says:

- Eli Lilly and Biden's unexpected good news: capping insulin at $35 per month out-of-pocket expenses.
- Eli Lilly bringing their program in line with the Inflation Reduction Act for all, not just seniors.
- Private insurance automatically applying the $35 cap, while uninsured individuals can register for Eli Lilly's Co-Pay Assistance Program.
- Biden administration likely to take credit for the connection between the Inflation Reduction Act and the insulin cost reduction.
- Eli Lilly's decision likely to pressure other companies in the market to follow suit due to their significant market control.
- Potential for other major companies to announce similar cost reductions, ultimately lowering insulin prices nationwide.
- Lilly's proactive approach to adapt voluntarily before facing heavy regulations amidst shifting demographics and increasing calls for regulation.
- Addressing overpriced medications as a first step towards more affordable healthcare and a shift away from profit-driven patient care.
- Importance of universal healthcare access to move towards a system where everyone is entitled to healthcare, not just the wealthy.
- Encouragement for diabetics to stay informed about the cost reduction and be aware of the company producing their insulin product.

# Quotes

- "Insulin is going to be capped at $35 per month out of pocket expenses."
- "This is a huge win."
- "It's kind of like a peace offering."
- "Once it's recognized as something that everybody should have, rather than, well, that's just something that rich folk get."
- "Y'all have a good day."

# Oneliner

Eli Lilly and Biden team up to cap insulin costs at $35 per month, signaling a potential industry-wide shift towards more affordable healthcare for all.

# Audience

Advocates for affordable healthcare

# On-the-ground actions from transcript

- Stay informed about the cost reduction and updates on insulin pricing (suggested)
- Identify the company producing your insulin product and monitor for cost changes (suggested)

# Whats missing in summary

The full transcript provides detailed insights into the impact of Eli Lilly's decision on insulin pricing and the broader implications for healthcare affordability and regulation.

# Tags

#Healthcare #Affordability #Insulin #Biden #EliLilly