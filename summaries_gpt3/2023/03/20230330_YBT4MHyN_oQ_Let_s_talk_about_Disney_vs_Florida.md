# Bits

Beau says:

- Disney and Florida are at odds over the management of the area surrounding Disney World.
- For over half a century, Disney had control through a special district, but Florida wanted to change that.
- Beau doubts if Florida's decision-makers have ever met Disney's lawyers and describes them as cold and calculating.
- Disney needs control to maintain the perfection and brand of Disney World.
- Disney's lawyers struck a deal giving Disney control over most things, surprising the new board.
- The deal is likely to stand as everything was done in the open, even though it went unnoticed.
- Florida may not have much recourse to challenge the deal, which gives control to Disney in perpetuity.
- The deal's length and implications are uncertain and may fuel future conflicts.
- The situation between Disney and Florida is set to continue brewing.

# Quotes

- "Disney needs control to maintain the perfection and brand of Disney World."
- "Florida may not have much recourse to challenge the deal."
- "The situation between Disney and Florida is set to continue brewing."

# Oneliner

Disney and Florida clash over control of Disney World's surroundings, with Disney's lawyers striking a surprising deal that may have lasting effects.

# Audience

Florida residents, Disney enthusiasts

# On-the-ground actions from transcript

- Contact local representatives to voice concerns about the deal and its implications (suggested)
- Stay informed about any updates or changes regarding the management of Disney World's surroundings (implied)

# Whats missing in summary

Additional context on the potential long-term impacts of the deal and how it may affect the overall Disney World experience.

# Tags

#Disney #Florida #Control #Management #Deal #LegalIssues