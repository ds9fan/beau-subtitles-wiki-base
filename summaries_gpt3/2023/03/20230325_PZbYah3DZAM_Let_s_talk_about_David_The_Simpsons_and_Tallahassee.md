# Bits

Beau says:

- A school principal in Tallahassee was fired because sixth-grade students saw images of Michelangelo's David and deemed it inappropriate by some parents.
- The connection to a Simpsons episode is made where Marge, an artist, disagrees with censoring Michelangelo, showing how the current world is beyond parody.
- The Simpsons episode aired 30 years ago, showing David from the back and front, something widely watched by children back then but deemed inappropriate now.
- Shielding students from Michelangelo's David in a school promising a classical education is mind-boggling and shows a lack of embracing creativity and art education.
- Beau questions the wildness in schools and the challenges for administrators and teachers in determining what they can teach.
- Art education and embracing creativity are vital for children to understand the world.
- The principal who was fired was given the option to resign but refused.

# Quotes

- "We are shielding our children from the wrong things."
- "A lack of art, a lack of art education, a lack of embracing creativity is going to lead to children growing up who do not understand the world."

# Oneliner

Firing a principal over Michelangelo's David in a school promising a classical education sparks a debate on censorship and the importance of art education.

# Audience

Parents, Educators, School Administrators

# On-the-ground actions from transcript

- Support art education in schools by advocating for comprehensive art programs (implied)
- Encourage creativity and open-mindedness among students by exposing them to various forms of art (implied)

# Whats missing in summary

The full transcript provides a deep dive into the importance of art education and creativity in schools and society. Viewing the entire transcript offers a more detailed analysis and insight into the issue.

# Tags

#ArtEducation #Censorship #Schools #Creativity #Michelangelo #TheSimpsons