# Bits

Beau says:

- Explains how individuals can create deep systemic change within the United States even if they can't physically participate in street protests.
- Emphasizes the importance of diversity of tactics, strategy, and shifting the Overton Window to achieve the grand goal of ending unacceptable things.
- Points out that disrupting business through street protests grabs the attention of the government, which is closely tied to business interests.
- Acknowledges that while small concessions from the government are helpful, the ultimate goal is changing societal thought.
- Defines the Overton Window as the acceptable range of political thought and stresses the need to shift it to make certain ideas unacceptable.
- Encourages individuals to use their influence on social media and in everyday interactions to change thought and challenge covert racism.
- Urges people who can't physically participate in protests to still play a vital role in changing societal perceptions and pushing unacceptable ideas out of the Overton Window.
- Suggests using hobbies, such as creating art or memes, to contribute to shifting the Overton Window and promoting social change.
- Stresses that any action taken to shift societal thought is valuable, regardless of the form it takes.
- Advocates for engagement in letter writing, signing petitions, contacting legislators, and supporting bail funds as ways to support street actions and create change.

# Quotes

- "The legislation is just a tool to help [change societal thought]."
- "You don't need combat boots and a mask to be involved in this campaign."
- "Gotta push it all the way out. Gotta get it out of the window completely."
- "Make it unacceptable in every way. Every form of covert racism."

# Oneliner

Beau explains how individuals can contribute to deep systemic change by shifting societal thought and pushing unacceptable ideas out of the Overton Window, even without physically participating in protests.

# Audience

Activists, Allies, Community Members

# On-the-ground actions from transcript

- Use your social media accounts to influence thought by discussing privilege and the need for a police database (implied).
- Call out covert racism and challenge unacceptable ideas in everyday interactions (implied).
- Support bail funds and other organizations involved in street actions (implied).
- Write letters to the editor, sign petitions, contact legislators, and vote with your dollars to support social change (implied).

# Whats missing in summary

The full transcript provides detailed insights into how individuals can make a significant impact on societal change through various actions, including influencing thought, challenging unacceptable ideas, and supporting street actions.

# Tags

#SystemicChange #OvertonWindow #SocialActivism #Influence #CommunityEngagement