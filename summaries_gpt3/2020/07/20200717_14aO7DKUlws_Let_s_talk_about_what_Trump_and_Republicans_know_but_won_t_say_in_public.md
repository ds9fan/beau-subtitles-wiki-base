# Bits

Beau says:

- The Republican National Convention in Florida will have 2,500 attendees, with a cap of 7,000 for guests.
- Originally scheduled in North Carolina with an anticipated 50,000 attendees, now reduced to 2,500 attendees.
- Republicans moved the convention due to attendance concerns but have drastically reduced numbers themselves.
- Despite taking precautions for their own safety, Republicans continue to push for normalcy and downplay risks to their supporters.
- Beau notes the indifference to human suffering and the disconnect between actions taken and messages being conveyed.
- Republicans seem more focused on post-election outcomes rather than the current health crisis.
- Trump's ego played a significant role in decisions around convention attendance and location.
- The convention is portrayed as more of a party or show rather than a serious decision-making event.
- Beau questions the disregard for science in the face of holding a large-scale event during a pandemic.

# Quotes

- "Nobody wants to be a tough guy until it's time to go through the door."
- "They're banking on it not getting out of hand until after the election."
- "He has to know how stupid he looks, moving the entire convention to satisfy his ego."

# Oneliner

The Republican National Convention in Florida drastically reduces attendance while maintaining misleading messages on COVID-19 risks.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Hold accountable political leaders who prioritize ego over public health (exemplified)
- Advocate for science-based decision-making in public events (exemplified)

# Whats missing in summary

The full transcript provides a deeper insight into the disconnect between actions taken for safety and the contradictory messaging on COVID-19 risks.