# Bits

Beau says:

- Explains the transition from peaceful protests to more intense movements in the streets.
- Describes the typical progression where an overreaction by authorities provokes a reaction.
- Talks about how individuals join movements not just for grievances but also moral reasons.
- Outlines the cycle of escalation and unintended incidents leading to splits within the movement.
- Details the five elements that movements typically split into: leadership, soldiers, underground, auxiliary, and mass base.
- Defines each element with examples like soldiers being those who pick up arms.
- Mentions differences in terminology in older manuals where auxiliary and mass base were combined as sympathizers.
- Emphasizes the importance of understanding these dynamics for insight into global events and transitions.
- Gives a hypothetical example of how different groups like Green Berets or activists fit into these elements.
- Concludes with the idea that this knowledge is valuable for understanding events both domestically and internationally.

# Quotes

- "It's almost never intended on the part of the establishment, on the part of the government. They don't mean to do it, but they do."
- "A lot of times it's kind of imperceptible to the people inside of it, until it's not."
- "Those who become soldiers, these are typically people without families. These are typically people who maybe they have a record."

# Oneliner

Beau explains the progression of movements from discontent to intensity, detailing the split into five elements and the roles within, offering insights into global events.

# Audience

Activists, organizers, researchers

# On-the-ground actions from transcript

- Analyze and understand the dynamics of movements and protests (suggested)
- Foster understanding of global events and transitions (suggested)

# Whats missing in summary

Insights into the importance of understanding movement dynamics and transitions for broader context and global events.

# Tags

#Protests #MovementDynamics #GlobalEvents #Leadership #Community