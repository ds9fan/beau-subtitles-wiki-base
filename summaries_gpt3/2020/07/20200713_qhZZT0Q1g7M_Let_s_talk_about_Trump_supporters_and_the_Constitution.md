# Bits

Beau says:

- Questioning which party in the United States truly supports the Constitution by examining if they encourage supporters to live up to its ideas.
- Testing politicians who claim to be defenders of the Constitution on whether they support and defend its contents, not just its symbol.
- Exploring different aspects of the Constitution like establishing justice, ensuring domestic tranquility, promoting general welfare, and securing liberty.
- Criticizing the response of sending in troops to maintain domestic tranquility instead of addressing underlying issues.
- Emphasizing the importance of promoting general welfare by opposing systemic issues that hinder prosperity.
- Calling out the Republican Party for valuing the symbol of the Constitution more than its principles.
- Asserting that it is a constitutional duty to address systemic racism, support fellow citizens in crises, and advocate for equality.
- Urging individuals to focus on their responsibilities rather than just their rights for the betterment of society.

# Quotes

- "You have a constitutional duty to say that black lives matter."
- "Stop being so concerned with your rights and be concerned about your responsibilities because you're letting us all down."
- "You're failing."

# Oneliner

Questioning if political parties truly uphold the Constitution by examining actions over symbols, Beau challenges individuals to prioritize responsibilities over rights for societal progress.

# Audience

American voters

# On-the-ground actions from transcript

- Join movements advocating for systemic change (implied)
- Support marginalized communities (implied)
- Prioritize community welfare over personal rights (implied)

# Whats missing in summary

Beau's passionate call for societal change through prioritizing responsibilities and actively supporting the principles of the Constitution.

# Tags

#Constitution #PoliticalParties #Responsibility #SystemicChange #Equality