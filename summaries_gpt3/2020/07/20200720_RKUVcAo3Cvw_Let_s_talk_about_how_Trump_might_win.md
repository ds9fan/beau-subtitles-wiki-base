# Bits

Beau says:

- Trump's re-election strategy involves powerful displays of American symbols like Rushmore and the flag to make people comfortable and safe by showing control within his loyal circle.
- The campaign will heavily focus on supporting the troops, which will now include militarized law enforcement, as part of a law and order platform.
- National security issues, particularly related to China, will be a recurring theme throughout the campaign.
- Trump will intertwine religious imagery and rhetoric, reinforcing traditional family roles and creating a narrative dominated by strong men.
- He aims to undercut labor unions, especially teachers' unions, to protect corporate power.
- Trump's strategy involves identifying the opposition as the radical left and promising mass arrests and long prison sentences.
- He plans to suppress the vote through actions like attacking mail-in voting and shutting down polling places in areas favoring Democrats.
- Trump attempts to control the narrative by discrediting any opposition, labeling them as fake news and threatening lawsuits against them.
- The campaign strategy shows characteristics of fascism, such as powerful nationalism, disdain for human rights, identification of enemies, supremacy of the military, and rampant sexism.
- Corporate power is protected, while labor power is suppressed, with an overall focus on control, corruption, and fraudulent elections.

# Quotes

- "He's literally running a campaign, a referendum on fascism."
- "It's no longer a question of it can't happen here or even admitting that it can happen here. It is happening here right now."

# Oneliner

Trump's re-election strategy focuses on powerful nationalism, militarized law enforcement support, and suppressing opposition, resembling characteristics of fascism.

# Audience

Voters, Activists

# On-the-ground actions from transcript

- Contact local election officials to ensure fair access to voting (suggested)
- Join or support organizations advocating for voter rights and access (exemplified)
- Organize community efforts to increase voter turnout in areas facing suppression tactics (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of Trump's re-election strategy and the concerning similarities to fascism, urging vigilance and action against authoritarian tactics.

# Tags

#Trump #Reelection #Fascism #VoterRights #PoliticalStrategy