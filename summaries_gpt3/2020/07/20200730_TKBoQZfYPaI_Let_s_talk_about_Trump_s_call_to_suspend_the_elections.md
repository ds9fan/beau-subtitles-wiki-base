# Bits

Beau says:

- President Donald Trump called for the suspension of elections, citing concerns about universal mail-in voting leading to inaccuracies and fraud.
- Trump's call to delay the elections until vague preconditions are met mirrors tactics used by dictators in history to stay in power.
- Beau criticizes the Republican Party for not openly opposing Trump's call to delay the elections, accusing them of undermining the Constitution.
- Beau urges Republicans to stand up against Trump's actions to protect American freedom, warning that history will judge them based on their response.
- He calls on Republican leadership to deny Trump the nomination, describing the situation as the most significant threat since 1860.
- Beau accuses Trump of displaying fascist characteristics and attempting to subvert the Constitution through his statements on social media.
- He warns that those who continue to support Trump are betraying American values and endangering the country's well-being.
- Beau concludes by stating that supporting Trump is incompatible with supporting the United States, implying a moral and patriotic duty to oppose him.

# Quotes

- "The silence from the Republican Party has been deafening."
- "If you still support Donald Trump, you do not support the United States."

# Oneliner

President Trump's call to delay elections, likened to dictator tactics, prompts Beau to urge Republicans to oppose him, warning of a threat to American freedom and values.

# Audience

Republican Party members

# On-the-ground actions from transcript

- Stand in open opposition to Trump's call to delay elections (suggested)
- Advocate within the Republican Party leadership to deny Trump the nomination (suggested)

# Whats missing in summary

The emotional intensity and urgency conveyed by Beau's plea for Republicans to take a stand against Trump can be fully grasped by watching the full transcript.