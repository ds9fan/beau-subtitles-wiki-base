# Bits

Beau says:

- Trump's convention in Jacksonville was moved virtually due to crowd limitations.
- Simultaneously, the administration pushes for schools to reopen, focusing on a quote about opening schools.
- The quote implies the critical importance of opening schools but overlooks the associated guidelines and resources.
- Guidelines suggest measures like regular hand washing, wearing masks, and disinfecting classrooms and buses.
- Schools are expected to implement social distancing, provide supplies, and broadcast health announcements.
- The guidelines propose unrealistic expectations, such as maintaining distance in classrooms and buses.
- Students are required to bring their own food and water, contradicting the issue of student poverty.
- Beau questions the feasibility of the guidelines and expresses concerns about the lack of leadership in ensuring safety.
- He criticizes the reliance on schools for addressing poverty and stresses the need for innovation in the education system.
- Beau calls for a new approach to education that does not burden teachers and administrators with unrealistic demands.

# Quotes

- "This is going to be really hard."
- "It's time to innovate."
- "We need something new."

# Oneliner

Trump's convention went virtual, while guidelines push for school reopenings with unrealistic expectations, prompting Beau to call for innovation in the education system.

# Audience

Educators, parents, activists

# On-the-ground actions from transcript

- Donate supplies like soap, hand sanitizer, and disinfectant wipes to schools (implied)
- Advocate for adequate funding for schools and support teachers (implied)
- Collaborate with local communities to address educational challenges (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the impractical guidelines for reopening schools amidst the COVID-19 pandemic, calling for a critical reevaluation of the education system and leadership in ensuring safety and innovation.

# Tags

#Trump #SchoolReopenings #EducationSystem #Safety #Innovation