# Bits

Beau says:

- Florida plans to reopen schools in person in August, five days a week, despite the ongoing situation.
- The decision to reopen schools seems to be based on unclear reasoning and not on data.
- Beau criticizes the lack of understanding and decision-making skills of those in charge.
- He questions the safety guidelines provided by the Governor and contrasts them with a typical school day scenario.
- The school environment poses challenges for following safety protocols like wearing masks and social distancing.
- The decision to reopen schools now, amidst a surge, lacks a logical basis.
- Beau advocates for having distance learning options ready as a more critical measure.
- He urges Floridians to prioritize safety and make informed decisions regarding sending children to school.
- Beau warns the ruling party in Tallahassee about the potential consequences of their decision on re-election.
- He concludes by suggesting that the decision-makers may be jeopardizing their chances for re-election by ignoring public safety concerns.

# Quotes

- "So we're just gonna go through an average day at a school and compare that to the guidelines."
- "There's no data to back up this decision. None."
- "We are Florida man. We are Florida woman. We do not need Tallahassee to make the right decision."
- "If you look at that data come August and you don't feel that your child would be safe, don't send them, homeschool."
- "You are quite literally killing your chances for re-election."

# Oneliner

Florida plans to reopen schools in August without clear data or rationale, posing risks to students and communities, urging individuals to prioritize safety over government mandates.

# Audience

Parents, educators, community members

# On-the-ground actions from transcript

- Monitor local school board decisions and advocate for safer alternatives (implied)
- Stay informed about the situation in your area and make decisions based on safety (implied)

# Whats missing in summary

The emotional impact of potentially risking the health and safety of students, families, and communities by rushing to reopen schools without adequate preparation or consideration for the ongoing situation.

# Tags

#Florida #Schools #Reopening #Safety #CommunitySafety #DecisionMaking