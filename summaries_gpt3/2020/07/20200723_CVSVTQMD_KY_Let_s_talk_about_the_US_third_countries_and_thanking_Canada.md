# Bits

Beau says:

- Explains the Safe Third Country Agreement between the US and Canada, governing immigration policies, particularly for asylum seekers.
- Notes that if someone arrived in the US first, they couldn't claim asylum in Canada at a traditional crossing.
- Mentions a dangerous loophole where individuals could cross into Canada through unmanned areas.
- Points out the danger of such crossings, especially for those traveling with children.
- Canadian court declared the agreement unconstitutional due to the US not being a safe third country anymore.
- Emphasizes that the US has been ignoring treaties and breaking its own laws related to asylum seekers.
- States that issues with asylum seekers predate the Trump administration, with Trump exacerbating the situation.
- Indicates the need for reforming policies beyond just returning to the status quo.
- Stresses the importance of honoring agreements entered into by the country.
- Expresses gratitude towards the Canadian courts for acknowledging the issues and pushing for necessary reforms.
- Acknowledges that the fight for asylum seekers' rights is not over despite the court ruling.
- Hopes that the Canadian government won't appeal the decision and will recognize the US's failure to adhere to international law.
- Appreciates Canada stepping up to potentially save lives through their actions.
- Concludes by noting the ongoing work required beyond removing Trump from office.

# Quotes

- "The US is not a safe third country anymore."
- "It's not a detriment. It's not something that's going to throw open the borders of Canada or anything like that."
- "This will save lives."

# Oneliner

Beau explains the implications of the overturned Safe Third Country Agreement between the US and Canada, urging for policy reform to honor international agreements and ensure asylum seekers' rights.

# Audience

Policy reform advocates

# On-the-ground actions from transcript

- Advocate for policy reform to ensure the protection of asylum seekers (implied).

# Whats missing in summary

The full video provides a deeper understanding of the historical context and implications of the Safe Third Country Agreement, along with the ongoing challenges faced by asylum seekers in the US and Canada.

# Tags

#US #Canada #AsylumSeekers #ImmigrationPolicy #InternationalAgreements