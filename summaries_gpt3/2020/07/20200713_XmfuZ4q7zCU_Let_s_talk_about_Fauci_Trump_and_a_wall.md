# Bits

Beau says:

- Private citizens raised money to build a wall on the border, but the wall is now unstable and may collapse into the river soon.
- Trump's reaction to negative news is to distance himself from it and reject reality.
- He mentions Trump's pattern of not taking responsibility, blaming others, and politicizing situations.
- Beau contrasts how a smart leader might respond to failure with Trump's actual response.
- The White House is discrediting Dr. Fauci, who has consistently based his opinions on the best available evidence.
- Beau criticizes Trump for prioritizing politics over dealing with the current crisis effectively.
- He points out the economic consequences of Trump's actions and the impact on other nations.
- Beau stresses the importance of acting responsibly during the ongoing crisis and not relying on guidance from authorities.
- He urges people to stay at home, wash hands, wear masks, and take necessary precautions to prevent the further spread of the virus.

# Quotes

- "His decisions, well, they're not his fault. They'll always be somebody else's fault."
- "Our obligation to humanity does not end at the border."
- "Stay at home as much as you can. Wash your hands. Don't touch your face."
- "When he gets bad news, he finds some way to distance himself from it, to reject reality and substitute his own."
- "We're all in this together."

# Oneliner

Private citizens' wall collapses, Trump distances himself, Fauci discredited, economy at risk—Beau urges action in crisis.

# Audience

American citizens

# On-the-ground actions from transcript

- Stay at home as much as you can. Wash your hands. Don't touch your face. (suggested)
- Wear a mask if you have to go out. (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's response patterns, the treatment of Dr. Fauci, and the need for individual action during the crisis.