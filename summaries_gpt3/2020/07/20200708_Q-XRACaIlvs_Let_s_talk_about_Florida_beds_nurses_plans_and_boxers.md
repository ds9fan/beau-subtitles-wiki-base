# Bits

Beau says:

- Talking about Florida again due to unforeseen events unfolding.
- Governor DeSantis plans to reopen schools in August for in-person learning despite concerning numbers.
- Florida is facing a critical shortage of ICU beds and nurses.
- Despite the crisis, the leadership is reluctant to change their plan.
- Beau draws a comparison between facing a crisis and being a boxer getting punched in the face.
- Urges for a change in strategy to avoid getting knocked out by the current situation.
- Emphasizes the need to listen to medical experts and follow simple guidelines like staying at home and wearing masks.
- Criticizes the government's failure to provide effective leadership during the crisis.
- Calls for community action and solidarity in the absence of government support.
- Stresses the importance of individuals taking responsibility for their actions to combat the situation effectively.

# Quotes

- "Every boxer will tell you that when you step into that ring you have a plan until you get punched in the face."
- "We are Floridians. We know that after a hurricane the government's pretty much useless."
- "We have to lead ourselves."
- "Stay at home. Do what you can to stay at home."
- "If you have a boss that tells you not to wear a mask or that you can't do it in their store or whatever, send me a message."

# Oneliner

Beau talks about Florida's crisis, urging a change in plan to combat the shortage of ICU beds and nurses, and stresses community action amid government failure.

# Audience

Community members

# On-the-ground actions from transcript

- Stay at home, follow health guidelines (implied)
- Wear a mask and take necessary precautions at work (implied)
- Support each other by standing up against unsafe practices (implied)

# Whats missing in summary

The full transcript provides additional context on the failures of leadership during the crisis and the necessity for individual and community action.

# Tags

#Florida #COVID19 #CommunityAction #PublicHealth #Leadership