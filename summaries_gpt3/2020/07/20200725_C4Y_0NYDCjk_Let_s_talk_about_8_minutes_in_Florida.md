# Bits

Beau says:

- Explains the challenges of governing Florida, a state with a diverse population and significant distrust of government.
- Describes the severity of the COVID-19 situation in Florida, with hospitals overwhelmed and rising cases.
- Notes the lack of effective leadership and messaging in handling the crisis, particularly in South Florida.
- Criticizes Governor DeSantis for his ineffective leadership and alignment with Trump, pointing out his absence during the crisis.
- Emphasizes the importance of honest messaging, acknowledging the severity of the situation, and promoting safety measures like wearing masks and staying at home.
- Praises county school districts for prioritizing virtual learning over returning to normalcy.
- Urges for a change in messaging and leadership to save lives and prevent further escalation of the crisis in Florida.

# Quotes

- "It's that bad. It's not a manufactured narrative."
- "We're losing a person on average every eight minutes and 20 seconds."
- "The best leadership we have is not coming from state government. It's coming from Walmart."
- "Governor DeSantis' little speech was probably premature."
- "It's not a media fiction. It's not a manufactured narrative. It is that bad."

# Oneliner

Florida faces a severe COVID-19 crisis with overwhelmed hospitals, lacking leadership, and a need for honest messaging to save lives.

# Audience

Floridians, Public Health Advocates

# On-the-ground actions from transcript

- Support county school districts opting for virtual learning (exemplified)
- Wear masks, stay at home, practice good hygiene (implied)
- Advocate for honest messaging and leadership to address the crisis (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the COVID-19 situation in Florida, criticizing the government's response and urging for better leadership and messaging to tackle the crisis effectively.

# Tags

#Florida #COVID19 #Leadership #Messaging #GovernorDeSantis #PublicHealth