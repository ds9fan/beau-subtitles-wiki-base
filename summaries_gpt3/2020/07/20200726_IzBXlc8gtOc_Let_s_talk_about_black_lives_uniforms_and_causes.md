# Bits

Beau says:

- Addressing the pushback he received after a previous video about uniforms and causes.
- Exploring the sentiment of the necessity of uniforms from a broader perspective.
- Questioning the practicality and implications of uniforms in certain situations.
- Emphasizing the importance of blending in for safety and freedom of movement.
- Identifying the diverse goals of the movement, ranging from social justice to equality for all.
- Tracing the roots of the movement back to helping, protecting, and elevating people of color.
- Warning against escalating to a next level where identifiable groups face greater risks.
- Urging against adopting uniforms that make marginalized groups easy targets.
- Rejecting the idea of sacrificing a demographic for political gains.
- Advocating for the harder but peaceful route for a better outcome.

# Quotes

- "Being able to blend in, in a situation like that, drastically increases your survivability."
- "If this goes open and escalates into something like that, they don't."
- "The end of something can often be determined by the beginning."
- "You give up, but you don't give in either."
- "It's harder. It's harder. It's going to have a better outcome."

# Oneliner

Beau addresses the implications of uniforms in movements, urging against easy targeting and advocating for the harder but peaceful route towards a better outcome.

# Audience

Activists, advocates, allies.

# On-the-ground actions from transcript

- Advocate for blending in and maintaining safety in movements (implied).
- Reject the idea of sacrificing any demographic for political gains (implied).
- Support peaceful approaches for better outcomes (implied).

# Whats missing in summary

The full transcript provides deeper insights into the risks and implications of adopting uniforms in movements, urging for thoughtful consideration and advocating against easy targeting.

# Tags

#Uniforms #SocialJustice #Equality #PeacefulResistance #MarginalizedGroups