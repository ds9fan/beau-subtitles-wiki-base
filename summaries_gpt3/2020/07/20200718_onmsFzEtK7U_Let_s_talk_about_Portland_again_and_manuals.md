# Bits

Beau says:

- Speculated on the response to federal government actions in Portland, predicting strengthening of protests.
- Mentioned that crowd size in Portland doubled in response to federal government actions.
- Referenced a military manual, FM3-24, which covers provoking responses in chapter four, paragraph 43-ish.
- Emphasized the wealth of resources available in military manuals that can be accessed for free.
- Encouraged everyone to tap into these resources, even if they think they may not be interested.
- Stressed the importance of the federal government reading and following their own manual to avoid escalating tensions.
- Pointed out that mishandling the situation could lead to things spiraling out of control quickly.
- Suggested that the only options left are to either wait for the situation to burn out or to listen to the protesters' demands.
- Urged the federal government to avoid continuing on the current path to prevent a potentially disastrous outcome.
- Mentioned an incident with a young woman facing law enforcement, which was not in the manual.

# Quotes

- "For every person they snatched, it would bring 10 more to the cause."
- "You don't give them a security clamp down in an area where a movement has popular support."
- "The only thing that occurred that wasn't in the manual was the young woman and the mask and only the mask that decided to face down law enforcement last night."

# Oneliner

Beau speculates on Portland protests, urges federal government to follow their manual to avoid escalation, and stresses the importance of listening to protesters' demands.

# Audience

Community members, protesters.

# On-the-ground actions from transcript

- Download military manuals from archive.org or fas.org (suggested).
- Utilize resources like Project Gutenberg and LibriVox for free access to valuable information (suggested).

# Whats missing in summary

The full transcript provides detailed insights into the potential consequences of mishandling protests and the importance of adhering to established strategies to prevent escalation.

# Tags

#Portland #Protests #MilitaryManuals #CommunityResources #FederalGovernment