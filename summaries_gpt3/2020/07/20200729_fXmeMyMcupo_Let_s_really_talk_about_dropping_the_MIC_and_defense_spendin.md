# Bits

Beau says:

- President Eisenhower's farewell address introduced the term "military-industrial complex" in 1961, warning about the influence of the arms industry on the government.
- The arms industry profits by influencing government decisions to purchase more arms, leading to a cycle of increased militarization.
- This influence results in policies that may not serve the public good, encouraging interventions and a war-based economy.
- The military-industrial complex extends its influence to schools, contributing to the normalization of a militarized society.
- Beau suggests curbing this influence to prevent further militarization and negative consequences for society.
- Noam Chomsky argues that it's not just the military-industrial complex but a broader issue of industrial influence over policy-making for profit.
- The term "military-industrial complex" adds a veneer of legitimacy to what is essentially corruption in various sectors like private prisons and big agriculture.
- Lobbying and campaign contributions lead to policies that benefit large industries at the expense of the public good.
- By framing these issues as corruption rather than complex terms, it becomes clearer how profit-driven decisions harm society.
- Beau advocates for recognizing and addressing corruption in government decisions, particularly in defense spending, which often goes unquestioned due to pervasive influence.

# Quotes

- "The military-industrial complex isn't really a thing. A guy named Noam Chomsky said there's no such thing as the military-industrial complex. It's just the industrial system."
- "All of this, all of this, it's just corruption."
- "It's corruption. We need to start calling it what it is."
- "It's just corruption. Maybe it's better to just give people the term that they know, that they can recognize."
- "They were all under the influence of corruption, not the military-industrial complex."

# Oneliner

President Eisenhower's warning about the military-industrial complex reveals a broader issue of corruption influencing policy-making in various industries for profit.

# Audience

Policy reform advocates

# On-the-ground actions from transcript

- Advocate for transparency in government spending and policy-making (exemplified)
- Support campaigns against corruption in various sectors, including defense spending (exemplified)
- Educate others on the influence of profit-driven decisions on public policy (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of how industrial influence for profit corrupts policy-making and the importance of addressing this corruption to ensure decisions serve the public good.