# Bits

Beau says:

- Beau talks about the cultural and historical significance of the Black Hills and its connection to the Constitution.
- Trump's visit to Rushmore has sparked controversy similar to his visit to Tulsa, raising questions about the timing of his appearances.
- Beau questions why the Sioux do not have the Black Hills despite the 1868 Fort Laramie Treaty granting it to them.
- The U.S. took back the Black Hills from the Sioux due to the discovery of gold, an act Beau deems unconstitutional.
- Despite a 1980 Supreme Court ruling that the taking of the land was unconstitutional, the Sioux were offered cash instead of the land's return.
- Beau explains that the Black Hills hold deep cultural and spiritual significance for the Sioux, being considered the birthplace of their culture.
- Beau stresses that it is ultimately up to the Sioux to decide the fate of their land, not outsiders like himself.
- Various proposed solutions to the issue of the Black Hills ownership have been suggested over the years, but Beau asserts that it is solely the Sioux's decision.
- Beau advocates for returning the Black Hills to the Sioux, urging respect for treaties, the Constitution, and the rightful owners of the land.
- Ending with a call to honor treaties, the Constitution, and give the land back to its rightful owners.

# Quotes

- "It is their land. Period."
- "It's their land. They get to decide."
- "Constitutionally, it's theirs. We need to honor the treaties."
- "Let's give it back."
- "Give it back. I don't know why this is a debate."

# Oneliner

Beau stresses the constitutional importance of honoring treaties and returning the Black Hills to the Sioux, respecting their cultural significance and rightful ownership.

# Audience

Advocates and Activists

# On-the-ground actions from transcript

- Contact indigenous-led organizations to support efforts for the return of sacred lands (exemplified)
- Join community initiatives that center indigenous voices and rights (exemplified)

# Whats missing in summary

Deeper insights into the cultural and spiritual significance of the Black Hills for the Sioux community.

# Tags

#BlackHills #IndigenousRights #Treaties #Constitution #CulturalSignificance #CommunityJustice