# Bits

Beau says:

- Explains the situation at a summer camp in Georgia where out of 597 attendees, 260 tested positive within four days, with the highest positive tests in the 6-10 age group.
- Dispels the idea that children are not impacted by the virus as about three quarters showed symptoms.
- Advises teachers, especially in areas with districts functioning as usual, to prepare like they are going on a combat deployment, suggesting they organize their affairs and have a designated person know where all their critical documents are located.
- Urges people to always wear protective equipment, drawing a parallel to soldiers removing their back plate because it was "cool," leading to dangerous consequences.
- Encourages community members with influence to advocate for online learning as the lowest risk option according to the CDC guidelines.
- Recommends being proactive in influencing school boards and districts to prioritize safety over following guidelines that pose moderate risks.

# Quotes

- "Treat [preparations] like a combat deployment."
- "Wear your protective equipment at all times. Doesn't matter how annoying it is. Doesn't matter how hot it makes you. Wear it."
- "We know what's going to happen. We don't need to reproduce the results."
- "If you're active in your community and you have any sway over the school board, the school district in your area, now's the time to use it."
- "Even with the guidelines, they're saying it's moderate risk."

# Oneliner

Beau advises teachers in high-risk areas to prepare for teaching like a combat deployment, stresses the importance of protective gear, and urges community advocacy for online learning amidst rising COVID cases.

# Audience

Teachers, Community Members

# On-the-ground actions from transcript

- Advocate for online learning with school boards and districts (exemplified)
- Prepare critical documents and share their location with a trusted person (exemplified)

# Whats missing in summary

Importance of advocating for safety measures and preparations in high-risk areas.

# Tags

#COVID19 #Education #CommunityAdvocacy #ProtectiveGear #SafetyPrecautions