# Bits

Beau says:

- Explains the typical cycle of Trump deflection when he gets in trouble, starting with "I didn't know" and moving to blaming Democrats.
- Asserts that Schiff, on the intelligence committee, likely knew about issues before the president, due to attending briefings diligently.
- States that Schiff holding hearings immediately could have endangered special operators and intelligence personnel in the field.
- Points out that intelligence agencies like the CIA work for the president, not Congress, and Schiff cannot direct them to act.
- Notes that blaming Schiff is a deflection tactic to target a disliked figure, as Republicans misunderstand the government's workings.
- Emphasizes that Trump's base is being manipulated with misinformation about intelligence operations to deflect blame effectively.
- Indicates that rumors of issues date back to the Obama administration, but concrete intelligence came during Trump's term.
- Criticizes the lack of action by the Trump administration despite intelligence reports, choosing to believe Putin without any response.
- Stresses that the options are not just ignoring the issue or going to war with Russia, dismissing these extreme notions.
- Concludes by remarking on the deliberate crafting of talking points to exploit ignorance among Trump's base regarding government operations.

# Quotes

- "Schiff knew first because he actually attends his briefings."
- "Secrets are secrets not really because of the information in them but how they're gathered."
- "This is a story that should not go away."
- "Those aren't even options really on the table in a sane world."
- "Crafting their talking points around the idea that their voters don't understand the very basics of how this stuff works."

# Oneliner

Beau explains the Trump deflection cycle, clarifies intelligence operations, and exposes manipulation of Trump's base with misinformation.

# Audience

Politically informed individuals

# On-the-ground actions from transcript

- Contact your representatives to demand accountability and transparency in intelligence operations (suggested)
- Join organizations advocating for governmental oversight and accountability (implied)

# Whats missing in summary

Detailed analysis and context on the manipulation of misinformation among Trump's base.

# Tags

#Intelligence #Trump #Government #Accountability #Misinformation