# Bits

Beau says:

- Introduces the topic of an election commissioner in Mississippi and the importance of sharing stories to understand systemic issues.
- Sheds light on a Facebook post by Gail Harrison Welch, an election commissioner, where she differentiated between "the blacks" and "people" regarding voter registration concerns.
- Explains how this differentiation perpetuates systemic racism and voter suppression to maintain institutional power.
- Raises the issue of gerrymandering and the manipulation of voting districts to silence minority voices.
- Criticizes Welch's attempt to explain her comments privately after posting them publicly, suggesting it raises further doubts about her actions behind closed doors.
- Questions Welch's statement about trying to motivate people to vote but points out the underlying racial undertones in her initial comments.
- Emphasizes the need to reject any form of differentiation between groups of people based on race or ethnicity.
- Urges for a new mindset in the South that moves away from racial discrimination and towards inclusivity and representation.
- Raises concerns about how Welch's mindset may have influenced her actions during her 20-year tenure as an election commissioner.
- Points out the upcoming election where the people of Jones County, Mississippi may need to be informed about Welch's history and attitudes towards voter registration.

# Quotes

- "If you are differentiating between any group of human beings and people, you're wrong. You're racist, you're sexist, you're a bigot of some sort, I guarantee it."
- "We're building a new South. We don't need this. We've rejected this. We're past this. We're getting away from it."
- "This shows one of the critical flaws in representative democracy."

# Oneliner

Beau sheds light on systemic racism and voter suppression through the comments of an election commissioner in Mississippi, urging for a new mindset towards inclusivity and representation.

# Audience

Mississippi residents

# On-the-ground actions from transcript

- Inform the people of Jones County, Mississippi about the upcoming election and the history of the election commissioner (suggested).
- Advocate for fair voting practices and representation in your community (implied).

# Whats missing in summary

Deeper insights into the impact of systemic racism on voter registration and the importance of addressing racial biases in public office.

# Tags

#SystemicRacism #VoterSuppression #Mississippi #ElectionCommissioner #Inclusivity #Representation