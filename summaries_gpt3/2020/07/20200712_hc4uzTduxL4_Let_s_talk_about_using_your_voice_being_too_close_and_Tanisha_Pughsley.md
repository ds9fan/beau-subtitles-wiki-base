# Bits

Beau says:

- Importance of the video for everyone, especially women.
- Impact of the topic on women when it spirals out of control.
- Focus on using your voice, being cautious, and dispelling myths.
- Learning from Tanisha Pugsley, a detective in Montgomery, Alabama.
- Description of Tanisha as a strong, law-abiding woman.
- The limitations of protective orders and their role in cases.
- Tragic outcome involving Tanisha's ex-boyfriend charged in her death.
- Challenging stereotypes of weak, dependent women in such situations.
- Emphasizing the need to recognize danger even when too close.
- Urging the use of voice, seeking help, and calling for backup in risky situations.
- Mention of shelters and resources available for individuals in need.
- Providing the national hotline number for assistance.
- Encouragement to reach out for help and not face such situations alone.

# Quotes

- "Use your voice. Call for backup."
- "You don't have to go through this alone."
- "If you knew somebody going through this, you'd want to help, right?"

# Oneliner

Beau stresses the importance of using your voice, seeking help, and dispelling myths around domestic violence, inspired by the tragic story of Tanisha Pugsley.

# Audience

Women, allies

# On-the-ground actions from transcript

- Call the national hotline for assistance (exemplified)
- Reach out to shelters for help (implied)
- Encourage others to seek help and not go through such situations alone (exemplified)

# Whats missing in summary

Full emotional impact and details of Tanisha's story can be better understood by watching the full video.

# Tags

#DomesticViolence #Empowerment #SeekHelp #Support #Community