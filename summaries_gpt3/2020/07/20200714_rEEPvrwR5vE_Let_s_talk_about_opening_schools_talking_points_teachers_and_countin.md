# Bits

Beau says:

- Beau addresses the administration's push to get all students back in person as a magic trick to create an illusion of normalcy and economic improvement.
- Large school districts have refused the push for in-person learning and opted for distance learning, showing independent thinking.
- The argument that students need socialization is countered by the rigid guidelines turning schools into prison-like environments.
- Beau provides evidence from Arizona, where three teachers contracted COVID-19 during summer school, with one teacher succumbing to the virus.
- He challenges Governor DeSantis to specify the acceptable number of teachers who can die before reconsidering in-person schooling.
- Beau argues that the talking point of returning to normalcy through in-person schooling is debunked, as the risks and inability to meet safety guidelines remain.
- He suggests alternative methods like online learning to protect teachers and students from unnecessary risks.

# Quotes

- "It's what the president is really hoping to achieve here."
- "They're going to open up using distance learning, and that's smart."
- "Turns it into a prison."
- "I have evidence."
- "The talking point is gone."

# Oneliner

Beau exposes the illusion of returning to normal through in-person schooling, advocating for safety and alternative teaching methods in the face of COVID-19 risks.

# Audience

Parents, Teachers, Community

# On-the-ground actions from transcript

- Advocate for safe learning environments in schools (implied)
- Support distance learning options for students and teachers (implied)
- Challenge local leaders to prioritize health and safety in educational decisions (implied)

# Whats missing in summary

The emotional impact and urgency of prioritizing safety and evidence-based decision-making in educational policies during the ongoing pandemic.

# Tags

#Education #COVID19 #Safety #OnlineLearning #CommunityConcerns