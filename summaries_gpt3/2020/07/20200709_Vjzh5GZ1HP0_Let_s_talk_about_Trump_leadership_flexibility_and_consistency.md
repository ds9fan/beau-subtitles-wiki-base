# Bits

Beau says:

- Shares advice on leadership from a warrant officer: leaders must be willing to take risks and be first through the door to inspire followers.
- Recalls the officer's experience in Panama, Grenada, and Vietnam as a real leader.
- Criticizes President Trump's approach to leadership, referencing his handling of the Republican National Convention in Jacksonville.
- Trump insists on holding the convention despite rising COVID-19 cases in Florida, showing inflexibility.
- Trump demands schools to reopen in Florida before August 24th, threatening to pull funding if they don't comply.
- Beau urges Trump to lead by example and move the convention earlier without additional safety measures to demonstrate that it's safe.
- Emphasizes that Trump's decision will reveal his priorities: politicians or children, teachers, and school staff.
- Asserts that Trump needs to show consistency in his leadership, even if it means maintaining a stance of "abject cowardice."
- Concludes by calling for decisive action from the President.

# Quotes

- "Be a leader. Be first through the door."
- "No flexibility on this one."
- "The entire country is watching."
- "It's time for the President to lead."
- "You can't have extra precautions."

# Oneliner

Beau advises President Trump to lead by example and prioritize the safety of children, teachers, and school staff over political events, urging for consistency and decisiveness in leadership.

# Audience

Politically aware citizens

# On-the-ground actions from transcript

- Contact local representatives to advocate for safe school reopening plans (implied)
- Organize community efforts to support schools with necessary resources for safe reopening (implied)

# What's missing in summary

The emotional impact and urgency conveyed by Beau's call for President Trump to prioritize the safety of children and demonstrate true leadership by example. 

# Tags

#Leadership #Consistency #Flexibility #SchoolReopening #COVID19