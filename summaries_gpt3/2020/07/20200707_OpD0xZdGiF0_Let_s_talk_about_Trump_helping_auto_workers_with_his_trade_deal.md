# Bits

Beau says:

- Beau dives into Trump's signature trade deal, the USMCA, aiming to bring back auto manufacturing jobs to the US.
- The deal requires 75% of car parts to be manufactured in the US, Canada, or Mexico, and 40-45% by workers earning at least $16 an hour.
- Trump's plan was to entice Japanese auto manufacturers to move to the US; however, companies are opting to raise wages in Mexico instead.
- The trade deal went into effect on July 1st, with companies in Mexico choosing to increase wages rather than relocating.
- Despite falling short of Trump's goals, the deal has a silver lining: benefiting workers, raising living standards, and boosting the Mexican economy.
- Beau acknowledges that the deal may increase car prices in the US due to higher labor costs but suggests the Mexican government might offer incentives to balance this.
- Trump is likely to tout the deal as a success on the campaign trail, but the reality differs from his claims.
- While Trump may claim to have helped auto workers, it's primarily benefiting non-American workers under the USMCA.

# Quotes

- "Trump is going to get out on the campaign trail and still talk about this as if it's a great success."
- "He says that he helped auto workers, that's a true statement. Just not American auto workers."

# Oneliner

Beau delves into Trump's USMCA trade deal aiming to boost auto jobs, but results differ from his claims, benefiting non-American workers primarily.

# Audience

Policy Analysts, Trade Experts

# On-the-ground actions from transcript

- Support policies that prioritize American workers' interests over foreign workers (implied)
- Advocate for trade deals that truly benefit American workers (implied)

# Whats missing in summary

Insights on the potential long-term impacts of the USMCA trade deal beyond immediate consequences.

# Tags

#TradeDeal #USMCA #AutoWorkers #Trump #EconomicImpact