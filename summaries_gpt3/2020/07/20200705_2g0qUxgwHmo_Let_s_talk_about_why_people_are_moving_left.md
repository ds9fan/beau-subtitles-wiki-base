# Bits

Beau says:

- The US political spectrum is skewed to the right, with even Democrats considered center-right globally.
- Leftism, defined as rejecting capitalism, is gaining traction in the US.
- There is a surge in black leftists, challenging traditional right-wing ideologies.
- White privilege denial contributes to pushing black Americans towards leftist ideologies.
- Issues of race and class in the US prompt people to seek egalitarian philosophies.
- Black leftists are advocating for systemic changes that impact them daily.
- The economic system is being challenged by those seeking systemic change.
- Black leftists are willing to fight for all lives, leading to a surge in leftists across different groups.
- Rejecting capitalism is a common trend among those advocating for environmental protection.
- The current form of capitalism in the US fails to lift black Americans out of poverty.

# Quotes

- "White privilege denial contributes to pushing black Americans towards leftist ideologies."
- "Black leftists are willing to fight for all lives."
- "Rejecting capitalism is a common trend among those advocating for environmental protection."

# Oneliner

Why the surge in black leftists and rejection of capitalism stems from challenging systemic issues of race and class in the US, pushing for egalitarian philosophies and systemic changes.

# Audience

Activists, Political Analysts

# On-the-ground actions from transcript

- Challenge systemic issues of race and class by advocating for egalitarian philosophies and systemic changes (implied).
- Stand up against white privilege denial and contribute to pushing for leftist ideologies (implied).
- Advocate for environmental protection and challenge capitalism by rejecting its harmful practices (implied).

# Whats missing in summary

Full context and depth on the history and reasons behind the surge in black leftists and rejection of capitalism.

# Tags

#USPolitics #Leftism #BlackAmericans #SystemicChange #Capitalism