# Bits

Beau says:

- Music captures moments in history better than anything, often not heard until years later because songwriters write passionately about their surroundings, not for an audience.
- Songs connect us to specific periods in time, playing out the idea of capturing a moment in every movie about the past.
- Introduces a song by Tom Burton, "I Bet You Can Hear Us Now," as a fitting theme for 2020.
- The lyrics of the song address standing up against injustice, corruption, and violence by those in power, particularly targeting law enforcement.
- Talks about the systemic issues, oppression, and violence faced by communities, calling for change and standing ground against it.

# Quotes

- "I bet you can hear us now."
- "Back to how it was before, we're gonna stand our ground."
- "A system corrupted by lies."
- "Blatant aggression, it seems their obsession."
- "We ain't gonna take any more."

# Oneliner

Beau talks about music's power to capture history, introducing a song that resonates with the theme of standing up against injustice and corruption in 2020.

# Audience

Music enthusiasts and advocates for social justice.

# On-the-ground actions from transcript

- Join protests against injustice and corruption (implied).
- Support community-led initiatives for change (implied).

# Whats missing in summary

The emotional impact of music in reflecting and addressing societal issues.

# Tags

#Music #SocialJustice #Injustice #Power #Corruption #Community