# Bits

Beau says:

- Explains the relationship between diplomacy and intelligence work, mentioning that every embassy engages in intelligence work and has spies.
- States that the Chinese government was spying on the US, particularly at a specific consulate.
- Suggests that the intelligence agencies' choice to make arrests rather than run a disinformation campaign was politically motivated.
- Notes that foreign intelligence services are spying on US medical research due to the public health crisis and Trump's mismanagement of it.
- Emphasizes that monitoring the research process gives other countries insight into how long the US forces could be degraded.
- Points out that the national security costs of Trump's errors in managing the crisis are significant and will have long-term consequences.
- Mentions that other nations are trying to exploit the weakness displayed by the US administration in handling the crisis.
- Concludes by stating that the concern is not about medical professionals sharing information but about disrupting the research process to make the US more vulnerable.

# Quotes

- "Every embassy ran by every country, everywhere in the world, engages in intelligence work."
- "The idea that the Chinese government was spying on us, that's a fact."
- "Trump's failure to manage this showed exactly how vulnerable the United States is to this type of warfare."

# Oneliner

Diplomacy and intelligence work are intertwined, with foreign nations exploiting vulnerabilities in US response to the crisis for their advantage.

# Audience

Policy makers, diplomats, activists

# On-the-ground actions from transcript

- Monitor and safeguard medical research facilities (suggested)
- Advocate for improved crisis management strategies (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of how foreign intelligence services exploit vulnerabilities in US response to the crisis, urging for increased vigilance and strategic improvements.

# Tags

#IntelligenceWork #ForeignEspionage #CrisisManagement #NationalSecurity #Diplomacy