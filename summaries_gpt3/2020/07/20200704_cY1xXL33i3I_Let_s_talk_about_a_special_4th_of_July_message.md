# Bits

Beau says:

- Sharing a special 4th of July message regarding Independence Day and the concept of equality in the country.
- Addressing the slogan "All lives matter" and its implications when used in response to "Black Lives Matter."
- Exploring the hidden motives behind using "All lives matter" to diminish the message of equality for black individuals.
- Pointing out the systemic racism embedded in using "All lives matter" to deflect focus from specific injustices faced by black people.
- Questioning the true intent behind advocating for justice for all lives while undermining the Black Lives Matter movement.
- Emphasizing the fear that equality for black individuals may challenge the privilege and status quo enjoyed by white individuals.
- Concluding with a thought on the implications of acknowledging true equality for all individuals in society.

# Quotes

- "If the only time you say all lives matter is as a response to somebody saying that Black Lives Matter, you have to question your own motives."
- "All lives deserve justice, if that's true, then you should have no issue with the phrase Black Lives Matter because black is part of all."
- "That's not what gets said. When people say it, what they're really doing is trying to turn down the volume on that message."
- "The fear of most people who say that is that if black people get equality, well, then white folk, they're gonna have to stand on their own."
- "Happy 4th of July, and all countries matter."

# Oneliner

Beau addresses the divisive slogan "All lives matter" and its implications, questioning hidden motives behind its usage in response to "Black Lives Matter" while exposing systemic racism.

# Audience

Social Justice Advocates

# On-the-ground actions from transcript

- Support fundraisers for shelters that aid survivors of domestic violence (suggested)
- Challenge systemic racism through education and advocacy efforts (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the intricacies of systemic racism and the importance of acknowledging specific injustices faced by marginalized communities.