# Bits

Beau says:

- Beau introduces the topic of getting off the current "highway" we're on, discussing different exits to take.
- He presents a strategy that, although harder and taking longer, has proven successful in the past.
- Beau explains the gravity of the situation through numbers, using the example of Syria to illustrate the potential loss if the current path continues.
- He stresses the importance of community action and starting now to create change rather than waiting for political figures like Biden.
- Beau advocates for building small community networks and making positive changes locally to gain political influence from the ground up.
- He warns against blindly following political figures and instead encourages grassroots movements for real change.
- Beau underlines the power of social networks and community organizing over violence in achieving a just society.
- He cautions against escalating situations into violence, advocating for defensive strategies in protests.
- Beau suggests using symbolic gestures like carrying the American flag in protests to gain wider public support and win the PR battle.
- He concludes by proposing the idea of building a new system rather than solely fighting against the existing one.

# Quotes

- "It's always bad."
- "Don't wait for him. It's going to take us to do it. Might as well start now."
- "Power to the people? That's how you achieve it. Social networks and cell phones, not guns."
- "Be the anvil."
- "Maybe we should just build the system that we want rather than trying to fight against a system that's been there."

# Oneliner

Beau stresses community action over waiting for politicians, advocating for grassroots movements to build a just society without violence.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Build small community networks and initiate positive changes now (suggested)
- Carry out local activism with friends to influence policy decisions (exemplified)
- Use social networks and community organizing to gain political clout (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of the urgency to take action and build a new system rather than relying on existing political figures or waiting for change.

# Tags

#CommunityAction #GrassrootsMovements #PoliticalChange #SocialJustice #ProtestActions