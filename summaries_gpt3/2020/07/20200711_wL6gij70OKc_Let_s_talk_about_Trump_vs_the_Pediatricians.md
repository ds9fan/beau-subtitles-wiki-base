# Bits

Viva Litzbow says:

- Trump administration insisted on schools reopening in the fall, threatening funding withdrawal if not in person.
- American Academy of Pediatricians' guidance emphasized the importance of safe reopening with CDC-aligned recommendations.
- Trump disagreed with CDC guidelines for schools, planning to pressure experts to change their guidance.
- American Academy of Pediatricians issued a statement co-signed by other organizations, prioritizing safe reopening based on science, not politics.
- They emphasized the need for evidence-based decision-making and leaving school reopening decisions to health experts and educators.
- The statement rebuked the President's stance and called for federal resources to ensure safe education.
- Withholding funding from schools not opening full-time in person was criticized as putting students and teachers at risk.
- The President's involvement in determining school reopening was deemed inappropriate; health experts should lead decision-making.
- The presidency's excessive power and the need to limit it, especially in light of the current administration's actions, were emphasized.
- The push to reopen schools was labeled as political, lacking a balanced nationwide approach to reopening based on varying threats.
- Adaptability and trust in health experts and educators were prioritized over the President's directives.

# Quotes

- "Science should drive decision-making on safely reopening schools."
- "Health experts do [decide when schools open], not the president."
- "The presidency has too much power. The Trump administration has shown this completely."
- "Eventually there will be another Trump."
- "The president attempting to control everything is going to make things worse."

# Oneliner

Viva Litzbow challenges the Trump administration's stance on school reopening, advocating for evidence-based decisions led by health experts and educators over political influence.

# Audience

Education advocates, parents, policymakers

# On-the-ground actions from transcript

- Advocate for evidence-based decision-making in school reopening plans and prioritize safety for students, teachers, and staff (implied)
- Support federal funding initiatives to ensure safe education during the reopening process (implied)
- Advocate for limiting presidential power to prevent political influences in critical decision-making processes (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the politicization of school reopening and the importance of relying on experts for evidence-based decision-making.

# Tags

#SchoolReopening #HealthExperts #PoliticalInfluence #Education #FederalFunding