# Bits

Beau says:

- Released a video on absurdity, then saw today's headlines reflecting absurdity of the administration.
- Trump Jr. had his Twitter suspended due to false healthcare tweets.
- Attorney General, overseeing mass incarceration, complains about unfair prison sentences.
- Acknowledges that US prison sentences are too long but makes no effort to address the issue.
- Describes tyranny as when the government can do things you can't, hinting at double standards.
- Teases upcoming long-format content where he will deep dive into specific topics.
- Long-format content won't replace daily videos but will be additional.
- Encourages watching his previous video for a laugh amidst current absurd events.

# Quotes

- "US prison sentences are far too long."
- "You know you're under tyranny when the government is able to do things you can't."
- "If you want to laugh instead of cry at today's events, go watch that video I released last night."

# Oneliner

Beau talks about the absurdity of the administration, from Trump Jr.'s Twitter suspension to the Attorney General's complaints about prison sentences, hinting at double standards and teasing upcoming long-format content.

# Audience

Viewers

# On-the-ground actions from transcript

- Watch Beau's video for a humorous take on current events (suggested).
- Stay tuned for Beau's upcoming long-format content (suggested).

# Whats missing in summary

The full video provides a deeper exploration of the absurdity in current events and offers insight into the double standards within the administration.

# Tags

#Absurdity #TrumpAdministration #DoubleStandards #LongFormatContent #PoliticalSatire