# Bits

Beau says:

- Joe Biden, despite not being Beau's preferred candidate, is currently the most powerful man in the United States.
- Trump inadvertently gave Biden this power by not understanding how politics works in the system he wants to implement.
- Biden can alter the political landscape with just one speech or tweet, thanks to Trump's actions.
- Trump is considering deploying his forces nationwide after their controversial presence in Portland.
- The threat of a nationwide deployment comes after a team member suggested shooting those who follow them.
- Despite this, there has been no investigation or calls for law and order from the president.
- Trump's approach has eroded trust in his administration and its reports.
- Biden is now seen as the anti-establishment candidate against Trump's authoritarian tactics.
- If Biden were to pardon those arrested by Trump's forces, it could render them ineffective and boost Biden's support.
- Biden has the power to incite chaos with a single tweet, contrary to Trump's fearmongering narrative.

# Quotes

- "Joe Biden is now the most powerful man in the United States."
- "This tough guy rhetoric that everybody likes to use, it's getting out of hand."
- "Had he read the manual, he [Trump] would understand that the people in the street pretty much always win."

# Oneliner

Joe Biden's newfound power and Trump's misunderstandings have shifted the political landscape, setting the stage for potential chaos.

# Audience

Voters, Activists, Political Observers

# On-the-ground actions from transcript

- Organize peaceful demonstrations to show opposition to authoritarian tactics (implied)
- Support transparency and accountability within law enforcement agencies (implied)
- Stay informed and engaged with political developments to understand power dynamics better (implied)

# Whats missing in summary

More insights on the implications of Trump's actions on civil unrest and democracy.

# Tags

#Politics #PowerDynamics #Authoritarianism #JoeBiden #DonaldTrump