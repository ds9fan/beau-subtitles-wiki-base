# Bits

Beau says:

- Addressing the case of Specialist Vanessa Guillen from Fort Hood, Texas.
- Acknowledging the efficiency of army investigators and the slow but thorough process.
- Mentioning the 300 interviews conducted by investigators.
- Emphasizing the need for political pressure to ensure a proper investigation.
- Noting that Vanessa Guillen wanted to report harassment but was afraid of retribution.
- Pointing out the improvements made by the US military in addressing harassment.
- Urging for accountability if policies weren't followed or if commands failed.
- Suggesting the Senate Armed Services Committee launch an investigation.
- Providing Twitter handles of committee members for public outreach.
- Mentioning strong allies like Elizabeth Warren and Tammy Duckworth for support.
- Stressing the importance of public support to keep the story alive and demand answers.
- Expressing concerns about the lack of security in the armory where the incident occurred.
- Calling for a clear picture of what happened beyond the basic details of the crime.
- Advocating for congressional hearings to address the larger issues beyond the crime itself.

# Quotes

- "We can't let this story fade away because there are a lot of questions."
- "The US military for years was moving ahead on this."
- "Because there are a lot of things that should have been in place that should have prevented this."
- "We need congressional hearings on this."
- "Y'all have a good night."

# Oneliner

Beau addresses the need for thorough investigation, accountability, and congressional hearings regarding Specialist Vanessa Guillen's case at Fort Hood, Texas.

# Audience

Advocates for Justice

# On-the-ground actions from transcript

- Contact members of the Senate Armed Services Committee (suggested)
- Tweet at committee members for support (implied)
- Reach out to committee members on social media (implied)
- Call committee members for action (implied)

# Whats missing in summary

Full context and emotional depth can be better understood by watching the full transcript.

# Tags

#Justice #Accountability #Military #Investigation #Congress #VanessaGuillen