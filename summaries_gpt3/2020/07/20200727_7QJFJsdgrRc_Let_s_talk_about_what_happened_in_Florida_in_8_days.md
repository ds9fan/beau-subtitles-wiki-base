# Bits

Beau says:

- Governor in Florida is forcing schools to reopen despite knowing it's a bad idea, following Trump's direction.
- In just eight days, cases involving children in Florida increased by 34%, with almost a thousand new cases daily.
- Hospitalizations also increased by 23% in the same period.
- Research from South Korea shows that children transmit the virus as easily as adults.
- Parents were misled into thinking it's safe for kids to return to school, neglecting the risks.
- Beau urges parents to opt for distance learning or homeschooling if possible.
- Florida districts have homeschooling-friendly policies that parents can utilize.
- The reopening of schools will further increase cases and pose a significant risk to public health.
- Beau criticizes the prioritization of the economy over the well-being of children and families.
- Despite warnings, some prioritize economic reasons for reopening schools, neglecting safety concerns.
- Beau stresses that there is no valid reason to reopen schools and that it's a dangerous decision.
- He calls out the disregard for facts and safety in favor of political allegiance.
- Teachers, school administrators, and data all indicate that reopening schools is ill-advised.
- Beau ends by questioning the logic behind prioritizing reopening schools against all evidence.

# Quotes

- "Kids transmit just as easily."
- "There's no reason to reopen these schools."
- "The numbers say it's a bad idea."
- "This is a bad idea."
- "But hey, the guy who didn't know how to put on his mask, well, he says it's
  important."

# Oneliner

Governor in Florida pushes to reopen schools against advice, risking children's
health and economy while prioritizing politics over safety.

# Audience

Parents, educators, concerned citizens

# On-the-ground actions from transcript

- Opt for distance learning or homeschooling if feasible (suggested)
- Research homeschooling policies in Florida districts and take necessary steps to enroll children (implied)

# Whats missing in summary

The emotional impact and urgency of protecting children and communities by making informed and responsible decisions.

# Tags

#Florida #Schools #COVID19 #Children #Safety #Economy #Government