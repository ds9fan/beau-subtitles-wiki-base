# Bits

Beau says:

- Introduces themselves as the instructor for the 2021 school year due to a lack of teachers in the school district.
- Mentions being on loan from Fort Benning, Georgia, through a Department of Defense initiative.
- Expresses wearing a whimsical mask to appear approachable but warns students to stay six feet away due to being "stinky-hand bags of germs."
- Informs students that they will be issued either a surgical mask or a P100 respirator, with politicians' children receiving the latter.
- Emphasizes the importance of following safety procedures like handwashing and not sharing food due to last year's incidents.
- Warns students that one of them may carry something home to their parents if safety protocols are not followed.
- Mentions the availability of forms for non-familial foster care and DNRs in the office.
- Instructs students to keep personal belongings, like a box of crayons labeled with their names, to themselves.
- Concludes the safety procedure meeting for second grade students and asks if there are any questions.

# Quotes

- "One of you will carry something home to your parents."
- "No birthdays."
- "Welcome to second grade."

# Oneliner

Beau instructs second grade students on safety procedures, warns about consequences of not following protocols, and limits interactions like sharing food or celebrating birthdays.

# Audience

Parents, Teachers, School Administrators

# On-the-ground actions from transcript

- Keep personal belongings like food and crayons to yourself (implied)
- Follow safety procedures like handwashing and wearing masks (implied)

# Whats missing in summary

The detailed tone and expressions used by Beau while addressing the students can best be understood by watching the full transcript. 

# Tags

#School #SafetyProcedures #DepartmentOfDefense #Instructor #Children