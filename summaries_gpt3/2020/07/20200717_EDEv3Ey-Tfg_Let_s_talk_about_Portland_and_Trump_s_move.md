# Bits

Beau says:

- Trump's recent actions suggest he has joined the movement in Portland by actively helping recruit for it.
- The administration's handling of the situation in Portland goes against established tactics for dealing with movements.
- The use of unmarked vehicles for snatch and grabs in Portland is seen as a form of harassment by the Trump administration.
- The arrests and detainments appear to be aimed at intimidating the movement in the streets rather than for legitimate reasons.
- Security clampdowns like this are strategically flawed and tend to backfire by generating fear and sympathy for the movement.
- Historical examples, such as Ireland in 1916, demonstrate how government overreactions can solidify movements and lead to significant changes.
- Overreactions to protests can crystallize movements by creating sympathizers out of fear and anger towards the government.
- Provoking a government overreaction is a known tactic for some militant groups seeking revolutionary change.
- The protesters in Portland have not done anything to warrant the security clampdown they are facing.
- Mainstream figures like Dan Rather have pointed out the counterproductive nature of the federal actions in Portland.

# Quotes

- "Every one of you they snatch brings ten more to your cause."
- "Trump has done more to grow the movement in Portland than any activist has."
- "Security clampdowns, these kind of measures, backfire always."
- "It appears to be just a method of harassing the movement in the streets."
- "Government overreactions can solidify movements."

# Oneliner

Trump's actions in Portland suggest he's supporting the movement, but his administration's tactics are backfiring, provoking sympathy and growth for the cause.

# Audience

Activists, Protesters, Community Members

# On-the-ground actions from transcript

- Connect with local activist groups to understand how best to support and amplify the movement (suggested).
- Share information and updates about the situation in Portland on social media to raise awareness (suggested).
- Stand in solidarity with those affected by the security clampdown in Portland by attending or organizing peaceful protests (exemplified).

# Whats missing in summary

The full transcript dives deeper into historical examples and the practical implications of government responses to movements, providing a comprehensive understanding of the current situation in Portland and its broader implications.

# Tags

#Trump #Portland #Protests #GovernmentOverreach #Activism