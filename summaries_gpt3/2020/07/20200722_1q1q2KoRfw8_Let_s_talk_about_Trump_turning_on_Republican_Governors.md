# Bits

Beau says:

- President Trump is turning on Republican governors, senators, and representatives who supported him.
- Trump is openly and publicly distancing himself from those who backed him up.
- Governors made policy decisions based on Trump's statements; for example, wearing masks.
- Trump is now following health experts' advice after months and 140,000 deaths.
- Governors who followed Trump's advice are now stuck as Trump distances himself from them.
- Trump will likely deny giving mandates or advice to these politicians.
- Governor DeSantis had to clarify that his advice on reopening schools was just a recommendation.
- Trump has a history of not taking responsibility for his actions.
- Trump's behavior with the Portland situation shows he will sell out anyone, including his allies.
- Those enabling Trump should be prepared for him to turn on them as well.

# Quotes

- "If you cannot read the writing on the wall, the president is selling these senators, governors, and representatives out very publicly."
- "None of this should be a shock."
- "He's going to sell you out. He's already started."

# Oneliner

President Trump is turning on his Republican allies, distancing himself publicly, leaving those who followed his lead stuck and at risk of being sold out.

# Audience

Politicians, Republican allies

# On-the-ground actions from transcript

- Stand on your own to preserve your seat (implied)
- Be prepared for Trump to potentially sell you out (implied)

# Whats missing in summary

The full transcript provides deeper insights into the consequences of blindly following Trump's lead and the importance of politicians standing on their own.