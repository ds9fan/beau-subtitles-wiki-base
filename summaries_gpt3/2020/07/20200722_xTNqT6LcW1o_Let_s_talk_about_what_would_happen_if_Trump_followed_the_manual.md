# Bits

Beau says:

- Trump administration is ignoring manuals and best practices in dealing with Portland, leading to mistakes and worsening the situation.
- Public perception drives Trump's response, but it is based on incorrect assumptions on managing movements like the one in Portland.
- Immediate results demanded by the public and politicians without understanding the situation can lead to further mistakes.
- Managing the situation in Portland requires patience and a long-term commitment, as advised by manuals.
- The goal should be to prevent escalation and violence, allowing grievances to be addressed in a calm manner.
- The response of using an iron fist concept always fails and is not effective in managing grievance-driven movements.
- More advanced texts beyond basic manuals are available to policymakers, developed by outside contractors and academics like RAND.
- Strategies like "hearts and minds" and "pacification" are recommended to secure the area and address grievances.
- Development efforts should focus on improving infrastructure, providing healthcare, education, and addressing income inequality.
- Implementing measures to improve living standards and address grievances is key to calming situations like the one in Portland.

# Quotes

- "Those who make peaceful revolution impossible make violent revolution inevitable."
- "No masks, no shields, no batons, no vans."
- "If the people want change, you give it to them."
- "It's not glamorous. It's not tough. It's not violent. But it wins."
- "This isn't a new theory."

# Oneliner

Trump administration's failure in Portland stems from ignoring manuals and best practices, requiring a patient long-term commitment and focus on addressing grievances non-violently.

# Audience

Policy makers, community leaders

# On-the-ground actions from transcript

- Address grievances calmly and non-violently (exemplified)
- Focus on improving infrastructure and addressing income inequality (exemplified)
- Provide healthcare, education, and relief to the community (exemplified)

# Whats missing in summary

Full understanding of the necessity for a long-term commitment and non-violent strategies in managing grievances.

# Tags

#Portland #TrumpAdministration #Grievances #Pacification #CommunityPolicing