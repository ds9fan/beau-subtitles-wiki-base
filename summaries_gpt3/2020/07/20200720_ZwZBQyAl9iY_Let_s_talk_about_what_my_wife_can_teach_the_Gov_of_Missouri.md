# Bits

Beau says:

- Beau contrasts the actions of his wife, a nurse, with the governor of Missouri, Mike Parson, in handling the spread of COVID-19.
- His wife follows a strict routine of disinfecting and isolating herself after work to prevent spreading the virus at home.
- Beau's wife sends a text before coming home so he can prepare their children to avoid immediate contact with her.
- She changes clothes, showers, and disinfects all personal items upon arrival to ensure safety.
- Beau criticizes Governor Parson for downplaying the risks of COVID-19 spread in schools and homes.
- He accuses Parson of prioritizing Trump's image over public health by pushing for a premature return to normalcy.
- Beau questions how many children the governor is willing to sacrifice to protect Trump's ego.
- He warns against dangerous messaging that misleads people into thinking the pandemic is over.
- Beau links Florida's spike in cases to its governor's premature celebration and lack of protective measures.
- He condemns leaders who prioritize political allegiance over the well-being of their citizens and country.

# Quotes

- "He chose to protect Trump's fragile little ego."
- "These governors are selling out their duty, their citizens, and their country."
- "This man is not capable of running a house with a nurse in it, let alone a state."

# Oneliner

Beau illustrates the stark contrast between his wife's cautious approach to COVID-19 and Governor Parson's reckless disregard for public health, putting political allegiances over people.

# Audience

Missouri residents

# On-the-ground actions from transcript

- Follow strict disinfection and isolation protocols after potentially exposing yourself to COVID-19 (implied)
- Advocate for evidence-based public health measures in schools and communities (implied)

# Whats missing in summary

The full transcript provides a detailed account of the personal sacrifices and precautions individuals must take to prevent the spread of COVID-19, contrasting it with the irresponsible leadership prioritizing political interests over public health.

# Tags

#COVID-19 #PublicHealth #Governance #Leadership #Missouri