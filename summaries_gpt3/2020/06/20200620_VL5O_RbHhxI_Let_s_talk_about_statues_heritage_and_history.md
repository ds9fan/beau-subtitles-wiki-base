# Bits

Beau says:

- Many statues are not historically significant or accurate, but rather mythologize and create a false narrative about American leaders.
- Statues in the South were often erected to send a message of dominance and segregation.
- American mythology, perpetuated by these statues, fails to address the country's problems and can lead to unrest.
- Beau mentions the example of Albert Pike, a Confederate officer whose statues downplay his controversial past.
- While mythologizing historical figures is okay to an extent, it must not be confused with history.
- Thomas Jefferson, despite his flaws, played a significant role in American history and should be remembered accurately.
- The U.S. was founded on flawed principles, but its strength lies in the ability to change and evolve.
- Preserving statues that are being taken down can serve as a reminder of a pivotal moment in American history.
- Beau stresses the importance of embracing change and moving forward as a nation.
- He advocates for a focus on real American history rather than mythological narratives.

# Quotes

- "These statues aren't about history."
- "Our most important heritage is the machinery for change."
- "It's time to move forward."
- "You're the person holding it back."
- "We should probably focus more on American history, real history, rather than mythology."

# Oneliner

Many statues mythologize rather than depict history, hindering progress; real American history is about embracing change and moving forward.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Preserve statues being taken down for historical significance (suggested)
- Focus on teaching real American history in educational settings (implied)

# Whats missing in summary

Beau's engaging delivery and passion for authentic American history are best experienced in the full transcript.

# Tags

#History #AmericanHeritage #Statues #Change #Mythology