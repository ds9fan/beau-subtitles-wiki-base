# Bits

Beau says:

- Explains the perception, association, and tradition lesson from the University of Florida.
- Conducts a demonstration using military contractors to illustrate a point about perception and association.
- Talks about the decision at the University of Florida to stop using a specific cheer due to its term.
- Describes the origins of the cheer and how it may evoke different connotations for different people.
- Emphasizes the importance of considering history and perceptions in making small changes for a better experience.
- Urges for moving forward and honoring more forward-thinking traditions.
- Advocates for acknowledging different perspectives and making small changes to enhance everyone's experience.
- Suggests that small changes, when combined, can lead to a significant shift in societal norms.
- Encourages embracing change and not hindering progress for the sake of tradition.

# Quotes

- "Let the small change happen. Enhance everybody's experience."
- "Little baby changes that can add up."
- "Don't be the person that stands in the way of that just for tradition's sake."
- "Let's move forward at UF. Let's move forward in this country."
- "Small changes. A bunch of them. By themselves, they mean nothing. But together, it's a revolution."

# Oneliner

Beau explains the importance of acknowledging history, making small changes, and moving forward for a better experience at UF and in the country.

# Audience

Students, alumni, community members

# On-the-ground actions from transcript

- Support the decision to drop offensive cheers at local events (exemplified)
- Advocate for small changes to enhance experiences in your community (suggested)

# Whats missing in summary

The full transcript provides a detailed example of how perceptions can differ based on history and tradition, urging for small changes to improve collective experiences.

# Tags

#Perception #Tradition #SmallChanges #Community #MovingForward