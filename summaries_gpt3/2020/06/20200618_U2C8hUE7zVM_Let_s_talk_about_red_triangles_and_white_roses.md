# Bits

Beau says:

- President of the United States ran 88 Facebook ads featuring a red triangle to identify his opposition.
- The red triangle historically was reserved for those who spoke out against the administration in Germany and rescued Jews.
- Trump wants to identify his enemies using the red triangle, drawing parallels to a dark historical context.
- Beau acknowledges taking inspiration from Sophie Scholl and the White Rose Society in meeting people where they're at.
- Trump seeks undying loyalty like certain supporters of the German regime during the war.
- Despite Trump's efforts to draw lineage, there is a fundamental difference in effectiveness between him and the German dictator.
- Trump's failures are evident in his inability to deliver on promises and manage various aspects of governance.
- Beau stresses the importance of pointing out Trump's failures repeatedly to reach his remaining supporters.
- Trump's appeal to uneducated voters who prioritize winning over moral character is a key aspect of his strategy.
- The need to continuously label Trump as a failure and point out his shortcomings to make his true nature clear.
- Beau advocates for meeting people where they're at, like Sophie Scholl, to effectively communicate with those who support Trump.
- Emphasizing Trump's incompetence and inability to fulfill promises is a critical strategy in addressing his supporters.
- The significance of Facebook removing the ads featuring the red triangle, exposing Trump's failed attempt at a dog whistle.
- Beau underscores the importance of consistently showcasing Trump's shortcomings and failures to counter his appeal to certain supporters.
- Encouraging the audience to be resilient in addressing Trump's inadequacies and communicating effectively with his supporters.

# Quotes

- "He wanted to make America great again. The country's on fire."
- "He's a loser. He's always been a loser and he's always going to be a loser."
- "He can't even racist right."
- "Be like Sophie."
- "It's just a thought. Y'all have a good night."

# Oneliner

President Trump's use of a red triangle reveals historical parallels, urging us to address his failures and meet his supporters where they're at to combat his divisive tactics.

# Audience

Political activists, concerned citizens

# On-the-ground actions from transcript

- Point out Trump's failures and shortcomings to his supporters (implied)
- Advocate for meeting people where they're at in political discourse (exemplified)
- Communicate effectively with those who support Trump by addressing his failures (implied)

# Whats missing in summary

The emotional intensity and depth of historical context captured in Beau's message may be best experienced in its entirety.

# Tags

#Trump #SophieScholl #WhiteRoseSociety #History #PoliticalActivism