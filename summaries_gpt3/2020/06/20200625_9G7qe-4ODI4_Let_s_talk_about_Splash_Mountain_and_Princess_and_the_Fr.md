# Bits

Beau says:

- Disney decided to rebrand Splash Mountain to focus on Princess and the Frog, sparking national debate.
- The outrage stems from right-wing sources prompting anger or deep-seated racism.
- The original theme of Splash Mountain was inspired by Song of the South, which had problematic imagery.
- The rebranding decision is not unusual, as companies frequently update their branding.
- Opposition to the change likely stems from discomfort with the removal of racist imagery.
- Beau questions why some people are upset over a theme park ride being updated to a more modern and inclusive theme.
- He challenges individuals to confront their reasons for resisting change, suggesting it may be rooted in upholding systemic racism.
- The debate over a theme park rebranding shows how absurd and divisive public discourse has become.
- Beau points out the absurdity of believing that children today prefer references to outdated and problematic content over more contemporary and inclusive themes.
- He stresses the importance of evolving as a society and addressing past mistakes to move forward.

# Quotes

- "You're the reason we can't move forward."
- "If you have an issue with this occurring at a children's park, you are the problem."
- "It's what we do. It's how we move forward."
- "Is there anybody who thinks the average child today going to Disney would rather have a ride reference Song of the South instead of Princess and the Frog?"
- "This is a private company doing what they think is best for their customers."

# Oneliner

Disney's decision to rebrand Splash Mountain sparks debate revealing deep-seated racism and resistance to change, challenging individuals to confront their role in upholding systemic issues.

# Audience

Theme park visitors

# On-the-ground actions from transcript

- Support and appreciate efforts by companies to update their branding to be more inclusive (exemplified).
- Challenge individuals who resist change and confront them about the root of their opposition (exemplified).
  
# Whats missing in summary

The full transcript provides a detailed analysis of the controversy surrounding Disney's decision to rebrand Splash Mountain, challenging individuals to confront their resistance to change and underlying motivations.

# Tags

#Disney #SplashMountain #Rebranding #SystemicRacism #Inclusivity