# Bits

Beau says:

- A number of top military officials, including General Thomas, have issued statements, notably using the word "listen," which is significant.
- General Thomas, the former commander of Special Operations Command, expressed displeasure at referring to the United States as a "battle space."
- The tradition of civilian control of the military usually keeps military officials from speaking out politically, but recent statements indicate a shift.
- The Sergeant Major of the Army, the highest-ranking enlisted person in the Army, also made statements about racial inequality that stirred controversy.
- The Sergeant Major's concise and direct messages were seen as controversial by some, especially his identification of racial inequality as a key issue.
- The use of the word "listen" in the statements by military officials is significant and suggests a message of importance.
- Beau notes the significance of these statements by military leaders who could potentially mobilize volunteers with a single tweet.
- All the generals and influential figures within the military community who have spoken out are unified in opposing the current situation.
- Beau expresses support for military members who have taken a knee with protesters, contrasting it with potential far-right disavowal.
- The overarching theme in the statements from military officials is about the importance of listening and paying attention.

# Quotes

- "NCOs say exactly what they mean."
- "All of the influential people within the military community who have spoken out, they're all on the same side."
- "Listen is not what you'd expect to hear from them if they believed what was going on was a good idea."
- "It's worth noting. It's worth paying attention to."
- "The idea of the far right disavowing soldiers who have taken a knee with protesters has arisen."

# Oneliner

Top military officials break tradition to address political issues with a message of listening and unity against current events.

# Audience

Military members, activists, community leaders.

# On-the-ground actions from transcript

- Contact military officials to express support for their statements and encourage further action (suggested).
- Engage in open dialogues with community members about racial inequality and the importance of listening (implied).

# Whats missing in summary

The full transcript provides additional context on the significance of military officials breaking tradition to speak out against political issues and the importance of unity in addressing current events.

# Tags

#Military #Leadership #Unity #RacialInequality #Listening