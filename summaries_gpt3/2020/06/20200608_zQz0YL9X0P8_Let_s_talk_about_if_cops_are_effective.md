# Bits

Beau says:

- Defining effectiveness as producing the desired result or outcome.
- Questioning the current effectiveness of law enforcement.
- Suggesting to use case clearance rate as a metric rather than conviction rate.
- Providing statistics from 2017 to analyze effectiveness.
- Homicide clearance rate at 61.6% - not highly effective.
- Rape clearance rate at 34.5% - especially low due to underreporting.
- Robbery clearance rate at 29.7% - not effective.
- Aggravated assault clearance rate at 53.3% - still lacking.
- Property crimes like burglary and theft with clearance rates below 20%.
- Advocating for reform to focus on crimes with victims and improve effectiveness.
- Stating that the current system's ineffectiveness is not a reason to avoid reform.

# Quotes

- "They're not effective now."
- "We need reform. We need it."

# Oneliner

Beau questions the effectiveness of law enforcement, using case clearance rates to show the system's ineffectiveness and advocate for reform.

# Audience

Community members, activists, policymakers

# On-the-ground actions from transcript

- Advocate for reforms to improve law enforcement effectiveness (implied)
- Support initiatives focusing on crimes with victims (implied)

# Whats missing in summary

Detailed breakdown of specific reforms needed to enhance law enforcement effectiveness.

# Tags

#LawEnforcement #Effectiveness #Reform #Crime #CommunityPolicing