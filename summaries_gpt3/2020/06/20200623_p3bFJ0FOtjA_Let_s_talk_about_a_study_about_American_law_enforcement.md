# Bits

Beau says:

- Explains the importance of reform and drastic change in policing.
- Mentions a study from the University of Chicago Law School conducted between 2015-2018 focusing on 20 major US cities.
- The study, named Deadly Discretion, assessed whether these cities were in compliance with basic human rights law.
- Out of the four main categories - accountability, proportionality, necessity, and legality - no department was legally required to maintain compliance with basic standards.
- Provides recommendations from the study, such as ending no-knock raids, using lethal force as a last resort, and eliminating qualified immunity.
- Suggests that the study is easily digestible, clear, and can help sway individuals who may not understand the experiences of dealing with law enforcement.
- Emphasizes the need for everyone to be interested in these recommendations, as compliance is not a legal requirement for any city.
- Encourages viewers to keep the study handy for educating others and facilitating informed discussions.

# Quotes

- "Is this whole movement out in the streets, is this even necessary?"
- "Getting rid of no knock raids, requiring lethal force to be as a last resort rather than the officer was scared."
- "A lot of people need self-interest."

# Oneliner

Beau explains the necessity of police reform through a study revealing the lack of legal requirements for departments to comply with basic standards, providing recommendations for necessary changes.

# Audience

Advocates for police reform

# On-the-ground actions from transcript

- Share the study "Deadly Discretion" and its recommendations with others to raise awareness (suggested).
- Advocate for the implementation of recommendations from the study in local law enforcement policies (implied).

# Whats missing in summary

The full transcript provides detailed insights on the lack of legal requirements for police compliance and offers clear recommendations for reform efforts.

# Tags

#PoliceReform #DeadlyDiscretionStudy #CommunityAction #Advocacy #PolicyChange