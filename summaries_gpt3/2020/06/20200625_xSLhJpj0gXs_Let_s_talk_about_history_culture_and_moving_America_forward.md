# Bits
Beau says:

- Addressing the concern of erasing history by putting disclaimers in front of movies, not idolizing certain songs and cartoons, or taking down statues.
- Proposing a need to rebuild history to benefit from the warnings it provides.
- Sharing a piece of the Berlin Wall as a symbol of history in the making and a celebration of freedom.
- Acknowledging the necessity of warnings in history, especially in light of recent events.
- Suggesting replacing statues of slave owners with monuments to abolitionists like Harriet Tubman and John Brown.
- Emphasizing the importance of learning from history's mistakes and not romanticizing or mythologizing it.
- Pointing out the hypocrisy of those defending racist historical figures while denying the existence of systemic racism today.
- Advocating for cultural changes through popular culture to address deep systemic issues.
- Urging for a change in mindset alongside changes in laws to create a more inclusive society.
- Encouraging people to be on the right side of history by acknowledging and addressing the country's past mistakes.
- Calling for the rebuilding of history through monuments that represent positive change and progress.
- Arguing that keeping up statues of controversial figures is embarrassing to the country and suggests putting up monuments that make people proud instead.

# Quotes
- "Freedom will crash through anything eventually."
- "We have to change it all."
- "Let's rebuild history."
- "Make America great."
- "They're embarrassing to the country. Take them down."

# Oneliner
Beau addresses the need to rebuild history by learning from its mistakes and replacing controversial statues with monuments that make us proud.

# Audience
Activists, Historians, Educators

# On-the-ground actions from transcript
- Replace statues of controversial figures with monuments to those who have positively impacted history (implied).
- Advocate for cultural changes through popular culture by promoting inclusive songs, cartoons, movies, and statues (implied).

# Whats missing in summary
The full transcript provides detailed insights into the importance of acknowledging and learning from history's mistakes, advocating for cultural changes, and replacing controversial statues with monuments that represent positive change.

# Tags
#RebuildingHistory #Monuments #CulturalChange #SystemicRacism #Inclusivity