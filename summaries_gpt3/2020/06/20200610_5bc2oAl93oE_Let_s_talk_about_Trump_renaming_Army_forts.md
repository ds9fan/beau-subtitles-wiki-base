# Bits

Beau says:

- Beau addresses the issue of renaming army installations named after Confederate Civil War generals, which has been a long-standing topic but is now at the forefront.
- The Trump administration displayed a lack of courage by refusing to make the changes, opting to delay the decision for someone else to handle.
- Active duty vets Beau spoke with showed unanimous agreement that the installations should be renamed, with some debate on how they should be renamed.
- Beau suggests renaming Fort Bragg to Fort Charles Beckwith, Fort Gordon to Fort Alvin York, Fort Binning to Fort Robert Rogers, and Fort Campbell to honor the first division commander of the 101st, not the Confederate general.
- Beau criticizes the Trump administration for missing an opportunity to show progress and move forward by renaming the installations, instead leaving soldiers stationed at bases named after those who fought to dehumanize them.
- He argues that renaming the installations with more fitting names can give soldiers a sense of pride and accurately represent the modern nature of the US Army.

# Quotes

- "They're going to kick that can down the road, let somebody else deal with it, let somebody else make the changes."
- "We can move forward. We can give a large portion of soldiers an installation name they can be proud of instead of one they don't want to talk about."
- "These guys, the names I gave, these would not be surrendering to the PC crowd."
- "The absolute cowardice of the Trump administration is leaving a large number of those serving in the United States Army stationed at installations named after people who fought to keep them seen as less than human."
- "That's pretty much peak 2020 right there."

# Oneliner

Renaming army installations named after Confederate generals is long overdue, with unanimous support from active duty vets, missed by the Trump administration's lack of courage, leaving soldiers stationed at bases named after oppressors.

# Audience

Soldiers, Activists, Politicians

# On-the-ground actions from transcript

- Rename army installations with names that accurately represent the modern nature of the US Army (suggested).
- Advocate for renaming army installations named after Confederate generals to honor more fitting individuals (implied).

# Whats missing in summary

The emotional impact and detailed reasoning behind each proposed new name for the army installations.

# Tags

#RenamingArmyInstallations #ConfederateGenerals #TrumpAdministration #SystemicRacism #Modernization