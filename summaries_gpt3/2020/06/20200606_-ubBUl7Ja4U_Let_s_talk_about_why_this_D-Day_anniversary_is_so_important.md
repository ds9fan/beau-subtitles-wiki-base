# Bits

Beau says:

- Commemorating D-Day in the US annually, glorifying it, and its significance as a turning point for the country.
- Lack of clear knowledge about the casualties on D-Day due to the chaos of the events.
- Thousands of Americans sacrificing themselves on D-Day against tyranny and injustice.
- Drawing parallels between the chaos of current events and the sacrifices made on D-Day.
- The importance of allowing the current chaos to be a turning point in history.
- Mention of Martin Luther King Jr. as a catalyst for the Civil Rights Movement and his role in spreading awareness.
- Encouraging to research the Long Hot Summer and its relation to civil rights.
- The need for a leader like Martin Luther King Jr. in the current movement against injustice.
- Reference to the controversy surrounding a football player protesting during the national anthem.
- Criticism of those who ignored the message then and now criticize those who take a knee for justice.
- Acknowledgment that change requires more than just 12% of the population to act.
- Emphasizing the importance of supporting and amplifying voices fighting against injustice.
- Urging action to address the systemic issues creating injustice in the country.
- Commitment to join a march supporting black lives matter and being a witness to history.
- Ending with a reminder that change is necessary to prevent cycling back to injustice.

# Quotes

- "It was a turning point for the country."
- "It is a turning point, if we're smart enough to allow it to be."
- "They don't need a leader, we do."
- "Black lives matter."
- "Eventually, changes will be made."

# Oneliner

Beau contemplates the chaos of current events, the need for leadership, and the importance of supporting the fight against injustice, culminating in a commitment to back Black Lives Matter at a march.

# Audience

Activists, Supporters, Allies

# On-the-ground actions from transcript

- Attend marches supporting movements against injustice (exemplified)
- Be a witness to history by actively participating in events supporting marginalized communities (exemplified)

# Whats missing in summary

The emotional impact and personal reflection that Beau shares throughout the transcript.

# Tags

#D-Day #CivilRights #Injustice #BlackLivesMatter #Support