# Bits

Beau says:

- Discloses receiving an educational sample from a company coincidentally before making a positive video mentioning them.
- Shares a message for Mr. Richter on his graduation, acknowledging the challenging times ahead.
- Provides insights into parenting, mentioning that he avoids giving specific advice due to his kids' unique personalities.
- Emphasizes instilling a moral compass and sparking curiosity in children to make education and career choices easier.
- Shares different tactics used to provoke curiosity in his kids, like Minecraft for his eldest and tangible items for his youngest.
- Describes giving his son a lunar material necklace from a company called Mini Museum, sparking curiosity and interest in history.
- Talks about Mini Museum's collection of small samples, including historical artifacts like a piece of the Enigma and Steve Jobs' turtleneck.
- Stresses the importance of leading by example for children to emulate curiosity, reading, and other positive behaviors.
- Concludes by suggesting that recognizing and embracing the uniqueness of each child is vital in parenting.

# Quotes

- "You can't tell a kid anything ever. You have to show them."
- "If you want your kid to be curious, you have to be curious."
- "Recognizing and embracing the uniqueness of each child is an important part of parenting."

# Oneliner

Beau shares insights on parenting, advocating for instilling a moral compass and sparking curiosity while recognizing the uniqueness of each child.

# Audience

Parents, caregivers, educators

# On-the-ground actions from transcript

- Order educational samples or tangible items related to your child's interests to spark curiosity (suggested).
- Lead by example by showing curiosity, reading, and engaging in educational activities in front of children (suggested).

# Whats missing in summary

The full transcript provides a nuanced perspective on parenting, stressing individualized approaches and leading by example to foster curiosity and moral values in children.