# Bits

Beau says:

- Explains qualified immunity as a shield that limits officers' liability, shifting it to taxpayers.
- Notes the current momentum to remove qualified immunity and make officers carry insurance.
- Defends officers' response that they will only show up for real emergencies if personally liable.
- Views officers' unintentional laying of foundation for police reform positively.
- Supports the idea of officers responding only to actual emergencies to prevent unnecessary situations.
- Suggests officers should limit involvement to emergencies, benefiting both them and the public.
- Advocates for giving freedom a chance by reducing over-policing.

# Quotes

- "We don't want to be over-policed."
- "Only respond if it's an actual emergency, if there's a victim."
- "Can we please just give freedom a chance?"

# Oneliner

Beau supports removing qualified immunity, advocating for officers to respond only to real emergencies to prevent over-policing and improve safety.

# Audience

Advocates for police reform.

# On-the-ground actions from transcript

- Support initiatives aiming to remove qualified immunity from law enforcement (suggested).
- Advocate for policies that encourage officers to respond only to actual emergencies (suggested).

# Whats missing in summary

Beau's reasoning behind supporting officers responding solely to real emergencies and the potential positive impact on society.

# Tags

#QualifiedImmunity #PoliceReform #OverPolicing #CommunitySafety #Freedom