# Bits

Beau says:

- Explains that the channel is interactive based on viewer comments and ethical considerations.
- Points out the misuse of statistics from the FBI's Uniform Crime Reporting Program.
- Clarifies that the statistics show demographics suspected or arrested, not actual criminals.
- Addresses the misconception of linking demographics to predisposition for crime as racist.
- Suggests adjusting data for poverty levels to reveal racial disparities disappear.
- Emphasizes that facts aren't racist, but people can be.
- Talks about the debate on the lethality of tasers and the need for consistency in terminology.
- Argues against justifying lethal force based on vague "what-if" scenarios.
- Warns against granting officers the power for summary execution based on unreasonable scenarios.
- Encourages self-examination of beliefs, morals, and biases in light of current events.

# Quotes

- "If you believe somebody's skin tone will increase their likelihood of being a criminal, that's racist."
- "Facts aren't racist. People are."
- "The device does not become more lethal simply because the scary black man is holding it."
- "We don't want facts to get in the way of a good story."
- "Now is the time to see where you really fit in. What your moral fiber is. What your biases are."

# Oneliner

Beau explains the misuse of statistics to establish causal relationships between demographics and crime, and calls for consistency in evaluating police actions and examining personal biases.

# Audience

Viewers, Activists, Educators

# On-the-ground actions from transcript

- Examine your beliefs and biases (suggested)
- Advocate for consistent terminology in evaluating police actions (implied)
- Educate others on statistics and biases (implied)

# Whats missing in summary

In-depth examples and explanations on statistical misuse and racial biases in crime analysis.

# Tags

#Statistics #Bias #Racism #PoliceReform #CommunityPolicing