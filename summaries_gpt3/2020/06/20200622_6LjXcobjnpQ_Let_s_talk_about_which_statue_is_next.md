# Bits

Beau says:

- Raises the question of which statue is next, signaling a shift in perspective.
- Engages in a meaningful exchange with someone seeking to understand systemic issues.
- Dissects the reluctance to acknowledge systemic racism, using Teddy Roosevelt as an example.
- Points out the flaws of historical figures like Teddy Roosevelt while acknowledging their positive aspects.
- Emphasizes the need to confront the systemic issues represented by certain statues.
- Advocates for taking down statues that perpetuate institutional issues.
- Contrasts the historical context of figures like Teddy Roosevelt with modern values.
- Stresses the importance of continuous progress and growth as a society.
- Encourages focusing on individual achievements and embracing change rather than clinging to tradition.
- Reminds viewers of Teddy Roosevelt's stance on criticism and the importance of building a better future for the country.

# Quotes

- "Hidden in this question, which statue is next, is the admission of systemic racism."
- "Taking down these statues, it's not going to change the world. It's one more thing."
- "He was about change. He was about what you as a person did."
- "It's really important to help other people get up, not hold them down."
- "And strong men, tough men, men like Teddy Roosevelt, they weren't afraid of the competition."

# Oneliner

Beau delves into the systemic issues behind statue removals, using Teddy Roosevelt as a lens to challenge perspectives and advocate for progress.

# Audience

Viewers, Activists, Educators

# On-the-ground actions from transcript

- Examine statues in your community that may perpetuate institutional issues and advocate for their removal (implied).
- Engage in constructive dialogues about systemic racism and historical figures to foster understanding and progress (implied).

# Whats missing in summary

Emotional impact of confronting historical figures' flaws and advocating for societal progress through introspection and action.

# Tags

#SystemicIssues #StatueRemoval #TeddyRoosevelt #Progress #InstitutionalChange