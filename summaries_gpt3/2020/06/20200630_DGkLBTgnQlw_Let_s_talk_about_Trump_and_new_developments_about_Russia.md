# Bits

Beau says:

- Questions what the president knew and when he knew it, a frequent concern during this administration.
- Reports from The Military Times suggest the president received information on Russia's activities over a year ago.
- Raises doubts about the president's evolving position, shifting from denial to unverified claims.
- Points out that intelligence is about establishing intent and predicting the future, not about verification.
- Criticizes the lack of action taken by the administration to verify critical intelligence.
- Expresses disbelief at the president's trust in Putin, a former Russian intelligence officer.
- Challenges the administration's inaction and failure to act on significant intelligence.
- Notes the contradiction in distrusting intelligence reports while trusting Russian sources.
- Emphasizes that reports did not originate from the intelligence community but from troops on the ground.
- Criticizes the president for neglecting critical issues while focusing on campaigning and self-promotion.

# Quotes

- "Intelligence is about establishing intent. It's about telling the future. It's a crystal ball."
- "Have skepticism. Have doubts. But please keep in mind, these reports didn't originate with the intelligence community."
- "Leave them as live action target practice for the opposition."
- "Whether you agree with them being there or not, the US sent them there."
- "While the president is planning rallies and tweeting about his enemies in social media, there are bags being filled and he doesn't care."

# Oneliner

Beau questions the president's knowledge, criticizes inaction on critical intelligence, and challenges blind trust in Putin.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Support troops by advocating for proper action to protect them (implied)
- Pressure politicians to prioritize national security over personal interests (implied)
- Advocate for transparency and accountability in intelligence handling (implied)

# Whats missing in summary

The full transcript provides detailed insights into the administration's handling of intelligence and the implications of prioritizing personal interests over national security. Viewing the full transcript gives a comprehensive understanding of these critical issues.

# Tags

#Knowledge #Intelligence #NationalSecurity #Putin #Troops