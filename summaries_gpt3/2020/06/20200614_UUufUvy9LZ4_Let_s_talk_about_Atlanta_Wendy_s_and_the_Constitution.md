# Bits

Beau says:

- Viral videos from 2018 on police tactics are still relevant due to the lack of change in training practices.
- Institutions in the US are reluctant to change, reform, or apply best practices in law enforcement.
- Law enforcement is trained to get the public to excuse, justify, or look away from their actions.
- Beau addresses the incident at Wendy's in Atlanta in relation to the US Constitution and due process.
- Due process is a fundamental concept enshrined in the Constitution, except in cases of immediate threat to life or bodily harm.
- Resistance alone does not warrant the use of lethal force by law enforcement according to Beau.
- Beau recounts the events at Wendy's where a man was shot by law enforcement after resisting arrest and running.
- The argument that law enforcement was justified in using lethal force because the suspect had a taser is challenged by Beau.
- Beau questions the justification of lethal force based on the presence of a non-lethal weapon being used against the suspect.
- Beau criticizes the idea that law enforcement officers can use lethal force without being held accountable, leading to authoritarianism.

# Quotes

- "Resistance alone is not enough for the use of lethal force."
- "If lethal force is justified, everything else is."
- "The premise that leads to authoritarianism."
- "There was no reason to do this."
- "Maybe all of these patriots should actually read the Constitution."

# Oneliner

Beau challenges the justification of lethal force by law enforcement and criticizes the lack of accountability, pointing out the dangers of authoritarianism and the importance of upholding due process for all. 

# Audience

Activists, Reformers, Advocates

# On-the-ground actions from transcript

- Read and understand the US Constitution (suggested)
- Advocate for accountability and transparency in law enforcement practices (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of a specific incident at Wendy's and its implications on law enforcement practices and the US Constitution, urging for accountability and reform.

# Tags

#PoliceTactics #DueProcess #Accountability #LethalForce #Reform