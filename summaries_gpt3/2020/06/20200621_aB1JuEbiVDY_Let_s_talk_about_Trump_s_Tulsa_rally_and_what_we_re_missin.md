# Bits

Beau says:

- Beau addresses the recent events in Tulsa, focusing on the importance of discussing the serious aspects rather than making jokes.
- The controversy surrounding the date and location of the event is mentioned, especially considering the current health concerns of gathering people closely.
- Trump exaggerated the expected crowd size, claiming a million people were interested when only 6,200 showed up.
- The discrepancy in numbers was due to younger Americans trolling by requesting tickets with multiple email addresses, leading to an inflated count.
- Despite the low turnout, the attendees were packed closely together without social distancing measures, risking their health for a photo op.
- Beau criticizes Trump for prioritizing his ego over the safety of his supporters and the country, showcasing his lack of leadership qualities and disregard for public health.
- The rally's failure is attributed to Trump's unwillingness to prioritize safety over appearances, ultimately showing his true priorities and character.

# Quotes

- "He jeopardized his biggest supporters."
- "He could have protected the country, but he chose not to for a photo op."
- "That's embarrassing."

# Oneliner

Beau addresses the serious implications of Trump's Tulsa rally, where his ego was prioritized over public safety, jeopardizing his supporters and showcasing his lack of leadership.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Contact local representatives to advocate for responsible and safe public events (implied)
- Organize community initiatives promoting public health and safety during gatherings (implied)

# Whats missing in summary

The full transcript provides a detailed analysis and commentary on Trump's Tulsa rally, offering a critical perspective on leadership, public health, and priorities that cannot be fully captured in this summary.

# Tags

#Trump #Tulsa #Leadership #PublicHealth #CommunityPolicing