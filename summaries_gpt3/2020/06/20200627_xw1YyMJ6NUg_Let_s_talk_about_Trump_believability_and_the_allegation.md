# Bits

Beau says:

- President of the United States faced wild allegation involving Russian intelligence incentivizing attacks against American soldiers in Afghanistan.
- Legal definition of treason in the US is narrow; looking the other way during attacks isn't legally treasonous.
- People are justifying President's inaction with excuses, rather than denying the allegation's truth.
- Russia's motive could be to derail peace deals and weaken the US military by incentivizing attacks.
- Allegation's truth doesn't matter; what's relevant is the President's history of self-centered actions benefiting Russia.
- American people lack confidence in the President; the allegation should be unbelievable but is disturbingly believable.
- The allegation's believability should be reason enough for resignation or non-re-election.
- Republican Party's support for the President implicates them in his actions, including this latest allegation.
- President mobilized to protect statues of racists but didn't act to protect American soldiers in Afghanistan over months.
- President's track record of lies makes it plausible he'd allow soldiers to die to avoid admitting fault or losing support.

# Quotes

- "It is completely believable that Russian intelligence would do this. It is completely believable that Trump would look the other way."
- "The president of the United States does not have the confidence of the American people."
- "If the Republican Party hands him the nomination, they own this."

# Oneliner

President faced wild allegation of inaction amid Russian intel on attacks against US soldiers; his history makes the disturbingly believable allegation enough for resignation or non-re-election.

# Audience

American voters

# On-the-ground actions from transcript

- Pressure elected officials to hold the President accountable (implied)
- Stay informed and engaged in politics to ensure accountability (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the wild allegation against the President and the implications of his inaction, urging for accountability and reflection on his actions.