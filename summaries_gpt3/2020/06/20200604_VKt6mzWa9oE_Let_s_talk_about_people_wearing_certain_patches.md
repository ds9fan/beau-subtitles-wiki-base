# Bits

Beau says:

- Explains the origin of the symbol "3%" in the United States, tied to historical inaccuracies about the colonists.
- Mentions that groups using the "3%" symbol are decentralized, with each group being different from the next.
- Describes the mission statement of the "3%" movement as defending the Bill of Rights and stopping government overreach.
- Criticizes individuals wearing the "3%" patch who go against the principles it supposedly stands for.
- Expresses disappointment in law enforcement wearing morale patches without understanding their meaning.
- Concludes by suggesting that law enforcement should refrain from wearing morale patches.

# Quotes

- "If you are currently holding a shield and a club, staring at Americans in the street exercising their free speech and peaceably assembling, and you're attempting to antagonize it and intimidate them and turn it to a situation where it is not peaceable assembly, you are not a threeper."
- "You're literally actively standing against what that patch means while wearing it."
- "Take that patch off."
- "They're not boot lickers. They're the boot."
- "Y'all have a good day."

# Oneliner

Beau explains the origins of the "3%" symbol, criticizes those who misuse it, and suggests law enforcement should avoid morale patches.

# Audience

Community members

# On-the-ground actions from transcript

- Remove any patches or symbols that represent ideologies you do not fully understand or support (implied).
- Refrain from wearing morale patches that carry meanings beyond your understanding (implied).

# Whats missing in summary

Beau's engaging storytelling and passionate delivery convey a strong message against misrepresenting symbols and ideologies.

# Tags

#Symbols #Misuse #LawEnforcement #CommunityPolicing #Accountability