# Bits

Beau says:

- Beau addresses historical facts and events that are often overlooked or not given enough coverage.
- Public school teachers can suggest topics for Beau to cover, regardless of his comfort level.
- Freedom can be intimidating, leading individuals to stick with what is familiar and predictable.
- The topic of black people and watermelons is discussed, shedding light on the historical significance.
- After slavery ended, many newly freed individuals turned to growing watermelons as a means of sustenance and income.
- Growing watermelons provided hope and a chance for advancement for these individuals.
- Watermelon should symbolize pride and freedom for black people, but it was twisted into a derogatory stereotype.
- The perpetuation of jokes and stereotypes about watermelons robbed black individuals of a source of pride.
- The tactic of using one group to look down upon to elevate others is a manipulation by those in power.
- Beau condemns the racist jokes and stereotypes surrounding watermelons, advocating for their eradication.

# Quotes

- "Watermelon should be the fruit of freedom."
- "It's been turned into a stereotype, and when I imagine that if I was black, I don't know that I would want to eat watermelon in public because of the stereotype."
- "Those jokes need to go away, not just because they're racist and they're stupid, but because it should be something that they could take pride in."

# Oneliner

Beau sheds light on the historical significance of black individuals growing watermelons post-slavery and condemns the racist stereotypes attached to it.

# Audience

History enthusiasts, educators, activists

# On-the-ground actions from transcript

- Challenge and confront racist jokes and stereotypes (implied)
- Educate others about the historical significance of watermelons for black individuals (implied)
- Support and uplift black voices and narratives (implied)

# Whats missing in summary

The emotional impact and depth of historical injustice addressed by Beau can be fully grasped by watching the full transcript. 

# Tags

#History #Racism #Watermelon #Freedom #Stereotypes #Injustice