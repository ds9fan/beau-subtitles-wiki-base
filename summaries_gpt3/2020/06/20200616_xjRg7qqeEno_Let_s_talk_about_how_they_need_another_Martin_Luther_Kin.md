# Bits

Beau says:
- People keep calling for another Martin Luther King, but he argues that it's not about needing a leader like MLK, but rather people who look like him taking responsibility to bring about change.
- Acknowledges that some may be speaking out for social credit, but points out that speaking out is often a vocation of agony and a necessary action.
- Notes the urgency within the black community who believe that white America determines how long change will take, leading to continual protests.
- Beau points out that riots don't happen out of thin air and stem from certain societal conditions that need to be condemned just as vigorously as the riots themselves.
- Urges people to listen to what is being said and understand that riots are often the result of being unheard.
- Emphasizes that Martin Luther King preached nonviolence, not non-confrontation, citing his work in Letters from Birmingham Jail as an example of his willingness to challenge the system.
- Compares current protests to historical lunch counter protests, indicating that the struggle for equality continues and those staying silent are on the wrong side of history.

# Quotes

- "Because in the final analysis, riots are the language of the unheard."
- "If you don't like what I just said, you don't really want another Martin Luther King."
- "There comes a time when silence is betrayal."

# Oneliner

People calling for another Martin Luther King misunderstand the need for personal responsibility in creating change and the urgency within marginalized communities.

# Audience

Activists, Allies, Community Members

# On-the-ground actions from transcript

- Join protests and demonstrations (implied)
- Speak out against societal conditions that lead to unrest (implied)
- Support marginalized voices and communities (implied)

# Whats missing in summary

The emotional depth and nuanced perspective offered by Beau in discussing the current state of protests and social movements.

# Tags

#SocialJustice #CivilRights #CommunityAction #Activism #Responsibility