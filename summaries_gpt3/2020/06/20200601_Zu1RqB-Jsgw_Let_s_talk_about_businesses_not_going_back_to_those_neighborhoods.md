# Bits

Beau says:

- Addresses the common statement about businesses not returning to certain neighborhoods due to potential risks.
- Questions the subtext of the statement and what it implies about the value of lives in those neighborhoods.
- Raises concerns about the economic decisions made by businesses and the disregard for the well-being of the residents.
- Suggests that the focus should shift towards supporting the communities economically rather than solely on businesses returning.
- Calls for change and investment in the people of these neighborhoods to break the cycle of neglect and lack of accountability.
- Challenges the notion that financial concerns outweigh the importance of people's lives in these areas.
- Emphasizes the need for representation and economic empowerment to prevent future tragedies.
- Criticizes the prioritization of money over human lives in the context of businesses abandoning neighborhoods.
- Urges for a shift in perspective towards prioritizing the well-being and economic stability of the residents.
- Encourages angel investors to support and empower the communities for meaningful change.

# Quotes

- "A pile of unarmed people, that's just the cost of doing business in those neighborhoods."
- "Money's what matters, not a pile of people."
- "Maybe it's better if all of those people who are so concerned about the shopping choices of those people in those neighborhoods, maybe a fund could be put together."
- "It really might be time for the angel investors of the world to help out the people in those neighborhoods."
- "It's a tragedy that it's going to happen again, and that it's something we need to fix because it's going to happen again, but because there's going to be a loss of money."

# Oneliner

Businesses not returning to certain neighborhoods reveal a stark reality: money matters more than lives in these communities, prompting a call for economic empowerment and change.

# Audience

Community members, activists, investors

# On-the-ground actions from transcript

- Support community funds for economic empowerment (suggested)
- Encourage angel investors to invest in neighborhoods (suggested)

# Whats missing in summary

The full transcript provides a deeper exploration of the economic dynamics and societal implications surrounding businesses' decisions to return to certain neighborhoods. Viewing the full transcript offers a comprehensive understanding of the importance of economic empowerment in creating lasting change.

# Tags

#CommunityEmpowerment #EconomicJustice #BusinessDecisions #SocialChange #Accountability