# Bits

Beau says:

- Addressing the guard on active duty, acknowledging their conflicted feelings.
- Describing a powerful interaction where a captain couldn't march with protesters due to duty constraints.
- Criticizing politicians who misunderstand and misrepresent the military for their own gain.
- Recognizing the soldiers' training, discipline, and reasoning skills within the modern military.
- Emphasizing the importance of soldiers policing their own people professionally.
- Noting the misunderstanding of soldiers as mere tools by politicians.
- Encouraging soldiers to maintain restraint and uphold American freedoms during unrest.
- Presenting an unprecedented chance for soldiers to protect American freedom by using their training and discipline.
- Urging soldiers to show politicians their loyalty to the American people and act accordingly.
- Ending with a message of empowerment and quiet determination for soldiers.

# Quotes

- "Keep us safe. Keep us safe. [...] That was the moment it all made sense, why they wanted you."
- "They don't understand the modern military."
- "You're the weapon. A walking, talking, breathing, thinking, reasoning weapon."
- "You can truly protect American freedom."
- "Make sure you act in a manner befitting who you are, not who they want you to be."

# Oneliner

Addressing the guard on active duty and politicians' misunderstanding, Beau empowers soldiers to protect American freedom with reason and discipline amidst unrest.

# Audience

Soldiers on active duty

# On-the-ground actions from transcript

- Show restraint and uphold American freedoms in interactions with protesters (implied)
- Act professionally and in accordance with values when deployed (implied)

# Whats missing in summary

The full transcript offers a deeper understanding of the complex dynamics soldiers face in policing their own people and the unique chance to protect American freedom in the midst of unrest.

# Tags

#Soldiers #AmericanFreedom #Policing #Professionalism #Empowerment