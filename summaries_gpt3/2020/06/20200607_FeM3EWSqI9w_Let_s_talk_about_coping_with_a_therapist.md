# Bits

Beau says:

- Beau talks about coping mechanisms and reaching out for help from Jody Strach, a therapist and mindfulness practitioner.
- Jody shares her journey into mindfulness, starting from marriage and family therapy to practicing yoga and meditation.
- Jody explains the importance of personal mindfulness practice in being able to help others effectively.
- Jody addresses coping with the stress and changes in society, focusing on acknowledging emotions and finding inner strength.
- The importance of turning inwards, embracing difficulties, and using mindfulness to navigate challenging situations is discussed.
- Jody encourages self-compassion and self-care practices to alleviate stress and anxiety during turbulent times.
- Dealing with feelings of hopelessness and paralysis by turning inward, grounding oneself, and taking deep breaths is emphasized.
- The concept of community service as a way to connect, support others, and find empowerment is explored.
- Jody stresses the significance of looking inward, exploring emotions, and connecting with others to create positive change.
- Recommendations for resources like Deborah Edenthal's work and Ruth King's book "Mindful of Race" are shared for further exploration.

# Quotes

- "Anger is a natural response when there's a violation of a boundary, that anxiety is such a human response to feeling threatened."
- "If there's a part of you that feels completely hopeless right now, really turning towards that part, finding a way to just meet that part and know that's not all of who we are."
- "Trusting that we start with looking at our own responses to things."

# Oneliner

Beau and Jody dive into coping mechanisms, mindfulness practices, and the power of community service amid societal challenges.

# Audience

Individuals seeking coping strategies and inner strength.

# On-the-ground actions from transcript

- Connect with a local community service group to volunteer and support those in need (suggested).
- Practice self-compassion and mindfulness techniques in daily life to alleviate stress (exemplified).
- Read "Mindful of Race" by Ruth King for insights on navigating racial issues (suggested).

# Whats missing in summary

Exploration of the interconnectedness between personal mindfulness practice, community engagement, and societal change.

# Tags

#CopingMechanisms #Mindfulness #CommunityService #SelfCompassion #InnerStrength