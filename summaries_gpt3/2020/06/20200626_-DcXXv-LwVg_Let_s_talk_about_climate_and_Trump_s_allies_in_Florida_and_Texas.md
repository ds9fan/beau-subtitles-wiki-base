# Bits

Beau says:

- Explains the aftermath of failed reopenings in states like Florida and Texas led by President Trump's allies.
- Points out how politicians, influenced by economic power, ignored scientists' advice and pushed for early reopening.
- Describes the pattern of creating counter-narratives against scientific consensus in various issues, like climate change.
- Emphasizes that unlike rolling back reopenings, there's no turning off the irreversible effects of climate change once it reaches a critical point.
- Urges to prioritize addressing climate change over playing into the culture war led by profit-driven politicians.

# Quotes

- "We have to stop listening to the people who put profits over people's lives."
- "We have one planet and the governors and leaders will not be able to do anything once it gets past a certain point."

# Oneliner

Failed reopenings show the danger of prioritizing profit over lives, urging action against climate change before irreversible damage.

# Audience

Climate activists, Environmentalists, Advocates

# On-the-ground actions from transcript

- Push for policies that prioritize environmental sustainability and address climate change (suggested)
- Support and vote for leaders who prioritize science and people over profit-driven interests (implied)

# Whats missing in summary

Importance of immediate action to prevent irreversible consequences of climate change.

# Tags

#ClimateChange #ScientificConsensus #PoliticalInfluence #ProfitOverPeople #EnvironmentalActivism