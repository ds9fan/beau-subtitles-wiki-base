# Bits

Beau says:

- People are expressing they've "seen enough" in response to footage from across the United States, leading to forward-thinking ideas for law enforcement reform.
- Suggestions range from drastic reforms to possible abolition, replacing law enforcement duties with social workers.
- Beau doesn't advocate for any particular idea but addresses common questions: what if cops quit with reforms, and what if reforms go too far?
- Beau dismisses concerns about cops quitting due to reforms, saying it's a reason to celebrate if they do because they shouldn't be cops if they resist positive change.
- He challenges worries about reforms being ineffective compared to the current state of law enforcement ineffectiveness captured in news footage.
- Beau believes that the level of force used by law enforcement is unacceptable, criminal in many cases, and far beyond mere unprofessionalism.
- He criticizes departments for merely suspending officers involved in misconduct, suggesting that such behavior should result in jail time.
- Beau insists on the need for drastic change in law enforcement and defers to experts on determining the specifics.
- While unsure of the best solution, Beau is certain that the current situation involving excessive force is entirely wrong.
- He supports officers quitting if they can't adapt to the necessary changes, as the public has witnessed more than enough.

# Quotes

- "If they're gonna quit because new policies are coming along that are gonna stop them from engaging in excessive force and hurting people, they shouldn't be cops."
- "We need drastic change, and I will defer to the experts on what that change needs to be."
- "It's exactly what should happen because we have seen enough."

# Oneliner

Beau challenges concerns about law enforcement reforms and supports drastic change, asserting that quitting over refusing to adapt to new policies is warranted.

# Audience

Law enforcement reform advocates

# On-the-ground actions from transcript

- Celebrate and support officers quitting if they resist reforms (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the current state of law enforcement and the urgent need for drastic change to address excessive force and misconduct.

# Tags

#LawEnforcementReform #ExcessiveForce #SocialChange #CommunityAction #Accountability