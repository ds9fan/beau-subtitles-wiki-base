# Bits

Beau says:

- Addressing the need for systemic reform and change in law enforcement culture.
- Describing the frustration of trying to explain the necessity for deep systemic change in law enforcement to others.
- Expressing anger and frustration when others try to excuse the flaws in law enforcement despite presenting statistics and evidence.
- Proposing to illustrate the need for systemic change by dissecting a specific case.
- Recalling various cases of unarmed individuals killed by police officers, struggling to identify a specific case mentioned in an article.
- Ending with the acknowledgment of numerous unarmed people killed by the police and suggesting a need for system change.

# Quotes

- "There are so many unarmed people killed by police, journalists can't even keep track."
- "We might want to take that as a sign that we need to change the system."
- "I think the easiest way to do it is to illustrate something that actually happened."

# Oneliner

Beau addresses the need for systemic reform in law enforcement, expressing frustration over the failure to understand the necessity for change, and recalling cases of unarmed individuals killed by the police.

# Audience

Activists, Advocates, Citizens

# On-the-ground actions from transcript

- Advocate for systemic reform in law enforcement (suggested)
- Educate others on the importance of systemic change in law enforcement (suggested)

# Whats missing in summary

The full transcript provides a deeper understanding of the challenges faced in advocating for systemic change in law enforcement and the emotional toll of trying to explain the necessity for reform.

# Tags

#SystemicReform #LawEnforcementCulture #PoliceViolence #UnarmedVictims #CommunityPolicing