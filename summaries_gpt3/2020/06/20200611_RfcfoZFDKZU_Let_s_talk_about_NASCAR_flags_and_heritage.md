# Bits

Beau says:

- NASCAR took a moral stand by banning Confederate flags at their events, showing more courage than the President of the United States.
- The President dodged addressing the issue involving the names of some U.S. Army installations, opting to pass it on to someone else.
- NASCAR has a history of striving to be inclusive as an organization, in contrast to some of its fan base.
- The ban on Confederate flags at NASCAR events has stirred controversy, particularly in the South.
- NASCAR's roots in hot-rodding and bootlegging tie into a historical defiance against unjust authority, including against taxes and law enforcement.
- NASCAR's essence has always been about defunding the police and resisting unjust systems.
- People criticizing NASCAR's stance on police accountability fail to recognize the organization's heritage and lineage.
- Organizations like NASCAR are no longer pandering to certain audiences and are pushing for growth and change.
- Failure to evolve over time and adapt to changing norms indicates a lack of personal growth and development.
- Self-reflection is necessary for personal and societal growth, especially when faced with changing ideologies and values.

# Quotes

- "NASCAR took a moral stand by banning Confederate flags at their events, showing more courage than the President of the United States."
- "Failure to evolve over time and adapt to changing norms indicates a lack of personal growth and development."
- "If all of your idols, if all of the organizations you like are suddenly turning against you, maybe it's because you didn't keep up with the times."

# Oneliner

NASCAR's bold stance on Confederate flags and police accountability exposes the need for growth and reflection in the face of evolving values.

# Audience

Fans of NASCAR and advocates for social change.

# On-the-ground actions from transcript

- Support organizations and companies that take a stand against injustice, like NASCAR (exemplified).
- Engage in self-reflection and personal growth to adapt to changing values and ideologies (implied).

# Whats missing in summary

The full transcript expands on the historical roots of NASCAR, the reactions to their stance on Confederate flags and police accountability, and the importance of evolving with the times to foster personal and societal growth.

# Tags

#NASCAR #ConfederateFlags #PoliceAccountability #Growth #SocialChange