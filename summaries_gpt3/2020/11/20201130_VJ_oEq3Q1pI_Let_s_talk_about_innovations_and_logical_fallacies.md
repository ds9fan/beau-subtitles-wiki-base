# Bits

Beau says:

- Discussed logical fallacies like the genetic fallacy and the fallacy of composition.
- Pointed out that ideas and inventions stand on their own merit, regardless of their origin.
- Mentioned historical dangers of these fallacies being used together.
- Credited US military for various innovations and inventions.
- Argued that military challenges and self-interest spur innovation more effectively than just research funding.
- Suggested that a global threat, like climate change, could mobilize humanity and lead to technological breakthroughs.
- Advocated for framing climate change as a jobs program to appeal to a broader audience.
- Proposed that the technological benefits of combating climate change could sway skeptics.
- Called for a World War II style mobilization to address climate change effectively.
- Stressed the importance of utilizing all available tools to combat climate change and achieve real progress.

# Quotes

- "Ideas, innovations, inventions, they stand and fall on their own."
- "But it's normally used in the negative sense. This isn't true."
- "We need a World War II style mobilization to get any real traction."
- "If we engage in this kind of mobilization, we will get technological breakthroughs that will send us ahead."
- "Anyway, it's just a thought."

# Oneliner

Beau discussed logical fallacies, credited the military for innovations, and advocated for a World War II style mobilization to combat climate change effectively.

# Audience

Activists, policymakers, educators

# On-the-ground actions from transcript

- Mobilize for climate action with community-based initiatives (implied)
- Advocate for framing climate change as a jobs program to reach a wider audience (implied)
- Engage skeptics by discussing the technological benefits of combatting climate change (implied)

# Whats missing in summary

Beau's detailed examples and explanations on the importance of challenging logical fallacies and spurring innovation through military-like mobilizations are worth exploring further in the full transcript.

# Tags

#LogicalFallacies #Innovation #USMilitary #ClimateChange #Mobilization #TechnologicalBreakthroughs