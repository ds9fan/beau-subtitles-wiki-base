# Bits

Beau says:

- An email from a daughter expresses concerns about her 67-year-old father with multiple medical conditions who is at risk but refuses to wear a mask, waiting for Beau to shave his beard first.
- Beau, a hermit who rarely leaves his property, initially shaved his beard to set a good example and comply with mask-wearing.
- Beau stresses the seriousness of the current situation, pointing out that it's worse than ever and the President is not adequately focused on it.
- Urging everyone to wear masks, wash hands, avoid touching faces, and stay home if possible, Beau underlines the importance of following safety protocols.
- Despite the dire situation, Beau reminds listeners to exercise caution as it's going to get worse with colder weather approaching.

# Quotes

- "Please wear a mask, wash your hands, don't touch your face, stay at home if you can."
- "It is worse now than it has ever been."
- "He's focused on other things. There was little response to begin with. There is no response right now."

# Oneliner

Beau stresses the seriousness of the current COVID-19 situation, urging everyone to wear masks and follow safety protocols as it's worse than ever and precautions are vital with colder weather approaching.

# Audience

General public

# On-the-ground actions from transcript

- Wear a mask, wash hands, avoid touching face, and stay at home if possible (suggested)
- Exercise caution and follow safety protocols diligently (suggested)

# Whats missing in summary

The importance of taking precautions and following safety protocols to combat the worsening COVID-19 situation.

# Tags

#COVID19 #MaskUp #SafetyProtocols #Precautions #PublicHealth