# Bits

Beau says:

- Talks about the type of government and political party we want.
- Expresses surprise that forward-thinking individuals expect major political parties to fully represent their beliefs.
- Encourages forward-thinking individuals to keep looking for the next issue and engagement.
- Suggests that major political parties and governments will never completely match radical beliefs.
- Emphasizes the importance of continuous progress and change, as there will always be new issues.
- Asserts that change comes from people, not government, and governments create reform, not real change.
- Encourages radical individuals to stay in the fight and lead by example.
- States that those on the fringe have volunteered to be on point and lead society through obstacles.
- Reminds that if you change enough minds, laws may not even need to change because society has shifted.
- Concludes by reassuring that those leading the way will eventually bring others along.

# Quotes

- "Change comes from you, not from DC."
- "Governments create reform. People create change."
- "You're leading the way."

# Oneliner

Beau talks about the type of government and political party we want, encouraging forward-thinking individuals to keep looking for the next issue and engagement, as change comes from people, not government.

# Audience

Forward-thinking individuals

# On-the-ground actions from transcript

- Lead by example and keep looking for the next issue and engagement (exemplified)
- Change minds to influence laws and society (exemplified)
- Clear obstacles for others as you lead the way (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of the difference between reform and real change, as well as the continuous nature of progress and societal evolution.

# Tags

#Government #PoliticalParty #Change #ForwardThinking #Leadership