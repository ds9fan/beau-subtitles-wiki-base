# Bits

Beau says:

- Society changes incrementally over time by shifting thought, not just laws.
- Progressives tend to win because they focus on shaping the future, not just controlling the present.
- Change is a process that requires looking ahead to how we want people to behave in the future.
- Effort and courage need purpose and direction to make a meaningful impact.
- Small changes in behavior now can influence thought and societal norms later.
- Time is a tool for change, not a barrier, and change should happen gradually.
- Historical movements were built on slowly changing opinions and eroding barriers.
- Seize moments when big changes are possible, but also focus on daily progress towards long-term goals.
- Building towards a better future requires understanding that progress may extend beyond our lifetime.
- Incremental change is a proven template for societal progress, both in positive and negative aspects.

# Quotes

- "Change is the law of life."
- "Effort and courage are not enough without purpose and direction."
- "Progressives tend to want to shape the future a little bit more."
- "Seize moments when you can make the big changes."
- "A nation reveals itself not only by the minute produces but also by the minute honors the minute remembers."

# Oneliner

Society changes incrementally through shifting thought, not just laws, focusing on shaping the future, and seizing key moments for progress.

# Audience

Community members

# On-the-ground actions from transcript

- Build parallel structures like community gardens or networks to gradually shift societal norms (exemplified).
- Educate and instill progressive values in future generations to continue the path towards a better world (implied).

# Whats missing in summary

The full transcript provides a detailed roadmap for achieving societal change through incremental shifts in behavior and thought over time.

# Tags

#SocietalChange #Progressives #Future #CommunityGardens #IncrementalChange