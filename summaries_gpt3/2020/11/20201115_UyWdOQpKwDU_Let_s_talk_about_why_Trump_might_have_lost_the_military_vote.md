# Bits

Beau says:

- Explains why a certain demographic didn't vote as expected in the recent election.
- Shares favorite quote about military personnel voting Democrat, contrary to expectations.
- Challenges the stereotype of the ultra conservative, socially regressive military.
- Points out the diversity within the military and reasons why some may have issues with Trump.
- Mentions specific concerns like Trump's handling of military installations' names and hurricane response.
- Talks about soldiers' dissatisfaction with Trump's attitude towards personal protective gear.
- Notes opposition to Trump's refusal of a peaceful transition of power and treatment of respected generals.
- Reveals that the betrayal of Kurdish allies by Trump was a major concern for every soldier Beau talked to.
- Suggests that judging a demographic by its most visible members is not representative of the whole.
- Warns Democrats not to assume they have secured the military vote long-term due to Trump's unpopularity.

# Quotes

"Saved it to the end because it's the only thing that everybody that I talked to said."
"The Kurds were our greatest ally in the Middle East, and Trump sold them out."
"No demographic should be judged by the most visible members of that demographic."

# Oneliner

Military personnel surprised by Trump's actions didn't vote as expected, showing the diversity and individuality within the ranks.

# Audience

Military personnel and political analysts

# On-the-ground actions from transcript

- Reach out to military personnel to understand their concerns and perspectives (suggested)
- Advocate for policies that support military members and address their grievances (suggested)

# Whats missing in summary

The full video provides more in-depth insights into the diverse reasons why military personnel may have voted unexpectedly and the implications for future elections. 

# Tags

#Military #Election #Trump #Demographics #Diversity