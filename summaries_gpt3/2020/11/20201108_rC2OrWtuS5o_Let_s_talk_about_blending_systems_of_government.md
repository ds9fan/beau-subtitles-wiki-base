# Bits

Beau says:

- Exploring the idea of comparable systems to social democracy, discussing a blend between normal capitalism, Western governments, and fascism.
- Outlining 14 characteristics of fascism according to Lawrence Britt, focusing on practical examples for Americans.
- Describing a light version of powerful nationalism, with flags symbolizing the nation rather than overt displays like marches.
- Touching on the disdain for human rights in a more subtle, de facto manner rather than legislated.
- Differentiating between internal and external enemies as a unifying cause in fascist regimes.
- Imagining a light version of military supremacy with revered status and a significant budget share.
- Addressing rampant sexism in a de facto manner in a male-dominated society.
- Comparing controlled mass media in terms of media collusion with government talking points.
- Exploring the obsession with national security and its impact on government compliance.
- Touching on the intertwining of religion and government with subtle rituals to show allegiance.
- Mentioning protection of corporate power, suppression of labor power, and disdain for intellectuals in the arts.
- Noting the obsession with crime and punishment leading to a large prison population.
- Describing rampant cronyism and corruption through examples like no-bid contracts and nepotism.
- Hinting at fraudulent elections and the facade of legitimacy in such regimes, with parallels to the United States.

# Quotes

- "If you want to know what fascism light looks like, look out the window."
- "The United States is the blend."
- "The major parties keep everything in house and really only give you a couple options."
- "Y'all have a good day."
- "The biggest dangers to the United States is the creep towards authoritarian rule."

# Oneliner

Beau outlines characteristics of fascism in a light version, drawing parallels to the United States' blend between capitalism, Western governments, and subtle authoritarianism.

# Audience

American citizens

# On-the-ground actions from transcript

- Analyze the existing political systems and their implications (suggested)
- Stay vigilant about authoritarian creep towards right-wing ideologies (implied)

# Whats missing in summary

The full transcript provides a detailed comparison between social democracy and fascism light, prompting viewers to critically analyze their societal and political structures.

# Tags

#Fascism #SocialDemocracy #PoliticalSystems #UnitedStates #Authoritarianism