# Bits

Beau says:

- Shares a personal story within his circle of friends, describing an inspirational journey from a laborer on a construction crew to a successful general contractor.
- The individual started off making $7 an hour, worked hard, made good decisions, and got lucky to achieve success.
- Despite his success, the general contractor still spends a day each week working with his crews to maintain quality and show solidarity.
- Another contractor in the same industry noticed that the general contractor's crews worked faster and asked for the secret.
- The key to the general contractor's success was sharing the early completion bonus with his employees.
- Beau mentions a recent minimum wage increase in Florida to $15 an hour, with the general contractor paying his employees a minimum of $19.
- Beau observes a common trend where individuals who rise from humble beginnings tend to be the best leaders rather than bosses.
- Successful capitalists often exhibit socialist behaviors by ensuring their employees have a stake in the company's success.
- Beau suggests that rewarding labor and giving employees a stake in the company's success leads to better performance and profitability.
- He questions the idea of striving to pay employees as little as possible and proposes that rewarding labor is key to generating wealth.

# Quotes

- "Those people who make it, you know, who came from kind of the bottom and worked their way up, they always tend to be the best boss."
- "The best capitalists that I have ever met all behaved like socialists when it came to their employees."
- "Maybe we shouldn't be striving to pay employees as little as possible."

# Oneliner

Beau shares an American Dream story of a laborer turned successful general contractor who prioritizes rewarding labor and employee well-being over profit margins.

# Audience

Business Owners, Employers

# On-the-ground actions from transcript

- Share early completion bonuses with employees (implied)
- Pay above minimum wage to show appreciation for labor (implied)

# Whats missing in summary

The full transcript includes insights on the importance of treating employees well and rewarding their efforts to drive success and profitability in a business.

# Tags

#Leadership #LaborRewards #EmployeeWellBeing #SuccessStories #Capitalism