# Bits

Beau says:

- Talks about a prevailing tone in the nation post-election.
- Describes his close circle of friends who support each other no matter what.
- Mentions how some friends supported Trump initially but withdrew support over border policies.
- Explains that his friends, former military contractors, have their own set of morals.
- Recounts how debates during elections ceased when real actions unfolded.
- Differentiates between close friends with integrity and others who didn't withdraw support for Trump.
- Shares a personal experience of cutting off contact with friends who supported immoral actions.
- Emphasizes that the issue was about morality, not politics.
- Asserts his stance on not reconciling with those who crossed moral lines.
- Stresses the importance of addressing moral failures before healing and reconciliation can occur.
- Compares the need for facing moral failures in America to Germany's handling of its past.
- Calls for public acknowledgment of past wrongdoings and faults before moving forward.
- Expresses the need for transparency and examination of moral failings before reconciliation.
- States that forgiveness is a personal matter between individuals and their beliefs.
- Concludes by advocating for laying bare and discussing the moral failings of the nation before seeking reconciliation.

# Quotes

- "It's not politics. That wasn't politics. That was morality."
- "Morality doesn't run on an election cycle. You either have it or you don't."
- "We can't move forward until it's all exposed."
- "Until the moral failings of large portions of this country are laid bare, discussed, examined, and we figure out why they happened, I'm not ready for reconciliation."
- "Because I have no desire to interact with them."

# Oneliner

Beau stresses the importance of addressing moral failings before seeking reconciliation and healing in the aftermath of political divisions.

# Audience

People reflecting on moral integrity.

# On-the-ground actions from transcript
- Have open, honest dialogues about past moral failings and faults (implied).
- Encourage public acknowledgment and examination of moral failings in the nation (implied).

# Whats missing in summary

The emotional depth and personal conviction Beau conveys through his experience and reflections on morality and reconciliation.

# Tags

#MoralFailings #Reconciliation #Healing #AddressingHistory #Friendship