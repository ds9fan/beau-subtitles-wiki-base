# Bits

Beau says:

- Talks about standardized responses in political discourse.
- Received a message critiquing his views on housing, healthcare, and ending hunger, comparing guaranteed services to prison or slavery.
- Responds by acknowledging that prisoners do receive basic necessities like shelter, food, and healthcare.
- Contrasts the provision of services in the military to society, where it's geared towards productivity and effectiveness.
- Points out that if everyone in society was supported like the military, focusing on a collective mission, the world could be better.
- Argues that the provision of basic needs in prison doesn't imply that society values these rights for all.
- Suggests that societal structures are designed to control people through coercion and fear of losing basic needs.
- Criticizes the focus on individual success rather than collective well-being and societal change.
- Advocates for a shift towards a society where people unite behind principles for the common good.
- Encourages reevaluation of societal norms and motivations handed down through generations.

# Quotes

- "If they were committed to the mission of I don't know making the world a better place. Might be a better world."
- "Because it is a human right. Because it's something that's required to live."
- "Maybe the way we look at things should change too."
- "Maybe we should be focused on building that society where people do have a mission."
- "Not just making sure our betters stay in power."

# Oneliner

Beau questions societal norms by examining basic needs provision in prison and advocating for collective well-being over individual success.

# Audience

Social Activists

# On-the-ground actions from transcript

- Question societal norms and advocate for collective well-being (exemplified)

# Whats missing in summary

The full transcript provides a deeper insight into societal structures and the need for a shift towards prioritizing collective well-being over individual success.