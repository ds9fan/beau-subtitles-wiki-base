# Bits

Beau says:

- Day 732 of waiting for election results, trying to shift focus from election-related content.
- Incorporates Easter eggs like hidden quotes from movies or songs in videos.
- Mentions a profound line from Snow the Product in the Hamilton mixtape: "America's Ghost Riders."
- Describes "America's Ghost Riders" as referencing undocumented immigrants and individuals throughout history who did the work but didn't receive credit.
- Points out that marginalized individuals often contribute significantly to shaping America's history.
- Emphasizes that those without power often do the work while those in power receive the credit.
- States that the country's future depends on ordinary people rather than those in power.
- Acknowledges that individuals have more control over the country's direction than they may realize.
- Encourages viewers to recognize their role in shaping the country's history as "America's Ghost Riders."

# Quotes

- "America's Ghost Riders."
- "The reality is that it's normally people who have no power who are writing America's story."
- "If you feel like you don't have any control, if you feel like you don't have any power… America's Ghost Riders."

# Oneliner

Beau shares the concept of "America's Ghost Riders," underscoring the often uncredited contributions of marginalized individuals throughout history and encouraging viewers to recognize their power in shaping the country's future.

# Audience

Viewers

# On-the-ground actions from transcript

- Recognize and appreciate the contributions of marginalized individuals in shaping history (implied).

# Whats missing in summary

The full transcript provides a deeper understanding of the concept of "America's Ghost Riders" and the importance of acknowledging the uncredited work of marginalized individuals in shaping history.

# Tags

#History #MarginalizedVoices #America #Influence #Community