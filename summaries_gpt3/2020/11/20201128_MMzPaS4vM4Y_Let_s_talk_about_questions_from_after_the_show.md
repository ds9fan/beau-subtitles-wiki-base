# Bits

Beau says:

- Beau did a live stream where he answered questions from Twitter, but didn't get to them all.
- Beau shares his thoughts on living in different countries, conservatives salvaging the GOP, and Trump's end game.
- He touches on modern feminism and the importance of masculinity not equating to weakness.
- Beau delves into politics, special operations forces, ranked choice voting, and the relationship between church and state.
- He also addresses the Trump administration, the census, US-Kurd relationship, and the future of US politics.

# Quotes

- "Masculine men are not afraid of competition. They don't mind equality with other people because competition is good."
- "Biden isn't a savior. Biden really isn't a progressive. If you want that deep systemic change, you can't change. You can't stop. You have to keep going."
- "I think by fact-checking the inarguable stuff, it may be better than trying to debate the opinions."

# Oneliner

Beau shares insights on various topics from living abroad to political strategies, urging continuous engagement for systemic change.

# Audience

Engaged citizens seeking political insights.

# On-the-ground actions from transcript

- Fact-check information shared by others online (implied).
- Engage in political activism to push for systemic change (implied).
- Keep informed and voice opinions on political matters (implied).

# Whats missing in summary

Beau's detailed analysis and recommendations on various political and social issues.

# Tags

#Beau #LiveStream #Politics #Masculinity #Feminism #RankedChoiceVoting #ChurchStateSeparation #Biden