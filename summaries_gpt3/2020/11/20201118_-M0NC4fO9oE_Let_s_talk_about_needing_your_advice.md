# Bits

Beau says:

- Used to be a security consultant, gave advice on security issues.
- Worked for a company owner who wanted to hire someone despite red flags.
- Advised against hiring the person due to inflated resume and bad reputation.
- Boss ignored advice and hired the person as a troubleshooter.
- Troubleshooter's projects failed, blamed others for his mistakes.
- Beau continued to warn the boss about the troubleshooter's incompetence.
- Troubleshooter may have broken laws, but the boss didn't take action.
- Despite causing problems, the troubleshooter was kept on for years.
- Compares the situation to President Trump and his followers.
- Concludes that the people, as the real boss, decided it was time for a change.

# Quotes

- "The guy is no good. I'm telling you this. I've been telling you this."
- "He's blaming people who have done a decent job."
- "The real boss here, the people, have decided it is time for a personnel change."

# Oneliner

Former security consultant warns against hiring problematic troubleshooter, drawing parallels to a larger political context and advocating for a change in leadership.

# Audience

Workplace colleagues

# On-the-ground actions from transcript

- Confront problematic hires in the workplace (exemplified)
- Advocate for responsible leadership in your workplace (exemplified)
- Engage with Trump supporters respectfully to challenge their beliefs (exemplified)

# Whats missing in summary

The full transcript provides a detailed account of Beau's experience advising against a problematic hire and draws parallels to larger political contexts, urging for responsible leadership and accountability.

# Tags

#Workplace #Advising #Leadership #Accountability #TrumpSupporters