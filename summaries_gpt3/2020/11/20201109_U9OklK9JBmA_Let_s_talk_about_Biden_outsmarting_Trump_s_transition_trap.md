# Bits

Beau says:

- Emily Murphy, head of the Government Services Administration, is refusing to acknowledge Biden as the apparent election winner.
- Murphy's refusal means Biden is denied access to nearly $10 million for his transition team.
- The Trump administration appears to be making foreign policy moves to hinder a Biden administration.
- Trump's intent seems to be disrupting the transition of power.
- Biden had prepared for this scenario months ago, assembling an expert team in various fields.
- Biden's team already picked, including public health experts and cabinet-level department selections.
- Despite the disruption, Biden's decision-making and transition plans are intact.
- Biden had already raised $7 million for his transition team before this obstacle.
- Trump's attempts to disrupt the transition appear to have been anticipated and outmaneuvered by Biden.
- Biden's team is well-prepared, with office space ready in the Department of Commerce if needed.

# Quotes

- "Trump is trying to disrupt the transition of power, but he was outsmarted."
- "Yes, it's going to be a talking point, and there are probably going to be some really scary things said because it is incredibly wrong for President Trump's administration to do this."
- "It does, however, show President Trump's intent, and it does answer the question of whether or not he actually cares about the United States or just himself."

# Oneliner

Emily Murphy's refusal to acknowledge Biden as the election winner and Trump's attempts to disrupt the transition of power are countered by Biden's well-prepared expert team, showing Trump's true intent.

# Audience

Politically active individuals

# On-the-ground actions from transcript

- Support Biden's transition team by staying informed and spreading awareness (implied)
- Stand against attempts to disrupt the transition of power through advocacy and information sharing (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's transition trap and Biden's preparedness for the obstacles, offering insights into the current political situation and strategic moves.

# Tags

#Trump #Biden #TransitionOfPower #Politics #Preparedness