# Bits

Beau says:

- Closing out 2020, looking forward to 2021, discussing billionaires, and food issues.
- United Nations released $100 million to help seven nations facing food insecurity.
- Predictions of 36 to 40 countries experiencing severe food insecurity in 2021.
- Challenges due to the world economy, public health crisis, climate issues, and more.
- The cost to prevent famine is $5 billion, with an additional $10 billion to ease malnourishment.
- Lack of funding from nation states poses a significant concern.
- World Food Program warns of potential famines of biblical proportions.
- Hoping for altruistic billionaires to step up and provide the needed $15 billion.
- Criticizes the broken system where individual billionaires are relied upon for global crises.
- Systemic issues leading to potential worsening of food insecurity globally.
- Political reluctance to address food insecurity compared to other priorities.
- Urges for international aid to prevent starvation and subsequent destabilization.
- Points out the interconnectedness of global issues and the impact of nationalism.
- Emphasizes the need for collective action and challenging nationalist perspectives.
- Warns that without swift changes, 2020's plague may be followed by 2021's famine.

# Quotes

- "15 billion dollars. All the nations of the world may not scrape together 15 billion dollars to stop people from starving."
- "Starvation causes destabilization, which causes migration."
- "In a world as plentiful as this one, this really shouldn't be a thing anymore, but it is."
- "The world is getting bigger. We are not as insulated from these issues as we used to be."
- "If things do not change and change pretty quickly, 2020 might have been the year of the plague. 2021 will be the year of famine."

# Oneliner

Beau addresses the urgent need for $15 billion to prevent global famine in 2021, calling out systemic failures and the reliance on individual billionaires for critical aid.

# Audience

Global citizens

# On-the-ground actions from transcript

- Advocate for increased government funding for food security programs (implied)
- Support organizations working to address food insecurity through donations or volunteering (implied)

# Whats missing in summary

The full transcript provides a comprehensive analysis of the impending global food crisis and the systemic issues contributing to it, urging immediate action and international cooperation to prevent widespread famine in 2021.

# Tags

#FoodSecurity #GlobalCrisis #FaminePrevention #SystemicIssues #InternationalCooperation