# Bits

Beau says:

- Trump's campaign promise to bring manufacturing back to the US is somewhat happening, attributed to companies wanting to move production closer to home to reduce supply chain length.
- Reasons for this include concerns about climate change and its impact on stability, exposure of weaknesses during crisis due to mishandling public health, national security, and trade disputes caused by Trump's tariff wars.
- Trump's actions worsened climate change, leading to companies bringing production closer to home to avoid interruptions in the supply chain.
- Companies are also responding to Trump's incompetence in handling public health crises.
- Trump's focus on national security led to companies reconsidering manufacturing components in countries that could become opposition in the future.
- The erratic nature of Trump's tariff wars and tweets created uncertainty for companies, prompting them to adjust their supply chains by bringing manufacturing closer to home.
- Despite claims of promises made and promises kept, Beau notes that manufacturing is returning, but not the manufacturing jobs due to automation and the increasing use of robots to reduce labor costs.
- Beau references Merrill, Bank of America's report suggesting a doubling of robot use in industries over the next five years due to decreasing automation costs.
- While manufacturing may be resurging, traditional union manufacturing jobs are not returning at the scale people imagine.
- Beau mentions Andrew Yang's warnings about automation and job displacement during the Democratic primaries, criticizing the Trump administration for not foreseeing this shift.
- The return of manufacturing does not equate to the return of traditional manufacturing jobs due to automation and evolving technologies replacing human labor.
- Beau questions the trade-off of worsening climate change and increasing the potential for war with major powers in exchange for a limited resurgence in manufacturing.
- He concludes by suggesting that the promised manufacturing jobs are obsolete and phased out, with the current landscape not resembling the past industrial era.

# Quotes

- "Manufacturing is coming back, but those jobs are probably not coming with it."
- "The robots are coming for your job."
- "Those good union jobs, they aren't."
- "It's not going to be like the old days because it isn't the old days."
- "Those manufacturing jobs, those that he promised, they're not coming. They are phased out."

# Oneliner

Trump's policies have led to a resurgence in manufacturing in the US, but automation means traditional jobs aren't returning as promised.

# Audience

Policy Analysts, Activists

# On-the-ground actions from transcript

- Advocate for policies that support workers transitioning to new industries (implied)
- Support education and training programs for emerging technologies and industries (implied)
- Stay informed about the impact of automation on job markets and advocate for measures to protect workers (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of how Trump's policies impacted the resurgence of manufacturing in the US and the lack of traditional manufacturing jobs due to automation and evolving technologies. Watching the full video can provide a deeper understanding of these dynamics and their implications. 

# Tags

#Manufacturing #Trump #Automation #JobDisplacement #ClimateChange #NationalSecurity #TradeWars