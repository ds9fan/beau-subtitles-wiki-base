# Bits

Beau says:

- Trump initially planned to wait for election results at one of his hotels but changed to the White House.
- Reports emerged that a non-scalable fence or wall will be built around the White House and Lafayette Park.
- Concerns rose as Trump planned to declare victory even if the votes weren't finalized.
- Beau expresses the importance of Trump losing by a landslide for the country.
- Trump's administration marginalized and "othered" people for political gain.
- Beau views rejecting Trump and Trumpism as vital for unity and progress.
- History is made by average people, not just great leaders.
- Beau hopes rejecting Trump will help bring the nation together and move forward positively.
- Rejecting the marginalization of large portions of the population is a critical goal.
- Beau plans to livestream during the election results with his team on YouTube, covering breaking events and state outcomes.

# Quotes

- "It's always told from the point of view of these great leaders. History isn't really made that way, though."
- "I think it's incredibly poetic that while that's hopefully happening, the president will be waiting behind a wall."
- "We're trying to build a better country, not go backwards."

# Oneliner

Beau shares the importance of rejecting Trump and Trumpism for unity and progress while criticizing the administration's divisive actions.

# Audience

American citizens

# On-the-ground actions from transcript

- Tune in to Beau's livestream on YouTube to stay updated on election results and breaking events (exemplified).

# Whats missing in summary

Importance of unity and progress through rejecting divisive leadership and marginalization. 

# Tags

#ElectionNight #RejectTrump #Unity #Progress #Livestream