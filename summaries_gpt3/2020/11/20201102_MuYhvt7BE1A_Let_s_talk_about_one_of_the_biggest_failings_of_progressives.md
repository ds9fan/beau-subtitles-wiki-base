# Bits

Beau says:

- Beau talks about his inspirations for communication style on his channel, mentioning Thomas Paine and Sophie Schall.
- Thomas Paine and Sophie Schall tried to meet everybody where they were at, avoiding jargon and academic words to make their messages more accessible.
- They framed their arguments philosophically and religiously, connecting with a broader audience.
- Beau admits he doesn't consciously frame his arguments in a religious or philosophical context due to the current polarization.
- Beau acknowledges the importance of reaching out to all demographics, including those who may need a religious or spiritual framing in their messages.
- He mentions the need to connect with people who have been left behind by progressives, mentioning Mary Ann Williams as an example.
- Beau shares an experience of seeing a priest on YouTube who framed a message about helping others as a sermon, realizing the impact of religious figures in delivering messages.
- Beau stresses the importance of having different messengers for different audiences, including religious figures who can reach people that others might not.
- He encourages actively seeking out religious leaders who advocate for a better world and using their messages to reach those who need moral and religious framing.
- Beau concludes by suggesting the value of having a diverse toolbox of messengers to connect with various demographics, including religious figures.

# Quotes

- "Sometimes it's not the message, it's the messenger."
- "Everybody can be reached."
- "Most people want a better world."

# Oneliner

Beau talks about the importance of diverse messengers, including religious figures, to reach all demographics and advocate for a better world.

# Audience

Progressive activists

# On-the-ground actions from transcript

- Reach out to religious leaders advocating for positive change (suggested)
- Use diverse messengers to connect with different demographics (implied)
- Seek out and amplify messages from religious figures promoting a better world (implied)

# Whats missing in summary

Beau's personal reflections on the impact of diverse messengers in reaching different audiences could provide valuable insights for communication strategies in advocacy and activism. 

# Tags

#Communication #DiverseMessaging #Activism #ProgressiveChange #InclusiveOutreach