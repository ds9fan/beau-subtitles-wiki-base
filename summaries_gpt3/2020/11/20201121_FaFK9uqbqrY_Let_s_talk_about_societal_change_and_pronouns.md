# Bits

Beau says:

- Changing societal thought doesn't always require changing laws but altering people's perspectives is key.
- Received a message suggesting putting preferred pronouns on Twitter bio to challenge societal norms.
- Society currently relies on guessing preferred pronouns based on appearance, creating potential for misidentification.
- Identifying preferred pronouns is specific to certain groups, leading to further differentiation and othering.
- Encourages those without an obvious need to state preferred pronouns to do so to normalize the practice.
- Simple actions like adding pronouns in bios can gradually shift societal views and make it a common practice.
- Inclusion of pronouns can act as a signal of acceptance and create a welcoming space for others.
- The widespread adoption of sharing pronouns can help individuals who don't neatly fit into traditional gender categories.
- Small actions like this can contribute to changing divisive issues over time.
- Predicts that in the future, assumptions about gender roles based on appearance will diminish.

# Quotes

- "It's a good example of how little actions can over time shift the way society looks at an issue that today is very divisive."
- "The widespread use of this can help them feel more accepted, can help them more included."
- "It's just a thought."

# Oneliner

To change societal norms, start by normalizing sharing preferred pronouns in everyday interactions, fostering inclusivity and acceptance.

# Audience

Social media users

# On-the-ground actions from transcript

- Add preferred pronouns to your social media bio (suggested)
- Encourage others to include their preferred pronouns in their profiles (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of how small actions like sharing preferred pronouns can lead to significant shifts in societal perspectives. It also underlines the importance of inclusivity and acceptance for individuals who don't conform to traditional gender roles. 

# Tags

#SocietalChange #Inclusivity #Acceptance #GenderIdentity #SmallActions