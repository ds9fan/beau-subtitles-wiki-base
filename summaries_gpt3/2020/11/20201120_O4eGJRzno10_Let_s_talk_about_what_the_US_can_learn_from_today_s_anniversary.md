# Bits

Beau says:

- Commemorating the seventy-fifth anniversary of Nuremberg trials.
- International community's unity against atrocities as a lesson.
- Questioning the different treatment standards by governments towards their people.
- Pondering the evolution of the line on permissible government force over seventy-five years.
- Emphasizing the importance of establishing a baseline of facts for societal progress.
- Drawing parallels between confronting past actions in Germany and the need for the US to face its history.
- Advocating for addressing past wrongs and establishing a factual basis for moving forward.
- Stressing the need for continuous vigilance to ensure atrocities are prevented before they escalate.
- Calling for a proactive approach to prevent history from repeating itself.
- Advocating for moving the line of what is unacceptable for humanity collectively.

# Quotes

- "All governments use force against their own people, but there was a line drawn seventy-five years ago that said this is too much."
- "We have to know what happened in order to move forward."
- "If you want never again, we should move that line and keep moving it to make sure that it's an affront to all of humanity."
- "If we want to reclaim our position as a world leader, it's a pretty good step in that direction."
- "Y'all have a good day."

# Oneliner

Commemorating Nuremberg trials' seventy-fifth anniversary, Beau questions the global community's progress in setting limits on government force and calls for establishing factual baselines to confront history and prevent future atrocities.

# Audience

Global citizens, policymakers, historians

# On-the-ground actions from transcript

- Establish a factual baseline by uncovering and addressing past wrongs (implied).
- Advocate for setting stricter limits on government force through activism and advocacy (implied).
- Proactively work towards preventing atrocities by collectively moving the line of what is unacceptable (implied).

# Whats missing in summary

Deeper insights on the importance of historical accountability and proactive measures to prevent future atrocities.

# Tags

#Nuremberg #GovernmentForce #HistoricalAccountability #Prevention #GlobalCitizenship