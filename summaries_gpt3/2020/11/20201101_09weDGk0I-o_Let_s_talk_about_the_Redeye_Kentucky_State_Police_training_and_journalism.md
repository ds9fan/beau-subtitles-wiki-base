# Bits

Beau says:

- Beau praises the high school newspaper Manual Red Eye for publishing materials and uncovered training materials from the Kentucky State Police.
- The training materials for peace officers are alarming, promoting a "warrior's mindset" and extreme aggression.
- The materials suggest law enforcement should view themselves as ruthless killers and prioritize violence over other tactics.
- Beau criticizes the training materials for being inappropriate and promoting dangerous concepts like disregarding policy and dehumanizing the public.
- He questions if officers trained under these materials have been retrained and calls for accountability.
- Beau stresses the importance of peace officers understanding their role in protecting the community rather than adopting a warrior mentality.
- He expresses concern about the loss of public confidence due to these training materials and their impact on current events like protests.

# Quotes

- "This isn't training. This is a justification for barbarism."
- "Regular employment of violence. That's how they intend on doing it. This is disturbing."
- "This is appalling on so many levels. This is not law enforcement. This is not being a peace officer."

# Oneliner

Beau praises a high school paper for exposing alarming training materials that push a warrior mindset onto peace officers, urging accountability to protect communities.

# Audience

Law Enforcement Oversight Organizations

# On-the-ground actions from transcript

- Contact local law enforcement oversight organizations for accountability and reform (implied).
- Join community efforts to advocate for peaceful policing and de-escalation training (implied).

# Whats missing in summary

Full context and emotional depth of Beau's passionate call for accountability and reform in law enforcement training.

# Tags

#LawEnforcement #TrainingMaterials #PeaceOfficers #CommunityOversight #Accountability