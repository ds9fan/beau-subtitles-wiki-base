# Bits

Beau says:

- Explains why he continues to reach out to people who resist being reached out to, using a story about his dogs as an analogy.
- Describes his German Shepherd, Baroness, as incredibly smart and capable of problem-solving.
- Shares that Baroness is like the raptor from Jurassic Park in terms of intelligence and abilities.
- Recounts how Baroness used to hide his keys when she noticed him packing a bag at the age of two.
- Talks about how Baroness can see through tricks like the fake tennis ball throw with contempt.
- Mentions that Baroness is still open to learning new things despite her age.
- States that Baroness is well-trained and proficient in various tasks.
- Introduces his other dog, Destro, a husky, who is not as intelligent in problem-solving as Baroness.
- Comments on Destro being easily tricked, specifically mentioning his confusion with glass even as an adult.
- Emphasizes Destro's love for children and his affectionate nature towards them despite being easily fooled.
- Draws a parallel between his dogs and people who trusted misinformation about mask-wearing from the presidency.
- Conveys the message that even those who are easily deceived have people who care for them and who they matter to.
- Acknowledges the frustration in trying to reach out to those who resist understanding the importance of certain issues.
- Ends with a reflection on the situation and wishes the audience a good day.

# Quotes

- "At the end of the day, they placed their trust in the institution of the presidency."
- "And they got tricked."
- "Even if you can trick him with a tennis ball over and over and over again for months."
- "Despite all of that, he's still a good boy."
- "Y'all have a good day."

# Oneliner

Beau explains his persistence in reaching out to those resistant to change through a heartfelt canine analogy, revealing the importance of care and understanding even for the easily misled.

# Audience

Empathetic individuals

# On-the-ground actions from transcript

- Reach out to those resistant to understanding with patience and compassion (exemplified)

# Whats missing in summary

The full transcript provides a touching analogy using dogs to illustrate the importance of persistence, compassion, and empathy in reaching out to those who may resist change or understanding.