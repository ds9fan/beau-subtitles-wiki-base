# Bits

Beau says:

- Examines the common statement that there's no difference between Republicans and Democrats, acknowledging some truth to it.
- Points out similarities between the two parties in terms of corporate interests, militaristic operations, and intervention in other countries.
- Acknowledges policy differences between the parties, such as immigration and climate change.
- Hypothetically considers a scenario where there are no policy differences and focuses on the importance of rhetoric.
- Illustrates the impact of rhetoric by contrasting Republican and Democratic statements on military intervention and how they can influence future outcomes.
- Stresses the significance of changing societal thought to drive legislative change.
- Encourages long-term strategic thinking for those seeking systemic change and societal improvement.
- Compares the role of speech and rhetoric in influencing societal perspectives and achieving long-term systemic change.
- Emphasizes the value of rhetoric in reinforcing ideas like human rights and regret over harmful actions, even if it's perceived as just words.
- Urges radicals aiming for deep systemic change to understand the power of rhetoric in shaping public discourse and influencing future generations.

# Quotes

- "You have to have long-term strategic thought."
- "Speech, rhetoric, that is designed to influence thought, is probably a lot more valuable than you think when it comes to achieving long-term systemic change."
- "Your goal is probably not going to be accomplished in your lifetime."
- "Saying that Democrats and Republicans are exactly the same doesn't help your cause because it comes across as an excuse to not get involved."
- "If you want that deep change, if you want to build a society where everybody gets a fair shake, where there is liberty and justice for all regardless of where you live or what you look like, it's not happening overnight."

# Oneliner

Examining the common belief that Democrats and Republicans are indistinguishable, Beau underscores the critical role of rhetoric in shaping societal perspectives for long-term systemic change.

# Audience

Activists, Political Advocates

# On-the-ground actions from transcript

- Think long-term strategic thought (implied)
- Understand the power of speech and rhetoric in influencing societal change (implied)
- Reinforce ideas like human rights through speech (implied)
- Get involved in shaping public discourse and influencing future generations (implied)

# Whats missing in summary

The full transcript provides a deep dive into the significance of rhetoric in shaping societal thought and achieving long-term systemic change.

# Tags

#PoliticalRhetoric #SystemicChange #Activism #Democracy #SocietalImpact