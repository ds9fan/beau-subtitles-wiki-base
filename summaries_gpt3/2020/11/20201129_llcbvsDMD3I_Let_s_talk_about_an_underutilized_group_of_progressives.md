# Bits

Beau says:

- Beau addresses a certain group of people, particularly progressive vets, who are underutilized and may not realize their potential in building communities.
- He shares a story about a veteran friend who feels he's done his part and just wants to stay isolated, despite the skills and experiences he possesses.
- Beau points out that young progressives, fresh out of AIT, might understand concepts in theory but lack the practical experience needed for upcoming challenges.
- He stresses the importance of veterans' skills in logistics, team-building, and community resilience, urging them to share their knowledge.
- Beau encourages veterans to utilize their skill set and mindset to contribute to making the country better and helping those who lack the necessary skills.
- He reminds veterans of the community-focused mindset they had during their service and how that level of community support can be beneficial in facing challenges.
- Beau acknowledges that some veterans may not want to lead but can still serve as valuable advisors and guides to those willing to learn.
- He underscores the idea that veterans can help shape the country by applying their skills differently to address ongoing issues.
- Beau concludes by expressing his belief that veterans have the potential to contribute to creating a better world or country, using their existing skills in new ways.

# Quotes

- "You're needed."
- "You don't have to lead. They don't need leaders. They are leaders."
- "We don't have to accept the way things are."
- "You can help shape this country."
- "What did you do it all for?"

# Oneliner

Beau addresses progressive vets, urging them to recognize their value in building resilient communities and shaping a better future for the country, stressing that they are needed as advisors and guides, not necessarily leaders.

# Audience

Progressive Vets

# On-the-ground actions from transcript

- Reach out to progressive vets in your community and encourage them to share their skills and experiences with building resilient communities (exemplified).
- Organize virtual or future in-person team-building events or workshops for community members with the help of progressive vets (exemplified).
- Support progressive vets in connecting with individuals who are willing to learn and build self-reliant communities (exemplified).

# Whats missing in summary

The full transcript provides a heartfelt call to action for progressive vets to recognize their potential in community-building and shaping a better future for the country through their unique skill set and experiences.

# Tags

#ProgressiveVets #CommunityBuilding #SkillSet #Resilience #Guidance