# Bits

Beau says:

- Lieutenant Governor of Texas, Dan Patrick, offers a million dollar reward for evidence backing up Trump's claims.
- Rewards are typically offered when there is evidence, leads, and a clear picture of what occurred.
- Lack of evidence and leads raises questions about the validity of Trump's claims.
- Ted Lieu offers a million dollar reward to find evidence of Bigfoot, which seems easier.
- Focus on offering rewards rather than addressing public health issues and the transition.
- People who support Trump's claims risk being recorded in history as attempting to undermine elections and the U.S.
- Trump's refusal to accept defeat was expected, but the extent of people supporting him was surprising.

# Quotes

- "A million dollars is a million dollars."
- "There's no evidence."
- "All of these people, when this is all said and done, their name is going to go down alongside Trump's."

# Oneliner

Lieutenant Governor offers rewards for evidence; lack of proof raises doubts about Trump's claims as supporters risk being remembered in history books.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Contact local officials to prioritize addressing public health issues (implied)
- Join organizations advocating for election integrity (implied)

# Whats missing in summary

The full transcript provides deeper insights into the implications of supporting baseless claims and the importance of focusing on critical issues rather than false narratives.

# Tags

#Texas #Trump #Election #Rewards #PublicHealth