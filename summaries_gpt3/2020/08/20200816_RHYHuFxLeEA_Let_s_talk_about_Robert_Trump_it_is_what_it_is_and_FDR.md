# Bits

Beau says:

- President Trump's brother, Robert Trump, passed away, sparking different reactions and trends on Twitter.
- The phrase "wrong Trump" trended on Twitter, causing moral outrage and demands for compassion.
- Some individuals demanded condolences for Robert Trump, while others dismissed the situation with "it is what it is."
- Beau points out the irony of those demanding compassion now, who previously prioritized economy over lives.
- Fear, according to Beau, is a necessary emotion that keeps individuals alive, especially in justifiable circumstances.
- Beau references FDR's quote about fearing fear itself, questioning its relevance and meaning in today's context.
- He dives into FDR's inaugural speech, focusing on the rejection of materialism and the pursuit of noble social values.
- Beau criticizes those using FDR's quote to justify prioritizing monetary profit over social values and public well-being.
- The speech condemns the callous and selfish pursuit of material wealth, urging a shift towards more noble societal values.
- Beau underscores the need to address each statistic as a tragedy rather than dismissing them as mere numbers.
- He advocates for proactive efforts to address issues instead of using statistics as excuses for inaction.
- Beau acknowledges historical examples where people found leadership and worked for change instead of succumbing to defeatism.
- While Beau acknowledges the importance of mourning individuals like Robert Trump, he also stresses the broader systemic issues at play.
- He contrasts past leaders with the current political climate, questioning the acceptance of failure and lack of empathy in leadership.
- Beau concludes by encouraging reflection on the type of leadership needed for positive change in America.

# Quotes

- "We have nothing to fear but fear itself."
- "One is a tragedy, a million is a statistic."
- "If you're looking for change, if you're looking to make America great again, you probably don't want a leader who is that accepting of his own failures."

# Oneliner

Beau dissects reactions to Robert Trump's passing, questioning the acceptance of failures and urging a shift towards noble social values in leadership amidst the ongoing crisis.

# Audience

Americans

# On-the-ground actions from transcript

- Seek leadership that prioritizes noble social values over material profit (implied).
- Advocate for proactive efforts to address societal issues instead of dismissing them as statistics (implied).
  
# Whats missing in summary

The emotional depth and nuanced analysis present in Beau's full transcript.

# Tags

#Leadership #SocialValues #Fear #AmericanValues #Change