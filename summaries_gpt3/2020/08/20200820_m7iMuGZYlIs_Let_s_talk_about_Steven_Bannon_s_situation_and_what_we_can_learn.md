# Bits

Beau says:

- Steve Bannon, former campaign advisor to Donald Trump, is facing charges related to a campaign to raise money for a border wall.
- Bannon and others allegedly pocketed some of the money meant for the wall and used it for personal expenses.
- If convicted, Bannon could face seven to nine years in prison.
- Bannon has pleaded not guilty, but there's a possibility of a plea deal for a reduced sentence.
- Social media response to Bannon's charges revealed Republicans blaming Democrats, showing party loyalty above all else.
- Political parties in the US have created a situation where supporters defend party members even if they harm them personally.
- The focus on party loyalty over policy has led to defending actions against one's own interests.
- Parties were originally meant to assist in getting legislation passed but now contribute to corruption.
- Americans need to shift focus from party affiliation to policy and its long-term effects.
- The current situation in the US is a result of prioritizing party allegiance over examining policy effects.

# Quotes

- "Parties are really there to assist in the good form of corruption."
- "Americans have decided that it's easier to look for an R or a D than it is to actually look at policy."
- "You have people who are defending the people who took advantage of them personally."

# Oneliner

Steve Bannon's charges reveal loyalty to political parties over personal interests, urging Americans to focus on policy, not party.

# Audience

Citizens, Voters

# On-the-ground actions from transcript

- Examine policies over party loyalty (implied)
- Advocate for transparency and accountability in political actions (implied)

# Whats missing in summary

Importance of holding politicians accountable and prioritizing policies over party loyalty.

# Tags

#SteveBannon #Politics #PartyLoyalty #Policy #Accountability