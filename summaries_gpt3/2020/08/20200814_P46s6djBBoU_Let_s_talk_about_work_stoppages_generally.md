# Bits

Beau says:

- Congress left without providing relief for normal folks.
- Suggestions of a mass strike are surfacing across different circles.
- Strikes have a history dating back to 500 BC in Rome.
- Strikes work because they impact everyone, not just one industry.
- Government reacts to strikes due to economic impact and lack of control over them.
- Community networks are vital for organizing strikes.
- Pitfalls in the US for a general strike include polarization and geography.
- Strikes in the US must focus on specific, unifying issues to be effective.
- A call for relief and a public health response could be key demands.
- Having influential figures supporting the strike idea can lend it legitimacy and expedite government response.

# Quotes

- "Everybody stops working."
- "You have to make this a battle big enough to matter, but small enough to win."
- "The fact that it's being talked about in numbers so big that it becomes a trending topic on Twitter gives you hope."
- "It is one of the biggest tools of social change."
- "Y'all have a good day."

# Oneliner

Suggestions of a mass strike emerge in the US due to Congress's lack of relief for normal folks, necessitating community networks and specific demands for effective action.

# Audience

Working-class activists

# On-the-ground actions from transcript

- Organize community networks for strike preparation (implied)
- Rally influential figures to support the strike idea (implied)

# Whats missing in summary

Importance of community organizing and influencer support for successful strikes.

# Tags

#GeneralStrike #CommunityOrganizing #WorkingClassActivism #InfluencerSupport #ReliefDemands