# Bits

Beau says:
- Emphasizes the importance of speaking the same "language" as the audience you are trying to reach to effectively communicate your message.
- Talks about framing things in a way that appeals to the target audience, rather than trying to make points that appeal to oneself.
- Mentions how individuals often fall into hypocrisy, especially in the conservative movement, and how pointing out this hypocrisy can inadvertently strengthen their position.
- Gives an example of a well-known religious figure who falls from grace and manipulates the situation by framing it as a moral lesson about temptation and money.
- Argues that focusing on hypocrisy is not the most effective approach, as the real issue lies in marginalizing others and failing to love and accept them.
- Encourages using the narrative of loving one's neighbor as a more impactful way to address transgressions and create positive change.

# Quotes

- "Don't believe me? Let's say hypothetically there was a well-known religious pundit who got caught up in a lifestyle that was just outside of the norm."
- "The failure of most people like this is not the hypocrisy. It's that they didn't love their neighbor as themselves."
- "And that's the real problem. Everywhere."

# Oneliner

Beau stresses the importance of speaking the audience's "language," avoiding hypocrisy call-outs, and focusing on loving one's neighbor to address transgressions effectively.

# Audience

Communicators and activists

# On-the-ground actions from transcript

- Reframe messages to resonate with the audience (exemplified)
- Focus on promoting love and acceptance within communities (exemplified)

# Whats missing in summary

The full transcript provides a deeper exploration of the pitfalls of focusing solely on hypocrisy rather than addressing the root issue of marginalization and lack of love and acceptance.

# Tags

#Communication #AudienceEngagement #CommunityBuilding #Hypocrisy #LoveThyNeighbor