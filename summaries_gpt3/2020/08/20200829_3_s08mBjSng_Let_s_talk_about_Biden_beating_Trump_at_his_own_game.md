# Bits

Beau says:

- The internet has given us new tools to connect with people, even those in their own echo chambers.
- In the past, seeking opposing views was necessary; now, it's easier to come across diverse perspectives.
- Individuals still tend to tailor their online feeds to support their beliefs, but there are ways to introduce opposing viewpoints.
- The Biden campaign acquired the domain keepamericagreat.com to showcase the disparities between Trump's promises and actions.
- Trump is depicted as seeking a do-over rather than re-election, shifting blame for failures onto others.
- Trump's lack of leadership and accountability is emphasized.
- Sharing keepamericagreat.com under the guise of supporting Trump could challenge his supporters with contrasting information.
- Trump's campaign lacks substantial policy, focusing instead on denigrating opponents and owning the libs.
- Demonstrating Trump's failures in trolling and belittling could be effective in reaching his supporters.
- Biden's savvy move with campaign domain names could be eye-opening for Trump voters.

# Quotes

- "He wants people to forget the last four years, blame that on the radical left of Joe Biden, and just let him do what has always occurred in his life."
- "Not only is he a failure as a president, he's not even the cool internet troll they thought he was."

# Oneliner

The internet offers tools to challenge echo chambers and confront Trump's lack of leadership and accountability.

# Audience

Social media users

# On-the-ground actions from transcript

- Share keepamericagreat.com on social media under pro-Trump hashtags (implied).

# Whats missing in summary

The full transcript dives into leveraging internet tools to challenge online echo chambers and expose Trump's shortcomings, urging action to confront misinformation and biases.

# Tags

#Internet #EchoChambers #Biden #Trump #SocialMedia #Accountability