# Bits

Beau says:

- 2020 teaches a lesson on not trusting politicians when it comes to science.
- Politicians have made inaccurate claims about the disappearance of cases and impact on kids regarding the current issue.
- Experts have consistently warned about the seriousness of the situation and the need for action.
- Politicians prioritize the economy over the well-being of the people, especially in matters of science.
- Climate data from the last 10 years shows alarming trends, but politicians downplay the severity of the situation.
- Scientists stress the importance of immediate action to prevent the situation from escalating further.
- The urgency to elect officials who prioritize addressing critical issues like climate change.
- Voters should hold politicians accountable for prioritizing short-term gains over long-term consequences.
- Delaying action on pressing issues can lead to catastrophic outcomes.
- Beau urges the American people to learn from past mistakes and not wait until it's too late to act.

# Quotes

- "Don't trust politicians when it comes to science."
- "We have to act or it's going to get out of hand."
- "Politicians will always put the economy, their pocketbook, over you."
- "It's going to be too late. It's going to get way out of hand."
- "If we wait, by the time we get to the point where everybody knows that the politicians lied, it's going to be too late."

# Oneliner

2020 teaches us not to trust politicians on science, prioritize urgent action over their economic interests, and elect officials who address critical issues promptly.

# Audience

Voters, concerned citizens

# On-the-ground actions from transcript

- Elect officials willing to prioritize urgent matters like climate change (implied).
- Hold politicians accountable for prioritizing short-term gains over long-term consequences (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how politicians prioritize economic interests over public well-being in matters of science and climate change.

# Tags

#Science #ClimateChange #Politicians #Trust #UrgentAction #Accountability