# Bits

Beau says:

- Filming outside due to a storm, discussing a recent incident where the police went too far.
- Addressing white country folks about their reactions to such incidents.
- Noting the disconnect between online support for police actions and real-life community values.
- Sharing how the community comes together to prepare for an upcoming storm, showcasing true solidarity.
- Emphasizing the importance of principles over superficial differences like skin tone.
- Criticizing those who change their principles based on race and lack consistency.
- Mentioning the implications of being untrustworthy in a close-knit community.
- Encouraging people to stick to their principles and not waver based on race.
- Reminding the audience of historical figures like moonshiners and the origins of the term "redneck" in collective action.
- Criticizing the shift in values influenced by social media and external factors.
- Urging individuals to stay true to their roots and not be swayed by external influences.

# Quotes

- "People hop on Facebook and you make your little post and you get four or five likes from people you know, people who actually live around you, from your actual physical community."
- "Your real principle is something else. And every time this happens, that mask slips a little bit."
- "Meanwhile, you're out there cheerleading for Boss Hogg."
- "You let Facebook, you let some dude with a red hat change who you are on a level so deep that those people who know you in real life don't want anything to do with you."
- "Either way, you better see if those people on Facebook come help you with the storm."

# Oneliner

Beau addresses white country folks, urging them to stay true to their principles and community values amidst societal influences and online disconnect.

# Audience

White country folks

# On-the-ground actions from transcript

- Reconnect with real-life community members to build trust and support (implied).
- Uphold consistent principles regardless of skin tone (implied).
- Stay true to historical community values like solidarity and collective action (implied).

# Whats missing in summary

Beau's passionate delivery and historical references can be best understood by watching the full transcript.

# Tags

#CommunityValues #Principles #Solidarity #HistoricalReferences #SocialInfluences