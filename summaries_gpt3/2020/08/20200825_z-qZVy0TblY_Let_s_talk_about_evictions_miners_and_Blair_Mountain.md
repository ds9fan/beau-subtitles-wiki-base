# Bits

Beau says:

- Blair Mountain incident in West Virginia, 99 years ago, had significant impacts on American history and policy due to evictions and miners' struggle.
- Mine operators cut wages post-World War I, leading to miners organizing for better conditions like an eight-hour workday and hourly pay.
- United Mine Workers played a vital role in organizing miners against oppressive mine operators who enforced their will through gun thugs.
- A conflict on May 19, 1920, led to the arrest of gun thugs, with a cop becoming a symbol of miners' struggle for better conditions.
- The cop faced charges and later a coal transport station explosion accusation, which intensified the battle for fair treatment.
- A sheriff, backed by gun thugs and mining companies, faced off against thousands of miners heading towards Blair Mountain.
- Fighting erupted on August 25, 1920, with the miners expecting federal support but ultimately facing arrests and heavy charges.
- Despite the union's initial downfall, the miners' actions led to federal legislation and widespread popular support for labor rights.
- The term "rednecks" originated from the miners wearing red scarves and gun thugs wearing white armbands at Blair Mountain.
- The rich exploiting the economic downturn by pushing down on the little people resulted in evictions and labor struggles.

# Quotes

- "During an economic downturn, the rich folk couldn't weather the storm, so they further kicked down."
- "Rednecks have labor organizing, unions. It's in their DNA."
- "All of this happened because during an economic downturn, the rich folk couldn't weather the storm, so they further kicked down."
- "They provoked that overreaction during a security clampdown."
- "It's a really important little chapter in American history, but it often gets overlooked."

# Oneliner

Blair Mountain's miners' struggle against oppressive mine operators led to federal legislation and popular support for labor rights, showcasing the impact of grassroots organizing in history.

# Audience
History enthusiasts, labor rights advocates

# On-the-ground actions from transcript
- Visit the Mine Wars Museum in West Virginia to learn about the origins of labor organizing and unions (exemplified).
- Support grassroots labor organizations and unions in your community (implied).

# Whats missing in summary
The full transcript provides a detailed account of the Blair Mountain incident and its long-lasting effects on American history, shedding light on the importance of grassroots organizing and standing up against oppressive systems.

# Tags
#BlairMountain #LaborRights #GrassrootsOrganizing #MineWars #History