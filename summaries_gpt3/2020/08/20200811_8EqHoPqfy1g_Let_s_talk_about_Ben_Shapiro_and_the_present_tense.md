# Bits

Beau says:

- Beau addresses Ben Shapiro's controversial commentary on a music video, expressing embarrassment on Shapiro's behalf.
- Ben Shapiro reviewed a suggestive music video by reading the lyrics aloud with feigned moral outrage.
- Beau criticizes Shapiro for questioning if the video represents what feminists fought for, pointing out the hypocrisy in his argument.
- He argues that feminism is about allowing individuals to express themselves freely and be independent, full, and rounded human beings.
- Beau condemns societal pressures that dictate how women should behave and be perceived, contrasting it with men's freedom from such expectations.
- He questions Shapiro's selective defense of rights and objectification, suggesting a double standard in his views.
- Beau challenges Shapiro to recognize and respect women's rights to be who and what they want, irrespective of societal expectations.
- He concludes by noting that facts do not care about feelings and hints at the potential impact of embracing true equality.

# Quotes

- "Is this what feminists fought for?"
- "But anytime somebody from a group that isn't me and him, the way we look, you know, anytime somebody from a different group tries to use that system and use the same things to get ahead, well he's right there to slap them down."
- "Ben doesn't like feminism. Got it."
- "They have rights, and that's a fact."
- "And facts do not care about your feelings."

# Oneliner

Beau criticizes Ben Shapiro's views on feminism and calls for true equality based on individual rights and freedom of expression.

# Audience

Social commentators

# On-the-ground actions from transcript

- Challenge societal pressures and double standards (implied)
- Support and uplift voices advocating for true equality (implied)
- Encourage respect for individual rights and freedom of expression (implied)

# Whats missing in summary

Deeper insights into the nuances of feminism and societal expectations regarding expression and autonomy.

# Tags

#BenShapiro #Feminism #Equality #FreedomOfExpression #DoubleStandards