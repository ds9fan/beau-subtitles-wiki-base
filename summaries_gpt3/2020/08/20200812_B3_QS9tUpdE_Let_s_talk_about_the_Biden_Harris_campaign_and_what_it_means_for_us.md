# Bits

Beau says:

- Explains the strategy behind the Biden-Harris campaign and why it seems to alienate some Democratic voters.
- Describes how the Democratic Party is targeting moderate voters rather than leftists to secure votes and deny votes to Trump.
- Points out that the campaign aims to beat Trump on Twitter and personal attacks rather than policy.
- Notes the importance of Black women as loyal Democratic voters and how Harris appeals to them.
- Analyzes the dynamics of the Biden-Harris campaign and how they plan to counter Trump's attacks.
- Urges viewers who seek deep systemic change to focus on down-ballot races for real change.
- Addresses the different schools of thought among leftists regarding voting in the upcoming election.
- Emphasizes the need for community action and involvement beyond voting for a just society.
- Stresses the importance of individual action and building community networks, especially for leftists seeking systemic change.

# Quotes

- "The Democratic Party has decided to forego those votes. We don't need the leftist vote."
- "If you are a liberal, if you are a Democratic party loyalist, you've got a ticket that might win."
- "There is no cavalry coming. It's just us. We have to make the changes."
- "Those community networks are more important than ever."
- "Being active, beyond voting, this isn't a once every four year or once every two year thing."

# Oneliner

The Biden-Harris campaign strategy targets moderates, alienating leftists, while community action becomes vital for systemic change seekers.

# Audience

Leftist activists

# On-the-ground actions from transcript

- Build community networks and foster involvement beyond voting (implied)
- Focus on down-ballot races for real change (implied)
- Be active in striving for a functioning, free, and just society (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the Biden-Harris campaign strategy, the importance of community action, and the need for systemic change beyond elections.

# Tags

#BidenHarris #DemocraticParty #CommunityAction #SystemicChange #LeftistMovement