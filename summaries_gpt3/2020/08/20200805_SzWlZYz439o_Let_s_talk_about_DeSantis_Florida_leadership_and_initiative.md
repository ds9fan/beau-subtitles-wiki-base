# Bits

Beau says:

- Addressing the lack of leadership and accountability in American politics.
- Criticizing Governor DeSantis for deflecting blame and failing to take responsibility for Florida's unemployment issues.
- Explaining the criteria for receiving unemployment benefits in Florida.
- Expressing disbelief at Governor DeSantis' excuse of not fixing the problem because no one asked him to.
- Pointing out the entitlement and lack of initiative among the political ruling class.
- Emphasizing the importance of individuals looking out for each other in the absence of government support.
- Advocating for setting up community networks and practicing mutual aid.
- Reminding that in times of injustice, it comes down to "just us" taking care of each other.
- Condemning the entitled attitude of politicians who neglect their duties.
- Using a cashier analogy to illustrate the absurdity of politicians shirking their responsibilities.

# Quotes

- "If you have the means, you have the responsibility to act."
- "Sometimes there's justice and sometimes there's just us."
- "They're admitting they're just puppets of lobbyists and government functionaries."
- "They don't even have to pretend they're doing their job."
- "Well nobody told me I needed to do that."

# Oneliner

Beau addresses the lack of leadership and accountability in American politics, calling for community solidarity in the face of government negligence.

# Audience

Community members

# On-the-ground actions from transcript

- Set up a community network for mutual aid (suggested)
- Commit to looking out for each other within your community (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the lack of leadership and accountability in American politics, urging individuals to take action and support each other in the absence of governmental assistance.

# Tags

#Leadership #Accountability #CommunitySolidarity #GovernmentNegligence #MutualAid