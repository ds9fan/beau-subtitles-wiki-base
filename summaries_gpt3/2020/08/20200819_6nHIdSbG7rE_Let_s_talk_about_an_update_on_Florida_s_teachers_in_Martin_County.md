# Bits

Beau says:

- Politicians' talking points don't alter reality, regardless of phrasing or metaphors used.
- Governor DeSantis referenced Martin County's school teachers, drawing a controversial comparison.
- Teachers lack training, funding, and logistical support to provide in-person instruction effectively.
- The Martin County Public School Teachers Union President criticized the situation as a logistical nightmare.
- Despite motivational speeches, no practical backup was provided, leading to student and employee quarantines.
- Many politicians prioritize reopening schools for economic reasons, not genuine concern for education.
- The focus is on providing childcare so parents can return to work.
- Beau suggests that economic assistance is necessary for parents to stay home with their kids during the pandemic.
- Countries that shut down effectively are faring better economically and in terms of public health.
- Cute speeches from politicians do not change the harsh reality faced by many, as evidenced by the 170,000 lives lost in the country.

# Quotes

- "Politicians' talking points don't alter reality."
- "Teachers lack training, funding, and logistical support."
- "Many politicians prioritize reopening schools for economic reasons."
- "Cute speeches don't change reality."
- "Countries that shut down effectively are doing much better."

# Oneliner

Politicians' talking points fail to change reality for teachers lacking support, while prioritizing school reopenings for economic gains amid a pandemic.

# Audience

Politically aware citizens

# On-the-ground actions from transcript

- Contact Senators and Representatives for a plan providing economic assistance to families (suggested)
- Advocate for properly funded schools and logistical support for teachers (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the impact of political decisions on education and public health during the pandemic.

# Tags

#Education #Politics #Pandemic #EconomicAssistance #CommunitySupport