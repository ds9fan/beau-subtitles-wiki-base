# Bits

Beau says:

- Provides tips for people not interested in helping during a storm but wanting to make it through safely.
- Emphasizes the importance of preparation and knowing your network.
- Advises on staying mobile and being ready to move at all times after the storm.
- Suggests seeking shelter in people's residences rather than abandoned warehouses.
- Recommends moving together with your group and looking unassuming to avoid drawing unwanted attention.
- Stresses the importance of focusing on your circle during the aftermath of the storm.
- Mentions the scarcity of emergency services post-storm and the need to rely on your own resources.
- Encourages being cautious when encountering groups looking to take advantage of the situation.
- Notes the value of comfort items and the uncertainty of when infrastructure will be restored.
- Reminds to gather all necessary supplies and be prepared for the possibility of more storms in the future.

# Quotes

- "Mobility is life."
- "Comfort items worth their weight in gold."
- "Nothing else matters."

# Oneliner

Beau provides tips for surviving a storm for those not interested in helping, focusing on preparation, mobility, and self-reliance post-storm.

# Audience

Survivalists

# On-the-ground actions from transcript

- Identify your circle and network for emergencies (implied)
- Gather necessary supplies and comfort items for survival (implied)
- Stay mobile and be prepared to move at all times (implied)

# What's missing in summary

The detailed examples Beau provides on how to navigate through and survive a storm for those solely focused on personal survival.

# Tags

#StormPreparation #SurvivalTips #EmergencyPlanning #CommunitySafety #DisasterManagement