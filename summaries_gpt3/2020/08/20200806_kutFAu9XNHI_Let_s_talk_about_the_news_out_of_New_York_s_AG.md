# Bits

Beau says:

- Beau discloses his long-standing support for the second amendment before discussing the recent news coming out of New York regarding the NRA.
- The Attorney General in New York is pursuing actions that may lead to the dissolution of the NRA due to allegations of corporate misconduct.
- Beau clarifies that the issue is not an attack on the second amendment but rather focuses on corporate behavior.
- He points out that the NRA primarily represents manufacturers, not owners, and functions as a lobbying group.
- Republicans are likely to frame the situation as an attack on the second amendment, as evidenced by a tweet from the President of the United States.
- Beau contrasts the President's accusations against Joe Biden with past statements where the President advocated for taking guns away without due process.
- He criticizes the NRA for not standing up against overreach, particularly because their main clients are manufacturers, specifically law enforcement.
- Beau expresses skepticism towards the NRA and suggests that supporters may be surprised by how their money is spent.
- He acknowledges the political implications of the situation and the risk of attorney generals becoming politically motivated.
- Beau believes that despite the uncertainties, there may be benefits from the developments surrounding the NRA in 2020.

# Quotes

- "Your guns will be taken away immediately and without notice. Yeah that sounds good and everything. You know, except none of that's true."
- "Trump is not a supporter of the second amendment. He never has been."
- "If you successfully defund, ban or whatever, anything, it just becomes a second market."

# Oneliner

Beau clarifies the NRA situation in New York, challenges misconceptions about gun rights, and questions the NRA's true allegiance, revealing the political implications and potential benefits amidst uncertainties.

# Audience

Advocates for gun rights

# On-the-ground actions from transcript

- Investigate how lobbying groups, like the NRA, utilize funds (exemplified)
- Stay informed about political actions that may impact gun rights (exemplified)

# Whats missing in summary

The full transcript provides additional context on the NRA controversy and raises questions about political motivations, calling for a closer examination of the NRA's actions and spending.

# Tags

#GunRights #NRA #SecondAmendment #PoliticalImplications #CorporateMisconduct