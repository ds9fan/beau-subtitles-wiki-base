# Bits

Beau says:

- Criticizes a flawed argument against Universal Basic Income (UBI) that suggests it will disincentivize work.
- Questions why people take on college debt or aim for higher-paying jobs if a basic income is provided.
- Challenges the notion that minimum wage workers will stop working if given a basic income.
- Points out that being poor is a lack of cash, not a lack of character, and everyone desires better opportunities.
- Argues that stagnant wages lead to people being taken advantage of by both companies and the government.
- Advocates for fair wages and criticizes the idea that some individuals deserve to be in poverty.
- Stresses the importance of not looking down on those with less power or influence in society.

# Quotes

- "Being poor is a lack of cash, not a lack of character."
- "Wages stagnated. And now they're so out of whack, what it
  will take to bring them into where they should be seems just out there."
- "People didn't get to where they were at simply because they made good choices or made bad choices."

# Oneliner

Criticizing flawed arguments against UBI, Beau challenges the belief that providing a basic income will disincentivize work and argues for fair wages for all.

# Audience

Advocates for fair wages.

# On-the-ground actions from transcript

- Advocate for fair wages for all workers (implied).
- Support policies that uplift individuals out of poverty (implied).

# Whats missing in summary

The full transcript delves deep into the issues surrounding wages, poverty, and societal perceptions, providing a comprehensive analysis of the flawed arguments against Universal Basic Income and the importance of fair wages for all individuals.

# Tags

#UniversalBasicIncome #Wages #Poverty #Fairness #Society