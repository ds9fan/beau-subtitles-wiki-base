# Bits

Beau says:

- Uplifting story from Paulding County, Georgia about students documenting school reopening.
- Students tally mask-wearing students, revealing lack of safety measures by the school.
- Act of advocacy journalism by students to inform public.
- School suspends students for exposing truth, likened to villains from Scooby-Doo.
- Student Hannah goes on CNN, suspensions lifted, but school fails to take further action.
- School refuses to mandate masks citing enforcement difficulties.
- Students warned school, punished instead of heeded, likely due to political reasons.
- Students' efforts should be encouraged, referenced as "good trouble."
- Uncertainty if school will change and protect students in the future.
- Implication that consequences of school's inaction may lead to sick students.

# Quotes

- "They were warned by their students. Rather than heed the warning, they punished the students."
- "What those students did is impressive. It should be encouraged."
- "You better hope that one of those sick kids isn't her."

# Oneliner

Students expose lack of safety measures in school reopening through advocacy journalism, facing punishment instead of praise.

# Audience

Students, educators, activists

# On-the-ground actions from transcript

- Support student journalists in advocating for safety measures at schools (implied)
- Encourage and empower students to speak up for the public good (implied)
- Stay informed about school policies and advocate for necessary changes (implied)

# Whats missing in summary

The emotions and determination of the students as they face adversity and push for the safety of their peers might be better understood by watching the full transcript.

# Tags

#SchoolSafety #StudentJournalism #Advocacy #PublicGood #Activism