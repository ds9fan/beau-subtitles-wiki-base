# Bits

Beau says:

- Emphasizes the importance of having a clear understanding of your core beliefs to bring about social change.
- Stresses the significance of knowing your ideal world or utopia to guide the steps towards achieving it.
- Acknowledges that deep systemic change will not happen overnight and may take generations.
- Contrasts the current foreign policy landscape with his ideal of American foreign policy focusing on peace and development.
- Advocates for celebrating small wins and acknowledging incremental progress towards long-term goals.
- Reveals his pro-immigration stance but strategically advocates for attainable ideas to persuade those who oppose his views.
- Warns against advocating ideas that are too extreme or far outside the mainstream as they may not resonate with the current societal mindset.
- Encourages individuals to determine their beliefs, envision their ideal society, and work towards it globally.
- Urges the importance of maintaining pressure and accountability on future administrations for real change.
- Reminds that progress requires consistent effort, focus, and unity even under a new administration.

# Quotes

- "You have to figure out what you believe, what your ideal society looks like."
- "If you want freedom, if you want systemic changes, you can't stop just because the new guy has a D instead of an R."
- "You have to figure out where you fit into that battle."
- "We progress. We move forward."
- "It's possible to get there. We just have to work for it."

# Oneliner

Beau stresses the need to understand core beliefs, visualize utopia, celebrate small wins, and maintain pressure for lasting social change even under new leadership.

# Audience

Activists, Advocates

# On-the-ground actions from transcript

- Determine your core beliefs and envision your ideal society (implied).
- Advocate for attainable ideas to persuade those in opposition (implied).
- Maintain pressure and hold future administrations accountable for real change (implied).

# Whats missing in summary

Beau's impassioned call for individuals to understand their beliefs, envision a better society, celebrate incremental progress, and continuously push for systemic change is best experienced in full for a comprehensive understanding.