# Bits

Beau says:

- Exploring the myth of the ideal American man and the tough guy image that one side of the political spectrum has been discussing.
- Refuting false assertions made by prominent figures like the President of the United States and Ben Shapiro regarding men being insulted by certain events.
- Sharing a personal experience about a group of tough guys he knew who defied the stereotypical tough guy image.
- Describing how these men, despite their tough exterior, focused on diverse topics like sports, music, and family rather than projecting toughness.
- Revealing the true toughest guy at the table, not because of physical strength but because he was a single dad to six kids.
- Emphasizing that true masculinity is not about destruction but creation, about lifting people up and making things better.
- Challenging the country's idea of masculinity and pointing out that real tough guys don't advertise their toughness but view fatherhood as the toughest assignment.
- Questioning the societal perception of masculinity and advocating for a shift towards understanding it as a force for good and upliftment rather than destruction.
- Suggesting that masculinity is about building something better and uplifting people rather than hurting or destroying.

# Quotes

- "Masculinity is not about hurting people. It's not about destruction. It's about making things better and uplifting people."
- "The hardest uniform that any of those guys had apparently ever seen wasn't a plate carrier. It was a burp cloth."

# Oneliner

Beau challenges the myth of the ideal American man, advocating for masculinity defined by upliftment and creation rather than destruction.

# Audience

Men, fathers, society

# On-the-ground actions from transcript

- Validate and support diverse expressions of masculinity (implied)
- Recognize and appreciate the role of fatherhood as a tough assignment (implied)
- Challenge stereotypes and misconceptions about masculinity (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of societal perceptions of masculinity and advocates for a more nuanced understanding beyond traditional tough guy stereotypes.

# Tags

#Masculinity #Fatherhood #MythsofManhood #Stereotypes #Upliftment