# Bits

Beau says:

- Addressing suburban voters and their importance in the upcoming election.
- President Trump's promise to bring law and order to the suburbs.
- Exploring the idea that grievances exist in major cities and must be acknowledged.
- Critiquing Trump's approach of sending in troops without addressing underlying issues.
- The failure of security clampdowns and historical examples of why they don't work.
- Emphasizing the importance of acknowledging grievances to move towards a solution.
- Comparing Trump and Biden's approaches to addressing social unrest.
- Questioning Trump's ability to handle the complex issue of racial injustice.
- Encouraging listeners to think critically about the strategies of political candidates.

# Quotes

- "If you don't address the grievances, this will just continue."
- "Sending in a bunch of cops is probably a bad idea."
- "All that matters is that they believe the grievances are justified."
- "Do you really believe that Donald Trump is up to the task?"
- "But I do know that he has acknowledged the grievances."

# Oneliner

Beau addresses the suburbs, criticizes Trump's approach to unrest, and questions his ability to handle grievances compared to Biden, urging critical thinking.

# Audience

Suburban voters

# On-the-ground actions from transcript

- Campaign for political candidates who acknowledge and plan to address grievances (implied)
- Engage in critical thinking and research on candidates' approaches to social unrest (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's strategy towards suburban voters and social unrest, urging listeners to critically think about the implications and solutions.

# Tags

#SuburbanVoters #SocialUnrest #Election2020 #Grievances #LawAndOrder