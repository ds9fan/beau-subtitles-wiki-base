# Bits

Beau says:

- Analyzing Trump's decision-making process regarding Dr. Scott Atlas's addition to the task force.
- Speculating on whether Trump's change in strategy is a genuine shift or politically motivated.
- Questioning the rationale behind adopting a controversial strategy of herd immunity.
- Predicting Trump's potential narrative shift towards herd immunity due to vaccine unavailability before the election.
- Criticizing the prioritization of reelection over public health and potential risks of herd immunity.
- Warning against being misled into thinking herd immunity is a viable solution.
- Pointing out the potential negative impact of herd immunity on Trump's reelection chances.
- Advocating for responsible health practices like handwashing, wearing masks, and social distancing.

# Quotes

- "He's willing to sacrifice his base."
- "As long as his base continues to believe everything that he says, he can get away with this."
- "Don't be fooled by this into thinking that herd immunity is suddenly a good idea."
- "Wash your hands. Don't touch your face. Don't go out. Stay at home."
- "Eventually we will get through this at some point."

# Oneliner

Beau questions Trump's motives behind adopting a controversial herd immunity strategy and warns against the associated risks, urging responsible health practices for everyone's safety.

# Audience

Voters, concerned citizens

# On-the-ground actions from transcript

- Wash your hands, wear a mask, practice social distancing (implied)
- Stay at home if possible (implied)

# Whats missing in summary

Detailed analysis of Trump's shifting COVID-19 strategy and the potential consequences of prioritizing politics over public health.

# Tags

#Trump #COVID19 #HerdImmunity #Election2020 #PublicHealth #Responsibility