# Bits

Beau says:

- Addressing the need to talk about managing situations better in light of recent events.
- Expressing concern over individuals supporting actions that deviated from best practices.
- Demonstrating a scenario with an assistant, Zed, who is alleged to have done something wrong.
- Explaining techniques like inducing compliance through urgent commands and physical methods to get someone in a car.
- Emphasizing the importance of not using unnecessary force once a suspect is subdued and cuffed.
- Challenging the notion that non-compliance justifies excessive force.
- Asserting that using force beyond what is necessary is unacceptable, and individuals should be held accountable if they do so.
- Concluding with a reminder that the footage of an incident does not change the facts of the case.

# Quotes

- "Do it now."
- "If you do it the same way, you need to turn in your badge."
- "That footage changes nothing, as far as this case."

# Oneliner

Beau addresses managing situations better, demonstrating tactics to induce compliance, and condemning excessive force in a thought-provoking video.

# Audience

Community members

# On-the-ground actions from transcript

- Practice de-escalation techniques when dealing with challenging situations (exemplified)
- Advocate for accountability and adherence to best practices in law enforcement (exemplified)
- Educate others on the importance of using only necessary force in law enforcement interactions (exemplified)

# Whats missing in summary

Real-life examples and practical demonstrations of techniques for managing situations better in law enforcement interactions.

# Tags

#CommunityPolicing #DeEscalationTechniques #Accountability #LawEnforcement #UseOfForce #Justice