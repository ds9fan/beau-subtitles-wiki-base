# Bits

Beau says:
- Congress members are pushing the idea that sending kids back to school is vital for the economy, but it isn't resonating with Americans.
- Lower-income Americans have already suffered significantly in the economy, with many losing their jobs.
- The statement about harming the economy by keeping kids at home implies exploiting them for economic gain.
- Beau questions why the focus isn't on Congress fixing the economy rather than rushing poor kids back to school.
- He suggests that Congress should focus on raising wages to a living point and providing better support for families.
- Beau points out that the real work to help the economy should start with Congress, not poor kids.
- Sending kids back to school, even with guidelines, poses more risk compared to staying at home.
- He calls for accountability on Capitol Hill rather than blaming lower-income individuals.
- The source of economic problems lies with those in power, not the powerless.
- Beau warns that if exploitation continues, the middle class will eventually become the working poor.

# Quotes

- "Nobody cares."
- "Maybe the real work that needs to be done to help the economy is you."
- "The poor are never the source of your problem."

# Oneliner

Members of Congress push to send kids back to school for the economy, but Beau argues the focus should be on fixing the economy and not exploiting poor children.

# Audience

Congress Members

# On-the-ground actions from transcript

- Advocate for raising wages to a living point and providing better support for families (implied).

# Whats missing in summary

Beau's impassioned call for accountability and action from those in power to address economic issues rather than exploiting vulnerable populations.

# Tags

#Education #Economy #Exploitation #Accountability #Wages