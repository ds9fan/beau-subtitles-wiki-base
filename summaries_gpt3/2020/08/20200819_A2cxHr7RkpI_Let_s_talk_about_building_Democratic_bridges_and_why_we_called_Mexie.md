# Bits

Beau says:

- Talks about communicating, building bridges, and engaging with different political ideologies.
- Explains the rationale behind choosing to feature a radical leftist named Maxi.
- Addresses the question of why showcase a group opposed to Biden-Harris during a critical election period.
- Describes the challenges in reaching out to radical leftists who do not see Biden-Harris as favorable.
- Explores the political spectrum beyond left and right, discussing the up and down authoritarian axis.
- Points out that radical leftists view Biden and Harris as unsupportive of their anti-capitalist and anti-authoritarian beliefs.
- Advocates for framing support for Biden-Harris as harm reduction rather than focusing on their positive attributes.
- Suggests finding common ground on issues related to marginalized people and workers rather than broader topics like the economy.
- Differentiates between supporting Democrats out of alignment with their platform versus seeing them as less harmful.
- Emphasizes the importance of defeating Trump as a unifying goal, especially for those who do not fully support Biden and Harris.

# Quotes

- "Biden and Harris are less bad than Trump-Pence."
- "Harm reduction, that's the way you're going to reach them."
- "Most people watching this channel are social Democrats."
- "Almost everybody who watches this channel is on the lower axis. They're anti-authoritarian."
- "Establish the bus that everybody's going to get on as defeating Trump."

# Oneliner

Beau explains why reaching out to radical leftists involves framing support for Biden-Harris as harm reduction rather than alignment with their beliefs.

# Audience

Community members, political activists.

# On-the-ground actions from transcript

- Challenge yourself to find common ground with individuals holding different political beliefs (implied).
- Prioritize harm reduction narratives when engaging with individuals who do not fully support a particular candidate (implied).
- Engage in respectful discourse and bridge-building efforts across political spectrums (implied).

# Whats missing in summary

The nuances of engaging with individuals across different political ideologies and the importance of harm reduction narratives for building bridges.

# Tags

#Communication #BuildingBridges #PoliticalEngagement #RadicalLeftists #HarmReduction