# Bits

Beau says:

- Beau introduces the topic of youth involvement in politics, particularly focusing on TikTok teens.
- He challenges the notion that youth are too inexperienced or uneducated to participate in politics by citing historical examples.
- Beau lists several historical figures who made significant contributions to American politics at a young age.
- He argues that the education of young people is the responsibility of the older generation.
- Beau suggests that forward-thinking ideas from younger individuals could benefit society.
- He questions the idea that age is the sole qualifier for understanding politics.
- Beau points out that younger individuals have to live with the consequences of political decisions for a longer time.
- He concludes by encouraging listeners to think about the importance of youth involvement in politics.

# Quotes

- "Young people have a very strong and illustrious tradition of being involved in American politics."
- "It is a citizen's primary job to make sure that the next generation is properly educated."
- "Maybe those are the voices we need to listen to the most."
- "To suggest that age is the only thing that can qualify somebody or even a qualification to understand politics, to have an opinion on it worth listening to, is just wrong."
- "Those who are younger have to live with the consequences a whole lot longer."

# Oneliner

Beau challenges misconceptions about youth in politics, citing historical examples and advocating for forward-thinking ideas from younger voices.

# Audience

Youth, Educators, Activists

# On-the-ground actions from transcript

- Support youth education (exemplified)
- Encourage forward-thinking ideas (exemplified)

# Whats missing in summary

The full transcript provides a historical perspective on youth involvement in politics, urging listeners to value the perspectives of younger individuals and support their education.

# Tags

#YouthInPolitics #Education #ForwardThinking #HistoricalPerspective #CommunityInvolvement