# Bits

Beau says:

- Raises the difference between doing what you're told and doing what's expected of you.
- Talks about the situation in Arizona where teachers are pushing back against reopening schools.
- Mentions teachers in Georgia also resisting the pressure to reopen schools in person.
- Shares a story of a parent in Georgia who pulled her kids out of school due to safety concerns.
- Questions the authority of governors pushing for school reopenings.
- Emphasizes that protecting kids is the community's responsibility, not the governors'.
- Criticizes the prioritization of money and power over safety by governmental authorities.
- Reminds that Americans should hold government accountable and prioritize the well-being of children.
- Advocates for online instruction as the least risky option during the pandemic.
- Calls for innovation in the education system and for parents to prioritize their children's safety.

# Quotes

- "Do what's expected of you."
- "Your job is to protect those kids."
- "Don't let a government authority put your child at risk."
- "They care about money and power."
- "The fact that the person has a government title means nothing."

# Oneliner

Beau raises the importance of doing what's expected to protect children, not just following orders, amidst school reopening debates.

# Audience

Parents, Teachers, Community Members

# On-the-ground actions from transcript

- Support teachers pushing back against unsafe school reopenings (implied).
- Participate in community actions like call floods if teachers request support (implied).
- Prioritize children's safety over governmental orders (implied).

# Whats missing in summary

The full transcript provides more context and detailed examples of individuals prioritizing safety over external pressure, urging community support for teachers, and questioning governmental decisions regarding school reopenings.

# Tags

#Parenting #CommunitySupport #Education #GovernmentAccountability #Safety