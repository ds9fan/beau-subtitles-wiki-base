# Bits

Beau says:

- Governor DeSantis stumbled into a good idea, urging teachers to be like seals in the Martin County school system.
- Teachers need proper training, not just guidelines on infection control, to provide in-person instruction.
- Training should be hands-on, rehearsals should be done repeatedly until teachers can handle situations in their sleep.
- Teachers need an inexhaustible budget for supplies, tools, training, and support.
- They should have access to experts, advisors, and total information awareness for student welfare.
- Expecting teachers to be like SEALs without proper resources is risky and unrealistic.
- SEALs train for years to improvise in the field; teachers are trained to teach, not manage public health crises.
- The comparison between teachers and seals is empty rhetoric if teachers aren't given necessary support.
- Treating teachers like SEALs without proper training risks their lives without recognition.
- Beau questions the sincerity behind the rhetoric of treating teachers like SEALs for motivation.

# Quotes

- "Expect our teachers to behave like seals."
- "Treating teachers like SEALs right now is risking their life."
- "This is a little outside of their scope."
- "They won't even get a flag."
- "It's just a thought."

# Oneliner

Governor DeSantis compares teachers to seals, but Beau questions the practicality and risks without proper training and support.

# Audience

Teachers, Education Boards

# On-the-ground actions from transcript

- Provide teachers with hands-on training on infection control (implied).
- Ensure teachers have access to necessary supplies, tools, and resources for effective teaching (implied).
- Establish a support system with experts available 24/7 for teachers (implied).

# Whats missing in summary

The full transcript provides detailed insights into the challenges teachers face when compared to seals and the need for adequate support and training.

# Tags

#Teachers #Training #Support #Education #PublicHealth