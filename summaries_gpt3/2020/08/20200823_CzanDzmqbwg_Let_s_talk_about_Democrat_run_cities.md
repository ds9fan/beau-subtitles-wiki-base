# Bits

Beau says:

- Explains how repeating statistics or statistic-like information can lead to false correlations being accepted.
- Points out that the President repeatedly mentions "Democrat run cities" to link Democratic policies with high crime rates.
- Notes that most cities are Democrat-run because the Democratic party appeals to urban residents.
- Mentions that looking at states, many of the top states for violent crime voted for Trump and have Republican governors.
- Refers to a company, SafeWise, that ranks dangerous cities, showing that some Republican-run cities have high crime rates.
- Counters the idea of a causal link between a party being in control and crime rates, attributing crime to poverty, income inequality, lack of education, and opportunities.
- Concludes that the President's claims about Democrat-run cities and high crime rates are baseless and meant to smear certain areas.
- Suggests the President's fixation on Chicago may stem from personal grievances rather than factual basis.

# Quotes

- "Crime is related to poverty, income inequality, lack of access to education, stuff like that. Not having opportunities, that's what has a causal link to crime. Not red or blue."
- "There is absolutely zero causal link between a party being in control and crime rates. That's not a thing."
- "This whole idea is garbage."
- "It's almost like maybe somebody he doesn't like kind of came into politics from there and he just won't let it go because he's a petty, petty man."
- "Y'all have a good day."

# Oneliner

Beau debunks the false correlation between Democrat-run cities and high crime rates, attributing crime to poverty and lack of opportunities, not political affiliation.

# Audience

Fact-checkers, Voters, Community Members

# On-the-ground actions from transcript

- Fact-check statistics and claims made by politicians (implied)
- Support community programs addressing poverty, education, and income inequality (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of how crime rates are linked to societal factors rather than political party affiliation.

# Tags

#Statistics #Debunking #Politics #CrimeRates #Poverty #Education #CommunityJustice