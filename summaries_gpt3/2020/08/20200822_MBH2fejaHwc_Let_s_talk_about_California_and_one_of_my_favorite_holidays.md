# Bits

Beau says:

- Introduces the topic of California and a unique holiday celebrated by about 500 people for the past five years.
- Mentions a veteran named Brian who made a thought-provoking statement about the origins of freedoms and rights.
- Describes the unconventional holiday, Think a Criminal Day, on August 22nd, honoring those who were once deemed criminals but later recognized as heroes.
- Explains the current situation in California with over two dozen major wildfires and 300 smaller ones.
- Points out the usual practice of using inmates to fight fires for minimal pay but how half the teams are now unavailable due to social distancing measures inside prisons.
- Notes that due to the lack of inmate firefighters, the governor called in soldiers from the National Guard to assist.
- Raises the question of how many historical heroes had mug shots, linking back to the idea of criminals contributing to positive change.
- Encourages viewers to ponder on the concept shared and wishes them a good day.

# Quotes

- "Therefore, August 22nd is Think a Criminal Day."
- "How many of them had mug shots?"
- "Y'all have a good day."

# Oneliner

Beau introduces the unconventional holiday "Think a Criminal Day" while discussing California's wildfire situation and the involvement of soldiers due to inmate firefighters' unavailability, prompting reflection on the criminal origins of some historical heroes.

# Audience

Community members

# On-the-ground actions from transcript

- Support firefighter relief efforts (exemplified)
- Volunteer for wildfire aid organizations (exemplified)

# Whats missing in summary

The emotional impact and depth of historical figures who were once considered criminals but later revered for their contributions.