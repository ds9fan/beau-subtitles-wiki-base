# Bits

Beau says:

- FDA warns against using certain hand sanitizers due to ineffectiveness and contamination.
- President criticized Dr. Fauci's COVID-19 response, claiming US high cases due to extensive testing.
- European countries shut down deeper than the US, leading to better COVID-19 outcomes.
- Lack of unified national response and leadership contributes to US COVID-19 situation.
- President's actions worsen the already bad COVID-19 situation.
- Administration lacks a clear plan to address the devastating COVID-19 impact.
- Allegations that the administration prioritized political interests over effective response strategies.
- The president's divisive actions disregard the well-being of all Americans, regardless of political affiliation.
- Beau criticizes the president for tweeting falsehoods and undermining national leadership during the crisis.

# Quotes

- "He's tweeting out falsehoods. And he's doing so at the expense of American lives."
- "We still have no national leadership."
- "The only leadership we're getting at the national level, the president tries to undermine at every turn."

# Oneliner

Beau warns against dangerous hand sanitizers, criticizes President's actions worsening US COVID-19 situation, and calls for unified national leadership. 

# Audience

Public Health Advocates

# On-the-ground actions from transcript

- Avoid using hand sanitizers listed by the FDA as ineffective or contaminated (implied)
- Advocate for a unified national response to COVID-19 (implied)

# Whats missing in summary

Detailed analysis and context on the impact of the president's actions on the COVID-19 crisis.

# Tags

#COVID-19 #HandSanitizers #PublicHealth #NationalLeadership #PoliticalResponsibility