# Bits

Beau says:

- Addresses the financial struggles of the Postal Service and its frequent need for funding from Congress.
- Points out that the Postal Service is a government service, not a business, explicitly authorized in the Constitution.
- Mentions the influence of campaign contributions on decisions related to the Postal Service, citing Mitch McConnell's ties to UPS and FedEx.
- Raises concerns about sacrificing services for rural areas if the Postal Service faces funding challenges.
- Emphasizes the importance of government services like the Postal Service, even if they don't turn a profit.
- Criticizes the prioritization of money over providing an essential service, referencing the Flint water crisis.
- Stresses the significance of the Postal Service in maintaining fair voting practices, especially during times of crisis.
- Condemns actions that hinder mail-in voting and suggests they are aimed at suppressing the vote.
- Expresses a lack of care for democracy, the Constitution, and the republic by certain individuals in power.
- Advocates for understanding the true nature of the Postal Service as a government service and the need to support vital services.

# Quotes

- "The Postal Service isn't asking for a bailout, it's asking for funding."
- "The postal service is not a business. It's a government service."
- "He wants to suppress the vote because he doesn't care about the Constitution. He doesn't care about democracy. He doesn't care about the republic, and he never has."

# Oneliner

The Postal Service seeks funding as a vital government service, critical for democracy, facing challenges due to misconceptions and political influences.

# Audience

Voters, Postal Service supporters

# On-the-ground actions from transcript
- Contact your representatives to advocate for adequate funding for the Postal Service (implied)
- Support initiatives that aim to protect and enhance mail-in voting processes (implied)

# Whats missing in summary

The full transcript provides more context on the financial struggles of the Postal Service, the importance of government services, and the political motivations behind actions affecting mail-in voting.

# Tags

#PostalService #GovernmentService #Funding #Democracy #Voting