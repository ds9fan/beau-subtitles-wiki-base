# Bits

Beau says:

- Explains how laws, like Murphy's laws of combat, can be applied to politics for those wanting to be politically active beyond social media.
- Emphasizes the importance of understanding that allies, not enemies, can sometimes be the biggest challenge in grassroots organizing due to differing views or mistakes.
- Advises grassroots organizers to acknowledge their vulnerability and not to see themselves as invincible.
- Stresses the need for adaptability in plans because no plan survives first contact and there is no such thing as a perfect plan.
- Encourages activists to believe in their cause genuinely rather than seeking recognition or medals.
- Warns about established roads always being mined in politics, indicating the need for innovative strategies rather than following traditional paths.
- Caution against staying when someone suggests "watch this" at street events, as it signals potential escalation.
- Reminds activists not to dismiss diversions as unimportant, as they might be part of a larger strategy.
- Mentions that experience is gained just after it is needed, urging persistence despite initial failures.
- Points out the significance of clear communication in slogans and messages to avoid misunderstandings.
- Acknowledges that success often happens unnoticed, while failures are magnified in public, especially in politics.
- Differentiates between professional predictability and amateur unpredictability, advising flexibility in dealing with both.
- Concludes by stressing the importance of community networks in activism, stating that change cannot be achieved alone.

# Quotes

- "Friendly Fire isn't. Your allies can be more devastating to you than whatever you're working against."
- "No plan survives first contact. Be adaptable."
- "You're not Superman. Keep that in mind."
- "Cavalry is not coming. It's just you."
- "Success occurs when nobody is watching."

# Oneliner

Beau shares insights on applying Murphy's laws to politics, stressing adaptability, genuine belief in causes, and the power of community in effective activism.

# Audience

Community organizers, activists.

# On-the-ground actions from transcript

- Build a strong community network committed to making your community better (suggested).
- Form a team with friends dedicated to supporting each other and working towards common goals (exemplified).
- Acknowledge the importance of allies and maintaining unity within the group (implied).

# Whats missing in summary

The full transcript provides detailed guidance on navigating politics through the lens of Murphy's laws, offering valuable insights for aspiring activists.

# Tags

#Activism #CommunityBuilding #GrassrootsOrganizing #PoliticalEngagement #Adaptability