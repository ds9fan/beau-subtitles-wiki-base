# Bits

Beau says:

- Transitionary periods occur in countries and personal lives, like in the US in recent years.
- Celebrates friends' accomplishments, like promotions, finishing a doctorate, having a child, getting married, and more.
- Acknowledges the significance of using new titles or names to mark these transitions.
- Emphasizes the importance of supporting friends during their transitionary periods.
- Views refusal to acknowledge and use new titles as a sign of not being a true friend or a good person.
- Encourages celebrating and embracing changes in friends' lives.

# Quotes

- "If somebody goes through a transitionary period and you're their friend, you want to celebrate it with them."
- "If somebody goes through something, goes through some form of transition and you refuse to use the new way of addressing them, you're probably not their friend."

# Oneliner

Transitionary periods in countries and personal lives call for celebration and using new titles to mark significant changes, reflecting true friendship and support.

# Audience
Friends and allies

# On-the-ground actions from transcript

- Celebrate friends' achievements with them (exemplified)
- Use new titles or names to acknowledge and celebrate their transitions (exemplified)

# Whats missing in summary
The nuanced examples of friends' transitions and the importance of embracing change in personal relationships.

# Tags
#TransitionaryPeriods #CelebrateChanges #Friendship #Support #EmbraceTransitions