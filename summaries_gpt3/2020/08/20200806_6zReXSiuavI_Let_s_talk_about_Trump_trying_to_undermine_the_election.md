# Bits

Beau says:

- President's changing views on mail-in voting discussed.
- President was for mail-in voting in Florida on August 4th, then against it in Nevada on August 5th.
- Beau suggests president should create a list of states he believes should or shouldn't be able to vote.
- President filing suit in Nevada to suppress the vote.
- Accusations of the president attempting to rig the election and deny the right to vote to his opposition.
- Criticism towards those who remain silent despite claiming to be patriots and lovers of the country.
- Warning that if the president succeeds in rigging the election, it marks the end of the United States as an experiment.
- Prediction that the president's view on mail-in voting will change based on which population it favors.
- Condemnation of the president's active attempt to undermine and rig the election.

# Quotes

- "The president is using the weight of the Oval Office to rig the election."
- "If he succeeds in this, if the president can rig the election, that's it. The United States is done."
- "Those who claim to be patriots and lovers of the country will remain silent and sell out their country."
- "This is an active attempt by the president of the United States to undermine the election, to rig the election."
- "What's going to happen the second there is a state that has a population of probable mail-in voters that seem like they're going to vote for him?"

# Oneliner

President's changing views on mail-in voting and active attempt to rig elections threaten the foundation of democracy in the United States, with a warning of the country's demise if successful.

# Audience

Voters, patriots, activists

# On-the-ground actions from transcript

- Contact local election officials to ensure fair and equal access to voting (implied)
- Join or support organizations working to protect voting rights and ensure fair elections (implied)

# Whats missing in summary

Analysis of potential consequences of voter suppression tactics and suggestions on how individuals can actively protect democracy.

# Tags

#MailInVoting #ElectionRigging #VoterSuppression #Democracy #UnitedStates