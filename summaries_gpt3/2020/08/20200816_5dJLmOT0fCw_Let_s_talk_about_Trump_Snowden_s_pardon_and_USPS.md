# Bits

Beau says:

- Trump's consideration of pardoning Edward Snowden is a distraction from his attack on the United States Postal Service.
- Trump's past tweets about Snowden show a different stance than his current consideration of a pardon.
- Trump's administration is more damaging to the U.S. than Snowden ever was.
- Beau supports a pardon for Snowden, believing he acted to honor his oath.
- Trump's move is to shift focus from the Postal Service and create a new debate.
- The U.S. Postal Service is vital for the election and Trump's actions against it are more harmful than Snowden's leaks.

# Quotes

- "This is an attempt to draw attention away from Trump's move against the United States Postal Service because that is what is in the news cycle right now."
- "The U.S. Postal Service is pretty critical to the election, and Trump's moves against it are far more damaging to the United States than anything, anything leaked by Snowden."

# Oneliner

Trump's consideration of pardoning Edward Snowden is a distraction from his damaging actions against the U.S. Postal Service, which is critical for the election.

# Audience

Voters, concerned citizens

# On-the-ground actions from transcript

- Contact your representatives to express support for the U.S. Postal Service and demand protection for its critical role in the election (implied).

# Whats missing in summary

The emotional impact of Beau's disappointment in Trump's actions and the urgency to prioritize protecting the U.S. Postal Service.

# Tags

#EdwardSnowden #DonaldTrump #USPostalService #Election #Distraction