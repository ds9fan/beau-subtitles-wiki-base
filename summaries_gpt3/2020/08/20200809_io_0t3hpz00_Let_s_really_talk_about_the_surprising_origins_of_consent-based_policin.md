# Bits

Beau says:

- The origins of consent-based policing in the United States date back to the Declaration of Independence, where the government's power was meant to derive from the consent of the governed.
- The core principles of consent-based policing include the idea that the police are part of the public and effective policing is measured by the lack of crime, not the number of arrests.
- Both conservative and liberal ideologies can find alignment in consent-based policing, which aims to break up the government's monopoly on violence and have law enforcement become a part of the community.
- Implementing consent-based policing requires a shift in laws to better represent the will of the people, allowing for a focus on prevention through community engagement rather than punishment.
- Beau outlines nine standing orders for consent-based policing, including prioritizing crime prevention, gaining public approval and cooperation, offering impartial service, and using force only as a last resort after persuasion and warnings fail.
- Beau references the historical basis of these principles, dating back to Robert Peel's standing orders for British cops in 1829, which emphasized community policing and minimal force usage.
- He challenges the notion that the presence of guns in the U.S. should prevent the adoption of consent-based policing, pointing to successful models in countries like the UK, Canada, and Australia with similar histories of armed populations.

# Quotes

- "Consent-based policing is as American as apple pie."
- "The government derives its power from the consent of the governed. It is supposed to display that just power with consent."
- "The public are the police, the police are the public."
- "Efficiency is the absence of crime, not a lot of arrests."
- "Moving in the direction of the Declaration of Independence is probably more American than moving in the direction of totalitarian governments."

# Oneliner

The origins of consent-based policing in the U.S. date back to the Declaration of Independence, with core principles focusing on community engagement, prevention over punishment, and public cooperation.

# Audience

Law enforcement agencies

# On-the-ground actions from transcript

- Advocate for a shift towards consent-based policing by engaging with local law enforcement agencies and policymakers (implied).
- Support initiatives that prioritize community engagement, prevention strategies, and public cooperation in policing practices (implied).

# Whats missing in summary

Historical context and reasoning behind the development of consent-based policing. 

# Tags

#ConsentBasedPolicing #CommunityEngagement #PreventionOverPunishment #LawEnforcement #DeclarationOfIndependence