# Bits

Beau says:

- Criticizes President Trump for linking the increase in COVID-19 cases to increased testing.
- Points out the inaccuracy in Trump's statement about testing causing the rise in cases.
- Emphasizes that hospitalizations have increased significantly, unrelated to testing.
- Argues against reopening schools, stating it will contribute to the spread of the virus and harm the economy.
- Addresses the importance of following CDC guidelines for reopening.
- Expresses concern about the lack of a unified plan and the potential for more loss of life and economic damage.
- Urges Trump to lead the nation in taking the necessary steps to combat the pandemic.

# Quotes

- "Cases up because of big testing, cases are up because more people have it."
- "Opening the schools is a bad idea. It's going to help it spread. It's going to hurt the economy."
- "Because we're not getting back on track, because we're not taking the steps we need, because we don't have a unified plan, all of this drags on."
- "If the president is concerned about the economy, he's going to have to lead the nation into doing the right thing."
- "In order to set the example, you have to take responsibility for something."

# Oneliner

Beau criticizes Trump's misleading COVID-19 testing claims, warns against reopening schools, and stresses the need for a unified plan to combat the pandemic.

# Audience

Concerned citizens, policymakers

# On-the-ground actions from transcript

- Follow CDC guidelines for COVID-19 prevention and reopening schools (suggested)
- Advocate for a unified national plan to combat the pandemic (suggested)
- Encourage responsible leadership to prioritize public health and safety (suggested)

# Whats missing in summary

Beau's detailed analysis and passionate plea for effective leadership and adherence to expert advice during the pandemic.

# Tags

#COVID19 #Trump #CDC #PublicHealth #Pandemic #Leadership