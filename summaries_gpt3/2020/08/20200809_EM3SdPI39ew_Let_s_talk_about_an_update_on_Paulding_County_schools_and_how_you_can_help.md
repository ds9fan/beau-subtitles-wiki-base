# Bits

Beau says:

- Paulding County Schools in Georgia reopened in person, with students documenting the reopening and posting online about non-compliance with safety measures.
- The school initially suspended the students but rescinded the suspensions after public involvement.
- Six students and three staff members have tested positive for COVID-19 after being in the school, suggesting that the numbers will continue to rise.
- During a school board meeting, suggestions were made to move students around every 14 minutes as a safety measure, which may not effectively prevent transmission.
- Beau recommends recovered individuals to donate plasma, as it contains antibodies that can potentially help in treatment.
- Leadership at federal, state, and county levels is lacking, and Beau urges people to listen to medical experts and take action themselves.

# Quotes

- "We're gonna have to listen to the medical experts. We're gonna have to act, because they're not going to."
- "It's not really about putting kids at risk just to reopen the economy."
- "We need to do it ourselves."

# Oneliner

Paulding County Schools face COVID-19 challenges, urging community action and plasma donation for treatment in the absence of effective leadership.

# Audience

Community members, COVID-19 survivors

# On-the-ground actions from transcript

- Donate plasma if you have recovered from COVID-19 (suggested)
- Listen to medical experts and take necessary actions (implied)

# Whats missing in summary

The full transcript provides additional details on the lack of leadership at different government levels and the importance of individuals taking responsibility for their actions during the pandemic.

# Tags

#COVID-19 #CommunityAction #PlasmaDonation #SchoolSafety #LeadershipVacuum