# Bits

Beau says:

- Using an Alice analogy to describe the current state of the United States where labels divide people.
- Conservatives advocate for social democratic policies like health care, housing, free college, and more.
- Despite denying it, conservatives support similar policies for subsets of the population, like the military.
- Conservatives believe these policies make people more productive and efficient.
- They fight for these policies within the military, recognizing its importance.
- The argument that military members "earned" these benefits is refuted as others in society could also benefit.
- Top officials push for division among Americans to maintain control and profit.
- Access to social features wouldn't make people lazy, as conservatives already support them for certain groups.
- Divisions in society benefit those at the top who want to maintain control and profit.
- Allowing access to social features may prevent young individuals from being easily recruited for unnecessary wars.

# Quotes

- "Everything is what it isn't."
- "They are trained to oppose it by those on the top who want those on the bottom divided."
- "Stop turning them into combat vets."

# Oneliner

Beau uses an Alice analogy to show how conservatives inadvertently support social democratic policies and the impact of division on society.

# Audience

Activists, Voters, Community Members

# On-the-ground actions from transcript

- Advocate for social democratic policies in your community (implied)
- Support initiatives that provide healthcare, housing, and education access for all (implied)
- Challenge divisions and work towards unity in your community (implied)

# Whats missing in summary

The full transcript delves into the hidden support conservatives have for social policies and the impact of societal division on maintaining control and profiting from insecurity.

# Tags

#AliceAnalogy #SocialDemocraticPolicies #Conservatives #Unity #CommunityActions