# Bits

Beau Young says:

- President wants a patriotic education program, but Beau believes it's a faulty premise.
- Quotes Thomas Paine, the founder of the American Revolution, to illustrate true patriotism.
- Thomas Paine's writings like "Common Sense" inspired rebellion and gave people something to believe in.
- Beau contrasts patriotism with nationalism through George Washington and Nathan Hale's quotes.
- Real patriots correct their government and defend a particular way of life, not seeking power or prestige.
- Patriotism is defensive and earned, not taught through indoctrination or mythology.
- Teaching only positive aspects of American history won't create patriots who question their government.
- To make America great, Beau suggests living up to the promises of past patriots like Thomas Paine.
- Real patriots question the government and strive to correct wrongs in society.
- Beau believes the president's patriotic education program is doomed to fail if it ignores critical aspects of history.

# Quotes

- "Patriotism is of its nature defensive, both militarily and culturally." 
- "Patriots do not obey no matter what, that's nationalists."
- "Just because you say something over and over again and people believe it doesn't mean that it's true."
- "Independence is my happiness, the world is my country, and my religion is to do good."
- "To make America great, that's how you do it. That was what moved people."

# Oneliner

President's patriotic education program is doomed to fail as true patriotism involves questioning the government and defending a way of life, not teaching mythology.

# Audience

Educators, policymakers, activists

# On-the-ground actions from transcript

- Question the government's actions and policies (implied)
- Defend a particular way of life that upholds values of true patriotism (implied)

# Whats missing in summary

The emotional impact and passion conveyed by Beau Young in his rejection of the president's patriotic education program.

# Tags

#Patriotism #AmericanHistory #Nationalism #Education #Government