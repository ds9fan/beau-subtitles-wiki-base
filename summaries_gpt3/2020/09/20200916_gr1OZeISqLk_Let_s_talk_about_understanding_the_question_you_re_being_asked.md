# Bits

Beau says:

- Recording on the edge of a storm due to heavy rain preventing recording in his shop with a tin roof.
- Talks about the importance of understanding the question you're being asked, which he didn't grasp for years.
- Shares his experience of helping people build community networks and the common query from younger individuals on how to start.
- Explains that younger generations, being more technology-based, struggle with the concept of building core groups in community networks.
- Mentions stumbling upon a video by Working Stiff USA that addresses this issue effectively.
- Emphasizes that the starting core group can be anybody from various walks of life, not necessarily close friends.
- Assures that personal connections don't have to happen immediately; commitment to the common goal is what matters.
- Points out that as networks grow, they naturally split into groups where personal connections form.
- Recommends a YouTube video (by Working Stiff USA) that focuses on building the initial core group for community networks.
- Stresses the importance of community networks, especially during emergencies like fires and hurricanes.

# Quotes

- "That question doesn't mean what we think it means."
- "All that matters is that commitment to that goal."
- "Don't focus too much on the production value, but focus on the content."
- "These community networks are incredibly important."
- "Y'all have a good night."

# Oneliner

Beau stresses the importance of understanding questions, building community networks, and the critical role they play in emergencies like fires and hurricanes.

# Audience

Community builders and emergency responders.

# On-the-ground actions from transcript

- Reach out to diverse individuals in your community to form the initial core group (exemplified).
- Prioritize commitment to common goals over immediate personal connections when building community networks (exemplified).
- Watch the suggested YouTube video by Working Stiff USA to learn more about starting community networks (suggested).

# Whats missing in summary

The full transcript provides detailed insights on building community networks, understanding queries, and the significance of these networks during emergencies.

# Tags

#CommunityBuilding #UnderstandingQuestions #YouthEngagement #EmergencyPreparedness #CommunityResilience