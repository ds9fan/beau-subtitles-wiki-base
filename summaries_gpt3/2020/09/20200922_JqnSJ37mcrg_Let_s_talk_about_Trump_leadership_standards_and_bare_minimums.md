# Bits

Beau says:

- Criticizes praising Biden for fulfilling basic civic duties as setting the bar too low.
- Warns against using Trump as the only standard for presidential candidates.
- Stresses the importance of addressing systemic issues and not just superficial fixes.
- Argues that the impact of Trump's presidency will be long-lasting regardless of the election outcome.
- References Sun Tzu's characteristics of a good leader and contrasts them with Trump's qualities.
- Emphasizes the need for competence, understanding of governance, and respect for others in a leader.
- Points out Biden's willingness to listen to experts and take responsibility, contrasting with Trump's behavior.
- Advocates for striving for higher standards in selecting public officials and government accountability.

# Quotes

- "If we set the bar as better than Trump, we could end up with a candidate whose only qualifications are that they don't lie constantly, they don't engage in nepotism, don't engage in overt corruption..."
- "Whether you think Biden has it or not, that's a decision you have to make."
- "That's what I had to do with."
- "We should set the standards pretty high."

# Oneliner

Beau criticizes setting the bar too low by using Trump as the sole standard for presidential candidates and advocates for higher standards in selecting public officials.

# Audience

Voters, citizens

# On-the-ground actions from transcript

- Advocate for higher standards in selecting public officials (implied)
- Strive for deep change in how candidates are evaluated and selected (implied)
- Hold public officials accountable for competence and integrity (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of leadership standards, contrasting Trump and Biden's qualities, and advocating for higher expectations in selecting public officials.

# Tags

#Leadership #PresidentialCandidates #Trump #Biden #Standards