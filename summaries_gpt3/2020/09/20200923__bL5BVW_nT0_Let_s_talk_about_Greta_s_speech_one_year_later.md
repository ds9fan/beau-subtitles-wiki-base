# Bits

Beau says:

- Addressing a speech given a year ago, dismissed due to various reasons like speaker's age and appearance.
- Urges to revisit the message rather than dismissing it outright.
- Quotes the impactful message: "My message is that we'll be watching you."
- Speaker expresses frustration at being on stage instead of in school, accuses adults of stealing dreams and childhood.
- Condemns the focus on money and economic growth while the planet suffers.
- Criticizes the lack of action despite clear scientific evidence on climate change.
- Draws parallels to public health issues and government's prioritization of profits over people's well-being.
- Emphasizes that those in power often escape the consequences of their actions due to wealth and privilege.
- States the relevance of the speech today and the need to learn from historical instances of downplaying crises for personal gain.
- Urges preparation and action for the impending challenges, stressing that no borders or flags will offer protection.

# Quotes

- "My message is that we'll be watching you."
- "You've stolen my dreams and my childhood with your empty words."
- "This issue that we're facing today, it's a dress rehearsal for what's coming around the bend."

# Oneliner

Beau revisits a dismissed speech, warning about the consequences of dismissing urgent messages and the need for preparation amidst looming challenges.

# Audience

Activists, Climate Advocates

# On-the-ground actions from transcript

- Get involved in electoral politics to push for necessary changes (exemplified)
- Prepare and insulate yourself for future challenges (exemplified)

# Whats missing in summary

The emotional impact of Greta Thunberg's speech and the critical need to heed warnings about climate change and societal issues.

# Tags

#Speech #ClimateChange #Power #Action #Preparation