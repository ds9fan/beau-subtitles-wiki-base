# Bits

Beau says:

- Explains the forgotten battle of Belleau Wood and its significance to the Marines.
- Mentions how the battle is part of Marine lore but largely unknown outside the Marine Corps.
- Describes key moments from the battle, like Marines filling in the line when French forces retreated and a Marine leading a charge across an open field.
- Talks about the renaming of Belleau Wood to the Wood of the Marine Brigade due to the intensity of the fighting.
- Addresses the impact of President Trump's alleged remarks on modern Marines and their connection to their spiritual ancestors.
- Points out the difference in reactions between modern vets who understand political aspects of deployments and the emotional response to attacks on their heroes.
- Emphasizes the significance of the Marine Corps and their history, especially in the face of political indifference towards military personnel.

# Quotes

- "That tough guy line you've heard in a dozen action movies actually said."
- "Well, come on, do you want to live forever?"
- "The deadliest weapon in the world is a Marine and his rifle."
- "This is an attack on the very foundations of the Marine Corps."
- "For all of the talk about supporting the troops, they don't care."

# Oneliner

Beau delves into the forgotten battle of Belleau Wood, its impact on Marines, and the indifference of politicians towards military history and heroes.

# Audience

History enthusiasts, Marines, Veterans

# On-the-ground actions from transcript

- Honor the memory of forgotten battles like Belleau Wood by sharing its story and significance within your community (implied).
- Support veterans and active-duty military personnel through local initiatives or organizations to show appreciation for their sacrifices (implied).
  
# Whats missing in summary

Beau's emotional delivery and personal connection to the story adds depth and authenticity to the retelling.

# Tags

#MarineCorps #BelleauWood #Trump #MilitaryHistory #Veterans