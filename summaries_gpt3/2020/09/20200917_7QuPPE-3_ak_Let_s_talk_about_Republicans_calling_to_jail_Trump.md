# Bits

Beau says:

- Beau introduces news from Nashville involving emails surfacing about downplaying data connected to the mayor's office.
- Emails suggest a decision to not highlight the lack of spread in bars and restaurants in Nashville.
- Republicans have criticized this decision, focusing on downplaying data leading to keeping bars closed.
- Beau hints at missing context, possibly related to similar events in Florida and Texas with Republican governors.
- Beau shifts the focus to Republican reactions, quoting Charlie Kirk and Donald Trump Jr. advocating for consequences for downplaying data.
- Beau challenges the Republican stance, suggesting accountability should also extend to the President if downplaying data is a punishable offense.
- Beau questions whether these statements are genuine concerns or simply political rhetoric.
- Beau insists that if downplaying data is a punishable offense, it should be applied uniformly.
- Beau calls for accountability and application of standards across the board in such cases.
- Beau stresses the importance of upholding the law and ensuring accountability for all public officials.

# Quotes

- "Any politician like the Nashville Mayor John Cooper, who intentionally covers up data on public health deaths in order to keep restaurants and bars closed should be removed from office and tried immediately." - Charlie Kirk
- "The dim mayor of Nashville knowingly lied about data to justify shutting down bars and restaurants, killing countless jobs and small businesses in the process. Everyone involved should face jail time." - Donald Trump Jr.

# Oneliner

Beau calls for accountability and uniform application of standards in response to Republican criticisms of downplaying data in Nashville.

# Audience

Public officials

# On-the-ground actions from transcript

- Hold public officials accountable for downplaying data. (implied)
- Advocate for uniform application of standards in similar situations. (implied)

# Whats missing in summary

Full context and details on the events in Nashville and Republican reactions. 

# Tags

#Nashville #DataDownplaying #RepublicanReactions #Accountability #PublicOfficials