# Bits

Beau says:
- President's attempts at deflection amid public health crisis reveal his deceit.
- American people not falling for distractions like Afghanistan withdrawal or Supreme Court suggestions.
- President's ineffective leadership led to more lives lost in months than in 20 years of armed conflict.
- Tom Cotton's suggestion to use US Army for restoring order raises concerns about his Supreme Court nomination.
- Advocating no-quarter reveals troubling mindset about extrajudicial execution of American citizens.
- Woodward's release of audio exposes President's downplaying of public health crisis, costing American lives.

# Quotes

- "President's willful deceit cost tens of thousands of American lives."
- "This other stuff, who he might suggest for the Supreme Court, you know, a possible withdrawal, that's not news."
- "The news is a bunch of bags that are full that didn't have to be."

# Oneliner

President's deflective tactics fail to distract from his deceit in handling the public health crisis, costing American lives.

# Audience

American citizens

# On-the-ground actions from transcript

- Call for accountability from leaders regarding handling of public health crises (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the President's attempts at deflection and deceit amid the public health crisis, exposing the consequences of his actions on American lives.

# Tags

#Deception #PublicHealthCrisis #Accountability #TomCotton #BobWoodward