# Bits

Beau says:

- President's attempt to blame Bob Woodward for his actions.
- Everyone already knew that the president was aware of the information.
- Woodward's effectiveness lies in showing the contrast between what was known privately and publicly.
- Americans were already aware of the information that Woodward revealed.
- Woodward informs the American people and gives them a choice for the future.
- The president did not let the experts do their job, actively undermining their work.
- Woodward's work demonstrates the president's prioritization of his approval rating over American lives.
- Had Woodward released the information earlier, it wouldn't have had the same impact.
- The president's base might have dismissed the information as just an offhand comment.
- Woodward's form of journalism is exactly what he did, and blaming him is not fair.

# Quotes

- "Everybody knew back then that the president was aware of this."
- "He informed the American people and he's giving them a choice."
- "He actively undermined their work and it cost tens of thousands of American lives."
- "His job, his form of journalism, what he does is exactly what he did."
- "Let's just hope that next year there are people who will listen to the experts."

# Oneliner

President's attempt to blame Woodward falls flat as everyone already knew, showcasing Woodward's contrast between public and private knowledge while underscoring the president's failure to prioritize American lives over his own approval ratings.

# Audience

American citizens

# On-the-ground actions from transcript

- Listen to the experts and prioritize public health over personal gain (implied).
- Support investigative journalists like Bob Woodward who inform the public and hold leaders accountable (implied).

# Whats missing in summary

The detailed emotional impact of Woodward's work on revealing the truth and its potential to influence future decision-making.

# Tags

#BobWoodward #InvestigativeJournalism #PublicHealth #Accountability #AmericanLives