# Bits

Beau says:

- Beau is discussing legislation coming out of Florida proposed by Governor Ron DeSantis, critiquing it as a way to mask the core issues rather than address them.
- The legislation includes a prohibition on disorderly assemblies of more than seven people and enhanced penalties for obstructing roadways, allowing vehicles to run over protestors, toppling monuments, and harassing people in public accommodations.
- It adds a RICO liability, mandatory minimum jail sentences for striking an officer, and denies bail for certain offenses.
- Local jurisdictions in Florida could lose state money if they defund the police, creating a victim compensation mechanism allowing residents to sue the government if it fails to protect them.
- Beau criticizes the legislation for not addressing the real problem: police violence leading to unarmed individuals being killed.
- He suggests implementing mandatory minimum sentences for police officers who shoot unarmed individuals and predicts the ACLU will challenge the unconstitutional aspects of the bill.
- Beau believes that increasing police presence and violence to suppress speech about law enforcement will only fuel the movement further.
- He concludes that the legislation is flawed, unconstitutional, and will likely be struck down, failing to address the underlying issue.

# Quotes

- "The problem isn't that there's people in the streets. The problem is people feeling they have to be in the streets."
- "The issue is folks getting killed by the cops while they're unarmed."
- "This is the type of legislation that gets proposed by failed leaders in failed states."

# Oneliner

Beau criticizes Florida's legislation as a flawed attempt to mask issues rather than address them, especially failing to tackle the core problem of police violence.

# Audience

Activists, Civil Rights Advocates

# On-the-ground actions from transcript

- Challenge unconstitutional laws through legal action (implied)
- Advocate for laws addressing police violence and accountability (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Florida's legislation and Beau's perspective on addressing police violence and systemic issues effectively.