# Bits

Beau says:

- Beau addresses the topic of presidential candidate Joe Biden's leadership, standards, and meeting bare minimums.
- People reached out to Beau with messages about Biden wearing a mask during a speech.
- Biden followed a local ordinance by wearing a mask throughout his speech, setting an example.
- Beau acknowledges the positive attributes of Biden following the ordinance and displaying leadership.
- Despite appreciating Biden's actions, Beau points out that wearing a mask is the bare minimum expected in the current climate.
- Beau argues that praising Biden for meeting the basic standard shouldn't be the norm but rather critiquing those who fall short of meeting standards.
- He suggests that rather than praising those who do the bare minimum, criticism should be directed towards those who don't meet standards to foster real leadership.
- Beau expresses the importance of not allowing Trump's lack of leadership to become the benchmark for judging other candidates.
- Biden's actions are viewed as standard behavior, and Beau stresses the need for real leaders in office.
- Beau, a strong critic of Biden, believes that applauding minimal displays of leadership shouldn't impede progress in the country.

# Quotes

- "You have to set the example."
- "But we can't let that become the standard."
- "Rather than heaping praise on somebody who does the bare minimum, we should criticize those who don't meet standards."
- "If we stopped to clap every time Joe Biden displayed more leadership than the president, we wouldn't get anything done in this country."
- "What Joe Biden is doing is standard."

# Oneliner

Beau addresses the importance of not settling for the bare minimum in leadership and the need to criticize those who fall short of meeting standards to cultivate real leaders.

# Audience

Voters, political observers

# On-the-ground actions from transcript

- Criticize those who fall short of meeting standards (implied)
- Advocate for real leaders in office by holding candidates accountable to higher standards (implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on leadership expectations and standards, urging for a shift towards recognizing and fostering real leadership qualities rather than settling for the bare minimum.

# Tags

#JoeBiden #Leadership #Standards #BareMinimum #Criticism