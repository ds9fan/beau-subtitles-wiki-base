# Bits

Beau says:

- Exploring the historical context of how the current situation unfolded, referencing past events like Bacon's Rebellion.
- Describing the catalyst of the current situation as involving a petty crime, "some pig," and an extrajudicial killing.
- Outlining the progression from petitions and outrage to protests, rioting, and looting, eventually leading to an open rebellion.
- Contrasting the common myth that Bacon's Rebellion was a forerunner to the American Revolution with the reality that it was not about independence from the king.
- Emphasizing that despite the genuine beliefs of those involved, nothing changed after Bacon's Rebellion.
- Warning against romanticizing rebellion and pointing out the potential consequences of a violent uprising in the modern world.
- Cautioning that hostile nations could exploit internal conflict in the US to weaken the country with devastating effects.
- Urging reflection on the mythological versus real-life implications of rebellion and the need for critical thinking about such actions.
- Stating that rebellion does not happen in isolation and could attract external interference, especially in a country with significant military power.
- Encouraging thoughtfulness and consideration of the broader implications before advocating for or engaging in rebellion.

# Quotes

- "A situation that nobody agrees on right now. It's disputed as to what happened. A petty crime, some pig, and an extrajudicial killing. That's how we wound up here."
- "The image of the glorious rebellion is myth. It's mythology. It's not real life."
- "Because this stuff doesn't occur in a vacuum. It's going to just sit idly by while a nuclear power, one of the strongest militaries in the world, rips itself apart."

# Oneliner

Beau delves into historical parallels to caution against romanticizing rebellion and the potential dire consequences of internal conflict in a powerful nation.

# Audience

History enthusiasts, critical thinkers

# On-the-ground actions from transcript

- Challenge romanticized views of rebellion (implied)
- Promote critical thinking before advocating for drastic actions (implied)
- Encourage thoughtful reflection on historical context and potential consequences (implied)

# Whats missing in summary

The full transcript provides a deep dive into historical events to underscore the dangers of romanticizing rebellion and the potential catastrophic outcomes of internal conflict in a powerful nation.

# Tags

#History #Rebellion #Consequences #CriticalThinking