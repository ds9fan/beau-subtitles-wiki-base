# Bits

Beau says:

- Ruth Bader Ginsburg has died, and the President will likely push through a Supreme Court nominee close to the election.
- President Trump believes the nominee will be loyal to him in any decisions regarding the election.
- Beau recalls Senator McConnell's statements in 2016 advocating for the American people to have a voice in the selection of the next Supreme Court justice.
- McConnell's role in pushing through a nominee is emphasized, noting the potential impact on closely contested Senate races.
- Beau raises concerns about a potential nominee on Trump's list who advocated for American soldiers to shoot unarmed citizens.
- Republican senators may face backlash from voters if they support a nominee tied to Trump's failures.
- Beau questions the wisdom of Republican senators closely associating themselves with Trump so close to the election.
- Democrats may only need a few Republican senators to halt the nomination process before the election.
- Beau urges smart Republicans to take a moral stance and let the American people decide on the Supreme Court vacancy.
- Democrats have obstructionist tactics at their disposal, but Beau hopes Republicans will act ethically without needing such tactics.

# Quotes

- "The American people should have a voice in the selection of their next Supreme Court justice."
- "I'm not sure that this close to the election, tying yourselves to such a failure of a president is a good idea if you're a Republican."
- "Which Republicans are going to be in that race to be the first ones to say they're not going to do it?"
- "Because, man, that's a moral stance."
- "And with all of the behavior the Republican Party has been associated with lately, taking a moral stance right before the election, that might be a good move."

# Oneliner

Beau warns Republican senators against hastily confirming a Supreme Court nominee tied to Trump's failures, urging them to take a moral stance and let the American people decide.

# Audience

Senators

# On-the-ground actions from transcript

- Hold off on confirming a Supreme Court nominee before the election (implied).
- Smart Republicans should take a moral stance and prioritize the voice of the American people in selecting the next Supreme Court justice (exemplified).

# Whats missing in summary

The full transcript provides a detailed analysis of the implications of rushing a Supreme Court nominee before the election, urging Republican senators to prioritize ethical decision-making.

# Tags

#SupremeCourt #Senate #Ethics #Election #Republican #Democratic