# Bits

Beau says:

- President of the United States won't be fact checked during upcoming debates.
- Lack of fact-checking gives President Trump a free pass to lie during the debate.
- It's embarrassing that fact-checkers won't be present at the presidential debates.
- The debate will likely be filled with lies, as Trump is known for not telling the truth.
- Beau quotes Octavia Butler about choosing leaders wisely.
- Debates and polls are deemed worthless; only actual voting matters in the end.
- Democrats should not be complacent based on poll numbers; what matters is voter turnout.
- The focus is on the importance of people showing up to vote rather than relying on polls.
- Beau questions the value of debates if candidates won't be held accountable for their statements.
- The emphasis is on the significance of action through voting rather than passive reliance on polls or promises.

# Quotes

- "President of the United States will get up there and say whatever he feels like."
- "It's embarrassing that we need fact-checkers at the presidential debates."
- "These debates are worthless. They're as worthless as his promises."
- "Those polls do not matter. They mean absolutely nothing."
- "Who shows up the day of? Who votes?"

# Oneliner

President won't be fact-checked, debates lack accountability, polls and promises are worthless - voting is what truly matters.

# Audience

Voters

# On-the-ground actions from transcript

- Show up and vote on election day (implied)
- Avoid complacency based on poll numbers; prioritize active voting participation (implied)

# Whats missing in summary

The full transcript provides a critical perspective on the lack of fact-checking in debates, the importance of voter turnout over polls, and the need for accountability in political leadership.

# Tags

#PresidentialDebates #FactChecking #VoterTurnout #PoliticalLeadership #Accountability