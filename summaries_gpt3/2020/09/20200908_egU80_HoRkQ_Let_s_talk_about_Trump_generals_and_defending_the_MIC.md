# Bits

Beau says:
- President Donald J. Trump's comment stirred mixed reactions regarding the military and defense contractors.
- The term "military industrial complex" was mentioned, pointing out a longstanding issue.
- The misconception lies in associating the complex with generals and not defense contractors.
- Generals, lacking monetary power, do not drive policy; it's influenced by defense contractors.
- These contractors use profits to sway policy by lobbying Congress and the executive branch.
- Policy decisions are ultimately made by Congress and the President, not generals.
- The system perpetuates itself as voters fail to hold accountable those involved in the complex.
- The real issue is the undue influence of campaign contributions on policy-making.
- Beau challenges people to take responsibility by voting out those perpetuating the complex.
- The root of the problem lies with Congress, the President, and the Secretary of Defense, not the military itself.

# Quotes

- "It's these campaign contributions. It's the undue influence that Eisenhower talked about. That's the problem, not a standing army."
- "The offenders aren't on the E-ring. They're on Capitol Hill."
- "The real issue is the undue influence of campaign contributions on policy-making."
- "The root of the problem lies with Congress, the President, and the Secretary of Defense, not the military itself."
- "Americans are more than happy to buy this fiction."

# Oneliner

President Trump's comments on the military-industrial complex expose a deeper issue: undue influence from defense contractors, urging voters to take action by holding accountable those in Congress and the executive branch.

# Audience

Voters, Activists

# On-the-ground actions from transcript

- Vote out officials perpetuating the military-industrial complex (implied)
- Hold accountable Congress and the President through active participation in elections (implied)

# Whats missing in summary

Beau's passionate call for accountability and action beyond mere awareness.

# Tags

#MilitaryIndustrialComplex #Accountability #PolicyChange #Voting #UndueInfluence