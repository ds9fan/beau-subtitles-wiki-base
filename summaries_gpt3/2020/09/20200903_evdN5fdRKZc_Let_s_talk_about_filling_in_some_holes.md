# Bits

Beau says:

- Acknowledges feedback about holes in previous video examples.
- Views local government restrictions as an asset for community networks.
- Emphasizes changing societal mindset to effect real change.
- Advocates for focusing on actions not prohibited by local ordinances first.
- Suggests garnering public support before tackling controversial issues.
- Encourages using media attention to challenge unjust regulations.
- Questions the logic of prohibiting community improvement.
- Proposes government facilitation instead of control over community initiatives.
- Considers government employees as public servants, not rulers.
- Advises choosing battles wisely to achieve meaningful victories.
- Stresses the importance of swaying public opinion over direct confrontation.
- Advocates for creating parallel systems to support communities where the government fails.
- Announces plans for a workbook to further these ideas.

# Quotes

- "Your local government has made it illegal to better your community. That seems a little silly."
- "You're creating a parallel one. You're creating something that people can turn to when the government fails as it typically does."
- "You want that secondary safety net for your community."

# Oneliner

Beau views local government restrictions as assets, advocates for changing mindsets, and building community support to challenge unjust regulations and create parallel systems for community safety nets.

# Audience

Community members

# On-the-ground actions from transcript

- Reach out to local press to support community improvement initiatives (implied)
- Advocate for government facilitation rather than control over community projects (implied)
- Begin planning and organizing community improvement projects with public support (implied)

# Whats missing in summary

The full transcript provides detailed insights and examples on navigating local government restrictions, building community support, and advocating for positive change in society, complementing the summarized points. 

# Tags

#CommunityEmpowerment #GovernmentRelations #CommunitySupport #SocialChange #Advocacy