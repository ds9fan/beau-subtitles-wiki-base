# Bits

Beau says:

- Declaration on funding and American identity discussed due to news from Mitch McConnell's Kentucky.
- Absence of progress, forward movement, reform, or justice apparent.
- Social media calls for defunding Louisville prompt Beau's reflection on American values.
- Beau questions the notion of defunding police being un-American.
- Compares current policing issues to grievances in the Declaration of Independence.
- No taxation without representation principle linked to police accountability.
- Defunding the police viewed as a patriotic act rooted in American history.
- Cutting police funding can prompt necessary priorities and accountability.
- Defunding specific problem departments is advocated for improved accountability.
- Politicians opposing defunding reveal their priorities lie elsewhere than with the people.

# Quotes

- "Defunding the police is as American as it gets."
- "No taxation without accountability."
- "Defunding the police is a just call, and it would be effective."
- "They don't get money if we can't hold them accountable."
- "It's literally one of the most American things you can say."

# Oneliner

Beau questions the American identity in relation to defunding the police, drawing parallels to the Declaration of Independence and advocating for police accountability.

# Audience

American citizens

# On-the-ground actions from transcript

- Advocate for defunding problem departments for improved police accountability (implied).
- Prioritize holding law enforcement agencies accountable in your community (implied).

# Whats missing in summary

Importance of linking historical American principles to contemporary issues for accountability and justice.

# Tags

#DefundThePolice #PoliceAccountability #AmericanValues #DeclarationOfIndependence #CommunityPolicing