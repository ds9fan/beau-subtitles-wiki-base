# Bits

Beau says:

- Exploring if the United States is behaving as a failed state due to erosion of legitimate authority to make collective decisions.
- Government's failure to obtain consent of the governed through polarizing society and not basing support on ideas and policies.
- Society's belief in enforcing laws rather than just writing them down on paper.
- Loss of control on the legitimate use of physical force by the government.
- Society's view on excessive force by law enforcement and its impact on legitimacy.
- Inability of the government to provide public services like clean water and healthcare.
- The United States' violations of international law and isolationist tendencies.
- The importance of individual action in shaping society's beliefs and laws.
- Encouraging individuals to be active and involved at the community level.
- The government's focus on self-benefit and division of society for their advantage.

# Quotes

- "Society's beliefs help shape the law."
- "It's up to you as an individual to help change this."
- "The national level has become too corrupt."
- "The United States is failing as it benefits those in power temporarily."
- "People just out for themselves and failing in their obligations as stewards of the state."

# Oneliner

Is the United States becoming a failed state due to erosion of legitimate authority, loss of control on physical force, failure in public services, and violations of international law?

# Audience

Activists, concerned citizens

# On-the-ground actions from transcript

- Be active and involved in your community to shape society's beliefs (implied)
- Advocate for policies based on ideas rather than parties (implied)
- Support initiatives that aim to provide public services (implied)

# Whats missing in summary

The full transcript delves deeper into the characteristics of a failed state and the importance of individual action in societal change.