# Bits

Beau says:

- Advises the Democratic Party on how they are mishandling information damaging to Trump's campaign regarding his tax documents.
- Trump's base views him as an establishment outsider and may actually see it as a good thing if he paid very little in taxes.
- Points out that Trump's base is skilled at "double think," believing contradictory ideas simultaneously.
- Emphasizes that the focus should be on how Trump's financial losses and inability to profit during a strong economy undermine his image as a successful businessman.
- Suggests that Trump's core supporters care only about themselves and have blind faith in him despite any lies or misrepresentations.
- Argues that the Democratic Party should frame the narrative around Trump's financial failures rather than just his tax payments to weaken his hold on supporters.

# Quotes

- "They care only about themselves. They've put their hope in this man based on what he told them. And it was a lie."
- "If you don't think Trump will sell out the American citizens as he has done everybody, you're more under his spell than most."
- "He couldn't even turn a profit in the middle of one of the best economies we've ever had."
- "The idea that he somehow scammed the government, it's not going to fly with them."
- "You're talking about selfish people. That's his base."

# Oneliner

Beau advises the Democratic Party to focus on Trump's financial failures rather than his tax payments to weaken his hold on supporters who believe he can do no wrong.

# Audience

Democratic Party strategists

# On-the-ground actions from transcript

- Reframe the narrative around Trump's financial failures and inability to profit during a strong economy to undermine his image (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of how Trump's base views him and suggests a strategic approach for the Democratic Party to counter his appeal.

# Tags

#DemocraticParty #Trump #TaxDocuments #PoliticalStrategy #UndermineSupport