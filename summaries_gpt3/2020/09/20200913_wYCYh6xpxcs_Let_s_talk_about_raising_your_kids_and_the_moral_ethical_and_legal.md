# Bits

Beau says:

- Advises on discussing philosophy and politics with children to avoid creating ideological followers.
- Suggests discussing ideas and policies instead of focusing on individuals or parties.
- Encourages humanizing historical figures to prevent blind hero-worship and cult of personality.
- Differentiates between legal, ethical, and moral values to guide children in decision-making.
- Uses examples like slavery in the 1850s to illustrate the distinctions between legal, ethical, and moral standards.
- Emphasizes the importance of teaching children to develop their own values based on critical thinking.
- Warns against sensationalizing historical events and emotions when discussing sensitive topics.
- Urges to avoid personalizing historical figures or events and focus on analyzing ideas and policies.
- Stresses the significance of helping children think independently and critically about moral, ethical, and legal dilemmas.
- Recommends focusing on policy, ideas, and values rather than individuals or rigid ideologies to cultivate independent thinking.

# Quotes

- "Teach them history, not mythology."
- "Legal, ethical, and moral. These three things aren't the same."
- "Morals are what shapes ethics and ethics are what shapes the law."
- "Talk about policy and ideas. Talk about moral, ethical, and legal."
- "You don't want to create little ideological foot soldiers."

# Oneliner

Beau advises discussing ideas and policies, humanizing historical figures, and teaching children to classify values as legal, ethical, and moral to foster critical thinking.

# Audience

Parents, educators, caregivers

# On-the-ground actions from transcript

- Teach children about historical events factually and without sensationalizing (implied).
- Encourage children to think critically about values and decisions based on legal, ethical, and moral considerations (implied).
- Engage children in discussing policies and ideas rather than focusing on individuals or parties (implied).

# Whats missing in summary

Importance of guiding children to think independently and critically about complex issues.

# Tags

#Parenting #Values #CriticalThinking #Education #Children