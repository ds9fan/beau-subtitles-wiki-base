# Bits

Beau says:

- Explains the historical revisions of the Pledge of Allegiance from 1892 to the current version in 1954.
- Talks about the importance of being concise in writing as a journalist.
- Emphasizes the need to eliminate irrelevant details to convey the main idea effectively.
- Suggests that focusing on the core values of liberty and justice for all is more impactful than pledging allegiance to a symbol.
- Advocates for a revised pledge that simply states "I pledge allegiance to liberty and justice for all" to represent the true essence of the idea.
- Points out that liberty and justice should be universal principles, transcending borders and applying to everyone.
- Encourages prioritizing ideas over symbols for a better world.
- Concludes with a reflection on the potential for positive change if people prioritize values over symbols.

# Quotes

- "I pledge allegiance to liberty and justice for all."
- "Not just does it better embody things, not just is it more concise, it's universal."
- "Gives us something to actually fight for, something worth fighting for."

# Oneliner

Beau suggests refocusing the Pledge of Allegiance on universal values of liberty and justice for all, transcending borders and symbols.

# Audience

Journalists, educators, activists

# On-the-ground actions from transcript

- Advocate for universal values of liberty and justice in your community (suggested)
- Prioritize fighting for equitable rights for all individuals (exemplified)

# Whats missing in summary

Importance of prioritizing core values over symbols for societal progress.

# Tags

#PledgeOfAllegiance #LibertyAndJustice #UniversalValues #Journalism #Activism