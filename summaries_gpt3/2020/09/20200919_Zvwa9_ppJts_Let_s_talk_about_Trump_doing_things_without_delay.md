# Bits

Beau says:

- President of the United States pushing for a quick appointment without delay and Senate is willing to oblige.
- President has a history of hiring individuals, praising them, then firing and criticizing them publicly.
- Beau questions the lack of thorough vetting for a lifetime appointment to the Supreme Court.
- Criticizes Senate for potentially forsaking their constitutional obligation by rushing the appointment.
- Calls out Senate Republicans for prioritizing power consolidation over the well-being of the people.
- Suggests that Senate is too eager to please the President at the expense of upholding their duties as a separate branch of government.
- Points out the hypocrisy of Senate's swift action for power-related matters compared to issues affecting the general population.
- Implies that Senate Republicans' actions are noticed and may influence public opinion and decision-making.

# Quotes

- "You're willing to undermine the Constitution and do whatever that man in the Oval Office says as long as you can get his crumbs."
- "When it comes to helping out the little guy, you don't seem to care very much."
- "Y'all are incredibly active. But when it comes to helping out the little guy, you don't seem to care very much."
- "Don't think we don't notice that when it comes to shoring up your power, all of a sudden, y'all are ready to go."
- "It's crystal clear. You're willing to undermine the Constitution and do whatever that man in the Oval Office says as long as you can get his crumbs."

# Oneliner

President rushes Supreme Court appointment without proper vetting, Senate complies, prioritizing power over duty to the people.

# Audience

Activists, concerned citizens

# On-the-ground actions from transcript

- Hold elected officials accountable for their actions and decisions (implied)
- Educate others about the importance of a balanced government and the responsibilities of each branch (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of how the current administration and Senate are prioritizing political gains over constitutional duties, urging viewers to pay attention and take action.

# Tags

#SupremeCourt #Senate #Constitution #PowerPrioritization #Accountability