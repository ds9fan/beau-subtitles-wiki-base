# Bits

Beau says:

- The United States Constitution acknowledges imperfection and the need for change from the very first line of the document.
- The Constitution enshrines mechanisms for change, including the right to petition for a redress of grievances and freedoms like speech, press, and assembly.
- Elected officials are not as responsive as they should be, regardless of party affiliation.
- The Republican Party is noted for attacking the machinery for change and methods of voicing concerns, such as suppressing the vote and criminalizing assembly and speech.
- Republicans tend to ignore issues like climate, public health, the economy, and racism, pretending they don't exist when it suits them.
- Republicans demonstrate a lack of interest in listening to the people by attempting to suppress the vote and undermine the integrity of elections.
- Beau criticizes the Republican Party for abandoning their duty to represent the people and instead seeking to rule over them.
- He questions why a party claiming to be for the working class and looking out for the little guy wouldn't want more people to vote.
- Beau argues that by suppressing the vote, Republicans show they are not interested in having the consent of the governed and are focused on ruling rather than representing.
- The duty of the government is to listen to the people, but Beau suggests the Republican Party has strayed from this principle.

# Quotes

- "You are always doing it wrong. There's never a right way to do it."
- "They attack that machinery so they don't have to listen to the people."
- "We're not going to listen to you. We're not going to give you a voice in your representative democracy."
- "Your rights don't matter."
- "Rather than protecting the machinery for change and encouraging its use, they're holding a lighter to the Constitution."

# Oneliner

The United States Constitution acknowledges imperfection and enshrines mechanisms for change, but Beau criticizes the Republican Party for attacking the machinery for change and undermining the people's voice.

# Audience

Voters, activists, concerned citizens

# On-the-ground actions from transcript

- Join organizations advocating for voting rights and fair elections (implied)
- Contact elected officials to express support for protecting mechanisms for change (implied)

# Whats missing in summary

Beau's passionate delivery and detailed analysis on Republican actions and their implications. 

# Tags

#Constitution #RepublicanParty #VotingRights #Government #Representation