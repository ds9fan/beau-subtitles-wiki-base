# Bits

Beau says:

- Talking about when people will be satisfied, when it'll be enough, and when it will stop.
- Exploring when those seeking change, progress, and justice will be satisfied.
- There are no new conservative ideas, but there may be new ways to package them.
- Conservatives typically defend old progressive ideas.
- The Republican Party, known as the party of Lincoln, was actually progressive.
- Lincoln received fan mail from Karl Marx, showing a connection between progressive ideas and conservatism.
- New ideas tend to come from the progressive side, while conservatives try to hold things back.
- When will people be satisfied? It doesn't end; there will always be new injustices.
- People asking when others will be satisfied are often wondering what the minimum acceptable change is.
- Beau suggests doing what you can, when you can, where you can, for as long as you can to make a difference.

# Quotes

- "When will those seeking change, progress, and justice be satisfied?"
- "There are no new conservative ideas."
- "New ideas come from the progressive side."
- "It doesn't end. Ever."
- "Do what you can, when you can, where you can, for as long as you can."

# Oneliner

Beau asks when those seeking change will be satisfied, explains the absence of new conservative ideas, and advocates for continuous efforts towards progress and justice.

# Audience

Activists, changemakers, advocates

# On-the-ground actions from transcript

- Do what you can, when you can, where you can, for as long as you can (suggested)

# Whats missing in summary

The full transcript provides a deeper understanding of the relationship between conservative and progressive ideas and encourages continuous efforts towards change and justice.

# Tags

#Change #Progress #Justice #ConservativeIdeas #ProgressiveIdeas