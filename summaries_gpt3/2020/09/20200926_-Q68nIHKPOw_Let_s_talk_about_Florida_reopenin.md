# Bits

Beau says:

- Florida is open for business with no restrictions, aiming to attract tourists despite the ongoing pandemic.
- The decision to reopen is primarily driven by the need for economic stability and attracting visitors for financial reasons.
- Politicians in Tallahassee have made decisions based on selective information to suit their agenda.
- Beau dismisses concerns about Florida's reputation and handling of the pandemic, suggesting that the state is already considered poorly managed in this regard.
- He mentions that the influx of tourists is seen as beneficial as they spend money and then return home, reducing the strain on Florida's healthcare system.
- Beau mentions the large older population in Florida, referring to it as "God's waiting room," and downplays concerns about their vulnerability.
- Capacity is expected to be an issue due to pent-up demand, but Beau assures that the crowded tourist areas are part of the appeal.
- Beau advises people not to trust politicians' statements from Florida and instead rely on medical experts for guidance.
- He urges visitors to make decisions based on expert advice rather than political influences.
- Beau expresses his willingness for a slower reopening if it ensures the safety and well-being of the people in Florida.

# Quotes

- "If you are from out of state, please, please, do not let anything a politician in Florida says ever influence any of your decisions about anything, ever."
- "Listen to the medical experts, listen to the people that know what they're doing, not the guy who didn't know how to put on his mask."
- "We don't need to become a nexus point for transmission."

# Oneliner

Florida opens for tourists without restrictions, prioritizing the economy over safety, urging visitors to rely on medical experts, not politicians.

# Audience

Travelers, concerned citizens

# On-the-ground actions from transcript

- Listen to medical experts for guidance on travel decisions (implied)
- Prioritize safety and well-being over economic interests when considering travel plans (implied)

# Whats missing in summary

The full transcript provides a detailed insight into the decision-making process behind Florida's reopening and the potential consequences for public health and safety. Viewing the entire transcript gives a comprehensive understanding of Beau's perspective on the situation.

# Tags

#Florida #Tourism #COVID19 #Economy #TravelSafety