# Bits

Beau says:

- Defines the role of a security consultant as being paid for what you can do on the days when your client has a bad day, needing help, advice, and leadership.
- Draws parallels between the role of a security consultant and that of the President of the United States, as someone who should be called upon during collective bad days.
- Points out that the President knew early on about asymptomatic transmission and should have advocated for masks but chose to downplay it.
- Mentions that the President was aware of the deadliness of the virus, comparing it to being five times as deadly as the most severe flu.
- Criticizes the President for failing in leadership, passing on information, and actively working against the American people during the pandemic.
- States that the President failed to act, which resulted in tens of thousands of needless deaths that could have been mitigated.
- Emphasizes that taking responsibility for the failure to act translates to accepting responsibility for the unnecessary loss of lives.
- Suggests that had the President acted differently, lives could have been saved, and the economy could have been better off.
- Condemns the President as unqualified for his role and unfit to handle future crises effectively.
- Expresses concern about the potential for more bad days in the future and does not want someone who failed at this level to be relied upon.

# Quotes

- "He did nothing."
- "We could have mitigated this."
- "He never should have been in that role."

# Oneliner

Beau points out President Trump's failure to act during the pandemic, resulting in needless deaths and economic repercussions, questioning his suitability for future crises.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact elected officials to demand accountability and action (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the President's handling of the pandemic, raising concerns about leadership, accountability, and future crisis management.

# Tags

#Leadership #PandemicResponse #Accountability #PublicHealth #CrisisManagement