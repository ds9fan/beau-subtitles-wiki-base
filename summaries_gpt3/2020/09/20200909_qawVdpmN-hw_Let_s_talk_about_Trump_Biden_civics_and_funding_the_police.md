# Bits

Beau says:

- Addresses President Trump's repeated claim that Joe Biden will defund the police, leading to chaos.
- Breaks down the funding of police departments, showcasing various grant programs and assistance.
- Mentions the COPS program with a $400 million budget, Edward Byrne Memorial Justice Assistance Grant program with $264 million, and DHS Preparedness Grant program with a budget of $450 million.
- Explains the Department of Defense's program 1033, which provides old military equipment to law enforcement.
- Calculates the total funding from these sources to be $2.6 billion, enough to run the NYPD for three months.
- Points out that federal funding is a small percentage of what state and local officials spend on law enforcement.
- Concludes that whether intentionally misleading or lacking understanding, President Trump's emphasis on defunding police by Biden is not a significant concern for voters.

# Quotes

- "Either President Trump just does not understand basic civics or he is intentionally misleading people to scare them."
- "Joe Biden defunding the police should not be a concern. It's not a thing."
- "At the local level, somehow fund your local police department. The feds don't foot that bill."

# Oneliner

President Trump's claims about Joe Biden defunding police lack merit; federal funding is a small portion of what local departments rely on.

# Audience

Voters, Civics Learners

# On-the-ground actions from transcript

- Contact local officials to understand how your community funds its police department (implied)

# Whats missing in summary

In-depth analysis of the potential impact of misleading political claims on public perception and voter decisions.

# Tags

#Politics #Civics #PoliceFunding #PresidentialElection #CommunityPolicing