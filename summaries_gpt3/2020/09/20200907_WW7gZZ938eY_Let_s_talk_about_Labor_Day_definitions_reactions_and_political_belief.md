# Bits

Beau says:

- Acknowledges Labor Day and urges viewers to put in labor for the day.
- Addresses the narrow scope of acceptable political beliefs in the United States.
- Points out the emotional reactions and lack of understanding towards socialism, communism, and fascism among Americans.
- Explains how fear of authoritarianism is the common thread that opposes these isms.
- Talks about how propaganda during the Cold War shaped American views on political beliefs.
- Suggests that most viewers of his channel can define political terms when stripped of emotional connotations.
- Mentions how people in the southern United States tend to lean towards socialism when ideologies are presented without labels.
- Encourages viewers to take a political compass quiz and answer questions based on ideal society rather than the current broken system.
- Emphasizes the importance of understanding one's true ideology beyond the narrow scope of acceptable beliefs in the US.
- Connects the exploration of political beliefs on Labor Day to celebrating American organized labor and their vision for a better world.

# Quotes

- "Your ideology shouldn't be based on the best you can do in a system that most people agree is broke."
- "Labor Day is the celebration of American organized labor. People who had a vision of a better world and worked to get it."

# Oneliner

Beau addresses the narrow scope of political beliefs in the US, encourages viewers to understand their true ideology beyond fear-driven reactions, and connects exploring political beliefs on Labor Day to celebrating organized labor's vision for a better world.

# Audience

Viewers

# On-the-ground actions from transcript

- Take a political compass quiz answering questions based on an ideal society, not the status quo (suggested).
- Understand and define political terms beyond emotional reactions (implied).

# Whats missing in summary

The full transcript provides a deep dive into political ideologies and encourages viewers to critically analyze their beliefs beyond the narrow scope imposed by society.