# Bits

Beau says:

- Explains the difference between the House of Representatives and the Senate in the US government.
- Points out the constitutional obligation of the Senate to be impartial and fair.
- Emphasizes that the Senate failed to uphold the Constitution during impeachment.
- Calls out those who claim the Senate didn't remove the guilty party, stating the Constitution mandates removal.
- Notes a growing trend of people not familiar with the Constitution, despite having "We the People" in their profile pictures.
- Stresses that the Senate and the House of Representatives are designed to be different, with unique roles and responsibilities.
- Argues that those justifying the Senate's actions are essentially admitting they haven't read the Constitution.

# Quotes

- "The Senate had a constitutional obligation to be impartial. They betrayed that obligation."
- "The relevant passage says, shall be removed, not can be, not could be if you want to, not could be if it won't hurt your reelection chances, shall be."
- "The Senate failed to uphold the Constitution. Period. Full stop."
- "The Houses, the House of Representatives, and the Senate, they're not the same. They're designed to be different."
- "Anytime you say it, all you're doing is telling everybody around you that you've never read the Constitution."

# Oneliner

Beau explains the constitutional obligation for Senate impartiality, condemning its failure during impeachment, and clarifies the distinct roles of the Senate and House of Representatives.

# Audience

Political enthusiasts

# On-the-ground actions from transcript

- Read and familiarize yourself with the Constitution (suggested)
- Advocate for accountability in government actions (implied)

# Whats missing in summary

Explanation on the importance of upholding constitutional obligations and promoting governmental accountability. 

# Tags

#Constitution #Senate #HouseOfRepresentatives #Impeachment #Government