# Bits

Beau says:

- Remind people of the stakes amidst the constant influx of news to refocus on what we're fighting for rather than against.
- Points out the history of a New York billionaire who has minimized civil rights, ignored income inequality, advanced surveillance and police state, and scapegoated minorities.
- Raises the question of whether the billionaire in question is Trump or Bloomberg, showcasing the danger of personifying what we're fighting against.
- Suggests a military analogy where allowing opposing forces to weaken each other could be a strategic move before targeting the ultimate opposition.
- Advocates for letting Sunday, Sunday, Sunday, the Battle of the Billionaires play out to potentially weaken the larger opposition force.
- Considers the possibility of Bloomberg winning and the implications of him securing the Democratic nomination.
- Expresses concern over voters' inability to see through propaganda and focus on principles rather than personalities.
- Warns against blindly following the "vote blue no matter who" mantra and the dangers it poses in allowing bad actors to secure nominations.
- Proposes a long-term strategy of allowing Bloomberg to weaken Trump before dealing with him in the primaries.
- Emphasizes the importance of remembering what is being fought for and prioritizing principles over personalities.

# Quotes

- "We have to think of things in the affirmative."
- "Principle over personality."
- "We have to think of things in the affirmative."
- "If we personify the evil that exists in one person, we become very easy to manipulate."

# Oneliner

Remind people of the stakes, focus on fighting for principles over personalities, and strategize wisely amidst political battles.

# Audience

Voters, Political Activists

# On-the-ground actions from transcript

- Strategize and prioritize focusing on principles over personalities in political decision-making (implied).
- Stay informed about political candidates' histories and priorities to make informed voting choices (implied).

# Whats missing in summary

The full transcript provides a nuanced perspective on the danger of personifying political opponents and the importance of strategic thinking in political movements.

# Tags

#Stakes #PoliticalActivism #PrinciplesOverPersonalities #StrategicThinking #VotingChoices