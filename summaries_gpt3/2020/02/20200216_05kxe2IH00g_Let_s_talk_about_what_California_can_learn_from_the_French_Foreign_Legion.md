# Bits

Beau says:

- Comparing the state of California to the French Foreign Legion, suggesting lessons to be learned.
- The Foreign Legion is known for giving individuals a new identity and a fresh start after leaving.
- Despite some members having criminal backgrounds, they put their lives on the line and are changed by their experiences.
- Mentioning Dien Bien Phu as a significant battle that marked the end of French involvement in Vietnam.
- Advocating for California's AB 2147 bill, which aims to create a California firefighter legion for inmates who are trained as firefighters.
- Emphasizing the benefits of allowing these inmates to clear their records and become firefighters, reducing recidivism.
- Expressing belief in the positive impact of providing second chances through skill-building opportunities.
- Stressing the importance of implementing this legislation as a way to help both the community and the state.
- Acknowledging past legislative challenges in California but underlining the potential of this bill to be a significant benefit.
- Concluding with a call to action for California not to miss out on this beneficial legislation.

# Quotes

- "Imagine being able to cut down recidivism."
- "If you can trust an inmate with an axe, probably not a danger."
- "There's no downside to this."
- "This is something that's helping the community and it's helping the state while it's helping them."
- "This seems like a no-brainer."

# Oneliner

Beau suggests California learn from the Foreign Legion and implement AB 2147 to offer inmates a second chance through firefighting, reducing recidivism and benefiting the community and state.

# Audience

California policymakers, activists

# On-the-ground actions from transcript

- Support the implementation of AB 2147 for the creation of a California firefighter legion (suggested).
- Advocate for legislation that provides second chances and skill-building opportunities for inmates (exemplified).

# Whats missing in summary

The full transcript provides more context on the history and impact of the French Foreign Legion, as well as Beau's personal reflections on the importance of second chances and community benefits.