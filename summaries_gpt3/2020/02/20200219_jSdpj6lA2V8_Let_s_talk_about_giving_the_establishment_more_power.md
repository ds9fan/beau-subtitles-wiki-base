# Bits

Beau says:

- Criticizes an op-ed suggesting giving elites more control in choosing the president.
- Establishment faces difficulty with preferred candidates and wants more influence.
- Proposes ranked choice voting combined with non-binding exit polls for elites.
- Calls out this proposal as oligarchy rather than democracy.
- Advocates for direct democracy where popular vote determines primary candidates.
- Suggests a process where candidates collect signatures, debate, and face elimination rounds.
- Emphasizes the need for year-round involvement to avoid last-minute choices and poor leaders.
- Stresses the importance of removing power from party elites and involving the rank and file.
- Believes in including third-party candidates with a million signatures to remove party affiliation.
- Concludes by urging for active citizenship and caution against letting elites dictate decisions.

# Quotes

- "That's oligarchy. That's not democracy."
- "Democracy is advanced citizenship."
- "Letting the elites do your thinking for you, that's how we wound up here."

# Oneliner

Beau criticizes proposals for elite control, advocates for direct democracy, and stresses active citizen involvement to avoid oligarchy.

# Audience

Voters, democracy advocates

# On-the-ground actions from transcript

- Collect a million signatures for presidential candidacy in January (suggested)
- Be actively involved year-round in political processes (implied)

# Whats missing in summary

Detailed breakdown of the proposed primary reform process

# Tags

#Democracy #PrimaryReform #DirectDemocracy #ActiveCitizenship #Oligarchy