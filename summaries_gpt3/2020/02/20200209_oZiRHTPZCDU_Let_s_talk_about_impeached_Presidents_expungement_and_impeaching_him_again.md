# Bits

Beau says:

- Explains the ongoing questions about the impeached president and the possibility of expunging his impeachment.
- Clarifies that the president will always be considered impeached regardless of acquittal, citing historical examples.
- Points out that expunging the impeachment from the House record may backfire and further the perception of a cover-up.
- Emphasizes that expunging the impeachment won't erase it from history but merely add a footnote.
- Affirms that the House can impeach the president again for any reason, even trivial ones like wearing a tan suit.
- Notes that the House has sole power over impeachment and can proceed without alleging a specific crime.
- Criticizes senators who may not have been impartial during the impeachment proceedings.
- Encourages people to read the Constitution to understand the impeachment process better.

# Quotes

- "He will always be an impeached president forever. That never that's never going to change."
- "The House can impeach for anything. The House has sole power over impeachment."
- "All of this is very, very simple if you actually read the Constitution."

# Oneliner

Beau explains the permanence of impeachment, potential futility of expunging, and the House's power to impeach for any reason.

# Audience

Constitutional enthusiasts

# On-the-ground actions from transcript

- Read and understand the Constitution (exemplified)

# Whats missing in summary

The full transcript provides a detailed explanation of impeachment, history, and the constitutional process for those seeking clarity.

# Tags

#Impeachment #Constitution #HousePower #History #Patriotism