# Bits

Beau says:

- Beau is conflicted about the media's oversimplification of the Stone case.
- Federal sentencing is based on a point system that determines the guideline range in months.
- Roger Stone's guidelines suggest seven to nine years due to enhancements, which Beau finds excessive.
- Beau believes lengthy incarceration for nonviolent offenders is unjust, regardless of his opinion of the person.
- The administration's attempted intervention in Stone's sentencing process is unethical, if not illegal.
- Beau questions why the president didn't attempt to change the guidelines if he found the sentence unfair.
- He believes the real issue is the need to amend federal sentencing guidelines for nonviolent crimes.
- Beau criticizes the existence of disproportionately long sentences for victimless crimes compared to violent crimes.
- He advocates for using the Stone case to address broader issues and push for necessary changes in sentencing guidelines.

# Quotes

- "Nine years for lying to Congress and telling other people to lie to Congress is unjust."
- "It creates two sets of laws. Those laws for people who are politically connected and those for everyone else."
- "The sentencing guidelines need to be amended, and that's what this should show."
- "I don't think it's right to cheerlead for a nine-year sentence for a non-violent crime."
- "This can be used to highlight a lot and hopefully fix a lot."

# Oneliner

Beau is conflicted over the oversimplification of the Stone case and argues against excessive sentences for nonviolent crimes, calling for amending federal sentencing guidelines.

# Audience

Advocates for Criminal Justice Reform

# On-the-ground actions from transcript

- Advocate for the amendment of federal sentencing guidelines to address disproportionate sentences (suggested)
- Push for changes in the criminal justice system to ensure fairness and equity (implied)

# Whats missing in summary

Deep dive into the implications of unfair sentencing and the need for systemic reform.

# Tags

#FederalSentencing #RogerStone #CriminalJusticeReform #UnjustSentences #Advocacy