# Bits

Beau says:

- American patriotism has become muddled in the United States, often confused with another term.
- American patriots in 1776, such as those at Yorktown, Lexington, and Concord, had no loyalty to the United States, as it didn't exist until 1787.
- The loyalty of American patriots was to the people, the countryside, and the principles, not to politicians or governance bodies.
- American patriots throughout history understood that governments are a creation of the people and sometimes need correction.
- Patriotism is not blindly obeying the government; it's about holding onto principles and correcting the system when needed.
- Nationalism, not patriotism, is blindly following government orders and giving up rights.
- The government's appeal to patriotism to gain control is akin to a child wanting cake for dinner - it's a manipulation tactic.
- Individuals hold the power in a democracy, and the government appeals to patriotism because they need the people.

# Quotes

- "Patriotism is not blindly obeying the government; it's about holding onto principles and correcting the system when needed."
- "Nationalism, not patriotism, is blindly following government orders and giving up rights."
- "The government's appeal to patriotism to gain control is akin to a child wanting cake for dinner - it's a manipulation tactic."

# Oneliner

American patriotism is about loyalty to people and principles, not blind obedience to the government; nationalism is the opposite.

# Audience

American citizens

# On-the-ground actions from transcript

- Correct the system when needed (implied)
- Uphold principles over blind obedience (implied)
- Understand your power in a democracy (implied)

# Whats missing in summary

The full transcript provides a deep dive into the historical origins of American patriotism and the importance of loyalty to principles over blind obedience to government.

# Tags

#AmericanPatriotism #Government #Nationalism #Principles #Democracy