# Bits

Beau says:

- Talks about the demographic shift in the United States and addresses the fear associated with becoming a minority in one's own country.
- Criticizes the inherently racist sentiment behind not wanting to be a minority in America.
- Points out that the concern about becoming a minority showcases systemic racism in the country.
- Advocates for treating everyone equally and fairly rather than resisting demographic changes.
- Addresses the argument of wanting everybody to speak English and questions its significance.
- Mentions the gradual demographic shift over decades and the evolution of languages spoken in the country.
- Emphasizes that resistance to change is often rooted in the history of using differences to marginalize others.
- Encourages letting go of fears and biases to build a better society and world.
- Expresses openness to diversity and multilingualism, envisioning a future where differences fade away.

# Quotes

- "It is inherent racism."
- "Try to treat everybody equally. Try to treat people fairly."
- "There's nothing to fear."
- "We will eventually all look like Brazilians and what a great day that will be."
- "I'm totally cool with pressing one for English."

# Oneliner

Beau addresses the fear of becoming a minority in America, calling out the inherent racism and advocating for equality and acceptance of demographic shifts towards a diverse future.

# Audience

Americans

# On-the-ground actions from transcript

- Embrace diversity and treat everyone equally (implied)
- Be open to different languages and cultures (implied)
- Advocate for inclusivity and fairness in your community (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of systemic racism and the importance of embracing diversity for a better future.

# Tags

#DemographicShift #Racism #Equality #Diversity #Inclusion