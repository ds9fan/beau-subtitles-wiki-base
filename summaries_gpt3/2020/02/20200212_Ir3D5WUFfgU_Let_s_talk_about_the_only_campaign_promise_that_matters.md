# Bits

Beau says:

- Addresses the importance of a specific campaign promise from candidates.
- Acknowledges President Trump's impact on exposing the disproportionate power of the executive branch.
- Emphasizes the danger of unchecked executive power.
- Stresses the need to restore balance among the branches of government.
- Argues that without this restoration, other campaign promises are essentially meaningless.
- Advocates for returning executive powers to the legislative and judicial branches.
- Calls for a president who prioritizes restoring normalcy over wielding power.
- Points out the risks of concentrating power in the executive branch.
- Questions the efficacy of pursuing policy agendas without addressing the imbalance of power.
- Urges for a president with the integrity to relinquish excessive power.

# Quotes

- "He's shown that the Constitution is not being upheld."
- "That Constitution, it's a piece of paper unless it's adhered to. It doesn't matter. It means nothing."
- "We need a president who has the integrity to give that power up."

# Oneliner

The campaign promise we need: restore balance of power to save democracy and make other promises matter. 

# Audience

Voters

# On-the-ground actions from transcript

- Demand accountability from candidates to restore balance of power (implied).
- Advocate for checks and balances within the government (exemplified).
- Support candidates committed to relinquishing excessive executive power (suggested).

# Whats missing in summary

The full transcript delves into the critical need for presidential candidates to prioritize restoring the balance of power among government branches to safeguard democracy effectively. Viewing the entire speech provides a comprehensive understanding of the urgency for this pivotal campaign promise in ensuring a functional and accountable government.

# Tags

#BalanceOfPower #Democracy #ExecutiveBranch #CampaignPromise #ChecksAndBalances