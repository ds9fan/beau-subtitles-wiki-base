# Bits

Beau says:

- Beau starts off by addressing the audience on Valentine's Day, mentioning the various ways people celebrate the day.
- The videos throughout the week had themes centered around tradition, radical ideas, and questioning the importance of holding on to traditions.
- Valentine's Day is rooted in tradition, with people exchanging chocolates without really knowing the full history behind the day.
- The popular theory behind Valentine's Day involves a priest named Valentine who defied Emperor Claudius II by marrying soldiers in secret, leading to his beheading and subsequent canonization as a saint.
- The romantic aspect of Valentine's Day didn't emerge until Shakespearean times, and the industrial revolution saw Hallmark commercializing the day with cards.
- February 14th was chosen by the church to coincide with a pagan festival involving music, love, nudity, and rituals like drawing names for temporary romantic partners.
- Beau questions the continuation of certain traditions, suggesting that some may be outdated or have lost their original meaning.
- He stresses the importance of examining and reevaluating the traditions and mythology prevalent in the U.S., particularly in times of social upheaval.
- Despite the discomfort that may arise from challenging established traditions, Beau believes it is necessary for societal renewal and progress.
- Beau warns that without proactive choices and planning from the people, those in power can easily introduce new traditions or uphold the status quo.

# Quotes

- "We don't question it. we've just always done it this way, so we don't change."
- "When you examine traditions sometimes, you realize that they may not be worth continuing."
- "Now is the time to examine those traditions and that mythology."

# Oneliner

Beau questions the origins of Valentine's Day, delves into its history, and urges reevaluation of traditions in times of social change.

# Audience

Curious individuals

# On-the-ground actions from transcript

- Examine and question traditions (suggested)
- Reassess cultural myths and practices (suggested)
- Plan for societal renewal (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of Valentine's Day traditions, shedding light on their historical roots and urging critical examination of established practices.

# Tags

#ValentinesDay #Traditions #CulturalHistory #SocialChange #Mythology