# Bits

Beau says:

- Exploring the interconnectedness of historical figures like the Rosenbergs, Roy Kahn, Joe McCarthy, Roger Stone, and Donald Trump.
- The Rosenbergs were highly effective spies for the Soviets in the 20th century, recruiting other spies and providing high-value information.
- Roy Kahn, one of the prosecutors in the Rosenberg case, was known for his hard-right stance and association with Joe McCarthy during the Red Scare.
- Kahn also worked with Roger Stone on Reagan's campaign and later represented Donald Trump in New York.
- Treason in the United States is narrowly defined, making it difficult to charge individuals with this offense.
- The Rosenbergs were not charged with treason but with conspiracy to commit espionage, carrying severe penalties.
- In the U.S., treason requires allegiance to the United States and an act of war or support for a war effort.
- Treason could involve a scenario where a person in public office aids a foreign government's act of war against the U.S.
- The Pentagon considers cyber attacks as potential acts of war, raising concerns about today's security landscape.

# Quotes

- "Treason in the United States is extremely specific."
- "Foreigners, people who do not owe allegiance to the United States, can't be charged with treason."
- "Acts of war don't happen that often."
- "The Pentagon has argued that cyber attacks are an act of war."
- "It's worth noting that the Pentagon has argued that cyber attacks are an act of war."

# Oneliner

Beau delves into the historical connections between spies, prosecutors, and politicians, shedding light on the narrow definition of treason in the United States and the implications of cyber attacks as potential acts of war.

# Audience

History enthusiasts

# On-the-ground actions from transcript

- Research historical events and figures mentioned (suggested)
- Stay informed about cybersecurity issues and potential threats (suggested)

# Whats missing in summary

Insights on the long-term impact of espionage and political connections in shaping historical narratives. 

# Tags

#Espionage #Treason #HistoricalConnections #Cybersecurity #PublicOffice