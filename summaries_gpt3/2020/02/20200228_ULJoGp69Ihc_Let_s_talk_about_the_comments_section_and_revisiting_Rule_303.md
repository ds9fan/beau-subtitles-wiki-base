# Bits

Beau says:

- Unwritten rule of internet journalism: never read the comment section under your work to avoid negativity and criticism.
- Shared a heartwarming experience of a viewer transcribing the entire video for a hearing-impaired person.
- Explained Rule 303, stating that if you have the means, you have the responsibility to act.
- Encourages helping others, especially during tough times in the US due to lack of solid leadership.
- Emphasizes the importance of individuals and communities stepping up to help those in need.
- Acknowledges the potential burnout from helping others and advises to do what you can sustainably.
- Calls for collective action and support during challenging times when leadership is lacking.

# Quotes

- "If you have the means at hand, you have the responsibility to act."
- "It's up to us as individuals. It's up to us as communities."
- "You do what you can when you can for as long as you can."
- "There's probably somebody out there that could use your help."
- "If you see somebody that needs help, you probably should. If you can."

# Oneliner

Beau shares the Rule 303: If you have the means, you have the responsibility to act, encouraging individuals and communities to help those in need, especially during challenging times in the absence of solid leadership.

# Audience

Online citizens

# On-the-ground actions from transcript

- Help those in need in your community by offering support and assistance (exemplified).
- Step up to assist individuals facing challenges during tough times (exemplified).

# Whats missing in summary

The full transcript conveys the importance of stepping up to help others and take responsibility, particularly during times of need and lack of leadership.

# Tags

#InternetJournalism #Rule303 #Responsibility #CommunitySupport #Leadership