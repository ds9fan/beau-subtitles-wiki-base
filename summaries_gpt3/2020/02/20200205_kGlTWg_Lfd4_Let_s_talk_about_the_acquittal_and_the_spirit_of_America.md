# Bits

Beau says:

- Criticizes the current administration for deviating from the spirit of the United States.
- Believes that the President will be acquitted due to partisanship overriding conscience.
- Blames senators for enabling the President's actions and violating the country's spirit.
- Mentions the Human Rights Watch report on El Salvador, linking Senate decisions to deaths there.
- Expresses concern over the focus on Trump rather than the system that enables him.
- Warns against the Democratic Party's reluctance for substantial change in candidates.
- Stresses the importance of running a candidate focused on real change to prevent Trump's reelection.
- Questions the significance of voting blue if policies remain unchanged.
- Emphasizes the need for genuine change to uphold the country's ideals and promises.
- Condemns sending people back to dangerous situations and the performative nature of American values.

# Quotes

- "Party over country."
- "We are not the land of the free or the home of the brave, because we deny freedom out of cowardice."
- "We're losing the plot and we're losing the war because this is a war for the very soul of this country."
- "We need real change not just a change of personality."
- "It's just going to be an act, completely performative, meaning nothing."

# Oneliner

The current administration, Senate decisions, and political system are critiqued for deviating from the spirit of the United States, risking lives and losing the country's ideals.

# Audience

American citizens

# On-the-ground actions from transcript

- Advocate for candidates focused on real change in elections (implied)
- Support policies that prioritize substantial change and system alteration (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the current political landscape in the United States, urging for significant change to uphold the country's ideals and prevent further deterioration.

# Tags

#UnitedStates #Politics #Change #DemocraticParty #HumanRights