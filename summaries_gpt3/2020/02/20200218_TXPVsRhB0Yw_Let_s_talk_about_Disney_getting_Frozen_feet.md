# Bits

Beau says:

- Beau watches both Disney Frozen films back to back with his daughter, noticing the lack of closure in the second film.
- He points out that Disney always includes subtext in their storytelling, like in Peter Pan, and meticulously plans everything as an organization.
- Beau describes the plot of the first Frozen movie, focusing on the two princess sisters, Elsa and Anna, and the secrets and conflicts between them.
- Elsa, the older sister, has powers to control ice and snow but also harbors another secret hinted at in the films.
- In the second film, Elsa hears a mysterious voice calling her out into the world, uncovering family secrets along the way.
- Despite various hints and subtext throughout both films, Beau notes that Elsa's true nature is never overtly acknowledged.
- He expresses his frustration at the lack of acknowledgment of Elsa's potential queerness and the importance of representation for young audiences.
- Beau suggests that Disney, as a powerful entity, could be a force for social change by portraying Elsa's difference more explicitly in future films.

# Quotes

- "I mean, come on. But it's never acknowledged. And it bothers me."
- "The fact is Disney is a massive machine. Disney could be an engine for social change."
- "Because the fact is Disney is a massive machine. Disney could be an engine for social change."

# Oneliner

Beau watches Frozen films with his daughter, pointing out Disney's subtle subtext and the unacknowledged queer potential of Elsa, urging for more explicit representation in future films.

# Audience

Disney Fans, LGBTQ+ Advocates

# On-the-ground actions from transcript

- Advocate for more diverse and inclusive representation in media through petitions and letters to production companies (implied)

# Whats missing in summary

Beau's emotional investment in seeing Elsa's potential queerness acknowledged in Disney's storytelling.

# Tags

#Disney #Frozen #Representation #Queer #Subtext #SocialChange