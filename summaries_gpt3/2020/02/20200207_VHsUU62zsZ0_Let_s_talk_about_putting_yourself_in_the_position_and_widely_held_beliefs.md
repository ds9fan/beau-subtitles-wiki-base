# Bits

Beau says:

- Exploring widely held beliefs that are wrong and the consequences they create.
- Referencing a lawyer in a high-profile case who claimed to have never been assaulted due to not putting herself in vulnerable positions.
- Disputing the notion that staying away from unknown people can prevent assault.
- Citing statistics showing that a significant number of assaults are committed by partners, family members, or other known individuals.
- Pointing out the fallacy of victim-blaming based on being drunk or with someone unknown.
- Revealing that a substantial percentage of assault incidents go unreported for years or not at all.
- Criticizing the rhetoric that shifts blame onto victims and protects powerful perpetrators.
- Drawing attention to the prevalence of sexual assault and the prioritization of success over addressing victimization.
- Contrasting the public's concern over coronavirus with the underreporting of sexual assault due to power dynamics.
- Condemning the victim-blaming culture perpetuated by high-profile individuals and the media.
- Emphasizing the need to challenge and correct widely held but erroneous beliefs surrounding assault and victimization.

# Quotes

- "It's about power."
- "Lawyers and high-profile cases say stuff like this publicly because they know it's a widely held belief."
- "Widely held beliefs are often wrong."

# Oneliner

Beau addresses the prevalence of sexual assault, challenges victim-blaming rhetoric, and calls out widely held but erroneous beliefs that perpetuate harm and injustice.

# Audience

Advocates, activists, allies

# On-the-ground actions from transcript

- Challenge victim-blaming narratives publicly and within your social circles (implied)
- Support survivors of assault and believe their experiences (implied)
- Advocate for policies and cultural shifts that prioritize addressing victimization over protecting perpetrators (implied)

# Whats missing in summary

The full transcript provides a comprehensive examination of the damaging effects of victim-blaming, the prevalence of sexual assault, and the need to address power imbalances in combating injustice.

# Tags

#SexualAssault #VictimBlaming #PowerDynamics #Beliefs #Activism