# Bits

Beau says:

- Introduces the concept of the iceberg theory in storytelling, where the storyteller can omit details as long as they know it should be there, and the audience fills in the gaps.
- Mentions the theory of the death of the author, where once a story is public, only the audience's interpretation matters, not the author's intentions.
- Talks about the significance of subtext in storytelling, especially when there are topics that can't be openly discussed due to social norms, censorship, or the climate of the time.
- Describes the context of 1984 in American history, marked by trade issues, returning soldiers, homeless vets, crack, AIDS, and high crime rates in New York City.
- Recounts the story of Bernie Getz, who shot four teens on a subway, sparking public debate on whether he was a hero or a psycho.
- Explains how gaps in information lead people to fill in the blanks with their own biases and preconceived notions, often creating false narratives.
- Warns about the dangers of filling in gaps with misinformation and how it can distort one's beliefs.
- Draws parallels between the events of 1984 and the present day, urging caution in interpreting information and avoiding self-delusion.
- Encourages critical thinking and awareness of the narratives we construct based on incomplete information.
- Ends with a reminder to be mindful of how we interpret and fill in gaps in information to avoid falling into self-deception.

# Quotes

- "Subtext and those omissions, they can be great. They can be very powerful for illustrating the truth."
- "People don't look at little bits of data and try to figure it out. They grasp for what they can find that will fill in the blanks to create a story they like."

# Oneliner

Beau introduces the iceberg theory in storytelling, warns of the dangers of filling gaps with misinformation, and urges critical thinking amid incomplete information.

# Audience

Story Consumers

# On-the-ground actions from transcript

- Question narratives and seek out multiple perspectives to avoid filling gaps with biased information (suggested).
- Encourage critical thinking and fact-checking when forming opinions based on incomplete information (implied).

# Whats missing in summary

The full transcript delves into the power of storytelling through subtext, the impact of societal contexts on narratives, and the need for critical analysis in interpreting information.

# Tags

#Storytelling #CriticalThinking #Subtext #Misinformation #Bias #Narratives