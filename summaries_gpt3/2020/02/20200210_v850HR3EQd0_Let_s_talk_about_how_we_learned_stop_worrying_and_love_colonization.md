# Bits

Beau says:

- The United States, historically anti-colonial, has become a colonial power.
- Colonialism involves extracting wealth, imposing cultural similarities, and using military force.
- Today, corporations plant logos instead of flags, still backed by military force.
- American belief in the right to colonize leads to shock when facing resistance.
- Support for troops should mean keeping them out of harm's way, not as fodder for profit.
- Despite protests, the American people embrace and love the idea of colonialism.
- American empire expansion mirrors past colonial entities' behaviors.
- Territorial disputes on Earth could lead to fear and hostility towards "aliens" among us.
- Beau suggests meeting extraterrestrial beings to learn, as we have much to discover.
- The expanding empire contradicts the principles of the American founders.

# Quotes

- "We have become exactly what the founders opposed."
- "The only alien I'm scared of are the ones up there."

# Oneliner

The United States, once anti-colonial, has transformed into a colonial power, backed by military force, extracting wealth and imposing cultural similarities, leading to acceptance and love for colonialism among the American people.

# Audience

Global citizens

# On-the-ground actions from transcript

- Question colonial practices and beliefs (suggested)
- Advocate for keeping troops out of harm's way (implied)
- Foster understanding and acceptance of diversity (suggested)

# Whats missing in summary

The full transcript delves into the paradox of America's history as an anti-colonial entity turning into a colonial power, urging reflection on the consequences and responsibilities of such actions.

# Tags

#Colonialism #AmericanEmpire #MilitaryForce #CulturalImperialism #AntiColonialism