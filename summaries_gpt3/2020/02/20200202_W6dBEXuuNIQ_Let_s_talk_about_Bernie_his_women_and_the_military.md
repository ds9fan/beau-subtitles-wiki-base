# Bits

Beau says:

- Bernie is receiving support from unlikely places, including active duty military.
- Bernie leads all candidates, including Trump, in donations from active duty military by 50%.
- This shift surprises many as the military has traditionally supported the Republican party.
- The abandonment of the Kurds by Trump had a significant impact on military support for him and the Republicans.
- Many active duty military members who were Trump supporters turned away after the abandonment of the Kurds.
- Military personnel work closely with the Kurds, and leaving them behind did not sit well with many.
- Another reason for military support of Bernie is related to systemic issues like income inequality.
- People join the military for better opportunities and benefits, such as universal healthcare and housing.
- Seeing the benefits in Bernie's platform makes military personnel question why these are not available to all Americans.
- Bernie's appeal to active duty military lies in his platform advocating for universal healthcare and other social benefits.

# Quotes

- "Bernie leads all candidates to include Trump in donations from active duty military."
- "Leaving them twisting in the wind, leaving a man behind, so to speak, did not sit well with them."
- "A lot of people join the military today because of systemic issues, income inequality."
- "It's a really bad mindset these women did not get where they are by you know worrying about the fact that some man can think better than their little baby lady brains."
- "If you have a problem with what they say, I suggest you take it up."

# Oneliner

Bernie garners support from unexpected quarters like active duty military due to Trump's actions, systemic issues, and appeal of his platform; criticism of Bernie's surrogates should not be gender-based.

# Audience

Democratic voters, Bernie supporters, Military personnel

# On-the-ground actions from transcript

- Address systemic issues like income inequality in your community (implied)
- Advocate for universal healthcare and social benefits for all (implied)

# Whats missing in summary

The full transcript provides more in-depth insights into the reasons behind the military's support for Bernie and the importance of addressing systemic issues like income inequality and healthcare access.

# Tags

#BernieSanders #MilitarySupport #SystemicIssues #IncomeInequality #DemocraticParty