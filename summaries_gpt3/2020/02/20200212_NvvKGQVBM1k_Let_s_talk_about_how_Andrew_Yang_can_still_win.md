# Bits

Beau says:

- Talking about Andrew Yang's success despite ending his run for the Democratic nomination.
- Andrew Yang presented himself as good at math and knew he was a long shot for winning.
- Yang saw himself as a messenger candidate, aiming to introduce new ideas like universal basic income.
- Hundreds of thousands of people are now familiar with the concept of universal basic income because of Yang.
- Yang's campaign succeeded in getting people to acknowledge the flaws in the current system and the need for systemic change.
- Beau views Yang's campaign as a success in introducing radical ideas, even if he didn't win the nomination.
- Questions arise for Yang's supporters about what to do next after his campaign ends.
- Beau encourages Yang's supporters not to give up on the idea of systemic change just because Yang is no longer running.
- Yang may have intended to create a movement for systemic change rather than solely aiming for the nomination.
- Beau stresses the importance of continuing to push for change even without a specific political leader to rally behind.

# Quotes

- "You don't need a political candidate to fall behind and follow everything they do and support no matter what."
- "If there's anything you can learn from that campaign, it's that radical ideas and new thought can come from anywhere."
- "You don't need a leader. You've got the thought. I run with it."
- "But for that percentage of his yang gang that looked a little deeper into it and got into the fact that this is something that has to happen, not necessarily UBI but some form of systemic change, you now have the means."
- "You can't stop. You can't become politically inactive simply because your candidate didn't get the nomination."

# Oneliner

Beau reminds Andrew Yang's supporters that systemic change doesn't depend on one leader and encourages them to continue pushing for change even after Yang's campaign ends.

# Audience

Yang supporters

# On-the-ground actions from transcript

- Continue advocating for systemic change (implied)
- Embrace radical ideas and push for new thought (implied)
- Stay politically active and engaged in driving change (implied)

# Whats missing in summary

The full transcript provides a detailed reflection on Andrew Yang's impact in introducing radical ideas and the need for systemic change, encouraging supporters to carry on advocating for change beyond his campaign.

# Tags

#AndrewYang #SystemicChange #RadicalIdeas #PoliticalActivism #Leadership