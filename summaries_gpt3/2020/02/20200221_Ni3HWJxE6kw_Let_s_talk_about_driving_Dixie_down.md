# Bits

Beau says:

- Explains the history of the Dixie Highway, a highway network in the United States running from Michigan to Miami, created in the early 1900s.
- Notes that Carl G. Fischer, the mind behind the highway, previously worked on the Lincoln Highway, suggesting that the choice of the name Dixie was not to glorify it.
- Mentions that the term Dixie refers to the region south of the Mason-Dixon line where slavery was legal.
- Talks about how the Dixie Highway did not gain the same tourism attention as other famous highways like Route 66.
- Acknowledges that historical events tied to Dixie, such as opposition to civil rights, make it a controversial symbol.
- Miami recently renamed its portion of the Dixie Highway to the Harriet Tubman Highway, which Beau praises as a positive step forward.
- Speculates on the economic and cultural benefits that could arise if other jurisdictions along the highway also choose to rename their sections.
- Suggests that renaming the highway could attract more tourists and revenue to rural communities along the route.
- Expresses curiosity about how different areas will respond to the renaming, seeing it as a test of whether they choose to honor a dark past or a heroic figure.
- Encourages moving forward by letting go of glorifying a period in American history associated with slavery and racism.

# Quotes

- "Miami has just elected to rename theirs, it will now be the Harriet Tubman Highway and I think that is just awesome on so many levels."
- "Economically, it makes sense. Morally, it makes sense. Historically, it makes sense."
- "There's no reason to keep this when it could be used to demonstrate that we do have a new South, that the country is moving forward."

# Oneliner

Beau explains the history of the Dixie Highway and advocates for renaming it to honor progress and reject a dark past.

# Audience

History enthusiasts, community activists

# On-the-ground actions from transcript

- Advocate for renaming controversial symbols in your community (exemplified)
- Support initiatives that celebrate diversity and progress in your area (exemplified)

# Whats missing in summary

The full transcript provides a detailed historical context and rationale for renaming the Dixie Highway, which may not be fully captured in this summary.

# Tags

#DixieHighway #History #Renaming #Progress #CommunityEngagement