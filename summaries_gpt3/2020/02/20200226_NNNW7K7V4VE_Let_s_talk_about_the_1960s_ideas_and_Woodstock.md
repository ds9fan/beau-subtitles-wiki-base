# Bits

Beau says:

- Woodstock was intended to be a money-making venture, not a free concert.
- The original Summer of Love happened in 1967 in San Francisco, not at Woodstock in 1969.
- Half a million people showed up at Woodstock when only 50,000 were expected, causing chaos.
- Woodstock faced logistical issues, leading to it becoming a free concert due to the overwhelming number of attendees.
- Jimi Hendrix, the headliner, played to a much smaller crowd as most people had already left by the time he performed.
- Woodstock organizers didn't make a profit until the 1980s from residual income, facing financial losses initially.
- Woodstock is iconic not because of free love or revolution but because it gave rise to punk rock.
- The event showcased ideas of the '60s that couldn't be packaged and sold, surviving through people walking through the gates without buying tickets.
- Despite the lack of visible security or real authority, Woodstock remained peaceful and became a cultural phenomenon, proving the validity of its ideas.
- Woodstock was the last great victory of a movement that couldn't be marketed, remembered for its essence of love and community.

# Quotes

- "Woodstock was intended to be a money-making venture, not a free concert."
- "Woodstock is iconic not because of free love or revolution but because it gave rise to punk rock."
- "The event showcased ideas of the '60s that couldn't be packaged and sold, surviving through people walking through the gates without buying tickets."

# Oneliner

Woodstock's chaotic journey from a commercial venture to a cultural phenomenon, proving the validity of '60s ideas through unity and music.

# Audience

History enthusiasts, music lovers

# On-the-ground actions from transcript

- Attend or organize community music events to foster unity and share ideas (suggested)
- Host gatherings where people can share music, ideas, and food in a peaceful, inclusive setting (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of how Woodstock, initially planned as a profit-making venture, transformed into a cultural phenomenon despite facing logistical challenges and financial losses, ultimately symbolizing unity and love within a chaotic yet peaceful setting.

# Tags

#Woodstock #1960s #Music #Culture #Community #Unity