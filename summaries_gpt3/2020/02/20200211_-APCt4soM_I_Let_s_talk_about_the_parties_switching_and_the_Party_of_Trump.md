# Bits

Beau says:

- Delving into the transformation of the Republican Party from Lincoln to Trump.
- Exploring the historical context of the party switch.
- Using a timeline approach to understand the shift.
- Noting the change in electoral votes between 1960 and 1964 due to the Civil Rights Movement.
- Detailing the primary candidates in 1964 to show the party stances on segregation.
- Pointing out Goldwater's role in voting against the Civil Rights Act in 1964.
- Addressing accusations of intentionally altering the platform to attract racist voters.
- Referencing Strom Thurman's Dixiecrat movement in 1948 as a pivotal moment.
- Connecting the shift in the Republican Party to campaign strategies targeting the racist vote.
- Encouraging individuals to prioritize being a good person over blind party loyalty.

# Quotes

- "It's real clear, it's history, it's not something you can really debate."
- "Maybe it's time for people to stop worrying about their party and start worrying more about being a good person."
- "Their policies and the way they try to move those policies forward is racist. There's no big mystery here."

# Oneliner

Exploring the Republican Party's transformation from Lincoln to Trump through historical context, campaign strategies, and societal perceptions of racism.

# Audience

History enthusiasts, Political analysts, Voters

# On-the-ground actions from transcript

- Reassess party loyalty and prioritize personal values (implied)

# Whats missing in summary

A deeper dive into the impacts of historical shifts on current political landscapes.

# Tags

#RepublicanParty #History #CivilRights #CampaignStrategies #Racism #PartyLoyalty