# Bits

Beau says:

- Talks about G.I. Joe and Cobra, and how toy designers reused vehicle molds for new storylines.
- Compares this toy designer tactic with the President's decision to reverse a policy on using mines.
- Explains the policy on non-persistent mines that degrade or stop working after a set number of times.
- Questions the relevance of using mines in modern warfare scenarios.
- Expresses concerns about the dangers and risks associated with using mines that can be repurposed by opposition forces.
- Criticizes the decision to lift the ban on mines, labeling it as a horrible idea.
- Points out the indiscriminate nature of mines and their potential harm to civilians and US forces.
- Mentions the lack of necessity for mines in modern warfare and suggests there are better tools available.
- Speculates on the possible reasons behind reversing the mine policy, including personal gain or political reasons.
- Condemns the decision and criticizes the Pentagon for not understanding the risks associated with mines.

# Quotes

- "This is a horrible idea."
- "They're unnecessary. They're obsolete."
- "To supply the opposition. To endanger civilians."
- "DOD has made a lot of dumb moves over the years."
- "The brass at the Pentagon apparently does not understand something that every E2 in the world knows and that even toy designers know."

# Oneliner

Toy designers teach reusing molds, but President's mine reversal poses dangers; unnecessary, obsolete, and risks harm to civilians and US forces.

# Audience

Advocates for peace and disarmament

# On-the-ground actions from transcript

- Advocate for peace and disarmament by raising awareness in your community (implied).
- Support organizations working towards banning the use of landmines (suggested).
- Contact lawmakers to express opposition to the use of mines in warfare (suggested).

# Whats missing in summary

The full transcript provides detailed insights into the risks and consequences of using mines in modern warfare, urging for peaceful and safer alternatives.

# Tags

#Disarmament #Landmines #ModernWarfare #PeaceAdvocacy #PoliticalDecisions