# Bits

Beau says:

- Exploring legends as windows into past beliefs and fears.
- The legend surrounding U.S. coins: Roosevelt dime and Kennedy half dollar.
- Red Scare paranoia in the 1960s linked to coded messages on coins.
- Debunking the socialist infiltration myth associated with the coins.
- Differentiating between social Democrats and socialists in modern U.S. politics.
- Correlation between American Republicans and Irish socialists.
- Warning against drawing shallow conclusions without deeper investigation.
- Robert Lincoln's presence at events of three assassinated presidents.
- Americans falling for repeated myths due to romantic nationalism.
- Historical trend of the world moving towards the left.
- Viewing past historical figures like Washington and Adams as far-left liberals.

# Quotes

- "They are a link to the past."
- "That habit of never scratching and getting below the surface of how something appears is why Americans in general, but specifically the right wing, fall for the same stuff over and over again."
- "So if you look at it from a long enough timeline, you may see things that appear to be infiltration. But the reality is, it's just a legend."
- "Those terms weren't used at the time, but that's what they were."
- "It's just the course of human events."

# Oneliner

Exploring legends on U.S. coins, debunking socialist infiltration myths, and understanding historical political shifts.

# Audience

History enthusiasts, political analysts.

# On-the-ground actions from transcript

- Challenge myths and misinformation in political discourse (implied).
- Encourage deeper investigation into historical and political narratives (implied).
  
# Whats missing in summary

Deeper insights into historical figures' political ideologies and the impact of myths on modern political beliefs.

# Tags

#Legends #Myths #Socialism #PoliticalShifts #History