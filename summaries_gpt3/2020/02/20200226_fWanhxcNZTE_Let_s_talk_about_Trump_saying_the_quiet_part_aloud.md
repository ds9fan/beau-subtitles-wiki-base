# Bits

Beau says:

- Explains the concept of the "quiet part" that remains unsaid in normal and political discourse to avoid discomfort.
- Points out how the President of the United States has a habit of saying the quiet part aloud at inappropriate times.
- Mentions the recent instance in New Delhi where the President openly talked about US military involvement in Syria being primarily about oil.
- Criticizes the act of using military force to extract wealth from a nation as a form of colonialism or pillaging, which goes against international law.
- Expresses concern over the President's statements indicating military alliances with Russia, Iran, Iraq, and Syria against non-state actors in the Middle East.
- Raises alarm about the potential long-lasting repercussions of such alliances and the implications for US dominance in the region.
- Condemns the President's actions that may lead to dividing countries in the region along oil interests rather than ideological lines.
- Stresses the importance of acknowledging Russian interference, especially when the President advocates for actions that benefit Russia.
- Urges for attention to be paid to these concerning developments and the potential consequences of such foreign policy decisions.

# Quotes

- "It's called pillaging, not just for pirates, and it is against international law."
- "He's pushing the most powerful non-state actor into Russia's arms."
- "It's dividing them, creating an oil curtain rather than an iron curtain."

# Oneliner

Beau Gyan explains how the President's habit of saying the unsaid aloud, especially regarding military involvement and alliances, raises concerns about potential repercussions and Russian interference.

# Audience

Foreign policy analysts

# On-the-ground actions from transcript

- Contact policymakers to express concerns about potential military alliances that could undermine US interests (implied)
- Stay informed about international developments and advocate for transparent foreign policy decisions (implied)

# Whats missing in summary

Insight into the broader context of US foreign policy decisions and their impact on global alliances and stability.

# Tags

#ForeignPolicy #USPresident #RussianInterference #MilitaryAlliances #GlobalStability