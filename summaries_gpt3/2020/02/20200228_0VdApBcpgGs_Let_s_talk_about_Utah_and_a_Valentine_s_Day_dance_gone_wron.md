# Bits

Beau says:

- Incident at a Valentine's Day dance in Rich County, Utah, involving an 11-year-old girl named Aislinn.
- Two versions of events: Aislinn refused a dance, principal insisted she dance with the boy; school denies forcing anyone but encourages saying yes.
- Aislinn felt uncomfortable with the boy and outlined reasons to the press.
- Beau believes the boy is not to blame; it was on Aislinn to communicate her discomfort to the school.
- School's actions send wrong messages about entitlement and consent, leaving boys with misguided ideas.
- Beau criticizes the school's policy and the need for a policy review, insisting that the issue should just go away.
- Emphasizes the importance of consent and how it should be a fundamental concept taught in schools.

# Quotes

- "Consent is a very basic premise of society."
- "She said no. That's the end of the conversation."
- "There is no reason for a principal to try to persuade or suggest or ask that she say yes."
- "This is a really simple concept."
- "If you're not teaching that, what can you possibly be teaching at this school?"

# Oneliner

An 11-year-old girl's discomfort at a school dance sparks debate on consent and entitlement, prompting reflections on what schools should be teaching.

# Audience

School administrators, educators, parents.

# On-the-ground actions from transcript

- Contact the school to express concerns about their policy and handling of the situation (suggested).
- Advocate for better education on consent and respect in schools (implied).
- Initiate community dialogues on consent and healthy boundaries in education settings (implied).

# Whats missing in summary

The emotional impact on Aislinn and the broader implications on teaching consent effectively in schools.