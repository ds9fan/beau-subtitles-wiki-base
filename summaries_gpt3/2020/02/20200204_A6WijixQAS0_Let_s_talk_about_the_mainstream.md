# Bits

Beau says:

- Defines the mainstream as widely held beliefs and agreed-upon facts by the majority.
- Questions the purpose of possessing radical ideas outside the mainstream.
- Encourages mainstreaming radical ideas for broader acceptance.
- Suggests that shifting the mainstream narrative can lead to a better world.
- Points out the challenge of communicating radical ideas effectively to the general public.
- Advocates for making radical ideas accessible and palatable for wider acceptance.
- Emphasizes the importance of speaking a common language to convey ideas effectively.

# Quotes

- "If you hold a radical idea, it's because at least at some point really wanted everybody to believe that so you could have a better world."
- "Sometimes you've got to speak the language of the people that you're talking to."

# Oneliner

Beau explains the importance of mainstreaming radical ideas for a better world and the need to communicate them effectively to the general public.

# Audience

Activists, Ideological Warriors

# On-the-ground actions from transcript

- Make efforts to communicate radical ideas in accessible and understandable terms (implied).
- Work towards mainstreaming radical ideas by making them more palatable to a broader audience (implied).

# Whats missing in summary

The transcript addresses the importance of not just holding radical ideas but mainstreaming them for broader acceptance and impact. It also underscores the necessity of effective communication to shift widely held beliefs.

# Tags

#Mainstream #RadicalIdeas #Communication #Activism #Ideology