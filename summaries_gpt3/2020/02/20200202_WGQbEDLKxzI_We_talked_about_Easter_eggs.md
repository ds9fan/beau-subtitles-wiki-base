# Bits

Beau says:

- Tells the story of St. Augustine and Alexander the Great to illustrate moral ambiguity between a pirate and an emperor.
- Criticizes the equating of legality with morality, pointing out that using force to plunder is the same regardless of legality.
- Questions the lack of progress on Trump's promised border wall and the methods used to acquire land for it.
- Addresses pastors planning to instigate conflict at the Pulse Nightclub on its anniversary and criticizes their behavior.
- Comments on the president's ultimatum and threats, comparing it to actions in Nazi Germany.
- Mentions China quarantining a city and stresses the importance of waiting for demographic information on patients.
- Reports from the Mexican border about Trump declaring a national emergency over asylum seekers.
- Shares a story about refugees building a tunnel with the help of NBC News.
- References Kennedy's emotional reaction to refugees escaping to West Berlin through a tunnel.
- Analyzes the political situation and the lack of expected outcomes in the 2020 election.
- Draws a parallel with Sherlock Holmes' story about a silent dog to suggest hidden knowledge in current events.

# Quotes

- "It's the same thing."
- "We are in occupied territory."
- "The dog isn't barking."
- "He should have barked."
- "Our senatorial dog recognizes the guilty person."

# Oneliner

Beau questions morality, criticizes political actions, and draws parallels with historical events while reporting from various locations.

# Audience

Activists, Political Observers

# On-the-ground actions from transcript

- Contact local representatives to express concerns about immigration policies (implied).
- Support refugee assistance organizations or initiatives (exemplified).

# Whats missing in summary

Beau's insightful commentary on current events and historical parallels can be further explored by watching the full transcript. 

# Tags

#StAugustine #MoralAmbiguity #BorderWall #Refugees #PoliticalAnalysis