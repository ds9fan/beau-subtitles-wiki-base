# Bits

Beau says:

- Explains the background of Northern Ireland being part of the United Kingdom and the Republic of Ireland not being part of the UK.
- Describes the potential consequences of Brexit on the border between Northern Ireland and the Republic, especially the risk of a hard border with checkpoints.
- Talks about the concerns related to the romanticization of conflicts and the potential for violence due to triggering issues like a hard border.
- Mentions the request from his friends to rename a popular drink in the US called a "car bomb" to an "Irish Slammer" out of sensitivity towards the Irish community.
- Points out the historical significance of bars in supporting different sides of conflicts and the need to avoid unintentionally supporting violence.
- Compares the perceptions of conflicts in Ireland between the United States and Ireland, with the US having a more romanticized view.
- Emphasizes the importance of understanding nuance in conflicts that have deep historical roots and real impacts on people.
- Stresses that rhetoric, imaginary lines, and conflicts like Brexit should not be worth people dying over and militarizing borders.
- Urges the Irish American community in the US to be cautious in their actions and words regarding conflicts like those in Ireland.
- Suggests that allowing the people directly involved to decide on matters like borders can prevent unnecessary violence and conflicts.

# Quotes

- "It's sparked by a request from some of my friends."
- "There's a whole lot of nuance to it."
- "It's not worth people dying over."
- "If you're in the U.S. and you are a part of that Irish American community, unless you're going to go there and fight, shut up."
- "Believe me, I understand."

# Oneliner

Beau explains the nuances of conflicts like Brexit, Ireland, and romanticization, urging caution and understanding to prevent violence and support peace.

# Audience

Irish American community members

# On-the-ground actions from transcript

- Rename the drink "car bomb" to "Irish Slammer" to show sensitivity and respect towards the Irish community (suggested).
- Avoid unintentionally supporting conflicts by being cautious with actions and words (implied).

# Whats missing in summary

The full transcript provides a comprehensive understanding of the historical context, potential consequences, and nuances of conflicts like Brexit and Ireland, urging for sensitivity, understanding, and peace.

# Tags

#Brexit #Ireland #Conflict #IrishAmericanCommunity #Violence #Peace