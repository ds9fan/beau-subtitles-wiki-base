# Bits

Beau says:

- Recalls an incident at Ole Miss where 40 black students staged a demonstration in 1962 by sitting at different tables in the cafeteria, demanding minor requests.
- The establishment did not grant their requests, resulting in mass arrests.
- Beau underscores the importance of understanding the American Civil Rights Movement's strategic and philosophical lessons for those seeking deep systemic change in the present United States.
- Points out the need for long-term commitment and readiness for an enduring campaign for change.
- Mentions the necessity of movements being centered around ideas rather than individuals to avoid vulnerability.
- Emphasizes the vulnerability of movements that focus on personalities, as they can easily be dismantled through various means.
- Notes that movements built around personalities often crumble when the individual is targeted.
- Argues that movements need to be about ideas, policies, and deep systemic change to be successful.
- States that the American Civil Rights Movement's success stemmed from focusing on the idea and the dream rather than individuals.
- Encourages rallying people behind ideas for systemic change and justice rather than personalities.

# Quotes

- "It's got to be about the ideas."
- "If it's about anything else, you're setting yourself up for failure."
- "You make it about a person, you make it about an individual, some personality, you will lose."

# Oneliner

Beau stresses the importance of long-term commitment and focusing on ideas, not individuals, for successful movements seeking deep systemic change.

# Audience

Activists, Social Justice Advocates

# On-the-ground actions from transcript

- Rally people behind ideas for systemic change and justice (implied).

# Whats missing in summary

The full transcript provides a detailed historical context of a civil rights demonstration at Ole Miss, showcasing the importance of long-term commitment and movement centered around ideas for successful systemic change. Viewing the entire transcript offers a deeper understanding of the strategic lessons from the American Civil Rights Movement. 

# Tags

#CivilRights #SystemicChange #MovementBuilding #LongTermCommitment #Ideas