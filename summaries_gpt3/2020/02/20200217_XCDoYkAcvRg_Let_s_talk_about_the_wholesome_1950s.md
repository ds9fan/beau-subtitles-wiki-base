# Bits

Beau says:

- Talks about the myth of the wholesome 1950s and the desire to reach back to that era.
- Mentions how certain political slogans reference the 1950s as a time to "make America great" based on movies' portrayal.
- Points out the reality behind the wholesome image, including icons like Marilyn Monroe and themes in Broadway productions.
- Explains how the production code in movies enforced a certain image by prohibiting controversial topics.
- Argues that books provide a more accurate view of the 1950s, mentioning works like "Catcher in the Rye" and "On the Road."
- Emphasizes that the 1950s were not all as wholesome as portrayed, referencing global works like "Dr. Zhivago."
- Concludes that the nostalgia for the 1950s is based on a myth and a politically correct version of history.

# Quotes

- "When people say, make America great, and they're looking to the 1950s, they're looking back to a mythology, they're looking back to something that never existed."
- "The 1950s were not all leave it to Beaver."
- "It wasn't real. It was a censored, politically correct version of the times."

# Oneliner

Beau dismantles the myth of the wholesome 1950s, revealing it as a politically correct version of history that never truly existed.

# Audience

History enthusiasts, truth-seekers

# On-the-ground actions from transcript

- Read books from the 1950s era to gain a more accurate understanding of the time (suggested).
- Challenge nostalgic narratives about historical eras by seeking diverse perspectives (exemplified).

# Whats missing in summary

The full transcript provides a deep dive into the myth vs. reality of the wholesome 1950s, urging listeners to question nostalgic narratives and seek a more nuanced understanding of history.

# Tags

#1950s #Nostalgia #MythVsReality #History #Books