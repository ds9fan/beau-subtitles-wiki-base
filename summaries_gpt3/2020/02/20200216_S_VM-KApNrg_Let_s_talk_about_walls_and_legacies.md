# Bits

Beau says:

- August 13th, 1961: the Berlin Wall went up, seen as a symbol of oppression dividing a city, country, and world.
- The Berlin Wall separated West Berlin from East Germany, symbolizing oppression until it fell in 1989.
- Trump's wall on the southern border is questioned for its potential legacy like the Berlin Wall.
- The wall's potential legacy may not be as weighty due to hardened hearts and prioritization of money over morality.
- The effectiveness of Trump's wall is doubted as reports show various ways people easily breach it.
- People are actively choosing to go to the wall because it's easier to use it than to go around it, indicating its ineffectiveness.
- The wall being defeated even before completion raises concerns about its actual utility.
- Cutting through the wall with saws and scaling it with rebar-made ladders are some tactics used to breach it easily.
- The wall might be remembered not as a symbol of oppression but as a testament to incompetence and fiscal irresponsibility.
- Beau predicts Trump's wall will be associated with his legacy of wasteful spending and inefficiency.

# Quotes

- "The wall is not built yet. If you want a good gauge of how easily the wall is defeated, understand they're choosing to go to the wall."
- "I don't see this wall being recorded as a great symbol of oppression, I think it's going to be recorded as a massive symbol of incompetence."
- "It will be a symbol of massive fiscal irresponsibility."

# Oneliner

Beau questions the legacy of Trump's border wall, foreseeing it as a symbol of incompetence and fiscal irresponsibility rather than oppression.

# Audience

Border wall critics

# On-the-ground actions from transcript

- Contact local representatives to voice opposition to the construction of the border wall (suggested).
- Support organizations advocating for immigration reform and more effective border security measures (exemplified).

# Whats missing in summary

The full transcript provides more in-depth analysis on the potential legacy of Trump's border wall, touching on themes of oppression, morality, effectiveness, and wasteful spending.

# Tags

#BorderWall #Legacy #Incompetence #FiscalIrresponsibility #Oppression