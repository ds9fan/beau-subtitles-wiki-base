# Bits

Beau says:

- The country is shifting back from the right and we have a chance to create something better rather than just returning to normal.
- Questioning our beliefs is necessary, particularly regarding long prison sentences and their effectiveness in deterring crime.
- Criminal justice reform is imperative and requires new ideas and heavy implementation.
- When certain currently illegal substances become legal, the approach towards those involved must change to focus on treatment and community service for non-violent offenders.
- Planning for the future of criminal justice reform needs to start now, not when the momentum has subsided.
- Beau acknowledges provoking reactions to draw attention to the excessive sentences within the system.
- Fixing the failures in the criminal justice system should be a top priority in creating a just society.

# Quotes

- "We have to start thinking about this stuff now."
- "If we are going to create a just society, fixing the failures in our criminal justice system should be pretty near the top of the list."

# Oneliner

Beau urges us to question the status quo, push for criminal justice reform, and create a better future rather than settling for past norms.

# Audience

Advocates for justice reform.

# On-the-ground actions from transcript

- Start questioning beliefs on the effectiveness of long prison sentences now, suggested.
- Push for criminal justice reform by advocating for new ideas and heavy implementations, implied.
- Plan for a future where the failures of the criminal justice system are rectified, exemplified.

# Whats missing in summary

The emotional urgency and call to action that Beau conveys throughout his message.

# Tags

#JusticeReform #QuestionBeliefs #CriminalJustice #CreateChange #CommunityService