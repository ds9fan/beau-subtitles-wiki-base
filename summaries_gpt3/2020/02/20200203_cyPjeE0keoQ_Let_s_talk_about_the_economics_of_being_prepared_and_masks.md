# Bits

Beau says:

- Advocates for preparedness and calmness in the face of potential emergencies.
- Demonstrates the difference between N95 masks and surgical masks for protection.
- Points out the exploitation of fear leading to price gouging of masks.
- Emphasizes the importance of being prepared before emergencies occur.
- Mentions the affordability of preparedness supplies under normal circumstances.
- Recommends preparing for various emergencies, not just the current situation.
- Suggests getting masks and other essentials for pandemic preparedness.
- Urges not to strain emergency services by being unprepared.
- Recommends taking the current situation as a teachable moment for readiness.
- Mentions that alternative filters for masks are still widely available.
- Advises against mistaking surgical masks for the protection level of N95 masks.
- Expresses confidence in authorities handling the current situation effectively.

# Quotes

- "Don't panic."
- "It's a teachable moment to show that you should get prepared for when there is an emergency."
- "Under normal circumstances an N95 mask at any hardware store will cost you a dollar maybe two."
- "You're taking yourself out of the equation because you have what you need."
- "I don't think any of this is necessary."

# Oneliner

Beau advocates preparedness and calmness, differentiates mask types, warns against price exploitation, and urges readiness in a teachable moment.

# Audience

Prepared individuals

# On-the-ground actions from transcript

- Stock up on emergency supplies like masks, Purell, and essentials (suggested).
- Get N95 filters for masks used for painting and similar tasks (suggested).

# Whats missing in summary

The full transcript provides detailed insights on the importance of preparedness, mask distinctions, and staying calm during emergencies.

# Tags

#Preparedness #EmergencyPreparedness #MaskProtection #PriceGouging #CommunitySafety