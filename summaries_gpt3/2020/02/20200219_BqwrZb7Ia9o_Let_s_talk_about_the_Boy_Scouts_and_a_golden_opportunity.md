# Bits

Beau says:

- The Boy Scouts started with solid values of service to community, self-reliance, and doing your best.
- Over time, nostalgia took over, the organization lost focus, and it became politically controversial.
- Many former Scouts value the moral code instilled in them by the organization.
- Beau suggests remaking the Scouts into a modern American image with inclusive values and less nationalism.
- He proposes creating a decentralized form of scouting led by parents in the community, focusing on core values and flexibility.
- The core issue with the Boy Scouts was the overrun of nationalism and a paramilitary structure.
- Beau believes that an organization dedicated to creating strong young people is needed, focusing on internal growth over external achievements.

# Quotes

- "The core values need to be instilled."
- "It should be more about fostering internal growth than putting patches on a sash."
- "This is something that somebody watching this can do."

# Oneliner

Beau suggests remaking the Boy Scouts into a modern, inclusive organization focused on core values and internal growth, rather than external achievements.

# Audience

Parents, community members

# On-the-ground actions from transcript

- Create a decentralized form of scouting led by parents in the immediate community (implied)
- Incorporate STEM learning into scouting activities (implied)

# Whats missing in summary

The full transcript provides detailed insights into the history and challenges faced by the Boy Scouts, as well as a comprehensive vision for its transformation into a more inclusive and relevant organization.