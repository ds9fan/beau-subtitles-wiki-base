# Bits

Beau says:

- President Trump insulted a reporter who asked what he had to say to Americans who were afraid.
- Instead of inspiring the American people, Trump insulted the reporter, calling the question sensationalist.
- Fear is normal and can be a good motivator to take necessary precautions, but panic is the enemy.
- American exceptionalism could be used positively during this crisis, given the country's resources and diversity.
- The United States is being looked upon for leadership during this time, but it seems to be lacking.
- Many tough media personalities are having breakdowns on Twitter, but they do not represent what America truly stands for.
- People celebrate America for its working class and the ability to overcome hard times.
- The current situation does not require drastic action like storming beaches but rather staying calm and doing our part.
- Those providing essential services are conveying calm and helping everyone get through this crisis.
- The United States historically thrives on chaos and can lead itself through these challenging times.

# Quotes

- "Fear in some ways is good. Panic is the enemy."
- "We're not being asked to storm the beaches. We're being sent to go watch Netflix."
- "We are the United States. We thrive on chaos."

# Oneliner

President Trump missed an inspiring moment, but Americans can find strength in fear, avoid panic, and rely on their resilience during this crisis.

# Audience

Americans

# On-the-ground actions from transcript

- Remain calm and convey calm to others (exemplified)
- Find strength in fear to take necessary precautions (exemplified)
- Support those providing essential services (exemplified)

# Whats missing in summary

The full transcript provides additional context on American exceptionalism and the historical perspective of the United States in overcoming challenges.

# Tags

#Leadership #AmericanResilience #CalmAmidCrisis #CommunitySupport #USHistory