# Bits

Beau says:

- Talking about morals and ethics to do things voluntarily before they become mandatory.
- Urges people to think about society as a whole, not just the current situation.
- Mentions pushback on his statement that every sane person becomes a socialist during an emergency.
- Gives examples from movies where people cooperate in survival situations.
- References historical instances of rationing during the 1940s.
- Questions why some people are not following the advice to stay at home during the current crisis.
- Emphasizes the global nature of the emergency and the responsibility to humanity.
- Talks about the need for large changes in philosophical outlook post-crisis.
- Stresses the importance of global cooperation in facing future threats like climate change.
- Raises the choice between voluntary action now or mandatory action later due to survival necessity or government force.

# Quotes

- "This is a responsibility to humanity that we all share."
- "Those lines on a map are worthless."
- "The cooperation and mobilization that is taking place right now is what will be needed to combat climate change."

# Oneliner

Beau talks about acting voluntarily now to prevent mandatory actions later, stressing global cooperation in facing crises.

# Audience

Global citizens

# On-the-ground actions from transcript

- Stay at home voluntarily to prevent the need for mandatory lockdowns (exemplified)
- Cooperate with global efforts to combat climate change through voluntary actions (suggested)

# Whats missing in summary

Importance of global solidarity and proactive voluntary action in facing current and future crises.

# Tags

#Morals #Ethics #GlobalCooperation #Responsibility #ClimateChange