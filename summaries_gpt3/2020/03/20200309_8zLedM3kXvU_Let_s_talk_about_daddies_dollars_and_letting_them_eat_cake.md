# Bits

Beau says:

- Comparing the United States to Marie Antoinette's "let them eat cake" moment, showcasing government disconnect.
- Contrasting European aristocracy with American aristocracy and their perceptions of wealth and privilege.
- Noting the awakening of the working class in the U.S. to the behavior of wealthy families and the rise of terms like oligarchy and plutocracy.
- Donald Trump Jr. challenging Hunter Biden to a debate on whose father helped them the most, reflecting tone-deafness amidst societal issues.
- Expressing the historical trend of ruling classes becoming more flagrant once their privilege is exposed.
- Acknowledging that while parents helping their children succeed isn't inherently wrong, corruption and taxpayer exploitation are problematic.
- Questioning whether political elites are engaging in corruption as they accumulate vast wealth post-office, prompting mainstream recognition of terms like kleptocracy.

# Quotes

- "My daddy can beat up your daddy."
- "Everybody wants a better life for their kids than they had."
- "Is the subconscious realization that corruption is happening one of the reasons those terms are coming into the mainstream?"

# Oneliner

Comparing government disconnect to Marie Antoinette, contrasting aristocracies, noting rising awareness of wealth behavior, and questioning corruption in political elites.

# Audience

Working-class Americans

# On-the-ground actions from transcript

- Question the actions and wealth accumulation of political elites (implied)
- Stay informed and aware of corruption within political circles (implied)

# Whats missing in summary

Insights on the societal impact of political corruption and wealth accumulation.

# Tags

#GovernmentDisconnect #WealthDisparity #PoliticalCorruption #SocialAwareness #EconomicInequality