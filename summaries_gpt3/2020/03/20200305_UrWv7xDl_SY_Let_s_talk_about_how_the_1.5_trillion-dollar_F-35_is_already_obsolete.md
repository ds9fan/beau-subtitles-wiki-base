# Bits

Beau says:

- Explains how institutions resist change, even when it's necessary, due to institutional traditions.
- Mentions how the military brings in outside experts for fresh perspectives, though sometimes these insights are not readily accepted.
- Recalls the first use of airships by the military in 1912, leading to an obsession with air superiority around the world.
- Criticizes the spending of $1.5 trillion on the F-35 fighter jets, which are considered obsolete even before being fully implemented.
- Elon Musk's suggestion to shift towards drone technology for military aircraft is supported, as it eliminates the need for human pilots and reduces risks.
- Raises concerns about the consequences of using drones in warfare, particularly the potential for increased civilian casualties due to reduced aversion to risk.
- Points out Turkey's use of drones in a recent campaign as evidence supporting Musk's stance on transitioning to drone technology.
- Emphasizes the wastefulness of funding obsolete military projects while claiming a lack of resources for other critical needs.

# Quotes

- "The age of the fighter pilot, much like the age of the airship, is over."
- "If we're not risking any human assets to accomplish something, we will probably do it more often because there's no risk."
- "If it's just machines, nobody's going to care."
- "We are wasting $1.5 trillion because it wasn't thought through."
- "We're worried about creating the next generation fighter. We shouldn't."

# Oneliner

Beau delves into the resistance of institutions to change, critiquing the military's reluctance to embrace drone technology despite its potential benefits and cost-effectiveness.

# Audience

Military decision-makers

# On-the-ground actions from transcript

- Advocate for the adoption of drone technology in military operations (exemplified)
- Question and scrutinize military spending on potentially obsolete projects (exemplified)

# Whats missing in summary

The full transcript delves deeper into the implications of technological advancements in military operations and the ethical considerations surrounding warfare conducted through unmanned drones.

# Tags

#Military #InstitutionalTraditions #DroneTechnology #MilitarySpending #EthicalConcerns