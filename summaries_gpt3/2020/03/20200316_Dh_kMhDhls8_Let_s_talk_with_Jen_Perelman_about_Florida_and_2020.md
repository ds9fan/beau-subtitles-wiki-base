# Bits

Beau says:

- Jen Perlman is priming Debbie Wasserman Schultz in Florida's 23rd congressional district, aiming to remove a long-standing incumbent.
- Perlman criticizes Wasserman Schultz for prioritizing corporate interests over community concerns like environmental protection.
- Perlman, a second-generation Floridian with a legal background, is focused on justice and serving the people, not a political career.
- She advocates for removing profit motives from industries like healthcare and education, supporting single-payer healthcare and affordable education for all.
- Perlman opposes bans on assault weapons, citing their inefficacy and the need to address root causes of gun violence.
- She argues against the privatization of public services like education through vouchers, advocating for quality options for all.
- Perlman denounces ICE as unnecessary and criticizes its treatment of immigrants, advocating for a humane approach to immigration.
- She addresses wealth inequality and lack of affordable housing in Broward County, calling for a living wage and healthcare as human rights.
- Perlman encourages community involvement and service as a way to enact change, urging people to connect with local organizations and address pressing needs.
- She stresses the importance of grassroots support, small donations, and community engagement in her campaign against corporate influence.

# Quotes

- "It's about justice. It's about social justice, economic justice, and environmental justice."
- "I have no career motives. I have no career ambition in this whatsoever. This is something that I want to do as a service."
- "I don't see them as protecting and serving. I don't see the people that they're going after as being necessarily dangerous to society."
- "I just have this crazy idea that people shouldn't have to work three jobs and drive an Uber to be able to live."
- "Find the need, there's no shortage. Even if it's going to the library and just doing a little bit of research and saying like, what's going on in this area and how can I help?"

# Oneliner

Jen Perlman, priming Debbie Wasserman Schultz in Florida, advocates for justice, single-payer healthcare, community service, and grassroots change against corporate influence.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Reach out to local organizations, attend meetings, and offer help in areas of need (exemplified)
- Organize clothing drives, toiletry collections, or beach cleanups to support local communities (exemplified)
- Support Jen Perlman's campaign by donating to gen2020.com and spreading the word (exemplified)

# What's missing in summary

The full transcript provides a comprehensive look at Jen Perlman's background, values, and platform, offering insight into her grassroots campaign and commitment to community service. Watching the full transcript can provide a deeper understanding of her approach to politics and activism.

# Tags

#Florida #Politics #CommunityService #SocialJustice #GrassrootsCampaign #CorporateInfluence #SinglePayerHealthcare #AffordableHousing