# Bits

Beau says:

- Sharing a message from someone feeling helpless about not being able to volunteer due to being at risk and staying at home.
- Mention of a project called "folding at home" run by Stanford University that utilizes computing power to research folding proteins.
- Describing how the distributed network of computers contributes processing power to aid in research.
- Emphasizing the importance of everyone finding a way to contribute, even from home.
- Encouraging individuals to seek out ways to help in any situation.
- Hinting at future short videos to disseminate vital information during the current circumstances.

# Quotes

- "Everybody can do something in any situation."
- "There will always be some way for everybody to contribute."
- "The power of the internet is disseminating information."

# Oneliner

In times of crisis, everyone can contribute, even from home, through projects like "folding at home" to aid critical research efforts.

# Audience

Online community members

# On-the-ground actions from transcript

- Set up "folding at home" project on your computer (exemplified)
- Use your computing power to aid research efforts (exemplified)

# Whats missing in summary

The full transcript provides detailed steps on how individuals can contribute by utilizing their computer's unused computing power to aid in critical research efforts.