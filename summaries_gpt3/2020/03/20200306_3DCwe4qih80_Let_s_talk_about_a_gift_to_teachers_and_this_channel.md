# Bits

Beau says:

- Reciting a list of historical events and figures from Billy Joel's song "We Didn't Start the Fire."
- Channel's purpose: to help students better understand history and current events through engaging content.
- Collaboration with public school teachers to incorporate elements from the song into videos.
- Emphasizing the importance of history in understanding current events, philosophy, and community activism.
- Stating the necessity of comprehending past events to remain relevant and effective in the present.
- Encouraging viewers to view various aspects of life through a historical lens.
- Conveying that the fire of history, once ignited, will continue burning.
- Stressing the significance of history in shaping our understanding of the world.
- Linking past events to present-day relevance and participation in shaping the future.
- Signifying that the channel serves as a platform for exploring history and its impact on contemporary issues.

# Quotes

- "While we did not start the fire, it will keep burning."
- "If you want to be relevant 70 years later, you're going to have to understand what happened 70 years ago."

# Oneliner

Beau recites historical events, urging viewers to understand the past to influence the future effectively.

# Audience

History enthusiasts, educators, activists.

# On-the-ground actions from transcript

- Collaborate with educators to create engaging historical content (implied).
- Engage in community activism informed by historical perspectives (implied).

# Whats missing in summary

The full transcript provides a comprehensive historical overview through a modern lens, encouraging viewers to connect past events with present actions for a better future.

# Tags

#History #Education #Community #Activism #Understanding