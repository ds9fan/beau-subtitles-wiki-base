# Bits

Beau says:

- Introduces a science project where people can help unlock universe secrets from home during this time of being stuck indoors.
- Mentions the project, Galaxy Zoo, which involves looking at pictures of galaxies and answering questions to aid researchers.
- Describes the multiple choice format and provides a field guide for understanding the task.
- Talks about the excess of information captured compared to our ability to process it, making contributions valuable.
- Expresses how engaging in this project can provide an escape to space for individuals feeling stressed or bored.
- Emphasizes that participating helps researchers understand galaxy evolution, internal workings, interactions, and merges.
- Acknowledges the present anxiety and suggests this project could offer a positive distraction and something to look forward to.
- Notes the lack of immediate practical application but anticipates its future relevance.
- Encourages staying positive and looking towards the future, as humanity has overcome challenges before.
- Concludes with well wishes for the viewers.

# Quotes

- "What better place to escape to than space?"
- "Maybe there are some people out there that need something to look forward to, need something a little bit less real to look at."
- "We are a very resilient species."

# Oneliner

Beau introduces Galaxy Zoo, a project enabling people to unlock universe secrets from home during difficult times, offering a space-based escape and a positive distraction while aiding researchers in understanding galaxy evolution and interactions.

# Audience

Science enthusiasts, Space lovers

# On-the-ground actions from transcript

- Join Galaxy Zoo project to assist researchers in understanding galaxies (suggested)
- Utilize spare time at home to contribute to the Galaxy Zoo project (implied)

# Whats missing in summary

The full transcript provides additional details and Beau's unique perspective, encouraging viewers to participate in an engaging and educational space project from the comfort of their homes.

# Tags

#Science #Space #GalaxyZoo #Research #Volunteer