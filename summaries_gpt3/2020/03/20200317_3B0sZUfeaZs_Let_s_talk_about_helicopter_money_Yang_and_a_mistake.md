# Bits

Beau says:

- The administration is considering the idea of giving everyone a couple thousand dollars to stimulate the economy, similar to Andrew Yang's plan.
- Republicans support this to boost the economy, while some Democrats oppose it, suspecting a hidden agenda to boost polls.
- Beau believes that money in people's hands will help them stay home, not lose their homes, and focus on flattening the curve.
- He criticizes the Democratic Party for opposing this move, stating that it's more about people than the economy.
- Beau prefers money to go to working individuals rather than Wall Street to keep people in their homes and stimulate the economy simultaneously.
- He acknowledges the danger outside and sees this as a controllable situation where the government can step in to help.
- Beau expresses concern that making the average American suffer just to show Trump's shortcomings is not necessary and suggests a different campaign strategy.
- Keeping people in their homes is emphasized as the best way to control the situation and focus on what's controllable.

# Quotes

- "Money in the hands of the average person will not only help stimulate the economy, which I literally do not care about."
- "If we're going to have an overreaching government that is capable of doing anything and has this power, this seems like a pretty good moment to use it."
- "Keeping people in their homes is the best way to do that."

# Oneliner

Beau believes giving people money to stay home and not lose their homes is more about people than the economy, criticizing the opposition for not focusing on controlling what's controllable.

# Audience

Policy Makers, Political Activists

# On-the-ground actions from transcript

- Distribute aid to keep people in their homes (exemplified)
- Focus on controlling controllable elements (suggested)

# Whats missing in summary

The full transcript provides more insights into Beau's perspectives on government intervention, party strategies, and the importance of prioritizing people over the economy during crises.