# Bits

Beau says:

- Addressing duty, obligations, and accusations in relation to supporting healthcare workers.
- Comparing healthcare workers to troops on the front lines.
- Criticizing the lack of support healthcare workers receive.
- Drawing parallels to past scandals where troops lacked necessary equipment.
- Calling out government leadership for blaming frontline workers instead of taking responsibility.
- Challenging accusations without evidence.
- Questioning the hoarding of ventilators by hospitals.
- Encouraging the release of unused ventilators for those in need.
- Urging leadership to lead by example and prioritize the needs of healthcare workers.
- Expressing concern over healthcare professionals contemplating quitting due to lack of support.
- Emphasizing that healthcare workers care about their job and the people they serve.
- Suggesting that those dedicated to their duty are the ones needed most during challenging times.

# Quotes

- "It should be a national scandal that we are sending our troops to the front lines without their armor."
- "When it's over, they're not quitting because of the risk. They're quitting because we're not giving them the support they deserve and they need."
- "Lead from the front, sir. Set the example."
- "When this is over, I am so done. I am out of here after this."
- "But see, the thing is, when it's over, they probably won't quit because they actually care about their job."

# Oneliner

Beau stresses the duty and support owed to healthcare workers on the front lines, likening them to troops without proper armor.

# Audience

Healthcare advocates

# On-the-ground actions from transcript

- Release unused ventilators for those in need (suggested)
- Advocate for better support and resources for healthcare workers (implied)

# Whats missing in summary

The emotional impact conveyed by Beau's plea for proper support and recognition of healthcare workers dedicated to their duty.

# Tags

#HealthcareWorkers #Support #Duty #Obligations #Frontline #Advocacy