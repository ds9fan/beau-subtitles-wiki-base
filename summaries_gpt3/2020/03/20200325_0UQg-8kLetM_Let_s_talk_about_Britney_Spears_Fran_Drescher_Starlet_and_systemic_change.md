# Bits

Beau says:

- Soviets sent up Sputnik 10 in 1961 with a mannequin and a dog named Starlet, the first dog in orbit, to capture the imagination and bring attention to space travel.
- Britney Spears and Fran Drescher have endorsed radical ideas for systemic change on social media, sparking mixed responses.
- Radical ideas related to systemic change are scary as they challenge the current system and require embracing the unknown.
- Mainstream figures like Britney Spears carrying radical ideas make them less threatening and help in spreading the message.
- Systemic change won't come from political leaders or the system itself, as evidenced by the stimulus package's allocation of funds.
- It becomes easier to question and advocate for radical ideas when influential figures like Britney Spears and Fran Drescher openly support them.
- Advocating for systemic change during challenging times is vital to push for change when the situation improves.
- Encouraging more celebrities and influential figures to introduce radical ideas to the mainstream is necessary for systemic change.

# Quotes

- "You go to war with the army you have. They enlisted. Give them the support they need."
- "We need to encourage more celebrities, more people with influence outside of the system to introduce those radical ideas to the general populace, to the mainstream."
- "If we want systemic change, we have to act like it."

# Oneliner

Beau talks about the importance of mainstream figures like Britney Spears and Fran Drescher endorsing radical ideas for systemic change and the need to support and encourage such voices.

# Audience

Advocates for systemic change

# On-the-ground actions from transcript

- Support and encourage celebrities and influential figures endorsing radical ideas (suggested)
- Advocate for systemic change in your community and beyond (implied)

# Whats missing in summary

The full transcript provides a deep dive into how influential figures can drive systemic change by endorsing radical ideas and the importance of supporting such voices in advocacy efforts.

# Tags

#SystemicChange #Advocacy #Influence #Support #Mainstream #Celebrities