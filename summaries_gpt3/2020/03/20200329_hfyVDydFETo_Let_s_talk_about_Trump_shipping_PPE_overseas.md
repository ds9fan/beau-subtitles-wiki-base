# Bits

Beau says:

- Beau addresses his recent decision to not talk much about Trump, focusing instead on the immediate problems at hand during the current crisis.
- He questions the administration's decision to provide protective equipment to foreign countries while medical professionals in the US are struggling to find the same equipment.
- Beau expresses curiosity about what Trump gained in return for sending protective equipment abroad, suspecting personal benefits rather than altruistic motives.
- He criticizes Trump for not following established crisis management plans and for spreading false information that endangers public health.
- Beau questions the media's continued coverage of Trump despite his misleading statements and lack of leadership.
- He warns about the danger of false hope and misinformation during a crisis and urges people to listen to experts rather than Trump.
- Beau concludes by urging listeners to stay informed and trust knowledgeable sources during these challenging times.

# Quotes

- "The president of the United States is a danger to the people of the United States."
- "False hope is extremely dangerous in a situation like this."
- "His desire to paint this rosy picture is harmful."
- "We have to wonder why."
- "Listen to the people that actually know what they're talking about."

# Oneliner

Beau questions Trump's motives in sending protective equipment abroad, criticizes his lack of leadership, warns about the danger of false information during a crisis, and urges people to trust knowledgeable sources.

# Audience
Community members

# On-the-ground actions from transcript

- Listen to experts and trusted sources for accurate information (implied)
- Stay informed about the situation from reliable sources (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's actions during the crisis and urges people to prioritize accurate information from experts.

# Tags

#Trump #COVID19 #Leadership #Media #PublicHealth #CommunitySafety