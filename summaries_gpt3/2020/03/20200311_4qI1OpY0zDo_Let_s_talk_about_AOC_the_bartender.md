# Bits

Beau says:

- Criticizes the backlash against AOC for either misspeaking or using internet speak.
- Questions the emphasis on AOC's past as a bartender, suggesting she has more qualifications.
- Points out that elected officials often distance themselves from working-class backgrounds.
- Argues that individuals like bartenders have a better understanding of community issues.
- Advocates for more diverse representation in government, including bartenders, fast food workers, nurses, teachers, welders, and ranchers.

# Quotes

- "Why should she clarify? Everybody up there likes to pretend that they're a man of the working people."
- "We need more bartenders. We need more fast food workers. We need more nurses. We need more teachers. We need more welders. We need more ranchers and less professional politicians."
- "The House of Representatives might, should be, representative of us."

# Oneliner

Beau criticizes the backlash against AOC, advocates for diverse representation, and questions the focus on professional politicians over working-class backgrounds.

# Audience

Voters, Political Activists

# On-the-ground actions from transcript

- Elect diverse representatives who understand community interests (implied)
- Advocate for more representation from working-class backgrounds (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the importance of varied representation in government and the need for elected officials who truly understand community interests.

# Tags

#Representation #AOC #WorkingClass #CommunityInterests #GovernmentRepresentation