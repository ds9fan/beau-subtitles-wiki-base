# Bits

Beau says:

- Talking about mistakes and misinterpreting data, leading to a lack of understanding of ongoing events.
- People comparing initial predictions with current ones, dismissing the effectiveness of social distancing.
- The importance of acknowledging that social distancing is working in lowering the number of cases.
- Criticizing the president for extending social distancing but not admitting his initial models were incorrect.
- Emphasizing the necessity to continue social distancing to prevent overwhelming medical infrastructure.
- Warning against disregarding evidence and blindly supporting a specific person, likening it to cult behavior.
- Encouraging people to prioritize public health over personal beliefs and political allegiances.
- Reminding everyone to follow safety precautions like washing hands, not touching faces, and wearing masks.

# Quotes

- "The desire to set aside facts because you want to support a specific person is indicative of cult behavior."
- "You made a bad choice. Everybody makes mistakes."
- "If you disregard these precautions, you're only going to hurt yourself and those people you know."
- "Is that level of selfishness and willful ignorance making America great?"
- "Y'all have a good day."

# Oneliner

Beau talks about the importance of acknowledging that social distancing works, despite political biases, and warns against disregarding evidence for personal beliefs.

# Audience

General public

# On-the-ground actions from transcript

- Stay at home, follow social distancing guidelines, and wear a mask when going out (implied).
- Wash hands frequently and avoid touching your face (implied).

# Whats missing in summary

The emotional impact of realizing the necessity to prioritize public health over personal beliefs and political allegiances.

# Tags

#SocialDistancing #PublicHealth #CultBehavior #SafetyPrecautions #CommunityLeadership