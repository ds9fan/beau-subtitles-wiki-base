# Bits

Beau says:

- Introduces the concept of taking action and responsibility, especially for those with the means.
- Mentions the impact of viewer participation through watching videos and live streams.
- Talks about the critical shortage of medical supplies, including blood products.
- Mentions the cancellation of blood drives due to restrictions on large gatherings.
- Encourages those who are eligible and able to donate blood through organizations like the Red Cross.
- Emphasizes the importance of following eligibility criteria and precautions before donating.
- Acknowledges that not everyone may be in a position to leave their house to donate.
- Urges individuals to help if they can, especially if they are universal donors.
- Stresses the global need for everyone to contribute during this pandemic.
- Concludes by encouraging people to take action and help in any way they can.

# Quotes

- "If you have the means, you have the responsibility."
- "We're in a global battle, and we all have to pitch in."
- "Desperately need the products."
- "Get out there, go do it."
- "We all want to help."

# Oneliner

Beau stresses taking responsibility and contributing, especially through blood donations, during the global battle against shortages.

# Audience

Donors

# On-the-ground actions from transcript

- Donate blood through organizations like the Red Cross (suggested).
- Follow eligibility criteria and precautions before donating (implied).
- Help by being a universal donor (implied).

# Whats missing in summary

The importance of community support and collective effort during times of crisis.

# Tags

#Responsibility #BloodDonation #GlobalBattle #CommunitySupport #CrisisResponse