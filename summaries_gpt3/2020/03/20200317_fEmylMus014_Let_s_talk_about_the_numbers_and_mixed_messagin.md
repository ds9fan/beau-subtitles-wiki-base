# Bits

Beau says:

- Encountered people ignoring medical advice due to low perceived risk.
- Explains the concept of a 0.2% chance to convey the real danger.
- Compares the risk to a Walmart analogy with a 500th person scenario.
- Urges following medical professionals' advice over politicians' on social media.
- Acknowledges differing opinions among medical professionals but advises following the strictest guidelines.
- Addresses accusations of mixed messaging regarding staying home.
- Reveals personal exposure risk due to wife being a medical professional.
- Describes going out to serve a greater cause while practicing strict self-isolation.
- Emphasizes the importance of staying home if there's no urgent need to go out.
- Urges not to risk life over economic concerns and not to use his actions as an excuse to violate self-isolation.
- Encourages listeners to determine their own level of involvement and prioritize safety.
- Reminds that the numbers represent lives, not just statistics on an exam.
- Urges trusting healthcare professionals over politicians during this crisis.
- Calls for heroism in staying home and following health guidelines.

# Quotes

- "Follow the advice of the medical professionals, not the advice of your favorite politician on Twitter."
- "You can be a hero right now by staying home."
- "It's your life."

# Oneliner

Beau stresses the importance of following medical advice during the pandemic, urging people to prioritize their lives over economic concerns and political influence, and be heroes by staying home.

# Audience

General public

# On-the-ground actions from transcript

- Stay home, wash hands, practice self-distancing, and follow medical professionals' advice (exemplified).
- Determine your level of involvement and prioritize safety (exemplified).

# Whats missing in summary

The emotional impact of choosing to stay home and prioritize safety during the pandemic.

# Tags

#Pandemic #HealthGuidelines #StayHome #MedicalAdvice #CommunitySafety