# Bits

Beau says:

- Beau expresses gratitude for the concern and well wishes he has received.
- He mentions that his opinion on the issue has not changed, but he is taking some new precautions.
- Despite being in a crowded place full of travelers the next day, Beau states he will not be altering his daily routine drastically.
- He criticizes the response of the state leadership in Florida, mentioning that the lack of information is causing fear.
- Beau points out the disappointing behavior of some Floridians, describing witnessing panic during his outing.
- Floridians are reminded by Beau about the various natural dangers they face in the state and the need to approach things with dark humor and community spirit.
- He encourages people to inform themselves, look at the numbers objectively, and follow the guidelines and precautions.
- Beau suggests that those at higher risk should take extra precautions and that the community should support vulnerable individuals by helping them with tasks like shopping and errands.
- In the absence of effective government leadership, Beau stresses the importance of individuals taking charge and not succumbing to panic.
- He criticizes panic buying behavior, especially pointing out the irrationality of hoarding gas as if it were a hurricane situation.
- Beau calls for Floridians to stay calm, maintain their sense of community, and use humor to navigate through challenging times.

# Quotes

- "We cannot let fear run our lives."
- "If we are not going to get leadership from the government, we have to lead ourselves."
- "Panic is not going to help."
- "Pull your stuff together. You're a Floridian."
- "Stay calm. Use the same humor and sense of community that gets us through everything else that gets thrown at us."

# Oneliner

Beau urges Floridians to stay calm, support each other, and lead themselves in the face of COVID-19 uncertainty, rejecting panic buying and advocating for a sense of community resilience.

# Audience

Floridian residents

# On-the-ground actions from transcript

- Support older or compromised individuals by doing their shopping and errands for them (suggested)
- Maintain a sense of community and humor to navigate through challenges (exemplified)

# Whats missing in summary

The full transcript provides a detailed perspective on how Floridians can handle the COVID-19 situation with resilience and community support. Watching the full text can offer a deeper understanding of maintaining calm and unity during times of crisis.

# Tags

#COVID-19 #CommunitySupport #Florida #PanicBuying #Resilience