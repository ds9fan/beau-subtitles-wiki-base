# Bits

Beau says:

- Addresses a message to medical professionals and from them, focusing on the stress and challenges faced by those on the front lines.
- Shares a story about prior service military members in a medical facility and how their training intensity and expertise are being called upon now.
- Describes the military's intense training scenarios and how it differs from civilian training due to profit motivations.
- Talks about the high level of confidence and posture that military training instills in individuals.
- Explains how prior service military members are falling back into their training mannerisms and attitudes during the current crisis.
- Emphasizes the importance of recognizing genuine emergencies in the ER and understanding the triage process.
- Advises against overwhelming medical infrastructure by visiting the ER unnecessarily or bringing multiple people.
- Urges visitors to follow guidelines, limit movements within the facility, and not challenge medical professionals' expertise.
- Acknowledges the stress and uncertainty faced by medical professionals on the front line and urges understanding and empathy.
- Concludes with a reminder to practice hygiene, social distancing, and follow medical advice.

# Quotes

- "Emergency being the key word."
- "Cut them some slack."
- "Their main patient is all of society."

# Oneliner

Beau addresses the stress and challenges faced by medical professionals on the front lines, urging understanding, empathy, and adherence to guidelines while reminding all to practice hygiene and social distancing.

# Audience

Medical community, general public

# On-the-ground actions from transcript

- Follow guidelines, practice social distancing, and adhere to medical advice (suggested)
- Limit visits to the ER to genuine emergencies and avoid overwhelming medical infrastructure (suggested)
- Show understanding and empathy towards stressed medical professionals (implied)

# Whats missing in summary

The full transcript provides detailed insights into the experiences and challenges faced by medical professionals and offers valuable guidance on how individuals can support and cooperate with healthcare workers during these trying times.

# Tags

#MedicalProfessionals #EmergencyRoom #Triage #Stress #Empathy #Hygiene #SocialDistancing #Support