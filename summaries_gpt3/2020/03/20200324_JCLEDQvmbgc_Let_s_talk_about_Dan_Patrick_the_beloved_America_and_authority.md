# Bits

Beau says:
- Lieutenant Governor Dan Patrick of Texas made a statement regarding older Americans and passing down the America they love to their grandkids, but then he tied it to the economy and suggested business as usual, even if only one in five older Americans might die.
- In California, 80% of COVID patients are aged 18 to 65, showing that it's not just the elderly who are affected.
- The American ruling class is prioritizing profit over lives, refusing to take necessary actions like a 21-30 day shutdown that experts recommend.
- Beau questions whether an America willing to sacrifice lives for profit is worth saving and suggests that those in power making such decisions need to be replaced.
- He stresses the importance of preparing for significant societal changes post-pandemic and calls for genuine representation in leadership that prioritizes lives over money.

# Quotes

- "Maybe that isn't the America everybody loves."
- "How can you believe somebody this callous and this incompetent has authority over you?"
- "The decisions that are being made are going to cost thousands of lives, and it's all about money."
- "If we are going to have a representative democracy, it needs to be representative."
- "We just have to do what we need to do. We've got to shut it down."

# Oneliner

Lieutenant Governor's economic focus over lives, American ruling class prioritizing profit, and the need for genuine representation in leadership during the pandemic.

# Audience

Politically active citizens

# On-the-ground actions from transcript

- Replace leaders prioritizing profit over lives and genuine representation (implied)
- Advocate for necessary actions to protect lives over economic interests (implied)
- Prepare for significant societal changes post-pandemic (implied)

# Whats missing in summary

The urgency of replacing profit-focused leaders with genuine representatives for a post-pandemic society.

# Tags

#COVID19 #AmericanRulingClass #GovernmentPriorities #Leadership #Representation