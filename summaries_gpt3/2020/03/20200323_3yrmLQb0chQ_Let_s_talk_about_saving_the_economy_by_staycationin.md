# Bits

Beau says:

- Economic pundits push the idea that measures taken are detrimental to the economy.
- Half-hearted measures will be ineffective and prolong economic slowdown.
- Acting decisively now will benefit the economy in the long run.
- Indecision and contradictory advice will destroy the U.S. economy.
- Flattening the curve is vital to prevent a prolonged economic downturn.
- Failure to support measures will prolong economic hardship and endanger lives.
- Uncertainty and flare-ups due to lack of action will hinder economic recovery.
- Action is needed, not just talk or tweets.
- Support staying at home measures for 21 or 30 days to curb the spread.
- Experts agree on the necessity of strict measures to protect society.
- Disregarding advice will harm the economy and people's lives in the long term.
- Temporary portfolio hits are a necessary sacrifice for long-term economic stability.
- Time is needed for treatments and preventative measures to be developed and tested.
- Collaboration and unity are key to overcoming the crisis.
- Failure to act now will lead to catastrophic consequences for the economy and lives.

# Quotes

- "Flatten the curve, not the economy."
- "Stay home. It doesn't matter what the president says."
- "If you want to protect the economy, support a 21 or 30 day, everybody stay at home."
- "Your portfolio during that period, yeah, it's going to take a hit."
- "This is how we buy enough time for treatments and preventative measures to be developed."

# Oneliner

Act decisively now to protect lives and the economy by supporting strict stay-at-home measures for 21 or 30 days.

# Audience

Policy makers, citizens, leaders

# On-the-ground actions from transcript

- Support a 21 or 30-day stay-at-home period to curb the spread (implied).
- Stay home to protect lives and the economy (implied).
- Act together with your community to prevent economic devastation (implied).

# Whats missing in summary

The full transcript provides a detailed explanation of how decisive action and unity in supporting stay-at-home measures are necessary to prevent a prolonged economic slowdown and protect lives. 

# Tags

#Economy #StayAtHome #COVID19 #Unity #Action