# Bits

Beau says:

- Introducing the topic of Joseph Lowery, a significant figure in civil rights history who deserves more attention from the media.
- Lowery played a key role in the Civil Rights Movement, including during the Montgomery bus boycott and the Selma to Montgomery march.
- Despite his major contributions, Lowery's legacy may not receive the comprehensive recognition it deserves.
- Lowery's impact extended beyond racial equality as he advocated for other marginalized groups, like the LGBTQ+ community.
- He used his influence and privilege to amplify the voices of others, showcasing what it truly means to be a good ally.
- Lowery's approach involved supporting and empowering others rather than seeking the spotlight for himself.
- His actions exemplify the essence of being a supportive ally and helping others gain power and agency.
- Beau underscores the importance of understanding Lowery's broader impact on American civil rights, not just within the black community.
- Lowery's legacy is a reminder of the power of quiet, behind-the-scenes advocacy and support for marginalized communities.
- Beau encourages the media to ensure that Lowery receives the recognition and memorial he deserves, portraying him as the "Where's Waldo of American civil rights."

# Quotes

- "He provided the nation with a shining example of what it means to be a good ally."
- "He understood what it meant to be a good ally, to help elevate others."
- "That's why he's the dean of American civil rights rather than just the dean of black civil rights."

# Oneliner

Beau sheds light on Joseph Lowery, a pivotal figure in American civil rights history, showcasing the essence of being a supportive ally and amplifying marginalized voices.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Honor Joseph Lowery's legacy by amplifying the voices of marginalized communities (implied)
- Advocate for civil rights and equality for all, following Lowery's example of being a supportive ally (implied)

# Whats missing in summary

The emotional depth and nuances of Lowery's impactful legacy beyond mainstream recognition.

# Tags

#JosephLowery #AmericanCivilRights #Allyship #MarginalizedCommunities #Recognition