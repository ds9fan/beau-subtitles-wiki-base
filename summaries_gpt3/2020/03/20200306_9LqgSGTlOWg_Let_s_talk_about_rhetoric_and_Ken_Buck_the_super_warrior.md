# Bits

Beau says:

- Introduces the topic of rhetoric and its purpose, using Congressperson Ken Buck as an example.
- Describes a legislator's role in debating, discussing, and convincing opposing parties.
- Criticizes Ken Buck for releasing a video filled with rhetoric instead of engaging in constructive debate.
- Points out the use of an AR rifle as a prop in the video and questions Buck's familiarity with handling it.
- Raises concerns about the irresponsible display of weapons and the potential dangers it poses.
- Calls out Buck for using threatening rhetoric that appeals only to the uninformed.
- Emphasizes the importance of setting a positive example as a congressperson.
- Condemns the use of guns as props and the dangerous implications it can have.
- Challenges Buck's image as a tough guy and questions his commitment to responsible gun ownership.
- Urges Buck to reconsider his campaign tactics and focus on logic and persuasion rather than intimidation.

# Quotes

- "Come and take it."
- "Guns are not toys, sir."
- "You're supposed to be respectable."
- "You're supposed to be somebody who uses logic and persuasion, not intimidation."
- "You need to rethink your campaign."

# Oneliner

Beau criticizes Congressperson Ken Buck for resorting to threatening rhetoric and irresponsible weapon display instead of engaging in constructive debate, urging him to reconsider his campaign tactics.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Educate others on responsible gun ownership and the dangers of using weapons as props (implied)
- Advocate for constructive debate and logical persuasion in political discourse (implied)
- Support candidates who prioritize respectful and informed communication in politics (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the problematic rhetoric used by a congressperson, urging for a shift towards responsible communication and debate in politics.

# Tags

#Rhetoric #GunControl #PoliticalDiscourse #ResponsibleCommunication #CampaignTactics