# Bits

Beau says:

- Introduction of special guest Mike Breuer, who is running against Mitch McConnell in Kentucky.
- Mike Breuer's background as a retired Marine Lieutenant Colonel, educator, substitute teacher, and farmer.
- Mike Breuer's platform focusing on economic and social justice for all based on equality under the law and common ownership of resources.
- Critique of McConnell as someone focused on accumulating power and wealth rather than the well-being of Kentuckians.
- Mike Breuer's emphasis on economic justice for Appalachian coal-producing states like Kentucky.
- The interconnectedness of economic and social justice and the impact on Kentucky residents.
- Mike Breuer's approach to communicating his message honestly and respectfully, drawing from his experience as a reporter and editor.
- Criticism of the prison system in Kentucky and the financial burden it places on taxpayers.
- Impact of budget cuts on public libraries and schools in Kentucky, especially during times like the coronavirus outbreak.
- Strategy to beat McConnell with truth and by pointing out his lack of significant contributions to Kentucky.
- Mike Breuer's stance on controversial issues like women's reproductive rights and universal background checks.
- Critique of McConnell's use of public funds for projects that benefit him politically rather than the people of Kentucky.
- Challenges faced by Kentucky farmers due to misguided policies like hemp legalization.
- Mike Breuer's progressive stance on gay rights and the importance of honesty and fairness in politics.
- Call for individuals to listen, understand, and respect others in order to make the world a better place.

# Quotes

- "Beat him with the unrelenting truth."
- "Don't be a dick."
- "He is not good for the Republic. And he needs to go."

# Oneliner

Beau introduces Mike Breuer, a candidate running against Mitch McConnell in Kentucky, who advocates for economic and social justice based on equality under the law and common ownership of resources while critiquing McConnell's focus on power and wealth accumulation.

# Audience

Voters

# On-the-ground actions from transcript

- Support Mike Breuer's campaign by donating at mikeforky.com (implied)
- Spread the message on social media to increase visibility and support for Mike Breuer (implied)
- Volunteer, donate, or come forward to help the campaign if in Kentucky (implied)

# Whats missing in summary

Mike Breuer's detailed plans on how to bring about economic and social justice in Kentucky.

# Tags

#Kentucky #MitchMcConnell #EconomicJustice #SocialJustice #Campaign