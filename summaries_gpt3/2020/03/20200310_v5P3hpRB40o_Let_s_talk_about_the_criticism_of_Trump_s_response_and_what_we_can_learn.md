# Bits

Beau says:

- Criticizes Trump's response to a situation, calling it less than ideal and worthy of criticism.
- Compares the response to the movie "Jaws," with scientists urging action while the establishment prioritizes immediate concerns like tourism and economy.
- Points out that the government often ignores warnings from experts and focuses on short-term gains rather than mitigating harm.
- Emphasizes the importance of taking action now to mitigate upcoming challenges rather than waiting until it's too late.
- Notes that the current situation is unnerving and can be disruptive and tragic, but shouldn't be surprising given past patterns.
- Criticizes elected officials, including Trump, for not taking necessary actions to address crises and for playing down serious issues like climate change.
- Warns about the severity of climate change, stating it poses a greater threat than the current situation and will lead to significant disruptions and climate refugees if not addressed promptly.
- Stresses the urgency of mitigating climate change before it escalates further.

# Quotes

- "What it's not is surprising."
- "We have to take action now to mitigate."
- "Climate change is [a threat to the species]."
- "It's going to get bad, a lot worse than this."
- "We're running out of time."

# Oneliner

Beau criticizes Trump's response, compares it to "Jaws," and stresses the urgency of taking action to mitigate climate change before it escalates.

# Audience

Aware citizens

# On-the-ground actions from transcript

- Take immediate action to mitigate climate change (implied)
- Advocate for strong measures to address climate change (implied)

# Whats missing in summary

The full transcript provides more context on the analogy between Trump's response and the movie "Jaws," as well as a deeper exploration of the consequences of failing to address climate change promptly.

# Tags

#ClimateChange #TrumpResponse #Mitigation #Urgency #Analogies