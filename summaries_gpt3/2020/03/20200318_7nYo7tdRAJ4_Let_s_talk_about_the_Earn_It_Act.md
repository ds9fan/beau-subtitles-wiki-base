# Bits

Beau says:

- Draws a parallel between the lack of privacy in the book/movie 1984 and current privacy concerns.
- Mentions a bill that allows the government to scan every message on the internet through private companies.
- Explains that the bill doesn't mention encryption but mandates scanning every message, leading to the death of encryption.
- Describes a committee headed by Attorney General Barr that will set best practices for online platforms, including scanning every message.
- Points out the bill's packaging as a measure to protect exploited kids, similar to justifying surveillance in every building.
- Raises questions about the extent of government surveillance and the justification for scanning every message.
- Warns that the bill, known as the EARN IT Act, has bipartisan support and could gain traction if not opposed.
- Encourages opposition to the bill and shares a resource from the Electronic Frontier Foundation.
- Reminds of the government's tendency to exploit crises for their benefit.
- Considers the implications of granting agencies access to every message sent, questioning if society is ready for such intrusion.

# Quotes

- "How far are you willing to take it?"
- "Do you want the agencies that have bungled this current crisis so much to have access to every single message that you send?"
- "Are you ready for Big Brother?"

# Oneliner

Beau warns about a bill allowing government surveillance through private companies, urging opposition and questioning societal readiness for extensive monitoring.

# Audience

Internet users

# On-the-ground actions from transcript

- Oppose the EARN IT Act to prevent extensive government surveillance (implied).

# Whats missing in summary

The full transcript provides a detailed explanation of the implications of a bill allowing government surveillance through private companies, urging viewers to take action and oppose the EARN IT Act.

# Tags

#Privacy #GovernmentSurveillance #EARNITAct #OnlinePrivacy #Opposition