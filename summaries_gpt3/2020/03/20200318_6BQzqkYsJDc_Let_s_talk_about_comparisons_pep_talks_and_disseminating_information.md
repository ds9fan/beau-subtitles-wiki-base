# Bits

Beau says:

- Shares a story from the early 2000s when running a protective detail for a high-risk client.
- Describes how a counter surveillance team alerted him about suspicious individuals conducting surveillance.
- Gives his team a pep talk after receiving photos of the suspicious individuals.
- Emphasizes the importance of disseminating information at the appropriate time.
- Criticizes the use of comparisons, like the likelihood of dying in a car accident versus gunfire, to convey calm during tense situations.
- Stresses that downplaying threats through comparisons fosters ignorance and does not help.
- Encourages meme creators to focus on spreading messages about handwashing and social distancing.
- Urges people to listen to medical professionals for guidance on staying safe during challenging times.
- Advises on practical measures like washing hands, avoiding face-touching, practicing social distancing, and staying home when possible.
- Expresses confidence in the resilience of the American people to overcome challenges efficiently.

# Quotes

- "Almost positive, bad guys, right here."
- "Meme lords, please use your powers for good instead of evil."
- "Please be part of the solution here."

# Oneliner

Beau shares a story from his protective detail work, stressing the importance of timely information dissemination and criticizing comparisons that downplay threats during challenging times, urging meme creators to focus on spreading positive messages instead.

# Audience

Meme creators, general public

# On-the-ground actions from transcript

- Wash hands for 20 seconds with soap and hot water (suggested)
- Avoid touching your face (suggested)
- Practice social distancing (suggested)
- Stay home if possible (suggested)

# Whats missing in summary

The emotional impact of receiving potentially alarming information and the necessity of heeding advice from experts during crises.

# Tags

#PepTalks #DisseminatingInformation #Comparison #TimelyAdvice #CommunityResilience