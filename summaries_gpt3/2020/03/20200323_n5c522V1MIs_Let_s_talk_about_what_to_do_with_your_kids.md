# Bits

Beau says:

- Beau talks about the challenges parents are facing with kids at home due to the current situation, with phones ringing off the hook.
- He stresses the importance of thinking like a teacher, having a schedule, planning activities, and being mindful of kids' short attention spans.
- Beau advises on watching children's diet and ensuring they get physical activity, even if they can't go outside.
- He suggests using YouTube for exercise programs and educational videos for crafts and science projects.
- Beau encourages involving kids in household tasks and repairs, viewing it as an opportunity to teach them valuable skills.
- He reminds parents to cherish the time spent with their children during this period and to be patient and understanding.
- Beau recommends being empathetic towards teachers who will have to handle many students once schools reopen.
- For older kids, he suggests having meaningful and nostalgic conversations, seeing the positive side of the situation.
- Beau underlines the importance of maintaining a schedule, limiting activities, ensuring exercise, and providing proper meals for children.
- He acknowledges the challenges of the current situation but motivates to make the best of it and cherish the time spent with kids.

# Quotes

- "Have a schedule and keep their activities limited to an hour or so and make sure that they get some exercise."
- "You got a chance to spend a lot of time with your kids and there will be a time later when you wish you could have spent more."
- "They're a little out of sorts too. So give them a routine."
- "But just keep them engaged and break it up by time periods."
- "This is the opportunity to show them."

# Oneliner

Beau shares practical advice on creating a routine, engaging kids in activities, and cherishing the time spent with them during challenging times.

# Audience

Parents, caregivers

# On-the-ground actions from transcript

- Involve your kids in household tasks and repairs, teaching them valuable skills (implied)
- Have meaningful and nostalgic conversations with older kids (implied)
- Maintain a schedule for children's activities and ensure they get exercise (implied)

# Whats missing in summary

Beau's warm and encouraging tone and the emphasis on finding positives in spending time with children during challenging circumstances.

# Tags

#Parenting #Kids #Routine #FamilyTime #Cherish