# Bits

Beau says:

- Addresses government assistance stigma and advice from a privileged perspective.
- Criticizes common advice about planning better, getting a valuable education, finding work, and joining the army.
- Points out the hypocrisy of those who give advice but still accept government assistance.
- Questions whether any advice given doesn't apply to everyone.
- Acknowledges the importance of jobs that are now deemed non-essential during the crisis.
- Compares government assistance to a stimulus to get through tough times.
- Talks about how crises like poverty and lack of opportunities disproportionately affect those at the bottom.
- Encourages cashing the government assistance checks but warns against looking down on others in worse positions.
- Challenges the notion that people in Section 8 housing are the source of problems, instead pointing to those in power.
- Calls for destigmatizing government assistance as more people may need it soon.

# Quotes

- "Somebody with less money, less power, and less influence is not the source of anything going wrong in your life."
- "Those in a worse position than you are never the source of your problems."
- "The source of your problems do not emanate from Section 8 housing."
- "We need somebody to look down on and blame."
- "We need to destigmatize government assistance because we're all about to be on it."

# Oneliner

Beau addresses government assistance stigma, challenges privileged advice, and urges against blaming those in worse positions, advocating for destigmatization as more people may need assistance soon.

# Audience

Advocates for social justice

# On-the-ground actions from transcript

- Destigmatize government assistance (advocated)
- Avoid blaming those in worse positions (advocated)
- Challenge privilege and hypocrisy (advocated)

# Whats missing in summary

The full transcript provides a deep dive into societal attitudes towards government assistance and challenges individuals to rethink their perspectives on blaming those in need.

# Tags

#GovernmentAssistance #Stigma #Privilege #Destigmatization #SocialJustice