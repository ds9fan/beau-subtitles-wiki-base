# Bits

Beau says:

- Advocates for the concept of never kicking down and always punching up.
- Points out the danger of personifying a movement and the discouragement that comes when the personified figure loses.
- Talks about Bernie's diminishing chances of being the nominee but stresses that the battle is not just about one person.
- Shares that he has been interviewing candidates challenging big political names like Nancy Pelosi and Mitch McConnell.
- Mentions that these interviews will be released over the next few days and are worth watching for insights into the future of politics in the United States.
- Acknowledges technical issues in the interviews but encourages watching them for the content.
- Emphasizes that the fight for systemic change is about the ideas, not just one person.
- Expresses his interest in watching a good political fight without endorsing any specific candidates.

# Quotes

- "It's never the people below you who are the source of your problems."
- "If you become discouraged by that, you're personifying the movement."
- "This fight is not about one person. If you want systemic change in the United States, it's not about Bernie."
- "I don't endorse candidates. But I do love to watch a good fight."
- "Y'all have a good day."

# Oneliner

Beau advocates for punching up, warns against personifying movements, and interviews candidates challenging big political names, stressing that systemic change is about ideas, not individuals.

# Audience

Political enthusiasts

# On-the-ground actions from transcript

- Watch the upcoming interviews with candidates challenging big political names (suggested)
- Stay informed about progressive ideas and candidates in politics (implied)

# Whats missing in summary

Insights on specific progressive ideas discussed during the interviews. 

# Tags

#ProgressivePolitics #PunchingUp #SystemicChange #PoliticalCandidates #Interviews