# Bits

Beau says:

- Analyzing the $1200 stimulus checks and the reasoning behind the amount.
- Exploring the challenges faced by minimum wage workers in the United States.
- Addressing the difficulty in garnering empathy for minimum wage workers.
- Pointing out the propaganda surrounding minimum wage workers and the need for self-interest to drive change.
- Revealing how taxpayers end up subsidizing big businesses that pay low wages.
- Providing historical context on the minimum wage and Franklin Roosevelt's perspective.
- Arguing for a living wage for all Americans, not just a subsistence level.
- Emphasizing the importance of decent wages for all workers, especially those deemed "essential" during the current crisis.

# Quotes

- "You're paying for it. You are providing a subsidy to big business out of your pocket."
- "Minimum wage workers in this country are being exploited."
- "Everybody who's working should have a decent wage."
- "Essential employees [...] I think they deserve decent wages."
- "This is something we might want to look at."

# Oneliner

Analyzing the $1200 stimulus checks, the challenges of minimum wage workers, and the need for decent wages for all Americans, especially during crises.

# Audience

Taxpayers, Activists, Workers

# On-the-ground actions from transcript

- Advocate for fair wages for all workers (implied)
- Support policies that ensure a living wage for all Americans (implied)

# Whats missing in summary

Historical context and specific examples

# Tags

#StimulusChecks #MinimumWage #LivingWage #DecentWages #Workers'Rights