# Bits

Beau says:

- Urges taking action instead of waiting for federal leadership.
- Encourages state and local governments to protect their people.
- Emphasizes the importance of individuals educating themselves and taking responsibility.
- Calls for putting people's well-being over the economy.
- Criticizes scapegoating and division, advocating for cooperation and unity.
- Reminds everyone to pitch in and do what they can.
- Stresses the importance of demographic information in determining involvement levels.
- Mentions the significance of health care and leave as national security issues.
- Warns against panic and the need to focus on protecting vulnerable demographics.
- Calls for collective mobilization in the absence of federal leadership.

# Quotes

- "Everybody has a part to play."
- "Put people over the economy."
- "It's humanity against this thing."
- "The sooner we start to act, the more manageable this becomes."
- "It's just us. And we're going to have to work together."

# Oneliner

Beau urges action over waiting for federal leadership, prioritizing people, unity, and cooperation to combat the global crisis effectively.

# Audience

State and Local Governments

# On-the-ground actions from transcript

- Protect the people within your jurisdiction (exemplified)
- Educate yourself and learn from successful approaches (exemplified)
- Put people's well-being over the economy (exemplified)
- Avoid scapegoating and focus on cooperation and unity (exemplified)
- Volunteer or help those who need support (exemplified)
- Stay home if you're at high risk (exemplified)
- Focus on protecting vulnerable demographics (exemplified)
- Mobilize at a community level in the absence of federal leadership (exemplified)

# Whats missing in summary

The emotional urgency and call for collective action against a global crisis.

# Tags

#CommunityAction #Unity #Responsibility #Cooperation #GlobalCrisis