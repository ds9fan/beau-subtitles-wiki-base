# Bits

Beau says:

- Introduces the topics of probability, pedigree, universities, and trust.
- Shares a personal story about a 69-year-old friend who trusted misleading information from the administration.
- Expresses frustration at his friend's trust in the administration's denial of the seriousness of current events.
- Questions the wisdom of trusting an administration known for dishonesty with one's life.
- Contrasts the credibility of institutions like Harvard versus the administration in handling the situation.
- Emphasizes the importance of considering the pedigree and honesty of the sources of information.
- Advocates for listening to institutions like Harvard that prioritize caution and transparency.
- Encourages viewers to trust reputable institutions over those with a history of deception.

# Quotes

- "Imagine spending decades of your life being prepared for situations like this and then dying because you listened to a con man."
- "It's not about keeping calm for them. It's not about conveying calm. It's about conning people."
- "So please for a moment, most students at Harvard are not in an at-risk demographic. They're in that 0.2% range."
- "You can trust Harvard or you can trust Trump University."
- "Leadership from a respectable institution."

# Oneliner

Beau warns against trusting deceptive administrations like Trump University over credible institutions like Harvard during uncertain times.

# Audience

Public, Community Members

# On-the-ground actions from transcript

- Trust reputable institutions like Harvard for accurate information (implied).
- Prioritize caution and transparency in handling situations (implied).

# Whats missing in summary

The emotional impact of witnessing a friend's misplaced trust and the urgency of relying on trustworthy sources during crises.

# Tags

#TrustworthySources #CredibleInstitutions #Deception #Caution #CommunityLeadership