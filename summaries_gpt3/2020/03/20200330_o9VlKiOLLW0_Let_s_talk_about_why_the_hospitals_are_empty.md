# Bits

Beau says:

- People are filming outside empty hospitals, claiming it's proof that the situation is overblown and sensationalized.
- Deanna Lorraine, a Republican candidate, and Sarah A. Carter from Fox News contributed to spreading this theory.
- Hospitals are empty because non-urgent operations were shut down per Surgeon General's advice.
- Most people going to emergency rooms are not experiencing actual emergencies.
- Filming inside hospitals and questioning why patients aren't visible shows ignorance about isolation procedures.
- Many hospitals have restricted visitors and are keeping patients separated.
- People seek out these theories to find comfort in the chaos and believe in a sense of control.
- The President's statement on potential loss due to the pandemic underscores the seriousness of the situation.
- Taking precautions, staying at home, and following hygiene practices are critical.
- Actions like filming inside hospitals can increase risks and contribute to the severity of the situation.

# Quotes

- "You want to stand outside a hospital and film it, whatever. Enjoy your hobby."
- "There is no curtain. There's certainly not a man behind the curtain."
- "People like you, who are running into these places and going around town, you're the reason it's going to get real bad."

# Oneliner

People filming empty hospitals for conspiracy theories ignore the reality of medical procedures during a crisis and endanger public health by spreading misinformation.

# Audience

Social media users

# On-the-ground actions from transcript

- Stay at home, wash your hands, and avoid touching your face to prevent the spread of the virus (exemplified)
- Refrain from entering hospitals unnecessarily and filming inside medical facilities (exemplified)

# Whats missing in summary

The full transcript provides a detailed explanation of why hospitals appear empty, debunking conspiracy theories with factual information and urging responsible behavior during the pandemic. Watching the full video offers a comprehensive understanding of the importance of following guidelines and avoiding unnecessary risks.

# Tags

#Hospitals #ConspiracyTheories #Pandemic #PublicHealth #Misinformation