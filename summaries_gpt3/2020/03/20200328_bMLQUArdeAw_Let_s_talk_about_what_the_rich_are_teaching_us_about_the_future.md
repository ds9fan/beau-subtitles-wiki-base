# Bits

Beau says:

- Talks about the lessons to be learned from the current situation, focusing on access to resources.
- Points out how the wealthy have advantages in times of crisis like being able to access tests, fly out of affected areas, and isolate easily.
- Mentions that major events will continue to favor the wealthy.
- Predicts more incidents like the current crisis as climate change accelerates, leading to expensive necessities.
- Explains how production of necessary items is currently high due to demand and cost, but prices will drop significantly after the crisis.
- Advises stocking up on needed items once the crisis is over, especially for those who can make large purchases.
- Emphasizes the importance of being prepared with supplies that have long shelf lives and can benefit the community.
- Recommends having food reserves and mentions affordable food buckets with a month's supply.
- Encourages readiness for future crises by taking advantage of surplus supplies post-crisis.

# Quotes

- "The wealthy. The wealthy."
- "Get more than you need. Get more than your family needs. Because your community is going to depend on you."
- "It's not delicious, but it will keep you alive and it will give you a sense of comfort anytime the shelf is empty."

# Oneliner

Beau explains how the wealthy have advantages in crises, urges stocking up on essentials post-crisis to aid community preparedness, and stresses the importance of being ready for future emergencies.

# Audience

Prepared individuals and community members.

# On-the-ground actions from transcript

- Stock up on necessary items post-crisis (suggested).
- Ensure proper storage of essentials (implied).
- Purchase affordable food buckets for a month's supply (implied).

# Whats missing in summary

The importance of community cooperation and support during and after a crisis.

# Tags

#Preparedness #CommunitySupport #WealthDisparity #EmergencyPlanning #ClimateChange