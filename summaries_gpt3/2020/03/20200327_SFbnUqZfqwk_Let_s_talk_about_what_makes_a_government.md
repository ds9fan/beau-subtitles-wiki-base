# Bits

Beau says:

- Governments require the perception of authority and a monopoly on violence to exist.
- A government's obligation is to control, not protect, its citizens through force.
- Patriotism, loyalty, and obligation to protect citizens are not necessary for a government to exist.
- Governments can delegate parts of their monopoly on violence to the people.
- The key function of a government is to preserve itself, not necessarily to look out for its citizens.
- Governments can be small or large, with or without territory, as long as they have authority and a monopoly on violence.
- The governments' job is to control people, not to serve them.
- The perception of authority is vital for a government to maintain power.
- Governments can weaken and fail if the populace loses faith in their authority.
- Refusing to accept the authority of a government can be a form of resistance.

# Quotes

- "A government does not have to do anything for the people that it controls."
- "Patriotism, loyalty, all of these things. It's not a requirement. It isn't."
- "Governments don't need flags. They don't need songs. They don't need anything."

# Oneliner

Governments rely on the perception of authority and a monopoly on violence to control, not protect, citizens, with loyalty and patriotism being optional.

# Audience

Citizens, Activists, Community Members

# On-the-ground actions from transcript

- Challenge authority through peaceful resistance (implied)
- Support movements that question governmental control (implied)
- Educate others on the nature of government authority (implied)

# What's missing in summary

The full transcript delves into the essence of government authority, challenging commonly held beliefs about patriotism and loyalty while underscoring the fundamental role of control and force in governance. Watching the entire speech can provide a deeper understanding of these complex dynamics.

# Tags

#Government #Authority #MonopolyOnViolence #PerceptionOfAuthority #Control