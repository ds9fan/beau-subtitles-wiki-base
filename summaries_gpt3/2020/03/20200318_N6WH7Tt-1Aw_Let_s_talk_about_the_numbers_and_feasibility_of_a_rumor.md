# Bits

Beau says:
- Squashing a rumor gaining ground by acting and discussing feasibility.
- Addressing the idea of martial law being declared coast to coast in the U.S.
- Explaining the specifics of martial law versus states of emergency.
- Breaking down the numbers needed for martial law and why it's practically impossible.
- Mentioning the size difference between Iraq and the U.S. in terms of troops required.
- Detailing the mobilization process for troops in the U.S. to attempt martial law.
- Clarifying that even in extreme scenarios, true martial law is unlikely in the U.S.
- Emphasizing the importance of following guidelines and not resisting authorities.
- Urging people to practice basic hygiene and social distancing measures.
- Encouraging trust in medical professionals' advice to combat the current situation.

# Quotes
- "Wash your hands. Don't touch your face. Stay at home. Practice social distancing."
- "Please stop spreading that. You can get somebody hurt."
- "I cannot envision a scenario in which true martial law is declared in the United States."

# Oneliner
Beau debunks rumors of imminent martial law in the U.S., explaining why it's practically impossible and stressing the importance of following guidelines to combat the current situation.

# Audience
General public

# On-the-ground actions from transcript
- Wash your hands. Don't touch your face. Stay at home. Practice social distancing. (implied)
- Follow the advice of medical professionals. (implied)

# Whats missing in summary
The detailed breakdown and analysis of the numbers required for martial law can be best understood by watching the full transcript.

# Tags
#MartialLaw #Rumors #Debunking #Hygiene #SocialDistancing