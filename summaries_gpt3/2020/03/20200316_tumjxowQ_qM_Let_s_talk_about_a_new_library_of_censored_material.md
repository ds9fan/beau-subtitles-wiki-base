# Bits

Beau says:

- Introduces a new library in Minecraft that holds information not meant to be shared, created by Reporters Without Borders and BlockWorks.
- Describes Minecraft as a video game similar to digital Legos.
- Mentions that the library contains articles and targets individuals in countries with restricted access to independent journalism.
- Talks about the striking decor inside the libraries, like a cemetery in the Mexico section with tombstones of killed indie journalists.
- Provides the server IP visit.uncensoredlibrary.com for multiplayer access and encourages downloading the archive for hosting elsewhere.
- Emphasizes the significance of the project in ensuring information reaches its intended audience.
- Stresses the importance of access to accurate information during the formation of ideas and values.
- Reminds viewers of the various ongoing fights for information freedom worldwide.
- Asserts that censorship is ultimately futile, as information always finds a way to be shared.
- Encourages reflection on the broader implications beyond current headline news.

# Quotes

- "Information will always find a way to get to where it needs to be."
- "Who's going to see this the most? People."
- "There are countries that have much more severe problems in that regard."
- "It's not going to work. It has never worked."
- "There will always be somebody willing to get that information out in some fashion."

# Oneliner

A Minecraft library defies censorship, ensuring information reaches those who need it most, showing that information always finds a way.

# Audience

Gamers, activists, information seekers.

# On-the-ground actions from transcript

- Visit visit.uncensoredlibrary.com to access the Minecraft library (suggested).
- Download the entire archive to host it in another location (suggested).

# Whats missing in summary

The full transcript provides a deeper insight into the innovative ways information can be shared and the importance of combating censorship on a global scale.

# Tags

#Minecraft #Censorship #InformationFreedom #ReportersWithoutBorders #BlockWorks