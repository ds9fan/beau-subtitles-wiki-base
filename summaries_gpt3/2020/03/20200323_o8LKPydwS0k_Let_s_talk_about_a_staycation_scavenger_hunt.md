# Bits

Beau says:

- Encourages a scavenger hunt to build bug out bags at home for emergency preparedness.
- Rules: only use items at home, nothing from existing kits, and items once in the bag stay in the bag.
- Goal is to increase the number of people prepared for emergencies.
- Participants to post photos of their bug out bags on social media under the hashtag BowBag for learning and sharing.
- Items in the bag should include food, water, fire, shelter, medicine, and a knife, with flexibility in interpretation.
- Beau will choose the best bag and send a reward.
- Reminder to continuously improve the bag for different emergency scenarios.
- Emphasizes the importance of being prepared for various emergencies.


# Quotes

- "We are all going to build bug out bags out of the stuff that we have at home."
- "We are going to exponentially increase the number of people who are prepared for an emergency."
- "Improving on it, in this scenario, yeah, everybody's staying at home. In the next scenario, you may not be able to."

# Oneliner

Beau encourages building bug out bags at home using only household items, fostering emergency preparedness for various scenarios and sharing for learning.

# Audience

Individuals at home

# On-the-ground actions from transcript

- Build a bug out bag using items at home and follow the guidelines provided (suggested)
- Share a photo of your bug out bag on social media under the hashtag BowBag (suggested)
- Continuously improve and update your bug out bag for different emergency scenarios (suggested)

# Whats missing in summary

Engaging in the scavenger hunt and building a bug out bag can enhance emergency preparedness and readiness for unforeseen circumstances.

# Tags

#EmergencyPreparedness #BugOutBag #HomePreparedness #CommunityAction #SocialMedia