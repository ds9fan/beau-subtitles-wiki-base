# Bits

Beau says:

- Acknowledges the importance of workers in grocery stores, big box stores, gas stations, and pharmacies during the current crisis.
- Points out the societal misconception that jobs with plastic name tags are perceived as inferior due to poor life decisions.
- Stresses that these jobs are actually essential and integral to society regardless of character or decisions made.
- Calls for maintaining class solidarity and recognizing the necessity of these roles even after the crisis ends.
- Argues that individuals in these vital positions should have a decent standard of living, including financial security and health insurance.
- Challenges the notion that these jobs can be done by anyone, citing the skill and importance they hold.
- Urges society to use the current situation as a learning experience to bring about necessary changes.
- Encourages supporting these workers by backing politicians who advocate for their well-being come election time.
- Emphasizes that ensuring a decent standard of living for these workers should be the minimum effort society puts forth.

# Quotes

- "If a job is being performed and it is essential, it must occur for society to function."
- "I think that's the bare minimum that we can do as a society."

# Oneliner

Recognize the vital role of low-wage workers, ensure decent living standards, and support them politically for a fair society.

# Audience

Workforce advocates

# On-the-ground actions from transcript

- Support politicians advocating for decent living standards for low-wage workers (implied).
- Recognize and appreciate the importance of workers in grocery stores, big box stores, gas stations, and pharmacies (implied).

# Whats missing in summary

The importance of valuing and respecting all types of work, especially during crises, and advocating for lasting changes to uplift low-wage workers. 

# Tags

#LowWageWorkers #EssentialWorkers #Society #DecentLivingStandards #Support