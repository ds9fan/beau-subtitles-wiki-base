# Bits

Beau says:

- Addressing the current state of news footage resembling a post-apocalyptic film montage.
- Expressing disappointment in the lack of a plan to deal with the situation.
- Mentioning the concept of the "deep state" and its role in providing continuity of government.
- Criticizing the focus on the economy over the well-being of those at risk.
- Advocating for utilizing existing plans and experts instead of creating new ones.
- Suggesting that governors or National Guard could step in to help citizens if the administration fails to act.
- Emphasizing the importance of handwashing, social distancing, and staying at home for those at risk.
- Noting the reluctance of the federal government to share information to avoid causing worry about the economy.
- Pointing out the economic impact of uncertainty and the failure to protect what should be prioritized during the crisis.
- Encouraging proactive measures to address the situation effectively.

# Quotes

- "The longer this goes on, the worse the economy is going to get."
- "We have the plans. We don't need the president."
- "Wash your hands, social distancing, if you're at risk, stay home."
- "The strategies and tactics used to contain it, they exist, and they're going to be the same."
- "He's failing at protecting that."

# Oneliner

Beau addresses the lack of a comprehensive plan to deal with the crisis, criticizes the focus on the economy over people's well-being, and advocates for utilizing existing resources effectively.

# Audience

Citizens, Governors

# On-the-ground actions from transcript

- Reach out to governors, National Guard, and Department of Defense to ensure preparedness (suggested)
- Practice handwashing, social distancing, and stay at home if at risk (implied)

# Whats missing in summary

The full transcript provides detailed insights on the importance of utilizing existing resources and experts effectively during a crisis, rather than solely focusing on economic concerns. It also underlines the significance of proactive measures and the need for proper communication from the government.

# Tags

#CrisisManagement #GovernmentResponse #Economy #PublicHealth #Preparedness