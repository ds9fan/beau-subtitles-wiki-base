# Bits

Beau says:

- Addresses the importance of signals, codes, videos, and theories in a quick and concise manner.
- Mentions an event down south where individuals got caught and appeared in videos.
- Talks about the shift in standards regarding making videos for propaganda purposes.
- Explains the theory that information becomes obsolete quickly in today's fast-moving battlefield.
- Notes that certain signals in videos can convey messages without being overtly noticed.
- Refers to a Wall Street Journal article that debunked conspiracy theories due to the likelihood of people talking.
- Criticizes conspiracy theories that paint the opposition as infallible and the system as unbeatable.
- Advocates for evolution over revolution to change the system by creating a redundant power structure based on cooperation.
- Emphasizes the importance of building the future rather than fighting against the past.
- Concludes by urging for a shift towards evolution instead of revolution to create the desired system.

# Quotes

- "We don't need revolution. We need evolution."
- "We can change the system at play. We can change the way the land. We can make them obsolete."
- "We don't get the system we want by fighting against the past. We get it by building the future."

# Oneliner

Beau addresses the importance of signals, codes, and videos, debunking conspiracy theories and advocating for evolution over revolution to create a better future.

# Audience

Activists, Advocates, Change-makers

# On-the-ground actions from transcript

- Build a redundant power structure based on cooperation (implied)
- Focus on evolving systems rather than resorting to revolution (implied)

# Whats missing in summary

The full transcript delves into the significance of information dissemination, debunking conspiracy theories, and promoting a shift towards evolution for societal change.

# Tags

#Signals #Codes #Videos #ConspiracyTheories #Evolution #Revolution