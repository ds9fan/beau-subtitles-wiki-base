# Bits

Beau says:

- Urges viewers to watch the entire video if it was sent to them, especially if it seems directed at them.
- Expresses frustration at receiving messages from young people asking how to communicate with family members influenced by biased media.
- Notes the divisive tactics employed by political outlets to drive a wedge between generations, especially in the lead-up to elections.
- Advises viewers to stop consuming biased information for 28 days to gain new perspectives and reduce anger.
- Suggests reputable news sources like Reuters, AP, NGOs, and Human Rights Watch for more balanced reporting.
- Warns against believing opinions are equivalent to facts and consuming sources that fuel anger and division.
- Criticizes news outlets that prioritize commentary over objective reporting and manipulate viewers with leading questions.
- Emphasizes the importance of choosing between family harmony and political affiliations.
- Condemns the impact of biased media on American families and the perpetuation of anger and division.
- Encourages viewers to break the cycle of consuming divisive information and observe positive changes in their outlook.

# Quotes

- "Stop consuming that information for twenty eight days. Just stop."
- "This habit that is forming is literally destroying American families."
- "You're destroying your families for the sake of somebody who reveled in the fact that he was supported by the uneducated."
- "You need to make a choice about what's more important to you. Your family or Red Hat?"
- "Y'all have a good day."

# Oneliner

Beau urges viewers to take a break from biased media consumption for 28 days to mend family divides and foster new perspectives.

# Audience

Family members influenced by biased media

# On-the-ground actions from transcript

- Stop consuming biased information for 28 days (suggested)
- Seek news from reputable sources like Reuters, AP, NGOs, and Human Rights Watch (suggested)

# Whats missing in summary

The full video provides deeper insights into the damaging effects of biased media consumption on families and society, advocating for a conscious choice between divisive politics and family unity.

# Tags

#MediaConsumption #FamilyUnity #PoliticalBias #ReputableSources #CommunityHarmony