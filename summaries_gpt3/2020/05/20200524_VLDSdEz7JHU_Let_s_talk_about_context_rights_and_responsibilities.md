# Bits

Beau says:

- Addresses rights and responsibilities in a video that was condensed and edited without full context.
- Believes that keeping the overall intent and message intact despite lacking context is beneficial for promoting ideas.
- Acknowledges that the edited video lacks context, but supports the approach taken.
- Notes that the Constitution's intent was to establish a more perfect union, ensure justice, domestic tranquility, defense, welfare, and liberty.
- Emphasizes that every right comes with an inherent responsibility.
- Raises concerns about individuals quoting founders without understanding the context of their quotes.
- Questions whether people today are motivated by quotes without context or by the larger intent defined in the Constitution.
- Stresses the importance of understanding the context to avoid drawing incorrect conclusions.
- Encourages viewers to seek and appreciate context when forming opinions about historical matters.
- Reminds that the Constitution's purpose was not to cater to selfish interests but to create a better society.

# Quotes

- "Every right has an inherent responsibility."
- "Maybe if you're going to have an opinion about what somebody meant 200 years ago, you need context."
- "Context is important."
- "In order to form a more perfect union, establish justice, ensure domestic tranquility..."

# Oneliner

Beau addresses the importance of context in understanding rights and responsibilities, urging viewers to appreciate the broader intent of the Constitution.

# Audience

History enthusiasts

# On-the-ground actions from transcript

- Seek context before forming opinions (implied)
- Educate oneself on the larger intent of historical documents (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of the relationship between rights, responsibilities, and context in historical contexts.

# Tags

#Rights #Responsibilities #Context #Constitution #History