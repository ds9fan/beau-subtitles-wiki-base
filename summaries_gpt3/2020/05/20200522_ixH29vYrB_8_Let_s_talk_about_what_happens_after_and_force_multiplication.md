# Bits

Beau says:

- Received a message referencing a self-defense video in Cincinnati, sparking a larger point about what comes after.
- Proposes a plan where individuals are trained to teach others, creating a network of millions ready to fight for change in less than a year.
- Emphasizes the importance of preparation for what happens after any potential revolution or change.
- Questions the effectiveness of movements that lack a clear post-revolution vision and systems in place.
- Advocates for decentralized power through education and embracing new ideas.
- Acknowledges the dirty and ugly reality of real-life revolutions, contrasting with glorified perceptions.
- Stresses the need to reach those clinging to old ideas before meaningful change can occur.
- Urges for building community networks, establishing redundant systems, and focusing on new ideas over violence.
- Shares an example of a class in Cincinnati focusing on sustainable farming, community networks, and mutual aid instead of revolutionary tactics.
- Refuses to teach offensive tactics to avoid inciting unnecessary violence and hostility.

# Quotes

- "If you don't have an answer to that, it means nothing."
- "I don't want to be one of the people who makes peaceful revolution impossible."
- "Without the new ideas to back it up, it's not a revolution. It's just a hostile takeover."

# Oneliner

Beau stresses the importance of planning for what comes after any revolution and advocates for decentralized power through education and new ideas.

# Audience

Change advocates

# On-the-ground actions from transcript

- Build community networks and establish redundant systems (implied)
- Focus on embracing new ideas and education (implied)
- Engage in mutual aid activities (exemplified)

# Whats missing in summary

The full transcript provides a deeper understanding of the importance of post-revolution planning and the necessity of embracing new ideas for meaningful change.

# Tags

#Revolution #CommunityEmpowerment #DecentralizedPower #Education #NewIdeas