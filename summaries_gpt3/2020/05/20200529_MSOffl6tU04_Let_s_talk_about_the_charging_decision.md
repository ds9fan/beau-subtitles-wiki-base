# Bits

Beau says:

- Provides an update and a follow-up after a wild week following his cautious initial analysis.
- Expresses disappointment in the delayed arrest and the choice of charges in the case.
- Questions the lack of evidence regarding Mr. Floyd's intent and criticizes the charges brought against the officer.
- Believes that the current charges are not sufficient and calls for fighting the necessary fights.
- Raises concerns about the state of the country, including the economy, COVID-19 deaths, media suppression, and the justice system.
- Expresses hope for a better America amidst the chaos.

# Quotes

- "Why'd you let the city burn? Could have charged that on day one."
- "You fight the fights that need fighting."
- "We have an economy in shambles. We have a hundred thousand gone due to inaction."
- "We have a broken justice system."
- "I really hope that America is great now. Because I don't think I can stand for it to get much greater."

# Oneliner

Beau provides a critical update on the delayed arrest, questionable charges, and the need for fighting necessary battles amidst a chaotic America.

# Audience

Activists, Justice Advocates

# On-the-ground actions from transcript

- Fight for the necessary charges and justice (implied)
- Advocate for transparency and accountability in law enforcement (implied)
- Stand up against injustice and inequality (implied)

# Whats missing in summary

The emotional impact and urgency conveyed by Beau's words.

# Tags

#Justice #PoliceReform #Activism #SystemicChange #SocialJustice