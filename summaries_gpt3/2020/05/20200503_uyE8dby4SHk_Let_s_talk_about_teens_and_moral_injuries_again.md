# Bits

Beau says:

- Teens are experiencing moral injury due to government response to the pandemic, feeling wronged and demoralized.
- Moral injury involves perpetrating, failing to prevent, or witnessing a gross transgression of morals.
- Teens face a systemic failure, not a singular act, realizing the facade of society crumbling.
- Suffering a moral injury leads to demoralization and self-handicapping behavior.
- Belief in the just world hypothesis and self-esteem are helpful in coping with moral injury.
- Teens can generate self-esteem by realizing they can be the change in a flawed system.
- Younger people are the driving force for change and can make a difference through consistent actions.
- Involvement in fighting against systemic failures requires a long-term commitment and patience.
- Government is critiqued for downplaying threats and putting families at risk for political gain.
- Younger generations are encouraged to challenge the status quo and work towards a better future.
- Beau urges teens to take action, do something daily for 28 days, and be the catalyst for positive change.

# Quotes
- "You want to change the world. You want to overcome the moral injury. You want to change the system that let you down. You can. Just got to get in the fight."
- "It's always the younger generation that actually changes the world."
- "You're more capable of changing the world than people my age."

# Oneliner
Teens facing moral injury from systemic failures urged to take daily action for positive change and overcome demoralization.

# Audience
Teens, Activists

# On-the-ground actions from transcript
- Get involved in the fight against systemic failures by doing one thing each day for 28 days (exemplified).
- Challenge the status quo through consistent actions and advocacy (implied).

# Whats missing in summary
Importance of younger generations in driving positive change and the need for long-term commitment for systemic transformation.

# Tags
#Teens #MoralInjury #SystemicFailures #YouthActivism #PositiveChange