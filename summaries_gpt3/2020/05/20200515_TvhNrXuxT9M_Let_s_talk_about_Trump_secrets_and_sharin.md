# Bits

Beau says:

- Talks about the importance of sharing information during critical times.
- Criticizes the White House's messaging on sharing information about the pandemic.
- Mentions the FBI warning American researchers about potential Chinese theft of research.
- Questions the lack of sharing valuable information for the sake of profit.
- Expresses concern about prioritizing money over public health.
- Advocates for a reevaluation of society and the system's values.

# Quotes

- "In times like these, when lives literally hang in the balance, money is what matters."
- "We've got to make a buck. Information related to what amounts to a cure, we're not going to share it."
- "It doesn't matter that failing to share it may delay development."
- "The weight of the US government will back up the idea that it's okay to delay to make a few extra bucks."
- "It's probably something we want to look at."

# Oneliner

Beau criticizes the prioritization of profit over public health, questioning the lack of sharing vital information during critical times.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Advocate for transparent and open sharing of information (implied)
- Support policies that prioritize public health over profit (implied)

# Whats missing in summary

Beau's passionate delivery and nuanced arguments

# Tags

#Sharing #Information #PublicHealth #Government #Profit