# Bits

Beau says:

- Thermostats in big office buildings and commercial places are often placebos, giving employees the illusion of control without actually doing anything when adjusted.
- Crosswalk buttons are similar to thermostats, often not connected to anything and merely synced with traffic signals for cars.
- Voting may not be the most effective means of civic engagement, but it is a tool that can be used to exploit the naturally occurring power vacuum in representative democracies.
- To take over the Democratic Party, focus on lower offices like sheriff, county commission, and state legislature, as they matter more than the presidency or Senate for deep systemic change.
- Real leadership starts at the bottom, with positions like sheriff and state legislature being easier to win and requiring less money and influence.
- By filling these lower offices with real progressives who think similarly, they can eventually move up into positions of real leadership as the older establishment politicians age out.
- Supporting politicians in attainable positions who want deep systemic change is key to altering the overall view of the democratic establishment.
- The strategy involves a long fight and gradual change, revolution by degrees rather than immediate wins.
- Influencing elections through community networks can help get people in office who mirror your ideals.
- The approach requires patience, as real leadership and systemic change start at the bottom.

# Quotes

- "Voting is not an extremely effective means of civil engagement, civic engagement, but it is what it is."
- "Real leadership starts at the bottom, not at the top."
- "Revolution by degrees, I guess."
- "It's a long fight. It takes a long time, but it's something that will work."
- "Your community network, that can help influence an election."

# Oneliner

Thermostats and crosswalk buttons as metaphors for illusory control lead Beau to advocate for seizing the power vacuum in representative democracies and focusing on lower offices for deep systemic change within the Democratic Party.

# Audience

Progressive activists

# On-the-ground actions from transcript

- Organize and support real progressives running for lower offices like sheriff, county commission, state legislature (exemplified)
- Build a community network to influence elections and support candidates reflecting your ideals (exemplified)

# Whats missing in summary

The full transcript provides detailed insights on leveraging voting and focusing on attainable positions to drive deep systemic change within the Democratic Party.