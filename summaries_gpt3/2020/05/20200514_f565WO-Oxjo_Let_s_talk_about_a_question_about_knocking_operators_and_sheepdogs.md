# Bits

Beau says:

- Addressing a question about banning certain types of entries, specifically no-knock entries, in law enforcement. 
- Stating that no-knock entries shouldn't be banned outright, but are overused (98% of the time) and inherently risky.
- Explaining the appropriate circumstances for a no-knock entry: when the suspect is dangerous and likely to flee.
- Emphasizing that no-knock entries should not be used for suspects who are not dangerous, as the risk is not justified.
- Suggesting a more coordinated approach where regular cops serve the warrant while a team of operators apprehend the suspect to reduce risk.
- Criticizing the lack of training and overreliance on firepower in these operations, leading to risks for innocent civilians.
- Urging law enforcement to prioritize preparation, intelligence, surveillance, and training over dynamic entries.
- Calling for significant reforms in law enforcement practices to prevent further unnecessary loss of life due to negligence.

# Quotes

- "The risk is absorbed by the people that you're ostensibly there to protect."
- "If your guys can't consistently put their rounds into a 5x7 card under pressure, real pressure, they have no business doing this."
- "Any loss of life is firmly on the hands of the department."

# Oneliner

Addressing the overuse and risks of no-knock entries, Beau calls for significant reforms in law enforcement to prioritize training and preparation over dynamic entries, to prevent unnecessary loss of life.

# Audience

Law Enforcement, Police Officers

# On-the-ground actions from transcript

- Train law enforcement officers to prioritize preparation, intelligence, and surveillance over dynamic entries (implied)
- Implement significant reforms in law enforcement practices to prevent unnecessary loss of life due to negligence (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the risks and overuse of no-knock entries in law enforcement, urging for reforms to prioritize training and preparation to prevent unnecessary loss of life due to negligence.

# Tags

#LawEnforcement #Reforms #Training #NoKnockEntries #RiskMitigation