# Bits

Beau says:

- Beau contrasts two historical figures who aimed to leave their mark on the world but had different approaches and outcomes.
- One figure, from a prominent political family, faced setbacks, including being captured by pirates, and worked to prosecute corrupt government officials.
- Despite facing challenges and setbacks, the figure did not inspire fear in his captors and ultimately accomplished significant achievements.
- The other figure was impulsive, setting fire to the Temple of Artemis to make an immediate impact.
- Beau points out the importance of understanding the long, challenging road to making a lasting change in the world.
- He warns that activism is exhausting and requires a long-term commitment to see real progress.
- Beau stresses the need for activists to realize that change won't happen overnight but through persistent daily efforts.
- Making little improvements each day is emphasized as a way to contribute to larger change over time.

# Quotes

- "You're going to have to be in it for a while."
- "There'll be little changes every day and you can make little improvements every day."
- "World's not going to change tomorrow, not completely."
- "To get to where you're going, where you want to go, it's a long road."
- "You've chosen a pretty big battleground."

# Oneliner

Beau contrasts two historical figures' approaches to leaving their mark on the world and stresses the need for a long-term commitment to create lasting change.

# Audience

Activists, changemakers

# On-the-ground actions from transcript

- Build and improve gradually (exemplified)
- Make little improvements daily (exemplified)
- Understand the long-term commitment required for activism (exemplified)

# Whats missing in summary

The full transcript delves into historical lessons about success and making an impact by contrasting two figures from the past. Watching the full video can provide a deeper insight into the stories shared and the importance of perseverance in enacting change.

# Tags

#Activism #Change #LongTermCommitment #History #Impact