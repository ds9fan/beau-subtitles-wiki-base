# Bits

Beau says:

- Beau introduces the topic of voting, philosophies, Biden, and the decision to vote.
- Beau acknowledges the diverse viewpoints of his audience regarding voting for Biden.
- The dilemma of choosing between Biden and Trump is discussed, with neither candidate being a perfect choice.
- He addresses the concept of lesser of two evils voting, a common scenario in the United States.
- Beau talks about how some philosophies view voting as taboo due to the moral responsibility attached to it.
- The importance of considering the potential members of the candidates' administrations is emphasized.
- Beau delves into the struggle of individuals with radical philosophies to find reasons to vote for Biden.
- The moral dilemma faced by those with certain beliefs on voting is explored.
- Beau touches on the notion of harm reduction and voting as a form of defense for marginalized communities.
- The significance of rejecting certain harmful ideologies is discussed in relation to the upcoming election.
- Beau expresses the importance of voting based on conscience and personal beliefs.
- He refrains from endorsing any particular candidate, leaving the decision to the individual's moral judgement.

# Quotes

- "It's not unique. It feels unique but the reality is this lesser of two evils voting it's what we normally end up with in the United States."
- "People who believe in one of those philosophies, they want everybody to get a fair shake. They want everybody to have some form of representation."
- "Your conscience tells you, you understand it's not what your philosophy dictates, but these people need help."
- "Your moral obligation if you are going to participate in voting, is to vote your conscience."
- "It's sad that the base is already this lackluster in support, but a whole lot of people asked the waiter for a 100 proof bottle of truth, and the waiter brought back Trump lite."

# Oneliner

Beau addresses the dilemma of voting for Biden, the concept of harm reduction, and the importance of voting based on conscience and moral beliefs.

# Audience

Voters, Philosophers

# On-the-ground actions from transcript

- Vote based on conscience and personal beliefs (suggested)
- Support harm reduction for marginalized communities through voting (implied)

# Whats missing in summary

The full transcript provides a nuanced exploration of ethical voting dilemmas and considerations surrounding harm reduction for marginalized communities.

# Tags

#Voting #Biden #Philosophy #Ethics #HarmReduction