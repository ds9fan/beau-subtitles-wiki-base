# Bits

Beau says:

- Beau suggests taking Mitch McConnell's advice on the stimulus package, despite their usual differences.
- McConnell has expressed no urgency in passing the next stimulus package, wanting to see the effects of previous spending first.
- Beau analyzes McConnell's campaign contributions from the last five years, focusing on companies contributing significant amounts annually.
- McConnell has received substantial donations from big investment groups, Altria Group (Philip Morris), and the health care industry.
- Campaign contributions seem to influence McConnell's stance on health care accessibility and profitability.
- Beau points out donations from Geo Group, a private prison and real estate investment company.
- Companies like FedEx and UPS also contribute to McConnell, potentially influencing his policies regarding competitors like the United States Postal Service.
- Beau urges viewers to research their representatives' campaign contributions on OpenSecrets.org to understand the influence of money in politics.
- Beau criticizes the significant role of money in shaping policies, suggesting that the common people lack effective lobbyists in the political system.
- In light of economic experts predicting ongoing struggles, Beau calls for urgency in providing stimulus to those in need.

# Quotes

- "Maybe you should display a little bit of urgency, given the fact that all of the experts are saying that we're going to be going through this ups and downs for a while."
- "The Senate and the House, they could give a master class on corruption to all of these smaller governments that we tend to criticize."
- "I strongly suggest you do this. Go see who has contributed to your candidate, to your representative."

# Oneliner

Beau suggests analyzing Mitch McConnell's campaign contributions to understand his stance on key issues like the stimulus package and health care accessibility.

# Audience

Voters, activists, constituents

# On-the-ground actions from transcript

- Research and analyze campaign contributions of your representatives on OpenSecrets.org (suggested)
- Advocate for urgency in providing stimulus to those in need, contacting elected officials if necessary (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of how campaign contributions may influence political decisions, urging viewers to scrutinize the influence of money in politics for a deeper understanding.

# Tags

#MitchMcConnell #StimulusPackage #CampaignContributions #HealthCare #PoliticalInfluence