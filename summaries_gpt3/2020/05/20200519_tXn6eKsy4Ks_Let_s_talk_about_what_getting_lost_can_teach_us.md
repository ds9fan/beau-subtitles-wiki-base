# Bits

Beau says:

- Beau introduces the idea of getting lost and what it can teach us as a country and a world.
- In search and rescue, the first thing instructors teach is that lost individuals tend to do nothing to help themselves.
- There is a strong impulse to keep moving once someone is lost, which Beau attributes to various factors such as evolutionary instincts or optimism.
- Beau advises that if you get lost, the best course of action is to sit down for 20-30 minutes to analyze the situation and come up with a plan.
- Even skilled individuals like Ralph Bagnold, a desert explorer, have felt the urge to keep moving when lost.
- Moving without a plan can lead to misguided decisions, like following the sun as a navigation strategy.
- Beau draws parallels between being physically lost and the state of the country and world, noting that many people recognize the current system is not ideal but continue to move without a clear plan.
- He urges for a collective pause to come up with a plan and choose a course for the future that prioritizes cooperation over competition.
- Beau stresses the importance of making a conscious choice to advocate for cooperation and shift towards a more sustainable path.
- He concludes by underlining the critical need to address the state of being lost at a national, global, and species level, advocating for proactive change.

# Quotes

- "We're lost. We're lost as a country. We are lost as a world. We're lost as a species."
- "We need to come up with that course, and we need to get moving."
- "This is not sustainable. It's not going to last."
- "If we would sit down and come up with a plan, we'd be able to chart a course."
- "We need to make the conscious choice to start advocating for that."

# Oneliner

Beau introduces the concept of getting lost, paralleling it with the directionless state of the country and world, urging for a collective pause to plan a cooperative and sustainable future.

# Audience

Global citizens

# On-the-ground actions from transcript

- Pause collectively to analyze current situations and challenges, then develop a plan for the future (exemplified)
- Advocate for cooperation over competition in all aspects of society (exemplified)

# Whats missing in summary

The full transcript provides a thought-provoking analogy between getting physically lost and the directionless state of society, urging for proactive planning and advocacy for cooperation to overcome challenges.

# Tags

#Lost #Cooperation #Sustainability #CollectiveAction #Advocacy