# Bits

Beau says:

- President Trump is campaigning against expanding mail-in voting, which could benefit the working class who may find it difficult to take time off to vote.
- Trump's voter base mainly consists of older individuals, particularly those aged 65 and above, who heavily rely on mail-in voting.
- Trump fears expanding voting access to younger individuals who may not support him, as his base is primarily composed of retired or financially stable individuals.
- Voter suppression is a term used to describe Trump's actions to limit voting access under the guise of preventing fraud.
- Trump's concerns about voter fraud are often a cover for his fear of losing elections.
- Trump's administration has consistently shown a lack of concern for the average American and their voices.
- Beau believes that Trump prioritizes his approval ratings over the interests of the working class.
- The military has been voting by mail for decades, showcasing the hypocrisy in Trump's opposition to expanding mail-in voting.
- Beau suggests that Trump's resistance to expanding voting access stems from a desire to cater to his core supporters who are older and watch Fox News regularly.

# Quotes

- "Voter suppression is a term for it."
- "Trump doesn't represent the working class."
- "He doesn't care about the average American."
- "Probably don't want that expanded either."
- "He just wants those old people who sit at home and watch Fox News all day."

# Oneliner

President Trump campaigns against expanding mail-in voting, fearing increased voter turnout from demographics less likely to support him, like the working class.

# Audience

Voters, Advocates

# On-the-ground actions from transcript

- Contact local officials to advocate for expanded mail-in voting access (suggested)
- Join organizations working to combat voter suppression (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of President Trump's stance on mail-in voting, offering insights into his motivations and the implications for different voter demographics.

# Tags

#MailInVoting #VoterSuppression #WorkingClass #TrumpAdministration #ElectionFraud