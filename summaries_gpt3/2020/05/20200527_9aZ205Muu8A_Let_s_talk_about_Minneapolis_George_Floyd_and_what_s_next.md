# Bits

Beau says:

- Breaks down events involving law enforcement captured on tape, analyzing the chain of events and policies involved.
- Does this for accountability and to provide closure to families of victims.
- Expresses uncertainty and disbelief regarding the George Floyd incident in Minneapolis.
- Urges viewers to watch a video from a year ago that outlines unjust killings by cops.
- Points out the difference between previous incidents and the prolonged, unacceptable nature of Floyd's case.
- Speculates on missing information that could make the situation even more tragic.
- Raises questions about the officers' histories, Floyd's state of mind, and prior interactions.
- Expresses discomfort with the ambiguity and unexplainable nature of the incident.
- Calls for full transparency from the Minneapolis Police Department through releasing all information.
- Stresses the importance of understanding every detail to prevent such incidents from recurring.

# Quotes

- "You can't fix it if you don't know what's broken."
- "This isn't confusion. This is unexplainable."
- "Everybody knows that those techniques lead to that."
- "We need to know what happened here."
- "I think there's more information that is going to make it more wrong."

# Oneliner

Beau breaks down police incidents for accountability, but the George Floyd case leaves him questioning and anticipating more tragedy with missing information.

# Audience

Advocates for justice

# On-the-ground actions from transcript

- Contact the Minneapolis Police Department for full transparency on the George Floyd incident (implied)
- Advocate for the release of all video footage, audio recordings, and radio transcripts related to the incident (implied)

# Whats missing in summary

The emotional impact and depth of analysis in Beau's breakdown is best experienced in the full transcript. 

# Tags

#Accountability #GeorgeFloyd #PoliceIncidents #Transparency #Justice