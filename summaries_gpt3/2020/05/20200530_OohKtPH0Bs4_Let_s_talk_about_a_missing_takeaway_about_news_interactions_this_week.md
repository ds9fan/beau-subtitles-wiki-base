# Bits

Beau says:

- Journalists, correspondents, and reporters had unfavorable interactions with law enforcement this week, sparking a debate on free speech implications.
- The prevailing narrative that these events will chill freedom of the press is inaccurate; journalists are not deterred and often see such incidents as cool stories.
- The more critical takeaway is that these journalists were not threats but still faced force from law enforcement.
- Law enforcement's poor ability to accurately threat-assess is concerning; they should not be able to determine when force is necessary if they can't discern non-threatening individuals.
- Beau suggests prioritizing threat assessment training over the warrior cop mentality in law enforcement.
- He challenges the perception of policing as a highly dangerous job by citing statistics showing other professions, like fishermen and roofers, face higher risks.
- The accuracy of police actions, especially in determining threats, should be held to a higher standard by the public.
- Beau stresses the need to address the issue promptly, urging communities to push for reforms within their local police departments.

# Quotes

- "None of these journalists were threats. None of them could be mistaken as a threat, but they had force used against them."
- "Law enforcement has a really poor record of being able to threat-assess."
- "Being a cop isn't even in the top 10 [most dangerous jobs]."
- "Only you can stop city fires."
- "This is something that needs to be addressed."

# Oneliner

Journalists face force from law enforcement despite not being threats, revealing a critical flaw in threat assessment within policing that needs immediate attention.

# Audience

Community members, Activists, Journalists

# On-the-ground actions from transcript

- Push for threat assessment training in local police departments (implied)
- Advocate for reforms addressing the accuracy of police actions in determining threats (implied)

# Whats missing in summary

The full transcript provides additional context on the flawed perception of policing as highly dangerous and the need for communities to actively address issues within law enforcement.

# Tags

#Journalism #LawEnforcement #ThreatAssessment #CommunityAction #PoliceReform