# Bits

Beau says:

- Acknowledges the irony and humor in life, discussing freedom of movement.
- Timing is key for discussing hypocrisy effectively.
- Points out the usefulness of calling out hypocrisy in emotional, illogical situations.
- Urges people to stop likening stay-at-home orders to Gestapo tactics.
- Notes the hypocrisy of those complaining about movement restrictions now.
- Compares reactions to movement restrictions at the southern border versus current stay-at-home orders.
- Raises the issue of privilege and safety concerns.
- Questions which group, those crossing borders or those violating stay-at-home orders, has caused more harm.
- Challenges the idea of citizenship granting special rights in the context of following the law.
- Reminds people of the consequences faced by others in authoritarian situations.

# Quotes

- "I wonder what would happen if you added up those lost that were attributable to each group."
- "It's almost like if you cheer on a government when they behave in an authoritarian manner, they become more authoritarian."
- "Those people on the bottom. The working class. All over the world."

# Oneliner

Beau addresses hypocrisy in reactions to movement restrictions, challenging privilege and safety concerns, and the consequences of authoritarian behavior.

# Audience

Social justice advocates

# On-the-ground actions from transcript

- Question hypocrisy and privilege (exemplified)
- Educate others on the consequences of authoritarian behavior (exemplified)

# Whats missing in summary

The emotional impact of confronting hypocrisy and privilege in societal attitudes.

# Tags

#Hypocrisy #Privilege #SocialJustice #Authoritarianism #CommunityPolicing