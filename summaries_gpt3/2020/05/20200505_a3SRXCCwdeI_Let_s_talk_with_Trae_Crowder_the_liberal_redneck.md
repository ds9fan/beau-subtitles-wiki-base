# Bits

Beau says:
- Introducing Trey Crowder, the liberal redneck comedian with substance behind the jokes.
- Trey explains his journey from standup comedian to creating videos with a political twist.
- Trey's inspiration to start making videos came from witnessing a viral far-right preacher video.
- The success of Trey's videos led him to quit his day job and pursue comedy full-time.
- Trey's authenticity and genuine upbringing in a rural, poverty-stricken part of Tennessee influence his content.
- Beau and Trey bond over the experience of hiding their accents and eventually embracing them.
- They address stereotypes about Southerners and the importance of debunking misconceptions.
- Trey talks about his book, "The Liberal Redneck Manifesto: Dragging Dixie Out of the Dark," co-written with his friends.
- Despite the success of the book, Trey feels disconnected from that period of his life, which was a whirlwind of change.
- Beau and Trey touch on how Trey's comedic work has had political ramifications and sparked meaningful dialogues.
- Trey shares his mixed feelings about being seen as an activist rather than solely a comedian and the expectations that come with it.
- They dive into Trey's intent behind his work, focusing on challenging stereotypes and showcasing the diversity in the South.
- Trey stresses the importance of not assuming extreme viewpoints in dialogues with others to foster better relationships and understanding.
- Trey talks about the challenges of the current standup comedy scene due to the pandemic and focuses on his upcoming projects.
- Despite uncertainties, Trey remains optimistic about progress and encourages everyone to soldier on.

# Quotes

"Write what you know and that's pretty much all I've ever done."  
"People should be aware that the image of Southerners isn't accurate at all."  
"I always wanted to be a comedian. Having said that, I think it's cool if my work reaches people and starts a conversation."

# Oneliner

Beau chats with Trey Crowder, the liberal redneck comedian whose authentic content challenges stereotypes and sparks meaningful dialogues, fostering understanding and showcasing the diverse perspectives in the South.

# Audience
Comedy fans, Southern residents

# On-the-ground actions from transcript

- Listen openly and without assumptions to bridge divides (implied)
- Challenge stereotypes and misconceptions about Southerners (implied)

# What's missing in summary
The full transcript provides an in-depth look at Trey Crowder's journey from standup comedian to creating impactful political content, showcasing the importance of authenticity and challenging stereotypes. Viewing the full transcript allows for a deeper understanding of Trey's motivations and the evolution of his work.  

# Tags
#Comedy #South #Authenticity #PoliticalContent #ChallengingStereotypes #Progress