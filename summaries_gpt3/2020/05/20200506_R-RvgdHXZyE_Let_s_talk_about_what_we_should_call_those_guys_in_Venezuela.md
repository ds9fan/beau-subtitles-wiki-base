# Bits

Beau says:

- Exploring terminology for individuals in Venezuela.
- Contrasting contractors and mercenaries.
- Drawing parallels between pirates, privateers, and the Navy.
- Emphasizing the difference a permission slip makes in the terminology.
- Addressing the moral implications of actions based on legality.
- Questioning the legitimacy of the government in Venezuela.
- Proposing a unified terminology for contractors and mercenaries.
- Warning against politicizing terminology to justify immoral actions.
- Mentioning the corporatization of the industry and its impact on international relations.
- Advocating for consistency in labeling individuals based on their actions rather than permissions.
- Expressing concerns about the conflation of legality with morality in current times.

# Quotes

- "Legality and morality are not the same thing."
- "It feeds into the idea that legality is morality."
- "If you allow it to be politicized, then we're playing into the idea that the government can authorize an activity that normally is immoral."
- "You should decide on one and apply it across the board."
- "The government can legitimize behavior that you'd normally view as immoral by simply saying it's okay."

# Oneliner

Exploring the thin line between legality and morality in defining contractors and mercenaries in Venezuela.

# Audience

Philosophy enthusiasts, international relations scholars.

# On-the-ground actions from transcript

- Settle on unified terminology (suggested).
- Avoid politicizing terms for contractors and mercenaries (implied).
- Advocate for consistency in labeling based on actions (implied).

# Whats missing in summary

The full transcript provides a nuanced exploration of the ethical implications surrounding the terminology used to describe individuals involved in military operations, urging for a thoughtful consideration of the relationship between legality and morality.

# Tags

#Contractors #Mercenaries #Legality #Morality #Venezuela #InternationalRelations