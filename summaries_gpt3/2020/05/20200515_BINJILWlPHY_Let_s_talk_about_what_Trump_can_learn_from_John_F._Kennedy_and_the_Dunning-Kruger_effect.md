# Bits

Beau says:

- Explains what President John F. Kennedy could teach President Trump, focusing on a critical lesson yet to be learned.
- Contrasts President Trump with Dr. Fauci in briefings, illustrating the Dunning-Kruger effect.
- Describes Kennedy's humility and willingness to delegate decision-making, citing an example from his Navy days.
- Recounts a story about Kennedy's visit to France, showcasing his humility and understanding of what matters to people.
- Criticizes President Trump's repeated wrong decisions and lack of importance in the current situation.
- Emphasizes the need for experts to lead and inform the public, not the President.

# Quotes

- "President Trump, you are the man who accompanies Dr. Fauci to the podium. Nothing more."
- "He was wearing his power lightly."
- "Nobody believes, no rational person believes, that Trump is going to oversee a V-shaped recovery."
- "You don't matter in those briefings."
- "Kennedy could definitely teach it."

# Oneliner

Beau explains what Kennedy could teach Trump, contrasting their leadership styles and stressing the importance of humility and expertise in decision-making.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Listen to experts and prioritize their insights in decision-making (implied)
- Advocate for leadership that values humility and expertise (implied)

# Whats missing in summary

The full transcript delves deeper into Kennedy's leadership style and contrasts it with Trump's, offering a critical analysis of their approaches to decision-making and public perception. 

# Tags

#Leadership #DecisionMaking #Experts #Humility #PoliticalAnalysis