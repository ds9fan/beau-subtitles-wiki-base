# Bits

Beau says:

- Recalls witnessing a series of interviews where a recruiter struggled to choose between two candidates for a job.
- The recruiter was looking for someone who was good at finding lost things.
- Both candidates had the required experience and good references but lacked direct client interaction.
- The recruiter's concern was that neither candidate had been held accountable before.
- One candidate confidently promised to do everything possible, while the other couldn't provide a clear timeline.
- The candidate who vowed to do everything was hired, showcasing the importance of under-promising and over-delivering in crisis situations.
- Beau explains how overpromising and underdelivering erodes trust during crises.
- He criticizes leaders who prioritize branding over inspiring faith and comfort in the people they serve.
- Beau points out the importance of leaders admitting when they don't know something instead of spreading misinformation.
- He stresses that true leadership involves honesty, accountability, and inspiring belief in the people.
- Beau concludes by urging individuals to step up and lead in the absence of genuine leadership in traditional positions of power.
- He calls for collective action and responsible leadership from the populace in navigating crises.

# Quotes

- "One of the basic rules of crisis mitigation is to under promise and over deliver."
- "The U.S. needs real leadership. It is probably not going to be in the Oval Office for a very long time."
- "What gets people through tough situations is leadership."
- "The American people would rather hear, 'I don't know,' than a lie."
- "It's up to all of the people who make up this country to lead it."

# Oneliner

Beau stresses the importance of under-promising, over-delivering, and honest leadership in inspiring faith during crises, advocating for collective action in the absence of genuine leadership.

# Audience

Americans, Community Members

# On-the-ground actions from transcript

- Lead with honesty and accountability in your interactions and decision-making (exemplified).
- Take responsibility for your actions and admit when you don't know something (exemplified).
- Inspire faith and trust within your community through transparent communication (exemplified).
- Step up as a leader in your community to navigate crises effectively (exemplified).

# Whats missing in summary

The full transcript provides a detailed analysis of leadership qualities needed during crises and the importance of honesty, accountability, and inspiring faith in inspiring collective action.

# Tags

#Leadership #CrisisMitigation #Honesty #Accountability #CommunityLeadership