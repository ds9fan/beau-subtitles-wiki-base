# Bits

Beau says:
- Emphasizes the importance of being a good person because it's the right thing to do.
- Criticizes those who need self-interest or partisanship as a reason to behave nicely.
- Mentions the impact of baseless accusations during Supreme Court nomination hearings and how it can forever alter relationships.
- Describes a story of a woman considering adoption based on the adoptive parents' behavior on social media.
- Stresses the value of character over material wealth and Easy Street living, advocating for facing challenges and defeats in life.
- Points out the significance of one's words and actions on social media, which can have lasting consequences.
- Argues that showing loyalty to harmful ideologies or politicians at the cost of relationships and values is not worth it.
- Encourages people to think for themselves and express genuine beliefs rather than blindly following harmful rhetoric.
- Asserts that abandoning principles for party loyalty can lead to losing valuable relationships and being perceived as not a good person.
- Affirms a belief in the prevalence of good people in the world who value not kicking others down.

# Quotes
- "Being poor is a lack of cash, not a lack of character."
- "It's better to be poor than be a racist."
- "If you kick down, people see it. And they have no reason to help you."

# Oneliner
Be a good person, prioritize character over material wealth, and avoid harmful rhetoric to maintain valuable relationships and integrity.

# Audience
Social media users

# On-the-ground actions from transcript
- Choose your words and actions carefully on social media (implied)
- Prioritize character and values over material gain (implied)
- Think for yourself and express genuine beliefs (implied)

# Whats missing in summary
Beau's engaging storytelling and emphasis on personal integrity are best experienced by watching the full transcript.

# Tags
#Character #GoodPerson #SocialMedia #Integrity #Relationships