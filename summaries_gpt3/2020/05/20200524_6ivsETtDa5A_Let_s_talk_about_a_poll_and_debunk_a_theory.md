# Bits

Beau says:

- Addresses a poll revealing concerning beliefs held by Fox News viewers regarding Bill Gates and vaccines.
- Mentions the skepticism towards Bill Gates despite his early warnings about pandemics.
- Notes the historical context of conspiracy theories related to tracking individuals.
- Speculates on potential motives behind the belief that Bill Gates will use vaccines to track people.
- Points out how much personal information individuals willingly provide through technology.
- Questions the need for a chip when ample data is already accessible through various means.
- Comments on the ironic situation where individuals worried about surveillance support restrictive measures like building a wall.
- Criticizes the lack of critical thinking among those who believe in far-fetched theories.
- Expresses concern about the lack of awareness among Fox News viewers regarding existing surveillance issues.
- Concludes by reflecting on the implications of blind belief and misinformation spread by certain news networks.

# Quotes

- "The surveillance society you're worried about, it's already here."
- "If you truly believe that this is a possibility, you should be screaming to have that wall torn down more than any liberal."
- "The same people that believe there's this James Bond villain Bill Gates guy out there [...] are the same people who want to cut off their only means of escape."
- "It's like they're easily led. Like somebody else is doing the thinking for them."
- "This is not something I'm worried about. I am more worried that there's 75% of people who view it as a possibility who watch Fox News who don't understand that what they're worried about is already happening."

# Oneliner

Beau addresses concerning beliefs held by Fox News viewers about Bill Gates using vaccines to track people, questioning the need for a chip when technology already provides extensive data on individuals.

# Audience

Critical Thinkers

# On-the-ground actions from transcript

- Inform others about the implications of blind belief and the importance of critical thinking (implied).
- Advocate for transparency and accountability in media coverage to prevent misinformation (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the misinformation spread through news networks and the implications of blind belief in conspiracy theories.

# Tags

#FoxNews #BillGates #ConspiracyTheories #Surveillance #CriticalThinking