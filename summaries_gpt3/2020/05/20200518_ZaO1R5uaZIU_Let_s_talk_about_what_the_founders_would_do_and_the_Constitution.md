# Bits

Beau says:

- Exploring the conflict around statements claiming that stay-at-home orders are unconstitutional and causing economic devastation.
- Emphasizing the importance of understanding the entire Constitution, especially the 10th amendment which grants power to the states.
- Clarifying that in situations like the current pandemic, restrictions on movement and assembly can be constitutional.
- Providing historical context like the yellow fever outbreak in Philadelphia during George Washington's presidency to debunk the claim that the founders wouldn't have tolerated such measures.
- Stressing the need for responsible exercise of rights and the importance of being well-informed to avoid misinformation.

# Quotes

- "If you cry wolf about tyranny, when tyranny arrives, nobody's going to believe you."
- "Rights come with responsibilities, and one of them is to become educated."

# Oneliner

Exploring the constitutionality of stay-at-home orders, Beau debunks claims that such measures are unconstitutional by providing historical context and stressing the importance of being well-informed.

# Audience

Citizens, Constitution Advocates

# On-the-ground actions from transcript

- Educate yourself on the Constitution and its amendments (suggested)
- Advocate for responsible exercise of rights (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the constitutionality of current measures, historical examples, and the importance of being informed.

# Tags

#Constitution #StayAtHome #Responsibility #Education #History