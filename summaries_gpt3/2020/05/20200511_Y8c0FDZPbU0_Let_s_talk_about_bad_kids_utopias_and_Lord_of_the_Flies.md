# Bits

Beau says:

- Raises the issue of labeling individuals as "bad kids" based on low expectations.
- Points out how low expectations can become a self-fulfilling prophecy for individuals.
- Compares society's negative perceptions of humanity to labeling children negatively.
- Challenges the idea that humans are inherently selfish or lazy.
- Advocates for portraying humanity in a more positive light based on evidence of positive behavior.
- Mentions the importance of expecting the best from people to achieve a utopian society.
- Refers to a real-life story similar to "Lord of the Flies" where kids thrived, contradicting the negative portrayal of human nature.
- Stresses the need to set high expectations to encourage positive behavior in individuals and society.
- Emphasizes the role of education in shaping behavior and setting standards.
- Encourages being a positive example for younger generations to create a better future.

# Quotes

- "If you want a utopian society, you have to expect the best from people."
- "Humanity isn't innately evil. Humanity just is. It's what we make of it."
- "If we want utopia, we have to build it. Step by step."
- "We should probably focus more on saying that it is possible. Not that it isn't."
- "We're not there yet, but we're going to get there."

# Oneliner

Labeling individuals negatively based on low expectations creates self-fulfilling prophecies, challenging society to expect the best from humanity for a utopian future.

# Audience

Educators, Activists, Parents

# On-the-ground actions from transcript

- Set high expectations for individuals and society, encouraging positive behavior (implied).
- Be a positive example for younger generations to foster a better future (implied).

# Whats missing in summary

The full transcript dives deeper into the impact of societal perceptions on individuals and the importance of education in shaping behavior. Watching the full transcript provides a comprehensive understanding of fostering positive change through collective optimism.

# Tags

#Utopias #HighExpectations #PositiveBehavior #Education #SocietyImprovement