# Bits

Beau says:

- Unemployment is at almost 15%, a significant number indicating the current economic crisis.
- Despite jobs reopening, not all employees are returning due to health concerns, leading to conflicts between employers and workers.
- Some employers are using government tools to force employees back to work, even if it jeopardizes their health.
- States like Ohio, Alabama, and others are setting up systems for employers to report employees who refuse to return to work.
- People's reluctance to return to work may also lead to decreased consumer spending on non-essentials.
- Small businesses that fought to reopen might face permanent closure due to reduced revenue and higher expenses.
- Beau criticizes employers who betray their employees' trust by reporting them for choosing safety over work.
- He suggests that consumers should be informed about businesses that mistreat their employees during these times.
- The current economic situation is predicted to worsen as people struggle financially and refuse to risk their lives for profit.
- Beau advocates for individuals to prioritize their safety, seek media attention if needed, and take legal action against employers who compromise their well-being.

# Quotes

- "You have to go out, wear a mask."
- "It’s always better to be a good person."
- "Enjoy the start of the depression."

# Oneliner

Unemployment rises, conflicts between employers and workers escalate, and consumer spending decreases as safety concerns prevail during the economic crisis.

# Audience

Employees, consumers, activists.

# On-the-ground actions from transcript

- Contact the media if your employer jeopardizes your safety (suggested)
- Wear a mask when going out (implied)
- Inform others about businesses mistreating employees (implied)

# Whats missing in summary

The detailed examples and insights shared by Beau provide a deeper understanding of the economic challenges faced by individuals and businesses during the current crisis.

# Tags

#Unemployment #EconomicCrisis #WorkersRights #SmallBusinesses #SafetyConcerns