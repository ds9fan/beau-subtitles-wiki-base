# Bits

Beau says:

- Critiques the President's eagerness to quickly reopen the economy without taking responsibility for the consequences.
- Points out that rushing into reopening without a proper plan could lead to more harm.
- Mentions the importance of having a safe and effective treatment or vaccine before fully stopping precautions.
- Expresses concerns about the potential dangers of not maintaining precautions and the possibility of flare-ups.
- Addresses the need for individuals to make informed decisions about when it's safe to resume normal activities.
- Emphasizes the role of leadership in mitigating risks and protecting the well-being of the people.
- Urges caution and awareness of possible supply chain interruptions that could have severe consequences.
- Encourages individuals to be proactive in monitoring the situation and being prepared for potential challenges.
- Stresses the importance of taking personal responsibility in navigating through uncertain times.
- Concludes by reminding listeners to stay vigilant and prepared for any future developments.

# Quotes

- "You're going to have to lead yourself here."
- "Lay down your life for the altar of corporate profits, I guess."
- "Being a warrior is about mitigating risk."
- "A warrior, a leader, would want to make sure that doesn't happen because that compounds everything."
- "And generally, it should be noted that when you're acting in that manner, you're not fighting against the people in front of you. You're fighting for the people behind you."

# Oneliner

Beau warns against rushing into reopening, stresses the need for caution, leadership, and individual responsibility in navigating through uncertain times.

# Audience

Individuals, concerned citizens

# On-the-ground actions from transcript

- Monitor the situation and stay informed about potential risks and developments (suggested).
- Be prepared to re-implement extreme measures such as staying home if necessary (suggested).
- Stay vigilant for possible interruptions in the supply chain and be proactive in addressing any challenges that may arise (suggested).

# Whats missing in summary

The full transcript provides a detailed analysis of the current situation and offers valuable insights into the importance of caution, leadership, and individual responsibility in handling the crisis effectively.

# Tags

#COVID-19 #Leadership #Responsibility #Economy #Precautions