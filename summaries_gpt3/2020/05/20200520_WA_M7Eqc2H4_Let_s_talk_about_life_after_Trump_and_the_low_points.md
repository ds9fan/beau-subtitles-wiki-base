# Bits

Beau says:

- Describes a true story of a man named Yamaguchi, a ship designer, who experienced the worst week ever during World War II in Japan, surviving both Hiroshima and Nagasaki atomic bombings.
- Yamaguchi's resilience and survival despite severe injuries stand out as an example of getting through low points.
- Beau draws parallels between the recovery of Hiroshima and Nagasaki after the devastation and the current low points faced by countries and individuals worldwide.
- Expresses concern over leaders prioritizing personal gains and neglecting critical issues like infrastructure, mass incarceration, and poverty alleviation during challenging times.
- Criticizes the glorification of past eras like the 1950s, pointing out the flawed nostalgia for a mythological time that neglects the realities of injustice and challenges faced then.
- Beau warns about the dangers of the United States behaving like a failed state due to inaction on impending crises despite knowing about them.
- Emphasizes the need for collective action, new ideas, and systemic change driven by people at the grassroots level to address current challenges effectively.
- Encourages individuals to be part of shaping a better future by focusing on real progress and advancement rather than regressive ideals.
- Stresses the importance of learning from low points to propel society, countries, and individuals forward with momentum and unity.

# Quotes
- "That's what's scary to me."
- "We're at a low point. Fact. But we're going to get through it."
- "It's not going to be up to them. It's going to be up to you and me."
- "We need new ideas. We need real systemic change."
- "Being a part of actually advancing."

# Oneliner
Beau shares a story of resilience amidst the worst week ever, urging collective action and systemic change to move forward from current low points towards real progress.

# Audience
Activists, Community Members

# On-the-ground actions from transcript
- Cooperate and work together with people at the grassroots level to generate new ideas and drive real systemic change (implied).
- Take part in shaping a better future by actively advancing societal, national, and global progress through collective efforts (implied).

# Whats missing in summary
The full transcript provides detailed insights into historical resilience, current challenges, and the need for collective action to overcome low points and drive real systemic change effectively.

# Tags
#Resilience #CollectiveAction #SystemicChange #MovingForward #GrassrootsEfforts