# Bits

Beau says:

- Explains the concept of "Once a Marine, always a Marine" and why it holds true.
- Touches on the significance of oaths taken by individuals in government or non-government entities.
- Mentions Edward Snowden and John Karyaku as examples of individuals who upheld their oath by blowing the whistle.
- Talks about the serious nature of taking oaths among people with honor and integrity.
- Draws parallels between the Marine Corps' iconic knife, the K-Bar, and the symbolism behind it.
- Questions the actions of a large group of people in the U.S. Senate who seemingly violated their oath to remain impartial.
- Expresses concerns about politicians prioritizing political advantage over truth, justice, and the Constitution.
- Raises doubts about whether politicians truly represent the interests of the people they serve.
- Criticizes politicians for potentially lying to the public about various issues, including war, social security, and the economy.
- Challenges the American people to question the integrity of their representatives and their tolerance for political misconduct.

# Quotes

- "Once a Marine, always a Marine."
- "Nobody can look at this and say that it was impartial."
- "What makes you think they won't lie to you about the economy?"
- "Has the United States, the people of the United States, has their honor and integrity fallen this far that they tolerate it?"
- "They don't care about you; these people aren't your representatives."

# Oneliner

Beau examines the lasting impact of being a Marine, the significance of upholding oaths, and questions political integrity and representation in the U.S. Senate.

# Audience

Active Citizens

# On-the-ground actions from transcript

- Contact your representatives and demand accountability for upholding their oaths (implied).

# Whats missing in summary

The emotional depth and passion in Beau's delivery can only be fully experienced by watching the full transcript.

# Tags

#Marines #Oaths #PoliticalIntegrity #Representation #Accountability