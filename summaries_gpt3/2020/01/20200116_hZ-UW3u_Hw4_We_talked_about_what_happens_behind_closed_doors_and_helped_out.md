# Bits

Beau says:

- Personal connections drive involvement in the fight against intimate partner violence.
- Domestic violence affects everyone regardless of race, gender, or class.
- Intimate partner violence accounts for 15% of all violent crime in the U.S.
- Half of women killed in the U.S. are killed by a current or former intimate partner.
- Financial control by abusers makes it hard for victims to leave.
- Leaving an abusive relationship is the most dangerous time for victims.
- Domestic violence shelters provide a safe haven for those leaving abusive relationships.
- Support organizations like Shelter House of Northwest Florida provide vital assistance to survivors.
- These organizations also help alleviate traumatic experiences, such as rape kits and fresh clothes for survivors.
- Animal shelters by these organizations help remove barriers to leaving abusive situations.
- Donations, especially old cell phones, can make a significant impact on survivors' ability to seek help and support.
- Community support and donations can help provide essentials and comfort to survivors, like Christmas presents for teens in shelters.
- PTSD triggers like scents can influence traumatic memories, suggesting potential avenues for support.
- Individual actions, even small ones, can contribute to collective goals in supporting survivors of domestic violence.

# Quotes

- "Women are 500 times more likely to be killed if they're in one of these relationships when they're just leaving."
- "Violence isn't always the answer."
- "Odds are you have an old cell phone sitting in a junk drawer, mail it to them. It means nothing to you, could save someone's life."
- "Community support and donations can help provide essentials and comfort to survivors."
- "Individual actions, even small ones, can contribute to collective goals in supporting survivors of domestic violence."

# Oneliner

Beau sheds light on the pervasive nature of intimate partner violence, the challenges faced by survivors in leaving abusive relationships, and the vital role of community support and organizations in providing safety and assistance.

# Audience

Supporters of survivors

# On-the-ground actions from transcript

- Donate money or old cell phones to organizations like Shelter House of Northwest Florida (suggested)
- Support organizations that provide essentials and comfort to survivors (exemplified)
- Volunteer time or resources to assist domestic violence shelters (implied)

# Whats missing in summary

The full transcript provides a comprehensive perspective on intimate partner violence, survivor challenges, community support, and practical ways individuals can contribute to supporting survivors.

# Tags

#IntimatePartnerViolence #CommunitySupport #DomesticViolence #Survivors #SupportActions