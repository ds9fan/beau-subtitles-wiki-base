# Bits

Beau says:

- Explains the current state of international affairs focusing on spheres of influence.
- Shares his perspective on how the world operates in terms of maintaining influence over smaller countries.
- Compares his beliefs with the actions taken by larger countries to maintain influence.
- Details the influence of larger countries in Syria, Iraq, Iran, and Afghanistan before and after Trump took office.
- Describes the repercussions of current foreign policy decisions on Russian influence stretching from Syria to Afghanistan.
- Criticizes President Trump's foreign policy legacy and its impact on American influence globally.
- Emphasizes the need for experienced diplomats to undo the damage caused by the current administration.

# Quotes

- "This doesn't really sound like what you believe."
- "A never-ending chain of Russian influence stretching from Syria to Afghanistan."
- "President Trump has one of the worst foreign policies in American history."
- "The Democrats need to understand that President Trump destroyed everything that this country fought and bled for."
- "This isn't anti-imperialism. It's assisting somebody else's imperialist ambitions."

# Oneliner

Beau provides a critical analysis of international affairs, discussing the shift in influence from the US to Russia under President Trump's foreign policy, urging the need for experienced diplomats to mitigate the damage done.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Seek out and support experienced diplomats for key positions in foreign policy (implied)
- Advocate for a comprehensive understanding of international relations and the Middle East and South America (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the impact of current foreign policy decisions on spheres of influence and the need for experienced diplomats to address the damage caused by the administration.

# Tags

#InternationalAffairs #SpheresOfInfluence #ForeignPolicy #Diplomacy #Critique