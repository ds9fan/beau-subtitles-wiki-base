# Bits

Beau says:

- Explains the fear of science and the historical example of King Henry IV's law against multipliers in 1404.
- Multipliers were alchemists who claimed to turn base metals into gold, causing concern among the rich and powerful.
- The law was passed to prevent the potential destabilization of the status quo if alchemy actually worked.
- Laws and regulations aimed at science are often driven by those in power fearing the unknown and potential disruptions to their control.
- Innovation has been stifled throughout history due to regulations designed to slow the pursuit of knowledge until legislators understand it.
- Legislators tend to learn slowly, leading to a slow acceptance of change in established systems.
- Breakthroughs have often occurred through less-than-legal means due to research being blocked by regulations.
- Regulations and prohibitions on new technologies or fields of study tend to persist once established, simply because "that's how it is."
- The law against multipliers was eventually repealed after prominent names in science, like Isaac Newton, spoke out against it.
- Beau encourages questioning authorities who discourage certain technologies or fields of study, as it may be a tactic to maintain their power.

# Quotes

- "When we have our betters telling us that certain technologies or certain fields of study aren't worth pursuing, we should always ask why."
- "A lot of laws and regulations that are aimed at science, that's what it's about."
- "It's about those in power, the establishment, the status quo, not really understanding it and therefore being afraid of it."
- "The establishment is slow to change."
- "It's just them attempting to preserve their place in the world above."

# Oneliner

Beau explains historical fears of science and regulations against innovation driven by those in power fearing disruptions to their control.

# Audience

Science enthusiasts, advocates for innovation.

# On-the-ground actions from transcript

- Advocate for increased support and funding for scientific research (implied).
- Challenge authority figures or institutions discouraging exploration of certain fields of study (implied).

# Whats missing in summary

The full transcript provides a detailed historical context and analysis of the fear of science, showcasing how regulations can inhibit innovation and progress. It delves into the motivations behind these regulations and the importance of questioning authority in such matters.

# Tags

#Science #Innovation #Regulations #History #Power #QuestioningAuthority