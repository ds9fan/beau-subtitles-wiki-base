# Bits

Beau says:

- Talks about a man from a country at risk of disappearing due to rising sea levels who fled to New Zealand and claimed he had to stay.
- The UN decided he could return because his country disappearing is 10 to 15 years away, not immediate danger.
- Mentions Americans who deny climate change and want to "own the libs" will have to face climate change refugees.
- States that Americans must choose between owning the libs by destroying the environment and hating brown people.
- Raises the question of whether people value owning the libs more than addressing climate change.
- Indicates the need for major changes in infrastructure, consumption, and addressing climate change.
- Mentions legal precedent protecting developing nations from actions of developed countries.
- Notes that those most impacted by climate change are often in locations with small carbon footprints.
- Suggests that people impacted by climate change may seek more habitable locations.
- Urges moving beyond bumper sticker mentalities and working on real solutions to address climate change.

# Quotes

- "Do they enjoy owning the libs more than they hate brown people?"
- "We have to make major changes. We have to address our infrastructure."
- "Those who are going to be most heavily impacted by climate change are in locations that aren't really responsible."
- "Might want to think about dropping the bumper sticker mentality."
- "A little bit of legal precedent protecting those in the developing world."

# Oneliner

Americans must choose between owning the libs by destroying the environment or addressing climate change to prevent a future influx of climate change refugees.

# Audience

Climate change activists

# On-the-ground actions from transcript

- Address infrastructure and consumption (implied)
- Work on real solutions to climate change (implied)

# Whats missing in summary

The emotional impact of individuals facing displacement due to climate change is best understood by watching the full transcript.

# Tags

#ClimateChange #Refugees #Immigration #Infrastructure #LegalPrecedent