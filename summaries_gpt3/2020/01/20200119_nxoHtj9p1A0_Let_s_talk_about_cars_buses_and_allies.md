# Bits

Beau says:

- Observes people debating allies instead of working towards a common goal.
- Defines allies as those generally moving in the same direction towards a shared destination.
- Compares allies to being on a city bus where people can join and leave at different stops.
- Emphasizes the importance of focusing on progress and maintenance rather than arguments.
- Acknowledges that allies won't always agree but should minimize conflicts due to greater goals.
- Encourages understanding that personal journeys may affect how far allies are willing to go.
- Stresses the long journey towards a shared utopia and the need for patience.
- Urges prioritizing actions that contribute to reaching the destination over theoretical debates.
- Reminds that allies are not perfect and disagreements may arise but should not hinder progress.

# Quotes

- "Your allies aren't perfect. They're never going to believe the same thing that you do."
- "If you're headed in the same direction your allies, yeah, there will be spats, there will be arguments, but they should be pretty short-lived."
- "It's a long journey, it's going to take a long time to get there."

# Oneliner

Beau explains the importance of allies moving towards a common goal like passengers on a city bus, focusing on progress over arguments.

# Audience

Community members

# On-the-ground actions from transcript

- Maintain the community bus: Ensure the vehicle (transcript implied) is well-equipped for the journey towards a shared goal.

# Whats missing in summary

The full transcript provides a detailed analogy on how allies should be viewed in terms of shared goals and progress.

# Tags

#Allies #Community #SharedGoals #Progress #Unity