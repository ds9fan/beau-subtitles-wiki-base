# Bits

Beau says:

- Beau talks about the positive side of humanity coming together during challenging times, like after Hurricane Michael when everyone on the coast united to help each other.
- He mentions the incredible diversity of people who banded together regardless of their backgrounds.
- The spirit of cooperation and support usually lasts about six months after a crisis before fading away.
- Beau expresses a desire to understand why this unity fades and hopes to find a way to sustain it without the need for constant existential threats.
- Shifting gears, Beau shares the heartwarming story of people in Australia helping wallabies by airdropping thousands of kilograms of sweet potatoes and carrots to their colonies affected by fires.
- He admires the willingness of people to go above and beyond to create a just world, even risking their lives by flying over fires to aid the wallabies.
- Beau expresses a wish for the same level of care and support to be extended to humans as well, reflecting on the need for such compassion to continue beyond natural disasters.

# Quotes

- "The spirit that exists after things go bad, it's the best that humanity has to offer."
- "Most people want a just world and they're willing to work for it."
- "I just can't wait until we do the same thing for people."

# Oneliner

Humanity's best emerges in unity during crises, but sustaining that spirit beyond disasters remains a challenge.

# Audience

Community members

# On-the-ground actions from transcript

- Support relief efforts in your community (exemplified)
- Volunteer to aid those affected by disasters (exemplified)

# Whats missing in summary

The full transcript provides a deeper insight into the temporary but powerful unity that arises in communities during crises and the importance of extending that same level of care and support to people beyond immediate emergencies.