# Bits

Beau says:

- The Supreme Court refused to hear cases related to the Flint water crisis, opening the door for officials involved to be held accountable.
- Government officials can be held accountable when the government stops protecting them.
- Beau questions if accountability will also happen in other high-profile cases.
- He suggests that the cover-up is often more significant than the crime itself.
- Beau hints at potential accountability actions even before the current administration leaves office.
- Government officials often live in an echo chamber, only listening to those who agree with them.
- Americans are unhappy with the current administration and its protectors.
- Beau mentions the possibility of another impeachment in the house.
- He points out that justice delayed is justice denied, drawing a parallel to the Flint water crisis.
- Beau concludes by reflecting on the potential consequences for senators during election time.

# Quotes

- "It's not the crime, it's the cover up."
- "Justice delayed is justice denied."
- "Americans are not happy with the current administration and those protecting it."

# Oneliner

The Supreme Court's refusal to hear Flint water crisis cases may signal accountability for officials, prompting reflections on cover-ups, potential future actions, and voter dissatisfaction with the current administration.

# Audience

Voters, Activists

# On-the-ground actions from transcript

- Contact elected officials to demand accountability for government actions (implied)
- Stay informed and engaged in political processes (implied)

# Whats missing in summary

The full transcript provides in-depth insights into government accountability, cover-ups, and the importance of timely justice in holding officials responsible.