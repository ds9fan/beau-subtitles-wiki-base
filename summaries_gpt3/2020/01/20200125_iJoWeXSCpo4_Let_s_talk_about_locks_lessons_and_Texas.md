# Bits

Beau says:

- Questioning the enforcement of a dress code that targets students with locks, particularly Black students.
- Examining the underlying issues and lessons being taught through these policies.
- Pointing out the inconsistency in enforcing the dress code based on race.
- Challenging the idea that certain hairstyles are distracting or unprofessional.
- Criticizing the expectation for Black students to conform to white standards.
- Expressing disbelief in the validity of the lessons being taught through this enforcement.
- Describing discipline for the sake of discipline as authoritarian and nonsensical.
- Emphasizing the message being sent to these young men about discrimination based on appearance.
- Inviting others to share their thoughts on the lessons being taught in this situation.
- Concluding with a powerful message about the impact of such policies on the students.

# Quotes

- "It doesn't matter how hard you work, what you do, what you achieve. Somebody can take it away from you because of the way you look."
- "I hate to break it to the school. I don't think there's a black kid in this country that needs that lesson taught to them because they see it every day from people like you."
- "Discipline for discipline's sake, that's just authoritarian nonsense."
- "The expectation is for a black student to try to be white and that's a really really bad expectation on a whole lot of levels."
- "I can't think of any reason for it to exist."

# Oneliner

Beau questions the enforcement of a dress code targeting Black students with locks, challenging the validity and lessons taught through such policies.

# Audience

Students, educators, activists

# On-the-ground actions from transcript

- Challenge and push back against discriminatory dress codes in schools (suggested)
- Advocate for inclusive and non-discriminatory policies regarding hairstyles and clothing (suggested)

# Whats missing in summary

Beau's passionate delivery and detailed analysis of the impact of discriminatory dress code policies can best be experienced by watching the full transcript.

# Tags

#Discrimination #DressCode #Inequality #Education #Community #Activism