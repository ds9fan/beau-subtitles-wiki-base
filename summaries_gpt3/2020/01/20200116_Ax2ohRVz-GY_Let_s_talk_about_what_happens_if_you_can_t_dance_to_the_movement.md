# Bits

Beau says:

- Addresses the question of how to keep going and not get worn down in the face of constant exposure to the worst of humanity through the news.
- Emphasizes the importance of finding happiness and joy amidst the pursuit of a better world.
- Encourages finding joy in simple things like raindrops on a tin roof, music, or being around people who bring happiness.
- Advocates for balance between political activism and personal joy by quoting Emma Goldman on not denying life and joy for a cause.
- Stresses the significance of maintaining a positive mindset and experiencing joy to sustain oneself in the fight for a better future.
- Urges self-care and the understanding that taking care of oneself is vital to staying effective in making a difference.
- Quotes Emma Goldman on the interconnectedness of aims, purposes, methods, and tactics in achieving goals.
- Mentions a part two to the Maddow interview and addresses Russia, advising not to panic as it's a normal restructuring.
- Encourages viewers to have fun and do things that make them happy as they prepare for what seems like a challenging road ahead.

# Quotes

- "If I can't dance to it, it's not my revolution."
- "The world's pretty messed up. But you have to take care of yourself."
- "If something comes into being full of anger, it's going to be angry."

# Oneliner

Beau addresses how to stay resilient amid exposure to negativity, urging the importance of joy in activism and self-care, quoting Emma Goldman on not denying life for a cause, and preparing viewers for upcoming challenges.

# Audience

Activists, Advocates, Supporters

# On-the-ground actions from transcript

- Surround yourself with people who bring you joy and remind you of the good in the world (implied).
- Take time to do things that make you happy and recharge your batteries (implied).

# Whats missing in summary

The full transcript provides a nuanced perspective on balancing activism with personal joy and the interconnectedness of methods and goals, offering a reminder to prioritize self-care and happiness in the pursuit of a better world.

# Tags

#Resilience #Activism #SelfCare #Joy #CommunitySupport