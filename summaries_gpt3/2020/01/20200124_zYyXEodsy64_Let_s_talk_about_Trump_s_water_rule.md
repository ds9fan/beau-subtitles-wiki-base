# Bits

Beau says:
- Water is life and clean water is good.
- The Clean Water Act has protected America's waterways since 1972.
- President Obama expanded the Clean Water Act to include smaller bodies of water based on scientific studies.
- Trump has rolled back protections on waterways further than ever before.
- The rule to undo protections was developed by political appointees, not based on scientific consensus.
- Environmentalists believe Trump's actions will lead to deaths and polluted waterways.
- The undoing of regulations allows developers to pollute for profit.
- This decision goes against the Clean Water Act and endangers water supplies and crops.
- Trump's actions will need to be undone by the next president to prevent further harm.

# Quotes

- "Water is life, clean water is good."
- "This is going to be a mistake."
- "Your water supply is tainted, your crops will die."
- "People will die. This is ridiculous."
- "We have a complete incompetent person in the White House."

# Oneliner

Beau stresses the importance of clean water and criticizes Trump's rollback of protections, warning of dire consequences and the need for future correction.

# Audience

Environmental activists, concerned citizens

# On-the-ground actions from transcript

- Contact local representatives to advocate for clean water protections (suggested)
- Join environmental organizations working to safeguard waterways (implied)

# Whats missing in summary

The emotional impact of Beau's frustration and urgency in protecting clean water.

# Tags

#CleanWater #EnvironmentalProtection #TrumpAdministration #PublicHealth #CommunityAction