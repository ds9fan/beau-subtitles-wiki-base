# Bits

Beau says:

- Recounts a story he heard around a campfire about a young boy forced to move from his village in winter.
- Describes the harsh journey of the boy and his village being moved by soldiers, indifferent to their suffering.
- Depicts the heartbreaking scenes of mothers struggling to keep their infants warm during the forced relocation.
- Narrates how the boy, disillusioned by the elders' words, sneaks off with his friends, becoming part of the Cherokee remaining in the southeastern United States.
- Mentions the Cherokee Rose flower, symbolizing the tears of mothers and the gold that drove them out during the Trail of Tears.
- Links the spread of the Cherokee Rose as an invasive plant to the dark moments in American history.
- Criticizes Congress for laying the foundation of such dark events and the executive branch for exploiting them.
- Points out how Congress often prioritized personal gains over the well-being of the country.
- Emphasizes the recurring theme of American dark points stemming from the shortsightedness and self-interest of those in power.
- Suggests a lesson from the story for current Capitol Hill officials and acknowledges the grim reality behind the Trail of Tears' name.

# Quotes

- "It was a trail of tears and death."
- "Most of America's dark points are because of stuff like that."
- "Sometimes, they're just an innate part of American culture."

# Oneliner

Beau recounts a poignant tale of a young boy's forced relocation and draws parallels to the dark moments in American history due to government shortsightedness and self-interest.

# Audience

Legislators, policymakers, historians

# On-the-ground actions from transcript

- Learn about the history of forced relocations like the Trail of Tears and their impact (suggested)
- Support initiatives that aim to preserve and honor the stories and cultures of indigenous peoples (suggested)

# Whats missing in summary

The emotional depth and impact of hearing Beau's storytelling firsthand.

# Tags

#TrailOfTears #AmericanHistory #GovernmentAccountability #IndigenousRights #SelfInterest #Injustice