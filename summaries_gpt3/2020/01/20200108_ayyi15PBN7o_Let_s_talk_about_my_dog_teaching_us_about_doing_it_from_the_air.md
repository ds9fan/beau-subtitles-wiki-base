# Bits

Beau says:

- Beau uses the example of his dog, Baroness, to illustrate the concept of doing something from the air with ease and precision.
- He questions why the strategy of taking out command and control from the air hasn't been widely implemented despite having the capability for a long time.
- The transcript delves into historical instances, like George Bush Jr.'s presidency, where this strategy wasn't employed, raising the question of why.
- Beau mentions the deterrent capabilities of Iran and how low-tech and high-tech deterrents play a significant role in international relations.
- He brings up the concept of state sponsors and their influence in funding organizations with significant force projection capabilities.
- The mention of General Soleimani and his successor, Connie, sheds light on the continuity of leadership and influence in such organizations.
- The transcript concludes by reiterating the idea of taking out command and control as a strategic move, questioning why it hasn't been executed before.

# Quotes

- "She is a literal dog of war."
- "Why hadn't anybody done it? We've had the ability to do this for a very, very long time."
- "They fund these groups. What does money come with? Strings, right?"
- "What's a brilliant strategy, again? Take out command and control."
- "It's just a thought."

# Oneliner

Beau questions why the strategy of taking out command and control from the air hasn't been widely implemented despite its capability, using his dog as an analogy.

# Audience

Policy makers, strategists, activists

# On-the-ground actions from transcript

- Analyze the implications of using the strategy of taking out command and control in conflicts (suggested)
- Engage in informed debates and dialogues about the effectiveness and consequences of such military strategies (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of historical events, military strategies, and the concept of deterrence in international relations.

# Tags

#MilitaryStrategy #ConflictResolution #InternationalRelations #Deterrence #Leadership