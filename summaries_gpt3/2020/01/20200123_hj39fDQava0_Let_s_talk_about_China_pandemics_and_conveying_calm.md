# Bits

Beau says:

- China has quarantined a large city, which can be unnerving.
- Attended a pandemic prevention conference where worst-case scenarios were discussed to instill caution.
- Presenter emphasized the importance of waiting for demographic information on patients.
- Majority of patients with the virus were older and had pre-existing conditions.
- Virus seems to affect those who are already compromised.
- Not everyone infected had a fever, which is concerning as fever is a common screening method.
- Prevention methods include washing hands frequently, staying away from sick individuals, and sneezing properly.
- Surveillance and early action are key in controlling new threats.
- Media sensationalism can lead to panic and desensitization to real pandemics.
- Beau believes it is not yet time to panic or wear gas masks.

# Quotes

- "There's no reason to panic."
- "So just stay calm, do what you do, and let them do what they do."
- "I don't see anything to really panic about."
- "I really don't see this as the giant pandemic everybody's worried about."
- "It's guaranteed we are going to have a bad pandemic at some point in the future."

# Oneliner

Beau conveys calm amidst pandemic concerns, stressing the importance of caution and trust in authorities while debunking media sensationalism.

# Audience
Community members

# On-the-ground actions from transcript
- Wash hands frequently, stay away from sick individuals, and sneeze properly (suggested)
- Trust in authorities and follow their guidelines (implied)

# Whats missing in summary
Tips on staying informed and critical of media sensationalism during health crises.

# Tags
#Pandemic #Prevention #Media #Trust #Community