# Bits

Beau says:

- Refuses to endorse politicians due to his worldview and the danger of creating cults of personality.
- Believes endorsing politicians is dangerous as it leads to blindly supporting the person, not policies.
- Warns against following politicians anywhere just because of personal support.
- Emphasizes the importance of ideas standing on their own merit, regardless of the source.
- Points out the danger in defending policies you don't truly support because of loyalty to a candidate.
- Cautions against falling into the trap of blindly supporting a candidate instead of their specific policies.
- Advocates for supporting policies and ideas rather than political figures.
- Rejects the idea of being ruled by politicians and encourages leading ourselves in politics.

# Quotes

- "Ideas stand and fall on their own."
- "You'll never see me endorse a politician."
- "My political endorsement for 2020? You and me."

# Oneliner

Beau refuses to endorse politicians, warning against cults of personality and advocating for supporting policies over individuals.

# Audience

Voters, political activists

# On-the-ground actions from transcript

- Lead ourselves in politics (implied)
- Support policies over individuals (implied)

# Whats missing in summary

The detailed examples and explanations Beau provides on why endorsing politicians can be dangerous and the importance of focusing on policies rather than personalities.

# Tags

#PoliticalEndorsements #CultsOfPersonality #SupportPoliciesNotPeople #LeadOurselves #IdeasMatter