# Bits

Beau says:

- Explains the process leading up to the reintroduction of selective service and military recruitment.
- Focuses on the steps taken before selective service is brought back, indicating the warning signs to look out for.
- Mentions that the Department of Defense (DOD) is not keen on selective service returning, as they prefer volunteer service over conscription.
- Talks about tactics used by the military to avoid a draft, such as re-enlistment bonuses and retention efforts.
- Describes scenarios like the back-door draft and inter-service draft, which are not official drafts but serve a similar purpose.
- Mentions reserve units and the ability to involuntarily recall individuals back to service.
- Explains how enlistment bonuses are used to incentivize new recruits, especially targeting low-income areas.
- Talks about lowering standards for recruitment, both secretly and openly, when there is a shortage of personnel.
- Indicates that selective service is unlikely in the current times, and even if it were to return, efforts will be made to make it more acceptable to the public.
- Concludes by stating that the avoidance of selective service is preferred, especially under a commander-in-chief with draft-dodging allegations.

# Quotes

- "Your strongest ally in this may be surprising. It's DOD."
- "This is the point where you need to start paying attention."
- "So it's not a fear that at this moment you should really have."
- "I think they want to avoid it at all costs."
- "Have a good night."

# Oneliner

Beau explains the process leading up to selective service reintroduction, detailing warning signs and military tactics to avoid a draft, ultimately suggesting its unlikelihood and efforts to make it palatable to the public.

# Audience

Military-age individuals

# On-the-ground actions from transcript

- Stay informed on military recruitment processes (implied)
- Monitor warning signs for selective service reinstatement (implied)
- Advocate for transparent recruitment standards (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of military recruitment processes and the steps leading up to selective service reintroduction, offering insights into warning signs and efforts to avoid conscription.

# Tags

#SelectiveService #MilitaryRecruitment #AvoidingDraft #DOD #Standards