# Bits

Beau says:

- Describes a storm approaching in the US and another place, with governments pumping people up for inevitable events.
- Points out similarities in how both governments are treating their people and using propaganda.
- Clarifies that he respects General Soleimani as a dangerous tool of Iranian foreign policy, even though he doesn't agree with him.
- Warns against underestimating one's opposition, stressing the fatal consequences of doing so.
- Criticizes the US administration for escalating tensions with Iran unnecessarily.
- Commends the security details at the embassy for handling the situation calmly and professionally.
- Expresses frustration at how the US administration's actions played into Iranian propaganda.
- Criticizes those who cheer for war without considering the real consequences or volunteering to fight themselves.
- Calls out the hypocrisy of politicians who support war but wouldn't personally participate in combat.
- Concludes by pointing out the similarities in behavior between the US and Iranian governments and the manipulation of people for political purposes.

# Quotes

- "You can't underestimate your opposition. That is a fatal error."
- "War isn't a spectator sport, gentlemen."
- "They're both doing what they can to energize their people. They need us. We don't need them."

# Oneliner

Both the US and Iranian governments manipulate their people for war, while armchair supporters fail to acknowledge the true costs of conflict.

# Audience

Citizens, policymakers, activists

# On-the-ground actions from transcript

- Challenge war narratives and propaganda by engaging in critical discourse with peers (implied)
- Advocate for peaceful resolutions and diplomacy in international conflicts (implied)
- Support organizations working to prevent war and violence through peaceful means (implied)

# Whats missing in summary

The emotional impact of war propaganda and the human cost of political manipulation.

# Tags

#WarPropaganda #PoliticalManipulation #InternationalConflict #Diplomacy #Peacebuilding