# Bits

Beau says:

- Beau introduces a heavy email he received from a mother concerned about her son's increasing interest in nationalism and warrior ethos, fearing he may follow in his late father's footsteps who was lost in the military.
- The mother expresses discomfort with her son's deep interest in history and martial arts, possibly influenced by his father's military background.
- Concerned about her son adopting hateful ideologies, the mother seeks Beau's help in guiding him away from nationalist tendencies.
- Beau suggests introducing the son to a TV show character, Duncan MacLeod from "Highlander," to provide a different perspective on warrior ethos and nationalism.
- He describes how Duncan's character evolves from a hotheaded warrior to someone who questions nationalism and patriotism.
- Beau believes that through watching this show with his mother, the son can learn valuable lessons and potentially shift his mindset away from hateful ideologies.
- He acknowledges that facts alone may not be effective in changing the son's views, suggesting that fiction like the TV show could help him understand deeper truths.
- Beau concludes by offering his assistance and expressing hope that this alternative approach may make a positive impact on the son.

# Quotes

- "Nationalism is inextricably tied to that journey of warrior-dom, it's there in the beginning and it fades as you know."
- "Sometimes fiction is what can get to truth, and that's what he's going to need to know."
- "I think it may be perfect for this."

# Oneliner

Beau helps a mother address her son's nationalist leanings by recommending the TV show "Highlander" to provide a different perspective on warrior ethos and nationalism.

# Audience

Parents, concerned individuals.

# On-the-ground actions from transcript

- Watch "Highlander" with individuals showing nationalist tendencies (suggested).

# Whats missing in summary

The emotional depth and context of the mother's plea for help in guiding her son away from nationalist ideologies.

# Tags

#Parenting #Nationalism #TVShowRecommendation #YouthEducation #CommunitySupport