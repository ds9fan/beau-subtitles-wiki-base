# Bits

Beau says:

- Introduces the story of Elizabeth Mary Jane Cochran, also known as Pinky, who pursued a career in journalism.
- Cochran conducted investigative reporting on factory conditions, particularly focusing on women in factories.
- Facing pushback from factories, she was reassigned to cover gardening and fashion shows, which she found unsatisfactory.
- Cochran decided to become a foreign correspondent and traveled to Mexico, but had to flee after advocating for an imprisoned journalist.
- Upon returning, she was relegated to covering fluff pieces but eventually quit.
- She joined the New York World, Pulitzer's paper, and made a significant impact with her investigative work.
- Cochran orchestrated being committed to an asylum on Blackwells Island to expose the neglect and brutality faced by women there.
- Her experiences led to reforms being implemented.
- Despite her impactful work, Cochran continued to seek new adventures, traveling around the world in 72 days and becoming an industrialist with patents.
- Cochran's willingness to embrace opportunities for adventure and follow them wherever they led was a key factor in her extraordinary life.

# Quotes

- "Because when she saw the white rabbit and had a chance for an adventure, she took it."
- "She followed the rabbit. She went where it led her."

# Oneliner

Beau shares the remarkable life of Elizabeth Mary Jane Cochran, known as Pinky, who fearlessly pursued journalism, exposed injustices, and embraced adventures wholeheartedly.

# Audience

History enthusiasts, aspiring journalists

# On-the-ground actions from transcript

- Advocate for reforms in institutions or systems that neglect or mistreat marginalized groups (exemplified)
- Embrace new opportunities for adventure and growth (exemplified)

# Whats missing in summary

The full transcript provides a detailed account of Elizabeth Mary Jane Cochran's life, inspiring viewers to seize opportunities for change and adventure.

# Tags

#Journalism #Adventure #Reforms #Injustice #Inspiration