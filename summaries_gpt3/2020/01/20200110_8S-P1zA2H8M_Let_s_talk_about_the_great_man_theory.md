# Bits

Beau says:

- Explains the "great man theory" and how it may influence recent decisions.
- Describes how the theory posits that certain individuals are born great leaders, destined to shape their own destiny.
- Compares the theory to the concept of divine right, where kings believed they were destined to rule based on traits and genetics.
- Raises questions about the theory's reliance on divine providence and a predetermined plan shaping governance.
- Suggests that the theory hinges on the great person shaping the world before being influenced by it.
- Points out how the current administration's interactions with world leaders and followers resonate with the great man theory.
- Argues that the theory can be shattered by understanding social sciences and acknowledging social pressures.
- Links the phenomenon of Trump's rise to a global social phenomenon driven by older demographics feeling unsettled by rapid changes.
- Notes the irony of those who believe in their own destiny being unaware of their participation in a broader social phenomenon.
- Advocates for being prepared for the end of this phenomenon to drive positive change towards interconnectedness and wider acceptance.

# Quotes

- "Some people are just born great, they're born to lead, and they're heroes of a sort when they're born."
- "The ultimate irony is that those who believe they're above it are not aware of the social phenomenon."
- "We need to be ready to jumpstart the drive for more interconnectedness, wider social norms that accept more and more people."

# Oneliner

Beau explains the "great man theory," linking it to Trump's rise as a global social phenomenon and advocating for readiness to drive positive change towards interconnectedness.

# Audience

Political analysts, activists

# On-the-ground actions from transcript

- Prepare for the end of the current phenomenon and work towards promoting interconnectedness and wider social acceptance (implied).

# Whats missing in summary

The full transcript provides a comprehensive analysis of the "great man theory" and its implications on current political dynamics, urging proactive steps towards societal progress. 

# Tags

#GreatManTheory #Trump #PoliticalAnalysis #SocialPhenomenon #Interconnectedness