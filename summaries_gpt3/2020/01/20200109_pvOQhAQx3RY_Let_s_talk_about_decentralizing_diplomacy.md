# Bits

Beau says:

- Defines diplomacy as managing international relations by a country's representatives abroad, but notes this definition is changing.
- Describes the transformation happening in the United States as the conservative movement is aging out and being replaced by younger, more liberal conservatives.
- Points out that traditional systems of diplomacy are being undermined by technology and a changing world.
- States that everyone is now a diplomat in the age of the internet, representing their individual nations online.
- Emphasizes the importance of understanding each other and finding common ground to prevent conflicts.
- Shares that people from 121 countries watched his channel in the last week, indicating a global audience.
- Stresses the power of individuals connecting across borders and realizing their commonalities.
- Expresses optimism about the decentralization of diplomacy due to increased access to information and communication.
- Encourages online users to be mindful of representing their nations positively in their social media interactions.
- Concludes by reflecting on the changing landscape of diplomacy in the digital age and wishing viewers a good night.

# Quotes

- "We're all diplomats now because we all have access."
- "Americans don't want war. Iranians don't want war."
- "If we can do that, we have successfully eliminated a power structure that the ruling elite held over us."
- "Diplomacy is being decentralized and it is amazing."
- "Y'all are watching trying to figure us out, good luck."

# Oneliner

Beau explains the changing face of diplomacy in the digital age, urging global understanding and representing nations positively online to decentralize power structures.

# Audience

Online users, Global citizens

# On-the-ground actions from transcript

- Connect with people from different countries online to foster mutual understanding and empathy (implied).
- Represent your nation positively in social media interactions to contribute to decentralized diplomacy (implied).

# Whats missing in summary

Beau's engaging delivery and nuanced insights on the evolving nature of diplomacy in the digital era can be best experienced through watching the full transcript.

# Tags

#Diplomacy #Decentralization #GlobalUnderstanding #DigitalAge #Representation