# Bits

Beau says:

- Beau shares a surprising question from his 5-year-old about building a wall, inspired by a visit to the Vietnam memorial in DC.
- The idea of building a wall to memorialize the lives lost during the current crisis is discussed, with inscriptions of quotes from leaders.
- The narrative criticizes political elites for their disconnectedness from the struggles of ordinary workers, especially in vital jobs like meatpacking.
- There's a call for a monument tied to leadership to serve as a reminder of the consequences of their decisions, particularly in handling the pandemic.
- The importance of basing decisions on reliable information rather than hope or personal desires is emphasized.
- Criticism is directed at how economic considerations have influenced decision-making, benefiting the wealthy at the expense of the average worker.
- Beau stresses the need for individual agency and decision-making in the absence of effective leadership during the crisis.
- The discourse touches on the profit motives behind certain actions and products during this challenging time.

# Quotes

- "If you are in public office, your words have consequences."
- "You have to lead yourself. You have to make a decision on what you're going to do."
- "Nobody can make it for you. Nobody can advise you on it."
- "This needs to be built. And it needs to be tied to the leadership."
- "You can't run a country on hope. On a guess."

# Oneliner

Beau shares his 5-year-old's unexpected question about building a wall to memorialize the current crisis, criticizing political elites and stressing the need for informed decision-making and personal agency amidst a lack of effective leadership.

# Audience

Citizens, decision-makers

# On-the-ground actions from transcript

- Build memorials tied to leadership to remind them of the consequences of their decisions (suggested)
- Base decisions on reliable information rather than hope or personal desires (implied)
- Support policies that prioritize the well-being of workers over economic considerations (implied)

# Whats missing in summary

The full transcript dives deeper into the themes of accountability, decision-making, and economic disparities during crises.

# Tags

#Leadership #DecisionMaking #PoliticalElites #Memorial #EconomicJustice