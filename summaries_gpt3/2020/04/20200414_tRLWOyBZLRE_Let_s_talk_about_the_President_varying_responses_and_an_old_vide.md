# Bits

Beau says:

- Criticizes the president for refusing to lead during the COVID-19 crisis.
- Points out the president's attempts to divert blame and not take responsibility for the situation.
- Mentions how the president disregarded advice from experts and failed to provide unified leadership, resulting in a prolonged crisis.
- Advocates for staying home as a solution to the pandemic, with government support for individuals.
- Calls for strong leadership to guide the country through the crisis and prioritize human life over the economy.
- Emphasizes the importance of unified action to effectively combat the pandemic.
- Expresses disappointment in the lack of leadership and decision-making at the national level.
- Urges individuals to make informed decisions and not rely solely on government directives.
- Warns against half measures and stresses the need for a comprehensive approach to battling the pandemic.
- Encourages everyone to prioritize public health and safety above all else.

# Quotes

- "His decisions caused this. And his decisions are prolonging it."
- "If you want leadership, you're going to have to do it."
- "You are expendable. You have to make the decision."
- "The shortest path to getting the economy back on track is everybody staying home for a month."
- "It's got to be everybody."

# Oneliner

Beau criticizes the president for lack of leadership during the pandemic, urging people to prioritize public health over the economy and make informed decisions.

# Audience

Americans

# On-the-ground actions from transcript

- Stay home for a month to help get the economy back on track (implied).
- Prioritize public health over economic concerns (implied).
- Make informed decisions regarding personal safety and well-being (implied).

# Whats missing in summary

Beau's detailed analysis of the president's actions and their impact, along with a call for unified action and prioritizing human life over economic interests.

# Tags

#Leadership #COVID-19 #PublicHealth #GovernmentResponse #CommunityLeadership