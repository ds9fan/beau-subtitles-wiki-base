# Bits

Beau says:

- Came across a video that kept appearing in his feed about hair growth tips that led to connecting with tonight's guest.
- They dive into the guest's evolution from beauty and fashion content to discussing social issues like racism and white privilege.
- The guest felt compelled to speak out on politics, particularly about Biden, due to frustrations with the DNC and mainstream media.
- Despite risking subscribers, the guest used their platform to express dissatisfaction with the political landscape.
- The guest hosts Saturday night live streams during quarantine, discussing pandemic profiteering and supporting local businesses.
- The ongoing theme of the guest's content revolves around addressing privilege, inequity, and amplifying marginalized voices.
- Both Beau and the guest touch on the responsibility of using their platforms to reach and impact their audience.
- The guest shares insights into staying at home during the pandemic and their background as a writer for digital content.
- They touch on the importance of voting and the challenges faced during primary elections amid voter suppression concerns.
- The guest expresses their perspective on the upcoming election, advocating for voting ethics and sharing their views on Biden and third-party options.

# Quotes

- "Compassion is not for the weak. Empathy is for the strong."
- "I'm fighting for a lot of the things that my ancestors were fighting for."

# Oneliner

Beau connects with a content creator discussing their shift from beauty to social issues, political frustrations, pandemic profiteering, amplifying marginalized voices, and the importance of compassion in a challenging world.

# Audience

Content creators, social justice advocates

# On-the-ground actions from transcript

- Support local businesses to counter pandemic profiteering (implied)
- Exercise compassion and empathy in interactions and activism (implied)
- Take steps to address privilege and inequity in your community (implied)

# Whats missing in summary

The full transcript provides depth on navigating activism, political engagement, and personal values in today's complex societal landscape. Watching the full interview offers a nuanced understanding of the guest's journey and perspectives.

# Tags

#SocialJustice #PoliticalActivism #CommunitySupport #Empathy #VotingRights