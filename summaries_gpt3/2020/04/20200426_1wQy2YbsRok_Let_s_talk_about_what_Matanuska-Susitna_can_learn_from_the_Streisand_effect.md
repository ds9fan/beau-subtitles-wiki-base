# Bits

Beau says:

- Barbara Streisand's situation serves as a lesson for the Matanuska Susitna School District in Alaska.
- Streisand fought to keep photos of her house private, which only piqued people's curiosity.
- Beau was asked for reading recommendations on a livestream and suggests books that the school district has banned.
- The school district banned teaching books like The Great Gatsby, Invisible Man, Catch-22, The Things They Carried, and I Know Why the Caged Bird Sings due to uncomfortable themes.
- Beau argues that literature with uncomfortable themes helps individuals distinguish facts from fiction and think critically.
- Exposure to such literature can prevent uninformed statements from people in power, like Lysol's recent statement.
- Beau stresses the importance of preparing students for life's discomforts through literature.
- Complaints about the banned books often stem from religious groups.
- Beau challenges the censorship, pointing out that uncomfortable themes exist in the Bible as well.
- He warns that once censorship starts, more books will follow.
- Beau offers support to librarians in the district who may need additional copies of the banned books.
- He encourages everyone to read these banned books, stating that they are significant in understanding alternative facts and censorship.
- Fiction, according to Beau, often leads to truth, making banned books invaluable.
- Beau concludes by encouraging his audience to have a good day.

# Quotes

- "Banned books are the best books."
- "Fiction is what gets you to truth."
- "Literature like this that covers uncomfortable themes, it creates a population that can distinguish facts from fiction."
- "Your book is next. The way it always works."
- "To make you a better person."

# Oneliner

Barbara Streisand's privacy fight teaches a lesson; banned books are vital for critical thinking and truth, challenging censorship.

# Audience

Educators, librarians, book lovers

# On-the-ground actions from transcript

- Support librarians in the school district by providing additional copies of banned books (suggested)
- Reach out via Twitter, Facebook, or email to assist in ensuring the availability of necessary texts (suggested)

# Whats missing in summary

The emotional impact of censorship on education and critical thinking is best experienced through the full transcript.