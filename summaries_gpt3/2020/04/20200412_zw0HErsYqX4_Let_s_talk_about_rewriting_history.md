# Bits

Beau says:

- History is constantly rewritten as we gather more information and gain a clearer picture of what happened.
- The idea of rewriting history is not inherently negative; it is a necessary process as history is a living thing.
- Some individuals resist rewriting history because they have given up on learning or are not interested in challenging American mythology.
- Historical figures, like Columbus and Pocahontas, are often portrayed inaccurately in American mythology.
- Rewriting history is vital for creating a positive future and avoiding repeating past mistakes.

# Quotes

- "History is a living thing."
- "People are flawed. People make mistakes."
- "Rewriting history is incredibly important."
- "Every time we get a new piece of information, every time the hindsight picture becomes more clear, we have to rewrite history."
- "We need to be aware of the notes so we don't make the same mistakes again."

# Oneliner

History is constantly rewritten as new information emerges, challenging American mythology and reminding us of the importance of rewriting history for a positive future.

# Audience

History enthusiasts, educators, activists

# On-the-ground actions from transcript

- Challenge and question the established historical narratives (implied)
- Educate yourself on accurate historical accounts and encourage others to do the same (implied)
- Advocate for the inclusion of diverse perspectives in historical education (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of the resistance to rewriting history and the importance of acknowledging flaws in historical figures for a more accurate understanding of the past.