# Bits

Beau says:

- Advises wearing masks to protect oneself, loved ones, and society.
- Mentions President not wearing a mask despite the advice.
- Adjusted his beard to be mask compliant.
- States his mask seals at the neck for better protection.
- Emphasizes the importance of setting a good example in the absence of real leadership.
- Addresses men's reluctance to wear masks due to questioning their invincibility and toughness.
- Demonstrates how to repurpose a tactic cool item into a mask.
- Encourages using any available item as a mask if needed.
- Stresses the significance of taking precautions seriously.
- Offers to do a live stream bonfire to combat boredom and foster community spirit.

# Quotes

- "Wear the mask. If you have the opportunity, wear the mask."
- "We are in this together and we will get through it together."
- "Now nobody has to know that you're not invincible."
- "You paid nine bucks for it, get some use out of it."
- "We do not have real leadership, we have to lead ourselves."

# Oneliner

Beau advises wearing masks, repurposing items as masks, and setting a good example in the absence of real leadership to protect oneself and society during the pandemic.

# Audience

Community members

# On-the-ground actions from transcript

- Use any available item as a mask if needed (exemplified)
- Join Beau's live stream bonfire for community engagement (suggested)

# Whats missing in summary

The full transcript provides detailed instructions on repurposing items as masks and offers community engagement through a live stream bonfire.