# Bits

Beau says:

- Talks about the importance of being okay with being wrong and the sunk cost fallacy.
- Mentions seeing the sunk cost fallacy being related to thought for the first time.
- Explains that a sunk cost is an expense in business that won't be recouped.
- Describes the sunk cost fallacy in the context of buying equipment that doesn't work as expected.
- Emphasizes the need to cut losses and move on rather than trying to salvage a bad investment.
- Draws parallels between the sunk cost fallacy in business decisions and beliefs or ideas.
- Points out that people often struggle to admit they're wrong and cling to incorrect beliefs.
- Stresses the importance of being open to changing opinions based on new information.
- Shares examples of how the sunk cost fallacy plays out in politics, specifically referencing supporters of a certain political figure.
- Asserts the need for responsive and articulate leadership, differentiating it from what he sees in current leadership.

# Quotes

- "It's OK to be wrong."
- "New information should always change your opinion."
- "We need responsive leadership."

# Oneliner

Be open to change, accept being wrong, and beware the sunk cost fallacy in thought, business, and politics for better leadership and decision-making.

# Audience

Individuals, Voters, Citizens

# On-the-ground actions from transcript

- Challenge and rethink your beliefs based on new information (implied).
- Advocate for responsive and articulate leadership in your community (implied).
- Encourage others to be open to changing opinions when new information arises (implied).

# Whats missing in summary

The full transcript provides a deeper insight into the importance of admitting when one is wrong, the consequences of the sunk cost fallacy, and the impact of these factors in business, thought, and politics. Watching the full video can provide a more comprehensive understanding of these concepts.

# Tags

#Leadership #SunkCostFallacy #Politics #OpenMindedness #DecisionMaking