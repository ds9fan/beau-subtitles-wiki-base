# Bits

Beau says:

- Criticizes the lack of leadership in the Oval Office during the current crisis.
- Points out that leadership involves telling people what they don't want to hear but motivating them through it.
- Notes the difference between leading and pandering to crowds.
- Expresses disappointment in the elected entertainer instead of a leader.
- Talks about governors taking action in the absence of federal leadership.
- Warns against authoritarian measures like state troopers at borders.
- Advises against putting first responders at risk of getting sick due to poor decisions.
- Comments on a community considering door-to-door welfare checks, expressing reluctance.
- Mentions a plan for welfare checks with color-coded signals.
- Criticizes the lack of leadership and calls for individuals to take charge and inform themselves.
- Emphasizes the importance of education and proper information in making decisions.
- Urges people to follow basic guidelines like staying home, washing hands, and not touching faces.
- Advocates for minimizing risk for essential workers by being efficient with trips outside.
- Questions the effectiveness of government in managing citizen health during this crisis.
- Encourages individuals to take responsibility since government leadership seems inadequate.

# Quotes

- "Leadership is telling a crowd of people what they don't want to hear, but doing it in a way that motivates them to get through it."
- "Aside from that, I suggest that maybe making sure that all of your first responders get sick is probably a bad idea."
- "You have to be responsible as an individual."
- "Most of the people we have in positions of power aren't leaders. They're panderers or they're entertainers."
- "You have to be a leader."

# Oneliner

Beau criticizes the lack of leadership in the Oval Office during the crisis and urges individuals to take charge and inform themselves amidst governmental inadequacies.

# Audience

Citizens, Individuals

# On-the-ground actions from transcript

- Stay informed and educated on the current situation (implied).
- Follow basic guidelines like staying home, washing hands, and not touching faces (implied).
- Minimize risk for essential workers by being efficient with trips outside (implied).
- Take responsibility as an individual and for society as a whole (implied).

# Whats missing in summary

The full transcript provides detailed insights into the importance of leadership, individual responsibility, and informed decision-making during times of crisis.

# Tags

#Leadership #Responsibility #Inform #Crisis #Government #Individuals