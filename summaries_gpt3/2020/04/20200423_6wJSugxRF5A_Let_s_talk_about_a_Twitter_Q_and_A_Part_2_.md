# Bits

Beau says:

- Conducting a Twitter Q&A to reach those who can't attend live streams.
- Addressing questions about moving away from capitalism and consumer culture.
- Believes the nation will have to move away from consumer capitalism due to its unsustainability.
- Suggests ways to transition, like growing your own food and reducing consumption.
- Warns about the potential rise of a fascistic government in the U.S.
- Prefers not to focus on his colorful backstory but on current issues.
- Advocates for focusing on those who understand the severity of COVID-19 rather than skeptics.
- Surprised by the lack of proactive response to the pandemic from government agencies.
- Criticizes the F-35 program and questions the necessity of fighter pilots in modern warfare.
- Doubts the Republican Senate's ability to stand up to Trump.
- Expresses support for the Kennedy half dollar from 1976 as his favorite U.S. coin design.
- Shares insights on the role of the CIA in focusing on human intelligence and determining intent.
- Criticizes Governor DeSantis's handling of safety measures in Florida theme parks.
- Supports Gaia theory and the idea of earth as a self-regulating entity.
- Explains the term "operator" in the context of special operations or intelligence communities.

# Quotes

- "We're definitely going to move away from the consumer capitalism that we have today."
- "I try not to get into it, to be honest, because once you start talking about that, that's all people want to talk about."
- "If they really just don't get it, they don't get it."
- "We didn't act. We have teams within DOD. We have the CDC. We have entire agencies devoted to this."
- "I think it's outdated before it's ever in the field."

# Oneliner

Beau addresses questions on moving away from consumer capitalism, warns about fascism, and criticizes governmental responses amidst other diverse topics.

# Audience

Socially conscious individuals.

# On-the-ground actions from transcript

- Stay informed and follow health guidelines to combat COVID-19 (implied).
- Advocate for sustainable practices like growing your own food and reducing consumption (implied).
- Support agencies focusing on human intelligence to determine intent (implied).
- Stay critical of government actions and hold leaders accountable (implied).

# Whats missing in summary

Insights on Beau's views on perennial favorite videos and recommendations for sharing them.

# Tags

#ConsumerCapitalism #Fascism #COVID19 #GovernmentResponse #Sustainability #Intelligence #SpecialOperations #SocialResponsibility