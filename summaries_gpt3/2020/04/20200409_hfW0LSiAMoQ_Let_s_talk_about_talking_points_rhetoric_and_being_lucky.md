# Bits

Beau says:

- Addresses the concept of nationalism and how it is being invoked in current rhetoric.
- Points out the dangers of nationalism, especially during times of crisis.
- Emphasizes the importance of global cooperation in fighting against the current situation.
- Mentions the international support received in the form of medical supplies from various countries.
- Advocates for pushing back against nationalist rhetoric and focusing on global unity.
- Stresses the need for immediate cooperation and support across borders.
- Warns about the ineffectiveness of trying to contain the situation by locking down borders.
- Calls for proactive and worthy actions to be taken, rather than just invoking nationalism.
- Criticizes lackluster leadership and the damage caused to the country's reputation.
- Encourages staying at home, practicing good hygiene, and unity in the global fight against the situation.

# Quotes

- "We are in this together."
- "It doesn't care what color your passport is."
- "This thing, it does not care about your borders."
- "Do something worthy of being proud of."
- "We are all in this together."

# Oneliner

Beau addresses nationalism, advocates for global unity, and criticizes lackluster leadership during a time of crisis, stressing the importance of international cooperation in fighting against the current situation.

# Audience

Global citizens

# On-the-ground actions from transcript

- Reach out to international organizations or individuals to offer support and aid (suggested)
- Advocate for global cooperation and unity in fighting against the situation (implied)
- Practice good hygiene, stay at home, and avoid touching your face (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of nationalism, the importance of international cooperation, and the need to push back against divisive rhetoric to effectively combat the current situation.

# Tags

#Nationalism #GlobalUnity #Cooperation #Leadership #InternationalAid