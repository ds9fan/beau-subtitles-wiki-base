# Bits

Beau says:

- Explains the cottage industry within news media and journalism involving the use of stock photos and footage.
- Describes his experience as a photojournalist selling photos to news outlets.
- Mentions that media outlets rely heavily on freelancers for visual content.
- Addresses the misconception that using stock footage means an event didn't happen.
- Points out that major news networks use old footage to fill visual space, not as evidence of a conspiracy.
- Suggests that labeling stock or file footage is an ethical practice that is often overlooked.
- Notes that some people misconstrue laziness in news management as evidence of fake news.
- Acknowledges the consumer demand for immediacy driving the use of stock footage.
- Encourages freelancers to push for ethical standards in labeling their work.
- Concludes by reminding viewers to understand how news media operates.

# Quotes

- "It's not evidence of a conspiracy. It's evidence of the consumer's desire to constantly have visual input."
- "This isn't evidence of a conspiracy. It's not. This is evidence of bad news management."
- "It's not true. But that's not the case. It just means that your news outlet's lazy."
- "Try to hold these major outlets to an ethical standard."
- "That alone is not evidence of anything other than a misunderstanding about how news media works."

# Oneliner

Beau explains the use of stock photos and footage in news media, dispelling misconceptions and advocating for ethical standards in labeling.

# Audience

News Consumers

# On-the-ground actions from transcript

- Push for news outlets to ethically label stock or file footage (suggested).
- Hold major news networks accountable to ethical standards in visual content usage (exemplified).

# Whats missing in summary

The full transcript provides a comprehensive breakdown of how the news media industry uses stock footage and photos, urging viewers to be discerning and informed consumers of news.

# Tags

#NewsMedia #StockFootage #Journalism #EthicalStandards #Misconceptions