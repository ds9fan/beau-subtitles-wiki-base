# Bits

Beau says:

- Medical professionals are being confronted in public for wearing scrubs, which is causing concern.
- People are harassing doctors, nurses, and techs in grocery stores and parking lots about their scrubs.
- It's irrational to harass medical professionals and get closer to them when concerned about potential illness transmission.
- Droplets may be released if medical professionals are upset and raise their voice due to harassment.
- Fabric doesn't harbor viruses well, unlike hard surfaces where viruses can live longer.
- Nurses and doctors take precautions like changing shoes, using booties, and disinfecting before leaving the hospital.
- People harassing medical professionals in public likely do not take the same precautions.
- Not everyone wearing scrubs has been in contact with infected patients.
- Medical professionals disinfect their belongings regularly, a practice that others may not follow.
- Proper hand-washing techniques, like focusing on fingertips and thumbs, are more effective than harassing healthcare workers.
- PPE shortage is a concern, and harassing healthcare workers is not a solution.
- Nurses are a close-knit community, and harassing one could lead to repercussions from others in the field.
- Harassing medical professionals adds unnecessary stress to their already challenging work environment.
- Beau suggests contacting representatives about the PPE shortage instead of harassing healthcare workers.
- Medical professionals need support, not harassment, especially during times of crisis.

# Quotes

- "Medical professionals certainly have enough on their plate. They don't need to be worried about being harassed when they stop to pick up essentials."
- "Nurses are like the mob. If you harass a nurse, every nurse in the country is gonna know your face."
- "Doing that [proper handwashing] would be a whole lot more effective at keeping you safe than harassing some nurse or doc out in public."
- "We have enough problems in society right now. We don't need a bunch of people running around causing more problems out of ignorance and fear."
- "If you don't want to say thank you, just stay away."

# Oneliner

Confronting medical professionals over scrubs in public is irrational and dangerous, support rather than harass healthcare workers during these challenging times.

# Audience

Healthcare supporters

# On-the-ground actions from transcript

- Call your representatives about the PPE shortage (suggested)
- Practice proper handwashing techniques (implied)
- Support medical professionals instead of harassing them (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of why confronting medical professionals over scrubs in public is illogical and harmful, as well as offering insights into the precautions healthcare workers take and the repercussions of harassment.

# Tags

#Healthcare #Support #PPEshortage #CommunityPolicing #PublicHealth