# Bits

Beau says:

- Explains how the brain seeks patterns to make sense of things like clouds resembling bunnies or faces.
- Talks about the motivation behind various theories - the desire to bring order out of chaos for comfort.
- Points out the common agreement among theories about ushering in a new authoritarian rule.
- Criticizes a theory that suggests the normalization of masks and gloves is a tactic for control.
- Questions the feasibility of theories that undermine the government's basis and ability to protect its citizens.
- Mentions how the enforcement class may be reluctant to exercise control due to undermining methods.
- Talks about the impact on military readiness and the restriction of social gatherings under authoritarian rule theories.
- Argues that chaos does not benefit those in power in the long term.
- Suggests a more plausible theory that an authoritarian system already exists, unnoticed by many.
- Addresses the coercion within the current system that limits individual autonomy and agency.
- Points out that many advocating against authoritarian rule are already living under its grasp.
- Notes that the government has had powers to enforce measures like stay-at-home orders for a long time.
- Asserts that surveillance measures should be opposed, especially due to government data handling concerns.
- Emphasizes the importance of existing in a system that allows freedom and autonomy.

# Quotes

- "Your brain is seeking out a pattern. It's trying to make sense of it."
- "The main plot point of these theories doesn't make sense."
- "The authoritarian rule people who subscribe to these theories are worried about, it's already here."
- "If you want freedom, you have to exist in a system that allows it."
- "If somebody can coerce you into going to risk yourself for their economic benefit mainly, and you get the crumbs, you are existing in Orwell's future."

# Oneliner

Beau explains theories seeking order from chaos, questions new authoritarian rule claims, and argues that current coercion hints at an existing authoritarian system.

# Audience

Activists, skeptics, truth-seekers

# On-the-ground actions from transcript

- Advocate for transparency in government surveillance practices (implied)
- Educate others about the potential coercion within existing systems (implied)
- Support policies that enhance individual autonomy and agency (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of how individuals may unknowingly exist within an authoritarian system and the importance of advocating for true freedom within societal structures.

# Tags

#BrainFunction #Theories #Authoritarianism #Coercion #Freedom