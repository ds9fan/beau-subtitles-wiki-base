# Bits

Beau says:

- The president provided inaccurate information about how the government functions.
- Operating under the assumption that the president is wrong when speaking about basic civics is safe.
- The president's tweets about reopening states were incorrect; governors have authority in their states.
- The federal government can remove federal guidance without consulting anyone.
- Local authorities, like county commissions or city councils, have more authority than the president in certain matters.
- The idea of a nationwide reopening is not realistic; there will be rolling changes and areas under stay-at-home orders.
- Social distancing should still be in effect even if stay-at-home orders are lifted.
- Reopening cities may lead to spikes in cases and a return to stay-at-home orders.
- It's vital to understand that the situation may last a year and a half until there's a vaccine or effective treatment.
- Precautions like wearing masks, washing hands, and minimizing trips are still necessary.

# Quotes

- "Operate under the assumption that whatever he says, the exact opposite is true."
- "Your local county commission or your local mayor, your local city council has more authority than the president."
- "There's not gonna be this grand, overwhelming victory where we all just march out into the streets and hold a parade."
- "You're not just protecting yourself, you're protecting others."
- "It's probably gonna last a year and a half, maybe, until there's a vaccine, or at least until there's a really solid treatment."

# Oneliner

Operating under the assumption that the president is wrong about government functions is safe, and local authorities hold more power than the president; expect rolling changes and continued precautions as the situation may last a year and a half.

# Audience

Citizens, Local Officials

# On-the-ground actions from transcript

- Keep practicing social distancing and wearing masks (implied).
- Continue to wash hands frequently and avoid touching your face (implied).
- Minimize trips outside and follow local health guidelines (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the inaccuracies in the president's statements regarding government functions and offers insights into the long-term implications and precautions necessary during the ongoing situation.

# Tags

#GovernmentFunctions #PresidentialStatements #COVID19 #Precautions #LocalAuthorities