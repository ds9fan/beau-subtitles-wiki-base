# Bits

Beau says:

- Dates for reopening the economy like May 1st or June 15th are irrelevant as the power lies with individuals, not politicians.
- The economy will only recover when people have confidence, not when the government reopens prematurely.
- Reopening too soon without consumer confidence will lead to a severe economic depression.
- Premature reopening can devastate small businesses with no consumer spending.
- Politicians pushing for immediate reopening are prioritizing their own interests over public safety.
- Hotspots for the virus are not just big cities but also smaller areas like Albany, Georgia, Idaho, and Mississippi.
- The pandemic will only end with a reliable treatment or vaccine, not based on political wishes.
- Trusting experts and following guidelines is more effective than protesting and spreading misinformation.
- Rushing to reopen the economy without proper precautions will lead to more spikes and loss of public confidence.
- Government relief for workers is vital until the situation is under control to prevent economic collapse.

# Quotes

- "The economy will get back on track when you have confidence. Not before."
- "If you want to destroy this economy, support these politicians."
- "The government should spend its time trying to do its job, rather than trying to spin its way out of this."
- "If you're done with the US, if you're just ready to bankrupt everybody, let's reopen now."
- "Wash your hands. If you have to go out, wear a mask. Don't touch your face."

# Oneliner

Reopening the economy prematurely without consumer confidence will lead to economic collapse, prioritize public safety over politics. 

# Audience

Individuals, Public

# On-the-ground actions from transcript

- Stay at home, follow guidelines for safety (implied)
- Wear a mask if you have to go out (implied)
- Wash hands regularly to prevent the spread of the virus (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the economic repercussions of premature reopening and the importance of public confidence in restoring the economy.

# Tags

#Economy #Reopening #Government #PublicSafety #TrustExperts