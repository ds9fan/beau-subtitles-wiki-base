# Bits

Beau says:

- Explains the steps he takes to choose a study to trust, prompted by a question from a follower.
- Emphasizes the importance of peer-reviewed studies and the significance of sample size in research.
- Mentions the methodology of the study and how participants were recruited as vital factors.
- Suggests that studies are not meant to provide definitive answers but to contribute to a broader understanding.
- Raises the point of understanding what exactly a study is measuring to avoid skewed results.
- Provides an example of how studies can be manipulated to fit a narrative, using a scenario of changing bar closing times.
- Advocates for looking beyond the apparent conclusion of a study and considering common-sense reasons for the outcomes.
- Gives an example of a study suggesting a causal relationship between women owning horses and living longer, offering practical explanations for this correlation.
- Critically analyzes a current study on air pollution and its link to health outcomes, questioning the significance of the findings based on population distribution.
- Concludes by sharing his approach of seeking common-sense explanations before putting faith in a study.

# Quotes

- "A larger sample size is almost always better."
- "Studies aren't there to fully answer a question. They're there to provide more information so you can combine it with other stuff to draw a conclusion."
- "I ask myself if there's another reason for the apparent conclusion. A common-sense reason."
- "If there is another reason, a common sense reason, the study has to be spectacular for me to put any stock in it at all."
- "That study alone doesn't do it for me."

# Oneliner

Beau outlines his method for selecting trustworthy studies, stressing the importance of peer-review, sample size, methodology, and common-sense reasoning in evaluating research findings.

# Audience

Researchers, students, academics

# On-the-ground actions from transcript

- Verify the peer-review status of studies before accepting their conclusions (implied)
- Scrutinize sample sizes in research to ensure representativeness (implied)
- Question the methodology and participant recruitment of studies for comprehensive evaluation (implied)
- Look beyond apparent conclusions and seek common-sense explanations for research outcomes (implied)

# Whats missing in summary

The full transcript provides detailed insights into Beau's thought process when evaluating studies, offering practical guidelines for discerning trustworthy research findings.

# Tags

#Research #Study #Evaluation #PeerReviewed #Methodology #CommonSense