# Bits

Beau says:

- Public service announcement about volunteering for a study led by Dr. Fosse with 9,999 other people.
- Goal is to track the spread of the virus among those who may not have known they had it by analyzing antibodies in blood.
- Volunteers must be 18 or older, currently healthy, not showing symptoms, and not tested positive before.
- The study involves a teleconference patient exam and blood analysis for antibodies.
- Information gathered will help understand the spread and assist in mitigation efforts.
- Volunteers can email clinicalstudiesunit@NIH.gov to participate.
- Beau acknowledges trust issues but stresses the importance of this study for a clearer picture.
- Results may reveal how far the virus has spread unnoticed and guide future containment efforts.
- Participation is voluntary, no pressure to join if uncomfortable.
- Sending an email expressing interest can lead to more information about the study.

# Quotes

- "If you're up to it, send the email."
- "Knowing how far it has spread among people who didn't have severe symptoms is really important."

# Oneliner

Beau encourages volunteering for a study tracking virus spread through antibodies, stressing the importance of participation for a clearer picture of the situation.

# Audience

Potential volunteers

# On-the-ground actions from transcript

- Email clinicalstudiesunit@NIH.gov expressing interest in the study (suggested)

# Whats missing in summary

Details on the potential impact of volunteering and how it can contribute to understanding and combating the virus on a larger scale.

# Tags

#Volunteering #PublicService #ResearchStudy #CommunityHealth #VirusSpread