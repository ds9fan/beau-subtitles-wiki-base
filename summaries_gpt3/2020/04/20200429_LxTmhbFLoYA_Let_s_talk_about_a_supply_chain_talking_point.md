# Bits

Beau says:

- Criticizes the idea of bringing all production back home to fix the supply chain, labeling it absurd and a talking point pushed by those in power.
- Explains that the current supply chain issues are due to global problems and increased demand, not evil foreigners as suggested by the talking point.
- Argues that decentralization and distributed networks usually improve systems, pointing out the flaws in isolationist approaches.
- Notes that localized supply chains can lead to vulnerabilities, as seen in the struggling meat packing industry with ripple effects on ranchers.
- Mentions the predicted shortages that have now occurred due to lack of preparation and blames the need for scapegoats on those in power.
- Compares globalists who want one jurisdiction for the world to nationalist governments, stating that the difference is only in scale.
- Advocates for redundancy, decentralization, and distribution of networks as beneficial, contrasting this with the weakening effects of isolationism and nationalism.
- Emphasizes the importance of strong community networks over relying on state and federal governments for protection.
- Concludes by warning that not learning from past mistakes will result in more vulnerabilities and situations like the current supply chain issues.

# Quotes

- "If you have one, you have none."
- "A nationalist is just a low ambition globalist."
- "We can't rely on governments to make sure that we're all okay."
- "It's present in a lot of other videos but I've never really gone into it in this way."
- "It's very clear that we haven't learned anything from this."

# Oneliner

Beau debunks the absurdity of isolationist supply chain fixes, advocating for decentralization and community resilience over nationalist ideologies.

# Audience

Community members, Activists

# On-the-ground actions from transcript

- Build strong community networks for resilience (implied)
- Advocate for decentralized systems in your community (implied)
- Challenge nationalist ideologies with facts and logic (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the flaws in isolationist supply chain fixes and the importance of community resilience in combating global issues.

# Tags

#SupplyChain #Decentralization #CommunityResilience #Nationalism #Globalism