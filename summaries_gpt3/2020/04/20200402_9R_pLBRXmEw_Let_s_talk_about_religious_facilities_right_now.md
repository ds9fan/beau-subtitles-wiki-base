# Bits

Beau says:

- Acknowledges people pausing or canceling Patreon support, assures no need for explanations.
- Raises concern about religious leaders holding congregations despite risks in the current climate.
- Advocates for livestreaming religious services as a safer alternative to in-person gatherings.
- Points out that people tend to let their guard down in religious facilities, engaging in risky behaviors.
- Questions the extreme test of faith in expecting divine protection during a pandemic.
- Suggests using livestreaming platforms like YouTube for congregations to avoid physical gatherings.
- Urges people to wash hands, avoid touching faces, and stay home to follow best practices.
- Stresses the importance of not needing government orders to understand staying home is necessary.

# Quotes

- "You owe me precisely zero explanation, none."
- "Livestream. Livestream."
- "In a moment of doubt, that's when it overcame them."
- "Please do your part. Wash your hands. Don't touch your face. Stay home."
- "You know what best practices are."

# Oneliner

Beau raises concerns about risky congregational gatherings, advocating for livestreaming services and following best practices during the pandemic.

# Audience

Religious Leaders

# On-the-ground actions from transcript

- Livestream religious services to avoid risky in-person gatherings (exemplified).
- Wash your hands, avoid touching your face, and stay home (exemplified).

# Whats missing in summary

Importance of prioritizing community safety and well-being over traditional practices during challenging times.

# Tags

#ReligiousLeaders #Livestreaming #PandemicSafety #BestPractices #CommunitySafety