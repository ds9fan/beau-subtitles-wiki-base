# Bits

Beau says:

- People are advocating to open the U.S. back up despite numbers indicating it's a bad idea.
- Loyalty to the president is chosen over the well-being of neighbors.
- Authoritarian measures by local governments wouldn't be necessary if there was proper leadership.
- Lack of leadership from the president results in confusion among the American people.
- Understanding basic science and history should guide individuals to take necessary precautions without police intervention.
- Referencing the 1918 pandemic, Beau warns against easing restrictions too soon.
- Advocating to reopen for economic benefit disregards risking friends and family.
- The economic impact on the 99% should not be overlooked for the benefit of the 1%.
- Lack of financial support from the government could lead to a collapse of the entire economic system.
- Beau urges people to stop listening to the president and prioritize safety over economic interests.

# Quotes

- "You shouldn't need a cop for this. You don't need a cop. You need a science book and a history book."
- "Easing up early now is wrong. It's the wrong move. We're winning, but we haven't won."
- "If you think opening the US right now, opening everything back up, going back to business as usual is a good idea, you need to get a science book."
- "He wants to be a leader. Then he has to lead."
- "We have to do the bare minimum to keep each other safe and not sell each other out because our betters have told us to die for their profits."

# Oneliner

Advocate for science, history, and community safety over premature reopening for economic gain, prioritizing collective well-being.

# Audience

Community members, advocates

# On-the-ground actions from transcript

- Stay at home, wash hands, avoid touching face to keep each other safe (exemplified)
- Advocate for financial support from the government to prevent economic collapse (suggested)
- Educate others on the importance of science and history in making informed decisions (implied)

# Whats missing in summary

In watching the full transcript, the emotional impact and urgency conveyed by Beau's message are best captured.

# Tags

#CommunitySafety #EconomicImpact #Leadership #Science #History