# Bits

Beau says:

- Societal change is imminent and historically triggered by events like the current one, potentially related to income inequality.
- Many seek an egalitarian society but may not know exactly what they are looking for due to the flaws in the current system.
- Reading social media arguments led Beau to reconsider his opinion, even changing it based on a counter-argument against Universal Basic Income (UBI).
- Beau challenges the idea that people wouldn't work at minimum wage jobs like McDonald's if their basic needs were met, pointing out the societal stigma attached to such jobs due to low pay.
- He questions the belief that comfort is the enemy of radical thought, acknowledging that even in times of comfort, people can still strive for radical change.
- Beau argues that radical thought and mutual aid, like making masks during the current crisis, can occur even when people are not pushed to extreme discomfort.
- Drawing from experiences in prison, Beau illustrates that human nature drives individuals to seek improvement even in challenging environments where basic needs are met.
- The current system is deemed broken by Beau, leading him to reject a return to the status quo and advocate for something different, although unsure if UBI is the solution.

# Quotes

- "People always want to better themselves, better their situation, better society, most people."
- "Human nature provides the carrot."
- "Even in one of the most depressing environments in the world, human nature still drives for improvement."

# Oneliner

Societal change, driven by income inequality, challenges stigmas and comforts, showing human nature's pursuit of improvement.

# Audience

Activists, policymakers, advocates

# On-the-ground actions from transcript

- Engage in mutual aid efforts like making masks for hospitals to support the community (exemplified)
- Advocate for systemic changes to address income inequality and societal flaws (implied)

# Whats missing in summary

The full transcript provides a deep exploration of societal change, income inequality, comfort's role in radical thought, and human nature's drive for improvement.

# Tags

#SocietalChange #IncomeInequality #UBI #RadicalThought #MutualAid