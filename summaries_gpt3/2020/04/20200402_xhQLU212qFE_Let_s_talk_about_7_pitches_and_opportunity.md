# Bits

Beau says:

- Recounts a significant baseball game from 1931 involving a 17-year-old pitcher named Mitchell facing Babe Ruth and Lou Gehrig.
- Mitchell struck both Ruth and Gehrig out, a rare feat that doesn't happen often.
- Draws a parallel between hitting a home run in baseball and achieving success in life.
- Mentions that during tough times, like the current economic situation, there are opportunities for those on the lower end of the income spectrum to rise.
- Encourages utilizing the current period to figure out goals and make concrete plans for future success.
- Shares anecdotes of friends who found financial success during a recession by planning ahead.
- Urges viewers to identify what they want and strategize how to achieve it, especially during economic downturns.
- Emphasizes the importance of being prepared and ready to take action when the economy improves.
- Suggests starting small and growing gradually, taking advantage of competitors' weaknesses and customer dissatisfaction.
- Calls for viewers to be proactive in pursuing their goals and dreams, leveraging the current downtime to plan for future success.

# Quotes

- "She, 17-year-old. That is a once in a lifetime thing right there."
- "Most people get struck out a whole lot more than they hit a home run."
- "As we come out of this is the time to make your move."

# Oneliner

Be prepared, plan ahead, and seize opportunities to achieve success, even during tough times.

# Audience

Entrepreneurs, dreamers, planners.

# On-the-ground actions from transcript

- Identify your goals and what you want to achieve during this downtime (suggested).
- Develop a concrete plan outlining steps you can take to reach your goals (suggested).
- Be proactive in preparing for future opportunities as the economy improves (implied).

# Whats missing in summary

Practical examples of how individuals can strategically position themselves during tough times to achieve success in the future.

# Tags

#Baseball #Success #Planning #Opportunities #EconomicDownturn