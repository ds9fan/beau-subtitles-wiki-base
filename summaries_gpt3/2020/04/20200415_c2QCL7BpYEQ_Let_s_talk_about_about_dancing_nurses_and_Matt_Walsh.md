# Bits

Beau says:

- Nurses dancing has sparked controversy, with Matt Walsh criticizing their choice to dance instead of working.
- Beau shares a story about Joe Bear, who once danced before a high-stakes mission in a combat zone.
- Dancing before battle has historical roots in boosting morale and psychological preparedness.
- Nurses dancing may serve a similar purpose, mentally preparing themselves for their challenging work.
- Posting dance videos online may help raise morale and show solidarity with those in more strict hospital environments.
- Walsh's criticism of healthcare workers causing economic issues is refuted by Beau.
- Staying home aims to prevent hospitals from being overrun, not because they already are.
- Walsh's lack of understanding of high-stress jobs is evident in his comments about nurses.
- Right-wing pundits like Walsh are looking for scapegoats instead of taking responsibility for their actions.
- Beau stresses the importance of supporting healthcare workers and ensuring their mental readiness for their critical roles.

# Quotes

- "Dancing before battle, that dates back to prehistory."
- "Nurses, docs, dance, do primal chanting, paint your face, whatever it takes for you to pregame and get in the mindset to walk into that room and do your job, do it."
- "If you have a platform right now, you have an obligation to do your part to lead people, to keep them safe."

# Oneliner

Beau defends nurses' dancing as a morale booster and mental preparation for their challenging work while critiquing those like Matt Walsh who fail to understand their high-stress jobs.

# Audience

Healthcare advocates

# On-the-ground actions from transcript

- Support healthcare workers by showing appreciation for their efforts and understanding the challenges they face (implied)
- Educate others on the importance of mental preparedness for high-stress jobs, like those in healthcare (implied)
- Advocate for proper support and resources for frontline workers to ensure their well-being (implied)

# Whats missing in summary

The full transcript provides a deep dive into the importance of morale and mental preparedness for healthcare workers, shedding light on misconceptions and criticisms surrounding their actions.

# Tags

#Healthcare #Nurses #MentalHealth #Support #CommunityAwareness