# Bits

Beau says:

- Addresses the concept of rights and responsibilities, particularly in the context of the U.S. Constitution's Bill of Rights.
- Emphasizes the importance of understanding the inherent responsibilities that come with exercising rights.
- Gives examples like the right to free speech and assembly, linking them to the responsibility to ensure those rights are exercised in a way that benefits society.
- Talks about the Second Amendment and the responsibility it entails in protecting the community.
- Criticizes those who claim to be ready to defend rights but fail to fulfill basic responsibilities.
- Raises the idea that ultimate freedom comes with shared responsibility for everything.
- Points out the selfishness of focusing only on rights without acknowledging responsibilities.
- Challenges the notion of using certain groups as shields for personal struggles.
- Calls for consistent support and care for all, not just in times of crisis that impact the middle class.
- Condemns the privilege and transparency of those who prioritize rights over obligations to society.

# Quotes

- "If you are not concerned about your responsibilities and you're only concerned about your rights, you're just selfish."
- "Don't use them for that."
- "It just reeks of privilege."
- "You have the right to free speech. It's in the First Amendment. Why was that put in there? Was it so anybody could say any silly little thing that popped into their head? No, of course not."
- "But you can't make the sacrifice of staying home and watching Netflix to protect your community."

# Oneliner

Beau delves into the intertwined nature of rights and responsibilities, exposing the selfishness of prioritizing one over the other, urging for a balance that truly benefits society.

# Audience

Rights and Responsibilities Activists

# On-the-ground actions from transcript

- Ensure that your exercise of rights is accompanied by a sense of responsibility towards society (implied).
- Advocate for policies that support living wages and access to healthcare for all individuals (implied).
  
# Whats missing in summary

The full transcript provides a comprehensive exploration of the societal implications of prioritizing individual rights over collective responsibilities, urging for a more balanced and empathetic approach to community welfare.