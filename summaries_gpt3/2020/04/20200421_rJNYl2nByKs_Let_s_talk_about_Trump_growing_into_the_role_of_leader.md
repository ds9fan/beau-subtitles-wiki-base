# Bits

Beau says:

- Trump is being seen as growing into the role of a world leader by closing borders to immigration, branding it as protecting American jobs.
- The decision to close borders is seen as a move to protect against the severity of the situation and encourage people to stay home.
- Trump's base understands there are no jobs to protect and that his branding is a way for him to demonstrate leadership.
- The real motivation behind closing borders is to show leadership and protect both Americans and foreign workers.
- Trump had to frame his decision in a certain way due to his international image as a "complete buffoon."
- Beau suggests that Trump is trying to empower governors to become leaders in their own right by taking actions such as reopening states cautiously.
- Trump's actions are viewed as an attempt to show leadership and create a sense of control in a chaotic situation.
- Despite criticisms of Trump's handling of the situation, Beau believes in giving him a second chance to prove himself as a leader.
- Beau compares Trump's actions to those of a true leader who empowers others and sacrifices for the greater good.
- He acknowledges the skepticism towards Trump's motives but ultimately believes in rallying behind the overall plan.

# Quotes

- "He's becoming the leader we all knew he was."
- "It was making sure that those governors could be seen as true leaders."
- "Trump is a real leader, and he's finally showing it."

# Oneliner

Trump is seen as growing into a world leader by closing borders, framing it as protecting American jobs, and empowering governors to take on leadership roles in response to the crisis.

# Audience

Political observers

# On-the-ground actions from transcript

- Rally behind leaders who demonstrate true leadership (implied)
- Empower governors to take on leadership roles (implied)

# Whats missing in summary

Analysis of the potential impact of Trump's actions on the political landscape and public perception.

# Tags

#Leadership #COVID-19 #Trump #Immigration #Governors