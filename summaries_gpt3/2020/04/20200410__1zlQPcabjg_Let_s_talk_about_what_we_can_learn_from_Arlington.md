# Bits

Beau says:

- Explains the history of Arlington National Cemetery, which was established in the 1860s after the Battle of the Wilderness.
- Mentions that Arlington was chosen for the cemetery due to its aesthetics, being already cleared, high ground to avoid flooding, and because it was Robert E. Lee's home.
- Points out that Robert E. Lee's decision to fight against the Union led to his home becoming a national cemetery, serving as a political statement.
- Draws a parallel between Arlington and Hart Island in New York, where unclaimed individuals are laid to rest.
- Expresses the need to not overlook the issues related to unclaimed bodies and the proximity to major problems.
- Suggests repurposing golf courses as visible sites for victims, contrasting with the hidden nature of Hart Island.
- Criticizes the current president for lacking basic information about antibiotics and viruses.
- Praises governors for their leadership in managing the crisis and combating federal incompetence.
- Emphasizes the importance of public reminders and visible sites as a lesson for the future.
- Advocates for repurposing golf courses as reminders of the current crisis.

# Quotes

- "We need to not overlook the issues related to unclaimed bodies and the proximity to major problems."
- "Those golf courses, to me, they're perfect."
- "They do not care about you."

# Oneliner

Beau delves into the history of Arlington National Cemetery, proposes repurposing golf courses as visible sites for victims, and criticizes federal incompetence during the crisis.

# Audience

Public citizens

# On-the-ground actions from transcript

- Repurpose golf courses as visible sites for victims (suggested)
- Advocate for public reminders and visible memorials (implied)

# Whats missing in summary

The full transcript provides a deeper historical context of Arlington National Cemetery and criticizes federal leadership during the crisis.

# Tags

#ArlingtonNationalCemetery #History #FederalIncompetence #GolfCourses #PublicReminders