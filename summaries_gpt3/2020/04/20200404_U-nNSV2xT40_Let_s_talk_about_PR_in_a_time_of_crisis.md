# Bits

Beau says:

- Critiques the Trump administration's handling of the current crisis.
- Points out the administration's focus on PR stunts rather than real solutions.
- Calls out Jared Kushner for giving advice to voters instead of managing the crisis.
- Questions the competence of the administration's leadership in managing the response.
- Mentions the administration's failure to properly maintain the strategic national stockpile.
- Criticizes the administration's use of PR to cover up mistakes and missteps.
- Talks about the administration's failure to follow through on promises like ordering ventilators.
- Emphasizes the lack of substance and leadership in the Trump administration's response.
- States that medical professionals are working despite the lack of leadership from the administration.
- Urges people to think carefully about who they vote for, considering future crises.

# Quotes

- "It's all PR. Everything they've done is PR."
- "There's no substance. There's no leadership."
- "The administration isn't made up of leaders. It's made up of panderers."
- "Every move he makes is for PR."
- "Without real leadership, it's going to be rough."

# Oneliner

Beau criticizes the Trump administration's PR-focused response, pointing out the lack of leadership in crisis management and urging voters to think carefully about future choices.

# Audience

Voters, concerned citizens

# On-the-ground actions from transcript

- Question and critically analyze political leadership (implied)
- Stay informed about government actions and responses (implied)
- Vote thoughtfully and responsibly in upcoming elections (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the Trump administration's response to the current crisis, focusing on their emphasis on PR over real solutions and lack of leadership in crisis management.

# Tags

#TrumpAdministration #CrisisManagement #Leadership #PRStunts #Voting