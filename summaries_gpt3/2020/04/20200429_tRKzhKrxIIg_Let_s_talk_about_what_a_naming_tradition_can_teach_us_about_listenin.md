# Bits

Beau says:

- Explains the importance of intent in life and how it should be measured, even though the effect is usually more significant.
- Talks about the naming traditions in the US military for ships, tanks, aircraft, drones, and helicopters, and how they are usually named after things that embody importance or honor.
- Addresses the naming tradition of helicopters in the Army, generally named after native groups or leaders, and clarifies that it was never meant to mock natives.
- Shares the historical context behind naming helicopters after native groups, like the Sioux, and how it was meant to acknowledge their speed, surprise, and violence of action from horseback.
- Emphasizes that the naming traditions are intended as a huge honor when it comes to people, not as insults.
- Mentions that not all natives are against this naming tradition and that some see it as a significant honor.
- Suggests that the opinion on whether this tradition should continue should be left to those impacted by it.
- Encourages understanding the perspective of others in a debate or discussion and listening while talking to potentially reach a resolution.

# Quotes

- "Intent is really important in life."
- "Was never meant to be an insult."
- "We may be closer to a resolution than we imagine if we actually listen while we talk."

# Oneliner

Understanding naming traditions in the military sheds light on honoring rather than insulting, urging consideration of intent and perspectives in debates.

# Audience

Military members, activists, historians

# On-the-ground actions from transcript

- Respect and honor the perspectives of those impacted by naming traditions in the military (implied)
- Engage in open, respectful dialogues to understand differing viewpoints (implied)

# Whats missing in summary

The full transcript provides a deeper historical context and nuanced understanding of the naming traditions in the US military, which can further inform perspectives on the topic.

# Tags

#NamingTraditions #Military #NativeGroups #Debate #Honor