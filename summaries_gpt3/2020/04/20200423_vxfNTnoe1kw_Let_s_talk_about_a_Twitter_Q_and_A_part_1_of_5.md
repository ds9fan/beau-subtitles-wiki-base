# Bits

Beau says:

- Conducting a Q&A session to answer questions from Twitter due to viewers' inability to attend live streams.
- Explains the use of GIFs of women to express emotions, attributing it to women being more emotive and clear in their expressions.
- Comments on the trend of anti-intellectualism in right-wing media in the United States, where opinions are valued over facts.
- Addresses the issue of charging for firearms training due to increasing demand, considering safety training free but charging for proficiency.
- Talks about managing Patreon messages and the unexpected growth of his platform with limited infrastructure.
- Shares his proficiency in multiple languages that have become less fluent over time due to lack of practice.
- Mentions influential figures in his life, politically, philosophically, and economically, including Emma Goldman, Sophie Scholl, Hunter S. Thompson, Thomas Paine, and Thomas Jefferson.
- Responds to questions about Nostradamus and the concept of putting all cops in prison and setting prisoners free for public safety.
- Expresses his thoughts on North Korea, authoritarian governments, and the potential scenarios if Kim doesn't recover.
- Humorously declines an offer to be someone's dad and explains why he wouldn't write a book about past rescue operations.

# Quotes

- "Everybody has skills to contribute, and I'm happy that I have found my place in that."
- "If you put all cops in prison and set all prisoners free, will the streets be more or less safe."
- "I enjoy the fight, I do."

# Oneliner

Beau conducts a Q&A addressing various topics from anti-intellectualism in media to charging for firearms training, sharing insights on languages, influential figures, public safety scenarios, and declining a book about rescue operations.

# Audience

Creators and Activists

# On-the-ground actions from transcript

- Reach out to Beau for firearms training (suggested)
- Support creators with limited infrastructure like Beau on platforms like Patreon (implied)

# Whats missing in summary

Insights on Beau's thoughts and perspectives on a wide range of topics, including media trends, public safety, multilingualism, influential figures, and personal boundaries.

# Tags

#Q&A #Anti-Intellectualism #FirearmsTraining #Languages #InfluentialFigures #PublicSafety #Creators #Patreon