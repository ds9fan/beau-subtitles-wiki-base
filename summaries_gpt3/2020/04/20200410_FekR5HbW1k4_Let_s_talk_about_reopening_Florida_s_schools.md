# Bits

Beau says:

- Governor DeSantis of Florida is considering reopening schools, despite experts advising against it.
- DeSantis previously kept beaches open when experts recommended closing them, leading to negative consequences in South Florida.
- DeSantis lacks understanding of the subject matter, evident from his actions such as cross-contaminating during a meeting.
- Kids can catch COVID-19 and spread it, impacting not just children but also teachers, janitorial staff, and older family members they come in contact with.
- Reopening schools solely to appease political figures like President Trump is illogical and risks the lives of educators.
- Beau plans to remove his kids from school if they reopen, urging others to do the same to protect teachers from unnecessary risk.

# Quotes

- "There's no reason to put these people at risk."
- "I for one will yank my kids out of school."
- "Our teachers' lives are not something the governor of Florida should be able to cash in."

# Oneliner

Governor DeSantis' push to reopen schools in Florida disregards expert advice and risks educators' lives for political gain.

# Audience

Florida residents, parents, educators

# On-the-ground actions from transcript

- Remove kids from school if reopened (exemplified)
- Advocate for teachers' safety (exemplified)
- Refuse to send children to school if deemed unsafe (exemplified)

# Whats missing in summary

The full transcript provides additional context on Governor DeSantis' decision-making process and the potential consequences of reopening schools.

# Tags

#Florida #GovernorDeSantis #Schools #COVID19 #Education