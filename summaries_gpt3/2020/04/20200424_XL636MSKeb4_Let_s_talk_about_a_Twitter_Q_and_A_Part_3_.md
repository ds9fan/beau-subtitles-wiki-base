# Bits

Beau says:

- Responding to questions in a Twitter Q&A, sharing off-the-cuff responses to various topics like general strikes and stateless societies.
- Expressing support for strikes as effective tools, suggesting setting up parallel institutions to defeat existing government structures.
- Explaining why some people still support Trump: sunk cost fallacy and being in echo chambers that shield them from differing viewpoints.
- Contemplating how a stateless society with a highly educated populace might handle situations like the current one of staying home and taking precautions.
- Advocating against armed conflict and discussing the potential benefits of a general strike as a form of peaceful protest.
- Sharing thoughts on Earthships and sustainable living, acknowledging the political motivations behind certain architectural choices.
- Providing advice on dealing with stress, acknowledging its universality and suggesting finding individual coping mechanisms.
- Addressing the logistics and implications of a military coup in the United States, expressing doubts about its likelihood and discussing potential outcomes.
- Describing personal methods of de-stressing, including working in the yard and occasional workouts.

# Quotes

- "If you don't run your life, somebody else will."
- "Good ideas generally don't require force."
- "There is no simple way to deal with stress."

# Oneliner

Beau responds to various questions in a Twitter Q&A, discussing topics like general strikes, sustainability, stress management, and the implications of a military coup, advocating for peaceful solutions and individual coping mechanisms.

# Audience

Community members

# On-the-ground actions from transcript

- Set up parallel institutions to provide better services and undermine government control (implied)
- Encourage education and critical thinking in the community to foster a highly educated populace (implied)
- Support peaceful forms of protest like general strikes as effective tools for change (implied)
- Promote sustainable living practices and environmental awareness in local communities (implied)
- Advocate for peaceful conflict resolution and individual stress management techniques (implied)

# Whats missing in summary

Insights into the importance of critical thinking, education, and community-driven solutions in navigating societal challenges.

# Tags

#SocialChange #GeneralStrikes #Sustainability #StressManagement #CommunityEmpowerment