# Bits

Beau says:

- Hospitals in metropolitan areas are refusing to let staff bring in needed protective equipment that was donated or scrounged up.
- Reasons provided include concerns about not everyone getting the equipment, it being counterfeit, and unauthorized.
- Beau points out the flaw in these reasons, urging hospitals to prioritize those dealing with high-risk patients.
- He notes that even if the equipment is counterfeit, it's better than nothing, especially considering the current shortage.
- The focus on bottom line and PR seems to be the driving force behind these refusals, with money being spent on bonuses rather than necessary supplies.
- Beau vows to contact private donors of hospitals that deny staff protection, aiming to cut off funding until those responsible are no longer employed.

# Quotes

- "If they can get the equipment, and I don't care how they get it, if they can get it, let them use it."
- "You refuse to allow your staff to protect themselves."
- "You had money, too, and you chose to spend that money on bonuses for the administrative staff."
- "We will be going out of our way to choke off funding until those people responsible for this decision are no longer employed."

# Oneliner

Hospitals denying staff protective equipment prioritize profit over lives, risking safety and facing accountability from donors.

# Audience

Hospital staff, donors

# On-the-ground actions from transcript

- Contact private donors of hospitals denying staff protection (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the reasons hospitals use to deny staff protective equipment and calls out their prioritization of profit over safety. Viewing the entire speech gives a complete understanding of the urgency and frustration conveyed.

# Tags

#ProtectiveEquipment #HospitalSafety #Accountability #DonorAction #PrioritizingProfit