# Bits

Beau says:

- Introducing Andy Rado, a lead organizer of New York City United Against Coronavirus, providing information and resources for mutual aid projects in response to the ongoing pandemic.
- Andy Rado explains that the project involves a variety of resources from government agencies, nonprofits, new groups, and individuals to address the extreme needs exacerbated by the coronavirus situation.
- Mutual aid projects aim to address existing problems like food insecurity, housing insecurity, and lack of access to medical resources through community organizing and resource distribution.
- Andy Rado shares his involvement in direct mutual aid organizing in Bed-Stuy, Brooklyn, and his role in disseminating vital information on available resources via social media and other communication channels.
- The importance of not duplicating existing resources and instead collaborating with established organizations to meet community needs effectively is emphasized.
- Advice is given for those looking to set up their own mutual aid network, suggesting starting by researching existing resources and partnering with established organizations.
- The focus is on meeting immediate needs while also considering the long-term societal and governmental shifts necessary to address systemic issues heightened by the pandemic.
- Beau and Andy Rado stress the importance of community connections, mutual support, and grassroots organizing in responding to crises at a local level.
- The need for continued community engagement beyond the pandemic, addressing ongoing issues like job loss, housing insecurity, and access to healthcare, is discussed.
- Andy Rado encourages individuals to contribute to mutual aid efforts by volunteering time, donating resources, or reaching out for help as part of a participatory and inclusive process.

# Quotes

- "We need to be prepared around housing issues that come up for people that can't pay rent."
- "Everyone has something to contribute, and everyone has the opportunity to make use of that."
- "We all have something to contribute, and we all are gonna face struggles and challenges."

# Oneliner

Be prepared to tackle housing issues, connect with existing resources, and contribute actively to mutual aid efforts to address community needs during the ongoing pandemic and beyond.

# Audience

Community members, volunteers

# On-the-ground actions from transcript

- Join existing mutual aid organizations in your community to contribute time or resources (suggested).
- Partner with established nonprofits or new groups to address local needs effectively (implied).

# Whats missing in summary

The full transcript provides detailed insights into the importance of mutual aid projects, community organizing, and the long-term impact of grassroots efforts in addressing systemic issues beyond the current crisis.

# Tags

#MutualAid #CommunityOrganizing #PandemicResponse #ResourceDistribution #GrassrootsEfforts