# Bits

Beau says:

- Addresses the issue of small YouTubers needing support and recognition.
- Notes the surge of new channels emerging during the current times when people are at home.
- Encourages small YouTubers to describe their channel content rather than just seeking subscribers.
- Advises that having genuinely interested subscribers is more beneficial than a large number of disinterested ones.
- Stresses the importance of having an active, engaged audience for YouTube's algorithm.
- Offers to support and showcase new channels to his active community.
- Mentions a previous video with advice for new YouTubers, particularly those focused on news content.
- Teases upcoming content about preparedness and the importance of being ready.

# Quotes

- "You want people interested in your content more than subscribers."
- "It's better to have 100 subscribers who are truly interested in your content than 1,000 who aren't going to go watch it."
- "Describe your channel. Short little sentence, tell us what it is."
- "Hopefully you can pick up subscribers who are actually interested in the type of content you put out."
- "Y'all have a good night."

# Oneliner

Beau addresses the need for genuine engagement over mere numbers in supporting small YouTubers, urging creators to describe their content to attract interested subscribers.

# Audience

Small Content Creators

# On-the-ground actions from transcript

- Describe your channel content clearly in the comment section when reaching out to support other small YouTubers (suggested).
- Engage actively with new channels and creators by watching their content and providing feedback (implied).

# Whats missing in summary

Beau's supportive and inclusive community-building efforts shine through in the transcript. Viewing the full video will provide a deeper insight into his commitment to uplifting emerging voices on YouTube.

# Tags

#YouTubers #Support #Engagement #CommunityBuilding #ContentCreators