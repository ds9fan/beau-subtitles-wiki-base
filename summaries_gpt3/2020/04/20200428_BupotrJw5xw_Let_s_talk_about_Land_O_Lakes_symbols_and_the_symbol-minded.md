# Bits

Beau says:

- Land O'Lakes removed a drawing of a native woman from their packaging, causing distress among Americans.
- Symbols are meant to evoke emotion and loyalty without requiring critical thought.
- Symbols, like corporate logos or flags, are designed to elicit immediate reactions.
- Beau criticizes the use of symbols to manipulate individuals who do not want to think critically.
- Political memes often aim to provoke emotional responses rather than inspire critical thinking.
- Americans expressed outrage over the removal of a drawing of a native woman from butter packaging, while real issues, like missing native women, are overlooked.
- Beau points out the danger of valuing symbols more than what they are meant to represent.
- Many individuals prioritize symbols, like national flags, over the true values they stand for.
- Beau argues that critical thinking is necessary for self-governance and making informed decisions.
- Symbols like political party logos often overshadow individuals' true beliefs and critical thinking.
- The focus on symbols in American society makes people easily manipulated and exploited by those in power.

# Quotes

- "Symbols are meant to evoke emotion and create loyalty without requiring critical thought."
- "We should probably think beyond symbols."
- "They care about using symbols to manipulate you. They don't care about you."
- "We are headed into tough times. We are going to have to critically think."
- "Butter is a divisive topic in this country, not because butter is that important, but because of the symbol."

# Oneliner

Beau criticizes the manipulation through symbols, urging people to think critically beyond loyalty to symbols in a society easily led by manipulative powers.

# Audience

Citizens, Critical Thinkers

# On-the-ground actions from transcript

- Question the symbols around you and their true significance (suggested)
- Foster a culture of critical thinking and questioning in your community (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the impact of symbols on society and the importance of critical thinking in the face of manipulation by those in power.

# Tags

#Symbols #CriticalThinking #Manipulation #AmericanSociety #PowerInfluence