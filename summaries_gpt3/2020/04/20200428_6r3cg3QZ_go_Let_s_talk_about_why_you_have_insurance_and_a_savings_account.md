# Bits

Beau says:

- Reframes emergency preparedness as insurance and a savings account.
- Urges to prepare for extreme scenarios as it also prepares for more likely ones.
- Compares emergency preparedness to having homeowner's insurance.
- Suggests that being prepared can help in unforeseen circumstances like job loss.
- Points out the common occurrence of disasters in the United States.
- Emphasizes the peace of mind that preparedness brings.
- Encourages preparedness to stay ahead of panic buying and panicked people during emergencies.
- Uses a mountain lion analogy to illustrate staying one step ahead.
- Mentions the importance of having 30 days' worth of food for emergencies.
- Compares preparing for emergencies to putting on your mask first on an airplane before helping others.
- Criticizes the government's track record in dealing with emergencies.
- Advocates for being self-reliant in times of emergencies.

# Quotes

- "It's a savings account of tangible assets that you use in your daily life."
- "You don't have to deal with the effects of whatever the emergency is in most cases. You're having to deal with a bunch of panicked people."
- "Being prepared can only help."
- "Their track record for dealing with emergencies is not good."
- "You have to lead yourself."

# Oneliner

Beau reframes emergency preparedness as insurance and a savings account, urging preparation for extreme scenarios while criticizing the government's emergency response.

# Audience

Preppers and community-minded individuals.

# On-the-ground actions from transcript

- Stock up on 30 days' worth of food and essentials to prepare for emergencies (exemplified).
- Create a bug-out bag with necessary supplies for potential evacuations (exemplified).
- Take the initiative to lead yourself and be prepared for emergencies (implied).

# Whats missing in summary

Demonstrations on how to create a bug-out bag and specific items to include for different emergency scenarios.

# Tags

#EmergencyPreparedness #Insurance #SavingsAccount #Preparedness #SelfReliance