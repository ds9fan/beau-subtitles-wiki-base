# Bits

Beau says:

- Urges Bernie supporters to take action post-election.
- Encourages building worker co-ops and organizing.
- Suggests utilizing the current economic pause to make systemic changes.
- Points out the availability of cheap retail and restaurant spaces post-pandemic.
- Emphasizes the need for workers to lead themselves and create change.
- Advocates for action over waiting for political change.
- Stresses the importance of organizing and educating co-workers.
- Mentions the possibility of creating alternatives to existing systems.
- Advises taking advantage of the chaos for positive change.
- Calls for demonstrating that alternative ideas can work practically.

# Quotes

- "Build your worker co-op. Organize. Educate."
- "This is the time to do it."
- "It's a wide cross-section. All it takes is organization."
- "Not with your vote, but with your action."
- "It's time to lead ourselves."

# Oneliner

Bernie supporters urged to build worker co-ops, organize, and seize the current pause in the economy to create systemic change - it's time to lead ourselves with action, not just votes.

# Audience

Bernie supporters

# On-the-ground actions from transcript

- Build worker co-ops, organize, and educate co-workers to create systemic change (exemplified)
- Take advantage of available cheap retail or restaurant spaces to start new ventures (implied)
- Create alternatives to existing systems, like a worker-friendly Uber Eats competitor (implied)
- Lead by example and show practical success of alternative ideas through action (exemplified)

# Whats missing in summary

The full transcript provides detailed examples and a thorough explanation of how Bernie supporters can actively create change post-election by building worker co-ops, organizing, and seizing opportunities during the economic pause.