# Bits

Beau says:

- Dr. Jill Biden was criticized for using the title "doctor" because she has a doctorate in education, not medicine.
- The New York Post reported on a medic who had a side gig on a certain website for her fans.
- The focus should have been on the financial struggles of frontline healthcare workers, not moral outrage over a medic's side job.
- There is a disparity between frontline healthcare workers and the multi-billion dollar industry profiting from them.
- The message conveyed was that women shouldn't be proud of their educational accomplishments, brains, or bodies.
- Society's approval seems necessary for women to be proud of themselves.
- Equality is still a work in progress in the country.
- Women should be free to be who and what they want to be without seeking society's approval.

# Quotes

- "You can do whatever you want in this world. As long as we approve."
- "In today's world, a woman has to be two things. Who and what she wants."

# Oneliner

Dr. Jill Biden faces criticism for being called "doctor," while a medic's side gig becomes a focus of moral outrage, showcasing the struggles of frontline healthcare workers amid societal judgment of women's accomplishments and bodies.

# Audience

Women, Healthcare Workers

# On-the-ground actions from transcript

- Support frontline healthcare workers financially and advocate for fair compensation (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the societal expectations placed on women and the ongoing struggle for gender equality.

# Tags

#GenderEquality #HealthcareWorkers #SocietalExpectations #Women'sRights #Equality