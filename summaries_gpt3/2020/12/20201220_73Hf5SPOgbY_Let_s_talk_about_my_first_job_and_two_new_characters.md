# Bits

Beau says:

- Recalls his first job experience at fourteen, washing dishes for an immigrant who was a happy and jovial guy but faced stereotyping due to his mannerisms.
- Initially found the stereotype of his employer being connected to organized crime cool as a teenager, but realized its harmful nature as he got older.
- Shares a memorable incident where the happy employer, who learned English from Sesame Street, admonished a young man for not taking his classes seriously.
- Expresses how Sesame Street, a timeless institution, has been a part of many people's childhoods and stories by not chasing trends and thinking in decades rather than years.
- Mentions the introduction of new characters Aziz and Nor who live in refugee camps on Sesame Street, indicating long-term educational support for children in such settings.
- Emphasizes the need to address the increasing migration of people due to climate change, requiring the eradication of stereotypes and nationalism.
- Acknowledges the changing world and the necessity for institutions to forecast and adapt to the movement of people globally.

# Quotes

- "I learned to speak English from Sesame Street."
- "A lot of the stereotypes that we have about people from other places they gotta go."
- "The world is changing and we're going to have to adjust."

# Oneliner

Beau shares insights from his first job, including the impact of stereotypes, learning English from Sesame Street, and the importance of adapting to global migration trends.

# Audience

Educators, policymakers, community leaders

# On-the-ground actions from transcript

- Support educational initiatives for children in refugee camps (implied)
- Challenge and dismantle stereotypes about people from different backgrounds (implied)
- Advocate for policies that address climate-induced migration and support displaced populations (implied)

# Whats missing in summary

The full transcript contains Beau's reflections on the lasting influence of Sesame Street, the role of institutions in addressing global challenges, and the need for societal adaptation to changing demographics.

# Tags

#SesameStreet #Migration #ClimateChange #Education #GlobalAdaptation