# Bits

Beau says:

- Provides an overview of the economy, stock market, and jobs report for November.
- Initially, the job report of almost a quarter million new jobs created seems positive.
- However, job growth is actually slowing down and trending downwards.
- Currently, there are still 10 million jobs less than before the economic downfall.
- It is projected to take about four years to recover the lost jobs at the current rate.
- The early job gains were mainly due to people returning to work after temporary layoffs.
- Long-term unemployment is rising, making it harder for people to re-enter the job market.
- Benefits for the unemployed are running out, indicated by a significant increase in long-term unemployed individuals.
- The total number of long-term unemployed individuals has reached 3.9 million.
- The jobless rate for the black community is significantly higher at 10.3% compared to 5.9% for white individuals.
- Despite the grim job market outlook, the stock market reacted positively to the news, expecting more stimulus.
- There is a significant disconnect between the stock market's performance and the real economy.
- The stock market is not an accurate reflection of how many people are struggling to make ends meet.
- Beau suggests that there needs to be a change in the way the economy operates to address these challenges effectively.

# Quotes

- "All of this is bad news for us."
- "The stock market is not the economy."
- "How the stock market is performing does not typically affect the way a whole lot of people are getting food on their table."

# Oneliner

Beau breaks down the disconnect between the stock market and the struggling economy, urging for sustainable economic changes.

# Audience

Economic policymakers, activists

# On-the-ground actions from transcript

- Advocate for sustainable economic policies to address job market challenges (suggested)
- Support initiatives that focus on helping long-term unemployed individuals re-enter the job market (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the disconnect between the stock market and the real economy, urging for fundamental changes to ensure economic sustainability.

# Tags

#Economy #StockMarket #JobsReport #Unemployment #PolicyChange