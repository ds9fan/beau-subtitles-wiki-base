# Bits

Beau says:

- Story about a friend needing help with a privacy fence but actually needing help moving agricultural fencing in the backyard.
- Importance of having the right tools for the job, even if tasks seem similar.
- Studies show that access to military equipment doesn't reduce crime or increase officer safety.
- Lack of training for law enforcement agencies with access to military equipment.
- Pushback against terminating programs that provide military equipment to law enforcement.
- Emphasis on the difference between military and law enforcement roles and the tools they require.
- Call for law enforcement to focus on consent-based policing rather than militarization.
- Critique on the lack of evidence supporting the effectiveness of military equipment for law enforcement.
- Hope for President Biden to end programs providing military equipment to law enforcement.
- Emphasis on the risks associated with militarization without clear rewards.

# Quotes

- "Tools for different jobs don't help."
- "Law enforcement should be focusing more on moving towards consent-based policing than trying to turn itself into a military."
- "In the risk versus reward, apparently there's only risk."

# Oneliner

Beau shares a story about having the right tools for the job, questioning the effectiveness of military equipment for law enforcement, and advocating for consent-based policing over militarization.

# Audience

Community members, advocates.

# On-the-ground actions from transcript

- Advocate for consent-based policing to local law enforcement agencies (implied).
- Support organizations pushing to terminate programs providing military equipment to law enforcement (implied).

# Whats missing in summary

The nuances and detailed explanations Beau provides in the full transcript.

# Tags

#Tools #Militarization #LawEnforcement #CommunityPolicing #Advocacy