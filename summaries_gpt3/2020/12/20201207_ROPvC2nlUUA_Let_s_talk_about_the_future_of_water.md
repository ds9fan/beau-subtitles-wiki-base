# Bits

Beau says:

- Water dripping onto a tin roof during the video sets a dystopian tone, despite not being a satire.
- Water futures, a new concept, allow locking in prices for commodities like large consumers and sellers.
- Speculation is enabled through futures, with price fluctuations driven by supply and demand.
- The NASDAQ-Valez California Water Index, symbol NQH20, indicates significant price increases since its introduction in 2018.
- Two billion people currently face water scarcity, with projections of two-thirds facing shortages by 2025.
- Despite water covering the world, fresh water scarcity is a critical issue due to accessibility.
- The futures market for water is a tangible tool to showcase the reality of water scarcity to skeptics of environmental issues.
- The emergence of water futures signifies industry leaders recognizing the pressing need to address water scarcity.
- Utilizing the futures market can help demonstrate the concrete impacts of climate change and environmental degradation.
- Beau advocates for proactive changes to address water scarcity before it becomes an overwhelming issue for the general population.

# Quotes

- "There is a whole futures market dedicated to water now. That's a thing. It exists."
- "This is a good tool because there are a whole lot of people out there that do not believe this issue is real."
- "Titans of industry here. They're saying, we need to start paying attention to this."
- "It's also handing us a tool that we can use to show people the impacts of this are real."

# Oneliner

Beau introduces the concept of water futures, indicating the severity of global water scarcity and urging proactive measures through industry recognition and tangible tools.

# Audience

Climate advocates, environmentalists

# On-the-ground actions from transcript

- Educate communities on the significance of water futures and their implications (suggested)
- Advocate for proactive measures to address water scarcity in local communities (implied)

# Whats missing in summary

The full transcript provides detailed insights into the emergence of water futures as a tool to address water scarcity and compel action on climate change and environmental issues.

# Tags

#WaterFutures #ClimateChange #EnvironmentalDegradation #GlobalWaterScarcity #IndustryRecognition