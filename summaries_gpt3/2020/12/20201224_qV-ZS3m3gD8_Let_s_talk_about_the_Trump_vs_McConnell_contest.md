# Bits

Beau says:

- Beau delves into the internal power struggle between Trump and McConnell within the Republican Party.
- The Republican Party will conduct a poll on January 6th to decide whether they want to be the party of Trump or McConnell.
- Despite being termed as a poll, it's more of a theatrical voting on objections to the electoral votes.
- The votes are expected to go in favor of McConnell over Trump due to long-term political capital considerations.
- The breakdown of votes will reveal who supports Trump's control and who prefers McConnell's leadership.
- Newer representatives may lean towards supporting Trump, thinking he holds more power, while experienced politicians understand McConnell's influence.
- McConnell is likely to win the poll, leading the Republican Party to revert back to obstructionist tactics during the Biden administration.
- Beau predicts that if McConnell wins, the party will adopt a George Bush-era style, whereas a Trump victory may further radicalize the party.
- Despite the theatrics expected on January 6th, the essence of the poll is deciding between McConnell and Trump's control over the party.
- Beau advises staying calm amidst the wild statements from Trump loyalists and suggests that smart politicians will lean towards McConnell for their own interests.

# Quotes

- "It's really deciding whether McConnell or Trump gets to hold the reins of power."
- "My best guess is that McConnell will win this poll."
- "A smart politician, somebody who is concerned with self-interest and maintaining their own power, they're probably going to go with McConnell."
- "You will be rooting for McConnell, because he is definitely the lesser of two evils out of that."
- "Just stay calm, because my guess is that you're going to see a lot of Trump loyalists trying to prove how much they love Trump."

# Oneliner

Beau delves into the internal power struggle between Trump and McConnell within the Republican Party, predicting McConnell’s victory in the upcoming poll and the party reverting to obstructionist tactics during the Biden administration.

# Audience

Politically engaged individuals.

# On-the-ground actions from transcript

- Stay informed about the outcomes of the Republican Party's poll on January 6th (suggested).
- Monitor how your representatives and senators vote in the poll and understand their stance on Trump or McConnell's control (implied).
- Advocate for political strategies that prioritize governance over theatrics and political capital (implied).

# Whats missing in summary

Insights on the potential implications of McConnell or Trump's victory for the future direction of the Republican Party.

# Tags

#RepublicanParty #Trump #McConnell #PowerStruggle #PoliticalPrediction