# Bits

Beau says:

- Comparing leadership styles of the outgoing Trump administration and incoming Biden administration.
- Biden planning to ask people to wear masks for the first hundred days, facing pushback.
- Emphasizing the use of numbers for certainty and drawing people in.
- Duration of mask-wearing until a safe and available vaccine is widely distributed.
- Lack of a public distribution plan despite assurances from the Trump administration.
- Mentioning a website that may indicate where individuals fall in the vaccine distribution line.
- Biden's approach of setting an example rather than mandating masks or vaccines.
- Encouraging and incentivizing vaccine uptake without mandating it.
- Acknowledging resistance but focusing on leading by example.
- Urging people not to travel during the upcoming holidays for safety.
- Stressing the importance of following health guidelines even if choosing to travel.
- Reminding to take precautions to protect vulnerable loved ones during the pandemic.

# Quotes

- "Biden is going to ask people to wear masks. He's not going to mandate it either."
- "Encourage it, incentivize it, do everything you can to get people to get the vaccine, but you can't really mandate it."
- "Do everything that you can, even if you're going to disregard that."
- "Take every precaution that you can. Mitigate in every way that you can."
- "If you mess this up, Granny June probably won't see spring."

# Oneliner

Beau compares leadership styles, advocates for mask-wearing without mandating, and stresses following health guidelines to protect loved ones during the ongoing pandemic.

# Audience

Health-conscious individuals

# On-the-ground actions from transcript

- Follow health guidelines strictly: wear masks, maintain distance, wash hands (implied)
- Encourage and support vaccine uptake without mandating it (suggested)

# Whats missing in summary

Importance of consistent adherence to health guidelines for overall community safety.

# Tags

#LeadershipStyles #MaskWearing #VaccineUptake #HealthGuidelines #COVID19 #PandemicAwareness