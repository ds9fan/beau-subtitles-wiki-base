# Bits

Beau says:

- Explains political capital as goodwill in politics, with two types: reputational and representative.
- Compares political capital in politics to social media, particularly YouTube.
- Illustrates reputational political capital by discussing consistency in policies and ideological stances.
- Differentiates between reputational political capital (support from base) and representative political capital (related to getting things done).
- Uses examples from YouTube to explain the concept of political capital.
- Mentions how failing to deliver on promises or facing scandals can impact reputational political capital.
- Relates political capital to current political situations involving Pelosi, AOC, Trump, and McConnell.
- Analyzes the battle for control within the party based on reputational and representative political capital.
- Cites an example of a decline in reputational political capital for Crenshaw due to issues with the VA and social media.
- Emphasizes the importance of political capital in politicians' decision-making and potential impact on re-election.

# Quotes

- "Political capital is basically goodwill."
- "It's worth noting that this isn't the way it should be, but it's the way it is right now."
- "That's how this works."
- "It's just a thought."
- "Y'all have a good day."

# Oneliner

Beau explains political capital as goodwill in politics, comparing it to social media, and how it impacts decision-making and re-election potential.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Analyze and understand how political capital influences decision-making (implied).
- Stay informed about politicians' actions and their impact on political capital (implied).
- Engage in community organizing to support or hold politicians accountable based on their political capital (implied).

# Whats missing in summary

The full transcript provides a detailed explanation of political capital, including its types, impact, and relevance in political decision-making. Viewing the entire transcript offers a comprehensive understanding of this abstract concept in politics.

# Tags

#PoliticalCapital #Politics #SocialMedia #DecisionMaking #Reputation #Representation