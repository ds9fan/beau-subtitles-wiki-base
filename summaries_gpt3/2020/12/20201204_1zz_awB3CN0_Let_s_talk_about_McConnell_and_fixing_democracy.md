# Bits

Beau says:

- Proposes two small changes on Capitol Hill to enhance accountability of representatives and make American politics more transparent.
- Suggests that any bill passed by one chamber of the House should be voted on by the other to avoid bills being held up by powerful individuals like Mitch McConnell.
- Points out the issue of negotiations being conducted in secret, leading to blame games between parties when they fail.
- Advocates for televising negotiations between parties to show the public the process and prevent politicians from blaming each other.
- Emphasizes the importance of accountability in a representative democracy and the current challenges in holding politicians accountable.
- Argues that these changes could significantly impact American politics by making politicians more accountable to the people.
- Urges the people, especially in Georgia, to hold politicians like Mitch McConnell accountable through voting.
- Calls for the Democratic Party, if in control, to prioritize implementing these changes to strengthen democracy and ensure accountability.
- Criticizes politicians who shield themselves from accountability and view themselves as rulers rather than employees of the people.
- Encourages viewers to think about making politicians more accountable and resilient in a representative democracy.

# Quotes

- "These two small changes if something is passed in one chamber it has to be voted on in the other and televising all negotiations between parties, live streaming, that would forever alter American politics and it would make our politicians more accountable."
- "If we want a representative democracy and people keep talking about how democracy is being undermined because it is right now. Maybe we should make it a little more resilient."
- "They view themselves as your rulers and because we can't hold them accountable for their actions. They kind of are."
- "It's an important concept when you're talking about representative democracy and right now it's very hard to hold them accountable."
- "If that happens and the Democratic Party gets control of the House and the Senate this should be one of the first changes to the rules they make."

# Oneliner

Beau suggests two critical changes on Capitol Hill to enhance accountability and transparency in American politics, advocating for bills passed in one chamber to be voted on by the other and for negotiations between parties to be televised.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Contact your representatives to advocate for bills passed in one chamber to be voted on by the other and for negotiations between parties to be televised (suggested).
- Vote in elections, especially in Georgia, to hold politicians like Mitch McConnell accountable (implied).

# Whats missing in summary

The full transcript provides detailed insights into the current lack of accountability in American politics and offers practical solutions to strengthen democracy by making politicians more answerable to the public. 

# Tags

#Accountability #PoliticalTransparency #RepresentativeDemocracy #MitchMcConnell #CapitolHill