# Bits

Beau says:

- Rarely issues calls to action, but made one regarding an investigation at Fort Hood.
- Investigation at Fort Hood happened due to public pressure after incidents like Vanessa Guillen's case.
- Investigation findings revealed command failure and issued 70 recommendations for improvement.
- Key finding: command knew of high risk to female soldiers and did nothing to mitigate.
- Surveyed about 30,000 troops at Fort Hood, with 93 credible allegations from 500 women soldiers.
- 14 people in leadership positions relieved or suspended following the investigation.
- Independent civilian review took action, but military's own investigation is ongoing.
- Actions taken aim to change the climate at Fort Hood and prevent similar incidents in the future.
- Investigation did not bring justice for Vanessa Guillen or other soldiers, but may lead to positive changes.
- Acknowledges community's role in pressuring for the investigation and its outcomes.

# Quotes

- "Command failed. They made 70 recommendations to make sure this sort of thing does not happen again."
- "One of those who was relieved was a major general. One was a colonel. One was a command sergeant major."
- "At the end of the day, did this bring justice for Vanessa Guillen or the other soldiers? No. Absolutely not."
- "Your action combined with a whole bunch of other people's actions made this happen."
- "Without enough pressure, this wouldn't have occurred."

# Oneliner

Rare call to action led to investigation at Fort Hood revealing command failures; actions taken to prevent future incidents, though justice not served.

# Audience

Community members, advocates

# On-the-ground actions from transcript

- Support live stream fundraiser on Friday (suggested)
- Stay informed about ongoing actions and investigations at Fort Hood (implied)

# Whats missing in summary

The full video provides more details on the investigation findings and the context of the actions taken.

# Tags

#CallToAction #FortHood #Justice #Prevention #CommunityPressure