# Bits

Beau says:

- People, including big names like Pat Robertson, are distancing themselves from Trump, not wanting to be associated with him when his support dwindles.
- Pat Robertson made a statement criticizing Trump, suggesting that he should retire and not run again, as his claims about Biden have not come to fruition.
- Trump is likely to lose support from evangelicals, but conservative people are slow to change, so the shift may take time.
- Pat Robertson is noted for his two different sides - the evangelical leader and the politically active figure who has supported progressive policies and candidates.
- Robertson's successful political career stems from his ability to read the political climate and transition between being in power and out of power.
- Establishment Republicans, who understand the political game, are expected to distance themselves from Trump publicly, making it harder for him to continue his tactics against reality.
- As support for Trump dwindles, it will be easier to sway his committed loyalists, particularly those who only know government under Trump, out of public office.

# Quotes

- "He should retire and not run again."
- "The more of the real power brokers who cut Trump loose, the harder it is for him to carry on his offensive against reality."
- "It will be easier to get the more committed of his loyalists gone."

# Oneliner

People, including Pat Robertson, are publicly distancing themselves from Trump as his support dwindles, making it harder for him to continue his offensive against reality.

# Audience

Political observers

# On-the-ground actions from transcript

- Publicly distance oneself from politicians or figures supporting harmful ideologies (suggested)
- Support policies and candidates that prioritize treatment over incarceration (exemplified)
- Be vocal about calling out political figures who enable harmful actions (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics surrounding Trump's dwindling support and the potential implications for the future of his loyalists in public office. Watching the full video will offer a deeper understanding of the evolving political landscape.

# Tags

#Trump #PatRobertson #PoliticalShift #ConservativeSupport #PublicOffice