# Bits

Beau says:

- 2020 is nearing its end, and many people are relieved, blaming the year for various issues.
- The current state of affairs is the result of not just this year, but a culmination of four years and more.
- The administration has brought existing issues to light through incompetence, leading to a desire for deep systemic change among viewers.
- 2021 is seen as the year to initiate real change, not simply because of a new year or administration, but due to a populace primed for transformation.
- Political engagement is at an all-time high, and individuals must capitalize on this for systemic change.
- Change will come from individual action, organizing at the local level, and demanding necessary changes.
- Those in the middle and upper classes may begin to understand systemic issues as economic challenges impact them.
- The delay in relief and stimulus efforts will affect the economy, potentially leading to a shift in perceptions among the more affluent.
- Biden’s administration is viewed as status quo, requiring individuals to drive real change towards a collective goal of systemic transformation.
- Organization at a grassroots level is emphasized, as top-down approaches are seen as likely influenced by existing systems.
- Real change requires individual involvement focused on specific issues, avoiding the tendency of political figures to play it safe.
- Society can progress by collectively working towards what is most vital to each individual, without impeding each other's efforts.
- The past years have hindered progress, and it is now up to individuals to guide society forward by recognizing and addressing existing problems.

# Quotes

- "2020 is coming to a close, but the real work doesn't start until next year."
- "The most effective people at social change had a very narrow focus and they changed it."
- "It's going to take you as an individual."
- "Real change requires individual action."
- "Society can progress by collectively working towards what is most vital to each individual."

# Oneliner

2020 is ending, but true change begins in 2021 through individual action towards systemic transformation.

# Audience

Activists, community organizers

# On-the-ground actions from transcript

- Organize your community at the local level towards systemic change (implied)
- Demand necessary changes in the system (implied)
- Get involved in social change efforts with a narrow focus (implied)
- Avoid stepping on each other's toes while working towards collective progress (implied)

# Whats missing in summary

The full transcript provides additional context on the impact of the administration, the need for grassroots organization, and the role of individuals in effecting systemic change.

# Tags

#SystemicChange #GrassrootsOrganization #PoliticalEngagement #CommunityAction #IndividualEmpowerment