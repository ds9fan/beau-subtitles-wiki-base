# Bits

Beau says:

- Introducing a series on speaking the "second language" of Trump and his supporters based on fear and ignorance.
- Explaining how fear and ignorance are key components of Republican talking points.
- Describing the common response of labeling anything left-leaning as communist without understanding the term.
- Pointing out that the fear of communism is based on imagery and misinformation rather than actual knowledge.
- Listing the common fears associated with communism: bread lines, rationing, propaganda, police presence, controlled press, cronyism, etc.
- Challenging the audience to name something from the list that did not occur under Donald Trump's administration.
- Illustrating how many of the fears associated with communism actually manifested during Trump's presidency.
- Emphasizing the manipulation of individuals through fear and misinformation to support authoritarian practices.
- Encouraging listeners to confront supporters with the reality of what has already taken place rather than focusing solely on Trump.
- Urging for a shift towards anti-authoritarianism rather than making the discourse about party politics.

# Quotes

- "Fear and ignorance are key components of Republican talking points."
- "Everything they fear about communism occurred under Donald Trump."
- "The truth isn't told, it's realized."
- "Make them anti-authoritarian."
- "The goal is not to turn them into progressives, but to make them anti-authoritarian."

# Oneliner

Beau delves into the manipulation through fear and ignorance in Republican talking points, challenging supporters to confront the reality of authoritarian practices under Trump to foster anti-authoritarianism.

# Audience

Political observers

# On-the-ground actions from transcript

- Challenge misconceptions about communism with facts and examples (implied)
- Encourage reflection on authoritarian practices and their implications (implied)
- Foster anti-authoritarian perspectives through open dialogues (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of how fear and ignorance are manipulated in political discourse, urging listeners to confront these tactics to foster anti-authoritarian sentiments.

# Tags

#Politics #Republican #Fear #Ignorance #AntiAuthoritarianism