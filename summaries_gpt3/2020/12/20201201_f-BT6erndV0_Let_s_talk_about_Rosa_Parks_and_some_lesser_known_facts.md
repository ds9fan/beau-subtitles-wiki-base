# Bits

Beau says:

- Beau addresses Rosa Parks' story, pointing out some lesser-known facts about her.
- Rosa Parks is not accurately represented in the story of her refusing to give up her seat on December 1st, 1955.
- It is mentioned that Rosa Parks had been battling injustice for more than a decade before her famous act.
- She had been involved with the NAACP since 1943, indicating a long history of activism.
- Rosa Parks was carefully chosen by the NAACP to be a symbol and carry a message due to her potential to win in court.
- Parks attended the Highlander Folk School in Tennessee, where she was mentored by Septima Clark, known as the "mother of the movement."
- The courage to refuse to give up her seat cannot be taught, but the ability to carry a message can be learned.
- Beau stresses the importance of workshops, training, and organization in social change efforts.
- He mentions that Rosa Parks worked for a liberal couple, the Durrs, who were also active in the civil rights movement.
- The story of Rosa Parks goes beyond just being tired; it's about a long-standing commitment to fighting injustice.

# Quotes

- "The courage and bravery that it takes to say, no, I'm not moving. That can't be taught."
- "Boiling it down to her being a seamstress and just being tired, not wanting to move. I don't think that's a fair representation of the commitment it takes."

# Oneliner

Beau sheds light on the lesser-known facts about Rosa Parks, stressing the depth of her activism and commitment beyond the familiar narrative of tiredness.

# Audience

Activists, History Buffs

# On-the-ground actions from transcript

- Attend workshops or training sessions focused on activism and social change (suggested)
- Support organizations like the NAACP that work towards justice and equality (exemplified)

# Whats missing in summary

The full transcript provides a deeper understanding of Rosa Parks' activism and the importance of continuous dedication to fighting injustice.

# Tags

#RosaParks #Activism #CivilRights #SocialChange #Education #History