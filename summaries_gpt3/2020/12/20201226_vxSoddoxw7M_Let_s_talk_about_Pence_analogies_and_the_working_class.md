# Bits

Beau says:

- Explains the analogy of a rising tide lifting all ships and its connection to the economy and working class.
- Criticizes the analogy for being inaccurate and misleading, particularly in relation to the stock market.
- Notes that the rich are getting richer while the working class is suffering, contrary to the analogy.
- Points out that most Americans are actually in the water, not on ships, hinting at the working class being the foundation that supports the wealthy.
- Mentions how senators favor the rich by artificially boosting their wealth, leading to income inequality.
- Emphasizes the importance of uplifting the working class for overall economic growth and prosperity.
- Advocates for stimulus efforts starting from the bottom to benefit those who need it most and boost economic activity.
- References the concept of velocity in economics, suggesting that money circulated among the working class is more impactful than stagnant wealth.
- Warns about the widening wealth gap and the detrimental effects of government policies favoring the wealthy.
- Concludes by encouraging reflection on these economic dynamics and their implications for society.

# Quotes

- "You want to raise the tide, you have to raise the working class."
- "The stimulus, yeah, it's got to start at the bottom."
- "This country has a widening gap between the haves and the have-nots."

# Oneliner

Beau explains a flawed analogy of rising tides in economics, criticizing wealth inequality and advocating for uplifting the working class to stimulate the economy.

# Audience

Policy Makers, Economic Activists

# On-the-ground actions from transcript

- Support grassroots movements advocating for economic equality (implied)
- Advocate for policies that prioritize supporting the working class (implied)

# Whats missing in summary

Deeper insights on the impact of economic policies on income inequality and societal well-being.

# Tags

#Economics #WealthInequality #WorkingClass #GovernmentPolicy #Stimulus #IncomeGap