# Bits

Beau says:

- A story about the Federation prompts an entertaining reaction on social media, involving aliens in contact with governments.
- Humans are not considered ready to join an exclusive club of intelligent beings in the galactic federation.
- The response to this story suggests that humanity is seen as the "bad neighborhood" in the galaxy.
- Lack of a warp drive is cited as a hindrance, but most people point to the state of the world as the reason for not being part of the Federation.
- Issues like food distribution, wars for resources, homelessness, and scientific focus on weapons are cited as reasons why Earth is seen as a problematic planet.
- Science fiction allows for political discourse in a different context, exploring the idea of advanced species wanting to help lesser planets.
- There is a belief that certain technological and philosophical milestones need to be met to join the Federation, influenced by science fiction.
- Beau questions whether the rejection of Earth's application to the Federation is due to humanity's acknowledgment of its failed state and lack of significant change efforts.
- He suggests that advanced beings might see Earth as a species that needs to be civilized and brought to a better future.
- The story includes an unbelievable aspect of Donald Trump being aware of the galactic federation but choosing not to talk about it.

# Quotes

- "We're very well aware of the fact we are behaving as a failed species, but we're not doing much to change it."
- "We know the world is messed up, and we're not doing much to change it."
- "Not technology."
- "He believed there were alien beings in space, he would, I don't know, build a wall in space or something."
- "We're going to have to face the fact that we have to change."

# Oneliner

Beau delves into a story about the Federation, reflecting on humanity's shortcomings and the potential for change to avoid a Mad Max future.

# Audience

Sci-fi enthusiasts, advocates for societal change.

# On-the-ground actions from transcript

- Start actively participating in initiatives that address food distribution and homelessness (implied).
- Advocate for peaceful resolutions to conflicts and work towards resource sharing (implied).
- Support scientific advancements focused on improving life for all beings (implied).

# Whats missing in summary

Exploration of how societal changes and global cooperation are vital for humanity to progress towards a more inclusive and advanced future.

# Tags

#GalacticFederation #ScienceFiction #Humanity #Change #Potential