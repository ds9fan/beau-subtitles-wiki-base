# Bits

Beau says:

- Explains the talking point of Trump winning more counties but Biden winning more votes.
- Counties vary significantly in population, so comparing them directly is not accurate.
- Illustrates with the example of Biden winning LA county with over three million votes.
- Contrasts Trump winning 142 counties with votes from Wyoming, North Dakota, and South Dakota.
- Emphasizes that Trump's vote count from those states includes every person, not just eligible voters.
- Points out that LA county has a larger population than most states that voted for Trump.
- Concludes that the talking point is misleading, not grounded in reality, and aims to confuse people.
- Encourages viewers to question sources spreading such misinformation.

# Quotes

- "Counties vary significantly in population, so comparing them directly is not accurate."
- "They're made up and they use different metrics and compare them to confuse people."
- "If a pundit, a commentator that you follow has pushed this idea, you might want to reconsider following them."

# Oneliner

Counties vary in population size, debunking the misleading talking point about Trump winning more counties but Biden winning more votes.

# Audience

Voters, Information Seekers

# On-the-ground actions from transcript

- Fact-check information from pundits or outlets spreading misleading narratives (implied).

# Whats missing in summary

The full video provides a detailed breakdown of the flawed comparison between Trump winning more counties and Biden winning more votes, offering clarity on how population discrepancies impact this analysis.

# Tags

#Election #Misinformation #Counties #Voting #FactChecking