# Bits

Beau says:

- In 2012, President Obama established DACA to protect individuals brought to the US as children without authorization, providing them with a sense of belonging.
- Trump attempted to end DACA in 2017, but the Supreme Court's ruling reinstated the program, safeguarding hundreds of thousands of individuals.
- Chad Wolf, the acting head of DHS, hindered new DACA applicants and reduced work authorization to one year. 
- A federal judge found Wolf's appointment unlawful, rendering his memo invalid and ordering DHS to resume accepting new DACA applications.
- With Biden's upcoming presidency, there is hope for DACA recipients as he is a vocal supporter of the program and may work towards a pathway to citizenship for them.
- Despite potential appeals from the Trump administration, it is unlikely that DACA will be terminated, and progress towards citizenship for DACA recipients may be on the horizon.

# Quotes

- "Start off your weekend with some good news."
- "This is a win."
- "They can stay here, but they're not citizens."
- "They're stuck in limbo."
- "Y'all have a good day."

# Oneliner

In 2012, DACA protected undocumented individuals brought to the US as children, and recent legal developments offer hope for their future under Biden's presidency.

# Audience

Advocates for immigrant rights

# On-the-ground actions from transcript

- Support organizations aiding DACA recipients (suggested)
- Stay informed about DACA updates and advocacy opportunities (implied)

# Whats missing in summary

The emotional impact and personal stories of DACA recipients and the significance of citizenship for their future.

# Tags

#DACA #Immigration #Biden #Citizenship #LegalRights #Hope