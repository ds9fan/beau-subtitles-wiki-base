# Bits

Beau says:

- Clarifies the misinterpretation of Martin Luther King Jr.'s quote about immediate results in pursuing social change.
- Emphasizes the need for tireless, persistent efforts in the long-term pursuit of social progress.
- Points out the misconception of using MLK's quote to justify instant gratification in activism.
- Urges the importance of continuous work towards social change, noting that progress is incremental.
- Stresses the necessity of reaching out to the 70 million Americans who supported Trump and engaging in meaningful dialogues.
- Advocates for speaking the same "language" as those with differing ideologies to bridge the gap.
- Warns against complacency after Biden's win, citing the lack of a progressive mandate and the need for individual action.
- Acknowledges the challenge of convincing a significant portion of Trump's supporters to reconsider their beliefs.
- Calls for framing issues in a way that resonates with different audiences, including both big picture and kitchen table issues.
- Criticizes the Democratic Party's messaging and failure to connect with low-income rural voters who share similar values.

# Quotes

- "The time is always right to do what is right."
- "Social change doesn't just will in on its own. You have to make it happen."
- "We have to reach out to them and we have to reach them successfully."
- "Trump lost, but he didn't lose big."
- "There's a lot of work to be done."

# Oneliner

Beau clarifies MLK's quote on immediate action, stresses incremental progress, and urges reaching out to Trump supporters to prevent future authoritarian leadership, advocating for effective communication across ideological divides.

# Audience

Activists, Communicators, Progressives

# On-the-ground actions from transcript

- Reach out to Trump supporters in a way that resonates with them, adopting effective rhetoric to bridge ideological gaps (implied).
- Frame issues as both big picture issues and kitchen table issues to appeal to diverse audiences (implied).
- Work towards convincing a significant portion of Trump's 70 million supporters to reconsider their beliefs through meaningful engagement (implied).

# Whats missing in summary

The full transcript provides detailed insights on the necessity of persistent efforts in pursuing social change, the challenges of bridging ideological divides, and the imperative to reach out to Trump supporters to prevent future authoritarian leadership.

# Tags

#SocialChange #Activism #Communication #PoliticalEngagement #ProgressiveMovements