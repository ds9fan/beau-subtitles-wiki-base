# Bits

Beau says:

- Raises questions about law enforcement officers in California refusing to enforce mandates.
- Notes that all law enforcement officers in California refusing to enforce mandates are sheriffs.
- Explains that sheriffs have more power than chiefs of police and can prioritize law enforcement actions.
- Mentions that sheriffs may delay enforcement if they doubt the constitutionality of a mandate.
- Points out that sheriffs cannot simply refuse to enforce laws based on personal preference.
- Suggests that sheriffs may deprioritize enforcement based on perceived unconstitutionality.
- Argues that sheriffs choosing not to enforce certain laws show they have discretion and are not just robots.
- Raises concerns about potential biases when law enforcement officers choose which laws to enforce.
- Questions the source of authority for law enforcement officers if they can disregard elected officials' laws.
- Criticizes the idea of law enforcement being autonomous and unaccountable, advocating for them to be public servants.

# Quotes

- "Sheriffs all over the country right now are showing that's not true. They are showing that when they enforce an unjust law, they are culpable."
- "There are a lot of laws that significantly impact populations in a manner that is unfair. When officers choose to enforce that, they're making the choice."
- "They're not supposed to be unaccountable people with guns. They're supposed to be public servants."
- "If law enforcement does in fact have the ability to disregard the laws and regulations coming from elected officials, where does their authority come from?"
- "Y'all have a good day."

# Oneliner

Law enforcement officers' selective enforcement raises questions about authority and accountability, undermining their role as public servants.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Question law enforcement actions (suggested)
- Advocate for accountability in law enforcement (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of the implications of law enforcement officers' discretion in enforcing laws and raises critical questions about their accountability and authority.

# Tags

#LawEnforcement #Accountability #Authority #PublicServants #California