# Bits

Beau says:

- Addressing Eric Trump's claim of his father saving Christianity, Beau questions the validity of this statement.
- Beau points out the lack of evidence for Christianity being in danger from legal or legislative battles.
- He criticizes the fear-mongering and manipulative tactics used to sway people's emotions.
- Beau questions if Trump exemplifies Christian teachings like loving thy neighbor and helping the needy.
- He warns about the danger of blending religion and the state, urging faith leaders to address this issue.
- Beau stresses that Christianity has survived various challenges and won't be destroyed by politicians.
- He cautions against blindly following leaders who don't embody the values of the faith.
- Beau calls for a rebuke of the notion that Christianity is under threat from politicians manipulating faith for their agenda.

# Quotes

- "There is a concerted effort by a group of people to use your faith to manipulate you."
- "Christianity is not in danger. It has survived a lot."
- "It's going to get stomped out because people believed politicians and were led astray."

# Oneliner

Beau questions the manipulation of faith for political gain and asserts Christianity's resilience against such tactics.

# Audience

Faith leaders and believers

# On-the-ground actions from transcript

- Address the issue of blending religion and state within your community (suggested)
- Encourage critical thinking and discernment regarding political manipulation of faith (implied)

# Whats missing in summary

Beau's engaging and thought-provoking delivery

# Tags

#Christianity #Politics #Manipulation #FaithLeaders #Resilience