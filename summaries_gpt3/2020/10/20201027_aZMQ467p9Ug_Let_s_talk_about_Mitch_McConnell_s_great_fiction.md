# Bits

Beau says:

- Mitch McConnell and the Senate pushed through a Supreme Court nominee instead of providing stimulus relief to Kentuckians.
- The nominee couldn't name the components of the First Amendment during the hearing.
- McConnell created a fiction that the Republican Party cares about the Constitution and working Americans.
- The new justice may not intentionally undermine the Constitution because she doesn't know what's in it.
- Republicans prioritize power and keeping working-class people down over helping them.
- McConnell and other senators serve the rich rather than their constituents.
- Senators were more focused on confirming Trump's nominee than on helping the economy.
- The nominee couldn't answer basic civics questions but will be interpreting the Constitution.
- Republicans care more about big companies than the common folk.
- Voters who re-elect these senators enable their actions and lack of accountability.

# Quotes

- "They care about power and keeping working-class commoners in their place."
- "The rich keep getting richer and the poor keep getting poorer."
- "You co-sign them. You say, hey, I'm okay with what they're doing."
- "You've been conditioned by that fiction to believe that the Republican Party is the party of patriotism."
- "They confirmed a Supreme Court nominee who could not tell you what the First Amendment was."

# Oneliner

Mitch McConnell and the Senate prioritized a Supreme Court nominee over providing stimulus relief, revealing their true priorities and lack of care for working Americans.

# Audience

Voters, Working Americans

# On-the-ground actions from transcript

- Hold senators accountable for their actions (implied)
- Pay attention to your representatives' priorities and hold them responsible (implied)

# Whats missing in summary

The full transcript provides detailed insights into the prioritization of power over helping working-class Americans by Republican senators.

# Tags

#MitchMcConnell #Senate #SupremeCourt #StimulusRelief #WorkingAmericans