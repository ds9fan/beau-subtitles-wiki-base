# Bits

Beau says:

- Addressing small government conservatives who are uncertain about the upcoming election.
- Small government conservatives prioritize down-ballot races and struggle with the presidential choices.
- They can't fully support Trump due to his failures and authoritarian tendencies.
- Similarly, they can't back Biden as he represents liberal policies.
- The primary concern for small government conservatives is about money and limited government intervention.
- They seek candidates with strong business credentials and aim to run the country like a business.
- Economic issues, such as reducing national debt and corporate subsidies, are key priorities.
- Their stance on social issues is often tied to economic considerations rather than social concerns.
- Beau suggests Joe Jorgensen as a candidate who closely aligns with small government conservative values.
- Encourages them to vote based on principles and conscience rather than party loyalty.

# Quotes

- "You're going to walk into that voting booth and you're going to vote for Trump simply because he has an R next to his name and you don't want to do that."
- "You're going to vote for him simply because he has an R next to his name and that's gonna be the other response."
- "All it takes is informing yourself."

# Oneliner

Addressing small government conservatives torn between Trump and Biden, Beau advocates for voting based on principles and introduces Joe Jorgensen as a candidate who closely resonates with their values.

# Audience

Small government conservatives

# On-the-ground actions from transcript

- Inform yourself about alternative candidates like Joe Jorgensen (suggested).
- Vote based on principles and conscience rather than party loyalty (implied).

# Whats missing in summary

The full transcript includes detailed reasoning and insights for small government conservatives struggling with their presidential vote choice.

# Tags

#SmallGovernmentConservatives #PresidentialElection #Principles #JoeJorgensen #Voting