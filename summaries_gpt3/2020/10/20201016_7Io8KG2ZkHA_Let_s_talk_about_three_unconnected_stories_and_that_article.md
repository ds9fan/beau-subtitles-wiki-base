# Bits

Beau says:

- Beau introduces three seemingly unconnected stories to shed light on how things may not be as they seem.
- Using Coca-Cola as an example, Beau explains the importance of having a product people want and the infrastructure to deliver it.
- Beau shares a personal experience with a knife that broke upon delivery, illustrating the importance of a product's quality.
- He talks about showcasing the idea that cost doesn't always equate to value in certain industries.
- Beau mentions a beverage company that stopped delivering a product with poor ingredients, showing responsibility.
- He recalls trying to order a controversial book at a bookstore and facing resistance due to its content.
- Beau contrasts how certain actions are considered normal, while others, like social media censorship, spark controversy.
- He questions the framing of social media giants reducing the volume of an article criticizing Biden as censorship.
- Beau explains that free speech doesn't guarantee a platform or promotion for every idea and stresses individual responsibility.
- He concludes by stating that bad ideas and information are not entitled to promotion or support.

# Quotes

- "Nobody owes bad ideas, bad information, a social safety net."
- "The market decided we don't want them."

# Oneliner

Beau introduces three stories to challenge how things are framed, discussing responsibility, censorship, and the market deciding what ideas are promoted.

# Audience

Consumers, critical thinkers.

# On-the-ground actions from transcript

- Challenge the framing of information and narratives (implied).
- Support responsible actions by companies and individuals (implied).
- Promote critical thinking and individual responsibility (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how narratives are framed and the importance of individual responsibility in promoting ideas.

# Tags

#Framing #Responsibility #Censorship #MarketDecides #CriticalThinking