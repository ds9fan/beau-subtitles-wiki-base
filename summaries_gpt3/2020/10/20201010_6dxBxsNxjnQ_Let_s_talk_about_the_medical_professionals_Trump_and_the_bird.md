# Bits

Beau says:

- Scientific American endorsed Joe Biden, breaking from tradition.
- New England Journal of Medicine, typically apolitical, criticized current political leaders for their response to the public health crisis.
- The response by the nation's leaders has been consistently inadequate.
- The crisis has revealed a failure in leadership in the United States.
- The journal pointed out that the country's testing rates are lower than countries like Kazakhstan, Zimbabwe, and Ethiopia.
- Some governors handled the crisis well regardless of party affiliations.
- Many politicians politicized the use of masks.
- The journal did not endorse Biden but emphasized the need for Trump to lose the election.
- Despite Trump's endorsements from law enforcement, the real issues in the US are related to health and science, not crime.
- Few respected scientific or medical figures are endorsing the president, with many suggesting he needs to be replaced.
- The New England Journal of Medicine's statement was signed by 35 editors, indicating a serious criticism of the US's handling of the crisis.

# Quotes

- "We should not abet them and enable the deaths of thousands more Americans by allowing them to keep their jobs."
- "They have taken a crisis and turned it into a tragedy."
- "Rather than just continuing to ignore the people who may know what they're talking about, we might want to heed their advice."

# Oneliner

Scientific American and New England Journal of Medicine criticize US leaders for inadequate response to public health crisis, urging Trump's removal.

# Audience

Voters, Health Advocates

# On-the-ground actions from transcript

- Pay attention to statements from respected scientific and medical experts, and advocate for necessary changes (implied).
- Vote in upcoming elections based on candidates' responses to public health crises and trust scientific expertise (implied).

# Whats missing in summary

The full transcript provides detailed insights on the New England Journal of Medicine's unprecedented criticism of US political leaders' response to the public health crisis, urging for a change in leadership based on health and science priorities.

# Tags

#PublicHealth #USLeadership #Election2020 #ScientificExpertise #PoliticalEndorsements