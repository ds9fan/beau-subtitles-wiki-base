# Bits

Beau says:

- Talks about the 2020 election and what to expect from the incumbent in the next few weeks.
- Describes the incumbent, Donnie, as a class bully with a rich background and lack of accomplishments.
- Mentions Donnie's lack of participation in class projects and failure to implement policies.
- States that it's too late for Donnie to propose policies or develop leadership skills.
- Comments on Donnie's endorsements, including support from the school resource officer and the school rival.
- Views the upcoming election as a referendum on Donnie's administration.
- Speculates on possible desperate tactics Donnie might employ, such as pushing a disliked student against lockers or spreading rumors.
- Anticipates Donnie resorting to making baseless claims due to his low chances of winning.
- Advises preparing for Donnie's potential unfounded claims and reminding others of his history of fabrication.

# Quotes

- "It's a referendum on Donnie's administration or lack thereof."
- "He's probably going to rely on what's always worked for him, making stuff up."
- "We should be ready for some explosive claims that are completely baseless and unfounded."

# Oneliner

Beau covers the 2020 election dynamics at Anytown High School, predicting desperate tactics from the incumbent and advising readiness for baseless claims. 

# Audience

Students, Voters, School Community

# On-the-ground actions from transcript

- Remind fellow students of the incumbent's history of fabrication (implied)
- Stay vigilant against potential baseless claims and misinformation (implied)

# Whats missing in summary

The full transcript provides a detailed insight into the dynamics of an election scenario at a high school, shedding light on potential tactics and challenges faced by the incumbent.

# Tags

#Election #HighSchool #Incumbent #DesperateTactics #BaselessClaims