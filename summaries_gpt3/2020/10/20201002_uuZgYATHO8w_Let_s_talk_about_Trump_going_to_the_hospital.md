# Bits

Beau says:

- Trump is on his way to Walter Reed with the White House claiming everything is fine.
- There's skepticism due to the administration's lack of truthfulness.
- The importance of Trump fully recovering or having no symptoms is not out of sympathy but to avoid the survival of "Trumpism."
- It's vital for Trump to lose at the polls to reject his policies and not let them be seen as martyrdom.
- Rejecting Trump at the polls is rejecting the hate, divisiveness, and authoritarian rule he represents.
- The rejection of Trump by all marginalized demographics in the country is significant.
- Beau isn't overly concerned about who wins in the election as long as Trump loses.
- Surviving a negative outcome for Trump prevents the people from rejecting his policies collectively.
- The defeat of Trump at the polls is critical for the nation's soul to reject everything he stands for, not just his person.
- Beau stresses the importance of American voters defeating Trump rather than a public health issue.

# Quotes

- "It's incredibly important that the president fully recovers or suffers no symptoms."
- "If we don't show at the polls that Trump's policies are anti-American, that Trump's policies are the cause of the division, they'll continue."
- "I want him to survive probably more than anybody because I think that's more important than anything."
- "It's not just a person anymore. It's Trumpism."
- "I want the American voters to."

# Oneliner

Beau stresses the critical importance of rejecting Trump at the polls to prevent the survival of "Trumpism" and its divisive policies.

# Audience

American voters

# On-the-ground actions from transcript

- Reject Trump's policies by voting against him in the upcoming election (implied)
- Encourage others to vote against Trump to reject hate, divisiveness, and authoritarian rule (implied)
- Advocate for marginalized communities by supporting their rejection of Trump (implied)

# Whats missing in summary

The emotional urgency and moral imperative conveyed by Beau can best be felt by watching the full transcript. 

# Tags

#RejectTrump #Election2020 #Trumpism #CriticalVote #CommunityAction