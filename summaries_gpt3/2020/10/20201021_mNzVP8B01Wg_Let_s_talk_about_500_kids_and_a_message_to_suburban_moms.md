# Bits

Beau says:

- 500 kids have been separated from their parents in 2017 and haven't been reunited since, some for more than half their lives.
- Finding the parents of these children seems unlikely as they've been bounced around the system for years.
- The children may now have addresses like row and plot numbers, making it harder to locate them.
- Beau questions if America has truly been made great again, considering the lack of respect on the international stage and the moral authority.
- The fear-mongering campaign may have created a nation afraid of everything, leading to actions like separating families.
- The forcible transfer of children by the United States violates treaties backed up by the Constitution.
- The President's actions have torn families apart through creating deterrents or displaying negligence towards public health issues.
- Each of the 500 stories of separated families should be heard and covered extensively to understand the consequences of apathy.
- Beau stresses the need to make a decision about the country's direction soon, as the lack of respect from other nations is evident.
- The President's actions have eroded the moral fabric of the nation, with the lack of media coverage on reuniting families showcasing a significant downfall.

# Quotes

- "What happened because of it."
- "We have a decision to make in this country."
- "The President has destroyed the moral fiber of this country."
- "We got work that needs to be done."
- "Y'all have a good day."

# Oneliner

500 kids separated from parents in 2017, still not reunited; America's moral fabric questioned under fear-mongering campaign, with urgent need for action.

# Audience

Every concerned citizen.

# On-the-ground actions from transcript

- Contact organizations working to reunite families (suggested)
- Spread awareness about the issue through social media and community networks (suggested)
- Join protests or advocacy campaigns demanding family reunification (suggested)

# Whats missing in summary

The emotional weight and urgency conveyed in Beau's delivery can best be experienced by watching the full transcript.