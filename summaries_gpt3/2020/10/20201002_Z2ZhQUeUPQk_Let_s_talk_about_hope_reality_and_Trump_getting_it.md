# Bits

Beau says:

- President of the United States tested positive for COVID-19, impacting many others.
- Expresses sincere hope for their full recovery and that they go through it without incident.
- Points out the disparity between working class Americans and those with access to top healthcare.
- Criticizes the administration for prioritizing forcing people back to work over their safety during the pandemic.
- States that the office of the Presidency is more significant than working at a regular job.
- Acknowledges that the President testing positive won't shield him from rightful criticism.
- Notes the bad timing of the President's diagnosis so close to the election.
- Emphasizes the seriousness of the situation and the need for everyone to take precautions seriously.
- Encourages basic preventive measures like washing hands, wearing masks, and staying at home.

# Quotes

- "I truly do hope they make it through this."
- "Wash your hands. Don't touch your face. Stay at home."
- "The office of the Presidency is more significant than working at Winn-Dixie."

# Oneliner

The President's COVID diagnosis doesn't shield him from criticism; everyone should take precautions seriously.

# Audience

General public

# On-the-ground actions from transcript

- Wash your hands, don't touch your face, stay at home, wear a mask (suggested)

# Whats missing in summary

The emotional impact of the President's diagnosis and the importance of taking the pandemic seriously.

# Tags

#COVID-19 #President #Health #Precautions #Election