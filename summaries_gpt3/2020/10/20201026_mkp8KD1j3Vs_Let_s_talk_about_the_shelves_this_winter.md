# Bits

Beau says:

- Talks about inventories, stocks, and a potential upcoming situation.
- Mentions discussing the economics behind masks eight months ago.
- Explains the difference between N95 masks, surgical masks, cloth masks, and respirators.
- Regrets not taking the previous situation seriously enough.
- Predicts possible shortages in the upcoming months, especially during winter.
- Advises preparing ahead by stocking up on essentials like canned food, paper towels, and toilet paper.
- Encourages having a stash for emergencies regardless of where you live.
- Emphasizes the importance of reducing strain by being prepared.
- Suggests getting what you need now to avoid being in a rush during shortages.
- Recommends taking oneself out of the equation by stocking up in advance.

# Quotes

- "I have been kicking myself for the last eight months over that."
- "If you have the means, if you can get ahead now, you can reduce some of that strain."
- "Go ahead and get what you need now so you're not one of the people out there straining the system then."
- "Just set it back and carry on as normal."
- "Anyway, it's just a thought."

# Oneliner

Beau predicts potential shortages in the upcoming months and advises stocking up on essentials to reduce strain and be prepared.

# Audience

Prepared individuals

# On-the-ground actions from transcript

- Stock up on essentials like canned food, paper towels, toilet paper, and other shelf-stable items (suggested).

# Whats missing in summary

Importance of being proactive and prepared for potential shortages. 

# Tags

#Preparedness #StockingUp #Shortages #Essentials #EmergencyPreparedness