# Bits

Beau says:

- An officer in the NYPD was suspended for displaying "Trump 2020" on their squad car, sparking controversy over political neutrality.
- The Sergeant's Benevolent Association (SBA) criticized the suspension, claiming it was unnecessary and that NYPD officers have a history of making political statements.
- Beau argues against the idea of maintaining the status quo just because it's always been done that way, especially in a politically charged environment like the current election.
- The SBA equated endorsing a political candidate with taking a knee with Floyd demonstrators, showing a lack of understanding between political matters and moral issues.
- Beau suggests that all in-custody deaths should be condemned, regardless of past practices or norms.
- He questions why Donald Trump is viewed as a racist, pointing out how his allies portray him and how it feeds into devaluing black lives.
- Beau contrasts the actions of deputies and sheriffs in the deep south who took a knee in solidarity, even though most are Trump supporters, to show that it's a matter of right vs. wrong, not politics.
- The SBA represents an institutional memory that hinders progress in law enforcement by resisting necessary reforms and improvements.
- Beau challenges the NYPD to lead reform efforts rather than being an obstacle to change and advancement within law enforcement.

# Quotes

- "Just because something has always been done that way is not a justification to continue doing it."
- "When you got sheriffs and deputies in the old south telling you to ease up on the black folk, you got a problem."
- "The SBA is part of the problem in NYPD."
- "It's the cops who don't want to get better."
- "Perhaps they should be leading the charge to advance and get better instead of holding it back."

# Oneliner

An officer's suspension for political display sparks debate, revealing resistance to change within NYPD and the need for moral over political stances.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Contact local law enforcement agencies to advocate for unbiased and apolitical conduct (suggested).
- Organize community dialogues on the distinction between political matters and moral issues (exemplified).

# Whats missing in summary

The full transcript provides additional insights on the importance of challenging institutional norms and advocating for ethical standards in law enforcement.