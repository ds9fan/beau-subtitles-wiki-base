# Bits

Beau says:

- Beau dives into a social media post by AOC that draws comparisons between the Panthers' food distribution network and a project AOC was involved in.
- He encourages people to look into the Black Panthers beyond the media's portrayal, noting their radical nature and effective community networks.
- Critiques of AOC's post revolved around its phrasing, framing, and being labeled as performative activism.
- Beau argues that the debate over the most effective method of social change is ongoing and complex, with no definitive answer.
- He suggests that performative posts, even if criticized, play a valuable role in drawing attention to social issues and making them socially acceptable.
- Changing societal thinking is key to societal change, and performative posts can contribute to this shift by making causes desirable and attracting support.
- Beau acknowledges that not everyone can directly participate in activism but sharing social media posts can still contribute to social change.
- By exposing thousands to the idea of solving local issues through community networks, AOC's post may have inspired direct engagement in the future.
- Beau stresses the importance of a diversity of tactics in creating social change and the role of performative actions in shaping societal thought.
- He concludes by underlining the significance of social media as a powerful tool for changing societal perspectives and the necessity of utilizing it for positive change.

# Quotes

- "If you want to change society, you have to change the way people think."
- "Those posts, performative or not, are incredibly valuable because our goal is to change society."
- "Performative posts are incredibly valuable."
- "Our goal is to change society. We have to change the way people think."
- "Social media is an incredibly, incredibly valuable tool."

# Oneliner

Beau explains the value of performative activism in shaping societal thought and sparking social change through exposure to new ideas and community networks.

# Audience

Social Media Users

# On-the-ground actions from transcript

- Share social media posts about community initiatives and social issues to raise awareness and shape societal perspectives (implied).
- Encourage community engagement and involvement in local projects to address social issues and create positive change (implied).

# Whats missing in summary

The full transcript provides a nuanced exploration of the role of performative activism in shaping societal attitudes and driving social change, advocating for a diversity of tactics and the power of community networks in addressing local issues.

# Tags

#PerformativeActivism #SocialChange #CommunityNetworks #BlackPanthers #SocietalThought