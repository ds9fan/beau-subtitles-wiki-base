# Bits

Beau says:

- Beau addresses the common question he receives about how to reach loved ones who are loyal to Trump and seem unreachable despite efforts to change their minds.
- He points out that many individuals asking for help in reaching out to their pro-Trump family members or friends often express a sense of failure because they couldn't convince them otherwise.
- Beau shares a personal anecdote about a commenter who used to vehemently support Trump but, after watching a video collaboration Beau did with another channel, reached out to apologize and express a change of heart.
- He suggests that sometimes the messenger, not the message, is what matters in getting through to someone who holds strong political beliefs.
- Beau notes that trying to argue based on the principles someone held five years ago might not be effective, as Trump loyalists today are often driven by loyalty to him rather than traditional principles.
- He warns against reinforcing the idea of Trump as a savior by pointing out how much the person has changed, as it could backfire and strengthen their loyalty to him.
- Beau acknowledges that there may be no universal combination of arguments that work on every person, citing his own relatives with Trump signs in their yard.
- He recognizes that some individuals may be too deeply entrenched in the cult of personality around Trump to be swayed by logical arguments, suggesting that external factors beyond one's control might be needed to break that loyalty.

# Quotes

- "Sometimes it's not the arguments you're using. Sometimes it's not your, sometimes there is no combination for you."
- "You can't look at this as a failure on your part. You did everything you could."
- "There is no combination. There's no pattern that's going to work on every person. It doesn't exist."
- "Some men you just can't reach."
- "It may just be that you're not the right person to make that argument."

# Oneliner

Beau addresses the struggle of reaching out to Trump loyalists, stressing the importance of the messenger and acknowledging the challenges of changing deeply held beliefs.

# Audience

Those struggling to reach pro-Trump family members or friends.

# On-the-ground actions from transcript

- Have open, empathetic dialogues with pro-Trump individuals (suggested).
- Recognize when to step back and accept that some people may be beyond your influence (exemplified).

# Whats missing in summary

The full transcript provides nuanced insights into the challenges of engaging with Trump loyalists and the importance of recognizing personal limitations in changing deeply entrenched beliefs.