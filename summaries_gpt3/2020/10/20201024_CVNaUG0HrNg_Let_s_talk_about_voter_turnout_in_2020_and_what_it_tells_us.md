# Bits

Beau says:

- Predicted voter turnout is incredibly high, with 150 million people expected to vote, around 65% turnout in the US.
- Historical parallels to the 1908 election where high turnout was seen, with similar strategies and concerns about crowd sizes.
- One candidate in 1908 didn't seek establishment support, campaigned against the elite, and embraced new technology like stumping.
- Teddy Roosevelt, a popular figure, supported Taft for the presidency, resulting in a high voter turnout and Taft's win.
- Experts suggest a potential landslide in the upcoming election due to high voter enthusiasm and mail-in voting accessibility.
- In a representative democracy like the US, high voter turnout should be encouraged to uphold the democratic ideals.
- Beau points out the importance of identifying those who try to suppress the vote, as it goes against the fundamental principles of America.
- He warns about the dangers of individuals undermining democratic values for personal gain and urges vigilance against such efforts.

# Quotes

- "To oppose high voter turnout, to attempt to suppress the vote, is to undermine the very ideas this country was founded on."
- "Those who believe in the ideas that America is supposed to embody, the idea that the people are the leader, they should want high voter turnout."
- "There is a systemic effort by one party to suppress the vote."
- "If they're willing to sell out a pillar of what's supposed to make up America, well they will certainly sell out you."
- "Y'all have a good day."

# Oneliner

Predicted high voter turnout in the US draws historical parallels to past elections, raising concerns about suppression efforts and the importance of upholding democratic values.

# Audience

American voters

# On-the-ground actions from transcript

- Investigate efforts to suppress the vote and support organizations working against voter suppression (exemplified)
- Encourage voter engagement and support initiatives that facilitate accessible voting methods (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of historical voter turnout, the significance of mail-in voting, and the dangers of voter suppression tactics.

# Tags

#VoterTurnout #HistoricalParallels #VoterSuppression #Democracy #MailInVoting