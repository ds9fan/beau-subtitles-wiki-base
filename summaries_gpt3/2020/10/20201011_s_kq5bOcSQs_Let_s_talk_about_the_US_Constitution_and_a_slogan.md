# Bits

Beau says:

- Criticizes a conservative talking point about restoring the Constitution by giving Trump more power.
- Explains that the Constitution is essentially a permission slip from states to the federal government.
- Points out that the Constitution established a representative democracy with checks and balances.
- Questions the logic of restoring a document (Constitution) that failed to prevent one branch from gaining too much power.
- Argues that if you believe the Constitution failed, it might be better to create a new one instead of restoring it.
- Emphasizes that giving more power to one branch of government (like Trump) goes against the principles of checks and balances.
- Stresses the importance of reading and understanding the Constitution rather than blindly following it as an icon.
- Suggests that the Constitution allows for change through a legal process instead of resorting to extreme measures.
- Advocates for using the existing machinery for change within the Constitution to address issues rather than resorting to violence.
- Proposes ideas like ranked choice voting and abolishing the electoral college to address power consolidation issues.

# Quotes

- "The whole idea was to protect against tyranny. So if you are saying that it failed to do that, there's no purpose in restoring it."
- "If any branch was able to gain too much power, the Constitution failed."
- "If you believe the Constitution is a failure, perhaps you want a new one."
- "It's normally accompanied by some pretty extreme rhetoric."
- "You don't need to reach into your gun cabinet if the Constitution has an issue."

# Oneliner

Beau questions the logic of restoring a Constitution that failed to prevent power imbalances and suggests considering new ways to address governance issues instead.

# Audience

Citizens, Voters, Activists

# On-the-ground actions from transcript

- Use the existing machinery for change within the Constitution to address issues (implied).

# Whats missing in summary

Exploration of how the Constitution can be effectively adapted and improved through legal processes and civic engagement.

# Tags

#Constitution #ChecksAndBalances #PowerBalance #LegalProcesses #CivicEngagement