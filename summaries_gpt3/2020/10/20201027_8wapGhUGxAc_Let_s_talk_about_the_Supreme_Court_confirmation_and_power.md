# Bits

Beau says:

- Criticizes the new Supreme Court justice for her lack of qualifications and inability to answer basic questions about the Constitution during her confirmation hearing.
- Points out that the Republican Party is willing to wield power to achieve their goals, regardless of qualifications or consequences.
- Suggests that if Democrats win the House, Senate, and White House, they should use their power decisively to make significant changes, including restructuring the courts and passing progressive policies.
- Expresses disappointment in both Republican and Democratic politicians for either selling out the country or not fighting hard enough.
- Urges the Democratic Party to exercise power and implement deep, systemic changes to energize and retain support from voters.
- Warns against the Democratic Party following the Republican Party's shift towards the far right and calls for real change to prevent becoming like the current Republicans.
- Emphasizes the need for Democrats to take action to nullify the Supreme Court justice's vote due to her lack of understanding of basic freedoms protected by the Constitution.
- Concludes by stressing the importance of real change and the necessity for House and Senate action to drive it, rather than relying on the White House.

# Quotes

- "She's not qualified for the job, period."
- "It's really that simple."
- "If the Democratic party has the House, the Senate, and the White House and utilizes that power to make things better for the average person."
- "The Democratic Party seems very unwilling to exercise power, very unwilling to wield the power when they have it."
- "Deep, systemic change and it's going to have to come from the House and the Senate."

# Oneliner

Beau criticizes the lack of qualifications in the new Supreme Court justice, urges Democrats to exercise power for real change, and stresses the importance of systemic change originating from the House and Senate.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Rally support for deep systemic changes by engaging with local political organizations and campaigns (implied)
- Advocate for nullifying the Supreme Court justice's vote through contacting elected representatives and supporting legislative actions (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the current political landscape, urging for significant changes to be made by the Democratic Party to maintain power and drive real systemic change.