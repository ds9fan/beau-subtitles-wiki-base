# Bits

Beau says:

- Influencers facing pressure to take political stands to retain audience.
- All topics have political implications, from travel to food to fashion.
- Encourages influencers to condemn bigotry and amplify voices of marginalized.
- Advocates for self-education on issues related to their content.
- Emphasizes the importance of not attacking those with less power.
- Pushes for uplifting others instead of tearing them down.
- Being a good person is key, rather than being a political expert.

# Quotes

- "Everything's political. Everything is political."
- "You don't have to become a political expert. Just try to be a good person."
- "Never kick down. Only punch up."
- "Just have to talk about the things that relate directly to your genre."
- "If you take the moral stands, you don't have to come out and endorse a candidate."

# Oneliner

Influencers are urged to acknowledge the political undercurrents in all topics, condemn bigotry, amplify marginalized voices, and uplift others, promoting goodness over political expertise.

# Audience

Influencers, content creators

# On-the-ground actions from transcript

- Condemn bigotry and police comments on your platform (suggested)
- Speak up for marginalized voices using your platform (suggested)
- Self-educate on issues related to your content (suggested)
- Never attack those with less power; focus on uplifting (suggested)

# Whats missing in summary

Beau's engaging and thoughtful delivery.

# Tags

#Influencers #Politics #SocialResponsibility #UpliftOthers #SelfEducation