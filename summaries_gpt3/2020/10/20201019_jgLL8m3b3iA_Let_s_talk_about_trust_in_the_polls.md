# Bits

Beau says:
- Addresses the topic of trusting polls and the consistent lead of Biden in polls.
- Mentions the doubts surrounding poll accuracy due to past incidents like Clinton's lead.
- Points out that pollsters previously undercounted uneducated voters, particularly white voters without a college degree.
- Expresses disagreement with the term "uneducated voters" and the correlation between education and susceptibility to fear-mongering.
- Talks about how pollsters have adjusted their methods by weighting uneducated voters more heavily.
- Notes the decrease in the percentage of whites without a college degree in the voting populace.
- Emphasizes that polls are not magical crystal balls and rely on people's actions matching their poll responses.
- Suggests that people need to act according to the poll results for them to be accurate.
- Concludes by advising to trust the polls due to statistical adjustments but also not to trust them entirely.

# Quotes

- "Education and credentialing are not the same thing."
- "Pollsters create their forecast based on what people say during the polls."
- "If people don't do what they say, then the poll, it's going to be wrong."
- "Yes, trust the polls because they fixed the statistical issue, but no, don't trust them because they don't actually mean anything."
- "It's really that simple."

# Oneliner

Beau explains the dynamics of trusting polls, addressing past inaccuracies and adjustments made by pollsters, advocating for both trust and skepticism in poll results based on people's actions matching their poll responses.

# Audience

Voters, Poll-watchers

# On-the-ground actions from transcript

- Follow through on reported poll demographics when voting (implied).

# Whats missing in summary

Beau's engaging and informative delivery style is missing in the summary.

# Tags

#Polls #Trust #Election #Voters #StatisticalAnalysis