# Bits

Beau says:

- Addressing comments on bias towards Trump and Biden.
- Defining bias as unfair favoritism.
- Stating his role on the channel is to apply common sense to current situations.
- Pointing out Trump's alignment with divisive and hateful individuals.
- Expressing that Biden is objectively less harmful.
- Anticipating critiquing Biden's actions soon.
- Acknowledging Biden's leadership as a private citizen.
- Sharing a personal encounter at a convenience store discussing the debates.
- Not believing the country can survive another four years of Trump based on his actions.
- Distinguishing between bias and applying consistent standards.
- Rejecting neutrality in the face of injustice.

# Quotes

- "I don't believe the country can survive another four years of Trump."
- "There is a lot of injustice in the world. I am not taking the side of the oppressor."
- "That's not bias. That's applying the same standard."
- "Biden is objectively less harmful."
- "I just want him to stop the decline into being a failed state."

# Oneliner

Beau talks about bias, Trump, and Biden, stressing common sense and objective assessment.

# Audience

Viewers, Political Observers

# On-the-ground actions from transcript

- Apply common sense to current situations (implied)
- Critique leaders objectively based on actions (implied)
- Engage in political discourse with awareness and understanding (implied)

# Whats missing in summary

Insights on specific policies or actions that could help prevent a decline into a failed state.

# Tags

#Bias #Trump #Biden #CommonSense #Injustice