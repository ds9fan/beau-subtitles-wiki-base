# Bits

Beau says:

- President Trump mentioned "rounding a corner" in debates, but Beau views it differently.
- The US recently hit its highest daily new cases ever: 80,005.
- Experts predict six-figure daily cases soon.
- Trump hasn't attended task force meetings in months, relying on Scott Atlas instead of Fauci's advice.
- Atlas spread misinformation about masks' effectiveness, leading Twitter to remove his tweet.
- A new study shows wearing masks and following guidelines can save lives.
- If 95% of Americans comply, 130,000 lives can be saved by February.
- People resistant to wearing masks often want to appear tough or brave.
- Beau urges everyone to wash hands, avoid touching faces, stay home, wear masks, and practice social distancing.
- Despite fatigue setting in, precautions must continue as 130,000 lives depend on it.

# Quotes

- "Your country is calling. They need you to wear a mask."
- "We cannot trust this administration to make the right calls."
- "You want to be a hero? Wear a mask."
- "We have to stay in the fight until it's over."
- "It's really that simple."

# Oneliner

Beau warns against trusting the administration's handling of COVID-19, urging heroism through mask-wearing to protect lives and maintain precautions.

# Audience

General public

# On-the-ground actions from transcript
- Wear a mask, wash hands, avoid touching face, stay home, and practice social distancing to protect lives (implied).

# Whats missing in summary

The full video provides additional context and a deeper understanding of the importance of continued vigilance against COVID-19.

# Tags

#COVID-19 #PublicHealth #MaskWearing #Precautions #Trust #Administration