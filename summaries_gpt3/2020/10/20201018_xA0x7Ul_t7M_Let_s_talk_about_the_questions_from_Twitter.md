# Bits

Beau says:

- Beau held a live stream where he answered questions from Twitter, but ran out of time and didn't get to address any.
- Beau decided to answer some of the questions in an off-the-cuff manner.
- He discussed the media's use of soft language like "meddling" instead of more accurate terms like "Russian intelligence operation."
- Beau shared his views on why some Americans still support Donald Trump, citing reasons like reluctance to admit being wrong and resonating with certain talking points.
- He talked about having civil, fact-based debates with those who disagree with you, focusing on sticking to objective facts.
- Beau expressed his belief that political engagement due to Trump's presidency could lead to a more progressive outlook in the country.
- He addressed the possibility of Trump's followers turning violent if he loses the election.
- Beau discussed the progressive stance on Biden's lack of progressiveness, acknowledging that while Biden may not be ideal, he is still better than the alternative.
- He shared his thoughts on whether Trump will leave the U.S. if he loses the election, expressing skepticism about potential legal consequences for Trump.
- Beau talked about the American perspective on public healthcare and the resistance due to the fear of government control.
  
# Quotes

- "It's a choice of language in which I think the media outlets are trying to elicit an emotional reaction when a rational one would probably better serve the country."
- "Most people watching this channel, Joe Biden doesn't represent the change you want. However, he's definitely better than the change you're going to get from Trump."
- "I think everybody should have a wide skill set. I don't think and understand when I say this I'm not necessarily talking about because what may happen in a few months."

# Oneliner

Beau provides off-the-cuff responses to questions on media terminology, Trump's support, civil debates, Biden's progressiveness, and public healthcare.

# Audience

Political enthusiasts

# On-the-ground actions from transcript

- Reach out to people on social media platforms like Twitter to start community building efforts. (implied)
- Stay politically engaged and push for progress regardless of election outcomes. (implied)

# Whats missing in summary

Insights on how to maintain political engagement post-election and foster inclusive representation in political platforms.

# Tags

#Media #Trump #Biden #PublicHealthcare #PoliticalEngagement #CommunityBuilding