# Bits

Beau says:

- Recounts a 1950s football game involving Johnny Bright, a black player destined for the Heisman.
- Bright was intentionally hit in a racially motivated incident, impacting college athletics.
- The incident was captured in photos, becoming a symbol for the civil rights movement.
- The imagery energized the civil rights movement by making the mistreatment visible.
- The NCAA changed rules and implemented safety measures in response to the incident.
- Beau draws parallels between this historical event and America's current political situation.
- Trump's actions are likened to breaking America's jaw on film, forcing the country to confront its issues.
- Beau questions whether the country will learn from this incident or require further crises to prompt change.

# Quotes

- "He broke America's jaw."
- "Are we going to be as responsive as the NCAA?"
- "Does the country need their jaw broken again?"

# Oneliner

Beau recounts a historical football incident to urge America to learn from its current crisis and enact necessary changes.

# Audience

Americans

# On-the-ground actions from transcript

- Implement changes in response to current political issues (suggested)
- Advocate for safeguards to protect against abuses of power (implied)

# Whats missing in summary

The full transcript provides deeper insights into the historical context of racial tensions and how past events can inform present-day responses.

# Tags

#CivilRights #RacialJustice #PoliticalChange #CommunityAction #America