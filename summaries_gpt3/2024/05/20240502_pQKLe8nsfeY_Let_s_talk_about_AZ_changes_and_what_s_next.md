# Bits

Beau says:

- A court ruling in Arizona reinstated a law from 1864 that essentially banned reproductive rights.
- The Arizona House and Senate have repealed this law, which is expected to be signed by the governor soon.
- Due to a unique structure, the repealed law will still be in effect for a period of time before being replaced by a 15-week ban.
- Most Republicans voted against repealing the law, with only two Republican votes in the Senate and three in the House supporting its repeal.
- There will likely be a ballot initiative in November regarding reproductive rights in Arizona.
- Reproductive rights are expected to be a major issue in the 2024 election.

# Quotes

1. "You are to be ruled, not represented."
2. "Even though this has been resolved, it is still incredibly likely to end up being a major issue and a deciding factor in the 2024 election."

# Oneliner

A court ruling in Arizona reinstated a law banning reproductive rights, but despite its repeal, reproductive rights remain a major issue set to impact the 2024 election.

# Audience

Arizona residents, reproductive rights advocates

# On-the-ground actions from transcript

- Support and get involved in ballot initiatives dealing with reproductive rights in Arizona (implied)

# Whats missing in summary

The emotional impact on individuals affected by the reinstated law and the importance of continued advocacy for reproductive rights beyond legislative changes.

# Tags

#Arizona #ReproductiveRights #Governor #Election2024 #BallotInitiative