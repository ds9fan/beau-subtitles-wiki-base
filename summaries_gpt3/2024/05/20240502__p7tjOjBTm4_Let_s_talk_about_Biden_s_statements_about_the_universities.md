# Bits

Beau says:

- President Biden's short and honest responses to questions regarding protests and the National Guard.
- Biden's response indicated that protests wouldn't change his decision-making in foreign policy.
- The question about National Guard intervention was met with a straightforward "no" from Biden.
- The ability to call out the National Guard lies with the governors, not solely the president.
- Beau suggests that immediate federal intervention may not be the most suitable response to protests.
- Using excessive force like a security clampdown might exacerbate tensions in protest situations.
- Handling demonstrations through establishment means is emphasized.
- Beau advises against interpreting Biden's statements as dismissive, rather as honest.
- The role of governors in calling out the National Guard is underscored.
- Beau concludes by urging viewers not to assume Biden's stance precludes National Guard intervention.

# Quotes

1. "That's the entirety of the answer."
2. "That should not be a goal of them because that's not going to occur."
3. "That is not what that means."
4. "They're honest."
5. "Do not take his statements to be dismissive."

# Oneliner

Beau dissects President Biden's concise responses on protests and National Guard intervention, stressing honesty and the governors' role in handling such situations.

# Audience

Activists, concerned citizens

# On-the-ground actions from transcript

- Contact local governors to understand their stance on National Guard intervention (implied).
- Join community-led efforts to address protests and advocate for peaceful resolutions (generated).

# Whats missing in summary

The nuances of Beau's analysis and commentary on the political responses to protests and potential National Guard involvement.

# Tags

#PresidentBiden #Protests #NationalGuard #Honesty #Governors