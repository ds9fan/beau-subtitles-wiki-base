# Bits

Beau says:

- Examines Trump, New York, Hicks, and strategy.
- Strategy seen before that diminishes Trump's claims.
- Hicks' statements viewed as damaging to Trump.
- Trump's go-to claim of "Democratic witch hunt."
- Strategy used during January 6 hearings.
- Most damaging statements not from political rivals but from supporters.
- Undercutting Trump's main narrative.
- Strategy likely to play out in future entanglements.
- Most critical information coming from people on Trump's side.
- Expect more revelations from Trump's supporters.

# Quotes

1. "It's hard to say that when the most critical pieces of information are coming from people who, well, I mean, they're partisan, but they're on your side."
2. "You know, you've already seen friends and people that worked with him for years, and you can expect to see more of that."
3. "Get used to that surprise because I think we're going to see it a lot."
4. "It's manufactured, that it's a political witch hunt."
5. "It'll only play within that base who, honestly, short of Trump walking out and telling them to believe that it was true, nothing would convince them."

# Oneliner

Trump's strategy of claiming a "Democratic witch hunt" is being undercut as damaging revelations come from his own supporters, not political rivals.

# Audience

Political analysts, activists, voters

# On-the-ground actions from transcript

- Analyze and understand the strategy being used by political figures (implied)
- Stay informed about developments in political entanglements (implied)
- Be prepared for potential surprises in future revelations (implied)

# Whats missing in summary

Analysis on the potential impact of these damaging revelations on Trump's political standing and future strategies.

# Tags

#Trump #Strategy #PoliticalAnalysis #Supporters #Revelations