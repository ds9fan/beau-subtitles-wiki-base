# Bits

Beau says:

- Explains his previous reference to Marjorie Taylor Greene as the "space laser lady" and why he started using her name.
- Discovers an image of an amendment introduced by Marjorie Taylor Greene to divert funding for space lasers in the Israeli aid package.
- Mentions other amendments introduced by her, including diverting funding for recovery from a fire in Hawaii and a conspiracy theory about laboratories.
- Reads a bizarre amendment requiring members of Congress to conscript in the Ukrainian military if they vote in favor.
- Comments on the shocking nature of these amendments being part of the congressional record.
- Speculates on the social media attention Marjorie Taylor Greene may be seeking through these amendments.

# Quotes

1. "Is this real?"
2. "Marjorie Taylor Greene introduced an amendment to the Israeli aid package to divert funding as needed to develop space lasers."
3. "But wait, there's more."
4. "These are now part of the congressional record."
5. "It is both totally unsurprising and completely shocking at the same time."

# Oneliner

Beau reveals shocking amendments by Marjorie Taylor Greene, including diverting funds for space lasers and a bizarre conscription requirement, now part of the congressional record, questioning the social media attention sought through these actions.

# Audience

Those following political developments.

# On-the-ground actions from transcript

- Contact your representatives to express concerns about these concerning amendments (suggested).
- Stay informed about legislative actions and hold elected officials accountable (implied).

# Whats missing in summary

The full transcript provides detailed insights into the concerning amendments introduced by Marjorie Taylor Greene and the potential implications on legislative processes.

# Tags

#MarjorieTaylorGreene #Amendments #Congress #Legislation #PoliticalAnalysis