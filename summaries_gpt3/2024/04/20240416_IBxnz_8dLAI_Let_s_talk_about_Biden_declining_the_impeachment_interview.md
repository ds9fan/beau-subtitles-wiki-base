# Bits

Beau says:

- Republicans in the House invited President Biden to talk for their impeachment inquiry, but he declined.
- The committee's impeachment inquiry into Biden has failed to find any evidence of wrongdoing.
- Despite ample evidence supporting Biden's innocence, the committee continues to push false allegations.
- The impeachment inquiry seems to be lacking substance, with no significant findings against Biden.
- Rumors suggest that the inquiry will be referred to the Department of Justice (DOJ), but it's unlikely to lead to any action.
- Referring the inquiry to the DOJ won't change their stance since they likely already have the information.
- The committee may use the lack of action by the DOJ to claim they were thwarted by the "deep state."
- The anticipated outcome is that the inquiry will continue to focus on baseless allegations and conspiracy theories.
- Biden's refusal to participate in the inquiry may lead to further accusations from the committee.
- Ultimately, the committee's actions may be viewed as a political tactic rather than a genuine pursuit of justice.

# Quotes

1. "The facts do not matter to you."
2. "There's no there there."
3. "They will frame it in a very conspiratorial way."
4. "He doesn't want to talk in public or whatever."
5. "It's just a thought."

# Oneliner

Republicans invite Biden to impeachment inquiry, fail to find evidence, likely shift focus to DOJ with little impact, resorting to conspiracy theories in the end.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Stay informed about political developments and misinformation (implied)

# Whats missing in summary

Insight on the potential implications of baseless political tactics and misinformation campaigns.

# Tags

#Impeachment #Politics #DOJ #ConspiracyTheories #Biden