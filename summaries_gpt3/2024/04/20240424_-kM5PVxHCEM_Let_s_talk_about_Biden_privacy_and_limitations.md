# Bits

Beau says:

- The Biden administration introduced a new federal regulation through HIPAA providing some shield for individuals seeking reproductive health care in another state.
- The rule aims to prevent medical records from being used against individuals seeking lawful reproductive health care.
- However, there are limitations to the rule, such as not covering individuals receiving mailed reproductive health care from another state.
- The rule may not provide a total shield as it could still be accessible to investigators with additional steps.
- About 20 attorneys general from Republican states opposed the rule when it was proposed and may challenge it in court.
- Beau advises not to rely solely on headlines about the rule and recommends consulting an attorney for accurate information.
- The rule's coverage may not be as all-encompassing as portrayed in the media.
- Beau suggests being cautious about assuming complete protection under the new rule, as its level of shielding could change over time, especially if challenged by the Republican Party.
- It is prudent to understand the limitations of the rule and be aware that its provisions could be subject to alteration through legal challenges.
- Beau ends by reminding viewers to stay informed and exercise caution regarding state laws and potential changes to the rule.

# Quotes

1. "No one should have their medical records used against them, their doctor or their loved one, just because they sought or received lawful reproductive health care."
2. "I wouldn't rely on this without talking to an attorney."
3. "Be cautious before you go and stick your tongue out at the local state laws."
4. "There are limitations to this and the level of shielding might change as time goes on."
5. "It's just a thought. Y'all have a good day."

# Oneliner

Beau warns about limitations of the Biden administration's new rule on reproductive health care, advising consultation with an attorney due to potential challenges and changes.

# Audience

Policy advocates

# On-the-ground actions from transcript

- Consult an attorney for accurate information on the Biden administration's new rule (suggested).
- Stay informed about the limitations and potential changes to the rule (suggested).

# Whats missing in summary

The full transcript provides detailed insights into the limitations and potential challenges surrounding the Biden administration's new rule on reproductive health care. Viewing the entire video can offer a comprehensive understanding of the nuances not captured in this summary.

# Tags

#BidenAdministration #ReproductiveHealthCare #HIPAA #LegalChallenges #ConsultAttorney