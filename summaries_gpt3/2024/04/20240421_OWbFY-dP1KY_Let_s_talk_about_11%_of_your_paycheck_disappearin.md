# Bits

Beau says:

- Talks about the year 2049 and the surprising proximity of it compared to past expectations of flying cars and jet packs.
- Mentions a study from the Potsdam Institute revealing a projected 11% reduction in median income in the United States over the next 25 years due to climate change.
- Emphasizes that this income reduction will affect almost all countries globally, not just highly developed ones.
- Compares the projected income reduction to a scenario where Congress raises taxes by 11%, indicating the impending economic impact.
- Urges viewers to understand that this study's projections are based on current conditions, not worsening future scenarios.
- Stresses that climate change consequences are transitioning from abstract issues to personal financial impacts on individuals' paychecks.
- Encourages a shift to cleaner energy sources to mitigate the economic effects outlined in the study.

# Quotes

1. "Imagine if your tax rate went up 11%. You probably wouldn't be happy."
2. "This study is based on now. Locked in is the term. It is locked in 11%."
3. "From now on, when you hear a politician say they don't want to do anything about climate change, what you need to hear is they want to raise your taxes by 11%."
4. "There's finally a study that's like, hey, here's how it's going to impact your pocketbook."
5. "Every time you hear a politician push back on doing what needs to be done, they want to raise your taxes by 11 percent."

# Oneliner

Beau warns of a projected 11% median income reduction due to climate change by 2049, urging action towards cleaner energy to avoid personal financial impacts.

# Audience

Climate activists, environmental advocates

# On-the-ground actions from transcript

- Transition to cleaner energy sources to mitigate the economic impacts of climate change (implied).
- Stay informed and advocate for policies that support a sustainable future (implied).

# Whats missing in summary

The full transcript provides in-depth insights into the economic implications of climate change, urging individuals to take proactive steps towards cleaner energy solutions.

# Tags

#ClimateChange #IncomeReduction #CleanEnergy #EnvironmentalAdvocacy #PolicyAdvocacy