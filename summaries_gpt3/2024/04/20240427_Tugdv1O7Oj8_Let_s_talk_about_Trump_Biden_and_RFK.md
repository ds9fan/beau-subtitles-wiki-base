# Bits

Beau says:

- Historically, American presidential choices were limited to candidates like Jack Johnson and John Jackson, offering minimal variation.
- Trump's presidency marked a departure from the norm, showcasing a different style of leadership.
- In 2024, some seem to have forgotten the lessons of 2020 about the dangers of authoritarianism and extreme right-wing ideologies.
- There is a segment of the population seeking alternative leadership options, considering RFK Jr. as a potentially different choice.
- However, the concern arises that new candidates may not truly expand the range of options but instead add more choices within the existing spectrum.
- Trump's criticism of RFK Jr. as a Democrat plant aiming to benefit Biden adds a layer of intrigue to the dynamics.
- The possibility that RFK Jr. may draw more votes from Trump than Biden raises questions about the overall impact on the political landscape.
- Building a wider range of options requires grassroots efforts to create a new political party with a distinct platform.
- Simply running third-party candidates without a comprehensive strategy for long-term change may not lead to the desired outcome.
- To effect real change and introduce new options, individuals must take action and shape the political landscape they desire, rather than waiting for top-down solutions.

# Quotes

- "You have to build the party you want with the platform you want because anybody who is going to try to come in up at the top and run as a third party and actually try to win well they're going to be within that same range."
- "You get more choice but you don't get more options. It's the same range. It's just packaged differently."
- "If you want your platform, if you want more options, if you just want to settle for the options that are going to be presented to you, sure, once every four years will do it."
- "You have to build it. You can't wait for a leader. You have to become one."
- "There is no power structure to support that. You have to build it."

# Oneliner

Building a wider range of political options requires grassroots efforts to create new parties with distinct platforms, rather than settling for existing choices.

# Audience

Political activists and reformers

# On-the-ground actions from transcript

- Build a new political party with a distinct platform (suggested)
- Start grassroots efforts to shape the political landscape (implied)

# Whats missing in summary

The full transcript dives deeper into the necessity of active citizen participation beyond election cycles to effect real change and create a political system that truly represents diverse viewpoints.

# Tags

#PoliticalOptions #GrassrootsEfforts #ThirdPartyCandidates #ActiveCitizenship #PoliticalChange