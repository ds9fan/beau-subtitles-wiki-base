# Bits

Beau says:

- Trump announced his position on family planning and reproductive rights, leaving it up to the states.
- Many in Trump's party wanted a national ban on reproductive rights, but he said it's a state matter.
- Trump claimed to be responsible for ending Roe v. Wade but emphasized that states will have varying laws.
- This decision by Trump is viewed as a betrayal by the pro-life voting block.
- Individuals supporting bodily autonomy and reproductive rights are also unhappy with Trump's stance.
- Politically, this move is seen as the worst Trump could have taken, as it does not satisfy either block.
- Republicans previously used the "up to the states" stance as a tactic, not a genuine position.
- Lindsey Graham and Mike Pence, along with national organizations, have expressed disagreement with Trump's stance.
- This decision may alienate a significant voting bloc that has a long memory.
- Overall, Trump's announcement is perceived as a poor political move that exposes the Republican Party's strategies.

# Quotes

- "He said many states will be different. Many will have a different number of weeks and some will have more conservative than others."
- "There's not a worse position because you don't get either of the blocks."
- "Now you've made us look silly."
- "That was his big announcement that he teased."
- "I don't think it went the way he hoped."

# Oneliner

Trump's stance on family planning and reproductive rights, leaving it up to the states, angers both pro-life and pro-choice groups, exposing political tactics and risking alienation of key voting blocs.

# Audience

Politically active individuals

# On-the-ground actions from transcript

- Contact national organizations advocating for reproductive rights and bodily autonomy (exemplified)
- Join or support local grassroots movements promoting reproductive rights (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's decision on family planning and reproductive rights, including its potential impact on different voter blocs.

# Tags

#Trump #ReproductiveRights #PoliticalAnalysis #ProLife #ProChoice