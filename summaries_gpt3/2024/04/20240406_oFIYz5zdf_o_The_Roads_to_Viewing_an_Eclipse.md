# Bits

Beau says:

- Solar eclipses have mesmerized humans for ages, evident from ancient structures made for observing the sky by our ancestors.
- These celestial events were significant historically, symbolizing favor with rulers or the end of dynasties.
- Eclipses are not mystical; they are simply one astronomical body passing in front of another.
- A solar eclipse occurs when the moon blocks the sun, while a lunar eclipse happens when the Earth obstructs the moon's light.
- About five solar eclipses are visible annually on Earth, with most being unnoticed due to the Earth's water-covered surface.
- Total solar eclipses occur roughly every 18 months, but they are rare and reoccur in the same place only about every 400 years.
- Looking directly at the sun can cause permanent eye damage, so it's vital to observe safety precautions.
- To safely view a solar eclipse, inexpensive eclipse viewing glasses are recommended, but a DIY pinhole projector can be made using a shoebox.
- Materials needed for the pinhole projector include a shoebox, white paper, aluminum foil, tape, a paper clip, a needle or tack, knife, and scissors.
- Steps for creating the pinhole projector involve cutting holes in the shoebox, attaching aluminum foil, and positioning white paper for viewing the eclipse.
- The projector allows you to view the eclipse safely by standing with your back to the sun and looking through the viewing hole with your right eye.
- Beau advises caution when purchasing eclipse viewing glasses to avoid counterfeit products.
- He stresses the importance of accurate information and preparation for observing celestial events like solar eclipses.

# Quotes

1. "Solar eclipses have captivated humans probably since before there was a spoken language."
2. "Observing a solar eclipse is easy to do when you're prepared."
3. "Please remember that if you're going to get these glasses, make sure that they're not fake."
4. "A little bit more information, a little more context and having the right information will make all the difference."
5. "Enjoy the show."

# Oneliner

Beau introduces the history of solar eclipses, safety precautions, DIY projector creation, and the significance of accurate information in observing celestial events.

# Audience

Astronomy enthusiasts

# On-the-ground actions from transcript

- Make a DIY pinhole projector to safely view solar eclipses (suggested).
- Purchase reliable eclipse viewing glasses for safe observation (suggested).

# Whats missing in summary

The detailed steps and materials required to create a DIY pinhole projector are best understood by watching the full video tutorial.

# Tags

#SolarEclipse #Astronomy #DIYProjector #SafetyPrecautions #AncientStructures