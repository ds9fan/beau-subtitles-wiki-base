# Bits

Beau says:

- Received messages from college students questioning the effectiveness of protests and demonstrations.
- Students express concerns about the lack of immediate impact and motivation to continue participating.
- Beau addresses the importance of remaining hopeful and staying in the fight to influence foreign policy.
- Talks about the significance of performative actions in carrying ideas forward, despite some being superficial.
- Emphasizes that influencing foreign policy is a long, tedious process with high stakes and often involves losing most of the time.
- Warns about the consequences, both intended and unintended, of attempting to influence foreign policy.
- Urges individuals to continue striving for change, even though it is challenging and may result in frequent losses.
- Notes the demoralizing effect of constant messages that nothing will change, and the detrimental impact of adopting a cynical attitude.
- Advises against using rhetoric that undermines motivation and reinforces a belief that change is impossible.
- Concludes by stressing the importance of persisting in efforts to influence foreign policy for the greater good.

# Quotes

1. "Don't expect immediate effects. How do you stay hopeful? Well, I mean, the easiest way is to remember that you don't have a choice."
   
2. "All of these messages, the goal is to save lives. The goal is good."

3. "It's worth doing. If your goal is the preservation of life and that's really what it's about for you."

# Oneliner

College students express doubts about the impact of protests, while Beau encourages persistence in influencing foreign policy for positive change despite frequent setbacks.

# Audience

College students, activists

# On-the-ground actions from transcript

- Reach out to representatives and actively participate in efforts to influence foreign policy (implied).
- Stay informed about global issues and understand the long-term nature of effecting change in foreign policy (implied).
- Persist in advocating for causes that aim to save lives, despite facing challenges and setbacks (implied).

# Whats missing in summary

Beau's insightful commentary on the importance of maintaining hope and continuing efforts to influence foreign policy in the face of adversity.

# Tags

#Activism #Influence #ForeignPolicy #Change #Hope