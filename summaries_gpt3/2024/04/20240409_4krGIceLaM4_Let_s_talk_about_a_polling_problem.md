# Bits

Beau says:

- Beau introduces the topic of polling and his history with discussing polling accuracy.
- He mentions how polling was incredibly accurate before 2020, even when the other person won within the margin of error.
- In 2020, Beau started discussing unlikely voters and how they were affecting polling accuracy, especially due to younger people becoming more politically engaged.
- There was also a public health issue affecting polling demographics in favor of Biden.
- By 2022, the issue with unlikely voters became broader, indicating that polling was not capturing the right demographics.
- Beau notes that recent polling seems off, with strange results like Biden leading among seniors and Trump leading among Gen Z, which seems unlikely.
- He expresses skepticism about the accuracy of current polling and mentions that reporting on polling may not be worth the time.
- Beau hopes that pollsters can fix the errors causing inaccuracies but admits he has no clue about the root cause this time.
- He warns about potential bad actors manipulating polling to influence voting turnout.
- Ultimately, Beau concludes that the only poll that truly matters is the one in November.

# Quotes

1. "The only real answer is a non-response bias, but we've talked about that on the channel before too. That's not a good answer."
2. "The only poll that is going to matter is the one you go to in November."
3. "So maybe they'll be able to fix it."
4. "I don't think it's a good idea to do that."
5. "Y'all have a good day."

# Oneliner

Beau breaks down the evolution of polling accuracy, casting doubt on current polls and pointing to the importance of the final election day poll.

# Audience

Political analysts, voters

# On-the-ground actions from transcript

- Be cautious of potential manipulation in polling data by bad actors (implied)
- The most impactful poll is the one you participate in during November (implied)

# Whats missing in summary

Insight into the potential consequences of inaccurate polling on voter behavior and election outcomes.

# Tags

#Polling #Accuracy #Election #Politics #Voters