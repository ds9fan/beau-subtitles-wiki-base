# Bits

Beau says:

- Johnson, the Speaker of the House, pushed through an aid package, sidelining Trump's biggest supporters in the House.
- Trump wasn't super in favor of aid to Ukraine and dislikes anything that hurts Russia.
- Johnson refused to get rid of FISA as ordered by Trump over Twitter.
- Trump's normal move when someone disobeys him is to come out saying mean things, but in this case, he called Johnson a good person.
- Republicans in Congress don't actually like Trump; even those who support him now disliked him back in 2016.
- Republicans in Congress fed Trump's ego to tap into his voting base, despite not liking him.
- They played into Trump's ego and worst instincts to secure their positions.
- In 2020, Republicans supported Trump's claims of fake news and other behaviors to stay in his good graces for re-election.
- Trump's relationships were transactional; loyalty was not genuine.
- Johnson outplayed Trump in a public fashion, with limited outcry from the House.
- Trump seems powerless to act against Johnson, with one of his biggest supporters leading the charge to oust Johnson.

# Quotes

- "It seems like a strange dynamic but it's not."
- "They played into Trump's ego and worst instincts to secure their positions."
- "You don't have outcry, not widespread, about what Johnson did. He repeatedly defied or undermined Trump."

# Oneliner

Johnson strategically outplayed Trump, revealing the transactional nature of Republican support and the lack of genuine loyalty.

# Audience

Political strategists

# On-the-ground actions from transcript

- Analyze political dynamics (implied)
- Monitor political strategies (implied)

# Whats missing in summary

Full context and depth of insights from Beau's analysis.

# Tags

#Trump #Republicans #PoliticalStrategy #loyalty #transactionalRelationships