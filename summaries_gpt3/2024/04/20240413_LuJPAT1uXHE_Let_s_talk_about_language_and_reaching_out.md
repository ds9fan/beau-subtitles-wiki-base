# Bits

Beau says:

- Talks about the importance of words, definitions, and connotations in communication.
- Mentions using practical tidbits of how to reach out to people using specific words.
- Recaps receiving a message discussing avoiding polarizing terms to keep people engaged during a video.
- Shares an example of avoiding the term "pandemic" to prevent shutdowns and cognitive dissonance in viewers.
- Explains the concept of avoiding certain words to reach a broader audience effectively.
- References Jonathan Swift's quote about reasoning people out of positions they didn't reason themselves into.
- Emphasizes the importance of using non-polarizing terms in critical topics like foreign policy and climate for effective communication.
- Mentions the significance of reaching the 10 to 15 percent of people who can be influenced by avoiding polarizing terms.
- Encourages using terms untainted by typical political discourse to have rational, productive conversations.
- Concludes by discussing the possibility of changing opinions through rational dialogues and the importance of careful communication in difficult situations.

# Quotes

1. "Reasoning will never make a man correct an ill opinion, which by reasoning he never acquired."
2. "Ten to 15 percent of people that are up for grabs."
3. "You can't reason somebody out of a position they didn't reason themselves into."
4. "Every hostage negotiator, everybody who has ever dealt with a DV situation that turned into a barricade situation, they know that's wrong."
5. "It's just a thought. Y'all have a good day."

# Oneliner

Beau shares insights on using non-polarizing terms to effectively communicate and potentially change opinions through rational dialogues, especially in critical topics like foreign policy and climate.

# Audience

Communicators, influencers, activists.

# On-the-ground actions from transcript

- Choose non-polarizing terms in your daily communications (suggested).
- Engage in rational dialogues with others using carefully selected words (implied).

# Whats missing in summary

In-depth examples and personal anecdotes shared by Beau.

# Tags

#Communication #Words #ReachOut #Opinions #Change #Perspective