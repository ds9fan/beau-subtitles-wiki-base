# Bits

Beau says:

- Former President Trump's actions led to the overturning of Roe v. Wade, setting off a chain of events in Arizona.
- The Supreme Court in Arizona upheld a law from 1864 that prohibits abortion, despite updates in 1901 and 1913.
- The current Attorney General in Arizona expressed reluctance to enforce the law, stating it's a low priority.
- Biden won Arizona in 2020, and it's unlikely that a policy shift could turn Biden voters into Trump supporters.
- Beau predicts that this decision on the 160-year-old law will boost support for Biden in Arizona significantly.
- He argues that politicians often create false divisions on issues that most Americans agree upon, like bodily autonomy.
- The impact of this decision could be detrimental to the Republican Party in Arizona, potentially causing irreparable damage.
- Beau advises individuals affected by the law to seek legal counsel and make their views known through voting.
- He warns that while the current Attorney General may not prosecute under the law, others might not follow suit.
- Beau underscores the importance of being cautious and informed about the implications of the state Supreme Court's ruling.

# Quotes

- "Politicians please stop saying it like that. You don't have the resources to do this."
- "The overwhelming majority of Americans shares one opinion on this, and that is that bodily autonomy is a thing."
- "Not all will."
- "I think there's a vote about it."
- "This just gave Biden a huge boost in Arizona."

# Oneliner

Former President Trump's actions led to a chain of events in Arizona as its Supreme Court upholds a 160-year-old law, potentially boosting support for Biden.

# Audience

Voters in Arizona

# On-the-ground actions from transcript

- Make your views known through voting in November (implied)

# Whats missing in summary

The full transcript provides additional context on the historical and political implications of the Arizona Supreme Court's decision and the potential consequences for women's rights and the political landscape in the state.

# Tags

#Arizona #SupremeCourt #AbortionLaw #Biden #Trump