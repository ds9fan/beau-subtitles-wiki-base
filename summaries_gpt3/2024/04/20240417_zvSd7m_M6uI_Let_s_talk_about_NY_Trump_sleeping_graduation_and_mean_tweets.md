# Bits

Beau says:

- Recap of events in New York involving the former president's entanglement and recent developments.
- Former president believed the judge wouldn't allow him to attend a family function, sparking outrage.
- Judge did not rule against former president attending, but mentioned trial time as a factor.
- Actual proceedings have seven jurors, aiming for 18, not halfway there yet.
- Possibility of trial starting on Monday is hopeful but ambitious.
- Former president reportedly fell asleep in court for the second time, causing upset.
- Hearing on gag order violation fines set for the 23rd or 24th.
- Bragg seeking $1,000 per violation as a fine, with potential for jail time up to 30 days.
- Proceedings adjourned till Thursday, to resume juror selection process.
- Former president likely to generate future outrage based on perceived events without full context.

# Quotes

1. "Two minutes of hate designed to keep you easy to manipulate, easy to control."
2. "It might be a good idea to find out what was actually said before just getting very, very angry."
3. "Old Dozing Don reportedly fell asleep for the second time in two days in court."
4. "He seems to be very upset about that and very bothered by the reporting of that."
5. "All of that is based on accepting that what Trump believed was true."

# Oneliner

Former president's perceived court restrictions spark outrage, but details show a different story; trial proceedings and potential penalties unfold amid ongoing drama.

# Audience

Courtroom spectators

# On-the-ground actions from transcript

- Attend court proceedings to observe and stay informed (exemplified)
- Stay updated on trial developments and outcomes (implied)

# Whats missing in summary

Details on the potential impact of the ongoing trial proceedings and the significance of staying informed on the actual events.

# Tags

#NewYork #FormerPresident #CourtProceedings #GagOrder #JurorSelection