# Bits

Beau says:

- Every year, the American Library Association releases a list of the 10 most challenged or banned books.
- In the previous year, there were 1,247 attempts to ban or challenge books, targeting 4,240 titles.
- The fact that there were more titles than attempts to ban them suggests organized efforts rather than random occurrences.
- Beau questions what books should be banned if we were a functioning society, suggesting they should focus on serious topics like war and famine.
- However, the actual most challenged books mainly revolve around LGBTQ+ themes.
- Despite the framing, the issue isn't about shielding children but rather about suppressing LGBTQ+ representation.
- Beau criticizes the consistent theme of targeting LGBTQ+ representation in these lists.
- He points out that books depicting "man's inhumanity to man" never make the most challenged list.
- Beau sees this targeting as a means to marginalize a group and prevent their representation.
- The motivation behind this censorship seems to be about maintaining power dynamics and having someone to look down upon.

# Quotes

- "It's always about going after people who are just finally starting to get some real representation and trying to stop that."
- "If your entire basis for saying that you're better is based on the idea that you are intentionally keeping somebody else down, maybe you're not."

# Oneliner

Every year, organized efforts target LGBTQ+ representation in the most challenged book list, revealing deeper motives than shielding children.

# Audience

Readers, activists, educators

# On-the-ground actions from transcript

- Support LGBTQ+ representation in literature by recommending and sharing books that provide authentic and diverse perspectives (implied).

# Whats missing in summary

The full transcript provides a deeper understanding of the systemic challenges faced by LGBTQ+ representation in literature.

# Tags

#Censorship #LGBTQ+ #BookBanning #Representation #Activism