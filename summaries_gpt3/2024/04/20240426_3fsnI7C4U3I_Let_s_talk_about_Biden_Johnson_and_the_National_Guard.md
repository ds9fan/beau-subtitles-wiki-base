# Bits

Beau says:
- Speaker Johnson and other Republicans want Biden to send National Guard to colleges.
- Mentions May 4th, 1970, Ohio event to caution against the idea.
- Governors, not presidents, should request National Guard deployment.
- White House defers to governors for such requests.
- Signing off on a deployment request is usually a formality.
- Congress's request goes against states' rights principles.
- Military force at demonstrations can escalate unrest instead of quelling it.
- Using force often leads to demonstrations spreading.
- References Trump's mistakes in the Pacific Northwest.
- Caution against wishing for forceful suppression of demonstrations due to potential harm.

# Quotes

- "Governors, not presidents, should request National Guard deployment."
- "Using force often leads to demonstrations spreading."
- "Caution against wishing for forceful suppression of demonstrations."

# Oneliner

Speaker Johnson and Republicans call for National Guard at colleges, but history warns against it; force at demonstrations spreads unrest, not quells it.

# Audience

Activists, protestors, policymakers.

# On-the-ground actions from transcript

- Contact local representatives to voice opposition to deploying National Guard at demonstrations (suggested).
- Educate others on the risks of using military force against demonstrations (implied).
- Support nonviolent methods of protest and conflict resolution in your community (generated).

# Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of deploying the National Guard at demonstrations, drawing on historical events and expert opinions.

# Tags

#NationalGuard #Protest #Demonstrations #StatesRights #CommunityPolicing