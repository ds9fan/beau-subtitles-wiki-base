# Bits

Beau says:

- Explains the recent earthquake in New York leading to various right-wing figures suggesting it as a sign from God.
- Points out the common occurrence of earthquakes before eclipses and the fallacy in connecting them as divine signs.
- Mentions the high frequency of earthquakes globally, around 20,000 per year with an average of 55 per day.
- Notes that even moderate earthquakes, starting at 5.0, happen frequently, around 1000 per year.
- Attributes the phenomenon of connecting natural events to political beliefs to American exceptionalism.
- Criticizes the use of people's religious beliefs to manipulate them for political gain.
- Advocates for individuals to not allow politicians to use their beliefs to manipulate or scare them.
- Emphasizes the importance of distinguishing between personal beliefs and attempts to manipulate those beliefs for political purposes.

# Quotes

- "American exceptionalism leads people to the belief that everything has to do with the United States."
- "Don't allow politicians to use those beliefs to manipulate you, to scare you."
- "Science doesn't back that up."
- "The USA is a country, it's a geographic area on the planet."
- "Not everything that occurs in the world is related to the United States."

# Oneliner

Beau deconstructs the fallacy of connecting natural events to political beliefs rooted in American exceptionalism, cautioning against manipulation through religious beliefs.

# Audience

Critical Thinkers

# On-the-ground actions from transcript

- Question the narratives pushed by politicians and media, especially when they attempt to link natural events to political agendas (implied).
- Educate others about the fallacy of connecting unrelated events as divine signs for political gain (implied).

# Whats missing in summary

Full understanding of Beau's insights and analysis on the manipulation of beliefs for political purposes.

# Tags

#Earthquakes #Eclipses #Politics #AmericanExceptionalism #Manipulation