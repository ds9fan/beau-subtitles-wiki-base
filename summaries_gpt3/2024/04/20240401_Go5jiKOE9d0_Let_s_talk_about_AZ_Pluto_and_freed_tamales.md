# Bits

Beau says:

- Governor Hobbs signed into law Pluto as Arizona's official planet, despite the International Astronomical Union's declaration that Pluto is a dwarf planet.
- The decision to make Pluto the state's official planet was to recognize a significant discovery made at Lowell Observatory in Flagstaff, Arizona.
- Arizona has a lot of official symbols, including a state dinosaur.
- Arizona has updated regulations to allow home cooks to offer tamales requiring refrigeration to the public.
- This change benefits those who provide food at job sites, a common practice in the state.
- The state agency in charge of enforcing regulations was not pleased with the change but won't likely intervene.
- The adjustment prevents grandmothers and aunts from facing hefty fines for sharing food, a cultural tradition at many job sites.

# Quotes

- "Governor Hobbs signed into law Pluto as Arizona's official planet."
- "Arizona has updated regulations to allow home cooks to offer tamales requiring refrigeration to the public."
- "This change prevents grandmothers and aunts from facing fines for sharing food."

# Oneliner

Governor Hobbs makes Pluto Arizona's official planet while Arizona updates regulations to allow home cooks to share tamales, preventing fines for a cherished cultural practice.

# Audience

Arizona residents

# On-the-ground actions from transcript

- Support local home cooks by purchasing their tamales and other food items (implied).
- Advocate for cultural practices like sharing food at job sites by engaging with local officials (implied).

# Whats missing in summary

The full transcript provides more context on the unique official symbols of Arizona and the regulatory changes affecting home cooks and their traditions.

# Tags

#Arizona #Pluto #StateSymbols #Regulations #HomeCooks