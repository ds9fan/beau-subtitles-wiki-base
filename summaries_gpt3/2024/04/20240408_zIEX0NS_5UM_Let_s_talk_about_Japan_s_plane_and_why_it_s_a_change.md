# Bits

Beau says:

- Japan has decided to authorize the export of a fighter plane that they are co-developing with Italy and the UK.
- This decision marks a significant departure from Japan's pacifist stance since 1945.
- China has expressed serious concerns about Japan's move to export arms, urging Japan to respect security concerns and its history of aggression.
- The Japanese government has reportedly decided not to export the fighter plane to Taiwan.
- The futuristic fighter plane is expected to be in service by 2035, with real deployment likely by 2037.
- This shift in Japan's export policy is a major change, as Japan has traditionally refrained from exporting arms.
- Despite China's concerns, other neighboring countries have not vocalized opposition to Japan's decision.
- Japan's move to export arms is a gradual change from their historical stance on military exports.
- The Global Combat Air Program involving Japan, Italy, and the UK is a significant collaboration in developing advanced military technology.
- The export of this fighter plane is not an immediate security concern, given its expected deployment timeline.

# Quotes

- "Japan has decided to authorize the export of a fighter plane that they are co-developing with Italy and the UK."
- "China has expressed serious concerns about Japan's move to export arms, urging Japan to respect security concerns and its history of aggression."
- "The futuristic fighter plane is expected to be in service by 2035, with real deployment likely by 2037."

# Oneliner

Japan's decision to export a fighter plane marks a major departure from its pacifist history, raising concerns from China and signaling a significant shift in its military export policy.

# Audience

Policy analysts, international relations experts

# On-the-ground actions from transcript

- Contact policymakers to advocate for transparency and accountability in Japan's military export decisions (exemplified)
- Stay informed about developments in global military collaborations and their implications (suggested)

# Whats missing in summary

The full transcript provides additional context on Japan's historical pacifist stance and the reactions from neighboring countries, offering a comprehensive understanding of the significance of this shift in military export policy.

# Tags

#Japan #MilitaryExport #GlobalRelations #China #Pacifism