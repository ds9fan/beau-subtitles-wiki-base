# Bits

Beau says:

- Trump wanted to get rid of the FISA program, used for US intelligence gathering, due to personal grievances.
- Most Republicans in Congress ignored Trump's order to eliminate FISA, pushing it through in the House.
- If FISA had been eliminated, it would have significantly benefited foreign intelligence agencies like Russian and Chinese intelligence.
- Despite media portrayal, Trump's political influence within the Republican party seems to be fading, as many Republicans rejected his push to kill FISA.
- FISA was eventually reauthorized for two years, with a potential for reform due to Trump's failure in this instance.
- Trump's failure may have unintentionally helped civil liberties advocates by bringing attention to the need for reform in the FISA program.
- The program is deemed critical for US intelligence but has long-standing issues that require reform.
- Trump's unsuccessful attempt to eliminate FISA could lead to a coalition forming for real reform in the coming years.

# Quotes

1. "Trump wanted FISA gone."
2. "Despite media coverage, Trump is not the political powerhouse that they make him out to be."
3. "Trump failing the way he did here. He might have helped the civil liberties left."
4. "Trump really might have actually succeeded at something here by failing in the way that he did."
5. "Trump's influence within Republicans on Capitol Hill, it's fading in a big way because this was public."

# Oneliner

Trump's failed attempt to eliminate the FISA program may inadvertently lead to much-needed reform in US intelligence, showcasing his diminishing influence within the Republican party.

# Audience

Civil Liberties Advocates

# On-the-ground actions from transcript

- Advocate for real reform in the FISA program (implied)
- Join or support organizations working towards FISA program reform (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's failed attempt to eliminate the FISA program and its potential implications for US intelligence and civil liberties advocacy.

# Tags

#Trump #FISA #USCongress #Intelligence #Reform