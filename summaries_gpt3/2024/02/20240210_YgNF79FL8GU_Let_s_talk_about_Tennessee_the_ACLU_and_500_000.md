# Bits

Beau says:

- Talks about Murfreesboro, Tennessee, as an example of a common theme.
- Mentions the manipulation by politicians to rally support by targeting and demonizing certain groups.
- Recalls a past instance where politicians used fear-mongering tactics to go after drag shows.
- Notes the involvement of the Tennessee Equality Project and the imposition of discriminatory rules to restrict free speech.
- Acknowledges the ACLU's intervention and a federal judge blocking the discriminatory rules.
- Points out the typical scenario where politicians refuse to admit wrongdoing and end up costing the community money in legal battles.
- Contrasts Murfreesboro's response by settling for half a million dollars, acknowledging the unconstitutional nature of the laws.
- Emphasizes the importance of voting for individuals who understand basic American principles and civics.
- Encourages voters to support candidates who embody the spirit of the country rather than those who simply claim to love America.
- Concludes by suggesting that ultimately, it's cheaper to be a good person.

# Quotes

1. "They made something up to vilify your neighbors, to scare you, to manipulate you, to play you, and you fell for it."
2. "Maybe next time you vote, maybe vote for people who understand the basic principles of this country a little bit more."
3. "It's cheaper to be a good person."

# Oneliner

Beau talks about the manipulation of politicians in Murfreesboro, Tennessee, urging voters to support those who understand basic civics and American principles.

# Audience

Voters

# On-the-ground actions from transcript

- Vote for candidates who understand basic American principles and civics (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the manipulation of politicians, the importance of understanding civic principles, and the consequences of falling for fear-mongering tactics.

# Tags

#Murfreesboro #Tennessee #Politics #Voting #Civics