# Bits

Beau says:
- Explains the surprising outcome of the South Carolina Democratic primary, where Biden won by a landslide.
- Contrasts the commentary with the actual results, pointing out that Biden's success was unexpected based on commentary about key demographics.
- Clarifies that leftist commentary criticizing Biden did not impact the primary results significantly.
- Notes that South Carolina is not a stronghold of leftism and leftists generally aren't heavily involved in primaries.
- Points out that Arab Americans, another demographic upset with Biden, did not have a significant impact on the primary due to low turnout.
- Emphasizes that liberals, not leftists, showed strong support for Biden in the primary.
- Predicts that future Democratic primaries in more left-leaning states may have different outcomes.
- Advises not to be misled by overrepresentation of leftist commentary compared to their actual percentage within the Democratic coalition.
- Stresses the importance of Biden retaining support from leftists and Arab Americans for the general election against Republican nominees like Haley or Trump.

# Quotes

- "Why did Biden do so well? Because it was a primary of liberals."
- "Liberals are united behind Biden."
- "The demographics that have problems with Biden, that have policy differences, or in some cases moral differences, they're not really represented in South Carolina."
- "Don't get caught up in the fact that leftist commentary is overrepresented."
- "Biden is going to need the two or three points from the leftists and the two or three points from the Arab Americans."

# Oneliner

Beau explains the surprising success of Biden in the South Carolina Democratic primary, attributing it to liberal support rather than leftist commentary.

# Audience

Democratic voters

# On-the-ground actions from transcript

- Organize efforts to involve leftists in primary elections (exemplified)
- Ensure support for Democratic candidates from diverse demographics for general elections (exemplified)

# Whats missing in summary

Insight into the potential impact of different demographics on future Democratic primaries.

# Tags

#SouthCarolina #DemocraticPrimary #Biden #LiberalSupport #LeftistCommentary