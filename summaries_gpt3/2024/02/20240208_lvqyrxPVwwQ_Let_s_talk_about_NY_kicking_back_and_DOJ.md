# Bits

Beau says:

- Federal government targets New York City Housing Authority in a bribery takedown.
- Between 65 and 70 people have been arrested in connection with the investigation.
- Allegations suggest a pay-to-play, kickback scheme within the Housing Authority.
- Scheme involves exploiting a loophole in no-bid contracts under $10,000.
- Potential charges include bribery and extortion.
- More revelations expected as cases progress.
- DOJ's actions likely a message against illegal practices.
- Emphasis on the illegality of pay-to-play and kickbacks.
- Possibility of further investigations and charges.
- Department of Justice intends to remind city employees of the legal consequences.

# Quotes

1. "DOJ has engaged in what is being referred to as the largest bribery takedown in their history."
2. "It appears that the way this functioned was basically it was a pay-to-play scheme."
3. "When something is as widespread as the feds are alleging here, there's probably more."
4. "Understand it's illegal and doing it with this many people was a way of generating headlines."
5. "This is probably going to expand into a much larger thing."

# Oneliner

Federal government targets New York City Housing Authority in a massive bribery takedown, exposing a pay-to-play scheme and sending a strong message against illegal practices.

# Audience

City residents, government employees.

# On-the-ground actions from transcript

- Report any suspicions of corruption to relevant authorities (implied).
- Stay informed about legal practices and regulations within your community (implied).

# Whats missing in summary

The full context and detailed implications of the corruption scandal. 

# Tags

#NewYork #Corruption #PayToPlay #Bribery #CommunityPolicing