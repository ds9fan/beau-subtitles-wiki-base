# Bits

Beau says:

- Michigan is at the forefront of a potential trend that could mitigate incidents at schools by holding parents accountable for their child accessing firearms.
- After the Parkland shooting, there was a push to raise the age to purchase a rifle from 18 to 21 to ensure parents manage firearm access, but it lost traction.
- A mother in Michigan was found guilty of involuntary manslaughter for allowing access to a firearm used in a school shooting, setting a possible trend.
- Comparisons are drawn to holding adults responsible for providing alcohol to minors who then drive, suggesting this trend is not unprecedented.
- Unsecured firearms granted access by parents could lead to legal consequences, making it vital to secure firearms regardless of individual beliefs about their children.
- There is a legal precedent being set in Michigan that firearms must be secured, with Beau stressing that this should have been standard practice already.
- Beau predicts that this trend in Michigan is likely to spread to other locations, underscoring the importance of securing firearms.
- The importance of securing firearms is emphasized as a general safety measure beyond legal implications and potential consequences.
- Beau concludes by suggesting that this trend of holding parents accountable for firearm access is something to watch as it unfolds and becomes more widespread.

# Quotes

1. "Michigan is at the forefront of a potential trend that could mitigate incidents at schools by holding parents accountable for their child accessing firearms."
2. "This is probably going to be a trend that is going to spread across the country."
3. "Those firearms need to be secured."
4. "There is a legal precedent that is being set that will spread."
5. "It should be something that was already occurring."

# Oneliner

Michigan leads trend in holding parents accountable for child's firearm access, setting legal precedent for firearm security spreading nationwide.

# Audience

Parents, community members

# On-the-ground actions from transcript

- Ensure firearms are securely stored and inaccessible to children (implied)
- Advocate for responsible firearm storage in your community (implied)

# Whats missing in summary

The emotional impact and urgency of addressing the issue of firearm access and accountability in light of school incidents. 

# Tags

#Michigan #FirearmSafety #ParentalAccountability #SchoolSafety #LegalPrecedent