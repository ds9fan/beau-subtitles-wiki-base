# Bits

Beau says:

- Mentioning a chain of events possibly connected to developments in New York involving the fire department.
- The Federal Bureau of Investigation and the city's Department of Investigation are looking into the fire department, specifically focusing on a couple of chiefs.
- Allegations suggest that individuals in the fire department expedited inspections rather than passing failed ones.
- The focus is on the timing of inspections being expedited and not on passing inspections that shouldn't have passed.
- Public integrity standpoint is concerned about potential corruption leading to other illegal activities if money is taken for one thing.
- Allegations indicate a significant sum of hundreds of thousands of dollars being involved.
- No individuals have been accused of any wrongdoing at this point, and investigations are ongoing.
- Recent arrests related to a kickback scheme within the housing authority last week are also part of the larger picture of uncovering corruption.
- Anticipation of more arrests and uncovering of low-level corruption in the upcoming months.
- Expectation of further developments and investigations into perceived corruption in New York.

# Quotes

- "If you're gonna take money for one thing you might take money for something else."
- "As far as the FBI is concerned, that's not going to matter."
- "I don't anticipate this being the end."
- "Generally speaking this stuff kind of comes in waves to send a message."
- "There are probably more developments dealing with perceived corruption, alleged corruption in New York."

# Oneliner

The FBI and city investigators probe alleged expedited inspections in the New York fire department amidst wider corruption concerns.

# Audience

Investigators, concerned citizens

# On-the-ground actions from transcript

- Stay informed about the ongoing investigations and developments in New York (suggested).
- Report any suspicions of corruption or illegal activities to relevant authorities (implied).

# Whats missing in summary

Full context and detailed analysis of the chain of events leading to investigations in the New York fire department.

# Tags

#Corruption #NewYork #FBI #Investigations #FireDepartment