# Bits

Beau says:

- Japan is leading a surprising development in satellite design by considering wood as a material due to the issue of space junk.
- Kyoto University tested Magnolia, Cherry, and Birch for satellite construction, with Magnolia emerging as the best option.
- The satellite, named Lingosat, has been designed and is set to be launched soon to test the performance of wood in space.
- Wooden satellites have the potential to address risks associated with traditional metal satellites during reentry and burning up.
- This innovative idea challenges the conventional metal satellite design portrayed in science fiction movies.
- The Japanese Space Agency is collaborating on this project, and the satellite is expected to launch on a US rocket.
- Wooden satellites may offer unique solutions to long-standing issues related to space debris and satellite disposal.
- The concept of using wood for satellites has roots going back years, with persistent ideas on countering space junk problems.
- Lingosat, the wooden satellite prototype, represents a significant step towards testing and potentially implementing wood satellites.
- The first wooden satellite, similar in size to a coffee cup, could revolutionize satellite design and space sustainability efforts.

# Quotes

1. "Wood satellites could greatly reduce risks from reentry to space junk burning up."
2. "Lingosat, the wooden satellite prototype, will test Magnolia's performance in space."
3. "A surprising development in satellite design - Japan leading with wood satellites."
4. "Challenging the norm of metal satellites with an innovative wooden design."
5. "The idea of wooden satellites challenges traditional metal designs portrayed in sci-fi."

# Oneliner

Japan leads an innovative shift in satellite design by testing wooden satellites, aiming to address space junk issues and revolutionize space sustainability efforts.

# Audience

Space enthusiasts, Environmental advocates

# On-the-ground actions from transcript

- Support research and development of sustainable satellite design by staying informed and spreading awareness (implied)
- Follow updates on the Lingosat mission and advancements in wooden satellite technology (implied)

# Whats missing in summary

Exploration of the potential environmental impact and long-term benefits of wooden satellites compared to traditional metal ones.

# Tags

#SatelliteDesign #SpaceInnovation #WoodenSatellites #SpaceSustainability #JapanSpaceAgency