# Bits

Beau says:
- Beau introduces the topics of solar eclipses and lost civilizations that have been generating many questions.
- Details about the upcoming solar eclipse on April 8, visible from the lower 48 states, a rare event not to miss.
- The next observable total eclipse in the lower 48 will happen in August 2044.
- The path of totality for the eclipse includes states like Texas, Illinois, Ohio, and Maine.
- Safety precautions are emphasized, including using solar glasses or indirect viewing methods.
- Beau mentions the rewriting of history books due to the discovery of a previously unknown civilization, the Pono people in Ecuador.
- The civilization existed for about a thousand years, and its discovery will lead to a significant rewriting of historical narratives.
- This discovery challenges the Eurocentric view of history and opens up a new chapter in understanding pre-Columbian civilizations.
- Beau speculates that many legends and myths might be connected to these newly discovered civilizations in the Amazon.
- He encourages viewers to look into these topics as they offer insights into either rare astronomical events or groundbreaking archaeological discoveries.

# Quotes
- "Get the solar glasses, which understand they're not expensive."
- "This discovery challenges the Eurocentric view of history."
- "It's a whole new chapter is going to have to be written."

# Oneliner
Beau introduces rare solar eclipse details and groundbreaking archaeological discoveries challenging historical narratives, urging safety and curiosity.

# Audience
Astronomy enthusiasts, history buffs

# On-the-ground actions from transcript
- Watch the upcoming solar eclipse safely using solar glasses or indirect viewing methods (suggested).
- Research and learn more about the newly discovered Pono civilization and its implications for historical narratives (suggested).

# Whats missing in summary
The full transcript provides additional context and depth on the upcoming solar eclipse and the discovery of the Pono civilization, offering a comprehensive understanding of these intriguing topics.

# Tags
#SolarEclipse #History #Archaeology #Discoveries #Astronomy