# Bits

Beau says:

- Natalie Klein from the Utah State School Board put out a social media post with an image of a 16-year-old girl, leading to hate and threats towards the girl who had to go into hiding with police protection.
- Klein, supposed to advocate for children's safety, well-being, and privacy, triggered a situation where the girl was believed to be trans due to the post.
- The governor and lieutenant governor mentioned holding Klein accountable, but unless the legislature impeaches her, she must resign or lose at the polls.
- The moral panic surrounding the incident has led people to attack and bully children under the guise of protecting girls in sports.
- Bullying a child and making assumptions about her body is not just about hate but also about causing lasting harm to the child's mental well-being.
- If your actions lead to bullying children online, you are not on the right side of the story.
- The consequences of online bullying can be severe, as seen with the 16-year-old girl needing police protection because of a post aiming to score political points.
- The situation raises questions about the morality and actions of those involved in cyberbullying children.
- The post and comments made assumptions about the girl's identity and body, leaving a lasting impact on her.
- Bullying any child, regardless of their background, is never acceptable and should be condemned.

# Quotes

1. "If your moral panic has you bullying children on the internet, you're not the good guys."
2. "Bullying a child and making assumptions about her body is not just about hate."
3. "A child, a teen girl, wound up needing police protection because of one of these posts."
4. "You thought it was okay to bully a child."
5. "The whole moral panic convinced people to attack and bully children."

# Oneliner

Natalie Klein's social media post led to cyberbullying a 16-year-old girl, showing how moral panic can harm children online.

# Audience

Social media users

# On-the-ground actions from transcript

- Support organizations combatting cyberbullying by raising awareness and educating communities (implied).
- Advocate for policies that protect children from online harassment and bullying (implied).

# Whats missing in summary

The emotional toll on the 16-year-old girl and the need for stronger measures against cyberbullying.

# Tags

#Cyberbullying #ChildSafety #MoralPanic #OnlineHarassment #Utah