# Bits

Beau says:
- Addresses the rumor about a character in an upcoming series from the X-Men franchise.
- Reminds viewers that the X-Men have always been about civil rights and were a metaphor for societal issues.
- Mentions the introduction of Black Panther in 1966 as part of advancing social justice.
- Emphasizes that using the X-Men to support LGBTQ rights is in line with their core values.
- Points out that resistance to these progressive changes in the franchise is akin to being on the wrong side of history.
- Encourages viewers not to be the bigoted characters from the X-Men series but to embrace progress and acceptance.
- Stresses that the X-Men have always been socially conscious and addressing real-world issues.
- Urges those resistant to the rumored non-binary portrayal to reconsider their stance and embrace inclusivity.
- Cites Stan Lee's past comments affirming that the X-Men have always been a platform for promoting civil rights and equality.
- Concludes by suggesting that resistance to positive changes in the franchise may indicate a lack of understanding of its core themes.

# Quotes
1. "The X-Men have always been woke. They've always been about civil rights."
2. "Using X-Men to advance LGBTQ rights is totally on-brand."
3. "You are not being the hero that Stan Lee knew you could be."
4. "The X-Men have always been socially conscious."
5. "Resistance to positive changes may indicate a lack of understanding of its core themes."

# Oneliner
Beau reminds viewers of the X-Men's history of promoting civil rights and encourages embracing inclusivity in the face of resistance to progressive changes.

# Audience
Fans and viewers of the X-Men franchise

# On-the-ground actions from transcript
- Revisit the core themes of the X-Men franchise and advocate for inclusivity (implied)

# Whats missing in summary
The emotional impact and passion Beau brings to defending the inclusive portrayal of characters in the X-Men franchise.

# Tags
#XMen #Inclusivity #CivilRights #SocialJustice #StanLee