# Bits

Beau says:

- Ads from the Montana National Guard showed up in Montana, aiming to recruit based on family inspiration.
- The main ad featured a command sergeant major holding a photo of his grandfather, a US military veteran, with a background photo of troops marching away.
- The troops in the background were wearing helmets associated with the World War II German Army, not the US military.
- The Montana National Guard acknowledged the error and took down the poster, stating it doesn't represent their history or values.
- The poster's removal from distribution was confirmed, but the mistake in including the German Army troops was significant.
- The ad campaign, themed "more than college, spirit of tradition," faced criticism for the oversight.
- The Montana National Guard leadership issued an apology for the inaccurate poster.
- The presence of German Army troops in the background contradicted the intended message of family tradition and military service.
- The mistake likely originated from a lack of understanding or research by those creating the ad.
- The incident serves as a cautionary tale about the importance of accuracy and sensitivity in military advertising.

# Quotes

1. "This poster does not represent our history or values."
2. "It's a spirit of tradition, having those soldiers in the background that was just all bad."
3. "Somewhere in Montana, a sergeant major's boot is being removed from the lower intestine of somebody in the public affairs section."

# Oneliner

Ads by Montana National Guard in Montana featured a major oversight, showing German Army troops in an ad promoting family military tradition.

# Audience

Recruitment officers, marketing teams

# On-the-ground actions from transcript

- Contact Montana National Guard for further clarification on their recruitment strategies (implied)
- Monitor military advertising campaigns for accuracy and cultural sensitivity (generated)

# Whats missing in summary

The full transcript provides a detailed account of an advertising mishap by the Montana National Guard, showcasing the importance of historical accuracy and cultural sensitivity in military recruitment efforts.

# Tags

#Montana #MilitaryRecruitment #AdvertisingMistake #GermanArmy #Accuracy