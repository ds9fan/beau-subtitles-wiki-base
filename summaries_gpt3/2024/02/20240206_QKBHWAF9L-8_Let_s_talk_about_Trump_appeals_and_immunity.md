# Bits

Beau says:

- Explains how Trump's dreams of presidential immunity were crushed by the appeals court decision.
- Trump argued for complete immunity to avoid indictment after leaving office, a claim not supported by history as past presidents have understood themselves to be subject to impeachment and criminal liability.
- The appeals court emphasized that there is no all-encompassing presidential immunity protecting Trump from criminal prosecution.
- Speculates on the next steps, suggesting Trump might take the case to the Supreme Court.
- There is uncertainty whether the Supreme Court will take up the case, with opinions divided on the possibility.
- Some believe the Supreme Court could clarify where immunity ends and liability begins for civil actions.
- Concludes by criticizing the notion of total presidential immunity, implying it is a desire for dictatorial power and undermining the Constitution.

# Quotes

1. "There is no all-encompassing presidential immunity that protects him from criminal prosecution. He made that up. He lied, again, it's not a thing."
2. "Your argument is bad and you should feel bad."
3. "They hate the Constitution and they hate the United States."
4. "President Biden's next step would obviously be to lock all of them up. Because there's nothing anybody can do about it, right?"
5. "If this was to be the case and presidents are totally immune from everything, President Biden's next step..."

# Oneliner

Trump's dreams of presidential immunity were crushed by the appeals court, denying his claim of complete immunity and raising concerns about dictatorial desires.

# Audience

Legal scholars, political analysts, activists

# On-the-ground actions from transcript

- Stay informed about legal proceedings and decisions related to presidential immunity (suggested)
- Advocate for accountability and transparency in governance (implied)
- Support efforts to uphold the rule of law and prevent abuses of power (implied)

# Whats missing in summary

The full transcript provides detailed insights into the legal arguments surrounding presidential immunity and the potential implications for accountability and governance.

# Tags

#PresidentialImmunity #LegalSystem #Trump #SupremeCourt #Accountability