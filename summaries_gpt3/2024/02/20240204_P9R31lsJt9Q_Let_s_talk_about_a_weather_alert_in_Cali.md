# Bits

Beau says:

- California is facing another atmospheric river bringing in a significant storm, with a storm impact level of 4 in some places and level 3 across coastal California.
- The storm is expected to bring in six months worth of rain in the next three days, leading to concerns about flooding and mudslides.
- The peak of the storm is expected on Sunday, starting tonight and lasting at least until Monday.
- Besides heavy rain, there's a high probability of winds reaching 60 to 80 miles per hour, significant snow in the mountains, rough surf, and a risk of water spouts and tornadoes.
- Emergency responders are urging people to prepare now, have their essentials ready, and make evacuation plans as some areas might see evacuations, potentially mandatory.
- It's advised to stay updated on developments, especially if you're in California from San Francisco South, and listen to radio alerts.
- Those providing weather information are notably concerned, indicating the severity of the situation.
- Having an emergency kit ready, an evacuation plan, and considering staying home on Sunday are recommended precautions.
- Tuning in to the radio and following the guidance of first responders and emergency management personnel is vital.

# Quotes

- "Get prepared now. Be prepared, have your stuff together, and have a plan."
- "Follow the advice of the first responders."
- "I know Santa Barbara, that area is level 4."
- "This isn't something I know a whole lot about but what I do know is that the people who feed me information about weather happenings, they're not really people known for panicking and they seem pretty panicked right now."
- "But I would turn on your radio and listen pretty carefully."

# Oneliner

California faces an imminent storm with severe impacts, urging residents to prepare, stay informed, and follow emergency guidance.

# Audience

Californian residents

# On-the-ground actions from transcript

- Prepare an emergency kit and have essentials ready (suggested)
- Make an evacuation plan (suggested)
- Stay updated on developments through radio alerts (suggested)

# Whats missing in summary

Detailed instructions on what to include in an emergency kit and specific evacuation routes and locations.

# Tags

#California #WeatherAlert #StormPreparation #EmergencyKit #EvacuationPlan