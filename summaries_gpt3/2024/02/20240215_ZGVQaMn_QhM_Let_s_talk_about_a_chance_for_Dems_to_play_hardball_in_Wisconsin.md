# Bits

Beau says:

- Explains the situation in Wisconsin with the Republican Party giving up and the Democratic party having a chance to play hardball.
- Talks about how the Republican Party has used gerrymandering to maintain an unfair advantage in Wisconsin.
- Mentions that the courts have ordered new maps to be created due to the unfair advantage created by the Republican Party.
- Points out that consultants deemed the map supported by the Republican Party as unworthy of consideration.
- Notes that the Republican Party has seemingly caved and decided to vote in favor of a map proposed by the Democratic governor.
- Suggests that the Republican Party's acceptance of the new maps is because they are still favorable to them compared to what the courts might decide.
- Expresses a personal opinion of allowing the courts to choose the map rather than accepting the Republican-approved one.
- Raises the possibility of the Democratic Party vetoing the map to allow the courts to select a more favorable option for them.
- Emphasizes that the Democratic Party could potentially gain even better maps through court decisions.
- Encourages a more strategic and assertive approach from the Democratic Party in this situation.

# Quotes

1. "The Republican Party has used gerrymandering to maintain an unfair advantage."
2. "I personally, if it was me, I would allow the courts to decide."
3. "If the Democratic Party wanted to play hardball for once, they could veto it."
4. "The odds are that the courts would choose a map that is even more favorable to the Democratic Party."
5. "It's just a thought, y'all have a good day."

# Oneliner

Beau suggests letting courts decide on new maps in Wisconsin, offering the Democratic Party a chance to play hardball and gain more favorable outcomes.

# Audience

Politically active citizens

# On-the-ground actions from transcript

- Contact Democratic representatives to advocate for a strategic approach to map decisions (suggested)
- Join local political organizations to stay informed and engaged in similar situations (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics in Wisconsin, offering insights into the implications of map decisions on party advantages and urging for a strategic approach.

# Tags

#Wisconsin #Gerrymandering #DemocraticParty #RepublicanParty #CourtDecision