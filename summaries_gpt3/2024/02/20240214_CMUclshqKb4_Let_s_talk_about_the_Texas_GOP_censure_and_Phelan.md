# Bits

Beau says:

- GOP executive committee in Texas voted to censure Dade Phelan, the Speaker of the House, 55 to 4, based on party rules.
- Censure is speculated to be related to Phelan's involvement in the impeachment of Paxton in Texas.
- Speaker's spokesperson accuses Texas state GOP of associating with neo-nazis and losing moral authority.
- In-fighting in the Texas Republican Party is between normal conservatives and ardent authoritarians.
- Republican Party's cohesion has disintegrated into multiple factions with varying ideologies.
- The split within the Republican Party is between conservatives and far-right authoritarians, especially pronounced in red states.
- Politicians in red states have adopted extreme positions, moving away from traditional Republican values.

# Quotes

1. "The executive committee has lost its moral authority and is no longer representative of the views of the party as a whole."
2. "In-fighting within the Texas Republican Party is based on whether or not you're a normal conservative or you're an authoritarian."
3. "The Republican Party of today is not all Republicans. Some of them are far-right authoritarians."
4. "Politicians adopted positions further to the right, and now they're no longer even really Republicans."
5. "There's gonna be more of this in various states."

# Oneliner

GOP in-fighting in Texas reveals a split between conservatives and authoritarians, with red states seeing the most pronounced shifts to the right.

# Audience

Texas Republicans

# On-the-ground actions from transcript

- Contact local Republican groups to understand their stance and influence change (suggested)
- Stay informed about the political climate and shifts within the Republican Party (exemplified)

# Whats missing in summary

The full transcript provides a deeper insight into the internal turmoil within the Texas Republican Party and hints at potential future divisions within other states' GOP.

# Tags

#Texas #RepublicanParty #GOP #In-fighting #Authoritarianism