# Bits

Beau says:

- The governor of Wisconsin signed new maps into law, breaking the Republican Party's hold over the state through gerrymandering.
- The maps are pretty evenly matched, with competitive districts in both the House and Senate for the upcoming November elections.
- Elections in Wisconsin are now entirely about turnout, especially in competitive toss-up districts.
- Representatives will have to truly represent constituents in these competitive districts as they need their votes.
- The decision to have fair maps instead of playing hardball by the governor has made some traditionally leaning blue districts more competitive.
- Justice Protasewicz is credited as the reason Wisconsin is no longer gerrymandered.

# Quotes

1. "It's weird that when you have districts that aren't gerrymandered and you have competitive districts, it turns into a situation where the representatives kind of have to represent you because they actually need your vote."
2. "These elections are now entirely about turnout."
3. "Y'all have a good day."

# Oneliner

The governor of Wisconsin signing new maps into law breaks Republican gerrymandering hold, making upcoming elections about turnout and competitive districts truly representational.

# Audience

Wisconsin voters

# On-the-ground actions from transcript

- Get informed about the competitive districts in Wisconsin and make sure to vote in the upcoming elections (implied).

# Whats missing in summary

The full transcript provides insights into the impact of fair maps on political representation and the importance of voter turnout in upcoming elections.