# Bits

Beau says:

- Updates on developments in New Mexico related to incidents involving the election.
- Incidents involved shootings at homes of people involved in the election.
- Allegations point to a GOP candidate named Solomon Pina, also known as the MAGA king.
- Pina allegedly contracted others to carry out the acts, even participating in some.
- Rounds from the shootings entered the bedroom of a child, fortunately without injuries.
- A person paid to help with the incidents has entered a guilty plea.
- The guilty plea may impact Pina's charges and trial scheduled for June.
- Possibility of Pina reaching an agreement with the prosecution due to the guilty plea.
- Acts of violence stemmed from false claims about the election being rigged.
- Same source of rhetoric inciting real-world violence.

# Quotes

- "Acts of violence that were committed by the allegations because of lies made up about the election."
- "It's the same rhetoric. It is coming from the same sources. It is causing real-world violence."

# Oneliner

Updates on New Mexico incidents involving election-related shootings and GOP candidate Solomon Pina, with a guilty plea impacting charges and trial, stemming from false election rigging claims and real-world violence.

# Audience

Community members in New Mexico.

# On-the-ground actions from transcript

- Contact local authorities to ensure safety measures are in place (implied).
- Stay informed about the case developments and trial dates (implied).

# Whats missing in summary

Full context and detailed analysis of the incidents and their implications.

# Tags

#NewMexico #ElectionViolence #GOPCandidate #FalseClaims #CommunitySafety