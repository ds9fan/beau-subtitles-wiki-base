# Bits

Beau says:

- In Iowa, a temple put up a religious display at the State Capitol to advocate for the separation of church and state.
- The display was from the Satanic temple, in response to displays from other religions.
- Michael Cassidy from Mississippi, a Republican candidate, allegedly destroyed the display and faced misdemeanor charges.
- Charges against Cassidy have been upgraded to a hate crime, as evidence suggests he targeted the display due to the victim's religion.
- The situation may escalate to a trial, with Cassidy potentially using it to boost his political profile.
- The Satanic temple uses such displays to draw attention to religious double standards in the country.

# Quotes

- "The temple decided they were going to put up a religious display too, kind of illustrate the need for a wall, a hedge, if you will, between church and state."
- "And we'll just catch everybody up on it."
- "Evidence shows the defendant made statements indicating he destroyed the property because of the victim's religion."
- "Cassidy did not win this engagement is exactly what the temple looks for."
- "It's worth noting that the whole reason that the temple does stuff like this is to get publicity and draw attention."

# Oneliner

In Iowa and Mississippi, a religious display leads to hate crime charges, exposing religious double standards and advocating for the separation of church and state.

# Audience

Activists, Advocates, Voters

# On-the-ground actions from transcript

- Support organizations advocating for the separation of church and state (implied)
- Stay informed about legal cases involving hate crimes and religious freedoms (implied)

# Whats missing in summary

The full transcript provides more context on the specific events in Iowa and Mississippi, shedding light on the importance of religious freedom and the implications of hate crimes.