# Bits

Beau says:

- Recent developments in Alaska regarding a new illness, Alaska Pox, have sparked curiosity.
- Alaska Pox is similar to smallpox, causing lesions, muscle pain, and swollen lymph nodes.
- Discovered in 2015, there have been seven confirmed infections, with the first fatality recently reported.
- The person who died was elderly and immunocompromised due to cancer treatment, impacting the severity of the illness.
- It is unclear if Alaska Pox can be transmitted from human to human.
- Officials advise staying away from wildlife, as most cases seem to come from animal spillover.
- Washing hands after outdoor activities is recommended to prevent potential transmission.
- The outbreak seems localized around Fairbanks and hasn't spread nationwide.
- Monitoring for new cases is ongoing, despite the relatively low number of reported cases since 2015.

# Quotes

1. "Alaska Pox is similar to smallpox, causing lesions, muscle pain, and swollen lymph nodes."
2. "Stay away from wildlife, wash your hands when coming in from outdoors."
3. "Recent developments in Alaska regarding a new illness, Alaska Pox, have sparked curiosity."

# Oneliner

Recent developments in Alaska have led to the discovery of Alaska Pox, a smallpox-like illness causing concern due to its severity and unknown transmission possibilities.

# Audience

Health-conscious individuals

# On-the-ground actions from transcript

- Stay away from wildlife and wash hands after outdoor activities (implied)
- Monitor for symptoms and seek medical attention if concerned (implied)

# Whats missing in summary

Importance of monitoring symptoms and seeking medical attention for any health concerns related to Alaska Pox.

# Tags

#AlaskaPox #HealthConcerns #Outbreak #Prevention #Monitoring