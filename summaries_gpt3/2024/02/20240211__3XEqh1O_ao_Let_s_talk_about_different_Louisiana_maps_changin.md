# Bits

Beau says:

- Louisiana is facing changes in state house and senate district maps due to violations of the Voting Rights Act.
- A federal judge ruled that the current maps must be changed as they intentionally diluted black voting power.
- New maps need to be created within a reasonable amount of time, likely not before the next election.
- The federal judge did not specify the exact changes needed in the new maps.
- There is an expectation of around six more majority black districts in the House and three more in the Senate.
- These numbers indicate significant gerrymandering to dilute black voting power in Louisiana.
- The ruling is in favor of the plaintiffs, aiming to replace biased maps violating the Voting Rights Act.
- The resolution of this issue is expected to involve appeals and delays, making it a lengthy process.
- Similar cases, which Beau has followed, have taken years to conclude.
- Beau concludes by acknowledging the time-consuming nature of such legal processes.

# Quotes

1. "The federal judge has ruled in favor of getting rid of the bias maps, the maps that are in violation of Section 2 of the Voting Rights Act."
2. "There was a very concerted attempt to dilute black voting power in the state."
3. "This is not going to be resolved quickly."
4. "There was some heavy, heavy, heavy gerrymandering going on."
5. "It's just a thought, y'all have a good day."

# Oneliner

Louisiana faces map changes violating Voting Rights Act, with significant gerrymandering diluting black voting power, leading to lengthy legal process.

# Audience

Louisiana residents, Voting Rights Advocates

# On-the-ground actions from transcript

- Monitor updates on the creation of new state house and senate district maps (implied).
- Stay informed about the ongoing legal process and potential appeals (implied).

# Whats missing in summary

Importance of staying engaged and advocating for fair district maps to ensure equitable representation.

# Tags

#Louisiana #VotingRightsAct #Gerrymandering #DistrictMaps #LegalProcess #Advocacy