# Bits

Beau says:

- Explains the recent developments involving the special counsel appointed by Trump who indicted a person for making false statements due to bias against Biden.
- Recalls discussing the FD-1023 form months ago, which contained unverified information about bribery, and how the Republican Party wanted to use it to mislead their base.
- Shares skepticism about the FD-1023 and expresses hope that the FBI, tired of political manipulation, will release determination memos proving the information false.
- Questions why the FBI had not released the determination memos yet and later discovers they were using them to build a criminal case.
- Points out how the media was aware that the FD-1023 allegations lacked credibility but still treated them as real, leading to a narrative now called into question.
- References an old video detailing a Ukraine-Biden timeline and journalism principles, stressing that despite new allegations, nothing has changed from when the video was first created.
- Emphasizes the need for evidence to change the narrative instead of relying on speculation or rumors.

# Quotes

1. "If you want to change that story, you need evidence, not any window."
2. "Years ago, I did a video, Journalism 101 and a Ukraine-Biden timeline or something like that."
3. "So without this information without these allegations that are now definitely under question, where does the storyline rest?"
4. "They treated it as if it was real."
5. "Y'all have a good day."

# Oneliner

Beau explains recent developments involving biased allegations against Biden, the misleading use of the FD-1023 form, and the importance of evidence to alter the narrative.

# Audience

Journalists, Fact-Checkers

# On-the-ground actions from transcript

- Examine and fact-check information before sharing or reporting (implied).
- Seek evidence-based narratives rather than speculation (implied).

# Whats missing in summary

In-depth analysis and context on the political manipulation of information and the impact on public perception.

# Tags

#Biden #SpecialCounsel #FD1023 #Media #Journalism