# Bits

Beau says:

- Hunter Biden's attorneys discredited the government's evidence of a photo showing a powdery substance, claiming it was sawdust, not coke.
- This mistake in evidence is amusing but not a fatal blow to the case against Hunter.
- Some right-wing commentators have aggressively questioned Hunter's explanation, with some even implying he's lying.
- Certain commentators, including those on Fox, have tried to cast doubt on the sawdust explanation, insinuating it looks more like coke.
- Beau points out the clear resemblance of the substance to sawdust, especially if one has experience in a woodworking shop.
- He suggests paying attention to when commentators use similar tactics of questioning in other situations.
- The incident reveals the lengths certain commentators will go to create doubt, despite their claimed audience being blue-collar workers.
- The situation serves as a reminder to be critical of the questions posed by media figures.
- Beau ends by urging people to be vigilant and aware of manipulation tactics used by commentators.

# Quotes

1. "Does that look like sawdust to you?"
2. "Anyway, it's just a thought."
3. "Y'all have a good day."

# Oneliner

Hunter Biden's attorneys debunked the government's evidence, causing uproar among right-wing commentators who questioned his explanation, revealing manipulation tactics to watch out for.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Pay attention to manipulation tactics used by media figures (suggested)
- Be critical of questions posed by commentators (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of how media figures manipulate narratives and sow doubt, urging viewers to remain vigilant in consuming information.

# Tags

#HunterBiden #MediaManipulation #RightWingCommentators #CriticalThinking #Vigilance