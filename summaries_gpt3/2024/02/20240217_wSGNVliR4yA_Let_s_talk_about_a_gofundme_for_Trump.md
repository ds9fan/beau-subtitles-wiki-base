# Bits

Beau says:

- Former President of the United States faced news from New York, seeking $355 million from him.
- A GoFundMe has been created to assist the former president in covering this amount.
- The GoFundMe had raised nearly $28,000 at the time of filming.
- Beau questions our ability to grasp the significance of large numbers like $355 million.
- To contextualize, he breaks down how long it'd take an average worker earning $15/hour to raise $355 million.
- If that average worker started in 678 BC, working 24/7 without breaks, they still wouldn't have raised the full amount.
- Trump's ability to fundraise over the years despite the staggering amount of $355 million is remarked upon.
- The mentioned $355 million is just a fraction of Trump's total worth, according to allegations.
- Beau expresses his curiosity about the outcome of the GoFundMe and leaves with a closing thought.

# Quotes

1. "We don't know what those numbers mean."
2. "When they get that big, average people, we have no way to visualize or have a concept of what that means, $355 million."
3. "If that person wanted to raise the $355 million dollars, How long would it take them? 23,666,666 hours and change."
4. "678 BC. It's strange to me how somebody who has the resources that Trump does was able to spend eight years or so fundraising the way he has been."
5. "I'm very curious to see how the GoFundMe plays out."

# Oneliner

Former President faces $355 million judgment in NY. GoFundMe set up, but can average people grasp such numbers? Beau breaks it down, leaving us curious about the outcome.

# Audience

Social Media Users

# On-the-ground actions from transcript

- Donate to the GoFundMe for the former president (suggested).

# Whats missing in summary

The full transcript provides a detailed breakdown of the challenges in comprehending large numbers and the context around a GoFundMe for the former president, offering insights into financial scale and fundraising efforts.

# Tags

#Numbers #GoFundMe #Trump #Finance #Curiosity