# Bits

Beau says:

- Addressing a new documentary sparking controversy and concerns.
- Referring to a documentary on Netflix called "Alexander, The Making of a God."
- Comments on the portrayal of Alexander the Great being gay within the first eight minutes of the documentary.
- Expresses surprise over the casting choices and assumptions made in the documentary.
- Asserts that historically, Alexander the Great may have been more accurately depicted as bi rather than gay.
- Points out that during the ancient period in question, individuals weren't labeled as gay because it was a common practice.
- Mentions historians' beliefs regarding Alexander's affection for some of his generals.
- Criticizes modern conservatives for being offended by historical depictions of homosexuality.
- Contemplates the sadness of being unable to watch a documentary without finding something to be upset about.
- Concludes by reflecting on the warped views of individuals concerned about ancient practices and orientations.

# Quotes

1. "Alexander the Great was, well, I mean, he wasn't gay."
2. "I want you to picture how sad it must be and how lonely it must be to be incapable of watching a documentary without finding something to be upset about."
3. "How sad it must be to have your view of the world so warped."
4. "I can't think of many reasons why people would be this concerned about the orientation and practices of people thousands of years ago."

# Oneliner

Beau clarifies historical misconceptions and criticizes modern conservatism's reaction to a documentary on Alexander the Great's sexuality, questioning the obsession with ancient orientations.

# Audience

Viewers, History Enthusiasts

# On-the-ground actions from transcript

- Watch the documentary to form your own opinions and understanding (implied).

# Whats missing in summary

Beau's humorous and insightful commentary on the intersection of historical accuracy, modern perceptions, and societal attitudes.

# Tags

#Documentary #AlexanderTheGreat #HistoricalAccuracy #ModernConservatism #Sexuality