# Bits

Beau says:

- Oregon's Republican Party staged boycotts, preventing lawmaking, which angered voters.
- Measure 113 was passed, disqualifying legislators with ten unexcused absences from running in the next election.
- Republicans changed the interpretation of the measure to avoid disqualification until a subsequent election.
- A six-week boycott by 10 Republican lawmakers led to their disqualification from running.
- The state Supreme Court upheld Measure 113, making the 10 lawmakers ineligible for the next election.
- Incumbents typically have an advantage in elections, but these Republican lawmakers lost their edge.
- The vacant seats will lead to primaries and political chaos due to the Republican Party's refusal to abide by voter expectations.
- Despite voter approval of Measure 113, lawmakers attempted to exempt themselves from its consequences.
- The state Supreme Court's decision ensured that legislators couldn't skip work and keep their jobs.
- Beau criticizes the lawmakers for their hypocrisy in disregarding the working class while expecting special treatment.

# Quotes

- "I cannot show up for work and keep my job. I'm not like you commoners."
- "What the voters intended is what's going to happen."
- "They did not show up for work, unexcused, as they often try to kick down at the working class of this country."
- "Incumbents have an edge, always."
- "I get to do whatever I want."

# Oneliner

Oregon Republicans face consequences for boycotting legislation, losing their edge in upcoming elections due to the state Supreme Court upholding Measure 113.

# Audience

Voters, Activists, Politicians

# On-the-ground actions from transcript

- Contact local representatives to ensure they uphold voter-approved measures (implied).
- Stay informed about local politics and hold elected officials accountable for their actions (implied).
- Participate in upcoming primaries to support candidates who prioritize representing constituents over ruling (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of how Oregon's Republican Party faced repercussions for attempting to circumvent voter-approved measures and the importance of accountability in politics.

# Tags

#Oregon #RepublicanParty #Measure113 #StateSupremeCourt #Voters #Accountability #Primaries