# Bits

Beau says:

- West Virginia proposed legislation criminalizing providing obscene material through libraries.
- Legislation framed as protecting children, but standards of obscenity are subjective.
- Librarians could face up to five years in prison and a $25,000 fine.
- Real goal is to chill the spread of information and keep people ignorant.
- Aim is to make it easier to control the population by limiting access to information.
- Libraries might remove controversial material to avoid legal repercussions.
- Implication that librarians may leave the state if the legislation passes.
- Attack on education raises questions about the rulers' intentions.
- Pushback against the legislation is necessary to protect access to information.
- Legislation threatens academic freedom and intellectual growth.

# Quotes

- "If your political party wants to put librarians in prison for five years, you need a new political party."
- "The real goal is to put a chill on the spread of information. The real goal is to put a chill on libraries."
- "They want you uneducated because that makes you easier to control."
- "What West Virginia needs is more academics leaving."
- "When your rulers, your betters are consistently attacking education, you need to ask yourself why they want your children ignorant."

# Oneliner

West Virginia's proposed legislation aims to criminalize providing obscene material through libraries, ultimately threatening intellectual freedom and education while raising questions about controlling access to information.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Rally support to oppose the legislation (suggested)
- Contact local representatives to voice concerns (implied)

# Whats missing in summary

The full transcript provides detailed insights into the potential consequences of the proposed legislation and the importance of defending intellectual freedom in West Virginia.

# Tags

#WestVirginia #Libraries #Education #Censorship #CommunityAction