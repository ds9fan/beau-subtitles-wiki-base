# Bits

Beau says:

- Reacting to a commercial aired during the Super Bowl that featured imagery of feet being washed, with a Christian message portraying Jesus washing feet.
- Surprised by the pushback from right-wing Christians who were upset by the commercial, particularly for showing a Christian in a subservient role.
- Explains the significance of the foot-washing imagery in the Bible, particularly during the Last Supper, where Jesus washed his disciples' feet.
- Emphasizes that the act of washing feet symbolizes humility, servanthood, forgiveness, and love, which are core teachings of Jesus based on love.
- Criticizes Christians who fail to understand the message of love, humility, and forgiveness that Jesus preached.
- Draws parallels to a similar symbolic act of foot washing by Mr. Rogers in a scene related to desegregation, showcasing service and love.
- Encourages Christians to focus on the actual teachings of Jesus rather than following potentially misguided interpretations from pastors.
- Challenges the notion of portraying Jesus as a capitalist and urges individuals to reexamine the Bible's teachings on wealth and service.
- Concludes by suggesting further exploration of Jesus' teachings and the importance of understanding his message of love and humility.

# Quotes

1. "Jesus was not a person who was advocating for subjugation of people and forcing them to your will."
2. "It's an act of humbling and being a servant and forgiveness and that one element that's always missing, love."
3. "The only message being sent is that there is too much hate and if you're really a Christian, you would humble yourself, forgive, love."
4. "It's just a thaw, y'all have a good day."

# Oneliner

Beau addresses the controversy surrounding a Super Bowl commercial featuring foot-washing imagery, urging a focus on Jesus' teachings of love and humility rather than misinterpretations.

# Audience

Christians, Activists

# On-the-ground actions from transcript

- Read the Bible by yourself, focusing on the actual teachings of Jesus rather than relying solely on a pastor (suggested)
- Reexamine Luke 18:25 and other passages related to wealth and service in the Bible (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the controversy surrounding a Super Bowl commercial, delving into the symbolic significance of foot-washing imagery and urging a deeper understanding of Jesus' teachings on love and humility.

# Tags

#SuperBowl #Controversy #Christianity #JesusTeachings #LoveAndHumility