# Bits

Beau says:

- National Hurricane Center conducting an experiment out of season to make a change and get people ready.
- The Cone of Uncertainty (or Cone of Doom) map used during hurricanes is familiar, but lacking.
- The National Hurricane Center has tons of information but doesn't provide it all to the public to avoid decision paralysis.
- New maps will include additional color-coded information about hurricane and tropical storm conditions further inland.
- Changes in weather patterns mean traditional safety expectations are no longer reliable.
- Providing easily digestible information to the public to assist in decision-making during hurricanes.
- People may resist the change due to the addition of more colors in the maps.
- Importance of embracing useful information to aid decision-making during emergencies.

# Quotes

1. "The National Hurricane Center has tons of information, but they don't provide it because the public can't handle it."
2. "People may resist the change because, you know, there's more colors. I'm sure the National Hurricane Center has gone woke."
3. "Changes in weather patterns mean traditional safety expectations are no longer reliable."

# Oneliner

The National Hurricane Center is introducing new color-coded maps with additional info to help the public navigate changing weather patterns and make informed decisions during hurricanes.

# Audience

Emergency Preparedness Planners

# On-the-ground actions from transcript

- Study and familiarize yourself with the new color-coded maps from the National Hurricane Center (suggested).
- Stay updated on changing weather patterns and safety recommendations in your area (suggested).

# Whats missing in summary

Additional context on the importance of adapting to changing weather patterns and utilizing available resources for enhanced decision-making during emergencies.

# Tags

#NationalHurricaneCenter #Hurricanes #EmergencyPreparedness #WeatherPatterns #DecisionMaking