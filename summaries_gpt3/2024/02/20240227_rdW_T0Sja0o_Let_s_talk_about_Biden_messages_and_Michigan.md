# Bits

Beau says:

- Michigan is organizing an effort to send a message to the Biden administration regarding Gaza policies during the primary.
- The goal is to encourage people to vote uncommitted rather than for Biden to influence change.
- The governor of Michigan is supporting Biden but expects a significant number of uncommitted votes.
- An organization aims for 10,000 uncommitted votes, but Beau believes this won't be enough to impact political strategists.
- In 2020, there were 19,000 uncommitted votes, showing that 10,000 votes may not be sufficient.
- Beau suggests that the public goal of 10,000 votes may be lower than the actual private goal.
- Beau thinks protest votes during the primary are vital and preferable to other forms of protest.
- The numbers during the primary will determine the impact; Beau believes 20,000 votes could make a difference.
- Beau encourages an open, public protest vote during the primary to ensure being heard.
- He ends by urging action during the primary to send a powerful message.

# Quotes

1. "Michigan is organizing an effort to send a message to the Biden administration regarding Gaza policies during the primary."
2. "This is the time to do it, this is the way to do it, during the primary and doing it in a very open public fashion through a protest vote."
3. "If you get the numbers, you'll be heard."

# Oneliner

Michigan's effort to influence Biden's Gaza policies through protest votes during the primary may need more than 10,000 votes to make a significant impact.

# Audience

Michigan residents

# On-the-ground actions from transcript

- Organize and encourage people to participate in protest votes during the primary (suggested)
- Ensure a high turnout of uncommitted votes to send a powerful message (implied)

# Whats missing in summary

The full transcript provides additional context and nuances around the importance of protest votes during the primary and the potential impact of different vote counts.

# Tags

#Michigan #ProtestVotes #BidenAdministration #GazaPolicies #PrimaryElection