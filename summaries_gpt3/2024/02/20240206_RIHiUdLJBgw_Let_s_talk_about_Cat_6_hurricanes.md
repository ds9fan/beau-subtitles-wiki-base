# Bits

Beau says:

- Talks about the potential development of category 6 hurricanes to differentiate and showcase the impact of stronger hurricanes in a warming world.
- Expresses concern that creating a category 6 hurricane will result in more people staying during category 5 hurricanes, leading to more deaths.
- Shares a personal experience of someone not evacuating during a category 4 hurricane and suffering devastating consequences.
- Believes that creating an additional level above category 5 will make category 5 hurricanes seem less powerful and undermine the urgency to evacuate.
- Stresses the importance of understanding that category 5 means immediate evacuation unless incredibly well prepared in a secure location.
- Concludes by expressing strong disapproval of the idea of introducing a category 6 hurricane and warns of the potential horrible consequences.

# Quotes

- "If you alter the current system, people will say, oh, it's only a cat 5, the way people do with a cat 4."
- "People should understand that cap five means get out. No matter what, get out."
- "I think it's wrong. I think it's bad and I think it's going to have horrible consequences if they go forward with it."

# Oneliner

Beau expresses strong opposition to the development of category 6 hurricanes, warning that it may lead to more fatalities during category 5 hurricanes by undermining the urgency to evacuate.

# Audience

Climate activists, policymakers, emergency response teams

# On-the-ground actions from transcript

- Prepare your evacuation plan (implied)
- Ensure you have a secure location to evacuate to during hurricanes (implied)

# Whats missing in summary

Importance of maintaining the current hurricane category system to ensure clear understanding and urgency for evacuation.

# Tags

#Hurricanes #Category6 #Evacuation #ClimateChange #DisasterPreparedness