# Bits

Beau says:

- Beau talks about the trending topic of Executive Order 9066 on social media and the misinformation surrounding it.
- Hundreds of thousands of people were exposed to false information about Executive Order 9066 on Twitter.
- The misinformation claimed that Executive Order 9066 was rewarding "illegals" with $5,000 gift cards, blaming Biden and Democrats for financial struggles.
- Executive Order 9066, signed on February 19th, 1942, led to the internment of Japanese Americans during World War II.
- The spread of misinformation coincided with the anniversary of Executive Order 9066, leading to confusion and false narratives.
- Large social media platforms lacking fact-checking mechanisms contribute to the rapid spread of misinformation.
- Beau advises the audience to fact-check information, especially during the upcoming election season.
- The misinformation surrounding Executive Order 9066 was not new, wasn't signed by Biden, and actually pertained to the internment of American citizens during World War II.
- Beau warns about the prevalence of misinformation and the importance of verifying information found online.
- The need to be vigilant and critical of information shared online, especially during times of heightened political activity.

# Quotes

1. "You're gonna have to fact-check your uncles and fathers and people who believe anything that's on the internet."
2. "Executive Order 9066 is rewarding illegals with $5,000 gift cards."
3. "It's an election season. You're gonna see this more and more and more."
4. "Large social media platforms that do not have fact-checking, that do not really have any cohesive way of avoiding the kind of misinformation."
5. "The executive order that is being referenced, again, it's not new and it certainly wasn't signed by Biden."

# Oneliner

Be ready to fact-check misinformation online, especially during elections, as seen with the false claims about Executive Order 9066.

# Audience

Social media users

# On-the-ground actions from transcript

- Fact-check information before sharing (implied)
- Educate others on the importance of verifying information online (implied)

# Whats missing in summary

Importance of critical information consumption and vigilance against misinformation during sensitive times.

# Tags

#Misinformation #FactChecking #ElectionSeason #ExecutiveOrder9066 #SocialMedia