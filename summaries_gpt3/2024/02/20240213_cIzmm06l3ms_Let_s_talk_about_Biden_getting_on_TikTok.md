# Bits

Beau says:

- Beau dives into the topic of Biden joining TikTok to reach out to younger voters.
- The idea of Biden getting on TikTok may not be as straightforward as it seems.
- TikTok's demographic is primarily people under 30, which may pose challenges for Biden's campaign.
- There are associated risks with Biden producing content for TikTok, including potential backlash and criticism.
- Beau suggests that Biden's campaign may need to involve surrogates who are younger to effectively reach TikTok users.
- The success of Biden's presence on TikTok depends on how authentic and genuine the campaign is in its approach.
- Beau mentions the importance of allowing Biden to be himself rather than trying too hard to fit in with TikTok trends.
- Surrogates may play a significant role in carrying forward Biden's message on TikTok.
- Beau points out the idealistic nature of a wide portion of TikTok's user base, which may scrutinize and question posts from the president.
- The success of Biden's TikTok venture hinges on how well the campaign navigates the platform's dynamics and engages with its audience.

# Quotes

1. "Hello fellow young people, how do you do today?"
2. "It's not the message, it's the messenger."
3. "If they allow Biden to be Biden and be, you know, kind of cool grandpa, it might do all right."
4. "There's a lot there that could go wrong."
5. "It all depends on how the campaign plays it."

# Oneliner

Beau analyzes the potential risks and challenges of Biden joining TikTok to reach younger voters, stressing the importance of authenticity and surrogate involvement.

# Audience

Campaign Strategists, Social Media Managers

# On-the-ground actions from transcript

- Involve younger surrogates in carrying forward the message on TikTok (implied)
- Ensure authenticity and genuineness in content production for TikTok (implied)
- Navigate TikTok's dynamics and audience engagement effectively (implied)

# Whats missing in summary

Insight into the impact of Biden joining TikTok on his outreach to younger voters and the potential consequences of not effectively engaging with TikTok's user base.

# Tags

#Biden #TikTok #YouthVote #SocialMediaMarketing #CampaignStrategy