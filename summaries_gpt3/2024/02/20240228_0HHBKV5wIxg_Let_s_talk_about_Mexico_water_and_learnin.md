# Bits

Beau says:

- Mexico City faces water supply issues, with people resorting to reusing water due to scarcity.
- Concerns arise about a potential "day zero" water crisis in Mexico City.
- Some question the accuracy of officials' statements and warn of a possible day zero within months.
- The hope that Mexico's water crisis will wake up the American right to climate change dangers is discussed.
- Beau explains that those who deny climate change often believe in American exceptionalism.
- He points out that American right figures will spin the crisis to reinforce biases rather than address climate change.
- Beau stresses the need for climate infrastructure improvement and resilience.
- The American right is unlikely to have a wake-up moment until climate change directly impacts Americans.
- Thought leaders in the right-wing community benefit from maintaining the status quo and fear-mongering.
- Beau underscores that the majority of Americans are aware of climate change but action from the government is lacking.
- He calls for faster action and transitions to combat climate change.
- Beau asserts that those resistant to change and content with fear-mongering won't be swayed by awareness campaigns.
- It's time to shift focus from raising awareness to demanding more concrete action on climate change.

# Quotes

1. "We need to move towards resiliency."
2. "Those people who enjoy listening to those who fear monger, they're not going to be reached in the way that you hope."
3. "It's time to move from raising awareness to trying to get more action."

# Oneliner

Mexico City's water crisis won't awaken the American right to climate change; action, not awareness, is needed.

# Audience

Climate activists, advocates

# On-the-ground actions from transcript

- Advocate for government action on climate change (implied)
- Push for faster transitions to combat climate change (implied)

# Whats missing in summary

The full transcript provides additional context on Mexico's water crisis and the challenges in addressing climate change denial within certain political circles.

# Tags

#ClimateChange #MexicoCity #WaterCrisis #Awareness #GovernmentAction