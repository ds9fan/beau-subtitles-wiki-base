# Bits

Beau says:

- Analyzing a law enforcement situation where an acorn prompted two deputies to fire into a patrol car at a handcuffed, unarmed suspect.
- The deputies fired every round in their magazines in quick succession, known as mag dumping.
- Despite firing multiple rounds, the suspect was not hit, but the use of force was deemed inappropriate.
- One deputy resigned, and the other was cleared because the practice of firing if another officer does is common in law enforcement training.
- Beau questions the flawed training that led to firing without visualizing the threat, stressing the potential dangers of such practices.
- He underscores the importance of changing this training policy to prevent unnecessary harm and legal consequences.
- Beau advocates for ensuring officers have a clear visual on a target before using lethal force to prevent tragic outcomes.
- He warns of the risks involved in blindly following the actions of other officers without assessing the situation independently.
- Changing the practice to require officers to see the threat before firing is emphasized as a critical step to saving lives and avoiding legal repercussions.
- Beau urges law enforcement officers to challenge current training methods and advocate for safer, more effective approaches to handling threats.

# Quotes

1. "An acorn hit the deputy and that prompted two deputies to mag dump into a patrol car at a handcuffed unarmed suspect."
2. "If you can't see the threat, you probably shouldn't be firing at it."
3. "It is not smart for a whole bunch of reasons."
4. "If this practice is changed, and you actually have to put eyes on the target, it will save lives."
5. "If it doesn't change, you can end up in jail."

# Oneliner

Law enforcement must rethink training practices to ensure officers visually confirm threats before using lethal force, preventing potential tragedies and legal repercussions.

# Audience

Law enforcement officers

# On-the-ground actions from transcript

- Challenge current training practices and advocate for safer approaches (suggested)
- Initiate dialogues with training officers about the need for visual confirmation of threats before firing (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of a concerning law enforcement incident involving mistaken gunfire and underscores the importance of revising training practices to prioritize visual confirmation of threats.

# Tags

#LawEnforcement #TrainingPractices #UseOfForce #Safety #LegalRepercussions