# Bits

Beau says:

- Comparing Trump and Taylor Swift fundraisers.
- Taylor Swift targeted for encouraging young people to vote.
- Trump supporters raised about $100,000 a day for him.
- Taylor Swift donated $100,000 to the family of a woman killed at the Kansas City Super Bowl Parade.
- Beau questions attacking good people to maintain group identity.
- Encourages finding new peers if needed.

# Quotes

1. "I refuse to believe that this many people in this country are innately bad."
2. "Your group's bad."
3. "I will totally attack somebody for telling people to do their civic duty."
4. "If you find yourself constantly having to hate and attack good people to stay in the good graces of your peers, you need new peers."
5. "It's just a thought, y'all have a good day."

# Oneliner

Beau compares fundraisers, questions attacking good people to fit in groups, and urges finding new peers if needed.

# Audience

Observers of group behavior.

# On-the-ground actions from transcript

- Find new peers (implied).

# Whats missing in summary

The full transcript provides additional context on the comparison between Trump and Taylor Swift fundraisers and the impact of attacking individuals for positive actions.

# Tags

#Trump #TaylorSwift #Fundraisers #GroupBehavior #PeerInfluence