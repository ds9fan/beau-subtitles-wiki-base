# Bits

Beau says:

- Explains the concept of "yes and" in the rules of improv, focusing on agreement and building on ideas.
- Uses an example of feeding the homeless in a city to illustrate how to influence people positively.
- Emphasizes the importance of maintaining a positive and constructive tone to influence others effectively.
- Talks about addressing root causes rather than just providing temporary solutions.
- Encourages moving people further along the same line of thinking rather than opposing them.
- Acknowledges the feelings of hopelessness and frustration in some individuals but offers a way to shift perspectives positively.
- Stresses the need to understand the intentions behind comments that may seem discouraging.
- Urges to continue caring and taking action instead of giving up, especially in progressive causes.
- Reminds that small actions, even if not the ultimate solution, can still make a significant impact.
- Advises using the "yes and" approach in interactions to build on shared beliefs and move the narrative forward positively.

# Quotes

- "Yes, and is really important to this person and the others who may be feeling this way."
- "This can be done, it can be scaled up, and it can do a whole lot of good."
- "If you are seeing those comments and you feel like it's hopeless, I promise you none of those people want you to wash your hands of this and not care anymore."

# Oneliner

Influencing positively through agreement and constructive engagement, even in challenging situations, can shift perspectives and drive collective action towards meaningful change.

# Audience

Progressive activists

# On-the-ground actions from transcript

- Approach interactions with a "yes and" mindset to build on shared beliefs and ideas (suggested).
- Continue caring and taking action, even in the face of discouragement (implied).

# Whats missing in summary

The emotional depth and nuanced approach to influencing others positively through constructive engagement and maintaining hope amidst challenges.

# Tags

#Influence #CommunityEngagement #ProgressiveActivism #PositiveChange #ConstructiveDialogues