# Bits

Beau says:

- Updates on the incident in Baltimore involving a ship striking a bridge are provided, with new information suggesting it was an accident.
- The ship lost power, issued a mayday, leading to traffic being blocked, potentially saving lives.
- Emergency responders are still in search and rescue mode, not just recovery, with some workers unaccounted for.
- At least six workers are missing, with two rescued from the water and one in a trauma center.
- Sonar has identified vehicles that fell into the water, including three passenger vehicles, a cement truck, and an unidentified vehicle.
- Pollution mitigation efforts have begun, with the pollution deemed not significant, although the Coast Guard has closed the waterway into the harbor.
- The incident has sparked a broader national debate on infrastructure, beyond just this specific event in Baltimore.
- Beau stresses that the US is behind in infrastructure, with aging and outdated technologies that need a more forward-thinking approach.
- Strengthening existing infrastructure is necessary, but Beau suggests the US needs to look beyond immediate issues to catch up.
- Beau proposes building out unused capacity at other ports to prevent future supply chain interruptions.

# Quotes

- "It's not just about fixing problems. It should be about stopping the problems from occurring in the future."
- "What we have is not enough. What we have is aging. What we have is outdated technologies."
- "It's thinking beyond the immediate that is going to get somewhere."

# Oneliner

Beau provides updates on the Baltimore incident, stressing the need for a more forward-thinking approach to US infrastructure beyond addressing immediate issues.

# Audience

Policy Makers, Infrastructure Planners

# On-the-ground actions from transcript

- Enhance infrastructure planning to prevent future incidents (implied)
- Advocate for forward-thinking infrastructure development (implied)

# Whats missing in summary

The full transcript provides a comprehensive analysis of the Baltimore incident and prompts a reevaluation of national infrastructure planning and development.

# Tags

#Infrastructure #Baltimore #ForwardThinking #Policy #EmergencyResponse