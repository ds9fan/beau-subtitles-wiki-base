# Bits

Beau says:

- Explains the Public Service Loan Forgiveness Program and its recent developments, with another $5.8 billion impacting 78,000 people.
- Mentions that the program is geared towards teachers, firefighters, and nurses, with average forgiveness around $75,000.
- Notes that the total forgiven debt under this program is now $143 billion for about 4 million individuals.
- Talks about how the original goal was $400 billion before the Supreme Court struck it down, leading to piecemeal forgiveness efforts.
- Addresses the confusion around attributing credit to Biden for the program, clarifying that it was established in 2007.
- Points out that during Biden's administration, around 850,000 people have utilized the program, a significant increase from previous years.

# Quotes

1. "This isn't his program. The stuff that's being used is stuff that has been in existence."
2. "If you are one of the firefighters who is going to benefit from it, yeah, this is huge."
3. "The program is still trying to find something that is a little bit wider in scope for other people."

# Oneliner

Beau explains the Public Service Loan Forgiveness Program, recent developments, and clarifies why credit is attributed to Biden, despite the program existing before his term.

# Audience

Public servants, borrowers

# On-the-ground actions from transcript

- Contact your local representatives to advocate for broader loan forgiveness programs (implied).

# Whats missing in summary

More details on the impact of the loan forgiveness program on various public service professionals.

# Tags

#PublicService #LoanForgiveness #Biden #DebtForgiveness #GovernmentPrograms