# Bits
Beau says:

- Joe Biden is leading in fundraising, with recent reports confirming his significant lead.
- A Trump fundraiser is planned for April, with tickets priced between $250,000 and $814,600, targeting Republican billionaires.
- Despite Trump's efforts, it seems he is not focusing on small donor donations.
- Some of Nikki Haley's donors are now fundraising for Biden after Trump's statement that they are not welcome in the MAGA camp.
- There are Republican super PACs dedicated to defeating Trump, showing stronger fundraising performance than him.
- Biden claimed that 97% of his donors were small donors, but this may only be accurate for a specific period, not overall.

# Quotes
1. "Biden has a huge lead on fundraising."
2. "It's an event for Republican billionaires."
3. "Trump's attempt to push people into leaving Haley for him appears to have backfired."
4. "There are Republican-oriented super PACs committed to defeating Trump."
5. "Biden said 97% of the donors were small donor donations."

# Oneliner
Biden leads in fundraising, Trump targets billionaires, and some of Haley's donors shift to Biden, while Republican super PACs aim to defeat Trump.

# Audience
Political activists

# On-the-ground actions from transcript
- Reach out to local organizations to get involved in fundraising efforts for political candidates (implied)

# Whats missing in summary
Analysis on the impact of fundraising strategies on the upcoming election.

# Tags
#Politics #Fundraising #Trump #Biden #SuperPACs