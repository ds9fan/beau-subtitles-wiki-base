# Bits

Beau says:

- An elderly woman's unexpected billion-dollar gift paid off the tuition for all students at Albert Einstein College of Medicine.
- Ruth Gottesman, a former professor, received a portfolio from her late husband containing stock worth a billion dollars in Berkshire Hathaway.
- Her husband's only instruction was to do what she thought was right, leading her to donate the entire amount to the medical school.
- This donation is the largest ever made to a medical school.
- The annual tuition per student at the school is around $60,000.
- With this generous gift, every student at the medical school will have their tuition paid for perpetuity.
- The heartwarming story goes beyond just the current students; it impacts all future students at the school.
- The act of kindness by Ruth Gottesman demonstrates how one person's decision can make a significant difference in the lives of many.
- This unexpected donation ensures that students at Albert Einstein College of Medicine will have their tuition covered indefinitely.
- The footage circulating on social media captures the moment when students were informed about their tuition being fully paid.

# Quotes

1. "A billion dollars worth. Billion with a B."
2. "It's everybody at the school forever."
3. "All because of an unknown portfolio and stock and a woman deciding to make the world a little bit better."

# Oneliner

An elderly woman's billion-dollar gift ensures free tuition for all students at Albert Einstein College of Medicine, impacting generations to come.

# Audience

Donors and philanthropists

# On-the-ground actions from transcript

- Donate to educational institutions (exemplified)
- Support students with tuition fees (exemplified)

# Whats missing in summary

The emotional impact of the unexpected and generous donation on the students and the community.

# Tags

#Education #Philanthropy #Generosity #MedicalSchool #TuitionAssistance