# Bits

Beau says:

- Explains the situation in New Mexico involving a person named Griffin who was disqualified under the 14th Amendment, Section 3, for their involvement in the January 6th events.
- Mentions the Supreme Court's rejection of Griffin's appeal, resulting in his disqualification remaining in place.
- Clarifies the supposed inconsistency people see between Griffin's case and a previous Colorado case involving Trump.
- Points out that the Supreme Court's decision regarding Griffin is consistent with the Colorado case ruling.
- Raises the possibility of a state being able to disqualify individuals from federal offices, particularly the presidency, based on the phrasing of the Colorado case decision.
- Notes Griffin's request to become Trump's vice president and speculates on Trump's potential response.
- Emphasizes that while many may not agree with the initial ruling, the decision on Griffin remains consistent with it.

# Quotes

1. "People obviously see an inconsistency here, but it's not."
2. "It's inconsistent with the first ruling."
3. "Now, obviously that [...] it's Trump so we don't know."
4. "The key thing here is that the two cases they don't contradict each other."
5. "I know a whole lot of people don't agree with that first ruling, but it was the ruling and this is consistent with it."

# Oneliner

Beau explains the disqualification of Griffin under the 14th Amendment, showing consistency with a previous case involving Trump despite perceived contradictions.

# Audience

Legal enthusiasts, political analysts.

# On-the-ground actions from transcript

- Analyze legal implications of state disqualifications regarding federal offices (suggested).
- Stay informed about constitutional interpretations and court rulings (implied).

# Whats missing in summary

Detailed analysis of legal nuances in the disqualifications and court decisions.

# Tags

#NewMexico #SupremeCourt #14thAmendment #StateDisqualifications #LegalInterpretations