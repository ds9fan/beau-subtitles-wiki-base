# Bits

Beau says:

- Biden's promise in 2021 to have a majority of new cars in the US be electric by 2030.
- Ongoing talks between the Biden administration and the automotive industry on emissions regulations, not a ban on any cars.
- Projections suggest 56% of new cars will be electric between 2030 and 2032, reducing seven billion metric tons of carbon dioxide emissions.
- The new rules will impact various vehicle categories, but no mention of semi trucks.
- Legal challenges are expected, but the rules focus on increasing emission standards, which may help them withstand challenges.
- Automotive industry pushback wanting more time to comply, with the requirement to start by 2027.
- Beau hopeful that Biden will fulfill this promise, even though he was initially skeptical.
- Transition to majority EVs could pave the way for further progress.
- The US's approach is less stringent than European nations, but there is more room for legal challenges.
- Overall, Beau sees this as a positive step in the right direction.

# Quotes

1. "If the projections are right, he will fulfill a promise that I was hopeful for."
2. "My understanding is that they have to start putting it together by 2027."
3. "It's not as strict as a lot of European nations, but there's more ability to take the administration to court here."

# Oneliner

Biden's plan for a majority of electric cars by 2030 faces industry pushback but promises positive environmental impact and legal resilience.

# Audience

Policy advocates, environmentalists

# On-the-ground actions from transcript

- Advocate for stricter emission standards in your local area (implied)
- Stay informed about the progress and challenges of transitioning to electric vehicles (implied)

# Whats missing in summary

The transcript provides insights into Biden's ambitious plan for electric cars by 2030, industry reactions, legal challenges, and the potential environmental benefits.

# Tags

#Biden #ElectricCars #Emissions #EnvironmentalImpact #PolicyChange