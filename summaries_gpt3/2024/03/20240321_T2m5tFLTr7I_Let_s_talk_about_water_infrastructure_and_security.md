# Bits

Beau says:

- The Biden administration and the EPA are urging governors to protect water infrastructure against cyber attacks.
- Basic cybersecurity measures like resetting default passwords and updating software are critical.
- The U.S. has around 150,000 water systems that need protection, making it a challenging task.
- Iran allegedly accessed a system to display anti-Israel messages, while China is reportedly finding vulnerabilities in systems for potential disruption.
- Despite the alarming nature of the warnings, there have been no disruptions to the water supply as a result of these cyber activities.
- The messaging to governors is proactive rather than reactive, aiming to prevent potential cyber attacks on water systems.
- Cyber warfare is increasingly recognized as a significant form of response short of war.
- The current proactive security measures are similar to strategies employed during the Cold War, which were never utilized.
- While attacks today may have lower impacts, there is a higher likelihood of them being used compared to the Cold War era.
- The messaging to governors serves as a precautionary measure for potential cyber threats in critical infrastructure.

# Quotes

1. "Anybody who's familiar with this type of security understands it's difficult at best."
2. "This is simply good preventative security presented in a direct way."
3. "Cyber warfare is becoming a very accepted form of response short of war."
4. "It's worth noting that during the Cold War, that messaging, that training, that proactive security stuff went on for decades and was never used."
5. "This messaging that went out to the governors is proactive, not reactive."

# Oneliner

The Biden administration and EPA are proactively warning governors about cyber threats to water infrastructure, advocating for basic cybersecurity measures to prevent potential attacks.

# Audience

Governors, Water Authorities

# On-the-ground actions from transcript

- Ensure cybersecurity measures like resetting passwords and updating software (suggested)
- Allocate necessary funding for cybersecurity upgrades (implied)

# Whats missing in summary

The full transcript provides a detailed insight into the proactive approach of the Biden administration and EPA towards safeguarding water infrastructure from cyber attacks, aiming to prevent potential disruptions.

# Tags

#BidenAdministration #EPA #CyberSecurity #WaterInfrastructure #Governors