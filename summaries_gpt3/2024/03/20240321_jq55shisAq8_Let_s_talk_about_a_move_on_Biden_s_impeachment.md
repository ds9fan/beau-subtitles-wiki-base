# Bits

Beau says:

- Representative Moskowitz wore a Putin mask to a House hearing, sarcastically suggesting impeaching President Biden.
- Moskowitz, a Democrat, challenged the ongoing impeachment inquiry, questioning the lack of evidence.
- Moskowitz proposed to immediately impeach Biden during the hearing, pointing out the lack of grounds for impeachment.
- He emphasized that the Democrats do not have enough votes in the House or Senate for impeachment.
- Moskowitz criticized the Democrats for misleading their base by discussing impeachment without solid evidence.
- He hinted at the Republicans doubling down on baseless claims in response to his actions.
- Moskowitz anticipated a week of intense debates and conflicts in the House following his bold move.

# Quotes

1. "Let's just do the impeachment."
2. "I just think we should do it today."
3. "It's all fake."
4. "They're lying to their base."
5. "It's a show."

# Oneliner

Representative Moskowitz sarcastically pushes for Biden's impeachment, exposing the lack of evidence and political theatrics in the House proceedings.

# Audience

House Representatives

# On-the-ground actions from transcript

- Challenge misinformation within political parties (implied)
- Stay informed and engaged with political proceedings (exemplified)

# Whats missing in summary

The full transcript provides a detailed account of a House hearing where Representative Moskowitz challenges the impeachment inquiry against President Biden, shedding light on the lack of evidence and political posturing.

# Tags

#HouseHearing #ImpeachmentInquiry #PoliticalTheatrics #LackOfEvidence #PartyPolitics