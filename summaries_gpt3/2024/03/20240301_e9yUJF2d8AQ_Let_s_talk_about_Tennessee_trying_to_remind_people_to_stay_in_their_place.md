# Bits

Beau says:

- Tennessee legislature expelled two members with darker skin tones while allowing the white member to stay, under the pretext of decorum and rules.
- The expelled members were supported by their communities for representing them authentically, as opposed to trying to rule over them.
- The Tennessee legislature is advancing a bill to prevent expelled members from being reappointed, seen as a way to keep certain individuals in their perceived place.
- Questions about the constitutionality of the bill arise, especially concerning the Tennessee State Constitution.
- There is a likelihood that the bill will pass in the state house but face challenges in the Senate due to constitutional concerns.
- The overarching message seems to be an attempt by those in power to remind the people of Tennessee that they do not decide who represents them; the authorities do.
- The tone of control and superiority from the Nashville leadership extends beyond this specific incident, reflecting a broader attitude towards all residents of Tennessee.
- The authorities are perceived as wanting to rule and dictate to the people rather than truly represent them.
- The message is clear: the people are expected to obey rather than have a say in their governance.
- Beau leaves with a reminder of the power dynamics at play in Tennessee and beyond, urging viewers to contemplate the situation.

# Quotes

1. "It's the same tone that has existed for so long."
2. "Your betters up there in Nashville, they have to find some way to remind you that you're wrong and that you don't really get to decide who your representatives are."
3. "They're not there to be their boss. They're not there to represent you. They're there to rule you and tell you what to do."
4. "None of you matter."
5. "You're supposed to obey."

# Oneliner

The Tennessee legislature's actions hint at a broader attitude of control and superiority, aiming to remind the people that they do not determine their representatives but are expected to obey authority unquestioningly.

# Audience

Tennessee residents

# On-the-ground actions from transcript

- Organize community meetings to raise awareness about the proposed bill and its potential impact (suggested)
- Reach out to local representatives to express concerns about the bill's implications for representation (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of the power struggles within the Tennessee legislature and the implications of bills targeting reappointment post-expulsion.

# Tags

#Tennessee #Legislature #Representation #PowerDynamics #CommunityPolicing