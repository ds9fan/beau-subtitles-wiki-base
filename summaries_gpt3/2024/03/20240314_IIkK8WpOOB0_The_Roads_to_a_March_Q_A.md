# Bits

Beau says:

- Addresses questions about the House passing a bill potentially banning TikTok and its impact on younger voters.
- Explains the concept of Gonzo journalism and its defining characteristics, using a personal story involving a bird and police brutality.
- Responds to inquiries about the potential post-war profiteering by construction companies and the sourcing of statistics in his content.
- Shares thoughts on waiting for further information before forming opinions on certain cases, such as the Benedict, Oklahoma incident.
- Provides insights into the relief of two US submarine commanders in 2024 and dismisses notions of a Navy effort to remove Trump loyalists.
- Expresses intention to revisit the topic of Gonzo journalism due to its relevance and increasing prevalence.

# Quotes

- "It's about drawing out a deeper social commentary, a deeper social critique."
- "The reason you didn't get scared is because you're not that animal's prey."
- "Having the right information will make all the difference."

# Oneliner

Beau answers questions on TikTok bans, explains Gonzo journalism, and shares insights on post-war profiteering and naval command changes.

# Audience

Content Creators, Aspiring Journalists

# On-the-ground actions from transcript

- Research and understand Gonzo journalism (suggested)
- Stay informed about developing stories like the Benedict, Oklahoma incident (implied)

# Whats missing in summary

In-depth examples and further elaboration on the topics discussed by Beau in this Q&A session.

# Tags

#TikTok #GonzoJournalism #SocialCommentary #StatisticalAnalysis #InformationSourcing