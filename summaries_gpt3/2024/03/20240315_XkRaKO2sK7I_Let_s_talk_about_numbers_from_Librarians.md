# Bits

Beau says:

- The American Library Association tracks challenges to books in libraries, with 2023 being the highest year on record with 4,240 books challenged.
- The tracking is based on reports from librarians and news coverage, suggesting the actual numbers could be higher.
- Many challenges were organized, targeting certain types of books rather than individual disapproval.
- Some challenges included multiple books, especially focusing on books featuring people of color and the LGBTQ+ community.
- The ALA releases a list of the top 10 challenged books each year, providing unique perspectives and insights.
- Despite successful efforts to make challenged books available, this news story faded from coverage.
- The release of the top 10 challenged books for the year is expected on April 8th, often serving as a recommended reading list.
- Challenged books offer diverse viewpoints and ways of seeing the world, making them enlightening reads.

# Quotes

1. "2023 is the highest year on record with 4,240 books challenged."
2. "The type of book that was being challenged the most were books that put a spotlight on people of color, on the LGBTQ plus community."
3. "The books that end up on these lists they're normally pretty enlightening."

# Oneliner

Beau talks about the increase in challenges to books in libraries, focusing on organized efforts against diverse voices and the upcoming release of the top 10 challenged books list.

# Audience

Library advocates, book lovers

# On-the-ground actions from transcript

- Support libraries by promoting diverse literature and advocating against book challenges (implied)
- Stay informed about the top challenged books list and encourage others to read them (implied)

# Whats missing in summary

Insights on specific actions individuals can take to support libraries and combat book challenges.

# Tags

#BookChallenges #AmericanLibraryAssociation #DiverseLiterature #ReadingList #LibraryAdvocacy