# Bits

Beau says:

- Providing an update on helping Project Rebound at California State University Northridge, a fundraiser aiding formerly incarcerated individuals with college access.
- The fundraiser has raised nearly $40,000 from almost 500 people at the time of filming.
- Last year's fundraiser raised around $23,000, showing significant growth.
- Moving on to news about a federal judge in Texas ruling that the Minority Business Development Agency must offer services to white individuals.
- The judge cited the 14th Amendment for equal protection under the law, demanding the agency not use race or ethnicity in applicant considerations.
- The ruling alters the operations of the agency established in 1969 under Nixon.
- Under normal circumstances, this ruling may not stand due to potential implications and legal challenges it opens up.
- With a Trump-appointed judge overseeing the case, the outcome becomes unpredictable given previous controversial rulings.
- These agencies address systemic issues that still persist, and the future standing of this ruling remains uncertain.
- Hope remains that the Supreme Court will not allow this ruling to stay due to its far-reaching consequences.

# Quotes

1. "Understand these entities came into being and exist because of systemic issues that have not been addressed."
2. "It's not like the problem is solved."
3. "I am hopeful that the Supreme Court won't allow this to stand."
4. "It totally altered everything about the Minority Business Development Agency."
5. "Okay, on to the news."

# Oneliner

Beau provides an update on supporting Project Rebound's fundraiser for formerly incarcerated individuals while discussing a controversial ruling by a Trump-appointed judge requiring the Minority Business Development Agency to offer services to white individuals, challenging previous practices and raising concerns about systemic issues.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Support Project Rebound's fundraiser by donating or sharing the link (suggested)
- Stay informed about legal developments and cases affecting marginalized communities (implied)

# Whats missing in summary

The full transcript provides more context on the specific details of the ruling and implications for ongoing systemic issues.

# Tags

#ProjectRebound #LegalRuling #SystemicIssues #Fundraiser #CommunitySupport