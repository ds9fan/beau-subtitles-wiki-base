# Bits

Beau says:

- Exploring generational differences in responses to crises, specifically focusing on hunger and famine.
- Recounts a viewer's observation of his emotional response in a video about starvation, noting the lack of desperation seen before.
- Describes a collective shift in response to hunger, particularly among older generations like Gen X and older millennials.
- Mentions the impact of past experiences, like watching coverage of famine in Ethiopia as children, on current reactions to crises.
- Suggests that the prolonged media coverage of past famines has shaped people's psyches and responses.
- Connects the lack of current aid efforts to the absence of continuous media coverage and celebrity involvement.
- Points out how different generations have varying levels of desensitization towards war and famine.
- Talks about moral injury and how different generations perceive and respond to global crises based on their upbringing.
- Hopes that the younger generation's aversion to military operations in urban areas stems from their exposure to famine-related issues.
- Concludes by mentioning the potential collective shift in attitude due to heightened awareness of famine and its impacts.

# Quotes

1. "You're not talking about boomers, you're talking about some Gen X and older millennials."
2. "This is your moral injury."
3. "Famine will tip scales. It will change things."
4. "It will be a collective shift. And yeah, there are people who might have been complacent."
5. "And it is because it does indeed hit different."

# Oneliner

Beau delves into generational responses to crises, citing past experiences like the Ethiopia famine coverage, to explain why hunger impacts different age groups distinctively.

# Audience

Generations reflecting on responses.

# On-the-ground actions from transcript

- Support famine relief efforts through donations or volunteering (implied).
- Advocate for increased media coverage and celebrity involvement in famine crises (implied).
- Encourage intergenerational dialogues on crisis responses and sensitization (implied).

# Whats missing in summary

Deeper insights on the emotional and psychological impacts of past media coverage on current responses to crises.

# Tags

#GenerationalResponses #Famine #MediaImpact #CrisisAwareness #CollectiveShift