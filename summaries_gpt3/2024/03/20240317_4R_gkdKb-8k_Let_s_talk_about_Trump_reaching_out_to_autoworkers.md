# Bits

Beau says:

- Trump reached out to autoworkers in Ohio, campaigning for himself and supporting his primary candidates.
- Trump's promises about bringing auto manufacturing jobs back to the US and imposing tariffs on cars from other countries were reiterated.
- The challenge for Trump lies in his previous term as president, where his promises to auto workers did not materialize, such as the GM walkout.
- Auto workers likely recall the lack of delivery on promised jobs and the impact of court decisions made by Trump's appointees on their union.
- Biden, who already has the UAW endorsement, presents a stark contrast to Trump in terms of support for the auto industry.
- Union leaders have referred to the prospect of Trump's second term as a "disaster," contrasting it with Biden's initiatives.
- The Biden administration has shown tangible support for auto workers, as seen with the first lady inviting a UAW member to the State of the Union.
- Trump's belief in the effectiveness of his rhetoric from 2016 may face challenges as people are unlikely to overlook his past actions while in office.
- The overall sentiment suggests that auto workers will likely recall the actual outcomes of Trump's presidency and compare them to his promises.
- The Labor Relations Board's situation and lingering effects from the Trump administration contribute to the skepticism surrounding Trump's outreach efforts.

# Quotes

1. "Trump's promises about bringing auto manufacturing jobs here and a hundred percent tariff on cars from other places."
2. "United Auto Workers already endorsed him."
3. "Union leaders were talking about a second term referring to Trump coming back as, quote, disaster."
4. "A lot of support that has come from the Biden administration that didn't materialize under a Trump administration."
5. "It seems like that they're going to remember what actually happened and compare it to the rhetoric."

# Oneliner

Trump reaches out to autoworkers with promises, but his past actions may hinder his credibility compared to Biden's support for the auto industry.

# Audience

Autoworkers, voters

# On-the-ground actions from transcript

- Support UAW-endorsed initiatives (implied)
- Stay informed about political candidates' stances on auto industry support (implied)
- Advocate for policies benefiting auto workers (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's outreach to autoworkers and the contrasting support offered by Biden, offering valuable insights for voters and individuals interested in labor relations and the auto industry.

# Tags

#Trump #Biden #Autoworkers #UAW #LaborRelations