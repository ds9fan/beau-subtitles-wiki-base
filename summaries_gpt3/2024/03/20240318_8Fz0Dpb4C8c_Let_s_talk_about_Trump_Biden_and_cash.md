# Bits

Beau says:

- Exploring the financial landscape of the Biden and Trump campaigns.
- Biden currently has $155 million cash on hand, the most ever for a Democratic candidate at this point in an election cycle.
- Biden received about $53 million in donations last month, with 97% being small donations of less than $200.
- Trump, on the other hand, has $36.6 million cash on hand in his two major committees, significantly less than Biden.
- Trump's fundraising numbers are not required to be disclosed until April, but it is expected that they will reveal his financial situation.
- There is a possibility for Trump to close the fundraising gap through a successful blitz, although Biden is currently out-raising him.
- Last month, Trump seemed to spend more than he brought in, indicating a concerning trend early on.
- The momentum of fundraising, particularly through small donor donations, will play a key role in gauging popularity and support.
- Small donor donations will be indicative of who holds more appeal among the electorate.
- These baseline numbers provide a starting point to track fundraising trends moving forward.

# Quotes

1. "Biden has about $155 million cash on hand, more than any other Democratic candidate ever at this point."
2. "97% of that are small donations of less than $200. That is healthy fundraising."
3. "The momentum of fundraising is going to be important."
4. "It's those small donor donations that are going to provide a pretty good indication of who is more popular."
5. "These baseline numbers provide a starting point to start with."

# Oneliner

Beau breaks down the financial status of the Biden and Trump campaigns, revealing Biden's significant lead in fundraising through small donations.

# Audience

Campaign strategists

# On-the-ground actions from transcript

- Support political campaigns through small donations (implied)
- Stay informed about campaign finance updates and trends (implied)

# Whats missing in summary

Analysis on potential implications of fundraising differences on campaign strategies and outreach efforts.

# Tags

#Biden #Trump #CampaignFinance #Donations #Fundraising