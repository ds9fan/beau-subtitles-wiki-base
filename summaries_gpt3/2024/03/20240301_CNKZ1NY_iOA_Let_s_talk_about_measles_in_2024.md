# Bits

Beau says:

- Talks about the unexpected topic of measles in 2024, after its elimination in 2000 due to successful vaccination programs.
- Notes the resurgence of measles in 2024 due to people doing their own research instead of relying on vaccines.
- Provides statistics on the effectiveness of the measles vaccine versus the chances of getting infected without it.
- Mentions the CDC guidance for children exposed to measles, requiring them to stay away from school for 21 days.
- Advises on the importance of isolating oneself from measles and the possibility of indirect spread.
- Alerts about the spread of norovirus and the need to maintain basic hygiene practices.
- Encourages checking on children's vaccination status and following preventive measures.

# Quotes

1. "We're gonna talk about measles in 2024, which is really weird."
2. "Just as a quick reminder, there's a vaccine for this."
3. "It's worth remembering it can spread even without direct contact."
4. "Wash your hands, don't touch your face, you know all the basic hygiene stuff."
5. "It's probably a good idea to see if your kids are up to date."

# Oneliner

Beau reminds us in 2024 about measles resurgence due to vaccine hesitancy, stressing the importance of vaccination, isolation, and hygiene practices.

# Audience

Parents, caregivers, community members

# On-the-ground actions from transcript

- Ensure your children are up to date with their measles vaccinations (implied).
- Follow CDC guidance on keeping children away from school for 21 days after exposure to measles (implied).
- Practice good hygiene by washing hands regularly and avoiding touching your face (implied).

# Whats missing in summary

Importance of community education on the effectiveness and necessity of vaccinations to prevent disease outbreaks.

# Tags

#Measles #Vaccination #PublicHealth #Hygiene #CommunitySafety