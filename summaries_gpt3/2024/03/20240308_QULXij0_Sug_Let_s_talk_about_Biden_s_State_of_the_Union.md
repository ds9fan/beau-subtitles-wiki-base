# Bits

Beau says:

- The State of the Union was different this time, with Biden being more energetic than usual and making jabs at Trump without mentioning his name.
- Biden used the phrase "the greatest story never told" when talking about the economy, possibly taking a swipe at negative media coverage and the Democratic Party's messaging.
- He emphasized the lies surrounding the election as the greatest threat to democracy and spoke extensively on foreign policy, including Ukraine, NATO countries, and Gaza.
- Beau encourages watching the State of the Union and suggests that calling representatives, especially if they are Republican, could help bring change regarding Gaza.
- He stresses the urgency of taking action now, mentioning suggestions like using Chinooks to deliver aid to those in need.

# Quotes

1. "This is the moment where it's not rhetoric. It's, you got to pull out all the stops."
2. "Whatever it takes. Whatever it takes."
3. "It has to be now."
4. "Our policies are at least better than the Republicans but that doesn't happen."
5. "If calling your representatives, especially if they are Republican, could help."

# Oneliner

Beau breaks down the unique aspects of Biden's energetic State of the Union, addressing jabs at Trump, the economy, foreign policy, and urgent calls for action on Gaza.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Contact your Republican representatives in the House to push for support on Gaza (suggested)
- Advocate for urgent action on delivering aid to those in need, using any means necessary (exemplified)

# Whats missing in summary

Insights on how to navigate the changing political landscape and take tangible actions to support urgent causes.

# Tags

#StateOfTheUnion #Biden #Gaza #ForeignPolicy #PoliticalAction