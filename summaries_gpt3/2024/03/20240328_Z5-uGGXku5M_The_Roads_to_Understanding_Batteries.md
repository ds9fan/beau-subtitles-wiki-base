# Bits

Beau says:

- Beau introduces the topic of adapting your surroundings and the importance of improvisation during tough situations.
- He talks about how altering your surroundings can make achieving desired outcomes easier.
- The focus is on creating power sources using simple materials like lemons, potatoes, and mud.
- Beau demonstrates creating a lemon battery using zinc and copper plates connected to a watch to generate electricity.
- The electrolytes in the water interact with the metals to create a chemical reaction that powers small devices like LED lights or digital watches.
- He explains the process step by step, making it accessible for anyone to try at home.
- Beau mentions building an electrolyte solution using table salt and water for another project involving cans, pencils, and wires.
- By introducing graphite from pencils to the solution, a chemical reaction occurs, generating an electric current to power a calculator.
- Another battery creation involves pennies, vinegar, table salt, and cardboard to build a functional flashlight.
- Beau provides detailed instructions on creating the battery using vinegar-soaked cardboard and copper-zinc coins.

# Quotes

1. "Your situation is a little bit easier to change in other ways."
2. "As you move forward, things will get more interesting."
3. "Having the right information will make all the difference."

# Oneliner

Beau introduces adapting surroundings through improvisation, demonstrating creating power sources from simple materials like lemons and pennies.

# Audience

DIY enthusiasts

# On-the-ground actions from transcript

- Build a lemon battery using zinc and copper plates (suggested)
- Create an electrolyte solution using table salt and water for a power project (suggested)
- Build a battery using pennies, vinegar, table salt, and cardboard to power a flashlight (suggested)

# Whats missing in summary

Practical demonstration of creating alternative power sources using everyday items.

# Tags

#DIY #Energy #Empowerment #Innovation #CreativeSolutions