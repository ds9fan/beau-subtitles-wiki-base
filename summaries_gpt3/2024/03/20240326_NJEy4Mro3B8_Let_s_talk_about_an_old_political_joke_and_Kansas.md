# Bits

Beau says:

- Explains an old joke about American politics involving lawmakers wearing jackets with logos like NASCAR drivers to show their sponsors.
- Mentions the enduring joke's origin in the 90s, reflecting the reality of money in American politics.
- Talks about a new requirement in the Kansas House where bills will now include the name of who asked for the bill, whether a lobbyist, lawmaker, or private citizen.
- Considers this requirement as the closest thing to the NASCAR jacket idea becoming a reality.
- Expresses curiosity about how long this requirement will last and if it will spread to the Senate and other states.
- Notes that if the House in Kansas is implementing this requirement, there’s no reason for the Senate not to do the same.
- Speculates on the potential positive impact of this transparency on American people understanding who is behind proposed bills.

# Quotes

1. "There are a lot of jokes about American politics because, I mean, sometimes you have to laugh or cry, right?"
2. "It is an interesting idea. I'm very curious how long it will last, and whether or not it will spread."
3. "Anyway, it's just a thought, y'all have a good day."

# Oneliner

Beau explains the enduring joke about lawmakers wearing sponsor-filled jackets in American politics and how a new bill requirement in the Kansas House may bring transparency to legislation.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Contact legislators to advocate for similar transparency requirements in your state (implied).

# Whats missing in summary

The full transcript provides a detailed exploration of the enduring joke in American politics and the potential impact of increased transparency in legislation.

# Tags

#AmericanPolitics #Transparency #Legislation #GovernmentReform #NASCARJackets