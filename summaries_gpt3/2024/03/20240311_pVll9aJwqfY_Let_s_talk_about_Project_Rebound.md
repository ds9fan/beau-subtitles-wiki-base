# Bits

Beau says:

- Introduces the topic of Project Rebound at California State University, Northridge, and the purpose of the video.
- Shares the successful outcome of the fundraiser, with the goal of matching last year's $23,000 achievement, but raising $90,990 instead.
- Explains that the funds raised go towards supporting formerly incarcerated individuals with essentials like food, books, school supplies, and childcare to help them navigate school.
- Emphasizes the importance of Project Rebound as a support system for individuals who lack belief and support from others, helping them through education.
- Points out the significant impact beyond monetary value – 1,185 individuals now have the support and belief of others, a priceless recognition.
- Mentions that the program is pleased with the results and expresses optimism for its future success.
- Plans to share two videos related to messages received, hinting at the potential impact Project Rebound could have had on those individuals.
- Encourages viewers to have a good day, ending on a positive note.

# Quotes

1. "Those students now know that one thousand one hundred and eighty five people that they may never meet believe in them."
2. "It goes to anything that helps get them through. It could be food, books, school supplies, child care."
3. "And that it probably would have benefited them."
4. "One of the questions that came in is what does this money actually, you know, go to?"
5. "Y'all have a good day."

# Oneliner

Beau shares the success and impact of Project Rebound, providing support and belief to formerly incarcerated individuals pursuing education.

# Audience

Supporters of education equity

# On-the-ground actions from transcript

- Support Project Rebound financially or through volunteering to help provide essentials and support for formerly incarcerated individuals (suggested).

# Whats missing in summary

The full video provides more context on the significance of Project Rebound and the potential impact it could have had on individuals who reached out to Beau.

# Tags

#ProjectRebound #CaliforniaStateUniversity #Fundraiser #Support #Belief