# Bits

Beau says:

- Beau introduces a story about a guy who loves space and encounters an astronaut on a plane.
- The guy sitting next to the astronaut tries to strike up a conversation and ends up embarrassing himself.
- Despite the guy's enthusiasm, the astronaut remains focused on a book about Portuguese art.
- Beau uses this story to explain why he hasn't expressed a firm opinion on nuclear power.
- He acknowledges his lack of expertise on the subject and the importance of being well-informed before forming opinions.
- Beau points out the danger of jumping to conclusions without sufficient knowledge or information.
- He stresses the importance of being open to changing opinions based on new information.
- The story serves as a reminder to avoid forming strong opinions without adequate knowledge.

# Quotes

- "If you don't know a whole lot about something, your opinion should be subject to change."
- "Your opinion should be subject to new information."
- "People have a very firm opinion about that, and they're wrong."

# Oneliner

Beau shares a story to illustrate the importance of being informed before forming firm opinions and the need for openness to change based on new information.

# Audience

Social media users

# On-the-ground actions from transcript

- Keep an open mind to changing opinions based on new information (implied)

# Whats missing in summary

The full transcript provides a valuable lesson on the importance of being well-informed before forming strong opinions and remaining open to changing perspectives based on new information.

# Tags

#Opinions #Knowledge #Informed #OpenMind #Change