# Bits

Beau says:

- Beau introduces Biden's proposed budget for 2025 and notes the slow pace in the house in finalizing the 2024 budget.
- The budget is based on non-recessionary conditions with low unemployment rates.
- Noteworthy proposals include optional free preschool for four-year-olds and affordable childcare for those making less than $200,000 a year.
- The budget focuses on taxes, aiming to reduce the deficit by 3 trillion over 10 years and increase revenue by 4.9 trillion through new taxes.
- Middle and low-income individuals receive tax breaks, while higher-income earners face higher tax bills.
- Significant changes include expanding the child tax credit from $2,000 to $3,600 and extending the Earned Income Tax Credit to more demographics.
- The budget also includes a $10,000 tax credit for first-time homebuyers.
- Funding for these proposals comes from increasing the corporate tax rate to 28%, implementing a minimum tax rate for billionaires, and raising the top tax bracket for individuals earning over $400,000 and couples over $450,000.
- Beau acknowledges that not all proposed measures may make it into the final budget as negotiations begin with this initial proposal.
- He anticipates opposition from Republicans and expects a process of negotiation to determine the final budget.

# Quotes

1. "Optional free preschool for four-year-olds."
2. "If you make less than 200,000 a year, it guarantees affordable childcare."
3. "The deficit goes down 3 trillion over 10 years."
4. "What I just said is incredibly unlikely to all make it in."
5. "This is a starting negotiation position."

# Oneliner

Beau introduces Biden's proposed budget for 2025, focusing on tax changes, childcare provisions, and potential negotiation challenges with Republicans.

# Audience

Budget analysts, policymakers

# On-the-ground actions from transcript

- Contact your representatives to express your support or concerns regarding the proposed budget (suggested)
- Join advocacy groups working on budget-related issues to stay informed and engaged (exemplified)

# Whats missing in summary

Detailed breakdown of specific tax changes and their potential impacts

# Tags

#Budget #Biden #TaxChanges #ChildcareProvisions #Negotiation #Republicans