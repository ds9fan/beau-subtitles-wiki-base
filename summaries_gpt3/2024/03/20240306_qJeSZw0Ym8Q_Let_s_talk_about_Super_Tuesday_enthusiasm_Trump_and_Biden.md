# Bits

Beau says:

- Beau provides an overview of Super Tuesday and the recent primary elections in the United States.
- He mentions that Trump and Biden were the winners, with Biden winning all his primaries and Trump losing one.
- Beau talks about how the percentages by which candidates win can indicate the enthusiasm within the party for that candidate.
- Trump won in various percentage brackets, while Biden had higher percentages showing more enthusiasm from voters.
- Beau notes that Biden seems to have more enthusiasm behind his campaign compared to Trump based on primary results.
- He mentions a program called Project Rebound at California State University Northridge, which supports formerly incarcerated individuals and encourages support for their fundraising day.

# Quotes

1. "Biden has more enthusiasm behind his voters."
2. "Trump underperformed again, as he normally does."
3. "The recidivism rate for that four-year period was zero."
4. "More people within the party were okay, at bare minimum, with voting for Biden."

# Oneliner

Beau analyzes Super Tuesday primaries, pointing out higher enthusiasm for Biden over Trump based on percentage wins and encourages support for Project Rebound.

# Audience

Voters, Supporters, Donors

# On-the-ground actions from transcript

- Support Project Rebound's giving day fundraiser at California State University Northridge by waiting for the official giving day to start and contributing to the program (suggested).
- Research and learn more about Project Rebound to understand its impact on formerly incarcerated individuals (implied).

# Whats missing in summary

Full understanding of the primary election breakdowns and the effectiveness of supporting programs like Project Rebound.

# Tags

#SuperTuesday #PrimaryElections #Biden #Trump #ProjectRebound