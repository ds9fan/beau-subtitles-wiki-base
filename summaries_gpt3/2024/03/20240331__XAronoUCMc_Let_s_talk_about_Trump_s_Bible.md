# Bits

Beau says:

- Trump is promoting a Bible, sparking various reactions and debates, particularly about its $59 price tag and where the money is going.
- Backlash from religious scholars includes concerns about nationalism, additions to the Bible, and Trump's appropriateness.
- Some view the Bible as just another product with Trump's name on it, akin to shoes or NFTs.
- The Bible includes the Constitution, prompting reflection on the importance of its ideas and the separation of religion and government.
- Beau questions the blending of religion and nationalism, citing historical examples where this mix led to horrific outcomes.
- He recalls a common saying about fascism in America being wrapped in a flag and carrying a cross, alluding to the symbolism of the Bible cover.

# Quotes

1. "When fascism comes to America, it will be wrapped in a flag and carrying a cross."
2. "There aren't a lot of good examples of when religion and nationalism got blended together."
3. "You'd think that if people viewed the Constitution with such reverence, they'd want it presented in this fashion."
4. "It's just another product, like the shoes or the NFTs, just something he's putting his name on."
5. "Normally, it's pretty horrific."

# Oneliner

Trump's promotion of a Bible prompts debates on nationalism, religion, and the Constitution, reflecting on the dangers of blending religion and government.

# Audience

Activists, Religious Scholars

# On-the-ground actions from transcript

- Question the blending of religion and nationalism (implied)
- Contemplate the importance of separating religion and government (implied)

# Whats missing in summary

Deeper insights into the implications of intertwining religion and nationalism, and the potential risks associated with such actions.

# Tags

#Trump #Religion #Nationalism #Constitution #Fascism