# Bits

Beau says:

- Critiques the current polling's accuracy in predicting election outcomes due to not accounting for unlikely voters.
- Notes Trump's lead in Iowa caucus polling and Nikki Haley's 20% of the vote.
- Mentions an interesting poll revealing that half of Nikki Haley's supporters in the caucus may choose Biden over Trump in the general election.
- Emphasizes that these voters are the most active and partisan in the Republican Party.
- Speculates on the implications of a significant portion of partisan Republicans not supporting Trump.
- Points out the challenge Trump faces in winning over independents, especially if they share similar reluctance towards him.
- Expresses skepticism about Trump appealing to moderates or independents before the election.
- Raises concerns about Trump potentially becoming more inflammatory, which could alienate more voters.
- Acknowledges Trump's strong chance in winning the primary but questions his prospects in the general election due to legal issues and partisan rejection.
- Concludes with uncertainty about how Trump will navigate these challenges in the upcoming election.

# Quotes

- "Roughly half of Nikki Haley's voters would vote for Biden over Trump in the general election."
- "If 10% of the most partisan people in Iowa won't vote for Trump, he's going to have a really hard time."
- "Trump stands a really good chance of winning the primary, but I don't know how he'll fare in a general."
- "It seems unlikely that he develops a softer tone. If anything, he is going to become more inflammatory."
- "I don't know how he's going to overcome that when it comes to the general."

# Oneliner

Beau explains how a significant portion of Nikki Haley's supporters might choose Biden over Trump in the general election, posing a challenge for Trump among partisan Republicans and independents.

# Audience

Political analysts, voters, strategists

# On-the-ground actions from transcript

- Contact local political groups to understand voter sentiment and engagement levels (suggested)
- Organize focus groups to gauge public opinion and potential election outcomes (exemplified)

# Whats missing in summary

Deeper analysis on the potential impact of partisan rejection on Trump's general election prospects.

# Tags

#Politics #Election2020 #Polling #Trump #NikkiHaley