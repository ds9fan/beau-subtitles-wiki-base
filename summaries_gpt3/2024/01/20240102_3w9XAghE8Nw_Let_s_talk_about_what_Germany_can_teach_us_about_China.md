# Bits

Beau says:

- Compares US presidents' speeches on Germany to Chinese leader's speech on Taiwan.
- US presidents spoke of Germany's reunification as a historical inevitability.
- Kennedy, Truman, and Eisenhower all expressed belief in Germany's reunification.
- Kennedy's speech in Berlin in 1963 echoed the sentiment of reunification.
- Lyndon B. Johnson's speech in 1964 emphasized the inevitability of Germany's reunification.
- Germany was reunified in 1990, long after the speeches were made.
- Chinese leader recently stated that China will be reunified, specifically mentioning Taiwan.
- The Chinese leader's statement mirrors the historical inevitability rhetoric used by US presidents.
- Beau clarifies that the Chinese leader's statement is not a threat of war but a statement of future reunification.
- Emphasizes that such political positioning is common among world leaders.
- Beau reassures that the current situation with China is not an indication of imminent conflict.
- States that neither party, China nor the US, desires a war with each other.
- Beau underscores the importance of understanding political posturing in foreign policy.
- Concludes by stating that the Chinese leader's speech is just good public speaking and not a cause for concern.

# Quotes

- "This is not a threat of war."
- "It's just good public speaking."
- "Neither party actually wants to go to war with each other."

# Oneliner

US presidents' historical inevitability speeches on Germany compared to Chinese leader's statement on Taiwan show political posturing without indicating imminent conflict.

# Audience

Foreign policy observers

# On-the-ground actions from transcript

- Understand the nuances of political posturing in foreign policy (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of historical speeches by US presidents on Germany's reunification, comparing them to the recent statement by the Chinese leader on Taiwan, offering insights into political rhetoric and foreign policy dynamics.