# Bits

Beau says:

- Explains why Trump wasn't charged with treason in the United States.
- Treason in the U.S. is narrowly defined as levying war against the country or aiding enemies.
- Refers to Article 3, Section 3, Clause 3 of the U.S. Constitution for the specific definition of treason.
- Mentions that treason was narrowly defined because the founders themselves had committed it.
- Talks about how treason charges against Trump wouldn't hold up in court due to other laws governing broader misconduct.
- Criticizes those who misuse the term "treason" for trivial matters like forgiving student debt.
- Points out that invoking treason in situations not involving war is a sign of ignorance about the Constitution.
- Emphasizes that treason, insurrection, and sedition are distinct charges with specific criteria.
- Advises to be cautious of individuals using inflammatory rhetoric about treason without a basis in war.
- Encourages understanding the specific definition of treason to combat attempts to manipulate public opinion.

# Quotes

- "Treason in the United States is incredibly specific."
- "If you understand that treason requires war, when you have people out there yelling that this is treason or that's treason and there's not a war, you know that they're using a rhetorical device to anger you."
- "Treason requires conflict. It is incredibly narrow."

# Oneliner

Beau explains why Trump wasn't charged with treason, clarifies the specific definition in the Constitution, and warns against inflammatory misuse of the term.

# Audience

Constitutional learners and those combating misinformation.

# On-the-ground actions from transcript

- Understand the specific definition of treason in the U.S. and spread accurate information (implied).

# Whats missing in summary

Additional context on the dangers of misusing legal terms like treason and the importance of educating oneself on constitutional matters.

# Tags

#Trump #Constitution #Treason #Misinformation #LegalKnowledge