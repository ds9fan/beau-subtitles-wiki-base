# Bits

Beau says:

- Louisiana Senate approved a new congressional map to address Voting Rights Act violation.
- Previous map diluted black voting power with only one out of six districts being majority black.
- New map includes a second majority black district, better reflecting the state's demographics.
- Speaker of the House, Johnson, objected to the new map, viewing it as surrendering a Republican seat.
- Governor called a special session for House approval before January 30th deadline.
- If House doesn't approve by deadline, trial begins.
- Plaintiffs may object to the new map, potentially leading to court involvement.
- Louisiana appears to be moving in the right direction without constant court battles.
- Governor likely to approve the new map if passed by the House.
- Update to follow once the House decision is made.

# Quotes

- "Louisiana Senate approved a new congressional map to address Voting Rights Act violation."
- "Louisiana appears to be moving in the right direction without constant court battles."

# Oneliner

Louisiana Senate addressed Voting Rights Act violation by approving a new map, reflecting state demographics, despite objections, aiming to avoid trial.

# Audience

Louisiana residents, Voting Rights advocates.

# On-the-ground actions from transcript

- Contact local representatives to express support for the new map (suggested).
- Stay informed and engaged in the legislative process regarding redistricting (implied).

# Whats missing in summary

The full transcript provides in-depth details on Louisiana's redistricting process and the significance of fair representation.

# Tags

#Louisiana #Redistricting #VotingRightsAct #Representation #Governance