# Bits

Beau says:

- Wisconsin is gerrymandered, with extreme partisan gerrymanders in the United States.
- Justice Protasewicz pointed out the gerrymandering issue in Wisconsin during her campaign.
- The State Supreme Court confirmed that the maps in Wisconsin are gerrymandered.
- Republicans in Wisconsin are taking the gerrymandering case to the US Supreme Court.
- Politicians who support gerrymandering do not support representative democracy.
- Gerrymandering is about picking voters rather than voters choosing representation.
- Gerrymandering dilutes voting power and aims to choose easily manipulated voters.
- Gerrymandering hurts even those in the party it benefits by making their votes irrelevant.
- Gerrymandering creates safe districts where representatives don't have to listen to constituents.
- The Republican Party in Wisconsin fights to maintain gerrymandered maps to ignore voters' needs.

# Quotes

- "Politicians who support gerrymandering do not support representative democracy."
- "Gerrymandering is about picking voters rather than voters choosing representation."
- "Your vote is irrelevant even if you're in their party because they know that they packed enough easily manipulated voters in there to outnumber you."
- "This is how you get a government that is completely unresponsive to your needs because they don't have to be."
- "The obvious answer here is to vote out anybody of any party that supports gerrymandering."

# Oneliner

Wisconsin's extreme partisan gerrymandering undermines representative democracy, rendering voters irrelevant even within their own party.

# Audience

Voters, Activists, Community Members

# On-the-ground actions from transcript

- Vote out any politician, regardless of party, who supports gerrymandering (suggested).
- Stay informed about redistricting issues in your state and take action to ensure fair representation (implied).

# Whats missing in summary

Importance of being vigilant and proactive in addressing gerrymandering issues to safeguard representative democracy.

# Tags

#Wisconsin #Gerrymandering #RepresentativeDemocracy #VotingRights #PoliticalReform