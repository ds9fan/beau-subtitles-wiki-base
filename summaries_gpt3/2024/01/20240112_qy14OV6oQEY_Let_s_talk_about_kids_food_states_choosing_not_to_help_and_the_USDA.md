# Bits

Beau says:

- USDA program to feed 21 million kids in the U.S. over the summer.
- Eligibility: below 185% of the poverty level and living in a specific state.
- Benefits include $40 per month per kid.
- Kids from low-income families struggle to eat during the summer when school meals aren't available.
- Not all states opted into the program; Alabama, Alaska, Florida, Georgia, and more chose not to participate.
- Reasons for not opting in include concerns about big government and paperwork.
- Summer Food Services Program is available in most places for kids to access meals at specific locations.
- States not participating can join in 2025 with a change of heart.
- Some states appear to prioritize bureaucracy over feeding hungry children.
- The program aims to address food insecurity among children during the summer months.

# Quotes

- "If the problem is a lack of money, yeah, it does help."
- "Seems like there's a little bit of a trend there."
- "States not participating can join in 2025 with a change of heart."
- "It's probably just the general trend of wanting to kick down at other people."
- "Now, for those that live in states that don't care about the children of their state..."

# Oneliner

USDA program to feed 21 million kids over summer faces resistance in some states, where bureaucracy seems prioritized over child hunger relief.

# Audience

Advocates for child welfare

# On-the-ground actions from transcript

- Support local summer food programs by volunteering or donating (implied)
- Advocate for expanding food assistance programs in your state (implied)

# Whats missing in summary

The full transcript provides detailed insights into the challenges faced in ensuring food security for vulnerable children, including state-level decision-making impacting access to vital nutrition programs.

# Tags

#ChildHunger #FoodInsecurity #USDAProgram #SummerMeals #StateResponsibility