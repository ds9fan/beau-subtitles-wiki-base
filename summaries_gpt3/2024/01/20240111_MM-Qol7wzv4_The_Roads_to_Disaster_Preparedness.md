# Bits

Beau says:

- Introduces the topic of disaster preparedness in the United States after receiving questions about dealing with tornadoes following recent tornadoes in the area.
- Explains the difference between a tornado watch (looking for a tornado) and a warning (one has been seen).
- Advises on seeking shelter in a basement, cellar, shelter, or panic room during a tornado, or a sturdy interior room with no windows on the first floor if in a place like Florida.
- Contradicts previous advice on seeking shelter under an underpass during a tornado, now recommending a low, flat location.
- Emphasizes the importance of the structure of the shelter during a tornado rather than its materials.
- Warns against the dangers of floodwaters post-hurricane, including contamination, bacteria, and unseen hazards.
- Cautions against using a chainsaw after a storm due to the high risk of injury from twisted debris.
- Advises against running a generator indoors due to harmful emissions.
- Provides tips for earthquake preparedness, including securing furniture and knowing how to turn off gas, water, and electricity.
- Recommends finding high ground during floods and having a plan to escape if trapped in an attic.
- Shares advice on staying warm during blizzards by creating a pillow fort in a smaller room with layers of insulation.

# Quotes

- "You need a disaster preparedness kit, you need an emergency kit, food, water, fire, shelter, first aid kit, which includes your meds, and a knife."
- "The first rule of rescue is to not create another victim."
- "Having the right information will make all the difference."

# Oneliner

Beau covers disaster preparedness essentials from tornadoes to blizzards, stressing the importance of proper knowledge and preparation for different calamities.

# Audience

Preparedness Seekers

# On-the-ground actions from transcript

- Contact volunteer firefighters in affected areas for aid group relief efforts (suggested)
- Create a disaster preparedness kit with essentials like food, water, fire supplies, and first aid items (exemplified)

# Whats missing in summary

The full transcript provides detailed guidance on disaster preparedness across various natural disasters, offering practical tips and debunking common misconceptions.

# Tags

#DisasterPreparedness #Tornadoes #Hurricanes #Earthquakes #Blizzards #CommunityAid