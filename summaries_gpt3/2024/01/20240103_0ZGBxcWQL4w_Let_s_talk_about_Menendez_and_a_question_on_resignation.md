# Bits

Beau says:

- Provides an update on Senator Menendez from New Jersey and the allegations against him, shifting from Egypt to Qatar and extending into 2023.
- Mentions that the allegations involve business deals and investments.
- Democrats have been calling for Menendez to resign since the first set of allegations in September, and more calls came with the superseding indictment.
- Explains that despite the calls for resignation, it is ultimately up to Menendez to decide.
- Notes a shift in tradition where politicians facing allegations used to resign once indicted, but that tradition seems to be fading.
- Speculates on Menendez's potential response to the superseding indictment and mentions his intention to take the matter to trial.
- References Menendez's past involvement with gold bars and the evidence supporting the allegations against him.

# Quotes

- "Their calls don't really mean anything."
- "Resignation is voluntary."
- "The tradition seems to be to try to stay in office as long as possible."

# Oneliner

Senator Menendez faces calls for resignation amidst shifting allegations, with Democrats urging him to step down, though the final decision rests with him.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Contact your representatives to voice your opinion on political accountability (implied)
- Stay informed about the developments in political scandals and hold elected officials accountable (exemplified)

# Whats missing in summary

Full details and context surrounding Senator Menendez's legal troubles and the Democratic calls for his resignation.

# Tags

#SenatorMenendez #PoliticalScandal #Democrats #ResignationCalls #Accountability