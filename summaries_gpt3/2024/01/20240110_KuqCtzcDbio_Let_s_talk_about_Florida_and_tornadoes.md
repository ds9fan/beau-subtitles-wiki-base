# Bits

Beau says:

- Multiple tornadoes hit the panhandle in Florida causing very bad weather.
- The community gardening video dealing with the greenhouse is gone due to the tornadoes.
- The areas hit the hardest were Panama City and Mariana, Florida.
- First responders and businesses in small towns stepped up to help the affected communities.
- Relief efforts like running errands will be carried out, but fundraisers are not planned currently.
- Communities have experience with disasters like Hurricane Michael, so they are coming together to recover.
- A campground and RV Park in Marianna, Florida was destroyed, but fortunately, there were no fatalities.
- Beau stresses the importance of paying attention to weather alerts to stay safe during natural disasters.
- Smaller communities are pulling together to support each other in the recovery process.
- Beau will provide updates on how the communities in the area are recovering from the tornadoes.

# Quotes

- "Make sure you pay attention to weather alerts."
- "Communities seem to be pulling together."
- "No major damage, everybody's fine."

# Oneliner

Beau provides an update on the tornadoes in Florida, stressing community support and the importance of staying safe during natural disasters.

# Audience

Local residents, disaster relief organizations

# On-the-ground actions from transcript

- Support relief efforts by running errands for the affected communities (implied).
- Stay informed and prepared for inclement weather by signing up for weather alerts (implied).
- Offer assistance and support to smaller communities affected by the tornadoes (implied).

# Whats missing in summary

The full transcript provides a detailed account of the impact of tornadoes on Florida, including community responses and the importance of preparedness for natural disasters.

# Tags

#Tornadoes #Florida #CommunitySupport #NaturalDisasters #WeatherAlerts