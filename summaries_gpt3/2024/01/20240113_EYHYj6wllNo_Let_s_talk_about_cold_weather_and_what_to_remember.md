# Bits

Beau says:

- Large portions of the United States are bracing for very cold temperatures due to an Arctic blast.
- Temperatures are expected to drop to zero or below in parts of Texas, Oklahoma, and Arkansas.
- Even colder temperatures in the negatives will be experienced further north.
- In the deep south, single digits or teens are expected.
- Beau urges people to be prepared for the cold and offers practical advice.
- He recommends checking on those who may not have shelter during this period.
- Tips include creating makeshift shelters using a dining room table or building a pillow fort for warmth.
- Beau advises against burning things indoors for heat and running generators inside.
- He stresses the importance of keeping pets and children away from any sources of heat.
- The cold spell is anticipated to last a few days, with the worst hitting the middle of the country first before moving east.

# Quotes

- "If you see somebody like that, no you didn't. You don't want to be the person who sends somebody out into the elements and then them not make it."
- "And that will help keep the temperature up a little bit. It'll give you a few degrees and those few degrees are gonna matter."

# Oneliner

Large parts of the US are facing extreme cold temperatures; Beau provides tips on staying warm and safe during the Arctic blast.

# Audience

Communities

# On-the-ground actions from transcript

- Create makeshift shelters using a dining room table or build a pillow fort for warmth (suggested).
- Ensure you have necessary supplies and preparations for the cold weather (implied).
- Regularly checking on vulnerable individuals in your community (implied).

# Whats missing in summary

Additional details on specific preparations and supplies needed to endure the extreme cold.

# Tags

#ExtremeCold #ArcticBlast #CommunityPreparedness #StayWarm #SafetyTips