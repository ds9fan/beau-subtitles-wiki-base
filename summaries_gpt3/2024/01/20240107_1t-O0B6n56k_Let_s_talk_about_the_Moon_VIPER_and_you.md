# Bits

Beau says:

- NASA is sending a rover called Viper to the moon in November to search for water ice.
- The purpose of this mission is to pave the way for manned bases on the moon.
- Viper will have to survive for around 100 days in challenging conditions.
- To involve the public, NASA is offering to attach people's names to the rover if submitted by March 15th.
- Submit your name at www.nasa.gov/send-your-name-with-viper by 11:59 p.m. Eastern Time on March 15th.
- This initiative aims to spark interest in space travel, especially among kids.
- Getting your name attached to the rover is a unique way to be part of a space mission.
- NASA provides a digital boarding pass and a QR code for more information when you submit your name.
- This is a great way to kickstart an interest in space and space exploration.
- Beau found this initiative interesting and thought others might enjoy it too.

# Quotes

- "Your name will go into space."
- "This is a great way to kickstart an interest in space and space travel."

# Oneliner

Be part of NASA's Viper mission to the moon by submitting your name for a unique space experience and sparking interest in space travel, especially for kids.

# Audience

Space enthusiasts

# On-the-ground actions from transcript

- Submit your name at www.nasa.gov/send-your-name-with-viper by March 15th (suggested).

# Whats missing in summary

The detailed process of how submitting your name can spark interest in space and encourage involvement in space exploration.

# Tags

#NASA #Viper #MoonMission #SpaceExploration #NASAInitiative