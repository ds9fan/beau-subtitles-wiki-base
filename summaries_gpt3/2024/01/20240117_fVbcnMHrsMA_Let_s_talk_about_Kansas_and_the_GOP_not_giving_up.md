# Bits

Beau says:

- Kansas voting bill introduced to restrict reproductive rights despite overwhelming public support for access.
- Republicans pushing HB 2492 despite clear public stance on reproductive rights.
- Co-sponsor claims students asked for the bill, but its passage is unlikely.
- If bill passes, governor likely to veto; Republicans may attempt to override veto.
- Courts expected to find the bill unconstitutional if enacted.
- Voting process in Kansas shown as a facade to demonstrate citizens' insignificance.
- Citizens express desire for access to reproductive rights, met with opposition from Republican Party.
- Republicans prioritize their agenda over public opinion, driven by corporate donors.
- Public stance in Kansas contrasts sharply with Republican Party's actions.
- Republicans view themselves as above the people of Kansas, dictating policy.
- Persistent attempt to limit rights despite public opposition.

# Quotes

- "Voting is a show in some ways, and it's to show the people of Kansas that you don't matter."
- "People in Kansas made it very clear where they stand on this issue."
- "The Republican Party is making it very clear where they stand on the people of Kansas, And that's above them, telling them what to do."

# Oneliner

Kansas voting bill introduced to restrict reproductive rights despite overwhelming public support; Republicans prioritize agenda over citizens' voices.

# Audience

Kansas Residents

# On-the-ground actions from transcript

- Organize community forums to raise awareness about the bill and its potential impact (suggested).
- Contact local representatives to express concerns about the restrictive bill (implied).
  
# Whats missing in summary

Full understanding of the context and implications of Kansas voting bill and its impact on reproductive rights advocacy.

# Tags

#Kansas #VotingRights #ReproductiveRights #RepublicanParty #PublicOpinion