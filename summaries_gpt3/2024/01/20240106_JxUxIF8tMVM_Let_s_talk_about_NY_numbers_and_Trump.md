# Bits

Beau says:

- Explains the increase in numbers in Trump's civil case in New York.
- Attorney General in New York is asking for an increase from the original 250 million to 370 million.
- Details the breakdown of the new number: $2.5 million in bonuses, $60 million in profit from Ferry Point, $139 million in profit from the old post office in D.C., and $168 million in saved interest from four commercial loans.
- State wants lifetime bans on Trump, Weisselberg, and McHoney from participating in the real estate industry or acting as officers or directors of any New York corporation.
- Trump's sons may face five-year bans.
- Closing arguments are on Thursday, after which the judge will make a ruling.
- Expectation is that Trump will have to pay, but the exact amount is uncertain due to changing numbers.
- Anticipated ruling includes some form of ban and a monetary penalty, likely exceeding a hundred million.
- Case is expected to conclude soon, moving on to the appeals process.
- Overall, the case is finally starting to wind down.

# Quotes

- "State wants lifetime bans on Trump, Weisselberg, and McHoney from participating in real estate."
- "Expectation is that Trump will have to pay, but the exact amount is uncertain."
- "Case is expected to conclude soon, moving on to the appeals process."

# Oneliner

Attorney General in New York is seeking an increase in Trump's civil case, with anticipated lifetime bans and monetary penalties, as the case nears its conclusion.

# Audience

Legal observers, political analysts

# On-the-ground actions from transcript

- Stay updated on the developments of the case (suggested)
- Analyze the implications of potential lifetime bans and financial penalties (suggested)

# Whats missing in summary

The full transcript provides a detailed breakdown of the increased numbers in Trump's civil case in New York and the potential consequences for him and others involved.

# Tags

#LegalCase #Trump #NewYork #AttorneyGeneral #CivilCase