# Bits

Beau says:

- Beau opens talking about American foreign policy and the desire to change it due to dissatisfaction with its current state.
- He shares a message from someone expressing frustration with the inability to alter American foreign policy outcomes.
- Beau acknowledges the difficulty in changing American foreign policy, especially due to its intractable nature.
- Changing American foreign policy is deemed nearly impossible, requiring a shift within the United States first.
- Beau stresses the importance of initiating change at the local level through community networks for systemic transformation.
- He explains that altering American society is the key to eventually influencing American foreign policy.
- Building power outside of political parties at the local level is emphasized as a way to drive significant change.
- Beau mentions the necessity of not relying on existing political structures to bring about transformation.
- The message underscores that changing American foreign policy starts with domestic changes within the United States.
- Beau likens altering American foreign policy to a game of world domination, requiring networks and bases of power.

# Quotes

- "Power at the local level because that's what's going to give you that deep systemic change."
- "When you actually get to the point where you are really trying to do more than just mitigate the effects of American foreign policy but you're actually trying to change it. You are literally engaging in a game of world domination."
- "It almost always is. The hardest part to really wrap your mind around is that it is going to be the last thing fixed."

# Oneliner

Beau stresses community networks and local power as the key to changing American foreign policy, which must start with altering American society.

# Audience

Activists, Community Organizers

# On-the-ground actions from transcript

- Organize at the local level to build power and create systemic change (suggested)
- Initiate community networks to drive transformation within American society and eventually impact foreign policy (exemplified)
- Avoid relying solely on political parties; establish parallel structures outside of existing systems to effect change (implied)

# What's missing in summary

The full transcript provides detailed insights into the necessity of grassroots organizing and community empowerment to drive systemic change within the United States, leading to eventual shifts in American foreign policy.

# Tags

#AmericanForeignPolicy #CommunityOrganizing #LocalPower #SystemicChange #GrassrootsMovement