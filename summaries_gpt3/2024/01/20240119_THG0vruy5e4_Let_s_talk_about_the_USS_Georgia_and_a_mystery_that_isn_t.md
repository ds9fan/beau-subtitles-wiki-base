# Bits

Beau says:

- USS Georgia, Ohio class sub, had the Blue Crew captain relieved of duty for "loss of confidence."
- Sheriff's Department in Camden County revealed the reason as a DUI and improper lane change.
- DUI is a significant issue in high-ranking positions of responsibility.
- Navy likely won't reveal the exact reason immediately, waiting for everything to play out.
- It's probable that the DUI incident is the simple explanation.
- People were expecting a more complex reason, but it seems straightforward.
- Having two crews means the impact on readiness is minimal.
- Losing one person from the crew can easily be managed within a military command structure.
- The incident isn't likely to have a major effect overall.
- The mystery surrounding the captain's relief of duty has been solved with a DUI being the probable cause.

# Quotes

- "DUI is a huge deal. Huge."
- "People were probably looking for something a little bit more Tom Clancy-ish."
- "They are certainly capable of losing one person and somebody else being sucked up into that position."

# Oneliner

The captain of the USS Georgia's Blue Crew was relieved of duty due to a DUI, revealing a straightforward explanation amidst expectations of a more complex mystery.

# Audience

Military personnel, Navy enthusiasts.

# On-the-ground actions from transcript

- Contact organizations supporting responsible drinking (implied).

# Whats missing in summary

More context on the specific impact within the USS Georgia crew and potential consequences for the captain.

# Tags

#USSGeorgia #Military #DUI #Navy #Responsibility