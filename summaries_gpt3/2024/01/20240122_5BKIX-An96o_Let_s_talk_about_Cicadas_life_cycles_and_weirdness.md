# Bits

Beau says:

- Talks about cicadas and the upcoming event of two large broods emerging at the same time.
- Explains the life cycle of cicadas, from eggs being laid on trees to the nymphs falling to the ground and going underground for several years.
- Mentions that this event, where billions of cicadas will emerge, happens every 221 years.
- Specifies that this phenomenon will be most observable in Iowa, Illinois, and Missouri.
- Describes how the male cicadas attract female cicadas through singing.
- Emphasizes that the mass emergence of cicadas at the same time guarantees the species' survival.
- Assures that although it may be a strange and unique occurrence, it won't turn into a horror movie scenario.
- Mentions that similar unusual events in nature might occur throughout the year.
- Concludes by wishing everyone a good day and suggesting to embrace and get used to these natural phenomena.

# Quotes

- "This is going to be most observable in Iowa, Illinois, and Missouri."
- "There are so many of them out, like this year there will be literally billions."
- "It kind of guarantees the reproduction of the species."
- "I'm pretty certain that it won't turn into some horror movie."
- "There's gonna be more stories like this throughout the year anyway."

# Oneliner

Beau explains the unique event where billions of cicadas will emerge in Iowa, Illinois, and Missouri, ensuring their species' survival without turning into a horror movie, part of many unusual occurrences in nature this year.

# Audience

Nature enthusiasts

# On-the-ground actions from transcript

- Observe and appreciate the extraordinary phenomenon of mass cicada emergence in Iowa, Illinois, and Missouri (suggested).

# Whats missing in summary

The detailed scientific aspects of the cicada life cycle and the potential impact on the environment and other species. 

# Tags

#Cicadas #Nature #Science #Phenomenon #Iowa #Illinois #Missouri