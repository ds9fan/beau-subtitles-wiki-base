# Bits

Beau says:

- The United States and China are entering a new space race, despite some involved not viewing it that way.
- NASA's Artemis program aims to return humanity to the moon, with Artemis 2 delayed until 2025 and Artemis 3 until 2026.
- Politicians seem more concerned about timelines and national identity than crew safety.
- China has its own moon program, expected to reach the moon by 2030, but they often exceed their timelines.
- American politicians are framing this space competition as a matter of national importance, reminiscent of Cold War games.
- Scientists and astronauts involved prioritize safety and peaceful exploration, rather than competitive space races.
- Expect a surge in propaganda pushing high stakes and national pride as the competition escalates.

# Quotes

- "The United States and China are entering a new space race."
- "Politicians seem more concerned about timelines and national identity than crew safety."
- "Scientists and astronauts prioritize safety and peaceful exploration."

# Oneliner

The US and China are in a new space race, with politicians prioritizing national identity over crew safety, while scientists advocate for peaceful exploration.

# Audience

Space enthusiasts

# On-the-ground actions from transcript

- Stay informed about developments in space exploration (suggested)
- Support initiatives promoting safe and peaceful space exploration (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the emerging space competition between the US and China, touching on political motivations, safety concerns, and the perspective of scientists and astronauts involved.

# Tags

#SpaceRace #ArtemisProgram #US #China #SpaceExploration