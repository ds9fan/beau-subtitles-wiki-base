# Bits

Beau says:

- Japan was hit with a series of major earthquakes, the largest being 7.6 on New Year's Day.
- The earthquakes were shallow, intensifying their impact, with Tokyo about 180 miles away feeling the tremors.
- Thousands of homes have been destroyed, with the human loss currently at around 50.
- Over 3,000 first responders are on the ground, including Japan's military forces.
- A high likelihood of aftershocks poses a serious threat to those trapped in collapsed buildings.
- The weather forecast of rain adds to the challenges of the rescue operation.
- International assistance has been offered, but it seems that everything possible is already being done.
- The focus is on rescuing as many people as possible before conditions worsen.
- Beau stresses the importance of being prepared for natural disasters wherever you are.
- Preparation is key to ensuring you have what you need in case of an emergency.

# Quotes

- "Make sure you have your stuff together."
- "It is a race against time."
- "They're doing everything they can."
- "This is them trying to get as many people out as they can."
- "Thousands of homes have been destroyed."

# Oneliner

Beau provides updates on Japan's earthquake situation, stressing the urgency of rescues amidst challenges like aftershocks and bad weather.

# Audience

Emergency preparedness advocates

# On-the-ground actions from transcript

- Ensure you have emergency supplies and a plan in place (suggested)

# Whats missing in summary

Importance of staying informed and supporting relief efforts

# Tags

#Japan #Earthquake #EmergencyPreparedness #NaturalDisaster #HumanitarianAid