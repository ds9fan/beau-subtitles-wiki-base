# Bits

Beau says:

- Contrasts Wisconsin's map situation with Louisiana's smooth process, noting Wisconsin's Republicans engaging in underhanded tactics.
- Wisconsin faces a deadline to create new maps, which seems unlikely to be met.
- Republicans in Wisconsin made minimal changes to a proposal, with the governor expected to veto it.
- Republicans seemingly adjusted the map to avoid incumbents facing each other, prioritizing party power over fairness.
- The court will likely have to intervene in Wisconsin's map situation, leading to a map Republicans will oppose.
- The state's long history of gerrymandering means a fair map will upset those accustomed to privilege.
- Democrats are taking a moral stance due to gerrymandering, knowing any court-given map will be better than what they have now.
- Democratic inflexibility appears idealistic but is pragmatic given the likely court outcome.
- Expect a resolution once the court intervenes, as they want a new map in use promptly.

# Quotes

- "When you have become accustomed to privilege for so long, equality seems like tyranny."
- "They want fair maps for our people and we're not going to compromise an inch."
- "A fair map comes out, they are going to be super upset."
- "They're going to come out ahead."
- "Y'all have a good day."

# Oneliner

Contrasting Wisconsin's map turmoil with Louisiana's smooth process, Beau exposes underhanded Republican tactics, impending court intervention, and Democratic pragmatism in the face of gerrymandering.

# Audience

Voters, Activists

# On-the-ground actions from transcript

- Contact local representatives to express support for fair redistricting (implied).
- Stay informed about court decisions regarding Wisconsin's maps (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics and strategies behind redistricting in Wisconsin, offering insights into the impact of gerrymandering on fairness and party power struggles.

# Tags

#Wisconsin #Redistricting #Gerrymandering #RepublicanParty #DemocraticParty