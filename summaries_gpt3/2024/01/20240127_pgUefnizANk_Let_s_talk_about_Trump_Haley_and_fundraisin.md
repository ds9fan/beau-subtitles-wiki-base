# Bits

Beau says:

- Trump expresses concern about Haley's ability to raise money and claims that those who contribute to her will be barred from the MAGA camp.
- Trump's attempt to dissuade donors from supporting Haley backfires, resulting in a significant influx of donations to her.
- The response on social media to Trump's actions was largely mocking, indicating a growing skepticism towards his tactics.
- Despite Trump's efforts to maintain control over donors, substantial amounts of money poured into Haley's campaign.
- Wealthier Republican donors appear to be distancing themselves from Trump, while his core base remains loyal.
- Trump's strategy to bully donors into giving him money does not achieve the intended outcome, leading to a loss of influence among wealthier supporters.

# Quotes

- "Anybody that makes a contribution to Birdbrain from this moment forth will be permanently barred from the MAGA camp."
- "The former president did not get the desired result which was to scare people and bully people."
- "People are starting to see through his antics a little bit more."
- "We don't want to have anything to do with your MAGA camp."
- "The wealthy ones, they will walk."

# Oneliner

Trump's attempt to dissuade donors backfires as wealthier supporters distance themselves, leading to a surge in contributions to Haley's campaign and mocking on social media.

# Audience

Political donors, activists, voters

# On-the-ground actions from transcript

- Support political candidates financially (exemplified)
- Engage in social media activism by sharing donation posts (implied)

# Whats missing in summary

Insights on the impact of shifting donor loyalty and the evolving dynamics within the Republican party.

# Tags

#Trump #Money #PoliticalFundraising #RepublicanParty #Donors