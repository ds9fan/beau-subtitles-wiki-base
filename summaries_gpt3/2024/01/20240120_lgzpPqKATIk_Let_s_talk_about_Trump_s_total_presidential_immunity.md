# Bits

Beau says:

- Analyzing Trump's statement on total presidential immunity.
- Trump claims that without total immunity, every president will be indicted, but this immunity doesn't exist.
- Only three presidents have been criminally investigated: Nixon, Clinton, and Trump himself.
- Trump's own history contradicts his claim about constant indictments without total immunity.
- The attempt to establish total presidential immunity goes against the American principle of government being responsive to the people.
- Creating presidential immunity akin to creating another king, contrary to the checks and balances system.
- Trump's desire for immunity may stem from a possible self-coup attempt to stay in power.
- The notion of a president being above the law is fundamentally un-American.

# Quotes

- "The idea that a president should be totally above the law is the most anti-American sentiment that could possibly exist."
- "Creating presidential immunity is akin to creating another king."
- "His claim, his reason for it having to exist, is disproven by history."

# Oneliner

Analyzing Trump's claim of total presidential immunity and its contradiction with history, revealing the anti-democratic sentiment behind it.

# Audience

Legal scholars, activists

# On-the-ground actions from transcript

- Challenge authoritarian actions in your community (implied).
- Advocate for accountability and checks and balances within government (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of Trump's unfounded claim of total presidential immunity and its implications for democracy.