# Bits

Beau says:

- Addressing the Star Wars and Fox controversy, focusing on a different aspect that is often overlooked.
- Advocating for the importance of making people uncomfortable through science fiction.
- Explaining how science fiction serves as a platform to address real-world issues and provoke critical thinking.
- Emphasizing that good science fiction challenges societal norms and biases.
- Drawing parallels between iconic sci-fi works like Star Wars, Star Trek, and Avatar in addressing societal issues.
- Asserting that science fiction should not just be about lasers and spacecraft, but about examining unresolved human issues.
- Arguing that discomfort in consuming science fiction is necessary to draw attention to underlying problems in society.
- Stating that creators who make audiences uncomfortable with their work are vital in running science fiction shows.
- Stressing the importance of incorporating moral lessons and impactful moments in science fiction storytelling.
- Concluding with a thought on the essence of science fiction and its purpose beyond commercial success.

# Quotes

- "Science fiction is supposed to make you feel uncomfortable."
- "Good science fiction will make you feel uncomfortable."
- "It's examining not just lasers and spacecraft. It's examining an issue that humanity really hasn't come to terms with yet."
- "You need the actual lesson. You need the moral."
- "You need that moment."

# Oneliner

Beau advocates for discomfort-inducing science fiction to challenge societal norms and provoke critical thinking, stressing the importance of incorporating moral lessons in storytelling.

# Audience

Science fiction enthusiasts

# On-the-ground actions from transcript

- Watch and support science fiction works that challenge societal norms and make you uncomfortable (implied).
- Engage in critical analysis and reflection on the societal themes presented in science fiction media (implied).

# Whats missing in summary

The full transcript provides a nuanced perspective on the purpose of science fiction beyond entertainment, encouraging viewers to embrace discomfort for meaningful reflection on societal issues.

# Tags

#ScienceFiction #SocialCritique #ChallengingNorms #MoralLessons #CriticalThinking