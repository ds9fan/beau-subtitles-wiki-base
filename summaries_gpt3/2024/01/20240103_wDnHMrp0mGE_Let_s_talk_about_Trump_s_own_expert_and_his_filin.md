# Bits

Beau says:

- Trump's recent filing lacks substance and fails to address key points raised by Smith and amicus briefs.
- Ken Block, Trump's expert hired to find voter fraud evidence, stated there was none, directly contradicting Trump's claims.
- Block's findings were shared with Mark Meadows, presenting a challenge for Trump.
- Block emphasized that voter fraud claims undermining democracy should be based on evidence, not falsehoods.
- Block advocates for addressing systemic weaknesses like gerrymandering to ensure fair elections.
- The expert suggests creating national infrastructure for elections and ending gerrymandering.
- Trump's team's own expert's suggestions go against the narrative of election fraud.
- Legal channels on YouTube likely to cover the issue extensively.
- The filing in question is 41 pages long but lacks new substantial information.
- Trump's persistence in false claims despite being informed of no fraud by his chosen investigator.

# Quotes

- "The expert that they chose flat out that there was no fraud."
- "Maintaining the lies undermines faith in the foundation of our democracy."
- "Leveling the playing field, ending gerrymandering, and creating national infrastructure for the elections. That's his suggestion."
- "It's wild to me."
- "Trump's statements persist to this day anyway."

# Oneliner

Trump's expert refutes voter fraud claims, undermining his own narrative while advocating for fair elections and national election infrastructure.

# Audience

Legal analysts, political commentators

# On-the-ground actions from transcript

- Contact legal channels on YouTube for in-depth analysis and commentary on the filing (implied).

# Whats missing in summary

Analysis on the potential impact of Trump's persistence in false claims despite evidence against them.

# Tags

#Trump #VoterFraud #ElectionIntegrity #LegalAnalysis #Gerrymandering