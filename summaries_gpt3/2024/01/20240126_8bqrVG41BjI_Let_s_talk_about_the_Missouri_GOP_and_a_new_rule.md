# Bits

Beau says:

- Missouri Senate rules change proposed due to confrontations within the Republican Party.
- Proposed rule change involving dueling as a satirical commentary on lack of civility.
- Duel challenge can be issued if a senator's honor is impugned beyond repair.
- Details of the duel process include choice of weapons and terms agreed upon by both senators.
- Beau questions the practicality and tradition of dueling inside the Senate.
- Suggests dueling should occur outside at dawn with an option for the seconds to negotiate a truce.
- Acknowledges that while dueling is dumb and immature, it could potentially shift rhetoric in the Republican Party.
- Points out the historical context of dueling and how it was once a common practice in the US.
- Beau believes the proposed dueling rule is a sharp rhetorical device but does not endorse its actual implementation.
- Expresses concern about the violent rhetoric in politics and how this proposed rule change could potentially alter behaviors.

# Quotes

- "Yeah, the Republican party's doing great."
- "If this was actually the rule, I feel like that rhetoric would change real quick."
- "But I do think pointing out that those people most likely to use that kind of rhetoric would never do it themselves, I think there's some value in that."

# Oneliner

Missouri Senate proposed a satirical dueling rule to address GOP infighting, sparking debate on political civility and rhetoric.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Stay informed about political developments and proposed rule changes in your area (implied)
- Engage in constructive political discourse and advocate for civil interactions (implied)

# Whats missing in summary

The full transcript provides historical context on dueling and raises awareness about the potential impact of aggressive rhetoric in politics.

# Tags

#Missouri #Senate #RepublicanParty #PoliticalCivility #Rhetoric #Dueling