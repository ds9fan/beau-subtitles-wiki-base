# Bits

Beau says:

- Mexican officials witnessed migrants in distress in the river and contacted Border Patrol for help.
- Border Patrol tried to access the river at Shelby Park but was stopped by the Texas Military Department.
- Texas Military Department sent people with night vision and lights to the river, saw Mexican officials responding, and called off their search.
- A woman and two children drowned in the Shelby Park area after Border Patrol was physically barred by Texas officials from entering.
- Texas engaged in publicity stunts under state control, separate from federal operations handling immigration.
- The lack of coordination between the two chains of command led to tragedy.
- The Biden administration asked the Supreme Court to intervene after Texas took over the area, but there has been no action yet.
- The incident resulted in unnecessary loss of life due to a mix of publicity stunts and poor communication.

# Quotes

- "Chains of command, plural."
- "A woman and two children because of what amounts to a publicity stunt and poor communication."
- "The two chains of command are obviously not functioning properly."

# Oneliner

Mexican officials call Border Patrol for help, but Texas officials' interference leads to tragedy, exposing a lack of coordination between state and federal chains of command.

# Audience

Border Patrol officials

# On-the-ground actions from transcript

- Contact Border Patrol for assistance if witnessing distress in border areas (implied)
- Advocate for improved communication and coordination between state and federal agencies to prevent tragedies (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the events leading to a tragic loss of life at the border due to a lack of coordination between different chains of command.