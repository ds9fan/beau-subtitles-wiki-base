# Bits

Beau says:

- E. Jean Carroll was awarded over 80 million dollars by a jury in a case against Trump.
- Carroll plans to use some of the money to buy premium dog food as a splurge.
- She also wants to use the money to support causes that Trump dislikes.
- Carroll's goal is to donate the money to things that will cause Trump pain.
- One suggested cause is a fund for women who have been assaulted.
- Despite wanting to make a joke about Trump hating him, Beau found Carroll's ideas for the money to be very worthy.
- Carroll's actions have the potential to create more support for causes due to her profile and the animosity she faced.
- Beau admires Carroll's decision to prioritize doing good with the money over just the bare minimum.
- Carroll's character shines through in wanting to use the money for positive impact.
- Beau sees Carroll's actions as inspiring and hopes to reach that level of giving back.

# Quotes

- "I'm going to donate it and give it away to things that will cause you pain."
- "She can show up to fundraisers and people will show up and support the cause because of that."
- "Carroll's character shines through in wanting to use the money for positive impact."

# Oneliner

E. Jean Carroll plans to use the money awarded by a jury, including buying premium dog food and supporting causes Trump dislikes, showcasing a desire to do good and create positive change.

# Audience

Supporters of social justice causes

# On-the-ground actions from transcript

- Support funds for women who have been assaulted by donating or volunteering (implied)
- Attend fundraisers for causes you believe in to show support (implied)

# Whats missing in summary

The emotional impact of using money awarded from a difficult situation to create positive change.

# Tags

#EJeanCarroll #SocialJustice #PositiveChange #SupportCauses #Activism