# Bits

Beau says:

- Explains the Biden administration's push to revamp the way banks handle overdraft fees.
- Describes the current system where if you use your debit card without enough money, banks charge an overdraft fee of around $20 to $40.
- Mentions Biden administration's proposals to limit the fees to what it costs the banks to process overdrafts, possibly around $3 to $14.
- Points out that major US banks make about $8 billion annually from overdraft fees, disproportionately affecting those with limited financial resources.
- Emphasizes that this proposed change aims to provide relief to those struggling financially.
- Notes that there will be periods for industry and public commentary on the proposed changes.
- Acknowledges that despite potential pushback, it seems like the changes are already in progress, with details still being finalized.

# Quotes

- "Some of the wealthiest, most profitable institutions in the United States take billions of dollars per year from people that don't have any money."
- "The Biden administration is trying to find a way to curtail this."
- "The goal here is to just provide a little bit of relief."
- "It's not like this is going to seriously address income inequality or anything like that, but it's a little bit of relief."
- "It's probably not going to get the coverage it should."

# Oneliner

Beau explains Biden's push to revamp overdraft fees, aiming to provide relief to those financially vulnerable, despite pushback and lack of media coverage.

# Audience

Banking consumers

# On-the-ground actions from transcript

- Contact local representatives to voice support for limiting overdraft fees (implied)
- Participate in industry and public commentary periods on the proposed changes (implied)

# Whats missing in summary

Details about the potential impacts of overdraft fee changes on individual consumers and communities.

# Tags

#Banking #OverdraftFees #BidenAdministration #FinancialRelief #CommunityImpact