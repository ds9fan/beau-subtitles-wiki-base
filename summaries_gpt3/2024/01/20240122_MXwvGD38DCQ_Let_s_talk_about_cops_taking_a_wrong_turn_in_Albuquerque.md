# Bits

Beau says:

- Recent developments point to a major issue within the Albuquerque Police Department related to DWIs and DUIs.
- A federal investigation is ongoing within the department, focusing on cops heavily involved in DWI arrests.
- Federal authorities have searched the homes of at least three cops and a law office, leading to the dismissal of 152 DWI-related cases.
- Speculation surrounds the nature of the investigation, including possibilities like an overtime scam or issues with calibration.
- There is a clear connection between the federal corruption investigation, Albuquerque Police Department, and the DWIs/DUIs.
- Public statements are anticipated in the coming week to shed light on the situation's depth and scope.
- Statements released so far have been cryptic, hinting at a stain on the police department without clear details.
- Questions remain unanswered regarding the reasons behind case dismissals and the extent of the issue.
- Clarity is expected soon as explanations are likely to be provided.
- The situation hints at ongoing developments that may reveal more about the issue.

# Quotes

- "My prosecutorial ethics demanded that I dismiss these cases."
- "It's going to be a stain on the police department."
- "There is some overlap between a federal corruption investigation, Albuquerque Police Department, and DUIs, DWIs."

# Oneliner

Recent developments indicate a major issue within the Albuquerque Police Department related to DWI arrests, prompting a federal investigation and raising questions about departmental integrity.

# Audience

Albuquerque residents

# On-the-ground actions from transcript

- Contact local authorities or community organizations for updates and ways to support needed reforms (suggested).
- Stay informed about the situation through local news sources and community updates (implied).

# Whats missing in summary

Full details and context on the ongoing federal investigation within the Albuquerque Police Department and its implications.

# Tags

#Albuquerque #PoliceDepartment #FederalInvestigation #DWI #DUI #Corruption