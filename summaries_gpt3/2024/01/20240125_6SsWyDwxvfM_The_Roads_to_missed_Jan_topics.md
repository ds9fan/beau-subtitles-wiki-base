# Bits

Beau says:

- Feeling under the weather, Beau does a short Q&A session addressing news events due to inability to do long format content without coughing.
- Reports on a disturbing story about a pauper's grave site in Jackson, Mississippi with hundreds of unmarked graves where next of kin were not notified.
- Civil rights attorney Ben Crump is involved in investigating the pauper's cemetery.
- Teases a future video on Russia's possible claim on Alaska as retaliation for US's stance on Ukraine.
- Addresses misconceptions about Biden's involvement in Ukraine, directing viewers to a video for a timeline breakdown.
- Mentions Iran's vote in favor of a two-state solution at the UN and its significance in foreign policy dynamics.
- Talks about potential defensive moves in Europe and NATO in response to international tensions.
- Explains the interim ruling by the ICJ and clarifies it's not the final ruling in a case.

# Quotes

- "Hundreds of unmarked graves where next of kin were not notified."
- "It's so much worse than it sounds."
- "The reality is what Vice President Biden did in Ukraine reopened an investigation."

# Oneliner

Beau addresses disturbing news about unmarked graves, clarifies misconceptions on Biden and Ukraine, and hints at future videos on international tensions.

# Audience

Viewers interested in current events and political analysis.

# On-the-ground actions from transcript

- Stay informed about international tensions and political developments (implied).

# Whats missing in summary

Exploration of the geopolitical implications and potential consequences of international tensions discussed.

# Tags

#CurrentEvents #BenCrump #Russia #Ukraine #Biden #Iran #ICJ #Beau