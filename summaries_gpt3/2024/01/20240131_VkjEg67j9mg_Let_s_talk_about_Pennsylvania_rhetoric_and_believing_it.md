# Bits

Beau says:

- Pennsylvania and rhetoric are discussed, focusing on where the rhetoric comes from and who believes it.
- A man in Pennsylvania was arrested after making a video referencing the Biden regime, an army of immigrants, and a civil war.
- The idea of an invasion is fabricated to scare and manipulate people, as there is no actual invasion occurring.
- The Republican Party is voting against border closures because the fabricated invasion serves as a tool for manipulation.
- Politicians and pundits use inflammatory rhetoric to manipulate and control people, appealing to inflated patriotism or fear.
- Rich individuals historically use inflammatory rhetoric to manipulate and control the masses for their own benefit.
- Right-wing outlets create rhetoric to rile people up and instill fear, ultimately aiming to control and manipulate them.

# Quotes

- "There is no invasion. They made that up."
- "They played you for a fool."
- "They do not have your best interest at heart."
- "They don't believe the same things you do."
- "They don't care about you, and they never did."

# Oneliner

Beau breaks down the fabricated rhetoric surrounding Pennsylvania, exposing manipulative tactics used by politicians to control and scare people.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Fact-check political rhetoric (implied)
- Challenge fear-mongering narratives in your community (exemplified)

# Whats missing in summary

The full transcript provides deeper insights into the manipulation tactics used by politicians and the consequences of falling for fabricated rhetoric.