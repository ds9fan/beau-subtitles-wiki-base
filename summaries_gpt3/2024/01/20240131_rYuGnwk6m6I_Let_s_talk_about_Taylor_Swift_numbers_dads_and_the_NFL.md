# Bits

Beau says:

- The right wing in the United States has launched a "holy war" against Taylor Swift, despite not being able to articulate a specific reason for their disdain.
- Taylor Swift's presence at a Chiefs game resulted in the highest ratings of the week and the second-highest of the season.
- The game attracted the highest ratings among women aged 12 to 49, with a significant increase in viewership among 12 to 17-year-old girls on October 1st.
- These young girls are likely watching with their fathers, who are influenced to dislike Swift based on online opinions rather than actual reasons.
- Beau criticizes the individuals who allow internet strangers to dictate their opinions and drive a wedge between themselves and their daughters over cultural battles.
- He draws parallels to the Kavanaugh hearings, suggesting that blind opposition to someone based on external influence can damage relationships irreparably.
- Beau urges reconsideration for those who are willing to jeopardize their relationships over trivial matters like a celebrity's political stance.
- He condemns the notion of hating Taylor Swift simply because she encouraged people to register to vote, portraying blind obedience to online hate as damaging and unpatriotic.

# Quotes

- "If you find yourself opposing America's sweetheart because some loser you never met told you to, you might want to rethink things."
- "You have no reason to hate her. They told you to hate her and you obeyed like an obedient lackey."
- "But yeah, you're a real patriot."
- "That might be the single most pathetic thing I have ever heard of."
- "Y'all have a good day."

# Oneliner

The right wing's unwarranted "holy war" against Taylor Swift reveals blind obedience to online hate, damaging relationships and patriotism in the process.

# Audience

Parents, voters, Swift supporters

# On-the-ground actions from transcript

- Have genuine, open dialogues with your children about celebrities and political views (implied).
- Encourage critical thinking and independent opinions within your family (implied).
- Refrain from blindly following online hate and instead foster healthy, respectful communication with your loved ones (implied).

# Whats missing in summary

The full transcript expands on the damaging effects of blind online influence and the importance of critical thinking in familial relationships.

# Tags

#TaylorSwift #OnlineHate #Politics #Parenting #Relationships