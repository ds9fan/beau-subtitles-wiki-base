# Bits
Beau says:

- China and Russia have a special relationship described as a "friendship without limits."
- China's interest in banking interest has led them to limit ties with sanctioned entities in Russia.
- The US authorizing secondary sanctions has influenced China's decision to distance itself from Russia.
- China's economy is stumbling, not collapsing, and it does not need further economic issues with the US.
- China's actions, though potentially helpful to Russia, may not have a huge impact beyond public perception.
- This situation illustrates that even countries with longstanding friendships have limits due to differing interests in foreign policy.

# Quotes
- "Countries don't have friends. They have interests."
- "Even if countries have a relationship of friendship for years and years, there are limits regardless of rhetoric."

# Oneliner
China's distancing from Russia due to US sanctions shows countries prioritize interests over friendships, even in longstanding relationships.

# Audience
Foreign Policy Analysts

# On-the-ground actions from transcript
- Monitor the evolving relationship between China and Russia to understand shifting geopolitical dynamics (implied).
- Stay informed about the impact of US sanctions on international relations (implied).

# What's missing in summary
The full transcript provides more in-depth analysis on the implications of China's actions on their relationship with Russia and the broader geopolitical landscape.