# Bits

Beau says:

- Explains the misinformation circulating regarding the UN Arms Trade Treaty and the Biden administration's alleged plans to re-sign it.
- Clarifies that the treaty regulates illegal international transfer of conventional arms, not American citizens buying guns domestically.
- Trump's decision to "unsign" the treaty was purely political as the treaty was never ratified.
- Emphasizes that the treaty does not impact the majority of Americans unless they are involved in illegal international arms sales.
- States that the treaty affirms a country's sovereign right to regulate arms within its territory and does not involve the UN coming to enforce it.
- Speculates that the treaty is back in focus due to the Democrats being in power, not because it affects domestic gun ownership.
- Points out that the treaty covers a range of conventional arms, from rifles to tanks, focusing on illegal international transfers.

# Quotes

- "It has somehow been suggested that the Biden administration plans to re-sign a treaty from the UN."
- "This whole thing is a talking point designed to make people believe that somehow this treaty from the UN has something to do with Americans buying guns."
- "So much so that in the treaty it says that it reinforces and affirms the sovereign right of any state to regulate and control conventional arms exclusively within its territory."
- "Trump unsigning it did absolutely nothing. That's not an actual diplomatic thing."
- "Realistically, this applies to all conventional arms. So rifles and pistols, all the way up to tanks and aircraft."

# Oneliner

Beau clarifies the misconceptions around the UN Arms Trade Treaty, stressing its focus on illegal international arms transfers and its lack of impact on domestic gun ownership.

# Audience

Policy Advocates

# On-the-ground actions from transcript

- Contact policymakers to advocate for accurate information dissemination about international treaties (suggested).

# Whats missing in summary

Beau's detailed explanation and debunking of misinformation around the UN Arms Trade Treaty can be best understood by watching the full transcript.

# Tags

#UNArmsTradeTreaty #Misinformation #GunRegulation #InternationalArmsTransfers #PolicyAnalysis