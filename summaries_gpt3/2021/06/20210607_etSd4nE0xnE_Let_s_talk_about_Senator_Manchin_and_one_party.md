# Bits

Beau says:

- Senator Manchin refuses to support the For the People Act or end the filibuster for voting protections.
- Beau compares Manchin's behavior to a Trump supporter for rejecting reality.
- Manchin believes partisan voting legislation will harm democracy, justifying his opposition to the For the People Act.
- Beau argues that American democracy is not a both sides issue and one party has shown intent to undermine it.
- Compromising on preserving democracy is aiding in its undermining, as one party clearly does not support it.
- Beau stresses that there can be no compromise on preserving Americans' voting rights.
- He points out that one party is actively working to undermine voting rights and American democracy.
- Beau criticizes the idea of bipartisan compromise when one party is actively against preserving democracy.
- He likens compromising on democracy to aiding in its erosion, drawing a clear line on preserving voting rights.
- Beau warns that without courage from senators like Manchin, the country may end up with one dominant party.

# Quotes

- "If your goal is to preserve American democracy and theirs is to undermine it, if you compromise, you are now assisting in undermining it."
- "You are either pregnant or you're not. There's not a lot of gray area here."
- "It doesn't matter how many stakes you throw to that tiger, it will not turn into a vegetarian."
- "There is one party responsible for this."
- "If the senators like Manchin and others of his sort do not find their courage, we will end up with one party in this country."

# Oneliner

Senator Manchin rejects voting protection acts, Beau argues compromising on democracy aids in its undermining by one party.

# Audience

Voters, democracy advocates

# On-the-ground actions from transcript

- Contact Senator Manchin to express support or opposition to his stance on voting protections (suggested)
- Join local democracy advocacy groups to actively work towards preserving voting rights (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Senator Manchin's stance on voting protections and the implications of compromising on democracy in the face of one party's efforts to undermine it.

# Tags

#SenatorManchin #VotingRights #Democracy #Compromise #PoliticalAnalysis