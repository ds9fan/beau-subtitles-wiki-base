# Bits

Beau says:

- Addresses the controversy around G.I. Joe becoming "woke" with diverse representation.
- Points out that G.I. Joe has always been inclusive with characters of different races and genders.
- Mentions the introduction of Russian characters during the Cold War era.
- Talks about the Eco Warriors released in 1991 who fought pollution, resembling trans Joe's fighting climate change.
- Explains how G.I. Joe resonated with a generation due to relatable characters from diverse backgrounds.
- Argues that embracing diversity was a strength that built the G.I. Joe teams.
- Criticizes the idea of provoking outrage over Snake Eyes being portrayed as Asian.

# Quotes

- "G.I. Joe has always been really woke."
- "The reason G.I. Joe resonated with an entire generation is because there was a character that was relatable to everybody."
- "It's sad that you missed that."
- "The whole point was to embrace diversity."
- "I don't think anybody is going to care that Snake Eyes went from white to Asian."

# Oneliner

Beau explains G.I. Joe's long-standing diversity and inclusivity, challenging the controversy around its "wokeness" and urging to embrace diversity for strength.

# Audience

Pop culture enthusiasts

# On-the-ground actions from transcript

- Watch and support media that embraces diversity and inclusivity (implied)

# Whats missing in summary

Beau's engaging storytelling and passionate defense of G.I. Joe's inclusive history can be best appreciated by watching the full transcript.

# Tags

#G.I.Joe #Diversity #Inclusivity #PopCulture #Representation