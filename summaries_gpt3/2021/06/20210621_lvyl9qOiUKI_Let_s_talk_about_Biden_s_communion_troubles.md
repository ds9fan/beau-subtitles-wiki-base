# Bits

Beau says:

- Beau was called out for not discussing the church denying communion to Biden, being reminded of his unique approach to talking about religion without ridiculing religious people.
- Beau typically speaks out against churches when they preach harm or attempt to use state power to enforce beliefs, supporting the separation of church and state.
- Regarding the church denying communion to Biden, Beau believes it is an internal church matter that doesn't involve the government.
- Beau expresses that he wouldn't argue for a religious institution to provide a religious right to a government official like President Biden, as it could violate the separation of church and state.
- While Beau sees the denial of communion to Biden as counter to the idea of communion itself, he defers to Reverend Ed Trevers for a theological perspective.
- Beau criticizes the church's move politically, believing it will alienate members and set a bad precedent for the church to make more political stances.
- He points out the hypocrisy in the church's decision, suggesting it might drive members away and encourage conservative demands for political involvement.
- Beau views the church's decision as a political stunt that will lead to less support and cause doubt among members.
- Despite his disagreement with the church's move, Beau concludes that it's not something he typically speaks out against since it's contained within the church and doesn't involve enforcing beliefs through state violence.
- Beau shares his thoughts and wishes his audience a good day.

# Quotes

- "That's not my business."
- "I don't think it's a good move politically."
- "It's a political stunt, in my opinion."

# Oneliner

Beau believes the church denying communion to Biden is an internal matter, critiquing it politically while maintaining it's not his usual cause for speaking out against churches.

# Audience

Religious commentators

# On-the-ground actions from transcript

- Watch Reverend Ed Trevers' video for a theological perspective on the situation (Suggested)
- Encourage open dialogues within religious communities about the intersection of religion and politics (Implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on the church denying communion to Biden, discussing the implications within the church and politically with a focus on separation of church and state.

# Tags

#Church #Religion #SeparationOfChurchAndState #PoliticalStance #Hypocrisy #CommunityActions