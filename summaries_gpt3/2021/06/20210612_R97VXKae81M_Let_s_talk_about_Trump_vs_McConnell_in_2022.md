# Bits

Beau says:

- Trump is intervening in primaries related to the Senate even though he's out of office.
- McConnell is pursuing his own interests through a PAC called the Senate Leadership Fund, making determinations on who to endorse.
- McConnell's goal is to become Senate Majority Leader again, not necessarily to get rid of Trumpism.
- The PAC may support moderate candidates over Trump loyalists based solely on polling to further McConnell's interests in regaining power.
- McConnell's actions may set up a contentious relationship with Trump, leading to more spending by candidates during the primary season.
- The Republican establishment wants to make 2022 a referendum on President Biden, but Trump's interventions might shift the focus back to himself.

# Quotes

- "Trump is intervening in primaries related to the Senate even though he's out of office."
- "McConnell's goal is to become Senate Majority Leader again, not necessarily to get rid of Trumpism."
- "The Republican establishment wants to make 2022 a referendum on President Biden, but Trump's interventions might shift the focus back to himself."

# Oneliner

Trump's interventions in Senate primaries and McConnell's pursuit of power may reshape the political landscape, impacting the focus on President Biden in the upcoming elections.

# Audience

Political observers, voters

# On-the-ground actions from transcript

- Support and get involved with candidates in Senate primaries (implied)
- Stay informed and engaged in political developments to make informed decisions during elections (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the power struggle between Trump and McConnell post-Trump's presidency, offering insights into their motivations and potential impacts on the upcoming elections.