# Bits

Beau says:

- Governors claim people don't want to work, cutting unemployment benefits.
- Mississippi cut $27 million a month in unemployment benefits, impacting 90,000 people.
- Businesses struggling to reach 50% staffing may have other issues besides people not wanting to work.
- Matt Roberts of Shaggy's Biloxi Beach found a solution by raising starting wage to $15/hour and offering benefits.
- Roberts' approach led to full staffing without insulting potential employees.
- People may not be lazy but tired of being exploited.
- States facing economic issues should look at policies and politicians responsible.
- Emphasis on policy over party is necessary for addressing economic challenges.
- Outdated ideas of blaming the poor for economic issues may no longer be politically viable.
- The world is moving into an era of change where old ideas won't suffice.

# Quotes

- "Imagine that it's not people just being lazy, it's just people being tired of being exploited."
- "Maybe it's time to start looking at policy over party."
- "We are moving into an era where there are gonna be a lot of changes."
- "Let's just blame the poor people, that's probably not gonna work much longer."
- "Y'all have a good day."

# Oneliner

Governors cutting unemployment benefits blame lack of work motivation, but a Biloxi business's success by raising wages shows it's exploitation, urging policy over party for economic solutions in changing times.

# Audience

State Residents, Workers

# On-the-ground actions from transcript

- Advocate for policy changes that prioritize fair wages and benefits for workers (implied).
- Support businesses that implement fair wages and benefits for employees (implied).

# Whats missing in summary

The full transcript provides additional context on the impact of outdated policies and the need for a shift towards prioritizing workers' rights and fair compensation.