# Bits

Beau says:

- The lieutenant colonel's mic was cut off during a speech about the origins of Memorial Day, specifically the first observance.
- The first national observance of Memorial Day was in Arlington on May 30, 1868, but the origins go back further to Charleston in 1865.
- Newly freed slaves in Charleston buried Union troops with proper honors and held a parade on May 1, 1865, a year before other observances.
- A Civil War Historical Society in 1916 tried to inquire about this event in Charleston but were met with denial from the Charleston Historical Society.
- The official birthplace designation of Memorial Day in Arlington was made in 1966 for political reasons with little real scholarship.
- The events in Charleston may not have definitively led to Memorial Day observances, but it's hard not to see the connection in spirit.
- Many national holidays have origins that are not widely known or admitted, similar to the unique beginnings of Memorial Day in Charleston.

# Quotes

- "Memorial Day began where the war began in Charleston."
- "It's incredibly hard not to draw that line."
- "The spirit was there."
- "I challenge anybody to find an event that is similar in nature prior to the one in Charleston."
- "You may not be able to draw that direct line via historical standards, but the spirit was there."

# Oneliner

A lieutenant colonel silenced for sharing the untold origins of Memorial Day that began with newly freed slaves honoring fallen troops in Charleston, challenging historical narratives.

# Audience

History enthusiasts, Memorial Day advocates, Civil War scholars

# On-the-ground actions from transcript

- Research and uncover hidden historical narratives (suggested)
- Share stories and histories that challenge traditional narratives (exemplified)

# Whats missing in summary

The emotional impact of uncovering hidden histories and the importance of acknowledging marginalized voices in shaping national observances.

# Tags

#MemorialDay #HiddenHistory #Charleston #CivilWar #HistoricalNarratives