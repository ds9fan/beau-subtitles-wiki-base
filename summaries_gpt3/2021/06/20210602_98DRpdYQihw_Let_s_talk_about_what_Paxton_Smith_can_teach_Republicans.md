# Bits

Beau says:

- Report suggests Republicans are worried about losing the next generation of leaders due to reliance on personality over substance.
- Republican Party has lost the current generation as well.
- Republicans leaned into the idea that facts don't matter and that alternative facts are acceptable.
- Real leaders and ethical people don't want to manipulate others for their base; they want to lead.
- Paxton Smith, a valedictorian in Texas, gave a speech at graduation that challenged legislative laws.
- She took a stand by giving a speech different from the one approved by authorities.
- Her speech focused on keeping laws off her body, met with applause.
- Beau believes if Paxton ran for public office, she wouldn't identify as Republican.
- Republican Party's reliance on appealing to the lowest common denominator and rejecting reality has caused future leaders to look elsewhere.
- People like Paxton may not be Democrats or Republicans, potentially leading in a different way or joining nonprofit organizations.
- Republican Party's actions have pushed the younger generation towards more progressive ideologies due to prioritizing party over policy and personality over country.

# Quotes

- "Strangers in the legislature, how about you keep your laws off my body?"
- "She refused to surrender the platform that she had been given, yeah, that's a leader."
- "She may not be a Democrat, but she won't be a Republican."
- "Republican Party has done more to push the younger crop left than any leftist ever could."
- "Illustrated very clearly the dangers of party over policy or personality over country."

# Oneliner

Reported concerns on losing future leaders due to GOP's focus on personality over substance lead to a valedictorian's impactful speech challenging legislative norms in Texas.

# Audience

Future leaders

# On-the-ground actions from transcript

- Support nonprofit organizations focused on family planning (implied)

# Whats missing in summary

The full transcript provides deeper insights into the consequences of prioritizing party over policy, illustrating a potential shift towards more progressive ideologies among younger generations.

# Tags

#RepublicanParty #FutureLeaders #YouthEmpowerment #ProgressiveIdeologies #PublicSpeaking