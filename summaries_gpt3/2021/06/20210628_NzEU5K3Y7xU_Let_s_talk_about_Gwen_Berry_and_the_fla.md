# Bits

Beau says:

- Gwen Berry drew attention to systemic issues in the US during a national anthem moment, a move unprecedented for athletes.
- Politicians have become more focused on generating outrage rather than policy, using the incident for political gain.
- The flag is described as a piece of cloth symbolizing the ideals of the country, which some politicians have failed to uphold.
- Politicians who have not addressed systemic issues don't get to feign outrage over Berry's actions.
- Berry's actions are aimed at policymakers, urging them to address long-standing issues, which many politicians have ignored for years.
- Rather than uniting the country and addressing real issues, some politicians prefer to stoke division for their own political careers.
- Beau suggests that kneeling may be a more powerful form of protest imagery but respects Berry's choice of protest.
- He questions the credibility of those criticizing Berry's actions, especially if they downplayed violent events involving the flag in the past.
- Beau points out the hypocrisy of using flag etiquette to criticize Berry when real issues are being ignored for political gain.
- He ends by encouraging viewers to think about the message shared and wishing them a good day.

# Quotes

- "That flag, it's a piece of cloth. In and of itself, it means nothing."
- "You don't get to pretend to be mad about this, because this isn't new."
- "Rather than addressing the issues, you are making them worse."

# Oneliner

Beau unpacks Gwen Berry's protest, calling out politicians for prioritizing outrage over policy, exposing their hypocrisy in feigned outrage.

# Audience

Politically Engaged Citizens

# On-the-ground actions from transcript

- Contact policymakers to address systemic issues (implied)
- Participate in peaceful protests for social justice (implied)

# Whats missing in summary

The full transcript provides more context and depth on the importance of addressing systemic issues and the hypocrisy of politicians in the face of protests against injustice. 

# Tags

#GwenBerry #Protests #PoliticalHypocrisy #SystemicIssues #FlagSymbolism