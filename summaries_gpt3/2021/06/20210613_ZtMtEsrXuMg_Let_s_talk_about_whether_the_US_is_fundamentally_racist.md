# Bits

Beau says:

- Addressing the debate on teaching American history through the lens of racism, particularly in relation to Joe Biden and Critical Race Theory (CRT).
- Clarifying that CRT is an academic movement, not specific to Biden, and discussing the fundamentally racist nature of the 1994 crime bill.
- Exploring the concept of the United States being fundamentally racist by examining historical instances of institutionalized racism from the country's founding to the present.
- Describing the long history of structural and institutionalized racism in the U.S., including legislation like the Violent Crime Control and Law Enforcement Act of 1994.
- Arguing that the United States' history is intertwined with racism, making it fundamentally racist according to historical evidence.
- Emphasizing the importance of acknowledging past injustices and using history to prevent repeating discriminatory patterns.
- Drawing parallels between a person's lifespan and the United States' history to illustrate the persistent nature of racism in the country.
- Asserting that denying the history of institutionalized racism in the U.S. hinders progress in addressing present-day racial issues.

# Quotes

- "United States was founded and built on institutionalized racism."
- "You can ban teaching this all you want, but it's reality."
- "History is littered with racism, with structural, institutionalized racism."
- "You learn history so you don't repeat the mistakes."
- "Until people are comfortable with acknowledging the past, we have to assume that there's still a lot of racism."

# Oneliner

Teaching American history necessitates acknowledging its institutionalized racism, from slavery to modern legislation, shaping a fundamentally racist nation.

# Audience

Educators, Historians, Activists

# On-the-ground actions from transcript

- Teach history truthfully and inclusively to acknowledge past injustices and prevent their repetition (implied).
- Advocate for curricula that address the historical reality of institutionalized racism in the United States (implied).

# Whats missing in summary

The full transcript provides a detailed exploration of the historical foundation of institutionalized racism in the United States, urging for an honest reflection on the nation's past to address present-day racial inequalities effectively.

# Tags

#AmericanHistory #InstitutionalRacism #CriticalRaceTheory #JoeBiden #HistoricalTruth