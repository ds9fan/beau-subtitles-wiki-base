# Bits

Beau says:

- President Biden set a goal for 70% of the population to have at least one shot by July 4th.
- The New York Times published a map showing states on target to hit the goal, with a clear gap between southern states.
- Maps comparing party affiliation of governors to vaccination rates show a clear correlation.
- Public health has become a partisan issue, with Republican-led states falling behind in vaccination rates.
- Beau criticizes the Republican Party for failing to lead and putting their own voters at risk.
- Republicans are rejecting objective reality for social media approval, endangering their base.
- Beau predicts a significant difference in COVID-19 losses based on party affiliation.
- He questions the selfishness of writing off their base to appease the former president and downplay the severity of the pandemic.
- Beau urges people to get vaccinated and do their part in combating the public health crisis.

# Quotes

- "Public health has become a partisan issue."
- "Republican Party is still failing to lead."
- "Imagine how selfish you have to be to quite literally write off your own base."
- "Go get your shots. Go get vaccinated."
- "Do your part."

# Oneliner

President Biden set a vaccination goal for July 4th, revealing a partisan divide in vaccination rates and Republican leadership failures, endangering their own base.

# Audience

Public Health Advocates

# On-the-ground actions from transcript

- Get vaccinated and encourage others to do the same (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the correlation between party affiliation and vaccination rates, urging individuals to prioritize public health over political allegiance.

# Tags

#COVID19 #Vaccination #PartisanPolitics #PublicHealth #RepublicanParty