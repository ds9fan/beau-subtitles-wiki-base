# Bits

Beau says:

- A large number of cops in Portland resigned from a specialized team dealing with civil disturbance after one member was indicted.
- The resignations were not just due to the indictment but also because the mayor, judge, and prosecutor criticized the team for using excessive force, including tear gas.
- Beau suggests that if the criticism from multiple officials doesn't prompt policy changes, those cops shouldn't be on the team or even be cops at all.
- The refusal to accept policy changes indicates a sense of being above accountability and the law within the team.
- The decision to quit over charges being brought against a cop in the Terry Jacobs incident shows a lack of willingness to be held accountable.
- Beau argues that if officials are pointing out excessive force, it's time for policy changes that should be accepted as part of the community.
- Resigning when faced with the need for policy changes implies a culture within the team of expecting to act without oversight or accountability.

# Quotes

- "If a judge, the mayor, and the prosecutors are all saying you're using too much force, it's time for policy changes."
- "That to me suggests that the bad apple has already spoiled the bunch."
- "If that is just unimaginable, unthinkable, something that you shouldn't agree to, you probably shouldn't be on this team and you probably shouldn't be a cop."

# Oneliner

A large number of cops in Portland resigned from a specialized team over criticism of excessive force, indicating a culture of avoiding accountability.

# Audience

Community members, Police Department

# On-the-ground actions from transcript

- Advocate for policy changes within law enforcement (implied)
- Support oversight and accountability measures for police (implied)
- Demand transparency in police actions (implied)

# Whats missing in summary

Further insights on the importance of community involvement in holding law enforcement accountable.

# Tags

#PoliceAccountability #ExcessiveForce #CommunityInvolvement #Portland #PolicyChanges