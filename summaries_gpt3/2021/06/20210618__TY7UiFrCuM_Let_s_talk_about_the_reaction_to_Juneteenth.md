# Bits

Beau says:

- Juneteenth becoming a federal holiday sparked predictable responses from the right.
- Despite GOP support for the holiday, some individuals have taken issue with it.
- Charlie Kirk's response criticizes Juneteenth, claiming it undermines the unity of July 4.
- Beau challenges Kirk's view, explaining the complementary nature of July 4 and Juneteenth.
- July 4 symbolizes the proclamation of equality, while Juneteenth marks an effort to fulfill that promise.
- Beau clarifies that Juneteenth is not about race but about a historic event marking progress.
- He points out that Union troops reaching Galveston to inform the last slaves is a significant part of Juneteenth.
- Beau dismisses the notion of competing holidays and stresses their harmonious coexistence.
- He suggests that future holidays may signify moments when more individuals were included in the promises of equality.
- Beau concludes by encouraging reflection on the significance of these historical commemorations.

# Quotes

- "July 4 and Juneteenth, these are not competing holidays. They're pretty complementary."
- "It's marking a historic event where Union troops got to Galveston and got the news to the last slaves."
- "They're not competing holidays. One does not take away from the other. They go together."

# Oneliner

Beau explains the complementary nature of July 4 and Juneteenth, challenging critics and advocating for understanding historical significance.

# Audience

Americans, History Enthusiasts

# On-the-ground actions from transcript

- Celebrate Juneteenth and July 4 together, recognizing their complementary nature (implied).

# Whats missing in summary

Beau's engaging delivery and nuanced perspective on historical holidays.

# Tags

#Juneteenth #July4 #HistoricalHolidays #Equality #Commemoration