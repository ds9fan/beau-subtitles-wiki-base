# Bits

Beau says:

- Biden administration is revamping asylum rules previously set by Trump administration, which limited the ability of asylum seekers to apply.
- Trump's policies aimed to disallow as many immigrants as possible using the asylum system, operating under the assumption that it was often manipulated.
- Trump removed the ability to seek asylum from domestic violence and actions of non-state actors, but Biden administration is changing that.
- Central American asylum seekers, especially those fleeing domestic violence or non-state actors, should have legitimate reasons for asylum due to lack of safeguards in their countries.
- Non-state actors like drug gangs in Central America often have more influence and control than the government, leading many to flee from them.
- The United States' war on drugs played a significant role in empowering these non-state actors, making it fitting for the US to take in asylum seekers fleeing from their actions.

# Quotes

- "Our asylum system is designed to take those who are in danger."
- "Non-state actors are often more prevalent than the government."
- "It only seems fitting and it only seems right that we would take asylum seekers who are fleeing their actions."

# Oneliner

Biden administration revamps asylum rules to allow fleeing from domestic violence and non-state actors, recognizing the dangers faced by Central American asylum seekers.

# Audience

Advocates for asylum seekers

# On-the-ground actions from transcript

- Support organizations aiding asylum seekers (suggested)
- Advocate for comprehensive asylum policies (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of how US policies have impacted asylum seekers and the importance of recognizing legitimate reasons for seeking asylum from dangers like domestic violence and non-state actors.