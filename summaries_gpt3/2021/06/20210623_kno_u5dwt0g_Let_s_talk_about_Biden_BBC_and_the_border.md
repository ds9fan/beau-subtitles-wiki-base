# Bits

Beau says:

- The Biden administration inherited multiple messes at the border, including handling an influx of unaccompanied minors seeking asylum and reunification.
- Ad hoc facilities were quickly set up to process and house these minors, reducing processing and holding times initially.
- Issues surfaced at these facilities, such as undercooked food, lack of COVID prevention measures, and lice infestations.
- Despite initial understanding, problems persist months later, with reports of ongoing issues like undercooked food and lice outbreaks.
- Relationships between staff and detainees, especially children, are forming, raising concerns given the nature of the situation.
- While applauding improvements in processing times, Beau stresses that the conditions these individuals are subjected to are unacceptable.
- Urgent action is needed to address the disturbing evidence of mistreatment and neglect at these facilities.
- Beau calls for immediate intervention by the Inspector General to fix the problems, rather than just producing a report.
- He criticizes the administration's focus on foreign policy moral authority while such issues persist domestically.
- The treatment of individuals in these facilities contradicts the care they should receive and undermines the administration's credibility.
- Beau underscores the importance of prioritizing the well-being of real people over political battles or posturing.
- He stresses that the situation, initially somewhat understandable, has persisted for months and must be resolved urgently.

# Quotes

- "That is not acceptable."
- "This needs to be fixed and fixed now."
- "They're supposed to be in the care, not custody."
- "This has got to get fixed."
- "It's been months. This has got to get fixed."

# Oneliner

The Biden administration must urgently address ongoing issues and mistreatment at border facilities housing unaccompanied minors seeking asylum, shifting focus from political posturing to real care.

# Audience

Advocates, policymakers, concerned citizens

# On-the-ground actions from transcript

- Contact elected officials to demand immediate intervention and improvement in conditions (suggested)
- Support organizations working to provide assistance and oversight at these facilities (exemplified)

# Whats missing in summary

The emotional urgency and call for immediate action is best experienced in Beau's original delivery.

# Tags

#BorderFacilities #Immigration #HumanRights #BidenAdministration #PolicyConcerns