# Bits

Beau says:

- Chelsea Manning's shift to creating STEM-focused content on YouTube, aiming to provide nuanced, politically-informed educational videos on technical topics.
- The importance of educating people on complex technical subjects like cryptocurrency and artificial intelligence in a simple, easy-to-understand manner.
- Chelsea Manning's emphasis on the societal impact of technology, addressing common misconceptions and misinformation from various political perspectives.
- The challenge of bridging the gap in technical literacy between different levels of understanding among the general audience.
- Chelsea Manning's journey in learning video production from scratch, including taking film classes and receiving support from a professional editor.
- The significance of community support in making the YouTube channel project self-sustaining through Patreon subscriptions.
- Chelsea Manning's reflections on the pressure and expectations surrounding the launch of the YouTube channel, including considerations related to being a trans public figure.
- The importance of community impact and education over viral success in content creation, focusing on making a difference regardless of viewership numbers.
- Chelsea Manning's message about taking small actions every day to make the world a better place, regardless of the scale or magnitude of the effort.

# Quotes

- "Do one thing every day to change the world and make it a better place."
- "Everybody is in a position to do the small stuff."
- "Do what you can, when you can, where you can for as long as you can."

# Oneliner

Chelsea Manning shifts focus to STEM education on YouTube, aiming to simplify complex technical topics with a nuanced political perspective to bridge the gap in technical literacy for a broader audience.

# Audience

Creators, educators, activists

# On-the-ground actions from transcript

- Start a community education project on STEM topics (implied)
- Support creators through platforms like Patreon (exemplified)

# Whats missing in summary

The emotional impact of Chelsea Manning's journey in creating educational content and the role of community support in making the project possible.

# Tags

#ChelseaManning #STEMeducation #YouTube #CommunitySupport #TechLiteracy #SocialImpact