# Bits

Beau says:

- Explains why he doesn't usually use sports analogies, citing his lack of sports-watching as the main reason.
- Introduces the concept of home team advantage in sports, where statistically the home team wins more frequently.
- Provides statistics showing that in the NFL, the home team wins 57% of the time.
- Explores possible reasons for home team advantage, such as crowd excitement, environmental factors, referee bias, and travel distance for the away team.
- Considers how these factors could also apply beyond sports to societal privilege and systemic advantages.
- Suggests renaming home team advantage to home team privilege to draw parallels with societal inequalities.
- Advocates for addressing and mitigating these privileges to create a fairer playing field in sports and society.
- Draws a comparison between addressing bias in sports officiating and bias in societal systems.
- Criticizes the prioritization of mitigating bias in sports over bias affecting people's lives.
- Raises the point that acknowledging and addressing privileges in society is necessary for promoting fairness and equity.

# Quotes

- "Let's not call it home team advantage. Let's call it home team privilege."
- "Privileges of all kinds exist. They're real. You can't deny that they're out there."
- "When the outcomes are as different as they are in reality, it's probably worth addressing."
- "It's probably worth trying to mitigate and see if we can get to a game that's a little bit more fair."

# Oneliner

Beau explains home team advantage in sports, linking it to societal privilege, advocating for addressing and mitigating systemic inequalities in both realms.

# Audience

Sports fans, social justice advocates

# On-the-ground actions from transcript

- Advocate for fair officiating by supporting measures like tracking chips in sports equipment to reduce bias (implied).

# Whats missing in summary

Beau's engaging storytelling and thought-provoking insights on addressing systemic privilege in sports and society could be best experienced by watching the full transcript. 

# Tags

#Sports #SocietalInequality #SystemicBias #HomeTeamPrivilege #FairPlay #Advocacy