# Bits

Beau says:

- Explains the concept of ecocide, a proposed international law that could be added to the Rome Statutes.
- Defines ecocide as unlawful acts committed with knowledge of causing substantial and severe damage to the environment.
- Notes the challenge of getting new international crimes accepted due to countries losing authority over such matters.
- Emphasizes the importance of holding governments and entities accountable for their environmental actions.
- Acknowledges that the process of adding ecocide as a crime will be lengthy but believes even the definition alone is useful.
- Points out that nonprofits and environmental protection groups can benefit from this legal definition.
- Urges individuals to reconsider their actions before the ICC starts holding them accountable under the ecocide definition.
- Raises awareness about the environmental damage caused by viewing it as just a cost of doing business.
- Stresses the need for significant changes to address environmental issues and the limited time available for implementing solutions.

# Quotes

- "It's called ecocide. It's called ecocide."
- "But this is a stopgap. And we are running out of time to use stopgaps."
- "A whole lot of environmental damage that's occurring, well, it's just a cost to do business."

# Oneliner

Beau introduces the concept of ecocide, a proposed international law aiming to hold governments and entities accountable for severe environmental damage, stressing the urgency for significant changes to address environmental issues before running out of time.

# Audience

World citizens, environmental activists

# On-the-ground actions from transcript

- Advocate for the recognition of ecocide as an international crime (implied)
- Support nonprofits and environmental protection groups in using the legal definition to raise awareness and protect the environment (implied)

# Whats missing in summary

The emotional impact of realizing that environmental damage is often viewed as merely a business cost and the urgent need for substantial changes to address these issues effectively.

# Tags

#Ecocide #InternationalLaw #EnvironmentalProtection #Accountability #Urgency