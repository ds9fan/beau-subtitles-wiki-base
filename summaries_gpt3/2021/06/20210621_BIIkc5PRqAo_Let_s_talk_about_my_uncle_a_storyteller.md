# Bits

Beau says:

- Introduces the topic about his uncle, a storyteller, who taught valuable lessons through his experiences.
- Shares how his uncle lost his finger before he was born, with varying stories about how it happened.
- Mentions that his uncle always had a story to tell and each story had a point or lesson to impart.
- Talks about learning about his family's native traditions and how to run irrigation from his uncle.
- Describes his uncle as an adventurer who served in the Navy and briefly expatriated to the Philippines.
- Mentions his uncle's three daughters and how every interaction with him was a learning experience.
- Expresses the loss of his uncle to COVID over the weekend.
- Urges people to get vaccinated, stating that misinformation is discouraging people from getting vaccinated.
- Emphasizes the importance of vaccination to prevent further loss of lives due to COVID.
- Encourages asking questions about vaccination and seeking answers to address concerns.
- Stresses the reality of the ongoing situation and the necessity for vaccination to save lives.
- Shares his personal experience of getting vaccinated and the minimal discomfort involved.
- Advises scheduling a vaccination appointment and taking the necessary step to protect oneself and others.

# Quotes

- "Everything was a learning experience. Everything."
- "We lost him over the weekend to COVID."
- "There's no reason for it to be you. Go get vaccinated."
- "It doesn't take long. It's pretty, well, I don't want to say it's painless."
- "Y'all have a good day."

# Oneliner

Beau shares lessons learned from his storytelling uncle, urging vaccination to prevent further loss to COVID misinformation.

# Audience

Individuals, Vaccine-Hesitant

# On-the-ground actions from transcript

- Get vaccinated to protect yourself and others (implied)

# Whats missing in summary

Personal anecdotes and emotional connections with Beau's uncle that can be best appreciated in the full transcript.

# Tags

#Family #Storytelling #COVID19 #Vaccination #LessonsLearned