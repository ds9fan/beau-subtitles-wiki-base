# Bits

Beau says:

- Former President Trump's Department of Justice used a secret subpoena process to gather information on members of Congress.
- The outrage shouldn't be because it happened to members of Congress; the focus should be on whether the Department of Justice followed normal procedures.
- If DOJ followed policies and rules in place to get the subpoena, there isn't a scandal – it's about applying the same standards to everyone.
- The burden of proof required to gather such information may be too low and needs to be raised.
- Politicizing law enforcement and using secretive forms can be dangerous for a free society.
- Legislative fixes are needed to ensure equal application of rules and procedures in gathering information.
- The real scandal lies in the low standards applied by DOJ and the need for legislative changes.
- The focus should not be on how it happened to a member of Congress, but on how it happened in general.
- Policies need to be resilient and capable of withstanding misuse by future administrations.
- Changing laws and rules with higher standards for gathering information is the solution, not pointless inquiries.

# Quotes

- "The outrage shouldn't be because it happened to members of Congress; the focus should be on whether the Department of Justice followed normal procedures."
- "If DOJ followed policies and rules in place to get the subpoena, there isn't a scandal – it's about applying the same standards to everyone."
- "The real scandal lies in the low standards applied by DOJ and the need for legislative changes."
- "Changing laws and rules with higher standards for gathering information is the solution, not pointless inquiries."
- "Policies need to be resilient and capable of withstanding misuse by future administrations."

# Oneliner

Former President Trump's Department of Justice used a secret subpoena process on members of Congress; the focus should be on ensuring equal standards and legislative fixes for information gathering.

# Audience

Legislators, Activists, Citizens

# On-the-ground actions from transcript

- Push for legislative changes to set higher standards for information gathering (suggested)
- Advocate for resilient policies that withstand misuse by future administrations (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the politicization of the Department of Justice and the need for legislative fixes to ensure equal application of standards in gathering information.

# Tags

#DepartmentOfJustice #LegislativeFixes #InformationGathering #PolicyChanges #ResilientPolicies