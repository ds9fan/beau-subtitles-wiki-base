# Bits

Beau says:
- Explains how authoritarians use talking points to undermine base principles of a country.
- Gives examples like anti-protest laws and purity of the vote laws as ways to erode freedoms.
- Talks about border checkpoints and how they infringe on citizens' rights.
- Mentions Trump and Miller setting up an office to further marginalize certain groups.
- Describes Biden reallocating funds to help immigrants in detention report abuse.
- Criticizes the use of talking points to shift public opinion towards authoritarianism.
- Encourages listeners to analyze political talking points for their impact on basic principles.

# Quotes

- "They create the problem then they create the solution."
- "That's how authoritarians throughout history have swayed public opinion away from the base principles of the country."
- "Trumpist talking points, the talking points that are used by his adherents, see if they undermine the base principles of the country and set the stage for authoritarianism."

# Oneliner

Beau explains how political talking points can erode fundamental freedoms and pave the way for authoritarianism.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Analyze political talking points for their impact on basic principles (implied)

# Whats missing in summary

Exploration of the historical context and examples of authoritarian tactics used to manipulate public opinion.

# Tags

#Authoritarianism #PoliticalTalkingPoints #UnderminingFreedom #CommunityAction #Analysis