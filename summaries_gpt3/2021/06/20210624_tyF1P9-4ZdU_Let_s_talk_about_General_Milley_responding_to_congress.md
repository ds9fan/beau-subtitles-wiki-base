# Bits

Beau says:

- Analyzing General Miley's exchange with Congress.
- Importance of diverse backgrounds within special forces.
- Emphasizing the value of education for warriors.
- Learning about different things to be a better warrior.
- Rejecting the notion that learning is bad.
- Sign of intelligence is entertaining ideas without accepting them as facts.
- Criticizing politicians dictating military education.
- Green Berets needing to understand different cultures.
- Critical race theory's potential applicability.
- Distinguishing education from indoctrination.
- Demonization of education and intellectualism in the US.
- People manipulated by trivial distractions from real issues.
- Politicians influencing what is taught is dangerous.
- Indoctrination leads to inability to discern fact from fiction.

# Quotes

- "No education is ever wasted."
- "The true sign of intelligence is being able to entertain an idea without accepting it as fact."
- "Being educated about something is not indoctrination."
- "Politicians determining what you can learn is a dangerous concept."

# Oneliner

Beau breaks down the importance of education for warriors and criticizes politicians dictating military learning to combat indoctrination.

# Audience

Educators, policymakers, activists.

# On-the-ground actions from transcript

- Support educational programs for military personnel (suggested).
- Advocate for diverse educational curriculums in the military (suggested).
- Combat misinformation by promoting critical thinking skills (suggested).

# Whats missing in summary

Importance of challenging indoctrination and promoting critical thinking skills to combat manipulation and misinformation.

# Tags

#Education #Warriors #Indoctrination #CriticalThinking #Military