# Bits

Beau says:

- Explains the context of a document released by the Biden administration from the Office of the Director of National Intelligence.
- Clarifies that misinterpretations circulating online about the document are causing confusion.
- Describes the different ways the document is being presented, including inaccuracies and misrepresentations.
- Breaks down the definition of domestic violent extremists (DVE) provided in the document.
- Emphasizes that the document does not target individuals based on their beliefs or activism.
- Assures that the document is not a cause for concern and follows a similar pattern to previous bulletins.
- Addresses the potential legal issues around designating certain groups within the U.S. as illegal.
- Notes that certain rhetoric online may lead to being tagged as a sympathizer and subject to surveillance.
- Stresses the importance of avoiding violent rhetoric online to prevent unwanted attention.
- Concludes by providing reassurance and encouraging viewers to have a good day.

# Quotes

- "Just because you're an environmentalist or an animal rights activist or any of the other stuff listed here, that doesn't mean that they view you as a DVE."
- "If you don't know, this is my area of study. I have read everything the Biden administration has released on this topic."
- "Without a constitutional amendment, the U.S. government does not have the authority to designate a domestic group."
- "It's just saying that some DVEs have this as a motivation."
- "Just on this one, unless you're hurting people, it doesn't apply to you."

# Oneliner

Beau clarifies misinterpretations of a document from the Biden administration, reassuring viewers that being an activist doesn't equate to being viewed as a domestic violent extremist.

# Audience

Concerned viewers

# On-the-ground actions from transcript

- Read and understand the actual document from the Office of the Director of National Intelligence (suggested)
- Avoid using violent rhetoric online to prevent unwanted attention and surveillance (implied)

# Whats missing in summary

In watching the full transcript, viewers can gain a comprehensive understanding of the misconceptions surrounding a government document and be reassured about the implications for activists and individuals with different beliefs.

# Tags

#BidenAdministration #NationalIntelligence #DVE #Misinterpretations #Activism