# Bits

Beau says:

- Talks about privilege and perception based on statistics.
- Demonstrates how privilege influences perception of reality.
- Mentions the impact of exposure to certain facts on awareness.
- Points out the misconception of being in a post-pandemic state in the United States.
- Provides statistics on hospitalizations and losses due to COVID-19.
- Emphasizes the different impacts of the pandemic on vaccinated and unvaccinated individuals.
- Raises awareness about the disparity in vaccine distribution between wealthy and poorer countries.
- Comments on people's unawareness of the unequal vaccine distribution and its impacts.
- States that the pandemic is far from over globally, despite improvements in some countries.
- Concludes by discussing how privilege can make people unaware of their own advantages.

# Quotes

- "Your reality is pretty broadly defined by what you see around you every day."
- "We see recovery. We see things getting back to normal. Everything's lessening here, but just here."
- "It's a privilege."
- "That's how privilege works."
- "Sometimes you're completely, completely unaware of the fact that you have it because you don't see the other side of it every day."

# Oneliner

Beau talks about privilege, perception, and the global impact of the ongoing pandemic while shedding light on the unequal distribution of vaccines and its implications.

# Audience

Global citizens

# On-the-ground actions from transcript

- Advocate for equitable vaccine distribution (implied)
- Stay informed about the global status of the pandemic and support initiatives for fair vaccine access (implied)
- Educate others about the disparities in vaccine distribution (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the global disparities in COVID-19 vaccine distribution and challenges viewers to reconsider their privilege and awareness of global issues.

# Tags

#Privilege #Perception #COVID19 #VaccineDistribution #GlobalHealth