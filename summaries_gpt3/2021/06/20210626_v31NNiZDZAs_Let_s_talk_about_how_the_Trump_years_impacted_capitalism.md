# Bits

Beau says:

- Acknowledging a contribution former President Donald J. Trump made to the United States, inadvertently shifting views on capitalism and socialism.
- Describing the United States as predominantly right-wing, even the Democratic Party is viewed as center-right, not left or leftist.
- Explaining the difficulty of discussing left-leaning ideas in the U.S. due to negative connotations associated with terms like socialism.
- Noting that Trump's actions, by breaking norms and using terms like socialism, sparked a curiosity that led to understanding leftist ideologies.
- Mentioning a shift in the Republican Party's views on capitalism, with more young Republicans wanting policies to reduce the wealth gap.
- Pointing out that Trump's rhetoric unintentionally helped mainstream socialism by trying to villainize it.
- Emphasizing how Trump's actions inadvertently widened the discourse on alternatives to capitalism.
- Recognizing that Trump's unintended consequences may have long-lasting effects on the dynamics of the GOP and political ideologies in the future.

# Quotes

- "He altered that dynamic because he broke an unspoken rule and he called people socialist."
- "It helped mainstream socialism. It helped mainstream that idea."
- "He didn't mean to. But at the end of the day, when you look at how the Trump years were for the capitalist class, I mean, yeah, overall it was pretty well."

# Oneliner

Acknowledging Trump's unintentional contribution to mainstreaming socialism by sparking curiosity and understanding leftist ideologies, altering Republican views on capitalism.

# Audience

Political Observers, Young Republicans

# On-the-ground actions from transcript

- Engage in political discourse with young Republicans to understand their shifting views (implied).
- Support policies aimed at reducing the wealth gap to resonate with changing perspectives (implied).

# Whats missing in summary

The full transcript provides a nuanced analysis of how Trump's rhetoric unintentionally influenced shifts in views on capitalism and socialism, especially among young Republicans.

# Tags

#DonaldJTrump #Capitalism #Socialism #RepublicanParty #PoliticalIdeologies