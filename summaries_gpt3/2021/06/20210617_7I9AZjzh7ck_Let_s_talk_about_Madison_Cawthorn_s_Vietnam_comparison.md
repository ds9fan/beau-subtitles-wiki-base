# Bits

Beau says:

- Representative Madison Cawthorn made a comparison about civilian arms in the US to prevent tyranny.
- He mentioned the idea that civilians with rifles could potentially stop tyranny and defeat a major military force.
- The comparison often overlooks the role of the North Vietnamese Army (NVA) backing up the Viet Cong during the conflict.
- Beau questions the validity of the comparison between civilians with arms and defeating a well-disciplined, conventional military.
- He points out the fantasy versus reality aspect of the comparison when it comes to enduring hardships and sacrifices.
- The image of the citizen-soldier and the VC being willing to endure anything is contrasted with modern societal behaviors.
- Beau challenges the notion of romanticizing conflict and war, especially when considering the actual consequences and sacrifices involved.
- He questions the motivations behind advocating for violent actions that could lead to the destruction of the country.
- The comparison with conflicts in Afghanistan and Iraq is brought up to illustrate the long-lasting and devastating nature of such actions.
- Beau advocates for focusing on constructive roles like engineers, scientists, nurses, and other professionals rather than glorifying combat.

# Quotes

- "Nobody ever won a war by dying for their country. They win a war by making the other guy die for his."
- "This country needs more engineers. It needs more scientists. It needs more nurses, doctors, welders, truck drivers. We need a lot of things in this country. The one thing we don't need is more combat vets."

# Oneliner

Representative Madison Cawthorn's comparison of civilian arms in the US to prevent tyranny is challenged by Beau, who questions the romanticized notions of conflict and war, advocating for constructive contributions to society instead of glorifying combat.

# Audience

US citizens

# On-the-ground actions from transcript

- Support educational programs for engineering, science, healthcare, and vocational training (suggested)
- Encourage youth to pursue careers in constructive fields (implied)
- Advocate for peacebuilding initiatives and conflict resolution strategies (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of the societal attitudes towards conflict, sacrifice, and the importance of focusing on constructive contributions rather than glorifying violence.