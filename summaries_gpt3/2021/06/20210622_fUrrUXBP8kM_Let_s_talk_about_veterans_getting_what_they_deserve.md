# Bits

Beau says:

- Addresses the news about trans vets being able to get confirmation surgery through the VA.
- Expresses excitement about this development, believing it helps integrate marginalized groups into the veteran community.
- Notes the social unacceptability of openly looking down on people for their immutable characteristics in the US.
- Reads comments criticizing this news, citing examples of veterans not getting hearing aids or insulin.
- Calls out the hypocrisy of commenters who claimed previous administrations had fixed VA issues.
- Points out that veterans can indeed get hearing aids and insulin through the VA, although the system isn't perfect.
- Emphasizes that the entitlement to medical care should make this policy a reason to celebrate if one truly cares about veterans.
- Criticizes the tendency to other and view veterans as lesser, perpetuating acceptable discrimination against them.
- Hopes that integrating different demographics into the veteran community will lead to broader societal acceptance.
- Mentions a trans veteran, Zoe, giving advice in the comments on how to access hearing aids through a tinnitus claim.

# Quotes

- "It's socially unacceptable to openly look down on people because of their race, because of their orientation, because of their religion."
- "They're entitled to medical care, right? If you actually cared about veterans, this would be a celebratory moment."
- "Nobody wants to talk bad about a vet in this country."
- "Veterans can get insulin through the VA. You know what veterans couldn't get? Confirmation surgery."
- "I read the comments, and in the comments saw stuff like..."

# Oneliner

Beau addresses the social unacceptability of discrimination based on immutable characteristics when discussing the news about trans vets' access to confirmation surgery through the VA and criticizes the hypocritical backlash in the comment section.

# Audience

Veterans, Advocates, Allies

# On-the-ground actions from transcript

- Contact trans veteran Zoe for advice on accessing hearing aids through a tinnitus claim (exemplified)
- Celebrate and support policies that integrate marginalized groups, like trans vets, into the veteran community (suggested)

# Whats missing in summary

Nuances in Beau's tone and delivery, along with the full emotional impact of his disappointment in the hypocrisy displayed in the comments section.

# Tags

#Veterans #Discrimination #Healthcare #SocialAcceptance #CommunityIntegration