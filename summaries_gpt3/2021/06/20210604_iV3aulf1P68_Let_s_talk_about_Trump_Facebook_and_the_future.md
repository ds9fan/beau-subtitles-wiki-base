# Bits

Beau says:

- Former President Trump will remain banned from Facebook for a couple of years.
- Facebook will not automatically assume that politicians' posts are newsworthy.
- Politicians will no longer be exempt from certain rules on social media platforms.
- Facebook will now apply exemptions for politicians and add a notice below the post.
- This change may have far-reaching effects on political discourse.
- Tech experts once struggled to remove racist content from Twitter due to potentially catching Republican politicians with algorithms.
- The impact of these changes may vary across different regions and countries.
- Different standards for newsworthy content may apply in different countries.
- The diversity of acceptable speech standards may lead to varying social media experiences globally.
- These changes may have more significant implications than Trump's ban for a couple of years.

# Quotes

- "Former President Trump will stay banned for a couple of years."
- "Facebook is no longer going to assume that politicians have newsworthy posts."
- "I think that's going to have much more far-reaching effects than people are imagining."
- "We may be reaching a point where the Facebook experience, where the social media experience differs even more from country to country."
- "As far as the partisan stuff goes, yeah, he's kind of out of the midterms."

# Oneliner

Former President Trump will stay banned from Facebook for two years, and politicians will no longer have automatic newsworthy exemptions, potentially changing the global social media landscape.

# Audience

Social media users

# On-the-ground actions from transcript

- Monitor and participate in the evolving policies and practices of social media platforms (suggested)
- Stay informed about how social media regulations could impact political discourse and representation (suggested)
- Advocate for transparent and consistent standards for social media content globally (implied)

# Whats missing in summary

Insights on the potential long-term impact of social media regulations on political discourse and global communication. 

# Tags

#Facebook #Trump #SocialMedia #Politics #GlobalImpact