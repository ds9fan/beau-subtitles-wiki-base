# Bits

Beau says:

- Explains the slow and deliberate approach taken by officers in San Jose during a shooting incident.
- Mentions that the officers were not well-trained in room clearing, leading to some cringe-worthy moments.
- Points out that the officers from different departments had not trained together, causing some disorganization.
- Acknowledges that despite some mistakes, the officers managed to clear the building without hurting anyone innocent.
- Expresses satisfaction with law enforcement's actions, stating that moving in when they did likely saved lives.
- States that while there were imperfections, the overall outcome was successful and no innocent individuals were harmed.

# Quotes

- "If you're watching that footage, when the supervisor comes out, the person they got the key card from, had they been moving quickly, they might have hurt that person by accident."
- "At the end of the day, it is almost certain that them choosing to move in when they did saved lives."
- "Wasn't perfect. At times, it was kind of ugly. But it worked."
- "And nobody got hurt."
- "Overall, I think they did fine."

# Oneliner

Beau explains the slow and deliberate approach taken by officers in San Jose, acknowledging mistakes but ultimately recognizing their success in ensuring no harm to civilians.

# Audience

Law enforcement personnel

# On-the-ground actions from transcript

- Analyze and improve inter-departmental coordination and training (implied)
- Recognize the importance of ensuring minimal risk to civilians during law enforcement operations (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of law enforcement tactics during a critical incident and underscores the importance of prioritizing civilian safety in such situations.

# Tags

#LawEnforcement #Tactics #CivilianSafety #CriticalIncident #InterdepartmentalCoordination