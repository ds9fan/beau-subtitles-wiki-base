# Bits

Beau says:

- Addresses the misconception of 750 million refugees wanting to come to the United States.
- Mentions the arguments against accepting such a large number, citing space and economic concerns.
- Counters the space argument by comparing the landmass of China and the US.
- Challenges the economic drain argument by suggesting that more people equal more economic activity.
- Refers to a 2018 Gallup study that clarifies the 750 million figure pertains to migrants, not just refugees.
- Points out that not all migrants want to come to the US, with only 21% expressing a desire to do so.
- Calculates that around 150 million individuals actually want to migrate to the US.
- Suggests that the US could feasibly accept every person who wants to come, given its size and resources.
- Argues that portraying the issue as insurmountable serves as an excuse for inaction.
- Asserts that economic and space constraints are not valid reasons to reject migrants.

# Quotes

- "We could actually do it."
- "Realistically, yes, the United States could accept every single person who wants to come to the US in the world."
- "It's just a talking point."
- "It's not going to be an economic one."
- "All of the stuff that gets thrown up, that's not true."

# Oneliner

Beau addresses the misconception around 750 million migrants wanting to come to the US, debunking space and economic concerns and suggesting that the US could feasibly accept them. 

# Audience

Policy advocates

# On-the-ground actions from transcript

- Challenge misconceptions about migrants (implied)
- Advocate for more inclusive migration policies (implied)

# Whats missing in summary

The full transcript provides a detailed debunking of the misconceptions surrounding the number of migrants wanting to come to the US, offering a nuanced perspective on the feasibility of accepting them.

# Tags

#Migration #Refugees #US #Misconceptions #Immigration