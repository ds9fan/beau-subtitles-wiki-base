# Bits

Beau says:

- Receives a message about a unique reaction to news about encryption technologies.
- Shares information about Phantom Secure being taken down by authorities.
- Mentions the emergence of a new encrypted app called Anom, which authorities used to monitor and run investigations.
- Explains the concept of the Five Eyes alliance involving the US, UK, Canada, Australia, and New Zealand for sharing signals intelligence.
- Raises concerns about intelligence agencies potentially circumventing laws by using this alliance to spy on their own citizens.
- Acknowledges the existence of civil liberties controversies surrounding the Five Eyes alliance.
- Mentions the predecessor program Echelon and its lack of self-regulation by intelligence agencies.

# Quotes

- "Just because something sounds like a wild theory doesn't mean that it is."
- "Five eyes definitely exists."
- "There is definitely a civil liberties controversy."

# Oneliner

Beau shares insights on encryption technologies, the Five Eyes alliance, and civil liberties controversies, debunking wild theories in the process.

# Audience

Internet users

# On-the-ground actions from transcript

- Research and stay informed about encryption technologies and surveillance practices (implied)
- Advocate for privacy rights and civil liberties in the digital age (implied)

# Whats missing in summary

Detailed examples and further elaboration on the controversies surrounding intelligence agencies' potential circumvention of laws through the Five Eyes alliance. 

# Tags

#Encryption #FiveEyes #CivilLiberties #Surveillance #DigitalPrivacy