# Bits

Beau says:

- Governor of South Dakota sending 50 National Guard troops to the southern border, funded by private donation.
- Questions raised about the legality of using private funds for National Guard deployment.
- Beau questions the purpose and effectiveness of deploying only 50 troops to secure the border.
- Raises the issue of National Guard members signing up to help their community, not for political stunts.
- Points out the difference in numbers between National Guard troops and Border Patrol agents.
- Criticizes the deployment as a political stunt rather than an effective security measure.
- Warns that governors involved in this deployment may not be fit for the presidency.
- Emphasizes that deploying troops for political gain sets a dangerous precedent.
- Condemns the deployment as a misuse of trust and resources for political purposes.
- Urges people to recognize the true intentions behind such deployments.

# Quotes

- "It's a joke."
- "It's politics and nothing more."
- "Any governor who participates in this can never be president."
- "They are showing you who they are right now."
- "It's a joke. It's a joke."

# Oneliner

Governors deploying National Guard troops for political stunts instead of real security risk their credibility and the lives of service members.

# Audience

Governors, Voters, Activists

# On-the-ground actions from transcript

- Call out misuse of National Guard for political gain (exemplified)
- Advocate for ethical deployment of National Guard troops (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the questionable deployment of National Guard troops for political purposes and raises concerns about the misuse of resources and trust.

# Tags

#NationalGuard #PoliticalStunt #BorderSecurity #Governors #MisuseOfResources