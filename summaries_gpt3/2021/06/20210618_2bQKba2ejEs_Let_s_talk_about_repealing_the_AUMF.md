# Bits

Beau says:

- The House voted to repeal the Authorization for Use of Military Force (AUMF), a long process expected to be undertaken by the Biden administration with some willingness.
- The 2002 AUMF authorized the Iraq War and has been used to justify other actions due to its broad nature.
- Power was transferred from Congress to the executive branch in the early 2000s, which needs to be corrected.
- Congress, not the executive branch, should declare and make war.
- These authorizations enable presidents to partake in military actions, with Biden supporting the repeal of both the 2002 and 2001 AUMFs.
- McConnell opposes the repeal without providing a valid reason, potentially to allow for future military actions justified by the broad authorization.
- There are uncertainties surrounding the repeal of the 2001 AUMF due to ongoing dynamics in Afghanistan.
- Efforts are being made to restore the military's use to its original intent and reduce military involvement in the Middle East and other regions.

# Quotes

- "It is Congress's job to declare war, to make war."
- "These authorizations allow presidents to engage in military adventurism."
- "There's a lot of people who look at the title of commander-in-chief as meaning that the military is at the total discretion of the president, and that's not the case."

# Oneliner

The House voted to repeal the broad Authorization for Use of Military Force, transferring power back to Congress and curbing military involvement. 

# Audience

Policymakers

# On-the-ground actions from transcript

- Contact your senators to support the repeal of the Authorization for Use of Military Force (implied).

# Whats missing in summary

Detailed explanation of the potential impacts of repealing the Authorization for Use of Military Force. 

# Tags

#AuthorizationForUseofMilitaryForce #AUMF #Congress #ExecutiveBranch #MilitaryInvolvement