# Bits

Beau says:

- Former president Trump is considering running for a house seat and becoming Speaker of the House.
- Transparency in how representatives vote might not be as beneficial as previously thought.
- A theory suggests that keeping representatives' votes secret can prevent corruption from lobbyists or the speaker.
- The concept of the "Speaker's Corrupt Bargain" involves manipulating new representatives based on their voting behavior.
- If Trump were to become Speaker of the House, he could wield significant power over Republicans, influencing their votes and enforcing purity tests.
- Progressive representatives may have to compromise their values to get bills they care about onto the floor.

# Quotes

- "Transparency in how representatives vote might not be as beneficial as previously thought."
- "If Trump becomes Speaker of the House, he has the ability to bring all Republicans in line."
- "Progressive representatives may have to compromise their values to get bills they care about onto the floor."

# Oneliner

Former president Trump considering running for a house seat and becoming Speaker of the House raises concerns about potential manipulation and power dynamics within Congress.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Pay attention to potential power dynamics and manipulation within Congress (implied)

# Whats missing in summary

Insights into the implications of having a former president as Speaker of the House. 

# Tags

#Trump #SpeakerOfTheHouse #Corruption #Transparency #Congress