# Bits

Beau says:

- Introducing an American hero whose name isn't mentioned enough in history despite being a pivotal figure in the civil rights movement.
- Born in 1925, he walked 12 miles to school daily, served in the military during WWII, and fought at the Battle of Normandy.
- In 1954, he applied to the University of Mississippi Law School but was denied due to his race.
- Became the first field secretary of the NAACP in Mississippi in the same year.
- Organized boycotts, marches, and voting drives, putting himself at risk for the cause.
- Survived multiple attempts on his life, always aware of the dangers he faced.
- Killed on June 12, 1963, carrying a t-shirt that read "Jim Crow must go," becoming the first black man admitted to an all-white hospital posthumously.
- His death sparked marches and galvanized the civil rights movement.
- The circumstances of his death took decades to bring his killer to justice, but it pushed the movement forward significantly.
- Despite being recognized more now, his name isn't as prominent as others due to the great man theory overshadowing collective movements.

# Quotes

- "You can kill a man, but you can't kill an idea."
- "It's never just one person. It's always a movement of people."

# Oneliner

Beau talks about an American hero, Medgar Evers, whose pivotal role in the civil rights movement is often overshadowed by the great man theory, stressing the importance of recognizing collective efforts over individual figures.

# Audience

History enthusiasts, civil rights advocates

# On-the-ground actions from transcript

- Research and share more about Medgar Evers' life and contributions (suggested)
- Support civil rights organizations and movements in your community (suggested)

# What's missing in summary

The emotional impact and personal sacrifices made by Medgar Evers for the civil rights movement. It's worth watching the full transcript to truly understand his story and legacy.

# Tags

#CivilRights #MedgarEvers #GreatManTheory #CollectiveMovements #History