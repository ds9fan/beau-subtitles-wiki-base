# Bits

Beau says:

- Explains the concept of the supply chain, which is the route from production to consumption turning raw materials into finished products and delivered to their final destination.
- Addresses the stress on the supply chain due to a worldwide public health issue that resulted in disruptions and shortages in some places.
- Critiques the proposed solution of an America-first supply chain, arguing that it wouldn't solve the underlying issues caused by global disruptions.
- Points out that a centralized supply chain within the United States could make it more vulnerable to various disruptions like blizzards, floods, or hurricanes.
- Challenges the notion that localizing production solely in the United States is a feasible or effective solution, as it could weaken the system instead of strengthening it.
- Compares the vulnerability of a centralized supply chain to military strategies of cutting off supply routes and explains how centralization can make it easier to disrupt.
- Emphasizes the importance of a global supply chain that has been in place for a long time and how localizing it within the U.S. wouldn't address the current disruptions.
- Concludes by stating that decentralization typically leads to more security and questions the validity of an America-first supply chain as a solution to the current challenges.

# Quotes

- "Our supply chain is not broken. It has not snapped."
- "An America-first supply chain is not going to solve the problem. It's just a talking point by politicians."
- "Localizing it within the United States wouldn't stop that. It's a made-up talking point."
- "It's politicians preying on the ignorance of their base."
- "Things are normally more secure if they are decentralized."

# Oneliner

Beau explains the flaws in an America-first supply chain proposal amid global disruptions, stressing the importance of decentralization for security.

# Audience

Policy makers, Supply chain managers

# On-the-ground actions from transcript

- Challenge proposed policies promoting centralized supply chains (implied)

# Whats missing in summary

Beau's engaging delivery and detailed analysis on the implications of supply chain localization versus globalization.

# Tags

#SupplyChain #Globalization #AmericaFirst #Decentralization #PolicyMaking