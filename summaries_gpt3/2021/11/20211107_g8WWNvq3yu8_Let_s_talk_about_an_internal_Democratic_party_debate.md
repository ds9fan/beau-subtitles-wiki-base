# Bits

Beau says:

- Democratic strategists debate whether the party should be "woke" to win elections or if it's costing them votes.
- Being "woke" signifies advocating for basic dignity and acceptance for all Americans.
- The real issue is whether it's politically expedient to leave marginalized groups behind to win elections.
- Beau questions the integrity of those in power who prioritize winning over fighting for marginalized communities.
- In the 2020 election, 66.1% of eligible voters cast their ballots, leaving 33.9% who did not vote due to various reasons.
- Beau urges focusing on engaging the 33.9% of alienated non-voters rather than solely targeting the 66.1% who voted.
- He stresses the importance of fighting for marginalized groups and making a compelling case for their rights.
- Beau criticizes the Democratic Party for potentially alienating socially progressive individuals by not advocating strongly for marginalized communities.
- He suggests that abandoning challenging issues because they are difficult reinforces negative perceptions of the Democratic Party.
- Beau calls for genuine advocacy, especially on issues like clean energy, to inspire and attract voters genuinely.

# Quotes

- "If you want to cast yourself as the left-wing party in the United States, at least pretend on some level to be the slightest bit socially progressive or I'm sorry woke."
- "The idea of just abandoning entire groups of people because it's too hard is why the Democratic Party has the reputation it has."
- "If you want to keep fighting for the 66.1%, go right ahead. But I'm willing to bet it would be easier to get that 33.9."
- "Nobody's making a case for them. Nobody's reaching out to them."
- "Your four human rights."

# Oneliner

Democratic strategists debate whether the party should prioritize winning elections over advocating for marginalized groups, while Beau stresses the importance of fighting for basic dignity and acceptance for all Americans.

# Audience

Political activists and Democratic Party members

# On-the-ground actions from transcript

- Reach out to the 33.9% of alienated non-voters and make a compelling case for their participation (implied).
- Advocate for marginalized groups, including making a case for reparations and trans rights (implied).

# Whats missing in summary

The full transcript provides deeper insights into the debate within the Democratic Party on prioritizing electoral wins over advocacy for marginalized communities. 

# Tags

#DemocraticParty #MarginalizedCommunities #Elections #SocialJustice #Advocacy