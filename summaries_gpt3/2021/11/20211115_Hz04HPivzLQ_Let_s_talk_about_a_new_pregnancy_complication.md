# Bits

Beau says:

- Mentions a study revealing high homicide rates for pregnant individuals compared to traditional pregnancy-related issues.
- Previous smaller study results were considered too high due to a small sample size.
- New comprehensive study covers all 50 states over two years (2018-2019).
- Push for CDC to include homicide in maternal mortality rates due to the alarming statistics.
- Risk factors for increased rates include being young and being a black woman.
- Experts in intimate partner violence confirm pregnancy as a dangerous time for women.
- Expectation that rates will increase due to limitations on family planning access.
- Anticipates higher rates in the future due to laws restricting family planning.
- Suggests shelters and DV organizations reach out more during pregnancy to provide support.
- Warns of unintended consequences for those advocating for restrictive family planning laws.

# Quotes

- "If you are pregnant or were pregnant in the previous 42 days, you are more likely to be murdered than you are to die of what would typically be seen as a pregnancy related issue."
- "The two most dangerous times for women is when they are leaving or when they are pregnant."
- "By limiting that type of family planning, they are leaving people exposed, leaving them more vulnerable."

# Oneliner

Pregnant individuals face higher homicide rates than pregnancy-related issues, leading to a push to include homicide in maternal mortality rates and anticipate unintended consequences from restricted family planning laws.

# Audience

Policy Advocates, Healthcare Workers

# On-the-ground actions from transcript

- Support shelters and organizations countering DV issues by reaching out during pregnancy (implied).
- Advocate for inclusive family planning policies to prevent increased vulnerability (implied).

# Whats missing in summary

Importance of addressing the intersection of pregnancy and intimate partner violence comprehensively.

# Tags

#MaternalMortality #HomicideRates #FamilyPlanning #DVAdvocacy #PolicyChange