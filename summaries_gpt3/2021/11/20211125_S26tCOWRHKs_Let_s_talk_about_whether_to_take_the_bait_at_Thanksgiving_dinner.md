# Bits

Beau says:

- Addressing the dilemma of whether to have difficult Thanksgiving dinner political debates.
- Describing a situation where someone is openly trans at an event with closeted LGBTQ individuals.
- Sharing a story about the impact of political debates on relationships during the Kavanaugh hearings.
- Advising on handling such situations by swiftly shutting down potentially harmful comments.
- Emphasizing the importance of being mindful of the impact of words on relationships.
- Encouraging ending contentious debates quickly rather than trying to change minds.
- Suggesting that if political subjects arise, one should be cautious as assumptions may not always be correct.
- Recommending aiming to prevent lasting damage to relationships during heated debates.
- Urging a quick response to harmful comments as a favor to both the speaker and others present.
- Reminding everyone to be aware that assumptions about others' beliefs may not always be accurate during political talks.


# Quotes

- "If you know this conversation is going to come up, I'd set the goal of not changing minds but ending it as quickly as possible."
- "To everybody else who may be listening to this, when you start talking about politics around that table, just remember that you may not know everything you think you do about every person sitting there."


# Oneliner

Beau addresses navigating uncomfortable Thanksgiving political debates, advising quick shutdowns to prevent relationship damage.


# Audience

Thanksgiving participants


# On-the-ground actions from transcript

- End contentious debates swiftly by addressing harmful comments immediately (suggested)
- Be mindful of assumptions about others' beliefs during political talks (implied)


# Whats missing in summary

Beau's tone and personal anecdotes add depth and relatability to navigating difficult political debates at Thanksgiving.