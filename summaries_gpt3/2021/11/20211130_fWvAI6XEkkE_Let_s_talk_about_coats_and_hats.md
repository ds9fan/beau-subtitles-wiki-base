# Bits

Beau says:

- Observes people on Instagram and YouTube all wearing coats and hats, finds it odd.
- Did meme research and discovered only 1601 people on average die from hypothermia yearly.
- Refuses to wear a coat because of the low risk of hypothermia, sees it as an infringement on freedom.
- Claims coats don't work and are a myth created to sell products, suggests rearranging "coat" spells "taco" which keeps you warm.
- Believes people promoting coats and hats are shills for big jacket companies.
- Responds to questions about discussing the new variant, vaccines, masks, and hygiene.
- Advocates for basic hygiene practices like washing hands, not touching face, staying home, wearing masks, and getting vaccinated.
- Encourages getting a booster shot and bundling up.
- Concludes by wishing everyone a good day.

# Quotes

- "Coats don't even work. It's a myth."
- "All these people wearing coats and hats on Instagram and YouTube, shills for big jacket."
- "This isn't actually a complicated subject when it comes to masks working."
- "Wash your hands. Don't touch your face. Stay at home as much as you can."
- "Get vaccinated. Get a booster and don't forget to bundle up."

# Oneliner

Beau questions the necessity of coats, debunks myths, and advocates for basic hygiene practices and vaccination to stay safe.

# Audience

Social media users

# On-the-ground actions from transcript

- Wash your hands (implied)
- Don't touch your face (implied)
- Stay at home as much as you can (implied)
- Wear a mask if you have to go out (implied)
- Get vaccinated (implied)
- Get a booster shot (implied)
- Bundle up (implied)

# Whats missing in summary

The full transcript includes Beau's humorous take on the correlation between wearing coats and hats and conspiracy theories, along with debunking myths around hypothermia and advocating for basic health practices during a global pandemic.

# Tags

#Coats #Hats #Hygiene #Vaccination #ConspiracyTheories #SocialMedia