# Bits

Beau says:

- There is a discrepancy between the societal issues portrayed in the news about the USA and the reality of a more integrated America.
- Racism is receding in the United States despite the negative news coverage, with a shift in attitudes towards race among younger Americans.
- The news tends to focus on systemic nationwide issues, such as cases involving law enforcement and old citizens arrest laws like in Georgia.
- To change society, the focus should be on changing thought rather than just laws, as thought drives legal changes.
- It is vital for white Americans who have shifted their views on race to actively become anti-racist and work towards dismantling racist power structures.
- The current conflict arises from the need to dismantle institutional systems rooted in racism that are still in place despite changing societal views.
- The media often presents issues as having two sides, even when they don't, which can lead to a skewed perception of reality.
- The seeming resurgence of old, broken ideas is actually their death throes, indicating progress towards a more inclusive society.
- Traveling and working internationally can help break down prejudices and lead to a more integrated and modern worldview.
- Beliefs must be converted into action, advocating for change and engaging in civic duties to dismantle institutional structures rooted in racism.

# Quotes

- "Travel kills prejudice."
- "If you aren't racist, you need to advocate and to whatever civic duty you feel is necessary."
- "To change society, you don't change the law, you change thought."
- "Thought has changed enough to where a lot of the laws that aren't overtly racist but still manage to be disproportionate in their impacts are still on the books."
- "We're witnessing in this seeming resurgence is really the death throes."

# Oneliner

There's a discrepancy between media portrayal and reality in the US, with a shift in racial attitudes but a need for action to dismantle racist power structures.

# Audience

Americans, Global Citizens

# On-the-ground actions from transcript

- Advocate for change and get involved in civic duties to dismantle institutional structures rooted in racism (implied)
- Travel and work internationally to break down prejudices and gain a more modern worldview (exemplified)

# Whats missing in summary

The importance of converting beliefs into action to bring about meaningful change in dismantling racist power structures.

# Tags

#US #Racism #Media #Integration #AntiRacism #InstitutionalStructures #Travel #Advocacy