# Bits

Beau says:

- Senator Manchin held an impromptu press conference criticizing progressives for playing political games.
- Beau questions Senator Manchin's framing of himself as not part of the establishment despite being in politics since the late 1900s.
- Senator Manchin is accused of diverting attention, feeling pressure, and playing political games to criticize progressives.
- Beau points out that Senator Manchin has used leverage tactics himself to hold up progress.
- Senator Manchin questions the cost and money behind climate policy and social spending, despite economist endorsements.
- Beau suggests Senator Manchin's reasons for opposing climate policy might be influenced by large contributions from energy sector companies.
- Beau believes that social safety nets for workers may threaten mine companies' ability to mistreat their workers, leading to Senator Manchin's opposition.
- Senator Manchin's biggest donors are revealed to be large investment banks, law firms, and oil and gas companies, not reflective of a regular working man.
- Beau criticizes Senator Manchin for deflecting blame onto progressives and not understanding the stakes of blocking climate legislation and social safety nets.
- Senator Manchin is accused of playing political games and standing in the way of progress, potentially harming the Democratic Party in upcoming elections.

# Quotes

- "Stop playing political games."
- "He's feeling heat. He's feeling pressure."
- "If you didn't want the climate policy and just came out and said, nope, I'm an oil and gas man, can't do that, I could at least respect it."
- "I view it all as one giant political game."
- "Senator Manchin is the person who is standing in the way of progress."

# Oneliner

Beau questions Senator Manchin's motives and actions, accusing him of playing political games while standing in the way of progress and deflecting blame onto others.

# Audience

Progressive activists

# On-the-ground actions from transcript

- Visit opensecrets.org to research and understand contributions made to representatives (suggested).
- Hold accountable politicians who prioritize corporate interests over climate legislation and social safety nets (implied).
- Advocate for transparency in political funding and hold representatives like Senator Manchin accountable for their actions (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of Senator Manchin's actions and motives, shedding light on the influence of corporate funding in politics and the consequences of obstructing climate legislation and social safety nets.

# Tags

#SenatorManchin #PoliticalGames #CorporateFunding #ClimateLegislation #ProgressiveAdvocacy