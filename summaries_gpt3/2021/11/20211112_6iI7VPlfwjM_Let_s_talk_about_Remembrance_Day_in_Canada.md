# Bits

Beau says:

- Addressing Remembrance Day in Canada and a group using it to express anti-vaccine beliefs.
- Comparing Remembrance Day to Memorial Day in the United States.
- Explaining the origins of Remembrance Day from Armistice Day after World War I.
- Connecting the significance of wearing the poppy flower to those lost during World War I.
- Sharing historical insights on the Spanish flu during World War I and its impact on battle plans.
- Mentioning the different theories about the origin of the Spanish flu, including its link to World War I trenches.
- Describing how the flu changed battle plans and was downplayed by the media due to patriotism.
- Criticizing the misinformation spread about the flu during the war and its consequences.
- Questioning the accuracy of reported flu-related deaths, particularly in Canada.
- Calling out the irony of using Remembrance Day to express anti-vaccine beliefs while wearing a poppy symbolizing lost lives.

# Quotes

- "If they have asked you for your papers they have already forgotten, this Remembrance Day has more significance than all the ones we've had since World War II."
- "I just want to point out that if you took this moment to stage this little stunt, you're wearing a poppy that symbolizes a whole lot of people who were lost because of great patriots just like you."

# Oneliner

Beau addresses the misuse of Remembrance Day for anti-vaccine beliefs and criticizes the irony of such actions while honoring lives lost during World War I.

# Audience

Canadians, History Enthusiasts

# On-the-ground actions from transcript

- Respect the significance of Remembrance Day and honor the sacrifices made by wearing a poppy (exemplified).

# Whats missing in summary

Deeper historical context and analysis on the connection between pandemics, war, and misinformation.

# Tags

#RemembranceDay #Canada #SpanishFlu #Misinformation #AntiVaccine #History