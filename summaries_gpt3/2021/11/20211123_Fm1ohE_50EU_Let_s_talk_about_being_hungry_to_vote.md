# Bits

Beau says:

- Beau introduces the topic of hunger to participate in voting and electoral politics in the United States.
- Joe Madison, a civil rights activist turned radio host, is on a hunger strike at 72 years old until voting rights legislation is passed.
- Democrats in the House have passed voting rights acts, but they stalled in the Senate, where 19 states have passed laws making it harder to vote.
- The Freedom to Vote Act has all 50 Democrats' support in the Senate, with Vice President Harris as a tiebreaker, but faces the filibuster.
- The hunger strike aims to pressure President Biden to fulfill campaign promises and end the filibuster, not just pass voting rights legislation.
- The filibuster is a Senate rule not outlined in the Constitution, historically used to obstruct civil rights, but its purpose was for debate and consideration.
- Joe Madison sees this as a new civil rights movement and urges eliminating the filibuster to ensure voting rights legislation passes.

# Quotes

- "As food is essential for the existence of life, voting is essential for the existence of democracy."
- "This hunger strike is definitely aimed at President Biden, asking him to make good on his campaign promises."
- "You're appealing to the humanity of the opposition."
- "Allowing large segments of the American population to be disenfranchised at this level, that's pretty risky too."

# Oneliner

Joe Madison's hunger strike calls on President Biden to fulfill promises and end the filibuster for voting rights legislation, appealing to the humanity of the opposition.

# Audience

Voters, Activists, Politicians

# On-the-ground actions from transcript

- Support voting rights organizations (implied)
- Contact senators to support the Freedom to Vote Act and eliminate the filibuster (implied)

# Whats missing in summary

The emotional impact of Joe Madison's hunger strike and the urgency to protect voting rights in the face of legislative challenges.

# Tags

#VotingRights #HungerStrike #CivilRights #Filibuster #Democracy