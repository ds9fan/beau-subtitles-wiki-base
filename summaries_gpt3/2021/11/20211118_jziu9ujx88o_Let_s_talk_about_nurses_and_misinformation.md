# Bits

Beau says:

- Nurses received exciting news regarding accountability for spreading misinformation related to COVID-19 vaccines.
- A joint statement from various nursing associations emphasized nurses' professional accountability for the information they provide to the public.
- Nurses who spread misleading information may face disciplinary action by their board of nursing.
- Misinformation can jeopardize public health and endanger nurses' licenses and careers.
- The statement aims to combat the profitability of misinformation spread by individuals with credentials.
- Nurses, particularly those on the front lines in ER and ICU settings, welcomed this news with relief and satisfaction.
- Fighting both the public health crisis and misinformation has left nurses exhausted.
- The spread of misinformation by credentialed individuals can have deadly consequences.
- Nurses are hopeful that the decline in misinformation from credentialed sources will make it less believable.
- Many individuals have used their professional credentials as a shield to profit from spreading misinformation.

# Quotes

- "Misinformation is profitable."
- "They're tired of fighting the public health issue and they're tired of fighting the misinformation."
- "That misinformation is deadly."
- "A lot of people have used those letters after their name as a shield."
- "Y'all have a good day."

# Oneliner

Nurses celebrate increased accountability for spreading COVID-19 vaccine misinformation, aiming to combat the deadly impact of false information profiteers.

# Audience

Nurses, healthcare professionals

# On-the-ground actions from transcript

- Contact nursing associations for guidance on combating misinformation (suggested)
- Support efforts to hold accountable those spreading misleading information (implied)
- Stay informed and vigilant against misinformation in healthcare settings (suggested)

# Whats missing in summary

The emotional weight conveyed by nurses' relief and hope for a decline in misinformation spread by credentialed individuals.

# Tags

#Nurses #Misinformation #COVID19 #Accountability #Healthcare