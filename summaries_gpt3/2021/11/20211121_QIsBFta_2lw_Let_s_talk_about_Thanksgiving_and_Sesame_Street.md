# Bits

Beau says:

- Beau sets the stage for a Thanksgiving family dilemma involving Ted Cruz, Sesame Street, and social progress.
- A man is reluctant to visit his father for Thanksgiving due to his father's admiration for Ted Cruz and disapproval of Sesame Street's introduction of an Asian-American character.
- The man's wife is Asian, with a family history of internment during World War II, and his father unfairly expects her to represent all Asian people.
- Beau praises Sesame Street as not just a kids show, but a tool for education and addressing difficult topics like food insecurity, addiction, and gender equality.
- He advocates for even more progressive content on Sesame Street, including characters discussing adoption, refugees, health, and gender equality.
- Beau points out that Sesame Street has a long history of addressing challenging topics and being inclusive and diverse.
- He criticizes those opposed to Sesame Street's progressive programming, labeling them as socially regressive individuals who resist change and uphold unjust power structures.
- Beau concludes with optimism, stating that social progress always prevails in the long run, despite resistance from those wanting to maintain the status quo.

# Quotes

- "Sesame Street isn't woke enough."
- "Humanity will crash through it and society will move forward and I am fairly certain that Big Bird and Oscar will be leading the charge."
- "Those who want to uphold the past, the status quo, to keep people down so they can stay comfortable, they always lose."

# Oneliner

Beau navigates a Thanksgiving dilemma involving Ted Cruz, Sesame Street, and social progress, advocating for more progressive content and addressing resistance to change.

# Audience

Viewers, Activists, Parents

# On-the-ground actions from transcript

- Support inclusive and diverse programming like Sesame Street (implied).
- Engage in difficult but necessary conversations with children about social issues (implied).
- Advocate for progressive content that educates and empowers children (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the role of Sesame Street in education and addressing social issues, along with a call for more progressive content on the show.