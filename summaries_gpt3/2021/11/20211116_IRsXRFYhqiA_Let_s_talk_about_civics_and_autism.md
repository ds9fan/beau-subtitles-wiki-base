# Bits

Beau says:

- A parent reached out about their child learning civics in the eighth grade in Florida.
- The child is experiencing a disconnect between what they are taught in civics class and what they see in the world.
- Beau researched autism before suggesting a framing device to address this disconnect.
- Despite hours of talking to experts on autism, Beau found a disconnect between academic research and practical application.
- Speaking to autistic adults provided Beau with more insight than speaking to academics.
- Beau views the Declaration of Independence as a promise or ideal to strive towards.
- He notes that the system may not function as it should due to human imperfections.
- There's a concern about the push for patriotic education and mythology in teaching civics.
- Beau believes that promoting healthy skepticism towards political leaders is necessary.
- He points out the significant gap between the promise of equality and the reality experienced by eighth-graders.

# Quotes

- "Nobody knows anything about autism."
- "That disconnect is real."
- "What it should be and what it is are two very different things."
- "A healthy skepticism of political leaders is probably something we need to encourage."
- "We're doing such a bad job of keeping those promises."

# Oneliner

A parent's concern about the disconnect between civics education and reality leads Beau to seek insight on autism, revealing a need for practical application over research in academia.

# Audience

Parents, educators, policymakers

# On-the-ground actions from transcript

- Talk to autistic adults for better insight on autism (suggested)
- Encourage healthy skepticism towards political leaders (exemplified)

# Whats missing in summary

The full transcript delves into the disconnect between academic research and practical application, urging society to bridge the gap between ideals and reality in civic education.

# Tags

#Civics #Autism #Education #Academia #Equality