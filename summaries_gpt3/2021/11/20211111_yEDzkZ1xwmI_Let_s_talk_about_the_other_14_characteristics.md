# Bits

Beau says:
- Explains 14 characteristics of a certain ideology, comparing Lawrence Britt's and Umberto Eco's work.
- Cult of tradition is about appealing to the past for greatness, rejecting modernism.
- Action for action's sake involves impulsive decisions for appearance over effectiveness.
- Disagreement is considered treason, fear of difference is used to unite against scapegoats.
- Obsession with plots and viewing enemies as both too strong and too weak.
- Pacifism is seen as the enemy, life must be a constant struggle.
- Contempt for the weak and hero culture where everyone is deemed heroic.
- Machismo and weaponry, selective populism, and favoring the uneducated are key characteristics.

# Quotes
- "Contempt for the weak. People that constantly kick down."
- "Everybody's a hero. There's a culture surrounding heroism, even if it's not really heroic."
- "Pacifism is the enemy, and the reason this is the case is because life has to be a struggle."

# Oneliner
Beau explains 14 characteristics of a certain ideology, pointing out the dangerous patterns present in modern politics.

# Audience
Political analysts, activists

# On-the-ground actions from transcript
- Educate others on recognizing and understanding the 14 characteristics of a concerning ideology (implied).

# Whats missing in summary
The detailed breakdown and examples provided by Beau in the full transcript give a deeper understanding of the characteristics of a concerning ideology.

# Tags
#Ideology #UmbertoEco #Politics #WarningSigns #ContemporaryIssues