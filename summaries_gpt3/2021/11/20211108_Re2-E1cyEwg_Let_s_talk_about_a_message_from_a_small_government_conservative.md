# Bits

Beau says:

- A small government conservative Republican is feeling disconnected from the Republican Party.
- The Republican Party is no longer for small government conservatives, but rather nationalists.
- Small government conservatives are now more in alignment with the Democratic Party.
- Being a small government conservative means accepting individual responsibility for the community.
- Beau suggests reading works by Lawrence Britt and Umberto Eco to understand political strategies.
- Beau predicts political moves based on what a fascist might do, not what a Republican might do.
- The essence of being a small government conservative is allowing others their freedoms as long as they don't harm anyone.
- Republican big government now focuses on limiting individual liberty, contrasting with Democrats who aim to uplift working-class people.
- The disconnect felt by small government conservatives stems from the shift in the Republican Party's values.
- Beau encourages the small government conservative to focus on community and individual responsibility.

# Quotes

- "I have never asked myself, what would a Republican do next? I've asked myself, what would a fascist do next?"
- "The reason you feel disconnected is because they're not small government conservatives anymore."
- "Small government conservatives are more in alignment with the Democratic Party."
- "Republican big government now is about curtailing individual liberty."
- "You have to accept that responsibility. I got it."

# Oneliner

A small government conservative Republican feels disconnected from the current Republican Party and finds alignment with Democratic values, focusing on community responsibility.

# Audience

Small government conservatives

# On-the-ground actions from transcript

- Read works by Lawrence Britt and Umberto Eco to understand political strategies (suggested).
- Focus on community and individual responsibility (implied).

# Whats missing in summary

The full transcript provides a deeper insight into the shift in values within political parties and the impact on small government conservatives.

# Tags

#SmallGovernment #RepublicanParty #PoliticalShifts #CommunityResponsibility #Alignment