# Bits

Beau says:

- Recent developments in a certain movement in the United States are being discussed, with advice for those with family members involved.
- A small civil war is happening within a certain alphabet-themed theory world.
- Len Wood released an audio claiming to be a chat between himself and former General Flynn, a key figure in this movement.
- In the audio, Flynn dismisses the movement as "total nonsense," which could significantly impact its followers.
- Despite initial reactions of disbelief, the audio might cause doubt among the movement's adherents.
- An open letter on Telegram from a former believer explains why he's stepping away, citing unfulfilled promises and personal hopes tied to the movement.
- Many followers of this theory were manipulated or sought hope due to personal stressors.
- The collapse of their belief system can be extremely stressful for these individuals who had invested heavily in it.
- Suggestions are made to give these individuals space and time to process recent events rather than using logic to convince them.
- It is advised not to push individuals too hard as they navigate their way out of this information silo.

# Quotes

- "And Len Wood released on Telegram audio of a recording that claims to be a conversation between himself and former General Flynn."
- "However, at the same time, something else circulating on Telegram is an open letter from somebody who believed in the movement."
- "Some of them really wanted it to be true because they are authoritarians at heart."
- "If I had a family member who had fallen down this little information silo, I'd give them space right now."
- "Maybe that's true. I don't know. But I would certainly give them time to process the things that are going on around them."

# Oneliner

Recent developments in a certain movement in the US lead to internal strife, prompting advice for those with involved family members to give space and time for processing.

# Audience

Family members of individuals involved in movements.

# On-the-ground actions from transcript

- Give space to family members involved in certain movements (suggested).
- Allow time for processing recent events (suggested).

# Whats missing in summary

The nuanced emotions and personal struggles of individuals deeply entrenched in a collapsing belief system.

# Tags

#US #Movement #BeliefSystem #FamilySupport #Advice