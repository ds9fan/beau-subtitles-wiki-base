# Bits

Beau says:

- Introducing a different perspective on "Help Me Ruin Thanksgiving Dinner" focusing on dealing with a liberal mom who is a labor organizer.
- The challenge is explaining that liberal policies don't go far enough and uphold harmful systems, but facing resistance and glazed-over eyes during these talks.
- Seeking a framing device to address this issue in both personal and labor organizing situations.
- Suggests two methods: one involving negotiation framing for organized labor understanding, and the other presenting an idealistic dream for long-term change.
- In negotiation framing, starting with a strong position and making the opposition come further to meet halfway seems to work, even if not fully believed.
- Presenting an idealistic dream of maximum freedom, cooperation, and quality of life for all is another approach to shift beliefs, even if seen as unattainable in the present.
- Encouraging people to strive towards the dream rather than just conceding ground might lead to more tangible shifts in beliefs.
- Both negotiation framing and idealistic dreaming have their merits in reaching and persuading a liberal mom or those allied with similar goals but pragmatic in approach.

# Quotes

- "Remind them that it's not. We should be striving for that world where everybody gets a fair shake."
- "This is where we want to get to. The thing is, most people like to make dreams a reality."
- "We want to get to that world of maximum amount of freedom, for the maximum amount of people."

# Oneliner

Beau suggests negotiation framing and idealistic dreaming as methods to shift beliefs and reach a liberal mom or pragmatic allies.

# Audience

Progressive activists

# On-the-ground actions from transcript

- Frame arguments as negotiations (suggested)
- Present idealistic dreams for change (suggested)

# Whats missing in summary

Detailed examples of how negotiation framing and idealistic dreaming have influenced beliefs and actions in real-life scenarios.

# Tags

#Activism #Progressive #PoliticalChange #FamilyDynamics #LaborOrganizing