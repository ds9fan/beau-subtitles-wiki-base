# Bits

Beau says:

- Income gap affects willingness to vaccinate kids: higher income, quicker vaccination.
- Education not likely the primary factor in vaccination discrepancies.
- Lower income families face barriers like transportation and work concerns for vaccination.
- Offering help with transportation could make a difference in vaccination rates.
- Committee reveals Trump administration's delayed response to CDC warnings in February.
- CDC's inability to brief due to upsetting Trump caused three months delay in response preparation.
- Delay in response preparation potentially cost lives due to ego-driven narrative control.
- Time is lives in organizing a response, especially in public health emergencies.
- Muzzling experienced individuals hindered effective mitigation efforts.
- The importance of remembering the consequences of delayed responses and narrative control in public health crises.

# Quotes

- "Time is lives in organizing a response, especially in public health emergencies."
- "His ego costs time, and when you're talking about organizing a response, time is lives."
- "The virus doesn't care about the political spin. It never did."
- "That needs to be remembered."
- "Offering help with transportation could make a difference in vaccination rates."

# Oneliner

Income influences vaccination rates; transportation barriers affect lower-income families. Trump's ego delayed public health response, costing lives.

# Audience

Parents, Community Members

# On-the-ground actions from transcript
- Offer to provide transportation assistance for families to get their kids vaccinated (exemplified)
- Advocate for transparent and timely public health communication (implied)

# Whats missing in summary

The full transcript provides additional details on the income gap in vaccination rates and the specific consequences of delayed response due to political interference.

# Tags

#PublicHealth #Vaccination #IncomeGap #TrumpAdministration #DelayedResponse