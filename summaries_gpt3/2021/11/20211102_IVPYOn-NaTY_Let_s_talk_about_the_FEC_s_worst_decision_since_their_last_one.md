# Bits

Beau says:

- The Federal Election Commission made a troubling decision allowing foreign entities to fund referendums in the United States, raising concerns about foreign influence.
- There are worries about political figures acting as agents for foreign intelligence services and the implications of this decision.
- This decision opens up avenues for obtaining favor through referendums, potentially influencing election outcomes.
- Beau points out the potential strategy of using referendums to mobilize specific voting blocs, like churchgoers, to support certain political candidates.
- From a counterintelligence perspective, this decision is a nightmare due to the risks of corruption and external influence.
- Beau expresses concern about the increased potential for corruption at both federal and local levels because of this decision.
- States have the authority to enact their laws regarding foreign funding of referendums, which could help prevent further corruption.
- Beau suggests considering proposing ballot initiatives or referendums at the state level to counteract the FEC's decision.

# Quotes

- "This is just another way to obtain favor."
- "From a counterintelligence standpoint, this is a nightmare."
- "Yet another avenue to funnel money, to encourage corruption."

# Oneliner

The FEC's decision allowing foreign funding of referendums raises concerns about corruption and foreign influence in US politics.

# Audience

Voters, Activists, Legislators

# On-the-ground actions from transcript

- Propose a ballot initiative or referendum at the state level to counteract the FEC's decision (suggested).

# Whats missing in summary

The full transcript provides a detailed analysis of the risks associated with the FEC's decision and suggests potential actions to address the concerns raised. Viewing the entire video will provide a more in-depth understanding of the topic. 

# Tags

#FEC #ForeignInfluence #Corruption #Referendums #USPolitics