# Bits

Beau says:

- Overview of the infrastructure bill and where the money is going.
- The White House estimates the bill will create 2 million jobs.
- Detailed breakdown of funding allocations, such as $39 billion for public transit and $65 billion for internet infrastructure.
- Noted investments in removing lead pipes, electric charging stations, and increasing electric grid resiliency.
- $110 billion earmarked for roads and bridges, the largest investment in bridges since the national highway system inception.
- The bill is considered a good start but not sufficient for the country's full infrastructure needs.
- The progressive members known as "the squad" voted against the bill to ensure social and climate infrastructure were addressed simultaneously.
- Republicans crossed the aisle to vote for the bill, some out of necessity and others to undermine the squad's leverage.
- Beau sees the squad's decision to vote against the bill as maintaining their promise and sending a message without causing harm.
- There are more components of the infrastructure package that the Biden administration aims to pass.

# Quotes

- "It's good, but not great."
- "Them voting against it is just honoring that promise."
- "Republicans crossed the aisle to make up their votes."
- "They sent a message to Pelosi saying hey if we say we're gonna vote against it we will."
- "It's just a thought."

# Oneliner

Beau breaks down the infrastructure bill's allocations and explains the squad's strategic voting against it to ensure broader infrastructure needs are met.

# Audience

Policy enthusiasts, activists, voters

# On-the-ground actions from transcript

- Contact your representatives to advocate for comprehensive infrastructure bills that address social and climate needs (implied).

# Whats missing in summary

Further details on the additional components of the infrastructure package proposed by the Biden administration.

# Tags

#InfrastructureBill #PublicTransit #ProgressivePolitics #BidenAdministration #CommunityAction