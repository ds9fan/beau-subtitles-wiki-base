# Bits

Beau says:

- A teacher wants to prevent her Uncle Bob from ranting about a theory during Thanksgiving dinner.
- The theory is critical race theory (CRT), which is often misunderstood by those who oppose it.
- Beau suggests creating a new concept, Critical History Theory (CHT), to redirect the conversation.
- CHT focuses on examining the intersection of history and law in the US to challenge mainstream approaches to justice.
- By removing race from CRT and inserting history, Beau aims to make the concept more palatable to Uncle Bob.
- Beau advises starting with historical laws like the National Firearms Act to demonstrate the impact of past legislation.
- Connecting past laws to present consequences can help Uncle Bob understand the importance of learning from history.
- Beau recommends discussing laws with unintended or negative consequences to illustrate the need for scrutiny and reform.
- By introducing the racial aspect of CHT later in the conversation, Beau aims to reveal underlying motivations behind opposition to CRT.
- Beau underscores the need to challenge ingrained American mythology that whitewashes history and perpetuates harmful narratives.

# Quotes

- "Most people who are ranting about CRT couldn't define it if their life depended on it."
- "The purpose of history is to look back so you don't make the same mistakes."
- "It's about the possibility of slowly dismantling power structures that have kept people down."

# Oneliner

A teacher tackles Thanksgiving dinner controversy by introducing Critical History Theory, challenging American mythology, and addressing racism subtly.

# Audience

Teachers, educators, activists

# On-the-ground actions from transcript

- Introduce Critical History Theory (CHT) in educational settings to challenge mainstream approaches to justice (suggested).
- Start dialogues on historical laws and their impacts on society to foster understanding and reform (suggested).
- Encourage critical thinking and reflection on past legislation and its consequences (suggested).

# Whats missing in summary

The full transcript provides detailed strategies for navigating difficult conversational topics and challenging ingrained beliefs by reframing concepts and engaging in meaningful historical dialogues.

# Tags

#ThanksgivingDinner #CriticalRaceTheory #CHT #AmericanMythology #Racism #Education