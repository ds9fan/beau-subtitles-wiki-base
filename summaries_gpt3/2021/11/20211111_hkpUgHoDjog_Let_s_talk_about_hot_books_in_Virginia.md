# Bits

Beau says:

- Addressing the removal of school books in Virginia, specifically in Spotsylvania, where officials proposed burning books.
- School board members in Spotsylvania suggested throwing books into a fire to eradicate "bad stuff" and wanted to display the books before burning them.
- Beau questions the logistics behind burning books in the community and how they plan to demonstrate this process.
- Noting that representatives from Cortland and Livingston are advocating for literal book burning.
- Expresses concern for individuals swayed by fear-mongering tactics, pointing out the danger of following a similar path to oppressive regimes.
- Observes a rapid progression in the adoption of characteristics seen in historical authoritarian regimes within the United States political landscape.
- Emphasizes the urgency of recognizing the alarming direction the country is moving towards, with government officials discussing burning controversial books.
- Urges for national attention on the situation in Spotsylvania, hoping it serves as a wake-up call for people to take action and strive for a better future.

# Quotes

- "We have moved on to the literal book burning portion of the show."
- "The 14 characteristics that exemplify the worst regimes in history are being parroted in the United States today."
- "This is, I'm hoping that this becomes a national story."
- "If nothing up until now has served as the wake-up call that you need, this should be it."
- "The phone's ringing."

# Oneliner

In Spotsylvania, Virginia, officials suggesting book burning prompts urgent call to action to prevent a dangerous slide into authoritarian practices nationwide.

# Audience

Community members, activists, educators

# On-the-ground actions from transcript

- Raise awareness about the situation in Spotsylvania, Virginia by sharing information on social media and engaging in community dialogues (suggested)
- Reach out to local news outlets and media platforms to encourage coverage of the book burning proposal in Spotsylvania (implied)

# Whats missing in summary

The emotional impact and urgency conveyed in Beau's message can be better understood by watching the full video.

# Tags

#BookBurning #Education #Authoritarianism #CommunityAction #UrgentAppeal