# Bits

Beau says:

- A woman in her 30s reached out for help ruining Thanksgiving dinner after her uncle condescendingly shut down her previous attempt to bring up reparations at the table.
- Beau advises focusing on making a moral argument when trying to win a moral debate, suggesting using indirect approaches to lead the person to acknowledge certain truths.
- He recommends using the example of Agent Orange exposure in Vietnam to help Uncle Bob understand the concept of reparations without directly discussing slavery or segregation.
- By getting Uncle Bob to recognize the government's responsibility in compensating those impacted by Agent Orange exposure, Beau guides the argument towards discussing how government policies like segregation limited generational wealth.
- Beau suggests reframing the argument from a moral standpoint to a property rights case to explain why reparations should be considered for past injustices.
- The goal is to guide the debate towards acknowledging the impact of historical injustices on generational wealth without directly confronting moral beliefs.

# Quotes

- "You're trying to win a moral debate."
- "Make a different case. Better yet, get them to make it."
- "Maybe it's time we start talking about it at Thanksgiving dinner."
- "Y'all have a good day."

# Oneliner

A guide to reframing the reparations debate during Thanksgiving dinner to focus on property rights and generational wealth impact.

# Audience

Americans engaging in Thanksgiving debates.

# On-the-ground actions from transcript

- Start open, rational dialogues about sensitive topics like money, politics, and religion at family gatherings (suggested).
- Encourage discussing challenging subjects to prevent harm and improve understanding (suggested).

# Whats missing in summary

The detailed step-by-step process Beau outlines for reframing the reparations debate without directly confronting moral beliefs. 

# Tags

#Thanksgiving #Reparations #FamilyDinner #GenerationalWealth #PropertyRights