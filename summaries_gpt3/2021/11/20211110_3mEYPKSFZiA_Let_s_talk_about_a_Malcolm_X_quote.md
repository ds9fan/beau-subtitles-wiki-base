# Bits

Beau says:

- Exploring a famous quote by Malcolm X about white liberals, often misrepresented and misunderstood.
- Malcolm X's warning about white liberals and their potential danger, compared to white conservatives.
- Malcolm X's emphasis on not falling prey to any political party and advocating for black Americans to find their own source of power.
- The historical context of Malcolm X's statements in relation to the Southern strategy and political power dynamics.
- Clarifying the misconception that Malcolm X's quote endorsed the Republican Party or right-wing ideologies.
- Beau's use of the term "white liberal" in contemporary contexts, pointing out the manipulation and empty promises made by politicians.

# Quotes

- "White liberals, well, they were foxes. White conservatives were wolves."
- "He was trying to advocate for black Americans to find their own source of power."
- "Whether it came from a wolf or a fox. But the fox is more likely to make promises they don't intend to keep."
- "Malcolm X today would be a right wing pro-gun conservative. Well, I mean, one out of three, I guess, isn't too bad."
- "That's what he was talking about."

# Oneliner

Beau dives into Malcolm X's quote on white liberals, clarifying its true meaning and advocating for black empowerment while debunking misconceptions.

# Audience

Activists, Political Historians

# On-the-ground actions from transcript

- Reach out to black creators on YouTube for impassioned explanations of Malcolm X's quotes (suggested)
- Advocate for black empowerment and community building (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of Malcolm X's perspective on white liberals and conservatives, urging for self-empowerment within the black community.

# Tags

#MalcolmX #WhiteLiberals #PoliticalPower #BlackEmpowerment #Misconceptions