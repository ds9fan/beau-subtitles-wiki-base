# Bits

Beau says:

- Explains the importance of images, observations, and stereotypes in understanding systemic racism.
- Shares a message from someone questioning the existence of systemic racism based on personal experiences.
- Challenges the listener to question where their perceptions of race come from.
- Points out the influence of media, news, and societal narratives in shaping stereotypes.
- Addresses the impact of systemic racism on how black individuals are perceived.
- Defines systemic racism as a byproduct of historical oppression and institutionalization.
- Urges the listener to trust their observations about the good qualities of black individuals.
- Encourages acknowledging and dismantling manufactured stereotypes.
- Compares societal stereotypes to personal misconceptions.
- Emphasizes the discomfort of confronting societal views on race.

# Quotes

- "Your own observations. 100% of the time, this is what it's like."
- "Trust your observations when it comes to whether or not it's going to rain."
- "The stereotype is manufactured. It's a holdover."
- "You know those stereotypes aren't right."
- "Your evidence is in your message."

# Oneliner

Beau challenges a listener to confront manufactured stereotypes by trusting their own observations about systemic racism.

# Audience

Listeners reflecting on personal perceptions.

# On-the-ground actions from transcript

- Trust your observations about the good qualities of individuals (implied).
- Acknowledge and challenge manufactured stereotypes (implied).

# Whats missing in summary

The full transcript provides a deep dive into understanding systemic racism through personal observations and societal influences.