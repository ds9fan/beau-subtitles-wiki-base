# Bits

Beau says:

- Analyzes tactics used by commentators, pundits, and news outlets to manipulate audiences online.
- Advises viewers to be wary of emotionally manipulative headlines.
- Explains how headlines play into confirmation bias by appealing to different political affiliations.
- Points out techniques like inserting politically divisive names, asking leading questions, and using quotes as headlines to shape narratives.
- Talks about personifying organizations or individuals in headlines to influence perceptions.
- Advises on being critical of polls and understanding how statistics can be misleading.
- Warns about false links leading to misinformation and using images out of context to incite outrage.
- Discloses methods like factual yet misleading statements and intentionally unclear communication.
- Describes assigning intent as a tactic used to manipulate audiences by creating false narratives.
- Encourages viewers to be vigilant, especially about headlines that provoke outrage daily.

# Quotes

"Headlines, in theory, should pique your curiosity. They shouldn't encourage you to form an opinion right away."
"A headline should inform you, not inflame you."
"If every single day this news entity has a new thing for you to be angry about, you need to be on guard."

# Oneliner

Beau explains manipulative tactics used in media to influence audiences and warns against emotionally charged headlines creating false narratives.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Fact-check sources before sharing information (implied)
- Verify statistics and polling methodologies (implied)
- Reverse image search to verify the context of images (implied)
- Be vigilant against emotionally manipulative headlines (implied)

# Whats missing in summary

In-depth examples and analyses of various manipulative tactics used in media and how to combat misinformation. 

# Tags

#MediaLiteracy #InformationConsumption #Misinformation #CriticalThinking #FactChecking