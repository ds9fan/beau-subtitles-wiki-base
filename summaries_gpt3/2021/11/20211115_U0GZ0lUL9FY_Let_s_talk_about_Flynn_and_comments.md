# Bits

Beau says:

- Flynn made a concerning statement about having one religion in the nation, which goes against American ideals of religious freedom and separation of church and state.
- He calls on patriots to vocalize their opposition to such ideas, as they contradict the values enshrined in the US Constitution.
- Beau references historical documents like the Treaty of Tripoli and quotes from founding figures to support the separation of religion and government.
- He warns that such rhetoric may increase before the midterms as a desperate move by those on a losing streak.
- Beau points out that supporting the intertwining of religion and government is a betrayal of the founding principles of the country.

# Quotes

- "If you consider yourself a patriot, you must vocalize your opposition to this."
- "There is no basis in American patriotism when it comes to supporting this garbage."
- "They are telling you who they are, and you better believe it."

# Oneliner

Beau warns against the dangerous rhetoric of pushing for one religion in the nation, urging patriots to stand up against such betrayal of American ideals.

# Audience

Patriots, defenders of American ideals

# On-the-ground actions from transcript

- Oppose any attempts to intertwine religion and government (implied)
- Educate others on the importance of the separation of church and state (implied)

# Whats missing in summary

Beau's passionate delivery and historical references provide a compelling argument against the dangerous idea of one religion in the nation.

# Tags

#ReligiousFreedom #SeparationOfChurchAndState #AmericanIdeals #Patriotism #Midterms