# Bits

Beau says:

- Conservative parents are receiving messages from their children who are feeling lost and conflicted about how to interact with them.
- The children express disillusionment with the hypocrisy they see in adults who preach moral values but don't practice them.
- Despite being raised Catholic, the children have distanced themselves from the church due to perceived hypocrisy.
- They strive to live by the values they were taught, including supporting progressive ideals and helping the disenfranchised.
- The children express frustration at being labeled as idealists and socialists by their parents for wanting to create positive change.
- The father mentioned in the message is portrayed as someone who has changed ideologically, becoming more dismissive of progressive ideals.
- The children struggle to communicate with their parents, especially when there are fundamental disagreements on social and political issues.
- Beau encourages conservative parents to understand that their children learned these values from them and urges them not to give up on fostering idealism.
- He acknowledges the importance of younger generations embracing idealism and striving for a better world, despite the challenges they face.
- Beau reminds the children that they have the power to shape a more equitable and compassionate society by upholding the values instilled in them.

# Quotes

- "Your kids, the ones that you're now calling a socialist, the ones you're alienating, where did they get the ideas? They got them from you."
- "The job of every parent is to make their child better than they are. They succeeded in that."
- "Don't let them down. Be better than they are. Fix the world."

# Oneliner

Conservative parents are urged to understand the ideological shifts in their children, who learned their values from them and now embody idealism to create a better world.

# Audience

Parents, Youth

# On-the-ground actions from transcript

- Have open and honest dialogues with your parents about your beliefs and values, encouraging mutual understanding (implied).
- Uphold the values of compassion, progressiveness, and empathy in your daily interactions and support efforts that aim to create positive change (implied).
- Embrace idealism and continue striving for a better world, even in the face of opposition or disagreement (implied).

# Whats missing in summary

The full transcript provides a deep insight into the generational divide in beliefs and values, urging both parents and children to communicate effectively and work towards a more compassionate and just society.

# Tags

#GenerationalDivide #FamilyValues #ProgressiveIdeals #Communication #YouthEmpowerment