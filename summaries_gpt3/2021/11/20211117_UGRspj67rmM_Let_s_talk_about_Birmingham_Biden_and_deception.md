# Bits

Beau says:

- Talks about a development in Birmingham, Alabama related to Biden's infrastructure package.
- Mentions a proposed belt line project around Birmingham, which is expected to boost economic development and create jobs.
- Notes the economic impact of completing the northern belt line could exceed $2 billion in 10 years.
- Points out that Gary Palmer, a congressperson representing Alabama's 6th District, released a press statement celebrating the project.
- Reveals that Gary Palmer actually voted against the infrastructure package he's praising.
- Raises the issue of politicians trying to take credit for projects they opposed.
- Suggests that politicians may prioritize partisan interests over the benefits to their constituents.
- Emphasizes the disconnect between politicians' actions and the interests of the people they represent.
- Encourages people to be aware of politicians who celebrate projects they voted against.
- Concludes by calling out politicians for prioritizing their own interests over those of the community.

# Quotes

- "They don't represent you. They represent themselves."
- "When it comes to the infrastructure package that's gone through, the components that are on the way, the reality is it is great for the economy."
- "Could it be better? Sure. But it's unlikely they're going to get this through because of stuff like this."
- "Expect to see a lot of this. These packages that the administration is pushing through over Republican opposition, they're good for the country."
- "They know it's good for the people of Birmingham, but they will still vote against it at a partisan interest."

# Oneliner

Beau exposes politicians celebrating projects they voted against, revealing the disconnect between their actions and constituents' interests.

# Audience

Voters

# On-the-ground actions from transcript

- Watch out for politicians who celebrate projects they opposed (suggested)

# Whats missing in summary

The full transcript provides more insight into the disconnect between politicians' actions and their constituents' interests.