# Bits

Beau says:

- Teacher at Muncie Central High School assigned V for Vendetta project, drawing parallels between modern social movements and the plot of the movie.
- Posters created by students were put up in the hallway, causing discomfort to the establishment and school resource officers.
- Establishment asked teacher to remove the posters, sparking a student-led demonstration and remote learning for students.
- Beau suggests watching V for Vendetta to understand the parallels between the movie and the events at the school.
- Beau criticizes the establishment for reacting negatively and militarizing the situation, likening them to the antagonists in the movie.
- Students are drawing their own conclusions, not being indoctrinated, based on what they see and hear.
- The situation at Muncie Central High School mirrors the plot of V for Vendetta, with students being silenced and controlled by the establishment.
- Beau encourages the school to acknowledge their mistake and embrace critical thinking rather than trying to suppress it.
- Beau warns that attempts to keep students ignorant and prevent critical thinking are happening in school districts across the country.
- Suppressing critical thought in education can lead to a society easily misled and manipulated.

# Quotes

- "You are reinforcing it every step of the way."
- "Trying to stop students from thinking critically about what they see and what they hear and their own experiences is indoctrination."
- "They see what exists. They see what's around them. And yes, there are parallels. They're not wrong."

# Oneliner

Teacher assigns V for Vendetta project, students draw parallels with modern movements, establishment suppresses dissent, Beau criticizes lack of critical thinking in education.

# Audience

Students, Teachers, Activists

# On-the-ground actions from transcript

- Speak out against attempts to suppress critical thinking in schools (suggested)
- Get involved in advocating for education that encourages critical thought (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the events at Muncie Central High School and the broader implications on education and critical thinking.

# Tags

#Education #CriticalThinking #StudentActivism #Indoctrination #Schools