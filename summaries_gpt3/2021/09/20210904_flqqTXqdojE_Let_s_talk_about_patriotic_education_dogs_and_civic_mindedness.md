# Bits

Beau says:

- Exploring the connection between patriotic education and civic responsibility.
- Questions the opposition to patriotic education.
- Argues that patriotic education aims to create obedience through conditioning, not civic-mindedness.
- Points out the lack of encouragement for basic community care practices by politicians advocating for patriotic education.
- Criticizes the focus of patriotic education on indoctrination rather than true civic education.
- Emphasizes that civic-minded individuals are harder to manipulate by politicians.
- Calls out the ulterior motives behind pushing for patriotic education, linking it to manipulation and control.
- Expresses skepticism towards the genuine intentions of those advocating for patriotic education.
- Concludes with a critical reflection on the true objectives behind patriotic education.

# Quotes

- "The goal isn't to create civic minded people. The goal is to create Pavlov's dogs."
- "They want to indoctrinate students. They want to create students that are easily manipulated."
- "They want, for lack of a better word, less educated people."
- "They don't care about education. They want indoctrination."
- "Y'all have a good day."

# Oneliner

Beau questions the true motives behind patriotic education, arguing that it aims for obedience through conditioning, not civic-mindedness, criticizing its indoctrination focus and lack of genuine civic education.

# Audience

Educators, activists, concerned citizens

# On-the-ground actions from transcript

- Question the motives behind proposed educational reforms (implied)
- Advocate for comprehensive civic education in schools (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the dangers of using patriotic education for indoctrination rather than genuine civic education.