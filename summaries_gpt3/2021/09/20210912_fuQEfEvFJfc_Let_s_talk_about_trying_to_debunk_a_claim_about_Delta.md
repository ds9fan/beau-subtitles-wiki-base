# Bits

Beau says:

- Investigating a claim about vaccines causing the Delta variant due to viral mutation.
- Acknowledging the plausibility of viruses mutating to survive.
- Exploring the concept of proving a negative claim and the challenge it presents.
- Emphasizing the importance of using a timeline as a scientific tool in investigations.
- Pointing out the global nature of the issue and the timeline of the Delta variant's identification in India before vaccines.
- Clarifying that the mechanics of evolution apply to natural immunity as well, leading to various flu strains.
- Asserting that vaccines did not cause the Delta variant, proven by a simple timeline.
- Suggesting fact-checking before making unfounded claims.

# Quotes

- "Life will find a way type of thing."
- "So no, the vaccines did not cause the Delta variant."
- "Have you ever tried to prove a negative?"
- "Why don't you fact-check before you open your ugly blank, blank, blank mouth?"
- "Y'all have a good day."

# Oneliner

Beau investigates a claim about vaccines causing the Delta variant, debunks it with a timeline, and stresses the importance of fact-checking.

# Audience

Science enthusiasts, vaccine skeptics

# On-the-ground actions from transcript

- Fact-check claims before spreading misinformation (suggested)
- Utilize timelines as a tool for investigating scientific claims (suggested)

# Whats missing in summary

The importance of using scientific tools like timelines to debunk misinformation and the global nature of the issue.

# Tags

#Vaccines #DeltaVariant #DebunkingMisinformation #FactChecking #Timeline