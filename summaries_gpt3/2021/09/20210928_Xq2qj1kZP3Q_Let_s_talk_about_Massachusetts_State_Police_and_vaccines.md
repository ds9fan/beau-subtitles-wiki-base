# Bits

Beau says:

- Acknowledges being critical of law enforcement but praises the local departments for being responsive to the community and quick to address issues.
- Shares an incident where a deputy planting evidence was caught and dealt with swiftly without public outrage.
- Talks about the SWAT team in a nearby jurisdiction made up of ex-military deputies who train for high-risk scenarios to protect the public, despite the dangers involved.
- Critiques the Massachusetts State Police union's claim of officers resigning over vaccine mandates, contrasting the union's rhetoric with the reality of only one officer actually resigning.
- Questions the commitment of officers who refuse to follow mandated policies and suggests that it may indicate biases affecting their work on the streets.
- Argues that officers unwilling to mitigate risks to the public by following policy should not be in law enforcement and sees their resignation as a way to weed out those not fit for the job.
- Expresses that enforcing unjust laws and refusing to follow safety protocols are different and believes that officers who can't follow safety protocols shouldn't be in the force.
- Concludes that even if multiple officers were to resign over policy disagreements, it wouldn't harm the state or its people, seeing it as a potential positive in removing those not adhering to policy.

# Quotes

- "Protect and serve. Mitigate risk to the public. If you're not willing to do that, you shouldn't be a cop."
- "This seems like an easy way to get rid of people who won't follow policy, who have biases they let impact their judgment."
- "At the end of this, I can't see any way in which this actually harms the state of Massachusetts or the people of Massachusetts."

# Oneliner

Massachusetts State Police union's claim of officers resigning over vaccine mandates scrutinized, with Beau questioning their commitment to public safety and policy adherence.

# Audience

Community members

# On-the-ground actions from transcript

- Hold local law enforcement accountable for following mandated policies and ensuring public safety (implied).

# Whats missing in summary

Detailed explanation on how biases and policy non-adherence can affect law enforcement's ability to protect and serve effectively.

# Tags

#LawEnforcement #PoliceUnion #VaccineMandates #PublicSafety #PolicyAdherence