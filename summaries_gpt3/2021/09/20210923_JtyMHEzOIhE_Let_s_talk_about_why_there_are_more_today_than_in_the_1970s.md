# Bits

Beau says:

- Exploring the expansion of public health initiatives in the United States over the years.
- Responding to a query about the increase in vaccinations from 18 to close to 75 by age 18.
- Acknowledging the business aspect of vaccinations in a capitalist system.
- Mentioning the immunization of babies before they leave the hospital and even in the womb.
- Emphasizing that the rise in vaccinations is not just a business strategy but a response to successful outcomes of past vaccines.
- Noting the significant drop in infant mortality rates and childhood mortality over the years, attributing some of it to safety measures like bike helmets and car seat mandates.
- Mentioning the decline in cases and deaths due to vaccines pre-1980, prompting the continued pursuit of vaccination as a public health tool.
- Quoting a statistic from The Lancet about millions of lives saved by vaccination in low and middle income countries, particularly children under five.
- Stressing the effectiveness and non-conspiratorial nature of vaccines in managing public health and mitigating risks.
- Concluding with the idea that the use of vaccines is based on their proven success in saving lives and managing public health.

# Quotes

- "Everything is commodified. Everything is a business. It absolutely is a business."
- "They use it because it works."
- "It's nothing conspiratorial."
- "Why not use a tool that works?"
- "Y'all have a good day."

# Oneliner

Beau explains the expansion of vaccinations in the US, attributing it to past successes and effective public health management, not conspiracy.

# Audience

Health advocates, policymakers, general public

# On-the-ground actions from transcript

- Get vaccinated to protect yourself and others (implied)
- Support public health initiatives and vaccination programs (implied)

# Whats missing in summary

In-depth analysis of specific diseases targeted by vaccines and their impact on public health.

# Tags

#PublicHealth #Vaccinations #PublicSafety #Healthcare #USHealthcare