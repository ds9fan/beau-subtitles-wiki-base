# Bits

Beau says:

- Beau receives a message asking how black men should respond to being called the word "boy" and the subsequent attacks that follow.
- The message describes the internal struggle of feeling tense, accused, and pushed towards violent reactions when faced with racism.
- Beau shares his personal experience of limited encounters with racism compared to the continuous discrimination faced by black men.
- He acknowledges the privilege of being able to respond violently without severe consequences due to his skin tone.
- Beau points out the hypervigilance and constant readiness for attacks that result in a form of PTSD from racial discrimination.
- Despite not having a clear solution, Beau encourages the message sender to use platforms like YouTube to convey positive messages without filtration.
- He suggests that finding solutions to dealing with racism may be an internal journey rather than seeking external advice.

# Quotes

- "Nobody likes an angry black man."
- "How do we deal with this literal torture?"
- "I don't have a solution. I don't have any advice."
- "Sometimes the question alone is the thought."
- "Y'all have a good day."

# Oneliner

Beau addresses the challenges faced by black men when responding to racial slurs and violence, pointing towards internal reflection and positive messaging as potential paths forward.

# Audience

Black men, allies

# On-the-ground actions from transcript

- Utilize platforms like YouTube to convey positive messages without filtration (suggested)
- Engage in internal reflection on how to respond to racial discrimination (implied)

# Whats missing in summary

The full transcript dives into the internal struggles and external pressures faced by black men in responding to racial slurs and violence, urging for internal reflection and positive messaging as potential ways forward.

# Tags

#Racism #BlackMen #PTSD #PositiveMessaging #InternalReflection