# Bits

Beau says:

- General Milley's actions have sparked a shift in talking points among the right wing.
- The focus has moved from accusing Milley of treason to undermining former President Trump.
- Critics failed to think critically and blindly followed the initial narrative.
- Even if Milley's actions undermined Trump, it was to prevent a catastrophic war, not for personal gain.
- Suggesting Trump's intentions were to start a war reinforces the need for Milley's actions.
- Moving the goalposts portrays Trump as someone not acting in the best interests of the country.
- Beau encourages accepting evidence, admitting faults, and learning from mistakes.
- Some individuals may realize they were misled into believing every action is a scandal.
- Blindly following those against the country's best interest is misguided.

# Quotes

- "Moving the goalposts portrays Trump as a loose cannon, a bad guy, somebody who did not have the best interest of the United States at heart."
- "The better response when faced with evidence to the contrary is to actually look at it, accept it, accept the correction, admit that you were wrong, and go from there."

# Oneliner

General Milley's actions shift the narrative from treason to undermining Trump, revealing the importance of critical thinking and accepting evidence.

# Audience

Critical Thinkers

# On-the-ground actions from transcript

- Fact-check narratives before blindly following them (implied)
- Encourage critical thinking and accepting evidence (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the shift in talking points surrounding General Milley's actions and the importance of critical thinking in analyzing political narratives.