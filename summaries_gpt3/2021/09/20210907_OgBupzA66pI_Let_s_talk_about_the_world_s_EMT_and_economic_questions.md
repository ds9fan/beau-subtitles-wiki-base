# Bits

Beau says:

- Explains the misconception that if the standard of living in other countries increases, the standard of living in the United States must decrease.
- Breaks down the concept of gross domestic product (GDP) and how it relates to economic output.
- Challenges the idea that there is a finite amount of economy to go around and that resource exploitation is necessary for economic growth.
- Counters the belief that there is no money in peace by illustrating how humanitarian efforts and peace-building activities can be profitable.
- Provides examples of how companies, particularly those focused on renewable energy and infrastructure development, can profit from peace.
- Advocates for investing in companies that "hill rather than kill" as a positive alternative to defense contractors.
- Teases future episodes where more questions will be addressed.

# Quotes

- "We live in a world where everything is commodified."
- "There's only so much that can be done."
- "Everything makes money."
- "It's just not defense contractors that are gonna make a lot of money."
- "It's going to primarily go to companies that hill rather than kill."

# Oneliner

Beau explains economic misconceptions about global standards of living, GDP, resource exploitation, peace profitability, and the potential for companies to profit from humanitarian efforts.

# Audience

Global citizens

# On-the-ground actions from transcript

- Support humanitarian organizations (exemplified)
- Advocate for renewable energy and infrastructure development (exemplified)
- Invest in companies focused on peace-building efforts (exemplified)

# Whats missing in summary

In-depth exploration of economic misconceptions and the potential for profit in humanitarian and peace-building efforts.

# Tags

#GlobalEconomics #Misconceptions #Peacebuilding #HumanitarianEfforts #RenewableEnergy