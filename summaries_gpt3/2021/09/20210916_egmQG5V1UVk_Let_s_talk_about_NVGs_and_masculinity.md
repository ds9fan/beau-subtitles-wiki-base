# Bits

Beau says:

- Addressing the concept of owning the night with NVGs left behind in Afghanistan.
- Providing examples of equipment like a medic's bag or an electrician's bag to illustrate the importance of training over tools.
- Critiquing the American mindset that having equipment equates to capability.
- Pointing out the fallacy in thinking that owning NVGs means owning the night.
- Emphasizing the significance of training and knowledge over equipment in achieving missions.
- Calling out the influence of this mindset on foreign policy decisions and domestic rhetoric.
- Challenging the habit of associating masculinity with violence and the militarization of the United States.
- Asserting that possessing tools does not automatically translate to proficiency in using them.
- Suggesting that opposition forces in Afghanistan are unlikely to dramatically change tactics due to acquiring night vision equipment.
- Concluding with the idea that tools do not make one capable, but rather training and knowledge do.

# Quotes

- "The gun doesn't make the man. The tools don't necessarily mean you know how to use it."
- "The reason those special teams, those high-speed teams, the reason they're special is because if need be they can accomplish the mission without all that stuff."
- "The problem is this idea is now so ingrained in American culture that it's influencing foreign policy decisions."
- "The militarization of the United States, the habit of conflating masculinity with violence, this needs to stop."
- "It's not how it works. It's the training. It's the knowledge."

# Oneliner

Beau challenges the American mindset that owning equipment equals capability, stressing the importance of training over tools and critiquing the influence of this concept on foreign policy and domestic rhetoric.

# Audience

Americans

# On-the-ground actions from transcript

- Challenge the mindset that owning equipment automatically equals capability (implied).
- Advocate for investing in training and knowledge over solely focusing on equipment (implied).
- Educate others about the importance of skills and expertise in achieving missions effectively (implied).

# Whats missing in summary

Beau's passionate delivery and nuanced breakdown of the misconception that owning tools equates to skill, urging for a shift towards valuing training and knowledge over equipment.

# Tags

#AmericanMindset #TrainingOverTools #Militarization #ForeignPolicy #KnowledgeIsPower