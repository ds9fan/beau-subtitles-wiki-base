# Bits

Beau says:

- Talks about possible water shortages in the Southwestern United States and how it might impact Lake Powell and Mead.
- Projects a 22% chance of Lake Mead dropping below 1,025 feet by 2023, affecting water usage for 25 million people.
- Mentions a 66% chance of Lake Mead dropping below 1025 feet by 2025, leading to required water usage reductions.
- States a 3% chance of Lake Powell not being able to generate hydroelectric power next year and a 34% chance the following year.
- Points out that these projections are based on historic drought levels in paleoclimate records, indicating severe drought conditions.
- Emphasizes the need for immediate action to mitigate climate change and address impending resource issues.
- Suggests that without action, water scarcity may lead to the costly trucking in of water, exacerbating climate issues.
- Urges the initiation of infrastructure projects now to prevent severe impacts in the future.
- Expresses concern over the lack of media attention given to such projections, despite their historical accuracy.
- Stresses the importance of taking significant and timely action to address the ongoing challenges rather than waiting for a return to normalcy.

# Quotes

- "This isn't going to fix itself."
- "Maybe we should just go ahead and start working on the infrastructure to fix these issues now."
- "We need significant action and we need it soon."

# Oneliner

Beau warns of impending water shortages in the Southwest, urging immediate action to prevent severe impacts and mitigate climate issues.

# Audience

Residents of the Southwestern United States

# On-the-ground actions from transcript

- Start working on infrastructure projects now to address water scarcity (suggested)
- Take significant action immediately to mitigate climate change impacts (suggested)

# Whats missing in summary

The full transcript provides detailed insights into the projected water shortages in Lake Powell and Mead, outlining the urgency for proactive measures to combat climate change and prevent severe consequences.

# Tags

#WaterShortages #ClimateChange #SouthwestUSA #Infrastructure #ResourceManagement