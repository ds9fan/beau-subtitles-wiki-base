# Bits

Beau says:

- Stresses the importance of understanding different philosophies beyond common misconceptions.
- Shares a comment about taxing the rich and the limitations of progressivism.
- Talks about the wealth gap and the changing economic landscape in the country.
- Describes a homesteader's perspective on value, wealth, and ownership.
- Connects the homesteader's ideals to leftist economic philosophies like communism.
- Mentions the disconnect between people's actual beliefs and their understanding of economic philosophies.
- Encourages gaining a basic understanding of various economic theories and philosophies.
- Points out that many rural people unknowingly live by leftist economic ideals in their daily lives.
- Addresses the stigma and confusion surrounding leftist economic philosophies in the United States.
- Emphasizes the need to look beyond common associations and understand the core philosophy behind terms like communism.

# Quotes

- "The real problem that no one addresses is the system that allowed the wealth to get sucked up to the top in the first place."
- "We have thrown money at these problems, and have only proved they cannot be solved with money."
- "You are to the left of me. What you are describing is communism."
- "But if you do, you're better informed."
- "Don't let the way words get used in common conversation dictate whether or not you're going to look at the philosophy behind that word."

# Oneliner

Understanding different economic philosophies beyond misconceptions can lead to surprising insights and inform personal beliefs, even for those who may not identify with leftist ideals.

# Audience

Philosophy enthusiasts, economic thinkers.

# On-the-ground actions from transcript

- Read up on different economic theories and philosophies to gain a better understanding (exemplified).
- Challenge common misconceptions about leftist economic ideals in your community (suggested).

# Whats missing in summary

The full transcript provides a comprehensive exploration of the impact of economic philosophies on individual beliefs and societal structures, urging a deeper understanding beyond surface-level associations.

# Tags

#EconomicPhilosophies #Progressivism #WealthInequality #Communism #LeftistIdeals