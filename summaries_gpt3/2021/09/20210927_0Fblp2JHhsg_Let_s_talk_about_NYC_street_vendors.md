# Bits

Beau says:

- Saw sanitation workers in New York City throwing away food from a produce stand, which is common in rural areas.
- Mentioned buying seafood and produce from pickup trucks without permits where he's from.
- People defended the permitting process in NYC for food safety after Beau's comments.
- Referenced a 2011 cantaloupe listeria outbreak to explain the focus on food safety.
- Criticized the notion that vendor permits in NYC ensure food safety, calling it more of a theater.
- Argued that business incentives and competition may be the real reasons behind vendor permits.
- Raised concerns about the complex permitting process affecting access to fresh fruit in food deserts.
- Mentioned non-profits like the street vendor project helping navigate the permitting maze.
- Suggested that the permitting process in NYC may be more about revenue for the city than safety.
- Noted a case in the Bronx where community pushback prevented sanitation workers from trashing a stand.

# Quotes

- "Buying veggies and fruit out of the back of a pickup truck is incredibly common where I'm at."
- "Generally what keeps the food safe is the fact that it's really bad business if your customers get sick."
- "So if it's not about food safety what is it about? What's it really about? Bringing in money."
- "This to me is a major issue when you are talking about an area that doesn't have a lot of fresh fruit."
- "Non-profits have been started to help people navigate the maze."

# Oneliner

Beau questions the true motive behind NYC's food vendor permits, suggesting they prioritize revenue over safety, impacting access to fresh produce.

# Audience

Community members, food advocates

# On-the-ground actions from transcript

- Support non-profits like the street vendor project in aiding people to navigate complex permitting processes (suggested).
- Advocate for simplified and fair food vendor permit regulations to improve access to fresh produce in underserved areas (implied).

# Whats missing in summary

Deeper insights on the impact of complex permitting processes on marginalized communities' access to fresh food. 

# Tags

#FoodSafety #VendorPermits #AccessToFreshProduce #CommunityAdvocacy #FoodDeserts