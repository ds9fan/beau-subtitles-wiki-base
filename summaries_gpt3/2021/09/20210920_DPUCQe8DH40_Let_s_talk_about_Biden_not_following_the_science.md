# Bits

Beau says:

- The Biden administration's decision to prioritize booster shots over sending doses overseas is facing pushback from medical professionals and scientists.
- The debate is not about the effectiveness of booster shots but about medical ethics and whether it's right to prioritize boosters for Americans over providing initial shots to those overseas.
- Beau raises the ethical question of whether fully vaccinated Americans should receive booster shots while many people overseas haven't even had their first shot.
- He criticizes the administration's possible nationalistic lens on vaccine distribution, focusing on Americans first.
- Beau suggests that getting your booster shot as soon as possible might actually help accelerate overseas distribution rather than delaying it through protest.
- Waiting to pressure the government to change its stance on booster shots may backfire and lead to more doses being kept in the US due to increased vaccine hesitancy.

# Quotes

- "The US government is going to look at the science through a nationalistic lens."
- "It's about ethics, about what you do with science, and whether or not that's bad."
- "There are ways to get the most vulnerable populations in the U.S., get them their booster shot, and send doses overseas at the same time."
- "Just go get it quickly so they can begin getting it overseas."
- "They're going to push even more heavily and keep more doses here."

# Oneliner

The Biden administration's prioritization of booster shots over global vaccine distribution raises ethical questions around nationalistic lens on science and medical ethics. 

# Audience

Global Health Advocates

# On-the-ground actions from transcript

- Get your booster shot as soon as possible to potentially accelerate overseas distribution (implied).

# Whats missing in summary

Beau's engaging delivery and nuanced perspective can best be appreciated by watching the full video. 

# Tags

#BidenAdministration #VaccineDistribution #BoosterShots #MedicalEthics #Nationalism