# Bits

Beau says:

- After discussing a woman explaining healthcare standards at a gas station, many argued against restrictions, claiming it was anti-freedom.
- Idaho is currently operating under crisis standards of care, contradicting the belief that such situations wouldn't occur.
- An excerpt detailing a Universal Do Not Resuscitate order for adult patients during a public health emergency under crisis standards of care is shared.
- The order specifies that aggressive interventions should be provided but no attempts at resuscitation should be made.
- The low survival likelihood after cardiac arrest for adult patients and the significant risks to healthcare workers from resuscitation are emphasized.
- Beau urges people, especially in Idaho, to get vaccinated, wear masks, and follow necessary precautions to avoid the situation currently faced by Idaho.
- He warns against risky activities like riding four-wheelers due to the increased danger posed by the current crisis standards of care.
- Beau stresses the preventability of the situation in Idaho through vaccination and following measures known to slow down the spread and increase survival rates.
- Despite the availability of vaccines and knowledge on effective preventive measures, the situation in Idaho exemplifies the consequences of prioritizing "freedom" over public health.
- Beau concludes by encouraging viewers to take necessary precautions and hints at the irony of valuing freedom while endangering lives.

# Quotes

- "Freedom's just another word for nothing left to lose."
- "Never is now. Never is now."
- "But freedom, right?"
- "This is kind of preventable."
- "We know what increases survival rates. But freedom, right?"

# Oneliner

Idaho's crisis standards of care exemplify the consequences of prioritizing "freedom" over public health, urging vaccination and precautions to prevent preventable crises.

# Audience

Public health advocates

# On-the-ground actions from transcript

- Get vaccinated, wear masks, and follow necessary precautions to prevent crises like the one in Idaho (exemplified).
- Avoid risky activities that could lead to injury in areas facing crisis standards of care (implied).

# Whats missing in summary

The emotional impact of witnessing the consequences of prioritizing personal freedom over public health and safety. 

# Tags

#PublicHealth #Idaho #CrisisStandardsOfCare #Vaccination #PreventativeMeasures