# Bits

Beau says:

- Describes a story about an inspiring instructor at Fort Benning who called everyone "Ranger," setting the expectation that they could achieve becoming a Ranger.
- The instructor's simple tactic of using the term "Ranger" instilled inspiration and motivation in all his students, regardless of their current status.
- Becoming a Ranger at Fort Benning is challenging, and the instructor's belief in each individual's potential motivated them.
- Beau relates this story to his approach in his videos, assuming viewers are seeking to improve themselves and aiming to set the expectation that they can grow.
- Despite knowing that not all viewers will change their perspectives, Beau operates on the belief that they want to better themselves and encourages them to do so.

# Quotes

- "He wanted to inspire them. He wanted to set the expectation that they could and that they should."
- "It doesn't cost me anything to come from the point of view that they can and that they want to."
- "He puts it out there that he believes that every single one of them can make it."
- "I want to set the expectation that that is what they want to do and that they can."
- "He was somebody who was foundational to them changing and to them really seeking to strive to hit that next level."

# Oneliner

Inspirational story of an instructor calling everyone "Ranger" to set high expectations, mirroring Beau's approach in encouraging growth and self-improvement.

# Audience

Viewers seeking self-improvement

# On-the-ground actions from transcript

- Encourage and inspire others by setting high but achievable expectations for them (exemplified).

# Whats missing in summary

The emotional impact of inspiring others and fostering a belief in their potential.

# Tags

#Inspiration #Motivation #SelfImprovement #SettingExpectations