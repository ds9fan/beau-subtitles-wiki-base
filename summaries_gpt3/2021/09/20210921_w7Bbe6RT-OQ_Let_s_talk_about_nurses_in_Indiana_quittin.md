# Bits

Beau says:

- Addressing claims about nurses quitting due to vaccine refusal in Indiana.
- Indiana Health University had 125 part-time employees who didn't provide proof of vaccination.
- No indication that the employees were nurses or that they didn't believe in the vaccine.
- Even if the claim were true, it's only a small percentage of the total staff.
- Over 99.5% of the employees at that location were vaccinated.
- Media may create an implied debate where there isn't one.
- The majority of medical professionals support vaccination.
- The situation with the 125 employees doesn't indicate a real debate within the medical community.
- Speculations on reasons for not providing vaccine documentation.
- More than 99.5% vaccination rate among 34,000 employees.

# Quotes

- "There isn't real debate about this."
- "You're talking about 125 employees."

# Oneliner

Beau clarifies misconceptions about nurses quitting over vaccines, showing overwhelming support for vaccination among medical professionals.

# Audience

Healthcare professionals

# On-the-ground actions from transcript

- Verify information before spreading it (implied)
- Support vaccination efforts in your community (implied)

# Whats missing in summary

Beau provides a fact-check and perspective on the situation of nurses quitting over vaccines, showcasing the strong support for vaccination within the medical community.

# Tags

#Vaccine #Nurses #Misconceptions #FactChecking #Healthcare