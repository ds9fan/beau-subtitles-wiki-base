# Bits

Beau says:

- Talks about the power of words and their meanings, focusing on keeping people engaged in a discourse.
- Mentions receiving a message questioning his avoidance of certain terms, advocating for precise and direct communication.
- Explains his choice of using softer and less emotionally charged words to maintain audience engagement.
- Shares insights from analytics, particularly the significance of emotionally charged words in retaining viewers' attention.
- Describes a shift in his rhetoric from using inflammatory and emotional language to softer tones to encourage critical thinking.
- Conveys the importance of reason and rationality in changing minds rather than emotional appeals.
- Refers to a poll indicating the impact of softer versus strong rhetoric on public opinion.
- Advocates for employing soft language in everyday communication to foster independent thinking.
- Acknowledges the limitation of soft rhetoric in creating like-minded individuals but values critical thinking and open debate.

# Quotes

- "If you make it through the first 30 seconds, you will make it through the main body."
- "That's more my goal."
- "It just creates people who are thinking on their own."
- "If people are thinking reasonably and rationally, there's room for discussion and debate."
- "Y'all have a good day."

# Oneliner

Beau explains the power of words, advocating for softer rhetoric to foster critical thinking and open debate, aiming to change minds through reason over emotion.

# Audience

Communicators and debaters

# On-the-ground actions from transcript

- Adjust your communication style to use softer and less emotionally charged words to encourage critical thinking and open debate (suggested).

# Whats missing in summary

The full transcript provides a nuanced exploration of the impact of rhetoric on audience engagement and critical thinking, offering practical insights for communicators and debaters.

# Tags

#PowerOfWords #Rhetoric #CriticalThinking #Communication #Debate