# Bits

Beau says:

- Addressing tattoos and what they teach about a new law in Texas.
- Explaining the base belief behind questioning personal choices.
- Criticizing the idea of legislating morality with examples like prohibition and the war on drugs.
- Pointing out the ineffectiveness of using laws to control behavior like visible tattoos.
- Describing how people of different economic classes navigate around restrictive laws.
- Arguing that such laws target and impact those without means and a voice.
- Revealing the true intention behind legislation like the new law in Texas.
- Stating that the goal is not to end the behavior but to provide judgmental people with someone to judge.
- Condemning the prioritization of such laws over more pressing issues in Texas.
- Concluding that these laws serve to create scandal and foster judgment rather than solve problems.

# Quotes

- "There's a base belief among a lot of people that suggests however you run your life is how you think society as a whole should be run."
- "So when you go on vacation, you get your tattoo. You cross state lines, do it somewhere else."
- "It's about taking a bunch of judgmental people and giving them somebody to judge."
- "There are a whole bunch of things going on in Texas that are way more important than this."
- "It's designed to create scandal and give you somebody to look down on."

# Oneliner

Beau addresses the ineffective nature of trying to legislate morality, using tattoos to illustrate how such laws disproportionately impact those without means, ultimately serving to provide judgmental individuals with targets rather than solve real issues.

# Audience

Policy Makers, Activists

# On-the-ground actions from transcript

- Advocate for policies that address real issues impacting communities, rather than targeting and marginalizing vulnerable populations (implied).
- Support initiatives that prioritize the voices and needs of marginalized groups in legislative decision-making processes (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how legislation aimed at morality often fails to achieve its intended goals and instead exacerbates existing inequalities and injustices in society.

# Tags

#Legislation #Inequality #Society #Morality #Texas