# Bits

Beau says:

- Biden administration cleaning up Trump's messes.
- Mistakes made now by Biden administration will need cleaning up by next presidency.
- United States is not like a compact car but an 18-wheeler, slow to change course.
- Policies take time to show results after being enacted.
- Effects of policies, positive or negative, take time to manifest.
- Conservatives often point to the 1950s as the golden age of the United States.
- The golden age reference actually spans from 1945 to early 1960s.
- FDR's administration from 1933 to 1945 laid the groundwork for the perceived golden age.
- Massive government spending, jobs programs, and high tax rates on the top created prosperity in the past.
- Similar policies may create real change and prosperity now post long war and economic issues.

# Quotes

- "United States is not a compact car that can stop on the dime. It's an 18-wheeler."
- "The effects of policies, positive or negative, generally speaking, take years to be shown."
- "The period conservatives point back to was created by somebody who championed all of the policies they oppose."

# Oneliner

Biden cleans Trump’s messes; Biden's mistakes need cleaning; Policies take time; Conservatives' golden age view; FDR's policies laid groundwork.

# Audience

History enthusiasts, political analysts, policymakers.

# On-the-ground actions from transcript

- Advocate for policies that focus on massive government spending, jobs programs, and high tax rates on the top to create real change (implied).

# Whats missing in summary

The full transcript provides a deep dive into historical policies and their long-term effects, urging for a reflection on current policy directions for future prosperity.

# Tags

#Policy #Government #HistoricalAnalysis #Policies #Prosperity