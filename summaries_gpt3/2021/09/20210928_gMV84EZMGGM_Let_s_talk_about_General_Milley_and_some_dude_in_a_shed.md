# Bits

Beau says:

- General Milley testified about his phone call with his Chinese counterpart.
- Someone messaged Beau, comparing General Milley's testimony to Beau's videos.
- The comparison suggests that General Milley and Beau may be sharing talking points.
- Beau argues that General Milley was just doing his job, suggesting the calls are routine.
- Beau mentions having friends in D.C. but denies receiving classified information from them.
- He implies that news outlets sensationalize routine activities for profit.
- Beau points out that calming nerves and de-escalation are key in military communications.
- He criticizes news outlets for not providing the full context of routine military calls.
- Beau believes the scandal narrative is driven by profit-seeking news outlets.
- He suggests that sensationalizing normal activities creates unnecessary panic among viewers.

# Quotes

- "Maybe General Milley was just doing his job."
- "They're trying to get ratings. They are sensationalizing the run-of-the-mill, the normal."
- "Calls like this between militaries probably occur on a weekly basis."
- "It's not abnormal. It's not a scandal. It's not a story."
- "The scandal profit motivator is probably the more likely scenario."

# Oneliner

Beau debunks sensationalism around General Milley's routine military calls, criticizing profit-driven news outlets for creating unnecessary panic.

# Audience

News consumers

# On-the-ground actions from transcript

- Fact-check news stories before reacting (implied)
- Support independent media sources (implied)
- Stay informed about routine government activities (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of how routine military communications are sensationalized by profit-driven news outlets, urging viewers to seek out unbiased information sources and not fall prey to unnecessary panic. 

# Tags

#GeneralMilley #RoutineMilitaryCalls #Sensationalism #NewsConsumers #MediaBias