# Bits

Beau says:

- Addresses the importance of achieving change and how to do it effectively.
- Draws an analogy using military terminology, referring to the "tip of the spear."
- Explains the different roles within social movements: the tip, the rest of the spear, and the support staff.
- Emphasizes the significance of not alienating those who may not fully understand the movement's ideologies.
- Talks about the negative effects of trashing individuals who are helping in carrying out the message of change.
- Urges for more inclusivity and understanding within progressive movements.
- Stresses the need for ideas to reach a wider audience to create real change.
- Critiques the infighting and ideological purity that can slow down progress.
- Encourages being less critical and creating a more welcoming environment for those willing to support the cause.

# Quotes

- "If you're part of the tip of the spear, you're part of the ideological advance. You're always going to be ahead."
- "Going after and trashing the people who are helping to carry it out, even if they have a bad take, maybe go a little bit easier on them."
- "There shouldn't be a lot of gates thrown up. They should be very accessible."
- "You can't hold them accountable for something they literally do not know."
- "If you want change, the idea needs to get to as many people as possible."

# Oneliner

Beau explains the dynamics of social movements through a military analogy, stressing inclusivity and understanding to achieve effective change.

# Audience

Change advocates

# On-the-ground actions from transcript

- Support and embrace individuals willing to carry out the message of change, even if they may not fully understand the ideologies (suggested).
- Encourage inclusivity and understanding within progressive movements (exemplified).

# Whats missing in summary

The full transcript provides a detailed analysis of the dynamics within social movements and the importance of inclusivity and understanding for effective change.

# Tags

#Change #SocialMovements #Inclusivity #ProgressiveIdeas #IdeologicalAdvance