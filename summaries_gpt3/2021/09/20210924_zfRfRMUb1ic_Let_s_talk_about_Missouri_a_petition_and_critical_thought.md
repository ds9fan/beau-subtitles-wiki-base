# Bits

Beau says:

- Students at Park Hill South High School circulated a petition requesting that slavery be reinstated.
- The school district is tight-lipped about the petition, calling it a disciplinary issue to protect the students' future.
- Missouri recently probed whether students were learning objectionable material about race and proposed an amendment against teaching institutions were racist.
- State legislature hearings on race in education mainly consisted of white people.
- The state's actions seem to contradict the "this isn't who we are" narrative.
- Beau questions if Missouri truly stands against critical thinking and acknowledging the impacts of slavery.
- He calls for elected representatives to decide if students should critically think or endorse the pro-slavery petition.

# Quotes

- "This isn't who we are though, right?"
- "I don't know how people can say this isn't who we are, because it certainly seems like that's what Missouri is."
- "I think it's time that the elected representatives in the state of Missouri decide whether they want to allow students to be encouraged to critically think."
- "Anyway, it's just a thought."
- "Y'all have a good day."

# Oneliner

Students circulated a petition to reinstate slavery, prompting Beau to question Missouri's stance on critical thinking and race education.

# Audience

Educators, activists, students

# On-the-ground actions from transcript

- Contact elected representatives in Missouri to advocate for critical thinking and anti-racism education (suggested).

# Whats missing in summary

The full transcript provides more context on Missouri's recent actions regarding race education and critical thinking. Viewing the entire transcript will give a clearer picture of the state's stance on these issues.

# Tags

#Missouri #Education #CriticalThinking #RaceEducation #Slavery #Petition