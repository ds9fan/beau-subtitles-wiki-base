# Bits

Beau says:

- General Milley took actions post-January 6th to prevent the former president from retaining power unconstitutionally.
- People are calling Milley a traitor and suggesting charging him with treason for his actions.
- Milley, who could have retired comfortably, continued his service for an additional 20 years.
- Accusations of treason against Milley overlook the narrow constitutional definition of the crime.
- Milley's actions were in line with his oath to support and defend the Constitution against all enemies.
- His communication with his Chinese counterpart was to provide assurances due to concerns about the US arsenal under a President talking election fraud.
- Milley's actions were not a surprise; he had communicated his intentions in a memo on January 12th.
- Punishing or asking for Milley's resignation is a way to cover up failures and lack of duty by others.
- The focus on pinning blame on Milley after 40 years of service seems misplaced.

# Quotes

- "Milley did his job."
- "He didn't have a right to do the things that he did. He had a duty to."
- "They want to point to this conversation he had with his Chinese counterpart. Yeah, you know, when the President of the United States starts ranting about election fraud and saying that, you know, he should kind of retain power, I woul imagine that other countries get really nervous about the status of the most powerful arsenal in all of human history."

# Oneliner

General Milley took necessary actions post-January 6th to protect the Constitution, despite facing accusations of treason for preventing unconstitutional power retention by the former president.

# Audience

Politically aware citizens

# On-the-ground actions from transcript

- Support leaders who prioritize upholding the Constitution (exemplified)
- Defend against misinformation and hold accountable those who neglect their duties (exemplified)

# Whats missing in summary

The full transcript provides a deeper understanding of the context and reasoning behind General Milley's actions post-January 6th.