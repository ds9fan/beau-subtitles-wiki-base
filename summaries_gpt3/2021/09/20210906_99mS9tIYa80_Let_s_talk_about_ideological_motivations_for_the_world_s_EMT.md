# Bits

Beau says:

- Explains the ideological motivations behind the idea of a World EMT.
- People from different backgrounds support the idea for various reasons.
- Love and peace advocates see it as a way to avoid war and unnecessary loss, like Marianne Williamson.
- Those like Thomas Barnett support it because it's an effective tool to pursue American national interests without war.
- Acknowledges the need for global action to address economic inequalities beyond just the United States.
- Points out that many countries, despite being resource-rich, remain poor due to wealth extraction.
- Africa is emphasized as a region of increasing importance due to its resource wealth.
- Advocates for a shift in doctrine to build up unstable countries so they can pursue their own interests and avoid wealth extraction.
- Suggests a soft form of colonialism where wealth isn't extracted but countries are empowered.
- Believes the global income inequality can be addressed through such initiatives.
- Acknowledges the necessity for U.S. involvement in initiating global change for equality.
- Proposes a doctrinal shift in American foreign policy that could improve living standards and reduce war.
- Urges for incremental change starting in the United States to achieve a fairer world for everyone.
- Argues that this pragmatic approach is more effective than waiting for radical changes in the distant future.
- Asserts that the proposed shift in foreign policy could gain support across different ideologies due to its practicality.

# Quotes

- "It's incremental, sure. It's not super radical, sure, but it'll work and it could be implemented like tomorrow."
- "This is a doctrinal shift that could occur in American foreign policy and in international foreign policy as a whole."
- "You can get behind it. There's not an ideological system that would oppose this with the exception of those who really are colonialists."
- "It's pragmatic. It would work."
- "This is a way. It's incremental, sure. It's not super radical, sure, but it'll work and it could be implemented like tomorrow."

# Oneliner

Beau explains the ideological motivations behind a pragmatic foreign policy shift to address global inequalities and reduce war, advocating for incremental change starting in the United States.

# Audience

Global Citizens

# On-the-ground actions from transcript

- Advocate for a pragmatic foreign policy shift that addresses global inequalities and reduces war (suggested).
- Support initiatives aimed at empowering resource-rich but economically disadvantaged countries (implied).
- Engage in activism to push for incremental changes in foreign policy towards a fairer world for all (implied).

# Whats missing in summary

The detailed examples and nuances of various ideological perspectives supporting the World EMT concept.

# Tags

#PragmaticChange #GlobalInequalities #ForeignPolicy #IncrementalAction #ReduceWar