# Bits

Beau says:

- Texas passed a law enabling a website for neighbors to report socially undesirable behaviors.
- People on TikTok and Reddit are encouraging tips to overwhelm the system.
- The tips are not factual but aim to flood the system with information.
- Tips include advice like not copying and pasting, using real locations, and avoiding pro-choice slogans.
- Overwhelming the system with information is a tactic used to render mass surveillance less effective.
- This tactic on TikTok may force a reconsideration of how the tip website operates.
- While it may not change legislation, it could make processing information more difficult for authorities.

# Quotes

- "Overwhelming the system with information is a tactic used to render mass surveillance less effective."
- "This tactic on TikTok may force a reconsideration of how the tip website operates."
- "It's going to make it more difficult to process any information they obtained through this tip website."

# Oneliner

Texas law enables reporting on a website; TikTok teens flood it with fake tips to overwhelm surveillance tactics.

# Audience

Online activists

# On-the-ground actions from transcript

- Encourage others to flood platforms with information (exemplified)
- Provide guidance on how to create overwhelming but fake information (exemplified)

# Whats missing in summary

The full transcript provides a detailed look at how online communities can strategically flood platforms with information to thwart surveillance tactics.

# Tags

#Texas #Surveillance #TikTok #Activism #CommunityPolicing