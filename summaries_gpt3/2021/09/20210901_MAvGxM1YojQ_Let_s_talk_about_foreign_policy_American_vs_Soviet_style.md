# Bits

Beau says:

- Foreign policy is like a poker game where everyone's cheating.
- Americans view foreign policy as poker, Soviets view it as chess.
- Poker allows for more players and acknowledges cheating.
- Chess is elegant, strategic, and confrontational.
- Soviets' foreign policy was ideologically driven and confrontational.
- Poker analogy includes more players, like Vietnam and Afghanistan.
- Acknowledging other countries adds layers of fluidity in foreign policy.
- Everyone cheating in poker mirrors foreign policy dynamics.
- Chess results in conquest, while foreign policy is ongoing.
- Foreign policy is about power, not money.
- Power is the currency in foreign policy.
- Poker analogy is a better way to understand foreign policy dynamics.
- The analogy is a trope but offers a more comprehensive view.
- Beau suggests asking the Soviet embassy for their perspective on the analogy.

# Quotes

- "Foreign policy is like a poker game where everybody's cheating."
- "Poker analogy includes more players and acknowledges cheating."
- "Power is the currency in foreign policy."

# Oneliner

Beau explains how foreign policy is akin to a poker game with cheating, contrasting American and Soviet views, advocating for the poker analogy's inclusivity and fluidity over the elegant but confrontational chess comparison.

# Audience

Foreign policy enthusiasts

# On-the-ground actions from transcript

- Call the Soviet embassy and ask for their perspective on foreign policy analogies (suggested)

# What's missing in summary

Exploration of the nuances and intricacies of foreign policy dynamics and power struggles beyond traditional comparisons like poker and chess.

# Tags

#ForeignPolicy #Analogies #PowerDynamics #Inclusivity #SovietPerspective