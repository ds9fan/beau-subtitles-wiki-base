# Bits

Beau says:

- Explains the inclusion of opposition members in the new government in Afghanistan, suggesting it's unusual.
- Compares the incorporation of former conflict figures into the government to practices in other countries.
- Expresses mock shock at the idea of war heroes being part of a government, hinting at perceived hypocrisy.
- Points out the surprise at former prisoners now holding government positions.
- Mocks the media's shock and outrage over the situation, implying it's a common occurrence globally.
- Questions why commentators are acting surprised and trying to provoke outrage when such scenarios are normal.
- Challenges the media's attempt to manipulate public opinion for ratings by sensationalizing common events.

# Quotes

- "Can you believe that? I mean, I can't think of another country on the planet that would do that."
- "It's shocking, I tell you. Shocking."
- "If you're surprised by this development, if you're shocked and you believe that this is something Americans should be outraged by, I demand to know who you believed was going to be in these positions."
- "They just believe that they can convince the American people to be outraged by it, and therefore milk those ratings."
- "Anyway, it's just a thought. Y'all have a good day."

# Oneliner

Beau questions the mock outrage over Afghanistan's government formation and challenges media sensationalism for ratings.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Challenge media narratives (implied)
- Be critical of sensationalism (implied)

# Whats missing in summary

The full transcript provides a satirical take on media reactions to commonplace political events, urging critical thinking and questioning of sensationalized narratives.

# Tags

#Government #Media #PoliticalCommentary #Sensationalism #PublicOpinion