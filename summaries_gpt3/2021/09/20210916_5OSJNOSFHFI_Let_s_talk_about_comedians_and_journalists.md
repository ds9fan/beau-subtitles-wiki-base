# Bits

Beau says:

- Questions the distinction between journalism and comedy in progressive TV programs like John Oliver, Stephen Colbert, and Trevor Noah.
- Points out that comedians often adhere to journalistic ethics better than major networks.
- Talks about the challenges of achieving objectivity in journalism and praises the Associated Press for striving towards journalistic ideals.
- Mentions the subjective nature of journalism and the importance of making editorial decisions.
- References Hunter S. Thompson and Gonzo journalism, which combines facts with a touch of fiction and humor to get to the truth.
- Applauds comedians for using their platform to better the world and move beyond being mere spectators.
- Addresses the difficulty of balancing objectivity and profitability in journalism.
- Criticizes the current industry practice of presenting "both sides" of an issue, which may not always be objective or accurate.
- Points out that many issues have more than two sides and some may only have one.
- Talks about journalists pursuing independent avenues to get closer to the truth outside traditional media constraints.

# Quotes
- "They're trying to get off the sidelines."
- "Once you acknowledge that objective journalism really isn't a thing, you know it's subjective."
- "If you end up with a huge platform like these comedians have, you'd have to be kind of a horrible person not to want to help."
- "At some point that got lost."
- "They're trying to avoid comments that say, well why don't you tell us the other side to this thing that is an objective fact."

# Oneliner
Beau questions the blend of journalism and comedy, praises comedians for upholding journalistic ethics, and criticizes the industry's struggle with objectivity and profitability.

# Audience
Content creators, journalists, viewers

# On-the-ground actions from transcript
- Support independent journalists and content creators who strive for truth and ethics (implied)
- Encourage critical thinking and fact-checking in media consumption (implied)
- Advocate for diverse perspectives and nuanced storytelling in journalism (implied)

# Whats missing in summary
Beau's insightful analysis on the evolving landscape of journalism and the role of comedians in filling gaps left by traditional media outlets.

# Tags
#Journalism #Comedy #Ethics #ObjectiveReporting #GonzoJournalism