# Bits

Beau says:

- Addresses the distasteful habit in American politics of using tragedies for political gain.
- Refuses to name individuals who have recently passed away, citing the importance of not using their deaths for personal commentary or political points.
- Believes expressing condolences may come off as insincere and prefers to simply say "I'm sorry for your loss."
- Criticizes the idea of approaching grieving families in the future to tell them their loved one's death was in vain.
- Suggests focusing on the positive outcomes of actions taken, like the humanitarian airlift, rather than politicizing tragedies.

# Quotes

- "I typically just say I'm sorry for your loss and I leave it at that because it's not words that dulls the pain. It's time."
- "Please do not talk to their families."
- "The worthless pointless mission gave a hundred thousand people a new lease on life."

# Oneliner

Beau addresses the exploitation of tragedies in politics, refuses to name recent deceased individuals, and suggests focusing on positive outcomes rather than politicizing tragedies.

# Audience

Social commentators

# On-the-ground actions from transcript

- Respect the privacy of grieving families and refrain from approaching them to express opinions on their loved one's death (implied).

# Whats missing in summary

The full transcript provides a nuanced perspective on handling tragedies and respecting the dignity of those who have passed away.

# Tags

#AmericanPolitics #Tragedy #Condolences #Respect #HumanitarianAid