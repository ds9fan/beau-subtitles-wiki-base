# Bits

Beau says:

- Talks about Jonathan Swift's popular quote regarding reasoning people out of beliefs.
- Disagrees with the notion that it's useless to reason with those who weren't reasoned into their beliefs.
- Believes in the power of reason and facts to counter blind belief.
- Mentions the difficulty in reasoning with certain demographics in the United States.
- Argues that most people can be reached with the right combination of reason, fact, truth, and perhaps a bit of fiction.
- Points out historical proof that reason can help people overcome unreasonable beliefs.
- Encourages trying to reason with others, even when it seems frustrating.
- Emphasizes the importance of not giving up on trying to persuade others with reason.

# Quotes

- "It is useless to attempt to reason a man out of a thing he was never reasoned into."
- "People can be reasoned with, most, not all."
- "We don't have an option because we're not going to get them out of their position by appealing to their unreasonable nature."

# Oneliner

Beau challenges the idea that it's impossible to reason with those who weren't reasoned into their beliefs, advocating for the power of reason and urging continued efforts to persuade through logic.

# Audience

Skeptics and advocates for evidence-based reasoning.

# On-the-ground actions from transcript

- Engage in constructive dialogues with those holding differing beliefs (implied).
- Use a combination of reason, facts, and possibly some storytelling to convey your point effectively (implied).
- Persist in attempting to reason with others, even when faced with resistance (implied).

# Whats missing in summary

The full transcript includes examples and anecdotes that illustrate the effectiveness of using reason to challenge deeply held beliefs.

# Tags

#JonathanSwift #Reason #Beliefs #Logic #Persuasion