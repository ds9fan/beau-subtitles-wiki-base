# Bits

Beau says:

- Exploring how white people should respond to racism when they witness it.
- Advises using tactics like pretending not to understand bigoted jokes to make the perpetrator explain the underlying racism.
- Suggests using sarcasm to convey that racist behavior is unacceptable within a group.
- Talks about direct correction either by firmly stating not to talk like that in front of them or educating the person about their racist behavior.
- Mentions the exhausting task of explaining basic concepts of humanity to people repeatedly.
- Recommends stepping in to help mitigate risk if a situation escalates and could turn violent.
- Shares a personal experience of witnessing a racist altercation and the importance of immediate response to racism.
- Emphasizes the importance of taking action immediately to make life more tolerable for those facing racism.

# Quotes

- "Do something to mitigate it, to end it, to redirect it, to diffuse the situation."
- "There's a whole bunch of different tactics, and it depends on the kind of person you are."
- "How do you respond immediately?"
- "The answer is immediately."
- "Y'all have a good day."

# Oneliner

When witnessing racism, white people should respond immediately using tactics like misunderstanding bigoted jokes or direct corrections to make a difference and mitigate harm.

# Audience

White allies

# On-the-ground actions from transcript

- Immediately respond to instances of racism by using tactics like sarcasm, misunderstanding bigoted jokes, or direct corrections to combat it and make spaces more inclusive (implied).

# Whats missing in summary

Personal anecdotes and in-depth examples from the speaker's experiences are missing in the summary but provide a real-world context for the advice given.

# Tags

#Racism #Allyship #ImmediateAction #CommunityResponse #Tolerance