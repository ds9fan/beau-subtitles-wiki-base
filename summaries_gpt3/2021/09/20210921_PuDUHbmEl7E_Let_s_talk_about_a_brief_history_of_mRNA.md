# Bits

Beau says:

- Explains the timeline of research on mRNA vaccines dating back to the 1960s.
- Mentions emergency FDA authorization for COVID-19 vaccines in 2020.
- Points out that mRNA technology is not as new as perceived.
- Describes the history of mRNA vaccine trials on mice and humans.
- States that the basis of this research predates major historical events like the Cuban Missile Crisis.
- Encourages not to hesitate based on the longevity of research.
- Suggests that understanding past research can aid in looking towards the future.
- Emphasizes that looking back at historical context is beneficial for understanding current advancements.

# Quotes

- "This isn't actually new technology. It's been around for a really, really long time."
- "The basis of this research, it started before the Cuban Missile Crisis."
- "Sometimes looking back can help you look forward a little bit."

# Oneliner

Beau explains the extensive history of mRNA technology, debunking the notion of rushed research for COVID-19 vaccines and encouraging understanding past research for future progress.

# Audience

Research Enthusiasts

# On-the-ground actions from transcript

- Educate others on the historical timeline of mRNA vaccine research, exemplified
- Encourage individuals to look into the background of scientific advancements, exemplified

# Whats missing in summary

The full transcript provides a comprehensive overview of the extensive history of mRNA technology, debunking misconceptions about rushed research for COVID-19 vaccines and advocating for informed decision-making based on historical context.

# Tags

#Research #mRNA #Vaccines #History #COVID19 #Science