# Bits

Beau says:

- Beau receives a video request from someone stating that he is good at creating compelling arguments that prompt people to action.
- The urgent need for people to get vaccinated is emphasized as appointments are being missed and time slots left unfilled.
- Beau considers various approaches to encourage people to get vaccinated, including live-streaming his second shot and using persuasive arguments.
- Instead of the traditional approaches, Beau shares a powerful analogy about calling a number for federal reimbursement for a loved one's final expenses due to the public health crisis, contrasting it with the ease of scheduling a vaccine appointment.
- He underlines the importance of getting vaccinated to avoid being a burden on loved ones who might have to seek reimbursement for final expenses.
- Beau provides details about the eligibility criteria for federal reimbursement for final expenses, including the necessity for the incident to have occurred in the US after January 20, 2020.
- He points out the stark contrast between the half-million people calling for reimbursement and the availability of vaccine appointments with operators ready.
- Beau encourages viewers to get vaccinated promptly to protect themselves and others from potential health issues.
- The analogy of a busy signal for federal reimbursement versus operators standing by for vaccine appointments serves as a strong call to action for getting vaccinated.
- He ends the video with a reminder to take the necessary steps to ensure no one has to call the reimbursement number for their final expenses due to the public health issue.

# Quotes

- "The urgency is lost."
- "But you can go get your shot and make sure that nobody has to call that number about you."

# Oneliner

Beau underlines the urgency of getting vaccinated by contrasting the ease of scheduling a vaccine appointment with the arduous process of seeking federal reimbursement for final expenses due to the public health crisis.

# Audience

Public Health Advocates

# On-the-ground actions from transcript

- Schedule a vaccine appointment to protect yourself and others (exemplified)
- Share Beau's analogy about the importance of vaccination with others in your community (suggested)

# Whats missing in summary

The emotional impact of Beau's analogy and the urgency of getting vaccinated are best experienced by watching the full video.

# Tags

#Vaccination #PublicHealth #CallToAction #Urgency #CommunitySafety