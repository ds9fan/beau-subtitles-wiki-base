# Bits

Beau says:

- Responding to a specific request, Beau talks about Montana's gun nullification law that recently got signed.
- Clarifies that it is not a nullification law; it prevents state and local agencies from assisting in the enforcement of federal firearm laws.
- Mentioning that the ATF can still enforce federal laws, despite the state law.
- Emphasizes that this concept is not new, as it involves local jurisdictions choosing not to assist in enforcing federal laws they find unjust.
- Points out that Montana could be considered a sanctuary state due to this stance.
- Notes the Republican Party's historical opposition to this idea, making their current support interesting.
- Suggests that the embracing of this idea by Republicans might lead to a change in their view on sanctuary cities.
- Predicts that the law won't have a significant impact as most people already assume Montanans are armed.
- Anticipates the mental gymnastics opponents of sanctuary cities will face due to this law being supported by the Republican Party.

# Quotes

- "It's not quite the owning of the liberals that you may believe."
- "I cannot wait to see the mental gymnastics necessary to oppose sanctuary cities now."

# Oneliner

Beau explains Montana's gun law isn't nullification but a refusal to assist in federal enforcement, challenging Republican views on sanctuary cities.

# Audience

Political observers

# On-the-ground actions from transcript

- Contact local representatives to understand and support or oppose similar legislation (implied).

# Whats missing in summary

The full transcript provides additional context and humor around the topic of Montana's gun nullification law and its implications for political dynamics.