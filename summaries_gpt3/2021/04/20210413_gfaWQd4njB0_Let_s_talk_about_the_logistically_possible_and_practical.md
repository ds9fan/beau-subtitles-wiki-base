# Bits

Beau says:

- Challenges the notion of something being logistically impossible and instead focuses on practicality.
- Addresses the question of whether confiscating guns in the US is logistically impossible.
- Points out the sheer volume of firearms in the United States compared to other countries.
- Calculates the number of gun owners who might resist confiscation, based on demographics.
- Estimates that dynamic entries to confiscate guns could take a hundred years at the current rate.
- Emphasizes the need for societal change and a shift in mindset towards glorifying violence.
- Suggests that a legislative fix alone may not be enough to address the issue of gun violence.
- Advocates for focusing on nonviolent means and changing societal attitudes towards violence.

# Quotes

- "Everything's impossible until it's not."
- "To change society, you don't always have to change the law. You have to change thought."
- "We have to stop glorifying violence at every turn."

# Oneliner

Beau challenges the practicality of confiscating guns in the US, advocating for a societal shift away from glorifying violence.

# Audience

Policy makers, activists, citizens

# On-the-ground actions from transcript

- Advocate for nonviolent means of addressing societal issues (implied)
- Work towards changing societal attitudes towards violence through education and awareness campaigns (implied)

# Whats missing in summary

In-depth analysis of the challenges and implications of attempting to confiscate guns in the US.

# Tags

#GunControl #ViolencePrevention #SocietalChange #Practicality #PolicyChange