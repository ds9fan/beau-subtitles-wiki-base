# Bits

Beau says:

- Explains the problematic phrase "we write the report," suggesting that it implies a sense of authority to put anything in the report and have it believed due to public trust in officers.
- Reads a report from May 25, 2020, about an incident in Minneapolis involving a man who died after a police interaction.
- Points out discrepancies between the written report and what was captured on body cameras, implying that the report was crafted to shape public opinion.
- Emphasizes the importance of contrasting official reports with what one sees with their own eyes.
- Notes the widespread use of the phrase "we write the report" within law enforcement culture, even in the age of ubiquitous camera phones.
- Urges people to be critical of official reports and press releases from law enforcement, as they may not always provide the full or accurate picture.
- Encourages viewers to maintain a skeptical eye towards official narratives and to prioritize personal observation and critical thinking.
- Raises awareness about the manipulation of information by authorities to avoid accountability and shape public perception.
- Calls for vigilance in analyzing and questioning the information provided by law enforcement in incidents.
- Advocates for holding law enforcement accountable for the accuracy and transparency of their reports and actions.

# Quotes

- "Contrast what they put here and what you saw with your own eyes."
- "That phrase, we write the report, it has been around a long time."
- "Make sure there is a report that can't be edited or fashioned to be less than accurate."
- "They still attempt to shape public opinion by putting out less than accurate information."
- "This press release was an attempt by the department to make sure that what just happened, the accountability that just occurred, to make sure it didn't and that it never would."

# Oneliner

Be critical of official reports; contrast with personal observation to combat narrative shaping by authorities.

# Audience

Critical thinkers, community members

# On-the-ground actions from transcript

- Compare official reports with personal observations (implied)
- Question law enforcement narratives (implied)
- Advocate for transparency and accountability in police reports (implied)

# Whats missing in summary

Importance of questioning authority and seeking transparency in law enforcement narratives.

# Tags

#PoliceReports #NarrativeShaping #Transparency #Accountability #CommunityPolicing