# Bits

Beau says:

- A federal judge ordered LA to find housing for all homeless individuals on Skid Row after the mayor promised a billion dollars to address the issue.
- In the city, there are 41,000 homeless individuals, and if you include the county, the number rises to 66,000.
- With a billion dollars, it could cost roughly $15,000 per person, leaving $330 million for program administration.
- Beau suggests using the money to actually solve the problem by providing housing rather than just achieving measurable results through various programs.
- Building a large facility could be more cost-effective than individual units when dealing with such large numbers.
- Beau agrees with the judge that the reality of increasing homelessness in LA is shameful, with more homeless individuals dying on the streets each year.
- He advocates for setting higher goals and directly solving the issue rather than simply mitigating it.
- Providing housing seems like the most direct route to achieving measurable results with the available billion dollars.

# Quotes

- "Maybe it's time to set our sights a little bit higher rather than mitigating. We should actually just solve the problem."
- "All of the rhetoric, promises, plans, and budgeting cannot obscure the shameful reality of the crisis."
- "When you have numbers and you have a billion dollars with which to work, you could probably actually solve the problem rather than just achieve measurable results."

# Oneliner

A federal judge orders LA to find housing for all homeless individuals on Skid Row, prompting Beau to suggest directly solving the issue with a billion dollars rather than just achieving measurable results through various programs.

# Audience

Community members, policymakers

# On-the-ground actions from transcript

- Provide housing for homeless individuals in your community (implied)
- Advocate for cost-effective solutions like building large facilities (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the cost implications of providing housing for homeless individuals and challenges policymakers to aim higher in addressing the homelessness crisis in LA.

# Tags

#Homelessness #LosAngeles #HousingCrisis #CommunityPolicymaking #CostEffectiveSolutions