# Bits

Beau says:

- Chicago incident involving the use of a strobe light is under-discussed.
- Strobe lights are disorienting and affect vision in low-light situations.
- Proper use of strobe lights differs from constant lights; they should be shined in the eyes.
- Using a strobe light alone can create misperceptions of movements due to the brain filling gaps.
- Lack of training on strobe light usage might have influenced the Chicago incident.
- Second officer shining a constant light on the suspect is a recommended practice with strobe lights.
- Misusing tools like strobe lights in law enforcement can lead to undesired outcomes.
- Questions arise about officer training in using strobe lights effectively.
- Importance of understanding the specific purposes and correct usage of law enforcement gadgets.
- Civilian oversight should inquire about officers with strobes on their weapons and their training.

# Quotes

- "Using them outside of those purposes because it's cool doesn't lend to getting the best results."
- "Understand you don't use it like a normal light."
- "I want to know how many officers have those strobes on their weapons and how many were trained to use them."

# Oneliner

The Chicago incident raises concerns about the under-discussed misuse of strobe lights in law enforcement and the importance of proper training and usage protocols.

# Audience

Law enforcement oversight

# On-the-ground actions from transcript

- Inquire about officers with strobe lights on their weapons and their training (implied)

# Whats missing in summary

Importance of addressing proper training and usage protocols to prevent potential misuse and harm in law enforcement situations. 

# Tags

#Chicago #StrobeLights #LawEnforcement #Training #Oversight