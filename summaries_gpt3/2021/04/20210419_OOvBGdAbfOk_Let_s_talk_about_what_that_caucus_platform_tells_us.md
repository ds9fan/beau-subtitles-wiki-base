# Bits

Beau says:
- Talks about a leaked document and the importance of not dismissing it as a joke.
- Expresses concern over an outside group influencing Congress with concerning documents.
- Questions the silence from the Republican Party regarding the platform filled with racist content.
- Points out the lack of response from colleagues in the party and the potential political motivations behind it.
- Expresses fear that Biden's win may not have been decisive enough to reject Trumpism.
- Warns about the dangers of underestimating the influence of groups promoting authoritarianism.
- Stresses the need to stay politically active and vigilant against such ideologies.

# Quotes

- "You can be outraged at the content. You should be."
- "You have to stay politically active because those who want that brand of authoritarianism, they are still out there."
- "I want to know who this group is."
- "It could be lifted from any of the most horrible groups throughout history because it's the same strategy."
- "That's when really horrible things happen."

# Oneliner

Beau stresses the danger of dismissing concerning documents, warns against underestimating authoritarian influences, and urges staying politically active against such ideologies.

# Audience

Politically active citizens

# On-the-ground actions from transcript

- Stay politically active and vigilant against authoritarian influences (implied).

# Whats missing in summary

Full understanding of the implications of dismissing concerning documents and the necessity of political vigilance against authoritarian ideologies.

# Tags

#Warning #Authoritarianism #PoliticalActivism #RepublicanParty #Trumpism