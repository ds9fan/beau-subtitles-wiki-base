# Bits

Beau says:

- USStratCom, the government account in charge of US nukes, tweeted a vague statement that was misconstrued by right-wing personalities.
- Instead of providing context, the right-wing capitalized on fear, spreading messages of imminent nuclear war.
- The tweet in question was actually a preview of the posture statement, a routine occurrence justifying the organization's existence.
- The annual testimony occurs to address the possibility of other countries using nukes in extreme scenarios.
- Some ex-military personnel also contributed to fearmongering, despite likely understanding the context behind the tweet.
- Misinformation on social media led thousands of Americans to believe in the false narrative of impending nuclear war.
- Beau stresses the importance of addressing and alleviating fears with facts to have productive dialogues with those influenced by fear-based narratives.
- He warns against the spread of baseless fear, particularly on social media platforms.
- Beau urges people to understand and counteract the fear-driven motivations of certain groups, particularly on the right wing.
- The irresponsible actions of some individuals on Twitter caused unnecessary panic and anxiety among the public.

# Quotes

- "The thing is, some of the people who retweeted it with these statements, they're ex-Secret Service, ex-Air Force."
- "You have to alleviate that fear or you're not going to get anywhere."
- "There are thousands of Americans right now who feel that we might be on the brink of nuclear war."

# Oneliner

Beau clarifies a routine government tweet about US nukes misconstrued as a harbinger of nuclear war, stressing the need to counter fear with facts.

# Audience

Social media users

# On-the-ground actions from transcript

- Alleviate fears with facts (implied)
- Address misinformation with accurate information (implied)

# Whats missing in summary

The full transcript provides a detailed account of how misinformation and fear-based narratives can spread rapidly online, impacting public perception and creating unnecessary panic. Watching the full transcript can offer a comprehensive understanding of the importance of countering fear with facts in online discourse.

# Tags

#Misinformation #Fearmongering #SocialMedia #USNukes #CounterNarratives