# Bits

Beau says:

- Addressing questions surrounding the Chauvin trial and the testimony about use of force.
- Explaining the concept of positional asphyxiation and its relevance in the trial.
- Pointing out the discrepancy between policy and practice regarding prone restraints in law enforcement.
- Describing how policies aim to prevent specific outcomes but are not always followed on the ground.
- Noting the ingrained culture within law enforcement that resists change in operational practices.
- Expressing skepticism towards defense experts who may deny the existence of positional asphyxiation.
- Emphasizing the importance of specific questioning by the prosecutor in challenging defense experts' claims.
- Acknowledging the ongoing nature of the trial and the hope for accountability.

# Quotes

- "The policies are set up that way because when something does happen, well, it's a big deal."
- "There is a running conversation between people like cops and cops themselves."
- "It's ingrained, and I don't see that changing."
- "But at the same time, most officers have used a prone restraint over and over and over again without issue."
- "Hopefully, the bad apple gets removed."

# Oneliner

Beau addresses discrepancies in law enforcement practices, policy, and culture amid ongoing Chauvin trial testimony, urging for accountability and challenging defense expert claims.

# Audience

Law enforcement reform advocates

# On-the-ground actions from transcript

- Challenge ingrained law enforcement practices through community advocacy (implied)
- Support accountability measures within police departments (implied)
- Advocate for specific questioning of defense experts in trials (implied)

# Whats missing in summary

Insights on the impact of continued advocacy for police accountability and reform efforts.

# Tags

#ChauvinTrial #PolicePractices #UseOfForce #Accountability #LawEnforcement #PolicyVsPractice