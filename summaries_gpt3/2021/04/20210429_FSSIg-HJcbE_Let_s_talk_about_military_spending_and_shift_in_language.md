# Bits

Beau says:

- Military commanders are now referring to China and Russia as peer nations, but Beau continues to refer to them as near peers.
- Military spending and a subtle shift in the use of the term "peer nations" are discussed.
- The United States spends approximately $780 billion a year on defense, more than several other countries combined.
- The idea behind US military expenditures was to be able to fight a war on two fronts independently.
- The US doctrine leads to spending more on defense than its two largest competitors combined.
- Despite excessive military spending, Beau believes the US can only fight China and Russia to a stalemate in a two-front war scenario.
- Referring to China and Russia as peers is seen as priming the populace for increased defense spending.
- Beau views the shift in terminology as propaganda to justify higher military expenditures.
- China is noted for its manpower, but Beau argues that without significant assets, their military capabilities are limited.
- Beau maintains that the term "peer nations" is inaccurate and will continue to use "near peer."

# Quotes

- "I view that as propaganda."
- "I don't think that it's accurate to say that they are peer nations militarily."
- "They're not the same."
- "It's getting ready for Cold War 2.0."
- "Y'all have a good day."

# Oneliner

Beau explains the subtle shift in military terminology towards China and Russia as peer nations, criticizing it as propaganda to justify increased defense spending, while maintaining they are actually near peers.

# Audience

Policy analysts, military personnel

# On-the-ground actions from transcript

- Advocate for transparent military spending to ensure resources are allocated effectively (implied).
- Stay informed about military policies and decisions to contribute to informed public discourse (implied).

# Whats missing in summary

Beau's detailed analysis of the implications of the shift in military terminology and the necessity of accurate representation in defense strategies. 

# Tags

#MilitarySpending #USDefense #ForeignPolicy #Propaganda #China #Russia