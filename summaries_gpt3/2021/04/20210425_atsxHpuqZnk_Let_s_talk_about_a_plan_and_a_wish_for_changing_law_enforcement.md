# Bits

Beau says:

- Beau introduces the topic of discussing plans and wishes for significant change in how things are done.
- He points out that simply wanting to be at a different point but not having a plan is merely a wish, not a plan.
- Beau proposes a rough sketch of a plan to address excessive force in law enforcement by preventing officers from immediately resorting to their firearms.
- He suggests using holsters with sensors that automatically call for backup and EMS when a firearm is drawn.
- Beau advocates for national accreditation for law enforcement agencies to ensure adherence to use-of-force policies and proper auditing of incidents.
- He argues against federalizing law enforcement due to jurisdictional differences but supports accreditation for agencies to receive federal funding.
- The plan includes holsters funded by the federal government to discourage tampering and ensure accountability.
- Beau explains that the ultimate goal is to reduce law enforcement's reliance on lethal force by keeping firearms holstered.
- He acknowledges the debate around abolition of the current law enforcement system and suggests harm reduction measures as an interim solution.
- Beau underscores the importance of gradually building a new system while undermining the current reliance on violence to facilitate its eventual replacement.

# Quotes

- "Just wanting to be at point B, that's not a plan, that's a wish."
- "Abolition, putting a new system in place, that's getting to the hospital with that injury. That's where you want to end up."
- "Nobody wants to fill out the report and nobody wants to be responsible for the bill of the EMS."

# Oneliner

Beau presents a plan to reduce law enforcement's reliance on lethal force through holsters with sensors, national accreditation, and gradual system transformation, advocating for harm reduction on the path to potential abolition.

# Audience

Law reform advocates

# On-the-ground actions from transcript

- Advocate for national accreditation of law enforcement agencies to ensure adherence to use-of-force policies (suggested)
- Support funding for holsters with sensors by the federal government to enhance officer accountability (suggested)
- Encourage agencies to prioritize de-escalation techniques through grant money incentives (implied)

# Whats missing in summary

The full transcript provides a detailed plan for reducing excessive force in law enforcement, addressing systemic issues, and advocating for harm reduction measures as steps towards potential abolition.

# Tags

#LawEnforcement #PoliceReform #Accountability #HarmReduction #SystemTransformation