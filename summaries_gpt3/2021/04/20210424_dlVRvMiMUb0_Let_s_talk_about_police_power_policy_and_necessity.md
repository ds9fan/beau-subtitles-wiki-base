# Bits

Beau says:

- Explains the overwhelming number of fatal shootings by law enforcement during the Chauvin trial.
- Addresses why he focuses on training and policy when discussing these situations.
- Challenges the notion that focusing on training and policy gives cops an excuse.
- Emphasizes that incidents often involve officers stepping outside of training and policy.
- Points out the importance of policy in driving accountability and reform.
- Stresses the significance of policy in changing officer behavior and ensuring accountability.
- Talks about the specific policies like positional asphyxiation, time, distance, and cover that are critical for accountability.
- Suggests that focusing on outdated training methods can lead to accountability in cases of officer misconduct.
- Differentiates between addressing systemic issues and pursuing accountability through policy.
- Encourages understanding and engaging with policy for impactful reform and accountability.

# Quotes

- "Knowledge is power."
- "If you can change the policy, well, the next time they do it and it's outside of policy, then they can be charged."
- "Policy is the most powerful tool you have."

# Oneliner

Knowledge is power in holding law enforcement accountable through understanding and engaging with policy for impactful reform and accountability.

# Audience

Reform Advocates

# On-the-ground actions from transcript

- Study and understand existing policies on law enforcement conduct (implied).
- Advocate for changes in policies to drive accountability and reform (implied).

# Whats missing in summary

Insights on specific examples and case studies could provide a deeper understanding of the impact of policy on accountability and reform in law enforcement.

# Tags

#PoliceReform #Accountability #Training #Policy #SystemicIssues #CommunityPolicing