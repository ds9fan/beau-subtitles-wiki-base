# Bits

Beau says:

- Singer, known for sewing machines, made 1911s for the US government in 1939, contributing to the war effort.
- The precision equipment they made, like the Sperry Sight for airplanes, helped defeat authoritarianism.
- Companies getting involved in politics isn't a new concept; Singer's involvement predates WWII.
- Companies influencing politics through campaign contributions challenges the idea of them staying out of politics.
- Confusing basic morality with politics is a larger issue in American society.
- Interfering with fundamental rights is a moral, not political issue.
- Organizations with exclusionary histories might be sensitive to attempts to deny basic rights.
- Many Americans fail to recognize the non-negotiable nature of American ideals when it comes to political interference.

# Quotes

- "Maybe they saw what was coming and they wanted to be on the right side of history."
- "Companies getting involved in politics isn't the biggest issue; confusing morality with politics is."
- "Interfering with fundamental rights is a moral issue."
- "Large segments of the American population are confusing basic morality with politics."
- "The promises made in the Declaration of Independence and the Constitution are not up for negotiation."

# Oneliner

Singer's historical involvement in WWII shows the importance of companies being on the right side of history, while confusion between basic morality and politics poses a more significant issue in American society.

# Audience

American citizens

# On-the-ground actions from transcript

- Advocate for upholding fundamental rights and values in society (implied).
- Educate others on the distinction between moral issues and political decisions (implied).

# What's missing in summary

The full transcript provides a deeper exploration of the historical involvement of companies in politics, urging individuals to critically analyze the intersection of morality and politics in societal issues.

# Tags

#Companies #Politics #Morality #FundamentalRights #AmericanHistory