# Bits

Beau says:

- Recalls incidents dating back to 1992 and stresses the long-standing nature of issues in law enforcement training and practices.
- Considers the root causes of problems in law enforcement tactics and sees them as a combination of training issues and deeper cultural problems within the institution.
- Compares the quick adoption of new policies in response to public health crises with the lack of progress in implementing new protocols in law enforcement despite knowing best practices.
- Expresses frustration at the lack of change in law enforcement tactics, with videos detailing necessary techniques dating back 10-25 years.
- Criticizes law enforcement leadership for being unwilling to change and prioritizing mimicking military high-speed teams over adopting effective practices to mitigate risk.
- Points out the military's willingness to adapt and change tactics compared to the resistance to change within law enforcement.
- Gives an example of swift action taken by military leadership in response to an incident involving a white NCO and a black civilian, contrasting it with the general unwillingness of law enforcement to acknowledge and correct mistakes.
- Emphasizes the need for a cultural shift in law enforcement to prioritize risk mitigation and the adoption of effective, proven tactics to prevent unjust deaths.

# Quotes

- "There hasn't been any new policies. There haven't been any new protocols to keep people safe."
- "They have been doing it wrong."
- "We need change in the culture of law enforcement, a lot of it."

# Oneliner

Beau stresses the urgent need for a cultural shift in law enforcement to prioritize risk mitigation and effective training practices to prevent unjust deaths.

# Audience

Law Enforcement Reform Advocates

# On-the-ground actions from transcript

- Advocate for and support the implementation of new policies and protocols in law enforcement to prioritize safety and mitigate risks (exemplified).
- Push for cultural changes within law enforcement institutions to prioritize training and willingness to change (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the longstanding issues in law enforcement training and tactics, calling for urgent changes to prevent further unjust deaths.

# Tags

#LawEnforcement #Training #CulturalShift #RiskMitigation #Justice