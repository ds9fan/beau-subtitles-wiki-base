# Bits

Beau says:

- Wishes happy birthday to Cleo before diving into discussing the potential fourth surge of COVID-19.
- Clarifies his stance on bodily autonomy, asserting support for personal decisions regarding tattoos, piercing, family planning, and vaccinations.
- Expresses being vaccinated against various diseases, including receiving the COVID-19 vaccine.
- Supports vaccines but opposes the idea of force, advocating for convincing others rather than mandating vaccinations.
- Notes the beginning of the fourth surge, likely due to a new variant, B.1.1.7, affecting younger age groups.
- Mentions laxity in following precautions, attributing it to fatigue and burnout after a year of vigilance.
- Emphasizes the importance of maintaining precautionary measures until vaccination rates are significantly higher.
- Alerts about the potential resistance of new variants to vaccines and stresses the need for continued vigilance.
- Acknowledges the coverage of the current vaccines against the B.1.1.7 variant.
- Urges people to uphold preventive measures like hand washing, mask-wearing, and staying home, while also advocating for vaccination.

# Quotes

- "You own your body."
- "Vaccines are good. I do not object to them. I object to force, not vaccines."
- "We have to continue to do this until we're there, until the vaccination rates are high enough."
- "Every variant is a variant that might be resistant to the inoculation, to the vaccine."
- "Wash your hands. Don't touch your face. Stay at home. If you have to go out, wear a mask."

# Oneliner

Beau clarifies support for bodily autonomy and advocates for vaccines, warning about the fourth COVID-19 surge and stressing the importance of ongoing precautions until vaccination rates are high.

# Audience

General public

# On-the-ground actions from transcript

- Wash your hands, avoid touching your face, stay home if possible, wear a mask when going out, and get vaccinated (exemplified).
  
# Whats missing in summary

The detailed analysis of COVID-19 trends and vaccination rates can be better understood by watching the full video.

# Tags

#COVID-19 #Vaccination #Precautions #Health #BodilyAutonomy #CommunitySafety