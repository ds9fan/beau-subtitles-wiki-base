# Bits

Beau says:

- Addresses Wayne LaPierre's hunting incident, revealing footage of the unmitigated failure and embarrassment.
- Urges viewers to watch the footage for two reasons.
- Describes the hunting expeditions where typically men pay tens of thousands of dollars to go on a safari.
- Points out that in these hunts, the guides do all the work and even line up the shots for the hunters.
- Emphasizes how these hunting trips are not the challenging, rugged experiences they are often portrayed as.
- Criticizes the hunting culture where animals are killed for trophies and the act is passed off as a rite of manhood.
- Mentions that the footage was likely obtained by The Trace.

# Quotes

- "It's worth seeing what that actually entails."
- "It's point and shoot. They don't do anything."
- "They really just drop their money so they can pretend that they are something they're not."

# Oneliner

Beau addresses Wayne LaPierre's hunting incident, exposing the truth behind these trophy hunting expeditions and challenging the notion of rugged individuality.

# Audience

Animal rights activists

# On-the-ground actions from transcript

- Watch the footage to understand the reality of trophy hunting (suggested)
- Share the footage to raise awareness about the true nature of these hunting expeditions (suggested)

# Whats missing in summary

The emotional impact of witnessing the reality behind trophy hunting and its implications on animal welfare.

# Tags

#TrophyHunting #AnimalRights #WayneLaPierre #RuggedIndividuality #HuntingCulture