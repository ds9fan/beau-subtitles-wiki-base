# Bits

Beau says:

- Addressing the contradiction between wanting criminal justice reform and being satisfied with Derek Chauvin's imprisonment.
- Acknowledging the gray areas and compromises inherent in effecting real change in a complex system.
- Exploring the interconnected systemic issues related to elephant poaching and the challenges in finding solutions.
- Emphasizing the difficulty of maintaining ideological consistency when actively working towards change.
- Recognizing the pragmatic approach of targeting certain aspects of a problem, such as going after poachers to disrupt the chain of violence funding.
- Pointing out the unrealistic nature of expecting immediate solutions to deeply rooted problems like demand for ivory.
- Arguing that individuals advocating for prison abolition may not prioritize defending someone like Chauvin.
- Suggesting the importance of guideposts for those navigating the gray areas of effecting change within existing systems.

# Quotes

- "Because a lot of our problems, a lot of the systemic issues, they're systemic. They're interlinked."
- "It becomes very hard to stay consistent because you're trying to get to that ideal, but that's not the world you exist in."
- "So where do you go? We're back to going after the poachers because that's the easiest link in the chain to break."
- "Most people who want prison abolition, they do so because they don't like the violence of the state."
- "A little lighthouse over there saying, no, this is too far."

# Oneliner

Navigating the gray areas of activism and reform, Beau delves into contradictions in justice reform and the complex web of issues surrounding elephant poaching.

# Audience

Activists and reformers

# On-the-ground actions from transcript

- Support elephant preserves by donating or volunteering (implied)
- Advocate for sustainable jobs programs in regions affected by poaching (implied)
- Raise awareness about the systemic issues contributing to poaching and violence (implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on the challenges of effecting change amidst systemic issues and contradictions in activism. Viewing the entire discourse offers a deeper understanding of navigating complex societal problems.

# Tags

#JusticeReform #Activism #SystemicIssues #PragmaticApproach #Compromises