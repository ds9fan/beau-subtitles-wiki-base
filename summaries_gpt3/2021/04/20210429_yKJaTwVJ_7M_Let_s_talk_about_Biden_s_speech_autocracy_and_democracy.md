# Bits

Beau says:

- Biden's recent speech hints at framing Cold War 2.0 as democracy versus autocracy, focusing on countering China and Russia.
- The domestic political side must be considered when discussing foreign policy, as it can be politically advantageous for Biden.
- China and Russia are unlikely to be bothered by being branded as autocracies, as many of their citizens actually favor strongman leadership.
- Lesser powers in the world may benefit from this framing as they won't have to match their economic system to a specific pole of power, allowing them to pursue regional interests.
- Most countries view China, Russia, and the United States as oligarchies, with a small group of people truly in charge.
- The democracy versus autocracy framing serves as a motivational tool for the U.S. government to rally its populace behind its foreign policy.
- This framing is seen as a good choice as it is less likely to raise global tensions compared to other options.
- The speech was testing the waters for this framing, and if well-received, it will likely shape future foreign policy approaches.

# Quotes

- "Democracy versus autocracy, autocratic governments, Russia and China."
- "If the overwhelming majority of the United States begins to view autocracy as it did communism, Trump's circle is done."
- "During the Cold War, they had to match their economic system to the pole of power that they wanted to be aligned with."
- "Old boss, same as the new boss."
- "It won't be a red scare. It will be an autocratic one."

# Oneliner

Biden's speech hints at framing Cold War 2.0 as democracy versus autocracy, a politically advantageous move domestically, with potential implications for lesser powers and global tensions.

# Audience

Global citizens

# On-the-ground actions from transcript

- Rally behind foreign policy framing (suggested)
- Understand the implications for global politics (suggested)

# Whats missing in summary

Analysis of the potential long-term effects of framing foreign policy as democracy versus autocracy.

# Tags

#ForeignPolicy #ColdWar #Democracy #Autocracy #GlobalPolitics