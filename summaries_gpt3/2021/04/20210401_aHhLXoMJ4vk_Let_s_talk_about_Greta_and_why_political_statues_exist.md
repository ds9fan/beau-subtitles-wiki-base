# Bits

Beau says:

- Twitter joke about a Greta statue led to a deeper reflection on preserving historical figures.
- Many people support keeping old statues but may not fully understand their purpose.
- Political statues are not solely historical artifacts but meant to inspire and embody ideals.
- Statues of famous southerners were mainly erected decades after the Civil War, not as historical markers.
- The purpose of political statues is to create heroes for people to emulate and be inspired by.
- Most southern statues were erected during times of romanticizing the war or civil rights movements.
- Political statues represent societal norms at the time of creation, not the person's era.
- When societal norms change, statues that no longer inspire or represent heroism should be reconsidered.
- Statues of those deemed villains by history should be removed if they fail to inspire heroism.
- Suggestions for statues to inspire include Greta Thunberg and Kristen Davis over Jefferson Davis.

# Quotes

- "Political statues are about creating heroes to inspire people."
- "If history has deemed a person not a hero but in fact a villain, maybe it's time for those statues to go away."
- "Most people are not inspired by old statues, most people are not inspired by old slave owners."

# Oneliner

Beau explains the true purpose of political statues: creating heroes for inspiration based on societal norms, suggesting reevaluation of statues that no longer embody heroism.

# Audience

History enthusiasts

# On-the-ground actions from transcript

- Re-evaluate statues in your community that may no longer inspire heroism (implied)

# Whats missing in summary

The full transcript provides a thought-provoking analysis of the true purpose of political statues and the importance of inspiring heroism through public art.

# Tags

#HistoricalFigures #PoliticalStatues #Inspiration #SocietalNorms #Heroism