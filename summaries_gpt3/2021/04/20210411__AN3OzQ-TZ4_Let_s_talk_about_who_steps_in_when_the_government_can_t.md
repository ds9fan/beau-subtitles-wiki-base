# Bits

Beau says:

- An intelligence estimate predicts a widening gap between people's expectations of government and what governments can provide, leading to non-state actors filling the role.
- Non-state actors are individuals or groups performing government functions without being part of the government, not necessarily armed or scary.
- Large charities and individuals who influence change on the ground are examples of non-state actors.
- People who helped during public health crises by providing medical supplies are considered non-state actors.
- The focus is on ensuring non-state actors are helping rather than hurting by organizing and being prepared for turbulent times.
- Building community networks separate from government reliance can prevent bad non-state actors from emerging.
- Adaptability is key for societies to prosper in the next 20 years, and decentralized power structures can fill gaps left by governments.
- The United States government is not very adaptable, so the responsibility falls on individuals to create decentralized structures.

# Quotes

- "Non-state actor is simply a person, organization, or group that is performing a function that is typically seen as the purview of government."
- "You go ahead and fill that vacuum yourself now."
- "We just have to start organizing now."

# Oneliner

An intelligence estimate predicts a gap between people's expectations and what governments can provide, urging individuals to become non-state actors and create decentralized structures for a prosperous future.

# Audience

Community members

# On-the-ground actions from transcript

- Build community networks to prepare for turbulent times (exemplified)
- Provide medical supplies and support to local areas (exemplified)
- Organize decentralized power structures separate from government reliance (exemplified)

# Whats missing in summary

The full transcript delves deeper into the concept of non-state actors and the importance of adaptability in societies for the next two decades.

# Tags

#NonStateActors #CommunityBuilding #DecentralizedStructures #Adaptability #SocietalProsperity