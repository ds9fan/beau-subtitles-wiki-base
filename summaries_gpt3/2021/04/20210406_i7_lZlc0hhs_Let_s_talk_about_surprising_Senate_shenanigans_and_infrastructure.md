# Bits

Beau says:

- The parliamentarian's surprising determination allows Democrats to revise a previous resolution.
- Biden's infrastructure bill is likely to pass despite Democratic infighting.
- Democrats may amend the 2021 resolution to include uncontroversial parts of the bill.
- Contentious aspects could be pushed into a resolution for fiscal year 2022.
- Funding in the bill includes removing lead pipes, public housing, electrical grid updates, VA hospitals, schools, rural internet, Amtrak, electric vehicles, child care facilities, and future public health prevention.
- Passing the bill through regular Senate channels without budget reconciliation seems unlikely due to Republican opposition.
- The parliamentarian's decision makes it probable that the majority of the infrastructure package will pass.

# Quotes

- "Surprise. Determined that the Democrats could revise or amend a previous resolution."
- "Biden's big infrastructure bill, yeah, that's probably going through."
- "Money to get rid of all the lead pipes, money for public housing, money for schools, money for high speed internet in rural areas."
- "It's a big infrastructure package."
- "It's just a thought y'all have a good day."

# Oneliner

The parliamentarian's surprising decision allows Democrats to revise a previous resolution, making Biden's infrastructure bill likely to pass with significant funding for various vital sectors.

# Audience

Politically engaged citizens.

# On-the-ground actions from transcript

- Advocate for the passing of the infrastructure bill through contacting elected representatives (implied).
- Stay informed about the progress of the infrastructure bill and its implications for different sectors (implied).

# Whats missing in summary

Details on the potential impacts of the infrastructure bill on communities and the economy.

# Tags

#InfrastructureBill #Politics #Senate #BudgetReconciliation #PublicHealth