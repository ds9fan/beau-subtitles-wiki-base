# Bits
Beau says:

- Three separate instances of having the same sobering chat with newly politically engaged allies.
- Outlining the three options when grading performance: fails to meet standards, meets standards, exceeds standards.
- Acknowledging that a verdict or outcome that meets standards may not always evoke a feeling of victory.
- Emphasizing that meeting standards is just the starting point for a better society.
- Calling attention to the fact that meeting standards should not require significant political pressure.
- Describing exceeding standards as the ultimate goal for society - where actions are automatic and just.
- Reminding allies and activists that progress towards exceeding standards is a continuous journey.
- Encouraging newly engaged allies to stay committed despite potential disappointments and the ongoing work ahead.
- Noting the societal tendency to celebrate when things almost function as they should, despite still falling short.
- Urging allies to not be discouraged by the work still needed but to recommit to the cause and the people relying on them.

# Quotes
- "If you're grading performance, you really have three options."
- "Standards are the starting point."
- "You cannot stop."
- "That's a reason to recommit."
- "We aren't even getting the bare minimum."

# Oneliner
Beau talks about the importance of meeting and exceeding standards in society, urging allies to stay committed despite the ongoing work ahead.

# Audience
Newly engaged allies

# On-the-ground actions from transcript
- Keep educating yourself and staying politically engaged to work towards exceeding standards (implied).
- Continue putting in the work to push for societal change and progress (implied).

# Whats missing in summary
The emotional impact and depth of the speaker's words can be better understood by watching the full transcript.

# Tags
#Standards #Allyship #SocietalChange #Commitment #Activism