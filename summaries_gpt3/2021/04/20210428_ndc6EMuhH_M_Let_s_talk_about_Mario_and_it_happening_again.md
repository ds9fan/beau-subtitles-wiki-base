# Bits

Beau says:

- Addressing a recent incident involving Mario Gonzalez in California where he was handcuffed, face down, and held down by officers.
- Comparing the incident to George Floyd's case, noting that this incident happened faster.
- Stressing the importance of considering positional asphyxiation, especially in cases involving larger individuals like Mario.
- Emphasizing that weight plays a significant role in positional asphyxiation, not just the weight of the officers but also the weight of the subject.
- Mentioning the significance of rolling a person into the recovery position to prevent positional asphyxiation.
- Urging law enforcement officers to prioritize following protocols, such as moving individuals into the recovery position when suggested by a colleague.
- Pointing out that officers' grips and commands to stop resisting do not matter in cases of positional asphyxiation, as it is involuntary.
- Warning that waiting for a person to stop moving before releasing pressure can be fatal in cases of positional asphyxiation.
- Stressing the need for these key points to be part of the ongoing conversation surrounding incidents like Mario Gonzalez's.
- Refraining from speculating on charges until all information about the incident is available.

# Quotes

- "Your grip doesn't matter. How many times you press down on them harder, doesn't matter. They will not stop. It's involuntary. You're literally killing them."
- "If you wait for them to stop moving to let up on them, 100% of the time you will kill them."
- "People who are succumbing to this, they will not stop squirming, simply because the officer says stop resisting. It's involuntary."
- "Those three things need to be talked about when this conversation occurs."
- "This is the time to get that information out."

# Oneliner

Beau stresses the importance of understanding positional asphyxiation and following protocols to prevent fatal outcomes in incidents like Mario Gonzalez's. 

# Audience

Law Enforcement Officers

# On-the-ground actions from transcript

- Move individuals into the recovery position when suggested by a colleague (implied)
- Prioritize understanding and preventing positional asphyxiation in law enforcement training and protocols (implied)
- Educate fellow officers about the risks of positional asphyxiation and the importance of following proper procedures (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of the importance of recognizing and addressing positional asphyxiation in law enforcement encounters. Viewing the entire video can offer additional insights and context on this critical issue. 

# Tags

#LawEnforcement #PositionalAsphyxiation #PreventativeProtocols #Training #MarioGonzalez #California