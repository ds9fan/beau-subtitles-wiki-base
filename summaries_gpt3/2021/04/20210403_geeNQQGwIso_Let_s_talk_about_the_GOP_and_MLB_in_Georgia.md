# Bits

Beau says:

- The Republican Party motivates its base by finding something to focus on and uniting them against it.
- Examples include boycotting Netflix, Gillette, Coke, Pepsi, baseball, football, Starbucks, and more.
- The party has turned supporters against masks, public health, refugees, trans people, and Spanish-speaking networks.
- They've influenced their base through appeals to bigotry numerous times over the years.
- The strategy involves turning supporters against groups like their own children.
- Overall, the Republican Party's motivation often relies on appealing to bigotry.

# Quotes

- "How many times has the Republican Party motivated its base by appealing to their bigotry?"
- "It's pretty much what it's always about."
- "When you get down to the root of it, that's what it is."

# Oneliner

The Republican Party motivates its base through bigotry, uniting them against various causes, turning supporters even against their own kids.

# Audience

Voters, Activists

# On-the-ground actions from transcript

- Boycott companies that support bigotry (implied)
- Support organizations promoting inclusion and diversity (implied)
- Challenge discriminatory practices in your community (implied)

# Whats missing in summary

Full context and depth of examples, and Beau's commentary on the Republican Party's tactics.

# Tags

#RepublicanParty #Bigotry #Inclusion #Boycott #Activism