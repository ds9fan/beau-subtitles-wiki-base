# Bits

Beau says:

- Addressing frustrations, misconceptions, and the concept of qualified immunity in law enforcement.
- Viewer frustration towards Beau for not discussing qualified immunity as a tool to hold criminal cops accountable.
- Qualified immunity does not directly relate to criminal prosecution but rather to civil liability.
- Explaining that ending qualified immunity won't lead to more criminal prosecutions but could make officers financially accountable.
- Emphasizing the importance of focusing on policy, training, and statutes in relation to criminal prosecutions, not just qualified immunity.
- Mentioning the broader scope of qualified immunity beyond law enforcement, like school administrators having it.
- Predicting changes in qualified immunity within the next few years due to ongoing political momentum.
- Urging attention towards policy, training, and governing statutes as the key areas needing reform for accountability.

# Quotes

- "Ending qualified immunity will not increase prosecutions."
- "Qualified immunity does not relate to criminal prosecution."
- "Policy, training, and governing statutes are what matters."
- "Qualified immunity as it exists today in the United States, in five years, it's not going to look like that."
- "That is a fight that hasn't even started yet."

# Oneliner

Beau explains frustrations around qualified immunity, clarifying its role in civil liability, not criminal prosecution, urging focus on policy and training for accountability.

# Audience

Advocates for police accountability

# On-the-ground actions from transcript

- Advocate for reforms in policy, training, and governing statutes to improve police accountability (implied).
- Stay informed and engaged in local and national efforts to address qualified immunity and other issues related to police accountability (implied).

# Whats missing in summary

The full transcript provides a detailed explanation of the limitations and implications of qualified immunity, stressing the need for broader reforms beyond its scope.

# Tags

#PoliceAccountability #QualifiedImmunity #PolicyReform #CivilLiability #Training #CommunityAction