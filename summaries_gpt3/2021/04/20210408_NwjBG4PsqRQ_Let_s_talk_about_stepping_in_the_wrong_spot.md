# Bits

Beau says:

- Introduces the topic of Princess Diana but shifts to discussing the Biden administration's consideration of continuing the use of landmines.
- Describes a hypothetical scenario of stepping on a landmine while exploring a peaceful area due to remnants left behind by the U.S.
- Questions the necessity of the U.S. continuing to use landmines, arguing that they serve no purpose and are antiquated.
- Points out that conventional conflicts, like the one depicted in M*A*S*H, are no longer common, making the use of landmines even more unnecessary.
- Argues that drones are a more effective and less harmful alternative to landmines in denying access to an area for the opposition.
- Advocates for the discontinuation of landmines, citing the potential dangers they pose even after conflicts have ended.
- Emphasizes that leaving behind tools like landmines for the opposition is illogical and unnecessary.
- Urges for the immediate removal of landmines without the need for further debate or consideration.
- Raises concerns about the risk posed to troops by leaving large quantities of landmines behind.
- Calls for action to remove and discontinue the use of landmines without delay.

# Quotes

- "We shouldn't. Period. Full stop. End of story."
- "There is no reason to keep these things. It needs to go away."
- "Seems like handing your opposition tools doesn't make a whole lot of sense."

# Oneliner

Beau argues for the immediate discontinuation of landmines, stressing their obsoleteness and potential harm even after conflicts have ended.

# Audience

Advocates and activists

# On-the-ground actions from transcript

- Contact your representatives to advocate for the immediate discontinuation of landmines (implied).
- Support organizations working towards a global ban on landmines (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the unnecessary nature of landmines in modern conflicts and the urgent need for their discontinuation.

# Tags

#Landmines #BidenAdministration #Conflict #Drones #Advocacy