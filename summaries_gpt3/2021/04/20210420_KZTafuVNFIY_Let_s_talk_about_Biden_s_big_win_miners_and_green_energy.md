# Bits

Beau says:

- President Biden secured a big win for green energy policies with the support of the United Mine Workers of America.
- The union leadership negotiated for funds for transitioning and training miners into green energy jobs.
- Miners are not ignorant; they understand their union's power through collective bargaining.
- The miners' union power relies on their numbers, which have been declining due to job losses in the coal mining sector since 2012.
- Waiting to negotiate for funds in the future may result in the loss of leverage for the miners.
- The media should avoid portraying miners as ignorant and instead recognize their understanding of collective bargaining.
- Insulting miners can have negative consequences for both them and green energy initiatives.
- Union members will access funds for retraining and transitioning, not the union itself.
- Academic perspectives often overlook the real-world understanding of working-class individuals.
- The importance of explaining the benefits to miners based on their current collective bargaining power.

# Quotes

- "Instead of casting them as ignorant hillbillies, perhaps talk to them as people who understand the power of collective bargaining because that's who they are."
- "Keep insulting them and it's going to backfire on everybody."

# Oneliner

President Biden's win for green energy policies through the United Mine Workers of America shows the power of collective bargaining and challenges media stereotypes of miners.

# Audience

Working-class communities

# On-the-ground actions from transcript

- Advocate for fair treatment and representation of working-class individuals in media (implied)
- Support union movements for transitioning workers into sustainable job sectors (implied)

# Whats missing in summary

The full transcript provides a detailed insight into the importance of recognizing the power and understanding of working-class communities in collective bargaining and transitioning to new job sectors.