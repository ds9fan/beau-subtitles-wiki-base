# Bits

Beau says:

- One year since the global mess began officially with 118 million cases and 2.6 million lives lost globally.
- The United States, with less than 5% of the world's population, has lost over 500,000 lives, almost 20% of the total.
- Despite having infrastructure, research, plans, and capabilities to mitigate, the US failed badly in handling the pandemic.
- Lack of leadership was a significant factor, with some downplaying the situation even with evidence against them.
- People in the public eye failed the country by ignoring the severity of the situation and pushing false narratives.
- Beau questions how the US, with all its resources, became the leader in COVID-19 deaths.
- The US failed to recognize danger due to comfort and complacency, leading to catastrophic consequences.
- Beau criticizes the focus on trivial issues like Dr. Seuss books instead of addressing the nation's failures.
- Despite the failures, Beau sees a glimmer of hope in the readiness for change and improvement post-pandemic.
- He calls for strong leadership, community building, and resilience to prevent such failures in the future.

# Quotes

- "The United States failed. Five percent of the world's population and a little less than 20% of the loss."
- "We're nearing the end of this, hopefully. And when we come out of it, we have to be ready to get to work."

# Oneliner

One year into the pandemic, Beau questions the US's failure in handling COVID-19 and calls for leadership and community resilience to prevent future disasters.

# Audience

General public, policymakers

# On-the-ground actions from transcript

- Build community resilience to tackle future crises (implied)
- Advocate for strong leadership in handling crises (implied)
- Focus on addressing critical issues rather than distractions (implied)

# Whats missing in summary

The emotional impact and personal reflection from watching Beau's message.

# Tags

#COVID19 #PandemicResponse #Leadership #CommunityResilience #USFailure #Change