# Bits

Beau says:

- Explains the significance of the PRO Act passing the House and heading to the Senate, where it may face strong opposition but also has powerful allies.
- Describes the PRO Act as Protecting the Right to Organize, a union bill that penalizes big businesses for retaliating against workers trying to unionize.
- Mentions that labor organizing is not his expertise and consulted labor organizers who strongly support the PRO Act.
- Shares insights from Abby at Working Stiff USA, mentioning the importance of solidarity strikes, collective bargaining for gig workers, and debunking myths about the impact on independent contractors.
- Points out that the ABC test in the PRO Act only applies to workers seeking to organize under the NLRA.
- Notes the need for 60 votes in the Senate to overcome a filibuster, with unions having significant influence that could potentially lead to filibuster reform.
- Suggests that organized support for the PRO Act could counter the opposition's financial influence and sway over senators.

# Quotes

- "Protects the right to organize."
- "Every single labor organizer I know is very in support of this."
- "It's only going to impact you if you try to start a union."

# Oneliner

Beau explains the PRO Act, a union bill protecting the right to organize, facing Senate opposition but potentially leading to filibuster reform, with key support from labor organizers. 

# Audience

Advocates, Workers, Activists

# On-the-ground actions from transcript

- Organize in support of the PRO Act (suggested)

# Whats missing in summary

In-depth analysis of the potential implications and broader impact of filibuster reform in passing other legislation.

# Tags

#PROAct #UnionBill #FilibusterReform #LaborRights #SenateOpposition