# Bits

Beau says:

- Beau delves into the debate on DC statehood, prompted by a tweet from Senator Mike Rounds of South Dakota.
- The intention behind creating Washington DC as a neutral federal jurisdiction was to avoid state influence on the federal government.
- Over time, the concerns of the founding fathers have evolved and largely been addressed.
- Beau explains the linguistic shift from "the United States are" to "the United States is" as the federal government's authority grew.
- While the founders didn't intend for DC to be a state, there's nothing prohibiting it constitutionally.
- DC was initially allocated 100 square miles but now stands at 68.3, indicating flexibility in its size.
- The debate also touches on the political implications of DC statehood, with a focus on party affiliations in voter registration.
- Republicans' opposition to DC statehood is linked to concerns about the political leanings of its residents.
- Beau criticizes the Republican Party for potentially denying representation based on residents not voting in their favor.
- He accuses the Republican Party of straying from the principles of representative democracy and the republic.

# Quotes

- "It's not really that they have a concern with people being represented. They wouldn't have a problem with it. It's that they're low-quality voters."
- "I'm fairly certain that the founders might have an issue with a population the size of a little bit bigger than the populations of Virginia and Pennsylvania at the time, not having representation."
- "The only thing that the Republican Party currently has in common with the founders is the willingness to deny people the right to vote."

# Oneliner

Beau dissects the DC statehood debate, addressing historical intent, political motives, and concerns over representation in a democracy.

# Audience

Political activists, voters

# On-the-ground actions from transcript

- Advocate for fair representation and democracy in all aspects of governance (implied)
- Support initiatives that uphold equal voice and representation for all citizens (implied)

# Whats missing in summary

Beau's emotional investment and deep analysis can be best experienced by watching the full transcript.