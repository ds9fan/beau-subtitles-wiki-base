# Bits

Beau says:

- Dr. Birx implied the first hundred thousand COVID deaths were inevitable, but everything after that could have been mitigated.
- There was immediate criticism towards Dr. Birx for not speaking up earlier if she knew the severity of the situation.
- Dr. Fauci and Dr. Birx adopted a "good cop, bad cop" approach with President Trump, treating him like a toddler due to his perceived lack of understanding.
- Some believe the advisors' claims of guiding Trump gently are just an excuse after the fact.
- Trump's advisors, not just Birx and Fauci, struggled with the decision-making process, as seen in foreign policy blunders like targeting an Iranian general.
- The Pentagon included extreme options in briefings to make other choices seem more reasonable to President Trump.
- Trump's tendencies to make wrong decisions led his advisors to treat him like a child, resulting in questionable choices being presented.
- Despite criticism, Trump initially chose a less extreme option in a foreign policy decision but later escalated tensions.
- Trump's advisors' normalization of his poor decision-making contributed to a team mentality in American politics rather than prioritizing the country's well-being.
- Blind support for Trump based on party lines and talking points rather than results may have led to significant loss of life during his presidency.

# Quotes

- "Maybe we should be more focused on the country rather than our team."
- "They started treating him like a child."
- "That team mentality led to hundreds of thousands of excess loss."

# Oneliner

Dr. Birx and Dr. Fauci faced criticism for not speaking up earlier about preventable COVID deaths, revealing a troubling dynamic of treating President Trump like a child, ultimately leading to a team-focused mentality causing significant loss of life.

# Audience

American citizens

# On-the-ground actions from transcript

- Question political loyalties and prioritize the country over party affiliations (implied)
- Advocate for transparency and accountability from leaders in crisis situations (implied)
  
# Whats missing in summary

The full transcript expands on the implications of blind partisan loyalty and the need for a shift towards prioritizing the country's well-being over political affiliations.

# Tags

#COVID19 #PoliticalAdvisors #TrumpAdministration #PartisanPolitics #PublicHealth