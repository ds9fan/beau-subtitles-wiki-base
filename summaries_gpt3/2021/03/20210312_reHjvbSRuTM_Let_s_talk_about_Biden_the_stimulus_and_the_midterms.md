# Bits

Beau says:

- Shares his evolving opinion of Biden post-election.
- Expected a reboot of the Obama administration but notes Biden is doing more than that.
- Acknowledges progressive expectations of Biden but personally only expected minimal changes.
- Explains why Biden didn't criticize Republicans in his speech.
- Talks about the stimulus bill and Republican opposition to it.
- Details leftist provisions in the stimulus bill like child tax credit and healthcare subsidies.
- Mentions the impact of these provisions on working-class Americans.
- Remarks on Biden's approach to more progressive policies than anticipated.
- Speculates on Biden's strategy for pushing through progressive policies.
- Comments on the potential outcomes of Biden's approach.

# Quotes

- "I do have a higher opinion of Biden today than I did before the election."
- "Biden is running on this theme of unity or whatever. We all know it's not real."
- "He is working to try out ideas that are far more progressive than anything I ever expected from Biden."
- "It's surprising, surprising to me. I didn't expect it from him."
- "I do have a higher opinion of it."

# Oneliner

Beau explains his evolving opinion on Biden, noting unexpected progressiveness amidst strategic political maneuvering.

# Audience

Political observers

# On-the-ground actions from transcript

- Share information about the provisions in the stimulus bill with working-class Americans (implied).
- Stay informed about Biden's policies and their potential impact on various groups (implied).
- Pay attention to the details and long-term goals of political leaders' actions (implied).

# Whats missing in summary

Insights on Beau's perspective and analysis on Biden's administration can be best understood by watching the full transcript.

# Tags

#Biden #ProgressivePolicies #StimulusBill #PoliticalAnalysis #EvolutionaryOpinion