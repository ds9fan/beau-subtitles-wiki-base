# Bits

Beau says:

- Responding to a message critiquing his views on parents subjecting their children to trips to the US.
- Describes the terrible conditions and hardships faced during these trips, likening them to historical European immigration.
- Points out the historical correlation where groups enduring the worst trips contribute significantly to building the country.
- Argues against demonizing and marginalizing these asylum seekers, stating they are trying to save lives.
- Distinguishes between migrants and asylum seekers, advocating for supporting those willing to make sacrifices to build a better future.
- Challenges the mythology surrounding early American immigrants and encourages reading more on the topic.

# Quotes

- "Do I think these kinds of people will build this country? Well, yeah. It's kind of the only thing that ever has."
- "At the end of the day, they are trying to save their children's lives, trying to save their own lives."
- "Somebody who is willing to leave their home country, go to a place where they don't have connections, don't really speak the language, have very little chance of being able to get too far ahead, to me, that's not somebody you turn away."
- "They will build this country. They'll be a benefit, just like every group before."
- "The mythology that surrounds early American immigrants who came here from Europe, it's not real."

# Oneliner

Responding to criticism, Beau defends asylum seekers' sacrifices and contributions to building the country, debunking myths about early American immigrants.

# Audience

Advocates for Immigration Rights

# On-the-ground actions from transcript

- Support asylum seekers in your community (implied)
- Educate others on the realities and challenges faced by asylum seekers (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of the historical context and challenges faced by asylum seekers, shedding light on the importance of supporting these individuals in building a better future.

# Tags

#Immigration #AsylumSeekers #MythBusting #History #CommunitySupport #DebunkingNarratives