# Bits

Beau says:

- Major Democratic Party figures support altering or repealing the filibuster, with some advocating for a return to a talking filibuster.
- The filibuster allows the minority party, currently Republicans, to block legislation unless 60 votes are secured in the Senate.
- Democratic leaders are pushing for changes to the filibuster, aiming to encourage healthy debate.
- Biden plans to advocate for a return to a talking filibuster, where senators must speak to hold up legislation.
- Beau suggests requiring senators to stay on topic during filibusters to prevent irrelevant speeches.
- Republicans, led by Mitch McConnell, strongly oppose filibuster changes, warning of drastic measures if it's altered.
- McConnell threatens to pass unpopular proposals if filibuster rules change, but this move may not deter Democrats.
- Beau points out that threats to pass divisive legislation could backfire on the Republican Party, revealing their true agenda.
- He notes that many Americans are unaware of the Republican Party's platform, which now centers around loyalty to Trump.
- Beau predicts potential movement on the filibuster debate in the coming weeks unless Republicans cooperate on Biden's agenda.

# Quotes

- "If I was rewriting the rules I would make sure that they also have to stay on topic while they are debating."
- "That's what you want. That's what you want. That's what you want to put out there."
- "It might inform people about what the Republican party really is."

# Oneliner

Major Democratic figures advocate altering the filibuster, while Republicans resist, risking revealing unpopular agendas.

# Audience

Legislative aides, political activists

# On-the-ground actions from transcript

- Contact your senators to express support for or against filibuster reform (suggested)
- Stay informed on the ongoing debate over the filibuster and its potential impact on legislation (implied)

# Whats missing in summary

Deeper insights into the potential consequences of filibuster changes and the importance of public awareness in shaping political decisions.

# Tags

#Filibuster #PoliticalDebate #DemocraticParty #RepublicanParty #Legislation