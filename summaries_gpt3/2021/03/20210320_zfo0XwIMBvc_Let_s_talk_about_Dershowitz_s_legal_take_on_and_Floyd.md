# Bits

Beau says:

- Critiques Dershowitz's theory on suggesting previous actions of the cop as evidence in defense of Floyd's death.
- Questions the logic behind the defense, giving examples of murder scenarios.
- Recalls a prediction from a viewer about the defense strategy, referencing the "skull egg theory" used in personal injury cases.
- Disagrees with the defense strategy and its application to the case.
- Disputes the idea of changing the trial venue due to events related to the incident.
- Shares personal rural living experience to counter the argument for a venue change.
- Comments on the defense possibly teaching the jury too much about the concept of due process.
- Concludes with a thought on the defense strategy and wishes the audience a good day.

# Quotes

- "Normally if something is true you can take it out of the context it's being presented in and put it in something else very similar and it would still be true."
- "I'm pretty sure that's still murder."
- "I don't think this is a strong defense."
- "It seems to me like he wants to move on to a different country."
- "Certainly a bold legal strategy."

# Oneliner

Beau questions the logic behind Dershowitz's defense strategy, providing examples to challenge its validity, while also expressing skepticism towards changing the trial venue and cautioning against teaching the jury too much about due process.

# Audience

Legal observers, activists

# On-the-ground actions from transcript

- Analyze and question legal defense strategies (implied)
- Stay informed and engaged in legal proceedings (implied)

# Whats missing in summary

Beau's insight and perspective on legal defense strategies and trial venue changes. 

# Tags

#LegalDefense #TrialVenue #DueProcess #Critique #Activism