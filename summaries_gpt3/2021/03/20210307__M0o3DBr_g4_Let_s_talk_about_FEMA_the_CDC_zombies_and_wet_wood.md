# Bits

Beau says:

- Introducing his second channel, The Roads with Beau, which features long-format content on historical events, community networking, and on-location experiences.
- Due to travel restrictions, a project focused on community networking was put on hold but is now coming back as restrictions ease.
- Explains why FEMA and the CDC use zombies as a teaching tool in emergency preparedness to make information more accessible and engaging.
- Touches on the importance of planning for surviving zombies as a way to prepare for other disasters like snowstorms or hurricanes.
- Addresses a question about starting a fire with wet wood, explaining that even wet wood may have a dry core that can be utilized with more effort.
- Encourages viewers to think about emergency preparedness and skills required for various scenarios like starting a fire with wet wood.
- Emphasizes the need to be prepared for future emergencies and disasters by honing necessary survival skills.
- Suggests setting wet wood near a fire to dry out and mentions that wet wood tends to be harder to keep burning and produces more smoke.
- Commends those who ask questions and seek to be prepared for future emergencies by developing necessary skills.
- Encourages viewers to contemplate what they would do in a zombie apocalypse scenario as a mental exercise in preparedness.

# Quotes

- "If you plan to survive zombies, you will make it through a snowstorm or a hurricane or whatever, because zombies disrupt everything."
- "But in that situation, what else do you have to do?"
- "You realized that there was an issue and you are trying to be prepared for next time."
- "And rest assured, there will be a next time, just as sure as there's going to be another hurricane where I live."
- "So just maybe take this afternoon and think about what you would do in the event of a zombie apocalypse."

# Oneliner

Beau introduces his new channel and explains why FEMA and the CDC use zombies in emergency preparedness, while also addressing survival skills like starting a fire with wet wood and encouraging preparedness for future disasters.

# Audience

Community members

# On-the-ground actions from transcript

- Contemplate emergency preparedness scenarios and develop necessary survival skills (implied)

# Whats missing in summary

Beau's engaging storytelling and practical advice on emergency preparedness through unconventional teaching tools.

# Tags

#EmergencyPreparedness #CommunityNetworking #ZombieApocalypse #SurvivalSkills #DisasterPreparedness