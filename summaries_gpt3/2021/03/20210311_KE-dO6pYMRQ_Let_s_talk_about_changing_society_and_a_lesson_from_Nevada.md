# Bits

Beau says:

- Introduces the concept of "breaking the hammer" as a defensive strategy to achieve societal change.
- Attributes Trump not being in office to four years of activist efforts through campaigning, organizing, and educating.
- Emphasizes that merely being against something doesn't bring the desired societal change; it only slows down what you oppose.
- States that building a just world is challenging compared to opposing injustice, which is relatively easy.
- Advocates for going on the offensive by establishing Community Networks to create political change rapidly.
- Cites the example of Nevada where the progressive part of the Democratic Party took over through years of organizing.
- Stresses the importance of offering solutions and building something better rather than solely opposing existing systems.
- Contrasts the approach of Trump, who capitalized on grievances without offering substantial solutions, with the need for a constructive plan to create real change.
- Points out that being against systemic issues or policies is insufficient; one must have a plan to address and replace them.
- Concludes by urging people to put in the work to build the society they desire rather than merely opposing the current state of affairs.

# Quotes

- "Building a just world is hard. Being against injustice, it's even easy socially to take that stance."
- "Being against something doesn't actually get you the society you want. Doesn't get you the world you want. It just slows the world you don't."
- "You have to put in the work to build the society you want, not just be against what exists."

# Oneliner

Beau touches on defensive and offensive strategies for societal change, advocating for Community Networks and constructive planning over mere opposition.

# Audience

Change-makers, Activists, Community Leaders

# On-the-ground actions from transcript

- Establish Community Networks to create rapid political change (implied)
- Offer solutions and build something better to attract support (implied)
- Organize and mobilize for systemic change (implied)

# Whats missing in summary

The full transcript provides additional depth on the importance of proactive efforts, community organizing, and the necessity of constructive solutions for societal transformation.

# Tags

#SocietalChange #CommunityNetworks #Activism #BuildingBetterSociety #ProactiveApproach