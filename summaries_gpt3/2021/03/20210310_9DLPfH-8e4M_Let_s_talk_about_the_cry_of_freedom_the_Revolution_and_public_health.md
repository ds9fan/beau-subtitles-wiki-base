# Bits

Beau says:

- Governors across the country are removing mandates while invoking the image of the founders and the fight for freedom.
- Washington embraced science and ordered strict quarantines during the smallpox outbreak in Boston, showing a proactive approach to public health.
- Washington faced opposition from close-minded and uninformed politicians regarding inoculation, a form of early vaccination.
- Despite Congress banning inoculation, Washington went ahead and inoculated his troops, leading to Congress repealing the ban later due to its effectiveness.
- The idea of the founders disregarding science for freedom in public health matters is historically inaccurate.
- During Valley Forge, many troops received inoculation, showing the founders' commitment to public health even in crude forms.
- If Washington were in charge today, there likely would have been massive lockdowns, mask mandates, and large-scale vaccine drives.
- Washington's action of ordering troop inoculation against Congress's wishes was the first major public health initiative in the United States.

# Quotes

- "Washington embraced science and ordered strict quarantines during smallpox outbreak."
- "The idea of founders disregarding science for freedom in public health is inaccurate."
- "If Washington were in charge today, we probably have had massive lockdowns."

# Oneliner

Beau dives into how Washington's proactive public health measures debunk the notion of founders prioritizing freedom over science in today's context.

# Audience

History enthusiasts, public health advocates.

# On-the-ground actions from transcript

- Join or support organizations advocating for evidence-based public health measures (implied).
- Advocate for mask mandates and large-scale vaccine drives in your community (implied).
- Educate others on the historical importance of public health initiatives in shaping the nation's health policies (implied).

# Whats missing in summary

The full transcript provides a detailed historical perspective on how Washington's proactive public health measures challenge the notion of founders prioritizing freedom over science.

# Tags

#PublicHealth #FoundingFathers #Inoculation #Lockdowns #VaccineDrives