# Bits

Beau says:

- Explains how those in power provoke emotional reactions to divert blame onto marginalized groups.
- Responds to a viewer request regarding the Republican Party's tactics of demonizing and blaming marginalized groups.
- Provides an example of Republican senators requesting information on vaccinated migrants to manipulate public opinion.
- Points out the dual tactics the Republican Party will use regardless of the information received: vaccine distribution criticism or disease fear-mongering.
- Raises the irony of senators downplaying public health suddenly showing concern to provoke outrage against demonized groups.
- Notes how blaming marginalized groups diverts attention from the government's failure in managing public health crises.
- Emphasizes the strategy of directing anger towards marginalized groups rather than those in power to scapegoat them.
- Illustrates how marginalized groups are unfairly targeted and scapegoated despite having no decision-making power.
- Stresses that problems often stem from those in power using emotional and divisive rhetoric to shift blame.
- Concludes with a metaphorical comparison of blaming immigrants for problems to the idea of immigrants taking vaccines.

# Quotes

- "People who have less influence and less institutional power are never the source of your problems. The source of your problems pretty much always comes from the people who have power."
- "It's easier to point to a class of people that have already been demonized."
- "That immigrant's going to take your vaccine."
- "Blame it on people who have absolutely no power."
- "The rich guy with all the cookies, one cookie left in the center of the table. That immigrant's going to take your cookie."

# Oneliner

Beau explains how the Republican Party manipulates public opinion by demonizing marginalized groups to deflect blame for policy failures onto those with no power.

# Audience

Viewers

# On-the-ground actions from transcript

- Challenge demonization of marginalized groups (implied)
- Advocate for accurate information sharing (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of how political parties manipulate public opinion through scapegoating and diversion tactics.

# Tags

#PoliticalManipulation #Scapegoating #PublicOpinion #MarginalizedGroups #PowerDynamic