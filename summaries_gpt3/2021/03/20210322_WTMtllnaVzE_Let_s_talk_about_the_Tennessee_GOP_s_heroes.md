# Bits

Beau says:

- Beau, a Tennessee native, expresses his opinion on the Republican Party's plan to terminate the historical commission in Tennessee.
- The historical commission decided to remove a statue of Nathan Bedford Forrest from Capitol grounds, prompting claims of cancel culture from the Republican Party.
- Beau acknowledges Forrest's historical significance but questions his deservingness of a prominent statue due to his controversial actions during the Civil War.
- Beau suggests that monuments to Forrest's legacies already exist in Tennessee, like the Fort Pillow park where Forrest's troops committed atrocities during the Civil War.
- He believes there are better heroes in Tennessee more deserving of being honored in the Capitol than Forrest.
- Beau expresses disappointment in the Republican Party's stance on keeping the Forrest statue, feeling that it doesn't represent the views of the people of Tennessee.

# Quotes

- "There are better heroes. There are people more deserving of being in that spot."
- "His legacy is secure. He will be remembered, no doubt."
- "I'm appalled that the Republican Party has a problem with removing it."
- "Maybe I'm wrong. Anyway, it's just a thought."

# Oneliner

Beau from Tennessee questions the Republican Party's defense of Nathan Bedford Forrest's statue, suggesting better heroes for Capitol grounds. 

# Audience

Tennessee residents

# On-the-ground actions from transcript

- Advocate for the removal of controversial statues in your community (suggested)
- Support the recognition of better heroes who unite rather than divide (implied)

# Whats missing in summary

Full context and emotional depth can be experienced by watching the entire video.

# Tags

#Tennessee #NathanBedfordForrest #CivilWar #HistoricalCommission #Monuments