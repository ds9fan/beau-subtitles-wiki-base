# Bits

Beau says:

- Governor Abbott used a political move to shift blame onto immigrants for potential COVID-19 spread in Texas.
- Abbott's tactic of blaming immigrants is a common political strategy in American history to deflect responsibility.
- Despite the blame game, statistics show that countries immigrants are likely from, like Honduras, Nicaragua, El Salvador, and Mexico, are doing better at preventing new COVID-19 cases compared to Texas.
- Even with potentially underreported numbers, these countries still fare better in controlling the virus than Texas.
- Abbott's playbook move of blaming immigrants is harmful and diverts attention from real issues, affecting everyone negatively.
- The tactic of blaming immigrants rather than accepting responsibility is outdated and needs to change.
- The blame game only serves to harm marginalized groups and society as a whole.
- Immigrants are often scapegoated instead of holding powerful figures accountable for their actions.
- Despite poorer healthcare systems in countries like Nicaragua, El Salvador, and Honduras, they are still more successful at preventing new COVID-19 cases than Texas.
- The blame game perpetuates harmful narratives and fails to address the real challenges effectively.

# Quotes

- "Blame the brown person."
- "This is bad for everybody."
- "They are still better at preventing new cases than Texas."

# Oneliner

Governor Abbott's blame game on immigrants diverts attention from real issues, despite statistics showing countries of origin faring better in preventing COVID-19 than Texas.

# Audience

Texans, policymakers, activists

# On-the-ground actions from transcript

- Confront harmful narratives against immigrants (exemplified)
- Advocate for accountability rather than scapegoating (exemplified)

# Whats missing in summary

The detailed statistics and comparison of COVID-19 cases between Texas and countries of origin of immigrants. 

# Tags

#Immigration #COVID-19 #BlameGame #GovernorAbbott #Texas