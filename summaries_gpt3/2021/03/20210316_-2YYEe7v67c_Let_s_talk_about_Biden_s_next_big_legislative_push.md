# Bits

Beau says:

- Questions arise about Biden's upcoming legislative agenda, including comparisons to the Green New Deal, infrastructure programs, and tax increases.
- Details of the agenda are yet to be released, leading to speculation based on campaign promises and advisors' recommendations.
- Predictions include a potential 6 to 7 percent corporate tax increase, aiming to encourage reinvestment or higher employee wages.
- Biden's tax plan does not advocate raising taxes for individuals earning less than $33,000 a month.
- The agenda may focus on taxing the wealthy, in line with Biden's campaign promises and advisors' suggestions.
- Beau argues that a tax increase on the wealthy is a fundamentally American concept, supported by the country's philosophical founders.
- He challenges the idea of no taxes, stating it eliminates the need for a government and relies on individuals' voluntary social responsibility.
- Beau points out the potential benefits of the tax increase, such as increased business investment or support for large foundations.
- The infrastructure package is speculated to be an environmentally friendly build-out, but not a full-fledged Green New Deal.
- Uncertainty surrounds the agenda's details and passage, with potential strategies including budget reconciliation and filibuster amendments.

# Quotes

- "The idea of increasing the corporate tax rate is to get companies to reinvest the money."
- "If you don't make more than $33,000 a month, none of it applies to you."
- "It's all guessing. So that's where we're at."

# Oneliner

Beau speculates on Biden's legislative agenda, focusing on potential tax increases for the wealthy and an environmentally friendly infrastructure package.

# Audience

Policy analysts, political enthusiasts

# On-the-ground actions from transcript

- Stay informed about Biden's legislative agenda and its potential impact (suggested).
- Advocate for socially responsible tax policies (implied).

# Whats missing in summary

Further insights on Biden's legislative agenda and its implications can be gained from watching the full video. 

# Tags

#Biden #LegislativeAgenda #TaxPolicy #Infrastructure #GreenNewDeal