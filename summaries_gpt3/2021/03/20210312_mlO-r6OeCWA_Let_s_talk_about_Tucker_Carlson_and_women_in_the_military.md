# Bits

Beau says:

- Tucker Carlson criticized the military for women getting proper maternity uniforms and relaxed hairstyles.
- Carlson's opinion on combat readiness holds no weight with those who have experienced combat.
- Women needing maternity uniforms for active duty activities don't need a lecture on toughness from Carlson.
- Beau suggests women veterans participating in a charity boxing event with Carlson.
- Beau challenges Carlson to an obstacle course and PT test against women from each branch.
- Chinese military is mentioned as becoming more masculine, but they have about 100,000 women in their military.
- Beau points out Carlson's lack of understanding when criticizing Lloyd Austin's qualifications.
- The idea of maintaining Roman Legion standards in the military is deemed ridiculous by Beau.
- Beau suggests PT standards should be more job-based rather than uniform across the board.
- The Army responded to Carlson's comments by tweeting images of women soldiers.

# Quotes

- "Nobody could honestly argue that he wasn't qualified."
- "After the last couple of years, I suggest it's vital that it changes."
- "Fox News is a detriment to morale, and that might change."
- "I think it'd be entertaining to watch."
- "It's just a thought, y'all have a good day."

# Oneliner

Beau challenges Tucker Carlson's criticism of the military's standards, calls out his lack of understanding, and suggests a showdown with women veterans to prove a point.

# Audience

Military advocates, Women in the military

# On-the-ground actions from transcript

- Challenge harmful narratives: Address misinformation and stereotypes (suggested)
- Support women in the military: Advocate for fair treatment and respect (suggested)
- Boost morale: Share positive stories and images of women soldiers (implied)

# Whats missing in summary

The full transcript provides detailed insights into the criticism faced by the military and the pushback against it, showcasing the importance of understanding and supporting women in the military.