# Bits

Beau says:

- Advocates for planting food in yards annually for food security, exercise, environmental benefits, and community building.
- Encourages planting fruit trees, nut trees, bushes, and gardens tailored to USDA growing zones.
- Points out misconceptions about difficulty, time, cost, and space constraints for planting food.
- Notes that planting trees and bushes requires minimal effort once established.
- Mentions the availability of seeds or plants for those on SNAP or EBT programs.
- Suggests using containers for limited space and replacing ornamental bushes with fruit-bearing plants.
- Emphasizes the community-wide benefits of sharing fresh produce with neighbors.
- Proposes a simple and enjoyable way to strengthen local communities through gardening.

# Quotes

- "Food not lawns."
- "It helps foster that community."
- "It's not hard if you're just focusing on trees and bushes."
- "Generally, it's not expensive."
- "It's just a thought."

# Oneliner

Beau advocates for planting food in yards annually for food security, exercise, and community building, debunking common misconceptions about difficulty, time, cost, and space constraints. 

# Audience

Community garden enthusiasts

# On-the-ground actions from transcript

- Plant fruit trees, nut trees, bushes, or garden in your yard tailored to your USDA growing zone (exemplified).
- Share excess produce with neighbors to foster community (implied).

# Whats missing in summary

The full transcript includes detailed explanations of the benefits of planting food in yards and debunks common misconceptions, providing a comprehensive guide for those interested in community gardening.

# Tags

#CommunityGardening #FoodSecurity #EnvironmentalBenefits #CommunityBuilding #USDAZones