# Bits

Beau says:

- He was repeatedly asked about the absence of other heritage months, like Irish, Asian, Native, Jewish, and Latino, during Black History Month.
- Explains the origins and formalization of each of these heritage months, pointing out that they already exist in federal law.
- Notes the lack of Latino Heritage Month, but mentions Hispanic Heritage Month, which was formalized into law in 1988.
- Questions the intentions behind the repeated inquiries, suggesting they were not in good faith.
- Emphasizes the importance of Black History Month for black Americans to connect with their history and heritage.
- Acknowledges the lack of cool origin stories for black Americans due to the impact of slavery on their lineage.
- Criticizes those who attempt to diminish the significance of Black History Month.
- Calls for addressing inequality and injustice that have resulted from historical practices.
- Expresses confusion over the desire to detract from something meaningful to others for no valid reason.
- Concludes by wishing a happy Irish-American Heritage Month and Women's History Month.

# Quotes

- "They all exist."
- "The goal wasn't to find anything out. The goal was to try to take away from Black History Month."
- "It's more important for black Americans to be able to look into their history a little bit."
- "I will never understand the desire of wanting to take something away from somebody that obviously matters to them for no reason."
- "Inequality and a lot of injustice that is eventually going to have to be addressed."

# Oneliner

Beau addresses the presence of various heritage months, debunking misconceptions and underscoring the importance of Black History Month amidst attempts to diminish it.

# Audience

Advocates for social justice

# On-the-ground actions from transcript

- Celebrate and uplift different heritage months within your community (suggested)
- Educate others about the significance of heritage months and the importance of recognizing diverse histories (suggested)
- Challenge misconceptions and misconstrued narratives surrounding heritage months (implied)

# Whats missing in summary

In-depth exploration of the impact of systemic inequality and historical injustices on marginalized communities.

# Tags

#HeritageMonths #BlackHistoryMonth #Inequality #SocialJustice #CommunityAwareness