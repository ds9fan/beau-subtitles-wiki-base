# Bits

Beau says:

- Advocates for diversifying friend groups for societal benefits.
- Acknowledges hurdles in diversifying friend groups, such as geographic limitations and unconscious bias.
- Warns against tokenizing people from different demographics.
- Emphasizes the importance of genuine friendships with diverse individuals.
- Addresses unconscious biases and the need to overcome them in diversifying friend groups.
- Encourages accepting corrections and growth through interactions with diverse friends.
- Shares a personal experience of realizing and eliminating biased vocabulary.
- Stresses the value of diverse friendships in gaining different viewpoints and understanding social issues.
- Mentions the enrichment and societal benefits of having a diverse friend group.
- Cautions against tokenization as it can hinder the benefits and worsen the situation.

# Quotes

- "You can't tokenize people."
- "If you do this, your life becomes significantly enriched."
- "Accept it. Don't get defensive. They're trying to correct you."
- "The big thing that you have to watch out for though is tokenizing people."
- "It's just a thought."

# Oneliner

Beau advocates for diversifying friend groups, warns against tokenization, and stresses the value of genuine, diverse friendships for personal growth and societal benefits.

# Audience

Individuals seeking to diversify their friend groups.

# On-the-ground actions from transcript

- Make genuine efforts to befriend individuals from different demographics (suggested).
- Overcome unconscious biases by acknowledging and addressing them within yourself (implied).
- Accept corrections and feedback graciously when interacting with friends from diverse backgrounds (implied).

# Whats missing in summary

Beau's engaging storytelling and personal anecdotes that make the importance of diversifying friend groups relatable and actionable.

# Tags

#Diversity #Friendship #UnconsciousBias #Acceptance #SocietalBenefits