# Bits

Beau says:

- Voting in the United States is taken for granted, with voting on various aspects being a common occurrence from an early age.
- The real power in the government lies with lobbyists and the money they have, making citizens' votes more of a suggestion.
- China is attempting to diminish the autonomy of Hong Kong through methods like adding 300 informed "quality voters" to the election committee, a move opposed by the GOP.
- State representatives and Republicans across the country are actively involved in voter suppression campaigns, including limiting polling stations and mail-in voting access to certain demographics.
- Beau criticizes the pretext of voter security used to pass legislation that suppresses votes, labeling it as an abandonment of democracy.
- He calls out a state representative in Arizona, John Kavanaugh, for focusing on the quality of votes and using security as a guise for voter suppression.
- The voter suppression efforts disproportionately impact certain groups, particularly those who helped flip Georgia blue.
- Beau urges those who support electoralism and want to participate in elections to pay attention to the nationwide Republican efforts to suppress votes.
- He warns that if these suppression tactics succeed, the voice of the people in government will be severely compromised.

# Quotes

- "Not everybody wants to vote. And if somebody's uninterested in voting, that probably means they're totally uninformed on the issues."
- "It's an abandonment of the idea of democracy."
- "I have a lot of issues with low-information voters. I have more issue with low-information representatives."
- "It's not the votes that count. It's who determines the quality of the votes."
- "If stuff like this gets through, even the pretext that the people have some voice in government is gone."

# Oneliner

Beau warns of nationwide Republican efforts to suppress votes, calling it an abandonment of democracy and a threat to the people's voice in government.

# Audience

Voters, Activists, Community Members

# On-the-ground actions from transcript

- Pay attention to and actively oppose voter suppression efforts by joining local initiatives and organizations (exemplified)
- Support voter education and outreach programs to ensure all citizens are informed and able to participate in elections (exemplified)

# Whats missing in summary

The full transcript provides a deeper insight into the ongoing challenges and threats to democracy posed by voter suppression efforts.

# Tags

#Voting #Democracy #VoterSuppression #GOP #Elections