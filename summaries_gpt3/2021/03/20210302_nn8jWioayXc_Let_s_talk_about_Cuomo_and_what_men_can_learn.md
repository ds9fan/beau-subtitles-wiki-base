# Bits

Beau says:

- Explains the confusion among men in the United States regarding flirting at work post-Cuomo situation.
- Points out that the motivation behind changing behavior doesn't matter as long as the behavior changes.
- Emphasizes the shift in responsibility from women avoiding vulnerable situations to men not creating them.
- Simplifies the rules for men by stating that they shouldn't flirt at work to avoid putting women in vulnerable positions.
- Advises to keep distance, not close doors, or block exits to maintain a professional environment.
- Urges men not to exploit power dynamics or vulnerabilities in interactions with women.
- Suggests having HR present for private interactions to ensure appropriateness and avoid misconduct.
- Criticizes the excuse of not knowing the rules, implying that men do understand but may be looking for ways to bend them.
- Argues that true flirting is subtle and not graphic or overt as often claimed in harassment cases.
- Encourages watching a video on consent and reiterates the simple rules of respecting boundaries and keeping interactions professional.

# Quotes

- "It's your society."
- "Don't exploit vulnerabilities and keep your hands to yourself."
- "Don't say anything you wouldn't say if HR was standing there."
- "The confusion, I don't buy it either because we know the rules."
- "How's that for PC?"

# Oneliner

Men in the US should refrain from flirting at work to prevent creating vulnerable situations for women, maintaining professionalism and respect.

# Audience

Men in the US

# On-the-ground actions from transcript

- Keep distance, avoid blocking exits, and refrain from inappropriate advances (implied)
- Have HR present for private interactions to maintain professionalism (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the rules and behaviors that men should follow to ensure a safe and respectful work environment for women. Watching the full transcript can provide a deeper understanding of these guidelines and the importance of respecting boundaries in professional settings.

# Tags

#WorkplaceBehavior #Professionalism #Respect #GenderDynamics #Boundaries