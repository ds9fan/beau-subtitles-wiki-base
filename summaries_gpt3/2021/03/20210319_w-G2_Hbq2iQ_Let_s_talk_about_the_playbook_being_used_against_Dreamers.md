# Bits

Beau says:

- Receiving a message critiquing his historical conclusions on America.
- Expressing disbelief in the idea of significant progress in the United States.
- Questioning whether the same discriminatory playbook is still in use today.
- Drawing parallels between past discriminatory practices and current attitudes towards undocumented workers.
- Mentioning the subtle ways discriminatory policies are implemented today.
- Bringing up issues like public school access for undocumented workers and scapegoating them for various problems.
- Referencing past instances of justifying reprisals against undocumented individuals based on isolated incidents.
- Pointing out how immigrants are blamed for various societal issues despite statistical evidence.
- Mentioning the lack of a direct "police tax" on immigrants, but suggesting the government's willingness to implement one if possible.
- Explaining how discriminatory policies are still institutionalized, such as the economic benefit requirement for legal immigrants.
- Providing historical context on immigration control in the United States and the perpetuation of exploiting and scapegoating different groups.
- Criticizing the continued use of discriminatory tactics in American politics, using Dreamers as a contemporary example.
- Emphasizing the cyclical nature of scapegoating and exploitation in American history.
- Stating the need to acknowledge past injustices to move forward and fulfill promises made to all Americans.

# Quotes

- "Have we really come that far?"
- "This isn't us. Yeah, it is us."
- "The institution of the United States is built on scapegoating people while exploiting their labor."
- "It hasn't changed. Just shifts the group of people being exploited and scapegoated."
- "The only way we can actually really achieve the promises that were made a couple hundred years ago is to acknowledge everything that's happened and make sure it doesn't happen again."

# Oneliner

Beau questions the progress of the United States, drawing parallels between historical discrimination and current policies towards immigrants, urging acknowledgment of past injustices to move forward.

# Audience

Activists, Advocates, Immigrant Rights Organizations

# On-the-ground actions from transcript

- Advocate for comprehensive immigration reform to protect the rights of all immigrants (suggested).
- Support organizations working towards fair treatment and opportunities for undocumented individuals (implied).
- Educate others on the historical and present-day discriminatory practices in immigration policies (suggested).

# Whats missing in summary

The full transcript provides a detailed analysis of historical discrimination in immigration policies and urges reflection on how acknowledging past injustices is vital for progress.

# Tags

#Immigration #Discrimination #Scapegoating #UnitedStates #History