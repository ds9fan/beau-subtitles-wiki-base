# Bits

Beau says:

- Public service announcement about the child tax credit cash in the relief bill.
- Child tax credits amount to $3,000 for children over six and $3,600 for those under six.
- Initially, news broke that people were to receive monthly checks starting in July.
- However, the IRS moved the filing deadline from April 15th to May 17th, potentially delaying the payments.
- The final legislation doesn't specify monthly payments, only "periodically," which could change the payment frequency.
- There might be a shift from monthly payments to receiving larger sums less frequently, like $600 every three months.
- Beau advises against making financial decisions based on expecting this money in July due to the uncertain timeline.
- To be eligible for this credit, individuals must have filed for the 2020 tax season.
- People who usually don't file taxes may need to do so this year to receive this benefit.
- The extension of the filing deadline was aimed at helping small business owners have more time to settle their tax obligations.

# Quotes

- "This is more of a public service announcement than a normal video."
- "Be aware that there have been some pretty significant changes that don't seem to be getting any press."
- "Just be aware that there have been some pretty significant changes."

# Oneliner

Beau shares updates on the child tax credit cash, cautioning against banking on July payments due to filing deadline shifts and potential changes in payment frequency.

# Audience

Taxpayers, parents, individuals eligible for child tax credit.

# On-the-ground actions from transcript

- File for the 2020 tax season to be eligible for the child tax credit (suggested).
- Inform others about the potential changes in payment frequency for the child tax credit (implied).

# Whats missing in summary

Detailed instructions on how to file for the child tax credit and updates on the relief bill.

# Tags

#TaxCredit #ChildTaxCredit #IRS #FilingDeadline #PublicService