# Bits

Beau says:

- Explains a bad faith argument surrounding accusations of Biden having dementia.
- Argues that there's no need to respond in good faith to baseless claims.
- Points out the futility of trying to prove a negative.
- Mentions the presumption of innocence and the endless shifting of goalposts.
- Describes how people latch onto any evidence to support their claims about Biden's dementia.
- Talks about how mispronunciation doesn't indicate lack of intelligence.
- Suggests leaning into the claim that Biden's advisors make decisions for him.
- Mentions the theory that Harris has the final say over Biden.
- Comments on Trump being beaten in the polls by a non-white woman.
- Concludes by discussing methods to handle such arguments effectively.

# Quotes

- "You cannot prove a negative."
- "Lean into that."
- "A non-white woman is beating him in the polls? That's gotta bother him."
- "We elected a guy with dementia over him. That's how bad he was doing."
- "Shutting them up is the next best thing."

# Oneliner

Beau explains the futility of proving baseless claims and suggests tactics to handle bad faith arguments effectively.

# Audience

Debaters, truth-seekers, critical thinkers.

# On-the-ground actions from transcript

- Lean into wild accusations (exemplified).
- Repeat process to shut down baseless claims (implied).

# Whats missing in summary

The importance of shutting down baseless claims effectively in debates or arguments.

# Tags

#Debunking #BadFaithArguments #Biden #Dementia #PresidentialPolitics