# Bits

Beau says:

- Kentucky state senate passed a bill making it a misdemeanor to insult or taunt law enforcement officers.
- The bill is a blatant violation of the First Amendment and is likely to be overturned in court.
- The bill raises questions about the senators' authoritarian views and their perception of the people of Kentucky.
- This legislation empowers law enforcement to use force more frequently, especially in a climate where excessive force is already a concern.
- The bill could lead to officers using violence based on subjective interpretations of gestures or words.
- Senators who supported this bill are seen as authoritarians giving law enforcement a blank cheque for excessive force.
- This legislation is an attempt to provide cover for excessive force and could result in numerous lawsuits.
- Beau urges Kentuckians to contact their senators and the governor's office to prevent this bill from becoming law.
- He predicts that if the bill passes, there will be significant legal repercussions and financial costs for the state.
- Beau stresses the importance of citizens taking action to prevent the potential negative consequences of this bill.

# Quotes

- "Any senator who voted for this needs to be voted out of office."
- "If this becomes law, I give it no more than 10 arrests under this statute before there is a million dollar payout."
- "The people of Kentucky probably need to get a hold of their senators."

# Oneliner

Kentucky bill criminalizing insults towards officers reveals authoritarian views and threatens excessive force empowerment, urging citizen action.

# Audience

Kentuckians

# On-the-ground actions from transcript

- Contact your senators and the governor's office to oppose the bill (suggested).
- Advocate for preventing the bill from becoming law by reaching out to lawmakers (suggested).

# Whats missing in summary

The full transcript provides a detailed analysis of the problematic bill and urges citizens to take action to prevent its harmful effects from becoming reality.

# Tags

#Kentucky #FirstAmendment #Authoritarianism #LawEnforcement #ExcessiveForce