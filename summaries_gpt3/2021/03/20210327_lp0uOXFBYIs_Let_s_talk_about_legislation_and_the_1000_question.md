# Bits

Beau says:

- Explains the challenge he received from a friend to define what the gun control crowd wants to ban.
- Sets conditions for defining the term and sharing an alternative data-driven solution.
- Defines the specific firearms the gun control crowd aims to ban: semi-automatic firearms chambered between 5.45mm and 7.62 with detachable magazines holding over 19 rounds.
- Points out that focusing on banning these specific firearms may not be effective as manufacturers can adapt and criminals may find alternatives.
- Advocates for focusing on prohibiting people with a history of domestic violence from owning firearms based on data showing a link between domestic violence and incidents.
- Emphasizes the importance of closing loopholes in existing laws related to domestic violence and firearm ownership.
- Suggests that prohibiting individuals with a history of domestic violence from owning firearms is a more effective and actionable solution than regulating firearm design.
- Mentions an additional observation about the correlation between people who are cruel to animals and those who commit violent acts.
- Encourages focusing on actionable and data-driven solutions that are attainable and likely to receive support.

# Quotes

- "What you are talking about when you are talking about these large incidents, what you're wanting to get rid of is semi-automatic firearms that are chambered somewhere between 5.45 millimeter and 7.62 that accept a detachable magazine that holds more than 19 rounds. That's it."
- "I've advocated this for a long time based off personal observation. There's now data. 749 incidents were looked at about 60 percent of them have one thread one common thread, domestic violence."
- "If it's about saving lives, that's where you need to look."

# Oneliner

Beau sets conditions for defining gun control terms, advocates focusing on domestic violence prevention over regulating design, and suggests an attainable, data-driven solution for reducing gun violence.

# Audience

Advocates, policy makers, activists

# On-the-ground actions from transcript

- Advocate for prohibiting individuals with a history of domestic violence from owning firearms (advocated)
- Support efforts to close loopholes in existing laws related to domestic violence and firearm ownership (advocated)
- Raise awareness about the correlation between domestic violence and incidents involving firearms (suggested)

# Whats missing in summary

The full transcript provides detailed insights and data-driven solutions on addressing gun control issues effectively.

# Tags

#GunControl #DataDriven #DomesticViolence #Advocacy #Prevention