# Bits

Beau says:

- Talks about the importance of backup plans, like the Seed Vault, to preserve historic artifacts and samples in case of any eventuality.
- Mentions a proposed ultimate backup plan to place samples of 6.7 million species on the moon in cryopreservation facilities.
- Points out the significance of decentralization in preserving samples to ensure their availability even in catastrophic events like Yellowstone erupting.
- Describes the amazing logistics involved in the lunar backup plan.
- Emphasizes the contrast between people's priorities on Earth, from preserving old books to preserving the genetic code of Earth on the moon.
- Raises the question of tradition versus future good as a key talking point that divides mindsets.
- Suggests that the way things have always been done may necessitate extreme measures like an ark on the moon.

# Quotes

- "Or we can explore the galaxy."
- "When you look at it like that, it really shows the difference in mindset, what the priorities are."
- "The way we’ve always done it may end up leading to the need for an ark on the moon."

# Oneliner

Beau talks logistics, decentralization, and contrasting priorities between preserving old books and genetic codes on the moon.

# Audience

Space enthusiasts, conservationists

# On-the-ground actions from transcript

- Preserve biodiversity locally through community gardens and conservation efforts (implied)
- Support initiatives that focus on environmental preservation and space exploration (implied)

# Whats missing in summary

The full transcript provides a deeper dive into the implications of contrasting priorities and the necessity of forward-thinking conservation efforts.

# Tags

#BackupPlans #Logistics #Priorities #Preservation #Decentralization #SpaceExploration