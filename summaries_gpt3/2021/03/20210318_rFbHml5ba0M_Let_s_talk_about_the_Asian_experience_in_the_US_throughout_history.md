# Bits

Beau says:

- Explains how the current issues in American society are not new and have historical roots.
- Talks about how Trump didn't create the anti-Asian sentiment but capitalized on it for his base.
- Provides an abridged timeline of anti-Asian actions and legislation in the United States.
- Mentions various discriminatory laws and actions against Asians throughout history.
- Points out the importance of not falling for divisive tactics and normalizing the Asian community.
- Emphasizes the significance of exposure and integration to combat ignorance and fear.
- Urges for bringing Asian Americans into communities to prevent marginalization.
- Stresses the need to acknowledge American culpability in perpetuating anti-Asian sentiments.
- Encourages taking action to change the current narrative and not allowing politicians to exploit prejudices.

# Quotes

- "It's probably way past time to stop falling for this."
- "Exposure ends that ignorance."
- "If you have Asian friends, bring them out with you."
- "We can set the example for younger people today that this isn't okay."
- "Then we can say it's not us."

# Oneliner

Beau outlines the historical context of anti-Asian sentiment in America, urging for integration to combat division and ignorance.

# Audience

All Americans

# On-the-ground actions from transcript

- Bring Asian friends out with you, frequent Asian businesses to integrate them into the community (suggested)
- Reach out to older Americans and set the example for younger generations by making the Asian community part of our communities (implied)

# Whats missing in summary

Full understanding of the historical roots of anti-Asian sentiment and the importance of integration and exposure in combating ignorance and fear.

# Tags

#AmericanHistory #AntiAsianSentiment #Integration #Community #Action