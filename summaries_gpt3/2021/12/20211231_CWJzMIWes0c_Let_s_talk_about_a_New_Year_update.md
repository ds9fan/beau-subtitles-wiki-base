# Bits

Beau says:

- Wishing a happy new year and giving updates on the channel's direction.
- No major changes planned for the main channel as it's working well.
- Second channel, "The Roads with Beau," will have more content, including long-format documentaries and how-to videos on various topics like gardening and emergency preparedness.
- Viewers are encouraged to subscribe to "The Roads with Beau" for future content.
- Patreon subscribers will soon receive an announcement of something special as a token of appreciation.
- Cautioning viewers against purchasing merchandise from bootleg companies duplicating Teespring products, as they are of lower quality and more expensive.
- Plans to increase video uploads to three a day during midterms, but overall content will remain consistent.
- Encouraging viewers to make resolutions, have fun, and stay safe in the upcoming year.

# Quotes

- "Make your resolution. Have fun. Be safe."
- "The only place I have merchandise is on Teespring."
- "So just bear that in mind."
- "Now, over the next year, you'll probably end up going back to three videos a day."
- "It's just a thought."

# Oneliner

Beau provides updates on channel plans, merchandise caution, and future content, urging viewers to subscribe and stay safe in the new year.

# Audience

Content Creators

# On-the-ground actions from transcript

- Subscribe to "The Roads with Beau" for upcoming content (suggested).
- Support Beau's work on Patreon (suggested).
- Purchase merchandise only from Teespring to ensure quality (suggested).

# Whats missing in summary

Details on the specific content of the upcoming videos and potential collaborations.

# Tags

#ContentCreation #YouTube #Updates #Merchandise #NewYear