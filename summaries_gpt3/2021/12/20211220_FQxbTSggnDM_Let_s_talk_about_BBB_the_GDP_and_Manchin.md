# Bits

Beau says:

- Senator Manchin announced his opposition to Build Back Better, siding with Republicans, impacting the bill's passing outlook.
- Wall Street's concern was negative growth implications due to failure to pass Build Back Better, leading to revisions in expected GDP growth.
- Goldman Sachs had to reduce expected GDP growth for three quarters due to the detrimental impact of not passing the bill.
- The decision will negatively impact the US economy for at least nine months, reducing growth projections significantly.
- Failing to pass Build Back Better will reduce economic output growth by a third, affecting the US economy by billions of dollars.
- This decision prioritizes causing the US to fail to blame Biden over helping those in need.
- The timing of this decision, right before Christmas, affects critical issues like tax credits that impact the most vulnerable.
- The Republican Party and Senator Manchin are portrayed as disregarding the needs of the poor in favor of political gain.
- The decision not to pass the bill is estimated to cost the United States tens of billions of dollars in economic growth.
- Goldman Sachs suggested that corporate tax rates probably won't increase, providing a silver lining amidst the negative impacts.

# Quotes

- "Failing to pass this will negatively impact the United States' economy for at least nine months."
- "The Republicans want the United States to fail right now because then you'll blame Biden."
- "Y'all better go buy some coal."
- "That might should be the talking point."
- "Never let these people fool you into thinking they care about you again."

# Oneliner

Senator Manchin's opposition to Build Back Better will negatively impact the US economy for at least nine months, prioritizing political gain over billions of dollars in economic growth.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Contact your representatives to advocate for policies that prioritize economic growth and support vulnerable populations (suggested).
- Join organizations focused on economic justice and advocacy for those in need (implied).

# Whats missing in summary

Analysis of the broader implications on society and vulnerable communities from the failure to pass Build Back Better.

# Tags

#BuildBackBetter #USPolitics #EconomicImpact #Senate #WallStreet