# Bits

Beau says:

- Received a message from a soldier claiming that if we stopped talking about racism, it will be eliminated in America.
- Soldier is Hispanic and believes that not acknowledging racism will make it disappear.
- Explains the individual versus systemic impact of not talking about racism.
- Suggests using statistics often favored by racists to demonstrate systemic racism.
- Points out the need to address institutional issues that contribute to disproportionate crime rates among certain groups.
- Emphasizes the importance of acknowledging societal pressures that create unequal outcomes.
- Recommends discussing and addressing institutional racism before considering stopping the discourse on race.
- Mentions the popular Morgan Freeman meme that suggests stopping talking about race as a solution to racism.
- Urges for leveling the playing field before ceasing to address race issues.
- Encourages engagement in difficult but necessary dialogues to combat racism effectively.

# Quotes

- "How do you demonstrate systemic racism to somebody? Use the statistics that racists use."
- "I've actually never had that not work. That's always been pretty successful."
- "We can't stop talking about it until the playing field's level."

# Oneliner

A soldier's belief that ignoring racism will eliminate it prompts Beau to explain the importance of addressing systemic issues through familiar statistics favored by racists.

# Audience

Activists, Educators, Allies

# On-the-ground actions from transcript

- Challenge misconceptions by using statistics to demonstrate systemic racism (implied)
- Engage in dialogues that address institutional issues contributing to racial disparities (implied)

# Whats missing in summary

Beau's thoughtful insights on using familiar statistics to illustrate systemic racism and the necessity of addressing institutional issues before considering ending race-related discourse.

# Tags

#Racism #SystemicRacism #InstitutionalIssues #CommunityEngagement #Dialogues