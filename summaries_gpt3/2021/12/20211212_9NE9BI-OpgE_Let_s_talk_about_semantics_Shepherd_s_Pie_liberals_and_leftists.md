# Bits

Beau says:

- Defines semantics as the search for meaning in words and linguistics.
- Explains that when people dismiss something as "just semantics," they mean it is an irrelevant argument over definitions.
- Illustrates the concept with an example of a train conductor and a passenger arguing over the destination.
- Shares his love for cooking shepherd's pie, a dish loved by many except for his five-year-old.
- Describes making shepherd's pie with various meats like chicken or bacon, acknowledging the material change in the dish.
- Differentiates between shepherd's pie and cottage pie based on the meat used, not just for semantics but due to a substantial difference.
- Emphasizes that correcting someone over material changes is not irrelevant but educational.
- Draws a parallel between his son not eating shepherd's pie due to misunderstanding and the intentional conflation of liberals and leftists by Republicans.
- Breaks down how the Republican Party's labeling of liberals as leftists creates fear and motivates their base through scare tactics.
- Calls for Democrats to challenge and correct the misrepresentation of liberals as leftists for political benefit.

# Quotes

- "Arguing over material changes, sometimes it's important."
- "They will tell their base, their constituents, that liberal and leftist mean the same thing."
- "Allowing them to use the term leftist, radical left, when they're talking about liberals, that's just handing them a way to scare their base."
- "That gap between being a liberal who is in support of capitalism and being a leftist who is generally against capitalism, that's a pretty big gap there."
- "It's probably more important for liberals to make that distinction."

# Oneliner

Beau explains semantics through cooking shepherd's pie, illustrating the importance of clarifying distinctions like liberals vs. leftists to combat political fear-mongering.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Challenge misrepresentations in political discourse (suggested)
- Educate others on the differences between liberals and leftists (implied)

# Whats missing in summary

In watching the full transcript, viewers can gain a deeper understanding of how semantic arguments and misrepresentations in political labels can impact public perception and drive political agendas.

# Tags

#Semantics #Liberals #Leftists #PoliticalDiscourse #Misrepresentation