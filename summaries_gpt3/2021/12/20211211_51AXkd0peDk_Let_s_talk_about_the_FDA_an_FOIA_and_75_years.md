# Bits

Beau says:

- Addressing a claim about a request taking 75 years, Beau explains the situation behind a Freedom of Information Act (FOIA) request made to the FDA for documents related to the emergency approval of the vaccine.
- The request includes hundreds of thousands of documents that need to be processed, with a standard of providing 500 pages per month for large FOIA requests.
- The FDA will start releasing the information immediately, but it will take 75 years to process all of it due to the massive request size, not because they are trying to hide something.
- Beau mentions that judges have intervened in similar cases to expedite the process due to intense public interest, suggesting that bringing in additional resources could speed up the information release.
- He advises the plaintiffs to narrow the scope of their request to focus on relevant information, as many pages may contain redacted or duplicate content that could slow down the process.
- Beau stresses the importance of releasing pertinent information quickly to address vaccine hesitancy and increase vaccination rates, stating that public interest and transparency should be prioritized.

# Quotes

- "There's enough little screenshots that could be turned into memes that could lead people to believe that."
- "Start with the important stuff."
- "It's in Freedom of Information Act request. 500 pages a month is normal."
- "There's no secret here, there's no grand conspiracy, it's just the way it works."
- "If they actually do want to assure the vaccine hesitant, they should be trying to make it as easy as possible for the FDA to get that information out."

# Oneliner

Beau clarifies a claim about a 75-year wait for information release, urging for transparency and efficiency in handling a large Freedom of Information Act request.

# Audience

Government officials, activists

# On-the-ground actions from transcript

- Bring in additional resources from other agencies to expedite the processing of FOIA request (suggested)
- Plaintiffs should narrow the scope of their request to focus on relevant information and ease the processing burden (suggested)

# Whats missing in summary

Beau's detailed explanation on the process and implications of large FOIA requests can provide clarity on bureaucratic procedures and transparency challenges. 

# Tags

#FOIA #VaccineApproval #Transparency #Government #PublicInterest