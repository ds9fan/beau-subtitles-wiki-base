# Bits

Beau says:

- Beau addresses questions about Kellogg and the ongoing situation with the union.
- He points out that he doesn't personally know anyone in the union, so his insights are secondhand.
- The issue at Kellogg revolves around a two-tiered system where newer employees do not receive the same benefits as longer-tenured employees.
- Public support by not consuming Kellogg products is seen as significant to support the union's cause.
- Beau stresses the importance of public support for union activity and its long-term impacts on companies.
- Referencing the John Deere strike, Beau illustrates how public support can influence future consumer choices and impact companies for a prolonged period.
- Supporting organized labor activities can help prevent wages from losing buying power over time.

# Quotes

- "I don't cross a picket line. There's nothing more to that sentence. Period. Full stop. I don't do it."
- "Public support is important, it is always helpful for the union."
- "The key part here, when you're talking about it, is the company saying they're just going to hire new employees."
- "Organized labor activity is good, actually."
- "Y'all have a good day."

# Oneliner

Beau addresses the Kellogg union situation, focusing on the two-tiered system and the importance of public support for unions to maintain fair wages and benefits.

# Audience

Workers, consumers, activists

# On-the-ground actions from transcript

- Support the union by not consuming Kellogg products (implied)
- Show public support for organized labor activities (exemplified)

# Whats missing in summary

Insights on the potential long-term consequences for companies and consumers when public support for unions is lacking.

# Tags

#Kellogg #Union #LaborRights #PublicSupport #OrganizedLabor