# Bits

Beau says:

- Explains the incident at Burlington and the Los Angeles Police Department involving a person with a bike lock hitting people and shots being fired.
- Describes cops arriving, suspect leaving and re-entering the building to beat a woman, and cops making a messy entry.
- Points out that the cops involved were not a SWAT team but regular police officers.
- Details the officer with a rifle firing three rounds, accidentally killing a 14-year-old girl in a dressing room.
- Criticizes the media for not accurately portraying what the officers encountered and questioning the officers' behavior.
- Raises concerns about the justification for shooting someone without a gun and the lack of de-escalation tactics used.
- Suggests that while the officers may be ruled justified within policy, it doesn't mean their actions were necessary or right.
- Calls for changes in policy, more training, and a reevaluation of the use of lethal force.
- Urges people to watch the body camera footage to understand the reality of such situations.
- Warns about the dangers of using firearms in home defense scenarios, citing risks of overpenetration in household layouts.
- Questions the necessity of shooting when nobody was in imminent danger, surrounded by cops.

# Quotes

- "It expands it a little bit."
- "That's what it demonstrates."
- "It's all fun and games until it's real."
- "The rounds are going into your kids' bedrooms."
- "The question is, was it necessary to shoot?"

# Oneliner

Beau provides a critical analysis of an incident involving police shooting, raising questions about policy, necessity, and the use of lethal force.

# Audience

Community members, activists, police reform advocates.

# On-the-ground actions from transcript

- Watch the body camera footage and listen to the officer's voice (suggested).
- Advocate for policy changes and more training (implied).

# Whats missing in summary

Deeper insights into police conduct and policy implications.

# Tags

#PoliceShooting #PoliceReform #LethalForce #PolicyChange #CommunitySafety