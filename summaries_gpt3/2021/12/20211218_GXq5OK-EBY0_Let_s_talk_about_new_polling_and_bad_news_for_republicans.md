# Bits

Beau says:

- New polling reveals trouble for the Republican Party due to a significant disparity in vaccination rates between Trump and Biden voters.
- The poll focuses on whether the government should encourage vaccination, not on mandates or policies.
- 40% of Trump voters are unvaccinated, compared to only 7% of Biden voters.
- Republicans are becoming more opposed to encouraging vaccination, with the number rising from 35% to 48% in three months.
- Democrats and independents show higher support for vaccination encouragement compared to Republicans.
- The increasing opposition among Republicans may lead to surprise victories for Democrats in elections.
- The trend of more people opposing vaccination encouragement is expected to continue.
- The disparity in vaccination rates between party supporters may impact future elections significantly.

# Quotes

- "40% of Trump voters are unvaccinated, compared to only 7% of Biden voters."
- "There are going to be surprise victories for Democrats because of these numbers."
- "It's unnecessary, and these numbers are getting bigger."

# Oneliner

New polling shows a significant vaccination divide between Trump and Biden voters, posing trouble for the Republican Party as increasing opposition may lead to surprise victories for Democrats.

# Audience

Voters, party supporters

# On-the-ground actions from transcript

- Encourage vaccination within your community (suggested)
- Support positive messaging around vaccination (implied)
- Advocate for bipartisan efforts to increase vaccination rates (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the impact of vaccination attitudes on political parties, urging for a shift towards bipartisan efforts for increased vaccination rates.