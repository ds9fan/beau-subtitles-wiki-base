# Bits

Beau says:

- Beau shares a humorous exchange he had with a British person over Twitter regarding statues of traitors in the South and across the country.
- The chat sparks an amusing dive into the story of Benedict Arnold during the American Revolution.
- Beau gives a brief overview of Benedict Arnold as a major general in the Continental Army who later became a traitor.
- Arnold's betrayal was driven by feelings of being overlooked for promotions and honors.
- The story goes that Arnold offered to surrender West Point to the British but ended up with a minor command instead.
- An anecdote is shared where Arnold jokes with an American officer about what they'd do if they captured him, referencing his wounded leg.
- A statue at Saratoga commemorates Arnold's left leg without explicitly naming him, showcasing history's sense of humor.
- Beau humorously appreciates the level of pettiness in creating a statue of Benedict Arnold's leg.
- History's irony is noted as Confederate statues are repurposed into memorials for civil rights leaders.
- Beau concludes with the importance of being on the right side of history and wishing everyone a good day.

# Quotes

- "History has a sense of humor."
- "There are people who see it who don't really understand what it represents because it isn't labeled."
- "It's a lot more prominent down here though, and it's nowhere near as funny."
- "History does have a sense of humor just like today."
- "It's just a thought."

# Oneliner

Beau shares humor and history, discussing Benedict Arnold and the irony of traitor statues with a lesson on being on the right side.

# Audience

History enthusiasts

# On-the-ground actions from transcript

- Visit historical sites to learn about significant events and figures (suggested)
- Support initiatives that repurpose controversial statues for positive purposes (implied)

# Whats missing in summary

The full transcript captures Beau's engaging storytelling style and his knack for blending humor with historical anecdotes.