# Bits

Beau says:

- Beau receives a message with philosophical and moral questions about following a rule when no one else around honors it.
- The message raises questions about volunteering at the hospital to honor a rule (Rule 303) and potentially exposing a dying father to COVID.
- Beau talks about the importance of maximizing good even when others don't seem to care or want it.
- He addresses the immediate question of continuing to volunteer in a risky situation when caring for an immunocompromised father.
- Beau stresses the concept of integrity, doing the right thing even when nobody else is, regardless of recognition or approval.
- Taking a break is encouraged to prevent burnout and enjoy time with loved ones.
- He acknowledges the importance of perseverance in doing good, even when faced with apathy from others.
- Beau reassures that volunteers will still be needed and encourages staying true to one's values.
- The message ends with a reminder to not let the world change you, even when faced with challenges.
- Beau advocates for taking breaks but getting back in the fight to continue making a difference.

# Quotes

- "Integrity is doing the right thing when nobody else is."
- "Sometimes you're not going to be able to change the world, but you should do everything you can to make sure that the world doesn't change you."
- "It's okay to take a break, you're allowed, everybody's allowed."

# Oneliner

Beau addresses moral dilemmas, stressing integrity, taking breaks, and continuing to do good even when faced with apathy.

# Audience

Helpers and volunteers

# On-the-ground actions from transcript

- Take a break to prevent burnout and spend quality time with loved ones (implied)
- Continue volunteering if possible, but know it's okay to take a break (implied)
- Stay true to your values and continue doing good even when faced with apathy (implied)

# Whats missing in summary

Advice on balancing volunteer work with personal well-being and the importance of staying true to one's values despite challenges.

# Tags

#MoralDilemmas #Integrity #Volunteering #Breaks #CommunitySupport