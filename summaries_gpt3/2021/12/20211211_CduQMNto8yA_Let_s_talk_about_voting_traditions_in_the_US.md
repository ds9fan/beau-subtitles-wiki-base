# Bits

Beau says:

- Explains the historical tradition in the US of looking out for smaller demographics in the voting process.
- Addresses a message criticizing his video on voting rights in Texas.
- Refutes the claim that the US Constitution does not protect smaller demographics in voting.
- Points to Article 1, Section 3 of the Constitution, which created the United States Senate to safeguard the interests of smaller states.
- Argues that safeguarding smaller demographics in voting is an integral part of the government's structure.
- Emphasizes that ensuring equal representation is an evolving concept in the US.
- Criticizes gerrymandering and its impact on diluting voting power.
- Condemns efforts to suppress votes of those who may vote against certain parties.
- Calls out Republicans for resorting to voter suppression tactics due to a lack of new ideas.

# Quotes

- "If they weren't concerned about that, they wouldn't have a whole house where every state, no matter its size, no matter its population, got two votes."
- "Gerrymandering is wrong. I can't believe this is a conversation we're still having today."
- "The only way they can preserve power is by suppressing the vote of people who would vote against them."
- "These are actions of people who are supporting bad ideas and they know they're supporting bad ideas."
- "They'll undermine the founding principles of the country."

# Oneliner

Beau explains the historic tradition of safeguarding smaller demographics in voting, addressing criticism and condemning voter suppression tactics.

# Audience

Voters, Activists, Citizens

# On-the-ground actions from transcript

- Contact your representatives to advocate against gerrymandering and voter suppression (implied).
- Join or support organizations working to ensure fair voting practices (implied).

# Whats missing in summary

The full transcript provides a detailed historical context on voting rights and the importance of safeguarding smaller demographics in the voting process. Viewing the entire video can offer a deeper understanding of these concepts.

# Tags

#VotingRights #Constitution #Gerrymandering #Representation #VoterSuppression