# Bits

Beau says:

- Boris Johnson proposed banning imports of trophies from at-risk animals, a move supported by Beau.
- The United States imports more wildlife trophies than any other country due to exemptions in the Endangered Species Act.
- Wealthy individuals can pay for permits to hunt endangered species under the guise of conservation.
- These "canned hunts" often involve hunting animals in enclosed spaces or with no natural fear of humans.
- Biden could make changes to the Endangered Species Act through executive order to ban such imports.
- Biden faces challenges in Congress due to Republican obstructionism and may need wins to show progress.
- Taking action through executive orders could pave the way for future legislative changes.
- Democrats need to push through wins to show progress before the midterms.
- Beau suggests Biden should focus on achievable causes like banning imports of wildlife trophies.
- Thinking creatively and acting promptly is necessary for the administration to make progress.

# Quotes

- "This could give Biden some wins."
- "The administration is going to have to start thinking outside the box if it wants to get anywhere."
- "It's just a thought."
- "You're not going to get a lot of pushback either."
- "You all have a good day."

# Oneliner

Beau suggests Biden take executive action to ban imports of wildlife trophies, a move that could yield wins amidst congressional challenges.

# Audience

Conservation advocates

# On-the-ground actions from transcript

- Ban imports of trophies from at-risk animals through executive order (suggested)
- Push for wins through achievable causes (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the loopholes in the Endangered Species Act and how Biden could address wildlife conservation issues through executive action.

# Tags

#WildlifeConservation #EndangeredSpeciesAct #ExecutiveAction #BidenAdministration #RepublicanObstructionism