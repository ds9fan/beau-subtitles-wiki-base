# Bits

Beau says:

- Senator Rand Paul sent President Biden a letter asking for emergency relief funds for tornado disaster in Kentucky.
- Paul has a history of opposing such funding and has voted against it multiple times.
- Beau generally doesn't criticize someone who changes their wrong decisions to right ones, but...
- Senator Rand Paul's past actions contradict his recent statement about uniting to help and heal after disasters.
- Beau believes in giving people the benefit of the doubt but questions Paul's sincerity based on his history.
- Beau finds relief work after disasters transformative and hopes people understand Paul's prior stance against such relief.
- He shares a lawyer's insight that people change their position on issues like law and order only when they are personally affected.
- Beau hopes Senator Paul's recent actions indicate a genuine change in position for future disaster relief votes.

# Quotes

- "Politicizing that suffering would be low for even the deepest partisan."
- "Everybody's for law and order until they get caught."
- "I hope I'm wrong."

# Oneliner

Senator Rand Paul's history of opposing disaster relief funding raises doubts about his recent change of heart, prompting Beau to question his sincerity and hope for consistency in future votes.

# Audience

Kentuckians, voters

# On-the-ground actions from transcript

- Contact Senator Rand Paul's office to express support or concerns about his stance on disaster relief funding (suggested).
- Participate in local disaster relief efforts to help those affected by natural disasters in Kentucky (implied).

# Whats missing in summary

The full transcript provides deeper insights into the dynamics of changing political stances in response to disasters and the importance of consistency in policy decisions.

# Tags

#SenatorRandPaul #DisasterRelief #Kentucky #PolicyChange #Consistency