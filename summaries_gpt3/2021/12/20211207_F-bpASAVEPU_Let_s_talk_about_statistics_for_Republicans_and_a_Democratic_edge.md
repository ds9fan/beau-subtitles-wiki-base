# Bits

Beau says:

- The vaccination status is most influenced by political affiliation.
- Counties with higher vaccination rates tended to vote for Biden, while those with lower rates voted for Trump.
- Republican-leaning counties have significantly lower vaccination rates compared to Democratic-leaning counties.
- The Democratic Party has a much higher vaccination percentage compared to the Republican Party.
- Unvaccinated individuals are significantly more likely to contract and not recover from COVID-19.
- The gap in vaccination rates between Democrats and Republicans results in a higher number of Republican deaths.
- These statistics could impact election outcomes in tightly contested areas.
- Misinformation allowed by the Republican Party has led to these concerning numbers.
- Beau hopes Republicans question other misinformation spread by their party.
- Encourages everyone, regardless of political affiliation, to get vaccinated.

# Quotes

- "Your political party is the most important factor in determining whether or not you are vaccinated."
- "If you are unvaccinated, you are 5.8 times as likely to get it. You are 14 times as likely to not recover from it."
- "Most of those lost will be Republicans."
- "Misinformation allowed to circulate by the Republican Party is costing their own constituents."
- "Anyway, go get vaccinated."

# Oneliner

The influence of political affiliation on vaccination rates and COVID-19 outcomes reveals stark disparities and potential electoral impacts, underscoring the consequences of misinformation spread.

# Audience

Republicans, Democrats, All

# On-the-ground actions from transcript

- Get vaccinated (exemplified)

# Whats missing in summary

The full transcript provides detailed insights into the correlation between political affiliation, vaccination rates, and COVID-19 outcomes, urging reflection on the impact of misinformation and the importance of vaccination.

# Tags

#Vaccination #PoliticalAffiliation #Misinformation #COVID19 #Elections