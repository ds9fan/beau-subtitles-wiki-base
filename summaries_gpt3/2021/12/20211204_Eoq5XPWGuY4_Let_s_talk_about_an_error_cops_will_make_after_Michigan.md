# Bits

Beau says:

- An incident involving students sheltering in a class and being told it's safe to come out by a person claiming to be a cop.
- Students hesitated when the person used the term "bro" and ultimately escaped through a window.
- Law enforcement agencies are looking for ways to prevent similar incidents but need to be cautious about the information they provide.
- Simply asking for department name and badge number might not be the safest approach as suspects could exploit this information.
- Beau suggests a safer alternative where officers provide a number that is not publicly visible, and students confirm the officer's identity with dispatch before opening the door.
- Training students on safety measures can inadvertently train suspects as well, so it's vital to have secure protocols in place.
- Beau warns against using information that suspects could easily exploit in such situations.
- Parents should communicate with their kids about any new procedures being taught and ensure they understand the safest course of action.
- Beau advocates for reevaluating the training methods for students in dealing with potentially dangerous situations.
- He questions the necessity of teaching middle schoolers about sign and countersign tactics.

# Quotes

- "You don't feel safe, get out."
- "Can't use information that is publicly visible like that."
- "Because anything that you're going to train the students to do, you're also probably training the suspect."
- "That won't work. That will go badly."
- "We should probably really start to re-evaluate stuff now that we're training kids how to deal with stuff like this."

# Oneliner

Law enforcement agencies need secure protocols to verify officers' identities without compromising safety in potential threat situations.

# Audience

Parents, educators, law enforcement

# On-the-ground actions from transcript

- Contact the Sheriff's Department if your child is being taught to ask for department name and badge number to verify an officer's identity (suggested).
- Communicate with your kids about the safer alternative where officers provide a unique number, not publicly visible, for verification before opening the door (suggested).
- Advocate for secure protocols in place to ensure students' safety in potential threat situations (implied).

# Whats missing in summary

Importance of maintaining safety protocols while verifying officers' identities in potential threat situations.

# Tags

#SafetyProtocols #LawEnforcement #Students #CommunitySafety #TrainingEvaluation