# Bits

Beau says:

- Updates on events in Canada, Alabama, and another location.
- Money raised for a homeless camp in Canada and children of minors on strike in Alabama.
- Money sent to Aaron for shopping in Canada; photos posted on Twitter.
- Heartwarming experience in Canada, where Beau was invited to have a meal.
- Beau drove to Brookwood, Alabama, for the minor strike.
- Signs of community support with yard signs all over town.
- Visited a meeting hall with organized supplies for strikers.
- Resemblance of the supply depots after Hurricane Michael.
- Importance of organization for long-term strikes.
- Stories shared by people who worked in the mine since the 1980s.
- Sense of community and support during strike rally.
- Lack of support from US politicians for the labor movement.
- History of labor movements in the United States being cyclical.
- Every kid who needed help received it during the events.
- Emphasis on community building and organization for a better society.

# Quotes

- "If you have the ability to foster that kind of camaraderie, that kind of community, that kind of network around you, it can only make your community better."
- "It's worth doing because you can weather storms, whether they be hurricanes or they be, well, other kinds of issues that impact the entire community."
- "Solidarity Santa came through. And you all played a part in that."
- "That's kind of the way the history of the labor movement in the United States works."
- "It makes it a whole lot easier to bargain for what you want and to create the kind of world you want."

# Oneliner

Beau catches up on events in Canada and Alabama, showcasing the power of community support and organization in times of need.

# Audience

Community organizers, activists, supporters

# On-the-ground actions from transcript

- Support local community initiatives (implied)
- Foster camaraderie and networks in your community (implied)
- Organize aid drives for those in need (implied)

# Whats missing in summary

The full transcript provides deeper insights into the importance of community solidarity and organization during challenging times.

# Tags

#CommunitySupport #LaborMovements #Solidarity #Organization #Activism