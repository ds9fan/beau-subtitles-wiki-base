# Bits

Beau says:

- Recent statistics comparing viewership in 2020 to 2021 show significant drops across major news outlets like CNN, Fox News, MSNBC, ABC, NBC, CBS, New York Times, and Washington Post.
- The decline in viewership could lead news outlets to produce more sensationalist content to attract viewers and boost ratings.
- News outlets, being businesses, need to increase viewership to stay profitable, possibly by focusing on sensational stories or expanding readership overseas.
- Anecdotal evidence from YouTube channels with over 50,000 subscribers shows an overall increase in viewership, possibly due to providing context and moving away from sensationalism.
- Americans in 2021 reportedly feel more comfortable and safer under President Biden compared to President Trump, potentially impacting how they respond to news and events.

# Quotes

- "News outlets are businesses. They have to make money."
- "Americans are more comfortable in 2021 than they were in 2020."
- "You watch the news because something scary happened."

# Oneliner

Recent stats show news viewership decline, leading to potential rise in sensational content; Americans reportedly more comfortable in 2021 under President Biden.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Support independent media outlets and platforms (suggested)
- Seek news sources that provide context and avoid sensationalism (suggested)

# Whats missing in summary

The full transcript provides a deeper insight into the changing landscape of news consumption and the potential impact of sensational content on viewers' perceptions and behaviors.

# Tags

#NewsConsumption #MediaTrends #Sensationalism #Viewership #PresidentBiden