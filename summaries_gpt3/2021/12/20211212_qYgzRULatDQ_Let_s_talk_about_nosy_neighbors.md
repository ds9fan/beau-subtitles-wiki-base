# Bits

Beau says:

- Explains a situation where neighbors on apps like Nextdoor brag about calling the police on people for minor reasons like driving up and down the street or sitting in a car for too long.
- Acknowledges the potential dangers of introducing armed police into non-threatening situations.
- Suggests curbing this behavior by appealing to self-interest or greed rather than solely relying on moral arguments.
- Points out that crime rates, not just crime itself, impact property values and that constant police presence can lower property values.
- Proposes that neighbors who excessively call the police may actually be harming property values in their own neighborhood.
- Recommends using monetary consequences as a deterrent for such behavior, as it may be more effective than moral appeals.
- Encourages looking up studies on the correlation between crime rates and property values to present factual evidence in such situations.

# Quotes

- "You should really only call the law if you think that lethality is an appropriate possible response."
- "Aside from that, that's happening to every house in the neighborhood."
- "If you could suggest that they could be removing tens of thousands of dollars of equity from themselves and all of their neighbors, they might stop."
- "It's statistics. It's the constant police presence. That's what drops property rates."
- "But if they call long enough on enough different people, there's going to be crime."

# Oneliner

Beau addresses nosy neighbors calling the police unnecessarily, suggesting monetary consequences over moral arguments to deter this behavior and protect property values.

# Audience

Community members

# On-the-ground actions from transcript

- Share statistical studies on crime rates and property values to raise awareness about the impacts of excessive police calls on property values (suggested).
- Encourage neighbors to reconsider their approach by discussing the potential financial consequences of their actions on property values (implied).

# Whats missing in summary

Importance of understanding the financial implications of excessive police calls on property values and neighborhood dynamics.

# Tags

#NosyNeighbors #CommunityPolicing #PropertyValues #CrimeRates #MonetaryConsequences