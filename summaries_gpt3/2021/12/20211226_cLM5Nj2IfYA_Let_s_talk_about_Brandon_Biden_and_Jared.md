# Bits

Beau says:

- Beau addresses the trend of saying "Let's go Brandon" as a way to express displeasure with President Biden within the conservative circle.
- He acknowledges that criticizing leaders, including President Biden, is valid but suggests doing it with actual criticism rather than juvenile chants.
- Beau finds humor in the irony of people who criticized liberals for saying "orange man bad" now using a similar chant against Biden.
- The incident that prompted the recent surge in "Let's go Brandon" chants involved President Biden responding casually to a family member saying it on a call during a Santa tracker event.
- Beau speculates on the various interpretations of Biden's response, ranging from being classy to being unaware of the context.
- He shares his belief that Biden's PR team likely informed him of the chant previously, but it may have been forgotten due to its irrelevance.
- The main takeaway from Beau is to not waste opportunities for meaningful communication like the individual, Jared, who said "Let's go Brandon" in a moment with a wide audience.
- He encourages thinking about what one might say if given a platform to reach millions and avoiding wasting such a chance.
- Beau expresses disappointment in the missed potential for Jared to advocate for something he deeply cares about instead of uttering a seemingly frivolous statement.
- In summary, Beau views the whole "Let's go Brandon" situation as irrelevant and a missed chance for constructive communication.

# Quotes

- "Don't be like Jared."
- "It's irrelevant. It's silly. It's a real-life version of Orange Man bad."

# Oneliner

Beau addresses the "Let's go Brandon" trend, advocating for meaningful communication instead of wasted opportunities like Jared's frivolous chant.

# Audience

Social media users

# On-the-ground actions from transcript

- Advocate for meaningful messages when presented with a platform (implied)
- Think about what you want to convey to millions before speaking in public (implied)

# Whats missing in summary

Beau's tone and delivery nuances are missing in the summary.

# Tags

#Brandon #PoliticalChant #Communication #Opportunity #Criticism