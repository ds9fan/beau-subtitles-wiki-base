# Bits

Beau says:

- Beau introduces a term that has been on his mind and expresses his intention to talk about religion, specifically the Christian religion, which he usually avoids discussing.
- He questions whether churches truly embody the teachings of Christ or serve as power structures with different motives.
- Beau prompts listeners to ponder if their churches encourage behaviors like feeding the hungry, welcoming strangers, caring for the needy, visiting the sick and those in prison, loving thy neighbor, and forgiveness.
- He contrasts these teachings with potential negative attitudes encountered in churches, such as stigmatizing welfare recipients, advocating for walls, dismissing the needy, and lacking forgiveness.
- Beau challenges the perception of the United States as a Christian nation and suggests that churches should advocate for Christ's teachings in government policies.
- He concludes by questioning if churches are genuinely Christian or merely existing as post-Christian power structures focused on socialization rather than embracing Christ's teachings.

# Quotes

- "Does your church encourage forgiveness? Or does it encourage you to hold a grudge?"
- "If you are a Christian, this is something you're going to have to reconcile."
- "Is it a Christian church? Or is it post-Christian?"
- "Suddenly I have an image of somebody walking around flipping over tables and chasing people with a scourge."
- "It is far easier and far more marketable if you give permission to be your worst rather than provide encouragement to be your best."

# Oneliner

Beau raises thought-provoking questions about whether churches truly embody the teachings of Christ or function as post-Christian power structures, urging Christians to reconcile teachings with church practices.

# Audience

Christians

# On-the-ground actions from transcript

- Question whether your church truly embodies Christ's teachings (implied)
- Advocate for church practices that genuinely encourage Christ-like behaviors (implied)
- Engage in introspection regarding the alignment of church teachings with personal beliefs and actions (suggested)

# Whats missing in summary

Beau's emotional delivery and emphasis on self-reflection can be best experienced by watching the full video.