# Bits

Beau says:

- Walmart is keeping its starting wage at $11 an hour, but aiming to increase the average wage to $15 an hour.
- The company's revenue grew by 7.3% last year, with e-commerce growing by 69%.
- Despite the revenue growth, Walmart's stock prices fell, which was seen as bad news on Wall Street.
- Walmart stated that raising the minimum wage to $15 an hour should benefit the economy, but Beau disagrees.
- Beau argues that the minimum wage is meant to benefit the worker, not the corporations.
- Walmart's CEO, McMillan, acknowledged that customers spend more when they receive stimulus money, benefitting the economy.
- Walmart wants the government to provide stimulus while not committing to raise their minimum wage.
- Beau believes that if a company cannot pay a living wage, it has no right to exist in the United States.

# Quotes

- "If you can't pay a living wage, the company has no right to exist in the United States."
- "It's to benefit the worker, not the corporations."
- "Walmart wants the government to pick up the slack."
- "They have to make money."
- "I wish my earnings increased by only 69%."

# Oneliner

Walmart aims to raise average wage to $15 but faces backlash for not increasing the minimum wage, prompting debates on corporate responsibility and worker benefits.

# Audience

Workers, advocates

# On-the-ground actions from transcript

- Advocate for fair wages in your workplace (implied)
- Support policies that prioritize worker well-being over corporate profits (implied)
- Stay informed and engaged in the debate on minimum wage increases (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of the debate around corporate responsibilities and worker benefits in relation to minimum wage policies.

# Tags

#Walmart #MinimumWage #CorporateResponsibility #WorkerRights #Economy