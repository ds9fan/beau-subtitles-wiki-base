# Bits

Beau says:

- Rochester, Rochester PD incident involving pepper spraying a 9-year-old child.
- Child was handcuffed and pepper sprayed during a family problem.
- Union defended the use of pepper spray, citing difficulties in getting people into cars.
- Beau criticizes the union for justifying the use of force on a child.
- Police officers failed to handle the situation appropriately without using pepper spray.
- Lack of empathy and understanding shown towards the child's traumatic experience.
- Beau questions the need for such excessive force on a child in this situation.
- Suggests alternative de-escalation techniques like using stuffed animals or talking.
- Points out the cultural issues within the department and the problematic defense by the union.
- Calls for a reevaluation of law enforcement practices towards consent-based policing.

# Quotes

- "You pepper sprayed a handcuffed kid in a traumatic situation. Congratulations."
- "We as a society really have to look at how we deal with law enforcement in this country."
- "Sure, yes, it is early. There is the possibility that information may come forward that changes my mind, but that seems super unlikely."
- "You talk and get them out of the vehicle."
- "We should probably move to consent based policing."

# Oneliner

Rochester PD pepper-spraying a 9-year-old child raises questions about policing practices and the need for consent-based approaches.

# Audience

Community members, advocates

# On-the-ground actions from transcript

- Advocate for consent-based policing (implied)
- Support mental health professionals (implied)

# Whats missing in summary

Full details and emotional impact can be better understood by watching the full video.

# Tags

#RochesterPD #PoliceBrutality #ConsentBasedPolicing #CommunityPolicing #Advocacy