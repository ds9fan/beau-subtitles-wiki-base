# Bits

Beau says:

- Media headlines on the southern border crisis seem to exaggerate the situation, using words like "surge," "challenge," "emergency," and "crisis."
- Large numbers of people showing up at the southern border have been a continuous issue for almost 20 years, becoming a problem only under specific administrations.
- The previous administration used the situation as a pretext to build a border wall, while the new administration claims that America is back, which should mean handling the situation without panic.
- Beau urges the Biden administration to honor international agreements, embrace refugees, and provide non-military assistance to countries of origin affected by failed US foreign policy.
- Transitioning from a world policeman to a world EMT could be a more effective and humane approach than building walls or increasing enforcement.
- Beau advocates for allowing people in while working on necessary actions, mentioning that the United States should not fear immigrants but instead offer assistance and support.
- He concludes by stating that the situation at the southern border is not a crisis or a challenge if the Biden administration's promises and rhetoric are to be believed.

# Quotes

- "Large numbers of people have been showing up at one time at that southern border for almost 20 years."
- "This is not a crisis. This is not a challenge."
- "The only aliens we're afraid of are the ones that chase Sigourney Weaver."

# Oneliner

Media headlines exaggerate southern border issues; Beau calls for humane responses and assistance, not panic.

# Audience

Policy Makers, Activists, Advocates

# On-the-ground actions from transcript
- Provide non-military assistance to countries of origin (exemplified)
- Transition from being the world's policeman to being the world's EMT (implied)

# Whats missing in summary

Beau's engaging delivery and nuanced perspective on immigration issues can be best understood by watching the full transcript.

# Tags

#Immigration #SouthernBorder #Refugees #USForeignPolicy #BidenAdministration