# Bits

Beau says:

- Addressing HR 127 and the concerns surrounding it.
- People from both sides view the bill as bad.
- HR 127 requires licensing, training, and insurance for gun owners.
- The bill poses logistical challenges, such as requiring individuals with future issues to report themselves.
- The financial burden of insurance disarms poor individuals.
- The bill does not address closing the domestic violence loophole.
- Assuring pro-2nd Amendment individuals that the bill won't pass the Senate.
- Mentioning that the bill won't achieve its intended goal.
- Warning Democrats that supporting this bill may cost them in the 2022 House elections.
- Emphasizing that the bill is unlikely to pass the Senate and could become a campaign tool for Republicans.
- Suggesting that the bill should be withdrawn before gaining more attention.

# Quotes

- "It is as bad as it seems."
- "It disarms poor people. That's what this bill does."
- "This will never make it through the Senate under any circumstance."
- "It becomes a crime for poor people, but if you have money, it's okay."
- "The bill should probably be withdrawn before it gets too far and too much attention gets drawn to it."

# Oneliner

Beau addresses concerns about HR 127, describing it as disarming poor individuals and unlikely to pass the Senate, cautioning Democrats about potential repercussions in the 2022 House elections.

# Audience

Advocates, voters, lawmakers

# On-the-ground actions from transcript

- Engage in advocacy efforts to raise awareness about the implications of HR 127 (suggested).
- Contact your representatives to express concerns about the bill (exemplified).
- Join local gun rights or gun regulation advocacy groups to stay informed and engaged (implied).

# Whats missing in summary

Deeper analysis on the potential impacts of HR 127 on marginalized communities and the importance of grassroots activism in influencing gun legislation.

# Tags

#HR127 #GunControl #PoliticalAnalysis #PolicyImpact #DemocraticParty