# Bits

Beau says:

- Addressing the impeachment trial and the recent vote in the Senate.
- Contrasting what the proceedings should be versus what they actually are.
- Expressing concern over senators meeting with the defense and withholding information.
- Pointing out that senators are not impartial as required by the Constitution.
- Mentioning how senators may vote based on political interests rather than facts.
- Urging average Republicans to take issue with their representatives' actions.
- Emphasizing the importance of upholding the Constitution and the oath within it.
- Calling for accountability by suggesting that senators violating their oath should be primaried.
- Encouraging individuals to act on their beliefs and convictions.
- Stressing that beliefs should translate into tangible actions for them to hold true value.

# Quotes

- "Those who require your consent, your support, those who represent you are doing everything within their power to show you that they don't believe it matters at all."
- "If you believe the Constitution matters, those who behaved in this manner cannot be reelected by you because your convictions, your beliefs are utterly worthless unless they motivate your actions."
- "If you still get chills when you hear the star-spangled banner, if you think it's a travesty that people don't recite the Pledge of Allegiance, you cannot simply look the other way as those senators openly and flagrantly violate their oaths."

# Oneliner

Addressing the impeachment trial and Senate vote, Beau underscores the disparity between what should be impartial proceedings and what actually transpires, urging accountability based on constitutional principles.

# Audience

Average Republicans

# On-the-ground actions from transcript

- Hold senators accountable through primary elections (suggested)
- Vote out senators who violated their oath (suggested)

# Whats missing in summary

Full context and detailed analysis of the impeachment trial proceedings and the implications of senators' actions.

# Tags

#Impeachment #Senate #Accountability #Constitution #Actions