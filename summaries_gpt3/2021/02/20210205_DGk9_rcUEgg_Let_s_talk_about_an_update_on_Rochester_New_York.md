# Bits

Beau says:

- Provides an update on the situation in Rochester, New York and mentions state legislators taking action.
- Talks about a bill introduced to ban the use of sprays against minors entirely.
- Acknowledges the good intentions behind the bill but questions its effectiveness in addressing excessive force.
- Shares a story about a former deputy's reaction to the footage and the potential escalation of force if one tool is taken away.
- Argues that excessive force is any force beyond the minimum necessary for an arrest, suggesting addressing attitudes rather than specific tools.
- Proposes legislation requiring random body camera footage checks and consequences for officers using excessive force.
- Appreciates the legislators' intentions but stresses the importance of effective legislation to address the root issue.

# Quotes

- "Excessive force is any force in excess of the minimum necessary to affect the arrest."
- "The issue isn't that a specific type of force was used. The issue is that force was used."
- "Go after the attitude."
- "Their heart is definitely in the right place. Now we just have to get the legislation there."
- "Y'all have a good day."

# Oneliner

Beau provides insights on addressing excessive force by focusing on attitudes over tools, urging for legislation to require random body camera footage checks.

# Audience

Legislators, activists, community members

# On-the-ground actions from transcript

- Advocate for legislation requiring random body camera footage checks and consequences for officers using excessive force (suggested)
- Support initiatives that focus on addressing attitudes towards the use of force within law enforcement (suggested)

# Whats missing in summary

Further details on the specific steps required to push for legislative changes and effectively address issues of excessive force and police conduct.

# Tags

#Rochester #Legislation #ExcessiveForce #PoliceReform #CommunityPolicing