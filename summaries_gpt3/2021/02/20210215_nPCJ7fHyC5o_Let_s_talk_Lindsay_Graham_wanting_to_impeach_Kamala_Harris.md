# Bits

Beau says:

- Senator Lindsey Graham suggested impeaching Vice President Harris if Republicans retake the House in 2022, citing her support for bail funds as equivalent to the former president's actions.
- He explains what bail funds are, accounts set up to help individuals post bail beyond their financial means so they can participate in their own defense.
- Beau argues that denying bail should be reserved for genuine flight risks or threats to society, not for those with bail set beyond their financial reach.
- He points out that excessive bail is against the Constitution, specifically the 8th Amendment, and supports Vice President Harris for backing a constitutional principle.
- Beau criticizes the Republican Party for allegedly abandoning founding principles, coordinating with defense in trials, and ignoring impartiality oaths.
- He challenges the potential impeachment of Vice President Harris as a partisan move that most Americans see through, suggesting it will only appeal to a small faction.
- Beau questions the selective targeting of Vice President Harris for impeachment, a woman of color, compared to the lack of action against an older white male.
- He concludes by expressing his thoughts and wishing everyone a good day.

# Quotes

- "Please by all means have your lackeys in the House impeach the Vice President because she tweeted support for a constitutional premise."
- "The majority of the United States sees right through it."
- "Just the woman who isn't white."
- "Play your silly little game."
- "Y'all have a good day."

# Oneliner

Senator Lindsey Graham's suggestion to impeach Vice President Harris for supporting bail funds is criticized as a partisan move that goes against constitutional principles, with Beau questioning the selective targeting based on race.

# Audience

Voters, political activists

# On-the-ground actions from transcript

- Challenge partisan moves in politics (implied)
- Support constitutional principles (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the implications of potentially impeaching Vice President Harris based on her support for bail funds, questioning the partisan nature and racial implications of such actions.

# Tags

#Impeachment #Partisanship #ConstitutionalPrinciples #BailFunds #SelectiveTargeting