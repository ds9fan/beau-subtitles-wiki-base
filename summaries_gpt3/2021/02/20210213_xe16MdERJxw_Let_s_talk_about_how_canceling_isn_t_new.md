# Bits

Beau says:

- Explains the phenomenon of cancel culture and how it's not a new concept in the United States.
- Talks about how some people are now negatively impacted by cancel culture after benefiting from it for a long time.
- Describes how those who have lost privilege due to cancel culture view it as oppression.
- Attributes the impact of cancel culture to basic math and the shift in societal beliefs.
- Points out that companies, driven by profit, are the ones making decisions on canceling individuals based on public statements.
- Notes that older individuals with outdated viewpoints are becoming less valuable consumers for companies compared to younger, progressive individuals.
- Mentions historical instances of pre-cancellation, such as discriminating against LGBTQ individuals in the workplace.
- Addresses the misconception that cancel culture is a new leftist plot, explaining it as a result of American capitalism.

# Quotes

- "It's just math."
- "Who decides who gets canceled? Companies. Large corporations."
- "They're obsolete viewpoints."
- "It's not new. It just used to work in their favor."
- "What they're complaining about is not some leftist plot. It is good old-fashioned American capitalism at work."

# Oneliner

Beau outlines the historical context and economic motivations behind cancel culture in the US, debunking misconceptions about oppression and leftist plots.

# Audience

Activists, Progressives, Consumers

# On-the-ground actions from transcript

- Educate peers on the historical context of cancel culture (implied)
- Support companies that prioritize inclusive values (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of cancel culture and its historical roots, offering a fresh perspective on the current discourse.

# Tags

#CancelCulture #Capitalism #Beliefs #Inclusivity #SocialChange