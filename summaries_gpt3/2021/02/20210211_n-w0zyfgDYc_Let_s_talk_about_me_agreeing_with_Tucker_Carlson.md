# Bits

Beau says:
- Analyzing Tucker Carlson's assessments and effectiveness.
- Mentioning the importance of looking at assessments from those who oppose you for a clearer picture.
- Critiquing Tucker Carlson's wild, theory-laden rant from the previous night.
- Pointing out Tucker Carlson's contradictory statement of not speculating while leading his audience to speculate.
- Refusing to focus on Carlson's truth-seeking quest and speculations that may arise from it.
- Addressing a particular passage where Carlson acknowledges the effectiveness of BLM.
- Noting the significant impact BLM and their corporate sponsors had on the country from Memorial Day onwards.
- Correcting Carlson's statement on the time frame of change brought about by BLM.
- Agreeing with Carlson on the necessity and positive nature of the change brought by BLM.
- Emphasizing the importance of change over stagnation in a society.

# Quotes

- "Their assessments are going to be more accurate because they're going to point to your weaknesses and your strengths."
- "Here we have Tucker Carlson admitting to BLM's effectiveness."
- "I agree with Tucker Carlson."
- "If I look back 50 years into history and my country hasn't changed, something is wrong."
- "This change is overdue, needed, and good."

# Oneliner

Beau analyzes Tucker Carlson's contradictory assessments, focusing on Carlson acknowledging the effectiveness of BLM and the necessity of overdue change in society.

# Audience

Media consumers

# On-the-ground actions from transcript

- Dissect contradictory statements in media (analyzed)
- Acknowledge and support overdue societal change (implied)

# What's missing in summary

Beau's engaging delivery and nuanced perspective on media assessments and societal change. 

# Tags

#TuckerCarlson #BLM #SocietalChange #MediaAnalysis #OppositionAssessments