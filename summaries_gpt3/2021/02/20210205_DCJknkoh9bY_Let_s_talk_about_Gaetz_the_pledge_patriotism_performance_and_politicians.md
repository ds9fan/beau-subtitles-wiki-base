# Bits

Beau says:

- Tells a story of a civilian and a colonel having a significant exchange about patriotism in the early 2000s.
- Describes how the civilian believed patriotism was about action, not just symbols.
- Recalls the civilian's 30 years of service to the country, which gave weight to his perspective on patriotism through action.
- Mentions Republicans led by Matt Gaetz expressing outrage over a committee's decision not to start meetings with the Pledge of Allegiance.
- Points out that members of Congress can demonstrate patriotism through daily actions rather than performative rituals.
- Criticizes politicians for prioritizing publicity stunts over putting the country and its people first.
- Emphasizes that true patriotism is displayed through actions, not mere words or symbolic gestures.

# Quotes

- "Patriotism is shown through action, not through worship of symbols."
- "You show your patriotism through action, not through publicity stunts."
- "Put the country and the people of this country above your own political interests."

# Oneliner

Beau shares a story from the early 2000s illustrating that patriotism is about action, not symbols, criticizing politicians for prioritizing performative gestures over genuine care for the country and its people.

# Audience

Politically conscious individuals

# On-the-ground actions from transcript

- Prioritize actions that benefit the country and its people over performative gestures (implied)

# Whats missing in summary

The full transcript provides deeper insights into the importance of genuine patriotism through actions rather than symbols alone.