# Bits

Beau says:

- Neera Tanden is Biden's nominee to run the budget and executive branch.
- Tanden faces criticism from all sides, including the far right and Republicans for her Twitter behavior.
- The left dislikes Tanden for being status quo and against Medicare for all.
- Biden promised no fundamental change in the economy, which Tanden represents.
- Beau doesn't see the need to talk about Tanden as her positions are well known.
- Biden's economic policies are not expected to be super progressive.
- Beau believes whoever comes after Tanden will be similar in policy stance.
- The lack of deep systemic change under Biden is not surprising.
- Beau suggests there's no real story with Tanden as her positions are in line with Biden's promises.
- Deep systemic change is unlikely in the economy under Biden.

# Quotes

- "Dog bites man. This is exactly what was promised. It's exactly what was delivered."
- "There's not really a story here. This is who she is."
- "You're not going to get deep systemic change on this front from Joe Biden."

# Oneliner

Beau breaks down why Neera Tanden's nomination and policies are not surprising under Biden's administration.

# Audience

Policymakers, Political Analysts

# On-the-ground actions from transcript

- Advocate for deep systemic change in economic policies (implied)
- Stay informed on political appointments and policies (implied)

# Whats missing in summary

Insights on Beau's perspective and analysis on the lack of surprises in Neera Tanden's nomination and policies.

# Tags

#NeeraTanden #JoeBiden #EconomicPolicies #SystemicChange #PoliticalAnalysis