# Bits

Beau says:

- Beau expresses surprise at the revelation of parents being able to pull their kids out of Black History Month curriculum in schools.
- The history of Black History Month dates back to 1926 when historian Carter Woodson proposed a week to study black history.
- In 1970, Black History Month was extended to a full month, starting from January 2nd to February 28th.
- Beau points out that objecting to Black History Month means never being able to say "get over slavery" again.
- He challenges the idea of opting out of certain history classes for civil rights reasons, using algebra as an example.
- Beau criticizes the notion of parents deciding to exclude their children from learning specific aspects of history, labeling it as wrong.
- He suggests that Black History Month can only end when white individuals stop being upset about it.
- Beau stresses the importance of a comprehensive world history curriculum in schools to combat Eurocentrism and misinformation.
- History education is vital, and Beau urges against limiting children's exposure to different historical perspectives.
- He concludes by encouraging viewers to think about the implications of restricting historical education for children.

# Quotes

- "If Black History Month bothers you, you never get to say, get over slavery again, ever."
- "The funny part about this is, do you know when Black History Month can end? When white folks stop getting mad about it."
- "History is important."
- "It is incredibly important to yank your kid out of something like this."
- "You are choosing to make certain your child is ignorant of things they need to know."

# Oneliner

Beau challenges objections to Black History Month, advocating for comprehensive historical education and condemning the exclusion of vital knowledge from children.

# Audience

Parents, educators, activists

# On-the-ground actions from transcript

- Teach comprehensive world history in schools (suggested)
- Advocate for inclusive and diverse historical curriculums (suggested)
- Emphasize the importance of history education to parents and educators (suggested)

# Whats missing in summary

Deeper exploration of the impact of limited historical education on children's understanding of societal issues and the importance of diverse perspectives in shaping a well-rounded worldview.

# Tags

#BlackHistoryMonth #Education #History #Inclusivity #Activism