# Bits

Beau says:

- Proposes better solutions for housing children instead of locking them up in camps.
- Advocates for understanding resources available and coming up with a more humane plan.
- Suggests processing unaccompanied teens quickly and efficiently at the border.
- Emphasizes the need to differentiate between asylum seekers and high-initiative individuals seeking a better life.
- Presents three housing options: Hilton hotels, boarding schools, and sponsorship/fostering.
- Points out the moral and practical flaws in the current system.
- Addresses potential objections and offers solutions to ensure the well-being and safety of the children.
- Critiques the current system as driven by fear and habit rather than logic or compassion.

# Quotes

- "We can come up with a better plan. We can do better."
- "It's cheaper to be a good person."
- "There's no reason to detain them. There's no reason to put them in camps."
- "We just have to get over that fear."
- "We can do better."

# Oneliner

Beau advocates for humane solutions to housing children at the border, criticizing the current fear-driven system and proposing alternatives like Hilton hotels, boarding schools, and sponsorship/fostering.

# Audience

Policy makers, activists, community leaders

# On-the-ground actions from transcript

- Contact local embassies and offer support in processing unaccompanied teens at the border (suggested)
- Reach out to boarding schools to inquire about accommodating high-initiative individuals seeking asylum (suggested)
- Advocate for sponsoring or fostering unaccompanied teens in your community (suggested)

# Whats missing in summary

The full transcript provides a detailed breakdown of the inefficiencies and moral shortcomings of the current system, urging for a more compassionate and logical approach towards housing children at the border. Watching the full transcript will provide a comprehensive understanding of Beau's proposed solutions and the reasoning behind them.

# Tags

#Immigration #ChildDetention #BorderCrisis #HumaneSolutions #CommunityEngagement