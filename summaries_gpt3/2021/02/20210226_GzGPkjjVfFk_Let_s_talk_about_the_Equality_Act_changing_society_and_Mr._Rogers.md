# Bits

Beau says:

- Introduces the topic of the Equality Act and its purpose to extend basic protections to marginalized groups in the United States.
- Notes that the Equality Act, having passed in the House, faces a challenge in the Senate where it is likely to be filibustered.
- Explains that Democrats need 60 votes to pass the Equality Act in the Senate, which may not happen.
- Comments on how Republicans have weaponized the Equality Act as a talking point to rally their base against marginalized groups.
- Recalls a scene from 1969 related to desegregation of pools as an example of societal change not just through laws but also by changing people's mindset.
- Urges individuals to take action in their daily lives to create a more inclusive and accommodating environment for marginalized communities.
- Encourages activities like advocating for inclusive hiring practices to make society more accepting without solely relying on legislation.
- Stresses the importance of individuals sending messages, normalizing inclusivity, and breaking stigmas to bring about societal change.
- Asserts that changing societal norms doesn't always require changing laws but focuses on changing people's perspectives.
- Concludes by affirming that individuals can drive change and create a more inclusive society without solely depending on legislative actions.

# Quotes

- "To change society, you don't have to change the law."
- "We don't have to wait on Capitol Hill."
- "We have to send the message as individuals."
- "You have to change the way people think."
- "Because it will become the norm without the legislation."

# Oneliner

Beau underscores the power of individual actions in creating societal change, advocating for inclusivity beyond legislation.

# Audience

Advocates, Activists, Citizens

# On-the-ground actions from transcript

- Advocate for inclusive hiring practices (implied)
- Engage in activities that foster a more accepting environment (implied)
- Send messages of inclusivity in daily life (implied)
- Normalize inclusivity and break stigmas (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of the historical context of civil rights legislation and the significance of societal mindset shifts in creating lasting change. 

# Tags

#EqualityAct #SocietalChange #Inclusivity #MarginalizedCommunities #IndividualActions