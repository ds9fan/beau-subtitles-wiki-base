# Bits

Beau says:

- Beau introduces the topic of two schools of thought on deep systemic change in the country.
- Reformists advocate using the system's machinery to address issues through voting, petitions, and campaigning for politicians.
- They focus on policies that reduce harm and enhance social safety nets.
- Revolutionaries, on the other hand, prioritize building new infrastructure and systems over electoral processes.
- They are more hands-on in addressing issues directly and creating solutions like co-ops and community networks.
- Beau argues that these two groups are not necessarily in opposition but are heading towards the same goal.
- He presents a metaphor of buses moving at different speeds but in the same direction to illustrate the relationship between reformists and revolutionaries.
- Beau stresses the importance of both short-term (reformist) and long-term (revolutionary) goals in achieving societal change.
- He points out the need for reformists to recognize areas that need change, which is often brought to light by revolutionaries.
- Similarly, revolutionaries require popular support and infrastructure usage from reformists to make a substantial impact.
- Beau warns against echo chambers on social media, urging a realistic assessment of support for ideas.
- He underscores the effectiveness of building new systems rather than solely opposing the existing one to drive societal change.
- The duty of revolutionaries is to eliminate objections and build infrastructure to garner popular support for their cause.
- Beau concludes by leaving the audience with his thoughts and well wishes.

# Quotes

- "The reformist says, well, you know, that's nice. That location you want to get to, that's nice. But it can't happen right now."
- "The reality is both schools of thought need the other."
- "The duty of the revolutionary is to get rid of those objections, to build the infrastructure, to overcome those pragmatic reasons."

# Oneliner

Beau presents two schools of thought on systemic change: reformists work within the system, while revolutionaries focus on building new infrastructure, both aiming for the same goal.

# Audience

Social Activists

# On-the-ground actions from transcript

- Start a community network to address local issues (exemplified)
- Get involved in building infrastructure like co-ops and intentional communities (exemplified)
- Support policies that reduce harm and improve social safety nets (exemplified)

# Whats missing in summary

The full transcript provides a detailed exploration of the dynamics between reformists and revolutionaries in driving societal change.

# Tags

#SystemicChange #Reformists #Revolutionaries #InfrastructureBuilding #CommunityNetworks