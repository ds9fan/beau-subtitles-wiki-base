# Bits

Beau says:

- Addresses the misconception that the US is solely responsible for global issues, like accepting refugees.
- Contrasts the number of refugees Colombia has accepted with the US's intake.
- Colombia extended legal status to around 600,000 to 1.7 million refugees from Venezuela during an economic downturn.
- Points out that Colombia has a GDP of about 320 billion, making their efforts notable.
- Mentions the US's GDP of 21.4 trillion, showing a stark comparison.
- Walmart's revenue of 520 billion is given as a reference point.
- Notes that economic refugees seek safety and better economic opportunities, not necessarily to come to the US.
- Colombia not only offers legal status but also employment status and healthcare to refugees.
- Criticizes the US for not meeting its moral responsibility to the world.
- Biden's cap of 125,000 refugees was considered ambitious, compared to Colombia's actions.

# Quotes

- "We're not in it alone. It's not always us. They don't always want to come here."
- "The United States is failing in its moral responsibility to the world."
- "Colombia just isn't extending the legal status like residency. They're getting employment status as well and health care."

# Oneliner

The US must recognize its global responsibilities in light of Colombia's extensive refugee support, revealing American exceptionalism's flaws.

# Audience

Global citizens, policymakers

# On-the-ground actions from transcript

- Advocate for increased refugee support in your community (implied).
- Support organizations aiding refugees locally (implied).

# Whats missing in summary

The emotional impact of recognizing the disparity in refugee support between countries and the call to action for a more equitable distribution of responsibility globally.

# Tags

#Refugees #GlobalResponsibility #USPolicy #AmericanExceptionalism #Colombia #GDP