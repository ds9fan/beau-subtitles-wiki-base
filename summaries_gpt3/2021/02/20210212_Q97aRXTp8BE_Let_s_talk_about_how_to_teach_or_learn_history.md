# Bits

Beau says:

- A mom is stressed about teaching history to her child.
- The mom feels like she's failing in the role of being a teacher.
- History teachers often face challenges in getting students interested in history.
- Beau suggests focusing on the "why" in history, as it makes it interesting and tells the story.
- He advises encouraging kids to ask why things happened in history.
- Beau mentions that the school may provide a curriculum, but it's not compulsory to follow it.
- He introduces the "Scooby-Doo method" of teaching history, focusing on mysteries and engaging storytelling.
- Beau recommends starting with dramatic events like D-Day to grab the child's attention.
- Teaching history in reverse chronological order can keep the learner interested with the curiosity of why.
- Understanding the reasons behind historical events helps in comprehending future motives.

# Quotes

- "If you can understand the whys of yesterday, you're going to understand the motives of tomorrow."
- "That is something that history teachers have an issue with."
- "The why is what makes it interesting. Those are the juicy parts."
- "Start with something that you can get their attention with."
- "You're doing fine. You're good."

# Oneliner

A stressed mom struggling to teach history gets guidance on making it interesting by focusing on the "why" and using engaging methods like the "Scooby-Doo method."

# Audience

Parents, educators

# On-the-ground actions from transcript

- Try encouraging kids to ask "why" about historical events (suggested).
- Use engaging storytelling and dramatic events to teach history (suggested).
- Focus on understanding the motives behind historical events (suggested).

# Whats missing in summary

The full transcript provides detailed guidance on teaching history effectively and engagingly, focusing on the importance of understanding the "why" in historical events.

# Tags

#Education #Teaching #History #Parenting #Engagement