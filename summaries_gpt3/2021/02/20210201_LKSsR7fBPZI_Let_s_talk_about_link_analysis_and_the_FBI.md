# Bits

Beau says:

- Explains the concept of link analysis in investigations prompted by a former assistant director of the FBI mentioning it on MSNBC.
- Describes how link analysis is portrayed in movies with pictures on a wall and different colored strings connecting them.
- Addresses the assumption that there is a criminal connection between members of Congress and those responsible for activities on the 6th.
- Emphasizes that link analysis doesn't necessarily mean criminal conduct but looks for shared motivations or sympathies.
- Mentions the historical context in the United Kingdom where separate groups shared goals and kept each other informed.
- Clarifies that link analysis aims to identify conduits between peaceful and potentially criminal actors.
- Raises the possibility that individuals may have unknowingly broken the law due to misconceptions.
- Warns against advocating for military involvement in law enforcement functions within the United States.
- Stresses the importance of upholding rights and principles in combating movements like Trumpism.
- Urges to be anti-authoritarian and maintain the presumption of innocence during investigations.

# Quotes

- "You can't give up those rights, surrender those rights, ignore those rights to combat those people or they won."
- "Just because your name's on the board doesn't mean you did anything wrong."
- "If you use the military to do this, they won."
- "During the investigation, during everything that comes out, we have to uphold the rights and the principles that they attacked."
- "The underlying theme of why the Sixth was so wrong is because it is widely seen as an attack on basic principles and rights within the United States."

# Oneliner

Beau explains link analysis, clarifies misconceptions, and warns against military involvement in law enforcement, stressing the importance of upholding rights to combat authoritarian movements like Trumpism.

# Audience

Investigators, activists, concerned citizens

# On-the-ground actions from transcript

- Uphold rights and principles during investigations (implied)
- Maintain presumption of innocence for individuals involved in investigations (implied)
- Advocate against military involvement in law enforcement functions within the United States (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of link analysis in investigations, addresses misconceptions, and underscores the significance of preserving rights and principles in combating authoritarian movements like Trumpism.

# Tags

#LinkAnalysis #Misconceptions #PresumptionOfInnocence #Rights #AntiAuthoritarian #Trumpism