# Bits

Beau says:

- Democrats want to increase the minimum wage, but it's unlikely to happen.
- The Senate parliamentarian ruled that using budget reconciliation for a minimum wage increase is outside the scope.
- Republicans are unlikely to support a minimum wage increase, especially at $15 an hour.
- Democrats may need to compromise and lower the proposed increase to $11 or $12 to have a chance of passing.
- A minimum wage increase is no longer a realistic topic until at least the midterms, and even then, it's uncertain.
- The rest of the bill may progress without the minimum wage increase.
- Movement on the minimum wage increase is unlikely for quite a while.

# Quotes

- "The odds of them pushing this through, especially at $15 an hour, are slim to none."
- "There's probably not going to be any movement on that for quite a while."

# Oneliner

Democrats push for a minimum wage increase, but obstacles make it unlikely, especially with Republican opposition.

# Audience

Policy advocates

# On-the-ground actions from transcript

- Advocate for a minimum wage increase in your community (implied)
- Support organizations working towards fair wages (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of the challenges facing the proposed minimum wage increase and the political dynamics that hinder its passage.