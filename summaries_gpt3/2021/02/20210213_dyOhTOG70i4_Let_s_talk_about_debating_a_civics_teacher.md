# Bits

Beau says:

- Beau introduces the topic of philosophy, sparked by a student wanting to debate a closely held belief with their teacher.
- The student questions the teacher's statement that governmental laws are the only way to achieve societal order.
- Beau argues that governmental laws do not achieve societal order; instead, it is the monopoly on violence that enforces order.
- He challenges the idea that government is separate from society and asserts that societal order comes from the monopoly on violence granted to the government.
- Beau proposes an alternative society where societal order is based on people abiding by the golden rule without the need for laws or violence.
- He envisions a society where individuals prioritize the interests of others over selfishness, believing it is possible but requires significant effort and education.
- Beau questions whether this alternative societal structure can be scaled up and acknowledges the potential challenges and arguments against it.
- He presents a scenario where borders dissolve, and nation-states disappear, exploring potential outcomes ranging from cooperation to the rise of violence-driven monopolies.
- Beau contemplates the dynamics between geographic areas and how power struggles may unfold in the absence of a centralized system.
- He concludes by suggesting that attempts at self-organization could potentially lead back to the current system of societal governance.

# Quotes

- "It's not laws. It's a monopoly on violence."
- "Societal order can exist without governmental law and without violence."
- "It's just a thought. Have a good day."

# Oneliner

Beau challenges the belief that governmental laws are the sole path to societal order, advocating for a society based on the golden rule instead of violence.

# Audience

Philosophy enthusiasts, educators, policymakers

# On-the-ground actions from transcript

- Advocate for community-based approaches to societal order (implied)
- Educate and instill values like the golden rule in society (implied)

# Whats missing in summary

Deeper exploration of the potential challenges and implications of transitioning to a society based on mutual respect and adherence to ethical norms.

# Tags

#Philosophy #SocietalOrder #Government #GoldenRule #Violence #Community-Based