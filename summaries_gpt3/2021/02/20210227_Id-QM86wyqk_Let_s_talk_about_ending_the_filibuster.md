# Bits

Beau says:

- Addressing the topic of changing the filibuster, citing significant public interest after recent events.
- Disputing the argument that the filibuster should be upheld simply due to tradition or Senate norms.
- Explaining the accidental creation of the filibuster, debunking surrounding myths.
- Outlining two main options for changing the filibuster: abolishing it entirely or implementing a speaking filibuster.
- Expressing concern that abolishing the filibuster could enable extreme policy changes when the opposition gains power.
- Countering that argument by proposing that showcasing successful policies without the filibuster could politically damage opponents.
- Advocating for a speaking filibuster, predicting it may reveal politicians' true thoughts and provide transparency on issues like the Equality Act and minimum wage.
- Acknowledging the need for government restraint but urging progress on social issues by altering or eliminating the filibuster.

# Quotes

- "Doing something simply because it's always been done that way, that's a really bad reason to continue doing something."
- "There's no doubt about that. But there is the law of unintended consequences that needs to be thought about."
- "A little bit of gridlock in government is good."
- "I think it needs to be a change."
- "Altering the filibuster, even getting rid of it, that would help move us along."

# Oneliner

Beau addresses changing the filibuster, proposing drastic options, and advocating for transparency through a speaking filibuster to progress on social issues.

# Audience

Policy advocates

# On-the-ground actions from transcript

- Advocate for transparency in government by supporting the implementation of a speaking filibuster (implied).
- Engage in debates and dialogues on the necessity of changing or abolishing the filibuster within political circles (implied).

# Whats missing in summary

The full transcript provides deeper insights into the potential consequences of altering or eliminating the filibuster and encourages critical thinking regarding government transparency and progress on social issues. 

# Tags

#Filibuster #Government #Transparency #PolicyChange #SocialIssues