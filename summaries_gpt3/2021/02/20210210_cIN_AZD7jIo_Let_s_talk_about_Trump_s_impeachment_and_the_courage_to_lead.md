# Bits

Beau says:

- Impeachment part two, day one, and the courage to lead are discussed.
- Leadership qualities are emphasized, including being willing to lead by example.
- A video is mentioned that lays out the case against the former president.
- Analysis of the former president's speech encouraging fighting and inciting the crowd is provided.
- Trump is criticized for lacking the courage to lead or take responsibility.
- The mob's actions and their loyalty to Trump are detailed.
- Republicans in the Senate are portrayed as seeking reasons to acquit despite evidence.
- The short-term focus of Republicans on appeasing an energized base is criticized.
- A majority of Americans want Trump barred from running for office again.
- The potential disillusionment of Trump's base and those who enabled him is discussed.

# Quotes

- "If you wanted to be a leader, every once in a while you had to be first through the door."
- "He wasn't going to be first through the door. He wasn't even going to show up."
- "Republicans in the Senate are looking for any reason to acquit."
- "Republicans in the Senate are chasing the ghost of a base rather than leading, rather than protecting the country."
- "At the time when the country needs them the most, Republicans in the Senate are chasing the ghost of a base rather than leading, rather than protecting the country."

# Oneliner

Impeachment, leadership courage, and the Senate's allegiance to a fading base are scrutinized.

# Audience

Citizens, Leaders

# On-the-ground actions from transcript

- Hold representatives accountable for their decisions (exemplified)
- Stay informed and engaged in political processes (exemplified)
- Advocate for putting the country's interests over party affiliations (exemplified)

# Whats missing in summary

The full transcript expands on the repercussions of blind loyalty and the potential consequences for those involved.

# Tags

#Impeachment #Leadership #Courage #Senate #Accountability #PoliticalProcesses