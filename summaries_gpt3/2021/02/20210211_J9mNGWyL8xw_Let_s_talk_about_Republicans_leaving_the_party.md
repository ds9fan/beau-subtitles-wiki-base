# Bits

Beau says:

- A conference call with over a hundred Republicans aimed to form a third party.
- Participants include representatives from recent Republican administrations.
- The goal is to capitalize on anti-Trump sentiment and move the conservative movement back to the center.
- They want to name their party the center-right party.
- Democrats in the U.S. are already considered center-right on the international spectrum.
- Most leftists do not view Democrats as their representatives.
- Beau believes this new party could gain seats in the House and Senate due to the current Republican Party's disconnect.
- The current Republicans might acquit Trump, leading to more damaging revelations.
- Beau suggests that a shift back to center for the conservative movement could be beneficial.
- If conservatives move to the center, Democrats will have to differentiate themselves further.

# Quotes

- "The goal is to move the conservative movement back to center."
- "They want to capitalize on the anti-Trump sentiment that is sweeping the nation."
- "Democrats in the U.S. are sure, they're left-ish, but that's not saying much."
- "It does appear that they're going to acquit Trump."
- "If the conservative movement does come back to center, I think it's a good thing."

# Oneliner

A hundred Republicans aim to form a center-right party to capitalize on anti-Trump sentiment, potentially shifting the conservative movement back towards the center.

# Audience

Politically interested individuals

# On-the-ground actions from transcript

- Form a community organization to support political shifts (implied)
- Stay informed about political developments and sentiments (implied)

# Whats missing in summary

The full transcript provides additional insights into potential shifts within the conservative movement and the implications for Democrats in the current political climate.