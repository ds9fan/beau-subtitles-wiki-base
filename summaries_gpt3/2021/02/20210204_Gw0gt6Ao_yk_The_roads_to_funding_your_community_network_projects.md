# Bits

Beau says:

- Beau shares insights on running a successful fundraiser for a community network.
- He breaks down a six-step process applicable to any fundraising venture.
- Planning is key, with a focus on setting defined and achievable goals.
- Details matter, including what to obtain, how to deliver it, and getting people involved.
- Pre-advertising phase involves announcing the goal, how to participate, and when it will happen.
- Beau stresses the importance of communication and avoiding assumptions when determining needed supplies.
- The event itself should be fun and engaging to maintain participant interest.
- Delivery is critical—ensure goals are met, communicate effectively, and document the process.
- Post-advertising involves sharing documentation with contributors to show the impact of their support.
- Evaluation is necessary to learn from successes and failures, adjust goals, and aim higher for future efforts.

# Quotes

- "Pick battles that are big enough to matter and small enough to win."
- "Make sure it happens. Make good on whatever it is you have set out to do."
- "You will make a difference."

# Oneliner

Beau outlines a six-step process for successful community fundraising ventures, stressing achievable goals, effective communication, and post-event impact sharing.

# Audience

Community organizers and fundraisers.

# On-the-ground actions from transcript

- Contact local shelters or organizations to understand their immediate needs (suggested).
- Plan and execute fundraising events with defined, achievable goals (exemplified).
- Document the process and share the impact with contributors (implied).

# Whats missing in summary

The full transcript captures Beau's detailed guidance on organizing and executing successful fundraising events for community networks, focusing on achievable goals, effective communication, and post-event impact sharing.

# Tags

#CommunityFundraising #EffectiveCommunication #AchievableGoals #ImpactSharing #CommunitySupport