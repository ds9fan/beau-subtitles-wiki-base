# Bits

Beau says:

- Hollywood unions, with 60,000 members, voted 98% in favor of authorizing a strike for better working conditions and benefits.
- This strike vote is a warning to the industry to negotiate in good faith rather than an immediate action.
- The demands include improved working conditions, larger contributions to pensions and health plans, better rest periods, mill breaks, and a larger share from streaming productions.
- Despite common misconceptions about unions always striking, this specific union has not had a nationwide strike in 128 years, indicating serious grievances.
- The resolution of the members and the high voter turnout suggest skepticism that the industry will negotiate fairly.
- Workers behind entertainment projects seek a fair share of the profits, not just crumbs.
- Delays in entertainment could occur due to the strike, impacting viewers but aiming to secure healthcare and retirement for workers.
- Collective bargaining with industries holding substantial resources can be challenging as they can withstand strikes by hiring replacements.
- Support for the Union from the public could be vital in ensuring a fair negotiation process.
- The strike vote represents a fundamental desire for workers to receive their fair share rather than being overlooked.

# Quotes

- "Delays in entertainment could occur due to the strike, impacting viewers but aiming to secure healthcare and retirement for workers."
- "Workers behind entertainment projects seek a fair share of the profits, not just crumbs."

# Oneliner

Hollywood unions vote overwhelmingly to authorize a strike for better conditions and benefits, signaling a warning to the industry to negotiate fairly and provide workers with their fair share.

# Audience
Entertainment industry workers

# On-the-ground actions from transcript
- Support the Union during negotiations (implied)
- Stay informed about the labor rights issues in the entertainment industry (implied)

# Whats missing in summary
The full transcript provides more context on the Hollywood unions' historical stance on strikes and the challenges faced by workers in negotiating fair treatment and benefits in the entertainment industry.