# Bits

Beau says:

- Exploring individual interests and collective interests, unions, and representation.
- Answering a question that starts off strong but repeats weak arguments from others.
- Contrasting the narrative of representing oneself with the reality of right-wing talking heads having agents.
- Pointing out the hypocrisy of those who advocate against representation while having agents themselves.
- Emphasizing the importance and power of collective representation through unions.
- Describing how collective interest through unions benefits both individuals and the group.
- Arguing that unions empower workers against management's dominance.
- Supporting the idea of unions as vital for balancing power dynamics in today's world.

# Quotes

- "They have somebody who takes a cut of what they make to represent them."
- "That collective interest is your individual interest."
- "It gives you more power at the bargaining table."
- "The only way you're going to be able to do that isn't by putting on your best suit and walking into the manager's office."
- "It's through collective power to achieve your individual interests."

# Oneliner

Exploring collective vs. personal interests, Beau dismantles anti-union rhetoric, advocating for unions as vital for empowering workers against management. 

# Audience

Workers, activists, advocates

# On-the-ground actions from transcript

- Join a union to collectively advocate for your rights (exemplified)
- Educate yourself on the benefits of unions and collective bargaining (suggested)

# Whats missing in summary

The full transcript provides a comprehensive analysis of the importance of unions in representing workers' interests and balancing power dynamics in the workplace.

# Tags

#Unions #CollectiveInterests #PowerDynamics #WorkerEmpowerment #Representation