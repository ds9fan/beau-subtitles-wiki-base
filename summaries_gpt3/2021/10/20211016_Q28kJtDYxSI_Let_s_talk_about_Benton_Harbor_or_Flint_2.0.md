# Bits

Beau says:

- Benton Harbor, Michigan, a town of under 10,000 people, faces lead levels above 15 parts per billion in the water.
- Initially, Governor Gretchen Whitmer's response to the crisis was lackluster, with a five-year timeline for resolution.
- Due to criticism and pressure, the timeline has been accelerated to 18 months for fixing the water issue.
- Residents in Benton Harbor will receive free bottled water during this period.
- Qualified homes with children under Medicaid will receive free or reduced-cost abatement services for issues within the home.
- The situation in Benton Harbor underscores the crumbling and decaying state of infrastructure in the United States.
- Infrastructure is vital for the country to function, and without proper maintenance, it will continue to degrade.
- Despite the improved response under Whitmer, there may not be similar legal consequences faced by former governor Rick Snyder.
- Continuous monitoring and support for communities like Benton Harbor are necessary as they can be easily forgotten by those in power.
- The presence of any level of lead in water poses risks, especially for children, necessitating urgent action.

# Quotes

- "There is no safe level of lead, especially when you're talking about kids."
- "We have to repair the infrastructure in this country. We don't really have a choice on this."
- "If your party claimed that they wanted to make America great for years [...] I doubt your commitment to your cause."

# Oneliner

Benton Harbor, Michigan faces a water crisis as infrastructure crumbles, demanding urgent action to protect residents and prevent further decay.

# Audience

Community members, activists, policymakers

# On-the-ground actions from transcript

- Monitor and support communities facing infrastructure issues (suggested)
- Advocate for swift and effective infrastructure repairs (implied)

# Whats missing in summary

The full transcript provides additional context on the delayed response to the water crisis in Benton Harbor and the importance of continuous monitoring to ensure timely resolution.

# Tags

#Infrastructure #WaterCrisis #LeadPoisoning #CommunitySupport #Advocacy