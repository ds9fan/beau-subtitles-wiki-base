# Bits

Beau says:

- Alec Baldwin was on set filming a movie called Rust when he was handed a prop gun with a live round, resulting in a woman's death and another person's injury.
- The gun community's response involved mocking the incident, with Donald Trump Jr. selling shirts making fun of Baldwin.
- Beau criticizes the lack of leadership following the tragedy and the missed chance to reinforce gun safety.
- Approximately 42 people end up in the hospital daily due to unintentional gunshot wounds, with about 500 of them not surviving each year.
- Beau points out that the incident could have been an educational moment for the gun community to revisit basic gun safety practices.
- He suggests that this was an ideal time to remind people, especially those in the gun community, of the importance of gun safety.
- There is a call for leadership during such incidents to provide guidance and education on firearm safety.
- The lack of leadership in the United States, particularly on the right, is criticized as being filled with internet trolls in positions of power.
- Beau stresses the importance of always treating every weapon as if it is loaded and following strict gun safety protocols.
- The most dangerous firearm is the one assumed to be unloaded, making vigilance in handling firearms critical.

# Quotes

- "Guns don't kill people, Alec Baldwin kills people."
- "This incident could have been used to reinforce gun safety, rather than mock it."
- "The single most dangerous firearm in the world is the one that you are pretty sure is unloaded."
- "They don't have leaders. They have what amounts to internet trolls who found their way to positions of power."
- "It's your friends at stake. It's your community."

# Oneliner

A missed chance for leadership to reinforce gun safety following Alec Baldwin's tragic on-set incident, urging vigilance in firearm handling and education.

# Audience

Gun owners, advocates

# On-the-ground actions from transcript

- Educate your community on basic firearm safety practices (implied)
- Remind friends and family about the importance of gun safety (implied)

# Whats missing in summary

The emotional impact and tone of Beau's message, as well as the urgent call for responsible leadership and education in the aftermath of tragic incidents.

# Tags

#GunSafety #AlecBaldwin #Tragedy #FirearmHandling #Leadership