# Bits

Beau says:

- Talks about accepting feedback and criticism in a professional setting.
- Shares a scenario of a person receiving negative feedback on accepting feedback.
- Advises on the standard way of accepting feedback: pause, listen, and apply.
- Mentions a personal issue with feedback perception rather than accepting it.
- Suggests switching out thanking the critic with asking questions to actively listen.
- Emphasizes the importance of understanding feedback is not personal and viewing it as an asset.
- Encourages actively listening to feedback and applying it for personal growth.
- Acknowledges the challenge of waiting for feedback without appearing dismissive.
- Notes the importance of fitting into the standard way of accepting feedback in a professional environment.
- Recommends training oneself to ask relevant questions to appear engaged during feedback sessions.

# Quotes

- "It's not actually about getting better at accepting the feedback. It's about doing better at that evaluation."
- "View it as an asset. These are free consultants. These are people who are trying to make you better."
- "Train yourself to actively listen and make sure that you understand it's not personal."

# Oneliner

Beau shares advice on accepting feedback in a professional setting, focusing on active listening, understanding feedback isn't personal, and viewing it as an asset to improve.

# Audience

Professionals

# On-the-ground actions from transcript

- Train yourself to actively listen and ask relevant questions during feedback sessions (implied).

# Whats missing in summary

The detailed breakdown of Beau's personal experience and how it relates to feedback perception. 

# Tags

#Feedback #ProfessionalDevelopment #ActiveListening #GrowthMindset #Acceptance