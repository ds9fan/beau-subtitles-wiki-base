# Bits

Beau says:

- Biden's administration restored the sizes of protected areas in the U.S. that Trump had shrunk, using the Antiquities Act.
- The impacted areas include Bears Ears, Grand Staircase, Northeast Canyon, and Cimonts.
- The celebration isn't just a PR move; there are multiple reasons for cheering the restoration.
- The restoration protects artifacts, burial sites, structures, fossil dig sites, and biodiversity.
- Trump's actions seemed to pave the way for mining and fossil fuel exploration, which could harm the environment.
- Native groups see ancient sites as messages from their ancestors, making their protection culturally vital.
- The Biden administration gave a native coalition a significant role in managing the protected areas, a positive step.
- Beau believes Trump's actions against the protected areas were against the law and should have gone to court.
- While a court ruling was missed, the priority remains protecting the sites rather than legal battles.
- The downside is not getting a legal ruling against Trump's actions due to the restoration rendering it moot.
- The restoration is vital for protecting areas that were lacking proper safeguards, although some damage done may be irreversible.

# Quotes

- "The celebration isn't just a PR thing; there's a lot to celebrate."
- "Having them around is incredibly important from a cultural standpoint."
- "It's far more critical to protect the sites than to get a ruling."
- "The restoration extends protections to areas that need it."
- "Undoubtedly, there was damage done during that window that will never be repaired."

# Oneliner

Biden's administration restores protected areas Trump shrank, celebrating cultural significance and environmental protection despite missed legal battle.

# Audience

Environmentalists, Conservationists, Activists

# On-the-ground actions from transcript

- Support and advocate for the protection of natural and cultural heritage sites (exemplified)
- Get involved with native coalitions working towards safeguarding protected areas (exemplified)

# Whats missing in summary

The full transcript provides more detailed insights into the significance of protecting these areas and the potential long-term impacts of environmental degradation if proper safeguards are not in place.

# Tags

#ProtectedAreas #EnvironmentalProtection #CulturalHeritage #NativeCommunities #AntiquitiesAct