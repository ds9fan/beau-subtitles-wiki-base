# Bits

Beau says:

- Beau debunks the notion that Manchin's actions were for coal miners, asserting that it was actually for the owners.
- Using the typewriter industry as an analogy, Beau explains how people switch to better alternatives when they become available, similar to the decline in coal usage.
- He presents statistics showing the significant decrease in coal production and usage in the US over the years.
- Beau points out that coal now contributes to less than a quarter of electricity in the US, falling behind renewables and nuclear energy.
- Despite declining demand domestically, attempts to rely on exports have also been unsuccessful.
- He predicts the inevitable demise of the coal industry, despite Manchin's efforts buying a few more years for the owners, not the miners.
- Beau criticizes Manchin for not advocating for beneficial programs like re-education, retraining, or tax breaks for coal miners transitioning to other industries.
- He expresses concern that miners will face layoffs and financial struggles as the industry continues to decline.
- Beau accuses Manchin of prioritizing the owners' profits over the well-being of the miners, leading to a bleak future for those employed in the coal industry.
- In conclusion, Beau asserts that Manchin's actions were not in the miners' interests but rather served the owners' agenda.

# Quotes

- "He did not do this for the miners."
- "He did it for the owners."
- "He pushed them under a bus and didn't even tell them it was coming."

# Oneliner

Beau debunks the myth that Manchin supported coal miners, revealing his actions favored owners' profits over workers' well-being.

# Audience

Advocates for workers' rights

# On-the-ground actions from transcript

- Advocate for programs like re-education, retraining, and tax breaks for coal miners transitioning to other industries (suggested)
- Support initiatives that prioritize miners' well-being over owners' profits (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of Manchin's actions and their implications for coal miners and industry workers. Viewing the entire speech offers a comprehensive understanding of the discourse on coal mining and the political decisions affecting workers.

# Tags

#CoalIndustry #MinersRights #PoliticalDecisions #TransitionAssistance #WorkersAdvocacy