# Bits

Beau says:

- Talks about the White Rose group, a historic group from WWII Germany known for opposing the regime.
- Admires the structure of White Rose arguments and how they used common imagery to make their points accessible to different groups.
- Mentions that White Rose members were all vaccinated, as mandatory vaccination was the norm in Germany at the time.
- Notes that White Rose never mentioned vaccines in their writings, focusing on nonviolent passive resistance instead.
- Criticizes the misuse of White Rose imagery by anti-vaccine groups, calling it historically illiterate and potentially propaganda.
- Points out that mandatory vaccinations were suspended during the Nazi regime, suggesting a disregard for public health.
- Urges people to drop WWII references from vaccine debates, as it can be offensive and shows historical ignorance.

# Quotes

- "Claiming the mantle of the White Rose does not make you look like a freedom fighter. It makes you look like you've never read a history book."
- "It's just a thought."

# Oneliner

Beau explains the historical context of the White Rose group to criticize the misuse of their imagery by anti-vaccine movements, urging a more informed and respectful approach to public health debates.

# Audience

History enthusiasts, public health advocates

# On-the-ground actions from transcript

- Educate yourself on the history of groups like the White Rose and their true messages (suggested)
- Use accurate historical references and avoid appropriating symbols for unrelated causes (implied)

# Whats missing in summary

The full transcript provides a detailed historical analysis of the White Rose group's stance on vaccines and passive resistance, urging a more informed and respectful approach in public health debates.

# Tags

#WhiteRose #History #Vaccines #PublicHealth #Education