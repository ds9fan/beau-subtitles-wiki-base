# Bits

Beau says:

- General Milley testified about Chinese developments and hypersonic missiles, potentially signaling a new arms race.
- Milley mentioned the Department of Defense needing to make strategic shifts, reprioritize, and spend money in response.
- Dr. Cameron Tricy, an expert on nuclear arms control, will provide insights in an upcoming interview.
- The new missiles aren't significantly faster, stealthier, or undetectable, challenging the narrative of a game-changing technology.
- Despite lacking funding for infrastructure, the defense budget will likely support these new weapons.
- Beau warns against another arms race and advocates for preparing to face the evolving threats.
- He criticizes the defense industry's potential exaggeration of new threats and the swift funding approval they receive.
- Beau questions the focus on defense spending while domestic issues remain unaddressed.
- The shift towards near-peer adversaries resembles the dynamics of the Cold War, with propaganda and arms race implications.
- Beau suggests a reevaluation of national priorities towards more productive endeavors.

# Quotes

- "We don't need another arms race. We need to inoculate ourselves."
- "If the United States is crumbling from within, what good is defense spending?"
- "Maybe we need to focus on things that are more productive and less destructive."

# Oneliner

General Milley's testimony on Chinese developments and hypersonic missiles may trigger an arms race, demanding strategic shifts and significant spending, while Beau calls for a reevaluation of national priorities.

# Audience

Policy makers, activists

# On-the-ground actions from transcript

- Pay attention to the potential exaggeration of threats by the defense industry (implied)
- Advocate for a reevaluation of national priorities towards less destructive endeavors (implied)

# Whats missing in summary

Dr. Cameron Tricy's insights on nuclear arms control and the nexus between science and security policy.

# Tags

#NationalSecurity #ArmsRace #DefenseSpending #ChineseDevelopments #HypersonicMissiles