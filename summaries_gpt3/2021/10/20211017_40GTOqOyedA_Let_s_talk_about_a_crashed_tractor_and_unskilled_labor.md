# Bits

Beau says:

- Addresses the term "unskilled labor" and argues it is a misnomer.
- Shares an incident where salaried employees were asked to replace striking workers at John Deere, resulting in a mishap.
- Challenges the idea that jobs labeled as unskilled truly require no skill.
- Mentions that even service industry jobs like waiting tables require time to develop the necessary skills.
- Criticizes the term "unskilled labor" for devaluing the work and justifying low wages.
- Asserts that most jobs require learning and skill development, even if they don't demand formal education.
- Urges for the retirement of the term "unskilled labor" due to its role in diminishing the value of workers' contributions.

# Quotes

- "The idea of unskilled labor, it's a fiction."
- "It's just a method of devaluing the workers."
- "There aren't a whole lot of jobs you can walk into and just do it."
- "If somebody is putting in the work, they should probably have access to a decent life."
- "The skills are required."

# Oneliner

Beau challenges the concept of "unskilled labor," arguing that all work requires skill development and deeming the term as a method to devalue workers.

# Audience

Workers, activists, labor advocates

# On-the-ground actions from transcript

- Advocate for fair wages and recognition of skill in all types of work (implied)
- Support labor movements and strikes to ensure workers are valued for their contributions (exemplified)

# Whats missing in summary

The full transcript includes Beau's engaging and relatable anecdotes that effectively illustrate his points and add a personal touch to his argument. Viewers can gain a deeper understanding and connection through these stories. 

# Tags

#LaborRights #SkillRecognition #FairWages #ValueOfWork #Activism