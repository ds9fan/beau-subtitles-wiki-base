# Bits

Beau says:

- Addressing the backlash surrounding Superman's son, John Kent, being portrayed as bisexual.
- Pointing out the hypocrisy of people claiming they don't care about superheroes' love lives when it has always been a central aspect of comic book characters.
- Listing various comic book characters who have had love interests throughout the comics.
- Calling out the underlying bigotry in objecting to John Kent's bisexuality.
- Emphasizing the importance of representation for individuals who may identify as bisexual.
- Questioning the lack of understanding and acceptance from those who object to LGBTQ+ representation in comic books.
- Reminding readers that comics often convey messages of overcoming biases and supporting each other.

# Quotes

- "It isn't that. It's that you don't like this particular kind. That's what it is. Yes, it's bigotry."
- "What happens if your kid is bi, and they don't see any representation for how they are, who they are?"
- "Most comics have that overriding theme that everybody's a hero, and that all we have to do is support each other."

# Oneliner

Beau addresses the backlash against Superman's son being portrayed as bisexual, calling out the bigotry and hypocrisy surrounding objections to LGBTQ+ representation in comics.

# Audience

Comic book fans, LGBTQ+ advocates

# On-the-ground actions from transcript

- Support LGBTQ+ representation in comic books (suggested)
- Provide positive support to individuals in your life who identify as LGBTQ+ (implied)
- Overcome biases, prejudices, and bigotry in your interactions (exemplified)

# Whats missing in summary

Importance of accepting and celebrating diverse representations in media and storytelling.

# Tags

#Superman #LGBTQ+ #Representation #Comics #Acceptance