# Bits

Beau says:

- Today is a big day for a group called trainees for child injury prevention, aiming to draw attention to the leading cause of childhood injury related deaths.
- Firearms, not cars, account for the leading cause of childhood injury-related deaths in the USA.
- 4.6 million kids live in homes where weapons are stored loaded and unsecured, increasing the risk of unintentional shootings.
- Safer storage of firearms is the focus of the group's day of action to encourage responsible gun ownership.
- The importance of securing firearms is stressed, as hidden weapons can be dangerous to curious children.
- Beau compares securing firearms to a vaccine mandate, stating that people should do it willingly without the need for a law.
- He mentions a webinar and chat session organized by the group, mainly comprising doctors, to raise awareness about firearm safety protocols.

# Quotes

- "Hidden doesn't mean safe. Hidden doesn't mean secure. Hidden means, wow, look what I found."
- "Lock up your firearms. 3,500 a year lost to guns."
- "Either you're going to do it because it's the right thing or it's going to become a law."

# Oneliner

Today is a critical day for child injury prevention advocacy, focusing on the dangers of unsecured firearms and the need for responsible storage.

# Audience

Parents, gun owners, community members

# On-the-ground actions from transcript

- Secure firearms in your home to prevent accidental shootings (exemplified)
- Educate yourself and others about responsible firearm storage (exemplified)
- Join online webinars and chats about child injury prevention (exemplified)

# Whats missing in summary

The emotional impact of accidental shootings on children and the urgency of addressing firearm safety proactively.

# Tags

#ChildInjuryPrevention #FirearmSafety #ResponsibleGunOwnership #CommunityAdvocacy #Awareness-Raising