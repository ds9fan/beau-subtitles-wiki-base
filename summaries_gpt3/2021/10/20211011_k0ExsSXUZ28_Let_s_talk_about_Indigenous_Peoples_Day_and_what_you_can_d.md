# Bits

Beau says:

- Indigenous Peoples Day prompts action for groups being recognized.
- Native causes have various needs, one being Oak Flat protection.
- Oak Flat, ancestral home to Apache, Hopi, and Zuni, faces copper mining proposal.
- Native groups oppose the mining proposal at Oak Flat.
- Public perception on the mining issue is starting to shift.
- The mining operation at Oak Flat will use a significant amount of water.
- Majority of likely voters in Arizona oppose the mining proposal.
- Legislation like the Save Oak Flat Act aims to stop the mining.
- A petition supported by ACLU is circulating to save Oak Flat.
- Today is a good day to get involved in supporting causes like saving Oak Flat.
- Support for causes like this can create a tipping point for change.
- Even small support can contribute to significant change.
- Building support for movements like this takes time.
- The shift in public opinion can lead to tangible outcomes.
- Encouraging support for causes can lead to impactful change.

# Quotes

- "Today's a good day to get involved in something."
- "Movements like this, they're always slow going."
- "Even small support might push it over the edge and get something done."

# Oneliner

Indigenous Peoples Day prompts action, with Oak Flat facing a critical tipping point for change through public support against the proposed copper mining.

# Audience

Activists, Supporters, Voters

# On-the-ground actions from transcript

- Sign and share the petition supported by ACLU to save Oak Flat (suggested).
- Support legislation like the Save Oak Flat Act to stop the mining proposal (suggested).

# Whats missing in summary

The full transcript provides detailed insight into the urgency of supporting Native causes, specifically focusing on the critical need to save Oak Flat from a destructive copper mining proposal.

# Tags

#IndigenousPeoplesDay #NativeCauses #SaveOakFlat #PublicSupport #Activism