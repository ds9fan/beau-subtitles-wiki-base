# Bits

Beau says:

- Provides a follow-up on a strike happening in Alabama at Warrior Met Coal.
- Hayden Rot, the auxiliary president for the MWA at two locals, gives insight into the strike.
- The mine specifically mines metallurgical coal for steel production, not thermal coal for energy.
- Workers have been on strike since April 1st due to safety concerns, unfair labor practices, and family time.
- The company has a policy of scheduling workers seven days a week, 12 hours a day, with harsh penalties for absences.
- Previous contract changes removed double-time and triple-time pay, limiting time off during holidays for families.
- Vehicular assaults on picket lines by company members or scabs have occurred.
- Negotiations have not progressed well, with the company offering improvements that favor scabs over union members.
- The company aims to hire back only those who they believe did nothing wrong during the strike, excluding vocal union members.
- Financial strain is increasing as the strike enters its seventh month, with efforts to support union families through the holidays.
  
# Quotes
- "It's not just about pay for us. It's about holding the company accountable."
- "We're still ready to be here. We're not ready to give this fight up until we win."
- "It's really only been through all the mutual aid that we've received and through the union that we have been able to hold on as long as we have."

# Oneliner
Beau follows up on a strike at Warrior Met Coal in Alabama where workers fight for safety, fair labor practices, and family time amidst company resistance, financial strain, and community support.

# Audience
Supporters of labor rights

# On-the-ground actions from transcript
- Donate to UMWH Striking Miners Pantry PayPal account for groceries, baby items, and hygiene products (suggested)
- Support the toy drive through the Target registry link provided by Hayden (suggested)
- Attend solidarity rallies or events organized by the union to show support (implied)

# Whats missing in summary
The full transcript provides detailed insights into the ongoing strike, challenges faced by workers, community support efforts, and the importance of unions in advocating for workers' rights.

# Tags
#LaborRights #Strike #Union #CommunitySupport #Alabama