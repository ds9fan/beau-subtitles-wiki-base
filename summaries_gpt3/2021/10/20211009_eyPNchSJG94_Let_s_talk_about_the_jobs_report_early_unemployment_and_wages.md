# Bits

Beau says:

- Analyzing the recent jobs report that revealed 194,000 new non-farm payrolls added but a shrinking labor force by 183,000, upsetting expectations.
- People anticipated more growth post the expiration of unemployment benefits, leading to the lowest numbers all year.
- Suggests the low numbers may not be due to laziness but a blend of the ongoing public health crisis, reluctance to risk it, and inadequate wages.
- Points out the struggle of finding workers due to inadequate wages, with some stores having to close two days a week for lack of staff.
- Emphasizes the basic economic principle of supply and demand, urging businesses to raise wages to attract workers.
- Criticizes those who terminated benefits early and labeled workers as lazy freeloaders, indicating they only ended up harming hard-working individuals.
- Asserts that workers should not be blamed for companies seeking to extract more from them, urging business owners facing hiring challenges to simply increase pay.

# Quotes

- "You have to pay more. It is that simple."
- "It is not the workers' fault that companies want to extract more and more from them."
- "If you're running into this issue, if you own a business and you're trying to get workers and you can't, bump the pay. It's really that simple."

# Oneliner

Analyzing disappointing job numbers post-benefits expiration, Beau stresses the need for higher wages to attract workers, debunking stereotypes and advocating for fair compensation.

# Audience

Business Owners, Workers

# On-the-ground actions from transcript

- Increase wages to attract workers (suggested)
- Support businesses paying living wages (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the impact of low wages on hiring challenges and advocates for fair compensation to address workforce shortages effectively.