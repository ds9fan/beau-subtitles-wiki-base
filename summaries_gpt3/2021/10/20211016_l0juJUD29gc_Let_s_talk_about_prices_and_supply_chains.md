# Bits

Beau says:

- Explains why prices are going up due to supply and demand dynamics.
- Consumer spending habits changed during the pandemic, leading to increased demand for certain items.
- Production was down as businesses and factories were closed, resulting in less supply and higher prices.
- Gives a personal example of fluctuating prices for building materials like lumber.
- Describes how the increase in home improvement projects led to a surge in demand for lumber.
- Many consumers unknowingly overpaid for products due to lack of comparison and market forces.
- Notes that prices are going up for a variety of products due to changing habits.
- Mentions that as people start venturing out more, demand for different products is shifting.
- Talks about disruptions in the supply chain due to factories shutting down during the public health crisis.
- Companies are now trying to catch up on production, leading to delays in getting products off boats.
- Foresees these supply chain issues taking about a year to resolve.
- Assures that severe shortages are unlikely but warns of ongoing annoyances due to supply chain hiccups.
- Advises being cautious during the holidays, especially with electronics-heavy gifts.
- Suggests preparing for potential scarcity of certain items without causing panic.
- Concludes by reassuring that the situation is manageable but may be inconvenient for a while.

# Quotes

- "It's just going to be more of a pebble in our shoe, constantly annoying, and it's going to last a while."
- "Rather than something severe that is short-lived, it's going to be annoying and it's going to last a long time."
- "You should be able to get your basic needs. Maybe not the particular brand of corn or whatever you like, but you'll be fine when it comes to basic survival."
- "It's just going to be more of a pebble in our shoe, constantly annoying, and it's going to last a while."
- "Maybe steer your children into asking Santa for things that don't have a whole lot of electronics in them."

# Oneliner

Beau explains the impact of changing consumer habits on prices, supply chain disruptions, and the lasting inconvenience of current market challenges.

# Audience

Consumers, holiday shoppers

# On-the-ground actions from transcript

- Be cautious with holiday shopping, especially items heavy on electronics (implied)
- Prepare for potential scarcity by focusing on basic survival needs (implied)
- Stay informed about market dynamics and adjust shopping habits accordingly (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of the current economic situation regarding prices, supply chain disruptions, and consumer behavior during and post-pandemic, offering insights into how these factors interplay and affect everyday life. 

# Tags

#Economics #SupplyChain #ConsumerBehavior #Pricing #HolidayShopping