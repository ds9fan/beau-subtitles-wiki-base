# Bits

Beau says:

- Explains how the right-wing machine shapes public perception before facts are known.
- Mentions FBI serving warrants on the New York Police's Sergeants Benevolent Association and union president's home.
- Notes the limited information available, with search warrants being served and involvement of Office of Public Corruption.
- Quotes the union's statement indicating the union president may be the target of the federal investigation.
- Observes the trending of the story on Twitter and the generation of a narrative by talking heads.
- Points out that the focus shifted to the union president's support for Trump and past inflammatory tweets.
- Criticizes the creation of a narrative without evidence to suggest political payback.
- Comments on mainstream outlets picking up the narrative created by right-wing media.
- Expresses skepticism towards the speculation around the search warrants without official statements.
- Concludes by reflecting on the narrative already established without concrete evidence.

# Quotes

- "This is how these wild narratives that the right-wing media in the U.S. likes to latch onto, it's how they start, it's how they're created, and it's how they distract from actual events."
- "We don't know what it's about yet. There haven't been any real official statements on it at time of filming."
- "It's the narrative that's already taking shape. And it's all without evidence."

# Oneliner

Beau explains how the right-wing machine shapes narratives before facts are known, using the example of search warrants served in New York, criticizing the premature generation of stories without concrete evidence.

# Audience

Activists, Media Consumers

# On-the-ground actions from transcript

- Fact-check narratives and wait for official statements (implied)

# Whats missing in summary

Full context and Beau's detailed analysis on the creation of narratives before facts are known. 

# Tags

#RightWingMedia #NarrativeShaping #SearchWarrants #PublicPerception #FactChecking