# Bits

Beau says:

- A candidate in North Carolina, Charles Graham, put out an ad referencing an event from the 1950s, leading to viral attention and questions about its accuracy.
- The event in question, the Battle of Hayes Pond, involved a confrontation between James "Catfish" Cole, a Klan leader, and the Lumbee tribe in 1958.
- Cole, a WWII veteran and Klan leader, organized motorcades with the police, causing fear and chaos in neighborhoods.
- On the opposing side was Robert Williams, supported by the NAACP, who formed the Black Armed Guard for community defense.
- In October 1957, Cole's motorcade targeted Dr. Perry's house but was met with armed resistance from the Black Armed Guard.
- Cole then shifted his focus to the Lumbee tribe, despite warnings from local police about potential consequences.
- The Lumbee tribe planned a strategic ambush near Hayes Pond, surprising and defeating Cole's group.
- The defeat at Hayes Pond drew significant attention, leading to the denouncement of the Klan by the governor and Cole's arrest.
- The events at Hayes Pond and the defeat of the Klan resulted in a decline in Klan activities in the area.
- Despite some minor disputes about details, the accuracy of the ad regarding the Battle of Hayes Pond remains largely undisputed.

# Quotes

- "What does it say that an account of the Klan being defeated is still a good political ad today."
- "The defeat at Hayes Pond drew significant attention."
- "It led to the governor denouncing the clan."
- "The accuracy of the ad regarding the Battle of Hayes Pond remains largely undisputed."
- "I don't see that as being very material to anything."

# Oneliner

A political ad referencing the Battle of Hayes Pond in the 1950s sheds light on Klan defeat, prompting questions about its relevance in modern American politics.

# Audience

History buffs, activists

# On-the-ground actions from transcript

- Support community defense groups (implied)
- Advocate for historical events that showcase resistance against hate groups (implied)

# What's missing in summary

The full transcript provides a detailed historical account of the Battle of Hayes Pond, shedding light on a significant event in the fight against the Klan in 1958.

# Tags

#Klan #History #Resistance #CommunityDefense #NorthCarolina