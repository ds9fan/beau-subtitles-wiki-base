# Bits

Beau says:

- Addressing criticism from the right wing about the Biden administration's handling of the border and public health.
- Refusing to advocate for forced vaccination at the border, focusing on appealing to protecting oneself and others.
- Stating a strong pro-vaccine stance but opposing using force or denying liberty to enforce vaccination.
- Criticizing the idea of locking up unvaccinated individuals, pointing out the underlying bigotry in such arguments.
- Challenging the notion that unvaccinated individuals won't seek vaccination once released.
- Expressing concern about those who advocate for measures they wouldn't want applied to themselves.
- Warning against blindly repeating arguments without critical thinking and understanding the implications.
- Asserting that people crossing the border have rights under the US legal system.
- Emphasizing the paternalistic nature of forcing vaccination on certain groups.

# Quotes

- "I don't believe the government should use force, deny your liberty, to make you get vaccinated. That would be wrong."
- "Anything you advocate for them, well, it should be done to you as well, unless, of course, you believe there's two justice systems in the United States."
- "Anything that you allow the government to do to a small demographic, you're just opening the door for yourself."
- "You will advocate for your own incarceration because they have that much of a hold over you."
- "Y'all have a good day."

# Oneliner

Beau addresses right-wing criticism on vaccination, opposing forced measures and challenging the bigotry behind locking up unvaccinated individuals.

# Audience

Public health advocates

# On-the-ground actions from transcript

- Challenge and critically analyze arguments before repeating them (implied)
- Advocate for equal treatment and rights for all individuals (implied)

# Whats missing in summary

Importance of critical thinking and advocating for equal rights and treatment for all individuals.

# Tags

#Vaccination #PublicHealth #BorderControl #Criticism #Rights