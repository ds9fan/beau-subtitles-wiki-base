# Bits

Beau says:

- National Police Week prompts reflection on the challenges faced by officers and deputies.
- Calls to action during this time are common when officers face significant risks.
- In 2020, 264 officers were lost, with 145 of them due to COVID, the leading cause again in the current year.
- Despite the high numbers, there's a lack of outcry, especially from those who claim to support law and order.
- Some governors prevent law enforcement leaders from mandating COVID precautions, unlike other safety measures like wearing a vest.
- The "back the blue" sentiment may be more of a slogan than genuine support, as actions don't always match the rhetoric.
- The loyalty test around COVID precautions has led to preventable deaths among officers.
- Politicians and governors who back law and order may not fully support law enforcement if they don't prioritize officer safety.
- The disconnect between rhetoric and action raises questions about the true intentions behind supporting law enforcement.
- Lack of support for necessary precautions may undermine the image of those in power as protectors of citizens.

# Quotes

- "Maybe 'back the blue' isn't quite what they really mean."
- "They certainly don't seem to support law enforcement."
- "The loyalty test around COVID precautions has led to preventable deaths among officers."
- "Despite the high numbers, there's a lack of outcry."
- "Calls to action during this time are common when officers face significant risks."

# Oneliner

National Police Week prompts reflection on officer challenges and the disconnect between rhetoric and action in supporting law enforcement.

# Audience

Law enforcement advocates

# On-the-ground actions from transcript

- Contact local law enforcement agencies to inquire about COVID precaution mandates (suggested)
- Advocate for necessary safety measures within law enforcement agencies (implied)

# Whats missing in summary

The emotional impact of the disconnect between expressed support for law enforcement and the lack of action to protect officers. 

# Tags

#NationalPoliceWeek #LawEnforcement #BackTheBlue #COVIDPrecautions #SupportOfficers