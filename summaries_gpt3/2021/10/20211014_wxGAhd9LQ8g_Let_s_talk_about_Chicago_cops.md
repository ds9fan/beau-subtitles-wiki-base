# Bits

Beau says:

- The Chicago police union advised officers not to disclose vaccination status, defying city requirements.
- Unions typically oppose non-pre-negotiated issues, defaulting to opposition.
- Police unions differ significantly from other unions.
- Ignoring vaccination policy raises concerns about following other public safety policies.
- Lack of vaccination value in law enforcement questions officers' judgment.
- Officers regularly face situations where vaccination is vital for public safety.
- Non-compliance with vaccination poses higher infection risk for officers and community members.
- The mission of law enforcement is public safety; non-compliance suggests a need for a different profession.
- Union concerns about firing officers gradually rather than all at once.
- Non-compliance with public safety measures is an easy way to filter out unsuitable officers.
- Society faces a choice: be part of the solution or follow propagandized talking points.
- The Chicago union made its choice; the city's response is critical.

# Quotes

- "This is a really easy way to weed out bad officers."
- "Do you want to protect your neighbor? Or do you want to go with a propagandized talking point?"
- "If they're not willing to commit to that mission, they probably need a different profession."

# Oneliner

The Chicago police union's defiance on vaccination status reveals deeper issues with public safety and judgment in law enforcement.

# Audience

Law enforcement officers

# On-the-ground actions from transcript

- Advocate for public safety measures within law enforcement (implied)
- Support policies that prioritize public health and safety (implied)

# Whats missing in summary

The full transcript provides additional context on the implications of the Chicago police union's stance on vaccination disclosure and its impact on public safety and officer judgment.