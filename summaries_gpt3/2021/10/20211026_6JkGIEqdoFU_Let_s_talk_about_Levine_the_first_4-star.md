# Bits

Beau says:

- Addressing information consumption and a message received about the "new four star."
- Expressing issues with incorrect name usage and putting "woman" in quotation marks.
- Sharing the story of Ann Dunwoody, the first woman four star back in 2008.
- Pointing out Admiral Michelle Howard as the first woman four star admiral in Navy history.
- Calling out Representative Jim Banks for spreading false information to manipulate anger.
- Emphasizing the trend of manipulating people by fabricating stories to evoke outrage.
- Questioning the ease with which people are manipulated by untrue narratives.
- Encouraging fact-checking or simply being nice as alternatives to falling for manufactured outrage.

# Quotes

- "The fact that you do shows how easily they can manipulate you with something that isn't true."
- "Completely manufactured. Completely made up."
- "Is it that hard just to fact-check?"
- "Just be nice."
- "Y'all have a good day."

# Oneliner

Beau addresses the manipulation through fabricated stories to provoke outrage, urging fact-checking or kindness over falling for manufactured anger.

# Audience

Social media users

# On-the-ground actions from transcript

- Fact-check before sharing information (suggested)

# Whats missing in summary

Deeper exploration of the impact of fabricated stories on manipulating public outrage and the importance of critical thinking and kindness in response.

# Tags

#InformationConsumption #ManufacturedOutrage #FactChecking #Kindness #SocialMedia