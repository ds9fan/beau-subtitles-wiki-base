# Bits

Beau says:

- Comparative analysis on the infrastructure bill versus the DOD budget over 10 years.
- Infrastructure bill aims to accomplish infrastructure development and address climate change impacts.
- DOD spending a significant amount on preparing for resource-based conflicts.
- The bill will provide necessary infrastructure to help the US cope with climate change impacts.
- UN study shows a significant increase in flood and drought-related disasters.
- Projection of inadequate water access for 5 billion people by 2050.
- Mitigating these effects could save lives and resources.
- Questioning opposition to the bill, suggesting it’s vital for national resilience.
- Implication that opposition may have ulterior motives benefiting from war machine profits.
- Stressing the importance of current actions shaping the future.
- Infrastructure bill seen as a means to reduce severity of climate-related impacts.
- Urging reevaluation of opposition stance towards investing in infrastructure.
- Choice between investing in infrastructure or future military casualties.
- Encouraging reflection and reconsideration of positions on the bill.

# Quotes

- "If you are in opposition to this, you might want to really rethink your position."
- "Not sure why people would be in opposition to this unless they want the United States to fail."
- "The future is determined by what happens now, by what we do today."
- "You might want to spend it on roads and bridges and cleaner energy or you want to spend it on flag-covered coffins in the future."
- "It is worth noting that DOD is spending a whole lot of that 8 trillion to gear up to fight over resources."

# Oneliner

Beau compares infrastructure bill costs to DOD's budget, urging reevaluation of opposition for a resilient future mitigating climate impacts.

# Audience

US citizens

# On-the-ground actions from transcript

- Support infrastructure bill to invest in sustainable infrastructure and combat climate change (implied).

# Whats missing in summary

The full transcript provides detailed analysis and statistics supporting the importance of investing in infrastructure for climate resilience and national security.