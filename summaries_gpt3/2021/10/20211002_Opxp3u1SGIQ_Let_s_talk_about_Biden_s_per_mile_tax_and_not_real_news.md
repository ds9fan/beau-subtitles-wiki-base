# Bits

Beau says:

- Debunks the misinformation about an 8-cent per mile fee in the infrastructure bill.
- Reveals that the misinformation was spread by a former Trump speechwriter.
- Explains that the bill actually includes a study on the feasibility of funding highways through a usage fee, not an 8-cent tax per mile.
- Points out that implementing a tax on gas usage is a more feasible method to fund highways and infrastructure.
- Mentions the existing gas taxes in Florida, proving that the fear over new taxes is unfounded.
- Notes that many people were unaware of the current gas taxes in Florida.
- Calls out the fear-mongering and unnecessary debate surrounding the non-existent 8-cent per mile fee.
- Recommends the AP's "Not Real News" series as a resource to debunk misinformation and understand how false stories develop.
- Suggests that higher gas taxes may be implemented in the future to support infrastructure and incentivize a switch to electric vehicles.
- Encourages readers to stay informed and not worry about the misrepresented 8-cent per mile fee.

# Quotes

- "If your favorite source of information has been railing about this the last few days, maybe you want a different source of information."
- "It's pretty common. So the fear mongering over it seems a little overdone."
- "I want to take this moment to point to something."
- "I wouldn't worry about an 8 cent per mile gas or 8 cent per mile fee. That's not a thing."
- "Y'all have a good day."

# Oneliner

Beau debunks the misinformation around an 8-cent per mile fee in the infrastructure bill, clarifying the actual contents and suggesting a more feasible solution while recommending the AP's "Not Real News" series for debunking false information.

# Audience

Fact-checkers, News Consumers

# On-the-ground actions from transcript

- Read the AP's "Not Real News" series weekly to spot and debunk misinformation (suggested).
- Stay informed about legislative proposals and separate fact from fiction to make informed decisions (exemplified).

# Whats missing in summary

The full transcript provides detailed insights into debunking misinformation, understanding legislative proposals, and staying informed about current affairs.