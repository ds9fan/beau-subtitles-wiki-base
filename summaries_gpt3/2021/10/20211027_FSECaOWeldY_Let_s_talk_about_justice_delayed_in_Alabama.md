# Bits

Beau says:

- Talking about buses and justice in Montgomery, Alabama during a specific historical event.
- Recalling the scenario where buses were divided and individuals were required to move if an area got too crowded.
- Describing the incident where a bus driver asked someone to move, leading to police involvement.
- Mentioning Claudette Colvin, a teenager who also resisted giving up her seat before Rosa Parks, but faced media scrutiny due to being unmarried and pregnant.
- Updating on Claudette Colvin's current efforts to have her record expunged at the age of 80.
- Questioning why Claudette Colvin hasn't been pardoned yet despite the historical significance of her actions.
- Expressing support for Claudette Colvin's endeavors while also advocating for a pardon to acknowledge the wrong that was done.
- Pointing out the lack of termination of Claudette Colvin's probation from the state of Alabama officially.
- Emphasizing the importance of recognizing and atoning for the past events that occurred during the civil rights movement in Montgomery.
- Advocating for a more significant gesture beyond record expungement to address the injustices faced by Claudette Colvin.

# Quotes

- "It seems like Alabama owes her."
- "When somebody is penalized for doing the right thing, at some point, there should be some kind of atonement."
- "It seems like there should be that moment where the state says, 'Yeah, we were wrong. We're sorry. We beg your pardon.'"

# Oneliner

Beau talks about buses, Rosa Parks, and the overlooked Claudette Colvin, questioning why Alabama hasn't pardoned her despite her historical significance and the need for atonement.

# Audience

Alabama residents, Civil Rights advocates.

# On-the-ground actions from transcript

- Support Claudette Colvin's petition to get her record expunged (implied).
- Advocate for a pardon for Claudette Colvin in Alabama (implied).

# Whats missing in summary

The emotional weight of Claudette Colvin's story and the importance of acknowledging and rectifying past wrongs in history.

# Tags

#CivilRights #HistoricalInjustice #Alabama #Montgomery #RosaParks #ClaudetteColvin