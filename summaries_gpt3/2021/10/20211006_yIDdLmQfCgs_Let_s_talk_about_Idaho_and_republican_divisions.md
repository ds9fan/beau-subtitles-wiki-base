# Bits

Beau says:

- Idaho's Republican Party is facing disunity between mainstream conservatives and the far right with authoritarian tendencies.
- Governor Little and Lieutenant Governor are not political allies in Idaho.
- Lieutenant Governor issued an executive order banning vaccine mandates in Governor Little's absence.
- The Lieutenant Governor also tried to mobilize the National Guard without proper authority.
- Major General Garshak clarified the National Guard's role and denied the request.
- Governor Little plans to undo the orders issued by the Lieutenant Governor upon his return.
- This isn't the first time the Lieutenant Governor has acted independently; she previously issued a ban on mask mandates.
- Beau questions the Lieutenant Governor's fitness for office if she continues to make power grabs.
- The discord within the Republican Party in Idaho is becoming more evident.
- Beau urges Republicans to stand against authoritarianism within their party to protect democracy.

# Quotes

- "If somebody is so overcome with the power granted by the absence of the governor that they begin issuing executive orders, this might not be somebody fit for the governor's office."
- "This display of a power grab should weigh pretty heavily on you."
- "They're taking shots at each other pretty regularly."
- "If there's a time for those people who are just conservatives to take a stand and say, we don't want to have anything to do with this, it's this moment."
- "Their party will be overrun with those who are into political grandstanding, those who are in for attaining power for power's sake, and then using it."

# Oneliner

Idaho's Republican Party grapples with disunity as power struggles between Governor and Lieutenant Governor play out, prompting questions on authoritarian tendencies and the need for party reform.

# Audience

Republicans, Idaho citizens

# On-the-ground actions from transcript

- Support candidates who prioritize democracy over power grabs (implied)
- Advocate for unity within the Republican Party to combat authoritarian influences (implied)

# Whats missing in summary

Full context and nuances of Beau's analysis and call for action.

# Tags

#Idaho #RepublicanParty #Governor #LieutenantGovernor #Authoritarianism #Democracy