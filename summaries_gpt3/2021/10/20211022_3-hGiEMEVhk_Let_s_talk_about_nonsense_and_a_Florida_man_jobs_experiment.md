# Bits

Beau says:

- Joey Holtz conducted an experiment in response to his boss's rant about lazy employees quitting due to $1,200 checks.
- Joey applied for 60 jobs in September, receiving nine email responses, one phone call, and only one job interview.
- The job interview offered was for a construction laborer position in Florida paying $8.65 an hour.
- Employers expecting people to work for such low wages is astounding, especially in a low cost of living area.
- This experiment challenges the narrative of a labor shortage and lazy employees, suggesting employers may be the lazy ones.
- Some employers might prioritize social media rants over actual hiring efforts.
- Joey targeted companies where bosses publicly denigrated employees on social media, raising concerns for potential applicants.
- Employers seem entitled to a cheap labor pool, unwilling to put in the effort to attract and retain employees.
- Some businesses are not hiring because they took care of their employees during the public health crisis.
- Joey's experiment sheds light on the disconnect between employers' expectations and the reality of the job market.

# Quotes

- "Employers are so entitled that they expect people to work in Florida doing construction laboring for $8.65 an hour."
- "It kind of casts doubt on the narrative that there's a labor shortage, that employees are lazy."
- "Employers have become so entitled to having a ready labor pool who will work for apparently $8.65 an hour."
- "It's what it sounds like. It's what it seems like."
- "Joey's experiment is extended. Anyway, it's just a thought."

# Oneliner

Joey's job application experiment challenges the narrative of a labor shortage and lazy employees, revealing employer entitlement to cheap labor.

# Audience

Potential job seekers

# On-the-ground actions from transcript

- Apply for jobs that value your worth and offer fair compensation (implied).
- Advocate for fair wages and respectful treatment in the workplace (implied).
- Support businesses that prioritize employee well-being and fair pay (implied).

# Whats missing in summary

The full transcript provides deeper insights into the dynamics between job seekers and employers in the current labor market.

# Tags

#LaborMarket #JobSeekers #Employers #Wages #Employment