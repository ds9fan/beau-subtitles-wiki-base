# Bits

Beau says:

- Addresses the topic of paternity leave after receiving viewer messages about Mayor Pete and supply chain issues.
- Shares his support for paternity leave, having taken time off after the birth of his children.
- Criticizes the Republican Party for discouraging spending time with kids despite promoting family values.
- Points out how the GOP manipulates working-class voters to go against policies that could benefit them, like paternity leave.
- Notes the party's tactic of associating policies they dislike with demographics they've marginalized, such as gay people.
- Calls out the stereotype that paternity leave is only for weak men, perpetuated by the GOP.
- Mentions the military parental leave program as a counter to the notion that paternity leave is weak or unmasculine.
- Criticizes the manipulation tactics used to sway individuals against policies that could personally benefit them.
- Encourages spending time with kids and challenges toxic masculinity views perpetuated by societal norms.


# Quotes

- "They play into that stereotype that paternity leave is just for gay men and gay men are weak and you don't want to be like them, right?"
- "It's wild to me, but this is how they do it."
- "This is how they convince you to go against your own interests."
- "All they have to do is convince their base that they're in a slightly better position than somebody else of a demographic they don't like."
- "Go spend some time with your kids."

# Oneliner

Beau dismantles the GOP's manipulation of working-class voters against paternity leave, exposing toxic masculinity stereotypes and advocating for spending time with kids.


# Audience

Working-class parents

# On-the-ground actions from transcript

- Spend quality time with your kids (exemplified)
- Challenge toxic masculinity stereotypes by supporting paternity leave policies (exemplified)


# Whats missing in summary

The full transcript provides a detailed breakdown of how political manipulation and toxic masculinity stereotypes impact working-class individuals' views on paternity leave.

# Tags

#PaternityLeave #PoliticalManipulation #ToxicMasculinity #FamilyValues #WorkingClassParents