# Bits

Beau says:

- Beau criticizes the blame on Mayor Pete for US supply chain issues, citing right-wing media.
- He checks his news app and discovers the supply chain issues are actually in the UK and Ireland, not Mayor Pete's responsibility.
- Beau questions why misinformation blaming Mayor Pete is allowed to circulate and blames information silos.
- He explains that Mayor Pete's role as Secretary of Transportation doesn't directly influence global supply chain issues.
- Beau warns about the dangers of information silos and how they can distort reality.
- Despite Mayor Pete's position and responsibilities, Beau clarifies that the supply chain issues are a global problem, not localized to the US.

# Quotes

- "You can be handed a perception that isn't grounded in reality."
- "Whether or not that person or demographic of people is actually responsible for it, that all depends on whether or not you look outside that information silo."
- "Mayor Pete has nothing to do with this."

# Oneliner

Beau clarifies misinformation blaming Mayor Pete for global supply chain issues and warns about the dangers of information silos.

# Audience

Information seekers, critical thinkers.

# On-the-ground actions from transcript

- Fact-check news from multiple sources (implied).
- Break free from information silos by seeking diverse news outlets (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how misinformation spreads and the importance of verifying information from various sources. 

# Tags

#Misinformation #SupplyChain #InformationSilos #CriticalThinking #GlobalIssues