# Bits

Beau says:

- Responds to criticism about not providing sources or links below videos.
- Believes in viewers' ability to fact-check using search engines.
- Acknowledges the importance of context in understanding information.
- Points out the dangers of falling into information silos created by misleading links.
- Mentions a video by Dr. Kennard critiquing Beau's content on cops resigning in Massachusetts.
- Talks about the importance of verifying information and looking at multiple sources.
- Mentions a story about a Florida deputy planting evidence and getting caught.
- Comments on the unreliability of sourcing alone.
- Recognizes Dr. Kennard's effort to ensure accurate information for viewers.
- Expresses different perspectives on linking sources in videos and the risks of misinformation.

# Quotes

- "If you really want to understand something you can't just follow a link that the person who makes a claim provides you."
- "The reporting lines up with the anecdote and the town gossip."
- "Just because there's a link down below doesn't mean the information is true."
- "There's too much money in misinformation."
- "Nobody's right in this case. It's, well, it's just a thought."

# Oneliner

Beau addresses criticism about sourcing, stressing the importance of context and caution with provided links, discussing a video by Dr. Kennard and differing views on ensuring accurate information.

# Audience

Content Creators, Information Consumers

# On-the-ground actions from transcript

- Fact-check information from online sources (implied)
- Verify information by looking at multiple sources (implied)
- Be cautious with provided links and do further research (implied)

# Whats missing in summary

Importance of critical thinking and independent verification in navigating online information.

# Tags

#Sourcing #FactChecking #Context #InformationSilo #Misinformation