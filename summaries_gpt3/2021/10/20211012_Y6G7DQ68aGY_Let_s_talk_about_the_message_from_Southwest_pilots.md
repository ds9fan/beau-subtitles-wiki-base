# Bits

Beau says:

- Southwest pilots are sending a message by standing up against misinformation and opposition to mandates, which has been debunked.
- The sick rate for pilots at Southwest remains the same, indicating there is no widespread coordinated action.
- Some pilots may be using sick time before leaving Southwest, but there is no evidence of a coordinated effort.
- Aviation experts explain how Southwest's system is susceptible to cascade failure, leading to the recent flight cancellations.
- Despite information debunking the narrative, certain individuals continue pushing it because they know it will work on a segment of the population.
- Those perpetuating misinformation rely on a segment of the population trained not to question but to believe blindly.
- Leaders like Trump, Boebert, Cruz, and media personalities exploit patriotism and lack of critical thinking in their followers.
- Individuals pushing false narratives have a history of misleading the public on various issues for their benefit.
- True leaders empower others to lead from their communities rather than manipulate them with false information.
- People who continue to push debunked narratives are exploiting and monetizing those who believe them, rather than educating them.
- Blindly following without evidence or critical thought is detrimental to society and plays into the hands of those spreading misinformation.
- The incident at Southwest is a minor example of a larger trend within the right wing that relies on anger and manipulation.
- Individuals should demand evidence from those making claims to avoid being emotionally manipulated and misled.
- Emotional manipulation and lack of critical thinking enable certain individuals to profit off spreading misinformation.

# Quotes

- "They have found a way to monetize you."
- "Smart people want to empower you to lead from your own community."
- "Ignore evidence. And listen to people who never provide the evidence they say they have."
- "They have gathered a group of middle-aged, middle-income, middle-America."
- "Start asking these people for evidence of their claims. You'll find out they don't have it."

# Oneliner

Southwest pilots debunk coordinated action narrative; misinformation benefits from blind belief and emotional manipulation.

# Audience

Community members, critical thinkers.

# On-the-ground actions from transcript

- Demand evidence from individuals spreading misinformation (implied).
- Empower others in your community to lead and think critically (implied).
- Stop listening to those who continue pushing debunked narratives (implied).

# Whats missing in summary

The importance of critical thinking and demanding evidence in combating misinformation and manipulation.

# Tags

#Southwest #Misinformation #Leadership #CriticalThinking #CommunityLeadership