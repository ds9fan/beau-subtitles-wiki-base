# Bits

Beau says:

- Rolling Stone reported on rally organizers naming Congress members involved in planning the Capitol insurrection, catching headlines.
- Organizers claim innocence, painting themselves in a positive light, saying they had no idea what was going to happen at the Capitol.
- Cowboys for Trump founder Griffin turned on the former president in a speech, expressing feeling betrayed and sold out.
- The crowd Griffin spoke to, many present at the Capitol on the 6th, are becoming disaffected and feeling abandoned by their leaders.
- The Republican Party, especially those embracing revolutionary rhetoric, has heavily influenced this disaffected group.
- The radicalized individuals are feeling betrayed and are about to be publicly disavowed by their leadership.
- Those who believed in the lies and wild claims of the president are now facing the reality of being misled and betrayed.
- The committee investigating the Capitol insurrection must hold the responsible parties accountable to prevent future anger and radicalization.
- Beau urges disaffected individuals to put down guns, pick up books, and focus on civic engagement and community involvement.
- The manipulation by leaders who preyed on anger and ignorance can be combated by educating oneself and being involved in local community issues.

# Quotes

- "They lied to you. And they are going to continue to do so as long as it works."
- "Your best defense against the manipulation that they used is to become educated."
- "We are in for a very bumpy ride if this crew that has been radicalized ends up feeling sold out."

# Oneliner

Rolling Stone exposed rally organizers' involvement in Capitol insurrection planning, while Cowboys for Trump founder turns on former president, leaving disaffected supporters feeling betrayed.

# Audience

Disaffected individuals

# On-the-ground actions from transcript
- Put down guns, pick up books, and focus on civic engagement and community involvement (suggested)
- Educate oneself and get involved in local community issues to combat manipulation (suggested)

# Whats missing in summary

Full understanding of the consequences of manipulation, betrayal, and radicalization among disaffected individuals can be best grasped by watching the full transcript.

# Tags

#CapitolInsurrection #Disaffection #Betrayal #Radicalization #CommunityInvolvement #CivicEngagement