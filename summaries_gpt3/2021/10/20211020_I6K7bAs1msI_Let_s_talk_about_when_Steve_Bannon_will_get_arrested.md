# Bits

Beau says:

- House panel voted unanimously to hold Steve Bannon in criminal contempt for defying subpoenas.
- Bannon has been defiant by not turning over documents or being deposed.
- After the house panel vote, it goes to the full house for certification, then to federal law enforcement.
- Federal law enforcement decides whether to proceed after reviewing the report.
- If they decide to proceed, it then goes to a grand jury.
- Only if the grand jury decides to proceed might Bannon get picked up, so it's not immediate.
- The charge is not huge, with a minimum sentence of 30 days and up to a year.
- The House panel seems serious about playing tougher with those obstructing the process.
- They aim to send a message to others considering obstructing.
- The House panel believes there's more information they don't know, hence their strong actions.
- Bannon being in trouble is a bad sign for former President Trump.
- Bannon might have information that could alter or reinforce existing beliefs.
- House panel believes Bannon has critical information that could change the narrative.
- Cooperation might not stop the proceedings once it reaches the Department of Justice.
- House panel believes Bannon holds information that could provide more insight into events.

# Quotes

- "House panel voted unanimously to hold Steve Bannon in criminal contempt."
- "The House panel seems serious about playing tougher with those obstructing the process."
- "Bannon might have information that could alter or reinforce existing beliefs."

# Oneliner

House panel votes Bannon in contempt, signaling tougher stance on obstruction and potential new information.

# Audience

Political observers, justice advocates.

# On-the-ground actions from transcript

- Stay informed on the developments in Bannon's case (implied).
- Support organizations advocating for transparency and accountability in government actions (implied).

# Whats missing in summary

Insights on the potential implications of Bannon's case on broader political narratives.

# Tags

#SteveBannon #Contempt #HousePanel #Obstruction #Justice #Accountability