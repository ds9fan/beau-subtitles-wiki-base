# Bits

Beau says:

- Companies blaming unemployment for labor shortage, refusing to raise wages.
- Unemployment is survival, not a cash cow; wages should be higher than unemployment benefits.
- Companies forgetting the industrialist rule: pay highest wages possible.
- American companies neglecting to pay high wages to attract labor.
- Labor is vital for creating capital and being a capitalist.
- Companies pressuring governments to reduce unemployment instead of raising wages.
- Working class faces the carrot (hope for social mobility) and the stick (threat of homelessness).
- Companies failing to grasp basic economics: supply and demand.
- Shortage of labor should lead to increased wages.
- Budgeting advice like "pull yourself up by your bootstraps" fails to address real issues.

# Quotes

- "Unemployment is not really a cash cow that's pretty much the bare minimum you can survive on."
- "You have to pay for it. It's really that simple."
- "Maybe it's just a budgeting issue. Y'all shouldn't go to Starbucks so much."
- "If you want it, you have to pay for it."
- "It's really that simple. Anyway, it's just a thought."

# Oneliner

Companies blame unemployment for labor shortage, refusing to pay higher wages, neglecting basic economics and the industrialist rule.

# Audience

Working Class, Employers, Government

# On-the-ground actions from transcript

- Raise wages to attract labor (implied)
- Advocate for fair wages in your workplace (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the challenges of unemployment, wages, and labor shortages in the current economic landscape.

# Tags

#Unemployment #Wages #LaborShortage #Economics #WorkingClass