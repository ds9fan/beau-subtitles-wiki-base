# Bits

Beau says:

- A friend reached out for help in explaining socialism to someone terrified of it due to World War II associations.
- The challenge was to explain without using history, politics, or ideology.
- Beau accepted the challenge but struggled to come up with an approach for two weeks.
- While looking for quotes by George Orwell, Beau stumbled upon a video where he quotes a piece of poetry.
- Beau recites a well-known poem that starts with "First they came for the socialists."
- The poem progresses to mention trade unionists and Jews, with the speaker not speaking out until it was too late for them.
- The point made is that if the persecutors were socialists, they wouldn't have targeted socialists first.
- Beau suggests that this simple and powerful poem can help explain the misconception about socialists during World War II.
- The poem provides a straightforward way to illustrate the issue without delving into complex historical or political details.
- Beau concludes by offering this poem as a tool for having a meaningful and accessible conversation on the topic.

# Quotes

- "First they came for the socialists and I did not speak out because I was not a socialist."
- "Your answer is in poetry."
- "It's not history-based. It doesn't have to require a whole lot of knowledge of different ideologies."
- "If this is true, then why would they do this?"
- "Y'all have a good day."

# Oneliner

A friend seeks help in explaining socialism without history, politics, or ideology; Beau uses a powerful poem to debunk misconceptions easily.

# Audience

Educators, Activists, Students

# On-the-ground actions from transcript

- Share the poem "First they came for the socialists" to debunk misconceptions about socialism (implied).

# Whats missing in summary

Exploration of the impact of using poetry as a tool for addressing misconceptions in political history.

# Tags

#Socialism #History #Poetry #Education #Misconceptions #GeorgeOrwell