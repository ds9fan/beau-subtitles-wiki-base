# Bits

Beau says:

- Slogans can mislead citizens into believing they represent actual government policy.
- The United States does negotiate with groups, contrary to popular slogans.
- Modern doctrine aims to remove militant leadership to encourage negotiation.
- Misunderstanding slogans can lead to a mistaken expectation of government behavior.
- Expecting a "get tough" approach can result in security clampdowns and innocent people being harmed.
- Clampdowns can inadvertently strengthen opposition groups.
- Justifying actions based on opposition behavior can lead to dangerous outcomes.
- Blaming protesters for everything can escalate violence and justify unjust actions.
- The story of Alexander the Great and the pirate illustrates the similarity in actions between pirates and emperors.
- Failing to recognize violence as violence leads to justifying any action by one's side and condemning the other's.
- In state versus non-state actor conflicts, a security clampdown often leads to an endless cycle of violence.
- The cycle of violence can only end when the state side commits to negotiation.
- In the United States, there were calls to deploy troops against citizens due to demonization and belief in slogans.
- Without a commitment to negotiation, the cycle of violence persists.
- Recognizing violence as violence is critical to breaking the cycle of violence.

# Quotes

- "Expecting a 'get tough' approach can result in security clampdowns and innocent people being harmed."
- "Failing to recognize violence as violence leads to justifying any action by one's side and condemning the other's."
- "The cycle of violence can only end when the state side commits to negotiation."

# Oneliner

Slogans can mislead citizens into expecting government actions, leading to violence and the perpetuation of conflict until negotiation is truly committed.

# Audience

Policy Advocates

# On-the-ground actions from transcript

- Contact local representatives to advocate for peaceful conflict resolution (implied).
- Organize community dialogues on the impact of slogans and policies on government actions (implied).
- Educate others on the dangers of justifying violence based on opposition behavior (implied).

# Whats missing in summary

The full transcript delves deeper into the dangerous consequences of misinterpreting slogans as policy and the necessity of committing to negotiation to end cycles of violence. Watching the full transcript provides a comprehensive understanding of these concepts.

# Tags

#Slogans #Policy #Violence #Negotiation #Government #Commitment #Community #Advocacy