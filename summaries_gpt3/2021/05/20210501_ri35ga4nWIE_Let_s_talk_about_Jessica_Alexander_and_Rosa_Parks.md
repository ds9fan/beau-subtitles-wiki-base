# Bits

Beau says:

- Jessica Alexander from Temecula, California, made a widely ridiculed and unjustifiable comparison of her anti-mask advocacy to Rosa Parks.
- Jessica Alexander also got a key fact wrong about Rosa Parks' story, claiming Parks moved to the front of the bus because she knew it wasn't lawful, when in reality, it was the law that enforced segregation.
- Rosa Parks was seated in the first come, first serve section of the bus, not in the white section reserved for whites.
- Parks' refusal to give up her seat was an act of resistance against the unjust segregation laws, not just mere fatigue or happenstance.
- By placing Parks in the white section of the bus, the true injustice and inequalities of segregation are downplayed and sanitized.
- The comparison between Jessica Alexander's anti-mask advocacy and Rosa Parks' civil rights activism is deemed ridiculous and nonsensical by Beau.
- Beau stresses the importance of getting Rosa Parks' story right and giving her the proper credit she deserves.
- The attempt to equate wearing masks in modern times to the struggles of individuals like Rosa Parks during segregation is dismissed as a false comparison.
- Beau calls out the trend of minimizing historical atrocities while exaggerating contemporary inconveniences as the ultimate victimization.
- Beau encourages viewers to watch a video detailing overlooked aspects of Rosa Parks' story for a more comprehensive understanding.

# Quotes

- "That was the whole problem. You know, the segregation thing, that was an issue."
- "She wasn't somebody who just happened to be in the right place at the right time."
- "When you say it out loud, it doesn't make any sense because it doesn't make any sense."
- "Something being asked to wear a mask to living in Montgomery, Alabama at that time, that's not the same."
- "Please get the story right because this is somebody who I truly believe is never given the proper credit that they deserve."

# Oneliner

Jessica Alexander's comparison of her anti-mask advocacy to Rosa Parks is not only factually incorrect but also dismissive of the true injustices faced during segregation, a comparison Beau deems ridiculous.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Watch the video detailing overlooked aspects of Rosa Parks' story (suggested)
- Ensure accurate and respectful retelling of historical figures' stories (implied)

# Whats missing in summary

Deeper insights into the importance of historical accuracy and honoring the legacies of civil rights icons like Rosa Parks could be gained from watching the full transcript.

# Tags

#RosaParks #CivilRights #HistoricalAccuracy #SocialJustice #CommunityStandards