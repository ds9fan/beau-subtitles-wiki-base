# Bits

Beau says:

- Gas prices began rising before Biden took office.
- Summer brings higher-quality gas and increased travel, raising demand and costs.
- The pandemic led to decreased travel last year, creating a pent-up demand for travel now.
- Stimulus checks and tax credits have increased disposable income, leading to more travel and higher costs.
- Crude oil prices dropped significantly last year and are now rebounding, contributing to increased costs.
- Several U.S. refineries closed under the previous administration, reducing production and increasing costs.
- The recent pipeline hack caused short-term disruptions but is not a long-lasting factor.
- Gas prices are rising in the UK and Canada as well, indicating a global trend.
- Biden's stance on fossil fuels may contribute to market instability but not solely to blame for gas price increases.
- Understanding basic economics, particularly supply and demand, is key to grasping the situation.
- Blaming Biden for gas prices without proper understanding is unjustified.

# Quotes

- "You can't blame this on Biden, not if you want to do it, honestly."
- "Because if your politician does not understand supply and demand, they probably shouldn't be your representative."

# Oneliner

Gas prices rose before Biden, summer travel demand, stimulus checks, global trend - Biden not solely to blame.

# Audience

Consumers

# On-the-ground actions from transcript

- Monitor gas prices and try to carpool or use public transportation when possible (implied).
- Educate others on the factors affecting gas prices and discourage unjustified blame (implied).

# Whats missing in summary

The full transcript provides a comprehensive breakdown of the factors influencing gas prices and challenges the narrative of solely blaming Biden for the increase.