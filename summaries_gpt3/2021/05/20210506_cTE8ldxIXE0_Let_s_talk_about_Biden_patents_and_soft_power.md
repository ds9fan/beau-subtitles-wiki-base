# Bits

Beau says:

- Explains the Biden administration's decision to support ending vaccine patents as an exercise in soft power rather than altruism.
- Differentiates between soft power (attracting countries to you) and hard power (going after them) in foreign policy.
- Advocates for the U.S. to be the world's EMT (emergency medical technician) rather than the world's policeman to illustrate soft power over hard power.
- Mentions the U.S. countering China and Russia's vaccine diplomacy through soft power.
- Describes how China and Russia have been successful in using soft power to gain influence internationally.
- Suggests that the U.S. is playing catch up with China and Russia in exercising influence through vaccine diplomacy.
- Points out the potential benefits of a global vaccine race triggered by countries trying to exert influence.
- Emphasizes that the U.S.'s move to support ending vaccine patents is about obtaining influence overseas, which coincidentally benefits everyone involved.
- Compares the immediate results of hard power with the longer-lasting benefits of soft power in foreign policy.
- Raises the possibility that the U.S.'s actions may be a response to counter Russian and Chinese soft power rather than a purely altruistic gesture.

# Quotes

- "We should be the world's EMT rather than the world's policeman."
- "This is about obtaining influence overseas."
- "Just don't let it be framed as we're out there being the world's savior."
- "All of this is good."
- "Y'all have a good day."

# Oneliner

Beau explains how the U.S.'s support to end vaccine patents is about obtaining global influence through soft power, not altruism, in response to China and Russia's actions.

# Audience

Foreign policy enthusiasts

# On-the-ground actions from transcript

- Support efforts to provide necessary supplies for vaccine production in other countries (implied)
- Advocate for equitable distribution of vaccines globally (implied)
- Stay informed about international relations and soft power dynamics (implied)

# Whats missing in summary

Analysis of how the U.S.'s shift towards exercising more soft power in foreign policy could impact global dynamics and relationships.

# Tags

#ForeignPolicy #SoftPower #VaccineDiplomacy #GlobalInfluence #USPolicy #China #Russia