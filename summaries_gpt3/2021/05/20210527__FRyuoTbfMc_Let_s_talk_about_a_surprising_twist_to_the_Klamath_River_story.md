# Bits

Beau says:

- Provides an update on the Klamath River story in Oregon and Northern California.
- Addresses a situation involving low water levels in the river, endangered species, native fishing communities, and irrigation for farmers.
- Mentions the closure of the canal to preserve water levels, fishing, and endangered species.
- Two farmers have bought land near the head gates and set up an information center to petition for grievances.
- Notes the connection of the farmers to Amon Bundy, known for involvement in public lands issues.
- Farmers express concerns about potential government intervention and mention past instances of forcing open head gates.
- Points out a historical similar situation where farmers had clashed with US Marshals over the head gates.
- Hints at a potential national story developing in Oregon with the first demonstration scheduled to happen soon.

# Quotes

- "The only thing separating us from the head gates is a chain link fence."
- "It's worth noting that about 20 years ago there was a similar situation."
- "We may all be turning our eyes to Oregon for a national story."

# Oneliner

Beau provides an update on the Klamath River situation, hinting at potential conflict with federal intervention and past history repeating itself, setting the stage for a developing national story.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Join or support demonstrations near the head gates to show solidarity (suggested)
- Stay informed about the developments in the Klamath River situation (implied)

# Whats missing in summary

Full context and depth of the developing conflict around the Klamath River situation can be better understood by watching the full video.

# Tags

#KlamathRiver #Oregon #NorthernCalifornia #AmonBundy #NationalStory