# Bits

Beau says:

- Questioning why the U.S. is leaving their allies behind, particularly the interpreters and Afghan people who assisted U.S. forces.
- Traditionally, the U.S. military has left allies behind in conflicts, but Congress is urging Biden to speed up the process of helping these individuals.
- Congress could pass legislation to airlift the 18,000 applicants and their families to the U.S. for processing, which seems more efficient than the current process in Afghanistan.
- Beau believes that Congressman Waltz from Florida, an ex-Green Beret, genuinely cares about the situation due to his personal experience working with these individuals.
- Beau expresses a cynical view that leaving these allies behind could serve as a motivation for them to join the opposition, knowing the consequences if the national government falls.
- He points out the pattern of the U.S. leaving allies behind worldwide, making it difficult to recruit local help.
- Despite the risks taken by these allies for U.S. interests, there is a delay in fulfilling promises made to them, with a wait time of possibly a couple of years.

# Quotes

- "Leave no man behind, right?"
- "We're leaving, we're heading out, we're not helping them but we know they could help the national government."
- "And now, when it's time to make good on the promises that we made, well, we'll get around to it."
- "Doesn't mean it's not true."
- "That's honestly what it seems like."

# Oneliner

Beau questions why the U.S. is leaving allies behind, urging for quicker action to fulfill promises made to those who risked their lives for U.S. interests.

# Audience

Advocates for allies

# On-the-ground actions from transcript

- Push Congress to pass legislation authorizing an airlift of the Afghan allies and their families to the U.S. for processing (implied).
- Advocate for expedited processes to fulfill promises made to allies who assisted U.S. forces (implied).
- Support organizations working to assist Afghan allies in relocating to safety (implied).

# Whats missing in summary

The emotional impact and urgency conveyed by Beau's words is best experienced by watching the full transcript.

# Tags

#US #Afghanistan #Allies #Congress #Promises #Support