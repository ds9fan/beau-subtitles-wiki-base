# Bits

Beau says:

- Addresses leaving behind people in Afghanistan who helped the US or NATO.
- Mentions the visa process backlog and the risks involved in applying for it.
- Talks about two counter arguments: moral and practical.
- Explains the moral argument against US occupation due to imperialism.
- Points out that combatants in conflict may not represent the will of the people.
- Stresses that being against imperialism involves self-determination and protecting civilians.
- Emphasizes that leaving behind those who helped the West is not just limited to interpreters.
- Suggests mounting an operation called Operation New Life to bring these individuals to safety.
- Mentions the success of a similar operation in 1975 that brought over 100,000 people to Guam.
- Argues that the capability to conduct such an operation exists and it's a matter of will.

# Quotes

- "We mount an operation to do it, and we will call it Operation New Life."
- "The ability to do this is there. That's just the idea that it isn't. That's just fantasy."
- "People are going to look to Biden for this. It's not really his call."

# Oneliner

Beau addresses leaving behind Afghan allies, proposing Operation New Life to bring them to safety, citing past successes and the capability to act now.

# Audience

Advocates for Afghan allies

# On-the-ground actions from transcript

- Mount an operation to evacuate Afghan allies, as proposed by Beau (suggested).
- Advocate for Operation New Life to be implemented by the government (suggested).

# Whats missing in summary

The full transcript provides a detailed insight into the moral and practical implications of leaving Afghan allies behind, proposing a feasible solution through Operation New Life.

# Tags

#Afghanistan #OperationNewLife #USoccupation #ForeignPolicy #Evacuation