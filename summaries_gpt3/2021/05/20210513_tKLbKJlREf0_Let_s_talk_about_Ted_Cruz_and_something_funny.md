# Bits

Beau says:

- Feels conflicted about doing something funny or addressing a serious message.
- Mentioned receiving a message questioning his claims about Republican politicians.
- Commented on Ted Cruz's tweet about gasoline outages in North Carolina.
- Explained the irony behind the connection made to the green new deal in Cruz's tweet.
- Noted that the green new deal is proposed legislation, not yet law, and has had no effect.
- Pointed out the potential benefits of the green new deal in reducing reliance on fossil fuels.
- Mentioned Ted Cruz's lack of enthusiasm for certain infrastructure types.
- Criticized Cruz for implying a connection between the pipeline issue and the green new deal.
- Suggested that Cruz's tweet could be seen as misleading and fear-mongering.
- Noted the high number of likes on Cruz's tweet, indicating support without critical analysis.

# Quotes

- "Welcome to the green new deal."
- "It's funny that a sitting senator doesn't know that this isn't law."
- "I find that funny as long as you don't think about it too long."

# Oneliner

Beau feels conflicted between humor and seriousness, critiquing Ted Cruz's misleading tweet about the green new deal's connection to gasoline outages.

# Audience

Social media users

# On-the-ground actions from transcript

- Fact-check misleading information shared on social media (implied)
- Encourage critical thinking and research before accepting claims blindly (implied)

# Whats missing in summary

The full context and nuances of Beau's commentary on the misleading nature of political statements and the importance of critical thinking.