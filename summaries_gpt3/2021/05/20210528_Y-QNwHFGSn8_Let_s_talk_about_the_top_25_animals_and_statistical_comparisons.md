# Bits

Beau says:

- Introduces the topic of expectations, perceptions, statistics, and comparisons, using a list of 25 animals ranked from least to most dangerous.
- Asks the audience to think about which animals they expect to be on the list and which they perceive to be more dangerous.
- Presents a comparison of the world's most dangerous animals, revealing surprising statistics like mosquitoes causing 1 million deaths per year.
- Raises questions about the accuracy and implications of such statistics, pointing out flaws in the comparisons made.
- Criticizes the use of statistics without clear criteria for comparison, warning against making decisions based on misleading information.
- Emphasizes the need to scrutinize statistics used to push agendas and make informed decisions.
- Urges the audience to be cautious when interpreting statistics and ensure consistent criteria are applied for accurate comparisons.
- Concludes by reminding viewers to critically analyze information before making decisions.

# Quotes

- "Statistics in the hands of those who want to enact an agenda, they have to be scrutinized pretty closely."
- "When comparisons are made, make sure that the same criteria gets applied to like items throughout it."
- "It's a tool of propaganda. It's a way to push an agenda."
- "You can end up basing your decisions on something that isn't accurate."
- "Y'all have a good day."

# Oneliner

Beau compares statistics on dangerous animals to caution against misleading comparisons and agenda-driven use of data.

# Audience

Statistical analysts, policymakers, researchers.

# On-the-ground actions from transcript

- Scrutinize statistics for accuracy and consistency in comparisons (implied).
- Educate others on the importance of critical analysis when interpreting data (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of how statistics can be manipulated and misinterpreted, urging viewers to be vigilant against misleading information for informed decision-making.

# Tags

#Statistics #DataAnalysis #AnimalSafety #Propaganda #CriticalThinking