# Bits

Beau says:

- Talks about an op-ed by Ted Cruz in the Wall Street Journal, which stirred controversy on Twitter due to implications of taking campaign contributions in exchange for favors to large corporations.
- Cruz criticizes woke companies and their responses to bills, especially regarding Georgia.
- Cruz vows not to overlook issues like Coca-Cola's back taxes or MLB's antitrust exemption.
- He announces that he will no longer accept money from corporate PACs, citing the $2.6 million received over nine years.
- Cruz implies a connection between campaign contributions and favors, leading to a reconsideration of conventional wisdom on political influence.
- Suggests setting up a GoFundMe account to afford a senator based on the relatively low cost.
- Acknowledges Cruz may not have intended to admit to this exchange but understands how it can be interpreted that way.
- Irony in Cruz's statement regarding not accepting money anymore due to perceived mistreatment by corporations.
- Questions the effectiveness of Cruz's message in the op-ed.

# Quotes

- "This time, we won't look the other way on Coca-Cola's $12 billion in back taxes owed."
- "Maybe we can afford a senator."
- "Y'all were mean to me so I'm not going to let you pay me off anymore."

# Oneliner

Beau delves into Ted Cruz's controversial op-ed, hinting at a potential exchange of campaign contributions for favors, leading to a reevaluation of political influence and the affordability of senators.

# Audience

Political enthusiasts, activists

# On-the-ground actions from transcript

- Start a GoFundMe campaign to fund political causes (exemplified)
- Analyze and question the connections between corporate contributions and political favors in your local political landscape (suggested)

# Whats missing in summary

Insights on the impact of corporate contributions on political decision-making and the need for transparency in political funding.

# Tags

#TedCruz #CorporateInfluence #CampaignContributions #PoliticalTransparency #GoFundMe