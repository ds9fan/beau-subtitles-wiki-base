# Bits

Beau says:

- Receives a message narrating someone's life story, leading up to seeking encouragement.
- Describes the backstory of a kid growing up in a neighborhood and getting involved in an organization.
- The individual goes to prison but uses the time to read and grow intellectually.
- After release, he aims to make his neighborhood better despite facing challenges.
- Expresses self-doubt due to past actions and worries about being judged.
- Mentions how rich white college kids come to the neighborhood for short-term fixes.
- Encourages the individual not to be deterred by past mistakes or external judgments.
- Emphasizes the unique skills and knowledge the person gained from their past experiences.
- Urges the person to push through self-doubt as their contributions are valuable.
- Stresses the importance of persevering despite uncomfortable moments for the greater good.

# Quotes

- "You're going to get that hate no matter what. That's not a reason. That's an excuse."
- "What you say is that you were in a gang. What I hear is that you know how supply routes work."
- "If you've already put in the groundwork and now you're just having second thoughts about it, a little bit of self-doubt, you've got to push through that."
- "There may not be somebody else for a long time that gets that Goldilocks sentence, that has the skills and the connections and the understanding that right now you have."
- "It probably resonated with you for another reason. The hero in that video is not the dude that had gas money because he had a YouTube channel. The hero were the people that got their neighborhood supplied."

# Oneliner

Beau receives a life story message, encouraging perseverance despite past mistakes, urging the individual to utilize their unique skills for community betterment.

# Audience

Community members

# On-the-ground actions from transcript

- Continue efforts to improve the neighborhood despite challenges (exemplified)
- Push through self-doubt and external judgments to make a positive impact (exemplified)
- Utilize unique skills and knowledge gained from past experiences for community organizing (exemplified)

# Whats missing in summary

Full understanding of the narrator's encouragement and motivation behind community betterment efforts.

# Tags

#CommunityBuilding #OvercomingChallenges #UtilizingSkills #Encouragement #Perseverance