# Bits

Beau says:

- Responding to a comment on Ted Cruz's Twitter feed about recruitment practices in government entities and modern art.
- Criticizes the idea that the Department of Defense (DOD) and CIA are becoming "woke" through their recruitment practices.
- Points out that the dictionary definition of "woke" is being alert to injustice, particularly racism.
- Mentions that the DOD has a history of recruiting people from marginalized groups, contributing to social change.
- Explains how the special forces and historical CIA operations have involved cultural exchange and appreciation.
- Talks about the CIA's recruitment strategies, including using abstract art to recruit intellectuals.
- Argues that both the DOD and CIA have always recruited individuals who understand ethnic divisions and class issues.
- Emphasizes that progress and intellectual acceptance do not lead to the downfall of a country.
- Concludes that the DOD and CIA are not actually "woke" but rather use the concept to attract recruits.
- Urges viewers to recognize the historical context of recruitment practices in these agencies.

# Quotes

- "They recruit people that you view as lesser."
- "Neither the DOD or the CIA is actually woke."
- "Progress and intellectual acceptance do not lead to the downfall of a country."

# Oneliner

Responding to misconceptions about government recruitment practices, Beau dismantles the idea that the Department of Defense and CIA are becoming "woke," explaining their historical recruitment strategies and the concept's misinterpretation.

# Audience

Social commentators, activists

# On-the-ground actions from transcript

- Research and analyze historical recruitment practices of government entities to understand their context and implications (suggested).
- Educate others on the historical roles of the Department of Defense and CIA in recruitment (suggested).
- Challenge misconceptions about government recruitment practices by sharing accurate information with others (suggested).

# Whats missing in summary

The full transcript provides a detailed analysis of the historical recruitment practices of the Department of Defense and CIA, debunking the misconception that these agencies are becoming "woke."

# Tags

#RecruitmentPractices #GovernmentEntities #Misconceptions #HistoricalContext #SocialChange