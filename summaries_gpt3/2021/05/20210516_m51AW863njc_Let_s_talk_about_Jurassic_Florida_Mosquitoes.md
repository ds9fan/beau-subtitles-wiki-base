# Bits

Beau says:

- Scientists are conducting a wild science experiment in Florida involving genetically modified mosquitoes.
- Male mosquitoes have been modified to pass on a gene to their female offspring, requiring them to have an antibiotic to prevent breeding.
- The goal is to reduce mosquito-borne diseases without using pesticides.
- Similar experiments have been done in other countries with positive results.
- The reduction of pesticides, especially in Florida, is seen as a positive aspect of this experiment.
- Beau mentions the famous line from a movie, "life finds a way," hinting at potential unexpected outcomes.
- The impact of mosquito-borne diseases is becoming increasingly significant due to climate change expanding the insects' ranges.
- Despite sounding like a plot from a science fiction movie, the experiment has multiple safety safeguards in place.
- The modified mosquitoes are expected to be self-limiting and last only a couple of generations, providing a safety net in case something goes wrong.
- Beau acknowledges that there might be concerns and invites experts to share their opinions on the experiment.

# Quotes

- "Life found a way."
- "Scientists are conducting a wild science experiment in Florida involving genetically modified mosquitoes."
- "The reduction of pesticides, especially in Florida, is seen as a positive aspect of this experiment."

# Oneliner

Scientists conduct a wild science experiment in Florida involving genetically modified mosquitoes to reduce mosquito-borne diseases without pesticides, despite potential risks.

# Audience

Environmentalists, scientists, community members

# On-the-ground actions from transcript

- Share information about genetically modified mosquitoes with your community (suggested)
- Seek out expert opinions on the subject and start a constructive discussion (suggested)

# Whats missing in summary

The full transcript provides a detailed insight into the science experiment involving genetically modified mosquitoes and the potential impact on reducing mosquito-borne diseases in Florida.

# Tags

#GeneticallyModifiedMosquitoes #FloridaScienceExperiment #PesticideReduction #MosquitoBorneDiseases #ClimateChange #CommunityEngagement