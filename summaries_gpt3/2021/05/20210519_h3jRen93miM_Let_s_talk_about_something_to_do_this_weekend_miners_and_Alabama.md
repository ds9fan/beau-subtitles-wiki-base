# Bits

Beau says:

- Miners in Alabama went on strike after facing deteriorating working conditions and mistreatment from management.
- The United Mine Workers of America in Brookwood, Alabama are leading the strike with over 1100 miners participating.
- The striking miners are in need of a strike pantry to support their families with food.
- The Valley Labor Report is hosting a livestream fundraiser from Friday to Sunday to support the striking miners.
- The Alabama Strike Fest on May 22 will feature music, barbecue, and comedians to raise funds for the miners.
- The event includes comedian Drew Morgan and aims to bring people together to support the cause.
- Donations are encouraged, with a suggested amount of $20, and it's free for UMWA members.
- Even if unable to attend in person, there's a link to donate directly to support the miners.
- Supporting organized labor fights like this can benefit everyone by raising wages in the area.
- Showing support for the miners is not just about being neighborly; it also serves self-interest and can have a wider positive impact.

# Quotes

- "They just want a fair shake."
- "These fights that organized labor engages in, they really do benefit everybody."
- "Showing a little bit of support here is not only the neighborly thing to do."

# Oneliner

Miners in Alabama strike for fair conditions, needing support with a strike pantry and fundraiser, showing solidarity benefits all.

# Audience

Supporters of fair labor practices

# On-the-ground actions from transcript
- Donate to support the striking miners (suggested)
- Attend the Alabama Strike Fest fundraiser event (exemplified)

# Whats missing in summary

The emotional impact of supporting the miners and the broader community benefits from raising awareness and providing aid.

# Tags

#LaborRights #SupportMiners #Solidarity #Fundraiser #FairWages