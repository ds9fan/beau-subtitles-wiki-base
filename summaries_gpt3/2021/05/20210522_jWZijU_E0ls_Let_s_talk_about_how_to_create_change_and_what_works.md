# Bits

Beau says:

- Questions the effectiveness of marches and rallies in creating real change, referencing the situation in North Carolina where they seem to have little impact on decision-makers.
- Emphasizes that all forms of activism work together as a diversity of tactics to create systemic change, including petitioning, marches, direct involvement, calling senators, and electoralism.
- Draws parallels between movements like Black Lives Matter and historical events such as the American Revolution to illustrate the concept of a movement versus a moment.
- Encourages individuals to focus on utilizing their unique skills and strengths to contribute to the broader goal of systemic change.
- Stresses the importance of consistent effort and a diversity of tactics in pushing forward for change, even when individual actions may seem insignificant.
- Advises against counting on any single event or action to make a significant impact, as real change often requires years of behind-the-scenes work and organizing.
- Emphasizes the need for diversity in tactics and promoting individuals who approach the same goal in different ways over creating a clique of like-minded individuals.
- Acknowledges the challenges of staying motivated in activism, noting that each small action contributes to the larger movement for change.
- Urges maintaining forward momentum and focus on the long-term goal of creating a fair and just world through collective efforts.

# Quotes

- "It's not a moment. It's a movement."
- "Each little piece, each little action is one moment in that movement."
- "Whatever you're best at. Whatever you're really good at. Those are the skills you need to use to further that drive for systemic change."

# Oneliner

What works and what doesn't in creating systemic change: diversity of tactics, consistent effort, and focusing on the long-term goal of a movement over individual events.

# Audience

Activists, community organizers

# On-the-ground actions from transcript

- Utilize your unique skills and strengths to contribute to the drive for systemic change (implied).
- Promote individuals who approach common goals in different ways to encourage diversity in tactics (implied).
- Stay engaged in activism, attend marches, rallies, and remain active in pursuing change (implied).

# Whats missing in summary

The full transcript provides a comprehensive understanding of the importance of diversity in tactics, consistency in efforts, and the long-term focus required for creating real systemic change.

# Tags

#SystemicChange #Activism #DiversityInTactics #LongTermFocus #CommunityOrganizing