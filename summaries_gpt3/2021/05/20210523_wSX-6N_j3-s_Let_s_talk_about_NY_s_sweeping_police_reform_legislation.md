# Bits

Beau says:

- Explains the legislation from New York regarding police reform, questioning whether it's accurately described as sweeping.
- Emphasizes the importance of using the minimum necessary force to effect an arrest as the gold standard of policy.
- Points out that the legislation clearly defines the minimum amount of force needed for an arrest.
- Stresses that the legislation prohibits escalating a situation and then using force or using force on someone already subdued.
- Notes that while the union is pushing back against the legislation, it's a unique template that could be replicated in other states.
- Clarifies that if officers are unsure if they can use force, they shouldn't, as they should always aim to use the minimum necessary force.
- Explains that the legislation requires officers to deescalate, exhaust other means before force, and doesn't create a strict step-by-step process.
- Mentions that some attorneys believe the legislation may make it easier to create reasonable doubt, but the legislation will still make it easier to charge officers.
- Shares feedback from a former cop and deputy who found the legislation to be common sense.

# Quotes

- "The minimum necessary to effect the arrest. That's what you should use. That's the gold standard of policy around the country."
- "If you don't know if you're allowed to use force, you're not."
- "It's good legislation. And it doesn't handcuff law enforcement. It makes them try to deescalate."
- "This is just common sense."
- "Y'all have a good day."

# Oneliner

Beau explains the New York legislation on police reform, focusing on using the minimum necessary force and promoting de-escalation. 

# Audience

Lawmakers, Police Departments

# On-the-ground actions from transcript

- Read through and understand the legislation (suggested)
- Advocate for similar legislation in other states (suggested)

# Whats missing in summary

The full transcript provides additional context and personal anecdotes that enrich the understanding of the legislation and its implications.

# Tags

#PoliceReform #Legislation #UseOfForce #Deescalation #LawEnforcement