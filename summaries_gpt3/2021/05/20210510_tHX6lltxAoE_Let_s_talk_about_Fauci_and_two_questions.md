# Bits

Beau says:

- Beau addresses the seasonal aspect of mask usage and the need to potentially get used to it.
- He expresses his willingness to continue wearing a mask as long as experts deem it beneficial to society.
- Beau shares that wearing a mask doesn't bother him and, in fact, he finds it somewhat enjoyable as he hasn't had to interact with unwanted high school acquaintances.
- He dismisses the notion that mask-wearing is a control mechanism, attributing such beliefs to a desire for order in a chaotic world.
- Beau references the effectiveness of facial recognition software demonstrated by the federal government, suggesting that any authoritarian group with such technology wouldn't encourage face coverings for control.
- He believes that the official COVID-19 numbers are likely undercounted based on personal experiences and lack of widespread testing in his area.
- Beau reiterates his stance that masks are not a form of control and expresses his comfort with wearing masks, even citing previous experiences with gas masks.

# Quotes

- "The world is a chaotic place and these theories attempt to make sense of the chaos."
- "I definitely believe the official numbers are low."
- "I do not believe masks are some kind of control mechanism."
- "It doesn't make sense and yeah it's not a big deal."
- "If it helps yeah I'll keep doing it."

# Oneliner

Beau addresses seasonal mask usage, expresses willingness to wear one based on expert consensus, and dismisses mask-wearing as a control mechanism.

# Audience

Individuals concerned about mask usage and control narratives.

# On-the-ground actions from transcript

- Follow expert recommendations on mask-wearing (implied).
- Advocate for widespread testing and accurate reporting of COVID-19 cases (implied).
- Encourage others to wear masks based on scientific evidence (implied).

# Whats missing in summary

The detailed nuances and personal anecdotes shared by Beau in the full transcript.

# Tags

#MaskUsage #COVID19 #ControlNarratives #ExpertConsensus #FacialRecognition