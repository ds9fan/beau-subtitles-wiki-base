# Bits

Beau says:

- Talks about the importance of asking the right question instead of focusing on the wrong question.
- Presents a story about a reporter named Alexandra Peterson whose apartment is blown up by a SWAT team.
- Points out the issue of debating whether Alexandra knew about the illegal activities happening below her apartment.
- Emphasizes that the real question should be about why the SWAT team used excessive force and caused additional damage.
- Expresses concern about society shifting blame to collateral damage victims instead of questioning law enforcement's actions.
- Criticizes the nation for getting distracted by irrelevant debates instead of addressing the actual problem of excessive force.
- Suggests that the country has lost sight of critical issues by engaging in misguided debates.
- Urges for a focus on asking the right questions amidst various ongoing debates and controversies.

# Quotes

- "The question is not whether or not Alexandra knew about what was happening in her building. The question is why did the SWAT team use too much force?"
- "I don't think that when a nation begins debating that amongst themselves, rather than discussing the obvious problem of too much force being used, I don't think that sits well for the future of that nation either."
- "There are a lot of questions being asked, and almost none of them are the right question."

# Oneliner

Beau stresses the importance of asking the right questions, focusing on why excessive force was used by law enforcement rather than blaming collateral damage victims.

# Audience

Advocates and Activists

# On-the-ground actions from transcript

- Question law enforcement's use of excessive force (exemplified)
- Shift focus to addressing the real issues instead of irrelevant debates (exemplified)

# Whats missing in summary

The emotional impact and Beau's tone throughout the talk.

# Tags

#AskingTheRightQuestions #PoliceBrutality #ExcessiveForce #SocialJustice #NationalDebates