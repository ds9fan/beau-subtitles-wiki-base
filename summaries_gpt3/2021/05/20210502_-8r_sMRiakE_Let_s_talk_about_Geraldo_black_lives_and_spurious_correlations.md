# Bits

Beau says:
- Responding to a tweet about statistics and Geraldo Rivera's comments on Black lives matter.
- Expresses how the focus always seems to be on crime when talking about Black lives matter.
- Criticizes the narrative that reinforces the idea that Black people are criminals, making it easier for law enforcement to handle them.
- Points out that statistics on crime don't show which skin tone kills the most but rather who gets arrested the most.
- Mentions the flaws in statistics, especially when missing significant information.
- Questions why statistics are divided by race if they don't provide useful information and may perpetuate systemic racism.
- Suggests that economic status might be a more relevant factor to analyze in relation to crime rates.
- Emphasizes the importance of establishing causal relationships in statistics rather than relying on spurious correlations.
- Concludes by urging to look for causal relationships in statistics to make informed policy decisions.

# Quotes

- "There is no evidence to suggest a causal link between skin tone and whether or not you are a killer."
- "When somebody uses this, it is inherently racist."
- "We have to look for the causal relationships and not for spurious correlations."
- "If we are basing policy on random information, we are probably not going to get good results."
- "Y'all have a good day."

# Oneliner

Beau addresses misconceptions around statistics on Black lives matter, urging to focus on causal relationships over spurious correlations.

# Audience

Policy analysts, activists

# On-the-ground actions from transcript

- Challenge and correct misconceptions about statistics and race (suggested)
- Advocate for policies based on causal relationships rather than spurious correlations (suggested)

# Whats missing in summary

The full transcript provides a detailed breakdown of the flaws in using statistics to perpetuate racial stereotypes and the importance of focusing on causal relationships over correlations.

# Tags

#Statistics #BlackLivesMatter #Racism #CausalRelationships #PolicyAnalysis