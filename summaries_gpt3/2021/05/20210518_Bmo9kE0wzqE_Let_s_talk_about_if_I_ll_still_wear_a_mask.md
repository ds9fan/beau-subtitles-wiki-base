# Bits

Beau says:

- Expresses frustration at Trump's lack of mask-wearing as a failure of leadership and civic duty to set an example for the American people.
- Stresses the importance of changing societal mindset over changing laws to effect real change.
- Mentions Biden administration's commitment to science-based guidance, indicating that fully vaccinated individuals have low risk and may not need masks in certain scenarios.
- Plans to continue wearing a mask personally as a visual reminder that the pandemic is not over and more people need to get vaccinated.
- Worries that without establishments checking vaccination status, unvaccinated individuals may exploit the lack of enforcement to avoid wearing masks, especially in areas with vaccine hesitancy.
- Believes in the power of visual cues like masks to encourage more people to get vaccinated and sees it as a social science issue.

# Quotes

- "You don't need a law to tell you to be a good person."
- "I will continue to wear mine. The guidance is that. It's guidance."
- "I really do believe that the social science would suggest more people would get vaccinated if they saw a bunch of masks."

# Oneliner

Beau stresses the importance of personal responsibility in wearing masks as a visual reminder to encourage vaccination, especially in areas with hesitancy.

# Audience

Individuals, Community Members

# On-the-ground actions from transcript

- Wear a mask as a visual reminder to encourage others to get vaccinated (implied)
- Encourage establishments to enforce mask-wearing or vaccination checks (implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on the importance of continued mask-wearing as a visual cue to remind people about the ongoing need for vaccination and COVID-19 precautions.

# Tags

#MaskWearing #Vaccination #PublicHealth #Leadership #CommunityResponsibility