# Bits

Beau says:

- Addressing the hesitancy of a certain group to get the COVID shot.
- Grandma's reluctance to get vaccinated due to fears related to the Tuskegee experiment.
- Explaining that Grandma's concerns are not irrational but based on historical events.
- The challenge of using rhetoric to persuade Grandma to get vaccinated.
- The impact of historical events like Tuskegee on individuals who experienced them firsthand.
- Emphasizing the importance of presenting factual information to address Grandma's concerns.
- Comparing past events like Tuskegee to the current COVID situation in terms of study and choice.
- Pointing out that Grandma's perspective is shaped by her life experiences and memories.
- Acknowledging the difficulty in finding common ground due to differences in experiences.
- Suggesting that having a grandchild around might help in addressing Grandma's concerns.

# Quotes

- "It's not irrational. It's not irrational."
- "You get to choose whether you're in the group that gets treatment, its protection or not."
- "She's not a rational. She's got a reason."
- "She has a reason to believe what she believes is to come at it with facts."
- "There are people alive today who were alive when it happened."

# Oneliner

Beau addresses the hesitancy of a specific group to get vaccinated, focusing on Grandma's valid concerns rooted in historical events and the importance of presenting facts.

# Audience

Family members

# On-the-ground actions from transcript

- Have open and honest conversations with hesitant family members about vaccination (suggested)
- Present factual information and historical context to address concerns (exemplified)

# Whats missing in summary

The full transcript provides a detailed and empathetic exploration of addressing vaccine hesitancy rooted in historical trauma, offering insights on approaching the issue with understanding and facts.

# Tags

#VaccineHesitancy #HistoricalTrauma #FamilyCommunication #FactualInformation #Empathy