# Bits

Beau says:
- Explains the unique role of street medics at demonstrations expected to get wild.
- Talks about how street medics experience a lull in activity during cold weather and start cooperating and coordinating during warm weather.
- Describes the challenges faced by street medics when they get bored, leading to cliques, in-fighting, and decreased effectiveness.
- Suggests ways to maintain effectiveness in groups like community networks or mutual assistance groups during lulls.
- Recommends training, staying on mission, and incorporating fun activities to prevent cliques and maintain focus.
- Advocates for healthy competition and mixing opposing groups to foster camaraderie.
- Encourages political engagement beyond election seasons, focusing on ongoing issues and causes.
- Stresses the importance of staying on mission, avoiding infighting, and maintaining effectiveness in achieving goals.
- Urges individuals to reach out and stay connected with their circle to remain effective in their activism.

# Quotes

- "Street medics, well they don't have anything to do. They get bored and when they get bored, well that's when cliques start to form."
- "Find something to focus on. Don't just rally around the politician during election season."
- "You have more contacts. And it makes you and your circle a little bit more politically valuable."
- "You can't let up."
- "If you want to stay effective, if you want to actually accomplish stuff, you can't let up."

# Oneliner

Beau explains the importance of staying focused, avoiding cliques, and maintaining effectiveness in activism, drawing lessons from street medics during demonstrations.

# Audience

Community activists

# On-the-ground actions from transcript

- Reach out to your circle of activists to stay connected and effective (suggested).
- Train and stay focused on mission objectives in community groups (implied).
- Organize fun activities or events within your activist network to build camaraderie (implied).
- Campaign for ongoing causes beyond election seasons (implied).

# Whats missing in summary

The detailed examples and nuances of Beau's storytelling and insights.

# Tags

#CommunityActivism #MaintainingEffectiveness #PoliticalEngagement #ActivistNetworks #StayingFocused