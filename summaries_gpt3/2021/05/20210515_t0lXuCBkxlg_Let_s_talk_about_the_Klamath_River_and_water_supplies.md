# Bits

Beau says:

- The Klamath River basin in Oregon and northern California is facing a drought, impacting people along the river and in reclamation programs.
- Farmers are being told there won't be water for irrigation, leading to a potential conflict with endangered salmon species.
- Low water levels in the river are causing bacteria to flourish, already impacting 97% of tested salmon, especially juveniles.
- The upcoming national news coverage might frame the issue as "farmers versus fish," overlooking the Yurok native group dependent on salmon for their way of life.
- The Yurok tribe signed a treaty with the U.S. in the 1850s to maintain their fishing lifestyle within a specific geographic area.
- Despite having good lawyers, the Yurok might be forgotten in national coverage due to powerful interests at play.
- It's vital to recognize and support those on the ground who lack influence over decisions impacting their livelihoods.
- Confined groups like the Yurok have a moral right to maintain their traditional lifestyle without external decisions lowering their standard of living.
- The drought in the Klamath River basin raises questions about water rights and conflicting claims between longstanding farmers and indigenous groups.
- More disputes over water rights are expected with increasing droughts, but it's crucial not to overlook the local communities most impacted.

# Quotes

- "Farmers versus fish."
- "Don't forget that there are people on the ground in these areas that are going to be impacted that have no control over what happens."
- "But who has more of a claim – the Yurok or people who showed up during the gold rush?"
- "There will be more and more disputes over water rights."
- "They're the people that may need the most support."

# Oneliner

The Klamath River basin faces a drought crisis, pitting farmers against endangered salmon and overlooking the indigenous Yurok group's vital connection to the river.

# Audience

Environmental activists, policymakers, community organizers

# On-the-ground actions from transcript

- Support the Yurok native group in maintaining their traditional fishing lifestyle (implied).
- Advocate for equitable water distribution to all affected groups in the Klamath River basin (implied).
  
# Whats missing in summary

The emotional impact on local communities and the urgency of addressing water rights disputes and supporting marginalized groups like the Yurok tribe are key takeaways from the full transcript.

# Tags

#KlamathRiver #DroughtCrisis #WaterRights #IndigenousRights #CommunitySupport #EnvironmentalJustice