# Bits

Beau says:

- Tennessee plans to require signs for restroom identity policies.
- Beau questions the necessity and impact of these signs.
- Draws a historical comparison to discriminatory signs in the South.
- Beau argues against moral approaches to bigots.
- Mentions the economic impact of military installations like Fort Campbell, Kentucky.
- Installation commanders have power over off-limits areas based on command.
- Beau suggests economic repercussions for businesses not displaying the required signs.
- Stresses the financial importance of being accepting and inclusive.
- Predicts repercussions for establishments without the signs, affecting tourism and business.
- Urges voters in Tennessee to reconsider supporting short-sighted leaders.
- Warns about the consequences for small businesses due to political decisions.
- Encourages reflection on the implications of discriminatory practices.

# Quotes

- "It is more profitable to be a good person."
- "You may not care about morality, but I bet you care about that money."
- "A whole lot of that situation has to do with their faulty leadership."

# Oneliner

Tennessee plans restroom identity policy signs, sparking economic and moral debates, urging reconsideration of leadership's shortsightedness.

# Audience

Tennessee voters

# On-the-ground actions from transcript

- Vote thoughtfully in Tennessee elections (implied)
- Support businesses displaying inclusive signs (implied)

# Whats missing in summary

The full transcript provides additional details on the economic impact of discriminatory practices in Tennessee.