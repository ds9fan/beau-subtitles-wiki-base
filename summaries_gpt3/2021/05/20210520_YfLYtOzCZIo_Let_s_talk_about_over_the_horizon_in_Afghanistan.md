# Bits

Beau says:

- The withdrawal of US forces from Afghanistan is happening, but no country has stepped up to provide a token security force.
- The US military plans to maintain over-the-horizon capabilities, primarily through airstrikes.
- US forces are looking for countries to host them near Afghanistan despite leaving the country.
- Most airstrikes in Afghanistan are currently carried out by the Afghan Air Force, maintained by US contractors who will likely leave.
- The Afghan military has changed in the last 20 years and may struggle to hold its own without US contractor support.
- General McKenzie hinted at the possibility of US forces going back into Afghanistan for special operations and drone strikes.
- The reliance on drones for surveillance and airstrikes raises concerns about civilian casualties and accuracy.
- The use of drones without human intelligence on the ground poses challenges.
- Beau suggests that without a token security force, there may be a return of some US special operations troops within a year.
- The withdrawal from Afghanistan appears more like a pause than an end, with uncertainties about the national government's ability to stand alone.

# Quotes

- "It's going to be worse than a US presence, as bad as that was."
- "The real issue here is that the US was real quick to go in without any clear idea on how to get out."
- "I don't think when people were talking about we need to leave Afghanistan, that this is what they were picturing."

# Oneliner

The US withdrawal from Afghanistan leaves uncertainties about security and the future of the Afghan military, relying heavily on drones for over-the-horizon capabilities.

# Audience

Foreign Policy Analysts

# On-the-ground actions from transcript

- Support organizations providing aid and assistance in Afghanistan (implied)
- Stay informed on developments in Afghanistan and advocate for responsible foreign policy decisions (implied)

# Whats missing in summary

Analysis of potential humanitarian impacts and civilian safety concerns in Afghanistan due to increased reliance on drone strikes.

# Tags

#Afghanistan #USForces #OverTheHorizon #Drones #ForeignPolicy