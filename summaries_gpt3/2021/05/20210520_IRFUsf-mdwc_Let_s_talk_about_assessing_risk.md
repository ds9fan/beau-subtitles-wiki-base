# Bits

Beau says:

- Explains the concept of assessing risk and how it factors into official statements regarding the use of force.
- Challenges the notion that fear alone constitutes a threat by providing examples of intent, capability, and opportunity.
- Describes the traditional formula for determining a threat as intent plus capability, and how an additional criteria of opportunity has been added over time.
- Illustrates the concept with a scenario of someone having intent and capability but lacking the current opportunity to pose a threat.
- Introduces a newer terminology of ability jeopardy, which equates to intent and opportunity in assessing threats.
- Emphasizes the importance of evaluating whether a situation truly warrants the use of force based on morality rather than justifiable fear.
- Advocates for a shift towards considering the necessity of using force instead of relying on fear-based justifications.

# Quotes

- "Fear doesn't make a threat."
- "This is how you determine what a threat is."
- "Maybe we should start leaning back towards the morality side of it."
- "Y'all have a good day."

# Oneliner

Beau explains assessing risk, challenges fear as a threat indicator, and advocates for morality over fear-based justifications in using force.

# Audience

Decision-makers, law enforcement.

# On-the-ground actions from transcript

- Revisit risk assessment protocols (implied).
- Challenge fear-based justifications for use of force (implied).

# Whats missing in summary

In-depth examples and further exploration of the impact of morality in assessing threats.

# Tags

#RiskAssessment #UseOfForce #Morality #ThreatAssessment #LawEnforcement