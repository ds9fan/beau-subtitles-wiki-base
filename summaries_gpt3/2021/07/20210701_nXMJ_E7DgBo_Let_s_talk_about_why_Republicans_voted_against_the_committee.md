# Bits

Beau says:

- The US House of Representatives voted on forming a committee to look into the events of January 6th at the Capitol.
- Only two Republicans voted in favor of investigating the Capitol incident, raising eyebrows.
- Some Republicans claimed it was the work of left-wing agitators, BLM, or the FBI to deflect blame.
- Beau questions why Republicans who made these claims wouldn't want an investigation to reveal the truth.
- Republicans voting against the investigation leads to only two conclusions: they believe the claims or they were lying.
- Beau suggests that Republicans on the committee may work to undermine it and turn it into a circus.
- Republican leaders aim to keep their base in the dark about what truly happened on January 6th.
- Enabling Trump and failing to serve as a checks and balance against executive power started the chain of events leading to January 6th.
- Republicans want to keep their constituents confused to manipulate them easily.
- Beau concludes by raising awareness of the Republican Party's actions regarding the Capitol incident.

# Quotes

- "Either they believe that these things happened and they're okay with it, or they know they were lying."
- "Because if they accept reality, well, they might be pretty mad with the Republican Party."
- "They want to keep their constituents in an echo chamber of confusion."

# Oneliner

The US House vote on investigating January 6th reveals Republican reluctance to uncover the truth, leading to manipulation of their base by keeping them in the dark.

# Audience

American voters

# On-the-ground actions from transcript

- Reach out to your representatives and demand transparency and accountability in investigating the events of January 6th (suggested).
- Stay informed about political events and hold elected officials accountable for their actions (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's stance on investigating the events of January 6th and their potential motives for avoiding transparency.

# Tags

#USPolitics #January6th #RepublicanParty #Accountability #Transparency