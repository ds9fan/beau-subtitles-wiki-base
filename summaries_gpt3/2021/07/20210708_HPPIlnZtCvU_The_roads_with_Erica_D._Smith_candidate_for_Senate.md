# Bits

Beau says:

- Beau interviews former state Senator Erica Smith, a candidate for Senate in North Carolina, to learn about her background and platform.
- Erica describes her upbringing on a family farm in North Carolina and her trajectory from engineer to public school educator to state senator.
- She shares her experiences in Uganda on a mission trip and the impact it had on her perspective on community building and service.
- Erica outlines her priorities if elected to the US Senate, focusing on income inequality, rural healthcare, and environmental justice through the Green New Deal.
- She advocates for universal broadband access, Medicare for all, and addressing the challenges faced by rural hospitals.
- Erica stresses the importance of canceling student loan debt, raising the minimum wage, and supporting the PRO Act for workers' rights.
- She details her stance on gun control, including banning assault rifles and implementing common sense policies to prevent gun violence.
- Erica also addresses the pre-funding mandate for the US Postal Service and proposes postal banking as a solution.
- She concludes by inviting viewers to learn more about her platform and support her campaign.

# Quotes

- "We're running a movement to create real structural change so that our government works for all of us, not just the wealthy, not just the well-connected, but all of us."
- "Policy is personal for me, Bo. That's why I fight so hard."
- "Healthcare is a basic human right. No one should have to ration their insulin just to put food on the table."
- "Common sense protections for our community."
- "We are truly one of us for all of us."

# Oneliner

Former state Senator Erica Smith outlines her trajectory from engineer to public servant, advocating for income equality, healthcare access, environmental justice, and more in her run for the US Senate.

# Audience

Voters in North Carolina

# On-the-ground actions from transcript

- Support Erica Smith's campaign by visiting www.ericaforus.com (implied)
- Learn more about her platform and policy proposals (implied)
- Invest in politicians who champion causes that support working families (implied)

# Whats missing in summary

Details on Erica Smith's advocacy for cancelling student loan debt and raising the minimum wage are not fully captured in the summary.

# Tags

#NorthCarolina #USsenate #IncomeInequality #HealthcareAccess #EnvironmentalJustice #PolicyAdvocacy