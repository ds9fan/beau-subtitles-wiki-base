# Bits

Beau says:

- Shares insights from his experience as a security consultant and being part of protective details.
- Explains the concept of inner and outer rings in security details.
- Describes how the outer ring, often comprising inexperienced individuals like local cops, serves as a deterrent.
- Notes a common breakdown in security during the post-event meet and greet phase.
- Emphasizes the importance of maintaining security posture even after the main event is over.
- Draws parallels between security details and the public health issue in the United States.
- Advises individuals to elevate their security posture regarding public health guidelines.
- Urges people to go beyond the standard guidance to account for those who are not following protocols.
- Warns of a potentially rough fall if attention to public health measures wanes.
- Encourages staying vigilant and proactive until the situation is under control.

# Quotes

- "The job's not over just because the main event is."
- "You should probably elevate your security posture a little bit when it comes to this."
- "That chain is broke."
- "The job isn't over until the client is secure."
- "A whole lot of people aren't paying attention."

# Oneliner

Beau explains how maintaining security posture post-event applies to public health measures, urging individuals to go beyond guidelines as attention wanes.

# Audience

Individuals

# On-the-ground actions from transcript

- Elevate your security posture regarding public health guidelines (suggested).
- Go beyond standard guidance and take additional precautions, like wearing masks inside all buildings (suggested).

# Whats missing in summary

Importance of remaining vigilant and proactive in public health measures until the situation is under control.

# Tags

#SecurityDetails #PublicHealth #Vigilance #Proactive #Guidelines