# Bits

Beau says:

- Explains a law in Tennessee requiring businesses to post signs allowing individuals to use the bathroom corresponding to their identity.
- Skips moral arguments and focuses on economic impact and constitutional issues.
- ACLU challenges the law in court, and a federal judge issues an injunction deeming it likely unconstitutional.
- Questions why groups using the Constitution as a backdrop keep writing unconstitutional legislation.
- Argues that voters should have access to information about lawmakers who support unconstitutional laws.
- Advocates for consequences for lawmakers writing bills found to be unconstitutional, such as being barred from public office.
- Suggests that legislation constantly challenging constitutional limits should be a warning sign.
- Emphasizes the importance of upholding the basic principles of the Constitution.

# Quotes

- "Why is it that a group of people who often use the Constitution as their backdrop keep writing legislation that is unconstitutional?"
- "You can't pretend to be a patriot and uphold the basic principles of this country if you are actively trying to undermine them."
- "If you write a bill that is found to be unconstitutional, you're barred from public office."
- "The fact that we have so much legislation headed to the Supreme Court with the express purpose of testing the limits of those hard limits, trying to push them as far as they can go, that should be a warning sign."
- "Maybe we should know the legislators who are out there who don't actually support the Constitution."

# Oneliner

Beau questions the logic behind lawmakers writing unconstitutional legislation and advocates for consequences for those who do, urging voters to have access to information about such actions.

# Audience

Voters, Constitution supporters

# On-the-ground actions from transcript

- Advocate for transparency in lawmakers' actions by supporting initiatives that track and make public information about unconstitutional legislation (suggested).
- Stay informed about laws and bills being introduced in your area and hold lawmakers accountable for upholding constitutional principles (implied).

# Whats missing in summary

The full transcript provides a detailed exploration of the impact of unconstitutional legislation on the Constitution's integrity and suggests accountability measures for lawmakers who introduce such bills.

# Tags

#Constitution #UnconstitutionalLegislation #Accountability #Transparency #Lawmakers