# Bits

Beau says:
- Addresses a topic he usually avoids discussing: other people on YouTube and their engagement in charity and mutual aid.
- Expresses his reluctance to join such dialogues due to viewing it as a waste of resources, but acknowledges the necessity this time.
- Talks about channels on YouTube that are left-leaning or anti-authoritarian but do not participate in charity or mutual aid.
- Mentions being sent screenshots of other channels engaging in these activities, which was meant to make him "look good" but didn't.
- Points out a commonality among channels engaging in mutual aid: many were journalists or organizers before YouTube.
- Emphasizes the importance of having a network and team for effective mutual aid efforts.
- Suggests reaching out to channels not engaged in mutual aid to offer assistance or collaboration.
- Acknowledges that some may be engaging in mutual aid for self-serving reasons, but underscores the importance of the end goal of helping those in need.
- Urges not to let ideological differences hinder collaboration for the greater good.
- Encourages utilizing resources from different channels for mutual aid efforts, regardless of their motivations.

# Quotes
- "I'm willing to bet almost all of the channels will. You just got to ask. Reach out to them."
- "You do not have the luxury of being ideologically pure. You need the resources and those channels can help you get them."
- "If you're concerned about it, you probably have that network. So you could team up."

# Oneliner
Beau addresses the lack of mutual aid engagement in certain YouTube channels, urging collaboration for the greater good.

# Audience
YouTube content creators

# On-the-ground actions from transcript
- Reach out to YouTube channels not engaged in mutual aid to offer collaboration (suggested)
- Team up with channels for mutual aid efforts (implied)

# Whats missing in summary
The full transcript provides a detailed insight into the importance of collaboration and utilizing networks for mutual aid efforts, urging individuals to look beyond ideological differences for the greater good.

# Tags
#YouTube #MutualAid #Collaboration #CommunitySupport #Resources