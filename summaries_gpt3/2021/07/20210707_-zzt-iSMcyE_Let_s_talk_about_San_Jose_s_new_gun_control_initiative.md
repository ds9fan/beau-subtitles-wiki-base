# Bits

Beau says:

- San Jose is planning to impose a fee on firearm ownership, but legal challenges are expected due to charging money to deter a constitutional right.
- The initiative aims to deter gun ownership and offset violence costs, but the impact may disproportionately affect certain demographics, particularly the poor.
- Systemic issues like institutionalized racism and inherited disadvantages could make it harder for poor individuals to afford firearm ownership, leading to violations of the law.
- The law is colorblind on the surface, but its enforcement could have racist impacts due to existing conditions and systemic issues in the country.
- Beau suggests that laws like this can have unintended racial consequences and may be selectively enforced, showing how colorblind laws can lead to discriminatory outcomes.
- Acknowledging the intersection of race with the law is vital for building a fair society, whether through evidence-based arguments or storytelling narratives.
- Building a country where everyone gets a fair chance requires confronting and addressing systemic issues and acknowledging the truth, whether through data or personal stories.
- Beau challenges the reluctance to openly address racial issues and suggests that ignoring these issues may indicate a desire to maintain existing discriminatory systems.
- Encourages critical thinking about how race intersects with the law and the importance of discussing theories that illuminate racial disparities.

# Quotes

- "Systemic issues like institutionalized racism and inherited disadvantages could make it harder for poor individuals to afford firearm ownership."
- "Laws like this can have unintended racial consequences and may be selectively enforced."
- "Acknowledging the intersection of race with the law is vital for building a fair society."
- "Building a country where everyone gets a fair chance requires confronting and addressing systemic issues."
- "Ignoring racial issues may indicate a desire to maintain existing discriminatory systems."

# Oneliner

San Jose's firearm ownership fee initiative sheds light on the potential racial impacts of colorblind laws and the importance of addressing systemic issues for a fair society.

# Audience

Policy advocates, activists

# On-the-ground actions from transcript

- Address systemic issues in your community through advocacy and support for marginalized groups (exemplified)
- Educate others on the impact of colorblind laws on different demographics (exemplified)

# Whats missing in summary

The full transcript provides a deeper exploration of how systemic racism intersects with laws, urging for critical reflection and open discourse on addressing racial disparities in society.

# Tags

#SystemicRacism #GunControl #ColorblindLaws #CommunityAdvocacy #FairSociety