# Bits

Beau says:

- Talking about the real-world numbers in LA related to the current public health issue.
- LA County is experiencing an uptick in new cases for five consecutive days.
- The percentage of hospitalizations remains low despite the increase in cases.
- Out of those hospitalized, none are fully vaccinated with J&J, Pfizer, or Moderna vaccines.
- The effectiveness of vaccination in preventing hospitalizations is evident in LA County.
- There may be rare breakthrough cases or individuals who did not complete their vaccine series.
- The debate about the effectiveness of vaccines is settled; they work.
- Politicians, particularly Republicans, may have a vested interest in vaccine failure for political gain.
- Republicans are hoping for failures to regain power, even if it means their constituents suffer.
- The reality in LA County shows that every COVID patient admitted was not fully vaccinated.

# Quotes

- "Real world right there. That's real world."
- "The question about whether or not it works is over. It does."
- "They are hoping that everything goes wrong and that their constituents suffer."

# Oneliner

Beau talks about the effectiveness of vaccines in preventing hospitalizations, revealing that in LA County, all COVID patients admitted were not fully vaccinated, settling the debate on vaccine efficacy.

# Audience

Public health officials, policymakers

# On-the-ground actions from transcript

- Get vaccinated with J&J, Pfizer, or Moderna vaccines (implied)
- Advocate for vaccination to prevent hospitalizations (implied)
- Combat misinformation about vaccine effectiveness (implied)

# Whats missing in summary

The detailed breakdown of LA County's numbers and the implications for vaccine effectiveness.

# Tags

#Vaccines #PublicHealth #LA #COVID-19 #Statistics