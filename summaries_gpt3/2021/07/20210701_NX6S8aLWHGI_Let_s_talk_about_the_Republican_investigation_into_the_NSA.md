# Bits

Beau says:

- Republicans refused to conduct an investigation into the events of January 6th on Capitol Hill, despite video evidence.
- Instead, they have chosen to launch an investigation into the NSA, citing Tucker Carlson's unbacked claims.
- Devin Nunes, known for his Twitter cow feud, will lead this investigation into the NSA.
- Beau questions Nunes' credibility as a defender of the NSA leading this investigation.
- Beau suggests that if the Republican Party genuinely cares about civil liberties, legislation is needed rather than just an investigation.
- He advocates for strong safeguards to protect Americans' rights and potentially rolling back intrusive NSA programs.
- Beau sees this investigation as a distraction and misinformation campaign aimed at the Republican base.
- He believes it's an attempt to shift focus from the events of January 6th.
- Beau suspects the timing of this investigation announcement isn't coincidental and vows to follow its progress.

# Quotes

- "Contrary to popular perception, the NSA is not and never was recording or listening to millions of Americans' phone calls." - Kevin Nunes
- "If you really believe that this occurred, we don't need an investigation. We need legislation." - Beau

# Oneliner

Republicans deflect from January 6th with an NSA investigation based on Tucker Carlson's claims, prompting Beau to call for legislation over investigations.

# Audience

Activists, Civil Liberties Defenders

# On-the-ground actions from transcript

- Contact local representatives to advocate for legislation protecting civil liberties (implied)
- Join civil rights organizations pushing for stronger safeguards against government intrusion (implied)

# Whats missing in summary

Full context and depth of Beau's analysis and skepticism towards the Republican-led investigation into the NSA.

# Tags

#CapitolHill #NSA #Investigation #CivilLiberties #RepublicanParty