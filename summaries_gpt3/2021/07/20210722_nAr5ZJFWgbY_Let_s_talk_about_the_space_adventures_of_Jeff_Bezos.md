# Bits

Beau says:

- Recalls childhood fascination with a giant rocket in Florida and the idealistic nature of space travel.
- Expresses initial concern over billionaires turning space exploration into a hobby.
- Mentions the "overview effect" experienced by astronauts, where they realize the fragility of Earth from space.
- Comments on Jeff Bezos going to space and his subsequent interview about climate change.
- Criticizes Bezos' idea of polluting space by moving heavy industry there.
- Emphasizes the importance of not repeating past mistakes like burning stuff or burying toxic waste.
- Advocates for finding a balance with Earth and changing the profit-driven mindset to push humanity forward.

# Quotes

- "Let's not pollute space. Let's not move all heavy industry."
- "Maybe the best idea is to figure out how to live in some kind of balance with earth."
- "That personal profit motivator rather than the drive to push humanity forward, that is something we have to change."

# Oneliner

Beau questions the wisdom of polluting space with heavy industry and advocates for prioritizing Earth's protection over interplanetary endeavors.

# Audience

Activists, Environmentalists, Space enthusiasts

# On-the-ground actions from transcript

- Advocate for environmental protection on Earth (implied)
- Educate others about the importance of sustainability (implied)
- Support initiatives that aim to protect the planet (implied)

# Whats missing in summary

Beau's emotional delivery and personal connection to the topic might be best experienced by watching the full video.

# Tags

#SpaceExploration #ClimateChange #EnvironmentalProtection #Sustainability #Billionaires #ProfitMotivation