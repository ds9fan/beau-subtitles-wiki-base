# Bits

Beau says:

- Family summer fun turned serious when a child almost drowned.
- Child rescued by family members after not responding in the water.
- Quick action taken to clear child's lungs and call 911.
- EMS arrived remarkably fast, within five to six minutes.
- Child needed to be checked out despite appearing fine.
- Importance of knowing CPR and taking classes emphasized.
- Acknowledgment that sometimes it's up to individuals to act quickly.
- Encourages getting certified in CPR as it can save a life.
- Stress on the need for quick response in emergencies.
- Reminder that knowledge and skills can alter outcomes significantly.
- Despite being okay, the child still needed medical evaluation.
- Urges people to be prepared for situations when help may not arrive in time.
- Knowledge is emphasized as a valuable tool that can be carried for a lifetime.
- Encouragement to prioritize learning life-saving skills.
- Closing thought on the importance of being prepared for emergencies.

# Quotes

- "Knowledge weighs nothing. You will carry it with you the rest of your life."
- "If you have the ability, please, please take a course."
- "There is no response that is going to get there fast enough."
- "A little bit of knowledge can greatly alter the outcome of events."
- "Have a good day."

# Oneliner

Family summer fun turned serious as Beau recounts a near-drowning incident, urging the importance of quick action, CPR knowledge, and individual preparedness for emergencies.

# Audience

Parents, caregivers, community members

# On-the-ground actions from transcript

- Take a CPR class (suggested)
- Get certified in CPR (suggested)
- Stay prepared for emergencies (implied)

# Whats missing in summary

The full transcript provides a detailed account of a near-drowning incident during a family summer outing, stressing the significance of CPR knowledge, quick response in emergencies, and individual preparedness for unforeseen situations.

# Tags

#SummerFun #FamilySafety #CPR #EmergencyPreparedness #LifeSavingSkills