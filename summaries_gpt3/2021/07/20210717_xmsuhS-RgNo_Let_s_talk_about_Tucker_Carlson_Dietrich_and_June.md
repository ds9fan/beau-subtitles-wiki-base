# Bits

Beau says:

- A plea to Tucker Carlson to publicly acknowledge his vaccination status to influence hesitant viewers like June.
- Drawing attention to the political divide in vaccination rates based on political affiliation, not medical guidance.
- Trump's initial downplaying of the pandemic and its impact on shaping conservative media's response.
- Criticizing modern news shows for not issuing corrections or admitting when they were wrong, contributing to misinformation.
- Urging people to seek medical advice from professionals rather than relying on screen personalities.
- Offering a firsthand account from a woman who worked in a COVID ward to help persuade vaccine skeptics.
- Calling out wealthy individuals and politicians who spread vaccine misinformation while being vaccinated themselves.
- Encouraging everyone to get vaccinated for the well-being of themselves and their loved ones.

# Quotes

- "It shows that it's purely political."
- "Don't take any advice from people on a screen, myself included."
- "Almost all of them are vaccinated. It's not that they believe any of this stuff they're saying."

# Oneliner

Beau sheds light on the political divide influencing vaccine hesitancy and urges seeking medical advice over media influence.

# Audience

General public, Vaccine skeptics

# On-the-ground actions from transcript

- Contact a doctor or nurse for advice on vaccines (implied)
- Listen to stories from healthcare professionals to understand the importance of vaccination (implied)

# Whats missing in summary

The emotional impact of stories from healthcare professionals on vaccine hesitancy. 

# Tags

#Vaccines #PoliticalDivide #Misinformation #PublicHealth #COVID19