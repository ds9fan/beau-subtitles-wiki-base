# Bits

Beau says:

- Addresses the topic of experts, consensus, and new guidance, specifically regarding wearing masks in schools.
- Responds to a message pointing out that he is not a medical expert but has some medical training.
- Acknowledges the challenges of determining which expert to trust when there are disagreements within a field.
- Notes that true consensus among experts requires statements that are painfully obvious and inarguable.
- Mentions the guidance from the Academy of American Pediatrics recommending kids wear masks when they return to school.
- Comments on the tendency for people to question experts in fields where they lack training.
- Emphasizes the importance of specialization and relying on the expertise of others.
- Stresses that there is overwhelming consensus among medical professionals on the effectiveness of masks and vaccines.
- Encourages kids to follow basic hygiene practices when they return to school.
- Concludes by sharing his thoughts on the matter and wishing everyone a good day.

# Quotes

- "Specialization is something that humans do very well."
- "There isn't a real argument when it comes to this topic."
- "Everybody knows, because it's a painfully obvious truth."
- "When kids go back to school, wash your hands. Don't touch your face. Wear a mask."
- "Y'all have a good day."

# Oneliner

Beau addresses the importance of trusting experts and consensus, particularly on the topic of wearing masks in schools, stressing the need to rely on specialization and the overwhelming consensus among medical professionals.

# Audience

Parents, educators, policymakers

# On-the-ground actions from transcript

- Trust the guidance provided by medical professionals when it comes to wearing masks and vaccines (implied).
- Encourage children to practice good hygiene habits, such as washing hands, not touching their faces, and wearing masks when they return to school (implied).

# Whats missing in summary

Beau's engaging delivery and perspective on trusting experts and consensus can be best appreciated by watching the full video to grasp the nuances of his arguments.

# Tags

#Experts #Consensus #Masks #Schools #Trust #Specialization #Hygiene #MedicalProfessionals