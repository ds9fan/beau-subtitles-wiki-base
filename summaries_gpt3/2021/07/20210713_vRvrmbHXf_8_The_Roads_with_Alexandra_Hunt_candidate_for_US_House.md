# Bits

Beau says:
- Introduces the guest, Alexandra Hunt, who is running for Congress in PA3, a Democratic district in Philadelphia.
- Alexandra shares her background in health care and public health, her motivation for running for Congress, and her challenging incumbent who does not support progressive policies.
- Alexandra talks about her journey through college, working as a stripper and server to pay bills, and obtaining master's degrees while working full time.
- The importance of harm reduction in criminal justice reform and decriminalizing drug use and sex work.
- Restorative justice, based on Alexandra's personal experience as a survivor of sexual assault, focuses on accountability and understanding harm.
- Alexandra's platform includes justice for tribal nations, housing rights, gentrification, economic justice, wealth tax, environmental justice, and ending food insecurity.
- Her commitment to representing the will of the people through town halls, accessibility, and engagement with the community.
- The tipping point for Alexandra's decision to run for office was witnessing the government's failure during the pandemic and the lack of support for vulnerable communities.
- Alexandra's grassroots campaign approach to avoid corporate influence and stay true to ethical principles.
- Soccer is a passion outside of politics that Alexandra expresses enthusiasm for.

# Quotes

- "Harm reduction is decriminalizing drug use, the same way that I have a platform to decriminalize sex work."
- "Justice, to me, is not putting someone away in prison. Justice, to me, is a person understanding the harm that they have done."
- "Every person should be housed and be able to stay in their family home without threat of violence or new construction pushing them out."
- "The money that comes earlier allows you to play out your field game. And so it's hard."
- "My big game plan was just to have as much courage as I could and just be OK with being the only person in a room for a bit."

# Oneliner

Beau talks with Alexandra Hunt about her journey from health care to Congress candidacy, focusing on progressive platforms like harm reduction, restorative justice, and grassroots campaign ethics.

# Audience

Voters in PA3 district

# On-the-ground actions from transcript

- Attend town halls and community events to voice concerns and needs (exemplified)
- Support grassroots political campaigns financially or through volunteer work (exemplified)

# Whats missing in summary

The full transcript captures Alexandra Hunt's diverse platform and personal journey towards running for Congress, offering insights into her motivations, advocacy for progressive policies, and dedication to grassroots campaigning.

# Tags

#AlexandraHunt #Congress #PA3 #ProgressivePlatform #GrassrootsCampaign #HarmReduction #RestorativeJustice