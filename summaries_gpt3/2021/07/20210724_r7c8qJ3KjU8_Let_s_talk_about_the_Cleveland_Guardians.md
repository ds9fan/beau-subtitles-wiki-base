# Bits

Beau says:

- A baseball team's name change is the new front in the culture war, with Ted Cruz and former President Trump lamenting the Cleveland Indians becoming the Cleveland Guardians.
- Beau addresses a message accusing him of deleting a video on army helicopters named after Indians due to Trump supporting the practice, calling out the sender as disingenuous.
- He clarifies that the video was not deleted but is hard to find because the title does not mention Indians or helicopters, urging the sender to watch it.
- Beau explains that when the military names helicopters after native groups, it is to honor them, not to create caricatures like sports mascots.
- He humorously notes Trump's focus on sports team names in his post-presidency phase, finding it entertaining but also acknowledging the anxiety name changes can cause.
- Beau jests about his own experience with sports teams changing names, illustrating the frivolity of the culture war and conservatives' lack of substantial policy.
- He points out that conservatives rely on outrage and fear-mongering to stay relevant, lacking real solutions or policies.
- The Cleveland Indians have had several name changes in the past, leading Beau to suggest that one more change won't hurt.
- Beau criticizes politicians and media personalities for focusing on trivial issues like team names rather than addressing real problems.
- He concludes by remarking on the conservatives' reliance on provoking outrage over insignificant matters to maintain power.

# Quotes

- "I understand that can cause a lot of stress and a lot of anxiety."
- "They want to maintain the status quo and the world is changing."
- "All they have is stirring up outrage and fear because they have turned into shock jocks."
- "They are hilariously wrong."
- "Because they don't have solutions, they don't have policy."

# Oneliner

Beau addresses the controversy around sports team name changes and criticizes conservative tactics of outrage without solutions or policy.

# Audience

Activists, Sports Fans

# On-the-ground actions from transcript

- Share educational content on the history and significance of indigenous names in sports teams (suggested).
- Challenge fear-mongering and outrage tactics by promoting informed and constructive dialogues (implied).

# Whats missing in summary

The full transcript provides a detailed insight into the frivolity of the culture war and the tactics used by conservatives to provoke outrage without offering real solutions. Viewing the full transcript will give a deeper understanding of these dynamics. 

# Tags

#Sports #Conservatives #CultureWar #NameChange #Outrage #Policy