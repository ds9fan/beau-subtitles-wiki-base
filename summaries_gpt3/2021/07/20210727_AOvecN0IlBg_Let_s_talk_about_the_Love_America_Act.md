# Bits

Beau says:

- Introduces a proposed legislation called the Love America Act by Senator Hawley, sparking controversy.
- Points out the contradiction between the reasons stated for the legislation and its actual content.
- Explains the four-step plan in the Love America Act to ensure American students don't view the US as inherently racist.
- Criticizes the Act's approach of having students memorize patriotic documents without acknowledging the racist history.
- Emphasizes that studying the founding documents of the US reveals its inherent racism due to slavery.
- Argues that rather than hiding the country's racist past, students should understand it to strive for a more inclusive future.
- Believes that politicians lying about historical truths hinders the country's progress in learning from its past.
- Suggests that reading and understanding these documents can provide insight into the current societal issues stemming from past racism.
- Acknowledges that the US is not a perfect nation but a work in progress towards fulfilling its promises of freedom and equality.
- Encourages students to read these documents not to deny the country's racist history but to grasp where improvements are needed for a more inclusive society.

# Quotes

- "You want them to love America? Give them an America worth loving."
- "Students should absolutely read these things. Not because they won't show that the US was racist when it was founded. It's definitely going to show that because it was."
- "The United States isn't perfect. It's not a finished project."

# Oneliner

Beau explains the flaws in the Love America Act, advocating for acknowledging the US's racist past to work towards a more inclusive future.

# Audience

Students, Educators, Activists

# On-the-ground actions from transcript

- Encourage students to study and understand the founding documents of the US (suggested).
- Promote critical thinking and historical awareness in educational curriculums (implied).

# What's missing in summary

The emotional impact of understanding and accepting the US's racist history to pave the way for a more just society. 

# Tags

#USHistory #Racism #Education #Inclusivity #SocialJustice