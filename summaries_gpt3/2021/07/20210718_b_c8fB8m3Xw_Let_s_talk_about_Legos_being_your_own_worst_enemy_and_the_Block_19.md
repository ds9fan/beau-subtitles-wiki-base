# Bits

Beau says:

- Mention of discussing consequences of actions, being one's own worst enemy, Legos, upcoming legislation, and a concerning issue.
- Introduction to the Block 19, a kit designed to make a Glock look like it's made out of Legos.
- Noting the cease and desist order from Lego and the potential compliance from the company.
- Criticism towards the gun crowd for being their own worst enemy and making guns look like toys.
- Reflection on the phrase "guns are not toys" and questioning the motive behind making firearms appear as toys.
- Connecting the issue to identity politics and the consequences of such actions.
- Concerns about potential increase in accidental shootings and plausible deniability for cops shooting kids with toy-like guns.
- Prediction of future legislation limiting firearm finishes and the possible impact on gun control.
- Critique on marketing a kit that makes a firearm resemble a toy with no practical purpose.
- Addressing the inevitable persecution claims from the gun crowd when legislation is enacted.
- Criticism of treating guns as more than just tools and integrating them into one's identity.
- Commentary on toxic masculinity and the need for a mindset shift regarding firearm ownership.
- Personal reflection on happiness, security, and calmness post distancing from firearm ownership.

# Quotes

- "The gun crowd, you are your own worst enemy."
- "There are consequences to your actions."
- "Making it part of your identity. You know, making it something, a substitute for masculinity. It's very toxic."
- "I'm not saying that you should sell off all your firearms and give up your identity or your hobby or whatever."
- "Anyway, it's just a thought."

# Oneliner

Beau addresses the concerning trend of making guns look like toys, predicting consequences and legislation while criticizing the gun crowd's identity association with firearms.

# Audience

Gun Owners

# On-the-ground actions from transcript

- Contact legislators to advocate for responsible firearm legislation (implied).
- Educate gun owners on the importance of treating firearms as tools, not toys (implied).

# Whats missing in summary

Deeper insights into the impact of identity politics on gun ownership.

# Tags

#GunControl #IdentityPolitics #Legislation #Consequences #FirearmSafety