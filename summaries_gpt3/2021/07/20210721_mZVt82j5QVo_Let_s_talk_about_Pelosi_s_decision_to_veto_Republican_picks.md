# Bits

Beau says:

- Nancy Pelosi stopped certain Republicans from being on a committee looking into the events of the 6th.
- Republicans submitted names of people who actively supported undermining the election.
- Beau believes Pelosi's decision should be analyzed beyond a partisan view.
- He draws a historical analogy to the development of factions in movements seeking to change governments.
- Beau explains the roles of the military and political wings in such movements.
- He mentions how politicians were likely to justify, excuse, downplay, and cover up what happened on the committee.
- Beau asserts that the ultimate goal is to install an authoritarian leader by undermining democracy.
- He suggests that politicians may not all fully support the movement's goals but might be appealing to certain voter bases.
- Beau argues that those ideologically or politically involved in undermining democracy should not be on the committee.
- He mentions that disrupting the hearings could be a beneficial move for the political wing of the movement.

# Quotes

- "It was an attempt to undermine the Constitution."
- "Deny the opposition that platform."
- "But her decision to stop those who are at least willing to appear to be ideologically
  aligned with those who attempted to stop the election, stop the certification, her decision
  to bar them from being on this committee is the right one."

# Oneliner

Nancy Pelosi's decision to exclude Republicans from a committee investigating the events of January 6th is a strategic move to prevent undermining democracy by denying a platform to those seeking to justify and cover up the attack.

# Audience

Political analysts, concerned citizens

# On-the-ground actions from transcript

- Disrupt hearings (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the potential motives and actions of politicians involved in undermining democracy, urging caution and vigilance against authoritarian movements.

# Tags

#NancyPelosi #RepublicanParty #UnderminingDemocracy #Authoritarianism #PoliticalAnalysis