# Bits

Beau says:

- Beau introduces the topic of interviews and explains the two series on his channel, one featuring interesting people and the other political candidates running in 2022.
- The purpose of the political candidate interviews is to allow them to talk about their platform and policies, informing viewers who may vote in 2022.
- Beau aims to determine if candidates genuinely believe in what they are saying or if they are just following instructions.
- He does not plan to push back on candidates' positions unless something objectively false is stated, focusing on letting candidates speak and viewers access unfiltered information.
- Beau uses the example of an interview with Erica Smith, a U.S. Senate candidate from North Carolina, to illustrate the approach of allowing candidates to present their platforms without engaging in debates or trying to change their positions.
- In interviews with interesting people like Chelsea Manning, Beau focuses on topics beyond what people might expect, aiming to show different aspects of their personalities beyond the usual questions.
- The goal of both types of interviews is to provide information and better inform viewers without getting into confrontational or sensationalist questioning.
- Beau's approach to interviews is about allowing guests to share their thoughts and projects rather than creating conflict or controversy.

# Quotes

- "The goal is to inform those people who choose to vote in 2022."
- "It's more about getting information out."
- "They'll be pretty friendly chats."

# Oneliner

Beau introduces two interview series on his channel—one featuring political candidates and the other interesting people—to inform viewers without engaging in debates or sensationalism, focusing on letting guests share their views and projects.

# Audience

Content Creators, Viewers

# On-the-ground actions from transcript

- Watch and share Beau's interview videos to better understand political candidates' platforms and learn about interesting people (implied).

# Whats missing in summary

The full transcript provides a detailed insight into Beau's approach to interviews, focusing on information dissemination and showcasing different aspects of guests beyond typical questions.

# Tags

#Interviews #Information #PoliticalCandidates #ContentCreation #CommunityInforming