# Bits

Beau says:

- Representative Massey and other Republicans introduced a bill to prohibit the US Department of Defense from requiring personnel to get the vaccine.
- The argument that troops should have bodily autonomy in decision-making doesn't hold as the military operates under strict hierarchy with no room for personal choice.
- Morality doesn't play a significant role in the military; readiness is the paramount concern.
- Unvaccinated troops could significantly impact readiness in various operations, including unconventional conflicts, humanitarian missions, and facing near peers.
- Biden is unlikely to sign such a bill, and even if passed, the military tradition suggests they may ignore it for the sake of readiness.

# Quotes

- "Morality and war-making capabilities really don't blend well."
- "The only legitimate reason to support the bill is the bodily autonomy argument, but that's literally it out because it's the military."
- "The US military has a long tradition of ignoring politicians who think they know better, who refuse to embrace science, and who try to degrade military readiness."

# Oneliner

Beau explains why the bill prohibiting mandatory vaccines for military personnel is impractical, focusing on hierarchy, readiness, and potential impacts on operations.

# Audience

Military personnel, policymakers.

# On-the-ground actions from transcript

- Contact military representatives to advocate for prioritizing readiness over political stunts (implied).
- Support efforts to ensure military personnel are adequately vaccinated to maintain operational readiness (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the implications of prohibiting mandatory vaccines for military personnel, offering insights into the hierarchy, readiness concerns, and potential operational impacts.

# Tags

#Military #Vaccines #Readiness #Government #Hierarchy