# Bits

Beau says:

- Masks and mandates are making a comeback in several places, with more to come.
- The messaging around these precautions might be off, so Beau offers his perspective.
- Fully vaccinated individuals have a low risk of negative outcomes due to the effectiveness of vaccines.
- However, some vaccinated people may become lax with precautions.
- Beau acknowledges that some individuals may think those facing consequences chose not to get vaccinated or wear masks.
- Precautions taken by vaccinated individuals mainly protect those who can't get vaccinated.
- Despite being protected, vaccinated individuals could still transmit the virus.
- Beau encourages people to continue following guidelines to protect those who can't get vaccinated due to medical conditions.
- He stresses the importance of considering those who are at risk through no fault of their own.
- Historical trends show peaks and valleys in response to events like the current situation.
- Beau advocates for basic protective measures like handwashing, avoiding face-touching, and wearing masks.
- While social distancing isn't emphasized in current guidance, Beau suggests it's still a good idea based on data.
- Even though it may be frustrating to protect those who refuse precautions, Beau urges doing it for those who can't protect themselves.
- He concludes by reminding viewers to stay vigilant and protect those around them.

# Quotes

- "Your actions are there to protect the people who refuse to protect themselves."
- "Stay on your guard to protect those around you."

# Oneliner

Beau reminds fully vaccinated individuals to continue following COVID-19 precautions not just for themselves but for those who can't, stressing collective responsibility and protection.

# Audience

Fully vaccinated individuals

# On-the-ground actions from transcript

- Continue wearing masks and following COVID-19 guidelines to protect those who can't get vaccinated (suggested).
- Practice social distancing and other basic protective measures even if not explicitly advised (suggested).

# Whats missing in summary

The full transcript provides a detailed perspective on the importance of maintaining COVID-19 precautions for the safety of all, especially those who can't get vaccinated.

# Tags

#COVID-19 #Vaccination #Precautions #CommunityResponsibility