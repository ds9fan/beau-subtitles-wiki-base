# Bits

Beau says:

- Explaining the controversy surrounding an eight-month sentence given to a Florida man for actions at the Capitol on January 6th.
- Clarifying that the person received the sentence for being a "selfie seditionist" and not for engaging in violent acts or causing damage.
- Mentioning that the sentence was relatively light due to the defendant pleading guilty early and having no significant criminal history.
- Addressing the question of fairness and justice in the sentencing, pointing out the potential consequences beyond just the time served.
- Questioning the purpose of prisons and the justice system – whether they are meant for punishment, rehabilitation, or simply separation from society.
- Rejecting the idea of longer sentences based on unfair past judgments, advocating for a system that focuses on restorative justice.
- Expressing concern over mass incarceration rates in the United States and the need to reconsider who gets locked up and for how long.
- Encouraging a deeper reflection on personal beliefs when discussing sentences and justice, rather than reacting out of anger or a desire for revenge.

# Quotes

- "I don't want to live in a society where the justice system is based on vengeance."
- "We have to determine as society what we want our prisons to be, what we want our justice system to be."
- "If it's about rehabilitation, if it's about restorative justice, then it's completely reasonable."
- "Make sure that Mark and Malik should be at home. Not that Florida man should do 20 years."
- "Not just that you're angry. Not just that you want punishment."

# Oneliner

Beau addresses the controversy of an eight-month sentence, questioning the purpose of prisons and advocating for a justice system not based on vengeance.

# Audience

Justice reform advocates

# On-the-ground actions from transcript

- Advocate for restorative justice practices and rehabilitation programs in your community (suggested)
- Support organizations working towards reducing mass incarceration rates and promoting fair sentencing policies (suggested)

# Whats missing in summary

A deeper understanding of the nuances of justice reform and the implications of different sentencing approaches.

# Tags

#JusticeReform #PrisonSystem #FairSentencing #RestorativeJustice #MassIncarceration