# Bits

Beau says:

- Exploring the concept of the "greatest generation" from the 1940s in American society.
- Addressing memes that suggest today's Americans are not up to the same task as the Americans from the 1940s.
- Disputing the idea that the 1940s generation was uniquely tough and downplaying the emotional and psychological impacts of war.
- Pointing out the historical inaccuracy in suggesting that liberals couldn't have participated in the same actions as the generation from the 1940s.
- Emphasizing that the greatness of that generation was not due to their going to war, but their unity and willingness to sacrifice for a common goal.
- Drawing parallels between the inconveniences that generation endured during wartime and the minor inconveniences asked of Americans today, like wearing masks.
- Stating that true patriotism is about putting the well-being of others above oneself and making sacrifices for the greater good.
- Encouraging people to wear masks, get vaccinated, and do their part in stopping the current crisis to truly honor the spirit of the "greatest generation."

# Quotes

- "Patriotism is having a loyalty to a geographic area and having a desire to protect those within it, putting that group of people above yourself."
- "If you want to claim to be the heir of those Americans, you need to do your part now."
- "The greatest generation was the greatest, not because it fought World War II, but because the entire country came together to stop something that needed to be stopped."

# Oneliner

Exploring the concept of the "greatest generation" from the 1940s and urging people to emulate their unity and sacrifices by wearing masks and getting vaccinated.

# Audience

Americans

# On-the-ground actions from transcript

- Wear a mask, get your shots, and go get your vaccine (exemplified)
- Put the well-being of others above yourself by following public health guidelines (exemplified)

# Whats missing in summary

The full transcript delves deeper into the comparison between past sacrifices and present challenges, urging individuals to embody the spirit of unity and sacrifice seen in the "greatest generation" by taking necessary public health measures.

# Tags

#GreatestGeneration #Unity #Sacrifice #Patriotism #PublicHealth #COVID19 #Vaccination #AmericanSociety