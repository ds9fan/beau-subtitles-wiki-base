# Bits

Beau says:

- Former President Donald J. Trump expresses buyer's remorse regarding Supreme Court Justice Kavanaugh.
- Trump saved Kavanaugh's life, suggesting he couldn't even get a job at a law firm without him.
- Trump expresses disappointment in Kavanaugh's lack of courage and inability to make great decisions.
- Trump is surprised by Kavanaugh not overturning the election, considering it a betrayal.
- The lifetime appointment of Supreme Court justices prevents influence and pressure from those who nominated them.
- Setting a mandatory retirement age might be a good idea, but the lifetime appointment serves a purpose.
- Removing the ability for elected officials to pressure justices ensures brave decisions.
- Despite Trump's regrets, Kavanaugh is likely to remain in his position for a long time.
- The system of lifetime appointments stops elected officials from being able to remove justices easily.
- Trump's pattern of making bad hires shows his need for a good HR director.

# Quotes

- "Who would have had him? Nobody, totally disgraced. Only I saved him."
- "The fact that the president once again made a bad hire, the former president made a bad hire, hired somebody he regrets."
- "That's why it exists."
- "If we have learned anything from the former president, it's that he really needs an HR director."
- "Have a good day."

# Oneliner

Former President Trump expresses buyer's remorse over Supreme Court Justice Kavanaugh, revealing his disappointment in Kavanaugh's decisions and the purpose behind lifetime appointments.

# Audience

Political observers

# On-the-ground actions from transcript

- Contact your representatives to advocate for reforms in the appointment process (suggested).
- Join advocacy groups working towards setting a mandatory retirement age for Supreme Court justices (suggested).

# Whats missing in summary

Insights on the potential impact of Trump's hiring decisions and the importance of informed appointments to key positions.

# Tags

#DonaldJTrump #SupremeCourt #LifetimeAppointments #Kavanaugh #PoliticalInsights