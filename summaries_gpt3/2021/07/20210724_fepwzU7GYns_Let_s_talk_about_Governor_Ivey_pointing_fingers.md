# Bits

Beau says:

- Analyzing the effectiveness of the governor of Alabama's statement blaming the unvaccinated for COVID-19.
- The governor's message may not be persuasive due to varying motivations among individuals.
- Some unvaccinated individuals may only comply if mandated due to a belief in authoritarianism.
- The statement's effectiveness may lie in changing the narrative rather than increasing vaccination rates.
- Beau questions whether the governor's goal was to shift blame onto the unvaccinated rather than encourage vaccination.
- The approach of blaming the unvaccinated might not significantly impact vaccination rates in Alabama.
- Southern cultural dynamics and historical context are considered in assessing the statement's potential impact.
- The statement may resonate more with those who already follow an authoritarian leader.
- Beau suggests that the goal was to divert attention and shift the narrative away from other responsible parties.
- Changing the narrative and blaming specific groups might be the main intent behind the governor's statement.

# Quotes

- "Will it work on the others? You've disappointed grandma? Maybe."
- "Blame the unvaccinated people. Don't blame the people who downplayed it."
- "Changing the narrative. And I think that's really what it's about."

# Oneliner

Beau questions the persuasive power of blaming the unvaccinated, speculating on a narrative shift rather than increased vaccination rates in Alabama.

# Audience

Social commentators

# On-the-ground actions from transcript

- Challenge narratives about blame (implied)
- Encourage critical thinking about shifting blame (implied)

# Whats missing in summary

The detailed nuances of Beau's analysis can be best understood by watching the full video.

# Tags

#COVID-19 #Vaccination #NarrativeShift #Blame #Persuasion