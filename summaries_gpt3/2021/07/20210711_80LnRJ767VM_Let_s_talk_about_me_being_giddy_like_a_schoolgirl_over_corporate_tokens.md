# Bits

Beau says:

- Explains his excitement when major companies show token gestures of progressiveness by changing the race or gender of characters in media.
- Compares this strategy to record companies paying radio stations to play their music for familiarity.
- Argues that conservatives fear these changes because they effectively combat fear-mongering and "othering."
- Notes that companies implement these changes for profit, not social change, similar to McDonald's and Coke.
- Emphasizes that exposure breeds familiarity and reduces fear of the unknown.
- States that conservatives use fear to motivate their base and uphold the current system by keeping people separate.
- Believes that familiarity with different ideas and demographics leads to less resistance to change.
- Sees these token gestures as effective in gradually shifting perspectives and making change less scary.
- Expresses optimism about these gestures indicating a shift in the market towards inclusivity and progressiveness.
- Concludes by suggesting that exposure and familiarity through media can lead to greater acceptance and less resistance to change.

# Quotes

- "Every time this happens, there's an orchestrated outrage. Right? Why are they doing this? Because they know how effective it is."
- "Familiarity, because it's everywhere. You walk into a gas station to buy a Coke, there is an advertisement for Coke on the cooler."
- "Their motive as to why they're doing it doesn't mean that it's not effective."
- "The tide is turning. It is more profitable to do this than to not."
- "And that exposure, well, it breeds familiarity. Makes them less afraid, which means they will mount less resistance to change when it comes."

# Oneliner

Beau explains how token gestures of progressiveness in media combat fear-mongering and pave the way for change by fostering familiarity and reducing resistance.

# Audience

Media consumers, Progressives

# On-the-ground actions from transcript

- Support media that showcases diversity and inclusivity (implied)
- Advocate for gradual exposure to different ideas and demographics through media (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of how exposure to diversity in media can combat fear and resistance, leading to societal change.

# Tags

#Media #Progressiveness #Diversity #FearMongering #Inclusivity