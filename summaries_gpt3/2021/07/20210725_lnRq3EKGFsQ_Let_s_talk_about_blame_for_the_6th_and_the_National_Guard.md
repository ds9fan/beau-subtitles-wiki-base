# Bits

Beau says:

- Politicians are questioning the National Guard's actions and decisions on January 6th.
- The scrutiny aimed at the National Guard is unwarranted, as they were following guidance.
- Orders from Acting Secretary of Defense limited the National Guard's capabilities on that day.
- The guidance restricted the National Guard from having necessary tools and engaging with protesters.
- The ultimate responsibility for the failures on January 6th lies with the White House at that time.
- The guidance was intended to prevent an overreaction by the National Guard.
- President Trump's directive to protect demonstrators influenced the restrictive guidance.
- The American people are more concerned about how the events started rather than operational breakdowns.

# Quotes

- "Nobody wearing a National Guard uniform is responsible for any of the failures or breakdowns that occurred on the 6th."
- "There's nothing the National Guard could have done. They had orders."
- "The ultimate responsibility for the failures that occurred related to the National Guard? You need to look to the White House at the time."
- "The American people do not care about this. This is not what people are concerned with."
- "You want to know who's ultimately responsible, it's President Trump."

# Oneliner

Politicians unfairly scrutinize the National Guard; ultimate responsibility for Jan 6 failures lies with the White House, not the Guard.

# Audience

Politically aware citizens

# On-the-ground actions from transcript

- Seek accountability from those in power (implied)
- Advocate for transparent investigations into the events of January 6th (implied)

# Whats missing in summary

Detailed insights on the specific actions and decisions made by the National Guard on January 6th

# Tags

#NationalGuard #January6 #Responsibility #Accountability #PoliticalScrutiny