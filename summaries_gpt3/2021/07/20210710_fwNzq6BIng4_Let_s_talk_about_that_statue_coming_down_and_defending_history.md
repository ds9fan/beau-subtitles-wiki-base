# Bits

Beau says:

- Beau has been waiting to talk about a specific statue in Charlottesville since 2017, which has now been removed.
- The statue of Robert E. Lee was defended in the past under the guise of defending history, but its true history isn't accurately represented by that defense.
- The statue was commissioned around the same time as the film "Birth of a Nation," which glorified the Ku Klux Klan.
- The motivations behind the statue's commissioning aren't entirely clear, but it was part of a broader context that included segregation and oppression.
- Washington Park, another project by the same commissioner, was also intertwined with racial segregation.
- The statue symbolized more than just the Civil War; it had ties to celebrating slavery and segregation.
- Statues are not just historical artifacts; they are political statements that convey specific messages.
- Defending a symbol without fully understanding its implications may mean upholding problematic ideas and values.
- Beau suggests that it's sometimes better to let go of symbols that represent old and harmful ideas rather than keeping them alive.
- Beau quotes Robert E. Lee, who himself believed Confederate monuments should not exist.

# Quotes

- "Statues are not really history. It's art. Sometimes art's good, sometimes it's bad. But all art is political and it conveys a message."
- "Be certain you are aware of the ideas contained in that symbol."
- "When you're talking about ideas that old and that wrong, maybe it's best to let them go."
- "When somebody has emotionally manipulated you into defending a symbol, be certain you know what that symbol represents."
- "I think it's wiser not to keep open the sores of war."

# Oneliner

Beau has been waiting to address the removal of a statue in Charlottesville, shedding light on its history and the importance of understanding the messages behind symbols.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Research the history and context behind statues and monuments in your community (implied)
- Engage in dialogues about the implications of symbols and their historical significance (implied)
- Advocate for the removal of statues that glorify oppressive histories (implied)

# Whats missing in summary

Deeper insights into the impact of symbols on society and the importance of acknowledging and understanding their true meanings.

# Tags

#Charlottesville #RobertELee #StatueRemoval #Symbolism #HistoryLessons #Segregation