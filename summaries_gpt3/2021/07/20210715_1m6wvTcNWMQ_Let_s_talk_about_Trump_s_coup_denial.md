# Bits

Beau says:

- Exploring former President Donald Trump's response to reports about General Milley's concerns.
- Trump's statement mocked for resembling a transcript of a not-so-smart criminal in an interrogation room on TV.
- Trump denies discussing a coup and claims the election was his form of coup.
- Trump criticizes General Mark Milley, citing conflicts with General James Mattis and Obama.
- Former President Trump acts authoritarian, believing he knows more than experts, and criticizes Milley's actions.
- Trump attempts to sow dissension between generals, particularly targeting Milley.
- Contrasting leadership styles between Mattis and Milley likely led to conflicts.
- Milley's background in Special Forces indicates more than just being a tough guy - he's also a weaponized educator.
- Milley understands the dynamics of a coup and was reportedly concerned about Trump staging one.
- It's vital to recognize the seriousness of Milley's concerns and actions against a potential coup.

# Quotes

- "Former President Trump behaves like the typical authoritarian with a god complex."
- "Milley was concerned about the former president staging a coup against the United States."
- "One of the most vital functions of Special Forces is to be a teacher."

# Oneliner

Beau delves into Trump's response to Milley's concerns, showcasing authoritarian behavior and Milley's serious worries about a potential coup.

# Audience

Politically conscious individuals

# On-the-ground actions from transcript

- Contact local representatives to voice concerns about potential political crises (implied)
- Support efforts to uphold democratic principles in government (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's response to General Milley's concerns, shedding light on authoritarian behavior and the seriousness of potential coup attempts.

# Tags

#Trump #GeneralMilley #Authoritarianism #Coup #Democracy