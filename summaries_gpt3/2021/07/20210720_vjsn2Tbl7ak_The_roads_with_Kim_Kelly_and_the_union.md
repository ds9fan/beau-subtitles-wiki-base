# Bits

Beau says:

- Introduction to guest Kim Kelly, a freelance labor reporter with a book coming out soon.
- Kim Kelly shares her background, living in South Philadelphia, and reporting on strikes in Alabama.
- Kim Kelly recounts her involvement with the Amazon warehouse workers' unionizing efforts.
- Details about the coal miner strike at Warrior Met Coal in Brooklyn, Alabama, due to unfair labor practices.
- Description of the strike, lack of mainstream media coverage, and the support network established by the community.
- Insights into the disturbing tactics used by Warrior Met, including vehicular attacks on strikers.
- Kim Kelly's reflections on the stark contrast between the supportive community efforts and the violent attacks.
- Overview of the unique structure of the picket lines at Warrior Met Coal in Brookwood.
- Beau and Kim Kelly discussing the lack of political support for the striking miners from both Democrats and Republicans.
- Information about upcoming events organized by the striking miners and their families to protest in New York City.

# Quotes

- "One day longer, one day stronger."
- "It's been my personal crusade to get people to pay attention to this strike."
- "These folks just want to go back to work and make their money and live their lives."

# Oneliner

Beau and Kim Kelly shed light on the underreported coal miner strike in Alabama and the community's resilience against corporate oppression.

# Audience

Advocates for workers' rights

# On-the-ground actions from transcript

- Support the striking miners by donating to the strike fund or contributing to the strike pantry (suggested).
- Amplify information about the strike and the community support efforts on social media (exemplified).

# Whats missing in summary

In-depth analysis of the cultural and political dynamics influencing support for the striking miners.

# Tags

#LaborRights #CommunitySupport #AlabamaStrike #CorporateOppression #Unionizing