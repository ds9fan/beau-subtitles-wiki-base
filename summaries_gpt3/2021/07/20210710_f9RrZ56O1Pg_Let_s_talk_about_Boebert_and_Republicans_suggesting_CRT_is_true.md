# Bits

Beau says:

- The Zen Education Project's pledge for teachers caused a stir, misconstrued by conservative outlets as CRT support.
- Representative Lauren Boebert suggested firing 5,000 teachers over the pledge, sparking backlash.
- Boebert's educational background being a high school dropout was criticized by Democrats.
- Equating intelligence or education level to credentials is not a good idea, as intelligence varies widely.
- The focus should be on Republicans suggesting CRT is true, not on Boebert's educational history.
- Calling for firings over free speech rights may be a First Amendment issue.
- The pledge was about refusing to lie to students about US history, not specifically about teaching CRT.
- Alienating rural Americans and other demographics with elitist attacks based on education level is a concern.
- Education level doesn't always correlate with intelligence or capability.
- It's more effective to focus on substantial issues rather than personal educational backgrounds.

# Quotes

- "Equating intelligence or education level to credentialing isn't a good idea."
- "Educational credentials and intelligence or how smart you are, they don't go together."
- "Alienating rural Americans and probably other demographics."
- "Education level doesn't always correlate with intelligence or capability."
- "It's more effective to focus on substantial issues rather than personal educational backgrounds."

# Oneliner

The focus should be on substantial issues like Republicans suggesting CRT is true, not on personal educational backgrounds, as education level doesn't always correlate with intelligence.

# Audience

Educators, Politicians, Activists

# On-the-ground actions from transcript

- Refuse to lie to young people about US history and current events, regardless of the law (suggested).
- Be cautious of equating educational credentials with intelligence (implied).

# Whats missing in summary

The full transcript provides additional context on the misconstrued pledge, the backlash faced by Representative Boebert, and the potential First Amendment concerns related to calling for firings over free speech rights.

# Tags

#Education #Politics #Intelligence #CRT #FirstAmendment