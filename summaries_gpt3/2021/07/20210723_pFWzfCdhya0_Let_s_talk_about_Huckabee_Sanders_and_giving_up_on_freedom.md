# Bits

Beau says:

- Beau delves into Sarah Huckabee Sanders' statements, the implications, and how some Americans unknowingly surrender their freedom.
- Sarah Huckabee Sanders expressed her desire to be the governor of Arkansas and announced her opposition to mask and vaccine mandates.
- Beau agrees with limited mandates due to valuing freedom and people's ability to self-govern.
- The past year has made Beau question people's self-governing ability due to their actions during the pandemic.
- Beau points out the contradiction in authoritarian followers' interpretation of no mandates as a lack of importance rather than government non-interference.
- Many Trump supporters are aware of his vaccination status and Fox News' vaccine passport but still resist mandates due to their perceived connection with Trump's stance.
- Beau argues that freedom comes with responsibility for one's actions and shared responsibility for others.
- People who submit to authoritarianism often neglect personal responsibility and allow leaders to dictate their choices.
- Beau criticizes those who prioritize perceived wishes of leaders over their own well-being by refusing vaccines or masks.
- Beau warns of the danger of public figures like Sarah Huckabee Sanders making firm statements against mandates in a dynamic public health crisis.
- Beau believes that all public officials should prioritize ending the public health crisis, labeling such statements as hindrances to this goal.
- Beau questions the real intent behind Sanders' statement, suggesting that it aimed to rally her base rather than contribute to resolving the public health crisis.

# Quotes

- "If you hear, we're not going to mandate vaccines, we're not going to mandate masks, and take that to mean is you don't need to wear one, you have given up on everything you say you believe in."
- "You have given up on freedom. You have given up on self-determination. You have given up on the very principles you scream about."
- "People who scream about freedom and have the Constitution as a profile pick have forgotten that they need to provide for the common defense and promote the general welfare."
- "Every candidate right now, everybody in public office right now should have one goal. That is to end the public health issue."
- "Freedom is something that carries responsibility with it. Responsibility for your own actions, shared responsibility for your neighbors."

# Oneliner

Beau warns against sacrificing freedom and personal responsibility in blindly following anti-mandate stances amidst the public health crisis.

# Audience

Americans

# On-the-ground actions from transcript

- Wear a mask and get vaccinated to protect yourself, your family, and your community (implied)
- Prioritize ending the public health crisis as the primary goal for all public officials (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the dangers of authoritarianism and blind obedience, especially in the context of public health crises and political manipulation. Watching the full transcript can offer a deeper understanding of these complex issues. 

# Tags

#Freedom #PublicHealth #Authoritarianism #Responsibility #Vaccination #Mandates