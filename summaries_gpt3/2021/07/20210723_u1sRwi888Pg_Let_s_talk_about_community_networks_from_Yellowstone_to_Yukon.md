# Bits

Beau says:
- Community networks start slow and small, like with his attempt to start one at a barbecue where only three people showed up for the first meeting out of forty interested individuals.
- Despite starting small, community networks can accomplish great things, as seen in the Yellowstone to Yukon conservation effort.
- Yellowstone to Yukon aimed to protect and connect a 2,000-mile stretch of land based on animal migration patterns, not geographical boundaries.
- The initiative initially started with only 10% of the land protected and 5% connectivity but has since doubled in protected land and increased connectivity to 30%.
- Yellowstone to Yukon has over 200 partners, including private individuals, conservation groups, and even a mining group in Canada.
- The project defied traditional conservation boundaries like state lines and international borders to protect the interconnected ecosystem.
- Beau encourages building community networks with few people, as even with three individuals, projects can be initiated and successes can attract more participants.
- Real and deep change requires thinking beyond one's lifetime and starting initiatives now, despite time constraints.
- The best time to make a change was in the past, but the next best time is now.
- Building momentum and showing early successes can attract more people to join and contribute to the community network.

# Quotes

- "The best time to plant a tree was 20 years ago. The next best time is right now."
- "Real and deep change requires thinking beyond your lifetime."
- "Just because it is small doesn't mean it can't accomplish things."
- "The more people you have, the more effective it will be."
- "People will be willing to join something that is already moving and already succeeding."

# Oneliner

Community networks start small but can achieve great things, like the Yellowstone to Yukon conservation effort, by defying traditional boundaries and building momentum slowly.

# Audience

Community organizers

# On-the-ground actions from transcript

- Start a community network with a small group of like-minded individuals (suggested)
- Initiate projects within the community network to show early successes (suggested)
- Hold events like barbecues to attract more participants to the community network (suggested)

# Whats missing in summary

Building community networks takes time and dedication, but starting small can lead to significant accomplishments in the long run.

# Tags

#CommunityNetworks #ConservationEfforts #BuildingMomentum #RealChange #CommunityOrganizers