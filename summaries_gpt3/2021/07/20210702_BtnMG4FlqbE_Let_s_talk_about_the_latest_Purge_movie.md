# Bits
Beau says:

- Introduces the topic of discussing the political nature of "The Purge" franchise.
- Describes the general premise of "The Purge" movies where all crime is legal for a set period every year.
- Talks about the latest installment of the franchise taking place along the southern border of the United States, focusing on a group wanting to "take America back."
- Points out the obvious parallels in the movie to Trumpism, describing it as ham-fisted and not subtle.
- Explains that horror movies, including "The Purge" franchise, often contain sociopolitical commentary exploring our darkest fears.
- Mentions the economic disparity commentary present throughout the entire franchise and in horror movies in general.
- Shares examples of political commentary in horror films regarding environmental issues, greedy politicians, and the military-industrial complex.
- States that all art is political in some way and that pretty much everything today is political.
- Suggests that those who dislike the political commentary in movies may refuse to see it or pretend it's a joke.
- Raises the point that the entire "Purge" franchise has always been political, with intentional political commentary in the latest installment.
- Notes that despite being too on the nose, the political parallels in the latest movie were filmed before certain real-life events occurred.
- Concludes by speculating that there may be backlash on social media due to the political nature of the latest "Purge" movie.

# Quotes
- "All art is political in some way."
- "The entire franchise is political in nature."
- "Life imitates art."

# Oneliner
Beau talks about the political nature of "The Purge" franchise, pointing out its intentional sociopolitical commentary intertwined with horror elements, and how all art carries political undertones.

# Audience
Movie enthusiasts, political analysts.

# On-the-ground actions from transcript
- Watch and analyze movies with a critical eye for sociopolitical commentary (implied).
- Engage in respectful debates and dialogues about the political themes in art (implied).

# Whats missing in summary
The full transcript provides a comprehensive analysis of the political themes present in horror movies, urging viewers to recognize and appreciate the intentional sociopolitical commentary woven into artistic creations.

# Tags
#ThePurge #PoliticalCommentary #HorrorMovies #Art #SociopoliticalThemes