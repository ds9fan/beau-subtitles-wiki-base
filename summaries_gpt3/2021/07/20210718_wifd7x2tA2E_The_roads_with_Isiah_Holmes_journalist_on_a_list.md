# Bits

Beau says:

- Introducing Isaiah Holmes, a journalist with the Wisconsin Examiner from Milwaukee, Wisconsin, who was recently placed on a list by the Wauwatosa Police Department.
- The list included over 200 individuals, including elected officials, lawyers, activists, and Isaiah Holmes, with detailed personal information and connections to their social media profiles.
- The list was shared with the FBI and the Milwaukee Police Department, raising concerns about surveillance and potential infringement on constitutional rights.
- The Wauwatosa Police Department claimed the list was a tool for investigating protests, including individuals who were witnesses or bystanders, not just protesters or suspects.
- Isaiah Holmes had a history of being monitored by law enforcement since his teenage years, including being followed and experiencing odd social media occurrences.
- There are talks of legal action and civil rights implications following the exposure of the list and other questionable actions by the Wauwatosa Police Department.
- Law enforcement responses to ongoing protests in Milwaukee, Wauwatosa, and Kenosha varied, with some departments being hostile and others more restrained.
- The determination and organizing efforts of protest groups like the People's Revolution in Milwaukee have continued despite challenges and lack of media coverage.

Isaiah Holmes says:

- Isaiah Holmes, a journalist, was placed on a list by the Wauwatosa Police Department as part of a surveillance effort targeting protesters and individuals associated with protests in Milwaukee and Wauwatosa.
- The list included detailed personal information about individuals, such as their names, ages, race, gender, affiliations, and even vehicle information.
- Despite being labeled as a journalist, Isaiah was included on the list as a potential witness to protest events, raising concerns about the chilling effect on press freedom and free speech.
- There are suspicions of deeper surveillance and privacy breaches, including tampering with phones and intelligence gathering on lawyers, lawmakers, and journalists involved in the protests.
- Isaiah's history with journalism and experiences of being monitored by law enforcement since his teenage years have added to concerns about privacy and constitutional rights violations.
- The ongoing determination of protest groups like the People's Revolution in Milwaukee and evolving responses from law enforcement indicate a complex and challenging environment for activism and civil rights.
- Isaiah encourages people to be aware of privacy measures like VPNs and password protection, stressing the importance of understanding and defending constitutional rights even if they seem unaffected by surveillance.

# Quotes

- "Even if law enforcement is routinely violating and disregarding the constitution, that matters even if it doesn't negatively impact you, because someday it will impact you and people you care about." - Isaiah Holmes
- "People really need to understand that these things are real and they have real consequences." - Isaiah Holmes
- "Always be informed about privacy. If it means paying for a little bit better antivirus, go for it." - Beau
- "You do lock your doors, right? That means you have things to hide, even if it's seemingly innocuous. It matters." - Beau
- "If it doesn't negatively impact you, someday it will impact you and people you care about." - Isaiah Holmes

# Oneliner

Isaiah Holmes and Beau expose the surveillance tactics of the Wauwatosa Police Department targeting protesters and journalists, raising concerns about privacy, constitutional rights, and the chilling effect on press freedom.

# Audience

Journalists, activists, civil rights advocates

# On-the-ground actions from transcript

- Contact civil rights organizations for support and legal advice (implied)
- Stay informed about privacy measures like VPNs and password protection (implied)
- Support independent media outlets covering civil rights issues (exemplified)

# Whats missing in summary

The full transcript provides a detailed account of the surveillance tactics used by the Wauwatosa Police Department, the ongoing determination of protest groups in Milwaukee, Wauwatosa, and Kenosha, and the broader implications for privacy, constitutional rights, and press freedom. Viewing the full transcript offers a comprehensive understanding of these complex issues.

# Tags

#Surveillance #CivilRights #Protests #Journalism #Privacy #ConstitutionalRights #Activism #Wauwatosa #Milwaukee