# Bits

Beau says:
- President Biden plans to issue an executive order shifting the national security framing on climate issues.
- Climate change impacts like food and water insecurity, retreating ice caps, and rising sea levels are considered national security issues by the Army.
- The use of national security framing is aimed at getting politicians who typically oppose environmental protection measures to support them.
- Beau acknowledges ideological concerns about using the military's clout but believes it will be effective in addressing climate change.
- He suggests that healthcare can also be framed as a national security issue due to its impact on readiness and economic growth.
- Beau advocates for utilizing every tool available, including the Department of Defense's influence, to address urgent issues.
- The urgency to address climate change has always been present, and framing it as a national security issue is a practical approach.
- Beau stresses the need to focus on addressing real issues rather than dwelling in the era of alternative facts.
- Using the Department of Defense's influence to push for environmental and healthcare reforms may not be ideologically pure but is seen as effective.
- The urgency to act on climate change and healthcare is emphasized due to the pressing nature of the issues.

# Quotes

- "Increased food insecurity, increased water insecurity, new areas becoming possible battle spaces because of retreating ice caps, migration patterns, rising sea levels that might affect ports. All of these things are national security issues."
- "We can't let the fact that we feel icky about it get in the way of saving lives."
- "It's probably one of the most effective. And we're running out of time."
- "The urgency has been there."
- "We are moving out of the alternative facts era and moving into actually addressing the issues that are facing this country and facing this planet."

# Oneliner

President Biden reframes climate and healthcare as national security issues to drive action, utilizing the military's influence for effective change, acknowledging urgency in addressing pressing global challenges.

# Audience

Policy Advocates

# On-the-ground actions from transcript

- Advocate for framing climate change and healthcare as national security issues (suggested)
- Support policies that address environmental and healthcare challenges (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of framing climate and healthcare as national security issues, urging action leveraging the Department of Defense's influence for effective change.

# Tags

#ClimateChange #NationalSecurity #Healthcare #Urgency #PolicyChange