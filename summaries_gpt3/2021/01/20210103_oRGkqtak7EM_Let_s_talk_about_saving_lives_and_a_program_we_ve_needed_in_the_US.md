# Bits

Beau says:

- People in the US have expressed interest in a European-style mandatory gun ownership program but realize it infringes on constitutional rights.
- Compelling individuals to surrender firearms through legislation is challenging and may deter people from seeking help.
- The gun community values strength, and stigma around seeking help exists, preventing legislative remedies from being effective.
- Hold My Guns is a voluntary program where federal firearms license holders store firearms for various reasons, not just during struggles.
- The program provides plausible deniability to safely store firearms off-site without linking it to mental health issues.
- Surrendered firearms can be picked back up, with a simple phone call to ensure the person is not prohibited.
- Hold My Guns aims to save lives by increasing use without stigma, promoting responsible firearm ownership within the community.
- Beau encourages spreading awareness about Hold My Guns by mentioning it to friends in relevant situations.
- The program operates outside of legislation, with individuals taking proactive steps towards a collective goal.
- Beau stresses the importance of discussing and promoting such initiatives to make them effective.

# Quotes

- "That image is not worth dying over."
- "This program will save lives if people know about it, if people talk about it."
- "They're just doing what's right."

# Oneliner

In the US, a voluntary gun storage program called Hold My Guns offers a stigma-free solution, promoting responsible firearm ownership and potentially saving lives.

# Audience

Gun owners, community members.

# On-the-ground actions from transcript

- Spread awareness about Hold My Guns by mentioning it to friends in relevant situations (suggested).
- Look for federal firearms license holders participating in the program through the website provided (implied).

# Whats missing in summary

The importance of community-driven initiatives outside legislation to address firearm safety and mental health concerns.

# Tags

#GunOwnership #MentalHealth #CommunitySafety #FirearmStorage #VoluntaryProgram