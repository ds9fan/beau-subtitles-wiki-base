# Bits

Beau says:

- Republicans are blocking the stimulus on Capitol Hill, despite Democrats having a slim majority in the Senate.
- Bernie Sanders is the chair of the budget committee and could use the reconciliation process to pass the stimulus with a simple majority.
- Reconciliation doesn't require 60 votes and can't be filibustered.
- Sanders has promised to use this procedural method to get the stimulus through without passing a new package.
- This process could also potentially adjust the minimum wage.
- McConnell won't be able to stop this process, but he may criticize Sanders for his past opposition to it.
- The House will also go through a reconciliation process after the Senate.
- The debate time in the Senate for reconciliation is limited to 20 hours, and the process between the House and Senate is limited to 10.
- This process can't be used for everything but can be pivotal in passing much of the stimulus package.
- Sanders needs support from the Democratic Party to follow through on this.

# Quotes

- "Bernie Sanders promised to take his gloves off yesterday."
- "Whether or not this is what the founders intended on how it's supposed to work, that's up for debate, but it is certainly how it works right now."
- "I just hope that the Democratic Party gives Sanders the backup that he needs."

# Oneliner

Republicans block stimulus, Sanders eyes reconciliation to pass it, needing Democratic support.

# Audience

Politically engaged individuals.

# On-the-ground actions from transcript

- Support Sanders in using the reconciliation process (implied).
- Push for Democratic backing of Sanders (implied).

# Whats missing in summary

The full transcript provides a detailed explanation of how budget reconciliation works and the potential implications of using it to pass the stimulus package.