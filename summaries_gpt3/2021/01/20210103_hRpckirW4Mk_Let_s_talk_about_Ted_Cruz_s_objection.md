# Bits

Beau says:

- Enumerates senators Cruz, Howley, Johnson, Lankford, Daines, Kennedy, Blackburn, Braun, and senators-elect Loomis, Marshall, Hagerty, and Tuberville, criticizing their performative loyalty test to undermine American values.
- Defines authoritarian power structures as objectively evil, pointing out that individuals within those structures are not necessarily evil themselves.
- Compares historical examples like the U.S. in the 1860s and Germany in the 1940s to illustrate the dangers of supporting authoritarian power structures.
- Emphasizes the danger posed by those who provide support through performative loyalty tests, like the current vote attempting to silence millions of voices over unfounded allegations.
- Stresses the attempt to create an authoritarian power structure by undermining democratic processes and removing voices of the people.
- Mentions Mitch McConnell's statement that the vote will be consequential, potentially a referendum on democracy itself.
- Calls for the people on the list and the 140 supporting Republicans to be precluded from public office for their actions against American values and democracy.
- Warns about the potential repercussions of rewarding such behavior by re-electing these individuals.
- Urges for these individuals to be held accountable and remembered for their actions against the American people and democracy.

# Quotes

- "If people do not have a voice, what kind of power structure do you have? An authoritarian one."
- "They have shown where their loyalties lie. They're attempting to do something that's not right."
- "The people on this list and those 140 Republicans, they should be a uniting element in this country."

# Oneliner

Senators and Republicans engaging in performative loyalty tests aim to undermine voices of millions over unfounded allegations, endangering democracy and supporting authoritarian power structures.

# Audience

American citizens

# On-the-ground actions from transcript

- Hold those senators and Republicans accountable for their actions (implied)
- Ensure these individuals are precluded from public office (implied)
- Unite across party lines to protect democracy and American voices (implied)

# Whats missing in summary

The full transcript provides a comprehensive analysis of the dangers of supporting authoritarian power structures and the importance of holding individuals accountable for undermining democracy.

# Tags

#Senators #Republicans #Authoritarianism #Democracy #Accountability