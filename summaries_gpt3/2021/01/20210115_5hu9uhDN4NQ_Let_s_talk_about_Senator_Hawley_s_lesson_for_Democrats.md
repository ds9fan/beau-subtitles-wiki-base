# Bits

Beau says:

- Addressing the situation in Howley, Missouri, where calls for resignation are mounting due to his role in objecting to electoral votes.
- Criticizing politicians who participate in political stunts solely to energize their base and gain poll numbers.
- Noting that the best way to energize a base is through delivering for them, not through headline-grabbing or loyalty displays.
- Emphasizing that national politics should focus on national policy and representing the specific group of people who elected officials are meant to serve.
- Warning Democrats in power to deliver for those who were marginalized by the previous administration to secure future success.
- Stressing the need for Democrats to address the root problems that led to Trump's presidency rather than reverting to the status quo.

# Quotes

- "The best way to fire up your base? It's not through sensational headline grabbing. It's by delivering for them."
- "National politics has become about national policy."
- "The people who are sent to DC are sent there to represent a specific group of people."
- "If they want to have a successful 2022 or 2024, they have to deliver."
- "Cannot go back to the status quo."

# Oneliner

Addressing political stunts, Beau urges politicians to deliver for their constituents, warning against reverting to the status quo to avoid facing calls for resignation like Howley in Missouri.

# Audience

Politically Engaged Individuals

# On-the-ground actions from transcript

- Hold politicians accountable for delivering on promises and representing their constituents (implied)
- Advocate for policies that address the needs and concerns of marginalized communities (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the consequences of engaging in political stunts rather than focusing on delivering for constituents.