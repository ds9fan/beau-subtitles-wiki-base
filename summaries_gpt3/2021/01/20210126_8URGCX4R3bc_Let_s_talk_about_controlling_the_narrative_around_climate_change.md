# Bits

Beau says:

- Conducted first global ice loss survey from 1994 to 2017, revealing a loss of 28 trillion metric tons of ice at the caps.
- Impacting Antarctica and Greenland the most, losing about 1.2 trillion tons per year.
- Less ice means less heat reflected, exacerbating the problem.
- Current rate of loss puts us on track for worst-case climate scenarios from the IPCC.
- Paris Agreement, based on IPCC, may not be sufficient in addressing the crisis.
- Urges individuals to draw parallels with 2020's public health denials to understand climate change denial.
- Warns against letting those who deny climate change control the narrative.
- Urges immediate action as delays will hinder tangible results.
- Calls for political leaders and influencers to prioritize understanding and addressing climate change.
- Emphasizes the catastrophic consequences of worst-case climate scenarios.
- Stresses the importance of not allowing financial interests to manipulate the narrative around climate change.

# Quotes

- "When you're normally talking about weight and you want to visualize a large amount of weight, people compare it to a 747. Eighty billion. Eighty billion 747s."
- "We have to start acting now to achieve any tangible results."
- "The worst-case scenario from the IPCC, that's a disaster movie."
- "The narrative cannot be controlled by those people with deep pockets who have a vested financial interest in going against the scientific consensus."
- "Worst-case scenario, we make the world a better place for nothing."

# Oneliner

Confronting the alarming reality of global ice loss and urging immediate action to combat climate change denial and its catastrophic consequences.

# Audience

Climate activists, policymakers, environmentalists.

# On-the-ground actions from transcript

- Contact political representatives to prioritize climate change action (implied).
- Join or support organizations advocating for climate action (implied).
- Educate community members about the urgency of addressing climate change (implied).

# Whats missing in summary

Detailed breakdown of specific actions individuals can take to combat climate change denial and contribute to environmental conservation efforts.

# Tags

#ClimateChange #GlobalIceLoss #IPCC #ParisAgreement #ClimateAction