# Bits

Beau says:

- Beau shares a story about his friend's son who got his Twitter account taken away by his mom because he was picking on another kid over Twitter and was subsequently banned from going to a sports camp.
- Drawing a parallel, Beau points out that the President of the United States had his Twitter account taken away, yet still holds immense power as the commander in chief.
- Beau questions why those in power have not taken action to either convince the President to resign, invoke the 25th Amendment, or impeach him given the situation.
- He criticizes Congress for being hesitant to take action, with Democrats worried about political implications and Republicans concerned about appearances.
- Beau argues that regardless of political capital or appearances, it is Congress's duty to rein in the President, especially knowing his behavioral problems.
- Mentioning the recent change in Trump's tone after being convinced to read from a teleprompter, Beau expresses doubt on his sincerity and predicts a return to erratic behavior once the teleprompter is off.
- Beau suggests that Congress should share the blame for whatever actions Trump takes during his remaining time in office, as it is their responsibility to rein him in as the commander in chief.

# Quotes

- "It's their job to rein him in."
- "They know he has a behavioral problem."
- "But he's still commander in chief."
- "I don't know that I can trust that."
- "I don't know that that's a situation they should let stand."

# Oneliner

Beau questions why Congress fails to rein in a President stripped of Twitter, drawing parallels with his friend's son losing camp privileges. Congress shares blame.

# Audience

Congress members

# On-the-ground actions from transcript

- Convince the President to resign, invoke the 25th Amendment, or impeach him (implied)
- Take necessary steps to ensure the President's power is in checked (implied)

# Whats missing in summary

The emotional weight and urgency of the situation as Beau expresses deep concern over the lack of action from those in power. Watching the full video provides a more personal and impassioned perspective on the issue.

# Tags

#Congress #Trump #Twitter #Responsibility #Impeachment