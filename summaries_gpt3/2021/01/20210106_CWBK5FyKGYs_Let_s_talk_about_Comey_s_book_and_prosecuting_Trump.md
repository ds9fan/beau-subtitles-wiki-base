# Bits

Beau says:

- Comey suggests not pursuing criminal investigation of Trump by the next Attorney General, prioritizing fostering trust over justice.
- Beau criticizes politicizing the justice system and warns of the dangers to a free society.
- He expresses concern about shielding individuals from prosecution based on their political beliefs or popularity.
- Beau advocates for the importance of truth over political expediency in investigations.
- He warns of the danger of creating a government where some are untouchable and law enforcement can act with impunity.
- Beau believes failure to hold Trump accountable for any crimes sets a dangerous precedent for future leaders.
- He stresses the need for accountability, investigation, and prosecution if crimes were committed, regardless of political implications.
- Beau criticizes Comey's advice and questions its intentions in creating buzz for his book.
- He underscores the importance of a full accounting of Trump's actions to help the American population understand the gravity of his presidency.
- Beau calls for adherence to judicial principles and the importance of faith in the judicial system.

# Quotes

- "Politicizing the justice system is wrong and it's incredibly dangerous to a free society."
- "Nobody should be prosecuted simply because of their political beliefs and nobody should be shielded from prosecution simply because of their political beliefs or their popularity with a segment of the American population."
- "If there is no accountability for any crimes the President may have committed, it sends a message to the next would-be tyrant that there's no risk."
- "You don't make the determination based on what's politically expedient."
- "To those who are politically savvy, we know what President Trump was. We know what he was trying to do, but large portions of the American population do not understand it."

# Oneliner

Comey's advice on not investigating Trump raises concerns about justice versus fostering trust, leading Beau to advocate for accountability and a full accounting of Trump's actions.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Advocate for accountability and transparency in investigations (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the implications of not holding Trump accountable for potential crimes, stressing the importance of upholding justice for all individuals.

# Tags

#JamesComey #DonaldTrump #Accountability #Justice #PoliticalBeliefs