# Bits

Beau says:

- Explains the pushback against newly confirmed Secretary of Defense, General Austin, despite his qualifications and historic appointment as the first Black Secretary of Defense.
- Notes potential conflicts of interest due to Austin's previous role on the board of Raytheon, a company that does business with the Department of Defense.
- Raises concerns about the short time span between Austin leaving the military and assuming the position of Secretary of Defense, potentially impacting civilian control of the military.
- Emphasizes the importance of maintaining civilian control over the military and avoiding the trend of appointing recently retired generals as Defense Secretaries.
- Acknowledges that while he personally doesn't have concerns about Austin in this administration, the broader issue of civilian control is significant.

# Quotes

- "He's the first black Secretary of Defense. All of this is good."
- "We can't get into the habit of using retired generals, especially recently retired generals as Secretary of Defense."
- "The tradition of civilian control of the military is really important in this country."
- "He will probably end up being a pretty embattled Secretary of Defense."
- "Civilian control of the military is important."

# Oneliner

Beau explains the concerns surrounding General Austin's appointment as Secretary of Defense, from conflicts of interest to the importance of civilian control over the military.

# Audience

Policy Advocates

# On-the-ground actions from transcript

- Monitor for conflicts of interest and influence related to defense contractors like Raytheon (implied).
- Advocate for and support civilian oversight of military appointments (implied).

# Whats missing in summary

Detailed analysis and depth from Beau's perspective and insights.

# Tags

#SecretaryOfDefense #CivilianControl #ConflictsOfInterest #MilitaryOversight #GeneralAustin