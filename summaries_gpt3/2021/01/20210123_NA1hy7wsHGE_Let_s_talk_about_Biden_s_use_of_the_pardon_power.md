# Bits

Beau says:

- President Biden's pardon power is absolute, drawn from Article 2, Section 2, Clause 1 of the U.S. Constitution.
- 35 Democratic lawmakers have requested Biden to commute the sentences of 49 people facing the death penalty in the federal system.
- Commuting the sentences doesn't mean releasing them; it means changing their sentences to something else, likely life imprisonment.
- Biden can commute all sentences to life, ensuring they serve life without any political backlash.
- Biden's use of the pardon power won't be a significant political issue given the support it may garner.

# Quotes

- "He can save 49 lives with a pinstripe. No big deal."
- "There is no waste of political capital here."
- "At the end of the day, this is the right thing to do."

# Oneliner

President Biden holds absolute pardon power under the Constitution; he can commute death sentences to life without much political risk, potentially saving 49 lives in a bipartisan move.

# Audience

Lawmakers, Activists

# On-the-ground actions from transcript

- Advocate for the commutation of death sentences to life imprisonment (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of President Biden's pardon power and the potential positive impact of commuting death sentences to life imprisonment, stressing the absence of political downside.

# Tags

#PardonPower #PresidentBiden #CommuteSentences #DeathPenalty #PoliticalImpact