# Bits

Beau says:

- Trump's impeachment and Biden's first hundred days set the tone for a new administration.
- Majority of Americans and Biden voters didn't passionately support Biden; he was seen as the best chance to oust Trump.
- Biden's campaign was centered on the idea of undoing the damage done by Trump, not on being a super progressive candidate.
- Beau advises Biden to lean into the impeachment process and not shy away from it.
- Biden should focus on undoing Trump's executive orders and reshaping the political landscape during the impeachment.
- Emphasizes the importance of accountability for those in power and undoing the damage caused by Trump to set the tone for the administration.
- Biden needs to reach across the aisle for unity but should prioritize those who opposed Trumpism, not those who supported totalitarianism.
- The Biden administration must undo Trump's actions and move forward with significant legislative changes to ensure real progress.
- Simply not being Trump won't be enough for a successful Biden re-election or Harris election; real changes and undoing Trump's policies are necessary.
- Beau suggests that Biden needs to show that his administration is more than just "not Trump" and must focus on real legislation and changes.

# Quotes

- "Lean into it. Stick with it. Don't hide from the impeachment."
- "They have to undo the previous administration and then they have to move forward."
- "Reach across to those who opposed Trumpism, not those who brought us to the abyss of totalitarianism."
- "Show us that."

# Oneliner

Beau advises Biden to lean into the impeachment process, undo Trump's damage, and prioritize real changes over simply not being Trump to set the tone for his administration.

# Audience

Political advisors, Biden administration

# On-the-ground actions from transcript

- Lean into the impeachment process and use it to undo Trump's executive orders (implied).
- Focus on reshaping the political landscape during the impeachment (implied).
- Reach across the aisle for unity with those who opposed Trumpism (implied).
- Prioritize undoing Trump's actions and enacting real legislative changes (implied).

# Whats missing in summary

The detailed nuances and explanations behind Beau's suggestions are best understood by watching the full transcript.

# Tags

#Biden #Trump #Impeachment #Accountability #PoliticalChange