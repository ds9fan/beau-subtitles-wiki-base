# Bits

Beau says:

- The media failed by focusing on sensational quotes rather than the true message.
- The message from the Joint Chiefs was intended for civilian leadership and the average American.
- Professional soldiers are trained to convey calm in high-stress situations.
- Officers with stars on their shoulders use the word "will" as a statement of fact from the future.
- The military will obey lawful orders, support civil authorities, and protect the Constitution against all enemies.
- Any act to disrupt the constitutional process is against their traditions, values, and oath.
- The military will ensure the inauguration of President-elect Biden on January 20th, 2020.
- The message was designed to convey calm and reassure the public about a peaceful transition of power.
- Beau criticizes political and media figures for creating a situation where the Joint Chiefs had to assure an orderly transition of power.
- The U.S. military will ignore civilian leadership (Trump's appointees) and ensure a smooth transition to the new president.

# Quotes

- "The U.S. military is flat out saying they're going to ignore civilian leadership, that's Trump's appointees."
- "It is appalling that this message had to be drafted."
- "The media did a disservice because they went through and they pulled out the most interesting quotes."
- "The word 'will' doesn't just mean will. It is a statement of fact from the future."
- "The American people have trusted the armed forces of the United States to protect them and our Constitution for almost 250 years."

# Oneliner

Beau reveals the Joint Chiefs' message, assuring civilian leadership and the public of a peaceful transition of power amid political turbulence.

# Audience

Americans

# On-the-ground actions from transcript

- Support the values and ideals of the nation (implied)
- Ensure public safety in accordance with the law (implied)
- Uphold the Constitution against all enemies (implied)

# Whats missing in summary

Deeper insights into the impact of political and media figures on military messaging can be gained from watching the full transcript. 

# Tags

#Military #TransitionOfPower #USConstitution #ProfessionalSoldiers #MediaSensationalism