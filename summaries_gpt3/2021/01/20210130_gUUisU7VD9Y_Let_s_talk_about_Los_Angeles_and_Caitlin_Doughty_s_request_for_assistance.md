# Bits

Beau says:

- Beau addresses the lack of response in LA, California, to a situation requiring assistance.
- Caitlin Doughty, a popular personality on YouTube and owner of a funeral home, requested help due to the overloaded network in LA.
- The Department of Defense has a plan, known as DSCA, that includes dealing with recovery, identification, registration, and disposal.
- Despite the plan being on the books for a long time, it hasn't been implemented due to politics.
- The process of assistance can be initiated through a request from California or a directive from the Secretary of Defense or the president.
- Budgetary concerns are the only potential obstacle on the checklist for assistance from the Department of Defense.
- Beau calls on President Biden to take quick action as he does not bear responsibility for previous inaction.
- Beau urges viewers to pressure for assistance as the plans, program, and personnel exist for a rapid solution.
- This situation serves as a reminder that the public health crisis is ongoing, and precautions like handwashing and mask-wearing are still necessary.
- Beau encourages viewers to watch Caitlin's video, take appropriate action, and help those in need during this challenging time.

# Quotes

- "This should have been the plan from day one. It was. It was the plan since way before day one. Why didn't it happen? Politics."
- "If you're having to call in the Department of Defense to assist in disposal, your mitigation efforts didn't go so well."
- "Biden is new. He can make this happen very quickly."

# Oneliner

Beau addresses the lack of response in LA to a situation requiring assistance, discussing the Department of Defense's plan and urging action from President Biden to provide help swiftly. 

# Audience

Advocates for swift assistance

# On-the-ground actions from transcript

- Contact California officials or urge the Secretary of Defense or President to initiate assistance (suggested)
- Pressure for quick action from President Biden to utilize existing plans and resources for assistance (suggested)

# Whats missing in summary

The full transcript provides detailed insights into the lack of response in LA, the existing plans within the Department of Defense, and the call for swift action from President Biden to provide necessary assistance.

# Tags

#Assistance #DepartmentOfDefense #PublicHealth #CrisisResponse #PoliticalInaction