# Bits

Beau says:

- President Trump plans to lean into a conflict with social media companies to distract from his failings.
- The purpose of the Bill of Rights is to protect individuals and states from the federal government.
- Private entities like Twitter are not obligated to platform President Trump; it's not a First Amendment issue.
- Trump's attempt to force a private entity to carry his message could be seen as a First Amendment violation.
- Trump's actions post-Capitol breach shouldn't overshadow the severity of allowing the Capitol to fall.
- Trump's tactics aim to manipulate media, emotions, and shift focus from his failures.

# Quotes

- "The president of the United States allowed the Capitol to fall. That's the story."
- "Don't allow yourself to be distracted by this. Don't get sucked into this."
- "He is very good at manipulating the media and manipulating people's emotions and changing the story."

# Oneliner

President Trump aims to distract from failings by sparking conflict with social media companies, but the real story remains: allowing the Capitol to fall.

# Audience

American citizens

# On-the-ground actions from transcript

- Stay informed and focused on the significant events rather than getting distracted (exemplified)
- Be vigilant against attempts to manipulate media narratives (exemplified)

# Whats missing in summary

The transcript delves into the importance of not allowing distractions to overshadow critical events like the Capitol breach and encourages staying focused on the truth amidst manipulation attempts.

# Tags

#Trump #FirstAmendment #SocialMedia #Distraction #CapitolBreach