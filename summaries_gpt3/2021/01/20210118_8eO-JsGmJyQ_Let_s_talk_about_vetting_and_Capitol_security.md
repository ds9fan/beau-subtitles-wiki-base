# Bits

Beau says:

- Explains the difference between perceived capability and actual capability in security measures.
- Gives examples of perceived capability (like a stadium on game day) and actual capability (like Disney World).
- Talks about the large security presence in DC and why there are so many troops present.
- Mentions the rings of security, with the National Guard likely being on the outer rings.
- Clarifies that the FBI vetting troops is a common practice, not necessarily due to a specific threat.
- States that the FBI is involved in vetting due to their expertise and quick processing abilities.
- Addresses the possibility of a threat from the troops but deems it unlikely due to the security measures in place.
- Emphasizes the shift from relying on perceived capability to actual capability after the events at the Capitol.
- Notes that while there are many troops present, there doesn't seem to be a specific threat identified.
- Points out the broader security concerns beyond just the immediate event, considering other opposition groups.

# Quotes

- "90% of security is perception."
- "As far as I know, as far as the public statements, as far as the activities that we're seeing, nothing indicates that they have a specific threat or that they have flagged anybody."
- "Because of the events of the Capitol and the actions of the people there, all of the foreign opposition groups are emboldened."

# Oneliner

Beau explains the difference between perceived and actual security capabilities in DC, addressing the troop presence and FBI vetting without a specific threat.

# Audience

Security analysts, event planners.

# On-the-ground actions from transcript

- Trust the security measures in place (implied).
- Be vigilant and report any suspicious activity (implied).

# Whats missing in summary

Insight into how recent events have impacted security measures and perceptions.

# Tags

#Security #PerceivedCapability #ActualCapability #FBI #NationalGuard