# Bits

Beau says:

- Analyzes moments in Trump's phone call to Georgia, noting his belief in his own propaganda and rhetoric.
- Describes how authoritarians like Trump surround themselves with fervent supporters, leading them to believe their rhetoric is true.
- Points out humorous and dangerous situations in the phone call where Trump dismisses contradictory information.
- Mentions cognitive dissonance experienced by Trump during the call when faced with opposing information.
- Critiques Trump's theory of a rigged election by Democrats and questions its feasibility.
- Suggests that many Republicans may have voted against Trump while maintaining party loyalty.
- Draws parallels between Trump's belief in his propaganda and rhetoric to how police sometimes believe they have the most dangerous job.
- Advises being mindful of the slogans and rhetoric one allows in life, as repeated exposure can lead to belief in them.
- Emphasizes the importance of positive slogans that encourage positive action.
- Warns about negative propaganda energizing but needing to be channeled into productive action.

# Quotes

- "Be aware of the rhetoric you use."
- "Make sure they're positive."
- "Words have power."
- "Be careful of the ones you allow in your life."
- "Y'all have a good night."

# Oneliner

Be conscientious of the slogans and rhetoric you allow in your life; words have power.

# Audience

Critical Thinkers

# On-the-ground actions from transcript

- Monitor and be mindful of the slogans and rhetoric you allow in your life (suggested)
- Surround yourself with positive rhetoric that spurs you to positive action (suggested)

# Whats missing in summary

The full transcript provides a deeper analysis of the dangers of believing one's propaganda and the importance of being vigilant about the information and rhetoric one consumes. 

# Tags

#Propaganda #Rhetoric #Belief #CognitiveDissonance #PositiveAction