# Bits

Beau says:

- Incoming Biden administration plans immigration reform package post-inauguration.
- Includes pathway to citizenship for those out of status.
- Conservative movement opposes reform, citing job loss and law-breaking.
- Beau questions opposition as bigotry, as bill details are not yet known.
- Conservative argument of needing to do things legally contradicts opposition to legal mechanism in bill.
- Reform bill details not public, could include stringent background checks and long waiting periods.
- Beau advocates for waiting to see bill before forming opinions.
- Urges basing decisions on policy rather than emotion or preconceived notions.
- Opposes bill if it doesn't go far enough but stresses importance of informed decision-making.
- Encourages audience to avoid manipulation and wait for bill details.

# Quotes

- "Base your decisions on policy rather than raw emotion."
- "It has nothing to do with policy. It has to do with skin tone."
- "Perhaps it would be best to wait and see the bill."
- "Y'all have a good day."

# Oneliner

Incoming immigration reform package sparks conservative backlash pre-details, prompting Beau to call out opposition as bigotry and advocate for informed, policy-based decisions.

# Audience

Advocates, Voters, Activists

# On-the-ground actions from transcript

- Wait for the immigration reform bill details before forming opinions (suggested)
- Base decisions on policy rather than emotion (suggested)
- Advocate for informed decision-making on immigration reform (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the conservative opposition to immigration reform and underscores the importance of informed decision-making based on policy details rather than preconceived notions or emotions.