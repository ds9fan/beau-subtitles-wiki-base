# Bits

Beau says:

- Talks about the importance of context in understanding events and politics.
- Mentions how we often ignore details to reinforce our own beliefs.
- Gives examples of how context determines moral and societal value.
- Contrasts two different events: the protests of this summer and the Capitol Hill riots.
- Emphasizes the significance of looking at the surrounding details to make informed judgments.
- Reminds us of the Civil Rights Movement and the importance of context in assessing actions.
- Points out that riots can be seen as the "voice of the unheard."
- Stresses the need to understand the context behind news and events for a deeper understanding.
- Encourages examining details like who, what, when, where, and why to grasp the full story.

# Quotes

- "Context is what determines moral value, societal value."
- "Riots are the language of the unheard."
- "You have to look at the context."

# Oneliner

Beau stresses the vital role of context in understanding events, contrasting the Capitol Hill riots with this summer's protests and reminding us of the Civil Rights Movement.

# Audience

Students, Activists, Educators

# On-the-ground actions from transcript

- Examine the context behind events to understand the full story (implied).

# Whats missing in summary

The full transcript provides a comprehensive exploration of the impact and importance of context in analyzing events and understanding societal dynamics. Viewing the entire talk offers a deeper insight into the significance of details often overlooked in mainstream narratives.

# Tags

#Context #Understanding #Events #Politics #CivilRights #Protests