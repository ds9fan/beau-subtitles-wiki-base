# Bits

Beau says:

- Explains Biden's executive orders and unconventional approach.
- Details the content and significance of each executive order.
- Mentions the extension of eviction and foreclosure moratoriums.
- Talks about pausing student loan payments and interest.
- Addresses rejoining the Paris Accord and abolishing the 1776 Commission.
- Comments on actions regarding diversity training and equity in policymaking.
- Notes the inclusion of undocumented immigrants in the census.
- Talks about preserving DACA and revoking the Muslim ban.
- Mentions changes in immigration enforcement policies.
- Comments on halting the wall construction and ending the emergency declaration.
- Talks about non-deportation of Liberians until 2022.
- Addresses interpreting the Civil Rights Act to include workplace discrimination based on orientation and identity.
- Mentions new ethics rules and reversing Trump's regulatory process changes.
- Explains how Biden fulfilled major goals through executive orders without legislation.

# Quotes

- "It's the patriotic thing to do."
- "Undocumented immigrants will be counted in the census."
- "That's going to have immediate tangible effects."
- "The United States is not a small town."
- "He can't run it in that manner."

# Oneliner

Beau explains Biden's unconventional approach to executive orders, detailing their content and significance while fulfilling major goals without legislation, addressing pressing issues with immediate tangible effects and setting the tone for his administration.

# Audience

Policy advocates

# On-the-ground actions from transcript

- Contact local representatives to advocate for the continuation of policies like eviction and foreclosure moratoriums (implied).
- Join or support organizations promoting diversity training and equity in policymaking (implied).
- Organize events or workshops to raise awareness about workplace discrimination based on orientation and identity (implied).

# Whats missing in summary

Insight into Beau's perspective and analysis on Biden's executive orders beyond the summarized points.

# Tags

#Biden #ExecutiveOrders #Policy #ProgressiveLeft #Equity