# Bits

Beau says:

- Former Secretary of Defense, General Mattis, had a private, non-publicized chat with Michael Vickers on the OSS Society webcast.
- They discussed external and internal issues facing the United States, including the erosion of democracy and the events of January 6th.
- General Mattis expressed concern about the erosion of democracy and criticized the actions of a sitting president.
- Michael Vickers emphasized the oath to the Constitution against all enemies, foreign or domestic, and expressed disbelief at the insurrection on January 6th.
- Vickers, less known than Mattis, has an extensive military and intelligence background, bringing credibility to his statements.
- Those well-versed in military matters see the events of January 6th differently from a political or legal perspective.
- Individuals like Mattis and Vickers, critical for such operations, do not support actions undermining democracy.
- Lack of accountability for such actions can lead to further attempts, considering failures as training rather than defeat.
- The Republican Party's prioritization of politics over the nation's well-being is concerning.
- Those who truly understand the situation view internal threats as more dangerous than external ones, questioning the patriotism of individuals putting politics over democracy.

# Quotes

- "They cannot pretend to be patriots. They cannot pretend to care about this country or they wouldn't be putting their own political careers over the underpinnings of a democracy."
- "Those who understand how these things go do not want to see it happen in the United States."
- "Not really something that was designed or intended to be widely distributed."

# Oneliner

Former Secretary of Defense Mattis and Michael Vickers privately discussed the erosion of democracy and the dangers of internal threats, criticizing actions that prioritize politics over the nation's well-being.

# Audience

Citizens, patriots, activists.

# On-the-ground actions from transcript

- Contact local representatives to prioritize democracy over politics (suggested).
- Join organizations advocating for accountability in political actions (implied).

# Whats missing in summary

Insights on the importance of upholding democratic values and accountability in political leadership.

# Tags

#Democracy #Mattis #InternalThreats #Accountability #PoliticalLeadership