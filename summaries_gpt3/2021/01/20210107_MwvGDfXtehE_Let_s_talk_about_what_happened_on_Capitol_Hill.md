# Bits

Beau says:

- Describes the events on Capitol Hill as an attempted coup to disrupt the transfer of power.
- Notes the difference between a revolution and a coup, where a coup focuses on changing the head of state, in this case, from Biden back to Trump.
- Mentions that the attempted coup failed due to lack of military support, which is a critical element for a successful coup.
- Urges Americans to acknowledge the seriousness of the event and not downplay it as it could lead to a normalization of authoritarian methods.
- Advocates for holding those responsible at the top levels accountable for fostering an environment that allowed the coup attempt to happen.
- Emphasizes the importance of becoming anti-authoritarian and making it the societal norm to reject authoritarian measures from the government.
- Calls for the removal of President Trump through the 25th Amendment, impeachment, or resignation, citing his erratic behavior as a reason.
- Stresses the need for citizens to set the tone against authoritarianism and work towards walking back from the edge to prevent such incidents in the future.

# Quotes

- "It was an attempted coup."
- "Nobody should suffer under the boot of authoritarianism."
- "For those who propose militarizing the police and militant action against the citizens by the government, that has to be outside the norm."

# Oneliner

Beau stresses the seriousness of the attempted coup on Capitol Hill, urges accountability, advocates for anti-authoritarianism, and calls for the removal of President Trump to prevent authoritarianism becoming the norm.

# Audience

Americans

# On-the-ground actions from transcript

- Hold those responsible at the top levels accountable for fostering an environment that allowed the coup attempt (implied).
- Advocate and work towards making anti-authoritarianism the societal norm by rejecting authoritarian measures from the government (implied).
- Support each other and seek equality and freedom, not oppression (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the attempted coup on Capitol Hill, the risks of authoritarianism, and the necessary steps for Americans to prevent such incidents in the future.

# Tags

#CapitolHill #CoupAttempt #AntiAuthoritarianism #Accountability #RemovalOfTrump