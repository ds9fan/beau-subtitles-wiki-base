# Bits

Beau says:

- Beau questions the portrayal of the Capitol crew as highly trained based on basic skills like using hand signals and getting into a stack.
- The crew did not properly execute the stack formation, revealing they lacked real training and experience.
- Beau explains the correct way to use the non-firing hand in a stack to maintain control of the weapon.
- By falsely portraying the Capitol crew as highly trained, the media may unintentionally elevate them to folk hero status.
- Beau warns against giving these groups or individuals airtime as it could serve as a platform for their PR campaigns with violence.
- He stresses that allowing inaccurate statements to be broadcast can be detrimental and shift public opinion.
- Beau points out that only a very small percentage of individuals had actual training present at the Capitol incident.
- He expresses concern that perpetuating the false image of highly trained opposition forces could fuel the movement's growth.
- Muscle memory from proper training, such as using the left hand in a specific situation, was absent in the Capitol crew's actions.
- Beau urges for a stop to the portrayal of untrained individuals as heroes, as it could have damaging consequences.

# Quotes

- "Those guys were play acting. They were pretending."
- "Your ratings are not worth ripping the country apart."
- "Even though they were unarmed, if they had ever trained, muscle memory..."
- "Don't make them out to be heroes."
- "That's probably really bad for the country."

# Oneliner

Beau questions the media's portrayal of the Capitol crew as highly trained, warning against elevating untrained individuals to hero status and fueling their movement's growth.

# Audience

Media consumers

# On-the-ground actions from transcript

- Refrain from giving airtime or publicity to groups or individuals with violent intentions (implied)
- Be critical of media portrayals and question narratives that could potentially glorify untrained individuals (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of media portrayal and its potential impact on public perception and movements.

# Tags

#Media #Training #Capitol #Violence #PublicOpinion