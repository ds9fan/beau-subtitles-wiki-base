# Bits

Beau says:

- Beau responds to a request to catalog Trump's accomplishments on his last day to understand his supporters.
- Beau outlines six positive aspects of Trump's presidency appreciated by his supporters, such as promoting nationalism and traditional family roles.
- Trump's supporters admired his focus on national security, clear identification of enemies, and prioritization of the military.
- Despite differing perspectives, Beau acknowledges that Trump's supporters valued his disregard for conventions in the pursuit of national security.
- Beau mentions Trump's negative action of engaging in cronyism by hiring family members for unqualified positions.
- Trump's refusal to let the media control him, protection of American business interests, and emphasis on law and order were key points for his supporters.
- Additionally, Trump's stance on religion and undermining of elections are discussed as positive and negative attributes by his supporters.
- Beau concludes by linking the 14 characteristics of fascism to the positive and negative aspects of Trump's presidency, raising concerns about the concentration of power.

# Quotes

- "His supporters loved the fact that under him nobody had to be ashamed that they loved the American flag or the bald eagle."
- "He engaged in cronyism. He hired his family members who were less than qualified for those positions."
- "That's why there was so much opposition. That's why people were so worried about the amount of power he was gaining."

# Oneliner

Beau outlines positive and negative aspects of Trump's presidency, reflecting on the characteristics of fascism, prompting reflection on the concentration of power.

# Audience

Supporters and skeptics alike.

# On-the-ground actions from transcript

- Listen to differing perspectives on Trump's presidency (suggested).
- Recognize the potential impact of concentrated power (implied).

# Whats missing in summary

A deeper understanding of the nuances and implications of Trump's presidency.

# Tags

#Understanding #Trump #Supporters #Fascism #Power