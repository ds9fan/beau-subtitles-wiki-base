# Bits

Beau says:

- Biden's new plan doesn't account for energy jobs, including those in the oil sector.
- The energy industry has been declining, with companies like BP planning to cut fossil fuel output by 40% by 2030.
- Technology is making many jobs in the industry obsolete.
- The industry has had significant political power and received handouts to stay afloat.
- Manufacturing jobs are unlikely to return as automation takes over.
- Workers in the energy industry should advocate for educational programs to transition to new jobs.
- Rather than supporting billionaires, workers should advocate for their own interests as the industry declines.
- Workers should not expect the industry to take care of them when it disappears.
- Jobs in cleaning up spills are also disappearing.
- Advocating for educational programs can help workers transition to new jobs.

# Quotes

- "Technology is making those jobs obsolete."
- "Manufacturing may come back, but the jobs won't."
- "Advocate for your own interest for a change."
- "They're not going to take care of you."
- "Those jobs are going away too. It's probably a good thing."

# Oneliner

Biden's plan overlooks energy jobs that technology is making obsolete, urging workers to advocate for their own interests amidst industry decline.

# Audience

Workers in the energy industry

# On-the-ground actions from transcript

- Advocate for educational programs to transition to new jobs (implied)
- Call up senators and representatives to push for programs benefiting workers (implied)

# Whats missing in summary

Beau's insightful commentary and call for workers to prioritize their interests amidst industry changes can best be appreciated by watching the full transcript.

# Tags

#EnergyJobs #IndustryDecline #Advocacy #Transition #Automation