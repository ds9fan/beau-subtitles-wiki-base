# Bits

Beau says:

- Explains the levels of U.S. State Department travel advisories: level one, everything's cool; level two, use caution; level three, reconsider your trip; level four, get out; and an unofficial fifth level, get out now no matter what.
- Details the embassy's escalating warnings to leave Afghanistan, culminating in a blunt "get out" notice on August 7th.
- Mentions the significance of embassies discussing repatriation loans as a signal to leave immediately.
- Notes that around 10,000 Americans were still in Afghanistan, with the official number likely being low due to some not checking in.
- Emphasizes the importance of paying attention to travel advisories for those traveling overseas or knowing someone who does.
- Addresses the cooperation with the opposition in Afghanistan to evacuate people and criticizes those who are against negotiating with the opposition.
- Argues that negotiating with the opposition is a normal part of post-war processes and that portraying surprise at this is uninformed.
- Asserts that the opposition in Afghanistan has nothing to gain from harming Americans and that such ideas stem from wartime propaganda.
- Stresses that negotiating, cooperating, and exchanging prisoners after wars are standard procedures.
- Encourages removing identifying information from photos of Afghan nationals for veterans who worked with them.

# Quotes

- "Once the US effort is done, there will still be stragglers. There'll still be people there, and nothing will let you know how bad off you are as the appearance of those people who are coming to get you."
- "Acting as if you're surprised [about negotiating with the opposition after war], that doesn't lend a whole lot to your credibility."
- "The surest way to avoid something like this happening again is to not engage in these types of military interventions."

# Oneliner

Beau explains the importance of heeding travel advisories, the necessity of negotiating with opposition post-war, and the need to remove identifiers from photos of Afghan nationals for veterans.

# Audience

Travelers and advocates.

# On-the-ground actions from transcript

- Blur identifying features in photos of Afghan nationals (suggested).
- Pay attention to State Department travel advisories when traveling overseas (suggested).

# Whats missing in summary

The full transcript provides detailed insights on the significance of travel advisories, negotiations with opposition post-war, and the importance of protecting identities in photos of Afghan nationals for veterans.

# Tags

#TravelAdvisories #Negotiation #Afghanistan #Veterans #CommunitySafety