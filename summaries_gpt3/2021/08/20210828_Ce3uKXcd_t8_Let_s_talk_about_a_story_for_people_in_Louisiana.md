# Bits

Beau says:
- Tells a funny story about a friend who enjoys hurricane parties in Louisiana.
- Friend's wife experiences her first hurricane, Hurricane Michael, causing unease.
- Urges people in Louisiana to pay attention to Hurricane Ida, scheduled to hit as a Category 4 with potential storm surges.
- Mentions the devastation caused by Hurricane Michael, with businesses still unrepaired to this day.
- Advises those in low-lying areas to evacuate and prepare emergency supplies, including medications, pets, and phone batteries.
- Emphasizes the importance of heeding evacuation warnings and not waiting until it's too late.
- Encourages residents to ensure they are prepared for rescue operations rather than recovery operations.
- Stresses the seriousness of Hurricane Ida and the potential damage it could cause based on where it hits.

# Quotes

- "Get your meds, get all of your emergency supplies, get your little battery for your phone, get everything that you need, your pets, your pet food, and get inland."
- "Make sure that those Cajun guys who are going to come help are going to be doing rescue operations and not recovery operations."
- "If you're in Louisiana, take this one seriously, because it is doing all of that same creepy stuff with the changes."

# Oneliner

Beau urges Louisiana residents to take Hurricane Ida seriously, evacuate if necessary, and prepare for potential storm surges and damage.

# Audience

Louisiana residents

# On-the-ground actions from transcript

- Evacuate if in low-lying areas and gather emergency supplies (suggested)
- Ensure you have medications, emergency supplies, pet essentials, and evacuation plans ready (suggested)
- Prepare for rescue operations by being inland and out of harm's way (suggested)

# Whats missing in summary

The emotional impact of experiencing a hurricane and the urgency of preparing and evacuating to ensure safety. 

# Tags

#Louisiana #HurricaneIda #Evacuation #EmergencyPreparedness #NaturalDisaster #CommunitySafety