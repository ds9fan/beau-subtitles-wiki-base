# Bits

Beau says:

- Small towns are experiencing people coming from larger cities to buy a certain anti-parasitic, ivermectin, commonly used in livestock, horses, and sheep.
- The feed store owner in small towns quizzes buyers about the animals the product is for and won't sell if they don't know basic answers, as they prioritize local customers over outsiders.
- Misinformation on the benefits of ivermectin for COVID-19 has led people to seek it out at feed stores based on Facebook memes and search results.
- Instead of focusing on what ivermectin does as an anti-parasitic, Beau suggests reframing the narrative to its true nature as a neurotoxin to dissuade potential consumers.
- Beau warns against the dangerous dosing differences between animals like dogs and humans when it comes to ivermectin, with severe consequences for improper usage.
- Merck, the pharmaceutical company that manufactures ivermectin, has publicly stated that there is no scientific basis or evidence to support its use as a COVID-19 treatment.
- Despite claims that ivermectin works and is being suppressed by Big Pharma, Merck's position contradicts this narrative as they express doubts about its efficacy and safety.
- Beau questions the logic of risking one's life by seeking out ivermectin when vaccines, readily available at numerous locations, provide proven protection against COVID-19.
- He stresses the importance of not falling for misinformation and desperation when safer alternatives like vaccines exist, urging people not to put their lives at risk by pursuing unproven treatments.
- Beau concludes by reminding viewers of the consequences of early dismissals of COVID-19 severity and mitigation efforts, warning of potential lack of hospital beds for those who may fall ill.

# Quotes

- "Perhaps we should stop talking about what it does, it's an anti-parasitic, and start talking about what it is, a neurotoxin."
- "Is your pride worth your life?"
- "Don't do this."

# Oneliner

Beau warns against the dangers of seeking out ivermectin as a COVID-19 treatment, stressing the importance of accurate information and available alternatives to protect lives.

# Audience

Health-conscious individuals

# On-the-ground actions from transcript

- Refrain from seeking out and using ivermectin as a COVID-19 treatment (implied)
- Prioritize getting vaccinated against COVID-19 for protection (implied)
- Educate others on the risks and lack of scientific evidence supporting ivermectin as a COVID-19 treatment (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the misinformation surrounding ivermectin as a COVID-19 treatment and the potentially fatal consequences of its misuse.

# Tags

#COVID-19 #Ivermectin #Misinformation #Vaccination #PublicHealth