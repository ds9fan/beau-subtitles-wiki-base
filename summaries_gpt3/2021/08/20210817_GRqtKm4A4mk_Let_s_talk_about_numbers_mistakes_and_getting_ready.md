# Bits

Beau says:

- The Delta variant shows no signs of peaking, with a possibility of 200,000 cases a day.
- Nearly 2,000 children were in hospitals due to the virus, setting a record.
- It's a mistake not to be vaccinated, but one that can be corrected.
- Unvaccinated individuals are described as "sitting ducks" by the director of NIH.
- Numbers remain high for those who haven't received their shots.
- Preparing for a potential scenario where lockdowns may occur again is advised.
- Stocking up on essentials in case of restrictions and supply chain disruptions is suggested.
- Politicians denying the situation may only act when it becomes unbearable.
- A rush on stores and disruptions in the supply chain are possible with sudden lockdowns.
- Contacting a trusted local doctor for vaccination advice is recommended amidst misinformation.

# Quotes

- "It's a mistake not to be vaccinated, but one that can be corrected."
- "Call your doctor, your doctor, not a YouTube doctor, not a doctor on Twitter or Facebook."
- "Your local doc, you probably trust them."

# Oneliner

Be prepared for potential lockdowns and supply chain disruptions by getting vaccinated and stocking up on essentials, while seeking advice from a trusted local doctor amidst misinformation.

# Audience

Community members

# On-the-ground actions from transcript

- Contact your local doctor for vaccination advice (suggested)
- Stock up on essentials in case of potential lockdowns (implied)

# Whats missing in summary

Importance of seeking accurate information and guidance from reliable healthcare professionals and preparing for possible future scenarios.

# Tags

#COVID-19 #DeltaVariant #Vaccination #LockdownPreparation #HealthcareAdvice