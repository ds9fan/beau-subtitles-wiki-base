# Bits

Beau says:

- Defines tough guys as those willing to do the hard right thing over the easy wrong thing, not necessarily the traditional image.
- Describes the failure of mitigation efforts in Florida and the resulting stress on medical professionals.
- Shares his wife's experience as a nurse who returned to work due to the shortage of medical staff in Florida.
- Hails individuals like his wife and Nikki Freed, Secretary of Agriculture, as tough guys for stepping up and providing leadership during challenging times.
- Contrasts genuine tough guys, like medical professionals and leaders advocating for public health measures, with those who pose as tough but fail to take basic precautions themselves.
- Stresses the importance of true leadership in the face of widespread misinformation and the need for more people to step up and provide guidance.
- Emphasizes that toughness is not about appearances or symbols like guns, but about doing what is right even when it's difficult.

# Quotes

- "What makes you tough is the willingness to step up and do the right thing when others won't."
- "We need more tough guys willing to step up, to lead, to provide the guidance that the country needs."

# Oneliner

Beau defines tough guys as those willing to do the hard right thing over the easy wrong thing, applauding medical professionals and leaders like Nikki Freed for providing genuine leadership during challenging times.

# Audience

Community members, activists, voters

# On-the-ground actions from transcript

- Support medical professionals by volunteering at hospitals or clinics to alleviate staffing shortages (implied)
- Advocate for public health measures like wearing masks and getting vaccinated to protect the community (implied)

# Whats missing in summary

The full transcript delves deeper into the challenges faced by medical professionals due to the failure of mitigation efforts in Florida and the importance of true leadership in combating misinformation and saving lives. 

# Tags

#Leadership #PublicHealth #CommunitySupport #Misinformation #Florida