# Bits

Beau says:

- Beau introduces a technique to reach out to parents based on an old army training film.
- The technique involves asking questions rather than telling parents they are wrong.
- Beau shares a story from the training film about a second lieutenant convincing a first sergeant about the effectiveness of a device.
- The key is to ask questions that lead parents to new information or perspectives.
- The approach is non-confrontational and aims to avoid conflicts during family gatherings.
- By using this tactic, Beau suggests that parents may see their children as having valid opinions.
- It's about managing perceptions rather than trying to change reality.
- Beau recommends being strategic in how you communicate with parents, especially when they hold different beliefs.
- The goal is to guide parents towards considering alternative viewpoints without direct confrontation.
- This technique has been effective for Beau in various situations over his life.

# Quotes

- "Rather than telling them they're wrong, ask them questions that they may not necessarily know the answer to."
- "It's about managing perceptions rather than trying to change reality."
- "You can't just walk in there and say you're wrong. I mean, you can, but it's not going to be productive."

# Oneliner

Beau introduces a technique to reach out to parents by asking strategic questions, aiming to guide them towards new perspectives non-confrontationally.

# Audience

Parents, Children

# On-the-ground actions from transcript

- Ask questions to guide parents towards new information or perspectives (suggested)
- Approach challenging topics with strategic questioning rather than direct confrontation (exemplified)

# Whats missing in summary

The full transcript provides detailed examples and insights on effectively communicating with parents by asking questions rather than engaging in confrontations. Viewing the entire transcript offers a comprehensive understanding of Beau's approach in navigating differing viewpoints within family dynamics.

# Tags

#ParentalCommunication #FamilyDynamics #NonConfrontationalApproach #PerspectiveShift #EffectiveCommunication