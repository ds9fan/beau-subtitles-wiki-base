# Bits

Beau says:

- Trump advocated for people to get vaccinated during a rally in Alabama, but his supporters booed him.
- Despite the booing, Trump recommended getting vaccinated and mentioned that he himself is vaccinated.
- Beau praises Trump for not pandering to his supporters and telling them the truth.
- The shift in tone within the Republican Party is due to realizing the impact of spreading anti-science beliefs.
- The Republican Party's base is losing segments due to their commitment to anti-science rhetoric, potentially affecting elections.
- Beau compares knowledge to wisdom using the Frankenstein analogy to illustrate the political establishment's situation.
- The establishment's promotion of conspiracy theories has backfired, leading to a lack of control and backlash from their own base.

# Quotes

- "I recommend you get the vaccine."
- "He told a group of his supporters something that they didn't want to hear."
- "Wisdom is knowing that the doctor was the monster."
- "You have a political establishment that pandered and it breathed life into this conspiratorial anti-science nonsense."
- "Think about what makes them cheer."

# Oneliner

Former President Trump recommended getting vaccinated, faced boos from supporters, and sparked a shift in Republican Party tone towards anti-science beliefs.

# Audience

Political observers

# On-the-ground actions from transcript

- Challenge anti-science rhetoric within your community (implied)
- Support leaders who prioritize truth over pandering (implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on the impact of political rhetoric on public health and the need for honesty in leadership.

# Tags

#Trump #Vaccination #RepublicanParty #AntiScience #Leadership