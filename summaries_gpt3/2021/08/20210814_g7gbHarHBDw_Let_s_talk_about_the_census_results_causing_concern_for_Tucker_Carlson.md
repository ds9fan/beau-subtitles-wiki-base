# Bits

Beau says:

- Addressing concerns about census results and information causing worry among some people.
- Sharing a message from a conservative viewer questioning lack of concern about decreasing white population.
- Mentioning watching conservative media that downplay the challenges faced by minorities in the country.
- Pointing out systemic issues in the U.S. that disadvantage certain racial groups.
- Encouraging working on correcting these problems rather than being afraid of becoming a minority.
- Expressing lack of personal concern about demographic shifts and focusing on working towards equality.
- Emphasizing the importance of fighting for equality over seeking vengeance.
- Stating that skin tone shouldn't hold such significance in society.
- Arguing against adjusting life or policies based on demographic changes.
- Noting concerns arise from realizing how the current system disadvantages minorities.

# Quotes

- "Skin tone shouldn't be that important, right?"
- "They're fighting for equality. They're not fighting for vengeance."
- "Now you're worried about that system being turned on you."

# Oneliner

Beau addresses concerns about census data, systemic racial issues, and the importance of working towards equality over fear of becoming a minority.

# Audience

Activists, Advocates, Community Members

# On-the-ground actions from transcript

- Coordinate with organizations working towards equality (implied)
- Work on correcting systemic issues related to race (implied)

# Whats missing in summary

The emotional impact and nuances of Beau's message can be better understood by watching the full transcript.