# Bits

Beau says:

- Examines the concept of personal responsibility deeply entrenched in American politics.
- Personal responsibility is seen as a way to reduce government intervention and increase individual freedom.
- Criticizes individuals who refuse to wear masks or get vaccinated under the guise of personal responsibility.
- Points out the contradiction in claiming personal responsibility while not taking necessary public health measures.
- Expresses disappointment in small government conservatives and libertarians for not advocating for responsible behavior during the pandemic.
- Notes that individuals leaving the Republican Party may not find solace in libertarianism due to lack of distinguishable differences.
- Critiques libertarians for failing to provide a viable alternative and for echoing Republican talking points.
- Argues that the refusal to wear masks or get vaccinated undermines the philosophy of self-governance.
- Suggests that libertarians could have gained more support during the pandemic by promoting responsible behavior.
- Emphasizes the need to act on personal responsibility by wearing masks and getting vaccinated without waiting for government mandates.

# Quotes

- "You don't need to make me wear a mask because I'm already doing it, because I'm exercising that personal responsibility."
- "They're not looking for a new philosophy. They're just tired of seeing funeral processions."
- "But as simple as that philosophy is, you have to act on it."
- "No, you don't need the government to tell you to do that because the government is a child of the people, not the other way around."
- "It makes it seem that you don't believe your ideology, in the best case."

# Oneliner

In a critical analysis of personal responsibility, Beau challenges the failure of small government conservatives and libertarians to advocate for responsible behavior during the pandemic, undermining the core principles they claim to uphold.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Wear a mask and get vaccinated (implied)
- Advocate for responsible behavior in the community (implied)
  
# Whats missing in summary

The full transcript provides a comprehensive breakdown of how personal responsibility intersects with political ideologies and public health crises.

# Tags

#PersonalResponsibility #AmericanPolitics #COVID19 #SmallGovernment #Libertarians