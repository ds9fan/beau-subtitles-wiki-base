# Bits

Beau says:

- Beau addresses concerns about books being found in dumpsters and invites Allison Macrina, a librarian, to explain the process of removing books from libraries.
- Allison explains that libraries have to make decisions about older books that are no longer in use through a process called "weeding."
- Libraries try to find new homes for discarded books through book sales, donations, or free distribution, but some books end up in dumpsters.
- Allison clarifies that the removal of books is not censorship but a matter of space and keeping collections current to meet community needs.
- She mentions the importance of libraries in providing free access to information, technology education, job skills training, and community programs.
- Allison introduces the Library Freedom Project, a community that advocates for privacy protection in libraries and fights against surveillance.
- She explains the historical connection between libraries and privacy advocacy, dating back to resisting anti-communist inquiries and post-9/11 privacy violations.
- Allison describes the online crash courses offered by the Library Freedom Project to teach library workers about privacy protection strategies.
- She addresses concerns about censorship in the publishing industry and the impact of media consolidation on the diversity of opinions available in libraries.
- Allison advocates for better funding for libraries to continue providing valuable services to diverse communities.

# Quotes

- "Libraries are immensely important. In fact, they are more popular than they ever have been."
- "Privacy is one of the core values of the library professions."
- "We want our collections to be current. We want them to reflect our community's needs."
- "Libraries need to be significantly better funded."
- "Libraries are one of the only things in society that are 100% free and not means tested."

# Oneliner

Beau and Allison dive into the importance of libraries, addressing concerns about discarded books, advocating for privacy protection, and calling for better funding to support vital community resources.

# Audience

Library supporters, privacy advocates

# On-the-ground actions from transcript

- Support your local library by visiting, borrowing books, attending programs, and advocating for increased funding (exemplified)
- Educate yourself on privacy rights and surveillance issues and support initiatives like the Library Freedom Project (exemplified)

# Whats missing in summary

Explanation of how the Library Freedom Project's crash courses empower librarians to protect privacy and advocate for community rights.

# Tags

#Libraries #PrivacyProtection #CommunitySupport #Funding #Censorship #Surveillance #Education #Activism