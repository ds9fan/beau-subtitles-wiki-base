# Bits

Beau says:

- Garth Brooks mandated mask-wearing at his concerts, sparking controversy over being labeled as "woke" for a mildly socially responsible act.
- Beau criticizes the oversimplification of the term "woke" to mean only being alert to injustice, arguing that true woke-ness involves actively working to end systemic issues and injustices.
- He suggests that being truly woke means recognizing and addressing food insecurity, fear of violence, racial injustices, housing as a human right, environmental injustice, and the privilege associated with money.
- Beau points out that Garth Brooks has a history of advocating for social issues, such as gay rights in his music in the 90s, challenging the notion that he has only recently become woke.
- He criticizes the tendency to label individuals as woke when they deviate from expected norms or demonstrate socially responsible behavior.

# Quotes

- "True woke-ness involves actively working to end systemic issues and injustices."
- "They were woke the whole time, trying to get that message out there."
- "Once again, it boils down to somebody stepping out of line and the right-wing cancel culture coming for them."

# Oneliner

Garth Brooks's mask mandate sparked debates on woke-ness, with Beau challenging the oversimplified definition and advocating for actively addressing systemic issues and injustices.

# Audience

Social justice advocates

# On-the-ground actions from transcript

- Advocate for addressing systemic issues and injustices (exemplified)
- Support social causes actively (exemplified)
- Challenge oversimplified definitions of terms (exemplified)

# Whats missing in summary

Deeper exploration of Garth Brooks's history of advocating for social issues and the implications of labeling individuals as woke prematurely.

# Tags

#GarthBrooks #Woke #SocialJustice #SystemicIssues #Advocacy