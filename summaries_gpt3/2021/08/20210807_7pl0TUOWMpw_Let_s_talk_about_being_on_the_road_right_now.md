# Bits

Beau says:

- Beau finishes a large-scale production for his second channel, Beau on the Road, after traveling seven days through eight states covering roughly 4,500 miles from the Gulf Coast to the West Coast, ending in LA.
- Despite testing, masking, and being vaccinated, Beau is hesitant about doing meet and greets and setting up community networks due to the lack of precautions along the highways and interstates.
- Native reservations are the only places with consistent protocols while most other areas lack clear safety measures.
- Places like LA show more compliance compared to areas like Arizona and Texas where people seem to disregard the ongoing threat of the pandemic.
- Beau expresses surprise at the reluctance of people to take the available vaccine or treatment despite its existence.
- The production filming went well overall, with minor hiccups expected, but Beau is uncertain about duplicating it soon due to concerns about COVID-19 transmission.
- Beau stresses the importance of taking precautions seriously, even if those around you are not, and mentions that the upcoming episode will take time to put together.
- He encourages everyone to stay safe and concludes with a reminder to take the situation seriously, even if others are not.

# Quotes

- "Please take it seriously. Take the precautions even if those around you aren't."
- "Y'all just be safe out there."
- "I'm not sure how soon we're going to duplicate this."
- "People still are just kind of blowing this off as if it's over."
- "I just didn't know that people weren't going to take it."

# Oneliner

Beau finishes a successful production trip but expresses hesitance due to lack of COVID precautions, urging everyone to take safety seriously.

# Audience

Travelers, community members

# On-the-ground actions from transcript

- Take COVID precautions seriously, even if those around you aren't (suggested)
- Stay informed about the COVID situation in your area and act accordingly (implied)
- Encourage others to follow safety guidelines (implied)

# Whats missing in summary

The emotional impact of seeing the lack of COVID precautions firsthand during a long production trip.