# Bits

Beau says:

- Addressing the harmful impact of accepting theories without evidence.
- Exploring the theory of super advanced species teaching ancient civilizations.
- Questioning the Eurocentric bias in theories about ancient civilizations.
- Criticizing the reinforcement of the idea that only European cultures were capable.
- Warning against accepting theories with scant evidence and their potential negative impact.

# Quotes

- "Those people over there, there's no way they could have done that. Brown people couldn't have done that. It had to be green people."
- "Not just is that wrong for obvious reasons, it's historically inaccurate."
- "It's probably not a good road to go down."

# Oneliner

Beau addresses the harm in accepting theories without evidence, particularly those reinforcing Eurocentric biases and downplaying the achievements of non-European civilizations.

# Audience

Critical Thinkers

# On-the-ground actions from transcript

- Question theories without evidence (implied)
- Challenge Eurocentric biases in historical interpretations (implied)

# Whats missing in summary

Full understanding of Beau's nuanced perspective on theories lacking evidence and their potential societal implications.

# Tags

#CriticalThinking #Eurocentrism #HistoricalBias #ChallengingTheories #Evidence-basedThinking