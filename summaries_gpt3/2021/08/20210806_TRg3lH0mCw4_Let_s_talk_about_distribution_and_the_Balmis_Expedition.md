# Bits

Beau says:

- Addresses the disparity in vaccine distribution, with wealthy countries getting doses first.
- Mentions past instances of similar distribution issues being faced.
- Talks about organizations working to streamline vaccine distribution to developing countries.
- Tells the story of Dr. Francisco Javier de Bamas in 1802, who wanted to inoculate Spain's colonies against smallpox.
- Describes how Dr. Bamas overcame logistical challenges by using cowpox for inoculation.
- Mentions the use of orphan boys to transport the serum during the voyage.
- Notes that younger people fared better during the inoculations across different countries.
- Emphasizes that mass vaccination programs have existed for a long time, indicating that distribution is possible.
- Stresses that distributing vaccines is a matter of will and money, not just logistics.
- Draws attention to the contrast between reluctance in wealthy countries to take vaccines and lack of access in developing nations.
- Advocates for prioritizing vaccine distribution to save lives globally.
- Calls for political will and resources to ensure equitable vaccine access.
- Urges for a collective effort to make vaccine distribution a reality.
- Acknowledges the challenges but remains optimistic about achieving widespread vaccination.
- Concludes with a call to action for political and financial support in vaccine distribution efforts.

# Quotes

- "It's a matter of will and money."
- "Beyond wealthy countries' borders do not live lesser people."
- "We just have to create a situation in which politicians and those with the money and those with the resources know that we want it done."

# Oneliner

Beau addresses vaccine distribution disparities, recounts a historical example, and advocates for global access through collective will and resources.

# Audience

Global citizens, policymakers

# On-the-ground actions from transcript

- Support organizations working on streamlining vaccine distribution to developing countries (suggested)
- Advocate for equitable vaccine access through political channels (implied)

# Whats missing in summary

The emotional impact of prioritizing vaccine distribution for saving lives globally.

# Tags

#VaccineDistribution #GlobalAccess #HistoricalExample #Equity #CollectiveAction