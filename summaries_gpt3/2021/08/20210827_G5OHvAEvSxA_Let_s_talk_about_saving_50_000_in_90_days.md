# Bits

Beau says:

- Providing an overview of projections for the next 90 days regarding public health.
- Mentioning the potential loss of 100,000 lives over the next 90 days without proper measures.
- Pointing out that wearing masks in public could reduce this number by half.
- Noting the quantified cost of not wearing masks in public: about 50,000 lives over three months.
- Sharing news about a judge in Florida overturning the ban on schools mandating masks.
- Emphasizing the individual choice to save lives by wearing a mask or ignoring the consequences.
- Stressing the reality of the situation and the tangible impact on lives if mask-wearing is not practiced.
- Encouraging engagement in all available mitigation efforts to reduce the loss of lives.
- Framing mask-wearing as a patriotic duty and an act of love for country and neighbors.
- Describing the current situation as an intelligence test with life-or-death implications.
- Urging people to change their behavior to prevent unnecessary loss of life.
- Summarizing that wearing a mask daily can significantly reduce the number of lives lost.
- Concluding with a call to action to wear masks for the greater good and as part of a critical battle.

# Quotes

- "It's worth noting that here in Florida, a judge did just throw out DeSantis' little edict saying that schools could not mandate masks."
- "If you love your country, if you love your neighbors, if you want to be a patriot and do your part in the greatest battle this country has seen in a very long time, all it takes is wearing a mask."
- "This isn't just a public health issue anymore. It's an intelligence test with life-or-death consequences."

# Oneliner

Beau stresses the importance of wearing masks to reduce the projected loss of 100,000 lives over 90 days, making it a patriotic duty and an intelligence test with life-or-death consequences.

# Audience

General public, mask-wearing advocates

# On-the-ground actions from transcript

- Wear a mask daily to reduce the projected loss of lives. (exemplified)

# Whats missing in summary

The emotional appeal and urgency conveyed by Beau in the full transcript may not be fully captured in this summary. Viewing the full content will provide a more profound understanding of his message.

# Tags

#PublicHealth #MaskWearing #Patriotism #Community #Mitigation