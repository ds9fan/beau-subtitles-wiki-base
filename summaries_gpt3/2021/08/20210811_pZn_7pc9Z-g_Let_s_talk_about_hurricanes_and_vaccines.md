# Bits

Beau says:

- President Biden's statement about getting vaccinated in hurricane-prone areas has sparked controversy.
- Biden's message is not fear-mongering but a statement of fact about the impact of hurricanes on medical infrastructure.
- In a pre-pandemic video, Beau discussed the consequences of being unvaccinated during a natural disaster like Hurricane Michael.
- Hurricane damage to hospitals reduces medical capacity, making vaccination even more critical in hurricane-prone states.
- The strain on medical infrastructure in hurricane-prone areas is already significant.
- Beau stresses the importance of vaccination as part of hurricane preparedness in states like Florida, Georgia, Alabama, and others.
- The reality is that without vaccination, individuals may face dire consequences during a hurricane due to reduced medical capacity.
- Beau underscores that the pandemic will persist until enough people are vaccinated, regardless of personal preferences.
- He warns that being unvaccinated during a hurricane can lead to severe consequences and loss.
- Vaccination is a vital component of preparedness for natural disasters in areas prone to hurricanes.

# Quotes

- "It's just reality."
- "It isn't fear-mongering."
- "Yeah, you need to get your shots."
- "This is going to continue until we have enough people vaccinated."
- "Add getting vaccinated to your hurricane preparedness plan."

# Oneliner

President Biden's vaccination advice in hurricane-prone regions is a factual warning, not fear-mongering, stressing the critical role of vaccination in hurricane preparedness amidst strained medical infrastructure.

# Audience

Residents in hurricane-prone areas

# On-the-ground actions from transcript

- Get vaccinated as part of your hurricane preparedness plan (suggested)
- Recognize the importance of vaccination in areas vulnerable to hurricanes (implied)

# Whats missing in summary

The full transcript provides additional context on the necessity of vaccination in hurricane-prone regions and the potential consequences of being unvaccinated during a natural disaster.

# Tags

#Vaccination #HurricanePreparedness #MedicalInfrastructure #PublicHealth #CommunitySafety #RealityBasedInformation