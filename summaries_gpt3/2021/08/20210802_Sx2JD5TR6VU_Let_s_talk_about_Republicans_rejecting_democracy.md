# Bits

Beau says:

- Talks about two polls showing alarming numbers among Republicans.
- 63% of Republicans do not believe democracy is working.
- 67% of Republicans view voting as a privilege, not a right.
- Beau warns that the Republican Party base is leaning towards dictatorship.
- Urges Americans to pay attention to these alarming numbers.
- States that government is an illusion, much like a dollar's value.
- Calls for accountability by subpoenaing everyone who has supported baseless claims.
- Suggests charging individuals with perjury if they lie under subpoena.
- Expresses concern that without action, authoritarianism will continue to grow.
- Warns that without accountability, current events may become a trial run for worse situations.
- Emphasizes the importance of defending democracy and institutions now.
- Points out the contradiction in viewing voting as a privilege but not gun ownership.
- Calls on politicians to act now in defense of democracy before the midterms.

# Quotes

- "Americans will ignore these numbers at their peril."
- "Government is an illusion. It's a lot like a dollar."
- "Their base rejects the founding principles of this country."
- "Now is your time, and you had better do something before the midterms."
- "It's a privilege. Probably trying to ensure the purity of the ballot box."

# Oneliner

Alarming poll numbers reveal Republican attitudes towards democracy and voting as privilege, urging immediate action to defend democracy before authoritarianism grows further.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact Congress to demand accountability for those supporting baseless claims (implied)
- Take action to defend democracy and institutions before the midterms (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of alarming poll numbers indicating Republican sentiments towards democracy and voting rights, urging immediate action to prevent the growth of authoritarianism.

# Tags

#Democracy #VotingRights #Authoritarianism #Accountability #DefendDemocracy