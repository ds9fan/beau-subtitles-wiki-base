# Bits

Beau says:

- Exploring the interpretation and meanings of the third verse of the US national anthem, the Star Spangled Banner, in response to a viewer's query.
- Providing context that the anthem is based on a poem written during the War of 1812, not the Revolutionary War, and questioning why it's the national anthem.
- Describing Francis Scott Key, the anthem's writer, as a racist who believed black people were inferior and evil.
- Analyzing the controversial third verse that mentions "hireling nor slave" and discussing four possible interpretations, including references to colonial marines and impressment.
- Asserting that the true meaning behind the verse remains unknown as Key never clarified it, debunking any certainty in interpreting it.
- Suggesting that the offensive nature of the anthem, considering its lyrics and Key's beliefs, gives grounds for people to be upset.
- Pointing out the lack of positive interpretations in the anthem and proposing that it should be changed due to its problematic content.
- Challenging the idea of tradition by revealing that the anthem was officially adopted by Congress as the national anthem less than 100 years ago in the 1930s.

# Quotes

- "There's no good way to read this."
- "The only interpretation that isn't just absolutely horrible is that it's an insult to all British people."
- "There's no reason it can't be changed."

# Oneliner

Exploring the controversial third verse of the US national anthem, Beau questions its racist origins, provides interpretations, and suggests that it should be changed due to its offensive content.

# Audience

Americans

# On-the-ground actions from transcript

- Advocate for changing the US national anthem (suggested)
- Educate others about the problematic history and lyrics of the anthem (implied)

# Whats missing in summary

Full understanding of the US national anthem's controversial third verse and its implications.

# Tags

#USNationalAnthem #Racism #FrancisScottKey #Interpretations #Change