# Bits

Beau says:

- Explains the concept of dueling definitions when experts disagree.
- Describes a scenario where two friends, one from the military and the other a lawyer, disagreed over the use of a term during hearings.
- The military friend argued against using the term "terrorist," proposing "coup" or "self-coup" instead.
- The lawyer, however, was correct in using the legal statute's definition, which differs from the academic perspective.
- Beau suggests that both friends were right from their respective standpoints due to a misunderstanding of definitions.
- Points out the broad and vague nature of the federal statute on what constitutes an act of terrorism.
- Emphasizes that regardless of the semantic debate, the core issue was an attempt to influence the government through violence or threat during the election overturn.
- Advocates for focusing on the critical aspect of the situation, which was the attack on vital US institutions by violence or the threat of violence.

# Quotes

- "When you run into this issue where you have two experts who disagree, maybe start from the position that they're both right, but there's a misunderstanding of what they're talking about."
- "This is a semantic argument. People who are experts, who are very well informed about a topic, generally they also have huge egos."
- "That's the main point is that the institutions that the United States relies on were under direct attack."

# Oneliner

Beau dives into dueling definitions between experts, showcasing a disagreement between a military friend and a lawyer over a term during hearings, illustrating the broader issue of influencing the government through violence.

# Audience

Analytical thinkers

# On-the-ground actions from transcript

- Organize educational sessions to clarify differing definitions and interpretations within specific fields (suggested)
- Engage in respectful dialogues to bridge understanding gaps between conflicting perspectives (implied)

# Whats missing in summary

Beau's engaging delivery and nuanced analysis can best be appreciated by watching the full transcript.

# Tags

#Definitions #ExpertDisagreement #LegalVsAcademic #Influence #Violence #Government