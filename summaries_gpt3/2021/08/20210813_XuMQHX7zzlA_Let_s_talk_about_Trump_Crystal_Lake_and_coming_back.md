# Bits

Beau says:

- Reporting live from Camp Crystal Lake on Friday the 13th, waiting for the return of a mythical figure to set things right.
- Former President Trump could have debunked false claims but didn't, benefiting politically.
- Those who believed in the false claims saw their family and social life suffer without concern from Trump.
- People who promoted Trump's return are the same advising against masks and vaccines for political gain.
- Republican leaders who remain silent are willing to sacrifice followers for political gain.
- If leaders don't encourage measures to protect health, they don't care about their followers.
- Leadership of the Republican party and the mythical figure walking out of the lake both end with people harmed.
- Advice to wash hands, wear masks, get vaccinated, and protect oneself despite misleading leadership.
- Leaders who allowed false beliefs without correction show their lack of care for their followers.
- Encouragement to take note of the character of leaders who mislead or fail to correct misinformation.

# Quotes

- "Leaders who allowed false beliefs without correction show their lack of care for their followers."
- "Those who promoted Trump's return are the same advising against masks and vaccines for political gain."

# Oneliner

Former President Trump could have debunked false claims, but his silence harmed followers, while Republican leaders prioritize political gain over followers' well-being.

# Audience

Followers of political leaders

# On-the-ground actions from transcript

- Stay at home as much as you can to protect yourself and your family (implied)
- Wash your hands regularly to prevent the spread of illness (implied)
- Wear a mask when in public spaces to protect yourself and others (implied)
- Get vaccinated to safeguard your health and the health of your community (implied)

# Whats missing in summary

The full transcript delves into the consequences of following misleading leaders and the importance of prioritizing health over political allegiances.

# Tags

#Politics #Misinformation #Health #Leadership #Accountability