# Bits

Beau says:

- Introduces the topic of discussing what his life might be like if he were single.
- Mentions being asked a question recently about his life if he were single.
- Talks about Panta Petrovic, a man living in southern Serbia in a cave for twenty years.
- Describes Panta's life in the cave, living off the land with basic furnishings and some animals.
- Notes Panta's occasional trips to town for dumpster diving.
- Compares Panta's presence near the town to that of Bigfoot.
- Speculates on the positive introspection that twenty years of solitude might bring.
- Shares Panta's recent emergence from the cave and his response to the public health situation.
- Commends Panta for getting vaccinated and urging others to do the same, setting an example for leadership.
- Echoes Panta's message of encouraging every citizen to get vaccinated.

# Quotes

- "Every citizen should get vaccinated."
- "A better example, better leadership than many governors in the United States."
- "Twenty years he's lived up there like that."

# Oneliner

Beau introduces the unique life of Panta Petrovic, a man living in a cave for twenty years who sets an example by getting vaccinated and urging others to do the same.

# Audience

Community members

# On-the-ground actions from transcript

- Get vaccinated (exemplified)
- Encourage others to get vaccinated (exemplified)

# Whats missing in summary

The full transcript provides more details on Panta Petrovic's solitary life in a cave and his actions during the public health situation.

# Tags

#Vaccination #CommunityLeadership #PublicHealth #Isolation #Serbia