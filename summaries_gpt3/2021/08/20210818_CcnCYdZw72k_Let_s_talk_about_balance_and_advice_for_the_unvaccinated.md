# Bits

Beau says:

- Addressing moral and ethical responsibilities by providing a wide range of viewpoints.
- Acknowledging being informed of neglecting duties and aiming to remedy that.
- Being advised by someone to advise anti-vax people despite being pro-vax.
- Explaining the importance of filling out certain documents for emergency preparedness.
- Providing instructions on filling out an Emergency Child Care Plan for children.
- Emphasizing not to include dogs in the Child Care Plan, as it is for human children.
- Mentioning the importance of filling out a will and power of attorney document.
- Cautioning against designating an unvaccinated individual as a power of attorney.
- Recommending discussing Do Not Resuscitate/Do Not Intubate (DNR-DNI) decisions with family.
- Instructing to designate a location for storing vital documents and records.
- Listing documents to store, including insurance policies, medical records, tax records, and more.
- Advising to include copies of last month's bills for someone taking over financial responsibilities.
- Suggesting deleting internet history as an additional precaution.

# Quotes

- "You're going to fill out this form and you're going to discuss this plan with your children. Don't make us do it."
- "Do not put your dogs on this. This is for human children."
- "You are asking somebody to take over your financial responsibilities in the event of an emergency. You want to make it as easy as possible for the person you are dumping this on."

# Oneliner

Beau addresses moral responsibilities, advises on emergency preparedness documents, and stresses the importance of comprehensive planning and communication for unforeseen circumstances.

# Audience

Parents, caretakers, and individuals needing guidance on emergency preparedness.

# On-the-ground actions from transcript

- Fill out an Emergency Child Care Plan with your children (suggested).
- Fill out a will, power of attorney, and DNR-DNI documents (suggested).
- Designate a secure location for storing vital documents and records (suggested).
- Include copies of last month's bills in the designated location for financial continuity (suggested).
- Delete internet history as a precaution (suggested).

# Whats missing in summary

Detailed step-by-step instructions for emergency preparedness planning and document organization.

# Tags

#MoralResponsibilities #EmergencyPreparedness #DocumentOrganization #Communication #ComprehensivePlanning