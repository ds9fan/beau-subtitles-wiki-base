# Bits

Beau says:

- Explains the importance of understanding historical context to comprehend current events.
- Recounts the tragic story of the St. Louis ship during WWII carrying Jewish refugees rejected by multiple countries.
- Draws parallels to a modern-day situation involving the ship "Open Arms" with refugees off the Italian coast.
- Notes the refusal of the Italian government and other European countries to allow the refugees ashore.
- Links the desire of refugees to reach Italy to historical ties, such as Ethiopia's past as an Italian colony.
- Mentions the long-standing relationship between Tunisia and Italy, despite Tunisia not being an official Italian colony.
- Comments on the impact of extreme nationalism leading to the current plight of refugees.
- Suggests a solution by proposing a strategy to address attacks by welcoming refugees, contrasting it with the current stance.
- Warns of the lasting effects and backlash of current actions on children and citizens.
- Concludes with a reflection on the repercussions of present actions influencing the future.

# Quotes

- "Everything that happens changes what happens tomorrow."
- "The scars that we're creating today, they're gonna have backlash too."
- "You know, if Trump was smart, he could stop these attacks overnight."
- "That relationship between the people of the area that is now Tunisia and the area that is now Italy has been around since Italy was known as Rome."
- "It's estimated that about a quarter of them got caught and sent to death camps where they died."

# Oneliner

Beau explains the significance of historical context in understanding current events, reflecting on the rejection of refugees reminiscent of past tragedies and proposing a compassionate solution to address the crisis.

# Audience

Global citizens, activists

# On-the-ground actions from transcript

- Welcome refugees in your community (implied)

# Whats missing in summary

The full transcript provides a deep dive into historical context shaping current events, urging for empathy and action to address the refugee crisis and avoid repeating past mistakes.

# Tags

#History #Refugees #Italy #Mediterranean #Nationalism