# Bits

Beau says:

- Explains the lack of liberal armed groups, attributing it to most liberals being anti-gun.
- Contrasts right-wing militias, characterized by conventional tactics and structure, with leftists who don't style themselves as militias.
- Mentions armed leftist groups like the John Brown Gun Club and Redneck Revolt that don't have traditional militia setups.
- Talks about the historical challenges faced by armed leftists in the US, with FBI scrutiny in the past.
- Describes the security consciousness of armed leftist groups, mentioning a unique vetting process involving friends.
- Notes the shift in security culture and how some groups now ask more probing questions, leading to hesitation from potential members.
- Points out that armed leftists train in guerrilla-style operations rather than conventional tactics.
- States that firearms are seen as a tool by the left, unlike a status symbol on the right.
- Emphasizes the importance of keeping things peaceful and discourages violence in any form.
- Encourages responsible gun ownership for defense or recreational purposes but warns against offensive uses.

# Quotes

- "Ideas travel faster than bullets."
- "We need to keep this peaceful as much as humanly possible."
- "The idea of sparking that kind of conflict, it's never good."

# Oneliner

Beau explains the differences between left and right-wing armed groups, touches on historical challenges, and stresses the importance of peaceful approaches over violence.

# Audience

Activists, Gun Owners

# On-the-ground actions from transcript

- Join or support organizations like the John Brown Gun Club or Redneck Revolt (exemplified)

# Whats missing in summary

The full transcript provides a nuanced exploration of the historical context and current challenges faced by armed leftist groups in the US.

# Tags

#ArmedGroups #Leftist #GunOwnership #PeacefulAction #Community