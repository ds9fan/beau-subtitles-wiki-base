# Bits

Beau says:

- High school students at Martin County High School in Kentucky wore pro-LGBTQ clothing and were forced to change by the school administrator.
- The school claimed they were worried about the students being bullied, but allowed students to wear Confederate flags.
- The students felt like they were silenced while the school didn't address the bullies.
- The administrator told the students that their clothing wasn't appropriate and that school wasn't the place to talk about sexual orientation.
- Despite the superintendent claiming the issue was resolved as miscommunication, a student fears further disciplinary action.
- Beau questions the lack of confirmation from the teens that the issue was truly resolved and urges them to speak up to prevent it from being ignored.
- Teens have the right to free speech and expression according to the Supreme Court, as stated in Tinker versus Des Moines.
- Beau encourages the superintendent and school administrator to leave the students alone.

# Quotes

- "Just do whatever the mean man says. Don't make yourself a target. Hide who you are."
- "Maybe it's been resolved. I don't know. But there's no quotes from the teens saying that it was. None."
- "They won't have any of those mean and hurtful words on them, and they will be completely in line with Tinker versus Des Moines."
- "My advice to the superintendent and the school administrator is to leave these kids alone."

# Oneliner

High school students forced to change for wearing LGBTQ clothing face silence instead of support, revealing a double standard in Kentucky's school system.

# Audience

Students, Educators, Activists

# On-the-ground actions from transcript

- Contact the students to offer support and ensure their voices are heard (implied).
- Advocate for inclusive policies and support systems within schools (implied).
- Stand up against discrimination and bullying in educational environments (implied).

# Whats missing in summary

The emotional impact on the students and the importance of standing up against discrimination in educational settings.

# Tags

#LGBTQ+ #SchoolDiscrimination #StudentRights #FreeSpeech #Equality