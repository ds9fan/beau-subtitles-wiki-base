# Bits

Beau says:

- Analyzing demographic information to identify potential perpetrators before they commit violent acts.
- Majority of mass shooters are male, white, and display concerning behaviors.
- Early childhood trauma, inciting incidents, and a planning phase precede violent acts.
- Characteristics of mass shooters include abusive behavior, mental health concerns, and leaked plans.
- Mass shooters often have personal grievances blended with other grievances.
- They have a desire for control and notoriety, similar to serial killers.
- Mass shooters may be more closely related to serial killers than terrorists.
- Need to respond, not react, to prevent future violent incidents.
- Current strategies focusing on political violence may not be effective.
- Urges a shift in perspective and approach to addressing mass shootings.

# Quotes

- "If somebody tells you they're going to do it, believe them."
- "We need to respond rather than react."
- "Mass shooters are more closely related to serial killers than they are terrorists."

# Oneliner

Analyzing demographic info to prevent violent acts, mass shooters more like serial killers, urge to respond not react.

# Audience

Policy makers, law enforcement, psychologists

# On-the-ground actions from transcript

- Contact local authorities or FBI to report suspicious behavior (implied)
- Advocate for standardized reporting methods for potential threats (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of mass shooters' characteristics, urging a shift in perspective towards prevention rather than reaction.

# Tags

#MassShooters #Prevention #Demographics #ResponseVsReaction #ViolencePrevention