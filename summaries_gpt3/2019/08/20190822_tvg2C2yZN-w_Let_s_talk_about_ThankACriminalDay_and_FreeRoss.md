# Bits

Beau says:

- Celebrates August 22nd, Criminal Day, created by Brian to honor criminals who have made a positive impact.
- Questions the notion that soldiers protect freedom, arguing that criminals actually obtain it for us.
- Mentions heroes like Martin Luther King Jr., whose actions were considered criminal at the time.
- Talks about Ross Ulbricht, the creator of Silk Road, an online platform for drug sales, and his controversial case.
- Points out that Ross Ulbricht received two life sentences plus forty years for a non-violent offense.
- Raises the question of whether prison is about rehabilitation or punishment in Ross's case.
- Describes Ross Ulbricht as a nerdy Eagle Scout who believed in the freedom for individuals to make their own choices.
- Emphasizes that Ross is not a danger to society and does not deserve his harsh sentence.
- Encourages viewers to visit freeross.org to learn more about Ross's case and support the petition for his clemency.
- Argues that if there is no victim, there is no crime, and advocates for a reevaluation of the justice system.

# Quotes

- "If there is no victim, there is no crime."
- "Life in the federal system means life."
- "That guy paid the price. The problem is, the price we're asking him to pay is too high."
- "Criminals have changed the world and they will continue to."
- "He won't get out. That's a little much for a website."

# Oneliner

Beau questions the criminal justice system's treatment of non-violent offenders and advocates for Ross Ulbricht's clemency, challenging the narrative around criminals on Criminal Day.

# Audience

Justice Reform Advocates

# On-the-ground actions from transcript

- Visit freeross.org to learn more and support the petition for Ross Ulbricht's clemency (suggested).
- Share the video with the hashtags #ThankCriminalDay and #FreeRoss (suggested).

# Whats missing in summary

The emotional impact of Ross Ulbricht's case and the broader implications for criminal justice reform.

# Tags

#JusticeReform #FreeRoss #CriminalDay #NonViolentOffenders #Injustice