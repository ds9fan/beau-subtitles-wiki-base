# Bits

Beau says:

- Addressing Floridians about Hurricane Dorian approaching the state, specifically central Florida.
- Warning about the potential impact of the hurricane, citing past surprises like Hurricane Michael.
- Urging people to take necessary precautions and evacuate if needed, especially for the elderly.
- Advising on essentials to have on hand: phone batteries, fuel for generators, food, water, first aid kit, and medications.
- Cautioning against risky behavior during the storm, calling out Gainesville residents.
- Noting the diversion of FEMA funds to immigration efforts, potentially affecting hurricane response.
- Mentioning the historical delays in federal funding post-hurricane, especially in the panhandle.
- Acknowledging Florida's preparedness due to previous experiences but warning of potential self-reliance needed.
- Mentioning the state of emergency declaration and the impending presence of the Florida National Guard.
- Connecting the lack of FEMA funding to immigration policies and questioning the effectiveness of a border wall.

# Quotes

- "Get your old people out."
- "Don't do anything stupid during the storm."
- "Be prepared to be on your own for a little while."
- "We know that 90 miles of ocean doesn't deter the drive for freedom."
- "Because of our experiences here in Florida, we know that wall isn't going to work."

# Oneliner

Beau warns Floridians about Hurricane Dorian's potential impact, urges necessary precautions, and questions FEMA funding diversion amid immigration policies.

# Audience

Floridians

# On-the-ground actions from transcript

- Evacuate if necessary and help others, especially the elderly, to do so (implied).
- Stock up on essentials like phone batteries, fuel, food, water, first aid kit, and medications (implied).
- Stay safe during the storm and avoid risky behavior (implied).
- Be prepared to be self-reliant for a few days post-hurricane (implied).
- Support local communities and organizations in hurricane preparation and response (implied).

# Whats missing in summary

Importance of community support and preparedness in facing natural disasters.

# Tags

#HurricaneDorian #Florida #Preparedness #Evacuation #CommunitySupport