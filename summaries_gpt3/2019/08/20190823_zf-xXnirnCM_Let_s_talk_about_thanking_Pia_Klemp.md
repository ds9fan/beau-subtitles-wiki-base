# Bits

Beau says:

- Beau introduces the concept of "Thank a Criminal Day" in his video.
- He shares a quote from Pia Klemp, a German ship captain who has saved thousands of migrants in the Mediterranean Sea.
- Pia Klemp is facing accusations in Italy for assisting illegal immigration and working with human traffickers.
- Italy is not the only government opposed to Pia's actions, with Paris offering her a medal which she turned down.
- Pia's response to the medal offer is scathing, criticizing the authorities for their treatment of migrants and asylum seekers.
- She rejects the notion of authorities deciding who is a hero and who is illegal, asserting that everyone is equal.
- Beau expresses gratitude towards Pia Klemp for her actions and her powerful statement.
- He commends Pia's sentiment and describes it as a wonderful statement.
- Beau concludes the video by wishing his viewers a good night.

# Quotes

- "We do not need authorities deciding about who is a hero and who is illegal."
- "Thank you, Pia Klemp."

# Oneliner

Beau introduces "Thank a Criminal Day," shares Pia Klemp's powerful stance on equality, and appreciates her actions.

# Audience

Advocates for equality

# On-the-ground actions from transcript

- Support organizations aiding migrants and asylum seekers (suggested)
- Advocate for fair treatment of migrants and asylum seekers (implied)

# Whats missing in summary

The full video provides a deeper exploration of the concept behind "Thank a Criminal Day" and the importance of challenging unjust laws to bring about more freedom.

# Tags

#ThankACriminalDay #Equality #Activism #Immigration #HumanRights