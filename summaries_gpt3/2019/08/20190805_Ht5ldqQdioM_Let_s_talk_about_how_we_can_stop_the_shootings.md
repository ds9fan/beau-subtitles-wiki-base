# Bits

Beau says:

- Addressing solutions to help stop the plague of violence sweeping the nation.
- Noting the cultural roots of the issue rather than blaming specific factors like firearms or pharmaceuticals.
- Sharing findings from a study by the Violence Project funded by the National Institute of Justice.
- Identifying four common factors among mass shooters that are almost universally present.
- Emphasizing early childhood trauma and its long-lasting impacts.
- Explaining the importance of identifying crisis points to intervene before shootings occur.
- Describing how shooters often study other shooters, contributing to a socially transmitted disease.
- Pointing out the prevalence of obtaining firearms from family members in mass shooting incidents.
- Advocating for social reform to address underlying issues like coping skills and trauma.
- Urging responsible gun ownership and securing firearms to prevent access by potential shooters.

# Quotes

- "Early childhood trauma. This is bullying, parental suicide, child abuse, exposure to violence at an early age."
- "There's an identifiable moment where the decision was made. That was the point of no return."
- "We need to let kids be kids, but that's going to require pretty wide-ranging social reform."
- "Secure your firearms and don't tell your favorite nephew the code."
- "Don't name them. Make them a non-person."

# Oneliner

Beau addresses the cultural roots of violence, advocating for social reform, coping skills education, responsible gun ownership, and de-glorifying shooters to stem the tide.

# Audience
Community members

# On-the-ground actions from transcript

- Secure your firearms to prevent unauthorized access (implied)
- Educate yourself and others on identifying crisis points in individuals in crisis situations (implied)
- Advocate for social reform to address early childhood trauma and improve coping skills education (implied)

# Whats missing in summary
In-depth analysis of the study by the Violence Project and the implications for addressing mass shootings comprehensively.

# Tags
#ViolencePrevention #SocialReform #CopingSkills #ResponsibleGunOwnership #CommunitySafety