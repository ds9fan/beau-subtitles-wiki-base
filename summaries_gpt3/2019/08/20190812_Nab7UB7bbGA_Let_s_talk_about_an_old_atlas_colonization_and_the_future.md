# Bits

Beau says:

- Talks about the Italian occupation of Ethiopia in 1935-1936 and how it was not a functioning colony.
- Mentions the conflict during the Italian occupation of Ethiopia.
- Questions why an old Collier's Atlas from 1940 marks Italian East Africa as if giving the fascists and Italians a win.
- Points out the propaganda and biased narratives in historical accounts and atlases.
- Differentiates between two types of propaganda: nationalist and the narrative of Western supremacy.
- Emphasizes the importance of challenging the image of the uncivilized African perpetuated by colonial powers.
- States that colonization never truly ended, just evolved into wealth extraction through different means.
- Criticizes the creation of Africa Command by the Department of Defense (DOD) in 2007 and its mission centered around military intervention.
- Argues that African nations desire business partnerships rather than exploitative military presence.
- Warns about the consequences of not seeking the truth from current powers and victors, especially in relation to Africa.

# Quotes

- "The victor will never be asked if he's told the truth."
- "Colonization never ended, it never stopped, it just changed a little bit."
- "It's still about wealth extraction."
- "The people of Africa, they don't want our troops. They want our business."
- "We have to start seeing through it or our reluctance to look at the facts is going to cost a lot of lives."

# Oneliner

Beau delves into the nuances of colonization, propaganda, and Western supremacy, urging a critical reevaluation of historical narratives for a more equitable future.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Challenge biased narratives in history books and educational materials (implied)
- Support African nations in building their economies through fair business practices (implied)
- Advocate for transparency and accountability from current powers and victors (implied)

# Whats missing in summary

Beau's engaging analysis prompts reflection on the pervasive influence of propaganda, challenging individuals to seek the truth beyond biased narratives for a more just society.

# Tags

#Colonialism #Propaganda #WesternSupremacy #AfricanNations #Truth