# Bits

Beau says:

- President regrets not raising tariffs higher, implying he wishes to tax Americans more.
- Americans misunderstand tariffs; President taxes American importers on Chinese goods, passing the cost to consumers.
- President has authority, under the International Emergency Economic Powers Act, to end trade with China.
- This authority grants the presidency immense power, designed for catastrophic scenarios like nuclear war.
- Beau believes the presidency has too much power, risking a dictatorship where a pen stroke alters lives.
- Advocates for limiting presidential power to uphold democracy and prevent potential abuse by future leaders.
- Urges rescinding executive powers to prevent authoritarian behavior in the future.
- Warns that without checks on executive power, a leader like Trump could severely impact millions without resistance.
- Acknowledges the president's authority to devastate the economy with a single decision.
- Calls for action to address the excessive powers vested in the presidency for safeguarding democracy.

# Quotes

- "President regrets not raising tariffs higher, implying he wishes to tax Americans more."
- "He can destroy the economy with a pen stroke. He shouldn't, but he could."
- "We're not living in a representative democracy because we subverted it."

# Oneliner

Beau warns about unchecked presidential power leading to potential dictatorship and advocates for limiting executive authority to safeguard democracy and prevent economic devastation.

# Audience

American citizens

# On-the-ground actions from transcript

- Advocate for legislative measures to limit presidential powers (implied)
- Educate others on the implications of unchecked executive authority (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of unchecked presidential power and the need to limit executive authority to protect democracy and prevent economic turmoil.

# Tags

#TradeWar #Tariffs #PresidentialPower #Democracy #EconomicImpact