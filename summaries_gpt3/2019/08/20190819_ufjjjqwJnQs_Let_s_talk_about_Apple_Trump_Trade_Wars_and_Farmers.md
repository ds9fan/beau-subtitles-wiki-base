# Bits

Beau says:

- Tim Cook exposed President Trump's trade war during a dinner meeting at the president's golf club.
- Apple pays tariffs on Chinese components, making it hard to compete with companies not involved in the trade war like Samsung.
- President Trump found Cook's argument compelling and is reconsidering the trade war.
- Beau questions why American farmers didn't receive similar consideration during the trade war.
- The Secretary of agriculture dismissed farmers' concerns as whining.
- President Trump's administration favors the wealthy, with policies benefiting those with high net worth.
- The administration's policies contradict the rhetoric aimed at common people.
- Beau criticizes Trump for prioritizing personal gain over the well-being of Americans.
- Trump's presidency is characterized by self-interest and exploitation of taxpayers.
- Beau concludes by asserting that Trump never cared about the American people.

# Quotes

- "He doesn't care about you, he never did, and he never will."
- "It was just stuff he said to get people to follow him, that's it, that's all it was."
- "That's President Trump's administration."

# Oneliner

Tim Cook exposes Trump's trade war, revealing the administration's favoritism towards the wealthy over American farmers.

# Audience

American voters

# On-the-ground actions from transcript

- Support policies that prioritize the well-being of all Americans, not just the wealthy (implied).

# Whats missing in summary

The emotional impact of Trump's policies on everyday Americans and the call to prioritize their interests.

# Tags

#TradeWar #TimCook #PresidentTrump #AmericanFarmers #PolicyPriorities