# Bits

Beau says:

- The Cherokee Nation wants to appoint a delegate to the House of Representatives as allowed by a treaty signed in 1835.
- This treaty ultimately led to the Trail of Tears, a genocidal event that forcibly removed indigenous people from their land.
- The federal government's relationship with indigenous cultures is framed as two sovereign nations interacting, but honoring treaties has been an issue.
- Despite having voting rights, gerrymandering has significantly limited the voting power of the Cherokee Nation, consisting of 350,000 to 375,000 people.
- The potential delegate from the Cherokee Nation, likely Kimberly Teehee, may not have voting powers but can represent Native nations' issues in the House.
- Concerns exist that only the Cherokee Nation has this provision in their treaty, leading to worries about exclusive representation.
- Kimberly Teehee is viewed as a front runner for the position and serves as a diplomat between the Cherokee Nation and the U.S. government.
- While the process may take time, having Native interests represented in the House could significantly impact Native communities positively.
- Beau believes that the representation of Native interests in the House will make C-SPAN more engaging and is worth the support.

# Quotes

- "The federal government has a horrible track record of honoring treaties with native nations."
- "The idea of Native interest being represented on the house floor is going to be great."

# Oneliner

The Cherokee Nation seeks to appoint a delegate to the House of Representatives, reflecting on historical treaties and the potential for Native representation in government.

# Audience

Advocates for Native rights

# On-the-ground actions from transcript

- Support Native representation efforts within the government (suggested)
- Stay informed about the developments in Native rights and representation (implied)

# Whats missing in summary

The full transcript provides historical context and insights into the potential impact of Native representation in government.