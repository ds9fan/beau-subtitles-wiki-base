# Bits

Beau says:

- Recalls taking a wilderness first responder course with realistic training scenarios.
- Volunteers to rescue a drowning victim during training, facing unexpected challenges.
- Draws a parallel between rescuing drowning victims and the current state of the administration.
- Describes how panic affects drowning victims and likens it to the president's situation.
- Analyzes the president's strategy to energize his base for the upcoming election.
- Points out that negative voter turnout, not overwhelming support, led to the president's victory in the past.
- Identifies the president's base as bigoted old white men and explains his focus on energizing them.
- Criticizes the president's proposal to end birthright citizenship through executive order.
- Explains the historical significance of the 14th Amendment and its importance to certain demographics.
- Condemns the president's rhetoric around the 14th Amendment as a huge dog whistle to his base.
- Questions the need to indefinitely detain migrant children and points out the profit-driven motives behind it.
- Challenges the illogical reasoning behind holding children longer in detention facilities.
- Dispels common misconceptions about birthright citizenship and who actually engages in birth tourism.
- Warns of future attacks on marginalized groups by the president to rally his base and maintain power.
- Urges readiness to identify and address the dog whistles used by the president to appeal to his base.

# Quotes

- "He has to energize his base and he thinks his base is bigger than it is."
- "His base is not going to carry him to victory, but that's who he wants to energize."
- "We just need to be ready for them."
- "Either you are completely incompetent or your dog whistling to a racist base."
- "Is he stupid or is he a racist? That's it."

# Oneliner

Beau reveals parallels between rescuing drowning victims and the administration's tactics, warning of divisive dog whistles to energize a shrinking base in the upcoming election.

# Audience

Voters, Activists, Allies

# On-the-ground actions from transcript

- Recognize and challenge dog whistles in political rhetoric (suggested)
- Stay vigilant against divisive tactics targeting marginalized groups (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the president's tactics, urging vigilance against divisive rhetoric and calling out potential dog whistles to energize a specific voter base.

# Tags

#Election2020 #PoliticalStrategy #DogWhistles #MigrantRights #MarginalizedGroups