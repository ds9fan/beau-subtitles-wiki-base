# Bits

Beau says:

- Recent days have been serendipitous for him, with news stories allowing him to make wider points.
- A native woman asked about bulletproof backpacks out of fear for her child being mistaken for a Latino due to the current climate in America.
- Beau advised against bulletproof backpacks, suggesting using a laptop bag with a steel plate instead.
- Beau receives questions related to his background but is selective in answering due to the dual-use nature of the information.
- He questions how the President can sleep at night knowing his rhetoric has led to shootings and deaths.
- Beau points out the President's role in dividing rather than uniting the country.
- He urges the President to change his rhetoric to save lives and prevent further violence.
- Beau expresses disappointment that as a small-time YouTuber, he is more conscientious about his words than the President.
- He calls on the media to press the President on how he can sleep at night given the consequences of his rhetoric.
- Beau stresses the need for the President to adjust his rhetoric and work towards unifying the country, as the divide is widening and leading to deaths.

# Quotes

- "How do you go to sleep at night knowing that your words killed people and they did?"
- "Dead bodies in Walmart built the wall. That's your legacy."
- "You can change your rhetoric and you can save lives."
- "I am a small-time YouTuber and I am more conscientious about what I say, than the President of the United States."
- "How does he sleep?"

# Oneliner

Beau questions the President's ability to sleep at night, holding him accountable for the consequences of his divisive rhetoric and urging a change to save lives.

# Audience

Media, concerned citizens

# On-the-ground actions from transcript

- Press the President to change his rhetoric and work towards unifying the country (implied)

# Whats missing in summary

The full transcript provides a detailed and impassioned plea for accountability and change in political rhetoric to prevent further harm and division.

# Tags

#Accountability #PoliticalRhetoric #Unity #Change #MediaAttention