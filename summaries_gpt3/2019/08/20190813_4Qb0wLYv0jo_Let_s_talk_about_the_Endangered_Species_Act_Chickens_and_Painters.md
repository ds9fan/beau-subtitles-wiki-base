# Bits

Beau says:

- The Endangered Species Act is being changed, causing concern among conservationists worldwide.
- Oil and coal lobbyists, David Bernhardt and Andrew Willer, along with Wilbur Ross, are involved in the changes.
- The current administration's key environmental positions are held by individuals with backgrounds in oil, coal, and mining.
- Under the new changes, economic impact will now be a significant factor in court decisions regarding the protection of species.
- Every species will essentially have a price tag attached to it under the new considerations.
- Conservationists are alarmed as over a million species face extinction, now subject to economic evaluations for protection.
- Multinational corporations lack loyalty to any particular country and prioritize wealth extraction over environmental protection.
- The impact of these corporations on the environment will directly affect those residing in the United States and the Western world.
- Beau urges his audience to not just be activists but also become conservationists to combat the environmental challenges we face.
- He stresses the urgency of placing individuals in power who can make significant environmental policy changes before it's too late.

# Quotes

- "Every species will have a dollar sign attached to it."
- "We have to become environmentalists or conservationists, otherwise we're all just house painters putting it over time on August 8th, 1945 in Nagasaki."
- "We're not trying to stop huge issues from occurring in the future. We're trying to mitigate, trying to slow it down, because it's going to happen."

# Oneliner

The Endangered Species Act changes put a price on species, urging everyone to become conservationists to prevent irreversible environmental damage.

# Audience

Environmental advocates

# On-the-ground actions from transcript

- Advocate for strong environmental protections through local initiatives and community action (implied)
- Support and volunteer for conservation organizations working to protect endangered species and habitats (implied)

# Whats missing in summary

The full transcript provides additional context on the impact of multinational corporations and the urgency of addressing environmental concerns through immediate action. 

# Tags

#EndangeredSpeciesAct #Conservation #EnvironmentalProtection #MultinationalCorporations #Activism