# Bits

Beau says:

- Students at Martin County High School in Kentucky wore pro-LGBTQ shirts to support a fellow student and were made to change by the administration.
- The administration expressed concern about bullying, but the students had already decided to wear the shirts again as a form of solidarity.
- Despite facing bullying every day, the administration fails to take action to address the issue.
- A hit list circulated at the school, causing students to stay home, only to find the administrator and police officer joking about it upon their return.
- The lack of seriousness and support from the administration is appalling to Beau, especially considering the safety and security of the students.
- Beau questions the administration's ability to provide a safe environment for the students and offers the assistance of friends from Fort Campbell or Wright-Patt to step in.
- The superintendent claimed a miscommunication had been resolved, but students were left in the dark without any communication from them.
- Students are labeled as attention-seeking by faculty when they are actually seeking support and tolerance.
- The students, despite facing cynicism and opposition, express their desire for acceptance and tolerance within the school.
- Beau encourages the students to continue fighting for what they believe is right and assures them of his support and that of others watching.

# Quotes

- "We just want tolerance because we tolerate everyone around us and we deserve that too."
- "Students are asking for your help, too."
- "You don't need help, y'all need backup, that's it, and you've got it."

# Oneliner

Students at Martin County High School face bullying daily, seeking support and tolerance from an administration that fails to take action.

# Audience

Students, LGBTQ community, supporters

# On-the-ground actions from transcript

- Support the students at Martin County High School by wearing pride flags or showing solidarity in any way possible (implied)
- Encourage tolerance and acceptance within your own community and schools (implied)

# Whats missing in summary

The full transcript provides a detailed look at the challenges faced by students at Martin County High School and the lack of support from the administration, showcasing the importance of standing up for what is right and supporting those facing discrimination.

# Tags

#Students #Bullying #LGBTQ #Support #Tolerance