# Bits

Beau says:

- Introduces the story of Dan Glenn, a Navy pilot shot down in Vietnam in 1966 and held in prison camps until 1973.
- Glenn built a cabin in his mind while in captivity, using ashes and rice water to sketch it out, a coping mechanism that saved lives and preserved sanity.
- Upon returning to the United States, Glenn physically built the cabin in Oklahoma.
- The story of Glenn's coping mechanism is taught at SEER, a survival school for the US military's tough guys.
- Beau explains coping skills, categorizing them into emotion-based (like imagination), problem-based (working harder, learning from mistakes), and others.
- Coping strategies aim to maintain a positive outlook, prevent overreactions, and accept situations objectively.
- Having a positive social support network is vital in coping effectively.
- Recognizing successes, learning from mistakes, developing self-control, and maintaining oneself are part of good coping strategies.
- Beau points out a pattern of early childhood trauma, lack of coping skills, and a triggering incident leading to mass shootings.
- The root solution to preventing mass shootings lies in developing effective coping strategies, addressing the motive rather than the means (such as limiting access to weapons).
- Beau stresses that education and understanding are key to implementing this solution effectively.

# Quotes

- "He built a cabin. Not physically, he built it in his mind."
- "The toughest people in the U.S. military are taught coping skills at one of the toughest schools in the military."
- "Good ideas normally don't require force, they require just education, just a thought."

# Oneliner

Beau introduces Dan Glenn's story of building a cabin in his mind as a coping mechanism during captivity, stressing the importance of effective coping strategies in preventing tragedies like mass shootings through education and understanding.

# Audience

Individuals, educators, mental health advocates

# On-the-ground actions from transcript

- Learn about different coping strategies and mental health practices (suggested)
- Educate others on the importance of coping skills and positive social support networks (implied)

# Whats missing in summary

The full transcript provides a detailed account of the importance of coping mechanisms in dealing with trauma and preventing tragic outcomes like mass shootings through education and understanding.

# Tags

#CopingSkills #MentalHealth #Prevention #Education #CommunitySafety