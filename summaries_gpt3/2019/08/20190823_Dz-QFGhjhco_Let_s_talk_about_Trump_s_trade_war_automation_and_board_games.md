# Bits

Beau says:

- Chinese tariffs on another $75 billion imposed.
- President orders American companies to find alternative to China.
- Blending corporate and government interests.
- President's exact words: American companies must start looking for alternative to China, including bringing companies home and producing in the US.
- Unlikely for companies to return production to the US; more likely to move to Vietnam or similar countries.
- Bringing production facilities back won't bring back jobs as automation has replaced manual labor.
- Automation is cheaper and more reliable than human labor.
- U.S. losing in trade war against China.
- Move increases chance of recession and income inequality.
- Need for a new system not built on human exploitation.
- Trump's scapegoating in blaming Jay Powell for economic issues.
- Loss of largest trading partner (China) without valid reason.
- Beau points out the need for a more equitable society where everyone can "stay in the game."
- Contrasts Monopoly game dynamics with real life inequality and exploitation.
- Calls for a fair society where everyone starts off equal.

# Quotes

- "Bringing production facilities back won't bring back jobs as automation has replaced manual labor."
- "Need for a new system not built on human exploitation."
- "People get so far ahead that they start mocking those in poverty."
- "Upheaval, the pitchforks come out."
- "Build a society that is more equitable, that is more fair, a society in which everybody can stay in the game."

# Oneliner

Beau criticizes the economic decisions affecting American companies, warns of increased inequality, and calls for a fair society where everyone can participate equally.

# Audience

Economic Activists, Social Justice Advocates

# On-the-ground actions from transcript

- Advocate for fair trade policies (implied)
- Support movements for income equality (implied)
- Engage in community organizing for equitable systems (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the impact of trade decisions on American companies, the role of automation in job loss, and the need for a more equitable society.

# Tags

#TradeWar #Automation #IncomeInequality #EconomicJustice #Equity #CommunityOrganizing