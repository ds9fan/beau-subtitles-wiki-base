# Bits

Beau says:

- Addressing an incident in Tennessee involving two 18-year-olds breaking into a school and stealing an AR-15 and vests, stressing the need to learn from it.
- Emphasizing the importance of acknowledging warning signs and making changes before harm occurs.
- Criticizing the downplaying of the incident by officials and the lack of notification to parents about the danger students were in.
- Advocating for proper gun safes and keeping ammunition on school resource officers to prevent unauthorized access to weapons.
- Challenging misconceptions about firearms, particularly around the use of AR-15s in school shootings.
- Arguing for practical solutions like real gun safes, better security measures, and more training with firearms to improve safety in schools.

# Quotes

- "You have to err on the side of caution."
- "If it can be defeated with this, the weapon's not secured."
- "They can make the decision whether or not to send their kid to school."
- "Keep the ammo on you."
- "Real gun safes. Keep the ammo on you."

# Oneliner

Addressing a Tennessee incident involving stolen firearms, Beau stresses learning from warning signs to prevent harm and advocates for better gun safety measures in schools.

# Audience

Parents, educators, community members

# On-the-ground actions from transcript

- Keep ammunition on school resource officers for immediate access (implied)
- Implement real gun safes for secure storage of weapons (implied)
- Educate on firearm safety and training for effective use (implied)

# Whats missing in summary

The full transcript provides detailed insights on firearm safety, addressing misconceptions, and advocating for practical solutions to prevent harm in schools.

# Tags

#SchoolSafety #FirearmSafety #CommunityAction #Prevention #Education