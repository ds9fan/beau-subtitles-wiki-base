# Bits

Beau says:

- Exploring the concept of supporting the troops in modern times, beyond bumper stickers and slogans.
- Emphasizing that supporting troops primarily means the local populace in areas of operations.
- Providing a historical example of the Hmong people's betrayal after assisting the U.S. during the Vietnam War.
- Criticizing the abandonment of Iraqi nationals who helped the U.S., facing visa backlogs and imminent danger.
- Pointing out the plight of interpreters waiting for visas, with a drastic decline in approvals.
- Expressing concern for individuals risking their lives and being left behind due to changing political tides.
- Raising the issue of child soldiers and questioning the widespread nature of this practice.
- Drawing parallels between past mistakes, like Vietnam, and the current abandonment of allies.

# Quotes

- "The reality is, in modern war, your support doesn't matter."
- "They are being left to die. That is what is going to happen to them."
- "We learned nothing from Vietnam. We are making the same mistakes again."

# Oneliner

In modern warfare, supporting troops means prioritizing local communities, not slogans, while history repeats the betrayal of allies risking their lives.

# Audience

Advocates for human rights

# On-the-ground actions from transcript

- Advocate for expedited visa processing for Iraqi nationals and interpreters (implied)
- Support organizations aiding at-risk allies and interpreters (exemplified)

# Whats missing in summary

The emotional depth and personal stories shared by Beau can be best experienced in the full transcript. 

# Tags

#SupportTheTroops #BetrayalOfAllies #VisaProcessing #HumanRights #ModernWarfare