# Bits

Beau says:

- Explains the impact of the trade war on American farmers, particularly with China no longer buying agricultural products.
- China bought $19.5 billion worth of goods from the U.S. in 2017, dropping to $9.2 billion in 2018, and now it's zero.
- Points out that food is a necessity, unlike luxury items like iPads, meaning the effects will be felt more intensely.
- Mentions deals between Brazil and China for soybeans, which used to make up 60% of U.S. soybean exports.
- Talks about the long-lasting effects of the trade war even after it ends, as American farmers will need to compete to regain lost business.
- Notes that subsidies for farmers might not be a preferred solution, as farmers want to rely on business rather than government aid.
- Predicts a significant impact on Trump in the 2020 elections due to economic repercussions felt by states like Iowa, known for soybean production.
- States that the trade war has resulted in $27 billion in extra costs for American taxpayers until June.
- Mentions potential store closings and job losses in companies like Joanne Fabrics and the American Textile Company due to increased prices and reduced demand.

# Quotes

- "They bought $19.5 billion worth. 2018, 9.2, a loss of $10 billion that was going to the American farmer."
- "It's going to have a long-lasting effect because food isn't like an iPad, you know?"
- "The problem with that is most farmers don't really want subsidies. They don't."
- "They know what their pocketbook looks like, and I think that's going to have a bigger impact than people are saying."
- "If it goes on much longer, they're looking at store closings and job eliminations."

# Oneliner

American farmers hit hard by China's halt in agricultural purchases, facing long-lasting consequences; trade war effects extend beyond economy to impact jobs and businesses.

# Audience

American citizens, farmers, policymakers

# On-the-ground actions from transcript

- Contact local representatives to advocate for fair trade policies that support American farmers (implied)
- Support local farmers' markets and businesses to help offset the impacts of trade wars (implied)

# Whats missing in summary

The full transcript provides detailed insights into the economic consequences of the trade war on American farmers and businesses. Viewing the complete video can offer a comprehensive understanding of the situation.

# Tags

#TradeWar #Agriculture #Economy #AmericanFarmers #ChinaTrade #PolicyImpact