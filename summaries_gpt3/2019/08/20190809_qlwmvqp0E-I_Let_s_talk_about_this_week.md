# Bits

Beau says:

- Recaps recent events, including Pentagon surveillance balloons in Wisconsin for total information awareness.
- Mentions the Walmart incident where thankfully no one got hurt.
- Talks about ice raids resulting in hundreds of people disappearing, leaving kids at daycare and coming home to empty houses.
- Shares a story of a man deported to Iraq where he eventually died, despite never having been there and not speaking Arabic.
- Describes a disturbing incident of a child's skull being literally cracked over not removing a hat during the National Anthem.
- Comments on an immigration activist caught with weapons standing by a Trump mural on his truck.
- Addresses the evolution of a gesture from a joke to a white nationalist symbol.
- Criticizes blind loyalty to symbols over their true meanings and the dangerous consequences.
- Warns about the increasing violence and threat faced by minority groups in the current political climate.
- Urges everyone, especially non-white and non-straight individuals, to be vigilant and cautious in the deteriorating situation.

# Quotes

- "If you were out there to own the libs, yeah, you are now forever identified as a white nationalist."
- "Imagine being so in love with an idea of a symbol that you start to hate what it's supposed to represent."
- "People are dying. Children are having their skulls cracked."
- "As things start to deteriorate in the economy, you're going to see them start to become disillusioned and it's going to make them more violent."
- "Winning comes at a cost, and it's gonna be the minority groups that's who's gonna be hit the most."

# Oneliner

Beau recaps recent events, warns of escalating violence against minority groups, and urges vigilance in the face of deteriorating political and economic conditions.

# Audience

Activists, minorities, allies

# On-the-ground actions from transcript

- Stay vigilant and aware of your surroundings (implied)
- Be cautious and careful in the current political climate (implied)
- Keep your eyes open for signs of escalating violence (implied)

# Whats missing in summary

The full transcript provides a detailed and emotional analysis of recent events, warning of the increasing violence and threats faced by minority groups in a deteriorating political climate.

# Tags

#CurrentEvents #Violence #MinorityRights #Vigilance #Activism