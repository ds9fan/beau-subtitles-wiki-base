# Bits

Beau says:

- Shares a story about misconceptions based on reputation, using his friend's experience in Chicago as an example.
- Recalls a personal experience from high school where a fight led to him gaining a reputation that wasn't entirely deserved.
- Examines murder rates in various cities and states, comparing them to challenge the reputation of Chicago as a crime-ridden city.
- Points out that Chicago doesn't have one of the highest murder rates when compared to other cities and states.
- Mentions the role of propaganda in shaping false narratives about crime rates in Chicago, related to a former president and certain news networks.
- Concludes by urging caution when interpreting statistics and narratives, especially when they are manipulated to create fear.

# Quotes

- "Chicago is not Mogadishu."
- "Reputations are sometimes unearned."
- "Be very, very careful when you're looking at statistics and you're looking at narratives that are supposedly based on statistics."

# Oneliner

Beau challenges misconceptions about Chicago's crime reputation and warns against believing manipulated statistics and narratives.

# Audience

Community members, skeptics

# On-the-ground actions from transcript

- Challenge false narratives about communities (implied)
- Verify statistics and information before forming opinions (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of reputation, crime rates, and propaganda, offering a broader perspective on how perceptions can be skewed.

# Tags

#Reputation #Misconceptions #CrimeRates #Propaganda #CommunityAction