# Bits

Beau says:

- Four boys, ages 15 and 16 in Arkansas, finished football practice and started fundraising in August 2019.
- The boys, African-American, were selling discount cards door-to-door when a woman confronted them with a gun, mistaking them for suspicious individuals.
- The woman called the cops, describing the boys as suspicious because they were African-American in a white neighborhood.
- Despite one of the cops recognizing the boys as school resource officer, the woman lectured them on "horseplay" instead of apologizing after the misunderstanding.
- Beau draws parallels to the infamous Dred Scott case, pointing out the persistent racist attitudes that lead to incidents like this.
- The woman, Jerry Kelly, 46, was charged with four counts of aggravated assault, false imprisonment, and endangering the welfare of a minor.
- Kelly's connection to law enforcement through her previous employment and her husband's position raised concerns about preferential treatment in the case.
- The incident underscores the ongoing racial prejudices and dangers faced by African-American youth, even in 2019.
- Beau questions the progress made in society when incidents like this continue to occur.
- The boys, engaging in a wholesome activity, faced a traumatic experience due to racial bias and ignorance.

# Quotes

- "Four teenage boys had a revolver shoved in their face because they were fundraising for football in a white neighborhood."
- "Man, that is some Dred Scott stuff right there."
- "How could this happen? But we have to say it every day."
- "Because well, I mean, they don't have any real rights."
- "Anyway, it's just a fault, y'all have a good day."

# Oneliner

Four African-American boys fundraising for football faced racial bias and a gun in a white neighborhood in 2019, leading to charges against the aggressor with ties to law enforcement.

# Audience

Community members

# On-the-ground actions from transcript

- Support community initiatives promoting racial equality and understanding (suggested)
- Advocate for fair treatment and justice for victims of racial discrimination (implied)
- Stand against racial prejudices and biases in your community (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of racial biases and injustices that persist despite societal advancements.

# Tags

#RacialBias #Youth #CommunityPolicing #Justice #Equality #Arkansas