# Bits

Beau says:

- Criticizes the administration for focusing on rewriting the immigration laws instead of addressing more pressing issues.
- Points out the media's role in perpetuating misleading narratives about legal immigration.
- Contrasts Trump's deportation numbers with those of the Obama administration.
- Notes the ineffectiveness of Trump's efforts due to his fixation on stereotypes of undocumented immigrants.
- Explains how the Obama administration utilized the surveillance state for deportations.
- Tells the story of Alejandra Juarez, who self-deported after facing legal challenges despite being in the US for 20 years.
- Criticizes the administration's treatment of deported military spouses and its impact on combat veterans.
- Addresses the demonization of immigrants receiving benefits compared to corporate welfare.
- Calls out the administration for causing harm through policies like the trade war.

# Quotes

- "It's about framing it so you kick down. You blame those with less power than you for the mistakes or the deliberate attacks on you by those at the top."
- "They find somebody to scare you. Somebody for you to kick at. Most times isn't real."
- "This administration is not the friend of anybody in the working class."

# Oneliner

Beau criticizes the administration's immigration focus, deportation tactics, and treatment of deported military spouses while urging working-class solidarity and exposing harmful policies.

# Audience

Working-class Americans

# On-the-ground actions from transcript

- Support deported military spouses' communities (exemplified)
- Advocate for fair treatment of immigrants and combat stereotypes (suggested)
- Educate others on the impact of harmful policies on working-class communities (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of immigration policies, deportations, and their effects on individuals and communities, urging viewers to question narratives and stand in solidarity with marginalized groups.

# Tags

#Immigration #Deportation #WorkingClass #Solidarity #Policy