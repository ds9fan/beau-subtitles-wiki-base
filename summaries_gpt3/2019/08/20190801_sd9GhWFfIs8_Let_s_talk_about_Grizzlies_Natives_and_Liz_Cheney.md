# Bits

Beau says:

- Beau usually avoids talking about native issues but made an exception due to a recent win regarding grizzly bear protection.
- A coalition of native groups and conservationists worked hard to protect grizzlies in the Greater Yellowstone area.
- Liz Cheney criticized the protection of grizzlies, referring to it as a threat to the Western way of life.
- Beau questions the concept of "Western way of life" and implies it may refer more to European colonizers than true Americans.
- Beau contrasts the lack of criticism towards Trump's jokes about genocide with the protection of natives, showing a selective approach.
- Grizzlies hold deep significance for native people, representing a sacred connection akin to God.
- Native practices involving grizzlies, like the bear dance, hold immense historical and spiritual importance.
- The use of grizzly claws in necklaces by natives is sacred and not a mere fashion statement.
- Despite the spiritual importance of grizzlies to natives, they had to resort to environmental law to protect them successfully.
- Beau criticizes the insensitive and dismissive attitude towards native issues and practices in the legal system.

# Quotes

- "That powerful creature is God."
- "Your buddies not being able to go on high dollar trophy hunts is not destroying your way of life."
- "The American way of life is not actually extracting and ripping every single thing you can out of the earth and giving nothing back."
- "We could learn a lot from the natives."
- "In the immortal words of your father, Miss Cheney, go have a good day."

# Oneliner

Beau questions the Western way of life narrative, defends native practices, and sheds light on the sacred significance of grizzlies to indigenous communities.

# Audience

Advocates for indigenous rights

# On-the-ground actions from transcript

- Respect and support indigenous-led conservation efforts (exemplified)
- Educate yourself on the cultural significance of animals like grizzlies to native communities (suggested)
- Advocate for fair treatment and representation of native issues in legal and political spheres (implied)

# Whats missing in summary

The emotional impact of Beau's defense of native practices and criticism of dismissive attitudes towards indigenous issues.

# Tags

#IndigenousRights #GrizzlyBears #Conservation #CulturalSignificance #EnvironmentalJustice