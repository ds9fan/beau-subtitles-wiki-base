# Bits

Beau says:

- Explains the situation with Title X funding and Planned Parenthood, pointing out misconceptions and facts.
- Describes how Planned Parenthood withdrew from Title X programs due to a rule prohibiting abortion referrals in exchange for funding.
- Clarifies that the $60 million given up by Planned Parenthood was not used for abortions but for services like STD screenings and birth control under Title X.
- Counters the idea of Planned Parenthood being defunded by putting the $60 million loss into perspective compared to their overall revenue of $1.67 billion.
- Reveals that Planned Parenthood received $563 million in tax dollars last year, the majority of which came from Medicaid and was not used for abortions.
- Notes that Planned Parenthood will need to increase fundraising by 10% to make up for the loss of $60 million.
- Urges individuals to donate or purchase merchandise to support Planned Parenthood's health centers.
- Emphasizes the various healthcare services provided by Planned Parenthood beyond abortion, including breast exams and birth control.
- Points out the irony that reducing access to birth control can lead to more unplanned pregnancies and, consequently, more abortions.
- Explains the importance of private donations in keeping Planned Parenthood health centers operational and serving underserved communities.

# Quotes

- "Well golly gee whiz, it seems like they do things other than abortion."
- "Why abort when adoption is an option?"
- "Congratulations, you played yourself."

# Oneliner

Beau breaks down misconceptions around Planned Parenthood's funding and services, urging support to maintain vital healthcare access beyond abortion.

# Audience

Advocates, Donors, Supporters

# On-the-ground actions from transcript

- Support Planned Parenthood by donating directly or purchasing merchandise (suggested).
- Increase donation to Planned Parenthood by 10% to help maintain their health centers (suggested).
- Educate others on the misconceptions surrounding Planned Parenthood's funding and services (implied).

# Whats missing in summary

Full context and depth of Beau's breakdown on Planned Parenthood's funding and the potential impact of misconceptions.

# Tags

#PlannedParenthood #TitleX #Funding #Healthcare #Misconceptions