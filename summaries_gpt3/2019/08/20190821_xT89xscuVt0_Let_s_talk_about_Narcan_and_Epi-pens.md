# Bits

Beau says:

- Addresses a question about a meme comparing the cost of Narcan to EpiPens.
- Clarifies that Narcan is not free; someone pays for it, and there are ways to cover the cost.
- Explains the process where EMS administers Narcan and bills for it, just like any other life-saving drug.
- Questions whether people want a scenario where insurance is checked before administering life-saving treatment.
- Mentions grants, insurance companies, treatment centers, and foundations that cover the cost of Narcan.
- Talks about free training available for Narcan administration on International Overdose Awareness Day.
- Debunks the argument that Narcan enables addiction, as it actually inhibits effects and can lead to recovery.
- Addresses the stigma surrounding substance abuse and how it influences perceptions of who should receive life-saving drugs.
- Comments on society's anger towards pharmaceutical companies for high prices, like $600 EpiPens with a manufacturing cost of $2.
- Conveys the message that societal anger often gets misdirected towards vulnerable groups like addicts.

# Quotes

- "Well, addicts should die."
- "It encourages the devaluation of human life."
- "Can't do anything about that. Need to kick down. That lowly addict."
- "No sane person is angry about that."
- "It's just a thought."

# Oneliner

Beau addresses misconceptions about Narcan and EpiPens, debunking myths and revealing societal attitudes towards addiction and pharmaceutical pricing.

# Audience

Advocates, Community Members

# On-the-ground actions from transcript

- Contact local EMS or substance abuse treatment centers to inquire about Narcan training opportunities (suggested).
- Support foundations that provide funding for Narcan distribution (exemplified).
- Advocate for pharmaceutical pricing transparency and fair drug pricing (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of the cost and accessibility of Narcan compared to EpiPens, as well as societal attitudes towards addiction and pharmaceutical pricing.

# Tags

#Narcan #EpiPens #SubstanceAbuse #Pharmaceuticals #Addiction