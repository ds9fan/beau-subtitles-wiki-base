# Bits

Beau says:

- Critiques the blending of corporate and state interests in the Shell Trump photo-op.
- Explains how Shell contractors faced unfair choices during the event.
- Mentions leaked memos revealing instructions for workers to appear as props.
- Describes how corporations repay politicians by using workers as props.
- Points out the false image created by such propaganda.
- Analyzes how politicians manipulate public perception through divisive tactics.
- Questions the dominance of certain professions in running the country.
- Calls for more representation of working-class individuals in positions of power.
- Advocates for unity among the working class for better governance.
- Suggests the need for genuine representative democracy.

# Quotes

- "You're not actually going to be doing any work you don't need your safety gear except for that shirt so it looks like you're working class because you're a prop nothing more."
- "What we really need is a lot more bartenders, carpenters, plumbers, people like that in the House of Representatives, people that actually represent us."
- "We need a little bit more working class unity."

# Oneliner

Beau criticizes the Shell Trump photo-op, exposes unfair treatment of workers, and calls for more working-class representation in government for true democracy.

# Audience

Working-class Americans

# On-the-ground actions from transcript

- Advocate for more working-class representation in government (implied)
- Foster unity and communication among the working class (implied)

# Whats missing in summary

Full context and detailed analysis of the Shell Trump photo-op and its implications.

# Tags

#CorporateInterests #WorkingClass #GovernmentRepresentation #Propaganda #Unity