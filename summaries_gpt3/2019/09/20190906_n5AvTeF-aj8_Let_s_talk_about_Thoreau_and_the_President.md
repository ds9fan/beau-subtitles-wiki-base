# Bits

Beau says:

- Beau introduces the topic of Henry David Thoreau leaving Walden Pond and rejoining society 172 years ago.
- Thoreau was Harvard-educated but rejected traditional careers, choosing to live deliberately and self-reliantly.
- Thoreau's connection with Ralph Waldo Emerson, a transcendentalist, influenced his beliefs in rejecting materialism and valuing spiritual over material.
- Thoreau lived at Walden Pond in a cabin, working one day a week and focusing on self-discovery.
- Thoreau believed that technology and social interactions were often used to fill inner voids.
- Thoreau appreciated nature, saw man as part of nature, and emphasized living deliberately before death.
- He wrote an essay "Civil Disobedience" criticizing President James K. Polk's administration and advocating for peaceful resistance.
- "Civil Disobedience" influenced figures like Gandhi, Martin Luther King Jr., and Sophie Scholl.
- Thoreau refused to pay taxes to a government supporting unjust practices like slave patrols, leading to his imprisonment.
- He believed that under an unjust government, a just man's place is in prison.

# Quotes

- "Under a government which imposes any unjustly, the true place for a just man is prison."
- "Civil disobedience is, you can download it for free on Google."
- "When confronted with a government that is behaving immorally, he said that a man cannot without disgrace associate with it."

# Oneliner

Beau talks about Thoreau's deliberate living, influence on civil disobedience, and standing against unjust governments.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Download and read "Civil Disobedience" for free on Google (suggested).

# Whats missing in summary

Exploration of Thoreau's impact on civil disobedience movements throughout history.

# Tags

#HenryDavidThoreau #CivilDisobedience #Activism #SelfReliance #Influence