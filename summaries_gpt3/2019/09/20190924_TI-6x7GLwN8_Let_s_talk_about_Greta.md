# Bits

Beau says:
- Woke up to news about Greta, a teenager leading the charge against climate change.
- Conservative and Republican pundits faced backlash for cyberbullying Greta.
- Expresses surprise at the backlash received by the pundits.
- Points out that bullying a teenager isn't the worst thing they've done.
- Mentions the lack of decency standards being held for the pundits.
- Reads comments on Twitter attacking Greta's character.
- Criticizes attacking Greta's looks and making fun of her hair.
- Comments on the ridiculousness of the attacks compared to discussing climate change mitigation ideas.
- Notes the hypocrisy of those accusing Greta of emotional child abuse.
- Points out the absurdity of attacking a teenager personally instead of discussing ideas.
- Acknowledges Greta's efforts in trying to save the world.
- Notes the irony of calling Greta a fear monger while promoting fear themselves.
- Encourages Greta to keep going despite the criticism.

# Quotes

- "She's better than you. So you try to attack her character."
- "Greta, you're judged in this life by your enemies more than your friends."

# Oneliner

Beau points out the hypocrisy and lack of decency in cyberbullying a teenager leading the fight against climate change.

# Audience

Social media users

# On-the-ground actions from transcript

- Show support for teenagers like Greta who are advocating for climate change action (implied)

# Whats missing in summary

The full transcript provides a detailed look at the cyberbullying faced by Greta and the importance of focusing on ideas rather than personal attacks.