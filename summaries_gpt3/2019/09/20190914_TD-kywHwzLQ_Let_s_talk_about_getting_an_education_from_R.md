# Bits

Beau says:

- Introduces an educational chatbot named Roo that provides information many parents are uncomfortable discussing.
- Only 24 states in the U.S. require comprehensive sexual education, with just 13 of those states mandating medically accurate content.
- Roo is capable of answering a wide range of questions, from puberty to relationship advice, and has had over a million interactions in nine months.
- Acknowledges that many teens turn to Roo because they lack someone in their lives to talk to about sensitive topics.
- Emphasizes the importance of open communication between parents and children to prevent misinformation and poor decision-making.
- Criticizes the irony of individuals opposing funding for organizations like Roo while their own children benefit from its services.
- Views the reliance on Roo for critical information as a reflection of societal failures in providing adequate education and support for teens.
- Stresses the significance of education in empowering young people to make informed choices and navigate complex issues.

# Quotes

- "Teens don't make the wisest decisions even when they have all the information."
- "Teens are turning to the internet, turning to a chat bot to get advice that they should very, that they should feel comfortable getting from their parents."
- "This is not a topic that you want teens making decisions without all the information."

# Oneliner

An educational chatbot fills the void left by parents and schools in providing vital information and support to teens, reflecting broader societal failures in communication and education.

# Audience

Parents, educators, policymakers

# On-the-ground actions from transcript

- Support and advocate for comprehensive sexual education in all states (implied)
- Encourage open communication with children on sensitive topics (implied)
- Promote resources like educational chatbots for teens lacking support systems (implied)

# Whats missing in summary

The full transcript provides detailed insights into the necessity of open communication and comprehensive education for teens to make informed decisions and avoid potential pitfalls.