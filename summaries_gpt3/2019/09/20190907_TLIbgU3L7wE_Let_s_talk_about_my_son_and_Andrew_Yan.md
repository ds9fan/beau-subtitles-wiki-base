# Bits

Beau says:
- Beau's son prompted him to talk about candidate Y's proposal X and Universal Basic Income (UBI).
- UBI is not a new concept; it dates back to the 1600s, proposed by Thomas Paine.
- UBI involves giving $1000 a month to all US citizens over 18, eliminating most social safety nets.
- Studies show positive outcomes of UBI, but criticism is often influenced by the establishment's interests.
- Andrew Yang advocates for UBI due to the threat of automation eliminating jobs.
- UBI could be funded by replacing social safety nets with a Value Added Tax (VAT).
- The potential impact of UBI on different sectors, like housing, is discussed.
- Beau acknowledges reservations about politicians using UBI to manipulate citizens.
- UBI is not socialism but rooted in capitalism and requires capitalism to function.
- Beau concludes that while he has reservations, UBI is a workable plan that deserves consideration.

# Quotes
- "UBI could work. So you're letting your own ideological biases interfere with you making videos about something that could work and change the world."
- "It's not new, it's not radical."
- "So when you hear criticisms of this, just remember that not all of them are ideological. You need to consider the source."
- "This isn't socialism. This is firmly rooted in capitalism."
- "It's worth talking about."

# Oneliner
Beau dives into Universal Basic Income (UBI), discussing its historical roots, potential impact, criticisms, and the need for open debate on its viability in a capitalist framework.

# Audience
Citizens, policymakers, voters

# On-the-ground actions from transcript
- Engage in open, constructive dialogues about UBI's implications and feasibility (implied)
- Support candidates who advocate for innovative social policies like UBI (implied)

# Whats missing in summary
Deeper insights into the potential socio-economic effects of UBI implementation and the importance of considering diverse perspectives in policy debates.

# Tags
#UniversalBasicIncome #AndrewYang #Automation #Capitalism #PolicyDebate