# Bits

Beau says:

- Recounts an experience in a cave with his family and a Spanish-speaking family.
- Observes young kids charging ahead during a guided cave tour.
- Criticizes adults who dismiss teenagers' political opinions.
- Points out the lack of guidance offered to youth about education.
- Expresses disagreement with the conclusions of the Parkland teens but respects their effort.
- Compares Parkland teens to adults making jokes about guns on social media.
- Emphasizes that youth speak up because adults won't.
- Draws a parallel between cave instinct and adults not leading in society.
- Calls out parents who neglect educating themselves and their kids about climate change.
- Condemns parents who prioritize politics over their children's future.
- Encourages parents to prioritize educating their children over political allegiance.
- Talks about the impact of Greta Thunberg's speech on children's awareness.
- Urges parents to prioritize their children over political parties for a better future.

# Quotes

- "You aren't working to protect your kids. You're leaving it to them to figure out."
- "Your kid will betray your ideology in her name. Because they're smarter. Because they're more willing to learn."
- "If you want teens out of the political discussion, you've got to step up."

# Oneliner

Beau criticizes adults for failing to guide youth and encourages parents to prioritize education over political allegiance to empower the next generation.

# Audience

Parents, Adults

# On-the-ground actions from transcript

- Educate yourself on climate change and other critical issues affecting your children's future (suggested)
- Prioritize educating your children over political allegiance (suggested)
- Encourage open-mindedness and learning in your children (suggested)

# Whats missing in summary

The emotional impact of parents prioritizing politics over their children's well-being and future.

# Tags

#Youth #Parenting #Education #ClimateChange #PoliticalEngagement