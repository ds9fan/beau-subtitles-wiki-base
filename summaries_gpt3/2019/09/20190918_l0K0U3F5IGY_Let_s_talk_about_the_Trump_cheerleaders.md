# Bits

Beau says:

- Responds to a message about Trump cheerleaders being punished for displaying a banner supporting Trump 2020 before a game at North Stanley High School in North Carolina.
- North Stanley High School has a policy against political signs, but the issue is whether the banner was disruptive to the school, not the political nature of the sign.
- Points out that students have rights at school, and the test is whether their actions are disruptive.
- Explains that the cheerleaders, as representatives of a government entity receiving government money, can't display political messages while in uniform.
- Clarifies that the punishment for the cheerleaders was probation, meaning they were simply told not to do it again and faced no significant consequences.
- Emphasizes that allowing students to have meaningful political discourse in schools is vital to avoid the rise of candidates like Trump.
- Encourages open, non-confrontational discourse to understand why the cheerleaders support Trump.
- Advocates for defending free speech within the bounds of a civilized society, even if one disagrees with the message being conveyed.

# Quotes

- "Students do not shed their rights at schoolhouse gate, period."
- "Given the fact that they were in uniform, yeah, that's probably fitting."
- "I think that is something that needs to change and in the meantime we need to defend what little ability students have to discuss politics at school."
- "We have to defend it as much as we can within the bounds of a civilized society."
- "Whether or not I agree with it shouldn't matter."

# Oneliner

Beau explains the controversy around Trump-supporting cheerleaders, advocating for meaningful political discourse in schools within the bounds of free speech.

# Audience

Students, educators, community members

# On-the-ground actions from transcript

- Contact the cheerleaders for non-confrontational political discourse (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the situation with Trump-supporting cheerleaders and the importance of allowing political discourse in schools while defending free speech.

# Tags

#Students #FreeSpeech #PoliticalDiscourse #Schools #Trump