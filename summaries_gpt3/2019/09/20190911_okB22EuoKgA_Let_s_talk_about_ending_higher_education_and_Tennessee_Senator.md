# Bits

Beau says:

- Tennessee Senator Kerry Roberts expressed the belief that higher education institutions should be abolished because they are "liberal breeding grounds" that teach ridiculous things contradicting American values.
- Roberts criticized a woman with higher education who spoke out against white supremacy and oppression, believing such views go against the country's values.
- Instead of reflecting on whether Republican policies are outdated, Roberts proposed banning schools and eliminating institutions of higher education.
- Beau mentions that individuals with higher education, or those who read more, are less likely to support Republican policies.
- Conservative figure George Will referred to the GOP as "the dumb party," indicating a disconnect with young people.
- Roberts' approach of promoting uneducated workers who can't think for themselves is seen as a strategy to maintain power and control over a permanent underclass.
- This view suggests a desire to create a workforce easily manipulated by corporate interests.
- The proposal to abolish higher education institutions serves the agenda of perpetuating a class of individuals reliant on authority figures for direction.
- Beau implies that the GOP's salvation, as perceived by Roberts, lies in limiting education to maintain control over the population.
- The focus appears to be on securing a compliant workforce rather than encouraging critical thinking and informed decision-making.

# Quotes

- "The stupid stuff that our kids are being taught is absolutely ridiculous."
- "Rather than take that information and say, hey, are our policies outdated? Have we lost touch? Have we not kept up with the growth of new information in the world? No, we need to ban schools."
- "They want to create that permanent underclass, that uneducated worker class that can't think for themselves, the proles, the good workers for Big Brother and their corporate donors."

# Oneliner

Tennessee Senator calls for abolishing higher education, believing it breeds liberalism, while overlooking policy reflections and opting for uneducated workforce to bolster GOP control.

# Audience

Advocates for education

# On-the-ground actions from transcript

- Advocate for accessible and inclusive education (implied)
- Support policies and initiatives that prioritize education and critical thinking (implied)

# Whats missing in summary

The full transcript includes additional context on the GOP's views on education and workforce dynamics that provide a comprehensive understanding of the implications of anti-intellectualism.

# Tags

#Education #RepublicanParty #CriticalThinking #PoliticalViews #Workforce