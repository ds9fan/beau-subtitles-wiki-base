# Bits

Beau says:

- Addresses questions about Trudeau and Yang, both involving a common component.
- Mentions the controversy surrounding Trudeau's blackface incident and the apology.
- Describes Southern American blackface as evil due to its malicious nature and historical context.
- Comments on the relevance of his opinion regarding Trudeau's apology, suggesting the focus should be on those directly impacted.
- Shares responses from people of color regarding Trudeau, including differing opinions on his actions and apology.
- Emphasizes the importance of understanding varying perspectives within impacted communities.
- Responds to questions about Yang and stereotypes, particularly regarding Asians excelling in math and becoming doctors.
- Advocates for seeking the opinions of those directly involved in a situation rather than assuming all opinions carry equal weight.
- Critiques the societal pressure to have an opinion on everything in the age of social media.
- Encourages considering whose voices need to be amplified and heard more in various situations.

# Quotes

- "Blackface is bad, okay? Southern American blackface is evil. It is evil."
- "If Jimmy insults Suzie, you don't ask Billy if the apology was sufficient."
- "Sometimes it's better to get the opinion of people that are actually involved."
- "Not everything requires your personal response."
- "Your personal response may drown out the opinions of those people that need to be heard more."

# Oneliner

Beau addresses controversies around Trudeau's blackface and Yang's stereotypes, urging to prioritize the voices of those directly impacted over irrelevant opinions in societal discourse.

# Audience

Social media users

# On-the-ground actions from transcript

- Reach out to and amplify the voices of those directly impacted by controversies rather than focusing on irrelevant opinions (implied).
- Engage in meaningful dialogues with communities affected by stereotypes to understand their perspectives (implied).

# Whats missing in summary

Beau's nuanced perspective and call for prioritizing the voices of those directly involved in controversial issues can best be appreciated through the full transcript.

# Tags

#Controversy #Opinions #PrioritizeVoices #SocialMedia #CommunityEngagement