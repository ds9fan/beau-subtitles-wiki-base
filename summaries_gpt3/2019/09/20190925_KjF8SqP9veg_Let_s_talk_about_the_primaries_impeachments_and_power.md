# Bits

Beau says:

- Talks about the first Republican primary debates between Weld and Walsh, which most people missed.
- Accuses the Republican party of rigging the election in favor of Donald Trump by shutting down primaries and not promoting debates.
- Points out the party's support for Trump amid impeachment proceedings, predicting the Senate will never convict him.
- Criticizes the lack of accountability and integrity in politics, focusing on the pursuit of power over truth and justice.
- Emphasizes that the solution lies with local communities and individuals who genuinely believe in their values.
- Challenges the notion that corruption in politics is a bug in the system rather than inherent to it.
- Expresses concern over Americans being misled by corrupt politicians who prioritize self-interest over serving the people.
- Condemns the Senate's potential refusal to hold the President accountable, questioning the fitness of all Republican senators for office.
- Urges people to recognize the widespread deceit and corruption in politics rather than accepting it as normal.
- Calls for a shift in mindset to acknowledge the truth about politicians' motives and representation of the public.

# Quotes

- "Your solution is not in D.C., it's local."
- "The fact that we don't realize that is why we're in trouble."
- "That is corruption on a wide level."
- "If that statement is true… none of them are fit to hold office."
- "Because they tell us they're liars and cheats."

# Oneliner

Beau challenges the accepted corruption in politics, urging a shift towards local solutions and a rejection of deceitful politicians prioritizing power over truth.

# Audience

American voters

# On-the-ground actions from transcript

- Reach out to local communities and individuals who prioritize truth and justice (suggested)
- Challenge corrupt practices within your local political sphere (exemplified)
- Reject the normalization of deceit and corruption in politics (implied)

# Whats missing in summary

The full transcript provides in-depth insights on the corrupt nature of politics, urging individuals to seek solutions locally and question the integrity of elected officials.