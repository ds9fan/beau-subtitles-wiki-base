# Bits

Beau says:

- Discussed a school in Michigan designed to mitigate the effects of an attack with a $48 million cost.
- Shared frustrations about the difficulties of defending schools due to their design flaws.
- Acknowledged the distressing nature of a teacher seeking advice on being armed at school.
- Praised the innovative design of the Michigan school for addressing security concerns effectively.
- Emphasized the importance of solutions like the Michigan school while addressing root causes like mental health and misconceptions about masculinity.
- Stressed the ineffectiveness of a "magic pill" solution like banning guns in a society with a vast number of firearms.
- Illustrated the impracticality of removing guns from society by using numerical examples.
- Urged for multiple solutions like the Michigan school design to save lives in the face of a complex societal problem.
- Called for early mental health care availability and addressing misconceptions about masculinity as part of the solution.
- Criticized politicians for promising quick fixes akin to a "magic pill" for the deep-rooted issue of gun violence.

# Quotes

- "That school will save lives."
- "The magnitude of the problem we are facing in this country is a lot bigger than I think most people realize."
- "There's no magic pill."
- "We need to make mental health care available early."
- "We are going to have to go down a long road to fix this problem."

# Oneliner

Beau explains a Michigan school's innovative design to enhance safety, criticizing "magic pill" solutions for gun violence while advocating for holistic approaches.

# Audience

Community members, policymakers.

# On-the-ground actions from transcript

- Advocate for early mental health care availability and challenge misconceptions about masculinity (implied).
- Support the implementation of innovative security designs in schools (implied).

# Whats missing in summary

The detailed explanations and context provided by Beau in the full transcript.

# Tags

#GunViolence #SchoolSafety #CommunitySolutions #MentalHealth #PolicyChange