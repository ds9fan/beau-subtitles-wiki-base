# Bits

Beau says:

- Responds to comments about helping the Bahamas, questioning why we can't help our own people.
- Believes in helping those in need regardless of skin tone, religion, or cultural differences.
- Challenges the idea of "othering" people based on nationality or location.
- Compares his relatability to a person from the Bahamas over politicians on Capitol Hill.
- Encourages thinking beyond borders and nationalism.
- Compares nationalism to gang mentality, questioning the difference.
- Emphasizes the commonalities between people born in different places rather than their differences.
- Stresses the importance of humanity and helping beyond borders.

# Quotes

- "Your obligation to humanity does not end at the border."
- "We have to start thinking broader than a border."
- "Those colors. So much of what you believe is a person and how you identify yourself, all hinges on what colors are flying on that flagpole outside of the hospital you were born in."
- "Me and that guy from the Bahamas, oh, we sit down and have a drink. We can relate on a meaningful level because we're kind of the same."
- "Your obligation to humanity does not end at the border."

# Oneliner

Beau challenges nationalism, advocating for helping beyond borders and finding common ground with those in need. 

# Audience

Global citizens

# On-the-ground actions from transcript

- Connect with individuals from different countries to understand their experiences (implied)
- Challenge nationalist rhetoric and stereotypes in your community (implied)
- Volunteer or donate to organizations supporting international aid efforts (implied)

# Whats missing in summary

The full transcript provides a deep exploration of breaking down barriers and finding common humanity beyond borders.

# Tags

#Nationalism #Humanity #Unity #Community #InternationalAid #BeyondBorders