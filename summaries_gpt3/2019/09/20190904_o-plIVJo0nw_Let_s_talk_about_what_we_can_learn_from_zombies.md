# Bits

Beau says:

- Compares zombie movies to surviving natural disasters and life in general, noting that the rules are the same.
- Suggests framing natural disaster preparedness for kids as a zombie apocalypse to make it less scary.
- Explains that learning survival skills changes how people view the world and what's truly valuable in life.
- Outlines key rules for surviving a natural disaster by drawing parallels to a zombie outbreak.
- Emphasizes the importance of staying with a group, finding safety, and maintaining communication in a crisis.
- Points out that most people in disaster situations are good but cautions about individuals looking to exploit vulnerabilities.
- Shares a real-life example where people overlooked needed supplies during a crisis due to societal norms.
- Stresses the importance of improvisation and seeing things differently to navigate challenging situations effectively.
- Encourages teaching youth survival skills to empower them to change their circumstances and use resources creatively.
- Concludes by underscoring the potential for individuals to reshape their world by realizing the possible amidst the seemingly impossible.

# Quotes

- "The rules are the same."
- "You see what things could be, if you looked at them in the right way."
- "People get in that mindset of, I have to get water."
- "The impossible is possible."
- "They can use the tools around them to make something else."

# Oneliner

Beau compares surviving natural disasters to zombie movies, urging a shift in perspective to see possibilities amidst challenges and empowering youth to reshape their world.

# Audience

Parents, educators, disaster preparedness advocates

# On-the-ground actions from transcript

- Teach children survival skills through fun activities and games (implied)
- Organize community workshops on disaster preparedness and improvisation techniques (implied)
- Encourage youth to think creatively and problem-solve in challenging scenarios (implied)

# Whats missing in summary

The full transcript provides a detailed analogy between surviving natural disasters and zombie outbreaks, stressing the importance of preparedness, group cohesion, resourcefulness, and a shift in mindset towards possibilities.

# Tags

#NaturalDisasters #ZombieMovies #SurvivalSkills #YouthEmpowerment #DisasterPreparedness