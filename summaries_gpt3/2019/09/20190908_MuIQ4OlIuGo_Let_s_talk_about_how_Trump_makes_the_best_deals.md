# Bits

Beau says:
- Expresses lack of surprise at the president's incompetence and hypocrisy on the international stage.
- Criticizes the choice of a real estate lawyer for Mideast peace negotiations.
- Calls out the president's decision to cancel peace negotiations with the Taliban.
- Questions the moral difference between the Taliban's actions and the president's deportation policies.
- Points out the futility of negotiating with the Taliban when the U.S. has announced its withdrawal.
- Accuses the administration of selling out allies in Afghanistan for domestic political leverage.
- Criticizes the president's negotiations with the Taliban while innocent people are dying.
- Compares President Trump's actions to his past criticism of negotiating with the Taliban.
- Condemns the administration's actions and questions if anyone is genuinely shocked by them.

# Quotes

- "What kind of people would kill so many in order to seemingly strengthen their position? Well for one, the Taliban."
- "Everybody knows this and everybody knows that we're selling out our allies."
- "The only thing we're doing right now is determining the body count."
- "I did for the first month or two, but at this point, every time they do something, I'm like, Now that wasn't as bad as I thought it was going to be."
- "It is going to happen."

# Oneliner

Beau expresses lack of surprise at the president's international incompetence, questions negotiations with the Taliban, and accuses the administration of selling out allies for political gain.

# Audience

Critically engaged citizens

# On-the-ground actions from transcript

- Protest against decisions that prioritize political gain over human lives (implied)
- Advocate for transparent and ethical international negotiations (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the administration's international actions and their consequences.

# Tags

#InternationalRelations #TalibanNegotiations #PoliticalCritique #HumanRights #EthicalLeadership