# Bits

Beau says:

- President Trump's strained relationship with the media began early in his campaign, attacking them consistently.
- Trump demands total subservience, compliance, and complicity from news outlets, similar to a dictator.
- Fox News, previously spared from Trump's attacks, is now facing his wrath for not fully supporting his agenda.
- Some Fox News anchors are standing up to Trump, pointing out that they don't work for him.
- Beau criticizes Fox News for justifying and promoting the administration's heinous acts to the American people.
- Trump's targeting of CNN and now Fox News shows his disregard for any dissenting voices in the media.
- Beau points out that Fox News is facing consequences for not taking a stand against Trump earlier when he attacked other journalists and threw children in cages.
- The attempt by some personalities at Fox News to reclaim objectivity now that they are under attack is seen as insincere by Beau.
- Beau finds it entertaining that Trump's actions are leaving former allies like Fox News on the sidelines.
- Comparisons are drawn between Trump and Nixon in terms of their controversial relationships with the media and desire for revenge upon leaving office.

# Quotes

- "Yeah, you do."
- "You standing up to Trump now means nothing."
- "When the president branded other journalists as fake news, that's when you should have said something."
- "Now you want to reclaim some of that objectivity. Doesn't matter, nobody's buying it."
- "Without Trump, what have they got?"

# Oneliner

President Trump demands complete subservience from the media, leaving even former allies like Fox News facing consequences for not taking a stand earlier against his actions.

# Audience

Media consumers

# On-the-ground actions from transcript

- Stand up to demands for subservience from any source, whether political or otherwise (implied)
- Take a stand against attacks on journalistic ethics and freedom of the press (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of President Trump's relationship with the media and the consequences faced by news outlets like Fox News for not taking a stand against his actions sooner.

# Tags

#Media #Politics #FreedomOfPress #Journalism #Trump