# Bits

Beau says:

- Talks about the Great Fire of London in 1666, starting with a bake house near Pudding Lane.
- Explains that a spark fell into fuel, setting off a massive fire due to the kindling-like surroundings.
- Around 60 to 100,000 people were left homeless, with a third of London destroyed.
- Despite claims of only six to eight recorded deaths, the true toll remains unknown.
- Describes the lack of infrastructure and planning that exacerbated the disaster.
- Mentions the warnings and knowledge of potential fire spread, but profit interests hindered preventative measures.
- Points out the parallels between this historical event and today's climate crisis.
- Challenges those who deny climate change to research and acknowledge the overwhelming evidence against their arguments.
- Emphasizes the disproportionate impact of such crises on the powerless and poor.
- Raises the issue of prioritizing profits over necessary investments in infrastructure and planning.

# Quotes

- "It's funny. Americans are the way we are."
- "Take your arguments against it, your go-to arguments. Type it into Google, followed by the word debunked, and just read."
- "People denying it when anybody who looks into it can see that it's very evident."
- "Just like this."
- "We don't want to waste money on something."

# Oneliner

Beau delves into the Great Fire of London, drawing striking parallels to today's climate crisis and the repercussions of profit-driven denial.

# Audience

Climate change activists

# On-the-ground actions from transcript

- Research the overwhelming evidence supporting climate change and share it with skeptics (suggested)
- Advocate for prioritizing infrastructure and planning for climate resilience in your community (implied)

# Whats missing in summary

The emotional impact and urgency conveyed by Beau as he draws a powerful connection between historical negligence and present-day climate crisis.

# Tags

#ClimateChange #HistoricalDisaster #Infrastructure #Planning #CommunityAction