# Bits

Beau says:

- Introduces the topic of Upton Sinclair, an American author known for writing almost 100 books and winning the Pulitzer Prize for fiction.
- Upton Sinclair's books, often journalism disguised as fiction, had a significant impact in the United States.
- "The Jungle," one of Sinclair's famous works, written in 1906 about the meatpacking industry, led to the passing of the Pure Food and Drug Act and the Meat Inspection Act.
- Upton Sinclair is credited with being a driving force behind better policing in slaughterhouses due to his impactful book.
- The USDA is removing prohibitions on line speed in slaughterhouses, allowing them to self-police, which can have detrimental effects on food safety.
- The industry, not just Trump, has been lobbying for removing regulations on line speed for profitability reasons.
- Despite past failures in pilot programs, the plan to let slaughterhouses develop their own standards is still moving forward.
- Companies prioritizing profits over safety may lead to a rise in foodborne illnesses from pork.
- Beau questions the effectiveness of companies self-policing and raises concerns about potential consequences.

# Quotes

- "It's going to make it more profitable for them to staff the safety inspectors than it is for the government to do it."
- "In my ideal world, yeah companies like this would police themselves, but in my ideal world they'd police themselves and we know that isn't what's going to happen here."
- "It's something that we might want to keep in mind."

# Oneliner

Beau warns of the dangerous implications of allowing slaughterhouses to self-police, prioritizing profits over consumer safety.

# Audience

Food safety advocates

# On-the-ground actions from transcript

- Contact local representatives to advocate for stricter regulations on slaughterhouses (implied)
- Join or support organizations that focus on food safety and regulation (implied)

# Whats missing in summary

The emotional impact of potential food safety risks and the call to action for individuals to advocate for safer food practices.

# Tags

#FoodSafety #Regulations #Slaughterhouses #ConsumerAdvocacy #UptonSinclair