# Bits

Beau says:
- Addresses the president's proposal to protect religious freedom by safeguarding religious sites, including those of historical significance.
- Supports the idea of protecting religious and cultural sites, expanding beyond just religious places.
- Mentions the existing organization UNESCO, which already works on this concept.
- Criticizes the president for pulling out of UNESCO and undermining global efforts to protect cultural and religious sites.
- Points out the importance of protecting religious freedom for individuals, as outlined in the Universal Declaration on Human Rights.
- Criticizes the president for undermining human rights through relationships with dictators.
- Calls out the president for failing to uphold treaty obligations regarding the treatment of refugees.
- Labels the president's proposal as a con, stating that it is nothing new and that he has undermined similar initiatives throughout his administration.
- Acknowledges the importance of protecting historical and cultural sites but criticizes the president for only now showing interest due to public backlash.
- Concludes by suggesting that the president's recent actions are merely attempts to deceive the public in light of his declining popularity.

# Quotes

- "It's a con."
- "There's nothing new in this. There's nothing visionary in this."
- "He realizes he's losing. He realizes that people are turning against him, realizing that he's a con."
- "If he actually cared about any of this stuff, he could have acted on it any time within the last three years."
- "It's just a thought."

# Oneliner

Beau criticizes the president's proposal to protect religious freedom, calling it a con and pointing out his history of undermining similar initiatives.

# Audience

Policy Analysts

# On-the-ground actions from transcript

- Support UNESCO's efforts to protect cultural and religious sites (exemplified)
- Advocate for upholding treaty obligations regarding the treatment of refugees (exemplified)

# Whats missing in summary

Beau's passionate delivery and detailed analysis can be fully appreciated by watching the full video. 

# Tags

#ReligiousFreedom #HumanRights #CulturalHeritage #TrumpAdministration #UNESCO