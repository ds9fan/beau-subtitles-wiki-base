# Bits

Beau says:

- Expresses no desire to change speech despite Democratic candidates' coarsening.
- Believes in reaching people with thought, not shock value.
- Views being edgy as a poor substitute for personality.
- Shares an incident on Twitter where he misunderstood a tweet and sought clarification.
- Emphasizes the importance of common courtesy and avoiding intentional offense.
- Stresses the value of reaching out for understanding.
- Mentions starting a podcast with reruns of older videos to introduce himself to the community.
- Talks about potential sponsored videos, stating they must fit the channel's theme.
- Mentions plans for workshops in the South to build community networks and possibly going on the road next summer.

# Quotes

- "Being edgy is not a substitute for having a personality."
- "It's almost always better to be nice."
- "Y'all have a good night."

# Oneliner

Beau believes in thoughtful speech over shock value, stresses common courtesy, and plans community workshops.

# Audience

Content creators

# On-the-ground actions from transcript

- Set up workshops to help build community networks (planned)
- Attend workshops to learn from others (planned)

# Whats missing in summary

Details about the podcast content and potential future episodes

# Tags

#Speech #CommunityBuilding #Podcast #ContentCreation #Workshops