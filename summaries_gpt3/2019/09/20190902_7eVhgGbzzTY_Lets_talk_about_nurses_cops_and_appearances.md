# Bits

Beau says:

- Former Olympic skier turned nurse arrested for obstruction of justice at University of Utah Hospital in 2017.
- Nurse intervened when police officer demanded blood draw from unconscious patient without consent.
- Nurse explained patient rights and was later released without charges.
- Arresting officer fired and lieutenant demoted after viral body cam footage of arrest.
- Nurse settled for $500,000, part of which went towards making body camera footage public.
- Officer sued for $1.5 million, claiming negativity from people who hate cops.
- Nurse Wubbles listed as one of the top physicians of 2017 for standing against officer abuse of authority.
- Officer started working as a civilian corrections assistant on August 9th.
- Opinions vary on officer's actions and whether he should be working in a facility without patient advocates.
- Beau leaves it open for viewers to share their thoughts on the situation.

# Quotes

- "Nurses tend to see the best in people."
- "She stood in the way of an officer abusing his authority."
- "Y'all have a good day."

# Oneliner

Former Olympic skier turned nurse intervenes to protect patient rights, leading to officer's firing, settlement, and controversial return to work as civilian corrections assistant.

# Audience

Advocates for patient rights.

# On-the-ground actions from transcript

- Support organizations advocating for patient rights (implied)
- Advocate for transparency in law enforcement through body camera footage (implied)

# Whats missing in summary

Details on the emotional impact on Nurse Wubbles, and the broader implications of this incident on police accountability and patient advocacy.

# Tags

#PatientRights #PoliceAccountability #BodyCameraFootage #NurseAdvocacy #Transparency