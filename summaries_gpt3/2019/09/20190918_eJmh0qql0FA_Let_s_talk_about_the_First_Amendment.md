# Bits

Beau says:

- People were mad about squelching free speech rights, exemplified by criminalizing pipeline protests in red states and Trump's suggestion to strip citizenship for flag burning.
- Republicans support making it illegal to burn the flag, prohibiting offensive statements against police or military, and banning face coverings.
- Republicans show less support for extending free speech courtesies to the LGBTQ community.
- Republicans favor stopping construction of new mosques and arresting hecklers rather than disciplining them.
- Republicans believe the press has too much freedom and express a negative view towards journalists.
- Beau dismisses claims of a free speech crisis on campuses, noting that professors being fired for political speech are mostly liberals, not conservatives.
- Beau explains that universities provide platforms for dissenting viewpoints, and students protesting speeches is protected by the First Amendment.
- The far right seeks a platform, not free speech, and universities are more accepting of dissenting viewpoints.
- Free speech means the right to speak, not entitlement to a platform or equal time for all ideas.
- Beau criticizes the far right for wanting to use the government to dictate speech and actions, contrasting it with the principles of free speech and dissent.

# Quotes

- "The far right doesn't want speech. They don't want free speech. They want a platform."
- "You don't need the government's gun. You better let this guy talk."
- "A good idea will find a platform. Doesn't need one handed to him."
- "You can be deplatformed and if the idea is solid, another platformer will arise where you can create your own."
- "That's the right and y'all are utter failures at it."

# Oneliner

Beau breaks down the misconception around free speech, exposing the far right's desire for a platform over true freedom of expression, critiquing their attempts to dictate speech through government intervention.

# Audience

Activists, Free Speech Advocates

# On-the-ground actions from transcript

- Stand up for true free speech by advocating for platforms for diverse voices (implied).
- Educate others on the distinction between a platform and free speech (implied).
- Support institutions that uphold principles of free speech and dissent (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of free speech misconceptions and the far right's pursuit of platforms over genuine freedom of expression. Viewing the full transcript offers a comprehensive understanding of these issues. 

# Tags

#FreeSpeech #FarRight #PlatformOverSpeech #Dissent #GovernmentIntervention