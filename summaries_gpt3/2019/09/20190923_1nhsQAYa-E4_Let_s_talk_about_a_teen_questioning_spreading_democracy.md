# Bits

Beau says:

- A 15-year-old asks why the US government wants countries to be democratic, questioning if democracy can be forced.
- Beau praises the question as advanced thinking for a teenager, stating that democracy requires active participation and desire.
- He mentions that the US has a powerful war machine and has been spreading democracy globally.
- Beau questions if the US truly spreads democracy or if it's just a narrative.
- Referring to a study by Rich Whitney, Beau reveals that the US provides military aid to many dictatorships, contradicting the promotion of democracy.
- He explains that intelligence operations prioritize American interests over idealistic values like democracy.
- Beau points out that stability is paramount for the US, sometimes conflicting with the establishment of democracies.
- Democracy is described as being in retreat, as per the 2019 Freedom House report.
- Beau encourages critical thinking and questioning narratives with holes.

# Quotes

- "Democracy is advanced citizenship, man. It's hard, you gotta want it."
- "Democracy doesn't matter. If it did, we wouldn't be doing that."
- "Most of them have holes you can walk through."
- "You really should. You are thinking at a level far beyond what is expected of you."
- "Democracy in retreat."

# Oneliner

A 15-year-old's profound query on democracy prompts Beau to reveal contradictions in US actions, urging critical thinking and questioning narratives with gaps.

# Audience

Youth, Critical Thinkers

# On-the-ground actions from transcript

- Question narratives and seek the truth (implied)
- Advocate for transparency in government actions (implied)
- Engage in critical thinking and analyze information critically (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the contradictions between the promotion of democracy and US actions, urging individuals to question narratives and think critically.