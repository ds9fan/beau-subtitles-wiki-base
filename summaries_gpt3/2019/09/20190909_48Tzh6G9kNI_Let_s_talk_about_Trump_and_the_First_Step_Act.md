# Bits

Beau says:

- Acknowledges Trump's signing of the First Step Act but questions the follow-up actions.
- Trump felt personally attacked by the news and lashed out on Twitter, making himself a national embarrassment.
- The First Step Act was described as a small first step in criminal justice reform.
- Despite its importance to those directly impacted, Beau likens it to a bandaid on a bullet wound in the grand scheme.
- Provides cost details of the First Step Act and contrasts it with the allocated budget amount.
- Points out the massive scope of the incarceration problem in America, with the highest rate globally.
- Trump's legislation is criticized for not addressing the root causes of the issue.
- Beau accuses Trump of increasing police militarization, expanding incarceration centers, and benefiting lobbyists profiting from human suffering.
- Mocks Trump for wanting recognition for minimal efforts in criminal justice reform.
- Concludes by calling out Trump's actions as insufficient and damaging, despite signing a bill.

# Quotes

- "A bandaid on a bullet wound."
- "You want a participation trophy."
- "You've done nothing other than increase militarization of police."

# Oneliner

Trump signed a small first step in criminal justice reform, but underfunded and undermined it, while failing to address root causes and perpetuating harmful systems.

# Audience

Reform advocates

# On-the-ground actions from transcript

- Advocate for comprehensive criminal justice reform (implied)
- Support organizations working towards true systemic change (implied)
- Hold elected officials accountable for meaningful reform (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of Trump's actions regarding criminal justice reform and calls for a deeper examination of systemic issues beyond superficial legislation.

# Tags

#CriminalJusticeReform #Trump #FirstStepAct #Incarceration #PoliceMilitarization