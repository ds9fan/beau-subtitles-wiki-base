# Bits

Beau says:

- Indicates recession indicators like the bond curve inversion, gold and silver price increase, and copper price decrease signaling hard economic times.
- Mentions that consumer confidence has dropped significantly, with the University of Michigan's data from August contradicting old data from the Bureau of Economic Analysis in July.
- Points out slowing growth in GDP, lowest manufacturing growth levels in a decade, and significant drop in freight shipments.
- Private domestic investments have fallen by 5%, indicating a significant economic shift.
- S&P 500 estimates of earnings growth are constantly dropping, a strong signal of an impending recession.
- Normal people may face layoffs but could benefit from cheaper goods and opportunities to start businesses or invest in property during a recession.

# Quotes

- "Toys get cheaper, and by toys I don't mean toys for children."
- "If you've been planning on bootstrapping and starting your own company, now may not be the brightest time to start it, but it may be the best time to start getting the equipment."
- "It's one of those things where if we continue fighting something that is probably inevitable, we may end up hurting ourselves."

# Oneliner

Beau warns of recession indicators like dropping consumer confidence and slowing GDP, offering insights on how normal people can navigate economic downturns.

# Audience

Economic observers

# On-the-ground actions from transcript

- Buy equipment for planned businesses or investments (exemplified)
- Look for deals on property during a recession (exemplified)

# Whats missing in summary

Detailed explanation of the impact of trade wars on the economy.

# Tags

#Recession #EconomicIndicators #ConsumerConfidence #Investing #TradeWars