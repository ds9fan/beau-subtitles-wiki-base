# Bits

Beau says:

- Received an email warning a shelter about his appearance before delivering charity supplies.
- Stereotype: resembling the image of a person you wouldn't want to buzz in - big beard, tattoos.
- Stereotypes can be dangerous as they may lead to missing signs of domestic violence.
- Domestic violence affects everyone regardless of race, ethnicity, gender, or socioeconomic class.
- Dangerous to perceive domestic violence as only happening in certain areas, leading to overlooking signs.
- Shelter previously printed shirts to dispel the stereotype associated with Beau's appearance.
- Domestic abusers don't have a specific uniform.
- Urges to be mindful of stereotypes and not overlook signs of domestic violence.
- Domestic violence is intersectional and can happen anywhere.
- Importance of dispelling harmful stereotypes surrounding domestic violence.

# Quotes

- "There is no uniform for a domestic abuser."
- "Domestic violence is truly intersectional."
- "We really need to remember there is no uniform for a domestic abuser."

# Oneliner

Beau warns about dangerous stereotypes surrounding domestic violence and the importance of not overlooking signs due to preconceived notions.

# Audience

Community members, advocates

# On-the-ground actions from transcript

- Support and volunteer at domestic violence shelters (implied)
- Educate others on the intersectionality of domestic violence (implied)
- Challenge and dispel harmful stereotypes about domestic violence (implied)

# Whats missing in summary

Importance of educating others to recognize signs of domestic violence and dispelling harmful stereotypes to ensure support for all victims.

# Tags

#DomesticViolence #Stereotypes #Intersectionality #CommunitySupport #Awareness