# Bits

Beau says:

- Introduction of Howie Hawkins, a Green Party candidate with a background in social movements and environmental activism.
- Howie Hawkins shares his history of being a teamster, his involvement in forming the Green Party, and his previous statewide campaigns.
- Emphasis on building the Green Party as a major party and advocating for eco-socialist solutions to pressing issues like climate change, inequality, and nuclear disarmament.
- Howie Hawkins details his Green New Deal proposal, contrasting it with the Democrats' version and stressing the urgency to address climate change by 2030.
- Importance of third-party representation in elections to push progressive agendas and hold major parties accountable.
- Howie Hawkins explains the dangers of the current nuclear arms race, advocating for disarmament and referencing past treaties.
- Addressing job concerns in the transition to a green economy, ensuring a just transition and providing millions of jobs.
- Expansion on the Economic Bill of Rights proposed by the Green Party, including job guarantees, income above poverty, affordable housing, universal rent control, and healthcare for all.
- Advocacy for open borders and ending mass surveillance, supporting whistleblowers like Edward Snowden.
- The need for grassroots organizing to bridge divides among working-class individuals and build a mass party for working people.
- Focus on electoral reform, advocating for a national popular vote with ranked-choice voting to eliminate the Electoral College.
- Challenges faced by progressive Democrats and the potential for a Green Party to attract disenchanted voters, particularly non-voters.

# Quotes

- "If we're gonna have a left and alternative in this country, we gotta have our own independent voice and our own independent power."
- "There's no such thing as an unorganized socialist. You gotta get in an organization."
- "Strength in numbers. When we're working together, we get our ideas tested."

# Oneliner

Howie Hawkins advocates for eco-socialist solutions, a Green New Deal by 2030, and grassroots organizing to build a major Green Party base, challenging the Democratic establishment and calling for electoral reform.

# Audience

Progressive activists, third-party supporters

# On-the-ground actions from transcript

- Contact the Howie Hawkins campaign via howiehawkins.us to volunteer, donate, and help with ballot access efforts (suggested).
- Host house parties for grassroots fundraising and awareness about Howie Hawkins' campaign (suggested).
- Engage in electioneering canvassing activities to reach out to potential supporters and collect information for future engagement (suggested).

# Whats missing in summary

The full transcript provides detailed insights into Howie Hawkins' background, policy proposals, challenges faced by third-party candidates, and the importance of grassroots organizing for political change. Viewing the full transcript offers a comprehensive understanding of his platform and strategies for political engagement.

# Tags

#HowieHawkins #GreenParty #EcoSocialism #GrassrootsOrganizing #ThirdParty #ElectoralReform