# Bits

Beau says:

- Beau sets the stage to talk about Giuliani's report and shares a hypothetical story about a cop convinced of someone's wrongdoing.
- The cop, obsessively watching the person's house, discovers stolen property inside.
- Both the cop and the person with stolen property get arrested after separate trials, illustrating accountability.
- Beau expresses curiosity about Giuliani's report, wanting to see the evidence against the Bidens.
- He mentions that if Biden abused his office, he should face consequences, but the same accountability should apply to Trump.
- Beau stresses the importance of holding leaders accountable for any abuse of power, regardless of political affiliation.
- He questions the timing of bringing up evidence during Biden's trial instead of the President's impeachment.
- Beau argues that evidence should not be used to muddy the waters but to ensure accountability.
- He concludes by urging for accountability for both Biden and Trump if wrongdoing is proven.

# Quotes

- "But it does not absolve the administration of any of their alleged crimes, even if they were right. It's not how this works."
- "Either what we believe is good and true is good and true, or we don't have any order. We don't have any laws."
- "If Biden abused his office for personal gain, he should go down. And if you believe that, you have to believe the same is true for Trump."

# Oneliner

Beau delves into Giuliani's report, advocates for accountability across political lines, and questions the timing of evidence presentation during trials.

# Audience

Citizens, voters, activists

# On-the-ground actions from transcript

- Hold leaders accountable for abuses of power (implied)
- Advocate for equal accountability across political affiliations (implied)

# Whats missing in summary

The passionate delivery and nuanced arguments made by Beau can be best experienced by watching the full transcript.