# Bits

Beau says:

- Exploring the question of why white people who seem to care or understand are often Irish or of Irish ancestry.
- Providing a timeline of the Civil Rights Movement in the 1960s, detailing protests, police response, bombings, and government infiltration.
- Drawing parallels between the Black Panthers in the U.S. and the Irish Republican Army in Europe during the same timeline.
- Noting the camaraderie between Irish and black communities in the American South due to shared experiences of oppression and intermarriage.
- Mentioning that Irish-Americans in the South tend to have strong family bonds and pride in their heritage.
- Questioning when personal responsibility should kick in, considering ongoing systemic issues like disproportionate sentencing and redlining.
- Stating that until institutional and systemic issues are eradicated, expecting individuals to solely overcome challenges is unreasonable.
- Emphasizing the need to first stop systemic issues before taking steps like reparations to make amends.
- Criticizing proposed reparations figures as initially insulting but acknowledging recent talks of higher figures being more appropriate.
- Stressing the importance of awareness that the fight against systemic issues is ongoing and subtle, requiring continued efforts to overcome.

# Quotes
- "The problem is in the US, it's not over."
- "We can't say, 'Pull yourself up by your bootstraps' until those institutional issues are gone."
- "The primary thing is we've got to make it stop first."

# Oneliner
Exploring why white people who care often have Irish ancestry, examining Civil Rights Movement parallels, and stressing the need to end systemic issues before focusing on reparations.

# Audience
Activists, Community Members

# On-the-ground actions from transcript
- Advocate for ending systemic issues (implied)
- Support reparations initiatives (implied)
- Stay informed and engaged in ongoing fights against systemic racism (implied)

# Whats missing in summary
The full transcript delves deeper into the historical context of racial dynamics and the importance of acknowledging ongoing systemic issues to achieve true equality. 

# Tags
#CivilRights #SystemicRacism #Reparations #IrishHeritage #CommunityPolicing