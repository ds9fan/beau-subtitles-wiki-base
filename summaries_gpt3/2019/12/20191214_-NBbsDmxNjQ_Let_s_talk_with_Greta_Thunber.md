# Bits

Beau says:
- Welcomes Greta Thunberg as a special guest, prepared for a confrontational interview due to her controversial figure.
- Questions Greta's lack of life experience to advocate for climate change and mocks her inability to work basic technology.
- Mistakes a Halloween decoration for Greta and continues the playful banter about generational differences in technology use.
- Applauds Greta for facing criticism from powerful figures like Putin and Trump, encouraging her not to chill out but to keep advocating and learning.
- Urges Greta to watch documentaries and be better than older generations who didn't take action on climate change.

Greta Thunberg says:
- Responds with silence to Beau's initial mocking about technology, prompting him to realize his mistake.
- Listens as Beau acknowledges the unjust bullying she faces online and praises her for standing strong against powerful critics.

# Quotes
- "I could have swore they said she's come by."
- "You're in this problem right now because a lot of us, that's what we did. We didn't do anything."
- "You keep watching documentaries. You be better than us."

# Oneliner
Beau confronts Greta Thunberg on her advocacy, mocking her tech skills, but ends up praising her resilience and urging continued activism and learning.

# Audience
Climate activists, youth advocates.

# On-the-ground actions from transcript
- Watch documentaries on climate change and take action to be better advocates (implied).

# Whats missing in summary
The full transcript provides a playful yet insightful look at the generational differences in activism and the importance of continuous advocacy for climate change.

# Tags
#ClimateChange #Activism #GenerationalDifferences #YouthAdvocacy #Documentaries