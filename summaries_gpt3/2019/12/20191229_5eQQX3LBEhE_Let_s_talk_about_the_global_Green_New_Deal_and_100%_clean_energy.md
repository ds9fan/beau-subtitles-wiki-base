# Bits

Beau says:

- Introduces the concept of the global Green New Deal as a massive infrastructure project to achieve 100% clean energy by 2050.
- Emphasizes that 95% of the technology needed for this project is already available commercially.
- Mentions that the remaining theoretical technology pertains to long-distance and ocean travel.
- Reveals the $73 trillion price tag spread across 143 countries, with potential to create 28.6 million more jobs than those lost.
- Asserts that the project will pay for itself within seven years, even if it takes longer.
- Notes the feasibility of the project both technologically and economically, with potential for completion by 2030 if resistance to change is overcome.
- Addresses human resistance to change as a significant factor in delaying the implementation of such projects.
- Raises the issue of inevitable opposition studies funded by oil companies that may slow down progress.
- Urges for immediate action due to the pressing need to combat climate change and pollution.
- Ends with a thought-provoking question on why not start the transition away from fossil fuels now.

# Quotes

- "It's a mortgage on the planet."
- "We are addicted to fossil fuels."
- "We are resistant to change."
- "We're going to have to do this at some point. Why not do it now?"
- "Y'all have a good night."

# Oneliner

Beau introduces the monumental global Green New Deal, suggesting immediate action to combat climate change and pollution, despite human resistance to change.

# Audience

Climate activists, policymakers, environmental advocates.

# On-the-ground actions from transcript

- Advocate for and support policies that prioritize transitioning to clean energy sources (implied).
- Raise awareness about the urgency of addressing climate change and pollution in communities (implied).
- Support initiatives that create job opportunities in the clean energy sector (implied).

# Whats missing in summary

The full transcript includes detailed insights on the economic feasibility, potential job creation, and challenges posed by human resistance to change when implementing monumental projects like the global Green New Deal.