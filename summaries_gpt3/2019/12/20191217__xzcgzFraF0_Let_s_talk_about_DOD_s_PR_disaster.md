# Bits

Beau says:

- Department of Defense posted a glowing biography of a literal war criminal without explanation.
- The post was shocking and lacked context, causing confusion and concern.
- The mistake was likely due to a management error, not a moral one.
- The narrative may have been intended to be posted in parts throughout a commemoration.
- The post has been deleted, but there may have been plans for more posts.
- The incident serves as a reminder that social media blunders may not always be what they seem.
- The importance of proper context and clarity in social media communications.

# Quotes

- "They didn't explain what was going on. It was shocking, to say the least."
- "When we see social media blunders they may not always be what they appear..."
- "The mistake was likely due to a management error, not a moral one."

# Oneliner

Department of Defense posted a glowing biography of a war criminal, sparking confusion and concern, revealing a management blunder, not a moral lapse.

# Audience

Social media managers

# On-the-ground actions from transcript

- Contact Department of Defense to express concerns about the post (suggested)

# Whats missing in summary

The impact of the incident on affected military units and individuals. 

# Tags

#SocialMediaBlunders #DepartmentOfDefense #WarCriminal #ManagementMistake #Context #Communication