# Bits

Beau says:

- Introduction of Catherine and her work as an intersectional anti-capitalist YouTuber covering social and political issues from an intersectional anti-capitalist perspective.
- Overview of the UK election results where the Conservative Party secured a significant majority while Labour faced a major defeat, with reasons including media bias and their stance on Brexit.
- Impact of the election results on Scotland and Northern Ireland, with the SNP gaining seats and potential calls for a second referendum for Scotland to leave England.
- Speculation on the impact of Brexit on the border between North and South Ireland, considering varying statements made by Boris Johnson regarding a hard border.
- Delving into Catherine's video on body positivity, critiquing how mainstream representations still reinforce normative beauty standards and capitalist consumption.
- Explanation of patriarchy as a system where men hold power, discussing how masculinity and femininity traits are valued unequally in society.
- Exploration of how patriarchy harms men, leading to issues like suppressed emotions, mental health problems, and harmful gender expectations.
- Connection between capitalism and patriarchy, detailing how capitalism historically oppressed women and continues to subjugate them for profit.
- Vision of a post-patriarchal society where all hierarchies are dismantled, allowing individuals to live without gendered norms and restrictions.

# Quotes

- "So many ways in which patriarchy harms men just as much well not just as much but definitely harms men as well."
- "Capitalism is inherently based off of the oppression of women."
- "I don't think that we can get rid of men dominating women without also getting rid of men, like humans dominating nature and dominating animals."

# Oneliner

Beau introduces Catherine, covers the UK election results, Brexit's impact on Ireland, body positivity's flaws, patriarchy's effects on men, and the ties between capitalism and patriarchy.

# Audience

Intersectional activists

# On-the-ground actions from transcript

- Advocate for gender equality and challenge societal norms through education and awareness (implied).
- Support organizations promoting body positivity and representation for all body types (implied).
- Engage in community dialogues about patriarchy and its impact on society (implied).

# Whats missing in summary

Exploration of deeper topics beyond the transcript, such as the podcast content and potential future collaborations.

# Tags

#IntersectionalActivism #UKPolitics #ElectionResults #BodyPositivity #Patriarchy #Capitalism #GenderEquality #SocialChange #CommunityAction #Awareness