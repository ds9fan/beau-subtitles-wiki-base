# Bits

Beau says:

- Addresses the issue of homeless veterans and the common response of prioritizing their needs over other social safety net concerns.
- Mentions the need for facilities across states to house homeless vets, providing them with necessities like hygiene, food, and shelter.
- Points out that there are already facilities capable of housing homeless vets, with room for more people.
- Suggests honoring treaty obligations to free up space in facilities currently holding asylum seekers and migrants.
- Proposes putting individuals with criminal records or deemed dangerous in county jails, while utilizing current incarcerated individuals for assistance.
- Challenges viewers to take action by contacting their senators instead of just discussing the issue online.
- Emphasizes the importance of prioritizing the well-being of homeless vets over political agendas.
- Advocates for a comprehensive solution to homelessness rather than temporary fixes.
- Urges for support in reintegrating homeless individuals back into society through assistance with housing costs.
- Calls for a shift in focus towards loving homeless vets more than harboring negative sentiments towards certain groups.

# Quotes

- "All it takes is for those people who say they care about homeless vets to love homeless vets more than they hate brown people."
- "The solution is there."
- "The facilities are already there."
- "What are we waiting for?"
- "It's just a thought."

# Oneliner

Beau addresses homelessness among veterans, proposing a solution that involves utilizing existing facilities, honoring treaty obligations, and prioritizing genuine care over political biases.

# Audience

Advocates, Activists, Supporters

# On-the-ground actions from transcript

- Call your senator to advocate for better facilities to support homeless veterans (suggested)
- Support efforts to reintegrate homeless individuals back into society by assisting with housing costs (implied)

# Whats missing in summary

The detailed statistics and specific examples provided by Beau in the full transcript are missing from this summary. Watching the full video can provide a deeper understanding of the issue and proposed solutions.

# Tags

#Homelessness #Veterans #SocialSafetyNet #Advocacy #CommunitySupport