# Bits

Beau says:

- Supreme Court non-ruling on Boise versus Martin decriminalized homelessness.
- Companies pushing to criminalize homelessness driven by desire to keep homeless away from storefronts.
- Companies now have to become humanitarians and contribute to communities they profit from.
- Need for homeless shelters and affordable housing will increase.
- Ruling currently applies on West Coast but expected to spread across the US.
- Predicted consequences: pushing homeless into areas covered by ruling, blaming politicians for increase in homelessness.
- Companies previously funding criminalization of homelessness may now support shelters and affordable housing.
- Technology exists to solve homelessness issue, lack of will is the obstacle.
- Advocates for housing, poverty, and homelessness urged to step up and take action.
- Courts now require alternatives before criminalizing homelessness.

# Quotes

- "In essence, it decriminalized homelessness."
- "They're going to have to chip into the communities they're profiting from."
- "We have the technology to solve this problem, we just need the will."
- "If you're an advocate for housing for all, if you are an advocate for people who are in poverty, if you're an advocate for the homeless, now is your time."
- "You can't criminalize this without an alternative."

# Oneliner

Supreme Court non-ruling decriminalizes homelessness, forcing companies to become humanitarians and invest in shelters and affordable housing nationwide.

# Audience

Advocates, Community Members

# On-the-ground actions from transcript

- Advocate for housing for all, poverty, and homeless (exemplified)
- Push companies to contribute to communities (implied)
- Support organizations providing shelters and affordable housing (implied)

# Whats missing in summary

The full transcript provides detailed insights on the legal implications of decriminalizing homelessness and the potential shift towards humanitarian efforts by companies and individuals.

# Tags

#Homelessness #Decriminalization #Humanitarianism #SupremeCourt #Advocacy #AffordableHousing