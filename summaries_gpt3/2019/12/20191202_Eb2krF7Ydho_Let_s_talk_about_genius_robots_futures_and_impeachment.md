# Bits

Beau says:

- Introduces the topic of discussing a genius.
- Talks about Isaac Asimov, a genius known for science fiction.
- Mentions that Asimov made predictions in 1964 about life in 2014, some of which have come true.
- Talks about Asimov's interest in history and politics, particularly Watergate.
- Criticizes the idea of excusing criminal actions based on historical precedents.
- Calls for holding leaders accountable.
- Mentions World AIDS Day and Asimov's death due to AIDS on April 6, 1992.
- Makes a prediction about the future if leaders are not held accountable.

# Quotes

- "He saw all this coming."
- "If we don't start holding those in the White House accountable 50 years from now, we won't be talking about impeachment because we won't have a presidency."

# Oneliner

Beau talks about Isaac Asimov's genius, his accurate predictions, the importance of holding leaders accountable, and makes a chilling prediction for the future.

# Audience

Citizens, voters, activists.

# On-the-ground actions from transcript

- Hold leaders accountable (implied).
- Advocate for transparency and accountability in government (suggested).

# Whats missing in summary

The emotional impact of discussing the genius of Isaac Asimov alongside the call for accountability in leadership.

# Tags

#Genius #IsaacAsimov #Predictions #Accountability #Leadership