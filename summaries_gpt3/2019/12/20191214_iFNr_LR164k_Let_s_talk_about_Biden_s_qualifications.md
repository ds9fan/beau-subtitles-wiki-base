# Bits

Beau says:

- Addressing Hunter Biden's qualifications and the repeated claims of his lack of qualifications for board positions.
- Mentioning George Bush's appointment of Hunter Biden to a board of directors and confirmation by the Senate.
- Noting Hunter Biden's past role on the Amtrak board of directors and his qualifications, including his education and work experience.
- Pointing out the false narrative perpetuated by some Republicans regarding Hunter Biden's qualifications.
- Exploring the idea of different standards being applied to Ukraine compared to the US government.
- Suggesting that the repeated falsehoods about Hunter Biden are believed by a base that does not fact-check.
- Contemplating whether there is a double standard in scrutinizing appointments based on political affiliations.
- Acknowledging the potential benefit of President Trump's administration in exposing lies and corruption in government.

# Quotes

- "He has the qualifications. They're right there."
- "At the end of the day, Trump in some ways has been a benefit because he's really laid bare the out-and-out lies and corruption in our government."
- "Maybe this type of corruption is just accepted here and the idea is that yeah, George Bush was trying to curry favor with Biden and you know, here it's okay, but over there, well, we have something different."

# Oneliner

Addressing the false claims around Hunter Biden's qualifications, Beau exposes the double standards and political narratives at play.

# Audience

Fact-checkers, political observers

# On-the-ground actions from transcript

- Fact-check repeated claims and correct misinformation (implied)

# Whats missing in summary

Full context and depth of analysis.

# Tags

#HunterBiden #Qualifications #PoliticalNarratives #DoubleStandards #Corruption