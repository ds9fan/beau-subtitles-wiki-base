# Bits

Beau says:

- Beau starts by discussing ICE-T's tweet about a Napoleon quote and a flaming queue image, which led to controversy.
- Politics is described as a hobby that some are deeply steeped in, mentioning a far-right theory related to the Trump administration.
- Politics should not just be a hobby but a powerful force that requires action to convert beliefs into tangible change.
- Beau encourages moving from politics as a hobby to a calling, suggesting taking tangible actions once a month to make a real-world impact.
- He stresses the importance of channeling passion into action to see significant changes in society, especially in the year 2020.

# Quotes

- "Politics isn't a hobby. Politics is power."
- "We stop letting politics be a hobby. We start letting it be a calling."
- "Stick to what you're truly passionate about and work towards bettering that particular topic."

# Oneliner

Beau says: Politics isn't a hobby; it's power. Let's turn passion into action for real change in 2020.

# Audience

Activists, Community Members

# On-the-ground actions from transcript

- Organize a tangible action once a month to affect real change (suggested)
- Channel passion into making a difference in a chosen topic (implied)

# Whats missing in summary

The full transcript provides detailed insights on the shift needed from viewing politics as a hobby to recognizing it as a powerful force that requires action for tangible change. Viewing the full transcript can provide a deeper understanding of the examples and perspectives shared by Beau.

# Tags

#Politics #Activism #Passion #Action #Change