# Bits

Beau says:

- Beau introduces a video of a young man struggling to use a can opener during Thanksgiving dinner, sparking a tutorial on can opener usage.
- The accompanying message suggests young people can't be trusted to vote because they struggle with basic tasks like using a can opener.
- Beau showcases various pop-top cans, indicating the prevalence of can designs that don't require a traditional can opener.
- He demonstrates how to use a can opener, expressing disbelief at the notion that young people can't figure it out.
- Beau criticizes the message that undermines young voters, pointing out the hypocrisy of older generations who mock youth for not knowing certain skills.
- Despite the ridicule, the young man in the video perseveres, in contrast to the elders who filmed and laughed at him.
- Beau questions why there's a tendency to blame young people for their supposed incompetence rather than acknowledging their strengths and resilience.
- He challenges the idea that older generations are inherently wiser and more capable of making sound decisions for the future.
- Beau suggests that young people, despite facing ridicule, will figure things out on their own while being undermined by those who are supposed to guide them.
- The message conveyed is to not underestimate young people's abilities based on isolated incidents of struggle or ignorance.

# Quotes

- "Can't trust young people to vote."
- "Don't listen to those young people who did not give up even though they didn't understand something."
- "Y'all should lay off the kids because at the end of the day whether or not they open that can doesn't matter they'll figure it out."
- "Most people today have an automatic can opener and even then most of the brands you don't need it anymore."
- "Let's listen to the people who will laugh and record a video because they don't understand what they did."

# Oneliner

Beau challenges the notion that young people can't be trusted to vote based on their ability to use a can opener, advocating for a shift in perspective towards youth resilience and capability.

# Audience

Young voters

# On-the-ground actions from transcript

- Support and encourage young voters (implied)
- Challenge stereotypes and biases against young people (implied)

# Whats missing in summary

Full understanding of Beau's passionate defense of young people's capabilities and resilience.

# Tags

#YouthEmpowerment #VotingRights #Stereotypes #GenerationalDivide #Resilience