# Bits

Beau says:

- Talks about the Electoral College and voting, prompted by his son's question about it.
- Shares a joke about John Quincy Adams approving an expedition to the center of the Earth.
- Explains his son's concerns about how to prevent unsuitable candidates from reaching the Oval Office.
- Mentions the issue of candidates campaigning only in major cities if the Electoral College is eliminated.
- Expresses disappointment at his son's elitist view associating better education with people in major cities.
- Comments on the importance of participation over geography in elections.
- Acknowledges flaws in the current system where votes matter more in swing states.
- Suggests that candidates currently win elections by dividing the country and playing one side against the other.
- Proposes a harder process for candidates to prove themselves and unite people before becoming president.
- Advocates for introducing ranked choice voting to address concerns about voting for third-party candidates.
- Emphasizes the need to set higher standards for presidential candidates to make America better.

# Quotes

- "It's not about education, it's about participation."
- "You want to sit in the most powerful chair in the world? Prove yourself out on the campaign trail."
- "We've had enough clowns stumble into that office and they've caused a lot of damage along the way."
- "Let's set the standards higher not lower."
- "I'm not certain of how to fix this problem but the one thing I am sure about is that the answer is not making it easier to sit in the most powerful office in the world."

# Oneliner

Beau delves into the Electoral College, advocating for higher standards and participation, suggesting a tougher process for presidential candidates.

# Audience

Voters, Political Activists

# On-the-ground actions from transcript

- Advocate for ranked choice voting to provide more options for voters (suggested).
- Participate in campaigns or initiatives that aim to reform the electoral process (implied).

# Whats missing in summary

Deeper insights into the potential consequences of maintaining or changing the Electoral College system.

# Tags

#ElectoralCollege #Voting #PresidentialCandidates #RankedChoiceVoting #PoliticalReform