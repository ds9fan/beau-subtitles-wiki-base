# Bits

Beau Gadd says:

- Beau organized a livestream on YouTube where donations were earmarked for Christmas presents for teens in a shelter.
- Five bags were prepared, each containing a tablet, a Netflix card, a tablet case, earbuds, and a board game.
- The tablets and Netflix cards were included to provide the teens with their own space, especially if they have younger siblings monopolizing the TV.
- The goal was quickly accomplished through collective effort; people donated money and time to make it happen.
- Additional items like cleaning supplies were also provided to the shelter, which are often overlooked but necessary for PTSD sufferers.
- Beau questioned why companies don't donate products like diapers and soap to domestic violence shelters to avoid triggering bad memories.
- He suggested that companies could benefit from such donations through tax deductions, advertising, and building brand loyalty.
- Beau emphasized the importance of individual actions contributing to collective goals, showcasing the power of community effort.
- He expressed gratitude towards the internet community for their swift action in addressing the initial problem and going beyond it.
- Beau reflected on the world we live in, where companies could do more to support shelters, but also praised the internet community for their quick and effective response.

# Quotes

- "Y'all did this in about an hour."
- "Seems like a win-win-win-win."
- "Shows the value of individual action in pursuit of a collective goal."

# Oneliner

Beau Gadd organized a livestream to provide Christmas presents for teens in a shelter, showcasing the power of collective action and urging companies to support domestic violence shelters.

# Audience

Community members, donors.

# On-the-ground actions from transcript

- Donate products like diapers and soap to domestic violence shelters for survivors (implied).

# Whats missing in summary

The full transcript provides a detailed account of how collective action can make a significant impact on community issues beyond initial goals.

# Tags

#CommunityAction #CollectiveEffort #SupportShelters #Donations #InternetCommunity