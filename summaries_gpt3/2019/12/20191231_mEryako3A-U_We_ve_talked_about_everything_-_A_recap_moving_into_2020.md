# Bits

Beau says:

- The president's decision to cut aid to Central American countries out of fear.
- Concern over the wall working both ways to confine Americans.
- Criticism of the younger generation's lack of exposure to accurate history.
- Pointing out the propaganda in history textbooks and the internet's raw information.
- Criticizing blind patriotism that supports harmful actions.
- Addressing the tactic of using fear to win elections and gain power.
- Challenging the unfounded fears and stereotypes around asylum seekers.
- Criticizing the dehumanization and mistreatment of migrants at the border.
- Addressing the issue of civilian gun violence and the role of governments.
- Arguing against the effectiveness of an assault weapons ban.
- Exploring the glorification of violence and its impact on society.
- Advocating for understanding the historical trauma of minority communities.
- Pointing out the importance of arming minority groups for protection.
- Addressing the fear and division around the changing demographics of America.
- Criticizing the lack of accountability and excessive use of force by law enforcement.
- Drawing parallels between legal actions and moral implications in history.

# Quotes

- "Guns don't kill people. Governments do."
- "We are a violent country. We always have been."
- "If you make your community strong enough, it doesn't matter who gets elected."

# Oneliner

Beau challenges blind patriotism, addresses gun violence, and advocates for community empowerment.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Contact your representative to address foreign policy impacting migration (implied).
- Advocate for understanding historical trauma in minority communities (implied).
- Strengthen your community to be self-sufficient regardless of political leadership (implied).

# Whats missing in summary

The full transcript provides a deep dive into societal issues, historical perspectives, and the importance of community resilience.

# Tags

#Patriotism #GunViolence #CommunityEmpowerment #HistoricalTrauma #Accountability