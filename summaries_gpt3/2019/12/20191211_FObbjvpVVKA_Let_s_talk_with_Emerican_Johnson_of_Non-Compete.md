# Bits

Beau says:

- American Johnson introduces himself as an anarcho-communist running the YouTube channel Noncompete, engaging with various leftist ideologies critical of capitalism.
- The channel, despite discussing complex topics like means of production, uses puppets and Legos to attract a younger audience, prompting concerns regarding COPPA guidelines.
- COPPA, aimed at protecting children's privacy online, fined YouTube $127 million for data harvesting from children, threatening content creators with penalties for not collecting data.
- American Johnson criticizes the FTC's COPPA enforcement, arguing that it contradicts the First Amendment by restricting free speech based on potential appeal to children.
- Concerns arise about potential FTC lawsuits affecting creators globally, like American Johnson's partner Luna, a Vietnamese citizen creating adult-focused content.
- American Johnson advocates for alternative social media platforms like Mastodon and PeerTube, promoting decentralized, community-owned networks as replacements for capitalist platforms.
- Encouraging viewers to join and create channels on these alternative platforms, American Johnson stresses the importance of supporting and building solidarity within the working class.
- In Vietnam, a culture of skepticism towards hierarchy and authority fosters local problem-solving and community-focused governance.
- American Johnson's call to action revolves around organizing, building solidarity, and fostering class consciousness as key solutions to societal challenges.
- He invites viewers to subscribe to his channel for updates on transitioning to PeerTube and participating in the YouTube walkout on the 10th, advocating for collective action and utilizing available tools to enact change.

# Quotes

- "The real solution to all of our problems is organizing together and building solidarity with the working class."
- "Unions have become a shadow of what they used to be. I think they're starting to come back, fortunately."
- "Let's use them, let's fight together, and let's change the world."

# Oneliner

Beau says the solution to societal problems lies in organizing together, promoting alternative social media platforms, and advocating for class consciousness and solidarity within the working class.

# Audience

Creators and activists

# On-the-ground actions from transcript

- Join Mastodon or PeerTube to support decentralized platforms (suggested)
- Start a channel on existing instances like PeerTube (suggested)
- Participate in the YouTube walkout on the 10th (implied)

# Whats missing in summary

The importance of collective action, utilizing alternative platforms, and fostering class consciousness are key takeaways from this transcript.