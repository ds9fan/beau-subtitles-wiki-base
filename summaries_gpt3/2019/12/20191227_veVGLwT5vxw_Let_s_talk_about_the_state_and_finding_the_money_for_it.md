# Bits

Beau says:

- Addresses the question of finding money for projects benefiting lower socioeconomic classes in the U.S.
- Points out the double standard in funding when it comes to projects that benefit wealthy classes vs. the general populace.
- Mentions Chuck DeVore's statement on progressive policies in California.
- Talks about the state's role in serving the people and the dangers of blending corporate interests with government.
- Explains the term fascism in the context of a state serving corporate interests.
- Notes the lack of funding for education, especially if opposed by wealthier classes.
- Gives an example of funding for a project benefiting the wealthy, the Slavery Abolition Act in the UK.
- Mentions the extensive loan taken out for reparations to slave owners, paid off in 2015.
- Emphasizes that policies benefiting the marginalized actually benefit society as a whole.
- Concludes by suggesting that failing to fund projects for the disadvantaged is equivalent to killing the state.

# Quotes

- "When you hear that, just assume it's something that rich people don't want to do."
- "Those social safety nets that keep the elderly, the disabled, and just the general poor, keep them alive. Well, those people, they are the state."
- "Failing to find the money for it is literally killing the state."

# Oneliner

Beau addresses the funding double standard, linking societal well-being to supporting the marginalized; failing to fund projects for the disadvantaged is akin to killing the state.

# Audience

Advocates for social justice

# On-the-ground actions from transcript

- Support and advocate for policies that benefit the lower socioeconomic classes (suggested)
- Educate others about the importance of funding social safety nets and infrastructure projects (suggested)

# Whats missing in summary

The full transcript provides historical context and examples to drive home the point about funding disparities and societal impacts.