# Bits

Beau says:

- December 6th should be a national holiday, marking the end of a dark American institution.
- The Emancipation Proclamation did not end the institution; it only freed slaves in conflict zones.
- Lincoln's strategic decision as commander in chief aimed to destabilize the South.
- The 13th Amendment was necessary to prevent the resurgence of the institution after the war.
- Lincoln pushed for the 13th Amendment, which was ratified on December 6th, 1865.
- Some states were slow to ratify the amendment, with Kentucky and Mississippi waiting until 1976 and 1995, respectively.
- The day marks the closing of a dark chapter in American history, yet it is not appropriately commemorated.
- It took another hundred years for further steps towards equality after the 13th Amendment.
- People who defended slavery until the end are remembered for their stance.
- Current events may lead to individuals being similarly remembered for supporting indefensible causes.

# Quotes

- "December 6th should be a national holiday, marking the end of a dark American institution."
- "The Emancipation Proclamation did not end the institution; it only freed slaves in conflict zones."
- "The day marks the closing of a dark chapter in American history, yet it is not appropriately commemorated."
- "People who defended slavery until the end are remembered for their stance."
- "Current events may lead to individuals being similarly remembered for supporting indefensible causes."

# Oneliner

December 6th should be a national holiday, marking the end of a dark American institution, but its significance is not appropriately commemorated.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Advocate for December 6th to be recognized as a national holiday (suggested)
- Educate others about the importance of the 13th Amendment and its significance in American history (suggested)

# Whats missing in summary

The full transcript delves into the historical significance of December 6th and the 13th Amendment, shedding light on overlooked aspects of American history.