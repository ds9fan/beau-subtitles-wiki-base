# Bits

Beau Gyan says:

- Criticizes the theatrics surrounding the impeachment process, labeling it as utter nonsense.
- Expresses concern over Republican representatives comparing impeachment to Pearl Harbor and the crucifixion of Christ.
- Points out the flawed argument of the lack of due process, especially when made by elected representatives.
- Explains the impeachment process, clarifying that the trial takes place in the Senate, not the House.
- Shares a historical anecdote about women gaining political power in Argonia in the late 1800s through a prank.
- Draws parallels between the current political situation and the waiting game for new evidence to sway opinions on impeachment.
- Emphasizes the importance of the Senate considering what the American people will accept before voting on impeachment.

# Quotes

- "It is utter nonsense."
- "Today I watched as Republican representatives compared the impeachment to Pearl Harbor and the crucifixion of Christ."
- "These are representatives on the House floor saying this, as if they don't actually understand how the process works."
- "The moral of this story is that right now, both Democrats and Republicans, they're waiting for that morning."
- "The Senate has to figure out what the American people will accept before they cast their vote."

# Oneliner

Beau Gyan criticizes the theatrics of impeachment, questions understanding of due process, and draws parallels to history and current political waiting games.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Understand the impeachment process and how it differs between the House and the Senate (implied)
- Stay informed about political proceedings and hold elected representatives accountable for their statements (implied)
- Advocate for transparency and accountability in political processes (implied)

# Whats missing in summary

A deeper insight into the historical context and implications of political theatrics in contemporary decision-making processes. 

# Tags

#Impeachment #PoliticalTheatrics #DueProcess #Accountability #HistoricalParallels