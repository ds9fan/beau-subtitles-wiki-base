# Bits

Beau says:

- Tells a true story about a trip where several planes had issues which leads to the point of needing to pay aircraft maintenance crews more.
- Mentions misleading vividness, where a detailed story doesn't actually support the claim being made.
- Explains logical fallacies including appealed hypocrisy, straw man, genetic fallacy, appeal to belief, appeal to tradition, appeal to authority, ad hominem, argument from ignorance, cherry-picking data, slippery slope, Nirvana Fallacy, moving the goalpost, false dilemma, fallacy of composition, divine fallacy, argument to moderation, and argument from fallacy.
- Provides examples for each logical fallacy and explains how they can lead to flawed arguments.
- Emphasizes the importance of understanding logical fallacies especially in the context of debates and arguments.
- Ends by suggesting that the information on logical fallacies will be valuable in the coming year.

# Quotes

- "We really need to pay our aircraft maintenance crews more."
- "Just because one of these fallacies was used does not mean the argument is wrong."
- "The middle ground is not always where you want to end up."
- "The world is a bad place sometimes."
- "It's just a thought y'all have a good Good night."

# Oneliner

Beau gives a crash course on logical fallacies through real-life stories, urging for better aircraft maintenance and critical thinking in debates.

# Audience

Debaters and critical thinkers.

# On-the-ground actions from transcript

- Learn about logical fallacies and how to identify them in arguments (suggested).
- Educate others on common logical fallacies and how they can weaken arguments (suggested).

# Whats missing in summary

In-depth examples and nuances of each logical fallacy discussed.

# Tags

#LogicalFallacies #Debates #CriticalThinking #AircraftMaintenance #Education