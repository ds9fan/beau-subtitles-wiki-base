# Bits

Beau says:

- Introduced the topic of straws and studies, addressing the controversy surrounding giving up plastic straws.
- Pointed out that most people misinterpret information and discussed the impact of 100 companies producing 71% of emissions.
- Raised concerns about how the U.S. might be defeated by a simple plastic straw due to lack of suitable alternatives.
- Proposed using plant-based, biodegradable straws as a viable solution, mentioning their existence and cost.
- Urged for individual responsibility and action in tackling environmental issues, contrasting popular misconceptions about emissions.
- Advocated for a cultural shift away from profit-driven motives and emphasized the power of individual action over waiting for governmental intervention.
- Stressed the importance of sending letters to major corporations like McDonald's to drive change and mentioned the effectiveness of boycotting.
- Encouraged viewers to believe in the impact of their voices and emphasized the significance of individual actions in inciting change.

# Quotes

- "We can't be defeated by a straw."
- "Your voice matters more than you think."
- "It requires individual action."

# Oneliner

Beau addresses misconceptions on emissions, advocates for plant-based alternatives, and stresses the power of individual action in driving environmental change.

# Audience

Environmental activists, conscious consumers

# On-the-ground actions from transcript

- Contact major corporations like McDonald's to advocate for sustainable alternatives (suggested)
- Boycott companies to drive change towards sustainability (exemplified)

# Whats missing in summary

The full transcript provides detailed examples and explanations on the impact of individual actions on environmental issues. Viewing the full transcript will offer a comprehensive understanding of Beau's arguments and suggestions.

# Tags

#Environment #PlasticStraws #IndividualAction #ClimateChange #CorporateResponsibility