# Bits

Aaron says:

- Aaron talks about his journey from being a conservative-leaning person to eventually becoming an anarcho-communist.
- He shares how his upbringing in a community with racist influences led him to adopt harmful beliefs and associate with neo-Nazis.
- Aaron recounts incidents of violence and vandalism he witnessed and participated in during his past.
- He describes his transition to rejecting his previous beliefs and embracing a more inclusive and empathetic worldview.
- Aaron credits skepticism and exposure to alternative arguments for helping him change his perspective.
- He explains his views on communism, socialism, and anarcho-communism, and distinguishes between them.
- Aaron criticizes the misrepresentation of socialism and communism in the United States.
- He provides insights into the differences between communism, socialism, social democracy, and democratic socialism.
- Aaron shares his support for worker-owned co-ops as a practical solution for societal issues under capitalism.
- He expresses the challenges of discussing complex global issues like protests in China due to propaganda and lack of in-depth knowledge.

# Quotes

- "Everybody is different, and they're all going to come to their positions in a different way."
- "It's about working people against the system that is corrupted and pushing down on all of us."
- "News crews have entire groups of people that deal with editing, they deal with sound, they deal with all of these sorts of things."
- "A lot of the military back coups done by the United States or done by any other major superpower always involves a certain amount of people on the ground that are actually angry and that actually have real concerns."
- "Putting physical bodies in physical spaces is the only way that we're going to actually see any sort of real reform, any sort of real change in the world."

# Oneliner

Aaron's journey from a conservative with racist influences to an anarcho-communist advocate, criticizing misrepresentation and advocating for direct action.

# Audience

Activists, Advocates

# On-the-ground actions from transcript

- Join protests and demonstrations to physically show support for social change (implied).
- Support worker-owned co-ops as a practical solution for societal issues under capitalism (exemplified).

# Whats missing in summary

The full transcript provides a detailed account of Aaron's personal growth from holding harmful beliefs to embracing an inclusive worldview, advocating for social change, and critiquing misrepresentations in political ideologies.

# Tags

#PersonalGrowth #AnarchoCommunist #SocialChange #DirectAction #InclusiveWorldview #Skepticism #WorkerOwnedCoops #Misrepresentation #Protests #Advocacy