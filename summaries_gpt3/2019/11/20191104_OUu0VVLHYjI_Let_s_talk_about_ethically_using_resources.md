# Bits

Beau says:

- Received a question from a person attending a university heavily funded by a nearby corporation.
- The person opposes the corporation but was given the chance to organize speaking engagements funded by them.
- Advises that as long as the funding doesn't affect the content or speakers, taking the money is acceptable.
- Mentions how the Department of Defense teaches soldiers to use resources left behind by the opposition.
- Argues that using the corporation's resources, even if it seems hypocritical, can be necessary in certain situations.
- States that altering society is like being behind enemy lines, and using the opposition's resources can be strategic.
- Emphasizes that taking the corporation's money for speakers who will speak out against them is not ethically wrong.
- Points out that large corporations fund events to generate goodwill and that drawing attention to their practices can be a win.
- Notes that in a society dominated by big corporations, it's challenging to completely avoid their influence.
- Concludes with the idea that ethical consumption is rare, but the focus should be on ensuring benefits to people outweigh those to the establishment.

# Quotes

- "When you're talking about altering society, we're behind enemy lines."
- "There are times when using the opposition's resources is your only option."
- "You have no ethical obligation to correct that mistake for them."
- "You just have to make sure that the benefit to the people outweighs the benefit to the establishment."
- "As long as the money isn't affecting the content that you're putting out, as long as it's not going to change your speaker list, as long as it's not going to alter what they're going to talk about, you know..."

# Oneliner

Beau advises using the opposition's resources strategically when behind enemy lines in altering society, focusing on benefiting people over establishments.

# Audience

University organizers

# On-the-ground actions from transcript

- Organize speaking engagements funded by corporations (suggested)
- Draw attention to questionable business practices of large companies (implied)

# Whats missing in summary

Beau's insights on navigating ethical dilemmas and strategic decision-making in a society influenced by large corporations.

# Tags

#University #Funding #Ethics #StrategicDecisions #CorporateInfluence