# Bits

Beau says:

- Introducing a Veterans Day special to talk about America's conflicts, including lesser-known ones.
- Listing major conflicts such as the Revolutionary War, War of 1812, Mexican-American War, and more.
- Noting that history often overlooks minor conflicts like the Cherokee American War, Shays Rebellion, and others.
- Explaining each conflict briefly and providing insights, such as tax revolts and internal wars.
- Mentioning wars like the Barbary Wars, Creek War, and the Seminole Wars that shaped American history.
- Describing events like the Texas Indian Wars involving land grabs and ethnic cleansing over 55 years.
- Sharing lesser-known conflicts like the Aegean Sea operations, Winnebago War, and Sumatran expeditions.
- Recounting sad events like the Black Hawk War and conflicts like the Pork and Beans War over borders.
- Touching on unique expeditions like the Ivory Coast expedition and discussing motivations behind conflicts.
- Concluding by reflecting on the vast array of conflicts often excluded from mainstream historical narratives.

# Quotes

- "History often overlooks minor conflicts."
- "Veterans Day is probably a good day to talk about all of these."
- "We don't like why the war started. We don't like the outcome."
- "Some of these weren't major, but some of them were huge."
- "It's just a thought."

# Oneliner

Beau introduces a Veterans Day special to shed light on America's lesser-known conflicts throughout history, reflecting on the vast array of wars often excluded from mainstream narratives.

# Audience

History enthusiasts, Veterans

# On-the-ground actions from transcript

- Research and learn about the lesser-known conflicts mentioned by Beau (implied)
- Take time to commemorate and acknowledge the sacrifices made in all conflicts, major and minor (implied)

# Whats missing in summary

Exploration of the impact of lesser-known conflicts on shaping American history.

# Tags

#VeteransDay #AmericanHistory #LesserKnownConflicts #Memorialize #Education