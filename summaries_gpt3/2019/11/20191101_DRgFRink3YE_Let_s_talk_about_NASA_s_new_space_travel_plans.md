# Bits

Beau says:

- NASA plans to put a crew of four on the moon for two weeks and launch an intentional interstellar space probe.
- Despite producing 150% of the food needed to feed everyone, a billion people go hungry nightly due to food distribution issues.
- Beau challenges the notion that there are logistical barriers to distributing food when transportation systems are already in place.
- He questions the lack of incentive to distribute food efficiently, pointing out the benefits of stability and reduced defense spending.
- Beau advocates for prioritizing basic needs like food over space exploration, suggesting it's a moral and monetary imperative.
- He stresses the importance of addressing hunger beyond America's borders and treating all people equally.

# Quotes

- "We're on the cusp of a new adventure in humanity, conquering the stars."
- "We produce 150% of what it would take to feed everybody."
- "NASA can plan to put a crew of four on the moon for two weeks and plan to launch an interstellar probe, the rest of us, we can figure out how to move some food."

# Oneliner

NASA plans space exploration while a billion go hungry; Beau questions food distribution priorities. 

# Audience

Global citizens

# On-the-ground actions from transcript

- Coordinate transportation networks to distribute excess food efficiently (implied)
- Advocate for stable governments through food aid to reduce conflict and defense spending (implied)

# Whats missing in summary

The full transcript provides an in-depth perspective on the contrast between space exploration ambitions and global hunger, urging a reevaluation of priorities.

# Tags

#NASA #SpaceExploration #FoodDistribution #GlobalHunger #SocialResponsibility