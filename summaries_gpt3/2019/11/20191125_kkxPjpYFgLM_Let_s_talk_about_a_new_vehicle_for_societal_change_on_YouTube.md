# Bits

Beau says:

- The FTC is enforcing a 1998 law on childhood privacy, affecting kids' content on YouTube.
- Creators of kids' content may see a drop in income due to the removal of targeted ads.
- Kid-directed content involves high production costs with visual effects, sound effects, travel, location shooting, and toy unboxing.
- Major kid content creators have the potential to influence change by educating their viewers about laws.
- There is a sense of learned helplessness among people when it comes to government actions.
- Despite having millions of subscribers, major creators may not utilize their influence for change.
- Starting a kids' content YouTube channel with low overhead costs could be profitable as YouTube is profit-driven.
- Creating content like story time teaching children with morals can be financially viable with minimal overhead.
- Beau suggests creating content that is more educational like Mr. Rogers instead of traditional cartoons.
- Using a YouTube channel for societal change is emphasized by Beau, encouraging those interested to get involved.

# Quotes

- "They can influence change, but they're probably not going to."
- "If you have that appearance or that voice that would appeal to children, you want to get involved, this is something you can do."
- "The driver's door just opened. You can really get somewhere with this."
- "You can literally raise the children that don't have the influence from the parents they need."
- "I think you can create a better world with a camera and a YouTube channel."

# Oneliner

The FTC's enforcement of a law on childhood privacy affects kids' content on YouTube, urging major creators to utilize their influence for societal change.

# Audience

Content Creators

# On-the-ground actions from transcript

- Start a kids' content YouTube channel with low overhead costs to adapt to the changes in kid-directed content (suggested)
- Create educational content like story time teaching children with morals to make an impact (implied)
- Use your appearance and voice to appeal to children and get involved in creating content for societal change (implied)

# Whats missing in summary

The full transcript provides detailed insights into the impact of FTC regulations on kids' content creators and the potential for societal change through educational YouTube channels.

# Tags

#YouTube #FTC #KidsContent #SocietalChange #ContentCreators