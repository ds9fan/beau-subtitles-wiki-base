# Bits

Beau says:

- Talks about a study from Carnegie Mellon University where people played a game against a robot named Pepper.
- Some players were encouraged by the robot while others were insulted.
- Players who were encouraged performed better, while those who were insulted played worse.
- Even though the players knew it was a robot and programmed to act that way, it affected their emotions and gameplay.
- AI researchers find this interaction between robots and humans extremely interesting.
- Beau raises the point that if robots can impact adults like this, the power of words on children must be considered.
- He mentions the influence of internet comments and how they can affect individuals more than they realize.
- Beau urges viewers to understand the impact their comments can have on others, as they are real people reading them.
- He stresses the importance of being mindful of the words we put out into the world.
- Beau concludes by reminding viewers that their words, even seemingly harmless ones, can have a significant effect on others.

# Quotes

- "Words have a lot of power."
- "You have a choice in what you put out into the world."
- "Your words will have an effect on those people that read them."
- "Even if you think it's just a harmless comment..."
- "The reality is that's a real person."

# Oneliner

Beau from Carnegie Mellon shows how a robot's words affect gameplay, urging us to understand the power of our own words, especially towards children and online.

# Audience

Online users

# On-the-ground actions from transcript

- Choose words carefully (implied)
- Be mindful of comments' impact (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the study on robot-human interaction and the implications for communication in various contexts.