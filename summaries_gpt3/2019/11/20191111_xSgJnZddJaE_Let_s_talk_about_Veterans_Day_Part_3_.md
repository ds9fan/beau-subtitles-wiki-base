# Bits

Beau says:

- Outlines various instances where veterans were turned into combat veterans throughout history.
- Mentions interventions and wars from early 1900s to present, including conflicts in Nicaragua, Haiti, Dominican Republic, Russian Civil War, World Wars, Korean War, Vietnam War, Bay of Pigs, and more.
- Talks about the U.S. involvement in different civil wars and conflicts globally, showcasing a pattern of intervention.
- Criticizes the continuous cycle of turning veterans into combat veterans and the impact it has had.
- Points out the importance of recognizing and reflecting on the history of U.S. military interventions.
- Suggests that the best way to honor Veterans Day is to stop turning veterans into combat veterans.

# Quotes

- "The best thing we can do for Veterans Day is to stop turning veterans into combat veterans."
- "This isn't even all of it."
- "Maybe go back through this list and try to figure out how many years the U.S. was at peace out of its entire existence."
- "Y'all have a good night."

# Oneliner

Beau outlines numerous instances of U.S. military interventions, urging to stop turning veterans into combat veterans and reflecting on the country's history of conflict.

# Audience

History enthusiasts, anti-war advocates

# On-the-ground actions from transcript

- Research and understand the history of U.S. military interventions (suggested)
- Advocate for policies that prioritize peace over war (implied)

# Whats missing in summary

The emotional impact of continuous warfare on veterans and the importance of acknowledging past mistakes to prevent future conflicts.

# Tags

#VeteransDay #USMilitaryInterventions #AntiWar #History #PeaceBuilding