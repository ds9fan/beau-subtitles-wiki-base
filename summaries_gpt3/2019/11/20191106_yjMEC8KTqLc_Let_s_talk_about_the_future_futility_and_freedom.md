# Bits

Beau says:

- Talks about the essence of conservatism, freedom, and futility, reflecting on the scary future and the theme of the universe being wild and free.
- Describes how the future should bring more freedom for more people, encouraging innovation and letting go of old ideas to become better.
- Compares the progress of freedom to waves on a beach, with each wave reaching a bit further ashore.
- Mentions how some people find the advancing freedom scary as it challenges their beliefs, leading them to reach back to preserve dated ideas.
- Shares the story of Americana City, where people moved to Brazil to preserve their culture but ultimately assimilated into the new society.
- Examines the Make America Great Again voter demographic, born in 1966 or before, seeking clarity and purpose in a changing world.
- Points out how the older generation's upbringing during segregation influences their views, even if they don't overtly hold those ideas anymore.
- Talks about the fear of the unknown and reluctance to change among certain groups, contrasting them with the younger, more progressive minds joining the electorate.
- Emphasizes the inevitable progression towards a future where all individuals are free and equal, predicting the fading of old ideas and institutions.
- Concludes by acknowledging the unstoppable nature of progress towards a more inclusive and equitable society, despite resistance from some individuals.

# Quotes

- "The future is more freedom for more people."
- "That wild and free future cannot be stopped."
- "The future is a future where women, racial minorities, gender and sexual minorities are free and equal."

# Oneliner

Beau dives into conservatism, freedom, and the pushback against progress, envisioning a future of increasing freedom and equality despite resistance from some.

# Audience

Progressive individuals

# On-the-ground actions from transcript

- Connect with younger individuals to encourage progressive thinking and inclusivity (implied)
- Challenge outdated ideas within communities and advocate for equality (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of the resistance to change and the inevitability of progress towards a more inclusive society.