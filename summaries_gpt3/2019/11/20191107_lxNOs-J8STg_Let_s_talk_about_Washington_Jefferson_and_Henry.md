# Bits

Beau says:

- November is National American History and Founders' Month proclaimed by Donald J. Trump, which initially sparked mixed emotions but Beau is warming up to the idea.
- Beau stresses the importance of understanding American history and the founding fathers to debunk the mythology surrounding them.
- George Washington, despite his military leadership, had a dubious past including surrendering due to not knowing French and allegedly committing war crimes.
- George Washington's treatment of slaves included brutalities and separating families by selling them to traders from the West Indies.
- Thomas Jefferson engaged in a relationship with one of his slaves, likely starting when she was 14 and he was in his 40s, bearing children he raised as slaves.
- Patrick Henry, known for "give me liberty or give me death," imprisoned his wife in a cellar for four years, leading to her believed suicide due to postpartum depression.
- The popular stories about the founding fathers, like George Washington's wooden teeth, are mostly myths; his teeth were reportedly from slaves.
- Beau points out the significant historical inaccuracies and the need to also recognize Native American Heritage Month.


# Quotes

- "Take some of the mythology out of it."
- "Those probably aren't the stories you know about the Founding Fathers."
- "You might be surprised."
- "There's a lot of mythology."
- "Just a thought, y'all have a good night."

# Oneliner

November is National American History and Founders' Month; Beau sheds light on the dark truths behind the popular myths surrounding George Washington, Thomas Jefferson, and Patrick Henry, urging a reevaluation of history and recognition of Native American Heritage Month.

# Audience

History enthusiasts, truth seekers

# On-the-ground actions from transcript

- Research and learn about the real stories of the founding fathers (suggested)
- Educate others about the dark truths of American history (implied)

# Whats missing in summary

The full transcript provides a deep dive into the dark realities of the founding fathers, shedding light on often overlooked or whitewashed aspects of American history.

# Tags

#AmericanHistory #FoundingFathers #Mythology #Slavery #NativeAmericanHeritage