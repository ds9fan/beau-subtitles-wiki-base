# Bits

Beau says:

- Discussed the reactions after seeing something trending on Twitter.
- Mentions the common responses to tragic events: better security, arming teachers, and gun control.
- Critiques thoughts and prayers as not being a solution.
- Explores the impracticality of extreme security measures at schools.
- Raises the issue of the underlying problem being kids wanting to harm others.
- Points out the cycle of violence being perpetuated by societal norms.
- Calls for comments on solutions that do not involve additional violence.
- Urges for a cultural shift away from glorifying violence.
- Expresses the need for a systemic change rather than relying on thoughts and prayers.
- Shares the perspective of a student feeling unprepared despite drills in schools.

# Quotes

- "We have a systemic problem."
- "Our high school students are talking like rangers after the first time they were in combat."

# Oneliner

Beau dives into the societal glorification of violence and the urgent need for non-violent solutions to address the underlying issues leading to tragic events.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Share and create solutions that do not involve additional violence (suggested).
- Advocate for a cultural shift away from glorifying violence (implied).

# Whats missing in summary

The emotional impact and urgency of addressing the root causes of violence in society.

# Tags

#Violence #CulturalChange #Solutions #CommunityPolicing #GunControl