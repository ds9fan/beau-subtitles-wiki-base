# Bits

Beau says:

- Secretary of Navy's resignation due to discrepancy between public statements and private actions.
- Administration prioritizes truthfulness and transparency, sarcastically mentioning their stellar reputation.
- Secretary of Navy's refusal to obey an order believed to violate his oath to support and defend the Constitution.
- Admirals' concern about good order and discipline after the President interfered in military proceedings.
- President's tweet expressing dissatisfaction with Navy Seal Eddie Gallagher's trial outcome.
- Gallagher was acquitted on major charges but found to have posed with an enemy prisoner's corpse.
- Beau questions the seriousness of posing with a corpse, explaining its impact on propaganda for the opposition.
- Beau stresses the repercussions of such actions on local populace and counterinsurgency efforts.
- Concerns raised about the President's orders being delivered via tweet to a wide audience.
- Beau criticizes the lowering of moral standards and its implications on military operations.
- Beau underlines the psychological impact of such actions on public perception and counterinsurgency success.
- The lasting effects of setting a standard where posing with a corpse is considered acceptable.
- Beau points out the significant damage done to the country's moral compass by such actions.

# Quotes

- "One guy has done more damage to this country's moral compass than any other president in history."
- "The standard has been set. This isn't a big deal."
- "You better fight harder."
- "Changed the way you looked at things."
- "We lowered the bar."

# Oneliner

Secretary of Navy resigns, President interferes in military proceedings, posing with a corpse deemed acceptable - damaging moral compass and counterinsurgency efforts.

# Audience

Military personnel, policymakers, activists

# On-the-ground actions from transcript

- Challenge and question actions that compromise moral standards (exemplified).
- Advocate for transparent and ethical leadership in the military (exemplified).
- Educate others on the psychological impact of actions on public perception (implied).

# Whats missing in summary

The emotional weight and depth of analysis present in the full transcript.

# Tags

#SecretaryOfNavy #MilitaryLeadership #MoralCompass #Transparency #Counterinsurgency