# Bits

Beau says:

- Two men approach a 22-year-old engineering student, seeking help to build a tunnel to the other side of the border.
- The student, a refugee himself, agrees and they locate a factory to conceal the entrance.
- As they dig towards their destination, realizing the need for more help, they recruit additional engineering students.
- NBC News funds the tunnel construction after the group faces challenges and realizes the need for better tools.
- A burst pipe floods the tunnel, causing a setback, but they manage to fix it without getting caught.
- Discovering another tunnel nearby, they offer their assistance and help complete it successfully.
- Despite the first tunnel's failure and arrest of others, the engineering student and his friends press on with their mission.
- They redirect their tunnel to a different building due to increased border patrols, aiming to help refugees escape safely.
- Using coded signals in bars, they coordinate the rescue operation, facing risks and uncertainty on both sides.
- Refugees emerge from the tunnel, including a woman, a baby, and many others seeking freedom in West Berlin.

# Quotes

- "It's amazing how your views on these things are just based on which side of the wall you're on and what decade you're at."
- "When Kennedy saw the footage, he cried."
- "Defeated that wall."
- "This is a good story to tell."
- "If you want more information on it, you can find it under Tunnel 29."

# Oneliner

Two men recruit an engineering student to build a tunnel to freedom, facing challenges and risks while aiding refugees on both sides of the border.

# Audience

Activists for refugee rights

# On-the-ground actions from transcript

- Support refugee organizations in your community by volunteering or donating (implied)
- Advocate for humane immigration policies and support refugees seeking safety (implied)

# Whats missing in summary

The emotional impact of the refugees' journey towards freedom and the resilience shown in the face of adversity.

# Tags

#RefugeeRights #Tunnel29 #Freedom #Resilience #CommunityAid