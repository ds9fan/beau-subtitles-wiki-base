# Bits

Beau says:

- Canadian journalism students and others conducted a massive investigation revealing a national water crisis worse than Flint, with hundreds of thousands unknowingly drinking contaminated water with high lead levels.
- Lead levels in several Canadian cities were consistently higher than Flint's, with 33% of tested water sources exceeding the Canadian guideline of 5 parts per billion.
- There is no safe level of lead, as even a single glass of highly contaminated water can lead to hospitalization, especially harmful to children by lowering IQ, causing focus problems, and damaging the brain and kidneys.
- Schools in Canada are not regularly tested for lead, and even if high levels are found, there is no obligation to inform anyone.
- Approximately half a million service lines in Canada are lead, affecting a significant number of people due to lead pipes corroding and contaminating the water.
- Aging infrastructure in both Canada and the United States is a key issue, necessitating costly repairs to prevent further lead contamination.
- Some Canadian jurisdictions like Quebec are taking steps to address the crisis, with plans to replace both public and private lead pipes.
- Beau questions the priorities of developed countries like Canada and the U.S., urging citizens to care about how government funds are allocated to prevent such crises.
- Beau advocates for prioritizing fixing infrastructure issues like lead pipes over military spending, questioning the government's ability to provide basic services like clean water.

# Quotes

- "There is no safe level of lead. It's all bad."
- "If a government cannot provide the most basic of services, water. If a jurisdiction can't provide that, if a government can't provide that, what good is it?"
- "I personally rather pay to have all of the pipes in Flint fixed rather than send some more bombs overseas."

# Oneliner

Canadian water crisis reveals widespread lead contamination worse than Flint, urging citizens to hold officials accountable and prioritize fixing infrastructure for clean water.

# Audience

Canadians, Americans

# On-the-ground actions from transcript

- Hold local officials accountable for ensuring clean water (implied)
- Advocate for regular testing of schools for lead contamination (implied)
- Support initiatives to replace lead pipes in affected areas (implied)

# Whats missing in summary

Importance of citizen involvement and oversight in ensuring access to clean and safe drinking water.

# Tags

#WaterCrisis #LeadContamination #Infrastructure #GovernmentAccountability #PublicHealth