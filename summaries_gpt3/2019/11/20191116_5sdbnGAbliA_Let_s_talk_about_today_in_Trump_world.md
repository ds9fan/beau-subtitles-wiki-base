# Bits

Beau says:

- Recapping the noteworthy events from Trump world, including a probe into Giuliani's potential personal benefit from a Ukrainian energy company.
- Giuliani questioned about whether Trump might betray him in the impeachment hearing and his cryptic reference to having "very, very good insurance."
- President Trump's intimidating tweet about witness Yovanovitch, adding to his list of impeachable offenses.
- Witness David Holmes testified about a call where Trump specifically inquired about the Biden investigation, eliminating plausible deniability.
- Roger Stone convicted on all counts for obstructing the Russia Trump investigation, signaling potential future criminal prosecutions post-administration.
- Republicans are warned about their defense of Trump, as future administrations will uncover the truth, tarnishing their legacies.
- Criticizing Trump's self-absorbed nature and lack of concern for the country's well-being.
- The episode ends with a foreboding message about the consequences for those blindly supporting Trump.

# Quotes

- "You're throwing away your legacies to protect somebody who is not going to protect you."
- "Certainly painting Trump as a very self-absorbed person who does not care about the United States."
- "Those in Congress, in the Senate, that are defending him will forever be remembered, for the rest of their very short political careers, as idiots who got conned by a real estate mogul."

# Oneliner

Beau recaps notable events from Trump world, warning Republicans of their legacy's impending tarnish for blindly supporting Trump's actions.

# Audience

Political observers, Republicans

# On-the-ground actions from transcript

- Hold elected officials accountable for their support of actions that harm the country (implied).
- Prepare for potential criminal prosecutions post-administration by staying informed and engaged (implied).

# Whats missing in summary

Insights on the potential long-term consequences for those involved in the controversies surrounding Trump.

# Tags

#Trump #Impeachment #Republican #Legacy #Accountability