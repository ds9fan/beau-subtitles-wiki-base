# Bits

Beau says:

- General Miles sent a telegram to D.C. stating that the "difficult Indian problem" could not be solved without fulfilling treaty obligations.
- Native Americans had signed away valuable portions of reservations to white people without receiving adequate support as promised.
- Leading up to the Wounded Knee massacre, the US government continued to seize Native land and ignore treaty agreements.
- The Ghost Dance movement, based on a vision of a native Jesus, scared settlers, leading to conflict with authorities.
- Sitting Bull was targeted for arrest, leading to a violent confrontation where he was killed.
- Natives fleeing reprisals were surrounded by the Seventh Cavalry, resulting in a massacre at Wounded Knee.
- The Army awarded 20 Medals of Honor for the massacre, indicating a cover-up to justify the excessive force used.
- The Wounded Knee massacre exemplifies ignorance, arrogance, supremacy, and fear of the other in US history and foreign policy.
- Beau suggests reflecting on Native American heritage and how it relates to modern-day foreign policy and exploitation.
- Native American history serves as a vital lesson on the consequences of colonialism, militarism, and exploitation.

# Quotes

- "It's Native American Heritage Month. But their heritage is our heritage."
- "Ignorance, arrogance, supremacy, fear of the other. It's what caused it."
- "We could learn a lot from our Native American heritage."

# Oneliner

General Miles' telegram reveals unfulfilled treaty obligations leading to the Wounded Knee massacre, showcasing American history's mistreatment of Native Americans and serving as a lesson for modern foreign policy.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Study Native American history and share the truth with others (suggested)
- Support organizations advocating for Native American rights and reparations (exemplified)

# Whats missing in summary

Importance of acknowledging and learning from the dark chapters of history to create a more just future.

# Tags

#NativeAmerican #History #WoundedKnee #Colonialism #ForeignPolicy