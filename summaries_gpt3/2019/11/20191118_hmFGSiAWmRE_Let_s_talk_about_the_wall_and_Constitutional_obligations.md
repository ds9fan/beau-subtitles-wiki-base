# Bits

Beau says:

- Down on the Mexican border, searching for Trump's wall, Beau gets lost and may have ended up in Mexico.
- Beau questions the lack of progress on the border wall, noting that not a single new mile has been built in three years.
- He criticizes the government for disrupting processes and seizing land without fair compensation under the Declaration and Taking Act.
- Beau points out the failure of Mexico to pay for the wall, despite promises.
- He addresses public opinion on the Ukraine call, suggesting that regardless of intentions, a crime may have been committed.
- Beau argues that the Constitution mandates the Senate to remove the president if he committed crimes, including bribery and extortion.
- The lack of progress on the wall, military decisions in Syria, and trade wars are factors causing even his base to turn against the president.
- Beau questions Trump's ability to handle the job as president, mentioning his focus on branding and economic issues.
- Despite some successes in avoiding recession, Beau believes all evidence points to Trump violating the law and suggests it's the Senate's obligation to remove him.

# Quotes

- "There was already a wall there. Where's the new wall?"
- "It is now the Senate's constitutional obligation to remove the president."
- "His base is shrinking rapidly because he keeps insulting them."
- "All evidence shows that he violated the law more than likely."
- "It's more than branding and that's what Trump is good at, branding."

# Oneliner

Beau on lack of border wall progress, potential crimes, and constitutional duty to remove the president amidst turning base.

# Audience

Voters, concerned citizens

# On-the-ground actions from transcript

- Contact your senators to express your views on potential violations of the law by the president (suggested).
- Join or support organizations advocating for government accountability (exemplified).
- Organize community dialogues on constitutional obligations and holding leaders accountable (implied).

# Whats missing in summary

Deeper insights into the impact of political decisions and messaging on public perception and support.

# Tags

#BorderWall #GovernmentAccountability #ConstitutionalObligations #PoliticalLeadership #PublicOpinion