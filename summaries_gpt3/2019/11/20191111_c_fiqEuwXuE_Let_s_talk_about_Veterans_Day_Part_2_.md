# Bits

Beau says:

- Veterans Day special covering America's conflicts, not just major ones in history books.
- Apache Wars lasted from 1852 to 1924, consisted of constant skirmishes.
- US military deployed 5,000 men to deal with Geronimo and his 30 followers.
- Bleeding Kansas (1854-186) determined if Kansas was to be a slave state.
- Puget Sound War (1855-1856) part of the Akimlu War, notable for a chief's execution.
- First Fiji expedition in 1855 due to a civil war incident involving an American businessman.
- Rogue River Wars (1855-1856) in Oregon marked another land grab.
- Various conflicts like Third Seminole War, Yakima War, Second Opium War, Utah War, and Navajo Wars involved land grabbing.
- Second Fiji expedition in 1859 rumored to involve two Americans being eaten by Natives.
- Dakota War, Colorado War, Shimonoseki campaign, Snake War, Powder River War, and Red Cloud's War were all land grabs.
- The Great Sioux War (1876), Buffalo Hunters War, Pierce War, Bannock War, Cheyenne War, Sheep Eaters War, and White River War were all about seizing land.
- Pine Ridge Campaign (1890-1891) led to the Wounded Knee massacre.
- Spanish-American War (1898) triggered by the USS Maine incident.
- Philippine-American War (1899-1902) followed the Spanish-American War.
- Boxer Rebellion (1899-1902) and Crazy Snake Rebellion (1909) occurred due to various reasons such as stolen meat.
- Border War (1910-1916) involving Pancho Villa, and the armed uprising of independence of color in Cuba (1912).

# Quotes

- "We're always at war. We just don't always talk about them."
- "Looking back at it through clearer eyes, a lot of this wasn't just."

# Oneliner

Beau covers America's lesser-known conflicts, exposing a history of constant war and injustice.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Research and learn more about the lesser-known conflicts in American history (suggested)
- Advocate for educational reforms to include a comprehensive history curriculum (suggested)

# Whats missing in summary

The emotional impact of realizing the continuous warfare and historical injustices in American history.

# Tags

#AmericanHistory #VeteransDay #Injustice #LandGrab #Activism