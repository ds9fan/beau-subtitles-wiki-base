# Bits

Beau says:

- A single mother's message and a sports player taking his boys to the range sparked the topic.
- Criticism arose regarding the reasons for taking the boys, things said, and teaching techniques.
- Despite the criticism, bonding experiences between parents and children were brought up.
- Beau shared his childhood experience of learning about military tactics and small arms from his father.
- He chose not to teach his son about violence but instead sent him elsewhere for unarmed defense lessons.
- Beau values memories of childhood involving cooperation, creation, and building over destruction.
- He emphasized the importance of teaching children cooperation and creation instead of violence.
- Beau discussed the quote about preparing for the next war rather than focusing on the last war.
- He stressed the need for future generations to study fields like mathematics, philosophy, and engineering.
- Beau emphasized that the next war for humanity's survival will require engineers, architects, and philosophers.

# Quotes

- "I don't want to bond over violence if it's really bonding."
- "Cooperation, creation, building, that's what they're going to have to struggle to learn."
- "The next war for humanity's survival is not going to be fought by soldiers or sailors and pilots."

# Oneliner

A reflection on parenting, violence, and preparing for the future in a changing world.

# Audience

Parents, educators, community leaders

# On-the-ground actions from transcript

- Teach children cooperation and creation through hands-on activities (exemplified)
- Encourage children to study fields like mathematics, philosophy, engineering (exemplified)

# Whats missing in summary

The importance of guiding children towards cooperation and creation rather than violence.

# Tags

#Parenting #Violence #Childhood #Education #FuturePreparation