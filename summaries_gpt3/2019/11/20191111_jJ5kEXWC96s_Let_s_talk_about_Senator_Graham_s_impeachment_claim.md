# Bits

Beau says:

- Senator Graham threatened that impeachment is dead on arrival if the whistleblower is not named, attempting to bully the House into revealing the whistleblower's identity.
- The Senator's demand is baseless, as the House has sole power over impeachment and the Senate's duty is to conduct a trial, not dismiss impeachment without due process.
- Beau criticizes Senator Graham's attempt to bully and manipulate the process by demanding the whistleblower's identity, likening it to the behavior that led to the President's impeachment.
- He questions the Senator's logic by sarcastically suggesting that Crime Stoppers and anonymous tip lines should be banned if anonymity invalidates due process.
- The importance of whistleblowers remaining anonymous is emphasized, as they fear being attacked by individuals who prioritize party loyalty over the country's well-being.
- Senator Graham's actions are seen as undermining the Constitution and the rule of law to protect a guilty party, raising concerns about the Senate's trustworthiness and integrity.
- Beau accuses the GOP of engaging in corrupt behavior by attempting to overlook wrongdoing in favor of reelection prospects, rather than upholding their oath and duty to uphold the Constitution.
- He argues that failing to uphold their oath and attempting to undermine the Constitution will have more significant negative consequences for reelection than acknowledging any illegal actions committed by the President.


# Quotes

- "You're trying to bully… Kind of sounds like what the president's getting impeached for, doesn't it?"
- "Whistleblowers and people who like to remain anonymous, do so because they don't want to be smeared by an attorney or some politician who has put his party over his country."
- "It certainly appears that Senator Graham is willing to undermine the Constitution and the rule of law to protect somebody he knows is guilty."
- "Failing to uphold your oath and your duty and attempting to undermine the Constitution through some rhetoric that we all see through, that's going to hurt your re-election chances a whole lot more than simply saying, yeah, Trump broke the law."

# Oneliner

Senator Graham's attempt to bully the House into revealing the whistleblower's identity undermines the Constitution and raises concerns about Senate integrity.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Contact your representatives to express support for protecting whistleblowers and upholding due process (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis and criticism of Senator Graham's actions regarding the impeachment process, showcasing the importance of upholding constitutional values and accountability in government.

# Tags

#SenatorGraham #Whistleblower #Impeachment #Constitution #Corruption