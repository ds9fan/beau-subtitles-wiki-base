# Bits

Beau says:

- Reverse format for this historical event.
- Hanging out with veterans and ex-contractors.
- Watching Trump's speech to troops in Afghanistan.
- Trump falsely claiming they kept oil in Syria.
- Beau not fact-checking, focusing on civilian control.
- Emphasizing civilian leadership over the military.
- President as a civilian, not a rank like general.
- Concept of civilian control of the military.
- President's power for strategic decisions towards peace.
- Importance of preventing military dictatorship.
- Historic taboo on presidents saluting troops.
- Reagan starting the tradition of saluting troops.
- Concerns about blurring the line between civilian and military leadership.
- Keeping Syria's oil as a potential war crime.
- Importance of Pentagon's integrity in addressing such issues.

# Quotes

- "The reason it exists is twofold."
- "He's a civilian, he's not a general, period, full stop."
- "When the head of state starts to be seen as the general, the next thing that happens is war crimes."
- "Keeping Syria's oil, that's a war crime."
- "It's illegal under the Geneva Conventions and the U.S. War Crimes Act."

# Oneliner

Beau explains the importance of civilian control over the military, cautioning against blurring the line and potential war crimes.

# Audience

Citizens, policymakers, military personnel.

# On-the-ground actions from transcript

- Ensure awareness and understanding of the concept of civilian control over the military (suggested).
- Advocate for integrity within the Pentagon to uphold international and U.S. laws regarding war crimes (exemplified).

# Whats missing in summary

The full transcript delves into the historical significance of civilian leadership over the military and the potential consequences of blurring this line, urging vigilance against actions that could lead to war crimes.

# Tags

#CivilianControl #WarCrimes #MilitaryLeadership #Ethics #HistoricalSignificance