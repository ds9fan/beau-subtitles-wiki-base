# Bits

Beau says:

- Debunks the widely held belief among older Republicans that Donald Trump is a defender of the Second Amendment.
- Compares Trump's gun control actions to Obama's, revealing surprising facts.
- Trump supported banning assault weapons, longer waiting periods, banning suppressors, universal background checks, and increased federal firearm prosecutions.
- Trump set a record for federal firearm prosecutions, surpassing Obama and even Bush.
- Obama received an F rating from the Brady campaign, indicating his stance on gun control.
- Trump continued and expanded upon Obama's actions, releasing military surplus weapons and allowing guns in national parks.
- Contrasts the perception of Obama as a gun grabber with his actual actions in expanding gun rights.
- Criticizes Trump's stance on guns, quoting his willingness to "take the guns first, worry about due process second."
- Draws parallels between Trump's approach to guns and his views on impeachment.
- Concludes by challenging the perception of Trump as a defender of gun rights.

# Quotes

- "President Obama expanded gun rights. He wasn't a gun grabber."
- "Trump, on the other hand, well, take the guns first. Worry about due process second."

# Oneliner

Debunks the myth that Trump is a Second Amendment defender, contrasting his actions with Obama's and revealing surprising truths about gun control.

# Audience

Second Amendment supporters

# On-the-ground actions from transcript

- Share this information with fellow Republicans to challenge misconceptions about Trump's stance on the Second Amendment. (implied)

# Whats missing in summary

The full transcript provides an in-depth analysis of Trump and Obama's actions concerning gun control, urging viewers to re-evaluate commonly held beliefs.

# Tags

#SecondAmendment #GunControl #Trump #Obama #Republican