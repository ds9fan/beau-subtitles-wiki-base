# Bits

Beau says:

- Two individuals stumbled upon a Viking treasure stash of coins and jewelry in 2015, with coins dating back 1100 years.
- The stash was worth roughly 3.8 million dollars in the US.
- Instead of alerting authorities, they decided to keep and eventually sell the treasure on the open market.
- Only 31 out of the 300 coins have been recovered, some of which have significant historical value.
- Some coins reveal an alliance between Alfred the Great of Wessex and Caelwulf II of Mercia, challenging traditional historical narratives.
- The coins suggest a different story where Caelwulf II was actively allied with Alfred the Great, contrary to the usual portrayal of him as a puppet of the Vikings.
- The disappearance of Caelwulf II from historical records around 879 raises suspicions about how Alfred the Great gained control of Mercia.
- Beau draws a parallel to the series "The Last Kingdom" on Netflix for those unfamiliar with the historical context.
- The discovery of these coins has the potential to rewrite history and challenge established traditions.
- Beau questions the intrinsic value of silver and gold, likening it to the value placed on the history and traditions of a nation.

# Quotes

- "Two guys stumbled across a viking stash of coins and jewelry."
- "The histories at the time, well they're a little murky."
- "This is upending history and tradition."
- "Silver and gold, even today, survivalists will tell you to keep some around because it's always valuable."
- "It's just a thought, y'all have a good night."

# Oneliner

Beau reveals a treasure trove of Viking coins challenging historical narratives and traditions, urging reflection on the value of history over precious metals.

# Audience

History enthusiasts

# On-the-ground actions from transcript

- Research local archaeology groups to support preservation efforts (suggested)
- Visit historical sites or museums to learn more about ancient civilizations (implied)

# Whats missing in summary

The full transcript provides a deeper dive into the significance of historical artifacts and their impact on our understanding of the past.