# Bits

Beau says:

- Republicans lost in Louisiana, a deep red state, because loyal Republicans stuck to their values.
- If your party doesn't offer a candidate worthy of your vote, you can choose to abstain from voting.
- Voting is not just about party loyalty; it's about becoming morally responsible for the actions of the candidate you support.
- By voting for a candidate, you become complicit in everything they do, past, present, and future.
- Voting for a candidate means you are endorsing and supporting all their actions.
- Beau questions whether individuals who support Trump through their vote truly agree with actions like putting kids in cages or weakening environmental protections.
- Voting for a candidate means taking ownership of their actions and decisions.
- Beau urges people to think critically about the actions of the candidates they support rather than blindly following party loyalty.
- Supporting a candidate through voting means endorsing everything they stand for and everything they do.
- You can choose not to vote if the candidate presented by your party conflicts with your values and principles.

# Quotes

- "You don't co-sign evil out of tradition."
- "Voting when you vote for someone, you become morally responsible."
- "You become complicit in everything they do."
- "If you vote for Trump, past, present, and future, Trump is you."
- "You own that. You co-signed it."

# Oneliner

Republicans lost in Louisiana because loyal Republicans stuck to their values, showing that voting means becoming morally responsible for the candidate you support.

# Audience

Voters

# On-the-ground actions from transcript

- Re-evaluate your loyalty to a political party based on the actions and values of the candidates they present (exemplified)

# What's missing in the summary

The full transcript provides a deeper exploration of the importance of holding politicians accountable for their actions and decisions, urging voters to prioritize values over blind party loyalty.