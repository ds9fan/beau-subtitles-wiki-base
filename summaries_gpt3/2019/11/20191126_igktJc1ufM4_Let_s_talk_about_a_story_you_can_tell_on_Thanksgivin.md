# Bits

Beau says:

- Tells a story fit for Thanksgiving with themes of poverty, sacrifice, duty, political conflict, and environmental consciousness.
- Narrates the tale of Nikolai Vavilov, a man from a rural town dedicated to ending famine by gathering seeds worldwide.
- Vavilov's pursuit led to the creation of the first seed bank in the Soviet Union.
- Despite his contributions, Vavilov was denounced by a rival with Stalin's favor and sent to the gulags, where he died of starvation.
- During World War II, scientists in Leningrad protected the seed bank through a siege, sacrificing their lives by not consuming the stored food.
- The dedication of these scientists ensured the preservation of vital crop samples for future generations.
- The seeds from this bank have influenced the food on our tables, connecting us to this impactful story of sacrifice and dedication.
- The narrative showcases individuals who prioritize the greater good over their own survival, symbolizing the essence of Thanksgiving.
- Beau leaves listeners with a reflection on the significance of being thankful for food and the enduring legacy of those who work to address global challenges.

# Quotes

- "When you really think about it, people that dedicated to helping others, they literally just waste away right next to food."
- "The idea of Thanksgiving, being thankful for food, I think it fits."

# Oneliner

Beau narrates the inspiring tale of Nikolai Vavilov and Leningrad scientists, embodying sacrifice and dedication for the greater good, tying the story seamlessly to the essence of Thanksgiving.

# Audience

History enthusiasts, environmentalists, storytellers

# On-the-ground actions from transcript

- Preserve local plant varieties and seeds to protect biodiversity (implied)
- Support seed banks or initiatives that focus on crop preservation (implied)

# Whats missing in summary

The emotional depth and impact of the sacrifices made by individuals like Vavilov and the Leningrad scientists in the face of adversity.

# Tags

#Thanksgiving #NikolaiVavilov #SeedBank #Sacrifice #EnvironmentalConsciousness