# Bits

Beau says:

- Talks about statistics from California regarding homemade firearms.
- Mentions the significance of the data but notes that it's often overlooked.
- Raises the issue of individuals building firearms at home to bypass bans.
- Mentions the ease of manufacturing firearms at home, referencing the ATF report.
- Points out that 30% of firearms in California are homemade, indicating a significant trend.
- Notes that homemade firearms are used to circumvent California's restrictions.
- Emphasizes that manufacturing firearms at home is easier than dealing with neighboring states' gun laws.
- States that a variety of firearms, from pistols to AKs, are being produced at home.
- Explains the process of making firearms at home, focusing on manufacturing the receiver.
- Stresses the importance of addressing root causes rather than focusing on banning specific parts.

# Quotes

- "Now we do because the information is out."
- "It tells us a lot and it tells us that this is something that has to be taken into consideration anytime legislation is proposed."
- "So this shows us again that we have to address root causes."
- "People in the United States have way too much of a desire to own a firearm."
- "Y'all have a good night."

# Oneliner

Beau reveals the significant trend of homemade firearms in California, underscoring the need to address root causes rather than focus on bans.

# Audience

Legislators, Gun Control Advocates

# On-the-ground actions from transcript

- Advocate for comprehensive legislation addressing root causes (suggested)
- Raise awareness about the implications of homemade firearms (implied)

# Whats missing in summary

The full transcript provides in-depth insights into the prevalence and implications of homemade firearms, urging a focus on root causes rather than reactive measures.

# Tags

#Firearms #Homemade #California #Legislation #RootCauses