# Bits

Beau says:

- Beau shares a personal anecdote about his son playing with a toy train, leading into a story he wanted to talk about.
- He introduces the story of Joe, known as the happiest prisoner on death row, in the 1930s.
- Joe, with an IQ of 45, faced a challenging life and was picked on before being arrested for vagrancy in Cheyenne, Wyoming.
- Sheriff Carroll questions Joe about crimes in Pueblo, and Joe falsely confesses to be with another man named Frank.
- Despite inconsistencies in Joe's confession and lack of identification by the surviving sister, he is convicted and sentenced to death.
- Joe's attorney tries to help with appeals, but Joe is executed without a pardon.
- Joe's last moments before execution involve playing with a toy train, showing innocence and ignorance of his impending death.
- Beau mentions Rodney Reed's case in Texas and urges viewers to research and possibly take action within a week.
- Beau concludes with a reflective note, leaving the audience with something to ponder.

# Quotes

- "His name was Joe Aradie, known as the happiest prisoner on death row."
- "Took me too long to see a toy train."

# Oneliner

Beau shares the story of Joe, the happiest prisoner on death row in the 1930s, urging action on Rodney Reed's case in Texas within a week.

# Audience

Advocates for justice

# On-the-ground actions from transcript

- Research Rodney Reed's case and contact the governor within a week (implied)
- Advocate for justice by raising awareness about cases like Rodney Reed's (implied)

# Whats missing in summary

The emotional impact and depth of the stories shared by Beau can best be experienced by watching the full video.