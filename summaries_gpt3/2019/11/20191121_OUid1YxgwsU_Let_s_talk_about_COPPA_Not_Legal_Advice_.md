# Bits

Beau says:

- Explains COPPA, a law impacting YouTube creators due to child privacy protection.
- Mentions emotional reactions from creators, some not fully understanding the law's implications.
- Notes the potential impact on wholesome kids' content on YouTube.
- Describes the practical implications of COPPA, including marking videos for kids and potential fines for non-compliance.
- Outlines factors determining kids' content, clarifying misunderstandings around the list provided by YouTube.
- Raises concerns about the subjective nature of compliance with COPPA.
- Emphasizes the severe consequences for creators if they mislabel their content.
- Suggests actions to address the issues with COPPA, including contacting the FTC and legislators to push for changes.
- Urges support for kid content creators through Patreon or merchandise purchases.
- Advocates for getting rid of COPPA altogether, citing its outdated nature and negative impact on content creation.
- Encourages viewers to take action by contacting representatives and engaging with the issue.

# Quotes

- "It's dead. There's no comment section. They won't get a notification. The monetization is severely limited."
- "What they're doing is they're going to limit their child's entertainment and educational opportunities, what they can see for viewing."
- "We don't need to succumb to learned helplessness. We don't just have to roll over and take this."

# Oneliner

Beau explains COPPA's impact on YouTube creators, urging action to address the negative consequences for wholesome kids' content and creators.

# Audience

Content Creators, YouTube Users

# On-the-ground actions from transcript

- Contact the FTC and leave comments to advocate for changing COPPA (implied).
- Reach out to representatives and senators to explain the outdated nature of the law and push for revisions (implied).
- Support kid content creators by signing up for their Patreon or buying their merchandise (implied).
- Tweet the president to raise awareness and potentially influence the FTC's decisions (implied).

# Whats missing in summary

The full transcript provides a comprehensive understanding of COPPA's implications on YouTube creators, including practical steps to address the challenges and advocate for changes. Viewing the full transcript can offer additional insights and details on the issue.

# Tags

#COPPA #YouTube #ContentCreators #ChildPrivacy #Advocacy