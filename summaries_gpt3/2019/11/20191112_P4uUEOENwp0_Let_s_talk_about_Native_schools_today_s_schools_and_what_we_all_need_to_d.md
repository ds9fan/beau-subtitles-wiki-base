# Bits

Beau says:

- Native American Heritage Month, discussing the brutal history of native schools imposed on Indigenous peoples, designed to crush their spirit and eradicate their culture.
- Schools historically aimed to assimilate Indigenous children, forbidding them from speaking their native tongue, using their real names, or practicing their culture.
- The impact of these schools on Indigenous communities has been long-lasting, with repurposed facilities sometimes revealing unmarked graves of children.
- Modern-day schools, while not as brutal, still prioritize assimilation and uniformity, stifling curiosity and individuality.
- Teachers are often overworked and focused on meeting standardized test requirements, leading to students learning what to think rather than how to think.
- As parents and community members, it's vital to supplement the education provided by the state and encourage curiosity and critical thinking in children.
- Beau shares an anecdote about encouraging his child's curiosity by exploring topics together, utilizing resources like YouTube to foster learning opportunities.
- The education system prioritizes uniformity over individuality, hindering the development of critical thinking skills in students.
- Beau stresses the importance of fostering curiosity in children to ensure they can apply their knowledge effectively and think for themselves.
- While acknowledging the challenges teachers face, Beau encourages a shift towards promoting critical thinking skills and individuality in education systems.

# Quotes

- "The schools that were imposed on the natives, they were something else."
- "The schools, they kill that individuality, they really do, it's the way they're designed."
- "We have to create that curiosity so that they can take that base knowledge that they're getting to the next level."
- "You try to impart that lesson. I guarantee you, you're never going to oppose a raise for teachers again."
- "It's just a thought."

# Oneliner

Beau brings attention to the brutal history of Native American schools, the modern-day impact of assimilation in education, and the importance of fostering curiosity to combat uniformity in learning systems.

# Audience

Parents, Community Members

# On-the-ground actions from transcript

- Foster curiosity in children through supplementary learning opportunities (exemplified)
- Encourage critical thinking skills and individuality in education systems (implied)
- Utilize resources like YouTube to support children's interests and curiosity (exemplified)

# Whats missing in summary

The detailed examples and anecdotes shared by Beau provide a deeper understanding of the impact of assimilation in education and the importance of fostering curiosity in children.

# Tags

#NativeAmericanHeritage #Education #Assimilation #Curiosity #CriticalThinking