# Bits

Beau says:

- Introduces one of his heroes, Martha Gellhorn, as one of the most interesting and prolific correspondents of the 20th century.
- Martha Gellhorn covered significant events like the Spanish Civil War, the rise of a German dictator, D-Day, camps during WWII, Vietnam, Israeli-Arab conflicts, wars in Central America, and almost went to Bosnia.
- Shares a fascinating story of Martha Gellhorn sneaking ashore on D-Day by impersonating a stretcher bearer.
- Mentions Martha Gellhorn's detention and escape after being arrested for going ashore without authorization on D-Day.
- Talks about Martha Gellhorn's marriage to Ernest Hemingway and how she challenged societal norms in combat correspondence.
- Recognizes Martha Gellhorn as someone who changed the rules and broke societal barriers by being wrong in the right ways.
- Acknowledges that many individuals who challenge societal norms often do not receive the recognition they deserve.

# Quotes

- "They are wrong in all of the right ways, and it moves our society forward."
- "It's changing because of people like her."
- "A lot of things in society change because people just do it anyway."

# Oneliner

Beau introduces Martha Gellhorn, an exceptional correspondent who defied norms, inspiring societal change.

# Audience

Journalists, historians, activists

# On-the-ground actions from transcript

- Read about Martha Gellhorn's extraordinary life and contributions (suggested)

# Whats missing in summary

The full transcript provides a detailed insight into the remarkable life of Martha Gellhorn and how she challenged societal norms, inspiring change.

# Tags

#MarthaGellhorn #Journalism #ChallengingNorms #SocietalChange #Heroes