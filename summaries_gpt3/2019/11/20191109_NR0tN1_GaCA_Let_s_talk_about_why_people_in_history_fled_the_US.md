# Bits

Beau says:

- Critiques the avoidance of discussing the negative parts of American history, particularly during National American History and Founders Month.
- Points out the fallacy of the idea that people in the United States do not flee, citing historical instances like during the Civil War and Vietnam draft.
- Describes a period when tens of thousands of people fled to Canada, challenging the narrative of Americans as problem-solvers.
- Explains the loophole in the agreement between Canada and the US that allowed refugees to seek asylum in Canada.
- Mentions upcoming court proceedings in Toronto led by Amnesty International Canada to challenge the US asylum system.
- Argues that the US is violating international law in its treatment of refugees, including family separation and unwarranted detention.
- Urges the Canadian government to recognize the flaws in the US asylum system and scrap the agreement due to the abuse in US detention centers.
- Draws parallels between Donald Trump's policies and historical reasons for people fleeing the country.

# Quotes

- "We don't like to talk about American history, not the bad parts anyway."
- "The U.S. is violating international law as far as the treatment of refugees."
- "It's not safe. The amount of abuse that goes on in those detention centers is just unimaginable."
- "As we talk about American history, it should be noted that Donald Trump gets to join the ranks of the Civil War and the draft as the few reasons people would flee this country."
- "Y'all have a good night."

# Oneliner

Beau challenges the romanticized American narrative by exposing historical refugee movements and criticizing the treatment of refugees in the US, urging Canada to reconsider asylum agreements.

# Audience

Advocates for Refugee Rights

# On-the-ground actions from transcript

- Support organizations like Amnesty International Canada in their efforts to challenge the US asylum system (suggested).
- Advocate for refugee rights and fair treatment of asylum seekers in your community (implied).

# Whats missing in summary

The emotional impact of discussing the mistreatment of refugees in the US and the importance of holding countries accountable for human rights violations.

# Tags

#RefugeeRights #USAsylumSystem #HumanRights #History #Canada #Critique