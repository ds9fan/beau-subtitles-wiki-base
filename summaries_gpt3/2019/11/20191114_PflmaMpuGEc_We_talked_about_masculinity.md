# Bits

Beau says:

- Talks about the ridiculousness of guides designed to help young American males find their masculinity.
- Shares a personal story about a Japanese concept called Shibumi explained by an old Japanese man.
- Describes masculinity as subtle, understated, in control of nature, and effortless perfection.
- Asserts that masculinity cannot be quantified or charted, and each person's masculinity is unique.
- Emphasizes masculinity as a journey of introspection and self-improvement, not a checklist or conforming to societal norms.
- Criticizes toxic masculinity and its origins in glorified violence and immature behavior.
- Challenges the idea of toxic masculinity glorification by some men.
- Counters arguments against feminism and patriarchy, pointing out the need for equal representation and the existence of patriarchal systems.
- Advocates for respecting women's autonomy and choices, regardless of personal opinions or insecurities.

# Quotes

- "Masculinity is something you're going to define for yourself."
- "The fact that you don't find her attractive doesn't matter. What you think doesn't matter."
- "All women have to be two things and that's it. Who and what they want."

# Oneliner

Beau talks about the fluidity of masculinity, the concept of Shibumi, and the importance of defining masculinity for oneself while challenging toxic masculinity and advocating for women's autonomy.

# Audience

Men, feminists, advocates

# On-the-ground actions from transcript

- Challenge toxic masculinity by uplifting others and focusing on self-improvement (implied).
- Advocate for equal representation of women in positions of power (implied).
- Respect women's autonomy and choices, regardless of personal opinions (implied).

# Whats missing in summary

The full transcript provides a deep exploration of masculinity, toxic masculinity, feminism, and patriarchy, encouraging introspection and challenging societal norms regarding gender roles.

# Tags

#Masculinity #ToxicMasculinity #Feminism #Patriarchy #SelfImprovement