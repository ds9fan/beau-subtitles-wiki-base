# Bits

Beau says:
- Explains the concept of Ukraine interfering in the 2016 election.
- Draws a parallel to a fabricated story about tearing down the Great Wall of China in 1899.
- Mentions a made-up person, Frank C. Lewis, and how journalists created a false narrative.
- Talks about how the fake Chinese story spread and led to the manufacturing of evidence.
- Debunks the conspiracy theory about Ukraine's interference in the election.
- Points out that CrowdStrike, a Ukrainian company, was falsely accused, though it's American.
- Criticizes the spread of misinformation by right-wing news outlets.
- Compares the repetition of false stories to how misinformation spreads.
- Raises concerns about the impact of repeated lies on public perception and trust.

# Quotes

- "Ideas travel faster than bullets."
- "How could they be this dumb?"
- "It doesn't even make sense, but it got repeated just like this story."

# Oneliner

Beau explains the debunked Ukraine election interference theory by comparing it to a historic fake story, showcasing how misinformation spreads without verification, impacting public trust.

# Audience

Information Seekers

# On-the-ground actions from transcript

- Verify information before sharing (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of the debunked conspiracy theory regarding Ukraine's interference in the 2016 election and sheds light on how misinformation can spread and impact public perception.

# Tags

#Ukraine #ElectionInterference #Misinformation #DebunkedTheory #PublicTrust