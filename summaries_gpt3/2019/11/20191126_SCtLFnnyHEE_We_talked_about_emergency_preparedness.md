# Bits

Beau says:

- Urges everyone to be prepared for natural disasters due to delayed government response.
- Criticizes the inefficiency of federal agencies in disaster response.
- Encourages families to create emergency kits with essentials like water, food, and first aid supplies.
- Emphasizes the importance of being self-reliant and having a plan for evacuation.
- Advocates for teaching survival skills to children in a fun and engaging way.
- Shares examples of people overlooking readily available supplies during emergencies.
- Stresses the value of improvisation and critical thinking learned through survival skills.
- Encourages seeing the world differently by recognizing possibilities and the importance of change.
- Concludes by underlining the significance of learning survival skills for the youth.

# Quotes

- "We're not the sons of the pioneers and the daughters of Rosie the Riveter anymore."
- "It's up to you. If not you, who? Who's gonna solve the problems if it's not you?"
- "Just take care of this stuff and you're going to be able to help people."
- "When people start to learn survival skills, they start to look at the world in a very, very different way."
- "Supplies are pretty much everywhere. You just have to know where to look and how to think about them."

# Oneliner

Be prepared for disasters, be self-reliant, teach survival skills, and change how you see the world - Beau covers essentials from emergency readiness to mindset shifts.

# Audience

Families, Communities

# On-the-ground actions from transcript

- Prepare an emergency kit with essentials like water, food, first aid supplies (suggested)
- Teach children survival skills in a fun and engaging way (suggested)
- Encourage critical thinking and improvisation skills through survival training (suggested)
- Practice looking at components as improvisational tools (implied)

# Whats missing in summary

The full transcript provides detailed insights on disaster preparedness, self-reliance, teaching survival skills, and changing perspectives on the world.

# Tags

#DisasterPreparedness #SelfReliance #SurvivalSkills #CommunityResilience #MindsetShifts