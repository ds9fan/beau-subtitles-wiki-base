# Bits

Beau says:

- Explains the history of the English language, starting with Old English in 450 to 1100 when Germanic people mixed with locals.
- Describes the transition to Middle English from 1100 to 1500, influenced by French brought over by William the Conqueror.
- Talks about early modern English from 1500 to 1800, with shorter vowels and more foreign words due to colonization.
- Mentions late modern English from 1800 to now, with a wider vocabulary from the industrial and technological revolutions.
- Details the development of prescriptive grammar in the 1700s, which dictated proper speech through books like Rob Lough's on English grammar.
- Explains how Sir Charles Coutts' expansion on Lough's work included using "he" and "him" as generic pronouns, later ratified by Parliament in 1850.
- Points out that historically, the English language had a singular gender-neutral pronoun, "they," for the majority of its history.
- Criticizes the idea of rewriting the entire English language for one person's preferences, as it historically happened through influential figures like Coutts.
- Ends by questioning the necessity of changing the entire English language for personal preferences.

# Quotes

- "We wouldn't really be able to read it today unless we were trained to do so."
- "For the overwhelming majority of the history of the English
  language, we had a gender neutral pronoun, a singular gender neutral pronoun, they."
- "It's not actually rewriting the English langauge to suit one person's personal preference."
- "That's how it happened. So they, them, theirs, that's actually the norm."
- "I guess we can rewrite the entire langauge because of one person's preference."

# Oneliner

Beau dives into the history of the English language and questions the necessity of rewriting it for one person's preference, advocating for the historically used gender-neutral pronoun "they."

# Audience

English Speakers

# On-the-ground actions from transcript

- Advocate for the use of gender-neutral pronouns like "they" in everyday speech (exemplified)

# Whats missing in summary

The full transcript provides a detailed historical background on the development of the English langauge and challenges modern perspectives on grammar and pronouns.