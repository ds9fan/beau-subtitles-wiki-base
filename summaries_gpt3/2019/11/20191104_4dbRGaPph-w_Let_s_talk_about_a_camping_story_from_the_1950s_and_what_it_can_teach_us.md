# Bits

Beau says:

- In 1954, two groups of 11 boy scouts who had never met before went camping at Robbers Cove State Park in Oklahoma.
- The boys quickly formed bonds and a social order, but things took a competitive turn when they found out about another group nearby.
- The competition escalated with sporting events, team challenges, and the winning team was promised medals and pocket knives.
- Name-calling, stealing flags, and animosity grew between the two groups, known as the Rattlers and the Eagles.
- The losing team stole the winning team's medals and pocket knives after the tournament, leading to continued grudges and anger.
- When the camp lost water, all the boys worked together to fix the pipes and even collaborated to pull a stuck pickup truck using a rope from a tug of war.
- The act of pulling together to solve a common problem led to the insults and grudges disappearing, transforming enemies into friends.
- Psychologists intentionally set up this experiment with similar groups to observe how shared struggles could unite people.
- A similar study in Beirut in 1963 with mixed Muslim and Christian teams did not go as planned, showing that shared struggles might not always overcome deep divisions.
- Beau suggests that when faced with a common struggle like a stuck truck or lack of water, people can unite despite their differences, transcending barriers like religion, skin tone, and borders.

# Quotes

- "When the struggle comes, we will unite."
- "Those petty divisions disappear. Skin tone, religion, borders, none of it matters because we'd all be united."
- "We have the struggles. We just need somebody to leave the rope, set things in motion, get us to unite."
- "The insults stopped. The grudges, well, they're gone. They became friends."
- "It's been repeated pretty often."

# Oneliner

In 1950s camping experiment, shared struggles united warring boy scout groups, showing how common challenges can dissolve divisions and foster unity.

# Audience

Community members

# On-the-ground actions from transcript

- Organize community events that involve collaboration and teamwork (implied)
- Encourage diverse groups to work together on common goals or projects (implied)
- Support initiatives that address shared struggles like poverty, hunger, or climate change (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of how shared struggles can unite diverse groups, offering insights into the power of collaboration and overcoming divisions through common challenges.

# Tags

#Unity #Community #Collaboration #SharedStruggles #BoyScouts