# Bits

Beau says:

- Microsoft is storing the world's open source code in a vault in the Arctic, similar to the crypt of civilization built in the 40s at Oglethorpe University in Georgia.
- The crypt at Oglethorpe University contains microfilm and was created due to the lack of information about ancient civilizations after their fall.
- The Arctic World Archive near the global seed vault in Norway houses the open source code on a microfilm-like substance.
- Time capsules aim to send information to the future, acknowledging that future civilizations may not have certain knowledge.
- There's a mix of useful information like open source code and encyclopedic knowledge, but also trivial items like the secret sauce recipe for McDonald's in the Vatican archives.
- Beau questions the arrogance behind presuming to know what future civilizations will need, suggesting a focus on preserving basic human knowledge instead.
- He proposes multiple vaults containing vital information on medicine, food preparation, and agriculture as a more practical approach to ensuring humanity's survival.
- The reality is that governments, large companies, and institutions like Microsoft and McDonald's foresee a world reset within 750 years, the lifespan of the microfilm.
- Beau concludes by suggesting the idea of preparing for such a collapse by disseminating fundamental human knowledge.

# Quotes

- "Perhaps, given humanity's seeming unwillingness to work together to avoid a fall, maybe we should focus on building vaults all over the place."
- "Basic information about medicine, food preparation, agriculture, that's what humanity needs to survive."
- "World governments and large companies foresee a world reset within 750 years because that's how long that film lasts."

# Oneliner

Microsoft stores open source code in Arctic vault, questioning human arrogance and proposing basic knowledge preservation for humanity's survival.

# Audience

Concerned citizens, futurists

# On-the-ground actions from transcript

- Build community-led vaults with basic human information for survival (suggested)
- Preserve knowledge on medicine, food preparation, and agriculture in accessible formats (suggested)

# Whats missing in summary

Importance of preparing for potential civilization collapse and the need for humility in preserving vital human knowledge.

# Tags

#Preservation #Knowledge #Humanity #Future #Vaults