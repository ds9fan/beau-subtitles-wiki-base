# Bits

Beau says:

- Conservative news sites are spreading a misleading story about AOC visiting an empty parking lot and pretending it's a detention center.
- The photos in question were actually taken a year before AOC became a representative, during a large protest at a detention center.
- No children were visible at the fence line because they were not allowed that close to the edge of the detention center.
- The story originated from InfoWars and was not fact-checked by reputable outlets.
- Beau urges people to fact-check information themselves, as news sites may push false narratives to fit their agenda.

# Quotes

- "This is how opinions are manufactured."
- "Y'all have a good night."

# Oneliner

Conservative news sites spread a false story about AOC visiting a detention center, revealing the dangers of manufactured opinions and the importance of personal fact-checking.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Fact-check stories spread by news sites (suggested)
- Be vigilant about misinformation and false narratives (implied)

# Whats missing in summary

The tone and delivery of Beau's message, as well as the call to action for personal responsibility in verifying information.

# Tags

#ElectionCycle #Misinformation #FactChecking #ManufacturedOpinions #MediaLiteracy