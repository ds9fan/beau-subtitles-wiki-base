# Bits

Beau says:

- Points out the incorrect usage of "left" and "right" in the US and explains common terms.
- Differentiates between Democrats as liberals and not leftists, more center-right.
- Mentions the lack of a viable left-wing party in the United States.
- Provides an example of a Facebook post and tweet supporting amnesty for undocumented individuals.
- Acknowledges potential backlash for his statement on amnesty.
- References Ronald Reagan's stance on amnesty, contrasting it with President Trump's wall policy.
- Criticizes the right for not having a hard line like the left, attributing it to the lack of theocratic elements.
- Encourages not to shy away from radical ideas and to defend them.
- Suggests that without the radical left, society tends to move further right.
- Emphasizes the importance of advocating for a fair and just society to move towards the left globally.

# Quotes

- "Without the radical left, well, we're all just right-wing."
- "History has a well-known leftist bias."

# Oneliner

Beau clarifies the misusage of "left" and "right," distinguishing Democrats as liberals, not leftists, and advocates for defending radical ideas to prevent society from moving further right.

# Audience

Activists, Progressives, Liberals

# On-the-ground actions from transcript

- Defend radical ideas when you hear them (advocated)
- Advocate for a fair and just society (advocated)

# Whats missing in summary

The full transcript includes a deep dive into political terminologies, historical biases, and the importance of advocating for radical ideas to prevent society from shifting further right.

# Tags

#PoliticalTerminology #Advocacy #LeftVsRight #RadicalIdeas #SocialJustice