# Bits

Beau says:

- Statue of Liberty arrived in New York in 1885, without the famous inscription.
- The President plans to target undocumented people for removal, including those who committed no other crime than crossing a line.
- ICE is hesitant about this approach due to bad optics and potential public backlash.
- Removals are typically done at job sites to avoid public scrutiny and dehumanization.
- The impact of seeing families torn apart during removals will be significant.
- The punishment for undocumented immigrants seems harsh compared to other violations like watching a movie without a ticket.
- Children of undocumented parents face dire situations if their families are targeted for removal.
- Cheering on mass removals contradicts the fear of government overreach.
- Amnesty is suggested as a humane solution to prevent the destruction of millions of lives.
- Prioritizing removals over going after real criminals like human traffickers is questioned.

# Quotes

- "You're going to see they're real people."
- "Amnesty, amnesty, amnesty. the law. Amnesty."
- "Let's help the tempest-tossed, the huddled masses."

# Oneliner

The Statue of Liberty, removals, and the call for amnesty – a poignant reminder of the humanity behind immigration policies.

# Audience

Advocates, Activists, Citizens

# On-the-ground actions from transcript
- Advocate for humane immigration policies (implied)
- Support organizations working to protect undocumented individuals (implied)
- Educate others on the realities of immigration enforcement (implied)

# Whats missing in summary

The full transcript conveys a powerful message on the need for empathy and compassion in immigration policies.

# Tags

#Immigration #Humanity #Amnesty #Government #Advocacy