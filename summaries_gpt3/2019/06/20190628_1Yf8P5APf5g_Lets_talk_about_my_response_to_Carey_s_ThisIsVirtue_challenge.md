# Bits

Beau says:

- Recites "New Colossus" sonnet, focusing on America's promise of welcoming immigrants.
- Raises questions about what people are doing to uphold American virtues and values.
- Challenges people to participate in the "This Is Virtue challenge" by reciting the sonnet and sharing it on social media with the hashtag #thisisvirtue.
- Encourages individuals to use their voices and be proactive in standing against policies that harm vulnerable populations.
- Points out the importance of not getting lost in trivial arguments when significant issues are at stake.
- Calls for action to remind people of American virtues and the country's promised values.
- Urges viewers to think about how they will answer future generations when asked about their actions during this era.

# Quotes

- "Give me your tired, your poor, your huddled masses yearning to breathe free."
- "What are you going to tell them? Are you going to tell them that you used your voice?"
- "The idea is to remind people what American virtues are, what they're supposed to be."

# Oneliner

Beau challenges viewers to recite the "New Colossus" sonnet and participate in the "This Is Virtue challenge" to remind people of American virtues, encouraging them to take action and use their voices against injustice.

# Audience

Social media users

# On-the-ground actions from transcript

- Recite and share the "New Colossus" sonnet on social media with the hashtag #thisisvirtue (suggested).

# Whats missing in summary

The emotional impact of participating in the challenge and the power of collective action to uphold American values.

# Tags

#ThisIsVirtue #AmericanValues #Immigration #SocialMedia #Activism