# Bits

Beau says:

- Tariffs are beginning to have real impacts, leading to price increases at major retailers like Costco, Walmart, Dollar General, Family Dollar, and Dollar Tree.
- Despite warnings from experts about the consequences of tariffs, many Americans allowed politicians to interpret the facts for them.
- The timeline for climate change is compared to driving a car, where we must act before passing the point of no return.
- By 2050, we are projected to reach a critical point in climate change, leading to the collapse of society.
- Climate change is described as an existential national security threat and a risk to Earth's intelligent life.
- A mere two-degree increase in temperature could displace a billion people globally.
- The younger generation is emphasized as the key fighters against climate change, needing to take significant action.
- Planting trees and other small measures can buy time but won't solve the climate crisis.
- A World War II-style mobilization is recommended to address climate change effectively.
- Individuals are urged to vote with their dollars and hold both multinational corporations and governments accountable for their environmental impact.

# Quotes

- "We're talking about a border crisis now. Worrying about refugees now. Wait until there's a billion of them."
- "Those meddling kids, when they finally get that villain, and they pull the mask off of them just like Scooby-Doo, they're gonna find out it's some old dude trying to make some money."
- "It threatens the premature extinction of Earth originating intelligent life."

# Oneliner

Beau breaks down the impacts of tariffs, the urgency of climate change, and the vital role of the younger generation in taking action for our planet's future.

# Audience

Climate activists, Young Generation

# On-the-ground actions from transcript

- Mobilize for climate action like in World War II (implied)
- Vote with your dollar to support environmentally responsible companies (implied)
- Hold governments accountable for their impact on the environment (implied)

# Whats missing in summary

The full transcript provides additional insights on the urgency of climate change and the need for collective action to address the impending crisis.

# Tags

#ClimateChange #Tariffs #EnvironmentalAction #YouthActivism #PoliticalAccountability