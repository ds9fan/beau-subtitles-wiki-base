# Bits

Beau says:

- Phoenix PD released a fact sheet light on facts, attempting to justify their actions with no use of force mentioned, despite contradicting evidence in original reports.
- The credibility of the Phoenix PD is questioned, given the discrepancies in their statements.
- Even if the facts were true, the situation worsens due to the presence of children during the incident.
- Beau questions the justification for excessive force and violence by the Phoenix PD, even in hypothetical scenarios like shoplifting.
- The core issues lie in the lack of proper training and tactics displayed by the officers involved.
- Failure to follow standard procedures like time, distance, and cover, and conflicting commands led to a chaotic and dangerous situation.
- The officers' actions instilled fear and panic unnecessarily, leading to a traumatic experience for the individuals involved.
- Beau criticizes the lack of de-escalation attempts and the absence of body cameras, which are vital for accountability and learning from such incidents.
- The safety of civilians, especially children, was compromised by the actions of the Phoenix PD.
- Beau calls for accountability within the department, suggesting that all officers on scene should be removed due to negligence and lack of accurate reporting.

# Quotes

- "You threaten to kill a mother in front of her kids. Yeah. Nobody cares."
- "If you drag this out it's just going to get worse."
- "Your credibility is already shot. You need to make your decision on what you're going to do."
- "You're going to get somebody killed."
- "You have the same propensity for violence as an addict looking for a fix."

# Oneliner

Phoenix PD's credibility is questioned as Beau exposes discrepancies, lack of training, and accountability in a troubling incident involving excessive force.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Demand accountability from Phoenix PD (suggested)
- Advocate for policy changes regarding the use of force (exemplified)
- Support civilians standing against police misconduct (implied)

# Whats missing in summary

Beau's emotional delivery and detailed breakdown of the incident can best be experienced in the full transcript.

# Tags

#PoliceAccountability #ExcessiveForce #CommunityAction #Training #BodyCameras