# Bits

Beau says:

- Addressing the controversy around certain terms used for current detention facilities.
- Comparing the conditions inside detention facilities to forms of torture.
- Mentioning sleep deprivation, temperature extremes, solitary confinement, and more as torture methods.
- Noting instances of nooses in cells, sex abuse complaints, and attempts to destroy records.
- Bringing up the government lawyer's argument about not needing to provide basic necessities like beds, toothbrushes, toothpaste, and soap.
- Referring to experts like George Takei and Andrea Pitzer who identify the facilities as concentration camps.
- Explaining that concentration camps don't necessarily mean death camps but are a part of a network.
- Suggesting differentiating between German concentration camps and current ones by adding "Trump" to the name.
- Defending ICE's lack of providing soap due to limited supply and potential misuse by employees.
- Expressing concern about the dehumanizing treatment and potential future consequences.
- Ending with a reference to the manipulation and deception faced by detainees.

# Quotes

- "Sleep deprivation, temperature extremes, solitary without calls. These are all forms of torture."
- "I will defend ICE on one thing. I do understand why they're not quick to provide soap to the detainees."
- "You can trick them and tell them they're going to take a shower, right? It's America."

# Oneliner

Beau unpacks the comparison between current detention facilities and concentration camps, urging differentiation and reflecting on potential future repercussions.

# Audience

Activists, Advocates, Individuals

# On-the-ground actions from transcript

- Contact organizations supporting detainees (implied)
- Advocate for humane treatment of detainees (implied)
- Educate others on the conditions in detention facilities (implied)

# Whats missing in summary

The emotional impact and detailed examples can be better understood by watching the full transcript.

# Tags

#DetentionFacilities #ConcentrationCamps #HumanRights #DetaineeSupport #Advocacy