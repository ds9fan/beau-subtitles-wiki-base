# Bits

Beau says:

- Bella Thorne's intimate photos were hacked and threatened to be released by a blackmailer.
- Thorne took control by posting the photos herself on Twitter.
- Whoopi Goldberg criticized Thorne, implying celebrities should expect hacking if they take such photos.
- Beau questions the victim-blaming mentality behind Goldberg's statement.
- He points out the unfair societal expectations placed on celebrities.
- The scandal around Thorne's photos stems from unrealistic expectations of public figures.
- Beau mentions the commonality of taking such photos, especially among young people.
- He criticizes the stigmatization of normal behavior when it comes to celebrities.
- There's a focus on the pressure for public figures to appear perfect all the time.
- Beau suggests removing the scandalization of leaked photos to combat blackmail and violations.

# Quotes

- "She's a victim of a crime."
- "This is extremely common behavior."
- "You're talking about consenting adults exchanging images of themselves."
- "She didn't do anything wrong, and she certainly didn't do anything wrong simply because of the way she was dressed."
- "Don't turn this into a scandal."

# Oneliner

Beau addresses the unfair stigma surrounding leaked intimate photos, advocating for a shift in societal attitudes towards privacy and normal behavior.

# Audience

Social media users

# On-the-ground actions from transcript

- Challenge societal norms around privacy and body autonomy (implied)
- Advocate for the destigmatization of common behaviors (implied)
- Support individuals affected by privacy violations (implied)

# Whats missing in summary

The full transcript dives deeper into societal expectations, privacy rights, and the need to combat victim-blaming mentalities.

# Tags

#Privacy #VictimBlaming #SocietalExpectations #Celebrities #Intimacy