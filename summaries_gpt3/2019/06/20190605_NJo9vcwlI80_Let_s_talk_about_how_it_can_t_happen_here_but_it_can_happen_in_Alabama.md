# Bits

Beau says:

- Contrasts the belief that certain atrocities can't happen in the United States with historical evidence suggesting otherwise.
- Reads a disturbing social media post by the mayor of Carbon Hill, Alabama, advocating for killing minorities.
- Comments on the mayor's apology after being exposed, stating that it doesn't excuse his actions.
- Calls for the mayor's resignation rather than accepting his apology.
- Raises concerns about the mayor's influence over law enforcement and treatment of minority groups.
- Mentions that he learned about the issue not through the news but from a far-right constitutionalist.
- Criticizes the lack of action from the governor and others to remove the mayor from office.
- Condemns the mayor's advocacy for mass murder and lack of due process.
- Urges for the removal of the mayor to send a message that such behavior is unacceptable.
- Expresses disappointment in the state of Alabama for allowing this situation to persist.

# Quotes

- "The only way to change it would be to kill the problem out."
- "They want your resignation. That's what they want."
- "Your prospective soldiers, they think you're a threat."
- "They see you as a fascist thug."
- "The fact that there are no wills churning already to remove this person says everything you need to know about the great state of Alabama."

# Oneliner

Contrasts belief in immunity to atrocities with the disturbing advocacy for mass murder by a government official, calling for his resignation and criticizing the lack of action in Alabama.

# Audience

Alabama residents

# On-the-ground actions from transcript

- Call for the mayor of Carbon Hill, Alabama, Mark Chambers, to resign (suggested)
- Pressure the governor and other officials to take action to remove Mayor Chambers from office (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the disturbing advocacy for mass murder by a government official and the urgent need for accountability and action to address such behavior effectively.

# Tags

#GovernmentAccountability #Advocacy #Resignation #Alabama #MassMurder