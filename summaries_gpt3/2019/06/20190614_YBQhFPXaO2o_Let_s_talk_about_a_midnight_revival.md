# Bits

Beau says:

- Beau sets the scene for a midnight revival outside, prompted by a congregation member's distress over a sermon.
- The congregation member, labeled a heretic, brought to Beau's attention a sermon filled with hate speech and calls for mass murder against the LGBTQ community.
- The sermon in question was delivered by a former detective from Knox County, Tennessee.
- The detective in the sermon uses anecdotes of drug abuse and rape to demonize the LGBTQ community, sparking Beau's reflection on the potential to vilify any community with such anecdotes.
- Beau condemns the sermon's message as rooted in hatred and points out the speaker's long-standing struggle with carrying that hate.
- He criticizes the congregation's silent complicity in listening to calls for mass murder and likens it to historical instances of silent consent to atrocities.
- Beau invokes the image of August Landmesser, a lone dissenter in a crowd, as a symbol of defiance against hateful ideologies.
- He warns of the country's choice between becoming brown shirts or white roses, referencing historical symbols of complicity versus resistance.
- Beau ends with a thought-provoking message about the need for individuals to stand up against hate and choose the path of resistance.

# Quotes

- "He's carried it, he struggled with it for so long for too long the sacrifice makes a stone of the heart and that is what happened when people talked about him he's giving himself over."
- "I thought of another congregation a long time ago, back in the 30s. It was in Hamburg, another firebrand comes to town. Preaching the same hate ends the same way, advocating for the government to take care of the undesirables, take care of those who just don't fit."
- "We are fast approaching a time in this country where we have a choice. We can become brown shirts. We can become white roses."

# Oneliner

Beau addresses a sermon calling for mass murder against the LGBTQ community, urging individuals to reject complicity in hate and choose resistance against oppressive ideologies.

# Audience

Congregation members, activists

# On-the-ground actions from transcript

- Stand against hate speech and discriminatory ideologies within your community (exemplified)
- Speak out against calls for violence and mass murder targeting marginalized communities (exemplified)

# Whats missing in summary

The full transcript provides a detailed narrative of Beau's impassioned plea to reject hate speech and complicity, drawing parallels to historical instances and urging individuals to take a stand against oppressive ideologies.

# Tags

#HateSpeech #LGBTQ+ #Resistance #Community #Activism