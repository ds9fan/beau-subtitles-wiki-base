# Bits

Beau says:

- Allies in World War II broadcasted messages to the resistance in occupied territories using various frequencies and codes to provide information and hope.
- A reenactment of one of those broadcasts is presented, including personal messages, music, and messages of hope.
- Messages of hope address the targeting of immigrant families by the federal administration, providing support and information to communities.
- Various mayors, including San Francisco, L.A., Atlanta, Denver, Baltimore, and D.C., stand in solidarity with immigrant communities and against inhumane practices.
- LAPD released a statement refusing to cooperate with ICE raids, distancing themselves from such actions.
- The president's threats and ultimatums are compared to past historical events, with a reminder that history rhymes but does not repeat itself.

# Quotes

- "History doesn't repeat. It rhymes."
- "The front line is everywhere."
- "No, it's not the same as Nazi Germany."
- "We are in occupied territory."
- "Y'all have a good night."

# Oneliner

Allies' WWII broadcasts inspire messages of hope amidst current immigrant community support and LAPD's refusal to cooperate, reminding that history rhymes with present challenges.

# Audience

Community members

# On-the-ground actions from transcript

- Stand in solidarity with immigrant communities (implied)
- Provide support and information to immigrant families (implied)
- Refuse to cooperate with inhumane practices (implied)

# Whats missing in summary

The emotional impact of historical comparisons and the importance of community unity and support.

# Tags

#WWII #Resistance #ImmigrantRights #CommunitySupport #HistoricalComparison