# Bits

Beau says:

- Predicts a storm coming and addresses the urgency of discussing Iran.
- Mentions the inevitability of war when discussed in a previous video.
- Questions the credibility of claims linking Iran to sponsoring Al-Qaeda.
- Criticizes the lack of believability in the narrative presented to Congress.
- Raises doubts about the U.S. Navy's claim of a drone being shot down in international waters.
- Reminds of past incidents like the U.S. shooting down Iran Air 655 without consequences.
- Rejects the idea of going to war over the recent drone incident.
- Calls out politicians, including Donald Trump, for using Iran as a tool for political gain.
- Challenges the idea of going to war for political motives at the expense of American soldiers and innocent Iranians.
- Ends with a thought-provoking message about not sacrificing lives for political gains.

# Quotes

- "You're not going to be able to fool anybody this time."
- "That accusation is a confession."
- "Get on those instead of standing on the graves of American troops and innocent Iranians."

# Oneliner

Beau predicts war, questions credibility, rejects war for political gain, and calls for valuing lives over politics.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Stand against war for political gain (implied)

# Whats missing in summary

Insights on the potential consequences of prioritizing political gains over human lives.

# Tags

#Iran #War #PoliticalGain #HumanRights #Credibility