# Bits

Beau says:
- Tonight, I'm sharing links on a topic most Americans might not know or believe.
- The U.S. Senate repealed the Gulf of Tonkin Resolution on June 24, 1970.
- This resolution authorized the Vietnam War based on a false incident.
- The Gulf of Tonkin incident was used to justify the war, but the truth was different.
- Operation De Soto and Opland 34A were running simultaneously in Vietnam.
- The North Vietnamese attack on the Maddox was a response to a previous US action.
- Fabricated evidence was used to justify the Vietnam War.
- Many lives were lost based on this lie.
- The war monument stands as a reminder of the cost of this deception.
- Similar pretexts for war have been sought since then, like tying Iran to al-Qaeda post-9/11.

# Quotes

- "The worst war in American history was fought over a lie."
- "All of those graves were filled over a lie."
- "The Vietnam War was based on a lie."

# Oneliner

Beau reveals the truth behind the Vietnam War, a conflict built on deception and lies, leading to immense loss of life and a stark reminder of the consequences of manufactured narratives.

# Audience

History buffs, truth-seekers, activists

# On-the-ground actions from transcript

- Share this history lesson with others (suggested)
- Educate yourself and others on the truths behind past conflicts (implied)
  
# Whats missing in summary

Delve deeper into the deceptive narratives of past conflicts and the importance of seeking the truth to avoid similar tragedies in the future.

# Tags

#VietnamWar #Deception #Truth #HistoryLessons #Activism