# Bits

Beau says:

- Lawsuit brought by federal asylum officers, not politicians like former Secretary of Homeland Security and James Clapper.
- Federal asylum officers listen to harrowing stories daily and make life-and-death decisions on asylum claims.
- Lawsuit is against the "remain in Mexico policy" or migrant protection protocols, considered a violation of international and domestic law.
- Migrant protection protocols were dictated by Trump and cause widespread violations of rights.
- Asylum officers are barred from asking asylum seekers if they fear for their safety in Mexico under the current system.
- Policy intentionally slows down asylum claims processing to the point where people suffer and die.
- Beau criticizes Trump's actions as dictatorial, bypassing constitutional checks and controlling concentration camps.
- Federal asylum officers challenge Trump's actions, despite their usual role in making tough decisions.
- They condemn the policy as immoral and fundamentally against the moral values of the nation.

# Quotes

- "The fact that they oppose Trump is not news. What is news is who brought the lawsuit."
- "It most certainly is."
- "It's intentionally barring asylum officers from doing their job."
- "It's about stopping people and then slowing down the rate at which we process asylums claims so they die."
- "People who are used to making life and death decisions. People who are totally comfortable sending people back to squalor. They're saying that this is immoral."

# Oneliner

Federal asylum officers challenge Trump's dictatorial control over the "remain in Mexico policy," condemning it as fundamentally immoral.

# Audience

Advocates for human rights

# On-the-ground actions from transcript

- Support organizations advocating for asylum seekers' rights (implied)
- Raise awareness about the violations caused by the "remain in Mexico policy" (implied)
- Advocate for policy changes to protect asylum seekers (implied)

# Whats missing in summary

The emotional impact of federal asylum officers challenging a policy they deem fundamentally immoral.

# Tags

#Immigration #AsylumSeekers #HumanRights #Trump #Policy