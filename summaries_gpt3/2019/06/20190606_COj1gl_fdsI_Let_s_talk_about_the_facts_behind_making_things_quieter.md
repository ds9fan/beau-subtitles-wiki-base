# Bits

Beau says:

- Receives questions about making something quieter from both Second Amendment advocates and gun control advocates.
- Explains the difference between suppressors and silencers, debunking myths.
- Describes how suppressors work by reducing the decibels of a gunshot through a tube with baffles.
- Notes that suppressors reduce recoil by about a third and help mask the location of a shooter by changing the sound profile.
- Mentions that suppressors also reduce muzzle flash and are primarily used for military purposes to avoid raising alarms.
- Addresses the illegal use of suppressors, stating that statistically, they are not significant in crimes.
- Emphasizes that suppressors have legitimate uses, particularly for hearing protection, and are popular among law-abiding gun owners.
- Explains the limitations of homemade suppressors compared to commercial ones.
- Concludes that a ban on suppressors could be effective but acknowledges that it targets law-abiding citizens who are unlikely to use them for criminal activities.
- Touches on the debate regarding the necessity of suppressors under the Second Amendment.

# Quotes

- "It doesn't make anything silent."
- "Decibels are a logarithmic system."
- "They reduce recoil by about a third."
- "If you're just letting off one shot, that's going to sound like anything other than a gunshot."
- "They're very, very law-abiding people."

# Oneliner

Beau explains the science behind suppressors, debunking myths and discussing their legitimate uses among law-abiding gun owners, while addressing the potential effectiveness of a ban.

# Audience

Gun Owners

# On-the-ground actions from transcript

- Contact local officials to advocate for sensible gun regulations (implied).
- Educate others on the legitimate uses of suppressors for hearing protection (implied).
- Support organizations promoting responsible gun ownership (implied).

# Whats missing in summary

In-depth analysis of the debate around suppressors in relation to gun control and the Second Amendment.

# Tags

#GunControl #SecondAmendment #Suppressors #DebunkingMyths #CommunityPolicing