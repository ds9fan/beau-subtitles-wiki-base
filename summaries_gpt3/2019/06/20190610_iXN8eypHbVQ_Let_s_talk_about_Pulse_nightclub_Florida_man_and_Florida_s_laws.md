# Bits

Beau says:

- Raises concern about pastors planning to go to the Pulse nightclub in Florida on the anniversary of the attack.
- Expresses dismay at the pastors' history of celebrating deaths and saying gay people don't deserve to live.
- Warns that the pastors' actions could be seen as a threat of bodily harm and instigating violence.
- Explains the concept of "Florida man" and how the state's unique environment influences behavior.
- Quotes the law allowing deadly force in certain circumstances to prevent harm or forcible felony.
- Advises the pastors to reconsider their plans as their actions could legally lead to harm in Florida.
- Urges them to call off their trip and conduct their activities in a more appropriate setting.
- Condemns the pastors' behavior as morally, ethically, and legally wrong, potentially leading to forcible felonies.
- Criticizes the pastors for mocking the deaths of those murdered at Pulse nightclub.
- Ends with a strong statement against the pastors and their actions, questioning their alignment with Christian values.

# Quotes

- "Y'all need to take this to another state."
- "What you're doing is wrong. It's wrong morally. It's wrong on every level."
- "And in Florida, oh, it's legally wrong."
- "You're going to travel across the country so you can have your little bigoted conference and mock the deaths of people who were murdered? Yeah, that's what Jesus would do."

# Oneliner

Beau warns pastors planning to visit Pulse nightclub in Florida, criticizing their morally and legally wrong actions and urging them to call it off before someone gets hurt.

# Audience

Community members, activists.

# On-the-ground actions from transcript

- Contact local authorities to raise awareness about the pastors' threatening behavior (implied).
- Support LGBTQ+ organizations in Florida to ensure a safe environment during sensitive anniversaries (implied).

# Whats missing in summary

The emotional intensity and passion in Beau's delivery, which adds power to his message, are missing in the summary.

# Tags

#Florida #PulseNightclub #LGBTQ+ #Threat #CommunitySafety