# Bits

Beau says:

- Explains the concept of dog whistles, which are statements with hidden meanings that give the speaker plausible deniability.
- Dog whistles are used to convey messages to a specific group while appearing innocent to others.
- Beau admits to using dog whistles in his videos, not necessarily racist ones, but to communicate with certain groups.
- Talks about how to identify and respond to dog whistles by focusing solely on the hidden meaning and not letting the speaker divert the attention.
- Mentions examples like the statement "if no government is best, then maybe no government is best" as a dog whistle for those who believe in a stateless society.
- Emphasizes the importance of dismantling these hidden messages to expose the true intentions behind them.
- Compares the use of dog whistles to racist jokes and suggests challenging the speaker to explain the hidden meaning behind their statements.
- Provides examples like statistics on crime rates among different groups to illustrate how dog whistles work.
- Encourages engaging with dog whistles to prevent others from being influenced by harmful ideologies.
- Stresses the need to focus on the core message of dog whistles to dismantle the underlying beliefs.
- Beau delves into the nuances of crime statistics and challenges stereotypes associated with criminal behavior.
- Talks about the prevalence of dog whistles in culture, including music, and how they perpetuate harmful stereotypes.
- Urges readers to confront dog whistles and dismantle the pillars of harmful ideologies.
- Beau concludes by encouraging active engagement with dog whistles to combat harmful beliefs in society.

# Quotes

- "You continually hammer on that one statement and get them to address that one thing."
- "You destroy those pillars, those things that hold up the rest of the ideology, those things that are so commonly believed that everybody can recognize them."
- "Engage those dog whistles every chance you get and you focus on that dog whistle."

# Oneliner

Beau explains dog whistles, urges dismantling hidden meanings to combat harmful ideologies, and advocates focusing solely on exposing the core messages behind them.

# Audience

Online activists, community organizers

# On-the-ground actions from transcript

- Challenge dog whistles by focusing solely on dismantling the hidden messages (suggested).
- Engage actively with dog whistles to prevent harmful ideologies from spreading (suggested).
- Address core messages behind dog whistles to expose harmful beliefs in society (suggested).

# Whats missing in summary

Beau's detailed breakdown of dog whistles and strategies for combating harmful ideologies through active engagement.

# Tags

#DogWhistles #CombatIdeologies #OnlineActivism #CommunityOrganizing #ChallengingBeliefs