# Bits

Beau Garg says:

- Driving to Pensacola on I-10, encountering billboards with scripture, including one targeting abortion.
- Billboards in the South costing a few hundred bucks in rural areas and thousands in major markets.
- Hundreds of billboards all over the South, with millions spent on lobbying and informational literature for the pro-life cause.
- Questioning the effectiveness of the money spent on billboards and propaganda.
- Mentioning the artificial uterus, artificial womb technology successfully maturing a lamb at 24 weeks.
- Research on artificial wombs could potentially end the abortion debate.
- Suggesting a shift in funding towards research on artificial wombs if the goal is truly about life preservation.
- Noting the significant number of children in foster care and the thousands aging out of the system annually.

# Quotes

- "Maybe, if it really is about the preservation of life, that money should go to research on these artificial wombs."
- "I think for a lot of people this isn't about the preservation of life. I think it's about controlling other people."
- "Tens of thousands will age out of the system this year."

# Oneliner

Beau Garg questions the effectiveness of pro-life spending, suggesting research on artificial wombs as a solution, while pointing out the reality of children in foster care.

# Audience

Pro-choice advocates

# On-the-ground actions from transcript

- Support research on artificial wombs to potentially address the abortion debate (implied).

# Whats missing in summary

The full transcript provides a deeper exploration of the financial aspects and potential solutions to the abortion debate.

# Tags

#Abortion #ArtificialWombs #ProLife #FosterCare #Research