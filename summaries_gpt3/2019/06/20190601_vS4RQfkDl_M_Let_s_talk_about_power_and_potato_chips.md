# Bits

Beau says:

- Questions the source of power and authority that the state derives from the governed.
- Raises concerns about delegating authority to the state that individuals do not possess.
- Draws parallels between giving authority to the state and giving something one does not have personally.
- Challenges the concept of majority rule justifying violations of others' rights.
- Advocates for taking infringement on others' freedom seriously and tying it to one's conscience.
- Suggests that if individuals lack the authority to do something on their own, they cannot delegate it to the government.
- Differentiates between a justice system based on conscience and a legal system.
- Criticizes the hypocrisy in immigration debates and the treatment of asylum seekers.
- Questions the morality of preventing others from seeking a better life based on arbitrary distinctions.
- Encourages following one's conscience over blindly obeying authority figures.

# Quotes

- "If something is beyond the power of your own conscience, it should be beyond the power of the state."
- "You don't have the authority to give something you don't have as individuals."
- "Infringing on the freedom of others is something that has to be taken very seriously."
- "Maybe it is time for this country to start following its conscience instead of simply following orders."
- "You have a right to be a bigot I guess. Whatever. But does that mean that you have the power?"

# Oneliner

Beau questions the state's authority, urging individuals to follow conscience over blind obedience and exposing hypocrisy in societal norms.

# Audience

Citizens, Activists, Voters

# On-the-ground actions from transcript

- Follow your conscience in decision-making (implied)
- Challenge unjust authority and norms (implied)
- Advocate for policies based on morality and conscience (implied)

# Whats missing in summary

Beau's thought-provoking analysis and call for introspection on power dynamics and moral responsibility.

# Tags

#Authority #Conscience #Justice #Immigration #Hypocrisy