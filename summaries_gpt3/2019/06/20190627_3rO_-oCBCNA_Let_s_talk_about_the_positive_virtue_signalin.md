# Bits

Beau says:

- Virtue signaling has positive aspects and is a significant part of human communication.
- The ice bucket challenge, despite criticism of being empty, raised over a hundred million dollars.
- Compassionate campaigns supporting asylum seekers are failing due to how they're framed, not their message.
- Campaigns framed in opposition to Trump may not produce the best results.
- Beau is concerned about stopping the abuse of kids in detention facilities and preventing drownings.
- Loyalty to political party often outweighs loyalty to principles, allowing wrong actions to be discounted.
- Beau calls for campaigns reminding people of America's virtues and what they should stand for.
- He finds it hard to believe that half the population tolerates sexual assault on kids and inhumane treatment of children in detention.
- Beau believes most Americans do not want kids to be abused but may justify it due to loyalty to Trump.
- Campaigns should focus on the morality of the situation and not political arguments.

# Quotes

- "We need more virtue signaling, a lot of it."
- "I believe that the average American does not actually want kids to be sexually assaulted."
- "We need campaigns that focus on the morality of what's actually happening."
- "We're funding rape rooms."
- "We need to show America what a virtue is."

# Oneliner

Beau talks about the positive impact of virtue signaling, the failure of compassionate campaigns, and the need for morality-focused campaigns to address critical issues like abuse and inhumane treatment.

# Audience

Advocates for social change

# On-the-ground actions from transcript

- Develop and support campaigns reminding people of America's virtues (implied)
- Share suggestions for campaigns focused on addressing critical issues like abuse and inhumane treatment (implied)

# Whats missing in summary

The emotional impact and Beau's call for action are best understood by watching the full transcript.