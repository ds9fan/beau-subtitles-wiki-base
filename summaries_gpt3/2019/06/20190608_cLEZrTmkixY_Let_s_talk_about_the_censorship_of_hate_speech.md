# Bits

Beau says:

- Expresses support for YouTube's efforts to clean up its platform by censoring hate speech.
- Defines hate speech as advocating for violence, discrimination, or negative actions against a group.
- Emphasizes that freedom of speech protects from the government, not private companies.
- Encourages individuals to build their own platforms if they disagree with censorship policies.
- Acknowledges mistakes made by YouTube in banning channels, including a history professor and an organization called News to Share.
- Defends the value of journalism like that done by Ford Fisher, documenting extremist activities and protests.
- Argues that documenting extremist activities can help identify individuals inciting violence versus those exploiting the situation for financial gain.
- Points out the importance of video evidence in intelligence work to determine intent and identify perpetrators.
- Stresses the value of News to Share's journalism, even though mistakes were made in its censorship.
- Mentions Ben Shapiro's support for Ford Fisher and the importance of Fisher's work in journalism.

# Quotes

- "You want to be a bigot? You got a right to be a bigot. The second you advocate for violence, discrimination, or some kind of negative thing to be visited upon whoever it is you're bigoted against, well then that's a problem."
- "Freedom of speech protects you from the government, not from a private company and not from public outcry."
- "Sunlight is a really good disinfectant. It makes it easier to debunk."
- "The market has spoken. We don't want you."
- "There are people that don't have access to your stuff. Video evidence of those speeches provides intent."

# Oneliner

Beau supports YouTube's censorship of hate speech, encourages building alternative platforms, defends valuable journalism, and acknowledges mistakes in the process.

# Audience

Content creators

# On-the-ground actions from transcript

- Contact Ford Fisher to offer support and solidarity (implied)
- Support independent journalism that exposes extremist activities (implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on censorship, the importance of independent journalism, and the need to balance free speech with responsible platform management.

# Tags

#Censorship #HateSpeech #Journalism #FreedomOfSpeech #YouTube