# Bits

Beau says:

- Virtue signaling is the expression of a positive social behavior to your in-group, not necessarily an action rooted in compassion.
- Virtue signaling can be effective in causing societal change, even though it may come across as empty or hollow.
- Examples of virtue signaling include the creation of the "warrior cop" and gestures of loyalty to certain dictators in history.
- Calling out virtue signaling can also be a form of virtue signaling, conveying superiority and rationality within a group.
- Experts have expressed concern over how Trump's rhetoric normalized dehumanization and spread through political parties and government agencies.
- The normalization of inhumane practices, such as in ICE's statement on immigration policy, can be seen as a form of virtue within certain groups.
- Conflicting views on immigration policies and human trafficking often overlook the root causes and focus solely on containment rather than solutions.
- Society needs positive virtue signaling to steer towards a more humane society by addressing underlying issues and promoting compassion.

# Quotes

- "Virtue signaling is extremely effective. It causes societal change."
- "Calling out virtue signaling can also be a form of virtue signaling."
- "The normalization of inhumane practices, such as in ICE's statement on immigration policy, can be seen as a form of virtue within certain groups."

# Oneliner

Virtue signaling, both effective and divisive, influences societal norms and political rhetoric, shaping perceptions and policies.

# Audience

Activists, Advocates, Observers

# On-the-ground actions from transcript

- Challenge dehumanizing narratives within your community (exemplified)
- Advocate for addressing root causes of issues (implied)
- Promote compassionate solutions in policy and discourse (exemplified)
  
# Whats missing in summary

The full transcript delves into the negative impacts of virtue signaling, calling for a shift towards positive virtue signaling to create a more humane society.

# Tags

#VirtueSignaling #SocietalChange #HumanRights #ImmigrationPolicy #CommunityAdvocacy