# Bits

Beau says:

- Beau was asked a question that led him to research and provide an insightful answer about the kind of people who immigrate.
- He shares a first-person account from the 1700s detailing the harsh conditions immigrants faced, similar to present-day situations.
- Immigrants arriving before the mid-1800s faced challenges like being purchased off ships and enduring sickness while waiting to come ashore.
- Families often had to give their sick children to strangers to save their lives during immigration.
- The conditions for immigrants improved slightly in the 1840s but were still challenging.
- Beau points out that throughout history, people came to the U.S. as refugees fleeing various crises.
- He questions individuals who suggest fixing their corrupt home countries instead of seeking better opportunities elsewhere.
- Beau criticizes those who blame immigrants for the country's issues while failing to address systemic problems like lack of basic necessities and democracy concerns.
- He challenges the notion of leaving the country if one has issues with it, citing the true embodiment of the American spirit in those enduring hardships at the border.
- Beau underscores the courage and spirit of immigrants and urges embracing them as additions to the American tapestry rather than condemning them.

# Quotes

- "People are still taking that risk today."
- "The spiritual sons and daughters of the people who built this country are the people dying down there on the border."
- "No self-evaluation will ever be as valuable or as accurate as an enemy's evaluation of you."
- "Who if you have the courage."
- "You'd be welcoming these people as new additions to the American tapestry, not crying and saying that their children deserve to drown."

# Oneliner

Beau dives into history to explain the courage of immigrants and challenges individuals to rethink their views on immigration and refugees.

# Audience

Advocates, Activists, Citizens

# On-the-ground actions from transcript

- Welcome and support immigrants and refugees as new additions to your community (implied)
- Challenge misconceptions about immigrants and refugees in your circles (implied)
- Advocate for humane treatment and support for those seeking refuge (implied)

# Whats missing in summary

The emotional depth and historical context of Beau's message can be best experienced by watching the full transcript.

# Tags

#Immigration #Refugees #AmericanSpirit #History #Humanity