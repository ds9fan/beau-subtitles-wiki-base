# Bits

Beau says:

- Beau shares a personal experience of a friend involved in a shooting incident at a gun-free zone.
- The friend secured his weapon in the car, heard gunfire, and moved to a safer location instead of engaging the threat.
- Beau challenges the myth of the "good guy with a gun," stating that untrained individuals are more likely to die in combat than successfully intervene.
- He mentions the prevailing myth of guns not killing people, but rather people killing people, and how it leads to a dangerous mindset.
- Beau refers to the conditioning required for individuals to kill and how only 4% of people are naturally wired for it.
- He talks about the flawed belief that arming more people will stop mass shootings and dismantles the idea of a "good guy with a gun."
- Beau criticizes the cultural glorification of guns and stresses the need to address the root cultural issues rather than rely on legislation.
- He mentions the case of the Parkland cop and the different standards applied due to training and duty to intervene.
- Beau concludes by stressing that the concept of a "good guy with a gun" is part of the problem, not the solution, and signifies toxic masculinity.

# Quotes

- "Guns don't kill people. People kill people."
- "The gun doesn't make the man."
- "It's a bandaid on a bullet wound."

# Oneliner

Beau challenges the myth of the "good guy with a gun" and addresses the cultural issues around gun ownership, advocating for a shift in mindset away from toxic masculinity.

# Audience

Advocates for cultural change

# On-the-ground actions from transcript

- Educate others on the dangers of the "good guy with a gun" myth (suggested)
- Challenge the glorification of guns in culture (implied)

# Whats missing in summary

Full understanding of the dangers of perpetuating the myth of the "good guy with a gun" and the need for cultural change.

# Tags

#GunViolence #ToxicMasculinity #CulturalChange #GunMyths