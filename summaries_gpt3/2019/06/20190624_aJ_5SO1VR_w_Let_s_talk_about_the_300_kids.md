# Bits

Beau says:

- 300 children were removed from a Border Patrol facility in Clint, Texas, designed for 100 people but housed 350.
- Visitors described the facility as having a stench.
- The children lacked adequate food, hygiene items like soap and toothpaste, and hadn't showered in almost a month.
- Their clothing had bodily fluids ranging from breast milk to urine.
- Many children don't know the whereabouts of their families.
- Flu outbreaks were present, and there was a lack of beds.
- Border Patrol had younger children taking care of toddlers.
- The children have not been rescued; they were moved to another facility, which Beau calls a "Trump concentration camp."
- Deaths in concentration camps were often due to disease and lack of food caused by poor hygiene.
- Beau questions when people will call their senators and address the crisis.
- Children are being treated like livestock, not as human beings.
- The situation is so dire that it's no longer shocking, which Beau finds tragic.
- The media's portrayal of the children being "removed" is misleading; they remain in the custody of the same agency that neglected them.
- These children are seen as numbers on a board, not as individuals facing a humanitarian crisis.

# Quotes

- "Do you want to wait until we have a bunch of deaths?"
- "This is a travesty. This is ridiculous."
- "They're livestock. They're a product."
- "This is not even surprising anymore."
- "They have no idea where their family is."

# Oneliner

300 children removed from overcrowded Border Patrol facility to another, facing neglect and inhumane conditions, a crisis overlooked on American soil.

# Audience

Advocates for human rights

# On-the-ground actions from transcript

- Contact your senator to demand immediate action to address the inhumane conditions faced by children in Border Patrol facilities (suggested)
- Advocate for policy changes to ensure proper care and treatment of detained children (implied)
- Organize or participate in protests to raise awareness about the crisis and pressure authorities for change (implied)

# Whats missing in summary

The full transcript provides a detailed and emotional account of the appalling conditions faced by children in Border Patrol facilities, urging immediate action and shedding light on the dehumanizing treatment they endure.

# Tags

#BorderPatrol #HumanRights #Children #InhumaneConditions #Crisis