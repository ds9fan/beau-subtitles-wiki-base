# Bits

Beau says:

- Addresses the portrayal of drowned individuals as "illegal immigrants invading our house."
- Explains that these people were fleeing from a country, El Salvador, with a corrupt government supported by the US.
- Describes El Salvador as being overrun by a gang that originated in the United States.
- Mentions issues in El Salvador leading to the ban on a police force as part of a peace agreement.
- Talks about how the gang, with US-trained leadership, became a transnational threat known as MS-13.
- Criticizes policies at the border and compares supporting them to committing a heinous act.
- Points out the role of the US in the situation and the consequences faced by the fleeing individuals.
- Emphasizes the luck and geographic advantage enjoyed by people in the US.
- Condemns the actions that led to the deaths of individuals including a little girl and her family.
- Concludes by reflecting on the impact of these events.

# Quotes

- "We set their house on fire and we're taking pot shots at them as they leave, and then when they die, well, it happens."
- "If you support these policies that are going on down at the border, it's this moral equivalent, is if you took that little girl, put your boot on her head, and held her head under that water until the bubble stopped, you killed her."
- "We killed them. We killed that little girl. We killed her father."
- "That's America today."
- "We live here, we won the geographic lottery, and we can look at these things that happen in other countries and act like, well, it doesn't have anything to do with us."

# Oneliner

Beau addresses the misrepresentation of drowned individuals as "illegal immigrants invading our house" and reveals the harsh reality of their situation, condemning US policies and acknowledging America's role in the tragic events.

# Audience

Policy Advocates

# On-the-ground actions from transcript

- Support organizations aiding asylum seekers (suggested)
- Advocate for policy changes regarding immigration and border control (implied)

# Whats missing in summary

The emotional impact of Beau's words and the call to action for viewers to acknowledge their country's involvement in the suffering of others.

# Tags

#Immigration #USPolicy #Injustice #HumanitarianCrisis #MS13