# Bits

Beau says:

- Moderates are dangerous and could be the downfall of the country.
- The Democratic Party is not truly leftist but slightly right, particularly evident in their foreign policy.
- Moderates trying to find compromise can enable extreme positions.
- Moderates are moving dangerously to the right, tolerating positions unthinkable in the past.
- Democrats are catering to far-right administrations and moving right to chase moderate votes.
- The moderate position has shifted significantly over time.
- Moderates are swayed by finding a middle ground between extreme positions, moving the country further right.
- Child sexual abuse in immigration camps is a result of the moderate position.
- Far-right extremists are taking advantage of moderates not researching or fact-checking.
- Moderates need to re-evaluate their morals and beliefs to prevent further dangerous compromises.

# Quotes

- "You're talking about murdering people. The moderate position of today? Well, can we just kill some of them? Trying to find that middle ground. It's dangerous."
- "Moderates in this country need to get their act together."
- "Are we a nation of ethnic cleansers? Are we a nation that enables child sexual abuse? Is that what we are? Is that moderate?"
- "Call me crazy. Maybe I'm an extremist."
- "What's the next compromise?"

# Oneliner

Moderates moving to the right enable extreme positions, endangering society by compromising on fundamental morals and ethics.

# Audience

Moderates, Activists, Voters

# On-the-ground actions from transcript

- Re-evaluate your morals and beliefs on society's ethics (implied).
- Research and fact-check claims instead of blindly seeking a middle ground (implied).

# Whats missing in summary

The full transcript provides a detailed exploration of how moderates' tendency to compromise dangerously shifts society's ethical boundaries.

# Tags

#Moderates #PoliticalEthics #ExtremePositions #FarRight #SocietyEthics