# Bits

Beau says:

- Congress investigates sexual abuse reports from migrant children in U.S. custody.
- A congressperson misspoke about responsibility for abuse, hurting Health and Human Services' feelings.
- Health and Human Services clarifies it was not all sexual abuse but also misconduct against children.
- Beau stresses that regardless of contractors, the responsibility lies with Health and Human Services.
- Health and Human Services demands an apology for the congressperson's misspeaking.
- Beau questions which staff member waived background checks, recommended shelters, and oversaw children's welfare.
- He questions who placed children at the mercy of others and made the decision to hinder investigations.
- Beau compares the situation to journalists' work despite criticism, pointing out the duty to act.
- He concludes by suggesting that staff hindering investigations should be replaced.
- Beau delivers a critical message on accountability and prioritizing the safety of children in care.

# Quotes

- "It doesn't matter if your feelings are hurt."
- "Which one of your staff should replace the person responsible for this decision?"
- "Probably don't deserve to be in the position you're in."
- "We had a job to do. So do you."
- "Y'all have a good night."

# Oneliner

Congress investigates migrant children abuse, Health and Human Services prioritizes feelings over accountability, Beau calls for action and accountability.

# Audience

Congressional oversight committees

# On-the-ground actions from transcript

- Replace staff hindering investigations (suggested)
- Prioritize child safety over personal feelings (exemplified)
- Demand accountability from Health and Human Services (implied)

# Whats missing in summary

The full transcript provides additional context and depth to the criticism of Health and Human Services for prioritizing feelings over accountability in cases of child abuse.

# Tags

#Congress #MigrantChildren #Accountability #ChildSafety #Oversight