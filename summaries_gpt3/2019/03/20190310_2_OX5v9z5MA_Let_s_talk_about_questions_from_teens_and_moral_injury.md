# Bits

Beau says:

- Teens are angry and apathetic but know they should get involved in societal issues.
- They face a sense of hopelessness and futility, making it difficult to take action.
- Beau warns against looking up to him as a role-model.
- Beau introduces the concept of moral injury, often associated with war zones.
- Moral injury stems from failing to prevent or witnessing transgressions of morals.
- It can result from witnessing everyday injustices like racism or environmental degradation.
- Similar to PTSD, moral injury can lead to suicide, demoralization, and self-harm.
- Ways to cope include belief in a just world view and building self-esteem through small daily actions.
- Beau advises starting with small actions to combat issues causing moral injury, like sitting with a kid wearing a hijab.
- He stresses the importance of developing skills gradually in activism and finding where you are most useful.
- Effective activists share a common motivation that breaks through demoralization and self-handicapping behavior.
- Motivations of activists can stem from various sources like monetary addiction, religion, ideology, or ego.
- Beau encourages holding onto motivations as a reason to persist in activism.
- True believers who are ideologues are the most dangerous but also the most committed in the fight for justice.

# Quotes

- "Get in the fight for 28 days. That's all it takes."
- "Don't go all militant right away. Figure out what you're good at."
- "The most dangerous people in the world are true believers."
- "Hold onto your motivation, it's going to be the reason you make it through 28 days."
- "True believers are all ideologues."

# Oneliner

Teens face anger and apathy but can combat moral injury by starting small actions, finding their role in activism, and holding onto motivations.

# Audience

Teens and young activists

# On-the-ground actions from transcript
- Start small actions to combat issues causing moral injury (suggested)
- Sit with a kid that faces discrimination at school (suggested)
- Use social media to raise awareness about societal issues (suggested)
- Find where you are most useful in activism (suggested)
- Hold onto your motivations as a reason to persist in activism (suggested)

# Whats missing in summary

The importance of persistence, gradual skill development, and finding motivation in activism.

# Tags

#Youth #Activism #MoralInjury #FindingMotivation #SocialChange