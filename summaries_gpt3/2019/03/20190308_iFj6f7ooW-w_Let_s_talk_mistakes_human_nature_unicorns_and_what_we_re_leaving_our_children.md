# Bits

Beau says:

- Society takes a long time to learn from mistakes and tends to double down without questioning.
- The war on drugs has resulted in wasted lives, resources, and money, with plants ultimately winning.
- Society embraces a system that encourages greed and exploitation.
- Political leaders often participate in corruption, rising through ranks to Washington D.C.
- Rare "unicorns" who genuinely care about people get corrupted in the political system.
- Beau rejects the current political system and believes in people living together without borders or government.
- He plants seeds for a future society he won't see, aiming for a stateless society doing no harm.
- Beau criticizes societal conditioning that discourages thinking globally and acting locally.
- Youth today are either angry and rebellious or apathetic due to the bleak future laid out for them.
- Technology offers immense potential to solve global issues, but society clings to outdated systems.

# Quotes

- "We embrace a system that encourages the worst instincts humanity has to offer."
- "People always say, think globally and act globally."
- "Simply, we plant the seeds today for trees that we will never sit under."

# Oneliner

Society's resistance to change, corrupt political systems, and the need for individuals to pave the way towards a better future, as Beau advocates for a stateless society based on cooperation rather than competition.

# Audience

Activists, Reformers, Individuals

# On-the-ground actions from transcript

- Plant seeds for a future society based on cooperation and doing no harm (suggested)
- Encourage critical thinking and acting locally to bring about change (suggested)
- Advocate for a society without borders or government, focused on people living together (implied)

# Whats missing in summary

Beau's call for individuals to reject the current flawed systems, think globally while acting locally, and work towards a cooperative society free from greed and corruption.

# Tags

#Politics #Society #Change #Activism #Future