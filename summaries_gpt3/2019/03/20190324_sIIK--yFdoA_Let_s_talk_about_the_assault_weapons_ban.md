# Bits

Beau says:

- Lists multiple school shootings under the assault weapons ban to illustrate the stakes.
- Points out the ineffectiveness of the assault weapons ban, citing that it didn't work previously.
- Argues that there's no such thing as an "assault weapon" in firearms vocabulary, labeling it a political term.
- Mentions that popular rifles like the AR-15 will still be present even with an assault weapons ban.
- Raises concerns that banning certain weapons could lead to the use of more powerful firearms like the .30-06.
- Notes that the U.S. has more guns than people, making it a unique case in terms of gun ownership.
- Shifts the focus from firearms to the individuals using them, discussing the importance of secure weapon storage.
- Criticizes the glorification of violence and the lack of emphasis on conflict resolution and coping skills.
- Shares a personal story of learning to shoot at a young age, stressing the importance of understanding firearms as tools for a specific purpose.
- Concludes with a thought on the consequences of actions and presentations in society.

# Quotes

- "It's not going to do any good because in the firearms vocabulary, there's no such thing as an assault weapon."
- "Glorify violence for everything, every little infraction, become something worthy of death."
- "It's a tool with one purpose, to kill. That's what a firearm is for."

# Oneliner

Beau questions the effectiveness of an assault weapons ban, pointing out the deeper issues surrounding gun violence and individual responsibility.

# Audience

Gun policy advocates

# On-the-ground actions from transcript

- Secure your firearms to prevent unauthorized access (implied)
- Teach conflict resolution and coping skills alongside firearm safety (implied)

# Whats missing in summary

The detailed examples and personal anecdotes shared by Beau provide a nuanced perspective on gun violence and the broader societal issues contributing to it.

# Tags

#GunViolence #AssaultWeaponsBan #FirearmSafety #IndividualResponsibility #ConflictResolution