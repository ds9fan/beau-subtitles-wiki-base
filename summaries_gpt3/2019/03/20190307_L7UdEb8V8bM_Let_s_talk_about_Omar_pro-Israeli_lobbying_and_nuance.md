# Bits

Beau says:

- Talks about Omar's tweet, Israeli lobbying, campaign contributions, and anti-Semitism.
- Mentions the importance of using OpenSecrets.org to track campaign contributions and lobbying money.
- Points out that Omar's tweet about APEC donating to congresspeople was factually inaccurate.
- Clarifies that AIPAC doesn't give campaign contributions but lobbies with a significant amount of money.
- Emphasizes the influence of pro-Israeli groups in Congress through campaign contributions.
- Questions why Democrats didn't support Omar, suggesting their ties to pro-Israeli groups.
- Explains how Israeli lobbying groups strategically spread their money around to influence both parties.
- Stresses the importance of discussing Israel's policies without coming off as anti-Semitic.
- Advises being precise with terminology when discussing Israel and its policies.
- Mentions the challenges faced by American Jews who speak out against Israel.

# Quotes

- "You have that obligation."
- "Jews did not bomb Gaza. Israel did."
- "American Jews who speak out against Israel. Wow."

# Oneliner

Be accurate with your words when discussing Israel's policies and influence to avoid anti-Semitism accusations and acknowledge American Jews' diverse perspectives.

# Audience

Advocates and activists.

# On-the-ground actions from transcript

- Contact your politicians to demand transparency on campaign contributions and lobbying influences (implied).
- Support American Jews who speak out against Israeli policies by amplifying their voices and stories (implied).

# Whats missing in summary

Deeper insights into the nuances of discussing Israel's policies and lobbying influence while avoiding anti-Semitic implications.

# Tags

#Israel #Lobbying #AntiSemitism #CampaignContributions #Advocacy