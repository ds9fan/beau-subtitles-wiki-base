# Bits

Beau says:

- Analyzing a new liberal plan with various components, including cutting military pay by 15% and creating employment opportunities for conservation work.
- The plan includes funding for renewable energy, railway projects, and paying artists for artwork to decorate public buildings.
- There is a focus on employee rights, strengthening unions, and the establishment of entitlement programs for different groups.
- A significant aspect of the plan is the proposal for a living wage for all American workers.
- Beau criticizes the economic impact of the plan and instead focuses on its historical context and implications.
- He challenges the myth that all World War II veterans were conservatives and dissects the reality of the greatest generation's values.
- The transcript delves into issues of bigotry, refugee support, and the caring nature of the greatest generation during hard times.
- Beau contrasts the values of the greatest generation with the modern American conservative ideology, pointing out discrepancies in their beliefs and actions.
- He argues that American history is complex, with flawed leaders and mistakes, challenging idealized notions of historical figures.
- The transcript concludes by reiterating the fearlessness and compassion of the greatest generation and contrasting it with the attitudes of the modern American conservative.

# Quotes

- "Living with a myth."
- "Nothing like the American conservative today."
- "Fearless is what made the greatest generation great."

# Oneliner

Beau critically examines a new liberal plan, challenges historical myths, and contrasts the values of the greatest generation with the modern American conservative ideology.

# Audience

American citizens

# On-the-ground actions from transcript

- Challenge historical myths about political ideologies (implied)
- Advocate for employee rights and a living wage (implied)
- Support conservation efforts and renewable energy initiatives (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of a new liberal plan, historical comparisons, and a critical examination of the modern American conservative ideology.

# Tags

#LiberalPlan #HistoricalComparison #ConservativeIdeology #EmployeeRights #LivingWage