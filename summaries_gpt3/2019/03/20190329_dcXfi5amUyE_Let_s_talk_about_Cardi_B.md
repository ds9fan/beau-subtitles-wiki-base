# Bits

Beau says:

- Addresses the controversy surrounding Cardi B and her past actions.
- Questions the victims of Cardi B's actions, implying they are partly to blame.
- Expresses disbelief that Cardi B needed to drug anyone, given her appearance.
- Raises doubts about the credibility of the victims and suggests they may have a history of blacking out.
- Criticizes those who compare Cardi B's actions to Bill Cosby's, arguing that it dehumanizes women.
- Warns against damaging relationships by making such comparisons.
- Urges people to stop equating drugging and robbing with drugging and raping.
- Ends with a message to have a good day.

# Quotes

- "I guess she kind of admitted that when she was younger, didn't have a lot of options or whatever, she enticed men to come back with her because they were going to pay her for sex, and then she drugged them and robbed them."
- "Not adding up to me at all."
- "You're telling me that she had to drug you? You? You gotta be kidding me."
- "They're property. They're not people."
- "Two things aren't the same."

# Oneliner

Beau questions victims, doubts credibility, and warns against harmful comparisons in the Cardi B controversy.

# Audience

Onlookers, individuals, allies

# On-the-ground actions from transcript

- Confront harmful comparisons in your social circles (suggested)
- Challenge victim-blaming narratives in discussions (implied)

# Whats missing in summary

The full context and nuance of Beau's perspective can be better understood by watching the complete transcript. 

# Tags

#CardiB #Controversy #VictimBlaming #Dehumanization #SocialAwareness