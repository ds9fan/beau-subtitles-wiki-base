# Bits

Beau says:

- Laws, no matter how trivial, are backed by the threat of death.
- Death is rarely listed as a penalty for not complying with the law.
- Using jaywalking as an example, Beau illustrates the escalation of consequences for not obeying the law.
- Non-violent actions can lead to violent arrests by law enforcement.
- The use of force continuum can ultimately lead to death.
- Beau questions the justification of laws that could result in someone's death.
- He points out that many deaths involve seemingly trivial offenses.
- The power of government violence is a real and concerning issue.
- The potential for fatal outcomes exists for any law enforcement interaction.
- Beau expresses reluctance to support laws that could lead to lethal consequences.

# Quotes

- "Every law, no matter how trivial, is backed up by penalty of death."
- "At the end of the road, whatever law that is being suggested, they will kill somebody over."
- "Arrest is a violent act."

# Oneliner

Beau explains how even seemingly trivial laws can escalate to violence, raising questions about the justification of laws that could lead to death.

# Audience

Policy advocates

# On-the-ground actions from transcript

- Question the justification of laws that could lead to lethal outcomes (suggested)
- Advocate for police reform and de-escalation training (implied)

# Whats missing in summary

The emotional impact and detailed examples can be better understood in the full transcript.