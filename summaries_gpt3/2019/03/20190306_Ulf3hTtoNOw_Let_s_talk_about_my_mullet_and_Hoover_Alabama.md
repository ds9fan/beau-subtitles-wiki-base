# Bits

Beau says:

- Recalls a photo from his past where he was skipping school with older kids in Stewart County, Tennessee.
- Mentions witnessing young men and women in Hoover, Alabama fondly discussing ethnic cleansing on the internet.
- Draws attention to a series of racist incidents in Hoover, Alabama, including a girl using racial slurs and a teacher's inappropriate behavior.
- Describes a case in Hoover where the police mistakenly shot the wrong person, noting a history of racial issues in the area.
- Points out that Hoover, Alabama is named after a figure associated with white nationalist movements and a synagogue bombing suspect.
- Attributes the recent rise in racism to a reaction against having a black president and white people feeling threatened.
- Compares the current surge in racism to a passing trend, like a mullet haircut, suggesting it will eventually fade.
- Expresses concern for the young individuals in Hoover who may face consequences for their community's racist views, instilled by older generations.
- Notes the danger of nationalism fostering unfounded pride and hatred towards others.
- Encourages young people to be critical of outdated ideologies perpetuated by elders in their community.

# Quotes

- "It's a fad, triggered because we had a black president and a bunch of white people felt like they were losing their place in the world."
- "This uptick in racism is a fad, triggered because we had a black president and a bunch of white people felt like they were losing their place in the world."
- "Society, they know that their elders in their community aren't always right. These kids know that. And they should have been able to discern what's right and wrong."
- "It's the community's fault, but it's gonna be these kids that pay."

# Oneliner

Beau addresses the rise in racism triggered by societal shifts, pointing out Hoover, Alabama's history of racial issues and urging young people to challenge outdated community ideologies.

# Audience

Community members, Youth

# On-the-ground actions from transcript

- Address outdated ideologies in your community by initiating open dialogues and challenging discriminatory beliefs (implied).
- Educate yourself and others on the harmful impacts of nationalism and racism to foster a more inclusive society (implied).

# Whats missing in summary

The full transcript provides a detailed examination of the roots of racism, particularly focusing on the impact of community influences on individual beliefs and behaviors.

# Tags

#Racism #Community #Youth #Nationalism #History