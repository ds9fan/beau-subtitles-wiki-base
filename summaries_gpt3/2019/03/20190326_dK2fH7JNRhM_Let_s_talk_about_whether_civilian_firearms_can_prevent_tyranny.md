# Bits

Beau says:

- Addressing the idea that Jews having guns during the Holocaust could have prevented it.
- Explaining the importance of having the right skills and knowledge along with guns.
- Mentioning that guns in themselves do not prevent tyranny; it's about how they are used.
- Stating that untrained individuals are ineffective in combat and are likely to perish.
- Pointing out the discrepancy between people's romanticized views of insurgency and the harsh reality.
- Describing the strategy of insurgency to make the government overreact and clamp down on civil liberties.
- Emphasizing that armed revolution is a last resort and a reflection of societal failure.
- Concluding with a reminder of the gravity and horror of armed revolutions.

# Quotes

- "You don't fight a tank. You don't fight a drone. You go find the tank driver's family or the drone operator's family."
- "Advocating for armed revolution doesn't make you tough. It's an indictment of it."
- "It's too hard to teach that violence is just easier, and it is."
- "At some point in the future, we may need another armed revolution. But if they do, it's because we failed them."

# Oneliner

Beau addresses the limitations and realities of armed revolutions, underscoring the need for societal change and the potential consequences of violence.

# Audience

Society members

# On-the-ground actions from transcript

- Educate others on the complex realities and consequences of armed revolutions (suggested).
- Advocate for peaceful societal change through education and awareness (implied).

# Whats missing in summary

The full transcript provides a deep dive into the complex dynamics of armed revolutions, urging for critical thinking and societal improvements to avoid resorting to violence in the future.

# Tags

#ArmedRevolution #Insurgency #SocietalChange #GovernmentTyranny #Holocaust