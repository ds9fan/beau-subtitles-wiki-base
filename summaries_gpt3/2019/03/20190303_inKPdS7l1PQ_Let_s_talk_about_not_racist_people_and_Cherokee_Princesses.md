# Bits

Beau says:

- Sharing a message received about being banned for using racist slurs on social media.
- Explaining his stance on banning racist slurs on his platform.
- Emphasizing the need to change societal behavior to combat racism.
- Encouraging people to move from not being racist to actively being anti-racist.
- Debunking the myth of having a Cherokee princess in one's family tree.
- Providing a historical tidbit about the mistranslation of Cherokee princesses.
- Suggesting a tactic to confront hidden racism through jokes by pretending not to understand them.
- Stating that confronting hidden racism can change behavior, even if not beliefs.
- Advocating for changing the behavior of racist family members by not tolerating their behavior.
- Mentioning a powerful example of changing a racist individual's mindset through becoming a grandparent to a mixed-race child.

# Quotes

- "To change society you have to change the way people act."
- "It's time for those people who are not racist to start being anti-racist."
- "You don't change their heart, you don't change their mind, you don't change the way they think, but you change the way they act."
- "It's time for those people who are not racist to actively oppose racism."
- "Most effective thing I've ever seen is when the racist dad becomes the grandpa to a mixed kid."

# Oneliner

Beau explains the importance of actively opposing racism and changing behavior to combat societal prejudices, offering tactics for confronting and challenging hidden racism.

# Audience

Social activists, anti-racism advocates

# On-the-ground actions from transcript

- Challenge hidden racism by pretending not to understand jokes that perpetuate harmful stereotypes (implied)
- Change the behavior of racist family members by not tolerating their racist comments or jokes (implied)
- Actively oppose racism by standing up against discriminatory behavior in social circles (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of confronting hidden racism and advocating for active anti-racism efforts in society.

# Tags

#AntiRacism #ConfrontRacism #ChangeSociety #HiddenRacism #FamilyConversations