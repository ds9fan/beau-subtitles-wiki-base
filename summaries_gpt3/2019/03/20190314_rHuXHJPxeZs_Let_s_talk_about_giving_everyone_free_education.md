# Bits

Beau says:

- Critiques the current education system for prioritizing credentialing over actual education.
- Mentions a bribery scandal where rich individuals paid to get their kids into elite schools, questioning the purpose of education in that scenario.
- Talks about the university system and its lack of overhaul despite advancements in technology.
- Mentions computer-based training (CBTs) in the military and how it simplifies the learning process.
- Points out that the government has no interest in breaking down class barriers or providing free education.
- Encourages individuals or groups to set up alternative education platforms in states with lax regulations.
- Suggests creating online certifications and vocational training programs funded through ads.
- Emphasizes the need for individuals to take action in revolutionizing education since the government won't prioritize it.

# Quotes

- "Education's easy. Right now, you've got access to the entire base of humanity's knowledge."
- "It's not about education, it's about status, it's about class, it's about a way of dividing us normal folk from our betters."
- "The government could have done this a long time ago, this training, the CBTs and this technology has been around for a very long time."
- "If the government isn't gonna do it, we just can't have it then I guess you do it."
- "You want a revolution? Start one."

# Oneliner

Beau criticizes the current education system's focus on credentialing over true education, urging individuals to take action in revolutionizing education themselves.

# Audience

Education reformists

# On-the-ground actions from transcript

- Set up alternative education platforms in states with lax regulations on educational institutions (suggested)
- Create online certifications and vocational training programs funded through ads (suggested)

# Whats missing in summary

The full transcript provides in-depth insights into the flaws of the current education system, the government's lack of interest in revolutionizing education, and the call for individuals to take charge in creating change.

# Tags

#Education #Revolution #Credentialing #Government #AlternativeEducation