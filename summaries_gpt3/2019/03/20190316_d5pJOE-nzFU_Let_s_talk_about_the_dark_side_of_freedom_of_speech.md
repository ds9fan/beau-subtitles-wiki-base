# Bits

Beau says:

- Mentioned ethnic cleansing in a recent video, sparking reactions in the comment section.
- People are talking about ethnic cleansing but using more subtle rhetoric.
- Individuals have the right to express bigoted views, but it's your obligation to speak out against it.
- There is a dangerous belief that atrocities only happen in countries with brown people.
- Gives an example from the 1990s where mass rape and murder occurred in a well-developed country.
- Warns that ethnic cleansing can happen in the United States and has already shown its face through mass shootings.
- Talks about the dangerous propaganda tactics used by certain groups to manipulate well-meaning individuals.
- Stresses the importance of waking up to the reality of the situation to prevent further atrocities.
- Ethnic cleansing doesn't always mean mass murder but can involve making certain groups uncomfortable or forcing them to leave.
- Emphasizes that history doesn't repeat but rhymes, indicating current dangerous trends.

# Quotes

- "It can happen here."
- "You have an obligation to speak over them."
- "Genocide. That's how it ends."
- "History doesn't repeat, but it rhymes."
- "The only thing that's going to change it is you."

# Oneliner

Beau stresses the importance of speaking out against dangerous rhetoric to prevent ethnic cleansing in the United States, warning that history's rhymes are already echoing.

# Audience

Activists, Advocates, Community Members

# On-the-ground actions from transcript

- Speak out against bigotry and dangerous rhetoric (implied)
- Educate others about the dangers of subtle forms of ethnic cleansing (implied)
- Advocate for inclusive and diverse communities (implied)
- Support organizations working to prevent hate crimes and discrimination (implied)

# Whats missing in summary

Beau's emotional delivery and depth of analysis can be best appreciated by watching the full transcript.

# Tags

#EthnicCleansing #SpeakingOut #Prevention #CommunityAction #History