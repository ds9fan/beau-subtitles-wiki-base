# Bits

Beau says:

- History books are created from the fingerprints of history, like letters, photographs, and government records.
- Historians shape history books to appeal to readers, often judging the past by today's standards.
- People of the future will judge us based on the standards of tomorrow, wondering why we didn't act on issues like the environment or political systems.
- Digital records today will shape how future generations view us, similar to how Thomas Jefferson is viewed for his actions and inactions.
- Society today is often busy but accomplishing little, confusing motion with progress.
- Our actions today will leave behind fingerprints for future generations to judge us by.

# Quotes

- "They're going to wonder why we didn't act for the environment."
- "We spend a lot of time being entertained by stuff that is mindless."
- "We know that a lot of the stuff going on around us is wrong. But we're not doing much to solve it."
- "Our fingerprints will be there for them to look at and the society we leave them."
- "Society today is often busy but accomplishing little, confusing motion with progress."

# Oneliner

Beau examines how history books are shaped by the fingerprints of history and how future generations will judge us based on our actions and inactions today.

# Audience

History enthusiasts, future generations

# On-the-ground actions from transcript

- Document your actions for future generations to learn from (implied)
- Actively work towards solving societal issues (implied)

# Whats missing in summary

The full transcript provides a reflective look at how history is shaped and how our present actions will be judged by future generations.