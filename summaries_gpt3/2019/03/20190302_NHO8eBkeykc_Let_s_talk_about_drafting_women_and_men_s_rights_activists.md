# Bits

Beau says:

- Men's rights activists are viewed negatively by society and the activist community for reasons like not wanting to pay child support or using their children as leverage.
- An MRA group in Texas filed a lawsuit claiming that the draft was discriminatory and won in federal court.
- The Supreme Court used the argument of women not being allowed in combat as a reason to disallow them from the draft, a ruling from the 80s.
- MRAs believe that if women were subject to the draft, they'd be more vocal in ending it, leading to its sooner end.
- Beau argues that the draft is immoral, using government violence to force people to fight is wrong, and not a modern-age responsibility.
- Beau points out the inconsistency of opposing the draft while advocating to draft women to fight, exposing them to government violence.
- He stresses the importance of ideological consistency in building a movement and seeking equality in freedom, not oppression.
- Beau criticizes the strategy employed by MRAs as akin to seeking equality in oppression, contrasting it with seeking freedom.
- Despite the controversy around the draft, a military commission is likely to advise the Department of Defense to end the Selective Service.
- Beau concludes by condemning the approach of taking hostages to achieve goals, urging for a stand without resorting to harming the innocent.

# Quotes
- "You seek equality in freedom."
- "You've taken hostages in an attempt to get what you want. You're willing to hurt the innocent, those that aren't complicit in it, to get what you want."
- "If you want to take a stand, take a stand. Don't take hostages."

# Oneliner
Men's rights activists challenge the draft's discrimination but risk harming innocents in their strategy, while advocating for equality in freedom.

# Audience
Activists, Advocates, Critics

# On-the-ground actions from transcript
- Advocate for ideological consistency within movements (implied)
- Stand against harmful tactics and advocate for equality without resorting to violence or harm (implied)

# Whats missing in summary
The full transcript provides a comprehensive breakdown of the controversy surrounding men's rights activists and the draft, delving into the strategy's implications and advocating for ideological consistency within movements. For a deeper understanding of the topic and Beau's perspectives, watching the full video is recommended.

# Tags
#Men'sRightsActivists #SelectiveService #Draft #GenderEquality #EqualityInFreedom