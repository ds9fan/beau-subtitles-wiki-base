# Bits

Beau says:

- Addresses questions about terrorism and non-state actors.
- Talks about a terrorist action committed by the government.
- Emphasizes the importance of definitions in understanding propaganda.
- References St. Augustine's story about Alexander the Great and a pirate to make a point about morality and legality.
- Compares street gangs collecting protection money to taxation.
- Draws parallels between a suicide bomber and a drone operator in terms of morality.
- Argues that legality does not equate to morality.
- Mentions historical instances where atrocities were legal under respective laws.
- Stresses the role of power in determining what is legal and not morality or social consensus.
- Encourages watching a related video to further understand the topic.

# Quotes

- "The technological advance doesn't somehow make it more moral. Just makes it legal."
- "Everything the Taliban did, well that was legal under their laws."
- "There's no difference in real life. It's the same thing."
- "Size doesn't matter. At least not in this case."
- "It's power."

# Oneliner

Beau addresses the blurred lines between legality and morality, drawing parallels between different actions and urging a critical examination of power dynamics.

# Audience

Ethical thinkers, justice advocates.

# On-the-ground actions from transcript

- Watch the suggested video to deepen understanding (suggested).

# Whats missing in summary

The tone and nuances of Beau's delivery can be best experienced by watching the full video.