# Bits

Beau says:

- Defines terrorism as a strategy to provoke an overreaction from the establishment.
- Explains how terrorists aim to make themselves relatable to gain support.
- Talks about fascists being hard to offend or make relatable.
- Analyzes a manifesto's brilliance in making the author relatable through nods to different groups.
- Mentions the ideological journey from left to right and doubts its authenticity.
- Points out the existence of a libertarian to fascist pipeline.
- Urges libertarians to address and clean out radical elements within their group.
- Emphasizes the importance of not getting defensive when discussing these issues.
- Stresses the need to keep terrorist groups marginalized and unrelatable.
- Connects the importance of keeping terrorists unrelatable to preventing radicalization and violence.
- Mentions the founding fathers as historical examples of terrorists.
- Suggests that pointing out flaws in terrorist ideologies and keeping them unrelatable is critical in combating terrorism.

# Quotes

- "Hang them from lamp posts. Nobody cares, they're fascists."
- "You've got to keep that wall up. It's the most important thing you, as an average citizen, can do to combat terrorism."
- "It's extremely important that you do this. Because if this group becomes relatable to the average person, people start to radicalize."
- "The founding fathers were terrorists, textbook."
- "Point out the flaws in the ideology and make sure they stay unrelatable."

# Oneliner

Beau explains terrorism as a strategy to provoke overreactions, urges keeping terrorist groups unrelatable to combat radicalization and violence.

# Audience

Average citizens, libertarians.

# On-the-ground actions from transcript

- Address and clean out radical elements within libertarian groups (implied).
- Keep terrorist groups marginalized and unrelatable to prevent radicalization and violence (implied).
- Point out flaws in terrorist ideologies to combat terrorism (implied).

# Whats missing in summary

Importance of understanding terrorist strategies and the role of relatability in radicalization.

# Tags

#Propaganda #Terrorism #Radicalization #CombattingViolence #IdeologicalJourney