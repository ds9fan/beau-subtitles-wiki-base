# Bits

Beau says:

- Secretary of Homeland Security requested authority to deport unaccompanied minors crossing the border, transitioning from baby prisons to baby deportations.
- Five-month-olds have been in legal proceedings, requiring crayons for them to draw since they can't write a statement.
- Administration policies are making the journey more dangerous, like a real-life Hunger Games.
- Reuniting deported children with their families poses challenges as their families might be the reason they fled.
- There's a moral dilemma in sending children back to dangerous situations or taking more drastic measures.
- Calls for militarization of the border to protect against children reveal underlying fears of losing dominance.
- Fear of the browning of America drives some to extreme measures, like calling for military intervention against children.
- The pervasive fear of losing dominance leads to irrational calls for military protection from perceived threats.
- The irony of calling for military intervention against children in the "land of the free and home of the brave."
- Beau shares a powerful anecdote about a Palestinian woman facing off against an IDF trooper, questioning if a similar scene will unfold on American soil.
- Deporting children may prevent them from being sexually abused in custody, but it doesn't address the root problem.
- Beau urges America to confront its contradictions in claiming to be the land of the free while seeking military protection from toddlers.

# Quotes

- "We want to maintain the idea that we are still the land of the free and the home of the brave."
- "Your ignorance of American foreign policy and the impacts it has doesn't mean it's not really our problem, it just means you're too ignorant to know it."
- "Land of the free, home of the brave."
- "Fear of white people who are afraid to lose their place in this country."
- "We cannot continue to call for the U.S. military to protect us from toddlers."

# Oneliner

Secretary of Homeland Security seeks to deport unaccompanied minors, revealing deeper fears and contradictions in the "land of the free and home of the brave."

# Audience

Advocates for migrant rights

# On-the-ground actions from transcript

- Advocate for humane treatment and fair legal proceedings for unaccompanied minors (implied)
- Educate others on the impacts of American foreign policy on migration (implied)
- Challenge calls for militarization of borders and protection against children (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of the moral and ethical implications of deporting unaccompanied minors and the underlying fears driving extreme responses.

# Tags

#Migration #HumanRights #Fear #Militarization #AmericanIdentity