# Bits

Beau says:

- Momo, a meme causing panic, is actually a joke known to kids for a year and a half.
- Children understand it's a joke and find parents' reactions hilarious.
- The fear around Momo reveals a lack of parental involvement in children's lives.
- Parents often use technology as a babysitter, unaware of what's on their kids' phones.
- Modern society values work over family time, leading to disconnection.
- Beau suggests that Momo's scare might have helped people focus more on their kids.
- He points out statistics on teen struggles like suicide attempts, drug use, and pregnancies.
- These real-life issues are much scarier than the Momo meme.
- Beau implies that the moral of the Momo story is to be more involved with our children.
- He encourages viewers to prioritize spending time with their kids.

# Quotes

- "We don't really spend enough time with our kids."
- "We sell hours of our life every day to some faceless entity at the expense of our families."
- "Maybe Momo is a good thing. Maybe like most good stories, it has a moral."
- "Become a little bit more involved if you can."
- "So maybe Momo is a good thing. Maybe like most good stories, it has a moral."

# Oneliner

Momo meme panic reveals lack of parental involvement, urging focus on real teen struggles over fictional scares.

# Audience

Parents, caregivers, families

# On-the-ground actions from transcript

- Spend quality time with your kids regularly (suggested)
- Monitor your children's online activities (suggested)

# Whats missing in summary

The full transcript provides a deeper exploration of the societal issues surrounding parental involvement and children's exposure to technology, along with statistical comparisons to illustrate the real dangers faced by teens.

# Tags

#Parenting #Children #Meme #TeenStruggles #FamilyTime