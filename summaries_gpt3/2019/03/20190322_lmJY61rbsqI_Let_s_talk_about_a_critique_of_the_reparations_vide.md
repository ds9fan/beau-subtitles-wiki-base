# Bits

Beau says:

- Beau was watching YouTube videos while feeling under the weather and stumbled upon a video critiquing reparations, prompting him to respond.
- The video he watched critiqued the reparations proposal, focusing on the $100 billion amount mentioned, which Beau believes is not sufficient given the true value of slavery.
- Beau appreciates the video's approach of sparking a constructive conversation and wants to contribute to the discourse.
- He suggests a tweak to the proposed tax exemption for reparations, advocating for a refundable tax credit instead to benefit those at the bottom economically.
- Beau sees tax credits as more politically feasible than direct dollar reparations funds, as it could garner support from unexpected allies who oppose taxes.
- Implementing tax credits for reparations is seen as more achievable and effective compared to other reparations plans, making it a practical and timely solution.
- Beau values the importance of turning criticisms into productive dialogues, which is the essence of his channel.
- He concludes by encouraging further reflection and engagement on the topic of reparations.

# Quotes

- "A Nazi wants to talk, let's talk."
- "Rather than tax exemption, what about a tax credit, a refundable tax credit more importantly?"
- "Taxation is theft, man."
- "You will find people that would march against you in the street supporting this just based on the idea that it's cutting down on government extortion."
- "This is something that if with the right amount of organizing could be done pretty quickly."

# Oneliner

Beau engages in a thoughtful response to a reparations video, proposing a refundable tax credit over tax exemption as a more feasible and beneficial approach to reparations, fostering unexpected allies through tax credits.

# Audience

Tax Reform Advocates

# On-the-ground actions from transcript

- Advocate for implementing refundable tax credits for reparations (suggested)
- Organize grassroots movements around the idea of tax credits for reparations (implied)

# Whats missing in summary

Beau's detailed analysis and proposal for using tax credits as a more practical approach to reparations can be fully appreciated by watching the entire transcript.

# Tags

#Reparations #TaxCredits #SocialJustice #CommunityOrganizing #PolicyChange