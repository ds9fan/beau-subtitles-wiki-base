# Bits

Beau says:

- Explains an incident in Indiana where teachers were mock executed during an active shooter drill.
- Clarifies that the incident was not part of the ALICE training program and expresses doubt that it was authorized by them.
- Criticizes the lack of effective training programs for active shooter situations and the mentality needed for such training.
- Describes how teachers were taken into a room, lined up against a wall, and shot in the back with a high-powered airsoft gun as part of the drill.
- Questions the effectiveness of using fear and pain in training teachers to respond to active shooter situations.
- Raises concerns about whether teachers were aware they could fight back during the drill.
- Criticizes the reactive nature of the training and suggests a more proactive approach involving psychologists to identify warning signs.
- Condemns the use of violence as a solution and points out the potential impact on students' mental health and well-being.
- Urges for a reevaluation of current training methods for teachers in dealing with school shootings.

# Quotes

- "I wonder where the school shooters get the idea from us, from us, because violence is easy."
- "Just kill them, two in the chest, one in the head. Blood makes the grass grow green."
- "If we keep this posture, we can't be surprised when there are more small caskets because of an event at a school."

# Oneliner

Beau criticizes the mock execution of teachers during an active shooter drill in Indiana, questioning the effectiveness and ethics of such training and advocating for a more proactive approach to school safety.

# Audience

Teachers, educators, school administrators

# On-the-ground actions from transcript

- Advocate for comprehensive and trauma-informed training programs for teachers (implied)
- Support initiatives that focus on proactive measures such as identifying warning signs of potential violence (implied)

# Whats missing in summary

The emotional impact on teachers and students involved in such drills and the potential long-term consequences on mental health and well-being.

# Tags

#SchoolSafety #TeacherTraining #ViolencePrevention #ProactiveApproach #EducationSafety