# Bits

Beau says:

- Questions the essence of libertarianism, describing it as freedom within societal boundaries and complete deregulation.
- Points out that libertarians believe legality is not equivalent to morality and value action over words.
- Contrasts the belief that anyone can achieve greatness through self-improvement in an ideal society with the reality of inherited wealth among today's affluent.
- Suggests Harriet Tubman as an alternative icon of freedom due to her journey from slavery to capability and action, understanding of morality, and fight against oppressive labor practices.
- Questions why Harriet Tubman is not celebrated by libertarians despite embodying values like freedom and empowerment over wealth accumulation.
- Encourages libertarians to shift their focus from idolizing the wealthy to guiding others towards freedom and assisting those in need, akin to Tubman's legacy.

# Quotes

- "It's freedom. As much freedom as is humanly possible within the confines of the society."
- "Action is more important than words."
- "Why on earth do Libertarians idolize the wealthy?"
- "If it's really about freedom, it would certainly seem that Harriet Tubman would be one of the ideals."
- "Seems like that [giving a hand up when needed] would be more in line with the rhetoric from libertarians."

# Oneliner

Beau questions libertarian principles, proposing Harriet Tubman as a better symbol of freedom and urging a shift from wealth idolization to aiding others.

# Audience

Libertarians

# On-the-ground actions from transcript

- Guide people to freedom and assist them when needed (implied)

# Whats missing in summary

The full transcript delves into the dichotomy between libertarian values and the idolization of wealth, prompting reflection on true freedom and empowerment.