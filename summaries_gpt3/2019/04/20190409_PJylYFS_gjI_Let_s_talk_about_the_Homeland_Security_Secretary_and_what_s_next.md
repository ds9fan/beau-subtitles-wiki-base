# Bits

Beau says:

- Reports indicate the former Homeland Security Secretary resigned because she refused to continue breaking the law for Trump.
- Trump referred to family separation as a deterrent, which Beau sees as a civil rights violation and cruel and unusual.
- A new Homeland Security Secretary is needed, someone willing to betray American values and take orders from Miller.
- Congress's role is to act as a check against executive power and stop potential dictatorships.
- Supporting Trump is supporting authoritarian rule and going against the Constitution.
- Soldiers don't fight for a flag or a song but for the ideals of the Republic and representative democracy.
- Beau criticizes uninformed patriotic citizens who have fallen for propaganda and sold out their country.
- He points out that working-class conservatives often vote for billionaires who don't truly represent their interests.
- Beau urges those not under Trump's spell to speak up against dangerous rhetoric and propaganda.
- He warns about the constant blame on immigrants and the need to call it out to prevent further deterioration.

# Quotes

- "A vote for Trump is a vote for authoritarian rule."
- "You have farmed off your sovereignty, farmed off your individual responsibility to some politician and you just believe whatever they say."
- "There's never been a democracy that hasn't committed suicide."
- "Soldiers don't go die for a flag or a song. In theory, they go fight for an ideal."
- "It's blindingly apparent, but people still support him."

# Oneliner

Former Homeland Security Secretary resigns over refusing to break the law for Trump, urging for vigilant defense against authoritarian rule and propaganda.

# Audience

Citizens, Activists

# On-the-ground actions from transcript

- Speak up against dangerous rhetoric and propaganda every time you encounter it (implied)
- Call out those supporting authoritarian rule without fail (implied)
- Educate yourself on political matters and history to understand the implications of supporting certain leaders (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the dangers of supporting authoritarian rule, urging citizens to be vigilant and speak out against propaganda to protect democracy.

# Tags

#AuthoritarianRule #Propaganda #Democracy #CallToAction #CivilRights