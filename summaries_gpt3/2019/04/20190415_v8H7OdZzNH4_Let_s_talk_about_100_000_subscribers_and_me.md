# Bits

Beau says:

- Started as a journalist downplaying his accent, which changed after colleagues discovered his Southern roots during a drinking session in 2016.
- Made videos showcasing his accent and redneck persona, which garnered more attention and acceptance for his social commentary.
- Initially played up his Southern accent but gradually reverted to his real accent, except for satire videos where he exaggerates it for humor.
- Clarified his background, confirming he is from the South and has lived in various southern states.
- Addressed false claims about being a convicted human trafficker, attributing them to libelous allegations from 2013.
- Shared insights into his past as a consultant contractor and emphasized always being a civilian, despite misconceptions about his military involvement.
- Responded to common questions about his personal life, activism, and philosophy, including clarifying his approach to conveying ideas over using specific terms.
- Encouraged viewers to find ways to help their communities based on their skills and interests, without needing to adopt a militant approach.
- Explained the purpose of his videos, discussing the difficulty of staying informed and avoiding scripted content in his delivery.
- Mentioned creating a playlist of music he listens to and thanked his audience for their support.

# Quotes

- "I don't think that the message should be clouded because somebody did something you think is cool or something that you hate, it shouldn't matter."
- "Figure out what your skill set is, I assure you it's needed. Get out there."
- "I don't use that word. That word is very loaded. You say that word, people picture teenagers in leather jackets with spiky hair throwing rocks at cops."

# Oneliner

Beau shares his journey from downplaying his accent to embracing his Southern roots, addressing misconceptions, and encouraging community engagement based on individual skills and interests.

# Audience

YouTube subscribers

# On-the-ground actions from transcript

- Reach out to various news networks like Greed Media or Pontiac Tribune based on interests (suggested)
- Find ways to get involved in community activism using your unique skill set (exemplified)

# Whats missing in summary

Details on Beau's specific journalism projects and the impact of community engagement on social change.

# Tags

#SouthernRoots #Journalism #CommunityEngagement #Activism #Misconceptions