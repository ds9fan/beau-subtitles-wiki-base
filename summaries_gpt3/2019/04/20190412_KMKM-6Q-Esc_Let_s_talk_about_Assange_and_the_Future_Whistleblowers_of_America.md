# Bits

Beau says:

- Assange is in custody and will be prosecuted, tried, and sentenced in the federal system.
- The focus should be on what future whistleblowers are learning from recent incidents involving Winter, Hammond, Karyaku, Snowden, and Manning.
- Whistleblowers are left with limited options due to lack of protection by the press or anonymous leak methods.
- Going to a foreign power becomes the only option for whistleblowers to disclose information.
- The US press complies with the US intelligence community in protecting means and methods.
- Disclosing means and methods can lead to serious consequences, including loss of life.
- US media's compliance with intelligence services shifts focus from illegal activities to the leak itself.
- The government's response to leaks should prioritize legal and moral behavior to prevent future incidents.
- The battle between disclosure of activities like waterboarding and means and methods obtained from reports will have serious implications in counterintelligence wars.

# Quotes

- "The U.S. government, their big brother in 1984, and like Winston said, they get you. always get you."
- "You can't go to the press. The press cannot protect you."
- "But see, now that they're going to end up going to a foreign power, that reporter, he doesn't have that patriotism."
- "We really need you to keep this secret. If you don't, this person will die."
- "We need to set aside our desire for punishment when it comes to this."

# Oneliner

Beau warns about the dangerous implications of whistleblowing and the limited options available due to lack of protection from the press, leading to potential loss of life by disclosing means and methods.

# Audience

Whistleblowers, journalists, activists

# On-the-ground actions from transcript

- Support and advocate for legal protections for whistleblowers (implied)

# Whats missing in summary

Importance of advocating for legal protections and ethical behavior in response to whistleblowing incidents.

# Tags

#Whistleblowers #PressFreedom #GovernmentAccountability #IntelligenceCommunity #Ethics