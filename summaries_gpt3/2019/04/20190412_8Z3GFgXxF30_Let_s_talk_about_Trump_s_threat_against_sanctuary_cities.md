# Bits

Beau says:

- Trump is considering releasing detained migrants into sanctuary cities, waiting for approval from President Miller.
- Beau urges Trump to stop considering and just do it, challenging his courage.
- He mentions that migrants statistically create less crime than native-born Americans.
- Beau questions the intention behind sorting violent migrants and the harm it may cause American citizens.
- He encourages Trump to bus in and drop off migrants in sanctuary cities.
- Beau expresses support for the idea, believing it will lower the crime rate and get migrants out of cages.
- He humorously points out that cities don't have walls around them, alluding to the lack of segregation.
- Beau concludes with a parting thought for his audience to have a good day.

# Quotes

- "Do it. Bus them in. Drop them off. It's fantastic."
- "Get them out of the cages, man."
- "But you do understand that these cities don't have walls around them, right?"
- "Stop considering it. Do it."
- "Y'all have a good day."

# Oneliner

Beau urges Trump to bus detained migrants into sanctuary cities to lower crime rates and challenges his courage, humorously pointing out the lack of walls around cities.

# Audience

Activists, concerned citizens

# On-the-ground actions from transcript

- Bus detained migrants into sanctuary cities (exemplified)
- Drop them off in sanctuary cities (exemplified)

# Whats missing in summary

The full video provides further context and elaboration on the impact of releasing detained migrants into sanctuary cities.