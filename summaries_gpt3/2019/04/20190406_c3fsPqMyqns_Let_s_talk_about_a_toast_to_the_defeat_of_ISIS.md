# Bits

Beau says:

- Winding down his Friday night, contemplating a toast.
- Questions who to toast between President Trump, Department of Defense brass, and others.
- Criticizes politicians for making false statements to score political points.
- Suggests toasting the veterans of future wars.
- Mentions American soldiers and Kurdish soldiers killed after ISIS was declared defeated.
- Acknowledges ongoing threats and attacks by terrorist organizations.
- Criticizes the Department of Defense for putting the situation back to 2002 Afghanistan.
- Expresses frustration and disbelief at the current state of affairs.

# Quotes

- "Terrorist organizations do not need land to operate."
- "This isn't over, it's not over."
- "After all of this time, you have basically put us back in the situation we ran in Afghanistan in 2002."

# Oneliner

Beau contemplates a toast while criticizing false political statements, acknowledging ongoing threats from terrorist organizations, and expressing frustration at the current situation.

# Audience

Politically aware individuals.

# On-the-ground actions from transcript

- Support veterans and current soldiers (implied)
- Stay informed about political statements and actions (implied)

# Whats missing in summary

Full context and emotional depth.

# Tags

#USPolitics #Counterterrorism #ISIS #Military #CurrentEvents