# Bits

Beau says:

- Drones are neutral technology but can be used for good or evil purposes.
- The U.S. drone program is portrayed as surgical strikes targeting command and control.
- Around 90% of those killed in drone strikes were not the intended targets.
- There is a possibility that some of the 90% killed were opposition forces near the target.
- The drone program has hit wedding processions and killed American citizens.
- Insurgency and terrorism thrive on overreactions from the U.S. government.
- For every opposition fighter killed, there is a family member or innocent ready to retaliate.
- The collateral rate in Afghanistan is high despite solid intelligence assets.
- Parents in Pakistan have withdrawn children from school due to fear of drone strikes.
- The secrecy and lack of transparency surrounding the drone program breed insurgency.
- The drone program is currently used as an assassination tool rather than surgical strikes.
- The lack of transparency suggests a potential increase in drone usage and collateral rates.
- Beau advocates for drones to be used similarly to manned aircraft missions, except for surveillance.
- The fear instilled by drone strikes, not freedom or culture, drives hostility towards the U.S.

# Quotes

- "They want to kill us because we've made them fear the sky."
- "The drone program is not being used as surgical strikes. It's being used as an assassination tool."
- "It's secret, it's ineffective, and it's breeding insurgency."
- "For every opposition fighter we take out, there's the family member of somebody we killed, some innocent that we killed, ready to pick up arms."
- "They want to kill us because we've made them fear the sky."

# Oneliner

Beau explains the flaws of the U.S. drone program: secrecy, collateral damage, and breeding insurgency, urging for transparency and responsible use.

# Audience

Policy makers, activists, citizens

# On-the-ground actions from transcript

- Advocate for increased transparency and oversight of the drone program (implied)
- Support initiatives demanding accountability for drone strikes (implied)
- Raise awareness about the consequences of drone strikes on innocent civilians (implied)

# Whats missing in summary

Deeper insights into the ethical implications and consequences of drone warfare, and the need for a reevaluation of current drone usage policies.

# Tags

#Drones #DroneProgram #USForeignPolicy #Transparency #Accountability #Insurgency