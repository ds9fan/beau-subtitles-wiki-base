# Bits

Beau says:

- Responding to a question from a heterosexual man who was briefly attracted to a trans woman and wondered if that makes him gay.
- Advocating against using terminology that can harm the transgender community.
- Pointing out the absurdity of questioning one's sexuality based on brief attraction to a trans woman.
- Suggesting that being overly concerned about your sexuality might be a better indication of being gay.
- Emphasizing that being attracted to someone who is trans does not automatically mean you are gay.
- Asserting that being gay is not a negative thing and should not be stigmatized.
- Encouraging individuals to embrace who they are and live their lives without worrying about societal norms or labels.
- Urging people to let go of unnecessary concerns about their sexuality and just be themselves, regardless of societal expectations.
- Reminding everyone that it is 2019 and people's sexual orientation should not matter to those who truly care about them.
- Encouraging individuals to embrace their uniqueness and live authentically without fear of judgment or stereotypes.

# Quotes

- "Stop with this. That is not good terminology because it reinforces the very question you're asking."
- "Just be you, live your life, and let go of all of this."
- "Embrace the weird, okay? Just live your life."

# Oneliner

Beau addresses misconceptions about sexuality, advocating for self-acceptance and rejecting harmful terminology in 2019.

# Audience

All individuals questioning their sexuality or facing societal pressures.

# On-the-ground actions from transcript

- Embrace your uniqueness and live authentically, disregarding societal norms (exemplified).
- Let go of unnecessary concerns about your sexuality and just be yourself (exemplified).

# Whats missing in summary

The nuances and depth of Beau's message on acceptance and self-discovery can best be experienced by watching the full video.

# Tags

#Sexuality #LGBTQ+ #SelfAcceptance #GenderIdentity #SocietalNorms