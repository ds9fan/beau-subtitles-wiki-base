# Bits

Beau says:

- A book was read in school, and a parent got outraged because the historical figure was gay.
- The outrage stemmed from discomfort with discussing gay people in school.
- Beau questions what will happen when children learn about uncomfortable topics like genocide or nuclear war if discussing a historical figure being gay is uncomfortable.
- Beau provides examples of significant historical figures who were gay, such as Alexander the Great, Julius Caesar, Michelangelo, Sally Ride, and many others.
- He argues that excluding the mention of gay people in history erases significant contributions and diminishes the richness of historical knowledge.
- Beau points out that homosexuality has existed for thousands of years and doesn't need to be "normalized" as it is already a normal part of society.
- He offers to have a face-to-face talk show to address the issue with those uncomfortable discussing gay historical figures.
- Beau underscores the importance of recognizing the presence and contributions of gay people throughout history, urging people to accept it as a part of reality.

# Quotes

- "History would get really, really, really bare if we didn't talk about gay people."
- "It's almost like they're here and you need to get used to it."

# Oneliner

A parent's outrage over a gay historical figure prompts Beau to challenge the discomfort with discussing LGBTQ+ history, stressing its integral role in shaping our past and present.

# Audience

Teachers, parents, students

# On-the-ground actions from transcript

- Educate yourself and others on LGBTQ+ history (implied)
- Advocate for inclusive and diverse historical education in schools (implied)

# Whats missing in summary

The emotional impact and depth of understanding LGBTQ+ history and its significance in educational settings.

# Tags

#LGBTQ+ #History #Education #Inclusivity #Acceptance