# Bits

Beau says:

- Deputies in Broward County beat up some kids, sparking a debate on the use of force.
- A key moment in the video is when an officer pepper-sprayed a kid who appeared to be picking something up off the ground.
- The focus should not only be on the excessive force used by one officer but also on the actions of the officer who pepper-sprayed the kid.
- The officer who pepper-sprayed the kid demonstrated deliberate and trained movements suggesting a proper technique for control without excessive force.
- Beau points out that the department may have proper training procedures, but these were not followed in the incident.
- The officer who pepper-sprayed the kid appeared calm and in control, contrasting with the excessive force used by another officer.
- Despite the questionable actions, it is suggested that the department's policy might justify the use of force as seen in the video.
- Beau stresses the importance of scrutinizing the actions of all officers involved in such incidents, not just those directly using excessive force.
- There is a need to focus on changing policies rather than solely seeking justice for individual cases to prevent such incidents from happening in the future.
- The situation is complex, with many unknown factors, but it is vital to analyze and address the systemic issues contributing to police brutality.

# Quotes

- "You can maintain control of a suspect without smashing their face into the concrete."
- "What it appears to me is that this department has been trained in the right way to do this."
- "The officer who pepper sprayed him was attempting to do it the right way."
- "It's very clear from the video."
- "There's a whole lot we don't know, but that's a pretty important piece that I don't see getting discussed."

# Oneliner

Deputies beating kids in Broward County reveal systemic issues beyond just excessive force, urging a focus on training and policy reform over individual justice.

# Audience

Police reform advocates

# On-the-ground actions from transcript

- Advocate for policy changes within law enforcement departments to ensure proper training and accountability (exemplified)
- Support initiatives that address systemic issues leading to police brutality (implied)

# Whats missing in summary

The full video provides additional context and details that can enhance understanding of the incident and the broader implications for police accountability.

# Tags

#PoliceBrutality #UseOfForce #PoliceTraining #PolicyReform #Accountability