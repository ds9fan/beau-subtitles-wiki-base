# Bits

Beau says:
- Introduces the topic of Yemen and focuses on the legal framework of U.S. arms transfers.
- Explains the importance of understanding the legalities separate from Trump's veto on a resolution.
- Points out the attempt by the U.S. government to establish a moral framework within arms export laws.
- Describes the dire humanitarian crisis in Yemen, comparing it to one of the worst since World War II.
- Talks about the indiscriminate killing of civilians, especially children dying of starvation.
- Examines U.S. laws like the Arms Export Control Act, Foreign Assistance Act, Presidential Policy Directive 27, and CATP regarding arms transfers.
- States that the U.S. is violating its own laws by transferring arms to Saudi Arabia for use in Yemen.
- Addresses the constitutional issue of Congress's authority over making war and supplying arms in conflicts.
- Raises questions about allowing a president to bypass laws and stresses the importance of upholding the nation's laws.

# Quotes

- "We're violating our own laws by doing it."
- "Do we allow a president to just run roughshod over laws?"
- "It was illegal then, and it's illegal now, and it needs to stop."

# Oneliner

Beau delves into Yemen's crisis, dissecting U.S. arms transfer laws, revealing violations, and questioning presidential authority.

# Audience

Policy makers, activists

# On-the-ground actions from transcript

- Contact your representatives to urge them to uphold laws on arms transfers (implied)
- Support organizations advocating for human rights and peace in Yemen (implied)

# Whats missing in summary

Deeper insights into the moral implications of U.S. arms transfers to Saudi Arabia for the Yemen conflict.

# Tags

#Yemen #USLaws #ArmsTransfers #HumanRights #ForeignPolicy