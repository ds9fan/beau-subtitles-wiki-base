# Bits

Beau says:

- Warning against de facto open borders under Trump's presidency, despite no one openly advocating for it.
- Trump's plan for processing immigrants resembles Ellis Island-style immigration.
- Fox News and mayors are unknowingly supporting de facto open borders due to Trump's manipulation.
- Trump's history of flip-flopping on issues like gun control and his current stance on immigration.
- Urging Democrats to prioritize freeing people from cages over political games.
- Suggesting that conservatives are being played by Trump into unintentionally supporting open borders.
- Emphasizing the need to maintain the appearance of opposition to de facto open borders.
- Speculating whether Trump is deceiving conservatives or simply acting impulsively.
- Stating that the ultimate goal is to get people out of cages, regardless of political maneuvers or motivations.

# Quotes

- "We're on the verge of the first open borders presidency."
- "He's advocating a return to Ellis Island-style immigration."
- "We have innocent people in cages."
- "He's just a man-child throwing a temper tantrum."
- "At the end of the day, we're going to get people out of cages and that's what matters."

# Oneliner

Beau warns of de facto open borders under Trump's presidency, exposing the unintentional support from conservatives, urging action to free caged individuals.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Advocate for freeing individuals from cages (implied)
- Stay informed and aware of political manipulations (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's immigration policies and their potential consequences, urging action to prioritize human rights over politics.

# Tags

#Immigration #Trump #PoliticalManipulation #HumanRights #DeFactoOpenBorders