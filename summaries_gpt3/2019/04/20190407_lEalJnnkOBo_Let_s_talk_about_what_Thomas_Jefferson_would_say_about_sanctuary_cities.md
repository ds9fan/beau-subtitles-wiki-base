# Bits

Beau says:

- Mentioning sanctuary states and the Supremacy Clause.
- Recalling an unofficial war with France in 1798 due to unpaid debts.
- Explaining the Alien Friends and Alien Enemies Act of 1798.
- Referencing Thomas Jefferson's Kentucky resolutions of 1798.
- Explaining Jefferson's views on federal power over naturalization and immigration.
- Citing the 10th Amendment regarding states' powers.
- Asserting that Jefferson supported the idea of sanctuary states.
- Emphasizing the importance of historical context and accuracy.
- Warning against distorting history for convenience.
- Questioning appeals to the Founding Fathers' perspectives.
- Beau's closing remarks on historical authenticity.

# Quotes

- "History is a thing. It exists. You can't just change it and repackage it because you want to."
- "When you make an appeal to the Founding Fathers and their views on things, understand that most of that stuff was written down."
- "Thomas Jefferson, well he didn't even think I should exist."

# Oneliner

Beau explains the historical context behind sanctuary states and challenges distorted interpretations of history.

# Audience

History enthusiasts, policymakers, activists.

# On-the-ground actions from transcript

- Fact-check historical claims and narratives (implied).

# Whats missing in summary

Beau's engaging delivery and historical anecdotes bring clarity to the debate on sanctuary states and historical accuracy.

# Tags

#SanctuaryStates #History #FoundingFathers #ThomasJefferson #DebunkingMisconceptions