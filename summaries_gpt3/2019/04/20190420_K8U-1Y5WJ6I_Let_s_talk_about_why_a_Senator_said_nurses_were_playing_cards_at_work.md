# Bits

Beau says:

- Nurses are like the mafia in how quickly they spread news and defend each other.
- A Washington state senator opposed a bill for nurses' mandatory breaks, claiming they spend time playing cards.
- The senator's top campaign contributor is the Washington State Hospital Association.
- The hospital association posted about unintended consequences of the bill on April 8th.
- The senator proposed an amendment limiting nurses to work 8 hours in a 24-hour period with no exceptions.
- The hospital association claimed no knowledge of this amendment, suggesting possible scripting.
- Beau questions the coincidence and suggests the senator might be parroting hospital association interests.
- American politics are criticized for being influenced by money, where politicians act in favor of their donors.
- The senator's actions raise concerns about legislators being swayed by financial interests rather than representing the people.
- Beau recommends examining how money impacts legislators and their decision-making processes.

# Quotes

- "Nurses are like the mafia."
- "I mean because I know what to do. Anytime a politician says something dumb all you have to do is go look at their campaign contributions and you'll find out why they said it."
- "I have never seen anything like this before."
- "There's so much money in it that it's that easy to find out why your representative, who is supposed to be your representative, is proposing the bills and amendments that they are and why they're voting the way they are."
- "I think we should really take a look at how money is affecting our legislators."

# Oneliner

Nurses unite against a senator's opposition to mandatory breaks, revealing potential scripting by donors in American politics.

# Audience

Voters, Activists, Nurses

# On-the-ground actions from transcript

- Question your representatives about their campaign contributions and how they may influence their decisions (implied)

# What's missing in summary

The full transcript provides a detailed example of how money influences politics and the importance of holding representatives accountable.