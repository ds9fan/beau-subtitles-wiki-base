# Bits

Beau says:

- Challenges viewers to imagine a Comanche warrior instead of a helicopter.
- Traces the historical connection between dogs and human culture through campfires.
- Emphasizes the importance of understanding American culture as constantly evolving.
- Comments on the older American culture of the Comanche tribe and their adaptation to new technologies.
- Describes how horses transformed the way of life for the Comanche people.
- Points out that newcomers have always influenced and enriched American culture.
- Raises concerns about the current resistance towards newcomers in America, drawing parallels with past immigrant groups.
- Criticizes the prioritization of avocados over people in the context of border debates.

# Quotes

- "I have a feeling that if horses and dogs can change cultures so drastically, so much, that people can too."
- "I have a feeling that if horses and dogs can change cultures so drastically, so much, that people can too."
- "Every group of people has that newcomer, has that thing that they need to accept for their culture to continue, to grow, otherwise it stagnates."
- "The sad part is, as we talk about closing the border, Americans are more concerned about avocados than people."

# Oneliner

Beau challenges viewers to rethink American culture by reflecting on the historical influences of newcomers like the Comanche tribe and urges acceptance and growth for a better future.

# Audience

Americans

# On-the-ground actions from transcript

- Accept newcomers for cultural enrichment (implied)
- Prioritize people over commodities like avocados (implied)

# Whats missing in summary

Beau's passionate delivery and historical anecdotes can provide a deeper understanding of the impact of newcomers on American culture.