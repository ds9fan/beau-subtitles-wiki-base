# Bits

Beau says:

- Describes spirals carved into cliffs and stones in the southwestern United States, representing an ancient hero's journey and quest.
- Recounts the Hopi creation story heard around a campfire, detailing the emergence of the Hopi people from the earth after their world was destroyed.
- Mentions the requirement placed upon the Hopi people by the caretaker to protect the earth and prevent the same destruction caused by greed.
- Talks about the quest assigned to the Hopi people by the caretaker to find center and build their society there, marked by spirals in different directions.
- Expresses admiration for the story as a creation myth where man has always sought balance and center, a theme relevant even today.
- Draws parallels between the ancient Hopi journey and a potential future journey of people emerging from the earth into a new world.

# Quotes

- "Every relationship, every society, every civilization is still seeking that."
- "Man was seeking center, trying to find balance."
- "I think of future people on a hero's journey because their world was destroyed."
- "I wonder if the caretaker of tomorrow will be as hospitable."
- "Given my views, it is not entirely lost on me that the migration ended in the southwestern United States."

# Oneliner

Beau recounts the Hopi creation story and its relevance today, reflecting on the quest for balance and center in society, both past and potentially future.

# Audience

History enthusiasts, culture appreciators

# On-the-ground actions from transcript

- Share and preserve indigenous creation stories through storytelling events (implied)

# Whats missing in summary

The emotional impact of the Hopi creation story and its resonance with modern challenges.