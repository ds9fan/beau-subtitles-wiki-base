# Bits

Beau says:

- Explains why it's detrimental for the US military to assist Central and South American countries in the War on Drugs.
- Mentions the oppressive nature of some governments and their military use against people.
- Describes how US military training involves learning tactics used by terrorists, leading to unintended consequences.
- Talks about how expensive military training is and how individuals can profit from it in the private sector.
- Illustrates how trained individuals sought after by cartels can compromise counter-narcotics efforts.
- Emphasizes the blurred line between cartels and governments in some countries, leading to corruption and collaboration.
- Points out that cartels operate like paramilitary organizations with intelligence and recruitment strategies.
- States that the dissemination of information to cartels undermines efforts and increases the cost of the War on Drugs.
- Criticizes the inefficacy of current approaches in the drug war, suggesting a shift towards harm reduction and treatment.
- Concludes by reflecting on the failure of the War on Drugs and the need for a different perspective.

# Quotes

- "The plants have won."
- "We've wasted the training, we've wasted all of that money, and it has done no good."
- "The war on drugs is a failure."
- "It's time to maybe look at it from a different perspective."
- "Y'all have a good night."

# Oneliner

Beau explains the pitfalls of US military assistance in the War on Drugs, citing unintended consequences and the inefficacy of current approaches, ultimately calling for a shift towards harm reduction and treatment.

# Audience

Policy advocates, activists

# On-the-ground actions from transcript

- Advocate for harm reduction and treatment approaches in drug policy (suggested)
- Support organizations working towards drug policy reform (implied)
- Educate others on the failures of the War on Drugs and the need for a different approach (implied)

# Whats missing in summary

In watching the full transcript, viewers can gain a deeper understanding of the unintended consequences of US military assistance in the War on Drugs, leading to a call for a more effective and humane approach to drug policy.

# Tags

#WarOnDrugs #USMilitary #CentralAmerica #SouthAmerica #HarmReduction #DrugPolicy