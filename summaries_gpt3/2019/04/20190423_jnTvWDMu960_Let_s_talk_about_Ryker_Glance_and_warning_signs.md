# Bits

Beau says:

- Two-year-old Ryker Glantz survived a horrifying incident where his father shot him in the face with a shotgun after a pistol malfunctioned.
- Beau stresses the importance of recognizing warning signs in domestic abuse situations, as they can escalate suddenly.
- Warning signs include jealousy, possessiveness, unpredictability, bad temper, animal cruelty, verbal abuse, extreme controlling behavior, and more.
- Beau urges victims to seek help from shelters and organizations and stresses the significance of getting out before situations escalate.
- Financial control, limiting access to funds, accusations of infidelity, and harassment at work are also warning signs of domestic abuse.
- Beau encourages everyone to be aware of these signs and act early to prevent potentially tragic outcomes.
- Despite not knowing the specifics of Ryker's situation, Beau advocates for recognizing warning signs and taking action early.
- Beau mentions a GoFundMe for Ryker's mom to support her in being with her son during his recovery.
- Ryker's survival is described as winning "every tough guy competition forever" and serves as a reminder to pay attention to domestic abuse warning signs.
- Beau concludes by encouraging viewers to take stock of the warning signs and to act early to prevent domestic abuse situations from escalating further.

# Quotes

- "The key is to get out before then. There's always a way out."
- "Left unchecked, warning signs always end bad, it always ends bad."
- "Get out early, get out early."
- "You win every tough guy competition forever."
- "It's the perfect opportunity for everybody to kind of take stock of these warning signs."

# Oneliner

Beau stresses recognizing warning signs, urging early action in domestic abuse situations to prevent tragic outcomes.

# Audience

Supporters of domestic abuse victims.

# On-the-ground actions from transcript

- Support Ryker's mom through the GoFundMe to help her be with her son during his recovery (suggested).
- Educate yourself on domestic abuse warning signs and ways to help victims (suggested).

# Whats missing in summary

The emotional impact and urgency conveyed by Beau's storytelling and the importance of community support for victims of domestic abuse. 

# Tags

#DomesticAbuse #WarningSigns #EarlyAction #SupportVictims