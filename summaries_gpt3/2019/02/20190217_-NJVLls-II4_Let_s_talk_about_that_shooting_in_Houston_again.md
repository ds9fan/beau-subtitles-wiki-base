# Bits

Beau says:

- Police union president in Houston made veiled threats after shooting incident.
- Warrant used for the raid was entirely fabricated.
- Confidential informants who were supposed to buy heroin from the house never did.
- One officer had heroin in his car.
- Houston PD targeted an independent journalist in 2016, leading to trumped-up charges.
- Journalists cultivated sources within the Houston Police Department after the incident.
- Chief of police claims isolated incident with a few bad apples, demonizes the victim.
- Private face tells officers to tear apart cases, suggesting it's not an isolated incident.
- Beau suggests bringing in the FBI for a thorough investigation.
- Raises questions about the source of heroin, integrity of officers, and demonization of the victim.
- Points out discrepancies in the evidence used for the raid.
- Calls for an end to demonizing the victim and a thorough, unbiased investigation.
- Questions the credibility of officers involved in the raid.
- Criticizes the use of evidence that wouldn't hold up in court to justify the raid.
- Raises suspicions about other possible illegal substances in the officer's possession.

# Quotes

- "Don't paint us all with a broad brush, isolated incident, a few bad apples, same song and dance we always hear."
- "And please stop demonizing the victim."
- "You need to bring somebody in that doesn't have loyalty, that doesn't want to cover for their buddy, that isn't part of that thin blue line."
- "In court that [evidence] would be suppressed."
- "What makes you think he didn't have pot and coke."

# Oneliner

Police union threats, fabricated warrants, and demonization of victims reveal systemic issues demanding an unbiased investigation.

# Audience

Journalists, Activists, Community Members

# On-the-ground actions from transcript

- Contact local journalists or media outlets to raise awareness of the fabricated warrant and biased practices within the Houston Police Department (suggested).
- Reach out to community organizations advocating for police accountability and transparency to support unbiased investigations (implied).

# Whats missing in summary

Full context and emotional impact can be best understood by watching the entire transcript.

# Tags

#PoliceAccountability #FabricatedWarrants #CommunityPolicing #HoustonPolice #SystemicIssues