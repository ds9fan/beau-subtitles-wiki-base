# Bits

Beau says:

- Introduces the topic of Dick Marcinko, the founder of SEAL Team 6, and addresses his controversial reputation.
- Questions the conviction of Marcinko, suggesting it may have been a result of personal vendettas.
- Comments on Marcinko's arrogance based on personal interactions but acknowledges his military accolades.
- Explores Marcinko's unique approach to building SEAL Team 6 by selecting individuals who had to struggle, showcasing psychological fortitude.
- Draws parallels between Marcinko's team-building strategy and insights into the incel community.
- Observes intelligence, self-awareness, misogyny, and racism within the incel groups.
- Identifies three types of incels: physically unattractive, psychological or cognitive issues, and those with negative personality traits.
- Notes that violent rhetoric and suicidal talk predominantly stem from the third group of incels.
- Encourages incels to potentially use their experiences and psychological resilience for positive purposes.
- Urges incels to avoid resorting to violence or isolation, suggesting that improving society benefits not only them but future generations.

# Quotes

- "He looked for people that had suffered, that had that psychological fortitude."
- "Killing people, killing yourself, it's not the answer."
- "You're not going to make it better for the people after you. You're going to make it worse."

# Oneliner

Beau delves into the controversial figure of Dick Marcinko, incel community insights, and the importance of psychological resilience in facing challenges and improving society.

# Audience

Incels, Community Builders

# On-the-ground actions from transcript

- Reach out to support groups or mental health professionals for assistance (suggested)
- Engage in positive community activities to channel experiences and resilience positively (implied)
- Advocate for societal change to prevent isolation and violence (implied)

# Whats missing in summary

Insights on the impact of societal stigmas and the importance of empathy in understanding and addressing the struggles faced by different groups in society.

# Tags

#DickMarcinko #SEALTeam6 #IncelCommunity #PsychologicalResilience #SocietalChange