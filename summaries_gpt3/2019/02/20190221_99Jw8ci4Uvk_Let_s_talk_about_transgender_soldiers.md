# Bits

Beau says:

- Responds to a question about transgender people in the military impacting readiness and the importance of their service.
- Approximately 1000 to 15,000 transgender individuals serve in the military, a small percentage.
- Military readiness is based on worldwide deployability and unit cohesion.
- RAND Corporation's study found no breakdown in unit cohesion in countries allowing transgender individuals to serve.
- Concerns about worldwide deployability due to hormone treatments requiring refrigeration, similar to insulin dependence.
- Loss of time for a transgender person in labor years is minimal, not a significant readiness issue.
- Beau compares the minimal impact of transgender individuals on readiness to pregnancy sidelining soldiers for nine months.
- Acknowledges a legitimate readiness concern for a small percentage of transgender individuals requiring long-term hormone treatment.
- Beau argues that turning away willing recruits impacts readiness more than allowing transgender individuals to serve.
- Draws parallels to historical military acceptance movements like desegregation and acceptance of gay individuals.
- Points out the military's role in driving social acceptance and the importance of diverse representation in the armed forces.
- Condemns the ban on transgender individuals serving in the military as politically motivated, not related to readiness.
- Expresses disappointment at the mistreatment and politicization of soldiers who have served.
- Mentions the California National Guard's decision to allow transgender individuals to serve despite DOD policy, expressing concerns about potential issues.

# Quotes

- "It is upsetting that soldiers who have fought are being discarded in this way by the government."
- "This small percentage of soldiers has become a political pawn."
- "At the end of the day, this ban, it has nothing to do with readiness, nothing."
- "The military has always been on the cutting edge of social acceptance."
- "I served with a black guy. They're all right."

# Oneliner

Beau breaks down the insignificant impact of transgender individuals on military readiness and stresses the importance of their service for societal acceptance and diversity.

# Audience

Advocates for inclusive military policies.

# On-the-ground actions from transcript

- Support efforts to allow transgender individuals to serve openly in the military (exemplified).
- Advocate for policies that prioritize diversity and inclusion in the armed forces (suggested).

# Whats missing in summary

The full transcript provides a detailed analysis of the impact of transgender individuals on military readiness and the historical context of diversity in the armed forces.

# Tags

#Military #TransgenderRights #Diversity #Inclusion #SocialAcceptance #Readiness