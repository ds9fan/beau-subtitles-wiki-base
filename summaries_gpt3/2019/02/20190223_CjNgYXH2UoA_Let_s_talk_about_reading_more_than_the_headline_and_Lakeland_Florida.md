# Bits

Beau says:

- Beau addresses the situation in Lakeland, Florida where a child who did not stand for the pledge is facing punishment despite headlines stating otherwise.
- The child's actions are protected by the West Virginia State Board of Education versus Barnett Supreme Court case law, allowing students to choose not to stand for the pledge.
- A substitute teacher attempted to shame or coerce the child into standing for the pledge, leading to interference with the educational process.
- The school administrator intervened and told the child to leave the class, which goes against the Tinker versus Des Moines ruling that students cannot be punished for peaceful protests that don't disrupt education.
- Law enforcement got involved, with a police officer backing up the school's decision to remove the child from the class.
- The child was arrested for disrupting the school and resisting arrest without violence, but questions arise about the validity of these charges.
- Beau planned activism involving playing the National Anthem on loop at the school board meeting but was overruled by local activists.
- The current plan of activism involves targeting campaign contributors and businesses associated with government officials for boycotts.
- Despite headlines suggesting the child may not be prosecuted, the situation is far from resolved, and punishing the child for exercising his rights undermines the value of the flag.
- Beau concludes by noting the importance of upholding constitutionally protected rights, regardless of how the child is punished.

# Quotes

- "You're punishing him for exercising his constitutionally protected rights."
- "If you do that, that flag is worthless. It means nothing."
- "You are making that flag worthless."
- "It's not worth a pledge."
- "It isn't."

# Oneliner

Beau dissects the situation in Lakeland, Florida where a child faces punishment for not standing during the pledge, exposing the attempt to spin headlines and uphold constitutional rights.

# Audience

Activists, Advocates, Parents

# On-the-ground actions from transcript

- Contact local activists to support their efforts in addressing the situation in Lakeland, Florida (implied).
- Participate in boycotts of businesses associated with government officials who are involved in the case (implied).
- Stay informed about legal developments in the case and support the child's legal counsel (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the events in Lakeland, Florida, shedding light on attempts to manipulate narratives and the importance of protecting constitutional rights in educational settings.

# Tags

#Lakeland #Florida #ConstitutionalRights #Activism #Punishment #Schools