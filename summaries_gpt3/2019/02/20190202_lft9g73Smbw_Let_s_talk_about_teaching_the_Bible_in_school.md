# Bits

Beau says:

- President Trump wants to put the Bible back in school.
- Christian students at school didn't like the idea much.
- The modern church is indicted for not fostering religious tolerance.
- Beau believes in teaching religious texts in schools if all are taught equally.
- Teaching philosophy is education; enforcing moral code is indoctrination.
- Philosophy, including religious philosophy, helps people think critically.
- Beau points out similarities in teachings across different religions.
- The downfall of Christianity in the US is due to hypocrisy, especially in politics.
- Christianity is on the decline due to blatant hypocrisy.
- Beau doubts the political expedience of disregarding Jesus' teachings.
- Public schools turning into religious institutions is unlikely to happen.
- Beau mentions the Church of Satan demanding their texts to be taught in schools.
- Teaching philosophy, not just religious philosophy, can benefit the American people.
- Education should focus on teaching how to think, not what to think.

# Quotes

- "Teaching philosophy is education; enforcing moral code is indoctrination."
- "The downfall of Christianity in the US is due to blatant hypocrisy."
- "The ultimate irony is that today's Church of Satan embodies most of the ideals of Christianity better than most Christian churches."
- "The goal of education should not be to teach you what to think. It should be to teach you how to think."

# Oneliner

President Trump's proposal to put the Bible back in schools sparks a reflection on religious tolerance, philosophy, and the decline of Christianity due to hypocrisy.

# Audience

Educators, Religious Leaders

# On-the-ground actions from transcript

- Advocate for equal representation of all religious texts in school curriculum (implied)
- Support teaching philosophy, including religious philosophy, in educational institutions (implied)

# Whats missing in summary

The full transcript offers a deeper exploration of the importance of philosophical education and religious tolerance.