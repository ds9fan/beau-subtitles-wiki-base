# Bits

Beau says:

- Beau introduces a historical figure who had a significant impact during WWII.
- The figure, Sophie Scholl, was part of the White Rose Society, which advocated passive resistance against the Nazi regime.
- Sophie, along with her brother Hans, was part of an anti-Hitler family but took a different path towards resistance.
- The White Rose Society wrote leaflets framed in biblical and philosophical arguments to reach a wider audience.
- Despite not engaging in violent resistance like blowing up bridges or taking out trains, Sophie believed in the power of passive resistance.
- Sophie's execution at 21 years old left a lasting impact, showcasing bravery and standing up for righteous causes.
- Her message emphasized the importance of meeting people where they are in life's journey and the difference between legality and morality.
- Sophie's actions taught that everyone can contribute to resistance efforts in their unique ways.
- She believed in the power of passive resistance, even in small acts like slowing down work to obstruct the Nazi regime.
- Sophie's defense during her trial reflected her belief that someone had to start expressing dissent, inspiring others to do the same.


# Quotes

- "How can we expect righteousness to prevail when there is hardly anybody willing to give himself up individually to a righteous cause?"
- "If through us, thousands of people are awakened and stirred to action."
- "Even in a sea of brown shirts there can be white roses."
- "Don't be as good at your job as you could be. Slow down at work."
- "Somebody after all had to make a start. What we wrote and said is also believed by others. They just don't dare express themselves as we did."

# Oneliner

Beau introduces Sophie Scholl from the White Rose Society, showcasing her impact through passive resistance against the Nazi regime and the importance of individual action for righteous causes.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Learn more about historical figures like Sophie Scholl and the White Rose Society (suggested)
- Educate others on the difference between legality and morality (implied)
- Engage in passive resistance in everyday actions to stand up against injustices (implied)

# Whats missing in summary

The emotional depth and historical context conveyed in the full transcript

# Tags

#WWII #SophieScholl #WhiteRoseSociety #Resistance #PassiveResistance