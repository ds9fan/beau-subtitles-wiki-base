# Bits

Beau says:

- Expresses dislike for Facebook memes and conducts a demonstration to illustrate his point.
- Shares a meme featuring AOC with a controversial quote about guns.
- Reads through comments on the shared meme, showcasing derogatory and misinformed remarks.
- Clarifies that the quote attributed to AOC actually belongs to Donald Trump.
- Criticizes memes for oversimplifying complex issues and hindering meaningful discourse.
- Advocates for engaging in thoughtful and informed conversations.
- Contrasts Trump and AOC's stances on the Second Amendment and due process.
- Points out Trump's actions regarding firearms regulations.
- Mentions AOC's position statement on gun violence and the Second Amendment.
- Concludes with a quote from George Washington about not believing everything on the internet.

# Quotes

- "This is why I don't like memes."
- "Boils things down to talking points, eliminates discussion. It's a slogan."
- "It completely destroys any meaningful conversation."
- "Donald Trump is not pro-Second Amendment."
- "Don't believe everything you read or see on the internet, George Washington."

# Oneliner

Beau conducts a meme demonstration, revealing misattributed quotes and advocating for meaningful discourse, contrasting Trump and AOC's stances on gun control.

# Audience

Social media users

# On-the-ground actions from transcript

- Fact-check memes before sharing (suggested)
- Engage in informed and meaningful dialogues about political issues (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the dangers of misinformation spread through memes and the importance of engaging in substantial dialogues for a better-informed society.