# Bits

Beau says:

- Addresses the topic of fake hate crimes, which was repeatedly requested by viewers.
- Initially found the subject uninteresting until looking into it due to right-wing rage pages.
- Questions the intensity of outrage over the fake hate crime incident compared to other similar cases.
- Criticizes the focus on partisan divides rather than individual beliefs.
- Condemns the blinding racism in only caring about this specific incident.
- Expresses disinterest in discussing topics that cannot be used to make a wider point.
- Points out the inconsistency and divisiveness caused by outrage-driven news coverage.
- Suggests that people prioritize ideological consistency over political affiliations.
- Raises concerns about innocent people possibly being arrested due to false complaints.
- Comments on the lack of utility in focusing solely on outrage without broader implications.

# Quotes

- "If you only care about this guy and what he did, this instance it's blindingly racist."
- "One of the biggest problems in this country is that people are more concerned about red versus blue than they are their own beliefs."
- "That's what they do, you know, they tell you why you need to be mad."
- "I think people call the cops too much anyway, and I don't even watch his show."
- "Can't imagine what the difference is. This isn't the 1960s."

# Oneliner

Beau questions the intense outrage over fake hate crimes, criticizing partisan divides and advocating for ideological consistency.

# Audience

Viewers

# On-the-ground actions from transcript

- Challenge partisan divides by prioritizing individual beliefs and ideological consistency (implied).

# Whats missing in summary

Deeper insights into the societal impact of focusing on outrage-driven news coverage and the importance of ideological consistency in addressing divisive issues.

# Tags

#FakeHateCrimes #PartisanDivides #IdeologicalConsistency #Outrage #Racism