# Bits

Beau says:

- Addresses misconceptions about domestic violence shelters and the challenges faced by survivors.
- Domestic violence affects everyone regardless of gender, race, or class.
- Personal connections drive Beau's involvement in the fight against domestic violence.
- Shares personal stories of friends who were victims of domestic violence.
- Women are 500 times more likely to be killed when leaving abusive relationships.
- Domestic violence shelters play a critical role in reducing risks and providing support.
- Financial control by abusers complicates leaving abusive relationships.
- Challenges include finding a safe place to go, caring for pets, and potential job changes.
- Domestic violence extends to the workplace, with a high percentage of women being killed by intimate partners.
- Beau supports Shelter House of Northwest Florida for their innovative solutions and stigma-fighting initiatives.

# Quotes

- "One of the comments was, I don't even understand why these things exist, why can't women just leave?"
- "It affects everybody, every class, but it's not as simple as just leaving."
- "Women are 500 times more likely to be killed if they're in one of these relationships when they're just leaving."
- "Any obstacle that's standing in the way, they get rid of it."
- "It's something that can be done."

# Oneliner

Beau explains the misconceptions around domestic violence shelters and the critical support they provide for survivors leaving abusive relationships.

# Audience

Supporters of domestic violence survivors.

# On-the-ground actions from transcript

- Donate money or old cell phones to organizations supporting domestic violence survivors (suggested).
- Contact Shelter House of Northwest Florida to inquire about ways to help (suggested).
- Support initiatives like providing kits for rape victims in hospitals (suggested).

# Whats missing in summary

The emotional impact and personal connection Beau shares in advocating for domestic violence survivors.

# Tags

#DomesticViolence #SupportSurvivors #Donate #CommunitySupport #Awareness