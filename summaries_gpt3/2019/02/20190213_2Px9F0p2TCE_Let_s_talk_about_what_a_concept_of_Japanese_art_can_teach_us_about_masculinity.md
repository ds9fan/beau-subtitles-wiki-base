# Bits

Beau says:

- Critiques resources aimed at helping young American males find masculinity on the internet as ridiculous.
- Recalls a time working in a call center with an old Japanese man during the overnight shift.
- Describes drinking sake with the old Japanese man who talked about a Japanese concept called Shibumi.
- Explains Shibumi as subtle, understated, part of nature but separate from it, in control of nature, and representing perfection without effort.
- Suggests that masculinity cannot be quantified or charted and should involve maintaining control over natural characteristics effortlessly.
- Emphasizes the importance of effortless perfection in masculinity and the need to accept oneself rather than mimicking role models.

# Quotes

- "Maybe masculinity can't be quantified the way people try."
- "Effortless perfection. Now that is a hard idea to get your head around."
- "When you see somebody trying to be masculine, you see somebody trying to be a tough guy, happens you immediately know they're not."

# Oneliner

Beau shares a Japanese concept, Shibumi, to redefine masculinity as effortless perfection, separate from conforming to charts or molds.

# Audience

Young American Males

# On-the-ground actions from transcript

- Embrace your natural characteristics and accept yourself as you are (implied).
- Refrain from trying to force yourself to conform to societal molds of masculinity (implied).

# Whats missing in summary

Exploration of the importance of embracing authenticity and effortless perfection in redefining masculinity.

# Tags

#Masculinity #Authenticity #Shibumi #Acceptance #SelfLove