# Bits

Beau says:

- Explains the division of the island of Ireland between North and South, controlled by the UK and Republic of Ireland respectively.
- Describes the significance of Brexit in relation to the open border between Northern Ireland and the Republic of Ireland.
- Mentions the US's stance on backing a post-Brexit trade deal that includes an open border on the Irish island.
- Raises concerns about potential violence if a hard border is implemented, specifically mentioning Republican violence.
- Differentiates Irish Republicans from American Republicans, noting the political ideologies of each.
- Advises individuals living near potential conflict areas to stay completely out of any involvement to ensure safety.
- Talks about the increased accessibility of explosives due to online materials, making it easier for individuals to manufacture them.
- Mentions the historical practice of giving warnings before bomb attacks and the potential for modern technology to aid in issuing alerts.
- Warns against getting involved in areas where violence might erupt and advises staying away from such hotbeds.
- Comments on the changing dynamic regarding targeting the royal family and British establishment in potential attacks.

# Quotes

- "I mean they make Bernie Sanders look like Trump."
- "Stay out of whatever area develops as a hotbed, just stay out of it."
- "This is just throwing a match on the powder keg."

# Oneliner

Beau explains the implications of Brexit on the Irish border, warning about potential violence and advising complete non-involvement for safety.

# Audience

Irish residents, Brexit observers

# On-the-ground actions from transcript

- Stay completely out of potential conflict areas (implied)
- Remove any nationalist symbols from visible locations (implied)
- Keep any political affiliations to yourself (implied)
- Pay attention to warnings through modern technology (implied)

# Whats missing in summary

The full transcript provides a detailed insight into the potential consequences of Brexit on the Irish border and the historical context of violence in the region. Viewing the full content offers a comprehensive understanding of the situation and the precautions that individuals can take.

# Tags

#Brexit #Ireland #Border #Violence #PoliticalImplications