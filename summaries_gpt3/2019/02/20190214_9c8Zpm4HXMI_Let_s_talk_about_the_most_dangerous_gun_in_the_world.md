# Bits

Beau says:

- Beau answers a question about getting a safe rifle for his son, sharing a personal story about firearm safety.
- A friend accidentally shoots a metal plate hanging on the wall with a .22 pistol, illustrating the danger of firearms.
- Beau stresses that all firearms are designed to kill and that safety must be a top priority.
- He mentions starting with an air rifle before introducing a .22 due to its misconception of being safe.
- Despite being tiny, a .22 bullet is dangerous, moving at high speeds and causing significant harm.
- Even experienced individuals can make mistakes with firearms, as seen in the story Beau shares.
- Beau talks about accidental discharges and how they can happen to anyone who handles guns.
- Emphasizes the importance of gun safety measures like keeping the gun pointed in a safe direction and finger off the trigger.
- Beau shares a humorous incident where a mistake with a gun led to beer being thrown but thankfully no serious harm.
- Concludes by stating that there are no truly safe firearms, only varying levels of danger.

# Quotes

- "Safe and firearm do not go together."
- "All firearms will kill."
- "There is no safe firearm, but there are a lot of dangerous ones."

# Oneliner

Beau stresses the inherent danger of firearms and the critical importance of gun safety measures to prevent accidents.

# Audience

Parents, gun owners

# On-the-ground actions from transcript

- Teach firearm safety to children and beginners, starting with air rifles (implied)
- Emphasize the importance of gun safety measures like keeping the gun pointed in a safe direction and finger off the trigger (implied)

# Whats missing in summary

The full transcript provides a detailed insight into firearm safety and the potential risks associated with owning and handling guns.