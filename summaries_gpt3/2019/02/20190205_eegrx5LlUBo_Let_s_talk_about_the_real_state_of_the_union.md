# Bits

Beau says:

- The United States is as politically divided today as during desegregation.
- Washington, D.C. is not in a good state.
- Trump's wall dominates headlines despite not being a top priority for most Americans.
- Families are still being torn apart for crossing a border.
- Government and large corporations preach conservation while harming the environment.
- $3.4 billion is spent on lobbying and corruption.
- News focuses on manipulating emotions rather than informing.
- The drug war continues, imprisoning innocent individuals.
- Fragile masculinity is evident in men's reactions to a razor ad.
- Teachers are forced to prepare for combat.
- Systemic racism persists in society.
- Youth are mocked and discredited.
- Hate crimes are increasing, and Nazis are present in the streets.
- Americans are fighting for safety and freedom for others, even at personal risk.
- Eight companies produce more pollution than the entire U.S. population.
- Communities are growing stronger, decreasing the power of DC politicians.
- Independent journalists strive to separate fact from fiction.
- Masculine men openly criticize toxic masculinity.
- Teachers are implementing diverse methods to combat violence in schools.
- People of all races are uniting against systemic racism.
- Youth are at the forefront of societal battles.

# Quotes

- "We don't defeat tyranny and oppression by complying with it while we fight it."
- "We defeat it by ignoring it while we quietly create our own systems to replace it."
- "The state of those people who care? Well, they're in a state of defiance."

# Oneliner

The United States faces political division, fragile masculinity, systemic racism, and rising hate crimes while individuals quietly rebel and build new systems for change.

# Audience

Americans

# On-the-ground actions from transcript

- Joining together with community members to combat hate crimes and systemic racism (exemplified)
- Supporting independent journalists striving for truth (exemplified)
- Implementing diverse methods to combat violence in schools (exemplified)

# Whats missing in summary

The full transcript provides a detailed insight into the current state of the United States, urging individuals to take action against oppression and division.

# Tags

#PoliticalDivision #SystemicRacism #CommunityAction #Defiance #Change