# Bits

Beau says:

- Urges viewers to watch the whole video even if they disagree with the content.
- Traces back the history of vaccines to 1902 when smallpox vaccination was made mandatory in France.
- Describes how the first vaccine came about through Jenner's experimentation with cowpox and smallpox.
- Acknowledges different perspectives on vaccine history, including ethical concerns and conspiracy theories.
- Mentions the practice of inoculation in Africa, India, and China before Jenner's discovery.
- Emphasizes the concept of risk versus reward in vaccination.
- Clarifies that adverse reactions to vaccines don't always mean death.
- Encourages anti-vaxxers to research and make informed decisions.
- Notes that parents not following post-vaccination instructions can complicate adverse reactions.
- Explains how vaccines work, addressing concerns about unvaccinated children posing a risk.
- Compares principles of bodily autonomy in vaccination and abortion.
- Asserts the importance of individual freedom in making healthcare choices.
- Raises ethical concerns about mandatory vaccines despite advocating for vaccination.
- Draws parallels between government control in vaccine mandates and abortion regulations.

# Quotes

- "Don't. Watch the whole thing."
- "Risk versus reward, that's what it's all about."
- "Every law is backed up by penalty of death."
- "Either we have bodily autonomy or we do not."
- "Either we're that servant's eight-year-old boy, or we do maintain control over our own destiny."

# Oneliner

Beau challenges viewers to watch his controversial take on vaccines, exploring their history, risks, rewards, and the ethical debate around bodily autonomy in healthcare decisions.

# Audience

Health-conscious individuals

# On-the-ground actions from transcript

- Research vaccines thoroughly before making decisions (suggested)
- Follow post-vaccination instructions from healthcare providers (exemplified)

# Whats missing in summary

In watching the full video, viewers can gain a deeper understanding of the historical context, ethical considerations, and individual freedoms surrounding vaccination debates.

# Tags

#Vaccines #History #Ethics #BodilyAutonomy #Healthcare