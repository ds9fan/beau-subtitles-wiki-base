# Bits

Beau says:

- Beau starts by sharing a story about a guy who meets an astronaut on a plane and tries to strike up a conversation with him.
- The astronaut's interest in Portuguese art despite being an astronaut is explained by his desire to learn and expand his knowledge.
- Beau addresses a common question he receives about how to become well-read without having the time to read.
- He recommends an app called LibriVox, which offers thousands of audio books for free, allowing people to listen while cooking or commuting.
- Beau underscores the importance of reading for education, understanding, and gaining insights from different perspectives.
- He mentions that banned books are often the best books and recommends exploring them for a richer reading experience.
- Beau clarifies that his endorsement of LibriVox is not sponsored and encourages people to find ways to achieve their goals.

# Quotes

- "Just because I'm sure somebody will ask, no, this isn't a commercial for LibriVox, they did not pay me, they don't even know I'm talking about it right now."
- "There's always a way to accomplish your goal."
- "Banned books are the best books."

# Oneliner

Beau shares a story about meeting an astronaut and advocates for using LibriVox to become well-read despite a busy schedule, stressing the importance of reading banned books.

# Audience

Book lovers, busy individuals

# On-the-ground actions from transcript

- Download the LibriVox app and listen to audiobooks during daily activities (suggested)
- Make time for reading by incorporating it into cooking or commuting routines (implied)

# Whats missing in summary

Beau's engaging storytelling and passion for reading are best experienced in the full transcript.

# Tags

#Reading #Books #Education #LibriVox #Learning