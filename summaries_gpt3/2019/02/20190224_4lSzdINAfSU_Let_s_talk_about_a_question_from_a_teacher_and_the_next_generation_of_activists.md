# Bits

Beau says:

- A teacher asked about a student refusing to leave with the principal, leading to police involvement.
- Questions arise about compliance, respect, and self-advocacy for students.
- Beau questions the training and support available to the student in advocating for their rights.
- Beau believes in diverse tactics for self-advocacy and questions the concept of a "right way."
- He expresses concerns about the complex nature of this teachable moment and its potential impact.
- Beau underscores the importance of teaching students about their rights and self-advocacy.
- He acknowledges the role of the Lakeland PD in sparking defiance and activism in students.
- Beau mentions the national attention drawn to the incident in Lakeland, Florida, and its impact on students' awareness of rights.
- Respectful activism may still provoke criticism, but effectiveness is key.
- Beau encourages individuals to determine their level of involvement in activism and self-advocacy.

# Quotes

- "How do we create the next generation of people that will look at an injustice and say, no, not today."
- "If you don't assert your rights, you don't have any."
- "It wasn't until this message that I realized we don't have to. People like Lakeland PD are going to do it for us."
- "There's always going to be somebody that's going to say whatever you did was wrong. The question is, was it effective?"
- "Just let them know that they can. Just let them know that it is okay to advocate for yourself, to stand up for your rights."

# Oneliner

A teacher seeks guidance on a student's defiance, sparking Beau's insights on diverse self-advocacy tactics and the role of activism in teaching students their rights.

# Audience

Teachers, Students, Activists

# On-the-ground actions from transcript

- Teach students about their rights and how to self-advocate (exemplified)
- Encourage diverse tactics for self-advocacy (exemplified)
- Spark awareness and activism among students through real-life examples (exemplified)

# Whats missing in summary

The emotional depth and personal anecdotes shared by Beau can be best experienced by watching the full transcript. 

# Tags

#SelfAdvocacy #Activism #Teaching #StudentRights #DiverseTactics