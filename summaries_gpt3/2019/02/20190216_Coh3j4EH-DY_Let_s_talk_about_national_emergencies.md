# Bits

Beau says:

- Expresses frustration over the abuse of national emergencies for political gain.
- Describes attempts to create a video addressing the issue, including initial anger and satire.
- Criticizes the current national emergency declaration as dangerous and unnecessary.
- Points out that border apprehensions are at a 41-year low, indicating no crisis.
- Emphasizes the dangerous precedent set by one person having too much power.
- Explains the limitations of a national emergency declaration in creating legislation.
- Raises concerns about potential misuse of power, such as seizing firearms or factories.
- Warns about the erosion of democracy if checks and balances are not upheld.
- Urges Americans to be more vigilant and proactive in protecting their rights.
- Calls for action from groups traditionally focused on constitutional rights.

# Quotes

- "This is how every democracy, every Republican history committed suicide."
- "No one person should have this much power. This is dictatorial rule."
- "The average American needs to start leading themselves."
- "Pandora's box is being opened and it's extremely dangerous."
- "If the Senate and House of Representatives, the Supreme Court, do not put an end to this immediately. That's it. It's over."

# Oneliner

Beau warns of the dangerous abuse of power and erosion of democracy through the misuse of national emergencies, urging Americans to take action before it's too late.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact your Senators and Representatives to urge them to uphold checks and balances in government (implied)
- Join or support organizations focused on defending constitutional rights and democracy (implied)
- Attend marches or protests advocating for governmental accountability and transparency (implied)

# Whats missing in summary

The emotional intensity and urgency conveyed by Beau's passionate plea for Americans to be vigilant and proactive in protecting democracy.

# Tags

#NationalEmergencies #Democracy #ChecksAndBalances #AbuseOfPower #Activism