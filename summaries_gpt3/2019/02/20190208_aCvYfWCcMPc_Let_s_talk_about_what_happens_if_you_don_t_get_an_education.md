# Bits

Beau says:

- Education often tied to devaluing labor in the United States, reinforcing class ideas.
- Misconception that blue-collar jobs equate to failure in achieving the American dream.
- Dangerous concept when education is confused with credentialing, favoring those with money.
- Public education system not what it used to be; emphasis on credentialing over true education.
- Those with money tend to receive the best credentialing, leading to power imbalances in a democracy.
- Education system in the country may need a revolution due to current flaws.
- Educators' decisions influenced by distant policymakers who may not understand teaching.
- Calls for a more diverse representation in governance, including more waitresses and fewer millionaires.
- Emphasizes the importance of creating a representative democracy where the governed have common ground with their representatives.
- Critique on how current system prioritizes the wealthy and powerful over the general population.

# Quotes

- "If you're working a blue collar, no collar job, and got your name on your shirt that you've done something wrong."
- "Education doesn't mean education anymore."
- "More welders, less lobbyists, more scientists, less CEOs, more teachers and less lawyers."
- "Let's create a system where you actually have something in common with the person governing your life."
- "They're taking care of their own and they're leaving us out in the cold."

# Oneliner

Beau questions the link between education and labor devaluation, calls for a more representative democracy that prioritizes common ground over privilege.

# Audience

Educators, policymakers, activists.

# On-the-ground actions from transcript

- Advocate for educational reforms that prioritize true learning over credentialing (implied).
- Support and amplify voices calling for more diverse representation in governance (implied).
- Engage in local politics to ensure representation that mirrors community diversity (implied).

# Whats missing in summary

The full transcript provides a deeper exploration of the flaws within the education system and the need for a more inclusive and representative democracy.

# Tags

#Education #Representation #Democracy #Inequality #PolicyChange