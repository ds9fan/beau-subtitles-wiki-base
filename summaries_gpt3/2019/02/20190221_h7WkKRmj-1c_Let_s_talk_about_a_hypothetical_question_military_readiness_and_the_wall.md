# Bits

Beau says:

- Poses a hypothetical question about a list of facilities being targeted by a foreign power and their potential goals and strategies.
- Mentions critical facilities like maintenance facilities for the F-35, dry dock facilities at Pearl Harbor, and training facilities for special operations.
- Suggests that targeting these facilities is an attempt to degrade readiness before a war.
- Reveals that the list of facilities in question are actually Department of Defense programs being sacrificed for border wall construction.
- Criticizes the idea of sacrificing vital military programs for a partial border wall, which falls short of fulfilling promises to rebuild the military.
- Expresses concern about the negative impact on military readiness and logistics due to diverting funding from vital programs.

# Quotes

- "They're crippling our air because if these aircraft aren't maintained, and they don't fly. It's that simple."
- "What we're doing is we're going to give up our ability to defend against an actual invasion to build part of a wall to stop an invasion he made up."
- "Two hundred thirty-four miles is not building the wall. It's not. It's a start."

# Oneliner

Beau poses a hypothetical scenario about targeting critical military facilities, revealing they are actually sacrifices for a partial border wall, jeopardizing military readiness.

# Audience

Policy Makers, Activists

# On-the-ground actions from transcript

- Contact policymakers to advocate for prioritizing military readiness over border wall construction (suggested).
- Join advocacy groups working to protect vital Department of Defense programs (exemplified).

# Whats missing in summary

The full transcript provides detailed insights into the potential consequences of sacrificing military programs for border wall construction.

# Tags

#MilitaryReadiness #BorderWall #DefensePrograms #DepartmentOfDefense #HypotheticalScenario #FundingPriorities