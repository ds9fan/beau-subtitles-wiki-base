# Bits

Beau says:

- Distinguishes between the Center for Migration Studies and the Center for Immigration Studies, cautioning against using the latter as a source.
- Presents a new study from the Center for Migration Studies with seven years of hard data on undocumented workers entering the United States.
- Notes that most undocumented workers enter the U.S. via visa overstays rather than border crossings.
- Breaks down the annual numbers: $320,000 from visa overstays and $190,000 from border crossings, questioning President Trump's claims of an invasion based on secret intelligence.
- Debunks the crisis at the border narrative, pointing out that the undocumented population in the U.S. fell by 1.1 million from 2010 to 2017, with more leaving than arriving.
- Provides insights into the lengthy wait times for legal immigration, using the example of a Mexican individual with ties to the U.S. needing to apply for a visa back in 1997.
- Suggests that addressing wait times for legal immigration could be a practical starting point if there are concerns about illegal immigration.
- Encourages focusing on areas within our control, like wait times, if there is a perceived crisis at the border.

# Quotes

- "There is no crisis at the border. It doesn't exist."
- "That's why people don't just get in line and come here legally."
- "If we're going to pretend that there is some crisis, maybe we should start with the stuff we have control over, which is the wait times."

# Oneliner

Beau debunks the border crisis narrative, reveals data on undocumented population decline, and underscores the lengthy wait times for legal immigration as real issues to address.

# Audience

Citizens concerned about immigration policies

# On-the-ground actions from transcript

- Advocate for reducing wait times for legal immigration processes (suggested)
- Share information on visa application processing times to raise awareness (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of immigration data and challenges the narrative of a crisis at the border, advocating for a focus on addressing wait times for legal immigration processes.

# Tags

#Immigration #BorderCrisis #UndocumentedWorkers #VisaOverstays #LegalImmigration