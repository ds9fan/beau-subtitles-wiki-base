# Bits

Beau says:

- A ten-year-old Cub Scout took a knee during the Pledge of Allegiance at a city council meeting, sparking varied reactions.
- Some people struggle to fit their standard responses to this unique situation.
- The attempt to blend racism and nationalism has led to young people associating the American flag with racism.
- The rejection of racism by the youth will result in them rejecting the flag as well.
- The young Scout's actions are rooted in independence, self-reliance, and free thought taught by scouting.
- Patriotism involves correcting your country when it's wrong, not blind obedience.
- Beau expresses gratitude towards the young Scout for embodying true patriotism at a young age and standing against racism.
- Beau shares his Scout background and praises the Scout for his independent and thoughtful act.
- The Scout's action symbolizes independent thought, self-reliance, and preparedness for future decisions.
- Beau sees hope in the younger generation dismantling racist symbols and standing up for what's right.

# Quotes

- "Patriotism is correcting your country when it's wrong."
- "Scouts are doing just fine."
- "This country can build that monument. And these kids are gonna tear it down."

# Oneliner

A ten-year-old Cub Scout's act challenges racism, embodying true patriotism and independent thought, symbolizing hope for dismantling divisive symbols.

# Audience

Youth advocates

# On-the-ground actions from transcript

- Support youth empowerment in community groups (implied)
- Advocate for inclusive and anti-racist education in scouting organizations (implied)
- Encourage independent thought and standing up against discrimination in youth (implied)

# Whats missing in summary

The full transcript captures the power of youth in challenging racism and nationalism, offering hope for a more inclusive future.

# Tags

#YouthEmpowerment #AntiRacism #Patriotism #CommunityAction #Scouting