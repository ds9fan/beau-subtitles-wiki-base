# Bits

Beau says:

- Reporting live from the Mexican border for the fifth column.
- President Trump declared a national emergency due to an invasion.
- No signs of any opposition forces at the border.
- Pentagon has not specified which country is invading.
- Refugees seen fleeing in terror.
- Assumption made that opposition forces must be close by.
- New information reveals it was a family of six, not an invasion.
- National emergency declared over people crossing the border to claim asylum legally.
- Beau questions who sent him there and expresses frustration with the situation.
- Beau is done with the network.

# Quotes

- "He declared a national emergency over people crossing the border to legally claim asylum."
- "I am so done with this network."

# Oneliner

Reporting from the Mexican border on Trump's national emergency declaration due to asylum seekers; frustration and disbelief at the situation.

# Audience

Media consumers, activists.

# On-the-ground actions from transcript

- Contact network to express frustration (implied).

# What's missing in summary

The full context and background details of the situation at the Mexican border. 

# Tags

#BorderReporting #NationalEmergency #AsylumSeekers #Refugees #MediaCoverage