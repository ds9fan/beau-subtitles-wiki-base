# Bits

Beau says:

- Patty Hearst was kidnapped 45 years ago today, and her family gave up $2 million in negotiations with the SLA.
- The $2 million given up by the Hearst family was to be turned into food and distributed to the needy, not handed over to the kidnappers.
- Patty Hearst's grandfather, adjusted for inflation, was worth about $30 billion when he died.
- Beau questions the morality of extreme wealth in society, using Jeff Bezos as an example.
- Bezos earned $78.5 billion last year, but most of it is not liquid cash.
- Beau imagines what Bezos could do if he decided to end homelessness in the U.S., estimating it to cost $5.7 billion.
- Beau suggests the idea of taxing the ultra-wealthy at a higher rate or holding them socially responsible.
- He points out the extreme income inequality and questions when it becomes a moral imperative to act against it.
- Beau criticizes the entitlement and lack of empathy shown by the wealthy in the United States.
- He warns that if things don't change, people may resort to drastic actions to address inequality.

# Quotes

- "One guy can end homelessness and give them $200 a week. It's crazy. It's insane."
- "When does it become moral to act? The system as it is, is turning the working class into the working poor and millionaires into billionaires."
- "The entitled nature of the wealthy in the United States now."
- "We still know where the pitchforks are. And if things don't change, those pitchforks are coming."
- "Y'all have a good night."

# Oneliner

45 years after Patty Hearst's kidnapping, Beau questions the morality of extreme wealth through a Jeff Bezos example, proposing an end to homelessness in the US for a fraction of his wealth.

# Audience

Social justice advocates

# On-the-ground actions from transcript

- Advocate for policies that address income inequality and homelessness (implied)
- Support initiatives that tax the ultra-wealthy at a higher rate (implied)
- Raise awareness about the moral implications of extreme wealth accumulation (implied)

# Whats missing in summary

The full transcript provides a detailed examination of income inequality and societal morality through the lens of extreme wealth, urging action against growing disparities.

# Tags

#IncomeInequality #WealthDistribution #SocialJustice #EndHomelessness #TaxTheRich