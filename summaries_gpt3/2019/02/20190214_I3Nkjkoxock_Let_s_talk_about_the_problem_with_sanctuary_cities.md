# Bits

Beau says:

- A man in Florida was lured into a trailer, tied up, beaten, robbed, and threatened by individuals who ultimately let him go after warning him not to go to the police because he was undocumented.
- The attackers were white, which may surprise some listeners.
- Sanctuary cities exist to protect undocumented individuals from being deported when seeking help from law enforcement after being victims of violent crimes.
- Some local police departments resist cooperating with ICE due to concerns about inaccuracies in ICE's database and potential liability issues.
- An example is shared of a man wrongfully detained by ICE for three weeks due to a database error.
- Even a veteran carrying military ID and a passport was detained by ICE, demonstrating the flaws in the system.
- The argument against sanctuary cities fostering non-assimilation is countered by the fact that undocumented workers statistically commit less violent crime than native-born individuals.
- Private prisons may be pushing for a focus on immigration enforcement as a new source of profit once marijuana legalization reduces the need for housing non-violent drug offenders.
- Beau argues against local law enforcement acting like immigration enforcers, stating that immigration is a federal issue and should remain as such.

# Quotes

- "Because if the cops, local cops, have to run everybody that they won't run into through ICE's database, I mean let's just set aside the whole fact of local law enforcement walking around like the Gestapo asking for papers."
- "They're not real people. They can't go to the cops."
- "Statistically proven, illegal immigrants commit less violent crime than native-born."
- "No local department should be enforcing laws outside of this jurisdiction."
- "There's no reason for your local deputy or your local cop to walk around like the Gestapo."

# Oneliner

The problem with sanctuary cities is the necessity they exemplify in protecting undocumented individuals from deportation when seeking help after being victims of violence, countering misconceptions and advocating for federal responsibility over immigration enforcement.

# Audience

Advocates for immigrant rights

# On-the-ground actions from transcript

- Advocate for the protection of undocumented individuals and support sanctuary city policies (implied)
- Educate your community on the importance of sanctuary cities in protecting victims of violent crimes regardless of immigration status (implied)
- Support initiatives that push for federal responsibility over immigration enforcement (implied)

# Whats missing in summary

The full transcript provides a comprehensive analysis of the challenges faced by undocumented individuals when seeking help after experiencing violence, shedding light on the necessity of sanctuary cities in ensuring their protection and advocating for federal intervention in immigration enforcement.

# Tags

#SanctuaryCities #ImmigrantRights #CommunitySafety #ICE #FederalResponsibility