# Bits

Beau says:

- Describes a man who live-tweeted mocking kids building LEGOs and robots at a competition.
- Points out the man's behavior as mocking, with backlash on Twitter.
- Shares a personal story of his own son building a temporary castle without plans or diagrams.
- Acknowledges LEGO building as nerdy but praises the creativity and skill involved.
- Notes the irony of the man mocking kids for their creative accomplishments.
- Criticizes the man's actions as unmanly and suggests apologizing to his son and the other kids.

# Quotes

- "Your kid's got a pretty valuable skill there already developed, and you're mocking it."
- "You might want to get on Twitter and apologize to your son and all the other kids that got caught in a crossfire."

# Oneliner

A father mocks kids building robots, but creativity trumps mockery in masculinity.

# Audience

Parents, educators, Twitter users

# On-the-ground actions from transcript

- Apologize to your child and other kids on Twitter for mocking their creativity (suggested).

# Whats missing in summary

The full transcript provides a deeper understanding of the importance of valuing creativity in children and the impact of mocking such skills on their development and self-esteem.

# Tags

#Parenting #Creativity #Masculinity #Mockery #Apology