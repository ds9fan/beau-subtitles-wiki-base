# Bits

Beau says:

- Border apprehensions along the southern border are at a 40-year low.
- The Constitution of the United States outlines a process for funding projects.
- Trump's rise to power and his promises of prosperity and military supremacy are discussed.
- A comparison is drawn between Hitler's actions and Trump declaring a national emergency.
- Fascism is defined by 14 specific elements, including corporativismo and disdain for human rights.
- The importance of checks and balances in the U.S. Constitution is emphasized.
- Congress serves as a vital checkpoint against tyranny and dictatorship.
- The potential consequences of Trump's national emergency declaration are explored.
- A choice is presented between supporting the Constitution or backing Trump's actions.
- The audience is urged to choose between being a patriot or a traitor in relation to current political events.

# Quotes

- "Either you support the constitution of the United States or you betray it."
- "You're a patriot or a traitor."
- "Circumventing Congress, it's a betrayal."
- "There is no way to support both anymore."
- "The choice is simple, you can support the constitution of the United States or you can support Mango Mussolini."

# Oneliner

Beau outlines the dangers of paralleling Hitler's actions with Trump's national emergency declaration, urging a choice between supporting the U.S. Constitution or backing authoritarian measures.

# Audience

Americans

# On-the-ground actions from transcript

- Support the U.S. Constitution by upholding democratic values and principles (implied).
- Take a stand against authoritarian actions by advocating for checks and balances within the government (implied).
- Choose to be informed and engaged in defending constitutional rights in the face of potential abuses of power (implied).

# Whats missing in summary

The full transcript provides detailed historical context and a thorough analysis of the current political climate that can deepen understanding beyond this summary. It also offers insight into the importance of civic engagement in safeguarding democracy.

# Tags

#Constitution #ChecksAndBalances #Fascism #Authoritarianism #PoliticalAnalysis