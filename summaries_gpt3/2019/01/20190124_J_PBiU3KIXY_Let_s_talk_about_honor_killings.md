# Bits

Beau says:

- Explains honor killings where a woman is killed for disgracing a man, often due to infidelity.
- Shares the story of Tanya Lynn, a victim of an honor killing in Georgia.
- Mentions the Supreme Court of Georgia granting the murderer a retrial over insufficient testimony about the victim's alleged infidelity.
- Points out that the court believed more testimony on her infidelity might alter the verdict.
- Emphasizes that the fact of the murder and body dumping were not in dispute.
- Reveals alarming statistics on women killed by intimate partners in the U.S.
- Connects honor killings to toxic masculinity and the view of women as property.
- Criticizes the use of sanitized terms like "intimate partner violence" instead of calling it what it is: honor killings.
- Condemns the idea that murdering a woman for infidelity is somehow justifiable.
- Stresses the link between toxic masculinity and violence, especially in cases like honor killings.

# Quotes

- "It's an honor killing."
- "This is toxic masculinity."
- "We need to call it what it is because this is toxic masculinity and that's the problem."

# Oneliner

Beau sheds light on honor killings, linking them to toxic masculinity and violence in modern society.

# Audience

Advocates against gender-based violence.

# On-the-ground actions from transcript

- Advocate for proper terminology in addressing gender-based violence (suggested).
- Support organizations working to combat toxic masculinity and violence against women (implied).

# Whats missing in summary

Full context and emotional depth.

# Tags

#HonorKillings #ToxicMasculinity #GenderViolence #Awareness #CommunitySafety