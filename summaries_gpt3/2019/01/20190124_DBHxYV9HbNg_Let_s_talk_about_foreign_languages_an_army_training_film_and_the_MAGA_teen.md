# Bits

Beau says:

- Emphasizes the importance of speaking the same "language" as the audience to aid communication and understanding, especially for activists and academics.
- Recounts an old army training film where a new lieutenant's attempts to communicate with his troops are hindered by a lack of understanding and respect for rank.
- Describes the significance of a first sergeant in the army and how his authority can impact communication and decision-making.
- Illustrates the importance of finding the right messenger to effectively deliver a message, even if the message itself is well-constructed.
- Mentions the need for considering presentation and perception in communication, citing examples of viral incidents where initial perceptions shifted with more information.
- Warns against jumping to conclusions based on initial viral content, pointing out the power of messaging and its ability to shape perception.

# Quotes

- "Sometimes it may not be that you're constructing a poor argument. It may be that you're just not the person that can deliver it to the audience."
- "The way something is presented is often times more important than the information presented in it."
- "Messaging is important and the way things are perceived a lot of times can be more important than the facts."
- "If we're fighting fake narratives, fake emergencies, we can't succumb to the same things."
- "The reaction wasn't because of the message, but because of the language used when it was first circulated."

# Oneliner

Beau underscores the importance of speaking the audience's "language," choosing the right messenger, and being wary of initial perceptions in communication to combat misinformation effectively.

# Audience

Communicators, Educators, Activists

# On-the-ground actions from transcript

- Find the right messenger for your message (exemplified)
- Tailor your communication to your audience's understanding (exemplified)
- Be cautious of initial perceptions and viral content (exemplified)

# Whats missing in summary

The full transcript provides a detailed narrative on effective communication strategies, historical military dynamics, and the impact of presentation on perception, offering valuable insights for navigating messaging in various contexts.

# Tags

#Communication #Messaging #Perception #AudienceEngagement #CombatMisinformation