# Bits

Beau says:

- Four cops were shot during a box drug raid in Houston.
- The reasons for the shooting are likely due to telegraphed movements or a lack of proper threat assessment.
- The focus of the talk tonight is on the response to the shooting, not the shooting itself.
- The police union president, Joe Jamali, issued a threatening statement in response to those critical of police officers.
- Beau shares an interesting statement about training from a group in Texas.
- This group is willing to train 15 officers for free if Joe Gimaldi resigns.
- Beau stresses the importance of freedom of speech and critiquing the government without fear of reprisal.
- He questions whether Joe Jamali cares enough about his officers to resign for their benefit.
- The offer of free training for officers is contingent on the resignation of Joe Gimaldi.
- Beau concludes with a call for Joe Jamali to resign for the sake of freedom and proper training for officers.

# Quotes

- "If you don't want to be cast as the enemy, don't be the enemy of freedom."
- "You representing an armed branch of government do not get to tell people what they can and cannot talk about."
- "Mr. Jamali, do you care about your officers enough to resign so they can get the training they need for free?"

# Oneliner

Four cops shot in Houston, but Beau focuses on police response, offering free training if union president resigns for stifling freedom of speech.

# Audience

Community members, advocates.

# On-the-ground actions from transcript

- Contact the group from Texas offering free training to see how to support their initiative (suggested).
- Advocate for police accountability and freedom of speech in your community (implied).

# Whats missing in summary

The emotional impact and detailed analysis can be best experienced by watching the full transcript. 

# Tags

#HoustonShooting #PoliceAccountability #FreedomOfSpeech #CommunityAction #TrainingInitiative