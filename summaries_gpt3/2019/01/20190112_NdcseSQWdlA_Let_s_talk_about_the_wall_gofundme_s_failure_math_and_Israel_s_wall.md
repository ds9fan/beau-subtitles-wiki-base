# Bits

Beau says:

- Explains the situation with the wall GoFundMe being refunded.
- Questions the intention behind redirecting donations to a new non-profit.
- Criticizes the misunderstanding that donations could dictate government actions.
- Breaks down the math behind the raised funds, exposing the minimal percentage in relation to the wall cost.
- Asserts that the lack of funding does not indicate widespread support for the wall.
- Emphasizes Americans' opposition to the wall on moral, ethical, legal, and economic grounds.
- Draws parallels between the proposed US wall and Israel's wall, focusing on differences in design and purpose.
- Argues that Israel's wall, often cited as a success, has not effectively stopped terrorism.
- Concludes by questioning the feasibility and effectiveness of a similar wall in the US.

# Quotes

- "Americans don't want the wall. They know that it's morally wrong, ethically wrong, legally wrong, economically wrong, and mathematically wrong."
- "If just a third of the people in the United States wanted the wall, it could have raised more than a hundred million dollars, but it didn't."
- "The reason the GoFundMe was an utter failure other than its complete lack of understanding of how the country works is the fact that Americans don't want the wall."
- "I think most Americans shudder at the idea of a foreign head of state looking at our president in 20 years going, Mr. President, tear down this wall."
- "Their wall is not a line that can be bypassed. Their wall surrounds their opposition."

# Oneliner

Beau breaks down the flawed math and misconceptions behind the wall GoFundMe, revealing Americans' strong opposition to the wall and questioning its feasibility based on Israel's example.

# Audience

American citizens

# On-the-ground actions from transcript

- Educate others on the inaccuracies surrounding the wall GoFundMe and the implications of supporting such initiatives (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the financial and ethical implications of the wall GoFundMe, shedding light on the public sentiment and debunking misconceptions surrounding border walls.

# Tags

#WallGoFundMe #BorderWall #PublicSentiment #EthicalConcerns #Misconceptions