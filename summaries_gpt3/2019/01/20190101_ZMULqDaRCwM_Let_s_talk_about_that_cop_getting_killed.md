# Bits

Beau says:

- Mentions the names of cops killed in the last 60 days by gunfire, some heroes like Sergeant Helles and Corporal Singh.
- Talks about how headlines focus on an officer's immigration status rather than their heroic actions.
- Compares the narrative around immigrant crime to the Molly Tibbets case and the flawed logic of holding entire demographics accountable for one person's actions.
- Criticizes the inconsistency in applying laws and concerns about bias in studies from organizations like FAIR and CIS.
- Raises the issue of race, pointing out the bias against brown people and the lack of outrage for officers like Officer Smith who was recently killed.
- Draws parallels between the lack of attention to certain military incidents based on political narratives and agendas.
- Urges people to stop using the graves of honorable individuals for political points.

# Quotes

- "Stop standing on the graves, the corpses, the flag-covered coffins of people far more honorable than you to make a political point they might not agree with."
- "You don't know the names of those cops, more than likely."
- "People far more honorable than you."

# Oneliner

Beau addresses the focus on officers' immigration status over their heroism, challenges the narrative on immigrant crime, criticizes bias in studies, raises issues of race, and urges against using graves for political points.

# Audience

Activists, community members

# On-the-ground actions from transcript

- Challenge biased narratives (implied)
- Support community policing efforts (implied)

# Whats missing in summary

The emotional impact of using fallen officers for political gain.

# Tags

#PoliceBrutality #Immigration #RaceRelations #PoliticalNarratives #CommunityPolicing