# Bits

Beau says:

- Beau introduces a different topic, avoiding the expected, and aims to bring original ideas or concepts to the table through his videos.
- He criticizes Trump's speech where he claimed to swear an oath to protect the country, pointing out the importance of swearing to defend the Constitution instead.
- Beau stresses that the Constitution's ideas must outlive the existence of the United States itself.
- He praises the revolutionary aspects of the Constitution, particularly the idea of a government by the people and its adaptability through amendments.
- Beau acknowledges the flawed nature of the Founding Fathers, like Thomas Jefferson, who despite owning slaves, recognized the moral depravity of slavery.
- Emphasizes that the Constitution is a living document and expresses his idealistic view of a world where government becomes unnecessary.
- Beau quotes Thomas Paine on government being a necessary evil and advocates for moving towards a society where government is no longer needed.
- He calls on people to use their voices to speak against injustices and corruption in government, implying a need for activism and vigilance from citizens.

# Quotes

- "When fascism comes to the United States, it'll be wrapped in a flag and carried across."
- "Government, even in its best state, is but a necessary evil."
- "Getting rid of government is not destroying the Constitution. It's carrying it to its logical conclusion."

# Oneliner

Beau challenges the importance of swearing an oath to defend the Constitution over the country itself, advocating for a society where government becomes unnecessary and individuals actively speak out against corruption and injustice.

# Audience

Citizens, Activists

# On-the-ground actions from transcript

- Speak out against injustices and corruption in government by using your voice (implied)
- Advocate for change through activism and vigilance in upholding democratic values (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of the importance of upholding democratic values and the evolving nature of governance through citizen activism.

# Tags

#Democracy #Constitution #Activism #Government #Freedom