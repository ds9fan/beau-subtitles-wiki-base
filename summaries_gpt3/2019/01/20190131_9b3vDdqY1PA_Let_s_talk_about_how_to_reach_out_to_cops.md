# Bits

Beau says:

- Explains the philosophy behind the ACAB slogan, which stands for "All cops are bad because they will enforce unjust laws" or because if good cops don't speak out against bad cops, they are complicit.
- Acknowledges that slogans like ACAB serve a purpose in energizing like-minded individuals but also points out that they can turn off those who don't agree.
- Suggests focusing on harm reduction and approaching the issue from a self-interest perspective to encourage engagement and critical thinking.
- Points out that the drug war is a significant factor in the strained relationship between law enforcement and the public, leading to militarization and negative perceptions of police.
- Uses statistics to illustrate the failures of the drug war and how countries like Portugal have seen positive outcomes by decriminalizing drugs.
- Criticizes the image that many police officers hold of themselves, likening their actions in drug raids to Gestapo tactics and warning about the potential future perception of law enforcement.
- Challenges the idea of police officers as patriots by questioning their enforcement of certain laws and suggesting that blind obedience to orders is detrimental.

# Quotes

- "If there's no harm, there's no crime. If there's no victim, there's no crime."
- "The drug war and gun control are probably the two best ways to reach out to cops."
- "Most of them see themselves as true patriots, true patriots."
- "Not waging the drug war is more effective than waging it if you actually believe in harm reduction."
- "So give it to them. give it to them."

# Oneliner

Beau explains the philosophy behind ACAB, suggests focusing on harm reduction and self-interest, and criticizes the failures of the drug war while challenging police perceptions of themselves.

# Audience

Activists, Police Reform Advocates

# On-the-ground actions from transcript

- Reach out to police officers to have constructive dialogues about harm reduction and the failures of the drug war (implied).
- Advocate for decriminalization and harm reduction policies in your community (implied).
- Encourage critical thinking and engagement by approaching issues from a self-interest perspective (implied).

# Whats missing in summary

Exploration of the potential impact of changing police perceptions and approaches on community-police relations.

# Tags

#PoliceReform #ACAB #HarmReduction #DrugWar #CommunityPolicing