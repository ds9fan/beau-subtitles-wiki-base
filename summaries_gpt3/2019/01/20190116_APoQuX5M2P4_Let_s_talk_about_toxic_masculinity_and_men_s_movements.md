# Bits

Beau says:

- Exploring toxic masculinity and its connection to men's movements.
- Men feeling marginalized by feminism and the separation from their boys.
- Emotional damage caused by constant accusations of sexism like the Me Too movement.
- The mythopoetic men's movement in the 80s and 90s aimed to reclaim slipping masculinity.
- Crediting the mythopoetic movement with reintroducing rites of passage and coining the term "toxic masculinity."
- Toxic masculinity distinguishing hyper-masculine immature behavior from deep masculine qualities.
- Concerns about men becoming hyper-masculine or feminized.
- Glorification of violence leading to men wanting to use violence for everything.
- Loss of ideals associated with masculinity and the glorification of violence.
- Real masculinity involving introspection, knowing oneself, honor, and integrity.

# Quotes

- "Not saying hello can get a woman killed."
- "Masculinity is not a team sport."
- "Men are more violent. That's fact."

# Oneliner

Beau delves into toxic masculinity, the mythopoetic men's movement, and the glorification of violence, challenging perceptions of masculinity and the need for introspection.

# Audience

Men, activists, allies

# On-the-ground actions from transcript

- Join organizations promoting healthy masculinity (exemplified)
- Educate others on the importance of introspection and knowing oneself (implied)
- Challenge toxic masculinity in everyday interactions (exemplified)

# Whats missing in summary

The full transcript provides a comprehensive exploration of toxic masculinity, the impact of men's movements, and the glorification of violence that shapes perceptions of masculinity.

# Tags

#ToxicMasculinity #Men #GenderRoles #Violence #Activism