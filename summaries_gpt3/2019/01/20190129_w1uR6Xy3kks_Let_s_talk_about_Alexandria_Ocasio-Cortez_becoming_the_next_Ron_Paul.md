# Bits

Beau says:

- Made a controversial comparison between Alexandria Ocasio-Cortez and Ron Paul, causing a stir among his libertarian friends.
- Discovered that 10-15% of his viewers are overseas, observing the U.S. like the fall of the Roman Empire.
- Describes Ron Paul as a libertarian icon known for his strict adherence to the Constitution and opposition to government overreach.
- Contrasts the views of Ron Paul and Alexandria Ocasio-Cortez, noting their differing perspectives on corporate greed versus government overreach.
- Suggests that while AOC and Ron Paul seem very different, they may both have valid points in critiquing the establishment.
- Points out the potential for AOC to grow in influence and impact, similar to how Ron Paul did over time.
- Raises the question of whether there will be someone to carry on Ron Paul's legacy and if the Democratic Party establishment will allow it to happen.

# Quotes

- "What if it's not corporate establishment versus government establishment? What if it's just establishment versus you?"
- "With a little bit of refinement and a little bit of time, she'll grow just like everybody does."
- "I think that we're looking at a powerhouse in the making."

# Oneliner

Beau contemplates the similarities and differences between Alexandria Ocasio-Cortez and Ron Paul, sparking a debate on establishment politics and the potential for change within the Democratic Party.

# Audience

Political enthusiasts

# On-the-ground actions from transcript

- Analyze and challenge establishment politics (implied)

# Whats missing in summary

Deeper insights into the evolving political landscape and the potential impact of emerging leaders like Alexandria Ocasio-Cortez. 

# Tags

#Politics #RonPaul #AlexandriaOcasioCortez #Establishment #Change #DemocraticParty