# Bits

Beau says:

- Beau addresses the topic of armed teachers in schools, describing it as a Band-Aid solution at best for a serious issue.
- Training teachers to use firearms effectively in active shooter situations is deemed impractical due to lack of resources and intense training requirements.
- The dynamics of a teacher being in a live shooting scenario are discussed, focusing on the high stakes and intense pressure to protect students.
- Suggestions are made for creating mobile cover using filing cabinets with steel plates and implementing frangible ammo to reduce risks to students.
- Beau underlines the psychological impact on teachers and the unrealistic expectations placed on them in such scenarios.
- He advises teachers to realistically gauge their ability to handle the responsibilities and risks associated with being armed in a school setting.
- The transcript concludes with an emphasis on exploring alternative solutions and setting better examples rather than relying on arming teachers.

# Quotes

- "It's a Band-Aid on a bullet wound at best."
- "You're talking about close quarters combat in incredibly, incredibly difficult scenarios."
- "You alone as an individual, as a teacher, as an educator, you have to become the expert in combat."
- "Liability and effectiveness don't go hand in hand. This is America today."
- "This isn't going to solve the problem. It's just a thought."

# Oneliner

Beau addresses the impracticality and risks of arming teachers, stressing the intense training needed for close-quarters combat and the need for better solutions.

# Audience

Teachers, educators

# On-the-ground actions from transcript

- Build mobile cover using filing cabinets with steel plates (exemplified)
- Implement frangible ammo to reduce risks to students (exemplified)
- Realistically gauge your ability to handle the responsibilities and risks associated with being armed in a school setting (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of the challenges and risks associated with arming teachers, offering practical insights and considerations for educators facing such situations.

# Tags

#ArmedTeachers #SchoolSafety #TeacherTraining #CombatSkills #RiskAssessment