# Bits

Beau says:

- Beau starts by addressing the internet people and delves into discussing razors, sparked by a Gillette ad his son brought up.
- His son's reaction to the ad made him proud, as it showcased his understanding of the ad's message against sexual assault, belittling, bullying, and violence.
- Beau asserts that the ad wasn't an attack on masculinity but a call for positive traits like honor and integrity.
- He questions why some men didn't see themselves as the ones stepping forward in the ad's message.
- Beau expresses surprise at feminists' discontent with a corporation leveraging feminism for sales, but he views the ad as promoting real masculinity more than feminism.
- Addressing objections about a razor company entering social issues, Beau defends Gillette's role in sparking a necessary societal debate.
- He contrasts modern rites of passage like shaving with historic brutal rituals that aimed to foster honor and integrity in men.
- Beau draws parallels between ancient rites of passage, like the agoghi, and the values portrayed in the Gillette ad.
- He describes a contemporary Amazonian rite where boys endure painful bullet ant bites to prove their manhood, contrasting it with American reactions to discomfort.
- Beau concludes by praising his son's maturity in understanding the ad's message, juxtaposing it with the fragile masculinity displayed by those upset with it.

# Quotes

- "My 12-year-old boy was more of a man than the people that are upset with this because he didn't identify with the bad guy."
- "If you felt attacked by that ad, it says more about you than it does Gillette."
- "They were creating warriors, real warriors."
- "Our boys need to be tough like that."
- "Any worthy goal is going to require you to suffer."

# Oneliner

Beau tackles misconceptions around masculinity and rites of passage, championing honor, integrity, and resilience in the face of discomfort and societal change.

# Audience

Men, feminists, society

# On-the-ground actions from transcript

- Start a constructive community debate on modern masculinity (suggested)
- Teach positive values like honor and integrity to young boys (implied)
- Challenge toxic masculinity in personal interactions (implied)

# Whats missing in summary

The full transcript provides deeper insights into societal expectations of masculinity and the necessity for positive values in rites of passage.

# Tags

#Masculinity #RitesOfPassage #GilletteAd #AmazonianCulture #HonorAndIntegrity