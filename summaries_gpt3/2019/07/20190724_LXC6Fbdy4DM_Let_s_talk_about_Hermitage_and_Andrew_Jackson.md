# Bits

Beau says:

- Beau provides historical context by talking about Andrew Jackson's victory at the Battle of New Orleans, which propelled him to the national scene.
- Jackson's victory happened after the War of 1812 had technically ended with the Treaty of Ghent signed, but news hadn't reached him yet.
- Jackson's success at the Battle of New Orleans, though attributed to a British battle plan failure, made him a hero and later helped him become President.
- Beau recalls visiting Jackson's plantation, Hermitage, as a child, where the sanitized version of plantation life was presented without addressing slavery or cruelty.
- He mentions growing up in an echo chamber in Tennessee with Confederate flags prevalent, hindering meaningful discourse.
- Beau draws parallels between the echo chamber of his upbringing and the potential echo chamber within ICE and Border Patrol regarding immigration issues.
- Public opinion has shifted on immigration, but the battles may continue despite the war being deemed over.
- Beau warns those working for ICE and Border Patrol about potential future repercussions and job prospects, contrasting their situation with that of political figures.
- He mentions a diverse group in Hermitage who publicly broke the law to rescue a father and son from ICE, showcasing opposition to current policies.
- Beau sees this incident as a turning point, indicating the strong level of opposition that could impact the future employment prospects of those in Border Patrol and ICE.

# Quotes

- "The view inside an echo chamber, the view inside of an agency like that, it's not the view of the public and the talking points on TV."
- "Hermitage, an impossibly diverse group of people, ages, ethnicities, languages, everything, banded together and broke the law publicly, no masks, broke the law to get that father and son away from ICE."
- "That's the level of opposition that exists."

# Oneliner

Beau analyzes historical echoes, warns against echo chambers in ICE and Border Patrol, and hails community action in Hermitage against ICE, foreseeing repercussions for those enforcing questionable policies.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Band together with neighbors to oppose unjust policies and take direct action to protect vulnerable community members (exemplified)

# Whats missing in summary

The full transcript provides a detailed examination of historical echoes, warning against echo chambers in enforcing agencies, and celebrating community action against unjust policies. Viewing the full transcript offers a deeper understanding of these interconnected themes.

# Tags

#AndrewJackson #Hermitage #CommunityAction #Immigration #HistoricalEchoes #ICE #BorderPatrol