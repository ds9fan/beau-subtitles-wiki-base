# Bits

Beau says:

- Explains the history of the Northern Triangle countries (El Salvador, Honduras, Guatemala) and their current situations.
- Details how the US intervention, support, and influence have significantly contributed to the turmoil in these countries.
- Mentions the role of organizations like MS-13 and Barrio-18, originally from the US but transplanted to El Salvador, in causing violence and instability.
- Describes the impact of US interventions in Honduras, particularly in controlling the economy and military.
- Talks about the United Fruit Company's control over Guatemala and the subsequent interventions that disrupted the country's attempts at self-governance.
- Criticizes US actions, including supporting coups and overthrowing democratically elected governments, leading to prolonged civil unrest and human rights abuses in these countries.
- Calls for accountability for the US's role in destabilizing Central and South America.
- Emphasizes the need for the US to stop causing harm in other countries rather than fortifying itself against the consequences.

# Quotes

- "We don't need to put better locks on our houses of the United States because other people are breaking in, we just need to stop setting other people's houses on fire."
- "It isn't that they can't fix their country. They did it multiple times. It got overthrown because you can't fix your country."
- "We can't constantly allow our government to do this and then cry when people try to get away from the situations that we created."

# Oneliner

Beau explains how US interventions have fueled instability in the Northern Triangle, urging accountability and a shift in foreign policy to stop perpetuating harm in Central and South America.

# Audience

Policy Makers, Activists, Advocates

# On-the-ground actions from transcript

- Hold policymakers accountable for past and present US interventions (implied)
- Advocate for a shift in US foreign policy towards non-intervention and support for self-governance in affected countries (implied)
- Support organizations working towards justice and stability in Central and South America (implied)

# Whats missing in summary

The full transcript provides a detailed historical context and analysis of US interventions in Central and South America, shedding light on the root causes of migration and instability in the region.

# Tags

#USIntervention #CentralAmerica #Migration #Accountability #ForeignPolicy