# Bits

Beau says:

- Introduction to gonzo journalism blending facts, statistics, fictions, opinions, stories, and historical tidbits to uncover truth.
- Emphasizes that everything is alleged until there's a conviction, video evidence doesn't change this fact.
- Content warning issued about discussing a sexual assault case in New Jersey involving a 16-year-old girl.
- The judge in the case, Judge James Troiano, didn't want it to leave family court for adult criminal court.
- Details of the alleged assault at a pajama party where the girl got drunk and raped.
- Beau questions the typical victim-blaming response that the girl might face if not for the evidence.
- The prosecution believes the accused texted a video of the act with a condemning caption.
- Despite the evidence, the judge hesitates to label it as rape due to the accused's background and achievements.
- Class distinctions and the privilege of the accused are brought into focus as factors influencing the case.
- Beau stresses that without the video evidence, the case might not have been pursued.
- Reminds the audience that most cases don't have a video, urging reflection on believing victims without such evidence.

# Quotes

- "Everything is alleged until there's a conviction."
- "In most cases, the accused does not text a video."
- "Without that video, never would have gone anywhere."

# Oneliner

Beau blends journalism styles to uncover truth, challenges victim-blaming narratives in a sexual assault case, reminding us to believe victims even without video evidence.

# Audience

Journalists, advocates, allies

# On-the-ground actions from transcript

- Support survivors of sexual assault (implied)
- Believe victims even without video evidence (implied)
- Advocate for fair treatment in the justice system (implied)

# Whats missing in summary

The emotional impact and depth of victim-blaming and privilege in legal proceedings.

# Tags

#Journalism #SexualAssault #VictimBlaming #BelieveSurvivors #JusticeSystem