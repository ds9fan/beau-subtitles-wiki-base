# Bits

Beau says:
- Beau missed a few days but assures everyone he is fine after attending a conference in Vegas.
- He attended AnarchoVegas, a big event for him, and had a unique experience.
- Beau interacted with a Taco Bell employee who recognized him from YouTube, sparking a meaningful connection.
- The conference included a fundraiser for the Free Ross campaign, aiming to raise funds for Ross Ulbricht's legal defense.
- The event mainly consisted of entrepreneurs striving to use their businesses for social change, alongside street activists.
- Beau reflected on feeling out of place but acknowledged the value of attending such conferences to learn and network.
- He realized the importance of following his own advice on getting involved based on skill set, leading him to plan community-building events in the deep south.
- Beau emphasized the significance of informal gatherings for networking and community-building, aiming to start the initiative in the deep south before expanding elsewhere.

# Quotes

- "I learned a whole lot, probably more than I contributed while I was there."
- "If you have the means, you have the responsibility."
- "Everybody who shows up to an event about building a community network, that's your network."
- "Rather than it being a lecture, it's a conversation."
- "You need to be attending these things, you need to be going because you can learn a lot."

# Oneliner

Beau shares insights from attending AnarchoVegas, stressing the importance of networking and community-building based on skill sets and responsibilities.

# Audience

Activists, Entrepreneurs, Community Builders

# On-the-ground actions from transcript

- Start organizing informal gatherings in your community to build networks and initiate social change (suggested).
- Attend conferences or events related to your skills and interests to network and learn from others (suggested).
- Participate in community-building initiatives to widen your network and influence society positively (suggested).

# Whats missing in summary

The full transcript provides a comprehensive look into Beau's experience at AnarchoVegas and the lessons learned about networking, community-building, and taking responsibility for social change initiatives.

# Tags

#Networking #CommunityBuilding #Responsibility #Activism #Entrepreneurship