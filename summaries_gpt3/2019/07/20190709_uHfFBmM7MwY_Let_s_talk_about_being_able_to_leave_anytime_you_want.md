# Bits

Beau says:

- Sharing stories from others' lives can help put your views and life into perspective.
- A family goes into hiding on July 9th out of fear of being rounded up after their asylum request is denied in the United States.
- The inevitable end of the family's story is being given up, rounded up, and separated in camps.
- Drawing a parallel to Anne Frank, who went into hiding on July 9th and later died in a camp.
- The comparison between people who turn in asylum seekers and those who remained silent during historical atrocities.
- Americans tend to remain ignorant of global situations to avoid facing culpability for actions done in their name.
- Refugees come to the U.S. due to interventions in their countries, seeking asylum, facing dehumanization and propaganda.
- The reality of conditions in camps, including audio tapes of children crying for their parents.
- The argument that refugees staying despite horrific conditions at home shows asylum is necessary.
- Calling for learning from mistakes and not repeating history by dehumanizing asylum seekers.

# Quotes

- "We like to remain blissfully ignorant of the situation of the world, and we do this as a defense mechanism."
- "They're people. And because we do this, we can say cute little things like, oh well, they just should have followed the law."
- "If we learn from our mistakes, the next time you hear a story about somebody collaborating with one of the worst regimes of the 20th century, you won't think it's about you."

# Oneliner

Sharing stories to put views in perspective, asylum seekers facing dehumanization, a call to learn from mistakes.

# Audience

Advocates for human rights.

# On-the-ground actions from transcript

- Contact local refugee support organizations to offer assistance and support (suggested).
- Educate yourself on asylum processes and advocate for humane treatment of refugees (implied).
- Volunteer at or donate to organizations working with asylum seekers (suggested).

# Whats missing in summary

The emotional impact and depth of understanding gained from Beau's storytelling and historical parallels.

# Tags

#Refugees #AsylumSeekers #HumanRights #Dehumanization #LearningFromMistakes