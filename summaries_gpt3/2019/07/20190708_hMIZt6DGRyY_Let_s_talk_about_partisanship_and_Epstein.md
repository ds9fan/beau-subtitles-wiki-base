# Bits

Beau says:

- Exploring the dangers of partisanship and blind loyalty to a political party.
- Mentioning recent headlines showcasing partisanship in the media.
- Speculating on the potential involvement of influential figures in criminal activities.
- Addressing the issue of overlooking wrongdoing based on party affiliation.
- Drawing parallels between excuses made based on past actions of different political figures.
- Criticizing the hypocrisy in condemning certain actions while supporting similar ones.
- Expressing disbelief at the lack of outrage over serious allegations in certain situations.
- Warning about the dangerous consequences of prioritizing political allegiance over morality.
- Urging people to speak out against injustice and not remain silent.
- Concluding with a call for reflection on complicity in enabling harmful actions.

# Quotes

- "I don't fault the second person in a gang rape. If it's wrong, it's wrong."
- "If you ever reach the point where you can up play or down play child abuse of that type for political ends, that should be a red flag."
- "We might want to remember that these types of things occur because we silently consent to it."
- "These things happen because we allow it."
- "You have lost yourself in support of an elephant or a donkey."

# Oneliner

Beau delves into the dangers of partisanship, urging individuals to prioritize morals over political allegiance and speak out against injustice.

# Audience

Active citizens

# On-the-ground actions from transcript

- Speak out against injustice and wrongdoing, regardless of political affiliation (implied)
- Raise your voice against harmful actions and policies in your community (implied)
- Refuse to overlook immoral behavior based on political party loyalty (implied)

# What's missing in summary

The emotional impact and tone of Beau's message, along with the urgency for individuals to take a stand against harmful actions enabled by partisanship.

# Tags

#Partisanship #PoliticalAllegiance #Injustice #Morality #SpeakOut