# Bits

Beau says:

- Explains the symbolism of Curious George on his hat and how it relates to his videos.
- Recounts his history with Curious George as a mascot for a group he was part of.
- Talks about his middle son's love for monkeys and how it reignited his interest in Curious George.
- Acknowledges problematic themes in Curious George stories, particularly regarding a white man taking a monkey from Africa.
- Discovers the true story behind Curious George, where George was actually Fifi, a monkey from South America, and his human parents were Jewish refugees escaping the Nazis in France.
- Expresses admiration for how the story of Curious George/Fifi mirrors the resilience and resourcefulness of refugees.
- Draws parallels between the refugee experience and the potential positive contributions refugees can make to society.
- Expresses gratitude for learning the true story of Curious George/Fifi and its impact on his perspective.

# Quotes

- "They're going to better the country if you let them."
- "They're going to re-instill that spirit of being able to accomplish the impossible."
- "All of those things that we pretend are still American traits."
- "It's a great story."
- "That's my thought."

# Oneliner

Beau explains the symbolism of Curious George, uncovers the true refugee story behind it, and advocates for welcoming refugees to better the country.

# Audience

Viewers, Refugee Advocates

# On-the-ground actions from transcript

- Welcome and support refugees in your community by offering assistance and resources (implied).
- Advocate for policies that facilitate the integration of refugees into society (implied).
- Educate others about the valuable contributions refugees can make to society (implied).

# Whats missing in summary

The emotional impact and personal reflection Beau experiences upon learning the true story of Curious George/Fifi.

# Tags

#CuriousGeorge #Refugees #Resilience #Symbolism #Advocacy