# Bits

Beau says:

- United States is resilient and populated by resourceful people.
- Americans can multitask and handle multiple challenges simultaneously.
- Challenges like fighting wars and sending a man to the moon were tackled simultaneously in the past.
- The issue at hand is homelessness among veterans.
- On average, 40,056 homeless veterans spend their nights on the streets.
- To address this problem, facilities offering food, beds, showers, hygiene items, psychological support, education, and recreation are needed.
- In 2018, there were over 180 facilities with more than enough beds to house 40,520 people.
- Beau proposes moving criminal aliens to county jails and releasing non-criminal migrants for court dates, in line with laws and international norms.
- Retraining guards in existing facilities for job reentry programs could solve the problem.
- Beau believes the money needed for this solution has always been available.
- Many people use homeless vets as an excuse to avoid helping others.
- Combat veterans, according to Beau, are compassionate and willing to help others in need.
- Beau questions the deportation of veterans and the lack of care for homeless vets.
- He suggests repurposing existing camps into facilities for homeless vets as a viable solution.
- Beau challenges the idea that helping migrants is prioritized over homeless vets, pointing out the available resources for veterans.

# Quotes

- "We can do more than one thing at a time."
- "The money's always been there for this to be done."
- "You want an excuse to not help other people, and they're using homeless vets to do that."
- "If you want a solution, there it is. Turn these camps into facilities to house homeless vets. Done."
- "The question is why do you want to lock up toddlers instead of helping homeless vets?"

# Oneliner

Beau challenges the false dilemma of helping migrants or homeless vets, proposing existing solutions for veteran homelessness while critiquing misplaced priorities.

# Audience

Advocates for homeless veterans

# On-the-ground actions from transcript

- Repurpose existing camps into facilities for homeless veterans (suggested)

# Whats missing in summary

The full transcript provides a passionate call to action for repurposing existing resources to address veteran homelessness effectively.

# Tags

#VeteranHomelessness #Priorities #Solutions #CommunitySupport