# Bits

Beau says:

- Today is Earth Overshoot Day, meaning all resources that will be regenerated by the Earth by the end of the year are already used up.
- Overconsumption and mitigating climate change often lead to accusations of socialism.
- Socialism is not just when the government does stuff; it's about community control of production, distribution, and exchange.
- Mitigating climate change is not socialism; in fact, not addressing climate change is a more likely path to socialism in the United States.
- Doomsday scenarios of flooding and disasters will lead people from affected areas to move to other states with better infrastructure, mainly red states.
- The influx of people fleeing climate disasters will require significant central planning and government involvement to allocate resources.
- History shows that in emergencies like nuclear threats, government control of production and distribution has been necessary.
- To avoid a future with authoritarian socialism due to climate crises, becoming conservationists and changing consumption habits is imperative.
- Failure to address consumption and unsustainable living will lead to an emergency situation where authoritarian socialism may become a reality.
- The choice is between embracing sustainable practices or facing a future of authoritarian socialism in emergency conditions.

# Quotes

- "You can become green, or you can become red."
- "If we don't do something about consumption, we're going to get socialism."
- "It's your choice."
- "We're going to get the one with guns and boots."
- "Means of production, distribution, and exchange. That's critical to socialism."

# Oneliner

Today is Earth Overshoot Day, consumption choices today determine whether we embrace sustainability or face authoritarian socialism tomorrow.

# Audience

Environmental advocates, policymakers

# On-the-ground actions from transcript

- Advocate for sustainable consumption practices within your community (implied)
- Support policies that mitigate climate change and prioritize environmental conservation (implied)

# Whats missing in summary

The urgency and potential consequences of failing to address overconsumption and climate change in the context of potential authoritarian socialism.

# Tags

#EarthOvershootDay #ClimateChange #Socialism #Conservation #Sustainability