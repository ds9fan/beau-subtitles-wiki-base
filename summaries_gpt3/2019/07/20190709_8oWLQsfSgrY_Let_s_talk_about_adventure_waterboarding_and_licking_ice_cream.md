# Bits

Beau says:

- Recalls a past demonstration where he was waterboarded by another person as part of an activist event following the US torture report release.
- Describes how a young man volunteered to be waterboarded during the demonstration despite Beau's skepticism.
- Talks about the importance of adventures in shaping history, especially during times of social upheaval.
- Mentions the need for individuals to choose their own level of involvement in activism.
- Encourages the youth to embrace adventure in service of a greater cause rather than engaging in superficial acts like licking ice cream.
- Emphasizes that impactful change often comes from individuals taking risks and going on adventures.
- Urges people to decide what they are willing to do in terms of activism and not let others dictate their level of involvement.

# Quotes

- "We're Coming into a period in American history that's going to require a lot of adventures."
- "You can change the world, or not."
- "It could be anything from donating to lawyers, to calling your senator, to marching, to organizing a protest, to anything, leafletting, all kinds of stuff."
- "If you let other people do that for you, you're going to end up over your head."
- "Take that adventure in service of something greater than yourself."

# Oneliner

Beau believes embracing adventures in activism can shape history and encourages individuals to choose their level of involvement to create meaningful change.

# Audience

Youth activists

# On-the-ground actions from transcript

- Reach out to the youth and encourage them to take meaningful actions (implied)
- Decide your own level of involvement in activism (implied)
- Embrace adventures in service of a greater cause (implied)

# Whats missing in summary

The full transcript provides a deep reflection on the significance of adventures in activism and the importance of individual involvement in creating impactful change.