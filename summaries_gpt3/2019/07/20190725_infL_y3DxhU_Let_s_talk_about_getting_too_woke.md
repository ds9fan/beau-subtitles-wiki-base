# Bits

Beau says:

- Explains the concept of "getting woke" when people see the establishment for what it truly is and start understanding the finer details of control systems, money, and politics.
- Mentions how veterans often experience disillusionment after multiple tours, realizing they are guarding poppy fields for the pharmaceutical industry's profit.
- Notes that the issue arises when this awareness leads to despair, especially when individuals perceive the opposition as overwhelmingly powerful.
- Describes a friend who feels exhausted and stagnant in their efforts, unable to see any progress or victory.
- Observes that the black community frequently faces this phenomenon, understanding the corrupt system but then feeling so woke that they lose hope and fall into depression.
- Emphasizes that while recognizing the system's corruption and power is not problematic, feeling like victory is unattainable is where the real issue lies.
- Encourages the idea of continuous fight and not succumbing to despair, reminding that change may take a long time but it's vital to hold onto the hope for a better world.

# Quotes

- "There's nothing wrong with getting woke."
- "It's okay to get woke, but never get so woke that you can't dream anyway."
- "We don't have to beat them today or tomorrow. We just have to keep fighting."

# Oneliner

Beau explains "getting woke," warning against despair when perceiving the establishment as unbeatable, urging continuous fight for a better world.

# Audience

Activists, Advocates, Community Members

# On-the-ground actions from transcript

- Keep fighting for a better world (implied)
- Maintain hope and vision for change (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the emotional journey of individuals as they transition from awareness to despair, underlining the importance of perseverance and hope in striving for a better future.

# Tags

#Awareness #Despair #Hope #Change #Activism