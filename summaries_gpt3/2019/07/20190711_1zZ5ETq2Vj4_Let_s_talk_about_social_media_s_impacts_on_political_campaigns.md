# Bits

Beau says:

- Social media may end up dismantling the political elite in the country by exposing inauthenticity and enabling non-establishment candidates to succeed.
- In the past, candidates could easily stage authenticity for the public, but social media now reveals the staged nature of these events.
- The rise of social media is already impacting politics, with the emergence of non-establishment candidates who win elections.
- People like AOC are examples of candidates on the fringes of the establishment gaining traction through social media.
- Social media allows for greater scrutiny of politicians' actions, making it harder for them to spin narratives like before.
- Marianne Williamson's campaign is described as anti-Trump, appealing to compassion and love rather than fear and bigotry.
- Williamson's strategy contrasts sharply with Trump's, focusing on appealing to the better instincts of humanity.
- Social media enables candidates like Williamson to target more educated Americans and run authentic campaigns.
- Candidates who are not part of the traditional power structures are finding a voice through social media platforms.
- The future of politics might involve more non-establishment figures being able to challenge the professional political class.


# Quotes

- "We're going to see more Justin Armash, AOC, candidates like this that are on the edges of the establishment."
- "She appears to be gearing up to run the exact opposite style of campaign, where she's hoping to appeal to the better instincts and the better nature of humanity."
- "It's a bold strategy. She seems, by tweeting that without any kind of explanation, she seems to be hoping and targeting the more educated Americans."
- "It is authentic. That is who she appears to be."
- "We're going to see them for the establishment. We're going to see them for those who are really there just to enrich themselves with the money they take from us."


# Oneliner

Social media is dismantling political elitism by exposing inauthenticity and enabling non-establishment candidates like AOC, paving the way for a new era in politics.


# Audience

Political Activists, Social Media Users


# On-the-ground actions from transcript

- Support non-establishment candidates in elections (implied)
- Engage in social media platforms to amplify authentic voices and challenge political elitism (implied)


# Whats missing in summary

The full transcript provides a detailed analysis of how social media is reshaping politics and enabling non-establishment candidates to challenge the traditional political class with authenticity and transparency.


# Tags

#SocialMedia #Politics #NonEstablishmentCandidates #Authenticity #PoliticalElite