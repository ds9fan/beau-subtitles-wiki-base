# Bits

Beau says:

- Beau is asked to dissect the Fullerton incident and explain what the cop did wrong.
- He expresses strong criticism towards police departments and their actions.
- Beau mentions his history of criticism towards the Fullerton PD.
- The cop in the Fullerton incident observed a woman with a firearm.
- Despite the firearm turning out to be fake, the officer had to make a split-second decision.
- The officer engaged with the woman without using excessive force.
- He called for medics before securing the scene or clearing the car, which is outside of standards.
- The officer then proceeded to render aid to the woman.
- Beau points out that the officer's actions were all within normal standards and best practices.
- He suggests that there could be a debate around implementing a "don't fire until fired upon" policy.

# Quotes

- "There's not. nothing that this cop did that any reasonable person, even extremely experienced, would do differently."
- "This wasn't a kid at a playground where one could reasonably expect it to be a toy."
- "It's just a thought."

# Oneliner

Beau explains the Fullerton incident, defending the cop's actions within normal standards, and suggests potential policy debates.

# Audience

Policing critics

# On-the-ground actions from transcript

- Debate and advocate for policies like "don't fire until fired upon" (implied)

# Whats missing in summary

Detailed breakdown of the specific actions taken by the officer and the context of the situation.

# Tags

#PoliceCriticism #UseOfForce #PolicyDebate #CommunityPolicing #PolicingStandards