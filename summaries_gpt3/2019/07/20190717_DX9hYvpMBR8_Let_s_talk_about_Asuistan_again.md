# Bits

Beau says:

- Addressing a suicide and discussing overlooked issues within the country.
- The regime in the country is denying international inspectors entrance and visas to ICC staff.
- Threatening judges to dissuade investigations into war crimes in a recent conflict.
- Military recruitment numbers have dropped due to lack of faith in the regime, leading to targeting children as young as 16 for recruitment.
- Children involved in hostilities must be 18 or older according to international law.
- Legislation in some provinces prevents military recruiters from entering campuses, prompting a digital campaign to target children.
- Clashes between protesters, rebels, and security services across the country due to security sweeps targeting a social minority group.
- Horrendous conditions and widespread sexual abuse of the targeted group leading religious institutions to shelter them.
- Centrist party within the government asking the regime's leader, Don Joe Trump, to step down, which is unlikely.
- Beau uses "Assuistan" to demonstrate how media coverage changes when events in the U.S. are portrayed as happening in another nation.
- The U.S. faces issues like targeting child soldiers, refusing international inspectors, disrupting war crimes investigations, and running concentration camps.

# Quotes

- "Assuistan is not a real country."
- "We need to look at it again."
- "In large part because of mass media."
- "It's just a thought, y'all."
- "The targeting of child soldiers, refusing international inspectors, trying to disrupt investigations into our own war crimes..."

# Oneliner

Beau addresses overlooked issues within the U.S., revealing alarming parallels with dictatorships and urging a critical reevaluation of media coverage.

# Audience

Policy makers, activists, citizens

# On-the-ground actions from transcript

- Contact religious institutions for information on how to support and help shelter the targeted group (implied).

# Whats missing in summary

The full transcript provides detailed insights into overlooked issues within the U.S. and the importance of critical media analysis.

# Tags

#US #Government #Media #WarCrimes #ChildSoldiers