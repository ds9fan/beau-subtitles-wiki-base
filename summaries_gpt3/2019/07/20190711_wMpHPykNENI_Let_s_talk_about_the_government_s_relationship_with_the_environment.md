# Bits

Beau says:

- Recalls the bombing of the Rainbow Warrior in 1985 by French intelligence due to its mission to draw attention to a French nuclear test, resulting in a death and French government conviction.
- Compares the French government's suppression of environmental advocacy to current actions by the U.S. government.
- Notes the U.S. President's contradictory claim of being a land protector while overseeing significant rollbacks of environmental regulations.
- Points out the administration's extensive list of environmental regulation rollbacks, including opening the U.S. coastline to offshore drilling and scrapping safety regulations.
- Mentions the removal of references to climate change by federal agencies, drawing parallels to the French bombing incident to prevent public discourse on climate change.
- Emphasizes that while debates exist on the origins of climate change, its occurrence is undeniable and poses a national security threat.
- Argues that governments prioritize control and power over protecting citizens, using malice to suppress climate change information.
- Compares historical military power motives for actions like the boat bombing to present-day colonization through corporate influence in environmental matters.
- Points out the presence of former industry lobbyists during the President's press conference, indicating a politicization of environmental issues driven by industry interests.
- Concludes with a reflection on the financial influence of industries like oil and coal in environmental policymaking.

# Quotes

- "Government's not here to protect you."
- "We don't colonize anymore with flags and guns, we do it with corporate logos."
- "But at the end of the day, it's happening."
- "The environment as a whole doesn't have any money but the oil and coal industry do."
- "Y'all have a good night."

# Oneliner

French intelligence bombed the Rainbow Warrior to silence environmental advocacy; Today, U.S. environmental regulations are rolled back while climate change information is suppressed for corporate interests.

# Audience

Environmental activists

# On-the-ground actions from transcript

- Organize local environmental protection events (implied)
- Support organizations advocating for environmental conservation (implied)

# Whats missing in summary

Detailed examples and historical context of environmental harm and government actions.

# Tags

#Environment #Government #ClimateChange #Regulations #IndustryInfluence