# Bits

Beau says:

- Introducing a unique video format unlike any done before.
- Explaining the reasons for not asking viewers to like, subscribe, or support Patreon in every video.
- Announcing the launch of a Patreon account due to viewer requests.
- Describing Patreon as a platform for sponsorships starting at a dollar per month.
- Detailing the perks of sponsoring, including access to a Discord server, voting on future projects, and early video releases.
- Ensuring that no content will be kept behind a paywall.
- Mentioning plans to use the Patreon funds to free up time from battling algorithms on YouTube and Facebook.
- Expressing a desire to expand into other media forms like podcasts and writing a book.
- Stating the intention to use the funds to pay people who might assist in these new ventures.
- Sharing a vision of potentially taking the show on the road to actively help build community networks and address social issues.

# Quotes

- "Beyond that, I'd like to branch out into other types of media, the podcast everybody's asking for, I'd like to write a book too."
- "It's tentative because we have no idea how this is going to play out."
- "The rest is up to you guys."

# Oneliner

Beau introduces a new video format, explains Patreon launch, and shares plans to expand into other media forms, seeking support for future projects and community initiatives.

# Audience

Viewers

# On-the-ground actions from transcript

- Support Beau's Patreon account to help him free up time from battling algorithms on social media and to assist in expanding into other media forms (suggested).
- Engage with the community by participating in live streams and supporting efforts to address social issues (implied).

# Whats missing in summary

More details on specific ways viewers can contribute to community-building initiatives and support Beau's expansion into other media forms.

# Tags

#ContentCreator #Patreon #CommunityBuilding #Support #SocialIssues