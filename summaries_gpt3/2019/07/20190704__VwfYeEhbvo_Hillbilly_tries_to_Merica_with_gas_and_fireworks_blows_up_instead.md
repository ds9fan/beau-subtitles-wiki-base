# Bits

Beau says:

- Beau plans to light a giant box of fireworks soaked in gasoline on the 4th of July.
- He questions the celebration of America while mentioning the government's treatment of people in concentration camps.
- Beau criticizes the lack of basic necessities like soap, toothpaste, and food for those in detention.
- He points out the indifference towards sexual abuse complaints in certain locations.
- Questioning the celebration of freedom when such injustices occur.
- Beau mentions the guarantee of the pursuit of happiness in the document but questions its application.
- Ending with a thought-provoking message about the 4th of July celebrations.

# Quotes

- "What are you celebrating? Freedom? No. No, don't think so."
- "I didn't see anything about, you know, lines in the sand prohibiting that."
- "Y'all have a happy Fourth of July. It's just a thought."

# Oneliner

Beau questions the celebration of America on the 4th of July in light of government treatment of individuals in detention, lack of investigation into sexual abuse complaints, and the pursuit of happiness being overlooked.

# Audience

Activists, Americans

# On-the-ground actions from transcript

- Question the celebration of national holidays (implied)
- Advocate for the rights and well-being of individuals in detention (implied)
- Raise awareness about overlooked injustices (implied)

# Whats missing in summary

The full transcript provides a deeper reflection on the contradictions between celebrating freedom and happiness on the 4th of July while neglecting the basic rights and well-being of certain individuals.

# Tags

#4thofJuly #Freedom #Injustice #HumanRights #America #Activism