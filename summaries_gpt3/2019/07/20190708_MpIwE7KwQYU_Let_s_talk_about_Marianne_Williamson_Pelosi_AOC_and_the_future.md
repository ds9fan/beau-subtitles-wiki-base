# Bits

Beau says:

- Examines the lessons from three women and their impact on the country's direction.
- Advocates for shedding 20th-century prejudices and embracing new perspectives.
- Criticizes the dismissal of Marianne Williamson as a viable candidate due to unconventional views.
- Challenges the hypocrisy of ridiculing Williamson's beliefs while accepting traditional forms of prayer from other candidates.
- Questions the notion of presidential behavior, contrasting showering with love versus bombing countries.
- Disagrees with Williamson on the importance of unraveling national secrets to understand sickness.
- Comments on the marketability of values like peace, love, and hope in elections.
- Addresses the pushback against younger Congress members like AOC by established figures like Nancy Pelosi.
- Considers the inevitability of more Millennials entering Congress and the impact of a growing younger voting population.
- Criticizes the lack of action on pressing issues such as border camps, incarceration rates, income inequality, drug war, and healthcare.
- Urges incumbents to question their own presence in Congress as younger generations demand change.
- Emphasizes the rising influence of a generation seeking tangible progress and holding leaders accountable.

# Quotes

- "Humanity needs a mental shower."
- "We need to wash off the prejudices of the 20th century."
- "You can get with the program or you can go home."
- "The question is why are you there still?"
- "They're not going to sit on their hands."

# Oneliner

Beau examines societal norms, challenges conventional thinking, and calls for accountability amidst generational shifts in politics and values.

# Audience

Voters, Activists, Politicians

# On-the-ground actions from transcript

- Join or support political campaigns that advocate for progressive change (exemplified).
- Get involved in local community organizing to address pressing social issues (exemplified).
- Engage in political education and encourage participation in elections, especially among younger generations (exemplified).

# Whats missing in summary

The full transcript provides a comprehensive analysis of societal norms, political stagnation, generational shifts, and the call for accountability and progressive change.

# Tags

#Politics #GenerationalShift #SocialChange #ProgressiveValues #Accountability