# Bits

Beau says:

- Disney is facing backlash for deciding to make the Little Mermaid black, with people upset because the story dates back to 1837.
- The original story from 1837 involves a mermaid saving a prince, losing her voice and identity, and ultimately dissolving into sea foam after failing to win his heart.
- The themes in the original story include giving up one's voice and identity to fit in and find love, perpetuating harmful ideas about women's worth and the importance of conforming.
- Beau criticizes those who oppose a Black Little Mermaid for not recognizing the positive theme of letting go of racial divisions present in the original story.
- He suggests that embracing a Black Little Mermaid could teach valuable lessons about breaking down racial barriers and challenging traditional narratives.

# Quotes

- "We could probably learn a lot from A Black Little Mermaid."
- "The one positive theme in it is giving up these illusions that these things make us different, that these things matter."

# Oneliner

People upset over a Black Little Mermaid fail to see the positive theme of breaking down racial divisions present in the original story, missing an opportunity to challenge harmful narratives.

# Audience

Storytellers, activists, educators

# On-the-ground actions from transcript

- Challenge traditional narratives and stereotypes (implied)
- Promote diversity and representation in storytelling (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the original Little Mermaid story and the implications of Disney's decision to introduce a Black Little Mermaid, offering insights into themes of identity, conformity, and racial divisions that can be explored further.

# Tags

#Representation #Diversity #ChallengingNarratives #RacialJustice #Empowerment