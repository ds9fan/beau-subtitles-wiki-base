# Bits

Beau says:

- Urges viewers for a call to action regarding the Connegates Amendment in the NDAA, aiming to prevent unauthorized attacks on Iran.
- Two versions of the Defense Department budget exist, with the House version containing the amendment but not the Senate version.
- The amendment requires President Trump to seek authorization from Congress before attacking Iran, limiting executive power.
- Viewers are encouraged to contact members of the Armed Services Committees, especially in the Senate, to support the amendment.
- Emphasizes the importance of maintaining the amendment to prevent potential devastating consequences akin to the Iraq war.
- Appeals to resistors, Republicans, anti-war advocates, and veterans to advocate for the amendment's retention.
- Stresses the potential loss of lives, referencing the toll of half a million in the Iraq conflict.
- Acknowledges Trump's restraint in military force but expresses concerns about his changing positions and escalating tensions.
- Calls upon viewers to take action to ensure Trump is held accountable before launching any strikes on Iran.
- Urges everyone to dedicate a few minutes to potentially save hundreds of thousands of lives.

# Quotes

- "A few minutes of your time can potentially save half a million lives or more."
- "If Trump has to go back to Congress before launching a strike, that's going to force him to come up with a good reason."
- "This is your time."
- "We need to keep this amendment in there."
- "Y'all have a good night."

# Oneliner

Beau urges action to support the Connegates Amendment in the NDAA, preventing unauthorized attacks on Iran and potentially saving half a million lives.

# Audience

Advocates, activists, citizens

# On-the-ground actions from transcript

- Contact members of the Armed Services Committees, especially in the Senate, to support the Connegates Amendment (suggested).
- Email, write, tweet to push for the retention of the amendment (suggested).

# Whats missing in summary

The urgency and importance of preventing unauthorized attacks on Iran and the potential devastating consequences similar to past conflicts.

# Tags

#CallToAction #ConnegatesAmendment #NDAA #PreventWar #Advocate