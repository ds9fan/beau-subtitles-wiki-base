# Bits

Beau says:

- Expresses love for coffee and keeps multiple flavors on hand at all times.
- Talks about Border Patrol feeling threatened by AOC, a young representative in Congress.
- Mentions jokes circulating about why Border Patrol felt threatened by AOC.
- Suggests that men in the country get their moral compass from their mothers, impacting their behavior.
- Emphasizes the importance of speaking a relatable and simple language to reach a broader audience.
- Points out the lack of education among Trump supporters and the need for simplicity in communication.
- Encourages using the word "dishonorable" to question immoral actions effectively.
- Advocates for challenging toxic masculinity and questioning the masculinity of individuals engaging in dishonorable actions.
- Urges women to lead the charge in addressing toxic masculinity and labeling actions as dishonorable.
- Stresses the need to paint Border Patrol's actions as dishonorable to drive change.

# Quotes

- "Men determine their morality based on the opinions of women."
- "Trump supporters are not a well-educated demographic."
- "It is time to use toxic masculinity as an advantage."
- "If Border Patrol starts to be seen as dishonorable, well, you'll see changes in a bunch of different ways."
- "They can't spin the morality of this. This is dishonorable."

# Oneliner

Beau challenges toxic masculinity and advocates for labeling dishonorable actions to drive change, urging simplicity in communication.

# Audience

Activists, Advocates, Women

# On-the-ground actions from transcript

- Challenge toxic masculinity by calling out dishonorable actions (exemplified)
- Use the word "dishonorable" to question immoral behavior effectively (exemplified)
- Lead the charge in addressing toxic masculinity and labeling actions as dishonorable (exemplified)

# Whats missing in summary

The full transcript provides a deeper understanding of how toxic masculinity impacts societal behavior and the importance of addressing dishonorable actions effectively.

# Tags

#ToxicMasculinity #DishonorableActions #SocietalChange #Communication #Activism