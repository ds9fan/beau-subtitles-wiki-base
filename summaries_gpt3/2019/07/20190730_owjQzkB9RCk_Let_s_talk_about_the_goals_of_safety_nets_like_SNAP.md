# Bits

Beau says:

- Most states ignore the asset requirement for SNAP, leading millionaires to receive food stamps.
- The federal government sets the asset requirement for SNAP at $2,250, including assets like cars.
- Americans are advised to save three to six months of income, but 40% can't handle a $400 emergency.
- A person on SNAP who saves up to $2,250 loses assistance, making it challenging to escape poverty.
- It takes 20 years under the current system to lift oneself out of poverty without setbacks.
- The goal of assistance programs like SNAP should be questioned: Is it to keep people above poverty or help them escape it?
- Beau suggests eliminating all means tests for SNAP to truly assist those in need.
- SNAP serves not only as a safety net but also as an economic stimulus during weak economic periods.
- Removing means tests and allowing easier access to SNAP could help people move above the poverty line.
- The issue with SNAP isn't about budget but about control and perpetuating income inequality.

# Quotes

- "If the goal is to build a stronger country, expand SNAP, don't close it."
- "SNAP is not just a safety net program. It's also an economic stimulus program."
- "It's about controlling you."
- "Expand the program. Don't shut it down."
- "Make it more accessible."

# Oneliner

Beau challenges the purpose of SNAP, suggesting eliminating means tests to truly help people escape poverty and build a stronger country.

# Audience

Advocates, policymakers

# On-the-ground actions from transcript

- Expand SNAP to make it more accessible (suggested)
- Challenge the purpose of assistance programs and advocate for change (implied)

# Whats missing in summary

The full transcript dives deeper into the impact of means tests and income inequality on SNAP recipients and society as a whole.

# Tags

#SNAP #Poverty #MeansTests #IncomeInequality #FinancialAssistance