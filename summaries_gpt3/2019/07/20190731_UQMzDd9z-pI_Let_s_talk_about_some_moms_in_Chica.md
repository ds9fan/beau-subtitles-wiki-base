# Bits

Beau says:

- Emphasizes community networking and building as a common, everyday practice for most people.
- Shares the story of two moms tragically gunned down at a bus stop in Chicago.
- Criticizes the media for misrepresenting the victims as anti-gun activists when they were actually community-building moms.
- Recognizes the organization the moms belonged to, M.A.S.K. (Moms/Men Against Senseless Killing), for their efforts in ending violence through love and increased opportunities.
- Encourages support for the GoFundMe set up by M.A.S.K. to gather information about the shooting.
- Expresses a sense of personal loss for the victims despite not knowing them personally.
- Calls for community action and not relying on law enforcement to address such issues effectively.

# Quotes

- "Community building from the ground up. Don't wait for somebody else to make it better, you do it."
- "They were moms. They lived in that community. They were just trying to make it better."
- "Trying to end violence through love and more opportunities."
- "Every dollar is going to help, because it loosens lips."
- "Now that organization has put together GoFundMe to get a reward together for information."

# Oneliner

Beau talks about the tragic shooting of two moms in Chicago, urging community action and support for M.A.S.K.'s GoFundMe to gather information and combat violence.

# Audience

Community members

# On-the-ground actions from transcript

- Support the GoFundMe set up by M.A.S.K. to gather information about the shooting (suggested).

# Whats missing in summary

The emotional impact and personal connection Beau feels towards the victims and the importance of community-driven initiatives in addressing violence effectively.

# Tags

#CommunityBuilding #ChicagoShooting #Support #EndViolence #CommunityAction