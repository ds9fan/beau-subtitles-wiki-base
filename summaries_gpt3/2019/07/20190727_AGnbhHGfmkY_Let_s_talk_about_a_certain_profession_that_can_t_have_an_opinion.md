# Bits

Beau says:

- Acknowledges viewers using videos for homeschooling.
- Reads an email criticizing his association with a woman in a particular industry.
- Shares the story of a woman active in street actions who he knew as an immigrant rights advocate.
- Emphasizes the importance of the woman's advocacy work over her past industry.
- Criticizes how people are treated differently once they voice opinions and show themselves as individuals.
- Notes the issue of objectification in different industries, like sports.
- Talks about being friends with military contractors and the lack of scrutiny compared to associations with other professionals.
- Raises societal concerns about valuing relationships with certain professions over others.

# Quotes

- "It's okay to be friends with a professional killer. It's not okay to be friends with a professional lover, I guess."
- "That's how violence happens."
- "Just shut up and play ball, right?"

# Oneliner

Beau tackles societal biases around professional relationships and advocates for valuing individuals beyond their professions.

# Audience

Activists, Advocates, Supporters

# On-the-ground actions from transcript

- Contact immigrant rights advocacy organizations to support their work (suggested)
- Join local community actions supporting marginalized groups (exemplified)
- Challenge objectification in industries by amplifying voices and stories of individuals (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of societal biases and the impact of objectification in different professions, urging viewers to re-evaluate their perspectives on relationships with individuals based on their professions.

# Tags

#SocietalBiases #Objectification #Advocacy #CommunityPolicing #Activism