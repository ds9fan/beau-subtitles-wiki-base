# Bits

Beau says:

- Lists 14 characteristics of a certain kind of government with examples like powerful displays of nationalism, disdain for human rights, identifying enemies and scapegoats.
- Mentions supremacy of the military and rampant sexism as characteristics.
- Talks about an obsession with national security, blending of religion and government, and corporate power protection or blending.
- Mentions labor suppression and disdain for intellectuals in the arts.
- Addresses obsession with crime and punishment, police brutality, cronyism, and corruption.
- Talks about fraudulent elections and the 10 stages of fascism, from classification to extermination.
- Raises awareness about the signs of genocide and ethnic cleansing happening currently.
- Urges viewers to pay attention to the unfolding situation and make a choice to correct it.

# Quotes

- "There's never been a fascist regime in history that did not go through these ten stages."
- "You have all of these lists that have been around a long, long time, and we're matching them up almost like we're following them like a blueprint."
- "There are people that need to understand this. They need to understand where we're at."

# Oneliner

Beau lists 14 characteristics of fascism, outlines 10 stages, and warns about the signs of genocide happening currently.

# Audience

Activists, Community Members

# On-the-ground actions from transcript

- Contact local representatives to demand accountability for human rights violations (implied).
- Support organizations working to protect human rights and fight against discrimination (implied).
- Educate others about the signs of fascism and genocide happening currently (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of the characteristics of fascism and the stages leading to genocide, urging viewers to pay attention and take action against these dangerous trends.

# Tags

#Fascism #Genocide #HumanRights #Activism #Awareness