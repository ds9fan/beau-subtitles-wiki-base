# Bits

Beau says:

- Responds to comments denying fascism and genocide.
- Border guards joked about a mother drinking out of a toilet.
- A Facebook group of guards planned a GoFundMe for assaulting AOC.
- The guards knew about upcoming inspections but were unfazed.
- Acts of dehumanization and abuse are rampant in detention centers.
- Deplorable conditions and inhumane treatment are overlooked.
- Laughter at dehumanization makes it easier to escalate violence.
- Points out the trend towards escalating violence and dehumanization.
- Urges action to prevent further descent into darkness.
- Calls for accountability and action to stop the worsening situation.

# Quotes

- "Guns don't kill people. Governments do and we're on our way."
- "We are headed to some very very dark times."
- "If you can sit there and laugh at a mother being told to drink out of a toilet, you'll probably be okay with putting a bullet in the back of her head."

# Oneliner

Beau responds to denial of fascism, recounts border guards' dehumanizing actions, and warns of escalating violence, urging action to prevent dark times ahead.

# Audience

Activists, Advocates

# On-the-ground actions from transcript

- Call for accountability and action to stop the worsening situation (suggested).
- Start calling, marching, or doing whatever you're comfortable with to prevent further descent into darkness (suggested).

# Whats missing in summary

The full transcript provides a detailed account of the dehumanizing treatment in detention centers and the urgency of taking action to prevent further atrocities.

# Tags

#Fascism #Dehumanization #DetentionCenters #Accountability #Activism