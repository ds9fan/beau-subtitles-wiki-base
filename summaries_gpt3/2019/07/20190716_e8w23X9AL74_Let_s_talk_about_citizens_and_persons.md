# Bits

Beau says:

- Beau engaged with internet individuals who believe undocumented workers have no constitutional rights.
- Beau found it enlightening to interact with these individuals who had misconceptions about constitutional rights.
- He explained that the Constitution guarantees rights to both citizens and persons.
- Beau's goal was to make these individuals realize that undocumented workers have rights protected by the Constitution.
- Beau pointed out that supporting the Constitution and supporting certain policies may be contradictory.
- He listed the rights guaranteed to persons under the Constitution, including the right to assemble peaceably and protection against unreasonable searches and seizures.
- Beau emphasized the importance of trials and equal protection under the law for all persons.
- He mentioned historical examples to illustrate how constitutional rights apply even in cases of terrorism.
- Beau raised the question of why some individuals seem to disregard the Constitution when discussing certain policies.
- He encouraged engaging with those who misinterpret or ignore constitutional rights, aiming to provoke critical thinking and questioning.

# Quotes

- "You can't do both. So what rights are guaranteed to persons? All the ones that matter."
- "So when you get to this point and you've explained all of this to them, you can then ask, you know, why do you hate the Constitution?"
- "Every constitutional protection that matters as far as getting locked up for no good reason, being detained without any kind of due process, being denied life, liberty, property, all of these things, they all apply to persons, not citizens."

# Oneliner

Beau engages internet individuals on constitutional rights, revealing misconceptions and contradictions, urging critical reflection on the Constitution's importance.

# Audience

Online Activists

# On-the-ground actions from transcript

- Challenge misconceptions about constitutional rights (suggested)
- Encourage critical engagement with individuals who misunderstand constitutional protections (suggested)
- Spark debates on the significance of constitutional rights in policy debates (suggested)

# Whats missing in summary

The full transcript provides a detailed breakdown of constitutional rights and the importance of engaging with those who misunderstand or misrepresent them.

# Tags

#ConstitutionalRights #CriticalThinking #Engagement #PolicyDebates #InternetActivism