# Bits

Beau says:

- Explains the rarity and unique features of a Glock 18C, a fully automatic firearm with a rapid cyclic rate of 1200 rounds per minute.
- Describes the strict regulations that essentially made the Glock 18C non-existent for civilians in the United States.
- Mentions the illegal conversion kits that can turn a semi-automatic Glock into a fully automatic one, which are now being tracked by the ATF.
- Points out that prohibition often creates demand for restricted items, as seen with the Glock 18C conversion kits.
- Argues against prohibition of certain items like firearms, drugs, and abortion, stating that it only increases their desirability without a clear victim.

# Quotes

- "Prohibition doesn't work with guns. It doesn't work with drugs. It's not going to work with abortion."
- "Focusing on prohibiting items like this is always a losing proposition."
- "When you take something that is, in essence, a victimless crime, possessing this, there's no victim."

# Oneliner

Beau explains how prohibition fuels demand for restricted items like firearms, showing why it fails without victims.

# Audience

Policy Makers, Activists

# On-the-ground actions from transcript

- Monitor and advocate for sensible gun control policies (implied)
- Support community programs that address root causes of violence (implied)

# Whats missing in summary

Detailed examples and further explanations on the impact of prohibition on demand and availability of restricted items.

# Tags

#Firearms #Prohibition #GunControl #Demand #Regulations