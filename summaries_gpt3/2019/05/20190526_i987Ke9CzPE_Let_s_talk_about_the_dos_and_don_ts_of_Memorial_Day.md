# Bits

Beau says:

- A veteran asked Beau to provide a do's and don'ts of Memorial Day, but Beau initially felt he wasn't the right person for it.
- Veterans already familiar with Memorial Day's significance, but the general public may not be aware due to lack of discourse by politicians.
- Beau acknowledges the diversity of perspectives among veterans, contrary to a singular portrayal.
- Armed Forces Day, Veterans Day, and Memorial Day serve different purposes: active duty appreciation, all who served recognition, and honoring the fallen, respectively.
- Memorial Day is a solemn occasion to commemorate those who have died in service.
- Beau advises against saying "Happy Memorial Day" as it may not be appropriate given the solemnity of the occasion.
- Younger veterans from recent conflicts may still be dealing with emotional wounds, so sensitivity is key.
- Beau stresses the importance of not asking certain questions to veterans, particularly about combat experiences or killing others.
- Gold Star families, who have lost a family member in service, should be acknowledged with a simple "I'm sorry for your loss" without trying to relate their loss to a cause.
- Beau suggests allowing veterans to enjoy Memorial Day without feeling the need to relate to their experiences.
- Some veterans believe kneeling during the National Anthem on Memorial Day is a way to honor those unjustly killed by law enforcement.
- Beau encourages advocating against unjust wars and ensuring that active-duty personnel get celebrated on Veterans Day instead of being remembered on Memorial Day.

# Quotes

- "Memorial Day is a day to remember the fallen. It's a day to remember those lost. It's not a happy day. It is not a happy day."
- "Just leave it at that. I'm sorry for your loss."
- "Make sure that those who are active duty today get to celebrate Veterans Day and not get remembered on Memorial Day."
- "You've got a voice, use it."
- "We stop losing people, we don't need to remember."

# Oneliner

A guide on navigating Memorial Day sensitively, from recognizing diverse veteran perspectives to honoring the fallen and advocating against unjust wars.

# Audience

Community members

# On-the-ground actions from transcript

- Acknowledge Gold Star families with a simple "I'm sorry for your loss." (implied)
- Advocate against unjust wars and ensure active duty personnel are celebrated on Veterans Day. (implied)

# Whats missing in summary

The full transcript covers the nuances of Memorial Day observance, including the diverse perspectives of veterans and the importance of sensitivity in honoring the fallen.

# Tags

#MemorialDay #Veterans #HonorTheFallen #Respect #Advocacy