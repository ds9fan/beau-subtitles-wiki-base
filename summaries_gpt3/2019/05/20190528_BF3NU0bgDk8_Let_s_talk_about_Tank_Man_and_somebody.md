# Bits

Beau says:

- Describes a scenario where graduates are worried about job prospects due to a changing economy and negative media portrayal.
- Mentions attacks on freedom of press, assembly, and speech, with intellectuals being demonized.
- Portrays a protest that grows exponentially and faces logistical problems like food and hygiene issues.
- Describes government response with tanks, armored personnel carriers, and a quarter million troops.
- Points out the role of surrounding suburbs in nonviolently blocking the government from reaching the protesters.
- Notes infighting among movement leadership and missed opportunities for reform.
- Describes a government crackdown on protesters with tanks and rifles, resulting in casualties.
- Talks about the iconic Tank Man image from Tiananmen Square, symbolizing the power struggle between the state and the individual.
- Emphasizes the courage and defiance displayed by Tank Man in standing up to the tanks.
- Concludes by urging viewers to recognize their own potential as change-makers in the face of adversity.

# Quotes

- "If there's ever a metaphor for the power of the state versus the individual, it's that image."
- "A million people with popular support were all waiting for somebody. You are somebody."

# Oneliner

Beau describes a scenario resembling Tiananmen Square, urging viewers to recognize their potential as change-makers in the face of adversity.

# Audience

Young activists

# On-the-ground actions from transcript

- Support movements for freedom and democracy (exemplified)
- Stand up against government oppression (exemplified)
- Be prepared to take courageous action in the face of tyranny (exemplified)

# Whats missing in summary

The full transcript delves into the parallels between Tiananmen Square and current U.S. situations, encouraging political activism and courage in fighting against oppression.

# Tags

#Activism #Protest #Courage #Change #TiananmenSquare