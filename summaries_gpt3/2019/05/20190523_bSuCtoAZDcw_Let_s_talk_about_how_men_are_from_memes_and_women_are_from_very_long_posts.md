# Bits

Beau says:

- Speaking in generalities, discussing the impact of social media posts on real-life perceptions.
- Sharing personal experiences related to the Kavanaugh hearings and the abortion debate.
- Emphasizing the importance of being mindful of the messages conveyed through social media comments.
- Explaining how a seemingly harmless meme on abortion led to a relationship ending.
- Pointing out that sharing memes can limit one's thought process and influence others' opinions.
- Advising against using memes and encouraging open, direct discussions instead.
- Warning against the implications of sharing content without fully understanding its message.
- Stressing the significance of being aware of how one's online actions shape others' perceptions in real life.
- Encouraging deeper understanding and communication rather than relying on superficial social media posts.
- Concluding with a reminder to be thoughtful about the messages shared online.

# Quotes

- "Your social media posts are seen by people in your real life, and it shapes their opinion of you."
- "Stop using memes. It limits your thought."
- "She saw something. She understood something that you didn't get."
- "Stop letting other people define your thoughts like that because this is what happens."
- "It's an important lesson to learn."

# Oneliner

Beau sheds light on the influence of social media posts on real-life perceptions and warns against the limitations of sharing memes, advocating for meaningful communication over superficial interactions.

# Audience

Social media users

# On-the-ground actions from transcript

- Have open, direct discussions instead of relying on memes (suggested)
- Be mindful of the messages shared online and their potential impact on real-life perceptions (implied)

# Whats missing in summary

Detailed examples of how social media posts can influence personal relationships and perceptions. 

# Tags

#SocialMedia #Communication #Relationships #Perceptions #Memes