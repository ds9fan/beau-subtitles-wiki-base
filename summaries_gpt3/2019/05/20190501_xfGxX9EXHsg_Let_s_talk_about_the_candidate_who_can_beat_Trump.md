# Bits

Beau says:

- Questions the Democratic establishment's focus on which candidate can beat Trump rather than policies
- Criticizes the idea of running a candidate to appeal to Trump's voters, moving policies to the right
- Points out that supporting a candidate who appeals to bigots is unacceptable
- Encourages people who don't support Trump's policies to stay home rather than vote for them
- Urges the Democratic establishment to listen to younger progressives who prioritize moral values
- Notes that younger demographics tend to be more progressive, and running a pro-war candidate may lead them to stay home
- Observes that Trump didn't receive a majority of votes in any age demographic until 50 years old or older
- Warns against trying to steal votes from Trump by appealing to his supporters
- Emphasizes the importance of supporting a candidate with a platform that appeals to eligible voters who stayed home
- Calls on the Democratic establishment to represent the people and do their job

# Quotes

- "Don't co-sign evil because you don't like the other candidate."
- "The moral thing to do is stay home."
- "Represent the people of this country. In other words, do your job."

# Oneliner

The Democratic establishment should focus on policies, not stealing votes from Trump, and represent the people by supporting a platform that appeals to those who stayed home.

# Audience

Voters, Democratic establishment

# On-the-ground actions from transcript

- Support a candidate with a platform that appeals to eligible voters who stayed home (suggested)
- Encourage the Democratic establishment to prioritize policies over stealing votes from Trump (implied)
- Represent the people by supporting candidates that truly resonate with the electorate (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the pitfalls of focusing on beating Trump rather than policies and advocates for supporting candidates based on moral values and platforms that resonate with voters. 

# Tags

#DemocraticEstablishment #Trump #Policies #Progressives #Voters