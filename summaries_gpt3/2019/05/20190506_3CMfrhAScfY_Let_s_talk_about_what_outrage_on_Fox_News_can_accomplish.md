# Bits

Beau says:

- Addressing outrage and a comment on YouTube about discussing unpleasant happenings.
- Explaining terrorism as a PR campaign with violence to provoke outrage.
- Describing how outrage leads to justifying and condoning extreme actions like drone strikes and torture.
- Linking overreactions to an increase in terrorism and insurgency.
- Connecting foreign fighters in Iraq to the abuse of detainees, Guantanamo Bay, and Abu Ghraib.
- Noting that outlets like Fox News contribute to radicalizing Middle Easterners through outrage.
- Criticizing Fox News for using fear, bigotry, and ignorance to stoke outrage and ratings.
- Emphasizing that being outraged plays into the hands of terrorist organizations.
- Suggesting that Fox News inadvertently aids in the recruitment of terrorists.
- Advocating for discussing sensitive topics in a calm and clinical manner to avoid perpetuating outrage.

# Quotes

- "Being outraged is not good when you are talking about countering terrorism."
- "I don't want them to win."
- "The reason I talk about these things in a very clinical manner so as not to provoke outrage."
- "Fox News and outlets of their ilk have done more to help that small percentage of radicalized Middle Easterners than their own propaganda networks have."
- "The Islamic State, Al-Qaeda, these guys, they should have been cutting Fox News a check the entire time because they were their greatest recruitment tool."

# Oneliner

Beau addresses how outrage fuels terrorism, criticizing outlets like Fox News for inadvertently aiding radicalization by stoking fear and bigotry.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Fact-check news sources and avoid outlets that sensationalize and provoke outrage (implied).
- Support media outlets that report calmly and objectively on sensitive issues (implied).

# Whats missing in summary

Beau's passionate plea for media consumers to be critical of sources and avoid falling into the trap of outrage-driven narratives.

# Tags

#Outrage #Terrorism #MediaConsumption #FoxNews #Radicalization