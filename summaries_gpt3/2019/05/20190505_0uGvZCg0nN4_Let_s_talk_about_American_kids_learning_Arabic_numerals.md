# Bits

Beau says:

- Criticizes the US government for planning to teach American kids Arabic numerals.
- Mentions polls on Facebook showing 60% to 85% against learning Arabic numerals.
- Expresses agreement with the majority, questioning the need for Arabic numerals.
- Believes that the push for teaching Arabic numerals is part of a plot and mentions "Shakira Law."
- Points out that Arabic numerals are the numbers commonly used today.
- Condemns basing opinions on misinformation and lack of context.
- States that knowledge should not be looked down upon and advocates for continuous learning.
- Argues that disregarding knowledge simply because of its Arab origin is ridiculous.
- Emphasizes that education and knowledge are never wasted.
- Encourages Americans to be open to learning, especially when it comes to new topics or ideas.

# Quotes

- "The fact that we have become a nation where knowledge is looked down upon, where gaining knowledge of any kind is looked down upon, it's a bad sign."
- "Education is never wasted. Knowledge is never wasted."
- "So anytime this topic comes up, the answer should pretty much always be yes."
- "It is how we end up with a society where 60 to 85 percent of people answer a poll they know nothing about."
- "Y'all have a good night."

# Oneliner

Beau questions the resistance to teaching Arabic numerals, advocating for a society that values knowledge and continuous learning.

# Audience

Americans

# On-the-ground actions from transcript

- Educate yourself about the origins and significance of Arabic numerals (implied)
- Challenge misconceptions and misinformation about learning new things (implied)
- Embrace a culture of continuous learning and curiosity (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of the resistance towards teaching Arabic numerals and the importance of valuing knowledge regardless of its origin or association.

# Tags

#Education #Knowledge #Learning #CulturalAwareness #Community