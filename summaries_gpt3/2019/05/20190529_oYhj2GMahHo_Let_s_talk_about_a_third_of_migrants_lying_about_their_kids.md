# Bits

Beau says:

- Beau addresses a headline claiming that a third of migrants lied about their family status with their children, expressing concern and calling for an investigation.
- He points out that people are sharing the headline without critically examining it.
- Beau clarifies that the 30% mentioned in the headline was rounded up to a third, but the actual percentage was 30%.
- The sample used in the study was not representative of all migrants, but specifically those suspected by ICE of lying about family relationships.
- ICE's DNA testing method to verify family relationships does not account for step parents, legal guardians, or other complex family situations.
- He criticizes ICE for being wrong 70% of the time in cases where they suspected migrants of lying about family relationships.
- Beau questions the ethics of sharing a false narrative that demonizes migrants and perpetuates harmful stereotypes.
- He underscores the dangers faced by unaccompanied minors who end up in the custody of Health and Human Services, where reports of sexual abuse are prevalent.
- Beau concludes by debunking the manufactured story and criticizing the 145,000 people who shared it without verifying its accuracy.

# Quotes

- "If you wouldn't lie to stop a kid from getting raped, you're a pretty horrible person."
- "This is made up and 145,000 people shared one version of this stupid story."

# Oneliner

Beau debunks the false narrative that a third of migrants lied about their children, exposing flawed statistics and calling out the harmful impact of sharing misinformation online.

# Audience

Online activists

# On-the-ground actions from transcript
- Fact-check and verify information before sharing online (implied)
- Advocate for the protection of unaccompanied minors and migrant families (implied)
- Combat misinformation by educating others on how to critically analyze news stories (implied)

# What's missing in summary

The emotional impact of spreading false narratives and the importance of empathy and fact-checking in online discourse.

# Tags

#Immigration #FactChecking #ICE #MigrantRights #Misinformation