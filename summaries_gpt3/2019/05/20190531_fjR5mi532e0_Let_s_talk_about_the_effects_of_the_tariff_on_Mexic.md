# Bits

Beau says:

- Trump's tariffs on Mexico, 5% on everything, will increase the cost of fruits and veggies from Mexico, ultimately paid by the American consumer.
- Adrian Wiley, a Florida man, criticized Trump's tariffs in a unique way, shedding light on the impact on consumers.
- Despite the humor, the tariffs will lead restaurants and grocers to seek alternative sources for produce.
- The tariffs may inadvertently benefit American farmers, creating a competitive edge and increasing demand for their products.
- This increased demand will require more labor, potentially incentivizing illegal border crossings.
- Trump's belief that Mexico should prevent its citizens from leaving undermines the concept of asylum and individual freedom.
- Trump's push for a border wall contradicts his stance on freedom of movement, as walls work both ways.

# Quotes

- "You actually end up paying for this."
- "Congratulations President, you have just increased demand and incentive to cross the border illegally."
- "Walls work both ways y'all need to remember that."

# Oneliner

Trump's tariffs on Mexico will raise costs for American consumers, potentially benefiting farmers but also incentivizing illegal border crossings, undermining asylum claims, and revealing contradictions in border policies.

# Audience

American consumers and those concerned about border policies.

# On-the-ground actions from transcript

- Support local farmers by purchasing their produce (suggested)
- Advocate for fair trade policies that benefit consumers and farmers (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the impact of Trump's tariffs on Mexico, American consumers, farmers, and border policies.