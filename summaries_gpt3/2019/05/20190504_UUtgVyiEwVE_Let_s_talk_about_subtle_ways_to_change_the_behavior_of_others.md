# Bits

Beau says:

- Recounts an encounter with a friend in South Florida who expressed bigoted and racist views, causing a moment of realization about institutional racism.
- Addresses questions received about dealing with bigots in one's life, focusing on changing their actions rather than beliefs.
- Advises not to waste time trying to change older bigots, as influencing younger generations has a longer-lasting impact.
- Suggests exposing a racist boss to potential consequences like losing business due to public backlash.
- Warns about dealing with a partner who exhibits bigotry as a defense mechanism, urging careful consideration before addressing the issue.
- Encourages women to recognize the power they hold in combatting bigotry by making it socially unacceptable, thus changing behavior over time.
- Advocates for challenging racist jokes by pretending not to understand them, forcing individuals to confront the racism behind the humor and potentially leading to a reduction in such behavior.

# Quotes

- "If bigots had trouble getting dates, it [bigotry] would disappear."
- "Women do not understand the power they wield in this particular battle."
- "Y'all have a good night."

# Oneliner

Beau shares strategies to address bigotry, focusing on changing actions rather than beliefs, leveraging influence wisely, and recognizing the power individuals hold in shifting societal norms.

# Audience

Individuals combating bigotry

# On-the-ground actions from transcript

- Leave a newspaper article exposing racist behavior for a boss to see, potentially impacting their actions (suggested).
- Challenge racist jokes by pretending not to understand them, forcing individuals to confront their racism (implied).

# Whats missing in summary

Beau's full transcript provides nuanced insights and practical strategies for addressing bigotry in personal and professional settings.

# Tags

#Bigotry #SocialChange #Influence #CommunityPolicing #AntiRacism