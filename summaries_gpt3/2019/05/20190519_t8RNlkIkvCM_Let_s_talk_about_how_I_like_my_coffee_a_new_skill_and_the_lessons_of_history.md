# Bits

Beau says:

- Shares how he likes his coffee, drawing parallels with his preference for markets.
- Talks about living on the Alabama line and learning how to conduct abortions out of curiosity.
- Mentions the misconception around back alley abortions and how things have changed.
- Points out that 97% of abortions occur before 20 weeks, mostly done with a pill.
- Explains the multi-use nature of the abortion pill and its availability.
- Notes the advancement in technology making abortion procedures safer.
- Mentions the availability of tools for the procedure at hardware stores.
- Talks about the affordability of ultrasound machines and how they enhance safety.
- Suggests that the new law in Alabama might increase access to abortions and make them cheaper.
- Points out the lack of a paper trail or protesters when abortions are performed at home.
- Mentions how this law could undermine government authority and resemble the failure of prohibition.
- Concludes by stating that leadership should not give orders that won't be followed.

# Quotes

- "I like my coffee like I like my markets."
- "It's almost like prohibition doesn't work."
- "Never give an order that won't be followed."

# Oneliner

Beau talks about his coffee preferences, learning about conducting abortions, and how a new law in Alabama might actually increase access and affordability while undermining government authority.

# Audience

Community members

# On-the-ground actions from transcript

- Contact local healthcare providers for information on abortion services (suggested)
- Research affordable and safe abortion options in your area (implied)

# Whats missing in summary

Beau's detailed explanations and examples are missing in this summary.

# Tags

#Abortion #Alabama #Access #Technology #Government