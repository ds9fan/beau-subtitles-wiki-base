# Bits

Beau says:

- Hurricane Michael relief efforts revisited due to lack of Congressional action after more than 200 days.
- Republicans trying to shift blame onto Democrats for the lack of progress in assisting Northwest Florida.
- Despite the hurricane occurring before a change in control, no significant aid has been provided.
- Previous hurricanes like Katrina and Andrew received Congressional action within days to weeks.
- Republicans are hesitant to allocate funds to Puerto Rico, seen as part of the U.S., leading to accusations of bigotry.
- Florida contributes more to the federal government than it receives but struggles to get assistance.
- The delay in aid extends to other affected regions like California wildfires and Florence victims.
- Criticism towards Republicans for withholding aid from Spanish-speaking brown communities.
- Reminder to voters to take note of the neglect during the next election, especially in states like Florida.
- Call for accountability and efficient allocation of resources in providing necessary assistance.

# Quotes

- "And now, here we are more than 200 days later and we got nothing."
- "You guys might want to get your act together."
- "Y'all might be making a mistake here."
- "There's a whole bunch of brown people that speak Spanish in Florida, Tambien."
- "You guys might want to get your act together."

# Oneliner

Hurricane Michael relief efforts face significant delays, especially in aiding Puerto Rico and brown Spanish-speaking communities, showcasing political negligence and a call for accountability in resource allocation.

# Audience

Voters, Community Members

# On-the-ground actions from transcript

- Vote in elections to hold accountable those responsible for delayed aid (implied).

# Whats missing in summary

The emotional impact of neglected communities waiting for aid and the urgency for immediate action to rectify the situation.

# Tags

#HurricaneMichael #CongressionalInaction #AidDelay #PoliticalNegligence #CommunitySupport