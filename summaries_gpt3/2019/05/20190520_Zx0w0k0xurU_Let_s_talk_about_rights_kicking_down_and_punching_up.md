# Bits

Beau says:

- Reacts to a message questioning his support for abortion due to having six kids.
- Expresses surprise and introspection at the hypocrisy pointed out by the message.
- Lists various causes he supports, despite not personally embodying all of them.
- Urges people to care about the rights of others beyond those that directly impact them.
- Warns against falling into the trap of division created by propaganda and those in power.
- Criticizes the manipulation of people's rights in the name of democracy.
- Acknowledges living by far-right ideals in his personal life but opposes forcing them on others.
- Advocates for freedom by not using force to restrict others based on personal beliefs.
- Condemns the idea of government having ultimate control over individuals' lives as oppressive.
- Calls for a shift in mindset to stop fighting amongst each other and focus on real freedom.

# Quotes

- "Stop caring about things that impact you directly and only those things."
- "You vote each other's rights away."
- "Stop kicking down. Only punch up."

# Oneliner

Beau reacts to a message questioning his support for abortion, urging people to care about the rights of others beyond themselves and warning against the manipulation of division for power.

# Audience

Activists, Advocates, Citizens

# On-the-ground actions from transcript

- Challenge yourself to care about causes that may not directly affect you (exemplified)
- Advocate for the rights of marginalized groups in your community (exemplified)
- Refrain from imposing personal beliefs on others through force (exemplified)

# Whats missing in summary

The full transcript includes Beau's reflections on societal issues, the manipulation of division for power, and advocating for true freedom beyond personal beliefs.

# Tags

#Abortion #Activism #Rights #Freedom #Division