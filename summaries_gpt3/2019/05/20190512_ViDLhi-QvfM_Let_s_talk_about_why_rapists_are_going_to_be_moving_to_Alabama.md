# Bits

Beau says:

- Alabama's legislature is compared to the Taliban due to a bill introduced by Dicky Drake to address false accusations of sex crimes.
- Filing a false report of a sex crime is already a crime in Alabama, but the penalties may be increased to 10 years in prison for the accuser if the accusation is proven false.
- The bill could lead to victims of rape being imprisoned if their accusations are proven false, even though studies show that false accusations of rape are in the single digits.
- Beau questions the flawed criminal justice system in Alabama and suggests that putting rape victims on trial is not the solution.
- The bill may create an environment where rapists flock to Alabama, as there is a risk of innocent victims being imprisoned based on false accusations.
- Beau expresses disbelief at the flawed logic behind the bill and its potential consequences on rape victims and the justice system in Alabama.

# Quotes

- "A careful rapist who doesn't leave physical evidence always walk and their victim will go to prison because Dickie Drake's friend, his ex-wife, said something."
- "So he gets to rape her and then put her in prison?"
- "You're saying the criminal justice system is broke, but you're just making the assumption that it's only broke one way."

# Oneliner

Beau questions Alabama's flawed legislation that could lead to innocent victims of rape being imprisoned based on false accusations.

# Audience

Legislators, advocates, activists

# On-the-ground actions from transcript

- Advocate for comprehensive justice system reform in Alabama (implied)
- Support organizations working to protect victims of sexual assault and improve reporting mechanisms (implied)

# Whats missing in summary

The emotional impact and urgency of addressing the dangerous implications of Alabama's proposed legislation.

# Tags

#Alabama #Legislation #SexCrimes #CriminalJustice #Victims #Advocacy