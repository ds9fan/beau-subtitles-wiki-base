# Bits

Beau says:

- Addressing the issue of pardoning war criminals after returning from a break on Facebook.
- Responding to messages about opinions on specific cases involving contractors.
- Describing a situation where an Army lieutenant interrogated a suspected bomb maker and ended up killing him in a physical altercation.
- Mentioning the ambiguity surrounding evidence in the case and the lieutenant's intention.
- Pointing out that incidents like these, although not right, do happen.
- Explaining that the lieutenant had already served his time in prison before being pardoned.
- Expressing understanding towards the lieutenant's emotional reaction leading to the death.
- Moving on to discussing potential pardons for a Navy chief accused of heinous acts in villages.
- Emphasizing the significance of SEALs turning in the accused Navy chief as a grave indicator.
- Expressing disbelief in the Navy chief's deservingness of a pardon due to his actions aiding the insurgency.
- Mentioning another case involving an Army Major killing a suspected bomb maker, casting doubt on the need for a pardon.
- Touching on the case of Marines urinating on opposition KIAs, condemning their actions but suggesting that they have already faced consequences.
- Acknowledging the impact of the Marines' actions in aiding insurgency propaganda.
- Stating that criminal justice consequences may not match the burden of living with the outcomes of their actions.
- Concluding with strong disapproval of a contractor behaving unprofessionally by opening fire on crowds.
- Arguing that the contractor should have faced justice under Iraqi jurisdiction rather than being brought back to the US.
- Asserting that such behavior is unacceptable in a professional setting.

# Quotes

- "Memorial Day is not Veterans Day. Memorial Day is for those who were killed."
- "If you want to behave like that, there's a whole bunch of places, whole bunch of locations, whole bunch of firms that you can go to and you can work like that and you'll end up at the end of the machete just like everybody else."
- "As a contractor, you are supposed to be a professional, not open fire on crowds, ever, period, full stop, end of story."

# Oneliner

Beau addresses specific cases of war criminals, expressing doubts about pardons and advocating for professional conduct, stressing Memorial Day's significance for the fallen.

# Audience

War critics and advocates

# On-the-ground actions from transcript

- Contact Iraqi authorities to address the contractor's actions under their jurisdiction (suggested)
- Educate on the distinctions between Memorial Day and Veterans Day to honor the fallen appropriately (exemplified)
- Share Beau's message to raise awareness about the implications of pardoning war criminals (implied)

# Whats missing in summary

Deeper insights on the implications of pardoning war criminals and the importance of professional conduct in military and contractor roles.

# Tags

#WarCriminals #Pardons #MilitaryJustice #ProfessionalConduct #MemorialDay