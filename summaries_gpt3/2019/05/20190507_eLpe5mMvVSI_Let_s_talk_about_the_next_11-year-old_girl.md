# Bits

Beau says:

- An 11-year-old girl in Ohio, allegedly raped by a 26-year-old, is pregnant after the state removed her choice through legislation.
- The focus on regulating issues like abortion rather than testing rape kits contributes to societal problems.
- The 11-year-old girl is at a critical developmental stage, facing emotional turmoil, peer pressure, and self-consciousness.
- She may struggle with eating disorders, substance abuse, and self-harm during this period.
- Laws in Ohio may prevent the perpetrator from getting parental rights if convicted of rape or sexual battery.
- Poorly written laws aiming to restrict abortion may lead to unsafe practices and disproportionately affect economically disadvantaged individuals.
- Beau challenges the notion that a six-week-old embryo is equivalent to a living child by presenting a stark moral dilemma.
- He underscores the complex shades of gray in the world that an 11-year-old is beginning to grasp, contrasting with black-and-white legislation.
- Beau questions the heavy burden placed on an 11-year-old in such circumstances.

# Quotes

- "That's the society we live in, in part because the legislatures of this country are more interested in regulating stuff like this than they are making sure that rape kits get tested."
- "It's just a thought, y'all have a good night."

# Oneliner

An 11-year-old girl in Ohio, pregnant after allegedly being raped, faces the consequences of legislation removing her choice, while societal focus shifts from critical issues like testing rape kits.

# Audience

Legislators, Activists, Advocates

# On-the-ground actions from transcript

- Advocate for comprehensive sex education in schools to empower young individuals (suggested)
- Support organizations providing resources for survivors of sexual violence (exemplified)

# Whats missing in summary

The emotional impact of legislation on young individuals and the societal implications beyond the immediate case.

# Tags

#Ohio #AbortionLegislation #SexualViolence #RapeSurvivors #YouthEmpowerment