# Bits

Beau says:

- Addresses choice, life, and statistics related to abortion, debunking false information dominating headlines.
- Mentions the religious issue surrounding abortion and references a passage in Numbers from the Bible.
- Talks about the misconception around heartbeats in embryos and the size of embryos at six weeks.
- Emphasizes the statistic that 97% of abortions occur before 20 weeks.
- Argues against viewing abortion as a consequence, stating it's more of a punishment due to legislative restrictions.
- Challenges the idea that consent for sex equals consent to carry a child.
- Counters the belief that women use abortion as birth control, presenting statistics on abortion frequency.
- Addresses the misconception that tax dollars fund abortions, clarifying the role of the Hyde Amendment.
- Links abortion rates to poverty and criticizes the lack of focus on real societal issues.
- Centers the abortion debate on bodily autonomy and challenges the notion of men's rights over women's bodies.

# Quotes

- "97%, remember that number."
- "Consent for sex is not consent to carry a child."
- "Poverty is a lack of cash. It's not a lack of character."
- "Bodily autonomy, it is not your body."
- "When does life exist and the rights of that new life trump the rights of the woman?"

# Oneliner

Beau debunks myths, presents statistics, and challenges the notion of bodily autonomy in the abortion debate, urging society to prioritize ethics over morals or religion.

# Audience

Advocates for reproductive rights

# On-the-ground actions from transcript

- Advocate for comprehensive sex education in states with limited education on the topic (implied).
- Support organizations that provide resources for women facing unplanned pregnancies (implied).
- Challenge misconceptions and stigma surrounding abortion through education and open dialogues (implied).

# Whats missing in summary

In-depth exploration of the societal implications and ethical considerations in the abortion debate.

# Tags

#Abortion #ReproductiveRights #BodilyAutonomy #MythsDebunked #Ethics