# Bits

Beau says:

- Recounts a story from when he was 20 about a friend's ex showing up with four kids, only one of whom the friend knew, and leaving them with no food or supplies.
- Stayed with the kids while his friend went to get baby formula, learning that a hungry baby won't stop crying no matter how much you try to soothe them.
- Criticizes the Trump administration for redefining poverty without actually improving people's situations through the Chained CPI method.
- Explains how Chained CPI manipulates statistics to make it seem like people are lifted out of poverty when in reality they lose benefits and struggle.
- Points out that with the current economy, even a slight increase in income can disqualify individuals from receiving necessary benefits.
- Shares statistics on the financial struggles of Americans, such as the inability to find good-paying jobs, pay credit card balances, or afford healthcare.
- Notes that while the economy may seem great for some, the majority of Americans are living paycheck to paycheck and cannot handle emergencies.
- Expresses the importance of not just focusing on the top earners but addressing the needs of the bottom percentile who significantly impact society.
- Suggests that cutting benefit programs leads to an increase in crime as people resort to illegal activities to survive.
- Advocates for real solutions to address the financial hardships faced by a large portion of the population instead of manipulating data for political gain.

# Quotes

- "Babies don't stop crying when they're hungry. What are they going to do?"
- "These benefit programs, entitlement programs, as people sometimes like to call them, that's just crime insurance at this point because if you get rid of them, you're going to see an uptick in crime."
- "Maybe it would be better if we didn't cook the books and we actually took some steps to try to fix the fact that 80% of Americans are living paycheck to paycheck."

# Oneliner

Beau shares a personal story, criticizes the manipulation of poverty statistics, and advocates for real solutions to address financial struggles faced by Americans.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Advocate for policies that genuinely address poverty and financial hardships (implied)

# Whats missing in summary

The emotional impact of witnessing a hungry baby crying and the urgency to address the financial struggles of a majority of Americans.

# Tags

#Poverty #Economy #FinancialStruggles #ChainedCPI #CommunityConcerns