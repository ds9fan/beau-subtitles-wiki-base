# Bits

Beau says:

- China has a history of issuing a catchphrase, "don't say we didn't warn you," that has led to shooting wars.
- The catchphrase has been recently issued by China to the United States, but it's likely related to the trade war rather than escalating to an actual war.
- China is considering cutting off or reducing exports of rare earth minerals in response to Trump's trade war.
- Rare earth minerals are vital for high-tech goods and China controls about 85% of the market.
- The trade deficit is not inherently bad, but Trump is using tariffs as a solution, which ultimately affects American consumers.
- Trump's focus on keeping campaign promises may lead to ineffective solutions that impact the economy negatively.
- The trade war won't bring back American manufacturing jobs but might shift them to other countries like Vietnam.
- The decision to move jobs abroad is driven by economic factors and the cost benefits of operating in countries with less stringent environmental regulations.
- Mining rare earth minerals has severe environmental impacts, as seen in China where they are currently mined.
- Trump's decisions regarding the trade war will have lasting negative impacts on the economy and future presidents will likely face the consequences.

# Quotes

- "China has issued a catchphrase that has led to shooting wars throughout history."
- "The decisions that Trump is making are going to have long, long lasting and far reaching impacts in the economy, and they're not good ones."
- "We're all gonna end up paying for this."

# Oneliner

China issues a warning to the US amidst trade tensions, signaling potential economic impacts and environmental concerns, with long-lasting repercussions.

# Audience

Global citizens, policymakers

# On-the-ground actions from transcript

- Research the environmental impacts of mining rare earth minerals (suggested)
- Advocate for environmentally responsible practices in mining (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of the US-China trade war and the environmental impacts of mining rare earth minerals.

# Tags

#USChinaTradeWar #RareEarthMinerals #EconomicImpact #EnvironmentalConcerns #GlobalTrade