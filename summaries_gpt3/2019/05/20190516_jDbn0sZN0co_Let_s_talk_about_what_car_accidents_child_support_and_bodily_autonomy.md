# Bits

Beau says:

- Driving and texting led to a four car pile up, resulting in an old man on life support.
- Beau suggested to the judge that the old man be taken off life support, but the family disagreed.
- Beau believes he is not obligated to pay for the medical bills in this situation.
- He draws a parallel to the argument of not wanting to pay child support if one doesn't have a say in the decision to keep the baby.
- Beau points out the lack of understanding of bodily autonomy in the country.
- He stresses that bodily autonomy is fundamental and simple—it's her body.
- Beau leaves with the message that it's just a thought and wishes everyone a good night.

# Quotes

- "It's her body. It's really that simple."
- "Guys this is what it sounds like when you say, well if I don't have a say in whether or not she keeps the baby, then I shouldn't have to pay child support."

# Oneliner

Beau draws parallels between bodily autonomy and obligations, stressing the simplicity of respecting individual choices.

# Audience

Advocates for bodily autonomy

# On-the-ground actions from transcript

- Respect and support bodily autonomy (implied)

# Whats missing in summary

The emotional impact and tone of Beau's message can be best experienced by watching the full video. 

# Tags

#BodilyAutonomy #Respect #Choice #Responsibility #Advocacy