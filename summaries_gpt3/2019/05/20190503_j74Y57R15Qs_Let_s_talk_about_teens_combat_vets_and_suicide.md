# Bits

Beau says:

- May's Mental Health Awareness Month.
- Beau addresses the heavy topic of suicide.
- Beau shares shocking statistics on suicide attempts by young girls.
- Suicide among teens is a significant issue.
- The rise in successful suicides is alarming.
- Beau provides the National Suicide Hotline number.
- Suicide is a prevalent issue among teens.
- The lack of national focus on suicide is questioned.
- Beau criticizes the lack of attention to suicide compared to school shootings.
- The impact of popular media like "13 Reasons Why" on suicide is discussed.
- Suicide is often a result of multiple factors, not just one reason.
- Beau talks about the struggles teens face in today's society.
- The importance of valuing and supporting youth is emphasized.

# Quotes

- "As Americans, we want to know, well, what's the reason this is happening? It's not a reason."
- "Life is never without the means to dismiss itself."
- "We have a society that is creating a suicide epidemic among teens."
- "There's not going to be an easy fix to this one, guys."
- "It's just a thought."

# Oneliner

May's Mental Health Awareness Month: Beau shares shocking stats on teen suicide, questioning the lack of national focus and urging to value youth.

# Audience

Parents, educators, policymakers

# On-the-ground actions from transcript

- Call the National Suicide Hotline at 1-800-273-8255 or text 741-741 for immediate help (exemplified)
- Take interest in the well-being of teens contemplating suicide; it can make a life-saving difference (exemplified)

# Whats missing in summary

Full context and emotional depth from Beau's message.

# Tags

#MentalHealth #SuicidePrevention #TeenSuicide #YouthSupport #CommunityAction