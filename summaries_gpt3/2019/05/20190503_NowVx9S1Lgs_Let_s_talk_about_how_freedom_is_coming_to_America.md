# Bits

Beau says:

- Describes a world with a higher level of freedom.
- Mentions various instances where this freedom is emerging.
- Talks about societal change and resistance against injustice.
- Points out different acts of solidarity and unity.
- Emphasizes on the importance of standing up for what's right, even if it doesn't directly affect individuals.
- Encourages the building of parallel power structures.
- Raises the question of individual participation in the ongoing fight for change.
- Addresses the global impact of positive ideas spreading.
- Concludes by wishing everyone a good night.

# Quotes

- "It's coming from the people who are standing in the streets facing off against MRAPs and tear gas over yet another unjust killing."
- "It's coming from anybody who stands up for what's right, even though it doesn't directly impact them."
- "Good ideas don't really require force."
- "It's coming from those people within the existing power structures who are attempting to dismantle them."
- "First we take DC, then we'll worry about Beijing."

# Oneliner

A call to action for freedom through societal change, solidarity, and standing up for what's right, regardless of direct impact.

# Audience

Activists, Community Members

# On-the-ground actions from transcript

- Stand up against injustice by joining protests and demonstrations (exemplified).
- Engage in acts of solidarity and unity with marginalized communities (suggested).
- Contribute to building parallel power structures within communities (exemplified).
- Educate oneself and others about ongoing social issues (implied).

# Whats missing in summary

The full transcript provides a detailed and inspiring look at how individuals are contributing to a more free and just society through various acts of resistance, solidarity, and unity.

# Tags

#Freedom #SocietalChange #Solidarity #Activism #CommunityBuilding