# Bits

Beau says:

- Twitter decided to ban political ads on its platform, signaling a shift towards valuing ideas over money as speech.
- The move is seen as a step in the right direction, contrasting with Facebook's approach of exempting political ads from fact-checking.
- Twitter aims for ideas to gain reach organically through user interaction rather than through paid promotion.
- Paid advertising can make false information seem true through repetition, shaping beliefs regardless of facts.
- Beau warns of potential loopholes where individuals with platforms may be influenced by politicians or supporters offering money to push certain agendas.
- While some may argue this move limits free speech, Beau believes it actually empowers individuals to control the spread of ideas based on their interactions.
- Supporting this change could potentially reduce the influence of campaign contributions and make politicians more accountable to the people.
- Beau suggests following candidates and commentators on Twitter to ensure their ideas appear in your feed organically.
- By promoting the worth of ideas over financial backing, social media platforms could diminish the impact of campaign contributions on elections.

# Quotes

- "Twitter has decided to ban political ads across its platform. That's cool."
- "Ideas should stand and fall on their own."
- "They can't use repetition as a weapon."
- "I think it is going to help return a little bit of power to the average person if they use it."
- "If those campaign contributions can't buy the platforms that swing elections, they're not that important."

# Oneliner

Twitter's ban on political ads shifts focus from money to ideas, empowering users to control the spread of information and potentially reducing the influence of campaign contributions.

# Audience

Social media users

# On-the-ground actions from transcript

- Follow candidates and commentators on Twitter to support their organic reach (implied).

# Whats missing in summary

The full transcript provides a deeper insight into the potential impact of Twitter's ban on political ads and the importance of individuals actively engaging with content to shape the online narrative.

# Tags

#Twitter #PoliticalAds #SocialMedia #CampaignContributions #Empowerment