# Bits

Carrie Wedler says:

- Started activism due to disillusionment with politics after supporting Obama in 2008 and realizing the continuation of wars.
- Believes in evolution over revolution, advocating for agorism and peaceful protest against systemic injustices.
- Trauma from her parents' divorce shaped her emotional responses, leading her to pursue healing and introspection.
- Recommends yoga and meditation for maintaining calm amidst chaotic news cycles.
- Emphasizes the importance of addressing deeper emotional wounds to prevent reactive responses.
- Advocates for skepticism towards all media sources, both mainstream and independent, promoting critical thinking.
- Plans to continue making videos challenging political authority and advocating for emotional exploration.
- Suggests planting trees as a simple yet impactful solution to mitigate carbon dioxide and contribute positively to the environment.
- Encourages curiosity and mindfulness towards emotional reactions as a pathway to healing and self-awareness.

# Quotes

- "My solution is not violence in the streets and taking down the government with guns as much as I support gun rights."
- "I think this is a really sweet one, and I think it's so symbolic of new beginnings."
- "Just start to become curious about what comes up for you."
- "I'm banned from Twitter, but my ha ha ha, it's so funny that I'm banned from Twitter. It's hilarious."

# Oneliner

Carrie Wedler advocates for agorism over violent revolution, prioritizing emotional healing, mindfulness, and curiosity to navigate turbulent times and advocate for positive change.

# Audience

Activists, Community Members

# On-the-ground actions from transcript

- Plant trees or support organizations that focus on tree planting to mitigate carbon dioxide (implied)
- Incorporate yoga and meditation practices into daily routines for emotional well-being (implied)
- Cultivate curiosity and mindfulness towards emotional responses as a pathway to healing (implied)

# Whats missing in summary

The full transcript captures Carrie Wedler's journey from political disillusionment to activism, advocating for agorism, mindfulness, and emotional healing as paths to personal growth and positive change.

# Tags

#Activism #Agorism #Mindfulness #EmotionalHealing #TreePlanting #CommunityEngagement