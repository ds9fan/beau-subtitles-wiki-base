# Bits

Beau says:

- Wishes a happy birthday to a young viewer before discussing UBI and Andrew Yang. 
- Mentions an 18-month study in Stockton, California, where 125 people received $500 a month, a boost for Yang's $1000 plan.
- Researchers from the University of Tennessee and University of Pennsylvania label the study as anecdotal, focusing more on storytelling than gathering hard data due to the limited sample size.
- Study aims to measure impact on mental and physical health, with results pending release.
- Breakdown of recipients: 43% working, 2% unemployed, 8% retired, 20% disabled, and 10% stay-at-home caregivers.
- Spending breakdown: 40% on food, 24% at stores like Walmart, Dollar General, 11% on utilities, 9% on auto repairs and gas, and 16% on medical expenses and insurance.
- Beau acknowledges the limitations of the sample size but appreciates the storytelling aspect supporting Yang's plan.
- Emphasizes the importance of younger generations like Lily in implementing such ideas that could revolutionize society.
- Acknowledges concerns about people's spending habits and stereotypes related to poverty.
- Concludes by hinting at the transformative potential of large-scale UBI implementation and the significance of public opinion, particularly from younger individuals like Lily.

# Quotes

- "Being in poverty is a lack of cash, not a lack of character."
- "They're the ones that are going to implement it, or not. They're the ones that will live with it."
- "Revolutionary ideas are coming out; this is an old idea. But implementing it on a scale that Yang is talking about, that's a pretty big undertaking."
- "People have said it before, being in poverty is a lack of cash, not a lack of character."
- "Matters what Lily thinks."

# Oneliner

Beau talks UBI, referencing a Stockton study on $500 monthly payments to 125 individuals, hinting at the transformative potential of large-scale implementation and importance of public opinion, particularly from younger generations like Lily.

# Audience

Policy advocates, UBI supporters

# On-the-ground actions from transcript

- Advocate for UBI implementation in your community (implied)
- Support research on UBI impacts on mental and physical health (implied)
- Challenge stereotypes about poverty and spending habits (implied)

# Whats missing in summary

The full transcript provides detailed insights into a small-scale UBI study's findings and implications, urging viewers to contemplate the transformative potential and societal impact of large-scale implementation. 

# Tags

#UBI #AndrewYang #Poverty #Society #Community