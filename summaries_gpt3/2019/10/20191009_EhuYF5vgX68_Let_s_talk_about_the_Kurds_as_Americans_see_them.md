# Bits

Beau says:

- Received a screenshot of a tweet referencing his bias in favor of the Kurds due to wearing a Kurdish scarf in a video about them.
- Explains that he tries to remain objective but acknowledges his bias.
- Disagrees with starting debates with other commentators, believing they reinforce existing ideas rather than lead to productive discourse.
- Expresses understanding and frustration over a tweet questioning the sudden interest in the Kurds by those who previously showed no concern for them.
- Provides historical context on the Kurds, dating back to the 7th century, their struggles with colonial powers, and the lack of recognition of their independence.
- Points out the unique aspect of Kurdish identity, where nationality comes before religion, unlike in many other Middle Eastern countries.
- Addresses common questions about the Kurds, including their religious diversity and presence across multiple countries like Syria, Turkey, Iraq, Iran, and Armenia.
- Talks about the geopolitical significance of the Kurds, their desire for independence, and how they are viewed by Western powers.
- Criticizes the notion that Turkey is strategically vital to the US, questioning the importance of air bases and troop numbers in NATO.
- Emphasizes the true strategic value lies in assets on the ground, like human intelligence and local support, which the Kurds provide.
- Expresses concern over the US's treatment of the Kurds and how it may push them towards other superpowers like Russia.
- Condemns the lack of action in granting the Kurds a homeland and promoting peace in the region.
- Advocates for recognizing the agency of Kurdish people in determining their future rather than oppressing them through other nations.
- Criticizes the American public's shallow knowledge of the Kurds, reduced to memes about their governance and attractive women warriors.
- Calls for more substantial support for the Kurds beyond social media sharing, considering the significant population without a homeland.

# Quotes

- "I think we can muster a little more support than just sharing a meme on Facebook."
- "There's 30 million people out there without a country."
- "We'd rather string them along in hopes of it, rather than actually do something that could promote peace."
- "I think this time we think beyond memes and realize there's 30 million people out there without a country."
- "I don't think that arguing with other commentators does a lot of good."

# Oneliner

Beau provides historical context on the Kurds, criticizes shallow discourse, and calls for meaningful support beyond memes.

# Audience

Advocates and Activists

# On-the-ground actions from transcript

- Educate yourself on the history and struggles of the Kurdish people (suggested).
- Support organizations advocating for Kurdish rights and independence (implied).
- Raise awareness about the plight of the Kurds in your community (implied).

# Whats missing in summary

The emotional depth and personal connection Beau brings to the discourse can be better understood by watching the full transcript.

# Tags

#Kurds #Geopolitics #Support #Advocacy #Community #History #Oppression #Activism