# Bits

Beau says:

- Dark and stormy nights are when real monsters, meaning other men, roam the earth to harm people.
- The conditions during dark and stormy nights, like rain, limited visibility, and dulled senses, make it easier for predators to attack.
- Groups of indoctrinated men are ingrained with the idea of never quitting and always accomplishing the mission.
- After Trump leaves office, the real work begins for the community to build the society they want.
- The next president won't have to do much to be better than Trump, so it's up to the community to push, guide, and lead them.
- It's not just about resistance; it's about building helpful ideas and creating power structures for a better society.

# Quotes

- "Dark and stormy nights is when it happens, even today."
- "We're waiting for our dark and stormy night to do it."
- "It's not just resistance, it's not destroying the ideas that are harmful, it's building the ideas that are helpful."
- "It's up to us."

# Oneliner

Dark and stormy nights make it easier for predators to harm, but after Trump, the community must build a better society by guiding and pushing the next leaders.

# Audience

Community members

# On-the-ground actions from transcript

- Guide, push, and lead the next president to build a better society (implied)
- Create local power structures to influence change (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the importance of community action in shaping a better society post-Trump.

# Tags

#CommunityAction #BuildingSociety #GuidingLeadership #DarkAndStormyNights