# Bits

Beau says:

- Toy soldiers lined up, game pieces on the board, watching the live play-out from a safe distance.
- Two groups of people ready to face off, at the mercy of their superiors, playing "Masters of the Universe."
- Turkish troops preparing to cross the border, Kurds needing more troops as theirs are occupied guarding suspected IS fighters.
- Guards from facilities likely to join the front, increasing the risk of jailbreaks.
- President's claim of defeating a side contradicted; potential for U.S. intervention.
- Slogans and campaign points overshadowing the disastrous consequences of decisions.
- Expansion of war and fresh combatants, not the end of imperialism.
- Decision's immediate fallout overlooked, resulting in lethal consequences overseas.
- Bad moves in one place lead to mass graves in another.
- The reality of waste amidst slogans and talking points.

# Quotes

- "Ended US imperialism because when Turkey and the US got together and decided how best to carve up a chunk of a third country, that was anti-imperialism."
- "What's a bad move over here is a mass grave over there."
- "The only thing that's happening over there right now is waste."

# Oneliner

Toy soldiers line up, slogans overshadow lethal consequences, waste prevails - a bad move here means mass graves there.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Contact organizations providing aid to affected regions (implied)
- Support initiatives offering assistance to refugees and displaced individuals (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the consequences of political decisions on the ground, urging viewers to look beyond slogans and talking points to understand the real impact of actions.

# Tags

#ForeignPolicy #Imperialism #Consequences #PoliticalDecisions #Slogans