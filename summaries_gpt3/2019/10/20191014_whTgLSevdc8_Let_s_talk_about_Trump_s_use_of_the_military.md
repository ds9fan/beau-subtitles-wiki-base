# Bits

Beau says:

- Sitting with a recently separated veteran who was upset after watching the President of the United States brag about accepting payment to deploy US military forces overseas.
- Criticizing the notion that recent actions were about bringing troops home, pointing out that it was more about anti-imperialism and enabling other countries' ambitions.
- Expressing concern that the US military is now seen as a mercenary force for Saudi Arabia due to financial agreements.
- Questioning the morality of deploying US military forces overseas as an elective option that can be bought, rather than a duty required for national security and freedom.
- Mentioning the uncertainty surrounding Trump's statements and the potential impact on how the US military is perceived in the Middle East.
- Indicating that US soldiers are now viewed as mercenaries being bought and sold, serving as bait for conflicts in the Middle East.
- Noting the historical perspective that war has always been intertwined with money and power, with soldiers essentially being mercenaries.
- Quoting General Butler, a highly decorated Marine, who referred to himself as a "gangster for capitalism" and acknowledged serving as muscle for big business interests.
- Describing how the Department of Defense has effectively become a private military contractor, sending troops overseas based on financial incentives rather than national interests.
- Suggesting that individuals considering joining the military should be aware that private firms may offer better pay and more competent leadership.

# Quotes

- "Deploying US military forces overseas should not be elective. It should be the only alternative."
- "The might of the U.S. military, well, it's on the market. It can be bought."
- "War as a racket always has been. It is possibly the oldest, easily the most profitable, surely the most vicious."
- "US servicemen have been bought and sold for a very, very long time. It's just normally not done this cheaply."
- "The Department defense has become the world's largest private military contractor."

# Oneliner

Beau criticizes the commodification of US military forces, exposing how they are being viewed as mercenaries for hire rather than defenders of national security.

# Audience

Veterans and anti-war advocates.

# On-the-ground actions from transcript

- Contact organizations supporting veterans' rights and well-being (implied).
- Educate yourself on the impacts of militarization in foreign policy (generated).
- Advocate for policies that prioritize national security over financial incentives (implied).

# Whats missing in summary

The full transcript provides a deep dive into the financial motivations behind US military deployments and challenges the narrative of military interventions solely for national security purposes.

# Tags

#USMilitary #WarProfiteering #AntiImperialism #Veterans #NationalSecurity