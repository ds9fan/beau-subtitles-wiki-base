# Bits

Beau says:

- AOC and the Department of Defense released dueling reports on climate change.
- AOC's report predicts rising sea levels displacing 600 million people, infectious diseases spreading, decreased food security, extreme weather incidents, and stress on the power grid.
- The Department of Defense report, with input from NASA and the Defense Intelligence Agency, describes climate change as a huge threat to be dealt with immediately.
- The U.S. Army is considering a culture change to reduce their carbon footprint and create fresh water from humidity for warfighters.
- Beau fabricated AOC's report to draw attention to the seriousness of climate change and the need for action.
- The Department of Defense, NASA, and environmental scientists all warn about the real threat of climate change.
- Beau contrasts listening to experts versus dismissing climate change as fake news promoted by oil companies.

# Quotes

- "AOC is right. The environmentalists are right and the Department of Defense is co-signing that."
- "This is a real threat. The people who are paid to decide what threats are, they say it is."
- "It's your choice. You can listen to the Department of Defense, NASA, the Defense Intelligence Agency, environmental scientists."

# Oneliner

AOC and the Department of Defense present contrasting reports on climate change, with experts warning of imminent threats and the urgent need for action.

# Audience

Climate activists, policymakers

# On-the-ground actions from transcript

- Contact local policymakers to advocate for climate action (implied)
- Join environmental organizations working towards climate solutions (implied)
- Support initiatives that reduce carbon footprint in your community (implied)

# Whats missing in summary

The full transcript provides detailed insights on the contrasting reports by AOC and the Department of Defense on climate change, urging immediate action to mitigate the threats outlined.

# Tags

#ClimateChange #AOC #DepartmentOfDefense #UrgentAction #EnvironmentalThreats