# Bits

Beau says:

- Explains the constitutionality of impeachment amid claims of unconstitutionality.
- Emphasizes the impeachment process as the indictment phase.
- Quotes Article 3 Section 2, which stipulates that impeachment trials are not criminal prosecutions.
- Clarifies the limited punishment the Senate can impose: removal from office and disqualification from holding future office.
- Points out that the President's right to pardon does not apply in cases of impeachment.
- Mentions the requirements for the Senate during impeachment trials: under oath, Chief Justice presides, and a two-thirds majority is needed to convict.
- Stresses that the President's right to confront accusers applies in a criminal trial, not impeachment.
- Asserts that there is nothing unconstitutional about the current impeachment proceedings.
- Addresses claims of denial of due process by explaining its inapplicability at this stage.
- Concludes by debunking notions of a coup and affirming the legitimacy of the impeachment process.

# Quotes

- "The House has sole power over impeachment proceedings."
- "There is nothing unconstitutional occurring."
- "It's not a coup. It's the way the process is laid out."

# Oneliner

Beau explains the constitutionality of impeachment, debunking claims of unconstitutionality and clarifying the process with a focus on indictment and limited punishment.

# Audience

Citizens, Activists, Voters

# On-the-ground actions from transcript

- Stay informed on the impeachment proceedings and the constitutional aspects involved (suggested).
- Educate others on the impeachment process and the constitutional guidelines (suggested).

# Whats missing in summary

Detailed breakdown of specific articles and sections of the Constitution relevant to impeachment.

# Tags

#Impeachment #Constitutionality #DueProcess #Senate #Punishment