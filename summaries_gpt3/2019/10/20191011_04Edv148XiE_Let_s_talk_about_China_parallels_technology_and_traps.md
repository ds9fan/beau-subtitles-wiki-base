# Bits

Beau says:

- Beau introduces a comment about learning about cultures only when something bad happens to them, specifically mentioning at-risk cultures.
- The parallel between China and the United States is discussed, focusing on technology becoming a trap through the example of the Uyghurs.
- The Uyghurs, residing in Xinjiang, China, have a rich history and were part of the Republic of East Turkestan before becoming part of China in 1949.
- The Uyghurs practice Sufism, a mystical component of Islam, which fundamentalists may not approve of due to its newer traditions and rituals.
- Beijing's uncomfortable relationship with the Uyghurs intensified after some Uyghurs were found in a training camp in Afghanistan, leading to riots, bombings, and a security clampdown in the 1990s and early 2000s.
- Beijing justified their actions as benevolent civilizing efforts, but they actually subjugated and relocated the Uyghurs, bringing in millions of Han Chinese for settlement.
- Mass surveillance on the Uyghurs began in 2016 with technologies like smartphones, DNA scanning, facial recognition, and more, under the guise of preventing extremism.
- A million adults are currently in camps termed "vocational centers" by China, facing erasure of their culture and ethnicity through forced assimilation.
- Uyghur children are being taught Chinese culture instead of their own, further erasing their identity.
- Beau draws parallels between the Uyghur experience and the native experience in the United States, reflecting on the rapid progression from technological advancements to mass internment camps.

# Quotes

- "We only learn about it when something bad happens to them."
- "Beijing was worried about extremification."
- "They're erasing the culture. They're erasing the ethnicity."
- "Mass surveillance started. Eight years later there's camps."
- "So that's a little brief primer on who they are."

# Oneliner

Beau introduces the issue of learning about cultures only after tragedies and delves into the parallels between China and the U.S. through the lens of the Uyghurs' plight, illustrating rapid cultural erasure through mass surveillance and internment camps.

# Audience

Activists, Humanitarians

# On-the-ground actions from transcript

- Contact human rights organizations to support the Uyghur cause (suggested)
- Join protests or awareness campaigns advocating for the rights of the Uyghur community (exemplified)
- Support initiatives providing aid to Uyghur refugees or families affected by the crisis (implied)

# Whats missing in summary

The full transcript provides a comprehensive understanding of the cultural erasure and human rights violations faced by the Uyghur community in China, urging viewers to take action and stand against such injustices.

# Tags

#Uyghurs #HumanRights #CulturalErasure #MassSurveillance #InternmentCamps #China #Activism #Awareness