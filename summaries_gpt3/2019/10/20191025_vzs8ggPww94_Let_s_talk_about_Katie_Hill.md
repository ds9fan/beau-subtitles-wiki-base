# Bits

Beau says:

- Congressperson Hill's scandalous photos surfaced, sparking a national debate.
- The photos show adults engaged in consensual activity and Hill using a substance that was once illegal.
- Hill has a tattoo that may have been an iron cross, a symbol associated with various groups.
- She is the first bisexual representative from California and these photos confirm her admission.
- There's ethical concern about her dating an employee, although not illegal.
- Beau questions why these photos are circulating; suggests it's due to Hill being a pretty woman.
- He challenges the double standard women face regarding purity and behavior.
- Beau points out the lack of harm in Hill's actions and questions the public interest.
- He criticizes the misplaced priorities of focusing on these photos rather than more critical issues.
- Beau contrasts the attention on Hill's photos with the President's lawyers arguing for his right to kill people.

# Quotes

- "Women are still held to this false idea, this standard that they need to remain pure and that they need to behave in a certain way."
- "None of this is any of our business."
- "This country has its priorities mixed up."
- "It's just a thought."
- "Y'all have a good night."

# Oneliner

Beau points out the double standard faced by women in politics through the scandal surrounding Congressperson Hill's photos, urging a shift in priorities towards more critical issues.

# Audience

Political activists

# On-the-ground actions from transcript

- Support organizations advocating for gender equality and against slut-shaming (implied)
- Engage in critical discourse on gender bias and double standards in politics (implied)

# Whats missing in summary

The full transcript provides a deeper analysis of gender biases in politics and society, urging viewers to question societal standards and focus on more critical issues.

# Tags

#GenderEquality #PoliticalBias #DoubleStandards #SocialJustice #ChangePriorities