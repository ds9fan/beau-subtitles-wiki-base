# Bits

Beau says:

- An American diplomat's wife, protected by diplomatic immunity, caused the death of a 19-year-old in the UK by driving the wrong way.
- The family of the victim is left with no closure as she used immunity to return to the US without facing consequences.
- The Kurds, a vital ally of the US in the Middle East, are being betrayed as the US greenlights Turkey's incursion into Syria.
- Despite the Kurds' loyalty and support to the US, they are at risk during this invasion with no country of their own.
- The US is actively supporting the invasion, unlike past betrayals that were more discreet.
- Beau connects these seemingly unrelated events, revealing a deeper theme of establishment versus the people.
- He criticizes the establishment for always protecting themselves at the expense of the people.

# Quotes

- "It's always portrayed as left versus right, as your demographic versus another equal or lesser maybe just above demographic. At the end of the day, it's not what it is."
- "It's establishment versus you. They will always write you off to protect themselves. It's always what happens."
- "George Carlin said it best. It's a big club and you ain't in it."

# Oneliner

An American diplomat's wife avoids accountability using diplomatic immunity while the US betrays its Kurdish allies in Syria, revealing a deeper theme of establishment versus the people.

# Audience

Global citizens

# On-the-ground actions from transcript

- Contact local representatives to advocate for justice for victims of diplomatic immunity (implied).
- Support organizations aiding Kurds and raise awareness about their plight (implied).

# Whats missing in summary

The emotional impact on the victim's family and the Kurds, and the need for accountability and support for those affected by diplomatic actions.

# Tags

#DiplomaticImmunity #USForeignPolicy #KurdishAllies #EstablishmentVsPeople #GlobalJustice