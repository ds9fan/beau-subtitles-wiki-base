# Bits

Beau says:

- Techniques to teach youth survival skills can be applied to any topic, not just survival.
- Outward Bound is a good option for training if you have the budget.
- Instructors who teach survival skills well go beyond practical skills to include critical thinking and problem-solving.
- Many instructors make a lot of money by training corporate executives.
- Beau encourages individuals to take on the role of the instructor themselves.
- Learning survival skills teaches critical thinking, problem-solving, improvisation, and self-reliance.
- Beau advises against trying to toughen up kids physically; mental toughness develops naturally.
- He recommends reading the U.S. Army Survival Manual as a starting point.
- Beau suggests making learning fun by not fighting popular culture and incorporating the youth's interests.
- Teaching survival skills through activities like escape rooms, chemistry sets, fishing, and camping can be engaging.
- Spending quality time with family while teaching these skills is a valuable aspect of the process.

# Quotes

- "Teach them the skills. Make it fun."
- "If you can survive a zombie apocalypse, you can certainly survive a hurricane."
- "Just make it fun and for God's sakes do not try to toughen up your kid."

# Oneliner

Teach survival skills to youth by making it engaging, fun, and tailored to their interests, promoting critical thinking and problem-solving while spending quality time together as a family.

# Audience

Parents, educators, mentors

# On-the-ground actions from transcript

- Read and familiarize yourself with the U.S. Army Survival Manual (suggested).
- Incorporate survival skill activities into family time like camping, fishing, or learning chemistry sets (exemplified).
- Organize hip-pocket classes and activities for hands-on learning (exemplified).
- Use escape rooms or paper and pencil puzzles to develop problem-solving skills (exemplified).

# Whats missing in summary

The full transcript provides detailed insights into engaging youth in learning survival skills through interactive activities and fostering mental toughness in a fun and relatable way.