# Bits

Beau says:

- Shares the chain of events in the morning following a poll on impeachment support.
- Trump's response to the poll results by soliciting interference in the election live on TV.
- Beau expresses his belief that Trump's actions are impeachable.
- Mentions Trump being a victim of the fake news media.
- Compares the Biden theory to a fabricated storyline about JFK's assassination involving Jackie Kennedy.
- Criticizes right-wing conspiracy sites for spreading misinformation.
- Talks about Giuliani showing the fabricated story to the president, who acts on it.
- Points out the irony of Trump being brought down by fake news after attacking journalists for the same.
- Beau humorously comments on the situation, suggesting everyone could use a good laugh.

# Quotes

- "45%, those are rookie numbers, we need to pump those numbers up."
- "I guess Trump saw that and was like, you know what, 45%, those are rookie numbers, we need to pump those numbers up."
- "The man that told actual journalists that they were fake news is going to be brought down by faking it."
- "And they say poetic justice is dead. It's not. It's alive and well."
- "You can't write stuff like this."

# Oneliner

Beau breaks down Trump's impeachment support, his response, and the irony of fake news in politics.

# Audience

Political observers

# On-the-ground actions from transcript

- Fact-check and verify information before sharing (implied)

# Whats missing in summary

The full transcript provides a humorous take on the unfolding political events, offering a moment of levity amidst the seriousness of the situation.

# Tags

#Impeachment #FakeNews #PoliticalCommentary #Satire #ElectionInterference