# Bits

Beau says:

- Criticizes Trump's administration and advisors for giving bad advice.
- Talks about the chain of events starting in Turkey and Syria, where Trump ordered troops out of the way, leading to a betrayal of allies who fought alongside American soldiers.
- Points out that Trump's base defended him, but it made Trump look worse than Obama in handling diplomatic situations.
- Mentions the obstruction of a State Department employee from testifying in the impeachment inquiry.
- Suggests that Trump release the audio of phone calls and testify before the House to set the record straight.
- Warns that Trump's legacy will be worse than Obama's if he doesn't take action.

# Quotes

- "Release the audio of those phone calls."
- "He's scared of everything."
- "You didn't vote for the guy who's going to hide in the White House."
- "He made the U.S. military look the way it does now."
- "The base is going to want to see him out there taking these hoaxsters and scamsters to task."

# Oneliner

Beau criticizes Trump's handling of diplomatic situations, suggests releasing phone call audio, and warns of a negative legacy if action isn't taken.

# Audience

Political activists

# On-the-ground actions from transcript

- Release the audio of phone calls and testify before the House (suggested)
- Hold hoaxsters and scamsters accountable (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's actions and their implications, urging for accountability and transparency.

# Tags

#Trump #Diplomacy #Impeachment #Legacy #Accountability