# Bits

Beau says:

- The President of the United States allowed Turkey to invade Syria, abandoning Kurdish allies who have fought alongside American soldiers for 15 years.
- Turkish-backed militants committed atrocities against the Kurds, driving them to seek support from Russia.
- The Kurds are a significant ally in the Middle East, not a country but an ethnic group who have been betrayed and sold out openly.
- The U.S. military moved a small number of troops, enabling Turkey's invasion of Syria, leading to Russian and Syrian troops filling the power vacuum left by U.S. forces.
- ISIS could potentially grow stronger due to recent events, posing a threat that might lead to U.S. military intervention in the future.
- The term "special forces" refers specifically to Green Berets, who train indigenous forces and stand out as instructors in hostile environments.
- The Department of Defense is seen as a private military contractor, with the U.S. military viewed as serving Saudi Arabia's interests in the Middle East.
- War is depicted as a profitable and vicious enterprise that exposes soldiers to being bought and sold for money and power.
- The current situation in the Middle East is characterized by waste, with slogans like "End the war" and "Anti-imperialism" failing to match the reality of the conflict.
- The speaker calls for a reevaluation of how indigenous forces are used and discarded for political convenience.

# Quotes

- "War is just a continuation of politics by other means, right? It has always been like this."
- "The Department of Defense has become the world's largest private military contractor."
- "The only thing that's happening over there right now is waste."
- "The American people. I got a feeling we're going to have to look at what we've done."
- "We are not going to use and abuse indigenous forces like this and to just hang them out to dry when it's politically expedient."

# Oneliner

The President's decision to allow Turkey's invasion of Syria and abandon Kurdish allies reveals the harsh realities of war and foreign policy.

# Audience

American citizens

# On-the-ground actions from transcript

- Support organizations advocating for the protection and rights of indigenous forces (implied)
- Advocate for a more transparent and accountable foreign policy that prioritizes ethical considerations (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the consequences of political decisions on the ground and calls for a reevaluation of how military forces are deployed and utilized.