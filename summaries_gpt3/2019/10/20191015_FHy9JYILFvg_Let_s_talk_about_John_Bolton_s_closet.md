# Bits

Beau says:

- Surprised by John Bolton's ethical objection to the Trump administration's actions.
- Bolton referred to Trump administration's actions as a "drug deal."
- Bolton previously covered up the Iran Contra affair as assistant attorney general in the 80s.
- Claimed happiest moment was getting the U.S. out of the International Criminal Court to protect U.S. war criminals.
- Accused of derailing a conference on stopping weapons of mass destruction and later using that as a pretext for invasion.
- Allegations of blackmailing the chief of the Organization for the Prohibition of Chemical Weapons.
- Describes Bolton as a true believer who rationalizes his actions to protect American interests.
- Three options presented: Bolton grew a conscience, Trump's actions were too shady even for Bolton, or the actions truly jeopardized national security.
- Beau criticizes Bolton's role in American foreign policy and warns against turning him into a hero even if he brings down Trump.
- Concludes by pointing out Bolton's deceit in not coming forward about the transgressions he knew of.

# Quotes

- "They're true believers. Bolton is a true believer."
- "True believers are the most dangerous people on the planet."
- "Even in his most noble act, deceit was at its core."

# Oneliner

Beau reveals John Bolton's shady past and questions his sudden ethical objection to Trump administration's actions, cautioning against turning him into a hero.

# Audience

Viewers, Activists, Critics

# On-the-ground actions from transcript

- Hold officials accountable for their actions (implied)

# Whats missing in summary

The full video provides a deep dive into John Bolton's history and actions, shedding light on the dangers of true believers in positions of power.

# Tags

#JohnBolton #TrumpAdministration #ForeignPolicy #TrueBelievers #NationalSecurity