# Bits

Beau says:
- Takes a day off to play with kids, recalls G.I. Joe show from the 80s and its plot dynamics.
- Draws parallels between G.I. Joe's preemptive actions against Cobra and real-life situations.
- Mentions Lavrenty Beria, a powerful figure in the Soviet Union, known for his infamous quote.
- Criticizes President involving himself in corruption cases without identifiable crimes.
- Questions the process of finding a person to hang and then looking for a crime to pin on them.
- Talks about the dangerous thought process of "Show me the man, I'll show you the crime."
- References Beria's role as chief of the NKVD secret police and his history of heinous actions.

# Quotes

- "Show me the person and I'll show you the crime."
- "Doing it any other way is extremely dangerous."
- "Find a person that we want to hang, and then we go find the crime to hang on them."
- "Show me the man, I'll show you the crime."
- "He is accused of doing some pretty horrible things using that thought process."

# Oneliner

Beau points out dangerous parallels between historical power dynamics and current events, warning against the implications of targeting individuals before crimes exist.

# Audience

Those concerned about justice.

# On-the-ground actions from transcript

- Investigate and stay informed about cases where individuals are targeted without identifiable crimes (implied).
- Advocate for due process and fair investigations in political matters (implied).

# Whats missing in summary

The nuance and historical context provided by Beau in the full transcript.

# Tags

#Justice #PowerDynamics #Corruption #HistoricalParallels #Accountability