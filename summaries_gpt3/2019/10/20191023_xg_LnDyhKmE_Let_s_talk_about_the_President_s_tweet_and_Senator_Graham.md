# Bits

Beau says:

- President mentioned Emmett Till, leading to the term "lynching" surfacing in political discourse.
- Joe Biden used the term 20 years ago, but it was still wrong.
- Republicans claimed impeachment proceedings were like a lynching, which is false.
- Senator Lindsey Graham echoed this sentiment, showing ignorance of history and the Constitution.
- Graham failed to understand that due process in impeachment occurs during the Senate trial.
- The impeachment inquiry is not extrajudicial; it follows a constitutional process.
- Wealthy individuals can't halt investigations, unlike what some may be accustomed to.
- President Trump's involvement in an actual lynching-like scenario was advocating for the execution of innocent people.
- Lynchings are not ancient history, with the most recent recorded case in 2011.
- Over 3,400 recorded lynchings of Black Americans occurred, dispelling the idea that it's ancient history or insignificant in magnitude.

# Quotes

- "The impeachment process is exactly nothing like a lynching."
- "The closest thing to a lynching that President Trump has been involved in is when he took out a full-page ad advocating for the execution of people who turned out to be innocent."
- "Over 3,400 recorded lynchings of Black Americans occurred."

# Oneliner

Beau clarifies the misuse of the term "lynching" in political discourse and sheds light on the historical and constitutional inaccuracies surrounding it.

# Audience

Politically aware individuals

# On-the-ground actions from transcript

- Educate others on the historical significance and brutality of lynchings (implied)

# Whats missing in summary

The emotional impact of using historical atrocities for political gain

# Tags

#EmmettTill #Lynching #Impeachment #HistoricalInaccuracy #Constitution