# Bits

Beau says:

- Beau dives into the story of General Butler, who was a highly respected Marine with extensive experience in various wars.
- FDR's progressive reforms didn't sit well with the wealthy business class, leading them to plan a coup to install a fascist government.
- Wealthy and powerful figures like J.P. Morgan and Prescott Bush were implicated in the coup plan.
- McGuire acted as an intermediary, gradually escalating the plan to have Butler lead a march on Washington and seize control.
- Butler, being a staunch anti-capitalist, was the wrong choice for the coup leaders.
- Butler exposed the plan by going straight to Congress, but newspapers dismissed it as a hoax initially.
- Even though the truth came out later, no one was prosecuted for the coup attempt.
- The coup attempt serves as a reminder of the dangers of political power plays and the importance of individuals' choices.
- If Butler had agreed to lead the coup, the course of history could have been drastically altered.
- The story underscores the significance of one person's decision in shaping the world's future.

# Quotes

- "One guy refused to take command. One guy refused. One person. Literally changed the entire course of human history."
- "Assuming that the allegations are true, the world owes General Butler the entire world."

# Oneliner

General Butler's refusal to lead a coup against FDR prevented a potential fascist dictatorship in the U.S., showcasing the immense impact of individual choices on history.

# Audience

History enthusiasts, advocates for democracy.

# On-the-ground actions from transcript

- Support and defend democratic institutions (exemplified).
- Stay vigilant against threats to democracy (exemplified).

# Whats missing in summary

The full transcript provides a detailed account of General Butler's role in foiling a coup attempt, shedding light on the fragility of democracy and the power of individual decisions.

# Tags

#GeneralButler #CoupAttempt #Democracy #Fascism #IndividualChoice