# Bits

Beau says:

- Introduces psychological effects: Milgram experiments and bystander effect.
- Milgram experiments show people tend to follow authority figures.
- Bystander effect means people are less likely to act in a group.
- California incident at Cal State, Long Beach involved a lockdown without locks.
- Some students barricaded doors during the lockdown.
- Others did nothing and waited in unlocked rooms.
- Beau praises proactive students who took action.
- He criticizes those who did nothing during the lockdown.
- Beau challenges law enforcement's approach to lockdowns.
- Urges schools to install locks immediately for safety.
- Warns that the school without locks is now a target.
- Emphasizes that lockdowns are guidelines for safety, not suicide pacts.

# Quotes

- "The purpose of a lockdown is to get as many people as possible somewhere secure to maintain their safety."
- "You made the right decision with the information you had at hand."
- "A lockdown is a guideline. It's there to keep you safe, okay?"
- "You get somewhere safe."
- "It's not there to order you to sit in a room that's unlocked."

# Oneliner

Beau explains psychological effects, praises proactive students during a lockdown without locks, and challenges misconceptions about safety protocols.

# Audience

Students, school staff, community members

# On-the-ground actions from transcript

- Install locks immediately in facilities without adequate security (implied)
- Take proactive measures during emergencies (implied)

# Whats missing in summary

Importance of taking proactive safety measures and being prepared for emergencies.

# Tags

#Lockdown #SchoolSafety #EmergencyPreparedness #CommunityPolicing