# Bits

Beau says:

- Mr. Beast, a 21-year-old YouTuber known for silly stunts, is involved in a unique project to raise $20 million for planting trees.
- Partnered with the Arbor Day Foundation, the goal is to plant 20 million trees across the country by December 2022.
- The foundation, with a 50-year history, will ensure that each dollar donated equals one tree planted.
- The project aims to plant native species, promoting environmental impact.
- Within just two days, $5 million has already been raised for the cause.
- Beau encourages viewers to donate to the Arbor Day Foundation, mentioning that donations may be tax-deductible.
- Beyond donating, Beau suggests planting native fruit trees in personal yards to enhance food security, reduce grocery bills, and foster community spirit.
- Beau acknowledges the project's success in engaging a wide audience, with various creative videos showcasing the tree-planting efforts.
- Unlike typical online challenges, this initiative is praised for its potential significant impact on the environment.
- Beau urges viewers to support the cause by contributing a dollar or two.

# Quotes

- "Your donation to them [Arbor Day Foundation] would be tax deductible in most cases."
- "Definitely kind of jumping on team trees here."
- "It's actually going to create some meaningful impact."

# Oneliner

Mr. Beast collaborates with Arbor Day Foundation to plant 20 million trees by December 2022, setting a goal of raising $20 million, already reaching $5 million in two days.

# Audience

Environmental enthusiasts, Tree advocates

# On-the-ground actions from transcript

- Donate a dollar or two to the Arbor Day Foundation to support tree-planting efforts (suggested).
- Plant native fruit trees in your yard to enhance food security, reduce grocery bills, and build community spirit (implied).

# Whats missing in summary

The full transcript provides more detailed insights on the impact of planting native species and how viewers can actively participate beyond financial contributions.

# Tags

#MrBeast #ArborDayFoundation #TreePlanting #EnvironmentalImpact #CommunitySpirit