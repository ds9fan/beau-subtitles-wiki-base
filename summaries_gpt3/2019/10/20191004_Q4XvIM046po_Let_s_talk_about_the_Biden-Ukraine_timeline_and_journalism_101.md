# Bits

Beau says:

- Introduces the topic of the Biden-Ukraine event and the importance of journalism in the narrative.
- Talks about disclosure in journalism and the basic principles of Journalism 101.
- Explains the relevance of laying out a timeline in investigative journalism.
- Provides a detailed timeline of events related to the Biden-Ukraine issue, starting from 2012.
- Addresses the misconception surrounding the investigation into Biden's son and the energy company.
- Mentions the concerns about corruption in Ukraine and the role of various Western powers in addressing it.
- Describes Vice President Biden's involvement in dealing with corruption issues in Ukraine.
- Points out the bipartisan effort within the United States to address corruption in Ukraine.
- Talks about the change in prosecutors in Ukraine in 2016 and the reopening of the investigation.
- Emphasizes the importance of evidence in journalism and refutes claims without substantial proof.
- Concludes by stressing the need for physical evidence to support any narrative changes.

# Quotes

- "If you are attempting to pass something off as objective and you have a bias, you should disclose it to your readers or your viewers, that is Journalism 101."
- "You need evidence, physical evidence, call logs, emails, something."
- "Unless there's physical evidence that you can present, there's no story here."
- "You need evidence to change this. And it might be out there."
- "But you need evidence, not any window."

# Oneliner

Beau breaks down the Biden-Ukraine timeline, debunks misconceptions, and stresses the need for concrete evidence in journalism.

# Audience

Journalism students, truth-seekers

# On-the-ground actions from transcript

- Fact-check information shared on social media platforms (implied)
- Support investigative journalism by subscribing to reputable sources (implied)

# Whats missing in summary

Full context and detailed insights on the Biden-Ukraine timeline can be better understood by watching the full transcript.

# Tags

#Biden-Ukraine #Journalism #FactChecking #Corruption #Evidence