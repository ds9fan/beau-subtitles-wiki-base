# Bits

Beau says:

- There is a call to action in minority communities post the Jefferson shooting, where a woman was shot by a cop in her own home.
- The call to action is to establish a community defense force, reminiscent of the original days of the Black Panther Party.
- Beau advocates for localized community defense organizations for all communities, not just particular racial or ethnic groups.
- He believes such organizations can avoid the pitfalls the Black Panthers faced and should focus on community development beyond just defense.
- Beau suggests avoiding a militant structure like the Black Panthers had to prevent negative media portrayal and law enforcement scrutiny.
- Winning the PR battle is critical for such organizations to avoid being labeled negatively by the media or law enforcement.
- Beau acknowledges the concerns and challenges but believes that with proper messaging and community support, these organizations can succeed.
- He encourages the establishment of organizations that create redundant and localized power structures to benefit communities.
- Beau stresses the importance of steering away from a paramilitary image to maintain a positive perception and prevent becoming a target.
- The focus should be on community development activities like microloans, mentoring, childcare, and skill development within these organizations.

# Quotes

- "Creating that redundant power, creating that localized power structure is really important."
- "The PR battle is what's gonna be important here."
- "The first battle is a PR battle."
- "And I don't think that they will become a target as long as they win the PR war up front."
- "The US has changed, it's gotten better, but it hadn't changed that much."

# Oneliner

Beau advocates for establishing community defense organizations with a focus on community development, stressing the importance of winning the PR battle to avoid negative portrayal and scrutiny.

# Audience

Community organizers

# On-the-ground actions from transcript

- Establish localized community defense organizations with a focus on community development (implied)
- Ensure proper messaging to win the PR battle and prevent negative portrayal (implied)
- Seek support from organizers within the community for setting up these organizations (exemplified)

# Whats missing in summary

The full transcript provides more context on the challenges and importance of community defense organizations in marginalized communities.

# Tags

#CommunityDefense #BlackPantherParty #PRBattle #LocalizedPower #CommunityDevelopment