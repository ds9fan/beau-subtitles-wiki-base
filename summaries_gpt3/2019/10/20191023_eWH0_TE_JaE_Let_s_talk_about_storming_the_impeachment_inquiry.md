# Bits

Beau says:

- Republican lawmakers stormed the impeachment inquiry to disrupt the process.
- McConnell instructed them to attack the process instead of defending the President's actions.
- Closed-door proceedings, like the one in question, are common and not unusual.
- Those excluded from the hearing are either on the wrong committees or lack security clearance knowledge.
- The double standard exists as normal individuals engaging in similar actions face severe consequences compared to lawmakers.
- The President's lawyer argued for immunity from prosecution, suggesting a disregard for the law.
- The focus on attacking the process signifies a prioritization of party over upholding the Constitution and justice.

# Quotes

- "They care more about their party than they do the Constitution."
- "They care more about their party than you."

# Oneliner

Republican lawmakers disrupt impeachment inquiry, prioritizing party over Constitution and justice.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Challenge political double standards and demand accountability for actions (implied)
- Stay informed and advocate for transparency in political processes (implied)

# Whats missing in summary

Insight into the specific details of the closed-door proceedings and the testimony that was deemed "devastating."

# Tags

#Impeachment #PoliticalDoubleStandards #Accountability #Transparency #Justice