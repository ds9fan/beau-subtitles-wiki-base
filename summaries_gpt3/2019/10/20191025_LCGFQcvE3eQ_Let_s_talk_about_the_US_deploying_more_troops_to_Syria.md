# Bits

Beau says:

- The US is sending armor, including tanks, to Syria to protect a gas plant, contradicting the idea of a withdrawal.
- Sending tanks without ground troops makes them vulnerable to anti-tank missiles in Syria.
- Ground troops, maintenance crews, supply guys, infantry, surveillance units, drones, and jets are needed for this operation.
- Despite claims of de-escalation, more troops are being sent to Syria than withdrawn.
- The true motive behind these actions is money and creating a U.S. zone in northeastern Syria.
- The President suggested Kurds move to oil regions, complicit in ethnic cleansing by allowing Turkish military forces in.
- The actions taken do not represent anti-imperialism or anti-colonialism but serve other interests.
- The US is prioritizing protecting gas plants despite becoming a net energy exporter soon.
- Beau criticizes the administration for selling out powerful Middle Eastern actors for questionable foreign policy decisions.

# Quotes

- "Sending armor to Syria contradicts the idea of a withdrawal."
- "More troops are being sent than withdrawn. It's about money and creating a U.S. zone."
- "The President is complicit in ethnic cleansing by allowing Turkish military forces in."
- "It's not anti-imperialist or anti-colonialist, just packaged differently for supporters."
- "We sold out powerful Middle Eastern actors for questionable foreign policy decisions."

# Oneliner

The US is sending more troops and armor to Syria, contradicting claims of withdrawal, revealing ulterior motives, and risking complicity in ethnic cleansing.

# Audience

Activists, policymakers.

# On-the-ground actions from transcript

- Contact policymakers to express opposition to military actions in Syria (suggested).
- Support organizations advocating for the protection of vulnerable groups in conflict zones (exemplified).
- Join local movements opposing questionable foreign policy decisions (implied).

# Whats missing in summary

Deeper insights into the geopolitical implications and consequences of the US military actions in Syria. 

# Tags

#Syria #USMilitary #ForeignPolicy #EthnicCleansing #Geopolitics