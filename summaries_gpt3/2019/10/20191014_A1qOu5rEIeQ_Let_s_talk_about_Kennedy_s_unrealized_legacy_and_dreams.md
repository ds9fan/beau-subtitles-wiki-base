# Bits

Beau says:

- President Kennedy is often mythologized as the most significant president in American history, associated with Camelot and the Golden Age.
- Kennedy's famous quote "Ask not what your country can do for you, ask what you can do for your country" has a great impact.
- Kennedy's vision aimed at using education programs to empower people to stand on their own.
- The Green Berets, an organization that flourished under Kennedy, could have been utilized differently according to his original vision.
- In 1960, Kennedy introduced the Peace Corps, envisioning Americans traveling globally to do good.
- The Peace Corps, originally intended for humanitarian purposes, eventually became a tool for US foreign policy.
- Beau advocates for a shift towards asking what you can do for society rather than focusing solely on what society can do for you.
- He stresses the importance of community networks at the local level to build communities without government intervention.
- Beau dreams of individuals helping each other across borders and without government interference, envisioning a world where people help people.
- He encourages action towards building community networks and helping others without borders or government involvement.

# Quotes

- "Ask not what society can do for you, but ask what you can do for society."
- "Just people helping people."
- "Choose to do it. Not because it's easy, but because it's hard."

# Oneliner

President Kennedy's legacy, from the Peace Corps to community networks, inspires a vision of people helping people beyond borders and government involvement.

# Audience

Global citizens

# On-the-ground actions from transcript

- Build community networks at the local level to empower communities (suggested)
- Help others without borders or government intervention (implied)

# Whats missing in summary

The full transcript provides a deeper insight into President Kennedy's legacy, the Peace Corps' transformation, and the importance of community networks in fostering a world where individuals help each other across borders without government interference.

# Tags

#PresidentKennedy #PeaceCorps #CommunityNetworks #GlobalCitizenship #PeopleHelpingPeople