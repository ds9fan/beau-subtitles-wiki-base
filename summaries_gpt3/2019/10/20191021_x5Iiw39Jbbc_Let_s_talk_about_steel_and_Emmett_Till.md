# Bits

Beau says:

- Introduction to discussing Emmett Till and his legacy with a focus on a new sign made of steel.
- Emmett Till, a 14-year-old boy, was murdered in 1955 for allegedly whistling at a white woman in Mississippi.
- The woman whose accusation led to Emmett Till's murder later admitted she fabricated parts of the story, and the accused men were acquitted.
- Emmett Till's mother displayed incredible strength by having an open casket funeral for her son, revealing the brutal reality of his murder.
- Photos of Emmett Till's body were published and became a catalyst for the civil rights movement.
- Rosa Parks credited Emmett Till with strengthening her resolve in the fight for civil rights.
- The acquittal of Emmett Till's murderers marked a turning point where the Black community realized they couldn't rely on the courts for justice.
- Emmett Till's legacy continues to inspire, symbolized by a new bulletproof sign made of steel in his honor.
- The significance of the steel sign lies in its resilience, mirroring the unwavering strength of Emmett Till's mother and the enduring impact of Emmett Till's story on American history.

# Quotes

- "His legacy was bulletproof, his mom was still unwavering, unflinching, just like that new sign."
- "It doesn't matter if somebody shoots the sign or spray paints it or takes it. It's there. It's carved in stone and blood in American history."

# Oneliner

Beau delves into the story of Emmett Till, from his tragic murder to his enduring legacy represented by a new bulletproof steel sign.

# Audience

History enthusiasts, social justice advocates

# On-the-ground actions from transcript

- Visit historical sites related to civil rights movements (implied)
- Support organizations working towards racial justice (implied)

# Whats missing in summary

The emotional impact of the brutal murder of Emmett Till and its role in fueling the civil rights movement.

# Tags

#EmmettTill #CivilRights #Legacy #RacialJustice #History