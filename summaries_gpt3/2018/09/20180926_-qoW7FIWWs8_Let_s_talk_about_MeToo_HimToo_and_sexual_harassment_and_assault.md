# Bits

Beau says:

- Addresses sexual harassment and assault in the context of the Kavanaugh nomination.
- Provides advice on avoiding such situations, including safeguarding reputation and watching signals.
- Mentions ending dates at the front door, avoiding going home with anyone if drinking, and being cautious with words.
- Talks about the importance of keeping hands to oneself to avoid sending wrong signals.
- Acknowledges the "him-too" hashtag and addresses men's concerns about false accusations.
- Indicates that false accusations of sexual harassment or assault are rare, around 2% of the time.
- Stresses mutual accountability between men and women regarding actions and situations.
- Refutes the idea that clothing choices can cause rape, asserting that rapists are the sole cause.
- Concludes with a powerful statement: "Gentlemen, there is one cause of rape, and that's rapists."

# Quotes

- "Be very careful with the signals you send."
- "False accusations of sexual harassment or sexual assault turn out to be false about 2% of the time."
- "There is one cause of rape, and that's rapists."

# Oneliner

Beau provides advice on avoiding sexual harassment and assault, stressing accountability and debunking victim-blaming myths.

# Audience

Men, Women

# On-the-ground actions from transcript

- Safeguard your reputation by being mindful of the signals you send (implied).
- End dates at the front door to prioritize quality over quantity (implied).
- Avoid taking anyone home when drinking to prevent risky situations (implied).

# Whats missing in summary

In-depth analysis of the impacts of victim-blaming and misconceptions on sexual assault prevention.

# Tags

#SexualHarassment #AssaultPrevention #Accountability #EndVictimBlaming #RapeCulture