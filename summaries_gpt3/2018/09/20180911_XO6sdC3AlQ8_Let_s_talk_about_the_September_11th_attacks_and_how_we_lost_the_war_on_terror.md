# Bits

Beau says:

- Recalls his experience on September 11th, watching the tragic events unfold live on TV.
- Expresses how a casualty of that day was the erosion of freedom and rights, not listed on any memorial.
- Talks about the erosion of freedoms post-9/11 due to government actions in the name of security.
- Mentions the strategic nature of terrorism and how it aims to provoke overreactions from governments.
- Points out the dangerous normalization of loss of freedoms in society.
- Urges for a grassroots approach to reclaiming freedom, starting at the community level.
- Advocates for teaching children to question and resist the normalization of oppressive measures.
- Emphasizes the importance of self-reliance in preparation for natural disasters and reducing dependency on the government.
- Suggests counter-economic practices like bartering and cryptocurrency to reduce government influence.
- Encourages surrounding oneself with like-minded individuals who support freedom and each other.
- Calls for supporting and empowering marginalized individuals who understand the loss of freedom.
- Talks about the effectiveness of U.S. Army Special Forces in training local populations as a force multiplier.
- Stresses the need for community building to resist government overreach and tyranny.

# Quotes

- "We didn't know it at the time, though, we were too busy, too busy putting yellow ribbons on our cars, looking for a flag to pick up and wave, or picking up a gun, traveling halfway around the world to kill people we never met."
- "You can't dismantle the surveillance state or the police state by voting somebody into office."
- "You have to defeat an overreaching government by ignoring it."
- "The face of tyranny is always mild at first. And we're there right now."
- "If your community is strong enough, what happens in DC doesn't matter because you can ignore it."

# Oneliner

Beau warns against the dangerous normalization of loss of freedoms post-9/11, urging for grassroots community action to reclaim freedom and resist government overreach.

# Audience

Community members

# On-the-ground actions from transcript

- Teach children to question and resist oppressive measures (implied)
- Prioritize self-reliance for natural disasters and reduce dependency on the government (implied)
- Practice counter-economic activities like bartering and cryptocurrency (implied)
- Surround yourself with like-minded individuals who support freedom (implied)
- Support and empower marginalized individuals who understand the loss of freedom (implied)
- Build community networks to resist government overreach (implied)

# Whats missing in summary

The full transcript provides deeper insights into the erosion of freedoms post-9/11 and the strategic nature of terrorism, urging individuals to take concrete actions at the community level to safeguard freedom and resist tyranny.

# Tags

#Freedom #Terrorism #CommunityAction #GovernmentOverreach #Grassroots