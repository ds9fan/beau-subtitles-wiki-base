# Bits

Beau says:

- Recounts a story from college where he intervened in a potentially dangerous situation at a bar to protect a girl he vaguely knew from being harassed by a guy and his friends.
- Draws parallels between his bar incident and the current atmosphere around the Kavanaugh hearing, discussing the importance of respecting women's autonomy and agency.
- Uses a hypothetical scenario with friends to illustrate the prevalence of sexual assault among women and how comments dismissing survivors can affect their willingness to come forward.
- Criticizes the double standards and misconceptions surrounding men's emotions and reactions to trauma, contrasting them with the Kavanaugh hearings.
- Emphasizes that sexual assault is about power and control, pointing out the significance of survivors speaking out against someone who may wield immense power.
- Urges reflection on the impact of dismissing survivors and perpetuating harmful narratives, stressing the lasting damage it can cause to relationships.

# Quotes

- "You might not want to do it again because you're altering real-life relationships over bumper sticker politics."
- "Sexual assault is normally about power and control."
- "You might not want to do it again because you're altering real-life relationships over bumper sticker politics."
- "You already ruined that."
- "How many of them do you know? How many of them talked about it with you?"

# Oneliner

Beau recounts a bar incident to illustrate respecting women's autonomy, drawing parallels to the Kavanaugh hearing's impact on survivors and urging reflection on harmful narratives.

# Audience

All genders, allies

# On-the-ground actions from transcript

- Reach out to survivors in your community, offer support, and believe them (implied)
- Educate yourself and others on the prevalence of sexual assault and its impact (suggested)

# Whats missing in summary

Importance of believing and supporting survivors, challenging harmful narratives, and fostering a culture of respect and accountability.

# Tags

#Respect #BelieveSurvivors #SexualAssaultAwareness #GenderEquality #CommunitySupport