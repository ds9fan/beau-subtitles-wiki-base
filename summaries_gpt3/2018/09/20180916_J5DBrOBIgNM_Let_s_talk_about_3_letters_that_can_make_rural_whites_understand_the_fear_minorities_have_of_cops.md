# Bits

Beau says:

- Explains the distrust that minorities have for law enforcement compared to white country folk.
- Illustrates how in rural areas, law enforcement is more accountable and part of a tighter community.
- Mentions the ability to hold law enforcement accountable through the ballot box or the cartridge box.
- Points out that minority groups lack institutional power to hold unaccountable men with guns in law enforcement accountable.
- Draws parallels between the distrust felt towards law enforcement by minorities and white country folk towards agencies like ATF and BLM.
- Addresses the frequency of unjust killings and lack of accountability in law enforcement.
- Encourages taking action by getting involved in local elections to ensure accountability.
- Suggests starting at the local level by electing good police chiefs to prevent corruption in federal agencies.
- Advocates for people from different backgrounds to unite and address the common issue of unaccountable men with guns in law enforcement.
- Urges individuals to care enough to take action and work together to solve the problem.

# Quotes

- "We can hold them accountable one way or the other, the ballot box or the cartridge box."
- "It's unaccountable men with guns. We can work together and we can solve that."
- "We've got to start talking to each other. We got the same problems."

# Oneliner

Exploring the distrust towards law enforcement, Beau urges unity and accountability to address the common issue of unaccountable men with guns.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Elect good police chiefs in local elections to prevent corruption (suggested)
- Advocate for accountability in law enforcement by participating in local elections (implied)
- Start dialogues with individuals from different backgrounds to address common issues (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of the distrust towards law enforcement, suggestions for accountability through community action, and the importance of unity in addressing systemic issues.

# Tags

#LawEnforcement #Accountability #CommunityAction #Unity #MinorityDistrust