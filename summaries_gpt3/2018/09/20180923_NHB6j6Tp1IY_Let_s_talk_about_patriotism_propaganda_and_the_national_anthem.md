# Bits

Beau says:

- Responding to an op-ed story sent by two people who knew a woman attacked for kneeling during the national anthem.
- Expresses irritation and reads the author's bio, prompting a response.
- Criticizes the author for calling the woman a petulant child for kneeling, questioning his understanding of protests.
- Challenges the author on patriotism, pointing out that patriotism involves correcting the government when it is wrong.
- Recounts a story about patriotism being displayed through actions, not just symbols, and suggests seeking guidance from a seasoned individual on patriotism.
- Differentiates between nationalism and patriotism, condemning the author's behavior towards the woman.
- Ends by asserting his stand on patriotism and inviting the author to contact him for further discourse.

# Quotes

- "Patriotism is not doing what the government tells you to."
- "Patriotism is correcting your government when it is wrong."
- "There is a very marked difference between nationalism, which is what this gentleman is talking about and patriotism."
- "You can kiss my patriotic behind."
- "You think that it's just some silly answer from somebody who doesn't really know anything."

# Oneliner

Beau responds to an op-ed story criticizing a woman for kneeling during the national anthem, discussing the difference between nationalism and patriotism and inviting further discourse.

# Audience

Patriotic individuals

# On-the-ground actions from transcript

- Contact Beau for further discourse on patriotism (suggested)
- Seek guidance from experienced individuals on patriotism (implied)

# Whats missing in summary

The emotional impact and depth of the discourse on patriotism and nationalism.

# Tags

#Patriotism #Nationalism #Protest #Community #Opinion