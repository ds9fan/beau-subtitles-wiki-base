# Bits

Beau says:

- Beau wants to burn some shoes today because it's raining and he feels the need to burn something.
- He mentions not owning Nikes due to personal reasons but his son has a pair.
- Beau questions the significance of burning shoes as a form of protest against Nike.
- He suggests donating unwanted Nikes to shelters or thrift stores near military bases for those in need.
- Beau challenges the idea of burning symbols without considering the workers behind the products.
- He draws parallels between burning Nikes and burning the American flag as symbolic acts.
- Beau questions the integrity of those who are quick to disassociate with Nike over a commercial but ignore their history of unethical practices.
- He criticizes people who claim to value freedom but turn a blind eye to issues like sweatshops and slave labor.
- Beau concludes by pointing out the irony of loving the symbol of freedom more than actual freedom itself.

# Quotes

- "If you're that mad and you can't wear a pair of Nikes because of a commercial, take them and drop them off at a shelter."
- "You're loving that symbol of freedom more than you love freedom."
- "Y'all have a good day."

# Oneliner

Beau questions the logic behind burning symbols of protest and challenges the true meaning of freedom.

# Audience

Consumers, activists, ethical shoppers

# On-the-ground actions from transcript

- Donate unwanted shoes to shelters or thrift stores near military bases (suggested)
- Educate oneself on the ethical practices of companies before supporting them (implied)

# Whats missing in summary

The full transcript provides a thought-provoking reflection on performative activism and the true essence of freedom, encouraging individuals to reexamine their actions and beliefs.

# Tags

#Activism #ConsumerEthics #Freedom #Symbolism #EthicalPractices