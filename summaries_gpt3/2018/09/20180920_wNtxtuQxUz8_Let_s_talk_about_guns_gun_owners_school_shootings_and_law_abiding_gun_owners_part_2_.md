# Bits

Beau says:

- Gun control has failed due to being written by those unfamiliar with firearms, leading to ineffective measures.
- The term "assault weapon" is made up and lacks a concrete definition within firearms vocabulary.
- Bans on assault weapons focused on cosmetic features rather than functionality, rendering them ineffective.
- Banning high-capacity magazines is flawed as changing magazines can be swift, making the rush tactic infeasible.
- Efforts to ban specific firearms like the AR-15 could lead to more powerful weapons being used in its place.
- Banning semi-automatic rifles entirely is impractical, given their prevalence and ease of manufacture.
- Restrictions on gun ownership based on age, particularly raising the minimum age to 21, could be a more effective measure.
- Addressing loopholes in laws regarding domestic violence offenders owning guns could enhance safety measures.
- Legislation focusing solely on outlawing certain firearms may not address the root issues effectively.
- Beau stresses the need to look beyond laws and tackle deeper societal issues for a comprehensive solution.

# Quotes

- "Assault weapon is not in the firearms vocabulary."
- "A soldier can enlist in the Army, come home, and not be able to buy a gun."
- "Legislation is not the answer here in any way shape or form."

# Oneliner

Beau explains why gun control measures fail and suggests solutions like raising the minimum gun-buying age and addressing domestic violence loopholes, pointing out the limitations of legislation.

# Audience

Advocates, Activists, Legislators

# On-the-ground actions from transcript

- Raise awareness about loopholes in laws regarding domestic violence offenders owning guns (suggested).
- Advocate for raising the minimum age for purchasing firearms to 21 (suggested).

# Whats missing in summary

Full context and depth of Beau's arguments and solutions can be better understood by watching the complete video. 

# Tags

#GunControl #AssaultWeapons #DomesticViolence #Legislation #CommunitySolutions