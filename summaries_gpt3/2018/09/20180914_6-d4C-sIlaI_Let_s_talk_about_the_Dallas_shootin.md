# Bits

Beau says:

- Expresses skepticism towards the official narrative of the shooting in Dallas, referencing experience with liars and motives.
- Raises concerns about the shooting being a potential home invasion due to new information about noise complaints.
- Points out the potential legal consequences in Texas for killing someone during a home invasion.
- Questions the lack of intervention or opposition within the police department regarding potential corruption and cover-ups.
- Connects the lack of justice in cases like this to the Black Lives Matter movement and the value placed on black lives.
- Criticizes the actions of the police in searching the victim's home posthumously for justifications.

# Quotes

- "There's two kinds of liars in the world."
- "I'm not a lawyer, but that sure sounds like motive to me."
- "If this is the amount of justice they get, they don't. They don't."
- "Not one cop has crossed the thin blue line to say that's wrong."
- "It doesn't matter what else you do."

# Oneliner

Beau questions the official narrative of a shooting in Dallas, pointing out potential motives and legal consequences, while criticizing police actions and lack of justice for victims, and connecting it to the Black Lives Matter movement.

# Audience

Community members, justice seekers, activists.

# On-the-ground actions from transcript

- Speak out against police corruption and cover-ups within your community (implied).
- Support movements like Black Lives Matter by advocating for justice and equality (implied).

# Whats missing in summary

The emotional impact and depth of analysis provided by Beau during his commentary.

# Tags

#Justice #PoliceCorruption #BlackLivesMatter #HomeInvasion #CommunityPolicing