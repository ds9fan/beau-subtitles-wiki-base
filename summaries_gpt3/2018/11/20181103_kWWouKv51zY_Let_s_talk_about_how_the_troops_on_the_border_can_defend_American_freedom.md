# Bits

Beau says:

- Recounts a story from the past about a general ordering a lieutenant to take a hill crawling with enemies, resulting in success without casualties.
- Explains the process of vague orders becoming specific on the ground and the importance of implementation.
- Talks about the unique chance for soldiers at the border to defend American freedom by upholding specific laws and rules of engagement.
- Warns against mission creep, using Afghanistan as an example, and the illegal request for U.S. soldiers to act as emergency law enforcement personnel.
- Emphasizes the need to stay within the law, especially regarding non-combatants, and the potential consequences for enlisted soldiers if rules are broken.
- Advises soldiers to keep their weapons safe and slung, not getting involved in illegal activities.
- Encourages soldiers to know the laws of war, the real rules of engagement, and not to follow vague or illegal orders.
- Urges those at the border or heading there to learn how to legally apply for asylum in the United States.
- Criticizes the political use of soldiers by higher-ups, warning about the misuse of trust and honor for political gains.
- Suggests requesting clarification on vague orders, following the law, and being prepared for a potential need for defense against illegal actions.

# Quotes

- "Order comes down, it's vague. Gets more specific until it gets to those guys on the ground. And they've got to figure out how to implement it."
- "You want to defend American freedom, you keep that weapon slung and you keep it on safe."
- "Honor, integrity, those things. It's bred into you, right?"
- "You wouldn't circumvent the law, earn that trust, keep that weapon slung and keep it on safe."
- "When that vague order comes down, you ask for clarification. Email. Clarification."

# Oneliner

A reminder to soldiers: implement vague orders wisely, defend American freedom by upholding laws, and beware of political misuse of your honor and uniform.

# Audience

Soldiers, Military Personnel

# On-the-ground actions from transcript

- Learn how to legally apply for asylum in the United States (suggested)
- Request clarification on vague orders and follow legal guidelines (implied)
- Keep weapons safe and slung, avoiding illegal activities (implied)

# Whats missing in summary

Importance of understanding and upholding legalities and regulations in military actions.

# Tags

#Soldiers #Military #DefendFreedom #LegalCompliance #AsylumApplications