# Bits

Beau says:

- Tells the legend of President Kennedy meeting with selected Green Berets in 1961, making them take an oath to fight back against a tyrannical government.
- Mentions the secrecy and lack of evidence surrounding the oath and the Green Berets' activities.
- Talks about the involvement of Green Berets in Kennedy's honor guard after his assassination.
- Raises questions about the existence of a Special Forces Underground and its investigation by the Department of Defense.
- Points out the annual visit by active-duty Green Berets to Kennedy's grave site for a brief ceremony.
- Connects Kennedy's criteria for fighting tyranny, including the suspension of habeas corpus, to current political considerations.
- Criticizes the idea of suspending habeas corpus as a threat to the rule of law and national security.
- Challenges the argument for suspending habeas corpus in dealing with migrants and asylum seekers.
- Urges people to understand the significance of habeas corpus and its potential removal from legal protections.
- Raises concerns about the erosion of the rule of law and the danger of unchecked presidential powers.

# Quotes

- "The suspension of habeas corpus is though, that is a threat to national security. It's a threat to your very way of life."
- "The suspension of habeas corpus is something that can't be tolerated."
- "The suspension of habeas corpus is an affront to everything that this country has ever stood for."
- "You need to look it up. You need to realize exactly what it means for that to be gone."
- "You can watch this country turn into another two-bit dictatorship or you can kill this party politics that you're playing."

# Oneliner

Beau raises concerns about the suspension of habeas corpus, linking it to a legend about Green Berets and urging people to understand its significance in preserving the rule of law and national security.

# Audience

Americans, Activists, Citizens

# On-the-ground actions from transcript

- Educate yourself and others on the significance of habeas corpus and its importance in upholding the rule of law (implied).
- Advocate for the protection of legal rights and civil liberties in your community and beyond (implied).
- Stay informed about political decisions that could impact fundamental rights and freedoms (implied).

# Whats missing in summary

The emotional weight and urgency conveyed by Beau regarding the potential threat of suspending habeas corpus and the need for active civic engagement and awareness to safeguard democratic principles.

# Tags

#HabeasCorpus #GreenBerets #CivilRights #PoliticalActivism #NationalSecurity #Democracy