# Bits

Beau says:

- Shares a Native American story of Matoaka, not the typical Thanksgiving narrative.
- Matoaka learned English, traded supplies, and was eventually kidnapped by colonists.
- Forced into marriage to escape captivity, she was taken to England as a showcase of "civilized savages."
- Matoaka died young, under suspicious circumstances—either poisoned or from disease.
- Debunks the romanticized tale of Pocahontas saving John Smith's life.
- Expresses how most Native stories in popular culture are fabricated and romanticized.
- Criticizes the prevalent portrayal of natives in media with the white savior narrative.
- Mentions a movie plot where a character leaves the tribe, becomes an FBI agent, and returns as a hero.
- References Leonard Peltier's unjust imprisonment by the FBI.
- Raises awareness about the misrepresentation of Native culture and myths in mainstream narratives.

# Quotes

- "Most of what you know of Native stories is false. It's completely false, completely made up."
- "A girl was kidnapped, held hostage, forced into marriage, which means raped, and then died an early death."
- "There's not a whole lot of stories about natives just being native."
- "It's a good time to bring it up and just kind of remember that most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."
- "Y'all have a nice night, it was just a thought."

# Oneliner

Beau uncovers the harsh reality behind the romanticized Native American narratives in mainstream media, urging reflection on fabricated tales and misrepresentations.

# Audience

History enthusiasts, educators, activists

# On-the-ground actions from transcript
- Research and share authentic Native American stories to counter misconceptions (suggested)
- Support Native activists and causes to raise awareness about misrepresentations in media (implied)

# Whats missing in summary

Deeper insight into the impact of misrepresentation on Native communities and the importance of amplifying authentic voices.

# Tags

#NativeAmerican #Misrepresentation #Colonialism #History #Awareness