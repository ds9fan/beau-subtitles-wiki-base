# Bits

Beau says:

- Correcting arguments against asylum seekers' entry after wild comments.
- Referencing U.S. Constitution, international treaties, and protocols on refugees.
- Explaining the legality of claiming asylum and entering a country.
- Addressing the argument about sponsoring immigrant families.
- Contrasting the focus on helping homeless vets with aiding asylum seekers.
- Critiquing the emphasis on money and questioning who will pay for refugee assistance.
- Comparing the cost of assisting asylum seekers to other government expenditures.
- Disputing claims that asylum seekers come for benefits and asserting they seek safety.
- Linking current immigration issues to past U.S. involvement in Central American countries.
- Challenging the notion of staying in a dangerous country rather than seeking asylum.
- Illustrating the scale of a tragedy like Syria happening in the U.S.
- Mocking internet tough guys and their unrealistic bravado.
- Offering a humorous take on potential survival scenarios.
- Acknowledging his own skills but critiquing those who boast about combat abilities.
- Condemning the idea of opening fire on unarmed individuals, especially women and children.
- Expressing readiness for challenging situations with a touch of sarcasm.

# Quotes

- "It's cheaper to be a good person."
- "Stop projecting your greed onto them. Maybe they just want safety."
- "Accept some responsibility for your apathy and stay and fix it."
- "I know killers, real killers. I don't know any that open fire on an unarmed crowd."
- "Your canteen still be cool because you got ice in your veins."

# Oneliner

Beau explains the legal and moral imperatives for supporting asylum seekers while challenging prevalent misconceptions about immigration and advocating for empathy and understanding.

# Audience

Advocates for humane immigration policies.

# On-the-ground actions from transcript

- Contact Office of Refugee Resettlement to offer support for asylum seekers (suggested).
- Advocate for increased funding for refugee assistance programs (implied).
- Educate others about the legal rights of asylum seekers and refugees (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of legal, moral, and practical arguments in support of asylum seekers, along with a scathing criticism of anti-immigrant sentiments and internet bravado.

# Tags

#AsylumSeekers #Immigration #LegalRights #Refugees #CentralAmerica