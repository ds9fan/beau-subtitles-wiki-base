# Bits

Beau says:

- Police militarization is a critical issue that affects communities, leading to concerns about the "warrior cop" mentality.
- The misconception that being a warrior means being a killer is problematic and has resulted in a focus on militarizing law enforcement.
- The shift towards viewing law enforcement as strictly enforcing the law, rather than protecting the public, raises moral questions for officers.
- Enforcing unjust laws, such as those criminalizing feeding the homeless, can make a cop complicit in wrongdoing.
- Propaganda within law enforcement, including inflated statistics on officer deaths, contributes to a heightened sense of danger and leads to hasty decision-making.
- The belief in a "war on cops" is not grounded in reality, yet it influences the desire for militarization and warrior-like behavior among law enforcement.
- The use of military equipment like flashbangs and MRAPs by SWAT teams is excessive and often lacks proper training, leading to dangerous outcomes.
- Lack of intelligence work before operations results in SWAT teams making deadly mistakes, as seen in cases of wrong addresses and unnecessary force.
- The discrepancy between perceived and actual capabilities of SWAT teams can have lethal consequences when faced with real resistance.
- Beau advocates for a reevaluation of law enforcement's role as public servants and protectors rather than warriors.

# Quotes

- "If you're a cop in an area where feeding the homeless is illegal, you're a bad cop."
- "Being a warrior, everybody in the military is a warrior."
- "They've bought into their own propaganda."
- "There is no war on cops."
- "You're the weapon if you're a warrior."

# Oneliner

Beau dismantles the "warrior cop" mentality, exposing the dangers of police militarization and the consequences of prioritizing enforcement over public protection.

# Audience

Law enforcement reform advocates

# On-the-ground actions from transcript

- Reassess law enforcement's role as public servants and protectors rather than warriors (implied)
- Advocate for proper training and accountability in law enforcement (implied)
- Support efforts to demilitarize police forces and redirect resources towards community policing (implied)

# Whats missing in summary

A deeper understanding of the implications of police militarization and the need for a fundamental shift in law enforcement culture.

# Tags

#PoliceMilitarization #LawEnforcement #CommunityPolicing #WarriorCop #Accountability