# Bits

Beau Ginn says:

- Addressing tear gas used on children in the United States.
- Criticizing violence and justification over trivial matters.
- Asking how one might react if their child was tear-gassed.
- Challenging the perceived peaceful nature of certain groups.
- Mentioning heavy cartel presence in specific areas.
- Implying potential violence against Border Patrol agents.
- Questioning the nation's response to fear-mongering leadership.
- Criticizing tear-gassing children and the impact on the nation's image.
- Noting the troops' disapproval of government behavior.
- Speculating on the lack of violence along the border.

# Quotes

- "What are you going to do when it doesn't happen?"
- "We tear gassed kids like any other two-bit dictatorship."
- "Because they don't like seeing their government behave like Saddam's."
- "Sometimes, we tear gassed kids."
- "Says they're better than us because I can't say we didn't earn it."

# Oneliner

Beau Ginn questions the use of tear gas on children, challenges perceptions of peace, and criticizes fear-mongering leadership.

# Audience

Advocates for justice

# On-the-ground actions from transcript

- Challenge the normalization of violence against children (implied)
- Speak out against tear-gassing and violence (implied)
- Support non-violent solutions to conflicts (implied)

# Whats missing in summary

The emotional impact and intensity of Beau's condemnation towards violence and government actions.

# Tags

#TearGas #Violence #FearMongering #BorderPatrol #Peacebuilding