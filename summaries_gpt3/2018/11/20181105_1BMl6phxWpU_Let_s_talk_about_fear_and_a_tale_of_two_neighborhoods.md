# Bits

Beau says:

- Describes running supplies to Panama City after a hurricane to ensure people get what they need.
- Talks about visiting two neighborhoods: a rough one and a nice one.
- Shares an encounter in the rough neighborhood where gang members help distribute supplies efficiently.
- Narrates an incident in the nice neighborhood where a neighbor with a gun accuses him of looting.
- Expresses frustration at the fear that pervades society, leading to irrational behaviors.
- Criticizes the reliance on government and the lack of personal initiative in solving community problems.
- Encourages individuals to take action locally rather than waiting for governmental intervention.
- Emphasizes the importance of teaching children through actions, not just words.
- Argues against demonizing migrants fleeing Central America and calls for empathy and understanding.
- Concludes by urging people to take responsibility for solving problems instead of waiting for others.

# Quotes

- "It's fear. It's fear."
- "If you got a problem in your community, fix it, do it yourself."
- "We've got to stop being afraid of everything."
- "One of these migrants may be a murderer, maybe, there's 7,000 people, I'm sure somebody's done something wrong in that group."
- "It's up to you, it is up to you."

# Oneliner

Beau talks about fear, community action, and the importance of taking personal responsibility instead of relying on government or succumbing to irrational fears.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Support non-governmental organizations like Project Hope Incorporated by donating supplies or volunteering (suggested)
- Take initiative in your community by identifying and addressing local issues without waiting for government intervention (exemplified)

# Whats missing in summary

The full transcript provides a detailed account of Beau's experiences in contrasting neighborhoods, shedding light on societal fears and the importance of community action.