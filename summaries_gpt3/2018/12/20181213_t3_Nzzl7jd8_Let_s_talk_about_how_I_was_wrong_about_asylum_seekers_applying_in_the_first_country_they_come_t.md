# Bits

Beau says:

- Admits being wrong about asylum seekers applying for asylum in the first country they come to.
- Discovered a EU court ruling stating refugees must apply in the first member state they enter.
- Explains the Safe Third Country Agreement between the US and Canada.
- Emphasizes the importance of not getting legal information from memes.
- Clarifies that the treaty only applies to the US and Canada, not other countries.
- Mentions that Canada is considering scrapping the agreement due to the treatment of asylum seekers and refugees in the US.
- Trump tried to designate Mexico as a safe third country, but the attempt was laughed at globally.
- Points out the dangers and lack of safety standards in Mexico, making it unsuitable as a safe third country.
- Expresses astonishment at the widespread belief that asylum seekers must apply in the first country they arrive in.
- Concludes by stressing the importance of knowledge and not relying on memes for legal information.

# Quotes

- "Knowledge is power. Don't get your legal information from memes."
- "People that know immigration law were so dumbfounded by this widespread belief."
- "Unless you're moving along the U.S.-Canadian border. That's the only place this treaty applies."

# Oneliner

Beau admits being wrong, sheds light on asylum laws, and warns against relying on memes for legal knowledge, revealing the limited scope of the Safe Third Country Agreement.

# Audience

Legal enthusiasts, asylum advocates

# On-the-ground actions from transcript

- Read and research international treaties and laws (suggested)
- Educate others on the limitations of the Safe Third Country Agreement (implied)

# Whats missing in summary

Understanding the limitations of the Safe Third Country Agreement and the importance of seeking accurate legal information beyond social media memes.

# Tags

#AsylumSeekers #SafeThirdCountryAgreement #LegalInformation #ImmigrationLaw #KnowledgeIsPower