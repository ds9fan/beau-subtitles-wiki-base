# Bits

Beau says:

- Recounts a story from the Vietnam War, referencing a Viet Cong trooper who continued fighting despite severe injuries by strapping a bolt to himself.
- Mentions the iconic photo from the Battle of Saigon where American troops showed humanity by giving water to their opposition.
- Criticizes the inhuman actions of Border Patrol agents who dump out water left by volunteers for migrants crossing dangerous routes.
- Compares the Border Patrol agents' actions to torturing videos posted by teenagers and questions their morality.
- Points out that deterrence methods like dumping water won't stop people seeking safety, freedom, and opportunities.
- Urges Border Patrol agents to quit their jobs, as they will eventually be held accountable for their actions.
- Emphasizes that blindly following orders is not an excuse, as history has shown that defense doesn't hold up.

# Quotes

- "You proud of yourself?"
- "Your badge is not going to protect you from that."
- "Just following orders isn't going to cut it."

# Oneliner

Beau recounts a Vietnam War story, criticizes Border Patrol agents for inhuman actions, and urges them to quit before being held accountable.

# Audience

Border Patrol agents

# On-the-ground actions from transcript

- Quit your job at Border Patrol (implied)

# Whats missing in summary

The emotional impact of Beau's storytelling and his powerful condemnation of inhumane actions by Border Patrol agents.

# Tags

#VietnamWar #Humanity #BorderPatrol #Migrants #Accountability