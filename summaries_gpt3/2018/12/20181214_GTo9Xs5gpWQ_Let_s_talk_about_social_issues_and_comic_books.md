# Bits

Beau says:

- Using Elf on the Shelf to address social issues has sparked anger and controversy on social media, particularly among those who are critical of feminists addressing social issues.
- Beau shares his perspective on how comic books like Punisher, G.I. Joe, X-Men, and Superman have always addressed social issues, despite some fans missing the underlying themes.
- He points out that even seemingly masculine-themed comics like Punisher and G.I. Joe actually tackle social issues like PTSD, feminism, and political commentary.
- Beau explains how characters in these comics represent deeper themes such as trauma, feminism, and societal commentary.
- He contrasts the strong feminist undertones in G.I. Joe with the perception that comics should not address social issues.
- Beau challenges the idea that addressing social issues in comics is a recent phenomenon, suggesting that it has always been present but may have gone unnoticed by some fans.
- The backlash against using Elf on the Shelf for social commentary is juxtaposed with the lack of empathy towards real-life stories of hardship, drawing attention to the disparity in reactions.
- He brings attention to how two plastic characters, Libertad and Esperanza, have sparked more reaction than the real-life stories of individuals facing hardships at the border.
- Beau shares a true story behind characters Libertad and Esperanza, shedding light on the real struggles that inspired his use of Elf on the Shelf for social commentary.

# Quotes

- "Social issues have always been in comics. You were just too dense to catch it."
- "If you have a privilege for so long, and you start to lose it, it feels like oppression."
- "Two chunks of plastic have had more of an impact than the actual stories of people who underwent some pretty horrible things."
- "There is no tyrannical, feminist regime coming for you."
- "It's always been there. The reason you're noticing now is because the social issues being addressed are striking at the heart of the comic book industry's core audience which is white males."

# Oneliner

Beau lays bare the longstanding tradition of addressing social issues in comics, challenging fans to see beyond the surface and confront their privilege amid societal changes.

# Audience

Comic book enthusiasts

# On-the-ground actions from transcript

- Acknowledge and appreciate the underlying social issues addressed in comic books (implied).
- Engage in critical analysis of media content to understand deeper themes and messages (suggested).

# Whats missing in summary

Exploration of the nuanced social commentary embedded in popular culture and its reflection on societal norms and values.

# Tags

#Comics #SocialIssues #Feminism #Privilege #CriticalAnalysis #PopularCulture