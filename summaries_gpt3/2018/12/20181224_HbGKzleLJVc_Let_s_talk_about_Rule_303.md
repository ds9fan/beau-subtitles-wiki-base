# Bits

Beau says:

- Explains the origins of "Rule 303" among military contractors, originating from a historical incident involving a soldier using his rifle caliber as justification for actions.
- Emphasizes that "Rule 303" meant having the means at hand and therefore the responsibility to act, not might makes right.
- Provides examples of how different situations have applied the concept of "Rule 303," including military interventions and law enforcement responses.
- Criticizes the excuses made by law enforcement officers who fail to act in critical situations, such as the Parkland shooting.
- Argues that duty is not bounded by risk and that officers have a responsibility to act, especially in protecting children.
- Challenges the notion that equipment superiority should deter action, stressing the importance of fulfilling one's duty regardless of perceived risks.
- Questions the message sent by prioritizing self-preservation over protecting those in need.
- Urges school resource officers and law enforcement personnel to reconsider their roles if they are not willing to lay down their lives to protect others.

# Quotes

- "Rule 303 meant having the means at hand and therefore the responsibility to act, not might makes right."
- "Your duty is not bounded by risk. Your duty is not to get up and go to work every day. The guy at Hardee's has that duty."
- "They care about the students. That's it."
- "The gun makes the man. Don't you think that might be reinforcing the thought process of those kids that go into schools and shoot the place up?"
- "If you cannot truly envision yourself laying down your life to protect those kids, you need to get a different assignment."

# Oneliner

Beau explains the concept of "Rule 303" and challenges law enforcement officers to fulfill their duty to protect without excuses or prioritizing self-preservation over others.

# Audience

Law enforcement officers

# On-the-ground actions from transcript

- Reassess your commitment to protecting others if you are in law enforcement and prioritize self-preservation over duty (implied).

# Whats missing in summary

The full transcript provides a detailed examination of the concept of "Rule 303," critiquing law enforcement responses to critical situations and urging officers to prioritize protecting others over self-preservation.

# Tags

#LawEnforcement #Responsibility #Duty #Protect #Rule303