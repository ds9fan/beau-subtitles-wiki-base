# Bits

Beau says:

- Recounts an encounter with cops during a training session where one suggested hitting a suspect in the knee with a baton for being more effective.
- Shares his concern about the lack of response from other officers in the room when this suggestion was made.
- Describes a cop who reached out to him after watching a video on police militarization, admitting to not knowing certain critical concepts like time, distance, and cover.
- Explains the importance of time, distance, and cover in police work, citing examples like Tamir Rice and John Crawford where not applying this principle resulted in tragic outcomes.
- Talks about auditory exclusion in high-stress situations and how it can lead to misunderstandings and potentially dangerous actions by law enforcement.
- Advises cops to give suspects time to comply with commands and not resort to excessive force due to misinterpretations.
- Warns against conflicting commands among cops during apprehensions, which can escalate situations and lead to excessive force.
- Mentions the dangers of positional asphyxiation and the importance of understanding it to prevent unnecessary harm during arrests.
- Urges cops to wear proper armor and stay behind cover during shootouts to avoid harming others due to stress-induced mistakes.
- Shares a personal anecdote about the importance of understanding the spirit of the law over its letter and warns against enforcing unjust laws blindly.

# Quotes

- "Time, distance, and cover. That's exactly what it sounds like."
- "Auditory exclusion is the hearing counterpart to tunnel vision."
- "The fact that that cop exercised the spirit of the law, rather than the letter of the law, is the only reason he's alive."
- "The most dangerous people on the planet are true believers."
- "You need to stay away from ideologically motivated people."

# Oneliner

Beau shares insights on critical police training principles, like time, distance, and cover, addressing issues of excessive force and misunderstandings in law enforcement.

# Audience

Law Enforcement Officers

# On-the-ground actions from transcript

- Implement training sessions on critical concepts like time, distance, and cover (suggested).
- Advocate for proper armor and cover usage during operations (suggested).
- Educate fellow officers on auditory exclusion and its effects in high-stress situations (suggested).

# Whats missing in summary

Importance of understanding critical training principles and the potential consequences of not applying them in law enforcement scenarios.

# Tags

#PoliceTraining #LawEnforcement #AuditoryExclusion #UseOfForce #SpiritVsLetterOfLaw