# Bits

Beau says:

- Analyzing the internet tough guy as a symptom and analogy for American foreign policy.
- The internet tough guy's behavior stems from being insulated from consequences, much like American foreign policy.
- Americans can ignore horrible foreign policy because they are shielded by the military machine.
- The insulation from consequences leads to a disconnect from global issues, viewing them as unrelated.
- Addressing the mentality of not needing to help foreigners because one was born in the U.S.
- Exploring the notion that caring from the top income earners could drive global change.
- Money plays a pivotal role in influencing change due to the wealth in certain countries.
- Acknowledging that injustices often arise from massive corporations driven by consumer choices.
- Emphasizing that change starts with individual actions and consumer decisions.
- Stating that any revolution in today's interconnected world must be global to truly bring change.

# Quotes

- "When we see it on TV, we turn it off. That's happening somewhere over there."
- "Money makes the world go round, right?"
- "It does have to be somebody from here."
- "At this stage in the game, the way everything is interconnected, any revolution that is not global, it's not a revolution."
- "It's just a thought."

# Oneliner

Beau delves into the parallels between the internet tough guy and American foreign policy, addressing insulation from consequences and the role of individuals in driving global change.

# Audience

Global citizens

# On-the-ground actions from transcript

- Choose consciously where to spend money to influence corporate behavior (implied)
- Start making changes individually in consumption habits (implied)

# Whats missing in summary

The full transcript provides a comprehensive analysis of the internet tough guy phenomenon, drawing parallels to American foreign policy and advocating for individual responsibility in driving global change.

# Tags

#InternetToughGuy #AmericanForeignPolicy #GlobalChange #CorporateResponsibility #ConsumerChoices