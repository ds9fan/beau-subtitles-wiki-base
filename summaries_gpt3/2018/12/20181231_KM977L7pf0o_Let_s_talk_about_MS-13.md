# Bits

Beau says:

- MS-13 has become a boogeyman, leading to calls for building a wall.
- Started in California among Salvadoran refugees as a street gang for protection.
- Civil war in El Salvador ended, and the US sent gang members back.
- The US military trained Ernesto Deris, turning MS-13 into a criminal enterprise.
- The School of Americas, linked to many atrocities, could be where Deris was trained.
- MS-13 is an American creation due to US foreign policy at every step.
- Beau questions the need for a wall and suggests curbing foreign policy instead.
- Criticizes the lack of scrutiny on those trained by the US military for violence.
- Urges stopping the problem at its source to prevent violence and fear.
- Beau advocates for a shift in approach to address the root causes of issues like MS-13.

# Quotes

- "MS-13 is an American creation, not just in the sense that it was founded here, but in the sense that every step of the way, it was American foreign policy that built it."
- "Tell me again why this is the reason we need a wall?"
- "Maybe a better idea would be to curtail our foreign policy instead of building a wall."
- "You never hear them calling for strict scrutiny or vetting of those people that the U.S. military trains to murder, torture, assassinate, and dominate a community by fear."
- "Stop the problem before it starts."

# Oneliner

MS-13, a product of American foreign policy, questions the need for a wall and advocates curbing violence at its source.

# Audience

Policy makers, activists, concerned citizens

# On-the-ground actions from transcript

- Advocate for changes in foreign policy to address root causes of issues like MS-13 (implied)
- Support organizations working to prevent violence and support communities affected by gang activity (suggested)

# Whats missing in summary

The full transcript provides a detailed historical context and analysis behind the creation and evolution of MS-13 through American foreign policy decisions. Watching the full video can offer a deeper understanding of the interconnected factors contributing to the gang's rise.

# Tags

#MS-13 #AmericanForeignPolicy #Immigration #ViolencePrevention #CommunityPolicing