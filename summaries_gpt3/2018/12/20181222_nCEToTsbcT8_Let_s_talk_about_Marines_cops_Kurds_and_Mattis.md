# Bits

Beau says:

- The theme of duty is prevalent in recent news, particularly with the ruling from a federal court in Florida stating that law enforcement has no duty to protect individuals.
- General Mattis's resignation is believed to be driven by his strong sense of duty, as described by other Marines who worked with him.
- Mattis's disagreement with President Trump on withdrawing from Syria stems from his belief that it leaves allies, like the Kurdish people, vulnerable.
- The Kurdish people, an ethnic minority spread across countries like Turkey, Syria, Iraq, and Iran, are seen as being left without support once again by the U.S.
- The U.S. historically has not supported Kurdish independence due to geopolitical reasons and the Kurds' potential to destabilize the region.
- The imminent U.S. withdrawal from Syria may present an uncertain but possibly opportune moment for Kurdish independence efforts.
- Law enforcement, as per existing case law, is not mandated to protect individuals, even in extreme scenarios like mass shootings.
- Beau underlines the idea that law enforcement is part of a government meant to control rather than protect citizens.
- Despite some officers' acts of heroism, there is no legal obligation for law enforcement to protect individuals.
- Beau concludes by urging people to be mindful of the government's role and the implications of legislation that may weaken citizens' ability to protect themselves.

# Quotes

- "Law enforcement is not there to protect you. We do not live under a protectorment."
- "Government is there to control you, not protect you."
- "Do some cops save lives? Yes, but they do it in spite of the law, not because of it."

# Oneliner

Beau delves into the concept of duty through the lens of recent events, from Mattis's resignation to the inherent lack of obligation for law enforcement to protect citizens, urging a reevaluation of governmental roles and citizen empowerment.

# Audience

Citizens, activists

# On-the-ground actions from transcript

- Connect with local community organizations for support and advocacy (implied)
- Educate others about the true role of law enforcement in society (implied)
- Support legislation that empowers citizens to protect themselves (implied)

# Whats missing in summary

The full transcript provides a comprehensive exploration of duty in the context of recent events, shedding light on the complex dynamics between government control and citizen protection.

# Tags

#Duty #LawEnforcement #Government #CitizenEmpowerment #KurdishIndependence