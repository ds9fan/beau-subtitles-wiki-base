# Bits

Beau says:

- Explains the concept of globalism and how it's often used derogatorily, especially by nationalists.
- Mentions that some extremists use "globalist" as a code word for being Jewish.
- Describes globalism as a potential new world order with a single government ruling over everyone.
- Compares nationalists to globalists, stating they both aim to control and motivate people through different means.
- Criticizes the idea of representative democracy and questions if politicians truly represent the public.
- Points out that both globalism and nationalism divert people from holding those in power accountable.
- Expresses his stance of not being a nationalist or a globalist, advocating for better ways to govern the world.
- Defines nationalists as low-ambition globalists who seek power within their own corner of the world.

# Quotes

- "All a nationalist is, is a low-ambition globalist."
- "It's a method of keeping you kicking down, keeping you in your place."
- "I am neither. I'm not a nationalist. I am not a globalist."
- "Just my opinion there. There are some pretty smart folks out there."
- "You're just being played, and it's turned into a buzzword."

# Oneliner

Beau breaks down globalism vs. nationalism, exposing them as tools to manipulate and divert attention from true accountability, advocating for a better world governance approach.

# Audience

Critical Thinkers

# On-the-ground actions from transcript

- Challenge derogatory terms and stereotypes in your community (suggested).
- Question politicians' accountability and representation (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of globalism and nationalism, urging viewers to question power structures and strive for better governance models.

# Tags

#Globalism #Nationalism #PowerStructures #Accountability #Governance