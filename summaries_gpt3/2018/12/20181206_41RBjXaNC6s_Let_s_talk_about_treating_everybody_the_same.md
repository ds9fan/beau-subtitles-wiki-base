# Bits

Beau says:

- Advocates for treating everybody fairly rather than the same to build a just society.
- Recounts a personal story about the director of photography for Olin Mills and the realization that treating everybody the same isn't always fair.
- Mentions McNamara's morons, illustrating the consequences of treating everyone the same during the Vietnam War.
- Shares a personal aversion to Arab males due to a cultural difference in personal space.
- Points out the challenge of promoting fair treatment in the U.S. due to a lack of education and understanding of other cultures.
- Emphasizes the importance of learning about others' backgrounds to treat everybody fairly.

# Quotes

- "The goal shouldn't be to treat everybody the same. It should be to treat everybody fairly."
- "Doesn't take you long to realize treating everybody the same is not treating everybody fairly."
- "We demonize education. We demonize learning about other cultures, other religions, whatever."
- "In order to treat everybody fairly, you have to know where they're coming from."
- "We demonize education. And it's something we need to work on."

# Oneliner

Beau advocates for treating everybody fairly over the superficial equality of treating them the same, pointing out the necessity of understanding and embracing cultural differences for true fairness.

# Audience

Americans

# On-the-ground actions from transcript

- Learn about different cultures and religions to understand and treat others fairly (implied).
- Advocate for education and cultural understanding in communities (implied).

# Whats missing in summary

The full transcript provides a deeper exploration of the importance of education, cultural understanding, and fairness in treating others, offering valuable insights into creating a just society.

# Tags

#Fairness #CulturalUnderstanding #Education #SocialJustice #Equality