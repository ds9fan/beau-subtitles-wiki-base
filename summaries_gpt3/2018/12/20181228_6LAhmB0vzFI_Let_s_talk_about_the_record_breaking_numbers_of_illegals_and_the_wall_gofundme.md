# Bits

Beau says:

- Border Patrol uses the number of border apprehensions as a gauge for the influx of immigrants, which is flawed due to technological advancements.
- Despite claims of record-breaking illegal immigration, the numbers are actually at a 40-year low.
- Trump's immigration policies may not be the reason for the low numbers, as they have remained relatively consistent for the past eight years.
- The US is no longer a beacon of freedom and critical thinking, electing rulers instead of leaders.
- The government's plan to spend $5 billion on a wall to address a non-existent problem is irrational.
- Lack of critical thinking is evident as people donate to the GoFundMe for the wall without understanding government processes.
- Beau humorously suggests starting a GoFundMe to audit the funds raised for the wall through the IRS.
- Trying to influence politicians through private donations is concerning and hints at fascism.
- Beau challenges the idea of the wall being effective, pointing out flaws in the plan and how smugglers could adapt.
- The fear-mongering around issues like MS-13 is used by politicians to manipulate public opinion and gain support for ineffective policies.

# Quotes

- "We are people who elect rulers instead of leaders."
- "We are going to spend five billion dollars to address a problem that doesn't exist."
- "It shows, one, you don't know how the government works, and two, there's actually a word for a blending of private business interests and the government. It's called fascism."
- "So the wall, assuming it works, will actually pump more money into a criminal cartel that is destabilizing our next-door neighbor."
- "Nobody in DC is going to look out for you."

# Oneliner

Beau challenges the irrationality of spending $5 billion on a border wall for a non-existent issue, criticizing the lack of critical thinking in politics and society.

# Audience

Critical Thinkers, Voters, Activists

# On-the-ground actions from transcript

- Start educating yourself on government processes and policies (suggested)
- Support and donate to organizations working towards comprehensive immigration reform (implied)
- Engage in critical thinking and fact-checking before supporting political initiatives (suggested)

# Whats missing in summary

The full transcript provides a detailed breakdown of the flaws in the proposed border wall and challenges the lack of critical thinking in politics and society.

# Tags

#Immigration #BorderWall #CriticalThinking #Government #Policy