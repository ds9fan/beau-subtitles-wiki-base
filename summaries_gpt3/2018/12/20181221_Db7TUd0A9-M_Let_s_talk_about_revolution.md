# Bits

Beau says:

- Addressing the comments section's interest in revolution, especially after the Yellow Vest Movement, and activists in the US seeking a revolution.
- Defines a revolution as not just regime change but bringing a new idea to the table, like changing forms of government.
- Notes that Western civilization's trend in revolutions is to decentralize power and give more power to more people.
- Warns about the consequences of a violent revolution in the United States, citing ethnic, racial tensions, and cultural divides.
- Mentions that the US is not self-reliant anymore and how a revolution interrupting transportation could lead to food and supply shortages.
- Emphasizes that violent revolution isn't the only way to change minds; focusing on new ideas is key to the future.
- Talks about the potential devastating impact of a violent revolution in the US and the misguided belief in readiness for such a scenario.

# Quotes

- "A new form of government. Changing forms of government. Something like that."
- "In the United States, a violent revolution will be horrible."
- "You can put a bullet in it, or you can put a new idea in it."

# Oneliner

Beau delves into the implications of revolution, warning against violence in the US and advocating for new ideas to drive change.

# Audience

Activists, thinkers, citizens

# On-the-ground actions from transcript

- Prepare emergency supplies and have a plan in case of disruptions in transportation (implied)
- Engage in constructive dialogues and debates about new ideas for governance (implied)

# Whats missing in summary

Detailed examples and historical contexts mentioned by Beau to support his insights on revolutions and violence.