# Bits

Beau says:

- Dismisses the stereotype that everyone from the South hunts, noting he used to hunt but stopped due to early mornings.
- Distinguishes between hunters who eat what they kill and trophy hunters who go after exotic animals primarily for status.
- Criticizes wealthy trophy hunters who go on canned hunts, where guides do all the work and hunters simply take the shot.
- Points out that while some claim canned hunts support conservation efforts, the practice actually fuels a market for artifacts and poaching.
- Suggests investing the hunting money in local communities instead, promoting self-sufficiency and economic growth.
- Proposes alternatives like volunteering with anti-poaching forces or supporting advocates like Kristen Davis for animal conservation.
- Recommends supporting organizations like the David Sheldrick Wildlife Trust for elephant conservation efforts.

# Quotes

- "It's about proving you're a man."
- "Number one, that stops this from being just an advanced form of colonialism."
- "It's about killing. It's about proving you're a man."
- "So alternatively, if you are interested in this topic, my suggestion..."
- "They don't want aid, they want business."

# Oneliner

Beau dismisses hunting stereotypes, criticizes wealthy trophy hunters for fueling poaching markets, and suggests investing in local communities as an alternative to canned hunts.

# Audience

Conservationists, animal advocates

# On-the-ground actions from transcript

- Invest the hunting money in local communities to support self-sufficiency and economic growth (suggested).
- Volunteer with anti-poaching task forces to combat poaching (suggested).
- Support organizations like the David Sheldrick Wildlife Trust for elephant conservation efforts (suggested).

# Whats missing in summary

Beau's passion and detailed insights on the impact of canned hunting and the importance of community investment may be best understood by watching the full transcript.

# Tags

#Hunting #TrophyHunting #Conservation #CommunityInvestment #AnimalAdvocacy