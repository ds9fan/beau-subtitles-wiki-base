# Bits

Beau says:

- Introduces the topic of protest and human shields in the context of current events in the U.S.
- Describes protesters upset with the government for not upholding rights and laws, resorting to violating international norms.
- Recalls a historical event, the Boston Massacre, where protesters were met with violence from militarized forces.
- Compares the recent events to the Boston Massacre, pointing out the use of human shields and the potential for harm.
- Criticizes those cheering on violence against protesters, questioning their support for undermining the Constitution.
- Raises concerns about the immoral act of using human shields and opening fire on unarmed civilians.
- Expresses disbelief at the debate surrounding these actions and advocates for upholding moral standards.
- Condemns betraying the values that America supposedly stood for and calls for reflection on the current state of the country.

# Quotes

- "You do not punish the child for the sins of the father."
- "You do not open fire on unarmed crowds."
- "That's America. That's how we make America great, betraying everything that this country stood for."

# Oneliner

Beau addresses the use of human shields in protests, drawing parallels to historical events and criticizing the immoral actions of opening fire on unarmed civilians.

# Audience

Activists, Protesters, Advocates

# On-the-ground actions from transcript

- Stand against the use of human shields in protests (implied)
- Advocate for non-violent methods of crowd control (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the current state of protests and the use of human shields, urging viewers to reconsider supporting actions that betray moral values.