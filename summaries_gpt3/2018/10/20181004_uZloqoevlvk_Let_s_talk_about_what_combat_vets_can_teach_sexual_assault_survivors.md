# Bits

Beau says:

- Admits feeling like a psychologist with overwhelming inbox messages.
- Acknowledges the benefit of sharing tough experiences.
- Advises women not to date combat vets due to detachment, fear of intimacy, and emotional issues.
- Points out the traumatic stress experienced by sexual assault survivors and combat vets.
- Questions the double standard of stigma towards dating sexual assault survivors.
- Challenges the notion of being "damaged goods" and unworthy of love.
- Criticizes the stigma around sexual assault survivors compared to combat vets.
- Condemns the concept of marking or branding women based on past experiences.
- Encourages women not to internalize feelings of being damaged goods.
- Shares a personal story illustrating the impact of trauma on memory and experiences.

# Quotes

- "You are not unclean. You are not damaged goods. You are certainly not unworthy of being loved."
- "We are all damaged goods in some way."
- "There's nothing wrong with you."
- "It's idiotic."
- "It is amazing what trauma can do to your memory."

# Oneliner

Beau addresses the stigma around dating sexual assault survivors, comparing their experiences to combat vets and challenging the concept of being "damaged goods."

# Audience
Survivors and Allies

# On-the-ground actions from transcript

- Support survivors by listening and validating their experiences (implied).
- Challenge stigmas and double standards around dating survivors of sexual assault (implied).
- Educate others on the similarities in emotional responses between combat vets and sexual assault survivors (implied).

# Whats missing in summary

The full transcript delves into the emotional impact of trauma on individuals and challenges societal stigmas surrounding survivors of sexual assault, urging for empathy and understanding.

# Tags

#CombatVets #SexualAssaultSurvivors #Stigma #Trauma #Empathy