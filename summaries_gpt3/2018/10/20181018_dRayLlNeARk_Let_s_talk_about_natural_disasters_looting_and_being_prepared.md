# Bits

Beau says:

- Updates internet audience on his situation after being caught in a hurricane.
- Expresses gratitude for the comments and reassures everyone that he and his loved ones are safe, despite the aftermath of the hurricane.
- Addresses the issue of looting after natural disasters and criticizes law enforcement's response.
- Points out the media's biased portrayal of looting, especially concerning different races.
- Criticizes law enforcement's decision to shut down areas due to looting threats, preventing volunteers from helping those in need.
- Questions the priorities of law enforcement when it comes to protecting property over saving lives during a disaster.
- Acknowledges the Florida National Guard's quick and effective response to the hurricane.
- Urges people to be prepared for emergencies by assembling a basic emergency kit with essentials like food, water, and medical supplies.
- Provides resources and links for viewers to create their emergency kits at various budget levels.
- Emphasizes the importance of being prepared for emergencies without going overboard or breaking the bank.

# Quotes

- "In the event of a natural disaster, protecting the inventory of some large corporation should pretty much be at the bottom of any law enforcement agencies list."
- "You can't count on government response. You can't."
- "Simple things, you know, little flashlights, stuff like that, just have it all in one spot."
- "We're not talking about building a bunker and preparing for doomsday."
- "Please take the time to read it, to put one of those bags together."

# Oneliner

Beau updates on hurricane aftermath, criticizes law enforcement's response to looting, praises Florida National Guard, and urges viewers to prepare emergency kits for survival.

# Audience

Community members

# On-the-ground actions from transcript

- Assemble an emergency kit with essentials like food, water, fire supplies, shelter, medical supplies, and a knife (suggested).
- Put together emergency bags for different budget levels (suggested).

# Whats missing in summary

The full transcript provides more details on Beau's personal experience during the hurricane aftermath and offers practical advice on preparing for emergencies.

# Tags

#Hurricane #EmergencyPreparedness #LootingAfterDisasters #CommunityResponse #NaturalDisasters