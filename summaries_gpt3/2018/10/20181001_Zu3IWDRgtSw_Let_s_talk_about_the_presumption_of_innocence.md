# Bits

Beau says:

- Comments on the presumption of innocence in relation to Kavanaugh and the job interview.
- Notes Kavanaugh's support for law enforcement to stop individuals on the street.
- Mentions Kavanaugh's support for warrantless searches by the National Security Agency.
- Points out Kavanaugh's involvement in the indefinite detention of innocent people.
- Criticizes Kavanaugh for apparently lying during his confirmation hearings.
- Emphasizes the importance of the presumption of innocence in the American justice system.
- Criticizes Kavanaugh for undermining the Fourth Amendment.
- Talks about the dangers of party politics and signing away others' rights.
- Warns about the potential consequences of supporting Kavanaugh's nomination based on party affiliation.
- Urges people to be aware of the implications of supporting candidates without understanding their rulings.

# Quotes

- "It's almost like they aren't familiar with his rulings."
- "You've bought into bumper sticker politics and you're trading away your country for a red hat."

# Oneliner

Beau covers the presumption of innocence, Kavanaugh's support for law enforcement overreach, and the dangers of party politics.

# Audience

Voters, Constitution advocates.

# On-the-ground actions from transcript

- Know the background of candidates before supporting them (implied).

# Whats missing in summary

Deeper analysis of how party politics can compromise constitutional rights.

# Tags

#PresumptionOfInnocence #Kavanaugh #PartyPolitics #Constitution #Rights