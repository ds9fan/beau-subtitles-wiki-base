# Bits

Beau says:

- Fathers posting photos with guns next to their daughters' boyfriends is a common social media joke, but it sends a harmful message.
- The joke implies that without the threat of a firearm, the boyfriend will force himself on the daughter, which should not be a possibility.
- Waving a firearm around also suggests a lack of trust in the daughter's judgment and autonomy.
- The message behind the photo is that the daughter needs a man to protect her and that she lacks the capability to defend herself.
- This tradition of intimidating boyfriends with guns is based on a dated concept of property rights and is ultimately disempowering for the daughter.
- Beau questions the need for a firearm to intimidate a teenage boy, stating that it says more about the father than the boyfriend.
- Guns should not be used as props or toys; their presence should signal a serious intent to destroy or kill.
- Beau shares a personal experience where he had to address a boyfriend's behavior without needing a firearm.
- Fathers should focus on empowering their daughters, building trust, providing information, and fostering open communication.
- Beau urges fathers to reconsider perpetuating this joke and to only bring out guns when absolutely necessary.

# Quotes

- "It's time to let this joke die, guys. It's not a good look. It's not a good joke."
- "Guns should not be used as props. If you're breaking out a gun, you should be getting ready to destroy or kill something."
- "She doesn't need a man to protect her. She's got this."

# Oneliner

Fathers intimidating their daughters' boyfriends with guns sends harmful messages about trust, autonomy, and empowerment.

# Audience

Fathers, Parents

# On-the-ground actions from transcript

- Have open and honest communication with your children about trust, autonomy, and safety (implied).
- Foster empowerment and independence in your children by trusting their judgment and capabilities (implied).
- Refrain from using guns as props or toys, and only bring them out when absolutely necessary (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of the harmful implications of fathers using guns to intimidate their daughters' boyfriends, urging for a shift towards empowering and trusting daughters rather than resorting to intimidation tactics.

# Tags

#Parenting #Empowerment #Trust #Communication #GunSafety