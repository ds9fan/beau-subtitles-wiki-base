# Bits

Beau says:

- President cutting off aid to Central American countries for not stopping people from coming to the United States is terrifying.
- The President believes it's the head of state's duty to prevent people from leaving their country.
- The wall along the southern border can also be used to keep Americans in.
- Beau warns about the slow creep of tyranny and the danger of losing freedom and liberty.
- Calling out those who support the President's actions as betraying their ideals for a red hat and slogan.
- Describing the wall as a prison wall that can trap people inside.
- Beau expresses concern about the implications of the President's statements on freedom and democracy.

# Quotes

- "That wall is a prison wall. It's not a border wall. It's gonna work both ways."
- "The face of tyranny is always mild at first and it's just now starting to smile at us."

# Oneliner

President's actions to cut aid for not stopping people from leaving their countries signal a dangerous erosion of freedom and democracy, with the wall serving as a potential prison barrier for Americans.

# Audience

Citizens, Activists

# On-the-ground actions from transcript

- Resist erosion of freedom: Stand up against policies threatening liberty (implied).
- Educate others: Share information about the potential consequences of oppressive measures (implied).

# Whats missing in summary

The emotional impact and urgency conveyed by Beau's speech.

# Tags

#Freedom #Democracy #Tyranny #BorderWall #Activism