# Bits

Beau says:

- Police can escalate any situation, even trivial ones, to the point of death.
- Jaywalking, a seemingly harmless act, can lead to fatal consequences.
- Non-compliance with police can result in violence or death, even for minor offenses.
- Calling the police on someone for insignificant reasons puts their life in jeopardy.
- Only call the police when the situation truly warrants a response of death.
- If nobody is being harmed, it's not a crime, even if it may be illegal.
- Patience with dealing with unnecessary police intervention varies based on race.
- Police involvement can escalate situations needlessly, especially for people of color.
- The potential consequences of involving the police should be carefully considered.
- Every call to 911 carries the possibility of a deadly outcome.

# Quotes

- "Every law is backed up by penalty of death."
- "If nobody's being harmed, it's not really a crime."
- "Don't call the law unless death is an appropriate response."
- "Calling the police on someone for insignificant reasons puts their life in jeopardy."
- "Every call to 911 carries the possibility of a deadly outcome."

# Oneliner

Police can escalate any situation to the point of death; only call them when it's truly necessary. Non-compliance with police can lead to violence or death, even for minor offenses. Be cautious in involving law enforcement, as every call to 911 carries the possibility of a deadly outcome.

# Audience

Community members

# On-the-ground actions from transcript

- Practice de-escalation techniques when faced with conflicts (implied)
- Educate yourself and others on alternatives to calling the police (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the implications of involving law enforcement and the disproportionate impact on marginalized communities.