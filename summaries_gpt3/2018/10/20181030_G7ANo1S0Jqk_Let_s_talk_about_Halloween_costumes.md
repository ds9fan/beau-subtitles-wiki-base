# Bits

Beau says:

- Addresses offensive Halloween costumes, including blackface and dressing up as Trayvon Martin or Adolf Hitler.
- Expresses lack of understanding for the shock when people face backlash for intentionally offensive actions.
- Points out that being offensive and edgy is not a substitute for having a sense of humor.
- Talks about how jokes about bullying and tormenting marginalized groups were considered acceptable in the past.
- Criticizes those who hide behind internet anonymity to spew hate and offensive remarks.
- Urges people to try being a good person instead of just being shocking or edgy.
- Stresses that while individuals have the right to dress and act however they want, others have the right to look down on them.
- Comments on the idea of political correctness and how it aims to change flawed ways of thinking.
- Mentions the current political climate but warns against mistaking it for a permanent change in culture.
- Concludes by stating that people will not accept or excuse harmful actions, even if they were meant as jokes.

# Quotes

- "Being offensive and edgy is not actually a substitute for having a sense of humor."
- "But you can't be mad when you suffer the consequences of your actions."
- "Bigotry is out. It's something that sane people have given up on."
- "Try being a good person. Like I said, you can do whatever you want."
- "People are going to remember it."

# Oneliner

Beau addresses offensive Halloween costumes, criticizes using edginess as humor, and warns against hiding behind internet anonymity to spread hate.

# Audience

Social media users

# On-the-ground actions from transcript

- Be a good person (exemplified)
- Refrain from making offensive jokes (exemplified)
- Speak out against harmful actions (exemplified)
- Challenge flawed ways of thinking (exemplified)

# Whats missing in summary

The full transcript provides additional context on the consequences of offensive behavior and the importance of societal change in rejecting bigotry.

# Tags

#Halloween #OffensiveCostumes #SenseOfHumor #PoliticalCorrectness #Consequences