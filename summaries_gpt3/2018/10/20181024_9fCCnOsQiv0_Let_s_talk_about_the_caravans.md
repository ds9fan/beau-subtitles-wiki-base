# Bits

Beau says:
- Caravans have been coming for 15 years, and they will continue due to US drug war policies and interventions in other countries.
- The current caravan is from Honduras, a country with a murder rate of 159 per 100,000, compared to Chicago's 15-20 per 100,000.
- Tax dollars fund assistance for migrants, costing about half a billion dollars annually.
- It's cheaper to assist migrants in getting on their feet than to detain them.
- Migrants travel in large groups for safety, similar to the principle of trick-or-treating in groups.
- Carrying other countries' flags allows migrants to connect with fellow countrymen during the journey.
- Wearing military fatigues is practical for durability during long journeys.
- Migrants are legally claiming asylum by arriving at ports of entry or within the country.
- Blaming migrants for seeking asylum overlooks the impact of US foreign policy and the drug war on their home countries.
- Beau challenges those advocating to turn migrants away by asking for one good reason to send them back to potential death, criticizing fear-based policies.

# Quotes

- "It's cheaper to be a good person."
- "Blaming the victim."
- "Give me one good reason to send these people back to their deaths."
- "You can't preach freedom and then deny it."
- "These people will die."

# Oneliner

Caravans fleeing violence will continue due to US policies; assisting migrants is cheaper than detention, and turning them away risks lives needlessly.

# Audience

Advocates, policymakers, activists

# On-the-ground actions from transcript

- Contact your representative to address how US foreign policy impacts other countries (suggested)
- Advocate for changes in drug war policies and intervention practices (suggested)

# Whats missing in summary

The emotional impact of blaming victims and the importance of advocating for humane treatment of migrants.

# Tags

#Caravans #USForeignPolicy #AsylumSeekers #HumanRights #Advocacy