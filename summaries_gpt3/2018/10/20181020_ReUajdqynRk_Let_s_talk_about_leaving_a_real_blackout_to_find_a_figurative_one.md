# Bits

Beau says:

- Beau is out for a drive to escape the noise of the chainsaw and trees, giving him a chance to scroll through the internet.
- Facebook and Twitter have censored independent news outlets opposed to government policies, alarming Beau.
- Beau differentiates between these outlets and Alex Jones, noting the dangerous nature of Jones' content.
- Many independent news outlets were shut down on Facebook and Twitter in one day, worrying Beau about the impact on free discourse.
- Carrie Wedler, a journalist Beau recommends, had her outlet and personal Twitter account banned in the censorship sweep.
- Outlets Beau works with were not affected, but he finds the censorship chilling as an independent network.
- Beau warns against comparing the censored outlets to Alex Jones and stresses that dissenting from government policies is not a reason for censorship.
- He points out that Facebook and Twitter are no longer platforms for free discourse and may be altering public discourse at the government's request.
- Beau encourages people to seek alternative social media networks as censorship may not stop with these outlets.
- He mentions platforms like Steam It and MeWe as alternatives where censored information may be found and shared.
- Tens of millions of subscribers were affected by the censorship, leading Beau to believe this may mark the end of free discourse on Facebook.
- Independent outlets like Anti-Media provide good reporting with accountability, something lacking in major corporate networks.
- Beau urges people to prepare for a shift away from Facebook to maintain access to diverse perspectives and information.

# Quotes

- "Facebook and Twitter are no longer free discussion platforms."
- "Be ready. Look for another social media network."
- "This is going to be the end of free discourse on Facebook."

# Oneliner

Beau warns against censorship of independent news outlets opposed to government policies and advocates for seeking alternative social media platforms to preserve free discourse.

# Audience

Social media users

# On-the-ground actions from transcript

- Find and join alternative social media networks like Steam It and MeWe to access and share censored information (suggested).
- Prepare to shift away from Facebook to maintain access to diverse perspectives and information (implied).

# Whats missing in summary

The full transcript provides a detailed insight into the censorship of independent news outlets on social media platforms and the implications for free discourse.