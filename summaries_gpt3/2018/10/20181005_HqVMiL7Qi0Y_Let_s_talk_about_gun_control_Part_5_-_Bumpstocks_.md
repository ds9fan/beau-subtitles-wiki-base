# Bits

Beau says:

- Explains how bump stocks work to mimic fully automatic fire.
- Mentions the inaccuracy of fully automatic fire.
- Recounts the infamous use of bump stocks in the Vegas mass shooting.
- Points out that bump stocks may increase a shooter's lethality in enclosed spaces like schools.
- Disputes the notion that bump stocks save lives during mass shootings.
- Analyzes the necessity of bump stocks under the Second Amendment's intent.
- Describes the different perspectives among gun enthusiasts and Second Amendment supporters regarding bump stocks.
- Talks about the concept of suppressive fire and its relevance to the Second Amendment.
- States his opinion that bump stocks are not necessary in the context of insurgency against the government.
- Expresses reservations about banning bump stocks but acknowledges potential concerns.

# Quotes

- "It's not going to save any lives."
- "A lot of guys believe that to exercise the intent of the Second Amendment, which is to fight back against the government if need be, you'd need to be able to lay down suppressive fire."
- "No man, nothing should be banned."
- "You're not gonna see me at a protest trying to make sure that a bump stock doesn't get banned."
- "Nine of them are gonna be complete people you probably don't want to hang around."

# Oneliner

Beau explains bump stocks, disputes their life-saving potential, and examines their necessity under the Second Amendment, ultimately questioning the need for a ban.

# Audience

Firearm enthusiasts, Second Amendment supporters

# On-the-ground actions from transcript

- Question the necessity of bump stocks in relation to the Second Amendment (implied).
- Engage in respectful and informed debates about firearm regulations (implied).

# Whats missing in summary

In-depth exploration of the implications of bump stocks on mass shootings and insurgency tactics.

# Tags

#GunControl #SecondAmendment #BumpStocks #MassShootings #FirearmRegulations