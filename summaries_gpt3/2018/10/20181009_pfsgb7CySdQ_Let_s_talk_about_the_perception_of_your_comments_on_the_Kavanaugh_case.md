# Bits

Beau says:

- Received a message from a young lady who is unable to face her father due to his comments, appreciating Beau's perspective.
- Urges men to reconsider their harmful comments and salvage relationships, rather than focusing on divisive topics like the Ford Kavanaugh case.
- Points out that one in five women have experienced sexual assault, and criticizes the common arguments used to doubt victims.
- Addresses the question of why victims wait to come forward, attributing it to fear of not being believed or facing consequences.
- Challenges the notion of victim blaming based on behavior, stating that regardless of circumstances, rape is never justified.
- Calls out the damaging impact of justifying inappropriate behavior, leading to underreporting of sexual assaults.
- Encourages men to have honest conversations with the women in their lives and apologize for harmful comments that perpetuate a culture of disbelief.

# Quotes

- "You don't get to rape her."
- "You've given them all the reason in the world to never come forward."
- "I got caught up in politics. I didn't care."

# Oneliner

Beau urges men to reconsider harmful comments, salvage relationships, and apologize for perpetuating a culture of disbelief towards sexual assault victims.

# Audience

Men and fathers

# On-the-ground actions from transcript

- Have honest and open conversations with the women in your life to understand the impact of harmful comments (suggested)
- Apologize for perpetuating a culture of disbelief and victim blaming (suggested)

# Whats missing in summary

The emotional impact and urgency conveyed by Beau's message can best be experienced by watching the full transcript.

# Tags

#GenderEquality #SexualAssault #Apology #Relationships #Conversations