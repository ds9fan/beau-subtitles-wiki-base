# Bits

Beau says:

- Beau addresses the importance of sound bites and context, using Kennedy's speech as an example.
- Kennedy's speech, often quoted for specific sound bites, is misunderstood without the full context.
- The speech was about the Soviet Union, not a vague conspiracy theory.
- The main aim of the speech was to encourage self-censorship among journalists, editors, and publishers.
- Beau criticizes the modern reliance on sound bites due to shortened attention spans from 24-hour news and social media.
- He points out the dangers of Twitter as a platform for substanceless communication.
- Beau questions the reasons behind U.S. involvement in Syria, attributing it to the U.S. starting the Civil War for ulterior motives.
- The motive may have been related to competing pipelines supported by different countries.
- Propaganda plays a significant role in shaping public perceptions and is on the rise.
- Beau urges viewers to look beyond sound bites and understand the interconnected nature of global events.

# Quotes

- "Context, it's really important."
- "If you want to understand what's going on in the world, you have to look beyond the sound bites."
- "Propaganda is coming into a golden age right now."
- "Most times the sound bite, just like in Kennedy's speech, it's the exact opposite of what's really being said and really being done."
- "Taking a whole bunch of information and getting down to what's actually important."

# Oneliner

Beau stresses the importance of context in understanding global events beyond misleading sound bites, cautioning against the rise of propaganda.

# Audience

Media Consumers, Critical Thinkers

# On-the-ground actions from transcript

- Question mainstream narratives (suggested)
- Research beyond headlines (exemplified)
- Analyze global events critically (implied)

# Whats missing in summary

The full transcript provides detailed insights on the manipulation of information through sound bites and the necessity of looking beyond them to grasp the true nature of global events.

# Tags

#Context #SoundBites #Propaganda #CriticalThinking #GlobalEvents