# Bits

Beau says:

- Viewers question his identity and credibility due to a contradiction between what he says and what they expect.
- People want to know his backstory to determine how much trust to place in his words.
- Beau rejects blind trust in authority figures and encourages fact-checking and critical thinking.
- He warns against blind obedience to authority, citing the dangers it poses.
- Beau mentions instances where people failed to fact-check, leading to disastrous consequences like the Iraq war.
- Referring to Milgram's experiments, Beau reveals how many individuals are willing to harm others under authority's influence.
- He stresses the importance of trusting oneself and thinking independently rather than relying solely on authority figures.
- Beau values dissenting opinions and encourages viewers to think for themselves.
- He believes that ideas should be judged based on their merit, not on the speaker's credentials.
- Beau concludes by advocating for fact-checking, trusting in personal judgment, and encouraging independent thinking.

# Quotes

- "That obedience to authority, that submission to authority. This person is an authority figure. They're a trusted source, so we're gonna believe what they say. It's not a good thing."
- "A lot of bad things have happened in history because people didn't do that."
- "Trust yourself. Trust yourself."
- "Ideas stand or fall on their own."
- "Don't trust your sources, trust your facts, and trust yourself."

# Oneliner

Beau warns against blind obedience to authority, advocates for critical thinking, and encourages viewers to trust themselves over external sources.

# Audience

Viewers

# On-the-ground actions from transcript

- Fact-check information before sharing or believing it (implied)
- Encourage critical thinking and independent judgment in your community (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of the dangers of blind obedience to authority and the importance of critical thinking in evaluating information.

# Tags

#ObedienceToAuthority #CriticalThinking #FactChecking #IndependentJudgment #TrustYourself