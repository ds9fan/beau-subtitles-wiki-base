# Bits

Beau says:

- Beau sarcastically toasts Justice Kavanaugh's ascension to the Supreme Court, celebrating that his life wasn't ruined.
- Expresses relief that Kavanaugh won't have to go back to coal mining, implying privilege and entitlement.
- Beau celebrates Kavanaugh's confirmation, claiming it will make America safe and free again.
- Mocks the idea of Kavanaugh being tied down by concerns like jury trials, lawyers, or charges before detention.
- Criticizes the notion that government surveillance and lack of accountability will keep people safe and free.
- Ridicules the efficiency of the National Security Agency and their ability to gather information without warrants.
- Sarcastically praises local law enforcement for being able to stop and detain people without reason.
- Irony in Beau's confidence that government power won't be abused, dismissing concerns about rights violations.
- Beau prioritizes safety and freedom over rights, implying a trade-off that undermines fundamental liberties.
- Mocks the lack of scrutiny on Kavanaugh's rulings, attributing his support to beating feminists and leftists at any cost.

# Quotes

- "We got him up there and now America is going to be safe. We're going to be free again."
- "There's no way that's going to be abused because the government, they're good people."
- "We're gonna be free now. Okay, so yeah, we had to give up some rights, and sell out our country, and tread the Constitution."
- "Congratulations, I'm very glad that your life wasn't ruined."
- "We beat the feminists, we beat the leftists, we got a man's man up there on the Supreme Court."

# Oneliner

Beau sarcastically praises Kavanaugh's Supreme Court confirmation, celebrating perceived safety and freedom while disregarding rights and accountability.

# Audience

Justice advocates

# On-the-ground actions from transcript

- Challenge unjust power structures (implied)
- Advocate for accountability and transparency in government actions (implied)

# Whats missing in summary

The full transcript provides a scathing criticism of celebrating Kavanaugh's confirmation while ignoring concerns about rights and accountability. Viewing the complete video offers a deeper understanding of the sarcasm and irony used to address serious issues.

# Tags

#Justice #Rights #Government #Accountability #Criticism