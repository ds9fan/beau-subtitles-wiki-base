# Bits

Beau says:

- Introduces a word-of-mouth story about a history professor in Japan before World War II.
- The professor predicts the war and moves his family to the secluded island of Iwo Jima.
- Despite having the potential to influence policy and possibly alter the course of the war, the professor chooses to remain silent.
- Beau underscores the significance of not remaining silent in the face of tyranny or oppression.
- Talks about the implications of remaining silent and how it equates to siding with those in power.
- Emphasizes the interconnectedness of power dynamics and communication in today's world.
- Criticizes the societal norm of avoiding topics like money, politics, and religion at the dinner table.
- Urges for open discussions and dialogues on critical issues instead of sticking to talking points.
- Stresses the importance of speaking out against tyranny to protect freedom.
- Encourages starting necessary dialogues among ourselves rather than relying on politicians.

# Quotes

- "When you remain silent in the face of tyranny or oppression, you have chosen a side."
- "Your silence is only helping them."
- "Tyranny anywhere is a threat to freedom everywhere."

# Oneliner

Beau shares a tale urging against silence in the face of tyranny, stressing the importance of speaking out for freedom. 

# Audience

Activists, Advocates, Citizens

# On-the-ground actions from transcript

- Initiate open dialogues with community members to address critical issues (suggested)
- Speak out against tyranny and oppression in your local context (implied)

# Whats missing in summary

The full transcript provides a thought-provoking narrative that challenges individuals to break the silence and have vital dialogues on pressing matters.

# Tags

#Tyranny #Freedom #SpeakOut #Silence #OpenDialogues