# Bits

Beau Ginn says:

- Beau is on the Mexican border with his militia commander, tasked with protecting America.
- They won't stop refugees on the Mexican side as it violates U.S., Mexican, and international law.
- Using guns to stop refugees could result in committing a felony and losing gun rights.
- Sending back individuals at gunpoint from the American side is also against the law and goes against their core principles.
- Beau acknowledges that individuals entering the U.S. have rights, including due process.
- They are essentially backing up the Department of Homeland Security (DHS) despite being labeled potential terrorists or extremists in a DHS pamphlet.
- Beau expresses concern that their actions could have negative consequences if they follow through with their rhetoric.
- Beau questions the wisdom of their militia commanders and suggests they might be strategizing with "4D chess" to manage optics.
- Despite portraying themselves as protecting America, Beau acknowledges the grim reality of potentially harming unarmed asylum seekers following the law.
- Beau ends with a casual greeting to a Colonel, leaving the situation open-ended.

# Quotes

- "We're down here on the border protecting America with our guns, ready to kill a bunch of unarmed asylum seekers who are following the law."
- "Hey, Colonel!"

# Oneliner

Beau Ginn on the Mexican border with militia, raising concerns about the legality and ethical implications of their actions while potentially compromising their group's secrecy and intentions.

# Audience

Militia members, border patrol supporters

# On-the-ground actions from transcript

- Question the ethics and legality of their actions (implied)
- Reassess the group's strategies and potential consequences (implied)

# Whats missing in summary

The urgency of addressing the potential harm caused by militia groups operating at the border.

# Tags

#BorderSecurity #Militia #Ethics #LegalIssues #Immigration #HumanRights