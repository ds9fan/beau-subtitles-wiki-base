# Bits

Beau says:

- President of the United States mocked sexual assault survivors on television, setting a shockingly low bar.
- The President's actions contribute to a concerted effort in the country to silence sexual assault claims.
- Referencing Trump's infamous "Grab-Em-By-The-P****" quote, Beau questions what it takes to stop such behavior.
- Beau contrasts Trump with Obama, acknowledging policy disagreements but noting the vast difference in character.
- He criticizes Trump for casting doubt on women coming forward with claims and talks about the presumption of innocence.
- Beau warns against dehumanizing perpetrators, stressing that they are people, not monsters.
- He draws a parallel between excusing Nazis as monsters and how society views rapists and assault predators.
- Beau underscores the danger of overlooking evil in plain sight due to political allegiance and nationalism.

# Quotes

- "There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim, and it's being led by the President of the United States."
- "Honor, integrity, are those words that come to mind when you think of President Trump?"
- "The scariest Nazi wasn't a monster. He was your neighbor."
- "Scariest rapist or rape-apologist, well they're your neighbor too."
- "y'all have a good night."

# Oneliner

President Trump's actions mock sexual assault survivors and contribute to a dangerous culture of silencing victims, while Beau contrasts his character with Obama and warns against dehumanizing perpetrators.

# Audience

Activists, Advocates, Voters

# On-the-ground actions from transcript

- Challenge efforts to silence sexual assault survivors (implied)
- Advocate for the presumption of innocence and fair treatment for survivors (implied)
- Refrain from dehumanizing perpetrators and understand the importance of accountability (implied)

# Whats missing in summary

The emotional intensity and depth of analysis present in the full transcript.

# Tags

#SexualAssault #Presidency #Dehumanization #Accountability #SocialJustice