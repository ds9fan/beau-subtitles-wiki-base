# Bits

Beau says:

- Talking about the new information released regarding an incident in Texas involving two Uvalde PD officers and a deputy.
- The officers had the chance to intercept a gun before it entered a school but didn't take the shot, fearing hitting a child.
- Raises concerns about the department's lack of transparency and hiring an outside law firm to avoid releasing footage.
- Emphasizes that simply having a weapon does not make someone an operator or shooter; extensive training is necessary.
- Criticizes the idea of arming teachers, pointing out the unrealistic expectations and lack of proper training.
- Mentions the unrealistic portrayal of firearms ownership and the misconception that having the right tool guarantees success.
- Expresses skepticism about arming teachers as a solution, citing the challenges even trained officers faced in similar situations.
- Urges people to understand the importance of training and proficiency in handling firearms before considering arming teachers.
- Questions the myths and misguided policies around arming teachers, stressing the need for realistic solutions.
- Encourages thoughtfulness and reflection on the implications of arming untrained individuals in critical situations.

# Quotes

- "Your policy shouldn't be guided by myths."
- "Without the training, the tool means nothing."
- "Arming teachers isn't the solution."

# Oneliner

Beau talks about the need for proper training and caution against arming unprepared individuals in critical situations, like teachers in schools.

# Audience

Policy makers, educators

# On-the-ground actions from transcript

- Ensure proper training for individuals in critical situations (implied).

# Whats missing in summary

The full video provides a more in-depth analysis of the risks and implications of arming untrained individuals in critical scenarios.

# Tags

#Texas #GunSafety #PolicyDebates #Training #SchoolSafety