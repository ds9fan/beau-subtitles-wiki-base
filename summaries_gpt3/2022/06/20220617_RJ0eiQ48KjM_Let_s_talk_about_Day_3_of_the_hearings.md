# Bits

Beau says:

- Committee established Trump knew his claims were false.
- Trump was aware plan to pressure Pence was illegal.
- Eastman, behind the plan, openly stated it required a violation of federal law.
- Ignorance of the law isn't an excuse; judges frown upon knowingly breaking the law.
- Pence was in actual danger; informant confirmed his capture could have been the end.
- Eastman was fine with potential riots if the plan succeeded.
- Eastman sought a pardon from Giuliani post-events.
- Eastman considered appealing decisions in Georgia on the 7th.
- Questions raised on why individuals who pushed back on the plan didn't go public.
- Raises concerns on whether Trump's endorsers could potentially overturn an election.

# Quotes

- "Ignorance of the law doesn't cut it."
- "If you actually succeed in this, you'll cause riots around the country."
- "At some point when the conversations turn to there's going to be riots around the country, it seems as though that should be widely broadcast."

# Oneliner

Committee reveals Trump's knowledge of false claims, illegal plans, endangerment of Pence, and post-event actions; raises questions on public disclosures and potential election interference.

# Audience

Congress, Investigators, Activists

# On-the-ground actions from transcript

- Contact Congress to push for transparency and accountability (suggested).
- Join or support organizations advocating for democracy and election integrity (implied).

# Whats missing in summary

Detailed analysis of testimonies and implications for future investigations.

# Tags

#Trump #CapitolRiots #ElectionInterference #Accountability #Democracy