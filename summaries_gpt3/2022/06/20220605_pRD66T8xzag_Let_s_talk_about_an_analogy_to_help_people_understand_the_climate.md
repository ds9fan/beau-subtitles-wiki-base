# Bits

Beau says:

- Addresses the current state and future projections regarding carbon dioxide levels.
- Emphasizes that human civilization has never experienced the current carbon dioxide levels before.
- Mentions the significant impact of high carbon dioxide levels on sea levels, vegetation, and crops.
- Points out that despite taking action, the situation will continue to worsen due to past milestones being surpassed.
- Compares those ignoring the environmental crisis to appeasers in the 1930s, suggesting that delaying action worsens the outcome.
- Calls for a global mobilization similar to World War II to address the environmental crisis.
- Stresses the urgency of taking action immediately and not waiting for tomorrow.
- Criticizes the denial of the crisis and the insistence on outdated technologies and dirty energy sources.
- Warns that siding with inaction is akin to being on the wrong side of history.

# Quotes

- "Human civilization has never seen this level."
- "The time for action was yesterday."
- "Going down this road is being an appeaser. It's siding with the bad guys."
- "You're on the wrong team, and history will judge you for it."
- "The longer it gets ignored, the worse it will be, just like back then."

# Oneliner

Beau addresses the urgent need for immediate action on escalating carbon dioxide levels, warning against delaying global mobilization to combat the environmental crisis.

# Audience

Global citizens

# On-the-ground actions from transcript

- Mobilize globally to address the environmental crisis (implied)

# Whats missing in summary

Further details on the historical context of appeasement in the 1930s and the potential consequences of inaction.

# Tags

#EnvironmentalCrisis #ClimateChange #UrgentAction #GlobalMobilization #History