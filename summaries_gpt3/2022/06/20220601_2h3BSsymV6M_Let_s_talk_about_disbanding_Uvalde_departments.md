# Bits

Beau says:

- Two departments in Texas, the regular police department and the police department for the school, have decided to stop cooperating with the Texas Department of Public Safety's investigation.
- The decision to stop cooperating came after the director of DPS criticized the delayed police entry into a classroom during a news conference.
- Beau asserts that the delayed police entry was clearly the wrong decision and contrary to protocol.
- He calls for the immediate disbandment of these departments to prevent creating unaccountable entities.
- Beau believes that allowing these departments to stonewall an investigation makes them believe they are above the law.
- He suggests that even if the departments start cooperating, disbandment is the only option for a positive outcome.
- Beau recommends that the Texas Department of Public Safety should treat the situation as a criminal investigation.
- He questions the accuracy of information being put out and suggests a probe within law enforcement may not be the right approach.
- Beau insists that the decisions made during the incident were clearly wrong, and there is no justification for believing otherwise.
- He warns that without action from the community, they will be under the control of an unaccountable department that doesn't believe it owes the public anything.

# Quotes

- "If this flies, if they are able to stonewall in an investigation in a situation like this, they are above the law."
- "There is no way you can look at what happened and think the right decisions were made."
- "If they don't believe they owe the public answers after what happened, they don't believe they owe the public anything."

# Oneliner

Two Texas departments have stopped cooperating with an investigation, prompting calls for immediate disbandment to prevent unaccountability and disregard for public safety.

# Audience

Community members, advocates

# On-the-ground actions from transcript

- Disband the departments immediately (implied)
- Advocate for treating the situation as a criminal investigation (implied)

# Whats missing in summary

The emotional urgency and call to action conveyed by Beau in the full transcript.