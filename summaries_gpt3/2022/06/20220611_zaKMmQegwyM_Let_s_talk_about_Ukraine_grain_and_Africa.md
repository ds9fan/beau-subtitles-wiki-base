# Bits

Beau says:

- Russia's invasion of Ukraine disrupted the world's grain supply, impacting Africa first, particularly the horn of Africa like Somalia.
- The disrupted grain supply combined with a drought in Africa has led to hundreds dying from famine with the situation expected to worsen.
- The cost of imperialism is often ignored, with wealthy and powerful countries not considering the consequences on poorer nations.
- The lack of aid in Africa is directly linked to Putin's invasion of Ukraine and the resulting disruption in the grain supply chain.
- The imperialist actions of powerful countries have far-reaching consequences on vulnerable populations in distant places.
- The coverage of Africa's crisis is tied to Putin's invasion, revealing the detrimental effects of global power dynamics.
- Aid efforts are hindered by the focus on money and other priorities, leaving many without food and facing dire circumstances.
- The foreign policy games played by powerful nations have real and devastating impacts on countries rarely acknowledged or visited.
- The situation in Africa serves as a stark reminder of the hidden costs of imperialism and global power struggles.
- Beau's reflection serves to shed light on the often overlooked repercussions of international conflicts on the most vulnerable populations.

# Quotes

- "The cost of imperialism is a cost that never gets talked about when wealthy countries play their little masters of the universe games."
- "The coverage that is going to come out of Africa is directly tied to Putin's invasion of Ukraine."
- "The lack of aid in Africa is directly tied to the disruption of the grain supply chain caused by that invasion."
- "The sad state of affairs and the reality of the imperialist nature of large powers."
- "These games that get played on the foreign policy scene impact countries that are seldom thought of and never visited."

# Oneliner

Russia's invasion of Ukraine disrupts grain supply, leading to famine in Africa, showcasing the hidden costs of imperialism on vulnerable nations.

# Audience

Policy Makers, Activists, Global Citizens

# On-the-ground actions from transcript

- Provide direct aid to famine-affected regions in Africa (suggested)
- Raise awareness about the impact of global power struggles on vulnerable populations (implied)
- Support organizations working to address food insecurity in Africa (exemplified)

# Whats missing in summary

The full transcript provides additional context on how international conflicts have tangible repercussions on distant and marginalized communities.

# Tags

#Russia #Ukraine #Imperialism #GlobalPower #Africa #GrainSupply #Famine #AidEfforts