# Bits

Beau says:

- Former President Trump sent a letter to the Pulitzer Board demanding they rescind the prize awarded to the Washington Post and the New York Times in 2018 for their coverage of possible Russian collusion.
- Trump's letter claims that the award was based on false and fabricated information published by the media outlets.
- The letter warns of potential litigation if the Board does not take action, akin to finding votes.
- Trump suggests the Board keep an eye on the ongoing criminal trial of Michael Sussman, a former attorney for the Clinton campaign.
- Sussman was acquitted, contrary to what Trump believed the Durham investigation was going to produce.
- Trump's actions are likened to Grandpa Simpson, writing letters to entities like the Pulitzer Board.
- The Durham investigation was a significant hope for Trump supporters to deflect from the Mueller investigation.
- Trump's aim was to discredit the origins of the investigations by claiming Sussman lied, but the jury found otherwise.
- Falling for his own propaganda, Trump's letter illustrates the danger of believing inaccurate stories.
- Being a Trump supporter amidst such revelations can be challenging.

# Quotes

- "The continuing publication and recognition of the prizes on the Board's website is a distortion of fact and a personal defamation."
- "Trump really is at this point, he is turning into Grandpa Simpson."
- "It can't be easy to be a Trump supporter right now."

# Oneliner

Former President Trump demands Pulitzer Board rescind awards based on false information, revealing dangers of believing propaganda.

# Audience

Political observers

# On-the-ground actions from transcript

- Monitor ongoing criminal trials (suggested)
- Stay informed about political developments (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis and commentary on former President Trump's actions and their implications for his supporters.

# Tags

#FormerPresidentTrump #Propaganda #PulitzerBoard #PoliticalAnalysis #BelievingFalseInformation