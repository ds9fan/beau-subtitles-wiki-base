# Bits

Beau says:

- Explains the Supreme Court's ruling on carrying things, clarifying misconceptions around permitless carry.
- Draws a comparison between may issue and shall issue terms using the example of driver's licenses.
- Details that states with may-issue licenses for concealed carry will have to rewrite their laws to make them shall-issue, with objective and quantified requirements.
- Mentions that the ruling doesn't mean anyone can walk in and get a license; requirements must still be met.
- Suggests that the ruling impacts only a few states, not a significant number.
- Indicates that the court's interpretation of "keep and bear" is broader than previous courts, potentially affecting other gun laws at state or local levels.
- Notes that bans on high capacity magazines may not survive this court's challenges.
- Speculates on potential broadening interpretations of "bear" by the court, hinting at rifle racks returning but not causing mass arming in New York.
- Concludes by stating that the ruling isn't as groundbreaking as some portray it, and states with may-issue laws can still regulate licenses objectively.

# Quotes

- "Most states have shall issue. This applies to, I want to say, five states."
- "The states that have may issue can still regulate concealed licenses. They just have to do it in a more objective way."
- "Their interpretation of bear leads me to believe that things like rifle racks like back in the day in pickup trucks, those may be coming back."
- "It's not going to be the outcome from this."

# Oneliner

Beau clarifies the Supreme Court's ruling on carrying things, debunking permitless carry misconceptions and explaining the shift from may issue to shall issue terms for concealed carry licenses, hinting at broader implications for gun laws.

# Audience

Legal enthusiasts, gun policy advocates

# On-the-ground actions from transcript

- Contact your state representatives to stay informed about any changes in concealed carry licensing laws (implied).
- Join local gun policy advocacy groups to understand and potentially influence future developments in gun legislation (implied).

# Whats missing in summary

Further details on the potential impact of the broader court interpretation of "keep and bear" on specific state or local gun laws.

# Tags

#SupremeCourt #GunLaws #ConcealedCarry #LegalInterpretation #PolicyImpact