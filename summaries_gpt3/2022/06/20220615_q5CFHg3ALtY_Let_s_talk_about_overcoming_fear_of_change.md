# Bits

Beau says:

- Addresses the idea of changing the system to combat institutional and systemic racism in the United States.
- Points out that the white working class may fear losing their comparative advantage over marginalized groups.
- Explains that the current system creates a false sense of security for the working class based on comparison rather than actual well-being.
- Emphasizes that kicking down on those with less power is not the solution to societal problems.
- Predicts a positive shift towards unity and improved conditions for all once scapegoating ends.
- Advocates for competition as a means of personal and societal growth, contrasting it with the current system of division by race.
- Uses personal anecdotes and examples to illustrate the benefits of healthy competition and growth.
- Encourages stepping out of comfort zones and challenging oneself for improvement.
- Posits that ending systemic racism will lead to a stronger, more united front addressing societal issues.
- Concludes with a message of hope and encouragement for positive change.

# Quotes

- "If you're kicking down, you're not actually looking in the direction of where the problems originate."
- "Ending systemic racism will improve the lives of the entire working class."
- "It doesn't actually, harder is probably not the right word. It's more competitive, but in a good way."
- "If you've ever been the smartest person in the room, you're in the wrong room."
- "Don't let it seem scary. It's not. It's a good thing."

# Oneliner

Beau addresses systemic racism, unity through competition, and the benefits of ending scapegoating for societal growth and improvement.

# Audience

Working-class individuals

# On-the-ground actions from transcript

- Foster unity and cooperation within your community to address societal issues (exemplified)
- Challenge yourself and others to step out of comfort zones for growth and improvement (exemplified)

# Whats missing in summary

Beau's passionate delivery and personal anecdotes can provide a deeper understanding and connection to the message.

# Tags

#SystemicRacism #Competition #Unity #SocietalChange #Growth