# Bits

Beau says:

- Explains the recent Supreme Court ruling related to Miranda rights, clarifying that it removed the civil remedy for police failing to read the Miranda rights.
- Quotes law professors and criminal defense lawyers advising people to remain silent and ask for a lawyer when dealing with the police, regardless of Miranda rights.
- Emphasizes the importance of not talking to the police, even if Miranda rights are completely overruled in the future.
- Notes the significance of the ruling that limits suing cops who ignore Miranda rights, urging people to understand the advice of remaining silent and asking for a lawyer for protection.
- Conveys the unanimous advice from lawyers to always remain silent and request a lawyer when interacting with law enforcement, as it is the only thing lawyers universally agree on.

# Quotes

- "Do not talk to the cops. Ask for a lawyer, then shut up."
- "You still have a right to remain silent. Use it. Do not talk to the cops."
- "Shut up and ask for a lawyer. That's what they will all tell you."

# Oneliner

The Supreme Court ruling removed the civil remedy for police not reading Miranda rights, reinforcing the universal advice to stay silent and ask for a lawyer when dealing with law enforcement.

# Audience

Everyday citizens

# On-the-ground actions from transcript

- Memorize and internalize the advice to remain silent and ask for a lawyer when interacting with law enforcement (implied).

# Whats missing in summary

Importance of understanding and exercising rights during police encounters.

# Tags

#SupremeCourt #MirandaRights #LegalAdvice #PoliceEncounters #CivilRights