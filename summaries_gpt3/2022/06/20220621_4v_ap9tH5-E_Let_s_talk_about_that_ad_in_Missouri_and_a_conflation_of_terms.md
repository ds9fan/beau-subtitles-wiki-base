# Bits

Beau says:

- Analysis of a controversial political ad for a Missouri Senate candidate involving military imagery and a shotgun.
- Republican candidate was reported to law enforcement for the threatening nature of the ad.
- The ad mentions a "rhino hunting permit" with no tagging or bagging limit, which are hunting terms related to animals, not people.
- Beau's observation at a burger joint reveals the intentional conflation of hunting terms with people, suggesting dangerous rhetoric.
- Alluding to historical radicalization on the right targeting less extreme individuals within their own group.
- The use of such imagery and rhetoric could have dangerous implications beyond just political discourse.

# Quotes

- "Get your rhino hunting permit."
- "Tagging and bagging? Nah, that has to do with people."
- "It's pretty dangerous rhetoric. It's pretty inflammatory."
- "Those people who hear what I heard and what the guys at the burger joint heard, they know what they're being told."
- "There's going to be a lot of questions about what was meant."

# Oneliner

Beau examines a controversial political ad for a Missouri Senate candidate, revealing dangerous rhetoric and intentional conflation of hunting terms with people.

# Audience

Voters, activists, analysts

# On-the-ground actions from transcript

- Contact local media to raise awareness about the dangerous implications of using inflammatory rhetoric (implied)
- Engage in community dialogues about the impact of political messaging on social cohesion and safety (implied)

# Whats missing in summary

The full transcript provides additional context on the potential impact of inflammatory political rhetoric on public safety and social divisions.

# Tags

#Missouri #SenateCandidate #PoliticalAd #InflammatoryRhetoric #CommunitySafety