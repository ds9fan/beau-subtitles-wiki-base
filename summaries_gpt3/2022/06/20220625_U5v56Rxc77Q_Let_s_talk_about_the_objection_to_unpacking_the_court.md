# Bits

Beau says:
- Exploring the idea of expanding and unpacking the court, addressing objections.
- Acknowledging the possibility of the Republican Party expanding the court if given the chance.
- Emphasizing the fight against authoritarianism and the role of the Democratic Party in slowing the descent.
- Suggesting that expanding the court could buy some time in the worst-case scenario.
- Urging people to stay engaged in the political process despite challenges.
- Warning against expecting fairness from those in power, who prioritize power over constituents.
- Staying vigilant in the fight against authoritarianism, even after Trump's presidency.
- Encouraging persistence and engagement in the face of setbacks, acknowledging the long-term nature of the struggle.

# Quotes

- "It's a fight, and it's going to be a long one because the United States has slid very, very far down that slope into authoritarianism."
- "Don't throw your hands up, though. There are a whole lot of people who are counting on us."
- "You need to wrap your minds around that right now."
- "They care about power for power's sake."
- "That slide towards authoritarianism [...] it didn't stop the day he left office. We're still there."

# Oneliner

Beau addresses objections to court expansion, advocating for Democratic Party as a temporary safeguard against authoritarianism and urging continued engagement in the ongoing political struggle.

# Audience

Political activists, Democratic supporters

# On-the-ground actions from transcript

- Stay engaged in the political process (implied)
- Advocate for court expansion as a temporary measure to slow authoritarian descent (implied)

# Whats missing in summary

The full transcript provides in-depth analysis on the implications of court expansion in the context of authoritarianism and urges sustained political engagement despite challenges.

# Tags

#CourtExpansion #Authoritarianism #DemocraticParty #PoliticalEngagement #FightForDemocracy