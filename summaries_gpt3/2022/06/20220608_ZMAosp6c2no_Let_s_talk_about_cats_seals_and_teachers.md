# Bits

Beau says:

- Explains how to use tourniquets and chest seals for emergency situations.
- Emphasizes the importance of proper training and staying updated on best practices.
- Shows the differences between various tourniquet types, like the Cat Generation 7.
- Advises on storing equipment securely and conveniently for easy access during emergencies.
- Demonstrates how to apply a tourniquet correctly and efficiently.
- Describes the purpose and application of a chest seal to prevent air from entering the chest cavity.
- Urges the audience to seek more comprehensive training beyond the basics he covers.
- Gives practical tips on how to improvise with available materials.
- Encourages taking courses like Stop the Bleed to enhance emergency response skills.
- Stresses the importance of continuous learning and preparedness in life-threatening situations.

# Quotes

- "This isn't training. You need real training."
- "You want to stay up to date because what is considered best practices changes over time."
- "This is how you become a superhero. This is how you save lives."
- "Please get more training. This isn't enough."
- "You can never have enough gauze. Ever."

# Oneliner

Beau explains the basics of using tourniquets and chest seals for emergencies, stressing the need for proper training and continuous learning.

# Audience

Emergency responders, teachers, concerned citizens

# On-the-ground actions from transcript

- Take a Stop the Bleed course (suggested)
- Ensure secure and accessible storage for emergency equipment (implied)
- Seek additional training beyond the basics covered (implied)

# Whats missing in summary

The full transcript provides a detailed demonstration of using tourniquets and chest seals for emergency situations, along with practical advice on improvisation and the importance of continuous learning.

# Tags

#EmergencyResponse #FirstAid #Training #Preparedness #CommunitySafety