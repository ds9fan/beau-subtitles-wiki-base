# Bits

Beau says:

- People feeling down about electoralism.
- Two common responses: Democrats won't do anything or aren't progressive enough.
- Beau poses a question: Why not run for office at local, state, or federal levels?
- Challenges notions of lacking qualifications or establishment barriers.
- Mentions individuals like Lauren Boebert and "space laser lady" who ran successfully.
- Points out that they weren't establishment favorites but still won.
- Encourages the idea of moving the Democratic Party left through personal involvement.
- Suggests considering running for office as a form of civic engagement.
- Emphasizes the potential for individuals to make a difference through running for office.
- Beau encourages the audience to think about the impact they could have by running for office.
- Stresses that running for office is a way to actively shape the future of the country.
- Acknowledges that not everyone may be suited for electoral politics but urges those who are to take action.
- Concludes with a positive encouragement for individuals to give running for office a try.

# Quotes

- "Maybe you should run."
- "I think that you're probably better than Lauren Boebert or the space laser lady."
- "I think you could be more effective."
- "I think you could probably better the country."
- "Y'all have a good day."

# Oneliner

Be the change by considering running for office to shape the future of the country and move the Democratic Party left.

# Audience

Potential candidates for office.

# On-the-ground actions from transcript

- Run for office at local, state, or federal levels (suggested).
- Engage in civic participation by considering a political candidacy (implied).

# Whats missing in summary

The full transcript provides detailed encouragement and reasoning for individuals to actively participate in shaping politics by running for office.