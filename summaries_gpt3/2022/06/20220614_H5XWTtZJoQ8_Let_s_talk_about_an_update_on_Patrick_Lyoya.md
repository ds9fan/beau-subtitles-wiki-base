# Bits

Beau says:

- Provides an update on the case of Patrick Leoya, a previous video that deeply affected him.
- Describes a communication issue at the beginning of the police engagement.
- Suggests that the officer initiated contact and escalated the situation.
- Mentions Patrick reaching for a taser, which is often used to justify police actions.
- Expresses surprise at charges being filed against the officer for manslaughter and second-degree murder.
- Notes the prosecutor's uphill battle but acknowledges a shift in attitudes towards presenting cases like this to a jury.
- Speculates that there may be more information known by the prosecutor and state cops.
- Concludes with uncertainty about the case's outcome.

# Quotes

- "The officer escalated at every possible opportunity."
- "It's worth noting that the prosecution does have an uphill battle here."
- "I've got to be honest, that's surprising."

# Oneliner

Beau provides an update on the case of Patrick Leoya, expressing surprise at charges filed against the officer for manslaughter and second-degree murder, indicating a shifting attitude towards prosecuting such cases.

# Audience

Advocates and Justice Seekers

# On-the-ground actions from transcript
- Support advocacy groups working towards police accountability (implied)
- Educate others on the importance of holding law enforcement accountable (implied)
- Stay informed about developments in cases of police misconduct (implied)

# Whats missing in summary

Additional context and details from Beau's emotional response and analysis of the case.

# Tags

#PoliceAccountability #Justice #Prosecution #CaseUpdate #CommunityPolicing