# Bits

Beau says:

- Teachers in Texas are taking issue with Ted Cruz's approach of hardening schools and arming teachers.
- Teachers are asking to be soldiers, not cops, as the police didn't prove effective.
- Being a teacher in this scenario is likened to being a soldier in combat, requiring extensive training.
- Dealing with students' issues may involve sending them to specialists with weeks of training.
- Teachers are being asked to undertake roles that in the military require multiple individuals.
- Ted Cruz is promoting arming teachers, essentially asking them to take on multiple military roles.
- Cruz's approach puts teachers in the position of being first responders and asks too much of them.
- Cruz's history and actions are critiqued, questioning his election and character.
- Teachers are already tasked with significant responsibilities, including shielding children with their bodies.
- Beau urges for a reconsideration of the extensive tasks being placed on teachers.

# Quotes

- "Teachers are asking you to be soldiers, not police."
- "He's asking you to be a teacher on top of five other jobs in the military."
- "I don't think that our teachers need to be first responders and rangers and everything else that we're asking them to be."
- "He is quite literally a meme for running away from a flurry of snowflakes."
- "We're already asking them to literally shield our children with their bodies."

# Oneliner

Teachers in Texas resist Cruz's militarized approach, questioning the burden on educators tasked with protecting children.

# Audience

Teachers, educators, activists

# On-the-ground actions from transcript

- Question the militarization of schools and the burden placed on teachers (implied)
- Advocate for better policies that prioritize student safety without burdening teachers with excessive responsibilities (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the challenges faced by teachers in Texas due to the militarized approach proposed by Ted Cruz. Watching the full video can offer a comprehensive understanding of Beau's perspective and insights.

# Tags

#Teachers #Texas #TedCruz #Education #Militarization #StudentSafety