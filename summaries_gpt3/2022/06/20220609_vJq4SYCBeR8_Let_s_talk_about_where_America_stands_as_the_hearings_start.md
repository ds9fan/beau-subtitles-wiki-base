# Bits

Beau says:

- Taking stock before hearings on Trump's actions post-January 6th insurrection.
- Initial poll post-January 6th: 54% of Americans believed Trump should face criminal charges.
- Recent poll shows 52% still support criminal charges against Trump.
- Only 42% believe Trump should not be charged.
- Committee can't charge Trump but can refer to Department of Justice.
- Rumors suggest committee wants consensus before making referral.
- Polls may influence committee's decision on referral.
- Department of Justice may or may not prosecute after referral.
- Majority of Americans see criminal liability for Trump.
- Committee's actions may lead numbers supporting criminal charges to increase.
- Trump's desire for White House may be cooled by public opinion supporting criminal charges.
- Trump's efforts as Republican kingmaker have not gained significant traction.
- More than half of Americans believing Trump should be charged does not bode well for his presidential ambitions.
- Public perception and committee's presentation to determine future course of action.

# Quotes

- "More than half of Americans see criminal liability for the former president."
- "Those who believe he should be charged are probably really unlikely to believe he should be in charge."

# Oneliner

More than half of Americans believe Trump should face criminal charges post-January 6th, potentially impacting his political future.

# Audience

Concerned citizens, political observers.

# On-the-ground actions from transcript

- Monitor and participate in future polls to gauge public sentiment on Trump's actions (implied).

# Whats missing in summary

Insights on the potential impact of public opinion on the political landscape.

# Tags

#Trump #CapitolInsurrection #DepartmentOfJustice #PublicOpinion #PoliticalFuture