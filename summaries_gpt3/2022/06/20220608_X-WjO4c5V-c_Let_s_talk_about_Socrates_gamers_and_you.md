# Bits

Beau says:

- Introduces the concept of the triple filter test attributed to Socrates, questioning if statements are true, good, and useful before speaking.
- Shares a modified version of the triple filter test: questioning if statements are true, moral, and necessary before speaking.
- Emphasizes the importance of filtering information before spreading gossip, especially in real-life organizing.
- Mentions a specific group of players of the video game War Thunder who leaked classified information on military equipment in forums.
- Recounts instances where schematics of military equipment like the Challenger tank and France's main battle tank were leaked due to personal disputes within the game community.
- Advises individuals, especially those involved in organizing, to avoid engaging in unnecessary conflicts or gossip that may hinder their mission.
- Raises awareness about the impact of personality conflicts and gossip on organizing efforts across the country.
- Stresses the significance of applying the triple filter test to avoid damaging consequences in the long run.

# Quotes

- "Is it true? Is it moral? And is it necessary?"
- "Your ego can often cause you to talk a little bit more than you should."
- "Does that personality conflict help or hinder you?"
- "There are large organizing efforts that are being really hindered by people not applying that triple filter."
- "It's something that could be damaging over the long haul."

# Oneliner

Beau shares insights on applying the triple filter test to prevent spreading damaging information and gossip, particularly relevant for those involved in organizing efforts.

# Audience

Organizers, gamers, activists

# On-the-ground actions from transcript

- Apply the triple filter test in your communication and decision-making processes (implied).
- Avoid engaging in unnecessary conflicts and spreading gossip that may hinder collective efforts (implied).
- Prioritize the mission and goals over personal conflicts and ego-driven interactions (implied).

# Whats missing in summary

The full transcript provides a detailed exploration of the consequences of sharing unnecessary information and engaging in conflicts within communities, urging individuals to prioritize truth, morality, and necessity in their actions and speech. 

# Tags

#Socrates #TripleFilterTest #Organizing #Gossip #CommunityPolicing