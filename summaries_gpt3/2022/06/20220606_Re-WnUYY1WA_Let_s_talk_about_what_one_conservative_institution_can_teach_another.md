# Bits

Beau says:

- A poll of Catholics reveals surprising departures from church leadership on key issues like Roe v. Wade and LGBTQ+ communion.
- 68% of Catholics want Roe v. Wade upheld, contrasting with the US Conference of Catholic Bishops' call to pray for its overturn.
- 77% believe LGBTQ+ individuals should receive communion, and 65% support openly gay priests.
- Despite these views, 68% of Catholics attend services less than once a month, with decreasing church attendance and worsening opinions.
- Beau suggests that conservative institutions, like the Republican Party, are losing touch by promoting extreme positions to energize a shrinking base.
- The strategy of scapegoating certain demographics for political gain may alienate the majority and lead to decreased engagement.
- Older individuals, who tend to support socially regressive positions, may age out in a few years, affecting the party dynamics.
- Beau predicts a potential split within the Republican Party as normal conservatives may seek alternatives rather than joining the Democrats.
- Most Republicans view Democrats as left-wing, but understanding their similarities to past Republicans could shift perceptions.
- Beau warns that in five years, the implications of current party strategies will become significant, leading to potential shifts within the Republican Party.

# Quotes

- "The longer this goes on, the less likely the majority of people are to be involved with that institution."
- "Scapegoating people, it energizes an ever-shrinking minority of people."
- "You're going to start to see the same type of thing play out within the Republican Party."

# Oneliner

A warning to conservative institutions: alienating the majority through extreme positions risks losing engagement and could lead to a party split.

# Audience

Conservative voters

# On-the-ground actions from transcript

- Re-evaluate messaging and policies to avoid alienating the majority (implied)
- Encourage open dialogues within conservative institutions to address shifting views (implied)

# Whats missing in summary

The full transcript provides deeper insights into the potential consequences of alienating the majority through extreme positions, especially within conservative institutions.

# Tags

#ConservativeInstitutions #RepublicanParty #CatholicChurch #PoliticalStrategy #PartySplit