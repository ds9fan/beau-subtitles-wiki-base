# Bits

Beau says:

- New York may need subject matter experts for legislation due to recent headlines about banning body armor.
- The bill defines body armor as soft, obsolete Kevlar, not modern hard plate armor.
- Regulating hard body armor like AR-500 steel is challenging and impractical.
- Legislation banning body armor seems to be a symbolic, ineffective measure.
- Understanding subject matter is vital for effective legislation.
- The bill is on its way to the governor despite banning outdated body armor.
- Modern body armor consists of hard plates, not soft, multi-layer fabric.
- Soft body armor became obsolete in the early 2000s due to its ineffectiveness against rifles.
- Legislation will be ineffective if it does not grasp the subject matter.
- Lack of understanding led to passing legislation that bans something rarely used.

# Quotes

- "Your legislation will be worthless if you don't actually understand the subject matter."
- "This legislation was designed to be a feel-good measure."
- "It's just a thought."
- "Understanding subject matter is vital for effective legislation."
- "Lack of understanding led to passing legislation that bans something rarely used."

# Oneliner

New York's legislation on body armor lacks understanding of modern armor, rendering it ineffective and symbolic rather than practical.

# Audience

Legislators, policymakers, concerned citizens

# On-the-ground actions from transcript

- Contact subject matter experts to inform legislation (suggested)
- Advocate for more comprehensive and informed legislation (suggested)

# Whats missing in summary

The nuances and detailed explanations provided by Beau in the full transcript. 

# Tags

#Legislation #BodyArmor #NewYork #SubjectMatterExperts #PolicyMaking