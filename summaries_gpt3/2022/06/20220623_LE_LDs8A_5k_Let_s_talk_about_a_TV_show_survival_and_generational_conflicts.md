# Bits

Beau says:

- A TV show about putting millennials through survival school, but they are actually 20-21-year-old kids, not millennials, tapping into generational nonsense.
- The show focuses on poking fun at a group of people rather than teaching actual survival skills.
- Beau questions why there is a push for younger generations to have the same skillset as the older generation, seeing it as a lack of progress.
- He shares a personal anecdote about a friend feeling like a failure for not teaching his son survival skills like hunting, only doing it once with him.
- Beau believes in progress and wants his kids to have a better skill set than he does, not the same.
- He references a quote about studying different subjects for progress and questions why society is stuck in perpetuating macho nonsense.
- Beau believes in basic survival skills but not at the expense of mockery, as it hinders true learning and self-reliance.
- He concludes by reflecting on how each generation believes the younger one will bring about downfall but is actually shaped by the previous generation.

# Quotes

- "Most complaints about the younger generation are really just admissions of failure for the older generation."
- "Yeah, I want everybody to have basic survival skills for an emergency, but it shouldn't be your life."
- "Society is supposed to progress."
- "Each generation feels the younger generation is going to be the one that brings down the world."
- "Y'all have a good day."

# Oneliner

Beau questions the mockery in teaching survival skills and advocates for progress and self-reliance in shaping future generations.

# Audience

Educators, parents, society

# On-the-ground actions from transcript

- Foster learning environments that prioritize teaching practical skills without mockery (implied).
- Encourage progress and self-reliance in education and skill development (implied).

# Whats missing in summary

The full transcript provides deeper insights into generational dynamics and the importance of progress in shaping future generations.