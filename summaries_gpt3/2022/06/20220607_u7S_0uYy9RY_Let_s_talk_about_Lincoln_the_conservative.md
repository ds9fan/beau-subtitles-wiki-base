# Bits

Beau says:

- President Lincoln was both a conservative and a progressive, with a key message challenging the assumption that conservatives want the status quo.
- Lincoln, despite being a Republican, was a progressive figure who fundamentally changed American society.
- Lincoln's leadership played a significant role in recognizing people as individuals legally, marking a progressive position in history.
- The common equating of Republicans with conservatives is misconstrued due to historical shifts in party ideologies.
- A letter from Karl Marx congratulating Lincoln on his reelection indicates Lincoln's progressive stance.
- The claim that Lincoln was a conservative is historically inaccurate, given his progressive actions and views.
- The Union flag under Lincoln represented progressives, not conservatives.
- The Republican party during Lincoln's time was more progressive than conservative.
- Lincoln's legacy as a progressive figure is undeniable, especially considering his impact on American history.
- The narrative that conservatives can claim Lincoln's legacy is debunked due to his progressive ideals and actions.

# Quotes

- "Lincoln was a progressive, not a conservative."
- "There is no way for the conservatives of today to claim him."
- "Lincoln got fan mail from Karl Marx."
- "He was a begrudging, pragmatic progressive."
- "Lincoln is responsible for one of the single most progressive moments in American history."

# Oneliner

Beau challenges misconceptions by illustrating President Lincoln's progressive legacy and dispelling the notion that conservatives can claim him.

# Audience

History enthusiasts

# On-the-ground actions from transcript

- Research and understand the historical context of President Lincoln's progressive actions (suggested)
- Engage in discourse about historical figures and their political ideologies (exemplified)

# Whats missing in summary

More in-depth exploration of the historical context and impact of President Lincoln's progressive actions.

# Tags

#PresidentLincoln #ProgressiveLegacy #ConservativeMisconceptions #PartyAffiliation #HistoricalContext