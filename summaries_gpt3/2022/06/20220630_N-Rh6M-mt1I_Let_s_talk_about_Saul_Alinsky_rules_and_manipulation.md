# Bits

Beau says:

- Explains about manipulation and Saul Alinsky, mentioning Alinsky's Rules for Radicals and its influence on current political control.
- Mentions a newspaper clipping warning about "useful idiots" influenced by Alinsky's writings.
- Lists the eight levels of control from Alinsky's book, including healthcare, poverty, debt, gun control, welfare, education, religion, and class warfare.
- Notes the misinterpretation of Alinsky's work, which was actually about empowering the have-nots, opposite to the newspaper clipping's claims.
- Talks about the demonization of Alinsky's work by attaching false power structures to it, creating fear and propaganda.
- Describes how the term "useful idiot" is used in political jargon to refer to those unknowingly promoting a cause.
- Concludes by discussing the spread of propaganda through fear-mongering tactics and the manipulation of information.

# Quotes

- "It is presently happening at an alarming rate in the US."
- "This isn't Alinsky's work. It's just not."
- "In political jargon, a useful idiot is a derogatory term..."

# Oneliner

Beau explains the misinterpretation and manipulation of Saul Alinsky's Rules for Radicals to create fear and propaganda.

# Audience

Citizens, Activists, Educators

# On-the-ground actions from transcript

- Fact-check and challenge misinformation spread through fear tactics (suggested)
- Educate others on the actual intentions of political terms and figures (exemplified)

# Whats missing in summary

The full transcript provides an in-depth analysis of how fear and propaganda are used to manipulate public perception, particularly in politics.

# Tags

#Manipulation #SaulAlinsky #Propaganda #PoliticalEducation #Misinformation