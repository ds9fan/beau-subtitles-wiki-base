# Bits

Beau says:

- Continuing a recent series of talks about tensions in the country.
- Addressing a message about radicalism and race.
- Emphasizing that radical black individuals seek more than just equality.
- Urging white Americans to recognize the need for radical societal changes for justice.
- Pointing out the lack of effort from white individuals to overcome racial ignorance.
- Stating that substantial investment in black Americans is necessary for progress.
- Describing the resistance to adjusting to black neighbors and genuine school integration among white Americans.
- Explaining the root causes of contemporary racial tensions.
- Stressing that racial and economic injustices require a radical redistribution of power.
- Quoting Martin Luther King's 1967 speech to illustrate longstanding radical ideas.
- Encouraging further exploration of the original vision for racial equality beyond surface-level knowledge.

# Quotes

- "White Americans must recognize that justice for black people cannot be achieved without radical changes in the structure of our society."
- "The problems of racial injustice and economic injustice cannot be solved without a radical redistribution of political and economic power."

# Oneliner

Beau addresses the need for radical societal changes to achieve justice for black individuals, drawing from Martin Luther King's 1967 speech to challenge perceptions on racial equality.

# Audience

Activists, allies, educators

# On-the-ground actions from transcript

- Educate on racial history and the original vision for equality (suggested)
- Advocate for societal changes to address racial and economic injustices (implied)

# Whats missing in summary

Exploration of how historical perspectives can inform current activism and advocacy efforts.

# Tags

#RaceRelations #RacialJustice #SocialChange #MartinLutherKing #Activism