# Bits

Beau says:

- Addressing a message sparked by a Patterson video, focusing on culture rather than workplace competition.
- Expressing uneasiness about the cultural power shifting away from white men towards more women and people of color in pop culture icons.
- Acknowledging the need for change and appreciating those leading the charge for change.
- Emphasizing the importance of representation for future generations in the fight for change.
- Explaining why white men may not be pop culture leaders in the fight for change due to advocating for the status quo.
- Sharing a personal anecdote about encouraging his son and his friend to participate in a march and the importance of being the representation you seek.
- Encouraging individuals to get involved in the fight for change, even if not taking a leadership role.
- Stating that the lack of white men in pop culture leadership roles is due to many choosing to maintain the status quo and not actively participating in the movement for change.

# Quotes

- "Nobody looks more like you than you."
- "You want your kid, your grandkid to have representation of people that look like you in the fight? You be that representation."
- "Just get involved. That's all it takes."

# Oneliner

Beau addresses the shift in cultural power away from white men in pop culture icons, urging individuals to actively participate in the fight for change to ensure representation for future generations.

# Audience

Creators and consumers of pop culture.

# On-the-ground actions from transcript

- Be the representation you seek in the fight for change (exemplified).
- Get involved in marches, protests, or community events advocating for change (exemplified).

# Whats missing in summary

The full transcript delves into the importance of individuals taking action to ensure representation and change in pop culture leadership roles.

# Tags

#PopCulture #Representation #Change #Community #Activism