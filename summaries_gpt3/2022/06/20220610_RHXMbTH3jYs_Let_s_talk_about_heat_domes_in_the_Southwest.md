# Bits

Beau says:

- Alerting about an upcoming heat dome over the southwestern United States impacting around 25 million people.
- Temperatures expected to be 20 degrees above normal, with many places hitting triple digits, including major cities like Austin, San Antonio, Phoenix, and Vegas.
- Warning about the dangers of underestimating extreme heat, citing the loss of 231 lives in British Columbia due to a heat dome last year.
- Advising people in the affected areas to stay hydrated by drinking water and utilizing water's cooling effect, like using wet rags or cold compresses.
- Suggesting limiting outdoor activities, wearing light and loose clothing, eating light, and checking on neighbors who may struggle in the extreme heat.
- Emphasizing the need to adapt and mitigate the impacts of climate change as these extreme heat events become more frequent.
- Cautioning that even small temperature increases can have significant consequences in already extreme climates like in Vegas.
- Urging people in the southwestern United States to take the heat dome seriously and not underestimate its severity.

# Quotes

- "Hydrate, hydrate, hydrate. Drink a lot of water. Not soda, water."
- "Don't underestimate the heat. It's a big deal."
- "Wear light baggy and light colored and lightweight clothing."
- "It causes a lot of loss when you're thinking about it."
- "Don't underestimate it. It's serious."

# Oneliner

Be prepared for an imminent heat dome impacting 25 million people in the southwestern US; hydrate, stay cool, and watch out for your neighbors in triple-digit temperatures.

# Audience

Residents in the southwestern United States

# On-the-ground actions from transcript

- Stay hydrated by drinking water and using cooling techniques (suggested)
- Limit outdoor activities and wear light clothing (suggested)
- Eat light and stay cool in extreme heat (suggested)
- Regularly checking on vulnerable neighbors (suggested)
- Adapt and mitigate climate change impacts (suggested)

# Whats missing in summary

The full transcript provides detailed tips on staying safe during extreme heat events and underlines the importance of not underestimating the severity of heat waves.

# Tags

#HeatDome #HeatWave #ClimateChange #CommunitySafety #Hydration