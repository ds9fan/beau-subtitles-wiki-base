# Bits

Beau says:

- Beau talks about the economy, unemployment, inflation, and concerns of big business.
- Despite global inflation issues, the United States economy is doing well.
- Big business is worried that the economy might be doing too well because of low unemployment rates.
- Employers have to compete for workers when unemployment is low, leading to higher wages.
- Big business is concerned that paying employees more will lead to inflation.
- Beau criticizes big businesses for not paying their employees more despite posting record profits.
- The media overlooks the fact that what benefits Wall Street might not be best for Main Street.
- Big business is worried about having to pay a living wage and raise their overhead costs.
- The system in the US operates on a carrot-and-stick approach for social mobility.
- Big business wants more people without jobs to avoid paying higher wages.

# Quotes

- "What's best for Wall Street may not be what's best for Main Street."
- "If your company is posting record profits, paying your employees more is just the thing you're supposed to do."

# Oneliner

Big business is worried about low unemployment leading to higher wages, illustrating the disconnect between Wall Street and Main Street.

# Audience

Workers, general public

# On-the-ground actions from transcript

- Advocate for fair wages for all workers (suggested)
- Support policies that prioritize workers over corporate profits (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the impact of low unemployment on big businesses and the disconnect between corporate interests and worker well-being.