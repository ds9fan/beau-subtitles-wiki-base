# Bits

Beau says:
- Supreme Court decision will fundamentally alter many states, reverting everything back to the state level.
- Republican states with laws on family planning will see devastating effects like increased teen pregnancy and poverty.
- Impacts will include loss of mothers, child marriage, and malicious prosecutions.
- Poor individuals won't have means to escape these laws by moving.
- Wealthy individuals will find ways to circumvent these laws.
- Predicts emergence of a discrete family planning industry catering to those seeking to avoid state restrictions.
- This decision targets poor people, not impacting the wealthy.
- Calls for unpacking the court by adding more justices as the only solution.
- Argues that current justices were appointed under false pretenses and need to be diluted.
- Lack of faith in the Supreme Court demands a change in its composition.
- Adding more justices is the way to address issues like marriage equality and other imminent threats.
- Diluting the votes of current justices is necessary to prevent worsening situations in the future.

# Quotes

- "It's time to unpack the court. That's your solution. It's really your only solution."
- "The solution is to add more justices."
- "Their only solution is to unpack the court, to get rid of these extremists that were put on there, right?"
- "The outcomes, the impacts, the effects of those decisions on your state, they're not good."
- "Adding more justices is the solution, to dilute the votes of these justices."

# Oneliner

Supreme Court decision will devastate states, targeting poor individuals, necessitating the addition of justices to dilute extreme views and prevent worsening situations.

# Audience

Voters, activists, lawmakers

# On-the-ground actions from transcript

- Advocate for adding more justices to the Supreme Court (suggested)
- Support efforts to unpack the court and dilute the votes of current justices (suggested)

# Whats missing in summary

Detailed explanation of the current Supreme Court decision and its potential ramifications.

# Tags

#SupremeCourt #Justice #PoliticalChange #StateLaws #Equality