# Bits

Beau says:

- Refuses to let the channel be dominated by content on firearms and gun control, opting to bulk questions together and address them collectively.
- Questions proposed solutions like making all guns pink and glittery, banning semi-automatics, and creating public armories, but Beau urges consideration of Second Amendment rights and Supreme Court interpretations.
- Beau explains why he doesn't advocate for banning all semi-automatics, citing concerns about authoritarianism and removing means of defense for vulnerable groups.
- Suggests a way to craft an assault weapons ban with real teeth by listing exemptions rather than defining what constitutes an assault weapon.
- Expresses reservations about proposals like liability insurance or taxing ammo, fearing they could disproportionately affect lower-income individuals.
- Clarifies that while the AR-15 can be used as a varmint gun due to its chambered caliber, it is not the primary reason most people own it.
- Addresses concerns about universal background checks potentially leading to a firearm registry and explains why he is not overly concerned about the government seizing firearms.
- Talks about a decline in mass incidents before the assault weapons ban, cautioning against attributing causation to correlation.
- Dismisses the claim that cops couldn't use a flashbang at a school due to civilians present, expressing frustration at the lack of clear answers in such situations.
- Advocates for working with those motivated by reducing violence, even if they lack expertise in firearms, and criticizes toxic masculinity's impact on gun culture.

# Quotes

- "People are dying. And that's their motivation."
- "To be a man, you've got to have this gun."
- "The gender roles in this country are blurry."

# Oneliner

Beau bulk addresses questions on firearms, advocating for solutions that prioritize rights, challenge authoritarianism, and confront toxic masculinity in gun culture.

# Audience

Gun control advocates

# On-the-ground actions from transcript

- Advocate for solutions that prioritize rights and challenge authoritarianism (implied)
- Address toxic masculinity within gun culture (implied)

# Whats missing in summary

The full transcript provides in-depth insights into Beau's perspective on gun control, addressing various proposed solutions and underlying societal issues related to firearms.

# Tags

#GunControl #SecondAmendment #ToxicMasculinity #Firearms #CommunitySafety