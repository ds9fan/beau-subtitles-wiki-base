# Bits

Beau says:

- Focuses on the pardons discussed in a recent hearing regarding requests made to Trump by specific individuals like Andy Biggs, Mo Brooks, Matt Gates, Gohmert, Scott Perry, and the Space Laser Lady from Georgia.
- Raises attention to the email from Brooks requesting pardons for himself and Gates, with the additional suggestion to pardon all congressmen and senators who voted against electoral college vote submissions of Arizona and Pennsylvania.
- Suggests two interpretations of Brooks' suggestion: either believing the act warrants a pardon or providing cover for their own pardons.
- Points out that individuals seeking pardons have been denying, deflecting, or downplaying the committee's claims, implying a tactical error.
- Warns that denials and dismissals may be refuted soon by the committee, as those who requested pardons are unlikely to have forgotten.
- Emphasizes that while the pardons are entertaining and sensationalized, the real focus should be on the pressure campaign and the committee's objectives.
- Acknowledges the attention-grabbing nature of the pardons but stresses the importance of understanding the broader context and objectives of the committee.

# Quotes

- "The pressure campaign, that's where the real goods are as far as what the committee is seeking to do."
- "The pardons, it's probably the most interesting, it is the most entertaining, no doubt."
- "Denials and dismissals, they need to be remembered because I have a feeling that all of these statements will be refuted in pretty short order."

# Oneliner

Beau sheds light on the sensationalized pardons discussed at a recent hearing, urging attention towards the broader context of the pressure campaign pursued by the committee.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Contact your representatives to express your concerns about the handling of pardons and pressure campaigns (suggested).
  
# Whats missing in summary

Deeper insights into the potential ramifications of denying or deflecting claims made by committees and the importance of transparency in political processes.

# Tags

#Pardons #PressureCampaign #CommitteeHearing #PoliticalAnalysis #Transparency