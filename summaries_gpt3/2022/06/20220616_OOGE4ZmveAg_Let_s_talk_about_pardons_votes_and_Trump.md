# Bits

Beau says:

- Committee suggested lawmakers sought pardons from Trump after Capitol events.
- Pardons imply wrongdoing.
- Beau wants comparison between pardon seekers' votes on impeachment.
- Beau suspects some sought pardons then voted against impeachment.
- Committee presenting strong case, likely to show people sought pardons.
- Beau hopes committee questions lawmakers about their votes.
- Sean Hannity suggested Trump pardon Hunter Biden to change the story post-Capitol fallout.
- Political parties often try to change the story when faced with bad news.
- Hannity's suggestion reveals real-life attempts to manipulate media coverage.
- Beau urges people to be mindful of manipulation tactics used by politicians.

# Quotes

- "Pardons imply wrongdoing."
- "Maybe [we should] have in the back of our minds, given the way they're talking about the hearings now."
- "It does kind of imply that you might have done something wrong if you are preemptively seeking a pardon."
- "I'd really like to see that."
- "That does happen."

# Oneliner

Committee suggests lawmakers sought pardons, Beau wants comparison with impeachment votes, and Hannity's suggestion to pardon Hunter reveals real-life media manipulation tactics.

# Audience

Voters, citizens, activists

# On-the-ground actions from transcript

- Question lawmakers about their votes (suggested)
- Be mindful of media manipulation tactics (implied)

# Whats missing in summary

Full insight into the connections between seeking pardons and voting behavior, as well as the impact of media manipulation on public perception.

# Tags

#Pardons #Impeachment #MediaManipulation #PoliticalTactics #Voters