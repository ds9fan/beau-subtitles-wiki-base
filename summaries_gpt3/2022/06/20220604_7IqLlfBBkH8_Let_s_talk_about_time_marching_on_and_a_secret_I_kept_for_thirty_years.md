# Bits

Beau says:

- Growing up in Tennessee in the early 90s, he befriended a girl in the neighborhood and developed a crush on her.
- The girl's father was irrationally angry and controlling, leading to uncomfortable situations.
- During a date to the movies with the girl, they watched "The Bodyguard," which caused her father to explode in anger afterward.
- The father's outburst was triggered by the interracial relationship portrayed in the movie, leading to a tense car ride home.
- The father expressed racist views and used derogatory terms, making Beau extremely uncomfortable.
- Following this incident, the girl's father started driving her to school, changing their dynamic.
- Despite the uncomfortable experience, Beau never spoke about it until now, recognizing parallels with modern parenting attitudes.
- Beau reconnects with the girl years later through social media and learns that she married a black man, a fact that he imagines upset her father greatly.
- Beau concludes with a message of hope, suggesting that over time, hate and prejudice diminish as each generation progresses.

# Quotes

- "There's obvious parallels between that and the attitudes of a lot of parents today."
- "On a long enough timeline, we win."
- "Each generation is better than the last."

# Oneliner

Beau in Tennessee shares a poignant story of past prejudice and hope for a better future, reminding us that hate fades with time.

# Audience

Parents, educators, youth advocates

# On-the-ground actions from transcript

- Support interracial relationships openly and actively in your community (implied).
- Challenge and address racist beliefs and behaviors when encountered (implied).

# Whats missing in summary

The deep emotional impact of experiencing racism and prejudice firsthand, and the enduring hope for a more inclusive future.

# Tags

#Prejudice #Racism #Hope #InterracialRelationships #Parenting