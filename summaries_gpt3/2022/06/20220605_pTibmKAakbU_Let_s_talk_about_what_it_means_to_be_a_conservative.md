# Bits

Beau says:

- Defines what it means to be a conservative, focusing on maintaining the status quo and traditional values.
- Points out that being conservative means defending the establishment and resisting change.
- Notes the irony in conservatives claiming to want to restore the Constitution while opposing change.
- Challenges the idea of wanting things to remain as they were in the past as defending the status quo.
- Emphasizes that conservativism inherently supports the current system and establishments.
- Urges letting go of the past and embracing change for improvement.

# Quotes

- "If you are a conservative, by default you support the Pelosi's of this world."
- "The worst reason in the world to do something is we've always done it that way."

# Oneliner

Being conservative means defending the status quo and establishment, resisting change, and not truly supporting a better world.

# Audience

Political activists

# On-the-ground actions from transcript

- Challenge traditional views (implied)
- Embrace change for improvement (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of the conservative mindset, illustrating the conflict between defending the status quo and advocating for a better world through change.

# Tags

#Conservatism #PoliticalIdeologies #Change #Establishment #StatusQuo