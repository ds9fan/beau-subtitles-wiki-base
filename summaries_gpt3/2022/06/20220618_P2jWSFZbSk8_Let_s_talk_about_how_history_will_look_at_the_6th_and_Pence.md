# Bits

Beau says:

- Talking about the historical significance of the events of January 6th and the ongoing hearings.
- Reminds that historians will document the actual story, ensuring it's available for those interested.
- Mentions the tendency to personify events in American history, focusing on one person rather than the collective.
- Suggests following the hashtag H-A-T-H on Twitter for historians at the hearings.
- Predicts that Mike Pence may be remembered as the hero who saved the American experiment by not cooperating with the attempted coup on January 6th.
- Emphasizes the critical nature of Pence's narrative from that day, which will shape his legacy.
- Urges Pence to explain his actions on January 6th to the American people without spin.
- Points out the importance of Pence continuing to show courage and accountability to prevent future authoritarian threats.
- Stresses the need for Pence to publicly address why he stayed on January 6th despite the risks.
- Warns that without accountability for the events of January 6th, there is a risk of similar incidents happening again.

# Quotes

- "This was a major historical event. It will probably be as transformative for the United States as Pearl Harbor was."
- "History is not done with Vice President Pence."
- "He's through the door. If he doesn't and there is no accountability, there is no message sent, they will try again."

# Oneliner

The historical significance of January 6th and the critical role of Mike Pence in shaping its narrative and preventing future authoritarian threats underscore the need for accountability and courageous leadership.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact historians at the hearings to stay informed and follow the hashtag H-A-T-H on Twitter (implied)
- Hold leaders accountable for their actions and demand transparency from public figures (implied)

# What's missing in summary

Importance of acknowledging historical events beyond partisan lenses and ensuring accountability for actions that shape the nation's narrative. 

# Tags

#USHistory #MikePence #Accountability #AmericanDemocracy #Authoritarianism