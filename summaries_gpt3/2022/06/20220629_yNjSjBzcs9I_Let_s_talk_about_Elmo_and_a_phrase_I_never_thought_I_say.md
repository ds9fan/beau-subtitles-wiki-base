# Bits

Beau says:

- Elmo from Sesame Street got vaccinated, leading political commentators to spark outrage and claim Sesame Street has gone "woke."
- Some are pushing back against Elmo getting vaccinated, fearing it might encourage children to get vaccinated.
- These critics, who are political commentators, failed to see through Trump for years and supported his actions, including a coup attempt.
- Despite claiming politics as their expertise, they now want to influence opinions on vaccination.
- Polio cases, which had decreased significantly, are now on the rise due to vaccination gaps.
- A virologist emphasized the importance of ensuring everyone is vaccinated for polio, even in places like London.
- Beau urges people to focus on polio vaccination alongside debates about Elmo and trusting political commentators over doctors.
- The anti-science and anti-education stance adopted by many is contributing to global health risks.
- Beau stresses the importance of getting vaccinated for polio, advocating against misinformation spread by political commentators.
- He questions the credibility and motives of those who prioritize outrage over trivial issues while neglecting serious health concerns like polio.

# Quotes

- "Elmo got vaccinated, therefore Sesame Street has gone woke."
- "Those doctors don't know what they're talking about, right?"
- "Vaccination gaps are causing an issue with polio."

# Oneliner

Elmo's vaccination sparks outrage among political commentators, distracting from vital issues like polio vaccination gaps.

# Audience

Parents, educators, advocates

# On-the-ground actions from transcript

- Ensure everyone is vaccinated for polio (suggested)
- Focus on promoting accurate health information and vaccination efforts in communities (implied)

# Whats missing in summary

The full transcript provides more context on the dangers of misinformation and the importance of prioritizing public health over trivial controversies.