# Bits

Beau says:

- Explains the premise of the show "Futurama" where a person is frozen in the year 2000 and wakes up in the year 3000, setting the stage for social commentary and satire.
- Addresses concerns about the reboot injecting social commentary clumsily, mentioning that the entire show is based on social commentary and satire.
- Breaks down episodes from the first season, showing how each one carries social commentary themes, such as political figures, consumerism, religion, and societal issues.
- Points out the strong feminist character in the show who belongs to the permanent underclass, residing in the sewers but vital for society's functioning.
- Lists various social issues addressed in "Futurama," including climate change, cancel culture, polyamorous relationships, and exploitation of native cultures.
- Asserts that "Futurama" has always been woke, combining satire with bluntness to deliver its social commentary effectively.

# Quotes

- "Futurama was always woke."
- "If you want a Futurama without social commentary, you're going to have to build your own Futurama with Blackjack."

# Oneliner

Beau breaks down how "Futurama" excels in delivering social commentary through satire, bluntly addressing various societal issues with a woke perspective.

# Audience

Fans of "Futurama"

# On-the-ground actions from transcript

- Watch and support the rebooted episodes of "Futurama" to appreciate and encourage the continuation of its social commentary (implied).

# Whats missing in summary

A deeper appreciation for the intricate and thought-provoking social commentary woven into the fabric of "Futurama."

# Tags

#Futurama #SocialCommentary #Satire #ScienceFiction #Woke #Reboot