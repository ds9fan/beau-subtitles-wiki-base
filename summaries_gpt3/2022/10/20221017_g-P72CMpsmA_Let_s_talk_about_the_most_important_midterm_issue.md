# Bits

Beau says:

- Identifies the most critical issue for the midterms, focusing on respecting election results as the number one question to ask candidates.
- Expresses disappointment that environmental policy cannot be the deciding factor due to the current political climate.
- Emphasizes the importance of candidates respecting election outcomes as a reflection of their commitment to democracy and representation.
- Raises concerns about candidates who undermine the electoral process and disregard the voice of the people.
- Argues that supporting candidates who do not respect election results means accepting rulers rather than representatives.
- Advocates for prioritizing candidates who genuinely represent the people before addressing other critical issues like environmental policy.

# Quotes

- "The number one question, the first question you have to ask, is whether or not that candidate respected the results of the last election."
- "If they didn't respect the results, especially now that we know and we've seen the footage of them talking about doing what they did before the election even happened, what does it say about them?"
- "They're not going to be your representative. They're going to be your ruler."
- "I don't see how you could vote for somebody who is flat out saying that they don't care about your vote, that your voice is irrelevant."
- "Until we have people that actually represent us and respect the results of elections, you're not going to get anywhere."

# Oneliner

Respecting election outcomes is the fundamental criterion for candidates, shaping true representation and democracy before addressing other critical issues like environmental policy.

# Audience

Voters

# On-the-ground actions from transcript

- Verify candidates' stance on respecting election results (suggested)
- Prioritize supporting candidates who value democracy and representation (exemplified)

# Whats missing in summary

Importance of choosing representatives who prioritize democracy and representation as a foundational step towards addressing other critical issues effectively.

# Tags

#Midterms #ElectionResults #RepresentativeDemocracy #CandidateCriteria #Voting