# Bits

Beau says:

- Addresses lessons learned from helping out with EIN after a hurricane, offering practical advice that can be immediately applied.
- Clarifies the relative scale of the hurricane's impact compared to past disasters like Michael or Katrina.
- Mentions areas like Arcadia and Sebring that may not have received as much coverage but are recovering.
- Advises on the importance of being prepared with gas when entering a disaster area, especially for relief efforts.
- Recommends considering generators that use gas, propane, or solar power for post-disaster situations.
- Emphasizes the need for proper footwear and essentials when assisting in disaster areas.
- Suggests stocking up on freeze-dried food to avoid relying solely on National Guard distributions post-disaster.
- Shares experiences with using generators and chainsaws to provide aid and why they are prioritized in relief efforts.
- Acknowledges the positive response of local governments in handling the aftermath of the hurricane.
- Talks about funding relief efforts through viewer contributions and the importance of bringing hope during disasters.
- Shares a personal experience of leaving supplies unsecured overnight in a disaster-stricken area and finding them untouched.

# Quotes

- "Treat it like going to Antarctica. If you didn't bring it, it's not there."
- "Your social media, your public messaging, that's your radio."
- "After every disaster, people start screaming about looters. Don't do that."
- "You're supposed to be conveying calm."
- "If you want to be that leader tough guy, that's what you should be doing, not inspiring paranoia and fear."

# Oneliner

Beau advises on practical disaster relief lessons, from preparedness to providing hope, showcasing effective community response.

# Audience

Disaster relief volunteers.

# On-the-ground actions from transcript

- Stock up on freeze-dried food for emergency situations (suggested).
- Ensure you have enough gas for both entry and exit when entering disaster areas (implied).
- Invest in generators that use multiple power sources like gas, propane, or solar (implied).
- Prioritize providing tools for self-rescue like generators and chainsaws in relief efforts (implied).

# Whats missing in summary

The full transcript provides valuable insights on disaster relief strategies, community responses, and the importance of conveying hope during challenging times.

# Tags

#DisasterRelief #CommunityResponse #Preparedness #Hope #LocalGovernment