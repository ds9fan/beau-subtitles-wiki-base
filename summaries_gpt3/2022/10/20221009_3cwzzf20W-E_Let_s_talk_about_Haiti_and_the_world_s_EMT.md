# Bits

Beau says:

- Haiti is facing a troubling situation and has requested foreign troops to help restore order.
- Beau advocates for the United States to act as the world's EMT (Emergency Medical Technician) rather than the world's policeman.
- He criticizes the use of the military for peacekeeping roles, stating that the military is meant to win wars, not maintain peace.
- Beau points out that the lack of a follow-on force like an EMT force leads to extended military presence in countries like Iraq.
- He mentions Thomas Barnett as a proponent of a separate force for non-warfighting missions and suggests watching his TED Talk.
- The request for foreign troops in Haiti has been acknowledged by the State Department, who are considering it.
- Beau believes that the US should prioritize developing a separate EMT force to handle failed states rather than deploying troops.
- He stresses the importance of investing in countries like Haiti rather than solely relying on military intervention.
- Beau argues that building the capability to stabilize failed states could do more for world peace than military actions.
- He suggests that with the decline of Russia, the US needs to be prepared for potential power vacuums in other countries.

# Quotes

- "The US military is not a peacekeeping force. It's a force that wins wars."
- "We don't have the world's EMT force built."
- "The military is there to take down governments, not build them."
- "This is a capability the United States needs that could prevent war."
- "Before we start doing it, we have to actually build that force."

# Oneliner

Beau advocates for the US to prioritize building an EMT force for stabilizing failed states over deploying troops to countries like Haiti, stressing the importance of investing in nations rather than relying solely on military intervention.

# Audience

Policymakers, activists, citizens

# On-the-ground actions from transcript

- Prioritize building a separate EMT force for handling failed states (implied)
- Advocate for investing in troubled nations like Haiti rather than relying solely on military intervention (implied)

# Whats missing in summary

The full transcript provides more in-depth analysis and historical context on the role of the US military in peacekeeping missions and the need for a dedicated EMT force to stabilize failed states.

# Tags

#Haiti #USMilitary #WorldPeace #EMTForce #ForeignIntervention