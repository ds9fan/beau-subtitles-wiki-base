# Bits

Beau says:

- Connects crabs, water, and gas prices to urge Americans to pay attention and think long-term.
- Mentions the impact of climate change on crab season in Alaska.
- Expresses how food supply interruption due to climate change is a scarier concept than just a closed crab season.
- Compares the volatility of the global food market to that of oil prices based on supply.
- Warns about the potential future volatility in basic food prices and eventually water.
- Optimistically believes in the possibility of change and mitigation but stresses the need to act quickly.
- Criticizes the influences of big oil companies and commentators in hindering the transition away from fossil fuels.
- Emphasizes that transitioning from gas is in everyone's best interest to avoid worsening market volatility and emissions.
- Reminds that politicians often lie and that those dismissing climate concerns are not acting in the public's best interest.
- Urges viewers to see through the rhetoric and take action towards a sustainable future.

# Quotes

- "The food supply was interrupted because of climate change."
- "Transitioning away from gas is in your best interest."
- "Those who are telling you you don't need to worry about the climate, that we don't need to transition, and in a quick manner, they're lying to you."

# Oneliner

Beau connects climate change impacts on crabs, water, and gas prices, urging immediate action for a sustainable future and criticizing those misleading the public on transitioning away from fossil fuels.

# Audience

Americans

# On-the-ground actions from transcript

- Transition away from gas to support a more stable market and reduce emissions (implied).
- Challenge misinformation and rhetoric from politicians and commentators invested in oil companies (implied).
- Advocate for funding transitions towards sustainability in the country (implied).

# Whats missing in summary

The full video provides more context on the interconnectedness between climate change, food supply, and market volatility, urging viewers to rethink their consumption habits and demand governmental action for a sustainable future.

# Tags

#ClimateChange #FoodSupply #MarketVolatility #TransitionAwayFromGas #Sustainability