# Bits

Beau says:

- Analyzing the disconnect between what political commentators claim to believe and what they are actually saying.
- Right-wing commentators who advocate for America first are not reflecting American talking points in the context of Ukraine.
- Questioning the fear-mongering tactics employed by these commentators regarding the use of nuclear weapons in Ukraine.
- Pointing out the contradiction in their behavior – claiming to be patriots but not acting in the best interest of America.
- Suggesting that these commentators are undermining the United States by supporting actions that weaken the country.
- Implying that these commentators might not truly support a constitutional republic but instead favor authoritarian rule by elite figures.
- Criticizing their admiration for Putin and the potential implications of his failure in the current conflict.
- Challenging the audience to reexamine the rhetoric they are being fed and whether it truly serves American interests.
- Warning that many individuals may have been deceived by these commentators into supporting positions contrary to American values.
- Concluding with a call for reflection on the audience's alignment with ideologies that may not be in the best interest of the country.

# Quotes

- "They're rooting for a country that has openly said it wants to sow discord in the United States and that it wants the US to fail."
- "Maybe they're not really America first. Maybe they don't really support a constitutional republic, as they claim."
- "They're not nationalists. They're not America first."
- "They just have their audience believing that they are while they feed them talking points that undermine their audience's actual beliefs."
- "Maybe that audience should really start to think about the rhetoric that they're being fed."

# Oneliner

Beau questions the patriotism of right-wing commentators who undermine America's interests while claiming to be patriots.

# Audience

Political observers

# On-the-ground actions from transcript

- Reexamine the rhetoric being consumed (suggested)
- Challenge allegiance to commentators who may not have America's best interests at heart (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of how certain political commentators may not truly support the values they claim to, urging viewers to critically analyze the narratives they are presented with.

# Tags

#PoliticalCommentators #Patriotism #Nationalism #AmericaFirst #Authoritarianism