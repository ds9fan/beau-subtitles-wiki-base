# Bits

Beau says:

- Compares his relief efforts after Michael and Ian, focusing on informal logistics.
- Describes his contacts in the impacted neighborhoods, retired military officers, and ex-contractors.
- Explains how he ran supplies and tools after Ian, while after Michael, he was deeply involved in relief work.
- Introduces the analogy of the spear to differentiate between combat operations and logistics in relief efforts.
- Emphasizes the importance of logistical support in relief work and activism.
- Talks about distributing cleanup tools like orange Home Depot buckets after Michael.
- Stresses the significance of providing tools for self-rescue in impacted areas.
- Mentions the network of people receiving supplies and directing them where needed.
- Points out the role of informal mutual assistance networks in strengthening communities.
- Addresses the question of how his efforts could do any good, focusing on setting up resources for others to take the next step in relief efforts.

# Quotes

- "You provide the tools for self-rescue, and the locals, the tip of the spear, will do it."
- "Everybody wants to be tip of the spear. The reality is you get in where you're most used, where you can be of most use."
- "It's not about the direct good that you're doing. You're getting the resources in place so somebody else can take that next step."

# Oneliner

Beau compares his relief efforts post-Michael and post-Ian, stressing the importance of informal logistics and providing tools for self-rescue in impacted areas.

# Audience

Relief volunteers

# On-the-ground actions from transcript

- Contact local organizations for relief efforts (implied)
- Distribute cleanup tools and supplies in impacted areas (implied)

# Whats missing in summary

The full transcript provides a detailed account of Beau's experiences with informal logistics in relief efforts, offering insights into the importance of providing resources for self-rescue and fostering mutual assistance networks.

# Tags

#InformalLogistics #ReliefEfforts #CommunitySupport #MutualAssistance #LogisticalSupport