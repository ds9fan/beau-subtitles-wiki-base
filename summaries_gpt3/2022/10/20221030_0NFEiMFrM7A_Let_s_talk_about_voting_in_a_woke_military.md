# Bits

Beau says:

- Exploring the effectiveness of voting and civic engagement.
- Responding to a viewer's disagreement on the impact of voting.
- Drawing an analogy involving a military scenario to explain the concept.
- Emphasizing that voting may not bring systemic change but can buy time.
- Touching on the topic of military inclusion and its implications.
- Acknowledging the background of the viewer's boyfriend as former 18F.
- Signing off with well-wishes for the audience.

# Quotes

- "Voting is never going to give you systemic change."
- "You're not going to achieve that greater world that you probably want through voting."
- "It can buy you time. You can have the least bad option."
- "There's another topic going on right now about the they them military."
- "That's about as hardcore as you get right there."

# Oneliner

Exploring the role of voting and civic engagement, Beau addresses disagreement, using a military analogy and stressing the temporary nature of voting's impact while touching on military inclusion concerns.

# Audience

Civic-minded individuals

# On-the-ground actions from transcript

- Engage in local capacity-building initiatives to drive real change (implied)

# Whats missing in summary

Deeper insights on the impact of military inclusion and its implications on society.

# Tags

#Voting #CivicEngagement #MilitaryInclusion #CapacityBuilding