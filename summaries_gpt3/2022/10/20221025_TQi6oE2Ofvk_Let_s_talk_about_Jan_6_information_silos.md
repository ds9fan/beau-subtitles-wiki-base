# Bits

Beau says:

- Addressing the aftermath of January 6th and the challenges of discussing it with individuals in closed information ecosystems.
- People on the right-wing often argue that January 6th wasn't an insurrection because there were no guns involved, influenced by misinformation from their sources.
- Despite the presence of significant weapons around the Capitol, some individuals deny the insurrection due to the absence of guns among the rioters.
- A specific case is mentioned where a person dropped a firearm during the Capitol breach, received a five-year sentence, and used a baton on a police officer.
- Beau suggests using such examples in dialogues to counter the false narrative that there were no guns present during January 6th.
- Many individuals in the far-right movement were misled and lied to, rather than genuinely believing in its core beliefs.
- Demonstrating objectively how they were misled might open the door for productive discourse and reflection.
- Beau encourages addressing the misinformation spread, particularly during election season, by simplifying the message: "They lied to you."
- Acknowledges that not everyone will be receptive to this information but believes it's still worth the effort to reach those who were misled.
- Emphasizes the importance of confronting individuals who manipulate reality and deceive their audience for personal gain.

# Quotes

- "They lied to you. What they told you wasn't true. Here is the evidence."
- "We're fighting against people who just don't care about objective reality."
- "You don't win every fight like this. But it'll work on a few."

# Oneliner

Beau addresses the misinformation surrounding January 6th, urging individuals to confront deception and manipulation with evidence, even if not everyone will be swayed.

# Audience

Activists, Educators, Allies

# On-the-ground actions from transcript

- Challenge misinformation in your community by using concrete examples like the Capitol breach incident (suggested).
- Engage in outreach efforts to confront deception and manipulation, especially during critical times like election season (suggested).
- Encourage critical thinking and evidence-based discourse when addressing misled individuals (suggested).

# Whats missing in summary

The full transcript provides a deeper insight into how misinformation can influence beliefs and actions, urging individuals to confront deception with evidence for productive discourse and reflection.

# Tags

#Misinformation #January6th #Deception #CommunityEngagement #CriticalThinking