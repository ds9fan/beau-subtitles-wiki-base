# Bits

Beau says:

- Providing historical context to an earlier video.
- Admitting to owning a mistake in not providing historical context in a previous video.
- Acknowledging the importance of historical context in bringing clarity.
- Linking the incident of soup throwers to a similar historical event in London during the early 1900s.
- Explaining the actions of the Women's Social and Political Union, also known as suffragettes.
- Drawing parallels between the suffragettes' actions and modern-day young climate activists.
- Recognizing the frustration of those who do not have a voice in the decision-making process.
- Acknowledging that when people lack a voice, they may resort to lashing out.
- Mentioning the tactic of drawing attention to climate change through disruptive actions.
- Noting the intentional reference made by one of the people involved with the Van Gogh painting to disobedience leading to rights.
- Suggesting looking up surveillance photos of militant suffragettes for more insight into the movement.
- Encouraging putting a face to a movement or idea by researching further.
- Ending with a thought-provoking message.

# Quotes

- "I made a mistake. Now we're going to turn that mistake into a happy little tree."
- "When people don't have a voice, they lash out. Right or wrong, that's what happens."
- "I can't condemn the lashing out without condemning the conditions that cause them to lash out."
- "It's not without historical precedent."
- "Sometimes it's good to put a face to a movement or an idea."

# Oneliner

Beau provides historical context, admits to a mistake, and draws parallels between suffragettes and modern activists, reflecting on the importance of having a voice in societal decisions.

# Audience

History enthusiasts, activists, educators.

# On-the-ground actions from transcript

- Research and learn more about the suffragette movement and their tactics (suggested).
- Look up surveillance photos of militant suffragettes to understand their struggle better (suggested).
  
# Whats missing in summary

The emotional impact of historical precedents on modern activism.

# Tags

#HistoricalContext #Activism #Suffragettes #ClimateChange #Voice