# Bits

Beau says:

- There is a push for psychological testing of officers to determine their suitability for duty.
- An audit in Alameda County Sheriff's Department resulted in 47 deputies having their weapons and arrest powers taken away.
- This amounts to 5% to 10% of the force, showing a significant oversight.
- The audit was prompted by a former deputy allegedly involved in a double shooting.
- Nationwide psychological testing for law enforcement officers is necessary.
- The issue arises with the expectation of retesting within two months, questioning if it's to address issues or beat the test.
- Law enforcement agencies sometimes believe they are above laws and regulations they enforce, indicating a systemic problem.
- Rushing retesting may lead to gaming the system rather than addressing underlying issues.
- There's skepticism about fixing personnel in two months to meet required standards.
- It's clear that nationwide psychological testing is vital to ensure officers' suitability.

# Quotes

- "One out of 20 cops that's out on the street shouldn't be there."
- "That's way more than a few bad apples."

# Oneliner

Psychological testing is vital for law enforcement suitability, but rushing retesting may lead to gaming the system rather than addressing deeper issues, raising concerns about systemic problems and the need for nationwide testing.

# Audience

Law enforcement oversight advocates

# On-the-ground actions from transcript

- Advocate for nationwide psychological testing for law enforcement officers (suggested)
- Raise awareness about the importance of addressing deep-rooted issues in law enforcement (implied)

# Whats missing in summary

The full transcript dives deeper into the potential consequences of rushing retesting and the systemic issues within law enforcement that contribute to these oversights.