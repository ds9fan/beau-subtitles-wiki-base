# Bits

Beau Jahn says:

- Explains why he doesn't talk about specific events like Hunter Biden and his recent troubles in Ukraine, as he doesn't run a gossip column.
- Mentions that as a private citizen, Hunter Biden's personal matters are not of interest unless they directly relate to an official capacity.
- Points out that he applies the same standard to all public figures, including Trump's kids and the former First Lady.
- Emphasizes the importance of focusing on real news instead of searching for scandals involving public figures.
- Suggests that media outlets can manipulate people by villainizing certain groups based on connections to others, leading individuals to compromise their beliefs.

# Quotes

- "I don't run a gossip column."
- "If there was a situation that Hunter Biden found himself in that did relate to an official capacity or the official capacity of Biden, even if it was just the appearance of it, I would talk about it."
- "There is enough real news to kind of parse through that searching for scandals about people that are related to public figures, that's just, I don't have time for that."
- "They give you something that is emotionally exciting."
- "Unless Hunter Biden's activity somehow impacted his father's activities, I literally don't care at all."

# Oneliner

Beau Jahn explains why he avoids discussing specific events like Hunter Biden's, focusing on real news and avoiding gossip.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Analyze news critically (implied)
- Focus on real news and avoid getting drawn into sensationalized scandals (implied)

# Whats missing in summary

The detailed explanation and reasoning behind Beau Jahn's editorial choices regarding what news topics to cover and why.

# Tags

#MediaConsumption #Gossip #HunterBiden #EditorialChoices #RealNews