# Bits

Beau says:

- Talks about the term "exit strategy" in relation to Ukraine and US foreign policy.
- Mentions the skewed perspective of Americans on war due to the last 50 years of US foreign policy.
- Explains the defensive nature of the war in Ukraine and how it differs from US elective wars.
- Emphasizes that the US is backing Ukraine, not leading the war, and should not negotiate with Russia directly.
- Stresses that decisions about peace and territorial concessions lie with Ukraine, not the US.
- Warns against imposing conditions on Ukraine for peace, as it could lead to US engaging in imperialism.
- Advocates for letting the Ukrainian people decide the course of their war and not pressuring them to fight or quit.
- Criticizes the idea of backing imperialist thoughts for better TV ratings.
- Calls for respecting the agency of those fighting for liberation in Ukraine.

# Quotes

- "There isn't an exit strategy. It doesn't exist because it's a defensive war."
- "This is something for the Ukrainian people to decide, not pundits on TV."
- "Just because the war isn't getting good enough ratings, I don't know that's a good enough reason."
- "The US shouldn't negotiate with Russia directly. That's 100% wrong."
- "We supply them, we keep their war running as long as they want to fight it."

# Oneliner

Beau explains why the US should not have an exit strategy in Ukraine and why it's up to the Ukrainian people to decide the war's course, not pundits or imperialist influences.

# Audience

Policymakers, Activists

# On-the-ground actions from transcript

- Support Ukraine by providing aid and supplies (suggested).
- Respect the agency of the Ukrainian people in deciding their war's course (implied).

# Whats missing in summary

The full transcript provides deeper insights into the nuances of US involvement in Ukraine and the importance of letting the Ukrainian people determine their future.

# Tags

#Ukraine #USForeignPolicy #DefensiveWar #Imperialism #SupportUkraine