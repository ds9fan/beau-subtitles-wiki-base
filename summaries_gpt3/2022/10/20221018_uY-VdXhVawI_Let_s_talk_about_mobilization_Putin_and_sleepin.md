# Bits

Beau says:

- Putin claimed mobilization in Ukraine will end in a couple weeks, but it is unlikely given Russian military's inability to foresee nightfall.
- Mobilization in Ukraine facing resistance, protests, mass exodus, attacks, and internal conflicts like a Russian officer insulting a Tajik god.
- Lack of unit cohesion among Russian and Tajik troops due to differing cultural identities and lack of sensitivity training.
- Beau criticizes hyper-masculine military culture in Russia, contrasting it with the U.S. military's likely response to such conflicts.
- Sensitivity training could have prevented the clash between Russian and Tajik troops at the mobilization center.

# Quotes

- "The whole point of being woke is so they don't catch you while you're sleeping."
- "Much more unlikely with the U.S."
- "That hyper-masculine military exists. It's that steaming pile of failure over there in Russia."
- "And one of the Russian officers basically insulted their god. And the Tajik troops lit him up."
- "Well, howdy there, Internet people."

# Oneliner

Beau analyzes Putin's statement on Ukraine mobilization, criticizes Russian military's lack of foresight and unit cohesion issues, contrasting hyper-masculine culture with a need for sensitivity training.

# Audience

Military reform advocates

# On-the-ground actions from transcript

- Implement sensitivity training for military personnel (implied)
- Advocate for diverse and inclusive military culture (implied)

# Whats missing in summary

Full context and nuances of Beau's analysis and commentary on the current situation in Ukraine and the contrasting military cultures between Russia and the U.S.

# Tags

#MilitaryReform #Putin #UnitCohesion #SensitivityTraining #UkraineMobilization