# Bits

Beau says:

- Introduces a new method of raising children focused on developing skills for the future, popular in conservative circles.
- Parents allow kids to get away with deceptive actions to nurture skills like deception and manipulation for later success.
- Examples include children hiding a cookie jar, creating fake permission slips, and mocking others without consequences.
- Parents prioritize fostering these skills over holding children accountable for their actions.
- Suggests that parents holding their children to a higher standard than political leaders may influence children's political views.

# Quotes

- "You hold a toddler more accountable for their actions than you do the leader of your political party."
- "If you're wondering why your children may have a different view when it comes to politics now..."
- "Parents need to have that ability if they want to succeed."
- "It's the only way they will ever be able to be the Republican nominee for president."

# Oneliner

Introducing an unorthodox method of raising children that prioritizes nurturing skills over accountability, potentially impacting political views.

# Audience

Conservative parents

# On-the-ground actions from transcript

- Reassess parenting strategies to balance skill development and accountability (suggested)
- Encourage open political discourse with children to understand differing views (suggested)

# Whats missing in summary

The tone and nuances of Beau's delivery can be best understood by watching the full transcript.