# Bits

Beau says:

- Alaska canceled the snow crab season, citing the population collapse.
- The term "overfished" allows the government to take action, even if fishing wasn't the cause.
- The snow crab population decreased from 8 billion in 2018 to 1 billion in 2021.
- Factors like warming waters, disease, or migration to cooler areas could have caused the decline.
- Coverage mainly focuses on the economic impact on fishermen, not enough on climate change.
- Beau suggests tying climate change to the economic impact to stress the cost of inaction.
- Transitioning to mitigate climate change requires significant investment and preparation.
- Failure to address climate change could lead to more industries collapsing and workers becoming economic refugees.
- Beau warns about the long-term consequences of not taking action to mitigate climate change.
- The focus on money in the US may require framing the cost of inaction to spur more action.
- Generations of fishermen are losing their livelihoods due to the collapse of the snow crab population.
- The disappearance of billions of a species should be a significant concern, not just an economic one.
- Beau urges for a shift in focus towards considering the costs of inaction when discussing climate change.

# Quotes

- "This is a warning."
- "Generations of a species disappearing should factor into this in some way."
- "Have a good day."

# Oneliner

Alaska cancels snow crab season due to population collapse, raising concerns on economic impact and urging a shift towards considering the costs of inaction on climate change.

# Audience

Climate activists, policymakers, seafood industry workers

# On-the-ground actions from transcript

- Advocate for policies that address climate change impacts on seafood industries (implied)
- Support initiatives to protect and conserve marine species affected by climate change (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the economic and environmental impacts of the snow crab population collapse and serves as a reminder to prioritize action on climate change to prevent further consequences.

# Tags

#ClimateChange #EconomicImpact #SeafoodIndustry #EnvironmentalConservation #ActionNeeded