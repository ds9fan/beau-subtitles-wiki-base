# Bits

Beau says:

- Someone went to Fort Myers to provide gas and food to people affected by a disaster.
- The person started by helping people in their parents' neighborhood and then expanded to nearby neighborhoods.
- They were criticized by members of a college organization for not being a "real activist" because they started with people they knew.
- Beau argues that the first rule of disaster relief is to not create a second victim.
- He suggests not listening to critics who aren't involved in relief work themselves.
- Beau commends the individual for starting with people they knew and expanding from there.
- He dismisses the idea of "purity testing" in disaster relief efforts.
- Beau explains that disaster relief involves networking and helping those you're connected to.
- He shares a specific example of how helping one person led to assisting others in need.
- Beau stresses the importance of utilizing existing networks and contacts in relief efforts.

# Quotes

- "The first rule of disaster relief is don't create a second victim."
- "You start with the points of contact on the ground that you have."
- "The whole idea is based around the premise that one network of people assists another network of people."
- "People who may not get involved themselves will always have a criticism of the way you did it."
- "Just keep in mind, you did it."

# Oneliner

Someone travels to help disaster victims, faces criticism for starting with known contacts, but Beau defends the importance of networking and mutual assistance in relief efforts.

# Audience

Disaster relief volunteers

# On-the-ground actions from transcript

- Reach out to your existing network to identify needs and provide assistance (exemplified)
- Collaborate with local organizations or supply depots to distribute aid effectively (exemplified)
- Utilize community connections to expand the reach of relief efforts (exemplified)

# Whats missing in summary

Importance of community networking and utilizing existing contacts in disaster relief efforts

# Tags

#DisasterRelief #CommunityNetworking #MutualAssistance #Criticism #Activism