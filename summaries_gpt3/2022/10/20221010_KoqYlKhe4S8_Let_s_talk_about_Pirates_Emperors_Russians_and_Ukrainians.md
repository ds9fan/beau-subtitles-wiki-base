# Bits

Beau says:

- Introduces the concept of pirates and emperors to illustrate a core concept.
- Tells the story of Alexander capturing a pirate to explain the distinction between a pirate and an emperor.
- Emphasizes that the core concept is doing the same thing through the same means.
- States that legality and morality are not the same, even though pirates and privateers share the same action.
- Questions the terminology and media response regarding an event on a bridge, pointing out the influence of state ties.
- Suggests that the delivery system does not change the morality of an action.
- Encourages understanding that nations are not exempt from normal morality and that this understanding helps combat propaganda.
- Compares Russia calling Ukraine a pirate to both sides using similar means for different purposes - defense and invasion.
- Stresses that the real difference lies in the moral aspects rather than the legal or illusionary aspects of conflicts.
- Concludes by leaving the audience with food for thought and well-wishes for the day.

# Quotes

- "Legality and morality are not the same."
- "Pirates and emperors."
- "Once you grasp that and really get it, it's much harder to fall for propaganda."
- "The real difference is that one side is defending and the other side is invading."
- "That's what matters."

# Oneliner

Beau introduces the concept of pirates and emperors, discussing the thin line between legality and morality in conflicts, urging a deeper understanding to combat propaganda.

# Audience

Critical Thinkers

# On-the-ground actions from transcript

- Understand the distinction between legality and morality when analyzing conflicts (suggested)
- Combat propaganda by seeking a deeper understanding of the moral aspects of conflicts (suggested)

# Whats missing in summary

The full transcript provides a deeper exploration of the interconnected themes of legality, morality, propaganda, and conflicts, urging viewers to critically analyze these concepts.

# Tags

#Morality #Legality #Propaganda #ConflictAnalysis #CriticalThinking