# Bits

Beau says:

- Talks about a positive lesson from January 6th defendants.
- Encourages people to show up and get involved locally.
- Emphasizes that everyone has something to contribute.
- Mentions the theme of "getting caught up" in the January 6th defendants' statements for leniency.
- Points out that surrounding oneself with motivated people can lead to positive outcomes.
- Compares the motivation gained from positive causes to the negative consequences of getting caught up in destructive actions.
- Suggests that redirecting energy towards positive actions can lead to significant good in the world.

# Quotes

- "Show up, show up."
- "Everybody has something that they can contribute."
- "Surround yourself with people who genuinely want to make the world a better place."
- "You're around people who genuinely want to make the world a better place."
- "Can you imagine the amount of good it would do if that much energy was expended in making things better, in building rather than destroying?"

# Oneliner

Beau encourages showing up locally, surrounding oneself with motivated individuals, and redirecting energy towards positive actions for a better world.

# Audience

Community members ready to get involved.

# On-the-ground actions from transcript

- Attend local community events and meetings to get involved (implied).
- Surround yourself with individuals motivated to make positive change (implied).
- Redirect energy towards building and improving the community (implied).

# Whats missing in summary

The importance of actively seeking out opportunities to contribute and participate in local initiatives for positive change.

# Tags

#CommunityInvolvement #PositiveChange #Motivation #LocalEngagement #Activism