# Bits

Beau says:

- Explains Biden's blanket pardon for simple possession of cannabis at the federal level.
- Clarifies that Biden's authority is limited to federal level pardons, not state level.
- Notes that thousands of people are being pardoned, with nobody being let out because the shift in prosecution has already started.
- Mentions the potential next steps after pardoning, including descheduling cannabis.
- Suggests that Biden's administration may be working out the details before more significant actions.
- Points out that prior to the big announcement on student debt forgiveness, billions of dollars were already forgiven.
- Addresses the legal paperwork trail that may still affect individuals despite receiving a pardon.
- Emphasizes that while pardons are not a clean slate, they represent a step forward in the process.
- Expresses the belief that there is more to come regarding Biden's actions.
- Concludes by hinting at further developments to watch out for.

# Quotes

- "He did in his executive orders kind of encourage them to do so, but he can't make them pardon."
- "That eternal question. Why aren't people currently in custody for this? Because the shift already started."
- "There's a little bit of a difference there in how a prospective employer might look at it."
- "I think there's a lot more that's going to be coming, but we have to wait and see."
- "Just understand you're going to see this material again."

# Oneliner

Beau explains Biden's federal level cannabis pardons, hints at more actions to come, and sheds light on student debt forgiveness.

# Audience

Reform advocates

# On-the-ground actions from transcript

- Monitor and advocate for further actions from the Biden administration (implied)
- Stay informed about developments in pardons and student debt forgiveness (implied)

# Whats missing in summary

Detailed insights on the implications and potential outcomes of Biden's pardons and student debt forgiveness.

# Tags

#Biden #Pardons #Cannabis #StudentDebt #ExecutiveOrders