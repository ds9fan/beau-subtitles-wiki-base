# Bits

Beau says:

- Biden administration's new policy on drones formalizes existing practices.
- Policy specifically addresses the use of drones outside areas of active hostilities.
- White House now makes the call on drone strikes, aiming to limit civilian casualties.
- Policy extends to in-person covert operations like raids.
- Public disclosure suggests White House intent to limit drone and covert operation use.
- Previous lax rules on drone strikes were not effective.
- Skepticism remains on whether the new policy will be effectively implemented.
- There is a history of attempts to clean up drone strike practices.
- The implementation of the new policy needs to be closely monitored.
- The policy may make it easier to conduct operations with low risk to U.S. personnel.

# Quotes

- "The White House determines individuals, groups, and countries that drones can be used in or against."
- "There has been a concerted effort to clean this up."
- "While the policy looks good, we need to see the implementation before we start sharing."

# Oneliner

Biden administration formalizes drone policy to limit civilian casualties, but skepticism remains on effective implementation and impact on covert operations.

# Audience

Policy analysts

# On-the-ground actions from transcript

- Monitor the implementation of the new drone policy closely (implied).
- Stay informed on any updates or changes in U.S. drone strike practices (implied).

# Whats missing in summary

The full transcript provides additional context on the historical background and challenges with drone strike policies, which can offer a comprehensive understanding of the topic. 

# Tags

#BidenAdministration #DronePolicy #CivilianCasualties #CovertOperations #Implementation