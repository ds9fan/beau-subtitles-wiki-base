# Bits

Beau says:

- The Cherokee Nation is seeking fulfillment of a 200-year-old promise from the 1835 Treaty of New Echota for a delegate in the House of Representatives to represent their interests.
- Despite the stipulation in the treaty, the promise has not been honored for almost two centuries, reflecting a historical pattern of neglect towards Native treaties and issues in the American government.
- The Treaty of New Echota, signed through debatable means, resulted in the Trail of Tears, but significant parts of the treaty remain unfulfilled.
- If granted, the Cherokee Nation's delegate in the House of Representatives, Kim Teehee, will be a non-voting member but will have the ability to participate in debates, introduce legislation, and potentially serve on committees.
- Beau advocates for honoring the promises made in treaties, addressing historical issues, and making amends for past injustices towards Native communities.

# Quotes

- "It's probably time for the U.S. government to honor the treaties and include all the words."
- "A little too realistic, I guess. A little bit too representative of the whole."
- "It might be time to do that. Or they could just, you know, forget about those few words again."
- "The Cherokee Nation are asking Congress to give them a delegate in the House of Representatives. They really shouldn't be asking, they should be demanding."
- "It's been almost 200 years and it hasn't happened because, as is often the case when it comes to Native treaties or anything that deals with Natives in the American government, well, let's just sort of ignore that part."

# Oneliner

The Cherokee Nation seeks a delegate in the House of Representatives to fulfill a 200-year-old promise, urging the U.S. government to honor treaties and address historical injustices.

# Audience

Advocates for Native rights

# On-the-ground actions from transcript

- Support the Cherokee Nation's request for a delegate in the House of Representatives (suggested).
- Advocate for honoring Native treaties and addressing historical injustices (suggested).

# Whats missing in summary

The full transcript provides historical context on unfulfilled promises to Native communities and advocates for honoring treaties and rectifying past injustices.