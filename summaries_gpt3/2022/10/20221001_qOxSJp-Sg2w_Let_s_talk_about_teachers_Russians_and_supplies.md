# Bits

Beau says:

- Comparison between supplies for troops in Russia and the United States.
- Russian troops facing challenges with basic equipment like armor and medical supplies.
- Russian troops being ill-equipped and unprepared for sustained or wider scope wars.
- Mention of a video showcasing a bag with everything needed to treat injuries for troops heading into combat.
- American teachers having better equipment, preparedness, and training to deal with gunfire compared to Russian troops.
- Indictment of both countries for the disparity in equipment between American teachers and Russian troops.
- Importance of not normalizing American teachers having the exact equipment needed by Russian troops in combat.
- Mention of tourniquets and chest seals as critical supplies lacking for Russian troops.
- Suggestion to work on ensuring proper supplies for Russian troops.
- Beau leaves viewers with a thought-provoking message about the supply disparities.

# Quotes

- "American teachers are better equipped, better prepared, and better trained to deal with gunfire than troops heading into Russia that were recently conscripted."
- "That is an indictment of both countries."
- "The tourniquets, the chest seals, this is stuff they desperately need over there and they don't have."
- "Probably something we should work on."
- "It's just a thought."

# Oneliner

Beau compares the disparity in supplies between American teachers and Russian troops, urging to address the critical equipment shortage.

# Audience

Global citizens

# On-the-ground actions from transcript

- Ensure proper supplies for troops (suggested)
- Advocate for better equipment for troops (implied)

# Whats missing in summary

The full transcript provides more context on the ongoing supply challenges faced by Russian troops and the need for urgent action to address this disparity.

# Tags

#Supplies #Russia #UnitedStates #Disparities #GlobalCitizens #Advocacy