# Bits

Beau says:

- Addresses the misinformation surrounding bridges and their military importance.
- Questions the narrative that bridges used for military purposes are civilian infrastructure.
- Uses the example of Ukrainian bridges being hit by Ukrainians to illustrate the military nature of targeting bridges.
- Stresses that taking out bridges has always been a military strategy to impede routes and slow advances.
- Mentions the importance of bridges in war strategies and the limited ways to move from point A to point B.
- Emphasizes that anyone claiming a bridge used for military purposes is civilian infrastructure is trying to deceive.
- Concludes with a thought about the significance of bridges in warfare.

# Quotes

- "Anybody who is trying to tell you that a bridge used to move troops and supplies is civilian infrastructure either knows nothing about war or is deliberately misleading you."
- "Taking out bridges has always been a military thing."
- "Anybody who is trying to sell you on the idea that a bridge used to move troops and supplies is civilian infrastructure is trying to sell you a bridge."

# Oneliner

Beau breaks down the misconception around bridges, exposing their military significance and debunking the idea of bridges for military use being classified as civilian infrastructure.

# Audience

Military history enthusiasts

# On-the-ground actions from transcript

<!-- Skip this section if there aren't physical actions described or suggested. -->

# Whats missing in summary

The detailed examples and historical context provided by Beau in the full transcript.

# Tags

#Bridges #Military #Warfare #Misinformation #Ukraine #Infrastructure