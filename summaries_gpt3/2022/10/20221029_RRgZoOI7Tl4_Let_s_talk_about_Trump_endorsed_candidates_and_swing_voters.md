# Bits

Beau says:

- Beau addresses American swing voters, independents, and moderates, discussing their rejection of Trump due to divisiveness, lies, anger, and hate.
- He points out that Trump-endorsed candidates are essentially mini-Trumps, following his orders and talking points directly.
- Referring to a specific incident in Arizona, Beau mentions Trump instructing a candidate to lie more about the 2020 election to appease his radicalized base.
- Beau warns that Trump-endorsed candidates embody hate, anger, ineffectiveness, and division because they simply follow Trump's directives.
- Despite Trump being viewed as a losing candidate, Beau stresses that as long as he controls his endorsed candidates, he maintains power without winning elections.
- Beau questions the false narratives spread before the election about "Biden's America" with riots and chaos, attributing those images to Trump's time in office.
- He urges swing voters to recall why they rejected Trump and to understand that Trump-endorsed candidates will exhibit the same negative traits.
- In summary, Beau cautions against supporting Trump-endorsed candidates as they represent the same divisive and harmful characteristics as Trump himself.

# Quotes

- "They're taking their orders directly from him."
- "That hate, that anger, that ineffectiveness, that division, that's what you can expect from Trump-endorsed candidates."
- "Where did those images come from? They came from Trump's time in office."
- "They're going to be exactly what you rejected."
- "It's the same hate, the same lies, the same division, the same ineffectiveness."

# Oneliner

American swing voters warned against supporting Trump-endorsed candidates embodying hate, lies, and division, mirroring the damaging traits of the former president.

# Audience

American swing voters

# On-the-ground actions from transcript

- Inform fellow swing voters about the dangers of endorsing Trump-backed candidates (implied)

# Whats missing in summary

The emotional impact of rejecting Trump's divisive politics and the importance of remembering his harmful influence on endorsed candidates.

# Tags

#AmericanPolitics #Trump #SwingVoters #Election2022 #Moderates