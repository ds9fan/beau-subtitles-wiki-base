# Bits

Beau says:

- Beau addresses Social Security and the potential intentions of the Republican Party in 2023.
- In 2023, there's a significant increase of 8.7% in Social Security payments due to inflation.
- Medicare Part B premium is expected to decrease for the first time in a decade.
- The concern arises from Biden's statement about Republicans putting Social Security on the chopping block.
- Rick Scott and Ron Johnson propose making Social Security discretionary rather than mandatory, potentially leading to cuts.
- House Republicans plan to leverage the debt limit deadline to push for changes in entitlements and safety net programs.
- There's a proposal to raise Medicare eligibility to $270 and Medicaid to $67.
- The Republican Party may use leverage to threaten the economy if they don't get their way, akin to steering the country towards economic ruin.
- Despite thinking it's outlandish, Beau acknowledges recent actions by the Republican Party make this theory plausible.
- Beau expresses concern about the future of Social Security and Medicare if the Republican Party gains more control.

# Quotes

- "If you are receiving Social Security, you know, that you paid into for your entire life, you're the fat, by the way."
- "It is worth noting the Republican Study Committee has put out a thing."
- "But then again, we did all just watch them follow Trump, to, you know, subvert the entire election."
- "I find it hard to believe that there is a major political party in the United States that really does think that leverage is, you know, basically steering the entire country into economic ruin."
- "Y'all have a good day."

# Oneliner

Beau warns of potential cuts to Social Security and Medicare by the Republican Party in 2023, using leverage and political maneuvers to push their agenda.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact local representatives to express support for protecting Social Security and Medicare (suggested).
- Join advocacy groups focused on safeguarding entitlement programs (exemplified).
- Organize community events to raise awareness about potential threats to Social Security and Medicare (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the potential threats to Social Security and Medicare, urging vigilance and proactive engagement to protect these vital programs.

# Tags

#SocialSecurity #Medicare #RepublicanParty #PoliticalThreats #EntitlementPrograms