# Bits

Beau says:

- Explains the mobilization in Russia and addresses misleading headlines in the United States.
- Mentions the headline about 200,000 Russian soldiers being drafted, clarifying that 200,000 troops have been conscripted, but it's shy of their goal.
- Points out that the most critical number is 335,000, the Russians who fled the country to avoid conscription.
- Compares the number of Russians fleeing to avoid conscription to those who fled the United States during the Vietnam draft.
- Indicates the lack of desire among Russian people to fight in the war, with more fleeing than volunteering.
- Notes that even if Russia hits its conscription goal, they still won't have enough troops.
- Mentions that half of the troops sent from one region were unfit for service, leading to the firing of the person in charge of conscription.
- Expresses a pessimistic view on Russia's mobilization and the fate of troops entering Ukraine.
- Describes the situation as "pure waste" due to the lack of substantial troop numbers and quality.
- Speculates on Putin's next moves and the unlikelihood of success in Ukraine.

# Quotes

- "More people have fled the country than have been conscripted."
- "The cause is lost. They don't have the troops."
- "Putin may push mobilization even further in hopes of just throwing enough people over there."
- "At some point, somebody in Russia is going to have to make the call and explain to Putin what his options are."
- "It doesn't look like any of the things that would allow him to remain are going to happen."

# Oneliner

Beau clarifies misleading headlines on Russian mobilization, pointing out the lack of troops due to mass avoidance of conscription, painting a grim picture of Putin's imperialist endeavor in Ukraine.

# Audience

Politically aware individuals

# On-the-ground actions from transcript

- Inform others about the realities of Russian mobilization (suggested)
- Support efforts to provide aid and resources for those fleeing conscription (exemplified)

# Whats missing in summary

Insights on the implications of Putin's actions and potential consequences for Russia.

# Tags

#Russia #Mobilization #Conscription #Putin #War