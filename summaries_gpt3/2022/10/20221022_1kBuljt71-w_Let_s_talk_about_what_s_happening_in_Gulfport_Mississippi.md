# Bits

Beau says:

- Introduces the familiar story of a police shooting in Gulfport, Mississippi, involving a 15-year-old.
- Eyewitnesses agree that the kid didn't have anything in his hands except for the police.
- Beau expresses distrust in both eyewitness reports and police reports.
- Emphasizes the importance of video footage in understanding what happened.
- Points out the lack of public footage, including body cam and dash cam footage, in this case.
- Raises questions about why officials are hesitant to release the footage.
- Mentions that the family has hired lawyer Crump and current demonstrations are demanding the footage.
- Suggests that if the department believes they did nothing wrong, they should allow Crump to view the footage to ease tensions.
- Argues that withholding footage when it could confirm the department's actions as justified puts the blame on the department if demonstrations escalate.
- Urges for transparency to avoid assumptions of wrongdoing by the department and to reassure the community.

# Quotes

- "If you hide it, the only thing the public can assume is that you're hiding it for one of the reasons that includes the department doing something wrong."
- "You have the opportunity to diffuse this now. It seems like the smart move."
- "And if it is one of the reasons where the department didn't do what they were supposed to, it's probably better if the public finds out sooner rather than later."

# Oneliner

Beau sheds light on the importance of releasing video footage in a police shooting involving a 15-year-old to prevent community unrest and assumptions of wrongdoing by the department.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Demand the release of video footage from the police department (suggested)
- Support demonstrations calling for transparency and accountability (exemplified)
- Advocate for allowing lawyer Crump to view the footage to ensure transparency (implied)

# Whats missing in summary

The emotional impact and urgency of addressing police transparency and accountability in cases involving minors like Jaheim McMillan.

# Tags

#PoliceShooting #Transparency #CommunityAccountability #ReleaseTheFootage #JusticeForJaheim