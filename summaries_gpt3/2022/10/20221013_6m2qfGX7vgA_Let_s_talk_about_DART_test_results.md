# Bits

Beau says:

- NASA's Double Asteroid Redirection Test (DART) successfully altered the orbit of an asteroid by crashing something into it.
- The test aimed to shorten the asteroid's orbit by 10 minutes, with a previous orbit of 11 hours and 55 minutes.
- After the test, the asteroid's new orbit is 11 hours and 23 minutes, shortening it by 32 minutes.
- This proof of concept shows NASA's capability to defend Earth from asteroids.
- Success in using this technology could justify NASA's budget indefinitely.
- Hollywood producers may need new plot lines as asteroid defense was a common theme.
- Beau suggests using climate change as a disaster flick theme to raise awareness.
- Asteroid collisions have occurred in the past with no defense until now.
- This NASA project eliminates worries about asteroid impacts on Earth.
- The successful test showcases the potential world-saving impact of NASA's work.

# Quotes

- "This is literally world saving."
- "They're going to have to come up with something new."
- "You no longer have to worry about that."

# Oneliner

NASA successfully alters asteroid orbit, potentially justifying its budget forever, leaving Hollywood producers scrambling for new plot lines.

# Audience

Space enthusiasts, environmentalists

# On-the-ground actions from transcript

- Support NASA's space exploration and defense projects (implied)
- Raise awareness about the importance of asteroid defense and space research (implied)

# Whats missing in summary

The full transcript provides more detailed information on NASA's Double Asteroid Redirection Test and its potential implications for Earth's defense against asteroids.