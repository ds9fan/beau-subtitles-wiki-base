# Bits

Beau says:

- The Department of Justice in the United States has charged Chinese nationals with engaging in intelligence operations within the country.
- Big Red Kelp, a commentator on Chinese spying, mentioned a "gentleman's agreement" existing in the spy game world.
- In the gentleman's agreement, the actual spy handlers are typically asked to leave the country quietly, instead of facing legal action.
- Disrespecting this agreement could lead to repercussions, as shown by the recent charges against Chinese spies.
- China's attempts to move away from international agreements, like this gentleman's agreement, may have triggered the recent actions by the US.
- The US may be increasing pressure on China due to Russia being perceived as a near-peer competitor.
- The US may use intelligence operations to secure American interests, often for commercial advantage, but with distinctions from China's alleged direct support to a specific company.
- Counterintelligence officers focus on law enforcement and catching foreign intelligence operations within their jurisdiction.
- Intelligence officers, on the other hand, are likened to trained criminals who conduct espionage and other covert activities.
- The US could be gearing up for a more intense intelligence operation against China in response to recent events.

# Quotes

- "They're cops. Think of them like that. They're law enforcement."
- "There's a different frame of mind between the two."
- "They're not spies. They're spy catchers and they have a very different frame of mind."

# Oneliner

The US ramps up intelligence operations against China amid concerns over Chinese espionage and flouting international norms.

# Audience

Policymakers, Intelligence Analysts

# On-the-ground actions from transcript

- Research and understand the implications of international espionage and the enforcement of related agreements (implied)
- Stay informed on developments in global intelligence operations and their impact on national security (implied)
- Advocate for transparency and accountability in international relations to prevent abuses of power (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of recent actions taken by the US against Chinese intelligence operations and sheds light on the dynamics of international espionage.

# Tags

#China #UnitedStates #Espionage #InternationalRelations #IntelligenceOperations