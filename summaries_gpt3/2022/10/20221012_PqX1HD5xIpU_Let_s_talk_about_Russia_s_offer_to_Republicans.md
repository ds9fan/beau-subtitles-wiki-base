# Bits

Beau says:

- Russia offered American states the chance to break away and join Russia, sparking various reactions.
- Mentioned Deputy Tolmachev in the Russian parliament expressing openness to American states joining Russia.
- Raised the scenario of Alaska being annexed by Russia and questioned the US response.
- Criticized those who support Russian intervention in Ukraine but oppose potential Russian interference in the US.
- Pointed out Russia's motive behind the offer as gloating and making fun of the Republican base.
- Discussed how successful Russian influence operations have led American patriots to take positions detrimental to the US.
- Noted that individuals influenced by Russian propaganda may unknowingly act against American interests.
- Urged people to be cautious of pundits echoing Russian talking points and potentially being Russian allies.
- Warned about falling into information silos and unknowingly betraying fellow Americans.
- Suggested reflecting on how one's beliefs and actions may have been influenced by external sources.

# Quotes

- "They got you to sell out your country without you ever even knowing it."
- "You ignored them and de facto allied with them."
- "You're the person I'm talking to."
- "Patriots right?"
- "Everything that you were afraid of, all of those crazy conspiracy theories that y'all hatched, in some ways it kind of occurred."

# Oneliner

Russia offers American states to join, revealing how successful influence operations sow division and weaken American patriotism.

# Audience

American citizens

# On-the-ground actions from transcript

- Question sources of information and be wary of pundits echoing Russian talking points (implied)
- Engage in critical thinking and analysis of political commentary to identify potential foreign influence (implied)

# Whats missing in summary

The full transcript provides a deeper understanding of how foreign influence can exploit divisions within a nation, urging individuals to critically analyze their beliefs and sources of information.

# Tags

#ForeignInfluence #Patriotism #CriticalThinking #RussianPropaganda #PoliticalAnalysis