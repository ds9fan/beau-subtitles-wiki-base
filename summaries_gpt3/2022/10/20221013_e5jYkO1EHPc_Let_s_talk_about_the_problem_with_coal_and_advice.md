# Bits

Beau says:

- Addressing the problem with coal and answering questions raised.
- Expressing the importance of understanding the causes advocated for.
- Providing information on thermal coal being the dirtiest energy source in the U.S.
- Explaining the implications of advocating for coal and supporting workers.
- Clarifying the effects of a strike failure on coal mines.
- Differentiating between thermal and metallurgical coal and their uses.
- Encouraging support for striking workers through a live stream.

# Quotes

- "This is followed by five paragraphs of facts and figures about how dirty coal is as an energy source."
- "I'm pretty sure that most schools of thought, when it comes to the left, support the workers."
- "If the strike fails, the management, the boss, hires new employees at a lower wage with less benefits and worse working conditions."
- "Understand the stuff that is mined at this mine is what makes the type of energy you want."
- "Maybe tune in."

# Oneliner

Beau addresses the problem with coal, advocates for understanding the causes supported, and clarifies misconceptions about coal mining and energy sources.

# Audience

Leftist activists

# On-the-ground actions from transcript

- Tune in to the upcoming live stream to support striking workers (implied).

# Whats missing in summary

Importance of supporting workers in energy production. 

# Tags

#Coal #Energy #LeftistActivism #SupportWorkers #LiveStream