# Bits

Beau says:

- Beau welcomes viewers to 2022 and muses about fictional stories set in the current year.
- He contrasts two visions of the future: the Jetsons' semi-idyllic world and the grim reality portrayed in a story about a cop investigating a company.
- Despite advancements like smart watches, the world is plagued by issues like climate catastrophe, overpopulation, and extreme wealth inequality.
- Beau references "Soylent Green," a story from the same era as the Jetsons, depicting vastly different futures.
- Urging for immediate action, Beau warns that without addressing global inequities and environmental concerns, the future may resemble "Soylent Green" more than the Jetsons.
- He stresses the importance of choosing a course towards a better future and being prepared to adapt to inevitable changes.
- Beau encourages viewers to make 2022 a year of commitment towards a brighter future akin to the Jetsons' world.
- He leaves viewers with a thought-provoking message about the potential for positive change in the current year.

# Quotes

- "We're running out of time to figure out what kind of world we're going to have."
- "If we don't make serious changes, it's definitely going to look more like Soylent Green than the Jetsons."
- "All we have to do really is set a course for some idyllic future and work towards it."
- "We're going to have to adjust and adapt."
- "This could be the year it starts to turn around."

# Oneliner

Beau contrasts two visions of the future in 2022, urging immediate action to prevent a dystopian reality and work towards a brighter, more sustainable world.

# Audience

General public

# On-the-ground actions from transcript

- Commit to making a better future by taking concrete steps towards sustainability and equity (suggested)
- Advocate for policies that address climate change and wealth inequality (implied)

# Whats missing in summary

The full transcript provides a reflective look at past predictions of the future and challenges viewers to actively shape a more positive reality by addressing global issues and embracing change.

# Tags

#Future #2022 #ClimateChange #Inequality #Adaptation