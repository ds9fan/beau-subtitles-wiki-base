# Bits

Beau says:

- Talking about the lost and found, Tucker Carlson, excuses, and silly things worldwide.
- People involved in the Capitol incident on January 6 tried to recover items they dropped.
- Tucker Carlson spun their actions as evidence of innocence, claiming they didn't know they were breaking laws.
- Beau disagrees with this excuse, mentioning a similar incident from 1993 involving a rented truck.
- Individuals at the Capitol on January 6 were misled by rhetoric about patriotism and history.
- They were unaware of the gravity of their actions, thinking they were fighting for their country.
- Beau questions who fed them this rhetoric and left them ill-informed.
- There was a disconnect between their actions and the consequences, as seen by interactions with law enforcement.
- Beau challenges the notion that they were innocent due to lack of understanding.
- Concludes by urging to question those who influenced and misled them.

# Quotes

- "Tucker Carlson spun their actions as evidence of innocence."
- "There was a disconnect between their actions and the consequences."
- "Who fed them that rhetoric? Who left them ill-informed?"

# Oneliner

Beau exposes the dangerous influence of misinformation and false patriotism leading individuals astray in the Capitol incident aftermath.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Challenge misinformation sources and hold them accountable (implied)
- Educate others on the dangers of false narratives and rhetoric (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the consequences of misinformation and blind patriotism, urging accountability and education.

# Tags

#Misinformation #Patriotism #CapitolIncident #Accountability #Rhetoric