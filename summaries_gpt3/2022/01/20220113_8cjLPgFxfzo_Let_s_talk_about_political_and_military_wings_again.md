# Bits

Beau says:

- Addressing concerns and questions about discussing wings and structures.
- Clarifying that the information shared is basic and not secretive.
- Mentioning examples from popular culture where similar structures are depicted.
- Noting a lack of successful right-wing groups adopting similar structures.
- Explaining the ideological differences between left and right-wing groups.
- Describing the challenges right-wing groups face in adopting decentralized structures.
- Pointing out the failure of right-wing groups in using certain strategies in the United States.
- Suggesting a focus on legal maneuvers rather than traditional tactics for right-wing groups.
- Emphasizing the importance of understanding the dynamics of different ideological groups.
- Concluding with a thought about potential future strategies for certain groups.

# Quotes

- "All of them have something in common though. What is it? They're all left wing."
- "It's really hard to operate under a strategy that requires a lot of autonomy when your entire ideology is about hierarchy and not having autonomy."
- "I think in the future what you would really need to be looking for is more of a series of legal maneuvers to affect a self-coup."

# Oneliner

Beau clarifies basic information on wings, contrasts left and right-wing groups, and suggests legal maneuvers over traditional strategies for certain groups.

# Audience

Activists, analysts, researchers

# On-the-ground actions from transcript

- Analyze and understand the dynamics of different ideological groups (implied)

# Whats missing in summary

Beau's detailed analysis and insights on the challenges faced by right-wing groups in adopting certain organizational structures and strategies.

# Tags

#Wings #Structures #Ideology #LeftWing #RightWing