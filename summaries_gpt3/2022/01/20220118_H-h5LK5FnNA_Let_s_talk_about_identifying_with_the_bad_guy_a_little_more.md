# Bits

Beau says:

- Talks about the choice of identifying with the bad guy throughout history.
- Responds to a message challenging the identification of founding fathers as racist due to owning slaves.
- Points out that owning slaves based on race is a clear sign of racism.
- Expresses dislike for the term "founding fathers," suggesting they were not actively involved in parenting the nation.
- Introduces Roger Sherman as a significant figure who played a key role in various foundational documents and never owned slaves.
- Questions why figures like Roger Sherman, who opposed slavery, are not more prominently discussed in history.
- Suggests that the narrative of all founding fathers being slave owners is a deliberate fiction to maintain a certain image.
- Lists examples of founding fathers like John Adams, John Quincy Adams, Thomas Paine, and Benjamin Franklin who did not own slaves or later opposed slavery.
- Dispels the myth that all founding fathers were slave owners and argues that it serves a purpose to maintain a false narrative.
- Addresses the issue of systemic racism, explaining how it can be pervasive and invisible to those benefiting from it.
- Encourages the audience to look for white heroes to identify with and challenges the notion of needing a white hero.
- Raises the question of whether race or moral values should dictate hero identification.

# Quotes

- "If you owned slaves based on race, you are in fact a racist."
- "Systemic racism is something that is so pervasive you don't even realize it's occurring."
- "Why are you looking for a white hero? Is it impossible to identify with a black person?"
- "Pigment more influential in determining who you can identify with, then right or wrong."
- "And if that's the case, you chose to identify with the bad guy."

# Oneliner

Beau challenges the narrative around founding fathers, systemic racism, and hero identification, questioning the choice to identify with the bad guy.

# Audience

History enthusiasts, anti-racism advocates

# On-the-ground actions from transcript

- Research and share information about historical figures like Roger Sherman who opposed slavery (suggested).
- Educate others about the diverse views and actions of founding fathers regarding slavery (suggested).
- Challenge traditional narratives and seek out lesser-known historical figures who stood against injustice (suggested).

# Whats missing in summary

Exploration of the complex narrative surrounding historical figures and the impact of systemic racism on society today.

# Tags

#History #SystemicRacism #HeroIdentification #FoundingFathers #Racism