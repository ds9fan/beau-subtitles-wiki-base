# Bits

Beau says:

- Explains the possible changes to the filibuster being discussed by Schumer due to Republicans not going along with voting rights legislation.
- Notes that the filibuster is a Senate rule, not part of the Constitution, originally intended for ample debate time.
- Schumer is open to changes in the filibuster but not getting rid of it entirely.
- Two options are on the table: creating an exemption for voting rights legislation or implementing a talking filibuster where senators have to actively speak.
- Democrats are hesitant to eliminate the filibuster as they see it as a tool beneficial when not in power.
- Concerns that if exemptions are made, Republicans will do the same for their priorities, ultimately leading to removing the filibuster altogether.
- Beau suggests a talking filibuster with strict rules on staying on-topic about legislation and penalizing falsehoods with an immediate end to the filibuster.
- Changing the filibuster sets a precedent for future alterations by whichever party holds power.
- Points out that the fear of providing Republicans with a tool disappeared once public discourse on changing the filibuster began.
- Beau argues for pursuing the talking filibuster with specific rules since it allows for potential filibustering in critical situations while removing unnecessary roadblocks.

# Quotes

- "It's about power. It's about their party. It's not about the country."
- "Changing the filibuster in and of itself sets the precedent that Republicans can change it next time they're in power."
- "The exemptions, sure, it's easier for this issue, but make no mistake about it, they will carve out exemptions for whatever issue they want next."
- "At this point in history, the filibuster isn't about debate, because no debate is taking place."
- "It's a talking filibuster, no lies, and it has to be about the legislation."

# Oneliner

Schumer considers changes to the filibuster, facing dilemmas on safeguarding voting rights while avoiding creating tools for opponents to exploit.

# Audience

Legislators, political activists

# On-the-ground actions from transcript

- Advocate for a talking filibuster with strict rules to ensure transparency and accountability (suggested).
- Push for changes in the filibuster system that prioritize legislative debate over partisan power struggles (suggested).
- Engage in public discourse and advocacy to support measures that safeguard voting rights while maintaining Senate functionality (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the current filibuster system and potential changes, discussing the implications for future political power dynamics and the importance of transparent legislative processes.