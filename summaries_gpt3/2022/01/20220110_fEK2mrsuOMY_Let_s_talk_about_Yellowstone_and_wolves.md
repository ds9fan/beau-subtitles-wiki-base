# Bits

Beau says:
- Yellowstone wolves faced threats after the federal government delisted gray wolves, shifting protection responsibilities to states.
- Yellowstone lost 20 wolves recently, impacting a pack significantly.
- States can now hunt, trap, and bait wolves outside of federal land, potentially leading to increased threats.
- Losing 20 wolves in a year is the highest since reintroduction efforts began.
- Montana's governor, one of the major offenders, failed to follow regulations and shot a radio-collared wolf near the park.
- Beau urges for wolves to be relisted under federal protection due to states' inadequate safeguarding efforts.
- States show little interest in protecting wolves, necessitating federal intervention for conservation.

# Quotes

- "Losing 20 wolves in a single year, that's the most that has happened since reintroduction efforts began."
- "It's time for the feds to relist the wolf."

# Oneliner

Yellowstone wolves face increased threats as states fail in their conservation duties, urging for federal intervention to protect the species.

# Audience

Conservationists, Wildlife Advocates

# On-the-ground actions from transcript
- Contact local representatives to advocate for relisting wolves under federal protection (exemplified)
- Support organizations working towards wolf conservation efforts (exemplified)

# Whats missing in summary

The full transcript provides a detailed account of the challenges faced by Yellowstone wolves post-delisting and the urgent need for federal intervention to ensure their protection and conservation.

# Tags

#Yellowstone #Wolves #Conservation #FederalProtection #StateResponsibility