# Bits

Beau says:

- Exploring the history of the Jetsons and the idea of an ideal future in the 60s.
- Received messages correcting the misconception that the Jetsons lived in the sky due to pollution.
- Some viewers opposed reboots becoming "woke" with added social commentary.
- The Jetsons initially aired in the 60s, with no pollution-related storyline.
- Pollution and social commentary were added in reboots to raise awareness.
- People may be less accepting of new ideas as they age, influenced by tradition and peer pressure.
- Introducing social commentary in franchises can have long-lasting impacts and raise awareness.
- New generations may not be aware of the original story and see added commentary as part of the plot.
- Social commentary serves as a warning for potential societal consequences.
- Criticism arises from those resistant to change and those who view added commentary as performative.
- Despite criticism, social commentary in reboots can be impactful and shape popular culture.
- Changes and social commentary in franchises can work and influence views over time.

# Quotes

- "If you believe the Jetsons live in the sky because of pollution, you are probably a little bit more in tune to environmental issues."
- "Decades later, people still remember those messages. They become part of popular culture, and they shift views."
- "Changes and social commentary in franchises can work and influence views over time."

# Oneliner

Exploring the history of the Jetsons reveals the impact of added social commentary in reboots and the lasting influence on societal views.

# Audience

Viewers, Franchise Fans

# On-the-ground actions from transcript

- Analyze the social commentary introduced in entertainment and its impact on shaping views (implied).

# Whats missing in summary

The full transcript provides a deeper understanding of the influence of social commentary in entertainment and how it shapes societal perspectives over time.

# Tags

#Jetsons #SocialCommentary #Franchises #Impact #EnvironmentalAwareness