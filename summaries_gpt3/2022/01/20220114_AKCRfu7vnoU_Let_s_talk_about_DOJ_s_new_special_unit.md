# Bits

Beau says:

- Department of Justice announced creating a new unit for domestic terror, sparking questions on its effectiveness and structure.
- New unit won't be a game changer, likely a small group of experts for coordination and information sharing.
- In the US, targeting domestic groups is limited due to freedom of assembly and speech.
- Concerns raised about the potential abuse of power if the government could designate groups.
- Setting up the new unit seems more academic and for coordination rather than a significant tool.
- Beau cautions against pushing for special teams with designation powers due to potential negative consequences.
- FBI and DOJ already respond to domestic incidents swiftly, so additional specialized teams may not be necessary.
- Beau believes the announcement is mostly for PR purposes, lacking a substantial reason.
- The new unit might be useful in preventing civilian targeting but won't eradicate movements in the US.
- Overall, Beau doesn't see this announcement as a significant development.

# Quotes

- "This isn't going to be a game changer."
- "That kind of power, under the control of the executive branch, is a really bad idea."
- "But it might be a useful tool for people who are trying to stop the targeting of the civilians."

# Oneliner

Department of Justice's new unit for domestic terror won't be a game changer due to limitations on targeting groups and potential abuse of power.

# Audience

Policy analysts, activists

# On-the-ground actions from transcript

- Advocate for community-based solutions to address domestic terror (implied)
- Engage in dialogues and workshops on civil rights and freedoms (implied)

# Whats missing in summary

Detailed examples and elaboration on the potential consequences of giving the government power to designate groups.

# Tags

#DOJ #domesticterror #abuseofpower #freedomofspeech #communitypolicing