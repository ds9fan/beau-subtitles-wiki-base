# Bits

Beau says:

- Exploring Mitch McConnell's tribute to Malcolm X and the confusion in society's upper echelons.
- McConnell's dismissive comment about African American voting rates and Malcolm X's powerful words on Americanism.
- The disconnect between the establishment and the struggles of the common folk.
- The failure of the elite to understand the diminishing patriotism and discontent in America.
- The detachment of the rich from the realities of socioeconomic struggles.
- Lack of access to healthcare, exploitation of soldiers, and voter suppression go unnoticed by the elite.
- Ignorance towards voting rights legislation, college costs, and the propaganda around education.
- Elite's disconnect from the American dream turning into a nightmare for many.
- Commonality among those at the bottom, irrespective of demographics, in facing the American nightmare.
- The elite's focus on legislating patriotism, pushing college narratives, and restricting education to maintain ignorance.

# Quotes

- "I'm speaking as a victim of this American system."
- "They don't live in a world where what they notice is the rich getting richer and the poor getting poorer."
- "If they want people to respect the country, they have to create a nation worth loving."
- "If this government cannot secure voting rights, you're not going to find a lot of patriotism."
- "But if there was a single person who could sum up what Malcolm X was talking about, it's probably Mitch McConnell."

# Oneliner

Beau delves into the disconnect between the elite and the struggles of the common folk, echoing Malcolm X's words and questioning the lack of patriotism in an America turning into a nightmare.

# Audience

Activists, Progressives, Voters

# On-the-ground actions from transcript

- Advocate for voting rights legislation (implied)
- Support initiatives to make college education more affordable (implied)
- Fight against voter suppression tactics (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the disconnect between the elite and the struggles faced by many in society, offering a critical perspective on patriotism and the American dream.

# Tags

#MitchMcConnell #MalcolmX #AmericanDream #Patriotism #VotingRights