# Bits

Beau says:

- School board in Tennessee decided to ban the graphic novel "Mouse," set in a dark period of history and told through animals, mice.
- Author attempted to address objections in good faith, considering each argument.
- Some arguments against the novel, like a mouse with no clothes, are not made in good faith.
- Conservative circles' reactions to animated characters without clothes show the hypocrisy in these arguments.
- The objections are not about the novel's content or theme; it's about something else.
- Beau suggests that educators are hiding the failure to recognize rising authoritarianism in the country by banning such books.
- The decision to ban the book is driven by a desire to preserve closely held beliefs and avoid embarrassment for failing to recognize authoritarian signs.
- Beau believes the ban aims to keep students ignorant and incapable of recognizing authoritarianism.
- Banning books like "Mouse" creates a generation blind to the signs of authoritarianism, leading to long-term impacts.
- The real danger lies in creating a generation unable to identify and resist authoritarian rhetoric and actions.

# Quotes

- "Banned books are the best books."
- "Please do not take these arguments in good faith."

# Oneliner

Beau sheds light on the true motives behind banning "Mouse" graphic novel, revealing a dangerous attempt to keep students ignorant of authoritarianism signs.

# Audience

Students, educators, community members

# On-the-ground actions from transcript

- Challenge book bans in schools (suggested)
- Advocate for diverse literature in education (suggested)

# Whats missing in summary

The emotional impact and depth of analysis in discussing the dangers of banning books like "Mouse" and the implications for future generations.

# Tags

#BookBans #Education #Authoritarianism #CriticalThinking #Literature