# Bits

Beau says:

- Carhartt expressed support for vaccines, sparking backlash on social media.
- Some individuals responded by setting Carhartt products on fire to show their disapproval.
- Beau questions the reasoning behind this extreme response.
- He suggests that those burning the products are not targeting employees but rather expressing dissatisfaction with leadership decisions.
- Beau acknowledges the right to protest and burn items as a form of expression.
- He reminds people that such actions don't directly impact the company but serve as symbolic protests.
- Burning a symbol is meant to convey discontent with those in charge, not harm the company financially.
- Beau urges individuals to understand the symbolic nature of their actions and their intended message.
- He encourages reflection on attachment to symbols and the need to separate them from their actual meaning.
- Beau concludes by leaving the audience with a thought to ponder on the importance of symbols and their representation.

# Quotes

- "You're not attacking Carhartt. You're not attacking the employees. In your mind, you're trying to make it better for them, right?"
- "It's a symbol. And it's a way that you are expressing your displeasure."
- "Perhaps you've grown a little too attached to those symbols as well."
- "Just a thought."
- "Y'all have a good day."

# Oneliner

Beau questions the symbolic burning of Carhartt products in protest, urging reflection on separating symbols from their actual meaning.

# Audience

Online activists

# On-the-ground actions from transcript

- Contact Carhartt to express views on their public statements (suggested)
- Engage in respectful dialogues with others about the significance of symbols (exemplified)

# Whats missing in summary

The full transcript provides additional insights into the dynamics of symbolic protests and the importance of understanding the intended message behind such actions.

# Tags

#Protest #Symbolism #Expression #CorporateResponsibility #SocialMedia