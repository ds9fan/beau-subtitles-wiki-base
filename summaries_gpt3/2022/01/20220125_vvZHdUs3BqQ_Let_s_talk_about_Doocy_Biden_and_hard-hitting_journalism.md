# Bits

Beau says:

- A Fox News reporter shouted a question at President Biden as he was leaving, asking if high inflation is a political liability for the incumbent party.
- President Biden responded, saying high inflation is a great asset, followed by a derogatory comment towards the reporter.
- Beau criticizes the question as not being hard-hitting journalism but a "gotcha" question designed for a soundbite.
- Beau believes journalism should provide new information and uncover things rather than aim for sensational soundbites.
- He suggests that the purpose of journalism is to inform and not to incite.
- Beau states that journalists should be educators, but many viewers prefer sensationalism over informative content.
- He points out that both sides of the aisle have criticized the reporter, indicating a bipartisan agreement on the nature of the question.
- While Beau admits finding humor in Biden's response, he acknowledges that it was not fitting behavior for the president.
- Beau concludes by stating that news outlets that do not inform are actually inflaming their audience and serving their own agenda rather than providing a service.

# Quotes

- "Questions like this don't inform, which should be the purpose. They inflame."
- "Journalists should be educators."
- "If your news outlet is not informing you, it's inflaming you."

# Oneliner

Beau criticizes a "gotcha" question posed to President Biden, advocating for journalism that informs rather than incites.

# Audience

Journalists, News Viewers

# On-the-ground actions from transcript

- Hold news outlets accountable for providing informative content (implied)
- Support journalism that educates and informs the public (implied)

# Whats missing in summary

The full transcript provides a deeper analysis of the role of journalism in informing the public and the dangers of sensationalism in news reporting.