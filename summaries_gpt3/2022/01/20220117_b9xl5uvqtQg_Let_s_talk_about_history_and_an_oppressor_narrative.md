# Bits

Beau says:

- Expresses support for a theory misunderstood by the right wing as a historical theory rather than legal studies.
- Receives message calling the theory racist garbage that divides people along race lines.
- Points out the historical struggle between oppressor and oppressed classes throughout history.
- Emphasizes that history is not a story but what actually happened.
- Asserts that history must be accurate to teach lessons and draw parallels to the present.
- States that the narrative of oppressed versus oppressor is accurate and not wrong to teach.
- Explains that being neutral in the struggle between oppressor and oppressed sides is choosing the side of the oppressor.
- Argues that history books must accurately depict the dynamic between oppressed and oppressor.
- Challenges the discomfort felt when reading history books and the identification with oppressors rather than those seeking change.
- Encourages reflection on why one identifies with the oppressors in history.

# Quotes

- "History can teach lessons, but it should never be brought back as a tool of division and accusation."
- "If you're neutral in that struggle that has existed throughout the human experience, then you have chosen the side of the oppressor."
- "If you are reading something that claims to be a history book, and it doesn't make you uncomfortable, you're not reading a history book."
- "If you read these history books, and you identify with the slavers, rather than those who helped the Underground Railroad, that says something about you."
- "It's not manufactured, point to a time in American history where that dynamic wasn't at play."

# Oneliner

Beau explains the accurate depiction of history as a narrative of oppressed versus oppressor and challenges readers to identify with those seeking positive change.

# Audience

History learners and critical thinkers.

# On-the-ground actions from transcript

- Analyze history books for accuracy and discomfort (implied).
- Identify with those seeking positive change in history (implied).

# Whats missing in summary

The full transcript provides a detailed exploration of historical narratives and challenges readers to critically analyze their perspectives on historical events.

# Tags

#History #Narratives #Oppression #CriticalThinking #Education