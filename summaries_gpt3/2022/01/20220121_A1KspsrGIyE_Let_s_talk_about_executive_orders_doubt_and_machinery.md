# Bits

Beau says:

- Talks about a draft executive order that proposed the seizure of voting machines in the 2020 election based on baseless claims.
- Points out the lack of evidence to support these claims and how they were fabricated.
- Describes the attempt to have Pence discard legitimate votes and electors in favor of an alternate slate to retain power.
- Criticizes those who supported these efforts and questions their understanding and motives.
- Urges for accountability and a swift investigation into the events surrounding the attempt to overturn the election.
- Warns that without consequences, failed attempts become practice and set a dangerous precedent.
- Labels the events of January 6th as a coup attempt, acknowledging the level of organization behind it.
- Emphasizes the importance of revealing the truth to the American people about the threat their democracy faced.
- Expresses concern that without accountability, future attempts to undermine democracy may succeed.
- Stresses the need to act quickly and decisively to prevent similar events in the future.

# Quotes

- "They're baseless claims."
- "This was an attempt to drive a stake into the heart of the Federal Republic."
- "Without accountability, it's not failed, it's practice."
- "It's time to show the American people what almost happened to their country."
- "The evidence that is being released piecemeal is backed up by more."

# Oneliner

Beau warns of a dangerous coup attempt, stresses the importance of accountability, and urges swift action to protect democracy.

# Audience

American citizens, Activists

# On-the-ground actions from transcript

- Demand accountability from elected officials (suggested)
- Support and push for a thorough investigation into the events of January 6th (exemplified)
- Educate others about the dangers of undermining democratic processes (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the attempted coup, stressing the need for accountability and swift action to protect democracy.

# Tags

#CoupAttempt #Accountability #Democracy #AmericanPeople #Investigation