# Bits

Beau says:

- Disclosing year-old news slowly to lessen surprise during hearings about a group taking over a building in D.C.
- During a coup attempt, Beau live tweeted and provided context and background information.
- Reports of specific helicopters being used by elite forces to retake the building.
- Questions about the legality of using active duty military against American citizens.
- Elite forces were seconded to the Department of Justice, not the National Guard.
- Military operators under the Attorney General's control operated under unknown contingency plans.
- The National Mission Force, including FBI's hostage rescue and render safe teams, was present.
- Military operators were ready to respond but not for crowd control.
- In emergencies, laws become flexible and gray.
- Elite military units wouldn't stand idly by during a coup attempt.

# Quotes

- "Laws become very flexible. They become very gray."
- "The most elite units of this country's military wouldn't stand idly by during a coup attempt."
- "The echo chambers that exist online can be dangerous."

# Oneliner

Disclosing year-old news slowly to lessen surprise during hearings about elite military forces involved in responding to a coup attempt.

# Audience

Activists, Advocates, Researchers

# On-the-ground actions from transcript

- Read the Newsweek article titled "Secret Commandos Will Shoot to Kill Authority Were at the Capitol" (suggested)
- Educate others on the flexibility of laws in emergencies (exemplified)

# Whats missing in summary

The full transcript provides a deep dive into the involvement of elite military forces during a coup attempt, shedding light on the flexibility of laws in emergencies and dispelling misconceptions about military support based on political affiliations.

# Tags

#CoupAttempt #EliteForces #MilitaryInvolvement #FlexibilityOfLaws #EchoChambers