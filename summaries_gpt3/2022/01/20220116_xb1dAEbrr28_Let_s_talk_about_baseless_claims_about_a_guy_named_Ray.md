# Bits

Beau says:

- Addresses a baseless claim surrounding a person named Ray that has gained traction among conservatives.
- Explains the lack of evidence supporting the claim that Ray was working for the government and responsible for the events on January 6th.
- Suggests a way to address conservatives asking about Ray Epps by questioning their stance on his arrest and actions.
- Compares the situation with Ray Epps to holding political leaders accountable for encouraging but not physically participating in events.
- Encourages challenging the belief that a single person without political influence could incite a crowd, raising questions about the power of influential figures' words.
- Notes the absence of concrete evidence to support the claim about Ray Epps, making it challenging to debunk.
- Raises the point that if Ray Epps warrants arrest, then many others in the Republican Party could also be implicated.
- Advocates for probing deeper into the logic behind calling for Ray Epps' arrest and drawing parallels with other political figures' actions.

# Quotes

- "You think he should be arrested?"
- "What did he do that warrants arrest?"
- "They are making the case to charge Trump."
- "Isn't that what Trump did?"
- "If Epps warrants arrest, there's a whole bunch of people in the Republican Party that also warrant arrest."

# Oneliner

Addressing a baseless claim about Ray Epps and questioning the accountability of political leaders for incitement without physical participation on January 6th.

# Audience

Those addressing baseless claims.

# On-the-ground actions from transcript

- Ask individuals questioning baseless claims to justify their beliefs (suggested).
- Challenge misconceptions by drawing parallels with other political figures' actions (exemplified).

# Whats missing in summary

The full transcript provides detailed guidance on addressing baseless claims effectively and engaging in critical discourse with individuals questioning such claims.

# Tags

#BaselessClaims #Accountability #CriticalThinking #PoliticalLeaders #January6th