# Bits

Beau says:

- The committee investigating the sixth has issued 14 subpoenas in seven states for individuals identified as alternate electors.
- Politicians may not fully comprehend the laws they have passed, making statements asserting their actions were legal in support of Trump's endeavors.
- Beau provides an analogy involving Bob and Jim to illustrate how seemingly legal actions, when done in furtherance of illegal activities, can lead to criminal liability.
- There is no specific statute for attempting a self-coup, but criminal charges could be brought based on related concepts.
- Individuals who claimed their actions were legal in supporting Trump's election overturn might face legal consequences.
- Beau acknowledges his layman status and uncertainty, offering his perspective as a thought rather than legal advice.

# Quotes

- "I'm starting to think that some of these politicians, they may not understand some of the laws that they have passed."
- "And I only did this so Trump could do what he's doing."
- "What I did was legal. And I did it to support the president who was trying to overturn the election."
- "That may have not been a sound legal strategy."
- "I could be wrong about this. It's just a thought."

# Oneliner

The committee issues subpoenas for alternate electors as politicians assert legality in supporting Trump's actions, potentially facing legal consequences.

# Audience

Legal analysts

# On-the-ground actions from transcript

- Contact legal experts for clarification on potential legal liabilities (suggested)
- Join organizations advocating for legal accountability in political actions (implied)

# Whats missing in summary

Insights on potential legal implications and accountability for individuals involved in supporting actions to overturn elections.

# Tags

#Subpoenas #LegalConsequences #PoliticalActions #Trump #ElectionFraud