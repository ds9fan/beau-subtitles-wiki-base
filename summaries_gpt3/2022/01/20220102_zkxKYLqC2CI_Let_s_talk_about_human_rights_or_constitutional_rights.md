# Bits

Beau says:

- Settling a debate between a father and daughter over constitutional rights and human rights.
- Daughter believes in Universal Declaration of Human Rights, father believes in US founding documents.
- Father argues that Constitution gives authority to sign the declaration, daughter thinks Constitution is outdated.
- Beau appreciates the beautiful language in the US founding documents but acknowledges flaws.
- Points out the similarities between the Universal Declaration of Human Rights and the US Constitution.
- Notes that both documents acknowledge existing rights rather than granting them.
- Suggests that if people read each other's preferred document, the debate might not exist.
- Advocates for choosing the document that provides the most rights in any given situation.
- Emphasizes that both human rights and constitutional rights aim to protect individuals from government actions.
- Encourages the family to have a night to compare and understand both documents.

# Quotes

- "At the end of the day, human rights are constitutional rights. There's not a difference."
- "I don't believe that either one of you have read the others preferred document."
- "Both of these documents are intended to shield people from the actions of their governments."
- "I don't really think this should be an argument, to be honest."
- "Y'all should plan a family night and sit down with both documents and see if you can figure out where they overlap."

# Oneliner

Settling a debate on constitutional vs. human rights, Beau finds common ground and encourages understanding.

# Audience

Father and daughter

# On-the-ground actions from transcript

- Plan a family night to sit down with both documents and compare where they overlap (suggested)

# Whats missing in summary

The full transcript provides a detailed comparison between the Universal Declaration of Human Rights and the US Constitution, urging for understanding and common ground.

# Tags

#Debate #HumanRights #ConstitutionalRights #Comparison #Understanding