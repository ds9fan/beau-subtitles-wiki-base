# Bits

Beau says:

- NPR compiled a list of candidates running for Secretary of State who spread misinformation or believed baseless claims about the 2020 election.
- These candidates hold the power to count votes and potentially alter election outcomes.
- Candidates who believed in misinformation may intervene due to a perceived moral duty.
- Those running for Secretary of State positions must have good judgment and understanding of election processes.
- The Democratic Party must prioritize winning these Secretary of State elections to secure fair elections.
- Historically, Secretary of State elections have not received much attention but are critical for election integrity.
- Beau urges viewers to read the article for a list of candidates and states they are running in.
- Residents in states with concerning candidates should get involved in electoral politics to make a difference.
- Beau stresses the long-term importance of these elections in maintaining election integrity.

# Quotes

- "These are must-win elections."
- "If you live in one of those states, and you have ever desired to become active in electoral politics, now is your time."

# Oneliner

NPR's list reveals Secretary of State candidates spreading misinformation about elections, urging Democratic Party action in must-win elections.

# Audience

Democratic Party members

# On-the-ground actions from transcript

- Get involved in electoral politics in your state (implied)
- Prioritize winning Secretary of State elections for fair and secure elections (implied)

# Whats missing in summary

The urgency and importance of active participation in upcoming elections can best be grasped by watching the full transcript. 

# Tags

#SecretaryOfState #ElectionIntegrity #DemocraticParty #GetInvolved #Misinformation