# Bits

Beau says:

- Exploring the structure of movements, particularly the division between the political wing and militant wing within movements seeking to change governance by force.
- The militant wing uses violence or the threat of violence to influence and achieve their goals, while the political wing provides cover and condemns the actions while advocating for the goals.
- Plausible deniability is key for the political wing to distance themselves from the militant wing, making it easier for them to be reelected.
- Communication between the political and militant wings is usually indirect, through dead drops or cutouts, to maintain separation and plausible deniability.
- The division between the political and militant wings is vital for the functioning and safety of the movement.
- Failure to maintain the division could lead to exposure and legal consequences, which may explain why certain individuals are not coming forward to make their case.
- Beau suggests that there may have been communication links between members of Congress and militant movements involved in the events of January 6th.

# Quotes

- "Plausible deniability is key."
- "The division between the political and militant wings is vital."
- "Failure to maintain the division could lead to exposure."
- "Communication between wings is usually indirect."
- "I have a feeling we are going to find out."

# Oneliner

Exploring the structure of movements and the importance of maintaining division between political and militant wings to ensure plausible deniability and safety.

# Audience

Observers, Activists, Politicians

# On-the-ground actions from transcript

- Investigate potential communication links between members of Congress and militant movements (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the structure and dynamics of movements, shedding light on the importance of maintaining separation between different wings for the movement's integrity and success.

# Tags

#Movements #PoliticalWing #MilitantWing #Communication #PlausibleDeniability