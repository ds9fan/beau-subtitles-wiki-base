# Bits

Beau says:

- Fulton County in Georgia is impaneling a special grand jury to look into disruptions after the election, including Trump's actions.
- The special grand jury in Georgia won't indict Trump but can make recommendations to the prosecutor.
- In Southern politics, there is a tradition of not pointing fingers at fellow party members, even if they broke the law.
- A special grand jury in Georgia has subpoena power and can demand records from government agencies.
- The grand jury will likely provide political cover for those who want to help with the investigation without risking their careers.
- The prosecutor may have already decided to try to get Trump indicted, and the grand jury will help gather evidence for this.
- The process is seen as more of an investigative tool rather than a means to directly indict Trump.
- Beau believes that crimes were committed based on tape evidence, but the grand jury will determine the legal implications.
- The grand jury convenes at the beginning of May and is good for a year, but Beau doesn't think it will take that long.

# Quotes

- "A special grand jury in Georgia is not a normal grand jury."
- "If you walk in and you put your hand on that Bible and you swear before God that you're going to tell the truth, the whole truth and nothing but the truth, you had better do that."
- "It has subpoena power. It has the ability to demand records from government agencies, stuff like that."
- "So if you are sworn to tell the whole truth, well, that overrides everything."
- "Y'all have a good day."

# Oneliner

Fulton County in Georgia convenes a special grand jury to look into post-election disruptions, providing political cover and investigative tools without directly indicting Trump.

# Audience

Georgia residents

# On-the-ground actions from transcript

- Contact local organizations or legal aid to understand how you can support the process (suggested)
- Attend community meetings or events to stay informed about the investigation (suggested)

# Whats missing in summary

Legal nuances and specific details about the potential outcome of the investigation.

# Tags

#Georgia #FultonCounty #SpecialGrandJury #Trump #Investigation