# Bits

Beau says:

- Representative Thomas Massey of Kentucky mistakenly tweeted a misattributed quote that has been widely circulated.
- The misattributed quote implied that criticizing a certain individual indicates who is ruling over you.
- Massey's attempt to criticize Fauci using this quote doesn't hold up logically because he openly criticizes Fauci, like many others.
- Beau points out that the true way to identify those trying to rule over you is by recognizing those who spread fear, blame, powerlessness, and victimhood.
- Beau suggests looking out for those who try to make you scapegoat others and direct your anger towards them as a means of control.
- The quote wrongly attributed to Voltaire about criticizing rulers doesn't make sense in the context of identifying true rulers.
- Beau stresses the importance of not falling for absurd rhetoric that leads to atrocities and the manipulation of fear to gain control.
- Beau concludes by encouraging critical thinking and awareness in recognizing true attempts at ruling over individuals.
- The core message revolves around identifying manipulative tactics of fear and blame in order to understand who is truly attempting to rule over you.

# Quotes

- "Those who can make you believe absurdities can make you commit atrocities."
- "Watch for people who are trying to make you feel like a victim when you're not."
- "Watch for those who are trying to scapegoat individuals or groups of people."
- "That's how you find out who is trying to rule over you."
- "Y'all have a good day."

# Oneliner

Beau explains the misattributed quote by Massey, urging to look out for those manipulating fear and blame to identify true rulers.

# Audience

Critical Thinkers

# On-the-ground actions from transcript

- Identify and challenge fear-mongering tactics in your community (implied).
- Refrain from falling into scapegoating behaviors and encourage others to do the same (implied).

# Whats missing in summary

The full transcript provides a comprehensive breakdown of the misattributed quote by Representative Massey and offers insight into the manipulation tactics used by those attempting to rule over individuals.

# Tags

#MisattributedQuote #ManipulationTactics #CriticalThinking #FearAndBlame #CommunityLeadership