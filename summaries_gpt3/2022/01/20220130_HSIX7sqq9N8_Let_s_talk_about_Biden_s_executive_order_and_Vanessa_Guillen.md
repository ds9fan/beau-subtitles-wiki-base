# Bits

Beau says:

- Biden signed an executive order in response to the Vanessa Guillen case at Fort Hood.
- The executive order makes sexual harassment a crime under the UCMJ, changing the handling of such cases.
- Previously, sexual harassment was handled within the chain of command, leading to issues like cover-ups and retaliation.
- With the new order, if someone covers up harassment, they can end up in cuffs, and cases can now be pursued outside the chain of command.
- Cases will now be investigated by CID instead of through administrative proceedings.
- The executive order will immediately change how cases are handled and investigated.
- It will likely be a significant tool in combatting sexual harassment in the military.
- Tracking of allegations of retaliation will also be implemented, likely referred to CID.
- CID has decided to increase the ratio of civilian investigators, which could lead to more effective investigations.
- The pressure applied after the Vanessa Guillen case led to these changes.

# Quotes

- "It is going to matter."
- "This is something that will have a positive impact."
- "While this doesn't actually bring justice to Vanessa Guillen, it may make the military a little bit more just."

# Oneliner

Biden's executive order in response to Vanessa Guillen case makes sexual harassment a crime under UCMJ, changing how cases are handled and investigated, a significant step in combatting military sexual harassment.

# Audience

Advocates, military personnel

# On-the-ground actions from transcript

- Reach out to Senate Armed Services and DOD directly to keep pressure for positive changes (implied).
- Support civilian investigators in handling cases effectively (implied).

# Whats missing in summary

The emotional impact and perseverance shown by those who pushed for change after the Vanessa Guillen case.

# Tags

#Military #SexualHarassment #VanessaGuillen #UCMJ #CID