# Bits

Beau says:
- Introduces the topic of Clyde Bellacourt and his significant impact.
- Expresses disappointment in the media coverage of Clyde Bellacourt.
- Emphasizes Clyde Bellacourt's continuous involvement in fighting for change.
- Details Clyde Bellacourt's contributions, including co-founding AIM and participating in the occupation at Wounded Knee.
- Mentions Clyde Bellacourt's efforts in setting up patrols to monitor law enforcement and curb police brutality.
- Talks about Clyde Bellacourt's initiatives in job training, medical care, and establishing funds for healthcare.
- Notes the resourcefulness of Clyde Bellacourt in achieving these initiatives without modern technology.
- Describes Clyde Bellacourt's establishment of aid networks, schools, summer programs, and fresh produce networks.
- Mentions Clyde Bellacourt's unique approaches in addressing substance issues with the youth.
- Acknowledges Clyde Bellacourt's legacy and his impact on future movements like Standing Rock.

# Quotes

- "Clyde Bellacourt, he helped co-found AIM in 1968, and he did participate in the occupation at Wounded Knee."
- "He's Clyde Bellacourt, and that's how you'll find him."
- "A man that spent decades in the fight trying to make things better, not just for his demographic, but for everybody as a whole."

# Oneliner

Beau introduces Clyde Bellacourt's impactful legacy, detailing his continuous efforts in fighting for change and establishing community programs, setting a precedent for future movements.

# Audience

Activists, History Buffs, Community Leaders

# On-the-ground actions from transcript

- Research Clyde Bellacourt's life and legacy to understand his impact (suggested).
- Support community programs and aid networks in your area (exemplified).
- Advocate for culturally relevant education programs and preservation of native languages (implied).

# Whats missing in summary

The full transcript provides a deeper insight into Clyde Bellacourt's life, legacy, and the importance of recognizing his continuous fight for change and community empowerment.

# Tags

#ClydeBellacourt #AIM #CommunityEmpowerment #Activism #Legacy