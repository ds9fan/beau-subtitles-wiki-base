# Bits

Beau says:

- Leadership and command are distinct; leadership is found on the left while command is prevalent on the right due to hierarchy and perceived authority.
- Command is built little by little through small, seemingly insignificant orders that establish compliance and build towards following larger orders.
- The essence of command lies in obeying perceived authority rather than the action itself.
- A key rule of command is to never issue an order that won't be obeyed, as it undermines authority.
- Former President Trump's commands are increasingly being ignored, with some even leading to him being booed.
- Trump's recent command for followers to leave Twitter and Facebook due to being "radical left" is likely to be disregarded.
- If Trump continues down this path, he risks rendering himself irrelevant, becoming just an elderly person shouting at the internet.
- The question arises of who will assume command after Trump and capitalize on the movement that surrounded him.
- Beau suggests watching for prominent Republican figures, potentially governors, who can effectively give and have commands followed as potential successors to Trump.

# Quotes

- "It doesn't matter because it's not the action that matters. It's obeying that perceived authority."
- "Former President Trump has been issuing a lot of commands lately and a lot of them are being ignored."
- "If former President Trump continues on his current path, he will order himself into an irrelevant position."

# Oneliner

Leadership vs. command: Beau dissects the distinction, Trump's waning authority, and potential successors in the political landscape.

# Audience

Political Observers

# On-the-ground actions from transcript

- Watch for prominent Republican figures giving and having commands followed as potential successors to Trump (implied).

# Whats missing in summary

Further insights on the implications of shifting command dynamics and the impact on political movements.

# Tags

#Leadership #Command #Hierarchy #PerceivedAuthority #PoliticalMovements