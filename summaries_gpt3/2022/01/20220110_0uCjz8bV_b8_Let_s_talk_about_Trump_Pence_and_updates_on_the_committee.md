# Bits

Beau says:

- Update on committee activities investigating the events of the 6th.
- Confirmation of suspected details regarding the events.
- Americans likely not shocked by findings, with most understanding what occurred on that day.
- Description of the events on the 6th as a self-coup.
- Details of Trump pressuring Pence to overturn the election.
- Pence's refusal to comply with Trump's demands.
- Former press secretary Grisham's cooperation with the committee and providing valuable information.
- Committee engaging with lower-level individuals who are cooperating.
- Subpoena of the pillow salesman's phone records by the committee.
- Public questioning of the Department of Justice's actions in response to the committee's investigations.
- Positive note on how the investigation is being conducted without leaks or a play-by-play.
- Interest in Vice President Pence as a witness and potential cooperation.
- Acknowledgment of luck and individuals' moral compasses in preventing certain outcomes.
- Reflection on the importance of individuals standing their ground in critical positions.

# Quotes

- "You don't understand, Mike. You can do this. I don't want to be your friend anymore if you don't do this."
- "They tend to not notice the help. They tend to not watch what they say in front of them."
- "A lot of this was luck."

# Oneliner

Beau provides insights into the committee's investigations into the events of the 6th, detailing pressure on Pence and the importance of individuals maintaining their moral compass.

# Audience

Committee members and concerned citizens.

# On-the-ground actions from transcript

- Contact your representatives to express support for thorough investigations (suggested).
- Stay informed about the developments surrounding the events of the 6th (implied).

# Whats missing in summary

Insights on potential future revelations and implications from ongoing investigations.

# Tags

#CommitteeInvestigations #TrumpAdministration #Pence #JusticeDepartment #MoralCompass