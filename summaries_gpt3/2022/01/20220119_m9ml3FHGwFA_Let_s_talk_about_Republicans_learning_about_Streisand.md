# Bits

Beau says:

- The Republican Party in the United States demonizes education, trying to make ignorance a duty and knowledge a crime.
- They have created lists of objectionable books and want to teach mythology instead of history.
- Students' response has been strong, with essays and banned book clubs forming in protest.
- An eighth grader in Pennsylvania started a banned book club, inspiring activism in the younger generation.
- Beau addresses a common question from high school students on how to start community networks.
- He praises the idea of meeting to read banned books as a litmus test for potential activists.
- Reading and discussing banned books can lead to real-world activism and community involvement.
- Beau encourages high school students to find like-minded peers through book clubs as a safe way to get involved.
- He contrasts adults afraid of education with youth advocating for knowledge and learning.
- The youth are pushing back against attempts to limit education and information access.

# Quotes

- "The response from students has been more pronounced than the response from a lot of people in the Democratic Party."
- "I would like to thank the Republican Party for creating the next generation of activists."
- "Band books are the best books."
- "Those are people that want to make the world a better place and are willing to invest the time to do so."
- "Welcome to the information age and the Streisand effect."

# Oneliner

The Republican Party demonizes education, but students fight back with banned book clubs, inspiring activism and community involvement among the youth.

# Audience

High school students

# On-the-ground actions from transcript

- Start a banned book club at your school to encourage activism and community involvement (suggested)
- Read and openly discuss objectionable books to challenge censorship and inspire others to join in activism (suggested)

# Whats missing in summary

The full transcript provides more context on the importance of education, activism, and community involvement, along with a reflection on the contrast between adults' fear of education and youth's advocacy for knowledge.