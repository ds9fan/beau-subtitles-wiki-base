# Bits

Beau says:

- Explains a federal case currently ongoing involving three officers with Derek Chauvin.
- Mentions the defense's strategy of claiming they were following Chauvin's orders.
- Criticizes the defense's reliance on the "just following orders" excuse.
- Points out that officers should not prioritize their careers over justice.
- Predicts that this defense may be used by higher-ranking individuals involved in the events of January 6th.
- Suggests that if a jury doesn't hold these officers accountable, there may be a backlash.
- Speculates on the potential transformative impact of George Floyd's death on future trials.
- Raises the possibility that the outcome of this trial could influence the future of the United States as a democracy.

# Quotes

- "If you're willing to allow what happened to Floyd to happen for the sake of your career, you probably shouldn't be a cop."
- "My guess is that there's going to be a backlash."
- "It's wild to think that his death may be even more transformative for this country."
- "Putting a lot on one person there."
- "Y'all have a good day."

# Oneliner

Beau explains how the defense strategy in George Floyd-related trials may impact the future of accountability and democracy in the United States.

# Audience

Jury members, activists, citizens.

# On-the-ground actions from transcript

- Hold law enforcement accountable (exemplified).
- Advocate for police reform (implied).
- Support systemic changes (implied).

# Whats missing in summary

Full context and depth of Beau's analysis on the impact of George Floyd's case on the ongoing federal trial and future implications. 

# Tags

#GeorgeFloyd #Accountability #PoliceReform #Justice #Democracy