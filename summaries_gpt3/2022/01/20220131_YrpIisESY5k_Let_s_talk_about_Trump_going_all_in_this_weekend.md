# Bits

Beau says:

- Former President Trump has broken from his typical playbook by indicating he might grant pardons to those involved in the January 6th actions if he were to be re-elected.
- Trump's recent actions, including hinting at potential pardons and suggesting he hopes for nationwide protests if criminal charges are brought against him, are attempts to cast investigations into him as political rather than legal.
- Despite indications from Trump, there is bipartisan support to update the Electoral Act of 1887, not change election results.
- Trump's statements about Vice President Mike Pence's role in the election results read like a confession to some, as he suggests Pence had the power to overturn the election.
- Trump's deviation from the typical playbook of wannabe dictators, by openly admitting to wanting to change election results, indicates potential criminal liability and a slipping standing with his base.

# Quotes

- "If you know there's a con man in the room but you don't know who the mark is, you're the mark."
- "Trump is not a planner, so when he's not following somebody else's plan, he's probably really likely to make some mistakes."
- "He may even incriminate himself when it comes to some of the things that he might have criminal liability for."

# Oneliner

Former President Trump breaks from playbook, hinting at pardons and admitting desire to change election results, potentially indicating criminal liability and slipping base support.

# Audience

Political observers, concerned citizens

# On-the-ground actions from transcript

- Contact representatives to support updating the Electoral Act of 1887 (suggested)
- Stay informed about political developments and hold leaders accountable (exemplified)

# Whats missing in summary

Analysis of potential future implications and consequences of Trump's deviation from the typical playbook.

# Tags

#FormerPresidentTrump #ElectionResults #Pardons #CriminalLiability #PoliticalStrategy