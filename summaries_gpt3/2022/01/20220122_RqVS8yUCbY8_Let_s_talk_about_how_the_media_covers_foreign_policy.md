# Bits

Beau says:

- Received messages about different perspectives on Russian and U.S. postures in foreign policy.
- Avoids injecting personal feelings into coverage to provide unbiased information.
- Media tends to cheerlead and establish narratives rather than inform objectively.
- Tucker Carlson's misunderstanding of NATO's purpose and history.
- NATO's existence justified by fear-mongering and the need for mutual defense.
- Foreign policy is not about good guys versus bad guys but about power and interests.
- Ideology and morality take a backseat to power in international relations.
- Consistently against all wars unless necessary for civilian life preservation.
- Power structures determine war decisions, not necessarily morality.
- When consuming media, be aware of framing and focus on understanding power dynamics.

# Quotes

- "Foreign policy is a giant international poker game where everybody's cheating."
- "It's not about good guys and bad guys. It's not about morality. It's not about right and wrong. It's about power."
- "My default is to be against war. You have to convince me that it is absolutely necessary."
- "If you want to support the troops, whichever side of whichever conflict, you have to support the truth first."
- "It's just framing. It's about power, and anything that distracts from that is the actual motive."

# Oneliner

Beau presents a critical view of foreign policy, focusing on power dynamics and the absence of good versus bad actors, stressing the importance of supporting truth over narratives.

# Audience

Media consumers

# On-the-ground actions from transcript

- Support truth over narratives (implied)
- Stay informed and critical when consuming media (implied)

# Whats missing in summary

The detailed breakdown of various perspectives on foreign policy, NATO's purpose, and the focus on power dynamics rather than morality in international relations.

# Tags

#ForeignPolicy #MediaAnalysis #PowerDynamics #NATO #WarOpposition