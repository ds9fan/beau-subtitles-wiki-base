# Bits

Beau says:

- Talks about two unrelated events involving government officials and draws parallels.
- Mentions the new reporting on what happened in Flint, Michigan, with prosecutors building a RICO case implicating government officials.
- Explains how the initial team of prosecutors was dismantled after an election, leading to a new team that brought fewer charges.
- Warns Congress about the risk of delays and political manipulation in investigations, using the events of January 6 as an example.
- Emphasizes the importance of completing investigations and releasing reports before elections to avoid potential fallout.
- Urges Congress to act swiftly and not politicize the investigation into the events of January 6 at the Capitol.
- Stresses the need for transparency and informing the American people promptly about the wide-ranging implications of the events on January 6.
- Advises against playing political games with the investigation and underscores the significant consequences that may arise.
- Encourages swift action and completion of the investigation to avoid negative repercussions.
- Urges for prioritizing duty and quick resolution in handling the investigation into the events of January 6.

# Quotes

- "The longer they wait, the longer they drag this out, while it may seem good politically, The worst it is for everything else."
- "This isn't something they can play political games with."
- "Let's get it done and go from there."

# Oneliner

Beau warns Congress against delaying and politicizing investigations, stressing the need for swift action and transparency to avoid potential fallout.

# Audience

Congress members

# On-the-ground actions from transcript

- Pursue investigations swiftly and transparently (implied)
- Inform the American people promptly about the wide-ranging implications (implied)

# Whats missing in summary

The full transcript provides additional context and details on the risks and consequences of delaying investigations and the importance of transparency in government actions.

# Tags

#GovernmentOfficials #Investigations #Congress #Transparency #PoliticalGames