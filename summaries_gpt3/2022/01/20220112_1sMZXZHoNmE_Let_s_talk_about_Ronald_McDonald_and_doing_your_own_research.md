# Bits

Beau says:

- Ronald McDonald House in Canada requires vaccinations for stay.
- Some in anti-science community criticize the decision.
- Beau questions those opposing vaccination requirement.
- Ronald McDonald House provides accommodations for immunocompromised individuals.
- Beau criticizes those prioritizing personal beliefs over public health.
- Ronald McDonald House assists individuals finding other accommodations.
- Beau advocates for taking every possible precaution around immunocompromised individuals.
- He mentions the importance of understanding Ronald McDonald House's purpose.
- Beau encourages individuals to research and not rely on misinformation.
- He stresses the significance of prioritizing the well-being of sick children.

# Quotes

- "Nobody cares what you think when it comes to people who refuse to take every possible precaution to keep a bunch of sick kids from getting sicker."
- "Your moral compass is broke."
- "Nobody cares about your moral determination because your moral compass is broke."
- "And I think that's a very good thing."
- "It's just a thought."

# Oneliner

Ronald McDonald House requires vaccinations, sparking criticism from anti-science groups, but Beau stresses the importance of prioritizing public health over personal beliefs and moral judgments.

# Audience

Public health advocates

# On-the-ground actions from transcript

- Support Ronald McDonald House or similar organizations assisting families of sick children (implied).
- Research and prioritize scientifically-backed information over misinformation (implied).
- Take necessary precautions to protect immunocompromised individuals in communal settings (implied).

# Whats missing in summary

The full transcript provides a detailed exploration of the controversy surrounding Ronald McDonald House's vaccination requirement and the importance of prioritizing public health over personal beliefs and moral judgments.