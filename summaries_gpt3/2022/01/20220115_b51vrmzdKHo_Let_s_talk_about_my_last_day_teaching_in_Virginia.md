# Bits

Beau says:

- Last day teaching in Virginia, moving to a different state.
- State-mandated curriculum requires teaching a debate between Abraham Lincoln and Frederick Douglass that never happened.
- Contemplated a fake debate but realized the critical nature of Douglass towards race wouldn't be suitable.
- Planned to show an epic rap battle between Frederick Douglass and Thomas Jefferson but found it critical of slavery and the founding systems.
- Constrained by restrictions and guidelines, making it impossible to teach history in Virginia.
- Leaving with a lesson from Thomas Paine, stressing the importance of knowledge and duty.
- United States is meant to be a government of informed people, requiring knowledge for self-governance.
- Criticizes new laws from state politicians making knowledge a crime and ignorance a duty.
- Blames parents for surrendering education to profit-driven individuals who benefit from keeping people ill-informed.
- Parents neglecting their duty to be informed and knowledgeable, allowing politicians to criminalize knowledge and perpetuate ignorance.
- Calls attention to a bill in Virginia mandating the teaching of a debate that never happened, questioning the competence of those behind it.
- Urges the people of Virginia to recognize manipulation and reclaim their duty to be informed.

# Quotes

- "Your duty is to get as much knowledge as you can because, in essence, you are supposed to be leading this country."
- "Your parents surrendered your education to a bunch of people who profit from you being ill-informed."
- "The people of Virginia should probably take a long, hard look at how they were manipulated."

# Oneliner

Beau addresses the absurdity of teaching a non-existent debate, criticizes the legislation making knowledge a crime, and urges Virginians to reclaim their duty to be informed.

# Audience

Students, Educators, Virginians

# On-the-ground actions from transcript

- Question the curriculum requirements and advocate for accurate and meaningful education (suggested).
- Challenge legislation that restricts knowledge and supports ignorance (implied).

# Whats missing in summary

The full transcript provides a deeper insight into the challenges faced in education and the importance of critical thinking and informed citizenship.