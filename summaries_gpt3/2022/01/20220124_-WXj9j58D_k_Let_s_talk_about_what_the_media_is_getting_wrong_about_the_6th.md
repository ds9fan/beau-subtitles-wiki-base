# Bits

Beau says:

- Talks about journalism in the United States failing in its obligation to uncover the truth.
- Mentions the importance of journalists looking outside the window to see if it's raining or sunny, not just printing conflicting quotes.
- Points out the current documentation revealing an attempt to overturn a free and fair election and an apparent coup.
- Criticizes the lack of journalists asking political figures about their support for the former president's coup attempt.
- Raises concerns about the media's focus on trivial details rather than pressing questions related to serious issues.
- Emphasizes the danger of misinformation thriving in an information vacuum.
- Calls for journalists to hold people accountable by asking tough questions and seeking the truth.
- Questions why journalists are waiting for government press releases instead of actively pursuing answers.
- Urges journalists to fulfill their obligation to pursue the truth and hold people accountable.
- Expresses the need for individuals to be transparent about their stance during critical events.

# Quotes

- "Your job as a journalist is to open the window and look outside."
- "Democracy doesn't die in the darkness."
- "It's an obligation of journalism to pursue this, the whole idea of the public trust and all of that."
- "These questions need to be asked."
- "People need to be put on record about where they stood then and where they stand now."

# Oneliner

Beau criticizes US journalism for failing its obligation to pursue truth, urging journalists to ask tough questions and hold people accountable.

# Audience

Journalists, News Outlets

# On-the-ground actions from transcript

- Challenge journalists to ask tough questions and pursue the truth (implied).
- Encourage news outlets to prioritize accountability over ratings and circulation (implied).
- Advocate for transparency from individuals regarding their stance on critical events (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the shortcomings in US journalism and the urgent need for journalists to fulfill their obligation to seek the truth and hold individuals accountable. Watching the full transcript will offer a comprehensive understanding of Beau's insightful perspectives on the current state of journalism in the country.

# Tags

#Journalism #US #Accountability #Truth #Democracy