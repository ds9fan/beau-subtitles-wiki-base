# Bits

Beau says:

- Questions journalistic ethics regarding Sean Hannity and his advising of the president.
- Points out the lack of accountability for Hannity's unethical behavior.
- Expresses confusion over why Fox News isn't holding Hannity accountable.
- States that Hannity is not a journalist but a commentator.
- Views Fox News as a propaganda machine rather than a journalistic outlet.
- Mentions the importance of personal ethics in disclosing biases.
- Suggests Hannity may face consequences for not disclosing information to viewers.
- Explains why he isn't leading a charge against Hannity for violating journalistic ethics.
- Comments on the misleading nature of Fox News using "news" in its name.
- Emphasizes the danger of government regulation on who can be considered a journalist.

# Quotes

- "Why isn't Sean Hannity being held to account for his unethical behavior?"
- "Because he's not a journalist."
- "Fox News is not a journalistic outlet. It's a propaganda machine."
- "Nobody's expecting him to be held to journalistic ethics because he's not a journalist."
- "Why isn't anybody riding that horse? Because it's a chicken."

# Oneliner

Beau questions Sean Hannity's journalistic ethics, labels Fox News as a propaganda machine, and explains why Hannity isn't held to journalistic standards.

# Audience

Media consumers

# On-the-ground actions from transcript

- Challenge media outlets promoting propaganda (implied)
- Advocate for ethical journalism standards (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of journalistic ethics, accountability in media, and the distinction between journalism and commentary in news outlets.

# Tags

#JournalisticEthics #SeanHannity #FoxNews #PropagandaMachine #Accountability