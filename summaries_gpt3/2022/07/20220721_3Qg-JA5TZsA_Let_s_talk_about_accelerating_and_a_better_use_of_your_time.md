# Bits

Beau says:

- Critiques the idea of accelerating change by burning down the system during election periods.
- Questions the impact of insurgency on civilians, especially the most vulnerable.
- Describes the ripple effects of disrupting infrastructure, like stopping truck deliveries.
- Details the consequences of a breakdown in systems, such as running out of food, fuel, electricity, and clean water.
- Challenges accelerationists to provide solutions for critical needs like medical infrastructure, clean water, and food.
- Emphasizes the importance of building local networks and power structures to help communities in crisis.

# Quotes

- "Your anger is a gift. Use it to help rather than hurt."
- "Start building power at the local level."
- "You can help people now. You don't have to wait."
- "You need a bunch of 55-gallon drums, fine sand, coarse sand, and gravel."
- "If you really care about those people who are being hurt by the system that exists today, you don't want to make it worse for them."

# Oneliner

Beau questions the consequences of burning down systems, urging for local empowerment to help communities in crisis.

# Audience

Community organizers

# On-the-ground actions from transcript

- Build local networks to support communities (suggested)
- Prepare emergency supplies like drums, sand, and gravel (suggested)

# Whats missing in summary

The full transcript provides a detailed breakdown of the potential consequences of advocating for radical change without considering the impact on vulnerable communities.

# Tags

#CommunityEmpowerment #LocalNetworks #CrisisResponse #CivilianImpact #SystemChange