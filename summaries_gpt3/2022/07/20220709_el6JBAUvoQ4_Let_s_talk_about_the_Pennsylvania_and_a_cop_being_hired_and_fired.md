# Bits

Beau says:

- Cop named Loman killed Tamir Rice in 2014 for playing with a toy at a park.
- Loman never charged, eventually fired for relying on their application.
- Loman resurfaced in Tioga Borough, Pennsylvania, got hired, but resigned after the community protested.
- Mayor was unaware of Loman's background, suggesting the borough council knew.
- Need for a police database evident in this incident.
- Rural communities like Tioga share distrust of cops and outsiders.
- Tioga Mayor, a big-bearded man, protested against cop but is the same person.
- Rural communities seek right and wrong, not political agendas.
- Common ground can be found between truly rural people.
- Stereotypes may cloud perceptions of allies in accountability.

# Quotes

- "This is a community that once the information was known, they decided they wanted something else."
- "There's common ground to be had because it's not a matter of a political agenda at that point."
- "Once I got this message, I went and found the footage."
- "Most times when you see somebody who looks like the mayor or me, you're probably not thinking ally."

# Oneliner

Cop killing of Tamir Rice revisited in rural Pennsylvania, leading to community pushback and mayor's surprising protest, showcasing the need for police accountability databases and common ground in rural distrust.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Contact local officials to advocate for police accountability databases (implied)
- Build bridges between rural communities and law enforcement for better understanding (implied)

# Whats missing in summary

The full transcript provides depth on the impact of police actions on rural communities and the importance of transparency and accountability in law enforcement.

# Tags

#PoliceAccountability #TamirRice #CommunityPolicing #RuralCommunities #SocialJustice