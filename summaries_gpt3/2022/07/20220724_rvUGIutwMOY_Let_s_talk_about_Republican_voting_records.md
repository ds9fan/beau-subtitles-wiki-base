# Bits

Beau says:

- Exploring Republican voting records and the disparity between rhetoric and actions.
- Recent examples of Republicans voting against protecting access to contraceptives, interracial marriage, and same-sex marriage.
- Republicans claiming to be moderate but voting against these protections.
- Republicans claiming it's about states' rights but voting against measures that allow interstate travel for family planning.
- Over 200 Republicans in the House voting against interstate travel for family planning, contradicting their supposed support for states' rights.
- Revealing the true intentions behind Republican votes: control, not states' rights or freedom.
- Over 150 Republicans voting against protecting interracial marriage and same-sex marriage.
- Over 190 Republicans voting against ensuring access to birth control.
- Republicans voting against these measures knowing they wouldn't stop them from passing, showing their commitment to their positions.
- The disconnect between Republican rhetoric and their actual votes, exposing their authoritarian tendencies.

# Quotes

- "It's not about states' rights. It's not about freedom. It never was. That's a lie."
- "They're telling you who they are."
- "They don't care about freedom. They don't care about states' rights. They don't care about anything that they said they do."
- "People need to acknowledge it, accept it for what it is."
- "You are taking away their rights, their freedoms, so you can continue a habit that you've had."

# Oneliner

Exploring Republican voting records reveals a pattern of control over freedom and rights, contradicting their claimed values.

# Audience

Voters

# On-the-ground actions from transcript

- Hold elected officials accountable for their actions (exemplified)
- Educate others about the disconnect between political rhetoric and voting records (exemplified)
- Support candidates who truly uphold values of freedom and rights (exemplified)

# Whats missing in summary

The full transcript provides detailed examples of recent Republican votes and the discrepancy between their rhetoric and actions.

# Tags

#Republican #VotingRecords #RhetoricVsActions #Rights #Freedom