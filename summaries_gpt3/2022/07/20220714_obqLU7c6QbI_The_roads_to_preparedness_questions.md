# Bits

Beau says:

- People tend to overlook the importance of keeping their papers, like birth certificates and insurance documents, in an emergency.
- When it comes to preparing for adrenaline and trauma, you can't really prepare but being comfortable with the tasks at hand is key.
- In an everyday carry (EDC) and survival kit, essentials include food, water, fire, shelter, first aid kit, and a knife.
- Refraining refrigerated medications during power outages is a challenge with limited solutions.
- Beau advocates for Wilderness First Responder courses for emergency preparedness but notes their cost and intensity.
- Using zombies as a metaphor can help in discussing emergency preparedness with friends.
- Beau suggests having a raft in case of heavy flooding as an additional preparedness item.
- Two weeks of supplies are recommended as a minimum for emergency preparedness.
- Emergency preparedness should not become an overwhelming lifestyle but a freeing safety net.
- Different considerations for emergency prep arise for targeted minorities due to potential biases in disaster situations.

# Quotes

- "Knowledge weighs nothing."
- "Emergency preparedness is supposed to be freeing."
- "Different considerations for emergency prep arise for targeted minorities."

# Oneliner

Be prepared for emergencies by safeguarding vital documents, understanding tasks, carrying essentials, and fostering self-reliance.

# Audience

Emergency planners

# On-the-ground actions from transcript

- Compile and digitize vital documents for emergency preparedness (suggested).
- Enroll in Wilderness First Responder or first aid courses for better readiness (suggested).
- Initiate zombie apocalypse metaphor talks to spark emergency prep awareness among friends (suggested).

# Whats missing in summary

The full transcript provides in-depth insights on emergency preparedness strategies, including the importance of self-reliance, understanding mobility issues, and staying off immediate front lines during disasters.

# Tags

#EmergencyPreparedness #DisasterResponse #SelfReliance #CommunityPreparedness #InclusivePlanning