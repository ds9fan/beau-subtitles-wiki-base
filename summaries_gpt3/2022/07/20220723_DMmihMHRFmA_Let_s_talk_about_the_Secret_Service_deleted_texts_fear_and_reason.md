# Bits

Beau says:

- The Inspector General's Office is investigating the Secret Service for potentially deleting text messages, treating it as a criminal investigation.
- Questions arose about the Secret Service's actions on January 6th, with concerns about their fear levels and communication practices.
- The Secret Service was criticized for sheltering in place during high-security events, but mobility is vital in such situations.
- Despite being scared and using less than ideal communication practices, the Secret Service handled the situation well on January 6th.
- Beau suggests that the fear exhibited by the Secret Service may have been based on perceived rather than actual threat capability.
- Beau calls for an inquiry into why the Secret Service was so scared on January 6th, pointing to the importance of looking at threat assessments.
- There is speculation about the content of deleted text messages and their potential connection to the level of fear within the Secret Service.
- Beau comments on the seriousness of deleting government records and the potential criminal implications if done intentionally or systematically.
- Despite some shortcomings in communication practices, the Secret Service's actions on January 6th were effective in ensuring Pence's safety.
- Beau underscores the importance of investigating the level of fear within the Secret Service on January 6th and understanding the basis for it.

# Quotes

- "If it was done intentionally, it would be a crime."
- "I don't believe they misjudged their opposition by that much without information that led them to do that."
- "They had every right to be scared."
- "The level of fear that they had, that's a question."
- "Yeah, they need to do whatever they can to get those text messages."

# Oneliner

The Inspector General investigates the Secret Service for potentially deleting text messages, raising questions about their actions and fear levels on January 6th.

# Audience

Oversight Committees

# On-the-ground actions from transcript

- Investigate the threat assessments and information the Secret Service received regarding the January 6th events (suggested)
- Take steps to retrieve the potentially deleted text messages for investigation (suggested)

# Whats missing in summary

Further details on the potential implications of the missing text messages and their impact on understanding the Secret Service's actions on January 6th.

# Tags

#SecretService #InspectorGeneral #January6th #Investigation #ThreatAssessments