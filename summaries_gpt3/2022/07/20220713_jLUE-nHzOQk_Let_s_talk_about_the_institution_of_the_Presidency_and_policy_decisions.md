# Bits

Beau says:

- Explains the importance of preserving the institution of the presidency.
- Mentions the emergence of talking points as evidence links to the White House.
- Questions the idea that indicting Trump is a policy decision rather than evidence-based.
- Raises concerns about the unchecked executive power if former presidents are not indicted.
- Warns about the danger of allowing a president to be above the law.
- Emphasizes that the Constitution was written to prevent unchecked executive power.
- Condemns preserving the institution of the presidency over upholding the Constitution.
- Raises the potential threat of a president refusing to leave office.
- Points out the risk of setting a dangerous precedent for seizing control using military support.
- Describes the situation as suggesting an attempted self-coup with no accountability.
- Warns that without checks, the presidency could devolve into a dictatorship.
- Stresses the importance of upholding the checks and balances within the Constitution.
- Urges to prioritize the Constitution over preserving the office of the presidency.
- Concludes by warning against sacrificing the Constitution for the sake of the presidency.

# Quotes

- "Preserving the institution of the presidency by allowing unchecked executive power is the ultimate betrayal of the Constitution."
- "The Constitution was written to protect this country from unchecked executive power."
- "The institution of the presidency is worthless. It is worthless."
- "If there's no accountability for that, it's not a failed coup attempt. It's practice."
- "Preserving the office of the presidency at the expense of that document is a pathway to disaster."

# Oneliner

Beau explains the danger of preserving the institution of the presidency over upholding the Constitution and warns against allowing unchecked executive power.

# Audience

Advocates and defenders of constitutional integrity.

# On-the-ground actions from transcript

- Challenge and question policies that prioritize preserving the presidency over upholding constitutional values (implied).
- Advocate for accountability and checks on executive power within the government (implied).
- Support initiatives that prioritize constitutional integrity over individual office preservation (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the risks associated with prioritizing the institution of the presidency over constitutional integrity.

# Tags

#Presidency #Constitution #UncheckedExecutivePower #Accountability #InstitutionPreservation