# Bits

Beau says:

- Talks about a friend's struggle in a rural area to find baby formula for his infant son.
- Beau helps his friend by locating and delivering baby formula from stores in his area.
- Laughs at the decentralized way different areas came together to meet the need.
- Mentions disruptions and how the idea of making everything at home doesn't always work out.
- The Trump-era policy of tariffs on baby formula from Canada contributed to the shortage.
- Eric Miller explains how the tariffs have affected supply and prices in the US.
- The shortage prompted the Bynum administration to seek solutions by allowing imports from safe countries.
- Points out that the tariffs were more about maintaining profit margins for certain companies rather than punishing other countries.
- Emphasizes the impact of tariffs on keeping prices high for consumers in the US.
- Wraps up by sharing these insights and wishing everyone a good day.

# Quotes

- "Different areas coming together to supply the needs."
- "That nonsense about make everything at home, blah blah blah blah it never works out."
- "The US has basically said our priority is keeping subsidized imports and products made with subsidized inputs out of the US market."
- "Keeping the price high for you."
- "It was never about punishing these other countries."

# Oneliner

Beau shares a rural area struggle to find baby formula, revealing insights on tariffs, supply disruptions, and profit margins.

# Audience

Community members, policymakers

# On-the-ground actions from transcript

- Help distribute baby formula in areas facing shortages (exemplified)
- Advocate for policies that prioritize affordable prices for consumers (exemplified)

# Whats missing in summary

The full transcript provides a detailed look at how policy decisions impact everyday people's access to essentials like baby formula.