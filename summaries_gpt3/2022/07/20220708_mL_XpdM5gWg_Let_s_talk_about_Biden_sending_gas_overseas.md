# Bits

Beau says:

- Explains the situation regarding gas, gasoline, oil, and the Strategic Reserve.
- Responds to a viewer's concern about Fox News possibly lying about Biden sending gasoline overseas.
- Confirms that the Biden administration did allow 5 million barrels of oil to be sent overseas.
- Clarifies that the oil came from the strategic reserve because US refineries are operating at capacity.
- Addresses the misconception about refineries running at full capacity and explains the reason for the backlog of oil.
- Mentions that sending oil overseas affects the global oil market and can potentially lower oil prices.
- Emphasizes that while the release of oil for export can help lower costs, it is a gradual process.
- Concludes that Fox News may not have lied but rather omitted relevant information in their reporting.

# Quotes

- "The Biden administration approved the release of 1 million barrels of oil per day."
- "So not really lying, just omission of pieces of relevant information."

# Oneliner

Beau clarifies the situation around oil being sent overseas, addressing concerns about Fox News and explaining the impact on global oil prices.

# Audience

Viewers concerned about accurate information on oil and gasoline.

# On-the-ground actions from transcript

- Contact your representatives to advocate for transparent and accurate information on energy policies (suggested).

# Whats missing in summary

The full transcript provides a detailed breakdown of the oil situation, including the impact of sending oil overseas and its effects on global prices.