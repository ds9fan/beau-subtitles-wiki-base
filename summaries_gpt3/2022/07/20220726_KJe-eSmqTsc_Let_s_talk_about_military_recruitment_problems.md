# Bits

Beau says:

- Addressing recruitment issues in the Department of Defense (DOD) due to trouble recruiting.
- Not surprised to find readiness issues in the establishment.
- Majority of recruitment issues not related to what Beau has discussed previously.
- Target recruitment age for new enlistees is 17 to 24.
- Reduction in eligible candidates from 29% to 23%.
- Criminal records, substance use, and obesity contributing to the shrinking recruitment pool.
- 57% of those not interested in serving fear emotional or physical harm.
- Army hitting only 68% of its recruitment goals through April.
- COVID and a strong job market impacting recruitment challenges.
- Falling patriotism and disillusionment contributing to lack of interest in joining.
- Critiques MAGA crowd's perception of patriotism.
- Issues with the demographic makeup, public health, and economy affecting recruitment.

# Quotes

- "Y'all aren't the solution. Y'all are the problem."
- "Not exactly Medal of Honor material, just saying."
- "If you don't want to make the country better and move forward, you are not a patriot."

# Oneliner

Addressing recruitment issues in the Department of Defense, Beau breaks down the factors impacting the shrinking recruitment pool, from criminal records to falling patriotism, and questions the country's values and treatment of its citizens.

# Audience

Recruitment officials, policymakers, concerned citizens

# On-the-ground actions from transcript

- Advocate for policies that improve recruitment eligibility (implied)
- Support programs that address substance use and obesity in communities (implied)
- Promote a positive and inclusive national narrative to boost patriotism and interest in service (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the complex factors affecting military recruitment, including societal issues and political influences, offering a deeper understanding of the challenges faced by the Department of Defense.

# Tags

#Recruitment #DepartmentofDefense #NationalSecurity #Patriotism #SocietalIssues