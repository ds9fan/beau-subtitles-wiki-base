# Bits

Beau says:

- Fox News guest criticized Monticello for being frank about Thomas Jefferson's history of slavery.
- Jefferson acknowledged slavery as evil, but engaged in it anyway.
- Jefferson believed that over time, people's attitudes towards slavery would change.
- Beau argues that Jefferson was aware of his actions and foresaw criticism for them.
- Not discussing Jefferson's history is not history, but American mythology.
- Beau asserts that addressing the flaws of historical figures is necessary for learning and understanding history.

# Quotes

- "Monticello didn't go woke, as is often the case."
- "Not discussing Jefferson's history is not history, but American mythology."
- "They were people just like everybody else. They made mistakes."
- "That's the belief that these people were infallible."
- "Thomas Jefferson was aware of the injustice, too, and he would be happy to know that people today were criticizing him for it."

# Oneliner

Fox News guest criticizes Monticello for being frank about Thomas Jefferson's history of slavery, but Beau argues that addressing Jefferson's flaws is necessary for understanding history.

# Audience
History enthusiasts

# On-the-ground actions from transcript

- Address the flaws and mistakes of historical figures in educational settings (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of the implications of acknowledging and learning from the flaws of historical figures.