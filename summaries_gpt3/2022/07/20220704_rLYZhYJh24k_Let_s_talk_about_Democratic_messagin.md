# Bits

Beau says:

- The Democratic Party struggles with messaging and getting their policies seen as a priority.
- Despite polls showing support for Democratic positions, the party faces challenges in making issues like Roe v. Wade a priority.
- A poll by AP revealed that before the decision on Roe v. Wade, only 13% saw family planning and women's rights as a priority. Afterward, this number rose to 30%.
- While 64% of Americans believe abortion should be legal in all or most cases, only 30% prioritize it as an issue.
- Beau suggests that the Democratic Party needs to focus on explaining the economic and social impacts of issues like Roe v. Wade to increase prioritization by voters.
- Candidates need to be frank about the consequences and investigations that may occur if certain policies are not protected.
- The messaging should center on the potential economic strain, impact on social safety nets, and invasion of privacy resulting from policy changes.
- Making issues like family planning and women's rights a priority is key to motivating voter turnout.
- Beau stresses the importance of clear and consistent messaging to raise the issue's priority level among voters.
- The Democratic Party needs to communicate effectively to elevate issues to ballot priorities.

# Quotes

- "It's not on the ballot just because it's an impact from the election."
- "You can actually put Roe on the ballot. But you can't be timid."
- "Those that were surveyed after the decision came down, 30%."

# Oneliner

The Democratic Party faces challenges in making issues like Roe v. Wade a priority despite majority support, stressing the need for clear messaging to motivate voter turnout and elevate these issues.

# Audience

Democratic Party members

# On-the-ground actions from transcript

- Communicate the economic and social impacts of policies like Roe v. Wade to raise voter awareness and prioritize these issues (suggested).
- Be frank with voters about the consequences and investigations that may occur if certain policies are not protected (suggested).

# Whats missing in summary

The full transcript provides a detailed analysis of the Democratic Party's struggle with messaging and prioritizing key issues, urging for clearer communication to mobilize voters effectively.

# Tags

#Messaging #DemocraticParty #VoterPriorities #RoevWade #ClearCommunication