# Bits

Beau says:

- The House released a defense budget blueprint of $839 billion, prompting questions.
- The Biden administration did not ask for such a bloated budget; it's $37 billion more than requested.
- The House rejected the administration's plans for force reduction.
- Congress holds the power of the purse, as outlined in the US Constitution.
- The defense budget often increases due to inflation and contributions from the defense industry.
- The Senate has yet to finalize its plan, which will eventually be sent to Biden for approval.
- This situation showcases the checks and balances system in government.
- The Senate's decision on the budget is still pending.
- The influence of lobbyists on the final budget decision remains uncertain.
- Congress ultimately determines the budget, not the administration.

# Quotes

- "Congress has the power of the purse."
- "The Biden administration did not ask for this."
- "The defense budget, like anything else, goes up each year because of inflation."
- "That is power reserved to Congress."
- "It's just a thought."

# Oneliner

The House proposed a $839 billion defense budget, $37 billion more than Biden's request, revealing Congress's power over budget decisions and potential industry influences.

# Audience

Budget-conscious citizens

# On-the-ground actions from transcript

- Monitor and advocate for responsible defense budget allocations (implied).
- Stay informed about campaign contributions to representatives, especially from the defense industry (implied).

# Whats missing in summary

The full transcript provides a deeper understanding of the intricacies of defense budgeting and the role of different branches of government in decision-making.