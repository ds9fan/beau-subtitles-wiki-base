# Bits

Beau says:

- Analyzing a meme sent by a conservative follower that portrays stick figures labeled left, right, and me.
- Revealing the message behind the meme and discussing the implications.
- Posing a question to conservative friends and family about changing political opinions.
- Expressing surprise at a conservative follower who admitted to never changing their political opinion.
- Noting the rigidity and inflexibility of holding the same political opinions for over thirty years.
- Criticizing the arrogance and entitlement associated with refusing to adapt political beliefs despite new information.
- Pointing out the entrenched nature of political beliefs that hinder progress and adaptation.
- Contrasting the perception of entitlement between young people and conservatives in terms of political beliefs.
- Critiquing the lack of substantial changes in the Republican Party's positions over time.
- Addressing the resurgence of a far-right movement within the Republican Party and its implications on revisiting past laws.
- Challenging the notion that the world should conform to conservative beliefs without updating them.
- Encouraging provoking political discourse by asking when was the last time someone changed their political opinion.
- Contrasting the readiness to adapt political opinions between left-leaning individuals and conservatives.
- Emphasizing the importance of accepting new ideas and information to progress and lead in society.
- Criticizing the tendency to cling to outdated political talking points instead of embracing progress.

# Quotes

- "When's the last time you changed your political opinion on something?"
- "That is wild."
- "You want the United States to be a world leader, you have to lead."
- "They're meeting the world head on instead of hiding in the past."
- "There's no progress at all."

# Oneliner

Beau challenges conservative beliefs by questioning the refusal to adapt in a changing world, criticizing entitlement and lack of progress.

# Audience

Conservative friends and family

# On-the-ground actions from transcript

- Provoke a political discourse by asking conservative friends and family when was the last time they changed their political opinion (suggested).

# Whats missing in summary

The full transcript provides additional insights into the rigidity of conservative beliefs and the necessity for adaptation in a changing world.

# Tags

#ConservativeBeliefs #PoliticalOpinions #Progress #Adaptation #Entitlement