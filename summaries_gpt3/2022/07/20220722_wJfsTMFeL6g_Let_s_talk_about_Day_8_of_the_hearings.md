# Bits

Beau says:

- Recap of day eight of the hearing focusing on the case against the former president's lack of leadership and initiative.
- Building a case suggesting the events were premeditated and Trump was in dereliction of duty.
- More hearings coming in September with new information and sources.
- Main focus on Trump's inaction during the Capitol attack.
- Detailing how easy it was for Trump to address the crowd and stop the violence.
- Secret Service agents attached to Pence were in fear for their lives.
- Testimonies backing up Hutchinson's account of Trump's behavior in the motorcade.
- Committee focusing on disproving minute details to discredit the entire hearing.
- Pence portrayed as taking action while Trump did nothing.
- Outtakes reveal insincerity of Trump's remarks and reluctance to condemn his supporters.
- Republicans challenged by continuing to support baseless election claims.
- The hearing aimed to show the premeditated nature of the events and Trump's involvement.

# Quotes

- "Main focus on Trump's inaction during the Capitol attack."
- "Pence portrayed as taking action while Trump did nothing."
- "Republicans challenged by continuing to support baseless election claims."

# Oneliner

Recap of day eight hearing focusing on Trump's inaction, Pence's actions, and the premeditated nature of the events.

# Audience

Committee members, concerned citizens

# On-the-ground actions from transcript

- Reach out to Department of Justice to support the investigation (implied)
- Be willing to talk to the committee if you have relevant information (implied)

# Whats missing in summary

Insights into the specific testimonies and evidence presented during the hearing.

# Tags

#CapitolAttack #Trump #Pence #Hearing #Inaction #Preplanned #Justice