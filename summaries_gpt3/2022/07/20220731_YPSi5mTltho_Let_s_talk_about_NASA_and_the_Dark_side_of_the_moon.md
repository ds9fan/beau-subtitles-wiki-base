# Bits

Beau says:

- NASA is planning to send unmanned payloads to the dark side of the moon, specifically to Schrodinger's crater, in 2025.
- The mission aims to gather information on the moon's history, general environment, impact from space objects, earthquakes, moonquakes, and seismic activity.
- This will be the first time NASA places anything on that side of the moon due to the challenges like no direct line of sight and the need for satellite relay for communication.
- Artemis, the program associated with returning humanity to the moon, is named after a Greek goddess heavily linked with the moon and Apollo's twin.
- NASA's mission to the dark side of the moon is geared towards better preparing for manned endeavors and astronaut trips.
- Beau finds stories like this inspiring as they propel humanity beyond Earth, towards new chapters in space exploration.
- Beau expresses excitement over events like this, which move humanity towards exploring beyond Earth.
- He believes these small steps are paving the way for humans to venture into space and embrace a new future.
- Beau suggests that, if humanity perseveres, the idea of having just one Earth might not hold true forever.
- He acknowledges the importance of considering the advancements on the horizon like space exploration.

# Quotes

- "At some point, assuming we don't filter ourselves out, we will be in other places."
- "These little steps, these are the first steps towards us really getting off of this rock."
- "It's events, launches, stuff that is propelling us off of this rock that I have always found inspiring."
- "Maybe it won't be true as long as we can make it till then."
- "Every once in a while it's important to note that stuff like that is on the horizon."

# Oneliner

NASA plans to send unmanned payloads to the dark side of the moon in 2025 to gather vital information, paving the way for future manned endeavors and inspiring humanity's journey beyond Earth.

# Audience

Space enthusiasts, Science enthusiasts

# On-the-ground actions from transcript

- Stay informed about NASA's Artemis program and future space exploration missions (implied).

# Whats missing in summary

The full transcript provides a detailed insight into NASA's upcoming mission to the dark side of the moon, touching on its significance in space exploration and humanity's future beyond Earth.

# Tags

#NASA #Artemis #SpaceExploration #MoonMission #Humanity #FutureInSpace