# Bits

Beau says:

- The ACLU is investigating government agencies buying bulk data from cell phones to track people's movements and develop a "pattern of life."
- The Supreme Court previously ruled in Carpenter v. U.S. that authorization is needed for such surveillance, but this current practice involves gathering information from over 250 million phones daily.
- The ACLU discovered more than 100,000 location markers in a three-day period in just one region of the U.S.
- There's bipartisan support for the Fourth Amendment is Not for Sale Act, which aims to prevent government agencies from bypassing the Fourth Amendment by purchasing information from third parties.
- Beau questions how the current Supreme Court, with some members not strongly supporting privacy rights, will respond to this issue.
- He warns that those cheering for reduced privacy protections may eventually have their own privacy rights affected.
- Beau believes that supporting the Fourth Amendment is Not for Sale Act is the most direct way to combat this surveillance practice.
- However, he acknowledges that the act's support might be impacted by other pressing political issues.
- The information gathered from cell phones can reveal detailed aspects of individuals' lives, such as their locations and social circles.
- Government agencies currently do not need a warrant to access this data; they can simply purchase it from third parties.

# Quotes

- "The ACLU is obviously fighting this."
- "At current, it seems like the best way to curtail this practice is the Fourth Amendment is not for sale act."

# Oneliner

The ACLU investigates government agencies buying bulk data to track individuals' movements, prompting bipartisan support for the Fourth Amendment is Not for Sale Act amid concerns over privacy rights.

# Audience

Privacy advocates, concerned citizens

# On-the-ground actions from transcript

- Support the Fourth Amendment is Not for Sale Act (suggested)
- Stay informed about privacy rights issues and advocate for transparency (implied)

# Whats missing in summary

The full transcript provides detailed insights on the ACLU's fight against government surveillance practices and the potential implications for privacy rights, warranting a deeper dive into the ongoing developments.

# Tags

#PrivacyRights #GovernmentSurveillance #FourthAmendment #ACLU #BipartisanSupport