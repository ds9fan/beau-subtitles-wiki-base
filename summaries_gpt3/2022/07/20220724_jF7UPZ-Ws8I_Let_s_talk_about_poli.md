# Bits

Beau says:

- Provides an update on a concerning situation involving polio, a disease thought to be eradicated.
- Reports a case of a young adult in New York contracting polio, the first confirmed case in the US since 2013.
- Explains the virus is vaccine-derived, meaning someone outside the US shed the virus after being vaccinated with the oral version.
- Emphasizes that vaccine gaps are causing the resurgence of polio.
- Notes the serious effects of the disease, such as infections of the brain, spine, paralysis, and death, with no known cure.
- Mentions warnings issued in Britain and on his channel to stay up to date with vaccinations due to traces found in sewage.
- Addresses accusations of being a "shill for big vaccine," clarifying that his motivation comes from opposing the industry of children's coffins.
- Concludes with a thought about the importance of staying informed and taking necessary precautions against diseases like polio.

# Quotes

- "I'm not a shill for big vaccine."
- "I have another motivation. And it's not that I'm for any particular industry. I'm actually against another large industry, children's coffins."
- "Y'all have a good day."

# Oneliner

Beau provides an update on a polio case in New York, stressing the importance of vaccinations and dispelling misconceptions about his motivations.

# Audience

Health-conscious individuals

# On-the-ground actions from transcript

- Get vaccinated and stay up to date with vaccinations to prevent the spread of diseases like polio. (suggested)
- Stay informed about health warnings and take necessary precautions. (suggested)

# Whats missing in summary

Importance of community education and awareness to prevent the resurgence of preventable diseases.

# Tags

#Polio #Vaccinations #Health #Prevention #PublicHealth