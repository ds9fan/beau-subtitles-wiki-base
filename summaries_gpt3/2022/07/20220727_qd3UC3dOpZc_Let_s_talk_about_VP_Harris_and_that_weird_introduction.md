# Bits

Beau says:

- Standing in a shop wearing a blue t-shirt with an iconic scene depicting Mr. Rogers and Officer Clemens with their feet in a kiddie pool.
- Mr. Rogers used subtle yet direct methods to encourage tolerance and kindness during a time when people had issues sharing pools.
- Vice President Harris introduced herself at a disability conference, practicing self-description for the benefit of those with limited sight.
- Mocking of the clip from the conference was primarily done by individuals who grew up watching Mr. Rogers, missing the point of the show.
- Mr. Rogers, known for encouraging people to look for helpers, had some odd behaviors like narrating feeding his fish for a blind viewer.
- The act of self-description, like Harris did, is a way to make events more accessible for those with disabilities.
- Beau questions the response of those mocking Harris's self-description, suggesting they may have missed the show's message of kindness and empathy.
- Encourages reflection on the deeper meaning behind seemingly simple actions like those of Mr. Rogers and Vice President Harris.
- Emphasizes the importance of understanding the intentions behind actions rather than just surface-level appearances.
- Beau implies that Mr. Rogers himself might have been disappointed in those who missed the point of his message of kindness and understanding.

# Quotes

- "If you did grow up on Mr. Rogers, and your first inclination upon seeing that clip was to mock it, I think you missed the point of the show."
- "It's weird."
- "I'd bet Mr. Rogers would be pretty disappointed in you."
- "Have a good day."

# Oneliner

Beau points out the missed message of kindness and empathy in mocking Vice President Harris's self-description, reminiscent of Mr. Rogers's subtle methods of promoting tolerance.

# Audience

Viewers

# On-the-ground actions from transcript

- Attend or support events and conferences designed to be accessible to individuals with disabilities (implied).

# Whats missing in summary

The full transcript provides a poignant reminder about the importance of empathy, understanding, and kindness in our actions towards others.