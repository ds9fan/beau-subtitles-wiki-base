# Bits

Beau says:

- Explains the reasoning behind government agencies' seemingly odd purchases of weapons, addressing concerns raised by the public.
- Points out that every federal agency has an Office of the Inspector General with law enforcement functions, hence the need for weapons.
- Mentions specific agencies like the IRS, Department of Education, and NOAA that have their own law enforcement divisions requiring firearms.
- Provides insight into the Department of Energy's Office of Secure Transport, which transports nuclear weapons and materials across the country, justifying their high-end purchases.
- Notes that agencies like Veterans Affairs also have police forces to protect hospitals.
- Emphasizes that the widespread presence of law enforcement functions within federal agencies showcases the extensive reach of policing in the United States.
- Clarifies that the concern over agencies purchasing weapons periodically is not a cause for alarm but rather stems from a lack of understanding about these agencies' functions.
- Draws a distinction between conspiracy theories and the factual basis of government agency purchases, stating this topic falls into the latter category.
- Addresses the common question of why non-traditional agencies like Department of Energy require firearms, shedding light on their unique functions.
- Concludes by reassuring the audience that the situation is nothing to worry about, framing the issue as more about curiosity than alarm.

# Quotes

- "There is a maze of police agencies, law enforcement agencies, armed agencies throughout the federal government that you've never heard of."
- "It really goes to show exactly how widespread law enforcement functions are in the United States."

# Oneliner

Beau sheds light on the reasons behind government agencies purchasing weapons, revealing the extensive presence of law enforcement functions within federal bodies and dispelling concerns as more about curiosity than alarm.

# Audience

Government Accountability Advocates

# On-the-ground actions from transcript

- Contact your local representatives to inquire about the law enforcement functions and purchases of weapons by government agencies (suggested).

# Whats missing in summary

The full transcript provides a detailed explanation of the rationale behind government agencies' purchases of weapons and the extensive reach of law enforcement functions within federal bodies.

# Tags

#Government #Agencies #LawEnforcement #FederalGovernment #Weapons #Purchases