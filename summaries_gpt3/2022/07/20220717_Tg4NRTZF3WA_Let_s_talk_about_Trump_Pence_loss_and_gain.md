# Bits

Beau says:

- Speculates on Trump and Pence testifying before a committee.
- Questions Trump's courage to testify and suggests he may not listen to good advice.
- Considers two potential outcomes if Trump does testify: vague, unhelpful answers or potential perjury.
- Suggests allowing Trump to testify only if his mic can be cut to prevent perjury.
- Examines the risks for Trump in testifying and how he currently benefits from standing apart and denying allegations.
- Contrasts Trump's situation with Pence, who stands to gain credibility and respect by testifying truthfully.
- Believes Pence's faith may motivate him to testify truthfully.
- Foresees Pence's testimony elevating his stature within the Republican Party.
- Suggests a different approach for getting Pence to testify, starting with a casual, behind-closed-doors interaction.
- Contemplates the motivations behind the committee's consideration of Trump and Pence testimony.

# Quotes

- "He has everything to lose. He has everything to lose by doing this."
- "Pence has everything to gain."
- "This is a person that it doesn't matter how you feel about him politically or even as a human being, okay?"
- "Y'all have a good day."

# Oneliner

Beau speculates on the potential testimonies of Trump and Pence before a committee, suggesting Trump has everything to lose while Pence has everything to gain, based on their actions and motivations.

# Audience

Committee members

# On-the-ground actions from transcript

- Approach Pence for a casual, behind-closed-doors interaction to begin discussing his testimony (implied).
- Contemplate the motivations behind the committee's consideration of Trump and Pence testimony (implied).

# Whats missing in summary

Insight into the potential implications of Trump and Pence's testimonies on their political futures.

# Tags

#Trump #Pence #Testimony #Committee #Speculation