# Bits

Beau says:
- Introduces the topic of science fiction and societal change, focusing on the show "The Orville."
- Jessie Gender is introduced as a guest, known for discussing social and political issues in nerdy communities.
- The episode was prompted by a flood of messages about "The Orville" and a particular episode.
- The episode being discussed features a character, Topa, who is intersex in an alien society called the Moklins.
- Topa is forced to undergo surgery as a child to conform to societal norms of gender.
- The episode "A Tale of Two Topas" focuses on Topa's journey to realize their true gender identity and desire to reverse the previous surgery.
- The distinction between being intersex and transgender is discussed, focusing on bodily autonomy and self-perception.
- Misinterpretations of the storyline within the show led to some viewers perceiving it as anti-trans.
- The importance of representation in sci-fi shows like "The Orville" and "Star Trek" is discussed in influencing societal change.
- Positive and negative aspects of using allegory and metaphor in sci-fi for social commentary are explored.

# Quotes
- "Transness is more about getting to dictate how you see yourself and other people getting to see you."
- "It allows them to sort of not see that adversary relationship with the discussion, but just a discussion about it."
- "It's supremely helpful. And even when we get to something like the Orville and older stuff in general."

# Oneliner
Beau and Jessie Gender dissect the nuanced portrayal of gender identity and societal norms in "The Orville," discussing the impact of representation in sci-fi on social change.

# Audience
Sci-fi fans, LGBTQ+ community

# On-the-ground actions from transcript
- Watch and support sci-fi shows that provide representation and meaningful social commentary (exemplified).
- Engage in critical analysis of media portrayals of gender identity and societal norms (implied).

# Whats missing in summary
Analysis of the impact of media representation on shaping societal perceptions and attitudes towards gender identity.

# Tags
#ScienceFiction #GenderIdentity #Representation #SocialChange #SocietalNorms #Transgender #Intersex #MediaRepresentation