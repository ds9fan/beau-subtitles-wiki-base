# Bits

Beau says:

- The Republican Party in Idaho recently set up their party platform during their convention, adopting extreme and unique positions. 
- They decided not to include rejection of the 2020 election results, unlike some other states that claimed Trump won and Biden is illegitimate.
- Idaho's platform included extreme planks like no exemptions for the life of the mother and suggesting the repeal of the 16th Amendment.
- This move by Idaho Republicans, known for being one of the most dependent states on federal support, is seen as surprising.
- Despite adopting extreme positions, Idaho Republicans didn't go as far as embracing Trump's baseless claims about the election.
- Idaho's decision not to adopt the rejection of the 2020 election results is a significant blow to Trump's attempts to rally support.
- The rejection of Trump's claims didn't even make it out of committee during the Idaho convention.
- This move from Idaho Republicans, a traditionally safe area for the party, is a clear message that they are not willing to be swayed by Trump's narrative.
- The unique development in Idaho's platform, despite its extreme elements, shows a departure from supporting Trump.
- The rejection of Trump's claims by Idaho Republicans is a notable and unexpected turn of events within the party.

# Quotes

- "They're willing to adopt these just wild party planks, and that's fine. It's their party."
- "That is really bad news for Trump."
- "For a state platform that was willing to be seen as incredibly extreme to not adopt that language, that is a slap in the face."
- "I imagine there will be some Idaho potatoes and ketchup on the wall when he hears the news."
- "Even with all of the extreme language, they're not backing Trump anymore."

# Oneliner

The Republican Party in Idaho adopts extreme planks but surprises by not backing Trump's election claims, delivering a significant blow to his support.

# Audience

Political observers

# On-the-ground actions from transcript

- Contact Idaho Republican Party officials to express support for their decision not to adopt rejection of the 2020 election results (suggested).
- Attend local political events or meetings to stay informed about party platforms and decisions (exemplified).

# Whats missing in summary

Insights on the potential impact of Idaho Republicans' decision on future party dynamics and Trump's support base.

# Tags

#RepublicanParty #Idaho #Trump #ElectionResults #PartyPlatform