# Bits

Beau says:

- High-ranking individuals from Pence's inner circle testified before a federal grand jury last week.
- Commentary has shifted from DOJ inaction to DOJ involvement with Trump.
- DOJ's investigation largely occurs out of public view, focusing on the fake electors scheme.
- Activities such as subpoenas, FBI and National Archives involvement, and Eastman's case are related to the scheme.
- Testimony from Pence's chief of staff and another individual fits neatly into the investigation.
- Speculation surrounds whether Trump is involved, with indications pointing in that direction.
- DOJ actively investigates the fake electors scheme, indicating White House involvement.
- Testimonies suggest Pence may not need to testify if individuals are forthcoming.
- Rumors suggest questioning focused on Giuliani and Eastman, related to the fake electors scheme.
- The investigation likely extends beyond what is currently publicly known.

# Quotes

- "DOJ is actively, very actively, looking into the fake electors scheme."
- "They believe this scheme reaches into the White House."
- "It's not really a turning point in the investigation."

# Oneliner

High-ranking Pence associates testify before grand jury, signaling DOJ's active investigation into fake electors scheme and potential White House involvement.

# Audience

Political commentators, concerned citizens

# On-the-ground actions from transcript

- Stay informed on developments in the investigation (suggested)
- Advocate for transparency and accountability in government actions (implied)

# Whats missing in summary

Insights on the potential impact of the investigation's findings on future political landscapes. 

# Tags

#DOJ #Investigation #FakeElectorsScheme #Pence #WhiteHouse #Transparency