# Bits

Beau says:

- Texas proposed replacing "slave trade" with "involuntary relocation" in history classes.
- Department of Education in Texas rejected the proposal, acknowledging the problematic nature of the term.
- The goal behind introducing such terminology is to soften the brutal reality of slavery and historical atrocities.
- There's a push to teach American history as mythology and propaganda rather than critically.
- People want to whitewash history to make themselves and the country look better.
- Teaching history inaccurately can lead to misinformation and a lack of understanding.
- Softening the truth in history education can lead to future generations repeating mistakes.
- American history is full of heroes who spoke out against atrocities, but their voices are often silenced.
- Education is key to ensuring that future generations make informed decisions and understand the past.
- It's vital to teach history accurately, including the horrors and atrocities that occurred.
- Softening the truth of history by using new terms is not a solution to the real issues that happened.
- The youth need to be educated, not indoctrinated or fed myths.
- Heroes in history were flawed individuals who did both good and bad.
- Understanding the truth of history is critical for raising informed and empathetic individuals.

# Quotes

- "You can't soften what happened. It was horrible. It was hideous."
- "They want American mythology. They want propaganda taught, and they want it packaged as if it was history."
- "There are no heroes. Not really. They were people. They were flawed."

# Oneliner

Texas proposed softening the truth of history by replacing "slave trade" with "involuntary relocation," aiming to teach American mythology instead of critical history.

# Audience
Educators, historians, students

# On-the-ground actions from transcript

- Ensure accurate and critical history education in schools (implied)
- Encourage the teaching of American history in its full context, including the good and the bad (implied)
- Advocate for educating the youth on the true, unfiltered history of their country (implied)

# Whats missing in summary

The full transcript provides a detailed exploration of the dangers of distorting history and the importance of educating the youth on the true and unvarnished version of American history.

# Tags

#History #Education #Truth #Mythology #AmericanHistory