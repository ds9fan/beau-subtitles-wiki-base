# Bits

Beau Egan says:

- Explaining the term "partisan" in relation to the committee, mostly used by Republicans in discussing it.
- Noting that the witnesses are mainly Republicans, particularly mentioning Liz Cheney's involvement.
- Republicans on the committee are trying to correct their party and address what they see as a threat to the country.
- Many Republicans are acknowledging that the strain of authoritarianism seen is not compatible with the United States or its Constitution.
- Cheney expressed that one cannot be loyal to both Donald Trump and the Constitution.
- The committee is viewed as an attempt by Republicans to address and remove what they see as a threat to their party and the nation.
- The committee is not partisan but rather an American effort, with critical evidence and testimony coming from Republicans.
- Beau criticizes those who try to undermine the committee's credibility by labeling it as partisan, similar to baseless claims about election fraud.
- The importance of the committee is emphasized, with Beau suggesting that many are missing out on a significant political event due to previous allegiances.
- Overall, the committee is seen as a critical effort by Republicans to address internal threats and defend American values.

# Quotes

- "You can be loyal to Donald Trump or you can be loyal to the Constitution. You can't do both."
- "If it is partisan, it is a Republican committee."
- "This isn't partisan. This is American."
- "This committee may very well be one of the most significant political events that occurs in your life."
- "Many are missing out on a significant political event due to past allegiances."

# Oneliner

Republicans on the committee are addressing threats to their party and country by confronting authoritarianism, making it an American, not partisan, effort.

# Audience

Political observers

# On-the-ground actions from transcript

- Watch the committee to understand the critical evidence and testimony presented (suggested)
- Acknowledge the importance of the committee as a significant political event (suggested)

# Whats missing in summary

Full understanding of the depth and significance of the committee's efforts and the need to prioritize American values over partisan allegiances.

# Tags

#Committee #Partisanship #Republicans #Authoritarianism #AmericanValues