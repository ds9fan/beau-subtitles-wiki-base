# Bits

Beau says:

- Providing a recap of a hearing and discussing the forest-for-the-trees issue with coverage.
- Key takeaways include the inciting tweet, the involvement of more Congress people, and the pre-planned Capitol march.
- Noteworthy moments from the hearing include Trump's former campaign manager attributing a death to Trump's rhetoric.
- The committee has shown that Trump continued to lie about the election results and attempted to legitimize baseless claims.
- Today's focus was on tying Trump personally to the events of January 6th, showing his role in inciting the Capitol riot.
- The evidence presented aims to directly link Trump's actions to the events of January 6th.
- While this hearing isn't a courtroom, the intent is to present evidence, keeping in mind the presumption of innocence.
- There will be a forthcoming video discussing the institution of the presidency and the protection of its integrity.
- Mention of ongoing criminal conspiracy attempts is made, possibly showing that current actions could be linked to past events.

# Quotes

- "Trump's rhetoric got someone killed."
- "Trump's former campaign manager said that Trump's rhetoric got someone killed."
- "Trump saw those tweets, that draft tweet, that meeting, everything shows that the events of the 6th can be directly tied to him as a person."

# Oneliner

Key takeaways from the hearing include tying Trump personally to the events of January 6th and showcasing evidence of his involvement, with implications for potential criminal charges and ongoing conspiracy.

# Audience

Congressional members, concerned citizens.

# On-the-ground actions from transcript

- Contact elected representatives to express concerns about the implications of the evidence presented (implied).

# Whats missing in summary

The emotional impact and tension of the hearing, as well as the potential implications for future legal actions based on the evidence presented.

# Tags

#Hearing #Trump #CapitolRiot #Presidency #CriminalConspiracy