# Bits

Beau says:

- Youth in Montana have launched a lawsuit against the state, claiming that the state's support for fossil fuels is contributing to environmental degradation and climate change.
- The kids argue that the state of Montana has a duty to maintain and improve a clean environment for present and future generations, as stated in the Montana State Constitution.
- The case, known as Held v. State of Montana, has faced resistance from the state but has been allowed to proceed by the Montana Supreme Court.
- The trial date for the case is set for early February 2023, with potential delays expected due to the case's significance.
- Success in this case could have a widespread impact, potentially influencing similar cases in Hawaii, Virginia, and Utah.
- Beau expresses admiration for the youth involved in the lawsuit and is eager to see how the case unfolds.
- The lawsuit represents a significant effort by young individuals to hold the state accountable for environmental protection and sustainability.

# Quotes

- "The kids are all right."
- "They do not have a constitutional right. That's not there."
- "The name of the case, if you're curious, is Held v. State of Montana."

# Oneliner

Youth in Montana challenge state support for fossil fuels, invoking constitutional duty to protect the environment; Held v. State of Montana faces resistance but moves forward, potentially setting a precedent for environmental cases nationwide.

# Audience

Youth, Environmental Activists, Legal Advocates

# On-the-ground actions from transcript

- Support youth-led environmental initiatives in your community (exemplified)
- Stay informed about environmental lawsuits and their potential impact (exemplified)
- Advocate for sustainable practices and policies in your area (exemplified)

# Whats missing in summary

The full transcript provides additional context on the legal battle between Montana youth and the state, offering insights into the constitutional basis of their claims and the potential implications of the case's outcome.

# Tags

#YouthActivism #EnvironmentalJustice #LegalAction #Montana #FossilFuels #ClimateChange