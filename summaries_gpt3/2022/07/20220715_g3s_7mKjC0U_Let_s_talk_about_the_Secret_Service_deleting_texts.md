# Bits

Beau says:

- The Secret Service and the Office of the Inspector General from DHS are at odds over the deletion of texts related to the events of January 6th.
- The Office of the Inspector General is meticulous and methodical, so their claims should be taken seriously.
- The Secret Service denies the allegations, attributing the missing texts to a migration process.
- Beau expresses skepticism towards the Secret Service's explanation and leans towards believing the Office of the Inspector General.
- If true, the intentional destruction of records by the Secret Service could have significant implications, including criminal conspiracy and cover-up.
- Beau distinguishes between ethical considerations in protecting a client's dignity and the serious nature of destroying government records.
- The issue goes beyond individual agents; it suggests a systemic problem within the agency.
- The situation is still unfolding, with accusations and denials but no concrete evidence yet.
- Beau underscores that this incident, if proven, will be a significant departure from acceptable behavior within the agency.
- The potential implications of these allegations could be a game changer.

# Quotes

- "You deleted what?"
- "The intentional destruction of records to impede, that's huge."
- "We're going to have to wait to see how this plays out."
- "This is definitely a very different ball game when it comes to acceptable behavior."
- "If these allegations are true, this is going to be a game changer."

# Oneliner

The Secret Service and the Office of the Inspector General clash over deleted texts, raising concerns of intentional record destruction and criminal conspiracy.

# Audience

Watchdog agencies, concerned citizens.

# On-the-ground actions from transcript

- Monitor developments and hold accountable those responsible (implied).
- Support transparency and accountability within government agencies (implied).

# Whats missing in summary

Full context and depth of analysis can be gained by watching the entire video. 

# Tags

#SecretService #DHS #OfficeoftheInspectorGeneral #GovernmentAccountability #Transparency