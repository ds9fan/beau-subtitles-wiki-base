# Bits

Beau says:

- Criticisms of Trump result in him claiming he doesn't know the individuals, disavowing them by pretending he never met them.
- Trump labeled the Capitol attack as a heinous attack after it could potentially harm his image, distancing himself from the incident.
- Despite wearing MAGA gear and being his supporters, Trump turned on the Capitol rioters, stating, "You will pay" and that they don't represent his movement or country.
- Trump's habit of disavowing people and distancing himself from mistakes by letting others pay for them is evident in his reaction to the Capitol attack.
- Loyalty shown to Trump by many individuals will never be reciprocated, as he easily discards people once they no longer serve his purpose.
- Even those who risked their safety, stood on the front lines, or faced custody for Trump are disregarded and excluded from his movement once he no longer needs them.

# Quotes

- "You do not represent our movement. You do not represent our country."
- "Even those people willing to put themselves at risk, those people willing to quite literally stand on the front lines for him, those people willing to be in custody for him, they're not part of his movement."
- "Loyalty shown to the former president will never be returned."

# Oneliner

Trump disavows supporters once their use is over, letting others pay for his mistakes, even those who risked everything for him.

# Audience

Supporters of Trump

# On-the-ground actions from transcript

- Stand up for accountability within political leadership (implied)
- Recognize and acknowledge patterns of behavior in political figures (implied)

# What's missing in summary

The emotional impact and frustration from individuals who have been discarded and disavowed by Trump despite their loyalty and sacrifices.

# Tags

#Trump #loyalty #accountability #politicalleadership