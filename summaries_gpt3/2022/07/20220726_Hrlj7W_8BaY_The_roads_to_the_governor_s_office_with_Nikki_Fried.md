# Bits

Beau says:

- Beau and Nikki Freed talk about the impact of the race for governor in Florida and its national implications.
- Nikki Freed explains her role as the commissioner of agriculture and consumer services, overseeing various aspects like agriculture, food programs, energy, and security.
- Freed shares her experience working with non-Democratic officials in Tallahassee and her efforts to find compromise.
- The transcript delves into Freed's stance on cannabis legalization and her lawsuit against President Biden regarding gun rights for medical marijuana patients.
- Freed differentiates herself from her primary opponent, Chris, by discussing her long-standing Democratic values and commitment to various social issues.
- The importance of beating current Governor DeSantis is emphasized, with Freed labeling him as authoritarian and a threat to democracy.
- Freed outlines her plans if elected, including declaring housing and gas emergencies, focusing on education funding, expanding Medicaid, and protecting women's rights.
- The transcript touches on the need for air conditioning in prisons as a basic necessity.
- Freed criticizes Governor DeSantis for his actions against organizations like Disney and the cruise industry, showcasing a lack of civil discourse.
- Nikki Freed encourages voter participation in the upcoming primary and presents herself as a new type of leadership for Florida.

# Quotes

- "Try something new."
- "This isn't about me at all. It's about the people."
- "We haven't had a female governor of our state."

# Oneliner

Beau and Nikki Freed dissect the impact of the governor race in Florida, Freed's roles, values, plans, and the necessity to defeat DeSantis for the people's sake.

# Audience

Florida voters

# On-the-ground actions from transcript

- Support Nikki Freed in the upcoming primary election (implied)
- Educate yourself on the candidates' policies and backgrounds (suggested)
- Encourage voter turnout and engagement in the political process (implied)

# Whats missing in summary

Detailed insights into Nikki Freed's background, her campaign's grassroots support, and the urgency of challenging authoritarian governance in Florida.

# Tags

#Florida #GovernorRace #NikkiFreed #DeSantis #Election #Democracy #VoterEngagement #GrassrootsSupport