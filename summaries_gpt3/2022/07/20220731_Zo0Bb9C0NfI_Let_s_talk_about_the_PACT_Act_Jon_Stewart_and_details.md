# Bits

Beau says:

- Explains the PACT Act and Jon Stewart's involvement in advocating for it to provide medical care to veterans exposed to toxic substances while working for the US government.
- Details how the bill aims to cover millions of people exposed to substances like Agent Orange and burn pits, with an estimated cost of $280 billion over 10 years.
- Notes that the bill didn't pass the Senate due to Republicans not backing it, leading to a filibuster.
- Points out the shift in Republican support from 84 votes in June to 55 votes in July, with a change in reasoning to make the funding discretionary rather than mandatory.
- Questions the Republican Party's stated reasons for the change and speculates on possible motives, including obstructionism or retaliation against Democratic reconciliation deals.
- Condemns the delay in providing healthcare to sick veterans and criticizes the idea of making the funding discretionary as a potential method for future cuts.
- Mentions a planned Senate vote on the bill and expresses concern over the impact of delays on veterans' health outcomes and time with their families.

# Quotes

- "Republicans are patting themselves on the back for denying health care to veterans..."
- "Every day this drags on over, in best case scenario, an accounting issue."
- "It's high stakes. People are sick. They need the treatment."
- "The longer the treatment is delayed, the more likely it is they don't have a positive outcome."
- "It's time sensitive because people are sick right now."

# Oneliner

Beau breaks down the critical details of the PACT Act, exposing the stakes for sick veterans and questioning Republican obstructionism with high emotional investment.

# Audience

Advocates, Veterans, Supporters

# On-the-ground actions from transcript

- Contact your Senators to support the PACT Act and urge them to prioritize veterans' healthcare (suggested).
- Join advocacy groups working towards ensuring medical care for veterans exposed to toxic substances (exemplified).

# Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics surrounding the PACT Act, shedding light on the importance of timely medical care for sick veterans and raising questions about potential Republican motives.

# Tags

#Veterans #PACTAct #Healthcare #RepublicanParty #Advocacy