# Bits

Beau says:

- Pence did not have the authority to order the National Guard to do anything.
- Acting Secretary of Defense Miller, who has the authority, likely received a strong suggestion from Pence.
- Miller could have then issued orders based on Pence's suggestion.
- General Milley might have been directed by Miller to assist Pence if needed.
- The media misunderstanding led to the perception of Pence giving orders, when it was actually Miller who did.
- There was no de facto 25th Amendment situation, and it's unlikely that Miller or Milley spoke directly to the Commander-in-Chief during the Capitol issue.
- Tough questions may arise due to the media's portrayal of Pence giving orders, but they can be answered by clarifying the chain of command.
- It's improbable that anyone bypassed the Commander-in-Chief due to concerns of a coup attempt.

# Quotes

- "Pence did not have the authority to order the National Guard to do anything."
- "The media misunderstanding led to the perception of Pence giving orders."
- "Tough questions may arise due to the media's portrayal of Pence giving orders."

# Oneliner

Pence lacked authority to command National Guard; media confusion led to misinterpretation.

# Audience

Citizens, Military Personnel

# On-the-ground actions from transcript

- Verify facts about chain of command (suggested)
- Seek clarification on military orders (suggested)

# Whats missing in summary

Detailed breakdown of military chain of command and decision-making process.

# Tags

#ChainOfCommand #Misinterpretation #MilitaryOrders #Pence #MediaConfusion