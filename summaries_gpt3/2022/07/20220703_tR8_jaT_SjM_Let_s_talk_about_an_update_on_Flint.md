# Bits

Beau says:

- The Supreme Court in Michigan declared charges against the former governor and others void due to procedural issues.
- The law authorizes a judge to issue arrest warrants and subpoena witnesses, but not indictments.
- The case is linked to the lead contamination crisis in Flint during 2014-2015.
- The state plans to continue trying to prove the allegations and see the process through.
- The Supreme Court emphasized the severe impact of the government's actions on innocent citizens in Flint.
- The crisis is regarded as one of the country's greatest betrayals of citizens by their government.
- The legal process involves an additional step before obtaining indictments.
- The state is committed to pursuing indictments and moving forward with the case.
- The Supreme Court decision was unanimous, possibly due to misinterpreting a rarely used law.
- Elected officials prioritizing self-interest over public service is a growing issue in the United States.

# Quotes

- "The Flint water crisis stands as one of this country's greatest betrayals of citizens by their government."
- "This type of betrayal, it's becoming more common in the United States where elected officials at least appear to be putting their own interest above the interests of the people they're supposed to serve."

# Oneliner

The Supreme Court voids charges in Flint case due to procedural errors, reflecting a growing issue of officials prioritizing self-interest over public service, impacting innocent citizens severely.

# Audience

Legal advocates, activists, concerned citizens

# On-the-ground actions from transcript

- Contact local representatives to advocate for justice for the Flint community (suggested)
- Support organizations working towards justice for communities affected by similar crises (implied)

# Whats missing in summary

The full transcript provides detailed insights into the legal process surrounding the Flint water crisis and the challenges faced in seeking justice for affected communities.