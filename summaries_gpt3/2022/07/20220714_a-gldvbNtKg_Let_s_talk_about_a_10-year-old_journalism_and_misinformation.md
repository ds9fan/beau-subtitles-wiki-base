# Bits

Beau says:

- Provides an update on a recent video to address misinformation and explain why he didn't cover the continuing story that emerged.
- Talks about a story involving a 10-year-old crossing state lines for a procedure due to a Supreme Court decision.
- Mentions pushback and accusations of the story being a hoax coming from centrist/liberal and right-wing outlets or politicians.
- Points out the arrest of the person allegedly involved in the incident as casting doubt on the hoax claims.
- Explains his confidence in the initial reporting and why he didn't request a retraction.
- Talks about red flags for spotting misinformation, focusing on the headline and journalistic standards.
- Addresses the plausibility of the story and the lack of red flags.
- Criticizes major outlets for sensationalizing the story and seeking confirmation aggressively.
- Explains the reluctance of involved parties to talk to larger outlets due to the sensitive nature of the story involving a 10-year-old.
- Condemns those who rushed to cast doubt on the story and the impact on future victims coming forward.

# Quotes

- "Ego."
- "If you're one of those people who couldn't wait, who couldn't just wait for things to play out, and instead cast doubt on a child, you never get to ask about why women don't come forward again."
- "It's a 10-year-old child. That's the story."

# Oneliner

Beau addresses misinformation surrounding a sensitive story involving a 10-year-old and criticizes those who rush to cast doubt, impacting future victims coming forward.

# Audience

Journalists, Activists, News Consumers

# On-the-ground actions from transcript

- Support responsible journalism by subscribing to credible news sources (exemplified)
- Advocate for ethical reporting practices in media outlets (exemplified)
- Stand against rushing to cast doubt on sensitive stories and victims (exemplified)

# Whats missing in summary

The full transcript provides a detailed breakdown of how misinformation spreads and impacts sensitive stories involving children, urging for responsible journalism and empathy towards victims.

# Tags

#Misinformation #Journalism #Ethics #SensitiveStories #SupportVictims