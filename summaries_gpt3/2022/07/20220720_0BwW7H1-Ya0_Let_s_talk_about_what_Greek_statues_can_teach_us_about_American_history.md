# Bits

Beau says:

- Addresses the Republican Party's stance on public education in response to a viewer's feedback.
- Talks about the importance of revisiting history, specifically mentioning Greek statues and their original coloring.
- Disputes the idea that new historical information always attacks conservative heroes.
- Mentions how information that challenges the existing narrative often gets overlooked until it enters public debate.
- Emphasizes the need to confront uncomfortable aspects of American history to fulfill the promises of the founding documents.
- Urges for a critical examination of history and a willingness to acknowledge the country's past shortcomings.
- Warns against viewing history through a rose-colored lens and advocates for facing the uncomfortable truths it reveals.

# Quotes

- "President Lincoln was not a conservative. He was a progressive who got fan mail from Karl Marx."
- "It's not new history. It's not revisionist history. It's just history."
- "If you're ever reading a book and you walk away from it thinking, yay, everything's great, you're not reading history."

# Oneliner

Beau addresses misconceptions about history, urging for a critical examination of American heroes and the country's past to fulfill its founding promises.

# Audience

History enthusiasts, educators, students

# On-the-ground actions from transcript

- Question existing historical narratives and seek out diverse perspectives (implied)
- Encourage critical thinking about historical figures and events in educational settings (implied)

# Whats missing in summary

A deeper dive into the impact of mythologized history on societal perceptions and the importance of confronting uncomfortable truths for progress.

# Tags

#History #AmericanHistory #RevisionistHistory #Education #CriticalThinking