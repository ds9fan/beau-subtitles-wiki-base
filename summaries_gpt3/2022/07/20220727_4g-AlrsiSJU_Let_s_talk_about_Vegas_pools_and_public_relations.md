# Bits

Beau says:

- Las Vegas is capping the size of backyard swimming pools due to inadequate water supply.
- The average pool size in Vegas is 470 square feet, while the cap is set at 600.
- This move targets those wanting giant pools, not for major water conservation.
- It serves as a public message about water scarcity in Vegas.
- The commission acknowledges this as a PR move, knowing its limited effectiveness.
- Despite saving a few million gallons of water, more significant decisions lie ahead.
- The world is warming, leading to more droughts and less water.
- Beau sees this as a warning shot rather than a solution.
- While a step in the right direction, tangible actions are needed for real results.
- Beau doesn't expect this cap to solve Vegas's future water issues.

# Quotes

- "This is a very public message that Vegas isn't going to have enough water."
- "I hope people understand that you can't continue to expand these cities."
- "The reality is we're way past warnings."
- "It's a step in the right direction for once."
- "I wouldn't expect this to even remotely impact the water issues that Vegas is going to have in the future."

# Oneliner

Las Vegas capping pool sizes sends a public message about water scarcity, but tangible actions are needed for real results as warnings alone won't suffice for future water issues.

# Audience

Residents, policymakers, environmentalists

# On-the-ground actions from transcript

- Address water scarcity through tangible actions (implied)
- Advocate for sustainable city planning to combat future water issues (implied)
- Raise awareness about the impacts of urban expansion on water resources (implied)

# Whats missing in summary

Beau's tone and nuanced perspective on the limitations of the pool size cap for addressing long-term water issues in Las Vegas. 

# Tags

#LasVegas #WaterScarcity #PoolSizeCap #EnvironmentalAction #UrbanPlanning