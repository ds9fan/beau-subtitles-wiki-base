# Bits

Beau says:

- Speculation about hearings on day seven and possibly day eight, including a potential surprise hearing.
- Topics to be discussed include White House connections to extremists, a meeting between Trump and "Team Crazy," and the possibility of seizing voting machines.
- Focus on statements by Cipollone that may corroborate previous testimony and be damaging to the Trump inner circle.
- Mention of testimony from someone linked to groups present on January 6th.
- Allies of the former president willing to testify only if it's live, raising concerns about shaping the format of the hearings.
- Not going well for Trump and his circle; their strategy may involve delegitimizing and disrupting the hearings.
- Warning against allowing witnesses to dictate the outcome and format of the hearings.
- Suspicions about witnesses facing serious charges and the potential impact of their testimony.
- Emphasizing the need for tying together individual pieces with dramatic testimony moving forward.

# Quotes

- "This is not going well for Trump."
- "The only real card they have to play is to try to delegitimize and disrupt the hearings."
- "It doesn't seem like putting them on TV live is a good idea."
- "There's only so much they can say and still mount a defense."
- "It's time to start tying them together with testimony that may even be more dramatic than what we've heard thus far."

# Oneliner

Beau speculates on upcoming hearings, discussing potential topics like White House connections to extremists and warning against witnesses shaping the format.

# Audience

Political observers

# On-the-ground actions from transcript

- Wait for updates and follow the developments in the hearings (implied)

# Whats missing in summary

Insights on the potential impact of tying together dramatic testimony for a conclusive outcome.

# Tags

#Hearings #Trump #Testimony #Extremists #Allegations