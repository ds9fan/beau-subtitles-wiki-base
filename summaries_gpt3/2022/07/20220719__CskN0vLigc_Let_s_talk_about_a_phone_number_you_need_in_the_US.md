# Bits

Beau says:

- Introduces the new number 988 as a mental health emergency hotline in the United States.
- Compares 988 to 911 but specifically for mental health crises.
- Mentions that currently, 988 connects callers to counselors, initially focused on suicide prevention.
- Describes future plans for 988, including mobile care units to provide mental health support.
- Advocates for shifting responsibilities away from law enforcement towards trained professionals in mental health.
- States that the United States government is investing a significant amount of money, around a quarter billion dollars, into developing 988 services.
- Envisions establishing physical locations similar to county clinics dedicated to mental health care.
- Expresses hope in this initiative as a positive step for a country with a history of neglecting mental health.
- Emphasizes the importance of remembering the number 988 for future use during mental health emergencies.
- Indicates that state legislation may be introduced to fund the 988 services, akin to how 911 is funded.

# Quotes

- "The number is 988. And it will be 911, but for mental health."
- "The idea is to eventually also have what amounts to like the little county clinics that exist, but for mental health."

# Oneliner

Beau introduces 988 as the mental health equivalent of 911, heralding a positive shift towards professional mental health support in the United States.

# Audience

Policy Makers, Mental Health Advocates

# On-the-ground actions from transcript

- Support state legislation designed to fund aspects of the 988 mental health emergency hotline (suggested).
- Memorize and spread awareness about the number 988 for mental health emergencies (implied).

# Whats missing in summary

Importance of advocating for continued funding and expansion of 988 services nationwide. 

# Tags

#MentalHealth #EmergencyHotline #SuicidePrevention #CommunitySupport #GovernmentFunding