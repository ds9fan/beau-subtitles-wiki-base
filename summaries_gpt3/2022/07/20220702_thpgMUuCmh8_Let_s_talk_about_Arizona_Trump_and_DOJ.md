# Bits

Beau says:

- Department of Justice subpoenas were issued to Arizona Senate President Karen Fann and State Senator Kelly Townsend.
- DOJ is seeking communications between Townsend and Trump's lawyers regarding the alleged fake elector scheme.
- The end goal of the subpoenas appears to be establishing a connection to Trump's inner circle.
- DOJ seems actively investigating a criminal case involving someone within Trump's circle.
- Efforts are focused on connecting the fake elector scheme to Trump's inner circle.
- There is skepticism about whether the DOJ will pursue Trump.
- Despite criticism of Merrick's handling, it seems in line with his usual approach of avoiding headlines until necessary.
- DOJ's actions indicate a tightening circle around Trump's inner circle, with Arizona playing a significant role.
- DOJ appears to be following up on interactions between Arizona officials and Trump's team regarding the election.
- The investigation appears to be moving towards a criminal case, although Merrick's approach keeps details scarce.

# Quotes

- "We will have to keep watching, but there is yet another little piece of evidence that fits into that pattern that says, yeah, they actually are doing something."
- "It's worth noting that if I'm not mistaken, Fan had a conversation with Rusty Bowers, who's the speaker out there, speaker of the house, who in that conversation kind of indicated that somebody from Trump's team had encouraged her to help him find a way to get the lead in the votes."
- "What we're seeing is that circle closing in slowly but surely around the Trump inner circle."

# Oneliner

Department of Justice's subpoenas in Arizona suggest an investigation linking the fake elector scheme to Trump's inner circle, indicating a potential criminal case.

# Audience

Legal analysts, political commentators

# On-the-ground actions from transcript

- Monitor developments and updates on the investigation in Arizona (implied).

# Whats missing in summary

Full context and nuances of the ongoing investigation and potential implications can be better understood by watching the entire transcript. 

# Tags

#DOJ #Investigation #Arizona #Trump #FakeElectorScheme