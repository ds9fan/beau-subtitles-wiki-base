# Bits

Beau says:

- Introduces the topic of starting a community network and finding people for it.
- Talks about receiving messages from people wanting to start a community network but not knowing where to find like-minded individuals.
- Explains using Twitter's search function to find people nearby.
- Shares personal experience of using Twitter to find people close by.
- Mentions searching for specific terms related to like-minded individuals.
- Describes finding familiar and unfamiliar people through the search function.
- Suggests reaching out, getting to know people online, and eventually meeting in person.
- Mentions that most social media platforms have similar search functions.
- Encourages transforming online communities into real-world impactful networks.

# Quotes

- "Got some people I want you to meet."
- "You can transform that online community into something they can do good in the real world."

# Oneliner

Beau explains how to use social media platforms like Twitter to find like-minded people nearby and build a community network that can make a real-world impact.

# Audience

Community builders, social media users

# On-the-ground actions from transcript

- Use social media platforms like Twitter to search for like-minded individuals nearby (exemplified).
- Reach out, get to know people online, and plan to meet in person (exemplified).
- Transform online communities into impactful real-world networks (implied).

# Whats missing in summary

The full transcript provides a detailed guide on leveraging social media platforms to connect with individuals nearby and establish a community network with real-world impact. Viewing the full transcript can offer additional insights and personal anecdotes from Beau on this topic.

# Tags

#CommunityBuilding #SocialMedia #Networking #TransformativeChange #OnlineCommunities