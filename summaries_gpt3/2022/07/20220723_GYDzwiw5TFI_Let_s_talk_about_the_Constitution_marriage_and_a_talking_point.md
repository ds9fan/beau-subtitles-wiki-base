# Bits

Beau says:

- Addressing the Republican Party's attempt to undermine gay marriage by claiming it's not in the Constitution.
- Explaining the Ninth Amendment and how it protects rights not explicitly listed in the Constitution.
- Criticizing those who push the talking point that rights must be specifically listed to exist.
- Pointing out the danger of authoritarian figures trying to limit freedoms by demanding specific rights be listed.
- Reminding followers to stop parroting misleading talking points and to understand the true spirit of the Constitution.
- Calling out individuals who claim to be "originalists" but fail to grasp the essence of the Constitution.
- Urging people to read and understand the Constitution rather than blindly following misleading narratives.
- Emphasizing the importance of recognizing and defending rights beyond those explicitly stated in the Constitution.
- Encouraging self-reflection and critical thinking when it comes to constitutional rights and principles.
- Condemning those who undermine freedom and the values enshrined in the Constitution.

# Quotes

- "Where in the Constitution does it say that straight people have a right to get married, right to travel?"
- "The people you're following, the people you're getting these talking points from, are the sort of people that the people who wrote the Constitution viewed as their enemy."
- "It's the Ninth Amendment."
- "Stop. It doesn't send the message you think."
- "Y'all have a good day."

# Oneliner

Addressing misconceptions about constitutional rights and the Ninth Amendment, Beau dismantles the argument that specific rights must be listed, urging critical understanding over blind adherence.

# Audience

Constitutional Rights Advocates

# On-the-ground actions from transcript

- Read and understand the Constitution (suggested)
- Stop parroting misleading talking points (suggested)
- Engage in self-reflection on constitutional principles (implied)

# Whats missing in summary

The detailed breakdown and examples provided by Beau can be best appreciated by watching the full transcript.

# Tags

#Constitution #Rights #Marriage #NinthAmendment #Originalist