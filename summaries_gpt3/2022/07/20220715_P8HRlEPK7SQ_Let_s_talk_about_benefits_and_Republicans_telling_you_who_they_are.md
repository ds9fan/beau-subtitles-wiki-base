# Bits

Beau says:

- Addressing the claimed benefits and perceived benefits of a situation involving a 10-year-old child.
- Jim Bopp, lead lawyer for the National Right to Life Group, expressed his opinion on legislation impacting children.
- Bopp's legislation lacked exceptions, meaning a 10-year-old could be forced to have a child.
- Bopp suggested that forcing a 10-year-old to have a child could have benefits, like saving money on childcare.
- Beau criticizes the idea of forcing a child to have a baby and questions the motives behind such legislation.
- The statement by Bopp reveals a worldview where child pregnancy is seen as a benefit, not a regrettable event.
- Beau calls out the Democratic Party for not effectively communicating the implications of overturning Roe v. Wade.
- Urging for more awareness about the potential consequences of legislation that normalizes child pregnancy.
- Beau stresses the importance of holding politicians accountable for their support of such legislation.
- Emphasizing the need for widespread attention to Bopp's statement and questioning politicians about their stance on such beliefs.

# Quotes

- "10-year-old moms, that's what they're saying. It's not regrettable. It's a benefit."
- "Roe is on the ballot, in fact. But they're not presenting that."
- "If they believe it is an ultimate benefit to have 10-year-old moms, that needs to be a question in every debate."

# Oneliner

Addressing the alarming belief that child pregnancy can be a benefit, revealing the need for political accountability and awareness of the potential consequences.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Question politicians about their stance on legislation impacting children (suggested)
- Raise awareness about the implications of normalizing child pregnancy (implied)

# Whats missing in summary

The emotional impact and urgency of addressing harmful beliefs regarding child pregnancy.

# Tags

#ChildPregnancy #Legislation #RoevWade #Accountability #Awareness