# Bits

Beau says:

- Recent poll data shows record low levels of American pride.
- Americans are asked how proud they are to be American, with options ranging from not at all to extremely.
- Only 38% of Americans said they were extremely proud, a record low.
- Republicans are at 58%, while Independents are at 34%.
- Demographics show that males (43%), those over 55 (51%), and those without college degrees (41%) are prouder.
- The demographic least proud are those aged 18-34, with only 25% extremely proud.
- Young people are not proud of the U.S. due to concerns like war, loss of rights, authoritarianism, lack of action on climate change, and economic struggles.
- The older population is more easily swayed against their interests, with 51% extremely proud of the country.
- Younger Americans want a better future and refuse blind nationalism without progress.
- The divide in pride levels is more about age than party affiliation.
- The youth feels left out and unrecognized by a country that doesn't prioritize their needs or rights.

# Quotes

- "Why should they be? What have they been handed?"
- "They want a better future and they're not going to be proud until they get it."
- "You can't force people to take pride in a country that doesn't care about them."

# Oneliner

Recent poll data reveals record low American pride, with young people feeling left out and unrepresented in a country that doesn't prioritize their future.

# Audience

American citizens

# On-the-ground actions from transcript

- Reach out to young people in your community to understand their concerns and amplify their voices (implied).
- Support initiatives that address the issues young Americans care about, such as climate change and economic opportunities (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the generational divide in American pride and the reasons behind it. Viewing the complete video allows for a deeper understanding of the impact of historical events and policies on different age groups' perceptions of national pride.

# Tags

#AmericanPride #GenerationalDivide #YouthConcerns #Nationalism #PollData