# Bits

Beau says:

- Explains a Department of Justice memo requiring approval for investigations into politically sensitive figures like presidential candidates to maintain DOJ's apolitical nature.
- Mentions that similar memos have existed for years and the purpose is to prevent the perception of partisan activity within the DOJ.
- Emphasizes Merrick Garland's response that nobody is above the law when asked about investigations, including former President Trump.
- Garland's decision to use a method starting from small investigations to big rather than a hub and spoke method is discussed, showing his commitment to conducting investigations privately and without leaks.
- Describes Garland's belief that the investigation into Trump is significant, relating it to the events at the Capitol on January 6th.
- Views the investigation as a wide-ranging conspiracy, potentially alarming certain individuals in Congress or former government positions.
- Acknowledges Garland's commitment to DOJ's apolitical stance but notes the influence politics can have on investigations.
- Speculates on the possibility of the investigation turning into a criminal matter against Trump based on Garland's statements about following the facts.
- Raises concerns about individuals wanting to protect the institution of the presidency possibly interfering with the investigation's integrity.
- Concludes that despite political posturing, Garland's commitment to following the facts offers hope for a thorough investigation.

# Quotes

- "No one is above the law."
- "This is the most wide-ranging and most important investigation in DOJ's history."
- "An investigation into Trump is part of what happened at the Capitol that day."
- "Garland has made it clear that in this investigation they're going to go wherever the facts lead them."
- "But, like a whole lot of this, we just have to wait and see."

# Oneliner

Beau explains a DOJ memo, Garland's response on investigations into politically sensitive figures, and implications of the wide-ranging Trump investigation.

# Audience

Legal analysts, concerned citizens

# On-the-ground actions from transcript

- Contact legal experts for insights on DOJ procedures and investigations (suggested)
- Stay informed about updates on the investigation and its implications (exemplified)

# Whats missing in summary

Full context and depth on Garland's approach to investigating politically sensitive figures and the wide-ranging implications of the Trump investigation.

# Tags

#DOJ #MerrickGarland #Investigations #Apolitical #Trump #CapitolInsurrection