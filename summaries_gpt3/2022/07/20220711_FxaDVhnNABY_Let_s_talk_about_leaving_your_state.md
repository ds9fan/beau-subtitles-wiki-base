# Bits

Beau says:

- Addressing the topic of moving due to recent legislation or rulings, or considering leaving the U.S. to return to one's home country.
- Encouraging individuals to make their own decisions about moving based on their unique circumstances.
- Advising that safety concerns should be a priority when deciding whether to stay or leave.
- Noting that leaving a state does not necessarily mean giving up, especially if it's related to resistance.
- Pointing out the importance of considering business implications when deciding whether to relocate.
- Mentioning the potential economic downturn in states with controversial legislation.
- Urging individuals to prioritize the opinions of those directly impacted by the situation.
- Acknowledging that the decision to move is significant and life-altering.
- Emphasizing that individuals should do what is best for themselves and their families, disregarding external opinions.
- Suggesting that the impact of these decisions may not be fully understood by the states involved in the long term.

# Quotes

- "If you're worried about your safety, yeah, that's an easy one. If you have the ability, yeah, get out."
- "Don't listen to anybody on social media, any commentator, anything like that. You do what's right for you and your family, nothing else."
- "I don't think it's wrong to leave. I don't think it's wrong to stay."
- "It's a huge, it's a life-altering decision."
- "These states probably do not understand what they have done to themselves."

# Oneliner

Beau advises making informed decisions about moving based on safety, resistance, business, and family impacts, prioritizing individual circumstances over external opinions.

# Audience

Individuals considering moving

# On-the-ground actions from transcript

- Make a safety plan for potential relocation (suggested)
- Consult with family and household members directly impacted by the situation (implied)

# Whats missing in summary

The emotional weight and personal reflection Beau brings to the complex decision-making process around moving in response to political circumstances.

# Tags

#Moving #Legislation #DecisionMaking #Safety #BusinessImpact