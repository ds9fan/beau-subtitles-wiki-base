# Bits

Beau says:

- Wild flooding in Yellowstone resulted in unpredicted devastation, including the evacuation of 10,000 people and destruction of property.
- Hydrologic models used for forecasting floods are becoming less reliable due to the changing climate rendering historical data inadequate.
- Climate change has led to less snowfall in winter, more rain in spring, creating a recipe for flash floods in areas like Yellowstone.
- Extreme weather incidents are becoming more common, with events previously considered 100-year occurrences now happening more frequently, almost every 20 years.
- Residents in areas prone to extreme weather need to be prepared for more frequent and intense events with less warning, such as floods or tornadoes.
- Emergency responders were caught off guard by the severity of the flooding, indicating a lack of adequate warning systems for these increasingly common events.
- People should start keeping necessary supplies on hand to better prepare for sudden extreme weather events.
- Historical weather patterns are becoming less useful for predicting future events due to climate change.

# Quotes

- "Extreme weather incidents are going to become more and more common."
- "If you live in an area where there are extreme weather events, you might want to be prepared for them to occur more frequently and with less warning."
- "It might be a good idea to start getting in the habit of keeping what you need on hand."

# Oneliner

Be prepared for more frequent and intense extreme weather events with less warning, as climate change continues to disrupt traditional forecasting methods.

# Audience

Residents in areas prone to extreme weather.

# On-the-ground actions from transcript

- Prepare an emergency kit with essentials for extreme weather events (implied).
- Stay informed about the changing weather patterns in your area and adapt your preparedness accordingly (implied).

# Whats missing in summary

Importance of adapting preparedness strategies to the evolving climate patterns for increased community resilience. 

# Tags

#ExtremeWeather #ClimateChange #EmergencyPreparedness #CommunityResilience #Forecasting