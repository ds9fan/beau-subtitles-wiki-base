# Bits

Beau says:

- Senate Armed Services Committee requested Pentagon to stop rooting out extremists in the military.
- Every Republican on the Committee voted for this request.
- Independent Senator Angus King also supported the request.
- The suggestion claimed combating extremism in the military is a wasteful use of taxpayer funds.
- The suggestion was included in the Senate's National Defense Authorization Act (NDAA).
- Both Democrats and independents opposed the suggestion.
- Beau questions the reasoning behind allowing extremists in the military.
- He calls for a cost-benefit analysis of this decision.
- Beau sees it as an attempt to shape a particular narrative.
- He urges for more debate and scrutiny on this issue.
- The Department of Defense is not bound to follow this suggestion.
- Beau is confident that the Department will continue rooting out extremists.
- He stresses the importance of understanding the motives behind this request.
- Beau calls for accountability and answers regarding why some Republicans support this stance.

# Quotes

- "Spending additional time and resources to combat exceptionally rare instances of extremism in the military is an inappropriate use of taxpayer funds."
- "That is something else."
- "Is that a good use of taxpayer funds?"
- "I think that this little part of this report should probably get a little bit more debate, a little bit more discussion, definitely more coverage."
- "We have to find out why exactly Republicans don't want the Department of Defense getting rid of extremist elements."

# Oneliner

Senate Armed Services Committee suggests halting efforts to root out extremists in the military, raising questions on motives and fiscal responsibility, met with strong opposition and calls for scrutiny.

# Audience

Congressional representatives

# On-the-ground actions from transcript

- Question the motives behind supporting the suggestion (suggested)
- Push for more debate and coverage on this issue (suggested)
- Call for accountability and answers regarding the stance on combatting extremism in the military (suggested)

# Whats missing in summary

The full transcript provides a deeper understanding of the concerning request made by the Senate Armed Services Committee and the strong opposition it faced, prompting a call for transparency and accountability in addressing extremism within the military.

# Tags

#SenateArmedServicesCommittee #Extremism #Military #DepartmentOfDefense #TaxpayerFunds