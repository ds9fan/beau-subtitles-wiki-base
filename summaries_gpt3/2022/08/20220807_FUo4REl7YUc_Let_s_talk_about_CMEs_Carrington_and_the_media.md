# Bits

Beau says:

- Explains solar storms, specifically CMEs and their potential impact on Earth.
- Describes the scale of disruptions caused by CMEs, ranging from GPS issues to total electricity shutdown.
- Mentions the current low-level CME headed towards Earth.
- Emphasizes that despite media hype, this particular event shouldn't be a significant concern.
- Notes that between now and 2025, such events may become more frequent, but mitigation plans are in place.
- Criticizes sensationalist journalism that capitalizes on fear to generate clicks and revenue.
- Stresses the effectiveness of early warning systems for solar storms.
- Encourages awareness of legitimate threats and avoiding unnecessary worry about non-global threats.
- Provides context on the Carrington event and suggests comparing it to the current situation for perspective.
- Concludes by reassuring viewers that while disruptions may occur, they are unlikely to be catastrophic.

# Quotes

- "The reality is you have to watch for those types of articles, those articles that are making bold claims, predicting things that are scary."
- "With everything that we have to actually worry about, we shouldn't spend time worrying about the things that aren't actually global threats."

# Oneliner

Beau explains solar storms, warns against media sensationalism, and encourages focusing on real global threats rather than hyped non-issues.

# Audience

Science enthusiasts

# On-the-ground actions from transcript

- Stay informed about solar storms and their potential impacts (implied)
- Avoid sharing or engaging with sensationalist articles for clicks (implied)

# Whats missing in summary

Comparison of current CME event to historical Carrington event for perspective.

# Tags

#SolarStorms #CMEs #MediaSensationalism #EarlyWarningSystems #GlobalThreats