# Bits

Beau says:

- Former Secret Service agent Tony Ornata's retirement has been announced.
- Ornata served as Trump's deputy chief of staff for operations before returning to the Secret Service.
- Ornata was at the center of a lot of testimony and questions after Hutchinson's testimony before the January 6th committee.
- Speculation suggests Ornata may need to return to answer more questions.
- James Murray, the Secret Service director, announced his retirement last month.
- President Biden announced Kimberly Cheadle as Murray's replacement.
- Ornata, having been with the Secret Service for 25 years, is leaving for the private sector.
- Ornata's responsibilities included training new agents, raising concerns given the circumstances.
- Speculation surrounds where Ornata will work next, with rumors suggesting not with Trump's companies.
- There may be challenges in getting Ornata to return before the committee as a private citizen rather than a federal employee.

# Quotes

- "A lot is probably going to be made of his departure."
- "You will see this material again in the future as things play out."
- "This timeline, this information, just kind of file it away."

# Oneliner

Former Secret Service agent Tony Ornata's retirement sparks speculation and questions about his past duties and future whereabouts, potentially impacting his return before the committee.

# Audience

Politically aware individuals

# On-the-ground actions from transcript

- Follow updates on Tony Ornata's future endeavors to stay informed (suggested).
- Stay engaged with news regarding the Secret Service for further developments (suggested).

# Whats missing in summary

The full transcript provides a detailed analysis of the personnel changes within the United States Secret Service, offering insights into the potential implications of Tony Ornata's retirement and future actions.