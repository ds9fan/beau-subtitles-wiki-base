# Bits

Beau says:

- Recounts are more about galvanizing a base and sowing doubt rather than actually changing outcomes.
- A recount altering an election outcome is incredibly rare.
- Activists in Kansas are funding a recount of nine counties related to a ballot initiative on family planning, costing over a hundred grand.
- In 2004 in Washington, the largest swing in a statewide election occurred, with Gregoire leading by 129 or 133 votes after a manual recount.
- The outcome of an election being altered by a recount is highly unlikely, especially with significant margins like in Kansas, where 165,000 votes separate the candidates.
- Politicians who push for recounts after losing by significant margins may not be fit for office.
- Recounts are often used to bring in money, galvanize supporters, sow doubt, and keep people angry rather than genuinely change outcomes.

# Quotes

- "Recounts are more about galvanizing a base and sowing doubt rather than actually changing outcomes."
- "The outcome of an election being altered by a recount is incredibly rare."
- "Politicians who push for recounts after losing by significant margins may not be fit for office."

# Oneliner

Recounts are about galvanizing bases, not changing outcomes; rare to alter elections, like in 2004's Washington case, with a swing of 129-133 votes.

# Audience

Voters

# On-the-ground actions from transcript

- Support fair and transparent election processes (implied).
- Stay informed about the realities of recounts and election outcomes (implied).

# What's missing in summary

Importance of staying grounded in reality and not being swayed by baseless claims or unrealistic expectations when it comes to election recounts.

# Tags

#ElectionRecounts #Activism #Reality #Voting #PoliticalEngagement