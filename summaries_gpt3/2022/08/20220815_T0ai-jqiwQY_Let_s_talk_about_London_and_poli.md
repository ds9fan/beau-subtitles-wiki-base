# Bits

Beau says:

- Health officials in London are providing booster vaccines to children aged one through nine due to finding polio in the sewage of eight different boroughs.
- Despite not having many reported cases, the concern is that the virus is spreading, and severe cases may not have manifested yet.
- Most individuals infected with polio do not show symptoms, with only a small percentage suffering extreme symptoms.
- The genetically similar virus found in London has no clear connection to cases in New York.
- Health officials are urging everyone, especially children under five, to ensure their vaccinations are up to date.
- The presence of polio in London is mainly attributed to vaccine gaps and potential strains from countries still using the oral vaccine.
- It is emphasized that the vaccines used in the UK and the US do not present the same issues as the oral vaccine.
- The situation with polio is unexpected and should have been eradicated by now.
- Health officials are operating with caution due to the uncertainty surrounding the current spread of polio.
- Keeping vaccinations up to date is vital to prevent the spread of polio.

# Quotes

- "Make sure that the vaccines are up to date, because right now they don't know what's going on."
- "This is something that it should be gone. It should be gone."
- "The vaccines that are used in the United Kingdom and in the United States, they're not the oral vaccine anymore, and they don't have that same issue."

# Oneliner

Health officials in London are on high alert as they provide booster vaccines for children due to the unexpected resurgence of polio, stressing the importance of updated vaccinations to prevent further spread of the virus.

# Audience

Parents, caregivers, community members

# On-the-ground actions from transcript

- Ensure that your and your children's vaccinations are up to date (suggested)
- Stay informed about vaccine recommendations and updates (suggested)

# Whats missing in summary

Importance of community awareness and proactive measures in preventing the spread of polio. 

# Tags

#Polio #Vaccination #PublicHealth #London #Children #CommunitySafety