# Bits

Beau says:

- Provides an update on the Vanessa Guillen case, mentioning the involvement of viewers in pressuring for results.
- Vanessa Guillen, a 20-year-old, was killed at Fort Hood in 2020, leading to public pressure, legislation, and reforms.
- Vanessa Guillen's family is pursuing a $35 million lawsuit for wrongful death and alleging military negligence.
- Department of Defense might argue against damages citing a law, but exceptions have been made.
- The case holds significance due to ongoing harassment and violence issues within the Department of Defense.
- Despite some progress, the problem is far from resolved, warranting continued attention.
- Public pressure has been instrumental in bringing about changes and maintaining focus on the issue.
- $35 million is not justice but a step in the right direction, potentially prompting further reforms.
- Financial penalties like this lawsuit can drive change and encourage more progress.
- Beau stresses the importance of following the case to understand consequences when progress stalls.

# Quotes

- "Public pressure really did create change."
- "Justice is her still being here."
- "Financial penalties tend to promote change."

# Oneliner

Beau provides an update on the Vanessa Guillen case, stressing the importance of ongoing attention to address harassment and violence issues within the Department of Defense, showcasing how public pressure can drive change.

# Audience

Advocates, activists, viewers

# On-the-ground actions from transcript

- Contact oversight, Senate Armed Services Committee to build pressure (implied)
- Stay informed and follow cases of injustice within institutions (suggested)

# Whats missing in summary

The emotional impact on Vanessa Guillen's family and the broader implications of seeking justice through legal means. 

# Tags

#Justice #VanessaGuillen #PublicPressure #DepartmentOfDefense #Reform