# Bits

Beau says:

- Republicans want to hold hearings to look into the Department of Justice's actions at Mar-a-Lago, with some calling to defund the FBI and labeling them as radically left.
- Beau believes it's necessary to have hearings and investigations into how the Department of Justice handled the situation at Mar-a-Lago, despite not thinking they did anything wrong.
- He speculates that a hearing might reveal that the Department of Justice moved slowly and was cautious, potentially influencing Trump positively.
- Beau suggests that Republicans calling for investigations are not genuinely seeking corruption but trying to frame a narrative where Trump is the victim and to energize their base.
- He points out the danger in portraying government institutions as enemies and warns against repeating past actions that led to harm.
- Beau criticizes the tactic of lying to the base, creating enemies, and riling them up to support Trump, which he believes might backfire in the current political climate.

# Quotes

- "This call, if anything, it shows the importance of DOJ continuing. Because they've learned nothing."
- "I've seen this movie before. I know how it ends."
- "Lie to the base, give them an enemy, rile them up, and therefore they'll coalesce together and support Trump."

# Oneliner

Republicans' calls for investigations into the Department of Justice's handling at Mar-a-Lago are seen as attempts to frame Trump as a victim and energize their base, risking repeating harmful past actions.

# Audience

Political observers, Activists

# On-the-ground actions from transcript

- Monitor and advocate for transparent and unbiased investigations into government actions (implied)
- Stay informed and push for accountability in political narratives (implied)

# Whats missing in summary

Insights on the importance of holding government institutions accountable and the risks of manipulating political narratives.

# Tags

#Republicans #DepartmentOfJustice #Investigations #PoliticalNarratives #Accountability