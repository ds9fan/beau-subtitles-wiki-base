# Bits

Beau says:

- Questions the intent behind a new talking point coming from the right regarding potential consequences if the former president were to be indicted.
- Points out that the talking point suggesting pandemonium in the streets doesn't make sense politically, given the former president's political acumen.
- Contemplates what might happen if the former president is indicted and speculates on whether he'd take a plea deal or go to trial.
- Considers the composition of a potential jury in a trial involving the former president, noting the presence of Trump loyalists.
- Raises concerns about the talking point potentially inciting riots or violent responses.
- Suggests that those pushing the talking point may believe in overwhelming evidence of guilt against the former president.
- Questions the logic behind trying to stop the indictment if the evidence is weak, as a trial could re-energize the former president politically.
- Concludes with uncertainty about the true motivations behind the talking point and its potential implications.

# Quotes

- "Why? That's weird. That's a weird talking point."
- "I don't understand the talking point."
- "It's a bad talking point for a whole bunch of reasons."

# Oneliner

Beau questions the odd political implications and potential motivations behind a talking point predicting chaos if the former president were indicted.

# Audience

Political analysts, activists

# On-the-ground actions from transcript

- Monitor and address potentially incendiary talking points in political discourse (suggested)
- Advocate for fair and unbiased legal processes (implied)

# Whats missing in summary

Insightful analysis on the potential political strategies at play during a hypothetical indictment scenario.

# Tags

#PoliticalAnalysis #FormerPresident #TalkingPoints #Indictment #LegalProcess