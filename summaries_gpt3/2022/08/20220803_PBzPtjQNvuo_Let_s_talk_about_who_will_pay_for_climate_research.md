# Bits

Beau says:

- Addresses the topic of for-profit research in relation to mitigating climate change.
- Contrasts NASA's research approach with SpaceX's for-profit endeavors.
- Points out the lack of motivation for the market to solve issues like climate change.
- Emphasizes the need for clean, limitless energy and water to address climate change effectively.
- Questions the profitability of producing clean water versus bottling and selling it in limited supply.
- Argues that for-profit companies lack the motivation to conduct research for solutions that may not be cost-effective.
- Describes corporations as solely profit-driven entities that may impede research if it affects their bottom line.
- Suggests that real solutions for climate change require either individual efforts or public funding.
- Mentions the scarcity of research for diseases that affect a small population due to cost-effectiveness.
- Advocates for research driven by knowledge-seeking goals rather than profit-driven motives.

# Quotes

- "There's no motivation for it to happen that way."
- "It's one of the major flaws in the system that we currently have."
- "If you are waiting for the corporations to save us, you're going to be waiting a long time."

# Oneliner

Addressing the flaws in for-profit research, Beau stresses the necessity of clean, limitless energy to combat climate change, urging for public or individual initiatives over corporate solutions.

# Audience

Climate activists, Scientists

# On-the-ground actions from transcript

- Advocate for public funding for research into clean, limitless energy (implied).
- Support individual initiatives focused on addressing climate change (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the limitations of for-profit research in tackling climate change and underscores the importance of alternative funding sources for transformative solutions.