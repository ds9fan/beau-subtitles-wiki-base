# Bits

Beau says:

- Republicans are spreading misinformation about 87,000 new IRS agents coming to audit people as a talking point against the Inflation Reduction Act.
- The $78 billion allocated in the Act for the IRS is spread out over 10 years to expand and hire new employees.
- Current IRS staffing is around 78,000, with half expected to retire in the next five years.
- The new employees won't all be auditors; they will also include customer service and IT staff.
- The fear-mongering claim that the new employees will focus on auditing the middle class or small businesses is false.
- The report suggests that by 2031, the IRS could hire 87,000 new employees due to the investment.
- The reality is that the increase in employees will be around 20,000 to 30,000, bringing the IRS back to previous staffing levels.
- There is no grand conspiracy to target the working class; it's merely fear-mongering and misinformation.
- Those worried about this misinformation are mostly W-2 employees who receive tax refunds, making them unlikely targets for collection.
- This misinformation is just another example of prominent Republicans creating scare tactics for their base.

# Quotes

- "It's fear-mongering. It's made up. It's just not true."
- "There is no big conspiracy here. No giant plot to go after the working class."
- "You can just add this to the giant list of things that prominent Republicans made up to scare their base."

# Oneliner

Republicans spread false fear-mongering about 87,000 new IRS agents coming to audit people, part of a pattern of misinformation to scare their base.

# Audience

Taxpayers

# On-the-ground actions from transcript

- Fact-check misinformation spread by politicians (implied)

# Whats missing in summary

Full context and detailed explanations can be better understood by watching the original video.

# Tags

#IRS #Republican #Misinformation #FearMongering #Taxpayers