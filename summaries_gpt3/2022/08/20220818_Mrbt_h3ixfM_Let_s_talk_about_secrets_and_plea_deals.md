# Bits

Beau says:

- Jonathan Tobey and his wife Diana are alleged to have attempted to sell U.S. state secrets to a foreign power.
- Jonathan, a naval engineer, smuggled secrets out in various ways, including using peanut butter as a cover.
- They passed the secrets to undercover FBI agents through dead drops, avoiding face-to-face meetings.
- No secrets were actually compromised, but the couple attempted to enter a plea deal which was rejected by the judge.
- The judge deemed the proposed 12.5 to 17.5 years sentence inadequate and demanded more time.
- Diana, alleged to have acted as a lookout, was set to receive three years in the rejected plea deal.
- The couple withdrew their guilty plea and are headed for trial next year unless a new deal is reached.
- The classified information passed on was not top secret or compartmented, but still serious enough to warrant more than 12.5 to 17.5 years.
- The judge views this crime, which exposes national security, as deserving of a punishment exceeding the initial plea deal.
- It's uncertain if a new deal is being negotiated, but this case is likely to attract significant attention due to its national security implications.

# Quotes

- "No secrets were actually compromised."
- "The judge rejected it, said that wasn't enough."
- "12 and a half to 17 and a half years was not enough."
- "Something that exposes national security is worthy of more than 12 and a half years."
- "It's just a thought, y'all have a good day."

# Oneliner

Jonathan and Diana's attempt to sell state secrets reveals gaps in sentencing for national security breaches.

# Audience

Legal observers

# On-the-ground actions from transcript

- Follow the developments of the trial and any potential new plea deals (implied)
- Stay informed about cases involving national security breaches (implied)

# Whats missing in summary

Insights on the potential implications of lenient sentencing for national security breaches.

# Tags

#NationalSecurity #SpyGames #PleaDeal #StateSecrets #LegalSystem