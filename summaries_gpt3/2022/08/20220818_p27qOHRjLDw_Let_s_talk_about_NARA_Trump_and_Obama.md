# Bits

Beau says:

- Exploring the records and archives of former presidents Trump and Obama, focusing on the false claim about Obama keeping 30 million records.
- Trump falsely claims that Obama took 30 million pages of records from his administration and didn't digitize them, which outraged historians.
- The reality is that the National Archives and Records Administration (NARA) have exclusive custody of Obama's presidential records, both classified and unclassified.
- NARA moved approximately 30 million pages of unclassified records to a facility in Chicago and maintains classified records in Washington DC.
- Beau clarifies that Trump's statements about Obama's records are inaccurate and essentially made up.
- Despite knowing where the records are kept, Trump continues to mislead by implying Obama took them with him.
- The Presidential Records Act dictates that all presidential records belong to the US government and should be maintained by NARA.
- Trump's false claims are viewed as a deflection tactic since he knows his base won't fact-check him and are unlikely to challenge his statements.
- Beau stresses the importance of not arguing with librarians when it comes to records, as they know where everything is stored.
- This misinformation about Obama's records is just one of many attempts to divert attention from the actions of the Trump administration upon leaving office.

# Quotes

- "No, he made it up. It's not true. It is inaccurate."
- "If Trump had done the same thing, we wouldn't be having this conversation."
- "Never argue with a librarian."
- "That is such a short statement to be wrong 33 million times in."
- "This misinformation about Obama's records is just one of many attempts to divert attention."

# Oneliner

Beau clarifies the false claims about Obama's records, exposing them as deflection tactics, while underscoring the importance of not arguing with librarians regarding records.

# Audience

History Buffs

# On-the-ground actions from transcript

- Contact local libraries or archives to learn more about preserving and accessing historical records (suggested)
- Support organizations that advocate for transparency and accountability in government record-keeping (implied)

# Whats missing in summary

Deeper insights into the implications of spreading misinformation and the importance of holding leaders accountable for factual accuracy.

# Tags

#PresidentialRecords #Misinformation #NationalArchives #Accountability #Transparency