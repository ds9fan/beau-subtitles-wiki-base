# Bits

Beau says:

- Announcement out of Indianapolis, Indiana, regarding a global company planning for more employment growth outside of Indiana due to a new law.
- Eli Lilly, a company headquartered in Indianapolis for over 145 years, is concerned about recruiting and attracting talent to a state that restricts rights.
- The new law is a result of a Supreme Court reversal that has led to concerns about recruitment and job retention in Indiana.
- This move by Eli Lilly may lead to job losses in Indiana and a shift of jobs elsewhere.
- The impact of this decision goes beyond one company as other companies may silently be making similar decisions due to laws curtailing rights.
- Companies with consumer-facing operations may choose to move jobs elsewhere to avoid upsetting a portion of the population supporting such laws.
- Republican leadership's victory in enacting these laws may result in economic downturns in states implementing them.
- Anticipated consequences include job losses, higher taxes, worsening education, and increased need for assistance as family planning rights are restricted.
- States with such legislation may face serious economic challenges in the coming years, with impacts not fully felt until later.
- The choices made by leaders to restrict rights based on outdated polling data may lead to long-term negative economic repercussions.

# Quotes

- "It's not one company. There's a bunch. They just can't all announce it."
- "This is your state on Republican leadership."
- "Less jobs. That's the Republican Party victory."
- "The economies of these states will suffer dramatically."
- "They chose to embark on a multi-decade campaign to strip half the nation of their rights."

# Oneliner

An Indianapolis company's decision to move jobs due to restrictive laws signals broader economic repercussions and challenges Republican leadership's choices.

# Audience

Indiana residents, activists, policymakers

# On-the-ground actions from transcript

- Contact local legislators to express concerns about the economic impacts of restrictive laws (suggested).
- Support organizations advocating for rights and fair employment practices in Indiana (exemplified).
- Engage in community dialogues about the implications of legislation on job retention and recruitment (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the economic consequences of laws curtailing rights and the long-term effects on job markets and education systems.

# Tags

#EliLilly #Indiana #RepublicanLeadership #EconomicImpact #JobLosses