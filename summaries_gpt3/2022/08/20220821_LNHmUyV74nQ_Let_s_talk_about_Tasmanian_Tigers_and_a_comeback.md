# Bits

Beau says:

- Introducing Tasmanian Park, a scientific endeavor to bring back the extinct animal, the Tasmanian tiger, mylocene.
- The project aims to extract DNA from skeletal remains, although they won't have the full genome.
- Some compare this project to the controversial woolly mammoth resurrection project.
- Critics argue that de-extinction is a scientific fairy tale and a waste of money.
- Beau acknowledges the high costs of these projects and suggests the money could be better used to prevent current biodiversity loss.
- Despite potential commercialization and ethical concerns, Beau sees value in sparking interest and curiosity in extinct animals and biodiversity.
- He believes the project could lead to valuable research even if unsuccessful in bringing back the Tasmanian tiger.
- Beau doesn't see significant harm in the project and believes it could potentially aid in biodiversity restoration.
- He acknowledges the ethical question of the animal's health and uncertainties surrounding the project but ultimately sees no harm in attempting it.
- Beau concludes by sharing his perspective that the project may have positive outcomes and encourages viewers to ponder the implications.

# Quotes

- "Everything's impossible until it's not."
- "Interest in the idea of extinct animals."
- "It's out there. It's weird. It's good."
- "I don't see the objection to trying."
- "Maybe if we can get a handle on things at some point, we can help restore biodiversity through this method."

# Oneliner

Beau introduces a project to resurrect the Tasmanian tiger, sparking controversy over de-extinction and raising questions about biodiversity preservation while seeing potential value in the endeavor.

# Audience

Science enthusiasts, conservationists

# On-the-ground actions from transcript

- Support initiatives focused on biodiversity preservation and preventing animal extinction (implied)
- Stay informed about scientific advancements in conservation efforts (implied)

# Whats missing in summary

The full transcript provides a nuanced exploration of the ethical and practical considerations surrounding de-extinction projects like resurrecting the Tasmanian tiger, encouraging viewers to think critically about the implications and potential benefits of such endeavors.

# Tags

#Deextinction #TasmanianTiger #Biodiversity #Ethics #Conservation