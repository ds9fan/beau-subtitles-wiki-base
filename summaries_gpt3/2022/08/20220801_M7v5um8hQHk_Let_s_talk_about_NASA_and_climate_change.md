# Bits

Beau says:

- Explains the importance of NASA's projects in relation to climate change.
- Acknowledges the need for urgent action to mitigate climate change impacts.
- Lists various critical areas for immediate focus, such as water purification, pollution remediation, solar research, and food preservation.
- Points out that NASA's technology has been instrumental in various everyday innovations like LASIK eye surgery, baby formula, and cordless vacuums.
- Emphasizes NASA's role in developing technology to ensure human survival in challenging environments.
- Advocates for continued support for NASA alongside dedicated climate change projects.
- Suggests using NASA's track record of spinoff technologies as a tool to advocate for climate-focused projects.
- Stresses the potential for significant benefits and spinoffs from well-funded climate change initiatives.
- Encourages the idea of simultaneously supporting NASA and investing in climate change solutions.
- Concludes with a call to action to recognize the dual benefits of supporting both NASA and climate change projects.

# Quotes

- "NASA's job is to basically make sure that people can survive in places where they shouldn't be able to."
- "Most people, people care about the pebble in their shoe. And they're just starting to fill it."
- "It's not either or. With the money that we spend on stuff that we really don't need to, I think that we can do both."

# Oneliner

Beau explains the symbiotic relationship between NASA's innovations and climate change initiatives, advocating for support in both areas to address critical challenges effectively.

# Audience

Advocates and policymakers

# On-the-ground actions from transcript

- Support NASA initiatives and advocate for increased funding for climate change projects (implied).
- Use NASA's technological advancements as examples to garner support for climate change initiatives (implied).

# Whats missing in summary

The full transcript provides a detailed exploration of how NASA's technological spinoffs can benefit climate change initiatives and why supporting both NASA and climate-focused projects is vital for addressing global challenges effectively.

# Tags

#NASA #ClimateChange #Innovation #Technology #Advocacy