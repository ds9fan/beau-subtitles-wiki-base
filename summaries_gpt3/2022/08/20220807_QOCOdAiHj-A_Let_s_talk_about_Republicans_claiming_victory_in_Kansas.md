# Bits

Beau says:

- Republicans claiming to be champions of democracy after a failed authoritarian move.
- Kansas allowed family planning, so Republicans sent it back to states claiming victory.
- Michigan fighting a law from 1931 with trigger laws in place, denying people a voice.
- Republican Party wants to rule rather than represent the people.
- Accusation of launching a multi-decade campaign to take away rights.
- Consequences: economic and political costs for states following anti-rights decisions.
- Accusation of painting women needing family planning services as evil.
- Criticism of Republicans trying to reframe as champions of democracy after backlash.
- People had rights, Republicans tried to take them away, now facing political consequences.
- Republicans pushed people away by trying to take away their rights and villainizing them.

# Quotes

- "No, you don't get credit for being a champion of democracy because you're a failed authoritarian goon."
- "You made it really clear what the goal was. This is your failure."
- "You pushed them away. You tried to take away their rights. You villainized them."
- "People had rights. You tried to take them away. Now you're suffering for it politically."
- "Deal with it."

# Oneliner

Republicans claiming to champion democracy after failed authoritarian moves, facing political consequences for trying to take away people's rights.

# Audience

Voters, activists, community members

# On-the-ground actions from transcript

- Organize community forums to raise awareness about reproductive rights (suggested)
- Support organizations advocating for family planning services (implied)
- Get involved in local politics to ensure representation over rule (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's attempts to paint themselves as champions of democracy despite their actions against people's rights. Watching the full video gives a deeper insight into the consequences of authoritarian approaches to governance.

# Tags

#RepublicanParty #Authoritarianism #Rights #Democracy #PoliticalConsequences