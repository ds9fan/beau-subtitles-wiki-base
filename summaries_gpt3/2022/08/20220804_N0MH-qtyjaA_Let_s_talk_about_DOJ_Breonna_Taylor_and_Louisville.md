# Bits

Beau says:

- Department of Justice charges four former or current cops in Louisville related to Breonna Taylor's death in March 2020.
- Charges include civil rights offenses, obstruction, use of force, and unlawful conspiracy.
- Allegation is that the cops falsified the initial search warrant leading to a comprehensive probe.
- The investigation began around April 2021 and is now yielding results.
- The Justice Department's process is unpredictable and catches people off guard.
- Federal convictions tend to be higher than state convictions, possibly leading to plea deals.
- Ongoing separate probe into the Louisville department for racial discrimination and excessive force.
- Expect more developments from the ongoing department probe.
- The charges may lead to convictions due to federal involvement.

# Quotes

- "Department of Justice charges four former or current cops in Louisville."
- "Federal theory is that they falsified the initial search warrant."
- "The Justice Department's process is unpredictable."

# Oneliner

Department of Justice charges cops in Louisville over Breonna Taylor's death, leading to further probes into the department.

# Audience

Justice Seekers

# On-the-ground actions from transcript

- Contact local officials for updates on police reform probes (suggested).
- Join or support organizations working on police accountability and racial justice (implied).

# Whats missing in summary

Further details on the ongoing probe into patterns of racial discrimination and excessive force within the Louisville department.

# Tags

#Justice #PoliceReform #BreonnaTaylor #DepartmentOfJustice #Louisville #RacialJustice