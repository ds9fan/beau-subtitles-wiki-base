# Bits

Beau says:

- Addressing an emerging public health issue related to polio in New York.
- New York state health officials are urging people to get vaccinated for polio due to concerns about community spread.
- Polio virus has been found in wastewater samples from Rockland and Orange counties with low vaccination rates.
- The confirmation of one paralyzed individual raises concerns about potential additional cases.
- State health officials advise ensuring vaccinations are up to date before sending kids to school.
- Emphasizing the importance of vaccination to prevent the spread of polio in the community.
- Polio was previously eradicated in the U.S. through vaccination efforts.
- Urging prompt vaccination to combat the re-emergence of polio.
- Stressing the need to prevent the situation from escalating.
- Encouraging individuals to consult with a doctor and get vaccinated as soon as possible.

# Quotes

- "New York state health officials are concerned about the possibility of community spread of polio."
- "This isn't something that we can let get out of hand."
- "This is a disease that was eradicated in the United States through vaccination."

# Oneliner

New York health officials raise alarm on community spread of polio, urging prompt vaccination to prevent escalation and protect public health.

# Audience

Health-conscious individuals

# On-the-ground actions from transcript

- Consult with a doctor to ensure vaccinations are up to date before sending kids to school (implied).
- Get vaccinated as soon as possible to prevent the spread of polio in the community (implied).

# Whats missing in summary

The importance of community-wide vaccination efforts to safeguard against the re-emergence of polio.

# Tags

#PublicHealth #Polio #Vaccination #CommunitySpread #NewYork