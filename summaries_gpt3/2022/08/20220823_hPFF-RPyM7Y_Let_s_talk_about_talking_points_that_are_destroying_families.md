# Bits

Beau says:

- Overheard a man in a grocery store expressing anger over LGBTQ content in schools.
- Shared statistics on LGBTQ identification percentages in the US.
- Explained how policies and rhetoric impact LGBTQ children in schools.
- Questioned the impact on parents if their child is LGBTQ.
- Discussed the consequences of parents prioritizing rhetoric over family.
- Revealed a personal anecdote about a man in the grocery store with an estranged LGBTQ son.
- Urged against destroying families over hateful rhetoric.
- Encouraged reflection on prioritizing family over divisive talking points.
- Ended with a message of reflection and well-wishes for the day.

# Quotes

- "It seems like it wouldn't be worth it to destroy your family over some slick graphics and bad talking points."
- "Overheard a man going off about normal right-wing talking points."
- "So when these policies, and these talking points, and this rhetoric gets pushed, it's impacting a child in every class."

# Oneliner

Beau shares a powerful story of how divisive rhetoric can rip apart families, urging against prioritizing hate over loved ones.

# Audience

Parents, Families, Advocates

# On-the-ground actions from transcript

- Prioritize open communication and understanding within families (implied).
- Challenge harmful rhetoric and discriminatory policies that impact LGBTQ children (implied).

# Whats missing in summary

The emotional impact and depth of the personal story shared by Beau.

# Tags

#LGBTQ #Family #Rhetoric #Community #Hate #Statistics