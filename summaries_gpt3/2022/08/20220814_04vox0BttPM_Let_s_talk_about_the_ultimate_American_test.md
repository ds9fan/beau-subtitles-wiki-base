# Bits

Beau says:

- Beau introduces a different format for his video, inviting viewers to participate in a quiz.
- He presents 10 questions related to American history and government, encouraging viewers to write down their answers.
- Questions range from the number of amendments in the U.S. Constitution to identifying writers of the Federalist Papers.
- Beau provides answers to the questions, revealing that they are from the American citizenship test.
- He mentions that most people can answer only five questions correctly, which is insufficient to pass the citizenship test.
- Beau references a recent incident where someone suggested immigrants should pass a test before entering the country.
- The person who made the suggestion agreed to take the test themselves but could only answer three questions.
- Beau concludes by leaving viewers with some food for thought about citizenship tests and knowledge of American history.

# Quotes

- "This is the ultimate test of America."
- "Most people can answer five of them. Incidentally, that means you don't pass."
- "America first, right?"
- "Y'all have a good day."

# Oneliner

Beau invites viewers to test their knowledge with 10 questions from the American citizenship test, shedding light on the importance of civic education and challenging assumptions about immigrants' knowledge.

# Audience

Viewers, citizens

# On-the-ground actions from transcript

- Take an American citizenship test (implied)
- Engage in civic education to understand American history and government (implied)

# Whats missing in summary

The full video experience and Beau's engaging presentation style.

# Tags

#AmericanHistory #CitizenshipTest #CivicEducation #Knowledge #Immigration