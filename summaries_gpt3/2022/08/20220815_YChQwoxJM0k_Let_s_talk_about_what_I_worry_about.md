# Bits

Beau says:

- Received a message questioning his usual dismissal of concerns in the media.
- Identifies three major worries for people in the United States.
- First concern is climate change, urging action to cut emissions and mitigate effects.
- Points out cities already facing drastic policies due to climate change.
- Warns of the unstoppable consequences if action is delayed.
- Second worry is the rise of authoritarianism in the U.S.
- Notes a significant portion of the population desiring authoritarian leadership.
- Expresses concern about the difficulty of removing an authoritarian regime once established.
- Emphasizes the importance of pushing back against authoritarian tendencies.
- The third worry is the potential for catastrophic civil unrest in the U.S. due to accelerationists.
- Stresses that these concerns could escalate quickly and have long-lasting effects.
- Urges vigilance and action to prevent these scenarios from unfolding.

# Quotes

- "That's a worry. That's a real concern."
- "An authoritarian regime that really got seated in the United States would be very, very hard to dislodge."
- "Civil unrest in the United States would be catastrophic, and it would be very long-lasting."

# Oneliner

Beau warns about climate change, authoritarianism, and civil unrest in the U.S., urging vigilance and action to prevent catastrophic consequences.

# Audience

United States citizens

# On-the-ground actions from transcript

- Advocate for policies to cut emissions and mitigate the effects of climate change (implied)
- Push back against authoritarian tendencies through activism and awareness (implied)
- Work towards preventing civil unrest by promoting unity and understanding in communities (implied)

# Whats missing in summary

Beau's detailed explanations and examples of each concern can be better understood by watching the full video.