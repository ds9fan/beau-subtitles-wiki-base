# Bits

Beau says:

- Pat Cipollone received a subpoena from the federal grand jury, sparking a unique situation due to his prior testimony invoking executive privilege.
- The Department of Justice (DOJ) issued the subpoena, not Congress, indicating a departure from their usual defense of privilege.
- DOJ's involvement suggests a deeper look into Trump, as other information could be obtained without Cipollone.
- There are ongoing negotiations to determine what Cipollone can disclose, with potential routes including discussing matters outside his duties or crimes where privilege doesn't apply.
- Cipollone seemed ethically bound by executive privilege during his previous testimony, hinting that he might share more if those restrictions are lifted.
- The length of the fight over privilege could be months, but Beau believes DOJ should facilitate Cipollone's ethical disclosures efficiently.

# Quotes

- "DOJ is looking at Trump. There's no other reason to talk to Cipollone."
- "This subpoena is very indicative of a Department of Justice that is looking into the Oval Office."
- "If DOJ is smart, they'd recognize him as a witness that wants to provide information."
- "This is going to be the real thing that kind of breaks through the wall to get inside that inner circle with all of Trump's people."

# Oneliner

Pat Cipollone faces a DOJ subpoena, revealing deeper scrutiny into Trump with ethical disclosure negotiations ongoing.

# Audience

Legal Observers

# On-the-ground actions from transcript

- Support organizations advocating for transparency and accountability in legal proceedings (implied).

# Whats missing in summary

Insights on the potential impact of Cipollone's testimony on the investigation and the broader political sphere.

# Tags

#DOJ #Subpoena #ExecutivePrivilege #Trump #Investigation