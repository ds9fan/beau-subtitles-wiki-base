# Bits

Beau says:

- Federal government announces cuts in water allotments from the Colorado River due to moving into a tier two shortage.
- Arizona will lose 21% of its water allocation, equivalent to 592,000 acre feet.
- Nevada will face an 8% cut, impacting an area with significant water needs.
- Mexico will experience a 7% reduction in water flow.
- Arizona's $15 billion agricultural industry heavily relies on these water flows.
- The 22-year-long drought in the region is not just a temporary situation; it's the new normal.
- Continuing to refer to the ongoing drought as temporary misleads people into expecting a quick resolution.
- The federal government's actions aim to prevent a catastrophic loss in the Colorado River basin but may not be sufficient.
- The increasing population and demands in the area, combined with limited water resources, pose a significant challenge.
- It's time to acknowledge the reality of climate change and the prolonged impact of the drought on water availability.

# Quotes

- "This is the new normal."
- "We have to stop pretending."
- "We can't continue to act like it's going to end tomorrow."

# Oneliner

Federal cuts in water allotments from the Colorado River signal the new normal of a prolonged drought, urging a shift in mindset towards addressing long-term climate challenges.

# Audience

Southwestern Residents

# On-the-ground actions from transcript

- Prepare for worsening water shortages by implementing water conservation measures (implied).
- Support local initiatives promoting water recycling and conservation efforts (implied).
- Stay informed and advocate for sustainable water management practices in the region (implied).

# Whats missing in summary

The emotional and environmental toll of the prolonged drought on ecosystems, livelihoods, and communities.

# Tags

#WaterCuts #ClimateChange #ColoradoRiver #WaterConservation #Drought