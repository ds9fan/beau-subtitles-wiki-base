# Bits

Beau says:

- Explains the opposition to universal access to higher education and free college.
- Describes how some view degrees as membership cards for higher paying job clubs.
- Points out the class barrier maintained by keeping education expensive.
- Talks about the fear of competition from bright, poor students by mediocre, upper-class kids.
- Emphasizes the need to break down class barriers and level the playing field in education.
- Challenges the notion of meritocracy and exposes the fear of competition among the privileged.
- Addresses the existence of class barriers in the United States and the resistance to breaking them down.

# Quotes

- "It's credentialing. It's a club pass. It gets you into the club where the higher paying jobs are and that's it."
- "Maybe somebody will get a little bit of social mobility out of it."
- "Everybody's getting awfully scared of a little bit of competition all of a sudden."
- "The system's broke in case you haven't figured it out."
- "We pretend these class barriers don't exist."

# Oneliner

Beau explains opposition to free higher education as perpetuating class barriers, challenging the fear of competition among the privileged, and advocating for breaking down social divides.

# Audience

Education advocates, activists

# On-the-ground actions from transcript

- Advocate for policies that support universal access to higher education (suggested)
- Support scholarships and financial aid programs for students from lower socioeconomic backgrounds (implied)

# Whats missing in summary

The full transcript provides a deep dive into the societal implications of opposition to universal access to higher education and the perpetuation of class barriers through expensive education systems. Viewing the full transcript will provide a comprehensive understanding of these issues. 

# Tags

#HigherEducation #ClassBarriers #FreeCollege #Meritocracy #SocialJustice