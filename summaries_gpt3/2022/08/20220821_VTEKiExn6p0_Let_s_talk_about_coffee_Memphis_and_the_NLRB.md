# Bits

Beau says:

- National Labor Relations Board intervened on behalf of seven employees fired by Starbucks in Memphis.
- The firings came after the employees led a unionization effort and allegedly allowed people in the store after hours.
- The NLRB determined a discriminatory motive behind the terminations and ordered Starbucks to reinstate all seven employees.
- Starbucks plans to appeal the judge's decision.
- The lawyer for the NLRB emphasized the importance of allowing workers to freely exercise their right to join together and improve working conditions.
- The push for unionization in the U.S. is growing across various industries.
- Unions are not limited to specific types of jobs but are for any employee seeking fair treatment and better working conditions.
- The ruling in favor of the fired Starbucks employees is significant for supporting the union movement.
- The increase in profits, stagnant wages, and rising prices across industries are fueling more union activity nationwide.
- Expectations are for a rise in union activity in various locations as these economic challenges persist.

# Quotes

- "Unions are for everybody."
- "The reality is a union is for any employee who isn't getting a fair shake or wants to collectively bargain."
- "The push for unionization in the U.S. is growing across various industries."
- "The increase in profits, stagnant wages, and rising prices across industries are fueling more union activity nationwide."
- "Unions are, well, I mean, they're for everybody."

# Oneliner

National Labor Relations Board orders Starbucks to reinstate seven fired employees in Memphis after leading a unionization effort, underscoring the growing push for unions across industries amid economic challenges.

# Audience

Employees, activists, labor advocates

# On-the-ground actions from transcript

- Support workers' rights to unionize (implied)

# Whats missing in summary

Full understanding of the impact of growing unionization efforts across various industries in the U.S.

# Tags

#Unionization #LaborRights #NLRB #Starbucks #EconomicJustice