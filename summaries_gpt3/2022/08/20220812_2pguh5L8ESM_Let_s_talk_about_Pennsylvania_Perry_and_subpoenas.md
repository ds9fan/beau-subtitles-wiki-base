# Bits

Beau says:

- Several Pennsylvania state House and Senate Republicans were visited by the Federal Bureau of Investigation or given subpoenas.
- Some members were explicitly told they were not targets of the investigation.
- Scott Perry stated that his lawyers were informed he's not a target and he's cooperating.
- The Department of Justice, Office of Inspector General (OIG) is running the investigation.
- Different labels like witness, person of interest, subject, target have fluid meanings in investigations.
- The status of individuals in investigations can change as new information emerges.
- The investigation is likely related to the fake electors scheme or the pressure campaign against Pence.
- While current coverage focuses on classified material investigation, there are other ongoing investigations.
- People may be involved in multiple aspects of the investigation with overlapping connections.
- The Department of Justice is continuing its investigations despite political claims.

# Quotes

- "Different terms like witness, person of interest, subject, target have fluid meanings in investigations."
- "People may believe they aren't targets but could end up as targets as investigations progress."

# Oneliner

Several Pennsylvania Republicans visited by FBI, some not targets; DOJ investigation continues with fluid statuses, potential ties to fake electors scheme or Pence pressure campaign.

# Audience

Legal Observers

# On-the-ground actions from transcript

- Stay updated on the developments in ongoing investigations (implied).
- Stay informed about the different terms used in investigations and their fluid meanings (implied).
- Cooperate fully with legal processes if involved in any investigations (implied).

# Whats missing in summary

Full context and detailed explanations are missing from the summary.

# Tags

#Pennsylvania #FBI #DepartmentOfJustice #Investigations #LegalProcess