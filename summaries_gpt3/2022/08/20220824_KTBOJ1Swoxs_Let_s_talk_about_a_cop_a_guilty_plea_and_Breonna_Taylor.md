# Bits

Beau says:

- Update on the Department of Justice news regarding the Breonna Taylor case.
- Former Louisville cop, Kelly Goodlett, pleaded guilty for helping falsify information to obtain the warrant.
- Goodlett wasn't indicted; she was charged via complaint and entered a guilty plea.
- Goodlett's sentencing is scheduled for November 22nd, but there might be delays due to extenuating circumstances.
- The judge's statement during the hearing indicates Goodlett's cooperation, which could influence her sentence.
- This development is positive news for Breonna Taylor's family but bad news for other cops facing charges.
- Three other cops are facing charges, with the maximum sentence being life imprisonment.
- Beau clarifies that "life" in the federal system means exactly that - life without the possibility of parole.
- Goodlett's potential sentence is uncertain, with the charge she pleaded to carrying a maximum of five years.
- Beau had predicted the possibility of cops pleading guilty due to information suggesting cooperation within their ranks.

# Quotes

- "It means life. You go into the cage, you come out when you go into the forever box."
- "Goodlett's plea significantly strengthens the federal case, if that is, in fact, what's happening."
- "This significantly strengthens the federal case, if that is, in fact, what's happening."

# Oneliner

Former Louisville cop pleads guilty in the Breonna Taylor case, potentially strengthening the federal case and indicating cooperation within their ranks.

# Audience

Justice seekers

# On-the-ground actions from transcript

- Support organizations advocating for police accountability (implied)
- Stay informed and engaged with updates on the case (implied)

# Whats missing in summary

Details on the federal case's progress and potential implications.

# Tags

#BreonnaTaylor #DepartmentOfJustice #PoliceAccountability #Cooperation #FederalCase