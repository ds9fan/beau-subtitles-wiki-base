# Bits

Beau says:

- Freedom is a term thrown around in the United States without a clear definition.
- People often define freedom as the ability to do what you want.
- In reality, freedom is about the absence of necessity, coercion, or constraint in choice or action.
- In the US, freedom is equated with money due to the societal system.
- Without money, individuals are coerced into working even when sick to avoid homelessness.
- Politicians opposing minimum wage and living wage are, by definition, anti-freedom.
- The coercion of the system keeps the lower levels of society in place.
- Lack of money leads to a life of necessity and coercion, not freedom.
- Changing one's social class in the US is rare and not easily achievable.
- Politicians defending corporations over citizens perpetuate a system of coercion and lack of freedom.

# Quotes

- "Politicians who push back against the minimum wage are anti-freedom."
- "In the US, freedom is equated with money."
- "Those on the bottom are kept in line through coercion."
- "Lack of money leads to a life of necessity and coercion, not freedom."
- "Changing social classes in the US is rare and not easily achievable."

# Oneliner

Freedom in the US is tied to money, with lack of funds leading to coercion and a life devoid of true freedom, perpetuated by politicians opposing fair wages.

# Audience

Advocates for fair wages

# On-the-ground actions from transcript

- Advocate for raising the minimum wage and a living wage for all (suggested)
- Support policies that empower individuals financially (implied)

# Whats missing in summary

The full transcript expands on the impact of financial coercion on freedom and the role of politicians in perpetuating this system.

# Tags

#Freedom #US #Money #Wages #Politicians #Coercion