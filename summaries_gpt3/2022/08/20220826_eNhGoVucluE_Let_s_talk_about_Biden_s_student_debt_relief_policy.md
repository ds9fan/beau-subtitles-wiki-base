# Bits

Beau says:

- Explains the Biden administration's policy on student debt and universal access to higher education.
- Debunks the myth that the working class will be paying off the student loan debt of doctors and lawyers.
- Points out the hypocrisy of right-wing rhetoric regarding taxes and job creators.
- Addresses the argument that removing student debt forgiveness is a tool for military recruitment.
- Challenges those who say "but I paid mine" by suggesting it may be a result of voting against universal education.
- Notes that universal access to higher education benefits society as a whole.
- Advocates for universal higher education, even though the current policy doesn't go far enough.
- Emphasizes that the right wing opposes education to maintain control over the populace.
- States that those who oppose universal education are voting against their children's interests.
- Argues that billionaires, as job creators, should bear the cost of higher education.

# Quotes

- "You paid yours off? Well, that's probably because of the way you voted."
- "They're doing everything they can to get people to vote against their own interest."
- "This helps those on the bottom."
- "If you're opposing the idea of universal education, you're voting against your kids' interests."
- "They want you easy to control."

# Oneliner

Beau breaks down myths and challenges the opposition, advocating for universal higher education as a benefit to society.

# Audience

Voters, Students, Advocates

# On-the-ground actions from transcript
- Advocate for universal higher education (implied)
- Educate others on the benefits of universal access to education (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the misconceptions surrounding student debt and higher education, advocating for universal access while challenging opposing viewpoints.

# Tags

#StudentDebt #HigherEducation #UniversalAccess #Voting #CommunityBenefit