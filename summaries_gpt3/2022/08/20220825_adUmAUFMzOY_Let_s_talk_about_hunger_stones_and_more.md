# Bits

Beau says:

- Explains the concept of hunger stones as markers near rivers in Europe that indicate famine when visible due to low water levels.
- Mentions the current 500-year drought in Europe leading to various historical structures and artifacts becoming visible due to low water levels.
- Points out similar situations in other parts of the world like China, where ancient Buddhist statues have become visible due to low water levels in the Yangtze.
- Raises concerns about the increasing global population's impact on water resources and the potential effects of climate change.
- Emphasizes the importance of taking real action beyond campaign promises to address climate change effectively.
- Stresses the need for mobilization and concrete policies to combat climate change on a global scale.
- Urges acknowledgment of the reality of climate change and the necessity of immediate action to prevent further deterioration.
- Warns about the worsening effects of climate change if high emissions levels persist.
- Calls for awareness and proactive measures to address the environmental challenges faced globally.

# Quotes

- "We can't act like this isn't happening."
- "It's not just Europe. It's not just the US. It is a real issue."
- "We can't allow ourselves to look away on this one."
- "We need that mobilization."
- "As long as our emissions continue, it's going to get worse."

# Oneliner

Beau explains hunger stones, global drought effects, and the urgency of concrete climate action beyond promises.

# Audience

Climate activists, policymakers, communities

# On-the-ground actions from transcript

- Advocate for concrete climate policies (implied)
- Support initiatives for renewable energy transition (implied)
- Raise awareness about climate change effects (implied)

# Whats missing in summary

The detailed examples of historical structures and artifacts becoming visible due to low water levels and the potential consequences of ignoring the effects of climate change.

# Tags

#ClimateChange #GlobalDrought #HungerStones #RenewableEnergy #EnvironmentalAction