# Bits

Beau says:

- Commentary on the future of the Republican Party, focusing on figures like Karl Rove and Dick Cheney speaking out against Trump.
- Dick Cheney criticizes Trump, labeling him as a threat to the Republic and questioning his Republican identity.
- Cheney's statements suggest that Trump is an authoritarian using the Republican Party for his gain.
- Despite Cheney's own history, he recognizes Trump as a different, more extreme authoritarian figure.
- The Republican Party is urged to choose between being a legitimate political party or a cult of personality around Trump.
- Beau underscores the urgency for the Republican Party to make this decision promptly.

# Quotes

- "Dick Cheney has been a Republican since the 60s. He has photos of him chilling with Nixon. He is the Republican Party."
- "The Republican Party is facing a reckoning. They have to decide whether or not they want to be a legitimate political party, a conservative party."
- "They have to make it now. There's not a lot of time to think about this one. You had that time for six years and it was wasted."

# Oneliner

Beau delves into Dick Cheney's criticism of Trump, urging the Republican Party to choose between legitimacy and a cult of personality around Trump urgently.

# Audience

Republican voters

# On-the-ground actions from transcript

- Organize community dialogues to critically analyze the current state of the Republican Party and its future (implied).
- Reach out to fellow Republicans to have open and honest conversations about the direction of the party (implied).

# Whats missing in summary

Insight into Beau's perspective on how Republicans should navigate their party's future. 

# Tags

#RepublicanParty #Trump #DickCheney #Authoritarianism #PoliticalAnalysis