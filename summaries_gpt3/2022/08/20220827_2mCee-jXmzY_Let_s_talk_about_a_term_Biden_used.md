# Bits

Beau says:

- Beau revisits Biden's recent speech and the term "semi-fascism" used.
- A girlfriend introduced Beau's channel to her boyfriend during the Russia-Ukraine conflict.
- The boyfriend was curious about Biden using the term "semi-fascism."
- Beau explains that the term can be misunderstood due to people associating it with a specific subset from World War II Germany.
- He mentions that not all fascists fit that specific image.
- Beau references Lawrence Britt and Umberto Eco for discussing the characteristics of fascism.
- He suggests watching his videos on Trump's time in office to understand the term better.
- Beau believes that "semi-fascism" accurately describes Trumpism as an Americanized version of fascism.
- He points out that Biden's use of the term might bother some people due to his usual mellow rhetoric.
- Beau expresses the importance of calling things what they are to understand the situation better.

# Quotes

- "I don't know if it's a good idea politically, because it may be too much coming from Biden."
- "When it comes to stuff like that, I think it's important for people to understand what they're dealing with."
- "As far as the characteristics, the general philosophy, the undertones, the structure, goals. Yeah, I mean, it's an accurate term."
- "So anyway, it's just a thought, y'all have a good day."

# Oneliner

Beau questions the appropriateness of Biden using the term "semi-fascism," urging people to understand the situation for what it is.

# Audience

Political analysts

# On-the-ground actions from transcript

- Watch videos discussing the characteristics of fascism for better understanding (suggested)
- Engage in political discourse to understand terminology and positions better (implied)

# Whats missing in summary

Deeper insights into the implications of political rhetoric and terminology can be gained by watching the full video.

# Tags

#Biden #SemiFascism #Trumpism #PoliticalRhetoric #Understanding