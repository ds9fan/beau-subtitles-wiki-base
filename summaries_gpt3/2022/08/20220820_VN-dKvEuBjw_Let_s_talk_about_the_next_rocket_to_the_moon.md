# Bits

Beau says:

- NASA is preparing to go back to the moon as part of the Artemis program.
- A giant new rocket is set to launch on August 29th, with no crew onboard, just mannequins with sensors.
- The capsule will orbit the moon for a couple of weeks before returning to Earth.
- The whole mission is expected to take about a month and a half.
- Previous dress rehearsals for the rocket haven't gone smoothly, but hopefully, this launch will be successful.
- The ultimate goal is to have a crew on the moon by 2025 for extended periods.
- Despite being a significant event, it might be overlooked due to distractions on Earth.
- This mission marks a critical step towards humanity's goal of space exploration.
- NASA aims to eventually have people on the moon for extended periods.
- Getting humans off Earth is a monumental achievement, and this mission is a significant step in that direction.

# Quotes

- "This is one of the new steps towards getting us off this rock."
- "It's a new phase."
- "Getting off of this rock, that will be a crowning achievement."

# Oneliner

NASA plans to send a rocket to the moon in a month-long mission, marking a critical step towards extended human presence in space, amidst potential Earthly distractions.

# Audience

Space enthusiasts

# On-the-ground actions from transcript

- Stay updated on NASA's Artemis program progress (implied)
- Support and advocate for space exploration initiatives (implied)

# Whats missing in summary

Details on the significance of prolonged human presence on the moon

# Tags

#NASA #SpaceExploration #Artemis #MoonMission #FutureInSpace