# Bits

Beau says:

- A single mom is torn about getting Google Play cards for her daughter, who plays a game where she builds a city and runs a business.
- The mom worries that the game gives her daughter an unrealistic idea of running a business and may be taking her away from real life.
- Beau suggests getting the cards and also opening accounts on various platforms together to challenge her daughter.
- He mentions underestimating kids' abilities and the importance of providing challenges for them to shine.
- Beau encourages the mom to support her daughter's interest in running a business, as it could be a valuable learning experience.
- He points out that kids today have a good understanding of marketing and brand management due to social media influences.
- Beau recommends embracing the daughter's interest in business and letting her experience the learning curve early on.

# Quotes

- "Our kids are doing something and we just don't get it."
- "They just need the challenge. They need that passion for something."
- "You can be the facilitator of her dream rather than the mom who wants you to put down the phone."

# Oneliner

A single mom seeks advice on her daughter's gaming obsession with running a business, prompting Beau to suggest embracing the interest and providing a real-world challenge.

# Audience

Single parents

# On-the-ground actions from transcript

- Open accounts on platforms like Teespring, Redbubble, Spreadshirt, Fine Art America together to challenge and support the daughter's interest in running a business (implied).

# Whats missing in summary

Beau's compassionate and practical approach to supporting a child's interest in business and providing real-world challenges.

# Tags

#Parenting #Support #Challenge #Gaming #Business