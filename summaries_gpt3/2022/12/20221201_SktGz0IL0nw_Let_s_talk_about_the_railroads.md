# Bits

Beau says:

- Railway workers and bosses are negotiating a new contract, primarily focusing on scheduling issues rather than money.
- Workers are treated poorly, expected to work long hours without adequate time off.
- There was a tentative agreement that the unions voted down, leading to the possibility of a strike on December 9th.
- A strike could cause billions of dollars in economic damage to the US, prompting government intervention.
- The House passed a bill imposing the rejected labor deal and providing additional sick days, now awaiting Senate approval.
- Bernie Sanders will play a key role in pushing the legislation through the Senate.
- Beau criticizes the Republican Party for prioritizing political power over the well-being of the economy and people.
- The Democratic Party's trust in Republicans to act sensibly is questioned by Beau.
- Beau expresses disappointment in Biden siding with railway companies over workers, damaging his standing with organized labor.
- The moral aspect is emphasized, where workers' absence causes economic harm while CEOs' absence does not.

# Quotes

- "If your job is so vital that if you don't show up, the whole country stops, you probably deserve a decent quality of life."
- "The administration and Congress had the ability to stand with the workers, but they did the absolute bare minimum."
- "It truly damaged his standing there and the Democratic Party standing as a whole with organized labor."
- "Nothing. Nothing. It doesn't cause billions of dollars per day in economic damage to the United States. That happens if the workers don't show up."
- "Biden has a pretty strong labor record. And in this situation, he's kind of sided with the bosses."

# Oneliner

Railway workers face negotiations over scheduling, with potential economic repercussions, as government intervention favors industry over workers' quality of life.

# Audience

Congress Members

# On-the-ground actions from transcript

- Contact Congress members to advocate for fair treatment of railway workers (suggested)
- Stay informed about the ongoing negotiations and potential strike actions (suggested)

# What's missing in summary

The full transcript provides a detailed analysis of the railway workers' situation, the political dynamics involved, and the moral considerations that should guide decision-making.

# Tags

#RailwayWorkers #Negotiations #GovernmentIntervention #EconomicImpact #WorkerRights