# Bits

Beau says:

- Addresses the urgency of climate change and the need for immediate action.
- Explains that there is no one magic solution but rather a need for widespread systemic changes.
- Emphasizes that everything, from energy sources to agriculture practices, must change to combat climate change.
- Talks about the importance of celebrating small wins in transitioning to sustainable practices.
- Acknowledges that time is running out to mitigate the worst effects of climate change.
- Asserts that systems will change one way or another, voluntarily now or abruptly later due to crisis.
- Describes the inevitable transition away from fossil fuels, whether voluntarily or due to supply chain disruptions.
- Stresses the necessity for humanity to address climate change now to ensure a livable planet for future generations.

# Quotes

- "It's not that one thing has to change. It's that everything has to change."
- "Either we come together and treat this as the global emergency that it is, or we don't."
- "Humanity has to deal with climate change. There's not a choice here."
- "If we don't act on climate change, they won't be there."

# Oneliner

Beau addresses the urgency of climate change, stressing the need for widespread systemic changes to ensure a livable planet for future generations.

# Audience

Climate activists and concerned citizens.

# On-the-ground actions from transcript

- Start celebrating small wins in transitioning to sustainable practices (exemplified).
- Advocate for systemic changes in energy, waste management, and agriculture practices (suggested).
- Join or support organizations working towards climate action (implied).

# Whats missing in summary

Importance of taking immediate action to combat climate change and transition to sustainable practices.

# Tags

#ClimateChange #SystemicChange #Urgency #Transition #Activism