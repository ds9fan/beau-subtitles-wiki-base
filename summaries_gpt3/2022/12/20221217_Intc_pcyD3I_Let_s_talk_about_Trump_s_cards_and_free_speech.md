# Bits

Beau says:

- Trump's major announcement about hawking NFTs turned out ridiculous and did not change the story.
- Many believe Trump's reign in the GOP is coming to an end, despite claims that he was only pretending.
- Trump's speech about a free speech platform is viewed as a con and a lie, as he has a history of threatening journalists and dissent.
- Trump plans to sign an executive order barring federal employees from labeling anything as misinformation or disinformation, raising questions about his own use of such terms.
- People who mimic Trump's style are seen as manipulative, aiming to maintain relevance and deceive the public.
- Those in the Republican Party who follow Trump or mimic his style might eventually realize they have been deceived.
- Trump's attempts to monetize everything, from businesses to government positions, showcase his true character.
- Researching Trump's history reveals numerous instances where he sought to silence dissent, contradicting his image as a free speech advocate.

# Quotes

- "It's a con. It's a lie."
- "He is not a free speech advocate. He never has been."
- "He is the antithesis of free speech."

# Oneliner

Beau exposes Trump's facade of a free speech advocate, revealing his history of silencing dissent and manipulation in politics.

# Audience

Politically aware individuals

# On-the-ground actions from transcript

- Research Trump's history of silencing dissent (suggested)
- Stay informed on political manipulations and tactics (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's actions and rhetoric, shedding light on his deceptive tactics and lack of commitment to free speech.

# Tags

#Trump #NFTs #FreeSpeech #Manipulation #RepublicanParty