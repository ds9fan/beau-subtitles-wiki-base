# Bits

Beau says:

- Hutchinson revealed Meadows was burning documents in the White House fireplace a couple of times a week.
- The White House has burn bags for incinerating sensitive documents, but Meadows chose to burn them himself.
- Trump administration staff used to piece together torn documents and burn the rest to comply with federal law.
- Meadows likely burned documents that couldn't be seen by White House staffers going through classified information.
- The documents burned were probably so sensitive that even attempting to piece them together for retention was risky.
- Meadows burning documents multiple times a week indicates extreme secrecy and importance attached to them.
- Conspiracy theories were brought into the White House by external sources like the "space laser lady" and Peter Navarro.
- The fact that higher-ups needed to be briefed on conspiracy theories debunks the credibility of those theories.
- Trump White House possibly entertained conspiracy theories to energize their base, as suggested by the need for briefings.
- Briefings on conspiracy theories could serve as evidence to help individuals break free from harmful beliefs.

# Quotes

- "Those briefings might be the best evidence to help break people free from it and make them really start to question what they have come to believe."
- "Trump White House possibly entertained conspiracy theories to energize their base, as suggested by the need for briefings."

# Oneliner

Hutchinson revealed Meadows burning sensitive documents, while conspiracy theories were briefed in the White House, debunking their credibility and hinting at a manipulative agenda.

# Audience

Media consumers

# On-the-ground actions from transcript

- Contact media outlets to ensure coverage of the revelations from Hutchinson's testimony (suggested)
- Engage in critical discourse to debunk conspiracy theories and encourage questioning of beliefs (implied)

# Whats missing in summary

Insight into the potential impact of media coverage of Hutchinson's revelations, leading to increased public awareness and scrutiny.

# Tags

#Government #ConspiracyTheories #TrumpAdministration #Secrecy #MediaCoverage