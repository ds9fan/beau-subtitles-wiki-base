# Bits

Beau says:

- Addresses a message critiquing his analogy comparing dancing Ukrainian soldiers to drag shows by American soldiers during World War II.
- Explains the historical context of drag shows by USGIs during World War II and their connection to changing societal norms.
- Mentions the establishment and success of the Women's Army Corps (WACC) in 1942.
- Notes the significant contributions of women from WACC in advising men on makeup for performances.
- Emphasizes the success of WACC and how it exceeded recruitment goals, leading to its expansion.
- Describes the societal views on women during World War II as weaker, lesser, and unequal.
- Explains why women were prohibited from performing on stage during the USGIs' drag shows.
- Clarifies the different reasons behind the prohibition of women from participating in the shows by WACC and the War Department.
- Points out that the shows aimed to push for societal acceptance of women as equals within the military.
- Concludes by reflecting on the progressive changes within the U.S. military during World War II and their impact on society.

# Quotes

- "They didn't fight, they didn't complain, they just did their jobs."
- "It's hard for a lot of people to believe, but during World War II, the U.S. military, they were woke AF in a whole lot of ways."
- "What's so wrong with just accepting?"

# Oneliner

Beau explains how drag shows by USGIs during World War II were part of a larger effort to change societal norms, particularly in accepting women as equals in the military.

# Audience

History enthusiasts, advocates for societal change

# On-the-ground actions from transcript

- Research and share more about the historical contributions of women in the military during World War II (suggested).
- Support initiatives that aim to challenge societal norms and push for equality (implied).

# Whats missing in summary

The full transcript delves deeper into the historical context and societal attitudes towards gender roles during World War II.

# Tags

#History #SocietalChange #GenderEquality #USMilitary #WorldWarII