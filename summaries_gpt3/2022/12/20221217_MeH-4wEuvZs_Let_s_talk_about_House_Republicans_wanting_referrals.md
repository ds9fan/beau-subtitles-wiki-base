# Bits

Beau says:

- House Republicans plan to launch an investigation into the president's son, indicating an authoritarian nature embraced by the Republican Party.
- Republicans are compared to the Democratic Party's committee hearings into the January 6th events, a crime investigation, not an individual.
- The proposed Republican hearing focuses on Hunter Biden's laptop without an identified crime, seeking the person first and then the crime.
- Allegations against Hunter Biden in Ukraine fell apart when the timeline and narrative were examined.
- Despite ethical concerns, Hunter Biden's actions do not appear to be illegal.
- The Republican Party's approach seems to mirror Lavrentiy Beria's "show me the person, I'll show you the crime" tactic from Soviet state security, lacking evidence for alleged crimes.

# Quotes

- "They have the person and they're looking for a crime."
- "They don't have an identified crime that they're looking into."
- "Show me the crime and then you go find the person."
- "I think that's a little unethical, but it's not illegal."
- "It will just continue to tie the entire Republican Party to his failure of an administration."

# Oneliner

House Republicans plan to launch an investigation into Hunter Biden without an identified crime, mirroring an authoritarian approach and risking further Republican Party damage.

# Audience

Political observers

# On-the-ground actions from transcript

- Examine political investigations critically (suggested)
- Stay informed about political developments (suggested)

# Whats missing in summary

Exploration of the potential impacts on the Republican Party's reputation and future political standing. 

# Tags

#Politics #Investigation #RepublicanParty #HunterBiden #Authoritarianism