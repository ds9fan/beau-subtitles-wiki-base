# Bits

Beau says:

- Introduction about discussing the removal of a statue.
- The statue in question is of Roger Brooke Taney, overshadowed by the infamous Dred Scott decision he wrote.
- The Dred Scott decision held that black Americans couldn't be citizens and that black people had no rights white people were bound to respect.
- Taney viewed the founding documents as an agreement to keep a permanent underclass, not as a promise of equality.
- Despite objections to Taney's views then and now, his statue is being removed by vote.
- The statue will be replaced by Thurgood Marshall, the first black associate justice of the Supreme Court.
- Beau sees this as a symbolic act, acknowledging that the promises of the founding documents are still unfulfilled.
- Symbolic acts like statue removals move towards real and tangible actions to address past wrongs.
- Beau expresses the importance of not just removing statues for embarrassment's sake but taking concrete steps to right historical injustices.
- Beau concludes with a call to aim for real action beyond symbolic gestures.

# Quotes

- "Removing a statue because it is embarrassing, yeah, I get it. Righting the wrongs, that's a whole lot better."
- "The promises laid out in those founding documents are still not being lived up to."
- "But each symbolic act carries us closer to real action, tangible action, to right the many things that this country has done."

# Oneliner

Beau talks about the removal of a statue of Roger Brooke Taney, known for the infamous Dred Scott decision, and the importance of moving from symbolic gestures towards concrete actions to address historical injustices.

# Audience

History buffs, activists, voters

# On-the-ground actions from transcript

- Advocate for the removal of statues glorifying figures linked to systemic racism (exemplified)
- Support initiatives that address historical injustices and work towards real action (exemplified)

# Whats missing in summary

Beau's emotional tone and full depth of analysis on the symbolism of statue removals and the need for tangible actions beyond symbolic gestures. 

# Tags

#StatueRemoval #HistoricalInjustice #SymbolicActions #RealChange #SystemicRacism