# Bits

Beau says:

- Kevin McCarthy plans to use a roadblock to defense spending in the House of Representatives by withholding the NDAA until the vaccine mandate is removed.
- Active duty military shows high compliance with the vaccine mandate, with over 97% reported compliance.
- The real issue lies with the National Guard and Reserve members who may not be vaccinated, affecting readiness during emergencies.
- Refusing the vaccine doesn't necessarily mean active duty members are against it; some may just be out of date.
- The Republican Party's move to block the NDAA sets a dangerous trend for readiness in future emergencies.
- McCarthy's actions are seen as weakening the U.S. military's readiness and serving the Republican Party's agenda.
- There's concern that those refusing vaccines might have extremist beliefs, leading to further complications within the military.
- Despite disagreements within the military, top brass remains firm on the importance of the vaccine mandate.
- The Republican Party's strategy to obstruct the NDAA marks their initial step towards achieving their goals.
- This move by the Republican Party is viewed as prioritizing their agenda over national security.

# Quotes

- "The Republican agenda is to weaken the U.S."
- "McCarthy's actions are setting a trend for a readiness issue next time."
- "The real issue lies with the National Guard and Reserve members who may not be vaccinated."
- "The Republican Party's move to block the NDAA sets a dangerous trend."
- "Given the Venn diagram of those refusing the vaccine and those with extremist beliefs, I'd want them gone anyway."

# Oneliner

Kevin McCarthy plans to block defense spending in the House of Representatives until the vaccine mandate is removed, risking national security and weakening military readiness for future emergencies, despite high compliance rates among active duty members.

# Audience

Military personnel, policymakers

# On-the-ground actions from transcript

- Contact policymakers to express support for maintaining the vaccine mandate to ensure military readiness (suggested)
- Join or support organizations advocating for the importance of vaccination within the military (implied)

# Whats missing in summary

Detailed insights on the potential consequences of compromising military readiness and national security for political agendas.

# Tags

#KevinMcCarthy #NDAA #VaccineMandate #MilitaryReadiness #RepublicanParty