# Bits

Beau says:

- Explains the importance of symbolism in the context of criminal referrals against the former president.
- Emphasizes that symbolism is significant because it conveys messages beyond the normal activist community.
- Describes the process of different branches of government signing off on determinations related to the case.
- Illustrates how the system functions with the legislative, judicial, and executive branches making determinations.
- Acknowledges that while symbolic, the referrals are not irrelevant and play a vital role in showcasing a unified decision by all branches of government.
- Advocates for understanding the symbolic importance and not letting commentators downplay its significance.
- Stresses that symbolism carries messages to the public and is a key aspect of activism and government operations.

# Quotes

- "Symbolism matters."
- "It's showing that the system as a whole, the government as a whole, representatives of all three branches have made the same determination."
- "The symbolic matters a lot."

# Oneliner

Beau explains the importance of symbolism in government actions and how it carries messages to the public.

# Audience

Government observers

# On-the-ground actions from transcript

<!-- Skip this section if there aren't physical actions described or suggested. -->

# Whats missing in summary

The full transcript provides additional details on the significance of symbolic acts in government and activism.