# Bits

Beau says:

- Artemis 2 will fly astronauts around the moon in 2024, with Artemis 3 following to put people back on the moon.
- NASA's Artemis program aims to establish a research base near the southern pole of the moon and a lunar space station for refueling missions.
- The delays in the Artemis missions are primarily due to budget constraints rather than technical upgrades.
- The successful completion of Artemis 1's mission involved deep space maneuvers and system tests on the Orion spacecraft.
- The spacecraft completed a 240,000-mile journey and was retrieved by the Navy on December 11.
- Following splashdown, the craft is being transported back to NASA for thorough examination.
- The current timeline for the Artemis missions includes establishing a base on the moon within the next few years.
- Financial constraints have caused significant delays in the Artemis program, impacting the timing of future missions.
- Despite budget challenges, NASA is continuing its efforts to advance towards establishing a base on the moon.
- The success of Artemis 1 marks a significant step towards the United States' goal of lunar exploration.

# Quotes

- "Artemis 2 will fly astronauts around the moon sometime in 2024."
- "It had to do with trying to fill a budget hole a few years ago. This is literally all about money."
- "The delays in the Artemis missions are primarily due to budget constraints rather than technical upgrades."
- "The successful completion of Artemis 1's mission involved deep space maneuvers and system tests on the Orion spacecraft."
- "Despite budget challenges, NASA is continuing its efforts to advance towards establishing a base on the moon."

# Oneliner

Artemis program faces budget delays in lunar missions, aiming to establish a base on the moon within the next few years.

# Audience

Space enthusiasts

# On-the-ground actions from transcript

- Support funding for NASA's Artemis program to ensure timely progress (implied).

# Whats missing in summary

Details on specific deep space maneuvers and system tests conducted during Artemis 1 mission.