# Bits

Beau says:

- Electrical substations in Moore County, North Carolina were taken offline abruptly, leaving tens of thousands without electricity.
- The sheriff's department doesn't have suspects yet, but the FBI has joined the investigation due to resource limitations.
- Speculation on social media about the incident is compelling but not evidence; investigators are needed to verify.
- The incident may be considered domestic terrorism depending on the intent of those responsible.
- Moore County has a significant population over 65, and the power outage occurred in freezing temperatures.
- North Carolina has felony murder charges, and the outage's impact on lives could be viewed as causal.
- Those responsible likely didn't anticipate the potentially fatal consequences of cutting power to older residents.
- If any deaths occur due to the outage, individuals involved may quickly cooperate with law enforcement.
- Official information and details on the investigation are limited at this point.
- The FBI's involvement suggests the coordinated incident is more complex than simple vandalism.

# Quotes

- "It may not matter. It may not matter."
- "If any deaths occur due to the outage, individuals involved may quickly cooperate with law enforcement."
- "There are a lot of developments here. There are a lot of things that can change all of the math in how this plays out."

# Oneliner

Electrical substations offline in Moore County, potential domestic terrorism, FBI joins investigation, and lives at risk due to power outage in freezing temperatures.

# Audience

Community members, investigators.

# On-the-ground actions from transcript

- Reach out to local authorities with any information that might assist the investigation (implied).
- Ensure the well-being of vulnerable community members during power outages (implied).

# Whats missing in summary

Details on specific actions individuals can take to support older residents in Moore County during power outages.

# Tags

#MooreCounty #NorthCarolina #PowerOutage #FBI #CommunitySafety