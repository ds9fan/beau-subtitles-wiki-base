# Bits

Beau says:

- Pan Am flight 103 incident, an airliner exploded over Lockerbie, Scotland in 1988, killing 259 in the air and 11 on the ground.
- One of the key figures sought after in the Pan Am flight 103 case, Massoud, is now in US custody.
- Speculation surrounds how Massoud ended up in US custody, with rumors suggesting Libyan involvement.
- Libya, with competing administrations in Tobruk and Tripoli, may have assisted in extracting Massoud due to international pressure and financial struggles.
- The National Oil Corporation in Libya recently sought foreign investors, hinting at economic desperation.
- The motive behind Libya's possible involvement in handing over Massoud to the US could be to attract European investment.
- Despite rumors implicating the Tripoli administration, the exact details of how Massoud was transferred to the US remain undisclosed.
- The US government remains vague about the circumstances of Massoud's custody, fueling speculation.
- Massoud's trial in the United States is anticipated to reignite interest in the Pan Am flight 103 incident.
- The case is expected to resurface in headlines, bringing awareness to those unfamiliar with the event.

# Quotes

- "For a lot of younger people, the only comparison is what happened in September."
- "Is there a reason for the government there to suddenly want to help the US. Yeah, there actually is."
- "Everything's going to be brought back up again."

# Oneliner

Massoud from Pan Am flight 103 case now in US custody, sparking speculation on Libyan involvement and potential motives amid economic struggles.

# Audience

International observers

# On-the-ground actions from transcript

- Monitor updates on the Pan Am flight 103 case to stay informed and aware of developments (implied).

# Whats missing in summary

Detailed information on the Pan Am flight 103 incident and the potential implications of Massoud's trial in the United States.

# Tags

#PanAmFlight103 #Lockerbie #Libya #USCustody #Speculation