# Bits

Beau says:

- Videos circulating show hard-right Trump supporters expressing leftist economic ideas like getting rid of banks and questioning the need for money.
- Rural Americans live by leftist economic principles in daily life through co-ops, unions, farm aid, and resource redistribution.
- Despite appearing leftist economically, the MAGA crowd is socially conservative, regressive, and sometimes racist.
- Their desire to eliminate banks and landlords stems from seeking a reset of the current system to benefit themselves.
- Their economic beliefs are often rooted in conspiracy theories and blaming specific groups.
- They are not seeking equality but rather hoping for a shift in power dynamics.
- Winning cultural and moral battles is key to reaching these individuals, as appealing to economic theories may reinforce negative stereotypes.
- The goal is to lead them to seek real freedom for everyone and reject the concept of oppressive power structures.
- Until they become socially progressive, they are not allies in the fight against inequality and racism.
- To create a society based on economic equality, it's vital to address cultural and moral aspects rather than solely focusing on economic theories.

# Quotes

- "Rural people live our lives by leftist economic principles every day."
- "They're not looking for equality."
- "We have to win the cultural battles."
- "They're not allies."
- "Playing into the economic stuff isn't going to help."

# Oneliner

Hard-right Trump supporters express leftist economic ideas but lack social progressiveness, not allies in the fight against inequality.

# Audience

Progressive activists

# On-the-ground actions from transcript

- Challenge conspiracy theories and negative stereotypes through community education and engagement (implied).
- Engage in cultural and moral dialogues to foster social progressiveness among those with differing economic beliefs (implied).

# Whats missing in summary

The nuances of navigating ideological differences for social progressiveness.

# Tags

#PoliticalBeliefs #EconomicIdeas #SocialProgressiveness #Inequality #ConspiracyTheories