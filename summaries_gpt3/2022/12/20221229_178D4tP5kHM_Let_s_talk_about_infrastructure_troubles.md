# Bits

Beau says:

- Infrastructure attacks in the US are increasing, but conventional wisdom suggests they won't succeed.
- Right-wing groups are suspected, but the true intent is still unclear.
- The US isn't under occupation, making typical attack objectives unlikely to succeed.
- Attacks aim to provoke a security clampdown, create disillusionment with the ruling party, or cause a reset.
- The US's robust control systems and lack of popular support make these objectives unachievable.
- Despite potential chaos and disruption, the attacks are unlikely to achieve their goals.
- Individuals should prepare for potential infrastructure disruptions in their areas.
- Suggestions include getting a generator, water purification tools, battery backups, and power inverters.
- Law enforcement's response will determine the duration of these attacks.
- Despite potential chaos, the attacks are not likely to succeed in their objectives.

# Quotes

- "It won't work."
- "The conditions necessary for this to work don't exist in the United States."
- "The chaos, the inconvenience, all of that will be there. But the goals, the conditions are not set for that."

# Oneliner

Infrastructure attacks in the US are on the rise, but the unique conditions make success unlikely; prepare for potential disruptions, but know that the chaos won't achieve its goals.

# Audience

Community members

# On-the-ground actions from transcript

- Stock up on essentials like water purification tools and battery backups (suggested)
- Get a generator or a power inverter for your car to prepare for potential infrastructure disruptions (suggested)
- Be prepared for chaos but understand that the attacks are unlikely to succeed in their goals (exemplified)

# Whats missing in summary

The full transcript provides a detailed explanation of why typical infrastructure attack objectives are unlikely to succeed in the United States.

# Tags

#Infrastructure #US #Security #Preparedness #CommunitySafety