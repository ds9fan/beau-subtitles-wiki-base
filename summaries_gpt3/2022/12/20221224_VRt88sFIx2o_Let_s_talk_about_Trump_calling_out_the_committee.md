# Bits

Beau says:

- Critiques Trump's response to the committee calling him out, particularly focusing on Trump labeling the committee members as Marxists.
- Beau breaks down the definition of Marxism as a belief in a stateless, classless, moneyless society where workers control the means of production.
- Questions whether members of Congress, who are often wealthy and receive campaign contributions from big businesses, truly advocate for the working class as Marxists do.
- Suggests that Trump and others who use the term "Marxist" as a scare tactic are fully aware of its meaning but exploit the ignorance of their base for manipulation.
- Points out how certain media outlets profit from spreading misinformation and manipulating their audience's lack of knowledge.
- Emphasizes the manipulation and trickery employed by certain political figures and media to control their supporters and advance their agendas.
- Calls out the Republican Party for deceiving its supporters about various issues, including the election, and warns about ongoing manipulation tactics.
- Urges people to recognize when they have been deceived and to be critical of the information they receive from political figures and media.

# Quotes

- "They stand there at those rallies. They give those speeches. They throw out that word. They get everybody angry. Then they walk behind that curtain and they laugh because they know what it means."
- "They know that the American education system isn't great. They know their base doesn't know what Marxism is. Because quote, I love the uneducated because they're easy to manipulate."
- "They played you about the election. They lied to you about that. And now they've moved on. A new term. They're lying to you again."
- "One of the hardest things in the world is to admit that you have been tricked."
- "They're tricking you."

# Oneliner

Beau breaks down the manipulation behind calling committee members Marxists, exposing the exploitation of ignorance for political gain and profit.

# Audience

Voters, Media Consumers

# On-the-ground actions from transcript

- Educate yourself on political terminologies and ideologies to be less susceptible to manipulation (implied).
- Stay informed from diverse and credible sources to combat misinformation (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how political figures use scare tactics and misinformation to manipulate their supporters, urging individuals to be vigilant and informed in the face of such tactics.

# Tags

#Manipulation #PoliticalDeception #Misinformation #Education #Awareness