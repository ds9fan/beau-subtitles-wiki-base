# Bits

Beau says:

- Explains influence operations on social media as marketing, advertising, and not just spy stuff.
- Describes how amplifying existing movements on social media can disrupt the status quo in a target country.
- Points out that stability is the ultimate goal in foreign policy.
- Mentions the role of amplifying opposition to create disunity and weaken a country's ability to respond internationally.
- Uses the example of the Black Lives Matter movement to illustrate how influence operations can work.
- Talks about how the US conducts influence operations in other countries and how the Kurdish people's decentralized movement hinders US intervention.
- Suggests that addressing inequality is the key to preventing exploitation of marginalized groups.
- Emphasizes that racial justice in the US is a matter of national security.
- Raises concerns about platforms like TikTok being used for influence operations due to their ability to control algorithms and spread messages.
- Advocates for addressing underlying issues rather than just trying to stop the exploitation.

# Quotes

- "It's marketing, it's advertising, nothing more."
- "Yes, racial justice in the United States is a matter of national security."
- "The easy fix here is to fix the inequality."
- "To me, the solution is fix the problem, fix the thing that can be exploited."
- "When people talk about influence operations, think of it in terms of Coca-Cola and Pepsi, not in terms of cameras and trench coats."

# Oneliner

Beau explains influence operations as marketing on social media, showing how amplifying existing movements can disrupt stability in a target country, and advocates for addressing inequality as a solution to prevent exploitation.

# Audience

Internet users

# On-the-ground actions from transcript

- Address inequality to prevent exploitation of marginalized groups (suggested)
- Advocate for racial justice as a matter of national security (suggested)

# Whats missing in summary

The full transcript provides a detailed explanation of influence operations on social media, using real-world examples to illustrate the impact of amplifying movements and advocating for addressing inequality as a preventive measure against exploitation.

# Tags

#InfluenceOperations #SocialMedia #ForeignPolicy #RacialJustice #Inequality