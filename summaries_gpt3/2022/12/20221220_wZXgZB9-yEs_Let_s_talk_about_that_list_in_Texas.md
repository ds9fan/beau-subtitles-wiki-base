# Bits

Beau says:

- Reports suggested the Texas attorney general's office wanted a list of people who changed their gender on their driver's license.
- More than 16,000 entries were involved, but the information didn't change hands due to the exhausting manual process.
- Beau usually investigates stories but couldn't find any context or information on this matter.
- Major news outlets like Washington Post, NBC, and New York Times haven't been able to get answers either.
- The Attorney General's office has remained silent on the matter, even after stories were published.
- Beau expresses concern about authorities making lists of marginalized groups.
- Due to the holidays, the story is unlikely to progress until after New Year's unless someone has inside information.
- Beau urges for transparency from the Attorney General's office regarding the motive behind seeking this list.
- He stresses that this story shouldn't be forgotten and requires further investigation.
- Beau hopes that reputable news outlets will continue to pursue this story despite the holiday season.

# Quotes

- "Reports suggested the Texas attorney general's office wanted a list of people who changed their gender on their driver's license."
- "The Attorney General's office certainly appears to have requested a list of trans people in the state with no explanation as to why."
- "When a group of authoritarians starts attempting to make lists of people that they have actively othered, it's not a good thing."
- "This isn't a story that should be forgot, though."
- "No, this doesn't need to be forgotten."

# Oneliner

Beau raises concerns about the Texas attorney general's office seeking a list of people who changed their gender on driver's licenses, stressing the need for transparency and continued investigation despite the holiday season.

# Audience

News Outlets, Activists

# On-the-ground actions from transcript

- Investigate the situation further to uncover the motives behind the request for the list (implied).
- Stay informed and keep the story alive by sharing information on social media and with relevant organizations (implied).

# Whats missing in summary

The full transcript provides more context on the concerning request for a list of trans people in Texas and the lack of transparency from the Attorney General's office.

# Tags

#Texas #AttorneyGeneral #TransgenderRights #Investigation #Transparency #NewsCoverage