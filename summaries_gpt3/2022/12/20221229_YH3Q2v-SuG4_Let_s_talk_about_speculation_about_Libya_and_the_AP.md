# Bits

Beau says:

- Released a video about events in Libya involving speculation about the government in Tripoli capturing someone and turning them over to the United States for oil investment.
- Received criticism for the speculation in the video.
- Associated Press confirmed that the government in Tripoli did snatch the individual and turn them over to the United States to curry favor, although the motive related to oil investment is not confirmed.
- People are upset about the situation due to concerns about the legality of the extradition, given Libya's two competing governments.
- US intelligence appears indifferent to the questionable nature of the extradition as long as they got the person they wanted.
- The individual was snatched by a group akin to a neighborhood watch before being in US custody.
- Analysts agree that the extradition was to curry favor with Western governments.
- Anticipates the story gaining significant traction in the headlines, especially once a trial commences.

# Quotes

- "US intelligence doesn't care how the sausage was made."
- "He was snatched quite some time before he was in US custody."
- "This will be a huge thing, especially once a trial starts."

# Oneliner

Beau clarifies speculation about Libya events, confirms government involvement, and anticipates a significant story gaining headlines.

# Audience

Journalists, Activists, World Citizens

# On-the-ground actions from transcript

- Contact local news outlets to raise awareness about the situation (suggested).
- Join organizations advocating for transparency in international dealings (implied).

# Whats missing in summary

Insight into the potential implications of this story on international relations and human rights.

# Tags

#Libya #Government #USIntelligence #Extradition #InternationalRelations