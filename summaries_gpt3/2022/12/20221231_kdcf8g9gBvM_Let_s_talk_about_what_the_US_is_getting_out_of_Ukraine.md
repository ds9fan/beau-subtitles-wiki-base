# Bits

Beau says:

- Addressing the US benefits from supporting Ukraine and the questions arising from right-wing talking points about the money spent.
- Nations prioritize interests over friendships, morality, or ideology.
- The economic power Ukraine could become after the war and its alignment with Europe benefiting the US and NATO.
- Investing in Ukraine to prevent it from becoming a Russian colony and helping keep Russia in check.
- The US benefits from buying a degraded Russian military, simplifying its international poker game with China now as the main competitor.
- The $50 billion investment in Ukraine is effective in knocking Russia out of the high stakes poker table.
- This support helps rebuild the US's reputation on the international scene and frame the contest as democracy versus authoritarianism.
- Ukrainians want to remain Ukrainian, not become Russian, which is often overlooked in foreign policy questions.
- Despite the strategic benefits, it's vital to always bring back the human aspect of conflicts and the impact on people.
- Overall, supporting Ukraine comes with multiple strategic and geopolitical advantages for the US.

# Quotes

- "Nations don't have friends. They don't have morality. They don't have ideology. They have interests."
- "The last time the United States engaged in a Cold War with Russia and engaged in the military expenditures that go along with this, it was $13 trillion."
- "When somebody is asking this question, 'what are we getting out of it?' I understand it from a two sizes, two small heart."
- "In real life, people do matter."
- "All of this money is spent on weapons because there are people who are having rockets rained down on their heads."

# Oneliner

Exploring the strategic benefits for the US in supporting Ukraine while acknowledging the human cost.

# Audience

Foreign policy enthusiasts

# On-the-ground actions from transcript

- Support organizations aiding Ukrainian civilians (suggested)
- Advocate for diplomatic solutions to conflicts (implied)

# Whats missing in summary

The emotional toll and human suffering amidst geopolitical strategies.

# Tags

#US #Ukraine #Geopolitics #ForeignPolicy #HumanitarianAid