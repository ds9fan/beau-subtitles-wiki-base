# Bits

Beau says:

- Addresses information consumption and recognizing cherry-picked information shaping narratives.
- Believes biases can lead to bigotry when assigning crimes to specific demographics.
- Critiques a message suggesting discussing looting, carjackings, and gun crimes in democratic-controlled cities.
- Breaks down the flaws in cherry-picked crime statistics and biases related to demographics.
- Compares smash and grabs to construction site theft, pointing out selective reporting.
- Analyzes the demographics behind catalytic converter thefts and the areas with the highest increases.
- Debunks the idea of gun crimes being solely a problem in Democrat-run cities.
- Argues that gun crime is more prevalent in red states, contrary to certain narratives.
- Attributes the root cause of high gun crime rates to cultural factors and misinformation.
- Encourages punching up against systemic issues rather than kicking down at marginalized groups.

# Quotes

- "Stop kicking down. Punch up."
- "You really think it's not white people that are out there ripping off catalytic converters? I have a hard time believing that."
- "It's a red state problem."

# Oneliner

Beau breaks down biases, cherry-picked crime stats, and misconceptions about gun crimes, urging to punch up against systemic issues instead of kicking down at marginalized communities.

# Audience

Information Consumers

# On-the-ground actions from transcript

- Challenge biases and stereotypes in your community (implied).
- Counter misinformation and cherry-picked data with facts and logic (implied).

# Whats missing in summary

In-depth analysis and examples of how biases and cherry-picked information can lead to harmful narratives and stereotypes.

# Tags

#InformationConsumption #BiasRecognition #CherryPickedStats #SystemicIssues #CommunityAction