# Bits

Beau says:

- Introduces the topic of a well-known pirate and directs viewers to a young man's YouTube channel aiming for a silver play button.
- The young man's channel reached and surpassed its goal of 100,000 subscribers with the help of various channels, including Beau's.
- The channel was later taken down by YouTube due to the age of the person associated with the email on the account.
- After YouTube realized the young man's mom was actively involved, they reinstated the channel.
- There is an Amazon wish list for the young man to create unboxing videos, which also includes items for the hospital.
- Beau notes that the Amazon wishlist was taken down before filming but mentions a link to donate to Rainbow's Hospice.
- Despite some issues with the Amazon account, the channel is back online, and there is a new goal mentioned.
- Beau sends a message of encouragement to Captain Corey, assuring him he will recover from the setback and mentions the negativity that comes with being a YouTuber.
- The young man is likely to face unkind comments but is reminded that people are aware of his channel.
- Beau wraps up with a heartwarming message for the holiday season and signs off.

# Quotes

- "all pirates end up in jail."
- "You'll be able to recover from this. You will do fine."
- "There are a lot of unkind comments that will come your way."

# Oneliner

Beau updates on a young YouTuber's journey to a silver play button, facing setbacks and support along the way.

# Audience

YouTube viewers

# On-the-ground actions from transcript

- Support Captain Corey's YouTube channel by subscribing and engaging positively (exemplified).
- Donate to Rainbow's Hospice (exemplified).

# Whats missing in summary

The full transcript provides a detailed account of the challenges faced by a young YouTuber in reaching his subscriber goal, the temporary takedown of his channel, and the subsequent reinstatement, along with heartwarming community support. 

# Tags

#YouTube #Support #Community #Encouragement #Donation