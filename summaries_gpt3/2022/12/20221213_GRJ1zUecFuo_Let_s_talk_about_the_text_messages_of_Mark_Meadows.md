# Bits

Beau says:

- Mark Meadows' text messages have been made public, revealing his contact with over 30 members of Congress before the events of January 6th.
- Meadows and others were discussing ways to keep Trump in office, disregarding the voice of the people and subverting democracy.
- Representative Ralph Norman's message urging the invocation of martial law to save the Republic has gained attention, despite a misspelling.
- The message was sent on January 17th, three days before Biden's inauguration, indicating ongoing attempts to overturn the election results.
- Advocating for martial law to save the Republic goes against democratic principles and actually subverts them.
- Meadows was being urged by a member of Congress to convince Trump to declare martial law after the events of January 6th.
- Despite court rulings and the truth coming out, some individuals were still willing to use military force to suppress the will of the people.
- The actions and intentions of those pushing for extreme measures after the election raise serious questions about their fitness for office.

# Quotes

- "We are at a point of no return in saving our Republic. Our last hope is invoking Marshall, misspelled, law."
- "Creating a dictatorship declaring martial law does not save a republic, it subverts it."
- "Even after everything that happened on the 6th, there were still at least a few who were entertaining the idea of using the United States military to suppress the people."

# Oneliner

Mark Meadows and members of Congress discussed extreme measures to keep Trump in office, including advocating for martial law, despite court rulings against their claims.

# Audience

Concerned citizens, democracy advocates.

# On-the-ground actions from transcript

- Contact your representatives to ensure they uphold democratic principles (suggested).
- Stay informed about political developments and hold elected officials accountable (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of the alarming messages exchanged by Mark Meadows and members of Congress, shedding light on attempts to subvert democracy and overturn election results.

# Tags

#MarkMeadows #TextMessages #January6th #ElectionSubversion #Democracy