# Bits

Beau says:

- People still seek heroes to look up to, desiring to measure themselves against someone who has lived the life they aspire to.
- Musk suspended the Jet Tracker account despite declaring he was leaving it up on his new Twitter account, raising questions about heroism in today's world.
- History's great personalities, like Gandhi and Mandela, are not without flaws, illustrating the impossibility of finding a perfect hero.
- Beau admires Teddy Roosevelt but acknowledges his flaws, underscoring that everyone, including heroes, is flawed.
- Heroic people do not exist, only heroic actions, as heroism can often mask underlying frustrations.
- It's possible to admire individuals for specific characteristics, but expecting perfection leads to disappointment.
- The pursuit of an idolized, perfect figure to always look up to is futile; the only person to measure yourself against is your past self.
- Continuous self-improvement should be the focus, rather than searching for a flawless hero to guide your life.

# Quotes

- "Nobody is perfect like that."
- "Everybody is flawed."
- "The only person that you should measure yourself against is you."
- "Your childhood hero should kind of be irrelevant at this point."
- "If you have lived five, ten, 15 years and you haven't changed your beliefs your opinions you've wasted your life."

# Oneliner

In today's flawed world, seeking heroes is natural, but the true measure of growth lies in self-improvement, not idolizing perfection.

# Audience

Aspiring individuals seeking guidance.

# On-the-ground actions from transcript

- Measure your growth by comparing yourself to your past self (implied).

# Whats missing in summary

The full transcript delves into the inherent flaws of historical figures and the futility of searching for perfection in heroes, guiding individuals towards focusing on personal growth and self-improvement.