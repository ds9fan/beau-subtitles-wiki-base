# Bits

Beau says:

- Beau addresses the difference between rewriting US history taught in schools, specifically about race relations and slavery, and removing Confederate statues.
- He points out that statues are not historically relevant; historians do not trust the inscriptions on them but use them to understand the values and messages conveyed when they were made.
- The statues honoring Confederate figures were mainly erected during the 1920s and 1960s to convey a message of subjugation and oppression, not historical accuracy.
- Beau explains that the Confederates themselves emphasized slavery as a central issue, not states' rights or other myths associated with the Civil War.
- He gives examples of historically inaccurate statues, like one commemorating General Lee's actions at Antietam when Lee was actually injured and not on horseback.
- Beau stresses that the message conveyed by Civil War statues is one of continued oppression, not historical accuracy or heroism.
- He contrasts the mythological elements associated with statues like the Statue of Liberty, which symbolize aspirational ideals rather than oppressive histories.

# Quotes

- "Statues aren't history. They're rocks meant to mythologize something."
- "They're not part of history in the way that people look at them. They weren't made by people right after the Civil War. They're not history, they're mythology."
- "The Confederates themselves said that. Anything else is just a lie."

# Oneliner

Beau explains the inaccurate historical representation of Confederate statues and their role in mythologizing oppression, contrasting them with aspirational symbols like the Statue of Liberty.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Educate others on the historical inaccuracies and mythological nature of Confederate statues (implied).
- Support initiatives to remove or relocate Confederate statues to more appropriate spaces like museums (implied).

# Whats missing in summary

Beau's passionate delivery and detailed explanations on the mythological aspects of statues and the importance of understanding their historical context could be best experienced by watching the full video.

# Tags

#History #ConfederateStatues #CivilWar #Oppression #Mythology #HistoricalAccuracy