# Bits

Beau says:

- Addresses comments made by a sitting member of Congress, referring to them as the "space laser lady" and discussing her statement about organizing to win with Steve Bannon on January 6th.
- Questions the appropriateness of a Congress member joking about armed resistance to the US government.
- Criticizes the notion of winning through armed entry and capturing symbolic buildings like Walmart, pointing out the impracticality of such actions.
- Raises the question of who "we" refers to in the context of the Capitol storming, suggesting it includes the Congress member herself.
- Examines the persistence of false claims and desires for victory despite debunked narratives and court losses, indicating a disregard for democracy and the Constitution.

# Quotes

- "That is not how you win."
- "Who is we?"
- "They're telling you who they are."
- "Still suggesting that we would have won indicates that she still wished they had."
- "Whoever we is would have done that."

# Oneliner

Beau questions the seriousness of a Congress member's comments and examines the implications of armed resistance fantasies on January 6th, prompting reflection on the true intentions behind such statements.

# Audience

Congressional accountability advocates

# On-the-ground actions from transcript

- Contact elected representatives to express concerns about inappropriate statements made by officials (implied)
- Stand against narratives that undermine democracy and the Constitution (implied)

# Whats missing in summary

Deeper analysis and context on the political climate surrounding false claims and desires for power post-January 6th.

# Tags

#Congress #PoliticalCommentary #Accountability #Democracy #Constitution