# Bits

Beau says:

- Enters a gas station late and encounters a man wearing a red hat, triggering a political debate.
- Engages in a 10-15 minute debate about Trump running the country like a business.
- Questions the notion of running the country like a business, challenges the popular slogan.
- Points out the flaws in the concept of treating the government like a business based on profit.
- Suggests that running the country like a business may not be beneficial for the working class.
- Encourages questioning the idea whenever it is brought up to reveal its flaws.

# Quotes

- "Most of you traded your country for a red hat."
- "Why do you like him? Because he ran the country like a business."
- "Running the country like a business is bad for the working class."

# Oneliner

Beau challenges the notion of running the country like a business and debates its implications, revealing its flaws and negative impact on the working class.

# Audience

American citizens

# On-the-ground actions from transcript

- Question the concept of running the country like a business when discussing politics (exemplified).
- Encourage critical thinking and questioning of political slogans in everyday interactions (exemplified).

# Whats missing in summary

The full transcript provides a detailed analysis of the flawed concept of running a country like a business and its negative consequences for society.

# Tags

#Politics #Trump #Government #Business #WorkingClass