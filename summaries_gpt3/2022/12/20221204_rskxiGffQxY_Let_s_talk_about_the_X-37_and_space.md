# Bits

Beau says:

- Talks about the X-37, a secretive space plane that recently returned to Earth after 908 days in orbit.
- Describes the X-37 as a small, baby space shuttle around 30 feet long.
- Mentions that there are two X-37 aircraft owned by the US government, with a combined time in space of over 10 years.
- Explains that the X-37 conducts experiments in space, including testing the effects of space on seeds and experimenting with electromagnetic propulsion.
- Notes the secrecy surrounding the X-37, with public-facing experiments and undisclosed projects.
- Traces the history of the X-37 from a joint NASA DARPA project to a Department of Defense project.
- Compares the US's X-37 project to a similar Chinese craft, indicating the US's decade-long lead in this technology.

# Quotes

- "908 days in orbit. It's a pretty big achievement."
- "There are two, we think, of these aircraft."
- "It's tiny."

# Oneliner

Beau delves into the secretive world of the X-37 space plane, detailing its lengthy time in orbit, experimental purposes, and the mystery shrouding its undisclosed projects.

# Audience

Space enthusiasts, researchers, military analysts.

# On-the-ground actions from transcript

- Research more about the X-37 and its experiments (suggested).
- Stay informed about advancements in space technology (implied).

# Whats missing in summary

The full transcript provides additional context on the history and significance of the X-37 project, offering a deeper understanding of its secretive nature and potential implications for space research and exploration.

# Tags

#Space #X-37 #SpaceTechnology #Secrecy #Research #Experiments