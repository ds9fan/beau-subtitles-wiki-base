# Bits

Beau says:

- Talks about a song from the 90s that spent 22 weeks on the country music charts, not really a country song, but a Christian song.
- Mentions people being dropped off in DC in freezing temperatures on Christmas by a governor for a political point.
- Acknowledges the justified anger towards the governor's actions.
- Praises the Migrant Solidarity Mutual Aid Network for providing shelter, food, clothes, and toys to those dropped off, showcasing the power of community networks.
- Points out the irony that leftists teamed up with religious institutions to help those in need, contrasting with the governor's actions.
- Criticizes the governor for using people as props and not following the values of his supposed Christian beliefs.
- Expresses concerns about the conservative Christian movement being consumed by hate and politicization.
- Raises the question of what if Jesus comes back as one of the city folk mentioned in the Christian song.
- Concludes by urging those who support the governor's actions to "need Jesus" and states there is no defense for the governor's actions.

# Quotes

- "Y'all need Jesus."
- "There is absolutely no way to defend this."

# Oneliner

Beau talks about a 90s Christian song, contrasts it with a governor's inhumane actions, praises community aid efforts, and criticizes politicization of conservative Christianity.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Support and volunteer with organizations like the Migrant Solidarity Mutual Aid Network (exemplified)

# Whats missing in summary

The emotional impact of the contrast between community aid efforts and the governor's actions, as well as the call for reflection on religious and moral values.

# Tags

#CommunityAid #ConservativeChristianity #PoliticalInjustice #MigrantSolidarity #CommunityNetworks