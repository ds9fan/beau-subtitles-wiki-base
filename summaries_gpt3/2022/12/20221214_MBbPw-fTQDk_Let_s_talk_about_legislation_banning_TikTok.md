# Bits

Beau says:

- Legislation introduced aims to ban TikTok in the United States and any network substantially controlled by an opposition nation like China, North Korea, Russia, Iran.
- Concern is that an opposition nation could use platforms like TikTok for coordinated influence operations targeting the US population.
- The focus is on the Chinese government potentially controlling what appears in people's feeds to influence large segments of the US population.
- Espionage concerns are secondary; TikTok is already banned in defense agencies for such reasons.
- Likelihood of legislation passing in the current Congress is low due to limited time, but it may pass when the new Congress convenes.
- Republicans are likely to support the legislation, viewing it as a way to combat the influence of China, especially on younger generations.
- If the legislation passes the House and Senate, it's expected that President Biden will sign it.
- If TikTok is impacted, it may be spun off as a public company based in the US or be replaced by a similar network.
- Ultimately, the focus is more on maintaining power and control rather than protecting individuals.

# Quotes

- "It's really about the ability of the Chinese government to potentially influence large segments of the US population by controlling what's in their feed."
- "It's not really about protecting you. It's about protecting power and control up at the top."
- "Make no mistake about it, this isn't really about protecting you. It's about protecting power and control up at the top."

# Oneliner

Legislation targeting TikTok and similar platforms is more about influence operations than espionage, focusing on protecting power and control at the top rather than individuals.

# Audience

Legislative observers

# On-the-ground actions from transcript

- Monitor the progress of the legislation and advocate for transparency and accountability (suggested).
- Stay informed about the potential impacts on social media platforms and influence operations (suggested).

# Whats missing in summary

Insights on the potential implications for data privacy and free speech rights.

# Tags

#Legislation #TikTok #InfluenceOperations #China #USPopulation