# Bits

Beau says:

- The post board in Minnesota has proposed two changes to regulations that could deny or revoke a cop's certification if found spreading hate material online or participating in hate group forums.
- These proposed changes also include requiring more information during background checks, such as asking if false information had been provided in court.
- Law enforcement associations in Minnesota have opposed these changes, delaying their implementation since the first proposal in 2020.
- The president of the Minneapolis Police Federation believes these changes won't increase public trust and may hinder recruiting efforts, despite aiming to keep bigots out of law enforcement.
- Beau questions why law enforcement associations oppose regulations that aim to prevent bigots from having power over people, stating it decreases public trust.
- The pushback from law enforcement is to argue that the legislature, not the post board, should make these decisions.
- Beau challenges law enforcement associations to use their lobby power to push for these rule changes through the statehouse, expressing skepticism that they will do so.

# Quotes

- "You shouldn't want bigots on your departments, plain and simple. They're a liability to the department, to other officers, and to the public."
- "Having law enforcement associations oppose regulations that prohibit bigots from gaining life and death power over people decreases public trust."
- "Make the calls. Y'all have enough legislators in your pocket. Back the blue, right?"

# Oneliner

The post board in Minnesota proposes changes to deny or revoke cop certifications for hate-related activities, facing opposition from law enforcement associations and raising questions on public trust.

# Audience

Law enforcement reform advocates

# On-the-ground actions from transcript

- Contact your legislators to support regulations denying certification for hate-related activities (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of proposed changes in law enforcement regulations in Minnesota and the opposition faced from law enforcement associations.

# Tags

#Minnesota #LawEnforcement #Regulations #PublicTrust #Opposition