# Bits

Beau says:

- Talks about what comes after the committee hearings, even though they are over and the show has theoretically ended.
- Mentions that the released summary doesn't offer much new information for those who watched the hearings, spanning about 150 pages.
- Points out that the next phase will involve the release of transcripts, where different political factions will try to manipulate discrepancies to create scandal and divert attention.
- Anticipates a phase where inconsistencies in statements will be magnified to cast doubt on testimonies.
- Emphasizes the importance of recognizing that if someone was lying under oath, it implicates others too.
- Notes the upcoming DOJ decision and the historic significance of the committee's actions regarding January 6th.
- Concludes by mentioning the illusion some hold about social media's impact on electoral strategies.

# Quotes

- "They are still under the illusion that social media shares and likes somehow are a valid electoral strategy."
- "The key thing you have to remember is if they were lying under oath, that just means that a whole bunch of people that were handpicked by Trump had no problem lying under oath."
- "Make no mistake, the January 6th event and the hearings afterward, they will be in history books."

# Oneliner

Beau talks about the aftermath of committee hearings, warns of upcoming political manipulations, and underlines the historic significance of recent events, including January 6th.

# Audience

Politically engaged citizens

# On-the-ground actions from transcript

- Contact local representatives to express support for transparency and accountability in political processes (suggested)
- Engage in informed political discourse with friends and family to counter misinformation and manipulation (suggested)

# Whats missing in summary

The emotional weight and nuance in Beau's delivery and analysis can be best experienced in watching the full segment.

# Tags

#CommitteeHearings #PoliticalManipulation #DOJ #HistoricEvents #SocialMediaImpacts