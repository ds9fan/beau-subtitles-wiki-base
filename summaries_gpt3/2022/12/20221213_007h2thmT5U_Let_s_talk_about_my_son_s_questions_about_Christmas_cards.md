# Bits

Beau says:

- His son questioned the tradition of sending Christmas cards, opting for texts instead.
- The son challenged the purpose of Christmas cards beyond tradition.
- Beau realized his mistake in the convo but continued.
- His son called tradition "peer pressure from dead people."
- The environmental impact of Christmas cards was brought up by the son.
- About 3,000 Christmas cards come from one tree, leading to half a million trees used per year in the U.S.
- Beau predicts that the younger generation will move away from the tradition of sending Christmas cards.
- He anticipates backlash, particularly from the Republican Party, labeling it as a "war on Christmas."

# Quotes

- "Tradition is just peer pressure from dead people."
- "Just because we always did something one way isn't a reason to continue doing it that way if a better option presents itself."

# Oneliner

Beau's son challenges the tradition of Christmas cards, pointing out their environmental impact, leading to a prediction of its eventual fade and potential backlash.

# Audience

Parents, environmentalists

# On-the-ground actions from transcript

- Opt for environmentally friendly alternatives to traditional practices (implied)

# Whats missing in summary

The full transcript provides a humorous and thought-provoking take on generational perspectives and traditions, prompting reflection on societal norms and environmental impact.

# Tags

#ChristmasCards #Traditions #GenerationalPerspectives #EnvironmentalImpact #Parenting