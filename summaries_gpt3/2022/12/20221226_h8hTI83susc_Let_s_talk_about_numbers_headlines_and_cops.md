# Bits

Beau says:

- Accepting a challenge to analyze numbers and headlines, particularly regarding police-related information.
- Responding to a message about 650 cops being gunned down this year, Beau clarifies that 655 cops died in the line of duty in 2021.
- Points out that COVID was the leading cause of death for cops, not gunfire, with 472 deaths attributed to COVID in 2020 and 214 deaths from gunfire in 2022.
- Emphasizes the importance of fact-checking and getting information from reliable sources like the Officer Down Memorial page.
- Criticizes the media for framing information in a way that generates emotional reactions rather than providing accurate data.
- Concludes by stating that COVID, not criminal activity, is responsible for the majority of cop deaths, urging people to critically analyze the information they receive.

# Quotes

- "You were provided information that, while accurate, led you to the wrong conclusion."
- "The person responsible for it, who's responsible for it, COVID."

# Oneliner

Beau challenges misconceptions about police deaths, revealing COVID as the primary cause and urging critical analysis of headlines and information.

# Audience

Critical thinkers, fact-checkers.

# On-the-ground actions from transcript

- Fact-check statistics from reliable sources like the Officer Down Memorial page (suggested).
- Encourage others to verify information before accepting it as truth (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of police deaths, revealing the true causes and advocating for critical analysis of information. 

# Tags

#Police #Misinformation #FactChecking #COVID19 #MediaBias