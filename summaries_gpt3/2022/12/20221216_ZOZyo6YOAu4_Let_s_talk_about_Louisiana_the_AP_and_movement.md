# Bits

Beau says:

- The Associated Press uncovered a possible cover-up within law enforcement in Louisiana related to the death of Ronald Green in May 2019.
- Initially, Green's family was told he died in a car crash, but the footage revealed a brutal incident.
- Federal charges against the officers involved were considered but not pursued due to concerns about proving willful misconduct in court.
- The state has decided to pursue charges, resulting in the indictment of five officers for offenses ranging from negligent homicide to malfeasance in office.
- The footage was so disturbing that even the state police's use of force instructor had nothing positive to say and viewed it as warranting charges.
- The journalists at the Associated Press played a significant role in bringing about justice in this case by pursuing the story relentlessly.
- Despite initial allegations of a cover-up, progress in the case has led to cracks in the "thin blue line" in Louisiana.
- Rebuilding trust with the community will be challenging after the release of the incriminating body camera footage and the disturbing actions it captured.
- The footage, while recently made public in May 2021, has been around for some time and showcases actions that no one could deem as normal in law enforcement.
- Law enforcement officials in the state, who typically support officers, are now openly condemning their actions due to the severity of the footage.
- The story will continue to gain widespread coverage, especially as more people view the footage, which is among the most disturbing seen by Beau.

# Quotes

- "The journalists at the Associated Press played a significant role in bringing about justice in this case by pursuing the story relentlessly."
- "Rebuilding trust with the community will be challenging after the release of the incriminating body camera footage."
- "The story will continue to gain widespread coverage, especially as more people view the footage."

# Oneliner

The Associated Press uncovered a possible cover-up within Louisiana law enforcement, leading to charges against officers for the death of Ronald Green and revealing the challenges of rebuilding community trust post-disturbing footage release.

# Audience

Louisiana residents, journalists, activists.

# On-the-ground actions from transcript

- Support investigative journalism by following and promoting the work of organizations like the Associated Press (exemplified).
- Advocate for transparency and accountability within law enforcement by engaging with local officials and demanding justice for victims (exemplified).
- Join community efforts to rebuild trust and foster positive relationships between law enforcement and residents (exemplified).

# Whats missing in summary

The emotional impact and urgency conveyed by witnessing the disturbing footage firsthand can best be understood by viewing the full transcript.

# Tags

#Louisiana #AssociatedPress #LawEnforcement #Justice #CommunityTrust