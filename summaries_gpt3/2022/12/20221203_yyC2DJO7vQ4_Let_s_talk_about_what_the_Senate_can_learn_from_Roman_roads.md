# Bits

Beau says:

- Roman roads built thousands of years ago are linked to economic prosperity today based on a study using satellite images taken at night.
- The study found that living near Roman roads meant living in a more economically prosperous area.
- There's a unique link between Roman roads from ancient times and economic activity today, although a causal relationship hasn't been established yet.
- The causal relationship is unclear - it's unknown whether Roman roads caused prosperity or just connected areas already prospering.
- Infrastructure is a key driver of economic activity and growth, as shown by the impact of Roman roads on economic prosperity.
- The survey found that areas in the US with less infrastructure tend to have lower economic prosperity.
- Infrastructure, whether it's roads, railroads, or broadband, plays a vital role in creating prosperity.
- Maintaining infrastructure is not just about the ability to move; it significantly impacts economic activity.
- The US needs to invest in sustainable infrastructure to avoid falling behind and causing damage to the country and the world.
- Making the decision to invest in viable and sustainable infrastructure is critical for long-term impact.

# Quotes

- "Infrastructure causes economic activity. It causes economic growth."
- "Infrastructure means money and it doesn't matter what kind you're talking about."
- "We're at a crossroads and we need to make the turn to start building infrastructure that is not only economically viable, but also sustainable."

# Oneliner

Roman roads from ancient times are linked to modern economic prosperity, stressing the critical role of infrastructure in driving economic growth and the urgent need for sustainable investments in the US.

# Audience

Policy makers, community planners

# On-the-ground actions from transcript

- Invest in local infrastructure projects (implied)
- Advocate for sustainable infrastructure development (implied)
- Support policies promoting economic growth through infrastructure investments (implied)

# Whats missing in summary

The full transcript provides a detailed historical context and a broader analysis of the impact of infrastructure on economic prosperity.