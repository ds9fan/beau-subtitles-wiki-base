# Bits

Beau says:

- San Francisco PD is considering using robots to deploy lethal force.
- The policy allowing the use of robots for criminal apprehensions is incredibly broad.
- There are very few situations where using robots for lethal force is remotely acceptable.
- The policy lacks limitations and allows too much room for misuse.
- Only the highest trained individuals with the best information should handle such technology.
- San Francisco residents are urged to make their voices heard on this issue.
- Civilian police departments should not have the ability to use robots for lethal force.
- The misuse of robots in this manner is inevitable and will result in lost innocence.
- The lack of necessary information and training makes this policy a dangerous idea.
- San Francisco's Board of Supervisors needs to reconsider this decision.

# Quotes

- "This is a horrible, horrible idea. This will go bad."
- "Civilian police departments should not need to be able to mete out lethal force via a robot."
- "It will become too tempting to use it in situations in which they do not have the information necessary."

# Oneliner

San Francisco PD's broad policy allowing robots for lethal force is a horrible idea that will inevitably lead to misuse, urging residents to speak out against it.

# Audience

San Francisco residents

# On-the-ground actions from transcript

- Make your voice heard on the issue (suggested)
- Advocate against the use of robots for lethal force (implied)

# Whats missing in summary

The full transcript provides additional insights on the potential dangers and ethical concerns surrounding the use of robots for deploying lethal force by civilian police departments.

# Tags

#SanFrancisco #Robots #LethalForce #PolicePolicy #CommunitySafety