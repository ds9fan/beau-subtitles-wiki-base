# Bits

Beau says:

- Elon Musk conducted a poll on Twitter asking if he should resign, with a majority voting yes, including about 10 million users.
- Musk engaged in a Twitter exchange where the suggestion of only allowing verified users (blue checks) to vote was made, which Musk seemed to entertain, hinting at voter suppression.
- Tesla, the company, was downgraded by Oppenheimer & Company due to concerns about banning journalists without clear standards, potentially affecting consumers who support climate change mitigation.
- There are worries about a negative feedback loop for Tesla stemming from the controversial content some unbanned users create on Twitter, leading to brands pulling out and users leaving the platform.
- Oppenheimer believes that Tesla's value drop, around 50%, could be influenced by these Twitter-related issues.
- Beau suggests that Musk stepping away from the company and bringing on a board with content guidelines could be a way out of the situation.
- Beau challenges the idea that going woke leads to going broke, citing the real issue as going fast without cash as the downfall for companies.
- The situation raises concerns about the impact of Twitter controversies on Tesla's market value and reputation.

# Quotes

- "The reality is, go fast, no cash."
- "In what is just a hilarious development after that, Musk was seen in a conversational Twitter with somebody who suggested that he only allows those people with blue checks to vote."
- "One of the reasons here, we believe banning journalists without consistent defensible standards or clear communication in an environment where many people believe free speech is at risk is too much for a majority of consumers to continue supporting Mr. Musk slash Tesla."

# Oneliner

Elon Musk's Twitter antics and potential voter suppression raise concerns about Tesla's market value and reputation, prompting suggestions for a way out.

# Audience

Social media users

# On-the-ground actions from transcript

- Contact Tesla and express concerns about the company's handling of controversies on Twitter (suggested)
- Join or support organizations advocating for responsible social media conduct by companies like Tesla (implied)

# Whats missing in summary

The full transcript provides deeper insights into the potential consequences of social media controversies on corporate entities like Tesla.