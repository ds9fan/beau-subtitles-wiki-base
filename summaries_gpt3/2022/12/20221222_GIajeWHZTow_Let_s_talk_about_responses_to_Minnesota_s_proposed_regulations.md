# Bits

Beau says:
- Addresses proposed regulations in Minnesota regarding law enforcement officers participating in online hate speech.
- Explains the purpose of the First Amendment and distinguishes it from the concept of free speech.
- Argues that law enforcement officers, as agents of the government, should be held to a higher standard.
- Suggests that being a bigot contradicts the job qualifications for law enforcement officers.
- Draws parallels with other professions like nursing and fast food service to illustrate the impact of biases on job performance.
- Counters the argument that regulating hate speech makes it more powerful, especially in the context of law enforcement.
- Points out the danger of bigoted officers within police departments and their potential impact on public trust and safety.

# Quotes

- "Not being a bigot is kind of a bona fide job qualification for law enforcement."
- "There is no reason for an officer to oppose getting rid of bigots on police forces, except for one."
- "Bigots on police departments. They are a danger to the department."
- "I personally cannot think of anything that makes that kind of speech more powerful than giving it a badge and a gun."
- "Making it more powerful."

# Oneliner

Beau explains why law enforcement officers participating in hate speech should face consequences, linking it to public trust and safety, and challenges the argument against regulating such speech.

# Audience

Law enforcement reform advocates

# On-the-ground actions from transcript
- Advocate for regulations that hold law enforcement officers accountable for hate speech (suggested)
- Support initiatives to remove bigoted officers from police forces (suggested)

# Whats missing in summary

The full transcript provides a detailed argument against allowing law enforcement officers to participate in hate speech and stresses the importance of maintaining public trust and safety within police departments.

# Tags

#LawEnforcement #HateSpeech #Regulations #PublicTrust #Accountability