# Bits

Beau says:

- San Francisco Police Department proposed a policy allowing armed robots, which was initially approved but later voted down by the Board of Supervisors due to public outcry.
- The chief of police defended the use of robots as a last resort option in deadly force situations to potentially prevent mass violence.
- The policy faced criticism for being overly broad, allowing for the apprehension of criminals, which could lead to misuse and unjustified lethal force.
- The public's demand for more restrictive policies stems from witnessing officers harming innocents and escaping accountability by citing training and policy.
- Beau warns that the department may attempt to reintroduce the policy once public attention wanes and urges people to scrutinize future policies for their scope and potential consequences.
- Deploying armed robots should be strictly limited to mass incidents, with clear justifications for lethal force already in place.
- There is a concern that broad policies on armed robots could result in mission creep and increased loss of innocent lives due to hasty deployment of lethal force.

# Quotes

- "We need to be looking at a way to stop that rather than new tools to do it."
- "The overwhelming outcry came from the fact that the policy was just ridiculously broad."
- "The key thing to remember when you're talking about anything like this is to actually read the policy, not the sound bites."
- "The department will undoubtedly try to sneak this through again."
- "It shouldn't be, well, maybe it would be justified. No, it has to already be deemed justified to take that thing out of the van."

# Oneliner

San Francisco rejected a broad policy on armed robots due to public outcry and the need for stricter, more justifiable regulations.

# Audience

Policy Advocates

# On-the-ground actions from transcript

- Scrutinize future policies on armed robots for their scope and potential consequences (suggested)
- Advocate for strict regulations on the deployment of armed robots only in mass incidents with clear justifications for lethal force (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the concerns surrounding the use of armed robots by law enforcement and the importance of ensuring strict, justifiable policies to prevent misuse and protect innocent lives.

# Tags

#SanFrancisco #PolicePolicy #ArmedRobots #PublicSafety #Accountability #CommunityPolicing