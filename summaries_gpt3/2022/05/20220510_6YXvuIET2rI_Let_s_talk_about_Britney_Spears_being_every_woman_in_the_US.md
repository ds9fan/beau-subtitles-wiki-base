# Bits

Beau says:

- Britney Spears' situation mirrors that of every woman in the United States right now, as she faced backlash for posting photos in her birthday suit.
- Despite being 40 years old, Britney is not trusted with bodily autonomy and is seen as needing someone else to make decisions for her.
- There is a push to return women to a legal structure where others control their choices, reminiscent of historical restrictions on women's freedom.
- The argument to overturn Roe v. Wade is not about moral reasons but about control and limiting women's autonomy.
- The double standard applied to Britney showcases society's desire to control women and restrict their agency.
- The focus on controlling women is about preserving power for mediocre men over women, rather than genuine concerns about family planning.
- The criticism and attempts to control Britney reveal deeper societal issues around gender roles and power dynamics.
- Restrictions on women's autonomy are not equally applied and are often rooted in outdated stereotypes and biases.
- The push to control women's choices extends beyond family planning and into various aspects of their lives.
- The underlying issue is about control and maintaining power dynamics that favor certain groups over others.

# Quotes

- "It's about control. It always has been. Everything else, that's just camouflage."
- "It's about returning women to a subservient class where they have to do what they're told."
- "It's about preserving mediocre men's power over women."
- "If you think it's about anything other than control, you're wrong."
- "Freedoms that should be extended to everybody at all times."

# Oneliner

Britney Spears' situation epitomizes the ongoing battle for women's autonomy and the underlying desire for control over their choices in society.

# Audience

Advocates for gender equality

# On-the-ground actions from transcript

- Challenge and push back against attempts to control and restrict women's autonomy (implied)
- Support organizations and movements that advocate for women's rights and autonomy (suggested)

# Whats missing in summary

The full transcript provides a deeper exploration of societal attitudes towards women's autonomy and the underlying power dynamics that influence decision-making.

# Tags

#BritneySpears #GenderEquality #Autonomy #Control #Women'sRights #PowerDynamics