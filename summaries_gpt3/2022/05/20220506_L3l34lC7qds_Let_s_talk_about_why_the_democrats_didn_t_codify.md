# Bits

Beau says:
- Dissects the historical fact about federal legislation and Roe v. Wade, pointing out its limited impact.
- Emphasizes the importance of understanding the raw exercise of power by the Supreme Court.
- Asserts that the solution lies at the local level and not in Washington, D.C.
- Warns that the current political situation is not ordinary and stresses the need for people to realize this.
- Mentions the ongoing threats beyond abortion rights, indicating a broader agenda.
- Criticizes the focus on federal legislation as merely adding a speed bump for those in power.
- Talks about the importance of building local power structures instead of relying solely on federal actions.
- Illustrates how certain groups like the Tea Party and MAGA have influenced national parties by starting at the local level.
- Concludes by reiterating the limited impact of past legislative actions in the current political climate.

# Quotes

- "The solution to this is not in D.C. It is at the local level."
- "You're not dealing with anything more than the raw exercise of power at this point."
- "If you are not a straight white guy who happens to also be a theocratic Christian, they're coming for you."
- "This isn't politics as usual."
- "It is more important to understand the kind of fight you're actually in and that this is just the beginning."

# Oneliner

Beau explains why focusing on federal legislation for Roe v. Wade isn't the solution; local power structures are key in the ongoing political battle against regressive policies.

# Audience

Activists, Community Leaders

# On-the-ground actions from transcript

- Build and strengthen local power structures to resist regressive policies (implied).
- Motivate and mobilize the base by developing policies that resonate with normal people (implied).

# Whats missing in summary

Beau's deep analysis and historical perspective provide critical insights into the ineffective nature of federal legislation in combating regressive policies and the importance of building local power structures for lasting change.

# Tags

#FederalLegislation #RoevWade #LocalPowerStructures #PoliticalActivism #CommunityLeadership