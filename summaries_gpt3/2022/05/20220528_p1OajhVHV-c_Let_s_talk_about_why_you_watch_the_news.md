# Bits

Beau says:

- Raises the question of why people watch the news, whether it's for entertainment or to stay informed.
- Emphasizes the importance of actionable information that can help in making better decisions.
- Points out that accurate information without context is not very useful.
- Criticizes news outlets that sensationalize stories with dramatic headlines that often turn out to be exaggerated or debunked.
- Warns against being misled by news sources that prioritize ratings over accuracy.
- Talks about how some news outlets intentionally publish false information or lack context, making it impossible to use the information for decision-making.
- Gives an example of misleading reporting on Biden's supply chain issues, stressing the importance of providing context.
- Explains how good information with context can prepare individuals for the future and help them understand the full picture.
- Draws a distinction between news outlets that inform and those that serve as propaganda, aiming to influence rather than inform.
- Encourages viewers to question the news sources they consume and to stop watching if it doesn't improve their understanding or quality of life.

# Quotes

- "Information, being better informed about something, should remove fear."
- "If it's just leaving you angry and scared, you should probably stop watching."

# Oneliner

Beau questions the purpose of news consumption, advocates for actionable information with context, and warns against propaganda outlets that aim to influence rather than inform.

# Audience

News consumers

# On-the-ground actions from transcript

- Question the news sources you consume (implied)
- Stop watching news outlets that leave you angry and scared (implied)

# Whats missing in summary

The importance of critically evaluating news sources and their impact on decision-making.

# Tags

#NewsConsumption #MediaLiteracy #Context #Propaganda #DecisionMaking