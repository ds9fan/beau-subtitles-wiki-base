# Bits

Beau says:

- Corrects criticism of law enforcement regarding countering a rifle with a pistol.
- Shares a recent incident where a woman used a pistol to stop a shooter with an AR at a graduation party.
- Points out that shooters often have a domestic violence connection.
- Emphasizes that countering an AR with a pistol is possible and done frequently.
- Argues that it's not about gear but about the warrior mentality and commitment.
- States that the U.S. military could issue gear to everyone, but it's the commitment that matters.
- Explains that what makes elite individuals special is their ability to perform without relying solely on equipment.
- Disputes the idea that gunfights are determined by the power of the weapons involved.

# Quotes

- "Countering an AR with a pistol can be done, and it's done all the time."
- "It's not the equipment. They've proven that over and over again."
- "It's just physics. I don't even know what that means."

# Oneliner

Beau corrects misconceptions about countering a rifle with a pistol, citing real-life incidents and stressing the importance of commitment over gear in law enforcement.

# Audience

Law enforcement critics

# On-the-ground actions from transcript

- Understand the importance of commitment and warrior mentality in law enforcement (implied)

# Whats missing in summary

In-depth analysis of the impact of mindset and training over equipment in law enforcement situations

# Tags

#LawEnforcement #Criticism #RifleVsPistol #WarriorMentality #Commitment