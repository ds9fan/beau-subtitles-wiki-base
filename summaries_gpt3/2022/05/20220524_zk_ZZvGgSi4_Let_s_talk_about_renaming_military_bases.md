# Bits

Beau says:

- The US military is renaming installations named after Civil War traitors by 2023.
- Congress-created commissions suggest new names for the installations, which must be confirmed by Congress.
- Fort Bragg will become Liberty, Hood will be renamed Cavazos, and Gordon will change to Eisenhower.
- Lee will be jointly renamed after General Greg and Charity Adams, the first black woman officer in the Women's Army Auxiliary Corps.
- Pickett will be renamed after Van Barfoot, a Medal of Honor recipient known for embarrassing Klansmen.
- A.P. Hill will be named after Dr. Mary Walker, an abolitionist and Medal of Honor recipient during the Civil War.
- Benning will be renamed after Hal Moore, known from the movie "We Were Soldiers."
- Rucker will be renamed after Michael Novosel, a Medal of Honor recipient who flew helicopters during Vietnam.
- Fort Polk will be named after Sergeant William Henry Johnson, a Medal of Honor recipient from World War I.
- The renaming process has widespread support and aims to address discomfort at installations named after controversial figures.

# Quotes

- "It is time for this to happen, and it has a lot of support, both across the country and within the military community."
- "There are a lot of people who don't like working at installations that are named after people that they really don't like."

# Oneliner

The US military is renaming installations named after Civil War traitors by 2023, with widespread support and a focus on honoring diverse heroes.

# Audience

Military personnel, historians, activists.

# On-the-ground actions from transcript

- Support the renaming process by advocating for the changes in your community (implied).

# Whats missing in summary

The transcript provides a detailed overview of the upcoming renaming of US military installations, shedding light on the diverse heroes who will be honored in place of controversial figures.

# Tags

#USMilitary #Renaming #CivilWar #Heroes #Support