# Bits

Beau says:

- Explains the historical significance of the Scopes trial in the 1920s and its impact on current social issues being debated in courts and legislation.
- Describes the Butler Act in Tennessee prohibiting the teaching of evolution and the teacher who challenged it in court in 1925.
- Details the teacher's appeal to the Tennessee Supreme Court on the grounds of the law being overly broad, violating free speech, Tennessee's constitution, and freedom of religion.
- Mentions the Supreme Court case in 1968, Epperson v. Arkansas, which found such laws violated the establishment clause of the First Amendment.
- Emphasizes the importance of changing societal thinking to bring about real change, rather than just changing laws.
- Criticizes current Republican actions as regressive, authoritarian, and against the majority of society's desires.
- Stresses the need for political power to drive change based on societal thought and values, pointing out that people already support freedom, choice, education, and critical thinking.
- Urges the Democratic Party to exercise political power effectively to counter regressive authoritarianism and uphold the majority's wishes for progress and freedom.

# Quotes

- "To change society, you don't have to change the law. You have to change the way people think."
- "It's regressive. It isn't trying to change thought and then change the law. It's trying to use the law to change thought."
- "Thought is already in agreement on the pro-freedom side, the side that wants choice, that wants options, that wants education."
- "It's the raw exercise of political power that will shift this dynamic."
- "If you don't fight back, then it doesn't matter what thought gets shifted."

# Oneliner

The Scopes trial history teaches changing societal thinking drives lasting change, urging political power to uphold majority values against regressive actions.

# Audience

Democratic activists, concerned citizens

# On-the-ground actions from transcript

- Mobilize for election issues (implied)
- Advocate for freedom, choice, and education (implied)
- Support critical thinking in schools (implied)
- Pressure representatives to act in accordance with majority wishes (implied)
- Fight against regressive authoritarianism through political power (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of historical events to draw parallels with contemporary challenges and stresses the importance of political power in upholding societal values against regressive actions.

# Tags

#ScopesTrial #PoliticalPower #DemocraticParty #Education #Freedom #Authoritarianism