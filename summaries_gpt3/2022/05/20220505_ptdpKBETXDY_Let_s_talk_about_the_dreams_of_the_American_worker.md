# Bits

Beau says:

- A group of combat vets share nightmares about their past jobs, revealing unexpected sources of trauma.
- The group finds humor in each other's stories, showing gallows humor as a coping mechanism.
- There is a misconception in the United States that PTSD only stems from war events.
- Trauma can manifest from various sources, not limited to combat situations.
- Beau's Twitter post about job nightmares sparks responses from others sharing similar experiences.
- Combat vets talk about stressful jobs like working at a cell phone store, challenging the stereotype of PTSD triggers.
- The cavalier attitudes towards American workers discussing their job conditions are concerning.
- Beau suggests that the United States needs a labor movement to improve worker conditions.
- The stress faced by tough men at places like car dealerships points to underlying issues in the American workforce.
- Complaints about job conditions shouldn't be dismissed by comparing them to deployed individuals' experiences.

# Quotes

- "The stress came from a car dealership, a restaurant, a home improvement store."
- "The United States needs a resurgence in the labor movement."
- "Trauma can come from all kinds of places."
- "PTSD really only affects war events. And it's not true."
- "Those who deployed apparently have the same complaints."

# Oneliner

Combat vets share nightmares about past jobs, challenging stereotypes on PTSD triggers, calling for a labor movement to improve worker conditions.

# Audience

American workers

# On-the-ground actions from transcript

- Support and advocate for labor movements to improve worker conditions (suggested)
- Educate others on the diverse sources of trauma and the need for empathy and understanding (implied)

# Whats missing in summary

The full transcript provides deeper insights into the unexpected sources of trauma for American workers and the importance of acknowledging and addressing these issues in society.