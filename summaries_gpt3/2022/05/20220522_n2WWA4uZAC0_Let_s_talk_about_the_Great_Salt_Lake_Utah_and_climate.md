# Bits

Beau says:

- The Great Salt Lake in Utah is receding, currently 11 feet lower than when pioneers arrived, causing damage to birds and wildlife.
- For every foot the lake drops, 150 square miles of the lake bed are exposed, impacting the economy and Utah's GDP.
- Proposed solution: building a pipeline to pump in salt water from the Pacific, facing potential environmentalist backlash.
- Climate change is a significant factor in the lake's decline, with impacts being felt particularly in the western and southwestern regions.
- The prevailing attitude of trying to overcome climate change without changing our way of living is unsustainable and doomed to fail.
- Exposed lake beds pose additional risks due to arsenic content, exacerbating the already dire situation.
- Delaying necessary changes will only worsen the effects of climate change and necessitate more drastic actions in the future.
- The impacts of climate change are inevitable and will affect everyone, requiring proactive measures from legislatures across the country.
- Utah's consideration of solutions like the pipeline is a start, but real change may involve altering consumption patterns and industries in the state.
- Internal and external migration due to climate change is likely, underscoring the need to acknowledge and prepare for these realities.

# Quotes

- "The longer we put off making the changes that are necessary, the worse it's going to get."
- "We can't just keep pretending like it's not happening."
- "There are impacts from climate change that are guaranteed to happen. They won't just skip us."
- "We're just seeing the tip of the iceberg that's melting."
- "There are news stories every single day that show the impacts that are already here."

# Oneliner

The Great Salt Lake's decline due to climate change underscores the urgent need for proactive, sustainable solutions to address environmental impacts and ensure a livable future for all.

# Audience

Legislatures, policymakers, environmentalists

# On-the-ground actions from transcript

- Start considering proactive measures to address climate change impacts (implied)
- Advocate for sustainable solutions and policies in response to environmental challenges (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of the environmental and economic consequences of the Great Salt Lake's decline, urging immediate action to mitigate climate change effects and transition to more sustainable practices.

# Tags

#ClimateChange #GreatSaltLake #Utah #EnvironmentalImpact #Sustainability