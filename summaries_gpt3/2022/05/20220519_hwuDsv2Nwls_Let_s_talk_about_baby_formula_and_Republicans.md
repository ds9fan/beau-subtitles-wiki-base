# Bits

Beau says:

- Addresses the shortage of baby formula in the United States caused by a plant shutdown and Trump-era trade policies.
- Republicans have been criticizing Biden for not doing enough about the baby formula shortage, despite having the power to propose legislation themselves.
- The Democratic Party introduced a bill to provide $28 million for FDA inspectors to address the formula shortage, but 192 Republicans in the House voted against it.
- Republicans seem more focused on making the American people suffer to blame Biden than on finding solutions.
- Biden has taken actions like directing the military to fly in formula and using the DPA to expedite ingredients to address the shortage.
- Beau questions why Republicans voted against the bill given the relatively small amount of money involved compared to campaign costs.
- Criticizes politicians for spending more on campaigns than on ensuring children have food.
- Points out the irony of passing legislation to restrict reproductive rights while neglecting efforts to provide safe food for children.

# Quotes

- "They'll spend more to make sure they get to sit in that seat than they will to make sure kids have food in this country."
- "But they're not going to put any effort behind making sure that child has safe food."
- "That doesn't seem very pro-life to me."

# Oneliner

Republicans block bill to address baby formula shortage, prioritizing politics over children's well-being.

# Audience

Legislative advocates

# On-the-ground actions from transcript

- Contact your representatives to advocate for legislation addressing issues like the baby formula shortage (implied).

# Whats missing in summary

The emotional impact of prioritizing political gains over the well-being of children. 

# Tags

#BabyFormulaShortage #Legislation #PoliticalPriorities #ChildWelfare #Biden #Republicans