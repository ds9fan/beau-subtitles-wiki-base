# Bits

Beau says:

- Secretary of the Interior released a report on federal government's role in boarding schools for indigenous children, parts of American history concealed and overlooked. 
- Report examines the 1819 Civilization Fund Act authorizing stripping indigenous people of their identities, culture, spirituality, hair, names, and families.
- Schools were horrific with unmarked graves and tragic stories.
- Purpose isn't just to dwell on historical wrongs but to pave the way for healing and closure.
- Legislation will be discussed to establish a commission similar to Canada's to address this history.
- Some resist discussing this history as it challenges the American myth taught in schools.
- Acknowledging and understanding these atrocities is vital to shaping the country's future.

# Quotes

- "Understanding these types of actions and making sure that they don't happen again is what will shape this country."
- "We have to go through it. We have to understand it. We have to make sure it never happens again."

# Oneliner

The Secretary of the Interior's report sheds light on the harrowing history of boarding schools for indigenous children, stressing the importance of understanding and addressing these atrocities for a better future.

# Audience

Educators, Activists, Legislators

# On-the-ground actions from transcript

- Establish legislation to create a commission for resolution and closure (implied)
- Advocate for the inclusion of this history in educational curriculums (implied)

# Whats missing in summary

The emotional weight and depth of the stories behind the horrors of the boarding schools can be best understood by watching the full transcript.

# Tags

#IndigenousHistory #BoardingSchools #Healing #Legislation #AmericanMythology