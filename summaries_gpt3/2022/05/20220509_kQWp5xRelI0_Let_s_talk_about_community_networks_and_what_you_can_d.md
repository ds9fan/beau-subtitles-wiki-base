# Bits

Beau says:

- Talks about community networking and addresses the common question of "What can I do?" when it comes to helping out.
- Uses Hurricane Michael relief efforts as an example to demonstrate the power of community networks in disaster relief.
- Describes how people from various networks came together to provide aid after the hurricane hit Panama City.
- Explains the different roles individuals played in the relief efforts, from collecting supplies to monitoring social media for requests and road updates.
- Emphasizes the importance of utilizing one's skills and resources in crisis situations.
- Acknowledges the valuable contributions of individuals who may not physically participate but support through other means like donations.
- Stresses the significance of multiple networks collaborating to achieve greater impact in addressing challenges.
- Encourages individuals to recognize their skills and find ways to contribute to making things better for everyone, regardless of their abilities.
- Mentions the necessity of local networks in mitigating harm and solving problems faced by communities.
- Concludes by affirming that everyone's skill set is valuable and needed in community efforts.

# Quotes

- "Your skill, whatever it is, is needed."
- "It's all a matter of applying what you're good at to making things better for everybody."
- "The solution to the problems that we're going to be facing has centered where you're at."
- "There's always a way you can help."
- "What can I do? And it's specific."

# Oneliner

Beau shares insights on community networking using Hurricane Michael relief efforts to illustrate the impact of individual contributions and the necessity of leveraging diverse skills in crisis situations.

# Audience

Community members

# On-the-ground actions from transcript

- Join local community organizations to build networks and support disaster relief efforts (exemplified)
- Organize collection drives for supplies in preparation for future emergencies (suggested)
- Monitor social media for real-time needs and updates during crises (exemplified)

# Whats missing in summary

The full transcript provides a detailed account of how diverse skills and resources can be utilized in community networking during disaster relief efforts.

# Tags

#CommunityNetworking #DisasterRelief #CommunitySupport #SkillsUtilization #Collaboration