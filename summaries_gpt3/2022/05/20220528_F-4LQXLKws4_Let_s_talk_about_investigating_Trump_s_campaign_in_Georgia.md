# Bits

Beau says:

- Federal government's investigation into Trump's attempts to overturn the 2020 election is shrouded in mystery, leaving many questioning their actions.
- Signs indicate that the government may be building a case regarding the fake electors scheme in Georgia and other states.
- The probe, originally in Georgia, has expanded to other states, with Michigan confirmed and signs of further expansion.
- The FBI in Georgia is questioning prominent Republicans about their interactions with Trump, Giuliani, and others tied to Trump.
- Subpoenas have been issued to individuals who backed out of being electors, suggesting they may have relevant information.
- Subpoenas request communications with over 20 named individuals dating back to before the 2020 election.
- In Michigan, a potential fake elector backed out, citing illness as the reason.
- The FBI's interest in individuals who didn't participate but may have knowledge indicates a focus on potential criminality.
- The presence of someone from the National Archives during interactions adds weight to the idea of building a criminal case.
- The investigation is ongoing, with a grand jury set to meet in D.C., indicating a lengthy process.

# Quotes

- "We won't find out for a while. These things take time."
- "It's worth noting when little bits and pieces like this come out."
- "The FBI believes there might be some kind of criminality involved."
- "So when little bits and pieces like this come out, it's worth noting."
- "But the interesting thing is that it's people who backed out."

# Oneliner

Federal government investigating potential criminality in the fake electors scheme post-2020 election, expanding probes across states with a focus on individuals backing out but possibly possessing critical information.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Contact local representatives to advocate for transparency and accountability in the investigation (suggested).
- Stay informed about updates on the investigation and share relevant information with your community (exemplified).
- Support efforts to uphold the integrity of elections and ensure attempts to undermine democracy are addressed (implied).

# Whats missing in summary

Insights on the potential implications of the ongoing investigation and the importance of monitoring government actions closely.

# Tags

#GovernmentInvestigation #FakeElectorsScheme #2020Election #FBI #NationalArchives