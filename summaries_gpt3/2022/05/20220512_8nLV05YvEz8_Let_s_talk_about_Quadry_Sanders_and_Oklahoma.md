# Bits

Beau says:

- Analyzing the Sanders case in Oklahoma where two cops shot and killed someone.
- Usually, after-action analyses focus on policy deviations leading to bad outcomes, but this case is different.
- The shooting is unexplainable and perplexing, with officers already arrested and charged with manslaughter.
- The severity of the charges gives a high likelihood of conviction, but sentences for taking a life in Oklahoma are relatively low.
- The minimum sentence on the charge is four years, requiring 85% of the sentence to be served before release.
- The video footage doesn't provide a clear understanding of why the officers fired, making it one of the most inexplicable shootings.
- The charge presupposes that the officers panicked and fired, potentially leading to convictions despite normal justifications being insufficient.
- Despite the expected convictions, there may be public outcry over the sentencing.
- Beau expresses disbelief and confusion over the events, unable to comprehend why the officers fired.
- This case stands out as exceptionally puzzling and raises doubts about whether justice will be perceived.

# Quotes

- "The shooting is unexplainable and perplexing."
- "I do not understand why they fired."
- "This is the most unexplainable shooting I've ever seen."
- "I have a high expectation that there's going to be convictions on this."
- "But the normal defenses that officers use are not really going to help in this one."

# Oneliner

Analyzing a perplexing shooting in Oklahoma where officers face charges, but justice may be questioned due to the unexplainable nature of the events.

# Audience

Advocates, Activists, Reformers

# On-the-ground actions from transcript

- Advocate for transparent investigations into police shootings (implied)
- Support initiatives for police accountability and training (implied)

# Whats missing in summary

Context on the legal and societal implications of the case.

# Tags

#Oklahoma #PoliceShooting #Justice #Accountability #Manslaughter