# Bits

Beau says:

- Talks about the future of the foreign policy of the United States, focusing on Africa and Russia.
- Mentions the creation of Africa Command by the United States in preparation for Africa becoming a priority.
- Describes a bill in Congress, HR 7311, aimed at countering Russian influence in Africa.
- Points out that while framed as countering Russia, the bill actually prepares the US for increased influence in Africa.
- Outlines the strategy of strengthening democratic institutions, human rights, and monitoring natural resources to bring African nations into the US sphere of influence.
- Notes that the US intends to use soft power to establish influence rather than military force.
- Mentions the creation of de facto agents of influence and elevating cultural leaders friendly to US interests as part of the strategy.
- Indicates that the US plans a long-term foreign policy strategy in Africa akin to its campaign in Ukraine.
- Emphasizes that while these operations may not be horrible for the countries involved, there is always a coercive element accompanying them.
- Concludes that with the bill's passage, Africa will become a hotspot for US influence and resources.

# Quotes

- "Africa is the new hotspot right then."
- "Generally speaking, these types of operations are not horrible for the countries that they're run in."
- "The United States is committed to this course."
- "The United States is going to try to implement."
- "Y'all have a good day."

# Oneliner

Beau outlines how the US is gearing up to extend influence in Africa under the guise of countering Russian activities, with a focus on soft power strategies and a long-term foreign policy vision.

# Audience

Policy Advocates

# On-the-ground actions from transcript

- Contact local representatives to express concerns about US foreign policy strategies in Africa (suggested).
- Join organizations focused on monitoring and advocating for transparency in foreign policies (exemplified).

# Whats missing in summary

Insights on the potential impacts of increased US influence in Africa and the importance of monitoring and questioning foreign policy decisions.

# Tags

#USForeignPolicy #Africa #Russia #InfluenceOperations #SoftPower