# Bits

Beau says:

- Explains why he started looking left politically: anti-authoritarianism and a desire to prevent suffering.
- Rejects the idea of accelerationism, which aims for suffering to push political awakening.
- Expresses disappointment in lower views on community networking videos, but values potential impact on even a small percentage of viewers.
- Believes in the importance of building a better world, even though it will be challenging.
- Emphasizes that changing the world is difficult but necessary to prevent suffering and create positive change.

# Quotes

- "I started looking left because I was always anti-authoritarian and then I got really tired of watching people suffer and there's nothing on the right to combat that."
- "I cannot get behind that, period."
- "Not every video is for every person."
- "It's worth doing because it's hard. It's worth doing because it's the right thing to do."
- "Encourage the suffering to get to the point where they learn their lesson. I will never sign off on that."

# Oneliner

Beau explains his anti-authoritarian stance, rejects accelerationism, values impact over views, and stresses the difficulty but importance of building a better world.

# Audience

Activists, community builders, supporters

# On-the-ground actions from transcript

- Start a community network (exemplified)
- Engage in building a better world through actions that prevent suffering and create positive change (suggested)

# Whats missing in summary

The emotional depth and personal conviction Beau brings to his rejection of accelerationism and commitment to preventing suffering and creating positive change can best be experienced by watching the full transcript.

# Tags

#CommunityBuilding #Anti-Authoritarianism #PreventingSuffering #PositiveChange #PoliticalActivism