# Bits

Beau says:

- Introduces a hypothetical scenario to counter arguments against family planning.
- Advocates for focusing the debate on bodily autonomy rather than false equivalencies.
- Describes a scenario involving a fertilized egg and a baby named Billy hanging over a cliff to illustrate the difference.
- Emphasizes that the argument of viability is key in discussing moral questions surrounding abortion.
- Advises always redirecting the debate back to bodily autonomy.
- Dismisses debates over when life begins as not scientific and irrelevant compared to viability.
- Stresses the importance of understanding moral questions change at the point of viability.

# Quotes

- "Can you force a person to do something with their own body against their will?"
- "The real question is about bodily autonomy."
- "Don't get into debates over when it starts, because it doesn't matter."
- "The question is, when is it viable on its own?"
- "There isn't really the moral question that they're trying to present."

# Oneliner

Beau presents a scenario to counter false equivalencies against family planning, urging a focus on bodily autonomy and viability rather than arbitrary debates on when life begins.

# Audience

Advocates for reproductive rights.

# On-the-ground actions from transcript

- Redirect debates on family planning back to bodily autonomy (suggested).
- Focus on the question of viability in the abortion debate (suggested).

# Whats missing in summary

The full transcript provides a detailed and effective strategy to counter false equivalencies against family planning by shifting the focus to bodily autonomy and viability rather than when life begins.