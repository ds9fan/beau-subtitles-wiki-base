# Bits

Beau says:

- Politicians use talking points that quickly expire, but they expect us to forget about them.
- Republicans were pushing for people to shop and consume at small businesses during the early days of the global public health issue.
- A bill went to the Senate to help gyms and restaurants impacted by the pandemic, but Republicans largely voted against it.
- The bill aimed to refill a fund to support struggling businesses, including restaurants and gyms.
- Only a fraction of the restaurants that applied for relief actually received any funds due to the program running out of money.
- The Independent Restaurant Coalition warns that up to half of the 177,000 restaurants awaiting funds could close without this support.
- The failure to pass the bill puts tens of thousands of restaurants and small businesses at risk of closure.
- The bill also had provisions for gyms, live event operators, and possibly ferries.
- Republicans used small businesses as talking points to encourage economic activity, but failed to follow through with meaningful support.
- The new talking point is to argue against providing relief by deeming certain groups as no longer useful.

# Quotes

- "Of course, maybe we do, because you don't really see politicians held accountable for this behavior."
- "Some of you may die, but it's a risk they're willing to take."
- "Guess that talking point expired."

# Oneliner

Politicians' talking points on supporting small businesses quickly expire without real action, leaving thousands of businesses at risk of closure due to lack of support.

# Audience

Voters, activists, community members

# On-the-ground actions from transcript

- Support local restaurants and small businesses by dining in or ordering takeout (implied)
- Advocate for government support for struggling businesses by contacting elected officials (implied)

# Whats missing in summary

The emotional impact of businesses closing and the lack of accountability for politicians' empty promises.

# Tags

#SmallBusinesses #PoliticalPromises #GovernmentSupport #CommunitySupport #Accountability