# Bits

Beau says:

- Explains the recent Senate vote on turning Roe into federal law and its failure.
- Notes that every Republican senator, along with Joe Manchin, voted against the legislation.
- Points out that without federal legislation, the decision reverts back to the states.
- Emphasizes the lack of exemptions for ectopic pregnancies in many state laws if Roe is overturned.
- Describes the risks of ectopic pregnancies, including infection, internal bleeding, and death.
- Criticizes the Republican Party for endangering the lives of tens of thousands of women by not including exemptions for ectopic pregnancies.
- Condemns the Republican Party for valuing a talking point over the lives of their own constituents.
- Raises concerns about Republican women being disproportionately affected by these laws.
- Suggests that decisions on women's reproductive rights are being made by "mediocre men" influenced by lobbyists.
- Urges Republican women to reconsider supporting senators who endanger their lives for political gains.

# Quotes

- "The Republican Party as a whole just voted to endanger the lives of tens of thousands of women per year."
- "This is the Republican Party actively targeting Republican women."
- "They put party over people. They put a talking point over the lives of people."
- "Women will die. Women will die."
- "Because the Republican Party doesn't have a backbone, because the Republican Party doesn't have any principle."

# Oneliner

Republicans in the Senate voted against turning Roe into federal law, endangering thousands of women's lives, prioritizing talking points over constituents.

# Audience

Republican women

# On-the-ground actions from transcript

- Mobilize within the Republican Party to demand exemptions for ectopic pregnancies in state laws (implied).
- Advocate for stronger protections for women's reproductive rights within the party (implied).

# Whats missing in summary

The emotional impact on women's lives and the urgent need for advocacy to protect reproductive rights.

# Tags

#Senate #RoeVWade #ReproductiveRights #RepublicanParty #Advocacy