# Bits

Beau says:

- Loop current active, big hurricane year expected.
- Stressed supply chains mean delays in supplies and relief post-hurricane.
- Private companies may struggle to provide relief due to supply chain issues.
- Individuals in hurricane-prone areas urged to prepare for longer recovery times.
- People in evacuation areas should also prepare for reduced supplies due to increased demand.
- Even those far from hurricanes should prepare for supply chain stress post-storm.
- Stress on supply chains nationwide after hurricanes will impact availability of goods.
- Urges viewers to plan evacuation routes, monitor weather, and ensure access to gas.
- Recommends checking home insurance policy and gathering necessary documents.
- Suggests preparing emergency supplies including water, fire, shelter, knife, first aid kit, meds, tarp, charcoal, and water.
- Encourages finding ways to charge electrical devices in advance.
- Warns of high prices during evacuation and need to be prepared for extended sheltering.
- Expects longer recovery periods post-hurricane due to stressed supply chains.

# Quotes

- "Make sure that you're prepared for that."
- "Plan your evacuation route now."
- "You need to prepare now, not once the news of the storm breaks."
- "It's going to take even longer to get back up and running."
- "Keep an eye on the weather."

# Oneliner

Be prepared for hurricanes this year - stressed supply chains mean longer recovery times and reduced supplies. 

# Audience

Residents in hurricane-prone areas

# On-the-ground actions from transcript

- Plan your evacuation route now (suggested)
- Gather emergency supplies including water, fire, shelter, knife, first aid kit, meds, tarp, charcoal, and water (suggested)
- Ensure access to gas for evacuation (suggested)
- Prepare to outlast normal period if sheltering in place (suggested)

# Whats missing in summary

Importance of charging electrical devices for emergencies

# Tags

#HurricanePreparedness #SupplyChainStress #EmergencyPreparedness #EvacuationPlanning #CommunitySafety