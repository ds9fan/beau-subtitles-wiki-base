# Bits

Beau says:

- Compares viewing events in other countries to using a mirror instead of a cartoon villain lens.
- Points out similarities between actions taken by the United States and actions condemned in other countries.
- Mentions Russia's invasion of Ukraine as an example.
- Talks about holding up a mirror through videos to show the reflection of actions.
- Emphasizes subtlety in conveying the message so that truth is realized rather than told.
- Expresses the challenge of getting the people behind US actions to admit the similarities.
- References former President George W. Bush's slip-up in mentioning Iraq instead of Ukraine, hinting at a Freudian slip.
- Notes the significance of Bush's slip in reflecting the mirror image.
- Draws parallels between propaganda tactics used by different countries to manufacture consent for wars.
- Stresses the repetition of similar stories and justifications in different conflicts due to their effectiveness in manipulating public opinion.

# Quotes

- "Truth isn't told, it's realized."
- "The aggressing country, the invading country, they have to invade that other country because there's that really bad group."
- "Truth is always the first casualty of war."
- "His mistake probably far more effective than any video that I could put together."
- "But it might change the next one."

# Oneliner

Beau compares international actions to a mirror, reflecting on the similarities and propaganda tactics used to manufacture consent for wars.

# Audience

Global citizens

# On-the-ground actions from transcript

- Share George W. Bush's slip-up video to raise awareness (suggested)
- Encourage others to watch and share the video to prompt realization (suggested)

# Whats missing in summary

Deeper insights into the manipulation of public opinion through propaganda and the importance of recognizing patterns in international conflicts.

# Tags

#USActions #Propaganda #ManufacturingConsent #Wars #PublicOpinion