# Bits

Beau says:

- Reacts to a new idea of desks doubling as bulletproof shields for kids in schools.
- Contemplates the practicality and feasibility of the idea.
- Describes the potential materials and features of the detachable bulletproof shield desks.
- Acknowledges the need for creative solutions to protect children in schools.
- Expresses that this idea might be the best one he's heard that could actually be implemented.
- Quotes an activist's principle of being a realist and an idealist.
- Recognizes the current political reality regarding gun control measures.
- Emphasizes the necessity for actionable and creative ideas to address school safety.
- Rejects the notion of turning every child into a warrior for their own protection.
- Calls for more innovative solutions to safeguard children.

# Quotes

- "We need creative ideas. We need to think in ways that can actually be enacted right now."
- "I refuse to believe that the best we can come up with is making sure that we turn every child into Ragnar Lodbrok just so they can survive elementary school."

# Oneliner

Beau contemplates a new idea of desks as bulletproof shields for kids, stressing the importance of creative yet feasible solutions for school safety.

# Audience

Activists, educators, parents

# On-the-ground actions from transcript

- Develop and propose practical, creative solutions for school safety (implied)
- Advocate for innovative approaches to protect children in schools (implied)

# Whats missing in summary

The full transcript provides a deeper exploration of the need for creative yet realistic solutions to enhance school safety and protect children effectively.