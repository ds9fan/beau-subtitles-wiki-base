# Bits

Beau says:

- Explains how civilians are getting the new rifle first, clarifying that gun companies develop designs based on Department of Defense criteria and rejected designs often end up in the civilian market.
- Details the reasons behind the Army changing rifles, including the need for increased range and power to counter body armor in engagements.
- Points out the potential impact of the new rifle on domestic incidents, especially in terms of defeating body armor and altering the dynamics of confrontations.
- Responds to a comment attempting to correct misconceptions about the term "AR" in AR-15 and provides historical context about weapon weights during World War II.
- Criticizes the blaming of gun violence on gun owners, questioning the lack of personal responsibility and endorsing violence within gun culture.
- Attributes America's decline to demonizing education and nostalgia for an idealized past, urging people to embrace progress and move forward.

# Quotes

- "The Army realized that during Afghanistan, engagements were testing the limits of the 5.56 round and AR platform."
- "Y'all have been saying guns don't kill people. People kill people. I'm blaming the people."
- "It's absolutely the gun crowd's fault."

# Oneliner

Beau explains the implications of the Army's new rifle, challenges misconceptions about gun culture, and addresses America's decline due to nostalgia and ignorance.

# Audience

Gun owners, policymakers, activists

# On-the-ground actions from transcript

- Contact local policymakers to advocate for responsible gun laws and regulations (suggested)
- Join community organizations working towards reducing gun violence (implied)

# Whats missing in summary

Deeper insights into the societal impact of weapon advancements and the importance of accountability in addressing gun violence.

# Tags

#GunControl #MilitaryTechnology #Misconceptions #Responsibility #Progress