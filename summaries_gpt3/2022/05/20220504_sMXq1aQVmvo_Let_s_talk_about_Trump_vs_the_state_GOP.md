# Bits

Beau says:

- Trump loyalists are aiming to control the Republican party at the state level by focusing on offices related to elections.
- Their attention is particularly on secretaries of state who didn't comply with Trump's request to find votes.
- Trump's move against state power structures faces resistance from states like Tennessee, North Carolina, Georgia, and now Nevada.
- In Nevada, despite Trump's endorsement, the state Republicans endorsed different candidates, showing a divide within the party.
- The state Republican parties are pushing back against Trump's influence by choosing candidates independently.
- Some view this resistance as a patriotic act against Trump's power grab, while others see it as political self-defense to maintain their own power.
- The ongoing struggle between the state GOP and Trump's machine is leading to resource depletion for both parties.
- Ohio's Republican candidate for governor successfully fought off three pro-Trump primary challengers.
- Despite media attention on certain Trump-endorsed candidates like Vance, overall, the state-level GOP seems to be conflicting with Trump's influence.
- The battle between the state GOP and Trump's faction is significant and continues to evolve.

# Quotes

- "They're making sure that they maintain their power, and that means undercutting Trump."
- "This struggle is sapping resources from both parties."
- "The state level GOP is at odds with the Trump machine."
- "It's just a thought."
- "Y'all have a good day."

# Oneliner

Trump loyalists target state GOP for control, facing resistance as state Republicans in various states push back, leading to a resource-draining struggle.

# Audience

Political observers

# On-the-ground actions from transcript

- Support state Republican parties in their efforts to maintain independence from Trump's influence (implied).
- Stay informed about the ongoing power struggles within the Republican party (implied).

# Whats missing in summary

Further insights into the potential long-term implications of this power struggle within the Republican party.

# Tags

#Trump #RepublicanParty #StateGOP #PowerStruggle #PoliticalResistance