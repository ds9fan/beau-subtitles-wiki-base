# Bits

Beau says:

- The Department of Defense (DOD) has launched various climate and environmentally friendly initiatives, aiming to reduce the carbon footprint by 50% and more.
- Politicians, backed by dirty energy companies, often criticize and mock these initiatives, claiming they are impossible to achieve.
- A common criticism is the idea that electric vehicles won't outrun Chinese tanks, like a Prius versus a tank scenario.
- In Ukraine, Russian tank drivers are facing a new challenge from Ukrainian soldiers on e-bikes.
- Ukrainian soldiers use e-bikes, donated and modified by a private company, to set up ambushes against tanks effectively.
- E-bikes provide speed, quietness, and agility, making them more efficient for military operations compared to soldiers on foot.
- The real-life scenario in Ukraine showcases tanks running from electric vehicles, contrary to the skeptics' claims.
- Russia's invasion in Ukraine faced setbacks not due to Ukrainian resistance initially but because their vehicles ran out of gas.
- The future of military vehicles may involve producing their own energy or having convoy vehicles that generate energy for others.
- Those deeming things impossible should not hinder those actively working towards achieving them.

# Quotes

- "Those who say something is impossible should probably stop interrupting those who are doing it."
- "It's one of those things you can keep up or get left behind, but nobody really cares anymore."

# Oneliner

The Department of Defense is advancing environmentally friendly initiatives while critics question their feasibility; Ukraine showcases the effectiveness of e-bikes in military operations, debunking skeptics' claims.

# Audience

Environmental activists, military personnel

# On-the-ground actions from transcript

- Support environmentally friendly initiatives in your local military or community by advocating for sustainable practices (exemplified)
- Donate resources or funds to provide efficient modes of transportation for military or emergency response teams (exemplified)

# Whats missing in summary

The full transcript provides a deeper insight into the intersection of environmental sustainability and military operations, offering a unique perspective on the feasibility and effectiveness of cleaner technologies in challenging scenarios.

# Tags

#Military #Environment #Sustainability #Ukraine #ElectricVehicles