# Bits

Beau says:

- Distinguishes between SETI (passive) and METI (active) in the search for extraterrestrial intelligence.
- Explains that METI involves sending messages to extraterrestrial intelligence, while SETI involves listening for signs.
- Mentions two upcoming METI projects: one from China in 2023 and another on October 4 sending a message 39 light years away.
- Raises ethical questions about contacting extraterrestrial intelligence, including the risk of a clash of civilizations.
- Questions who speaks for humanity and makes decisions about sending messages to potential extraterrestrial life.
- Points out the lack of regulations in determining who can send messages to space, as it currently rests in the hands of unaccountable scientists.
- Draws parallels between unaccountable scientists in the space messaging field and those making decisions impacting the world today.
- Encourages reflection on who truly represents and speaks for individuals and the global population.
- Calls for a pause to ponder the accountability of decision-makers, both in the space messaging realm and on Earth.
- Ends with a thought-provoking message about accountability and representation in decision-making processes.

# Quotes

- "Who speaks for everybody? Who speaks for humanity?"
- "It's really just in the hands of scientists. They're not accountable to anybody."
- "Makes you think about all of the people who are not accountable to anybody, who are making decisions for the entire world here."
- "Who's speaking for you? Who's speaking for us today? Here."
- "Have a good day."

# Oneliner

Beau questions accountability and representation in decision-making processes, drawing parallels between unaccountable scientists in space messaging and decision-makers impacting the world today.

# Audience

Decision-makers, activists, thinkers

# On-the-ground actions from transcript

- Question accountability of decision-makers and demand transparency (implied)
- Advocate for regulations and oversight in decision-making processes (implied)

# Whats missing in summary

The full transcript provides a comprehensive exploration of the ethical implications of messaging extraterrestrial intelligence and prompts reflection on accountability and representation in decision-making processes.

# Tags

#SETI #METI #extraterrestrial #intelligence #accountability