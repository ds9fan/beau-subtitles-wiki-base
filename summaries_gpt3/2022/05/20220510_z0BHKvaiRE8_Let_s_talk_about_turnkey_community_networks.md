# Bits

Beau says:

- Emphasizes the importance of community networks, specifically turnkey networks that need activation or mutual assistance agreements.
- Mentions the example of the Republican Party using the Evangelical Church to push their agenda and how similar power structures exist on the progressive side.
- Suggests looking into nonprofits, newly unionized places, art galleries, gay clubs, liberal churches, student groups, environmental clubs, and more as potential allies.
- Points out the value of combining networks to create a stronger collective impact beyond just voting power.
- Recommends utilizing social media to connect with like-minded individuals and expand networks.
- Advises on the benefit of not starting with a network of close friends to maximize reach and impact.
- Encourages viewers to recognize existing power structures they can collaborate with or recruit from and teases more upcoming videos on the topic.

# Quotes

- "There already are power structures that you can ally with or recruit from."
- "When you start combining your network with others, that's when that block develops."
- "It's better if everybody involved isn't close friends to begin with."
- "You show up for them, they show up for you type of thing."
- "There are more of these videos coming."

# Oneliner

Beau stresses the importance of activating existing community networks and forming alliances to create a stronger collective impact for progressive causes.

# Audience

Community organizers, activists

# On-the-ground actions from transcript

- Join a nonprofit or community organization to connect with like-minded individuals (implied).
- Establish connections with newly unionized places for mutual support (implied).
- Collaborate with art galleries, gay clubs, liberal churches, student groups, environmental clubs, and other organizations to build a network (implied).
- Utilize social media to find and connect with potential allies (implied).
- Encourage diverse connections within networks to maximize reach and impact (implied).

# Whats missing in summary

Importance of building diverse and interconnected community networks for collective impact.

# Tags

#CommunityNetworks #Activism #Allies #Progressive #Collaboration