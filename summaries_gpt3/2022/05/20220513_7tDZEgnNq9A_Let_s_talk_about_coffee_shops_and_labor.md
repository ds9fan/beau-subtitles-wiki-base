# Bits

Beau says:

- The National Labor Relations Board (NLRB) filed a complaint against Starbucks in Buffalo, alleging 29 unfair business practices and 200 violations of the National Labor Relations Act.
- Starbucks was accused of trying to prevent unions from forming and putting pressure on them against the law.
- In Memphis, the NLRB filed a petition to reinstate seven employees due to Starbucks' conduct interfering with their federally protected rights.
- Kathleen McKinney from the NLRB emphasized the need for immediate relief to prevent harm to the campaign in Memphis and to ensure all Starbucks workers can freely exercise their labor rights.
- The power of unions lies in collective action, as demonstrated by big businesses' efforts to prevent their formation and the resources they invest in doing so.
- Companies go to great lengths, including hiring consultants, conducting surveillance, and holding mandatory meetings, to combat unionization because they recognize the threat unions pose to their control.
- The significant monetary investment made by companies to impede unions showcases the effectiveness and importance of collective action in improving working conditions.
- Unions represent power and protection for workers, leading to better conditions that companies wouldn't be concerned about if unions were ineffective or insignificant.
- The collective power of unions is substantial, with the ability to negotiate for better wages, benefits, and working conditions through unified action.
- The resistance and tactics employed by companies against unionization underscore the threat unions pose to the status quo and the transformative potential of collective organizing.

# Quotes

- "There is power in numbers. There's safety in numbers."
- "It represents power. Collective action will often get the goods."

# Oneliner

Starbucks faces allegations of unfair business practices and violations as unions fight for workers' rights – the power of collective action against corporate resistance.

# Audience

Workers, activists, advocates

# On-the-ground actions from transcript

- Support unionization efforts in your workplace to protect workers' rights and improve working conditions (exemplified)
- Educate fellow employees on the benefits and importance of collective action through union representation (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the challenges faced by workers in unionizing efforts and the significance of collective action in holding corporations accountable and improving labor conditions.