# Bits

Beau says:

- Explains the concept of dominoes falling in relation to systemic racism.
- Points out how systemic racism affects social safety nets, wages, and housing investments.
- Links tipping to a legacy of slavery and suppression of black wages.
- Connects objections to affordable housing to racism and voter motivations.
- Attributes the militarization of police to the war on drugs targeting specific communities.
- Describes the cycle of underfunding in schools due to racial disparities.
- Reveals how racism is used as a tool to manipulate white voters against their interests.
- Raises questions about airport security, war involvement, and public pool closures in the context of systemic racism.
- Stresses the need to dismantle systemic racism as the first step towards positive change.

# Quotes

- "The first step to getting to any of this is systemic racism."
- "We've got to move past that in this country."
- "As long as they're doing better than those other people, well, they'll keep voting that way."
- "It's so pervasive in this country that even something as simple as pools doesn't escape its grasp."
- "The first step to getting to any of this is systemic racism."

# Oneliner

Beau explains how dismantling systemic racism is the key to addressing various societal issues affecting white people.

# Audience

White Americans

# On-the-ground actions from transcript

- Research the history of tipping and its impact on racial wage disparities (suggested)
- Investigate the reasons behind objections to affordable housing in your community (suggested)
- Learn about the racial implications of the war on drugs and militarization of police (suggested)

# Whats missing in summary

Insights on the ripple effects of systemic racism on various aspects of society and how dismantling it can lead to positive change. 

# Tags

#SystemicRacism #SocialJustice #WhitePrivilege #CommunityAction #Equality