# Bits

Beau says:

- The Supreme Court decision makes it easier for representatives in Congress to be bought by wealthy donors by striking down a law limiting post-election campaign repayments.
- Politicians can now use their own money to fund their campaigns, win elections, and then accept repayments from donors.
- Donors can give money to elected officials after they win, essentially putting cash in their pockets in exchange for potential favors.
- The dissenting opinion warns about the heightened risk of corruption when politicians can personally benefit from post-election repayments.
- The law that was struck down already allowed for $250,000 post-election repayments, but it wasn't enough for politicians.
- Senator Ted Cruz brought the challenge to this law, paving the way for increased corruption in politics.
- The Supreme Court's decision undermines efforts to keep money out of politics and ensures that representatives prioritize the wealthy over constituents.
- This decision is seen as a way for the political establishment to protect and enrich themselves at the expense of the country and its people.

# Quotes

- "The Supreme Court decision makes it easier for representatives in Congress to be bought by wealthy donors."
- "It is going to do everything it can to make sure the establishment can protect itself and can further enrich themselves at the expense of the country and the people."

# Oneliner

The Supreme Court's decision enables wealthy donors to buy influence in Congress by allowing post-election repayments, undermining efforts to keep money out of politics and prioritizing the interests of the political establishment over the people.

# Audience

Voters, Activists, Advocates

# On-the-ground actions from transcript

- Challenge corrupt campaign finance practices by supporting and advocating for campaign finance reform (suggested)
- Hold elected officials accountable by demanding transparency and accountability in their campaign finances (suggested)

# Whats missing in summary

The full transcript provides a detailed explanation of how the Supreme Court decision impacts political corruption and the influence of money in politics, offering insight into the potential consequences for democracy and the American people.

# Tags

#SupremeCourt #CampaignFinance #PoliticalCorruption #MoneyInPolitics #RepresentativeDemocracy