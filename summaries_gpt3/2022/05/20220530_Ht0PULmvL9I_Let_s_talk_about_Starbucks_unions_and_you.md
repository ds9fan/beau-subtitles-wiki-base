# Bits

Beau says:

- Unions are traditionally associated with blue-collar jobs like coal miners and electricians, but the service industry is seeing a rise in unionization.
- There's a misconception that unions only thrive in progressive areas, but recent events like Starbucks in Birmingham, Alabama, voting to unionize, challenge this idea.
- Unions are not limited to blue-collar workers; they are for any worker who needs representation in a hostile environment.
- The process of unionizing is not as difficult as it may seem, despite pressure from businesses against it.
- The National Labor Relations Board outlines the process for forming a union, including holding elections if 30% of workers express interest in unionizing.
- Businesses often resist unions, but collective bargaining through unions gives workers more power and representation.
- Unions are effective, as evidenced by the significant efforts businesses put into defeating them.
- If you're in a workplace where you're not treated well and lack necessary benefits, a union might be the solution.
- There are networks available to help workers navigate the unionization process, making it more accessible than perceived.
- Considering joining a union can empower workers and improve their working conditions.

# Quotes

- "Unions aren't just for blue-collar workers. Unions are for any worker where the boss is not really looking out for them."
- "Divided we beg, united we bargain."
- "If you're in a workplace and you're not treated well, a union might be the thing for you."
- "If unions weren't effective, if they didn't work, big business wouldn't spend so much money trying to defeat them."
- "There are networks that can help you do it."

# Oneliner

Unions are not just for blue-collar workers; they empower all workers in hostile environments, providing representation and collective bargaining power to improve working conditions and rights.

# Audience

Workers seeking empowerment

# On-the-ground actions from transcript

- Contact networks that assist with unionizing (suggested)
- Join a union or start the process of forming one (implied)

# Whats missing in summary

The full transcript provides detailed insights into the importance of unions, the process of unionizing, and how they empower workers in various industries.