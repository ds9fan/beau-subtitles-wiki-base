# Bits

Beau says:

- Russian troops in Ukraine looted a John Deere dealership, taking equipment back to Chechnya.
- John Deere equipment is not easily repairable by smaller operators due to technology.
- Smaller farms are opting for other companies over John Deere due to complex technology.
- The expensive equipment stolen by Russian troops is controlled via remote access by John Deere.
- Russian troops won't be able to use the high-ticket items they stole due to being locked out remotely.
- The looted equipment is worth $5 million, including harvesters worth a quarter million each.
- Only smaller tractors might be salvageable, while the expensive equipment is rendered useless.
- This act by troops violates the laws of armed conflict.
- The Russian forces' activity is easily traceable, even down to the farm in Grozny.
- Crime doesn't pay, even in a war zone.

# Quotes

- "Crime doesn't pay, not even in a war zone, I guess."
- "John Deere has locked them out of it via remote access."
- "Smaller farms going with other companies."
- "Russian forces' activity is easily traceable."
- "They had a bunch of scrap metal, maybe a few engine parts and tires."

# Oneliner

Russian troops in Ukraine loot John Deere dealership, discover they can't use the high-ticket items due to remote access lockout, rendering $5 million worth of equipment useless.

# Audience

Farmers, tech enthusiasts, activists

# On-the-ground actions from transcript

- Contact local authorities about potential thefts or suspicious activities in your area (suggested)
- Join or support organizations that work towards accountability for armed forces (suggested)

# Whats missing in summary

The full transcript provides additional context on the impact of technological advancements in farming equipment on smaller operators.

# Tags

#JohnDeere #RussianTroops #Looting #Technology #ArmedConflict