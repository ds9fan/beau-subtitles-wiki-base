# Bits

Bowie Gun says:

- Addresses the need for the US military to become more "woke" by having basic respect for others.
- Describes a Marine School for Intelligence where instructors were investigated for harassment and misogynistic behavior.
- Mentions inappropriate relationships, a chat room with misogynistic attitudes, and a general tone of acceptance towards such behavior.
- Points out the negative impact of this behavior on female students, potentially discouraging them from pursuing intelligence roles.
- Warns that male students may adopt misogynistic attitudes, leading to undervaluing their female counterparts.
- Emphasizes the danger in deploying individuals who underestimate their female counterparts due to misogyny.
- Notes that women can excel in intelligence roles as they are often underestimated and can access certain information more effectively than men.
- Criticizes the lack of adequate corrective action taken in the Marine School for Intelligence.
- Raises concerns about the dehumanizing nature of militarism and its implications for civilian casualties.
- Stresses the importance of proper intelligence in minimizing civilian losses during military operations.
- Calls for the military to address dehumanizing behaviors to prevent further civilian harm.

# Quotes

- "Because their counterparts who are women, well they'll be underutilized because they're undervalued, because they're underestimated, because of the misogynistic attitude."
- "How does that happen normally? In real life, it isn't, you know, the sergeant gets hit and then the lower enlisted just lose it on an entire village. That happens, but it's super rare."

# Oneliner

Bowie Gun stresses the need for the US military to combat misogyny and dehumanization to enhance intelligence capabilities and reduce civilian casualties.

# Audience

Military personnel, advocates for gender equality

# On-the-ground actions from transcript

- Advocate for gender sensitivity training in military institutions (suggested)
- Support initiatives that address misogyny and harassment in military training (exemplified)

# Whats missing in summary

The full transcript provides detailed insights into the harmful effects of misogyny and the importance of gender equality in intelligence training within the US military.

# Tags

#US military #Misogyny #Gender equality #Intelligence #Civilian casualties