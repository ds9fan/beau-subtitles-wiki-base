# Bits

Beau says:

- Addressing the controversy surrounding the shortage of baby formula and criticism towards Biden for not doing enough.
- Criticizing the idea of government intervention in directing corporations on what to do as a merger of state and corporate power.
- Drawing parallels between advocating for government control over corporations and fascism, mentioning Benito Mussolini and corporatism.
- Pointing out that members of Congress should be introducing legislation to address issues like the baby formula shortage instead of looking to the White House for solutions.
- Suggesting that breaking up monopolies to prevent widespread issues like a recall impacting half the country could be a solution.
- Expressing concern over the trend of people looking to the White House as the ultimate solution to all problems, including inflation.
- Questioning the actions of governors and members of Congress who are quick to ask what the White House is doing about inflation without taking steps within their own states to address the issue.
- Warning against the dangers of constantly relying on the White House to solve every problem, leading to excessive control by one person.

# Quotes

- "You're literally advocating for fascism just to throw that out there."
- "They're not supposed to be looking to the White House to have dear leader tell them what to do."
- "We have to stop looking to the White House to solve every single problem that comes along."
- "It's not a good system."
- "Y'all have a good day."

# Oneliner

Beau addresses the baby formula shortage controversy, criticizes government intervention as a merger of state and corporate power, and warns against excessive reliance on the White House for solutions.

# Audience

American citizens

# On-the-ground actions from transcript

- Advocate for legislation to address issues like the baby formula shortage (implied).
- Support efforts to break up monopolies that control significant portions of the market (implied).
- Take steps within your own state to help with issues like inflation, such as reducing gas taxes (implied).

# Whats missing in summary

The nuances and detailed explanations provided by Beau in the full transcript.

# Tags

#BabyFormulaShortage #GovernmentIntervention #CorporatePower #BreakUpMonopolies #WhiteHouseReliance