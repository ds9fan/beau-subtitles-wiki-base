# Bits

Beau says:

- Contrasting the citizen response to climate change in Canada and the United States.
- Canadian government crowdsourcing plans to deal with climate change.
- Asking citizens for suggestions on how to make Canada more resilient to climate change impacts.
- Categories for suggestions include improving natural environment, disaster resilience, health, and managing infrastructure.
- Canada using a "whole of Canada approach" to tackle climate change.
- Shift in focus from stopping climate change to mitigating its impacts.
- Deadline for submitting ideas is in July of this year.
- Comparing the approach in Canada to the situation in the United States.
- Mentioning the Mississippi River as natural infrastructure.
- Urging for quicker action due to the urgent nature of climate change.

# Quotes

- "Countries are going to start paying more attention, and they're going to come up with various ways."
- "There has been a pretty marked shift in what's being talked about, and it's shifting away from stopping climate change."
- "Canada is using what it is calling a whole of Canada approach."
- "Americans will always do the right thing as soon as we try everything else first."
- "Maybe we could just speed that along this time because we don't have a lot of time."

# Oneliner

The Canadian government crowdsources citizen suggestions to tackle climate change while the U.S. lags behind, urging for faster action.

# Audience

Climate activists, concerned citizens.

# On-the-ground actions from transcript

- Submit your ideas via letstalkadaptation.ca (suggested).
- Participate in crowdsourcing efforts for climate change adaptation (suggested).

# Whats missing in summary

More details on specific suggestions and ideas from citizens in Canada for climate change adaptation.

# Tags

#ClimateChange #Canada #CitizenEngagement #Adaptation #USPolicy