# Bits

Beau says:

- Social media inquiries led to the question of starting a community network after previous videos.
- The network Beau is part of incorporated the channel to supply shelters during the public health crisis.
- Leveraging the network helped in providing supplies for hospitals and clinics when the pandemic started.
- Under normal circumstances, acquiring supplies like gowns and masks for nurses requires significant effort.
- A community network is described as a versatile and effective way to bring people together to help the community.
- It's suggested to keep the idea of the network open-ended to encourage participation.
- The ideal size for a network is around 8 to 12 people to avoid personality conflicts.
- Allowing natural splits in the group can lead to more coverage and collaboration in the community.
- Bringing different social circles together is key to creating a network that can achieve positive outcomes.
- Identifying like-minded individuals willing to commit time is the first step in starting a community network.

# Quotes

- "A community network is a versatile and effective way to bring people together to help the community."
- "Bringing different social circles together is key to creating a network that can achieve positive outcomes."
- "If you're going to try to start doing this, your first step is to sit down and think about all of the people you know who might be like-minded."

# Oneliner

Beau explains the importance of community networks, focusing on inclusivity, versatility, and collaboration to achieve positive outcomes.

# Audience

Community organizers

# On-the-ground actions from transcript

- Identify like-minded individuals willing to commit time to start a community network (implied)
- Encourage open-ended participation in the network to foster collaboration and inclusivity (implied)

# Whats missing in summary

More details on developing larger networks, securing funding, and the role of electoral politics in community networks.

# Tags

#CommunityNetworks #Collaboration #Inclusivity #LocalAction #CommunityBuilding