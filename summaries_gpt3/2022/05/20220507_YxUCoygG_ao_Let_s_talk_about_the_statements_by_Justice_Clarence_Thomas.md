# Bits

Beau says:

- Questions Justice Clarence Thomas' statements and ideas presented in the Washington Post.
- Expresses confusion over the erosion of faith in American institutions.
- Questions which institutions are being referred to, mentioning the presidency, Congress, and the Supreme Court.
- Criticizes the Supreme Court for potentially stripping rights from half the country for partisan and religious reasons.
- Raises concerns about justices on the Supreme Court potentially lying to secure their positions.
- Argues that Americans may be losing faith in institutions due to catering to a smaller group of people.
- Criticizes the idea of protests being seen as bullying institutions rather than exercising First Amendment rights.
- Expresses skepticism about maintaining faith in institutions given recent actions undermining trust.
- States that the US may be heading towards a long road of regaining lost rights.
- Questions the logic behind reversing freedoms and limiting rights that have been in place for years.

# Quotes

- "I don't understand why something like that would be written."
- "It's not that I think he's wrong. It's that I don't understand why he's surprised."

# Oneliner

Beau questions Justice Clarence Thomas' statements, expressing concerns about the erosion of faith in American institutions and the potential loss of rights, urging for a reevaluation of recent actions.

# Audience

American citizens

# On-the-ground actions from transcript

- Challenge actions undermining trust in institutions (suggested)
- Advocate for maintaining and expanding rights for all (implied)

# Whats missing in summary

Insight into Beau's perspective and analysis 

# Tags

#Justice #AmericanInstitutions #Faith #Rights #Government