# Bits

Beau says:

- Analyzes Madison Cawthorn's statement and the narrative building around it.
- Talks about information consumption and shaping narratives in the media.
- Mentions the habit of fitting new information into existing narratives.
- Suggests using a method to filter out narrative building and access raw information.
- Encourages looking into the origins of terms before narrative building began.
- Explains how to use the date filter option on search engines to access raw information.
- Links a Newsweek article about "Darkmaga" and describes it as fascism.
- Criticizes the rebranding and insertion of authoritarian ideologies into conservative politics.
- Emphasizes the importance of calling out fascism when it is present, without downplaying it.
- Raises awareness about the implications of certain political movements, including fascism.
- Points out the significance of recognizing and labeling authoritarian governance attempts.
- Encourages viewers to analyze meme movements and understand their underlying messages.

# Quotes

- "The time for, I'm assuming this means Gentile, but that's not what he typed, politics as usual has come to an end."
- "This isn't something that the media should try to downplay because it is very open."
- "When you see this, just call it what it is. It's fascism."

# Oneliner

Beau dissects Madison Cawthorn's statement, urges raw information consumption, and exposes fascist undertones in "Darkmaga".

# Audience

Media Consumers

# On-the-ground actions from transcript

- Read the Newsweek article about Darkmaga and understand its implications (suggested).
- Analyze meme movements to comprehend their underlying messages (suggested).
- Call out authoritarian ideologies and fascism when identified (exemplified).

# Whats missing in summary

In-depth analysis of Madison Cawthorn's statement and the media's role in shaping narratives.