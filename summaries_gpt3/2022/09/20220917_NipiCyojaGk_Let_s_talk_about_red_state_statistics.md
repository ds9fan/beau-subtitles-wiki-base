# Bits

Beau says:

- Exploring narratives and information consumption in response to a video on a particular topic.
- Addressing the challenge of breaking free from established narratives, even when faced with contrary evidence.
- Critiquing the misconception about Democrats allowing violent criminals on the streets.
- Pointing out the high murder rates in states that voted for Trump in 2020.
- Providing statistics that challenge the narrative of Democrat-run cities being hubs of violence and murder.
- Mentioning specific cities and states with high crime rates that voted Republican.
- Emphasizing how certain narratives are perpetuated by media outlets to influence beliefs.
- Describing how people become emotionally invested in false narratives and resist conflicting information.
- Criticizing the tendency to hold on to misinformation despite it being easily debunked.
- Concluding with a reminder not to believe everything presented by certain outlets and to think critically about information consumption.

# Quotes

- "Democrat run cities are cesspools of violence and murder."
- "It's a narrative. They repeat it often enough. You believe it."
- "You have an emotional reaction about it. You get angry because now that piece of information doesn't fit with your narrative."

# Oneliner

Beau challenges misconceptions about crime rates in Democrat-run cities and urges critical thinking in information consumption.

# Audience

Information consumers

# On-the-ground actions from transcript

- Fact-check information from media outlets (suggested)
- Challenge narratives with data and statistics (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of how narratives are constructed and perpetuated, urging viewers to critically analyze the information they consume.

# Tags

#Narratives #InformationConsumption #CrimeRates #MediaBias #CriticalThinking