# Bits

Beau says:

- Putin ordered a partial mobilization of around 300,000 Russian reservists, who are not equivalent to American reservists but amateurs forced into military service.
- Russian reservists are being sent to face combat-hardened Ukrainian military, which is likely to end poorly due to their lack of training, equipment, and morale.
- Demonstrations and protests have broken out across Russia against Putin's decision, showing cracks in the facade of domestic tranquility.
- Efforts to insulate the civilian population and maintain economic stability in Russia are failing, leading to domestic turmoil.
- The price of plane tickets out of Russia has skyrocketed as people try to flee being forced into a conflict they have no desire to partake in.
- The best chance for a Russian reservist is to break away from their unit and turn themselves into the Ukrainians upon arrival.
- The war in Ukraine is deemed lost, with the political objectives failing and the conflict being about imperialism and ego rather than ideals worth fighting for.
- Continued protests, desertion among reservists, and low morale are expected consequences of Putin's decision for a partial mobilization.
- A full mobilization may not significantly change things on the battlefield and may result in unnecessary waste of lives and resources.
- The Ukrainian military's ability to disrupt movements behind the lines may prevent many reservists from even reaching the front lines.

# Quotes

- "This is not something that is likely to turn the tide there, despite the numbers involved, because it's quantity and quality type of thing."
- "At the end of this, what you have is another rich man sending a whole bunch of poor boys off to fight his war."
- "The war is lost. The political objectives of the war were lost as soon as other countries decided it was better for them to join NATO."
- "The cracks in the facade are showing."
- "The greatest chance of making it for a Russian reservist is to immediately, upon arrival, break away from their unit and turn themselves into the Ukrainians."

# Oneliner

Putin's order for a partial mobilization of Russian reservists reveals cracks in the facade of domestic tranquility and leads to protests, economic instability, and a rush for plane tickets out of the country.

# Audience

Activists, Peace Advocates, Humanitarians

# On-the-ground actions from transcript

- Join protests against Putin's decision for a partial mobilization (exemplified).
- Support efforts to aid Russian reservists who may seek to desert and avoid forced combat (exemplified).
- Provide assistance to those trying to leave Russia to avoid involvement in the conflict (exemplified).

# Whats missing in summary

Insights on the potential long-term effects of Putin's decisions on Russia's domestic situation.

# Tags

#Russia #Putin #Reservists #Ukraine #Protests #Imperialism