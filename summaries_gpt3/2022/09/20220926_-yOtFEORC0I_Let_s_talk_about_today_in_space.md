# Bits

Beau says:

- NASA is conducting the Double Asteroid Redirection Test today, where a spacecraft will be slammed into an asteroid moving at 15,000 miles per hour to adjust its orbit as a proof of concept for a planetary defense system.
- The impact is scheduled for 7:14 PM Eastern, with NASA starting broadcasting on NASA TV at 6 PM.
- This test is purely a proof of concept, not in response to an immediate threat, to ensure effectiveness in the future if needed.
- The spacecraft will deploy a smaller camera bird before impact to film the event, flying within 25-50 miles of the asteroid.
- The impact and any resulting changes in the asteroid's orbit will be observable from Earth.
- Despite having about 100 years before any potential asteroid threat, not all objects are cataloged, leaving room for unknown objects in space.
- Beau mentions sending birthday wishes to one of his favorite rabbis and cheers for the Gators.
- Viewers can watch the test on NASA TV, with the impact scheduled for 7:14 PM Eastern.

# Quotes

- "Today is the big day. The Double Asteroid Redirection Test."
- "You can watch it, because prior to impact, it will deploy a smaller camera bird."
- "This is just to see if they can do it."

# Oneliner

NASA conducts the Double Asteroid Redirection Test, slamming a spacecraft into an asteroid at 15,000 mph to adjust its orbit as a planetary defense proof of concept, viewable on NASA TV at 7:14 PM Eastern.

# Audience

Space enthusiasts

# On-the-ground actions from transcript

- Watch the Double Asteroid Redirection Test on NASA TV at 7:14 PM Eastern (suggested).

# Whats missing in summary

Details on the potential impact of the test and the significance of planetary defense systems.

# Tags

#Space #NASA #Asteroid #PlanetaryDefense #NASA_TV