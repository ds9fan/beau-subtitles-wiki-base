# Bits

Beau says:

- Recounts a 2017 incident in Philadelphia involving a 25-year-old man, Dennis Plowden, who was shot by Officer Eric Rook.
- Rook fired at Plowden within six seconds of arriving on the scene, hitting Plowden in the hand and head. Plowden was unarmed.
- Officer Rook was recently found guilty of voluntary manslaughter, becoming the third cop charged with similar offences by District Attorney Larry Krasner.
- Rook claimed he was scared because he didn't have cover unlike the other officers who did.
- Beau stresses the importance of time, distance, and cover in police tactics to prevent unnecessary shootings.
- Beau argues that if these basic principles were followed, Plowden might still be alive.
- Officers often receive training on best practices but may disregard them when out on the streets based on the guidance of their training officer.
- Beau points out the discrepancy between training and real-world practices that can lead to fatal consequences.
- The sentencing for Officer Rook is scheduled for November 17th, with Beau expressing uncertainty about the potential outcome.
- Beau concludes by urging for adherence to proper training protocols to avoid tragic incidents like the one involving Dennis Plowden.

# Quotes

- "Time, distance, and cover."
- "Those rules, those tactics, those best policies, whatever your department calls them, they're there for a reason. They work."
- "One of them, one way, saves lives. One of them ends up with you in a cage."

# Oneliner

Beau stresses the importance of police officers following basic principles like time, distance, and cover to prevent unnecessary shootings and save lives.

# Audience

Police officers, law enforcement.

# On-the-ground actions from transcript

- Attend or follow the sentencing of Officer Eric Rook on November 17th (suggested).
- Advocate for proper training adherence and accountability in law enforcement practices (implied).

# Whats missing in summary

The full transcript provides detailed insights into a 2017 incident in Philadelphia and the importance of police officers following proper training protocols to prevent fatal shootings and uphold accountability.

# Tags

#PoliceTraining #Accountability #CommunityPolicing #Philadelphia #Justice #LawEnforcement