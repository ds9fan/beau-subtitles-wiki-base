# Bits

Beau says:

- Talks about trying to understand and relate to the other side's perspective, even if it's conflicting.
- Mentions how people in Russia only mobilized against the war when they faced the risk of being drafted.
- Acknowledges that the same behavior can be seen in the United States, where people may not protest against injustices until it directly affects them.
- Expresses sympathy for regular people being forced into war due to the actions of those in power.
- Points out the increased likelihood of atrocities when scared and untrained individuals are forced into conflict.
- Encourages empathy by putting oneself in others' shoes and recognizing one's own past actions or inactions in similar situations.

# Quotes

- "People care about the pebble in their shoe, not the rock slide 1,000 miles away."
- "I don't want anybody else to be thrown into a cauldron of war and strife for some rich guy's ego."
- "This move, it's a bad move. It's bad all the way around."
- "But it also doesn't mean it's uncommon."
- "Y'all have a good day."

# Oneliner

Beau talks about understanding conflicting perspectives, mobilization against war, and the human tendency to act only when directly impacted, urging empathy and reflection on past behaviors.

# Audience

Global citizens

# On-the-ground actions from transcript

- Put yourself in others' shoes and empathize with their perspectives (implied).
- Recognize past behaviors or inactions and strive to be better (implied).

# Whats missing in summary

The full transcript provides a deeper reflection on human nature, empathy, and the impact of conflicts on ordinary people, urging individuals to learn from past behaviors and strive for better understanding and action.

# Tags

#Empathy #Conflict #HumanNature #GlobalCitizens #Reflection