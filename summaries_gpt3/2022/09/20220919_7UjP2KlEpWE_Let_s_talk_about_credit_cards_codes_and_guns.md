# Bits

Beau says:

- The gun industry will now have its own merchant code when purchasing with credit cards from Visa, MasterCard, and American Express.
- Pro-gun advocates fear mass surveillance, tracking, and politically motivated monitoring with this new merchant code system.
- The gun industry not having its own merchant code until now is what's unusual, considering the level of detail in merchant codes.
- There is no new information being tracked with the introduction of the gun industry's merchant code; the information was already available with credit card purchases.
- Gun control advocates believe the merchant code could help identify suspicious gun purchases, but Beau argues that typical purchases like a rifle, magazines, and ammo may lead to false alarms.
- Beau believes that law enforcement action based on flagged gun purchases is unlikely to result in anything significant, as legal gun ownership is not a crime.
- The limited potential benefit of the new merchant code system lies in tracking purchases between areas with differing gun laws to prevent illegal sales across jurisdictions.
- Overall, Beau sees the new merchant code as more of a talking point and a political move rather than a practical tool for tracking gun purchases effectively.
- The new merchant code is not expected to be a useful tool for law enforcement or a form of mass surveillance, contrary to some fears surrounding its implementation.

# Quotes

- "There's no new information that's really being gathered."
- "The fact that guns didn't exist in one of these already is what's weird."
- "It's not going to be a super useful tool."
- "It's a talking point coming out before elections."
- "It's not going to be Big Brother watching you."

# Oneliner

Beau breaks down the new gun industry merchant code, debunking fears of surveillance and tracking while questioning its actual utility for law enforcement and gun control efforts.

# Audience

Policy advocates

# On-the-ground actions from transcript

- Educate policymakers on the limited practical use of the new gun industry merchant code (implied).

# Whats missing in summary

Further details on the implications and limitations of the new gun industry merchant code can be better understood by watching the full transcript.

# Tags

#GunIndustry #MerchantCodes #CreditCardPurchases #GunControl #LawEnforcement