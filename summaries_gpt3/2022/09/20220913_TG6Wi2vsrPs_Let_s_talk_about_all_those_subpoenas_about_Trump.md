# Bits

Beau says:

- Department of Justice sent out 30 to 40 subpoenas last week, targeting a range of individuals.
- Subpoenas request broad information on post-election actions of the Trump administration.
- Recipients were compelled to appear before a grand jury and had their phones searched.
- Information requested includes communications with Trump allies like Giuliani and Powell.
- Recipients were asked to provide information on any members of the executive or legislative branches involved in obstructing the election certification.
- Senate staffers might be feeling nervous due to the wide-reaching subpoenas.
- The investigation keeps expanding due to the complex nature of the case.
- Staffers may have access to significant information due to their roles.
- Seizing phones indicates the seriousness of the investigation.
- The case seems to be targeting not only Trump's inner circle but also other political figures involved in altering election outcomes.

# Quotes

- "There's a seditious underground parking garage that they need to go through next."
- "Some of these people, they're staffers, and they are gonna have access to a lot more information than people are gonna give them credit for."
- "Taking the phones, that's pretty significant."
- "They are kind of building a case that appears now to be actively targeting not just people that are seen as Trump's inner circle, but possibly other political figures."
- "There will probably be more information flowing out pretty regularly between now and September 23rd."

# Oneliner

The Department of Justice's wide-reaching subpoenas target post-election actions, potentially implicating Trump allies and political figures, with a focus on obstruction.

# Audience

Journalists, Activists, Legal Observers

# On-the-ground actions from transcript

- Stay updated on developments in the investigation and share information with your community (suggested).
- Support transparency and accountability in legal proceedings by discussing the implications of these subpoenas with others (suggested).

# Whats missing in summary

Insights into the potential implications on the political landscape and future accountability measures.

# Tags

#DOJ #Subpoenas #TrumpAdministration #ElectionObstruction #LegalProceedings