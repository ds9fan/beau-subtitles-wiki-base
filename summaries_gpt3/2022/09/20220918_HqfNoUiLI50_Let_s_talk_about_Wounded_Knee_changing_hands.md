# Bits

Beau says:

- The Oglala and Cheyenne River Sioux are purchasing 40 acres of land around the national historical landmark for wounded knee.
- The land purchase is significant because in 1890, almost 300 Lakota people were massacred at this location.
- In 1973, there was a 71-day standoff between the American Indian movement and the government at the same location.
- The purchase allows them to preserve the land and potentially turn it into an educational center.
- Manny Ironhawk expresses the significance of the ghost dance to their people and the duty they carry on at Wounded Knee.
- The misunderstanding of the ghost dance contributed to the tragic events in 1890.
- The land, previously occupied by a trading post, will now be owned by the Sioux tribes.
- It is hoped that the land will serve as a place for people to learn about the history from the perspective of the Lakota people.

# Quotes

- "The ghost dance, the misunderstanding of what they were seeing is one of the contributing factors to what happened in 1890."
- "Today, we are the new ghost dancers, and we carry on a duty that came to us to do what we can for our relatives there at Wounded Knee."

# Oneliner

The Oglala and Cheyenne River Sioux purchase land at Wounded Knee, aiming to preserve history and create an educational center, continuing their duty as "new ghost dancers." 

# Audience

History enthusiasts, Indigenous rights advocates

# On-the-ground actions from transcript

- Support educational initiatives on Native American history and culture (implied)
- Learn about and spread awareness of the significance of Wounded Knee (implied)
- Respect and honor Native American perspectives on historical events (implied)

# Whats missing in summary

The emotional depth and cultural significance conveyed by Beau in discussing the historical importance of the land purchase at Wounded Knee. 

# Tags

#IndigenousRights #History #Education #NativeAmerican #LandPurchase