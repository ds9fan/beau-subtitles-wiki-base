# Bits

Beau says:

- Republicans want to stall on bringing the vote on marriage equality because they perceive it as complex.
- Democrats aim to bring the vote on marriage equality to the floor on Monday.
- Republicans are bothered by this and claim they need more time to figure things out.
- Beau questions how much time is needed to decide on people's basic rights.
- Republicans may be stalling until after the midterms to vote against marriage equality.
- Republicans want to vote it down but are hesitant when voters have a voice.
- Some Republicans argue they need more time to work out finer details in the text before voting.
- Beau suggests that Senators should put in extra effort considering the importance of the rights of 20 million Americans.
- The issue boils down to Republicans trying to figure out who is vulnerable to appeal to their base.
- Beau urges Democrats to hold the vote on Monday, forcing Republicans to decide on granting rights to 20 million Americans.

# Quotes

- "Exactly how much time do you need to decide whether or not people should have basic rights?"
- "If a senator is afraid of making a vote, of going on the record about something this simple just prior to an election, it's because they know the decision they would make, the vote that they cast, is unpopular."
- "Either they vote in favor of giving rights to 20 million Americans or they vote against it."

# Oneliner

Republicans stall on marriage equality vote, aiming to push it past midterms; Democrats urged to hold the vote, forcing a decision on basic rights for 20 million Americans.

# Audience

Democrats, Activists, Voters

# On-the-ground actions from transcript

- Hold Senators accountable for their stalling tactics and urge them to vote on marriage equality promptly (suggested).
- Advocate for timely decision-making on significant issues that affect millions of Americans (implied).

# Whats missing in summary

Nuances of Beau's passionate delivery and the urgency for swift action on granting basic rights to all Americans.

# Tags

#MarriageEquality #DemocraticParty #RepublicanParty #BasicRights #Voting #Accountability