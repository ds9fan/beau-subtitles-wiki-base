# Bits

Beau says:

- Talks about the importance of document-based journalism in providing context and revealing information.
- Mentions a Politico article containing interesting numbers that should concern the Republican Party.
- Compares the number of online donors for the Republican and Democratic parties from the last half of 2022 to the first half of 2022.
- Republican Party had a loss of 43,000 online donors from 2021 to 2022.
- Democratic Party saw a substantial increase in online donors during the same period.
- Democratic Party's enthusiasm supposedly increased significantly after a specific Supreme Court decision in June.
- The surge in Democratic online donors might include unlikely voters, potentially affecting polling accuracy.
- Indicates the need for political strategists to pay attention to these trends.

# Quotes
- "These additional 600,000 online donors for the Democratic Party, those very well may be those unlikely voters that we've talked about as far as things that might cause polling to be a little off."
- "That Supreme court decision, that's what people see as a motivating factor."
  
# Oneliner
Beau breaks down online donor numbers revealing Democratic Party's surge and Republican Party's loss; a Supreme Court decision seems to be a significant motivator.

# Audience
Political analysts, strategists

# On-the-ground actions from transcript
- Analyze political trends and donor data for strategic insights (implied)

# Whats missing in summary
Analysis of the potential impact of online donor numbers on political strategies and future elections.

# Tags
#Document-based Journalism #Online Donors #Republican Party #Democratic Party #Political Trends #Supreme Court Decision