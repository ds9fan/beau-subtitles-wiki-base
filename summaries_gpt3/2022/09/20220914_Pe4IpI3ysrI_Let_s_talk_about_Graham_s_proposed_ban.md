# Bits

Beau says:
- Lindsey Graham's proposal of a 15-week abortion ban is seen as surprising in light of current political trends.
- National politicians like Lindsey Graham are distancing themselves from extreme state bans with this proposal as a compromise.
- The 15-week ban is positioned as a way to appeal to moderates and independents while still supporting abortion restrictions.
- Graham's proposal, although framed as a compromise, is criticized for compromising on women's bodily autonomy.
- The goal behind the proposal is to allow Republican senators to cater to pro-ban supporters while maintaining an image of reasonability.
- Beau does not view this compromise on women's rights as acceptable or politically viable.
- He sees the proposal as a token gesture to find middle ground, which he believes is untenable.
- Beau criticizes the Republicans for being out of touch with the average American on this issue.

# Quotes

- "You showed us who you were. We believe you. There's no compromise to be had here."
- "We're not going to compromise or negotiate on women's bodily autonomy."
- "I don't see any compromise to be had here."
- "It just illustrates exactly how out of touch Republicans are with the average American."
- "I don't see this move as politically tenable."

# Oneliner

Lindsey Graham's 15-week abortion ban proposal is seen as a calculated compromise that compromises women's rights, illustrating the GOP's disconnect with Americans.

# Audience

Advocates for women's rights

# On-the-ground actions from transcript
- Advocate for women's bodily autonomy and reproductive rights (implied)
- Support organizations that work towards protecting women's rights (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Lindsey Graham's proposal and its implications on women's rights and political positioning.

# Tags

#LindseyGraham #AbortionBan #ReproductiveRights #GOP #WomenRights