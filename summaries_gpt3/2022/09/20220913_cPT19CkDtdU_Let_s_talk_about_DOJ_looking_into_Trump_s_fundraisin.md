# Bits

Beau says:

- Department of Justice grand jury in DC is now looking into potential fraud regarding Trump's fundraising activities post-election.
- Subpoenas have been issued to high-profile individuals, with one signed by a federal prosecutor specializing in fraud.
- Allegations surfaced during the January 6th hearings regarding Trump fundraising for the Official Election Defense Fund.
- Reporting suggests the fund raised around a quarter of a billion dollars, but there are discrepancies in the reported amounts.
- Money raised may have gone into Trump's Save America PAC, which poses issues due to the loose regulations surrounding PACs.
- Federal investigators are likely examining whether funds raised for a specific purpose were diverted elsewhere for personal gain.
- Similar financial discrepancies led to Steve Bannon's arrest, though this case is separate from the New York investigation.
- The complex web of investigations surrounding Trump's actions has led to Beau needing a whiteboard to keep track.
- Speculation points to the Department of Justice focusing on potential financial crimes based on the subpoenas and previous hearings.
- While reports indicate ongoing investigations, definitive details on the specific crimes being scrutinized remain unclear.

# Quotes

- "It's worth noting that the money that was raised was not spent on what the feds are looking into at this point."
- "I literally have a whiteboard now divided up by the various investigations and the various jurisdictions, just so I can figure out which piece of information goes with which case."

# Oneliner

Department of Justice grand jury in DC investigates potential fraud in Trump's post-election fundraising, raising concerns about diversion of funds for personal gain.

# Audience

Legal watchdogs, investigators, concerned citizens

# On-the-ground actions from transcript

- Monitor updates on the investigation closely (implied)
- Stay informed about the potential financial crimes being investigated (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the Department of Justice's inquiry into potential fraud surrounding Trump's fundraising post-election, shedding light on the complex web of financial discrepancies and investigations.

# Tags

#DepartmentOfJustice #Investigation #Trump #Fundraising #FinancialCrimes