# Bits

Beau says:

- Story from Otero County, New Mexico involving a county commissioner and the 14th Amendment to the U.S. Constitution.
- Commissioner Coy Griffin being removed from office for actions on January 6th at the Capitol.
- Griffin, founder of Cowboys for Trump, involved in refusing to certify election results.
- 14th Amendment prohibits holding office if engaged in insurrection against the U.S. Constitution.
- Judge ruled Griffin incited violence on January 6th, contributing to delaying election certification.
- Courts will need to decide if January 6th events constitute insurrection or rebellion under the Constitution.
- Likely appeal in Griffin's case, potentially impacting future similar cases.
- Griffin's charges currently minor, with a jail sentence of around 14 days served.
- Case could set a precedent for future cases and raise political attention.
- Possibility of courts ruling rhetoric leading to violence as aiding and comforting.

# Quotes

- "No person shall be a Senator or Representative in Congress, or Elector of President and Vice President, or hold any office, civil or military, under the United States or under any State, who having previously taken an oath as a Member of Congress, or as an Officer of the United States, or as a Member of any State Legislature, or as an Executive or Judicial Officer of any state to support the Constitution of the United States shall have engaged in insurrection or rebellion against the same, or given aid or comfort to enemies thereof."
- "Griffin incited, encouraged, and helped to normalize the violence on January 6th."
- "This case could set the tone for other cases later and it's definitely something we need to keep an eye on."

# Oneliner

A county commissioner faces removal under the 14th Amendment for his actions on January 6th, potentially setting a precedent for future cases and attracting political attention.

# Audience

Legal enthusiasts, political analysts

# On-the-ground actions from transcript

- Keep an eye on the legal proceedings and outcomes surrounding cases related to the January 6th events (implied).

# Whats missing in summary

Deeper analysis on the potential implications of this case on future interpretations of the 14th Amendment and its application in similar scenarios.

# Tags

#OteroCounty #14thAmendment #CoyGriffin #PoliticalJustice #LegalSystem