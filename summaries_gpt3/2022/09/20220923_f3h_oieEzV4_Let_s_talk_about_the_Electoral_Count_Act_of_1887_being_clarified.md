# Bits

Beau says:

- The Electoral Count Act of 1887 has come under scrutiny due to a misreading that suggested the vice president could disregard votes.
- Both the House and the Senate are taking steps to revise the act to prevent potential coup attempts.
- The House has passed a version that requires one-third of House members to sign off on objections to certifying electors, among other clarifications.
- The revised act clarifies the ceremonial role of the Vice President and requires governors to transmit electors chosen by popular vote.
- Despite being straightforward clarifications, only nine Republicans in the House supported the revision, while others may be reserving the right to support a coup in the future.
- The Senate is also working on a comparable bill with support from 10 Republicans, which is likely to pass.
- The necessity for such clarifications is concerning, as it should have been common knowledge that the vice president couldn't unilaterally discard votes.
- The lack of overwhelming support for this revision from those who claim to respect the country's foundational principles is disheartening.

# Quotes

- "The fact that the legislative body of the United States is having to explicitly say the vice president can't just throw out the votes and do what they want, that's appalling."
- "People who claim to respect a constitutional republic, a representative democracy, the general ideas that this country was founded on, this should have breezed through with full support."
- "There's nothing controversial in this."

# Oneliner

The Electoral Count Act of 1887 is being revised to prevent potential coup attempts, with clarifications on objections and the ceremonial role of the Vice President, but lack of overwhelming support is disheartening.

# Audience

Legislative watchers

# On-the-ground actions from transcript

- Contact your representatives to express support for clarifying and revising the Electoral Count Act (exemplified)
- Stay informed on the progress of the comparable Senate bill and advocate for its passage (exemplified)

# Whats missing in summary

Importance of staying vigilant to protect democratic processes and prevent potential misuse of power.

# Tags

#ElectoralCountAct #Revision #CoupPrevention #House #Senate