# Bits

Beau says:

- Introduces the topic of the man known as "the man of the hole" or the "loneliest man in the world."
- Describes how the man lived in the Amazon alone for 25 years after his community was attacked by cattle ranchers.
- Mentions the last attack on his community in 1995, when he became the sole survivor and eventually died.
- Points out the lack of knowledge about the man's community and customs due to failed contact attempts.
- Quotes Fiona Watson from Survival International about the deliberate genocide of the man's people by cattle ranchers for land and wealth.
- Emphasizes the violence and cruelty inflicted on indigenous peoples globally in the name of colonization.
- Talks about how the man resisted contact and just wanted to be left alone.
- Mentions Survival International's advocacy for uncontacted or recently contacted indigenous groups worldwide.
- Points out that similar situations exist globally, not just in the past.
- Addresses ongoing cultural assimilation and inequitable treatment of indigenous communities in the US and worldwide.

# Quotes

- "The deliberate wiping out of an entire people by cattle ranchers hungry for land and wealth."
- "Resistance. We can only imagine what horrors he had witnessed in his life and the loneliness of his existence after the rest of his tribe were killed."
- "It's happening now."
- "The loss to humanity of these cultures, it's still something that's occurring."
- "This type of fight, it's not over and it won't be over for a very very long time."

# Oneliner

Beau talks about the loneliest man in the world, a victim of genocide by cattle ranchers, shedding light on ongoing global struggles faced by indigenous communities.

# Audience

Advocates, activists, community members

# On-the-ground actions from transcript

- Support organizations like Survival International that advocate for uncontacted or recently contacted indigenous groups (suggested)
- Educate yourself and others about the ongoing struggles faced by indigenous communities globally (implied)

# Whats missing in summary

The emotional depth and impact of the man's solitary existence and the continued fight for indigenous rights globally.

# Tags

#IndigenousRights #CulturalGenocide #SurvivalInternational #Amazon #GlobalAdvocacy