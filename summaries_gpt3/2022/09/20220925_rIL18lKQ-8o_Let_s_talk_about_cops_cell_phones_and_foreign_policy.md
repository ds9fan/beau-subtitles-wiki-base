# Bits

Beau says:

- Explains why he separates videos on what's happening from how it could be better in foreign policy.
- Stresses the importance of understanding the cold reality of foreign policy without notions of justice or morality.
- Compares the need for change in foreign policy to the drive for police accountability before cell phones were widespread.
- Draws parallels between the impact of cell phone footage on police accountability and the potential for real-time footage to change perceptions of war and foreign policy.
- Emphasizes that the availability of real-time footage from conflicts like Ukraine is shaping public perception and eliminating glorified views of war.
- Argues that major global events like climate change necessitate a shift towards more cooperative foreign policies.
- Points out that most countries engaging in international affairs exhibit similar behavior to the US.
- Clarifies that he is not always right, acknowledging his fallibility and urging against blind faith in his analysis.

# Quotes

- "Don't bring silly notions like justice or morality into this, because that doesn't have anything to do what we're talking about."
- "We're getting our cell phones. Those people who want a different foreign policy, we're getting that tool."
- "I am not always right. Human, flawed, make mistakes."

# Oneliner

Beau explains why he separates analysis of foreign policy realities from ideal solutions, stressing the need for a wake-up call on the cold truth, devoid of notions of justice or morality, while drawing parallels between the impact of cell phone footage on police accountability and the potential for real-time footage to reshape perceptions of war and foreign policy.

# Audience

Activists, Policy Makers

# On-the-ground actions from transcript

- Watch and share real-time footage from conflicts to raise awareness and shape perceptions (implied).
- Advocate for more cooperative foreign policies in light of global challenges (implied).

# Whats missing in summary

In-depth analysis and examples of the effects of real-time footage on public perception and its potential to drive policy change.

# Tags

#ForeignPolicy #RealTimeFootage #PerceptionChange #CooperativePolicies #GlobalChallenges