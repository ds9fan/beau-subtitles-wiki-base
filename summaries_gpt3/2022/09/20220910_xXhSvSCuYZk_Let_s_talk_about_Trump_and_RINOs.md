# Bits

Beau says:

- Explains the concept of a "rhino" and its application to Trump and other prominent Republicans like Barr, McConnell, Cheney, and Rove.
- Trump's history of switching political affiliations, starting as an independent in 2012, Democrat in 2009, and Reform Party member in 2001, contrasts with the long-standing Republican backgrounds of the others.
- Trump is characterized as an authoritarian rather than a traditional Republican, using the party's platform for his own agenda.
- The wedge driven into the Republican Party by the divide between small government conservatives and authoritarians is emphasized.
- Beau expresses his disagreement with figures like Barr, Rove, Cheney, and McConnell, acknowledging their Republican identity while criticizing Trump for not truly adhering to Republican principles.
- Trump's manipulation of rhetoric and base-building strategies is discussed, pointing out how he deviates from traditional Republican policies.
- The contrast between the Republican Party's small government conservatism rhetoric and Trump's authoritarian tendencies is outlined.

# Quotes

- "Trump is the rhino, certainly not Karl Rove."
- "Trump's not a Republican. He's an authoritarian."
- "He went and shopped the various political parties trying to find the one that was easiest to manipulate."
- "The rhino concept has driven a wedge in the Republican Party."
- "Their rhetoric was always about individualism and stuff like that."

# Oneliner

Beau explains the "rhino" concept, contrasting Trump's authoritarianism with traditional Republican values represented by figures like Barr, McConnell, Cheney, and Rove.

# Audience

Political observers

# On-the-ground actions from transcript

- Analyze political candidates' policies and rhetoric to understand their true intentions (suggested)
- Engage in respectful political discourse to bridge ideological divides (implied)

# Whats missing in summary

Beau's engaging delivery and nuanced analysis can provide deeper insights into the complex dynamics within the Republican Party. 

# Tags

#RepublicanParty #Trump #Authoritarianism #PoliticalAnalysis #SmallGovernment