# Bits

Beau says:

- Labor Day origins in September 1882, national holiday in 1894 by Grover Cleveland.
- Connected to leftist worker-oriented thought during the Second Industrial Revolution.
- Labor Day in September to avoid association with May Day and leftist ideologies.
- Debate on who created Labor Day - Peter J. McGuire or Matthew McGuire.
- Imagery associated with Labor Day: industrial jobs, blue-collar workers, and Second Industrial Revolution.
- Questioning the purpose of Labor Day: honoring labor workers or the labor movement?
- Honoring the labor movement includes figures like Blair Mountain, Eugene Debs, Cesar Chavez, and more.
- Labor movement achievements include the weekend, end to child labor, 40-hour work week, and more.
- Labor Day is for every worker in the United States, not just industrial workers.
- Importance of celebrating the worker of today and being part of the ongoing labor movement.

# Quotes

- "Labor Day is for you. It's for every worker in the United States."
- "The labor movement is still alive and well."
- "It's not history. It's not a moment. It's a movement."

# Oneliner

Labor Day origins, labor movement celebration, and ongoing worker empowerment emphasized by Beau.

# Audience

American workers

# On-the-ground actions from transcript

- Join a union for collective bargaining (implied)
- Advocate for better working conditions and standard of living (implied)
- Educate others on the importance of the labor movement (implied)

# Whats missing in summary

Importance of continued advocacy for workers' rights and collective empowerment.

# Tags

#LaborDay #LaborMovement #WorkerEmpowerment #CollectiveBargaining #AmericanWorkers