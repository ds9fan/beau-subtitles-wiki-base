# Bits

Beau says:

- Talks about the idea of the Republican Party having a plot to subvert the US Constitution through a constitutional convention.
- Explains that even if the amendments were pushed through, they still need to be ratified by 38 states, which is unlikely.
- Points out that the Republicans are resorting to this scheme because they lack the power and support to achieve their legislative goals through normal means.
- Emphasizes that the fixation on quasi-legal theories shows the weakness of the Republican Party.
- Mentions that if the Republicans manage to subvert the Constitution and push through amendments, it won't hold as they lack the consent of the governed.
- Draws parallels with Prohibition as an example of an unpopular amendment that eventually went away.
- States that progressives shouldn't fear this Republican tactic but should be prepared to counter it, as it indicates that progressives are winning.
- Notes that the Republicans' attempts to skirt the Constitution alienate more people and reveal their true intentions of wanting to rule rather than represent.
- Concludes by stating that this situation is a sign of progressives winning and that it's not something to fear but rather a display of the Republicans' disconnect from the majority of Americans.

# Quotes

- "They call themselves patriots while trying to find some way to skirt the constitution, which undoubtedly is their profile picture."
- "It's a sign that progressives are winning."
- "It's not a scary thing."

# Oneliner

Beau explains the Republican Party's scheme to subvert the Constitution, showcasing their weakness and lack of support, a sign that progressives are winning.

# Audience

Progressive activists

# On-the-ground actions from transcript

- Be aware of the Republican Party's tactics and be prepared to counter them (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's motivations and actions, offering insights into their weaknesses and the implications of their schemes.

# Tags

#RepublicanParty #USConstitution #Progressives #PoliticalAnalysis #PowerStruggles