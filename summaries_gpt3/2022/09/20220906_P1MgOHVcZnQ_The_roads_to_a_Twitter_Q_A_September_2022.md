# Bits

Beau says:

- Beau answers Twitter questions, providing live responses and insights on various topics ranging from nuclear documents to history book recommendations.
- He shares his thoughts on a potential Constitutional Convention, imitating a train, high school history classes, and favorite anime.
- Beau offers advice on studying history, suggests positive news channels, and addresses ethical questions about pet ownership.
- He talks about Liz Truss, VOSH, community organizing, defeating anxiety attacks, and advocating for unionizing.
- Beau touches on automation, defeating fascism, NATO's charter, Batman's ethics, and dealing with political fallacies.
- He shares insights on democracy, civil war avoidance, media leanings, and the importance of electoral engagement.
- Beau comments on sleep habits, favorite snacks, t-shirts, and political analysis strategies.
- He offers advice on navigating societal conflicts, engaging with right-wing media, and countering fascism.

# Quotes

- "Find what you're good at, what you're passionate about, and use that because you're going to be better at that than anything else."
- "Panic is the inability to affect self-help. You never want to be in that situation."
- "Nobody's born and wants to have their identity controlled by the state and wants everything that they get to think, do, or say determined by a massive national government."

# Oneliner

Be engaged, find passion, and resist fascism with Beau's insightful Q&A responses on various topics.

# Audience

Active citizens

# On-the-ground actions from transcript

- Join community organizations to advocate for unionizing (implied)
- Engage in disaster relief efforts or support those who are actively involved in it (implied)
- Support positive news channels like PLN to stay informed and spread good news (implied)

# Whats missing in summary

Insights on engaging with right-wing media for balanced perspectives and effective activism.

# Tags

#Q&A #CommunityEngagement #Activism #PoliticalInsights #Fascism