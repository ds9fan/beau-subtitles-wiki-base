# Bits

Beau says:

- Get Loud Arkansas, led by state senator Joyce Elliott, is working to ensure up to 104,000 people who may have been improperly removed from voter rolls can vote in the upcoming election.
- Voter purging occurs frequently, especially in red states, where voters are removed for various reasons.
- It's vital for individuals participating in electoral politics to verify their voter registration status to prevent disenfranchisement.
- Not every state has an organization like Get Loud Arkansas actively ensuring voter registration is up to date.
- Individuals from demographics that ruling parties may not want to vote or those in areas that typically vote against the predominant party should be particularly vigilant about their voter registration.
- The rhetoric surrounding elections has been used to justify actions that undermine people's voices.
- In states, especially in the South, it's advisable to verify your voter registration status before the upcoming election.
- Ensure you have enough time for any necessary registration processing before the election.
- Familiarize yourself with the voter registration laws in your state to understand the requirements.
- It's critical to safeguard your right to vote by confirming your voter registration status.

# Quotes

- "It's probably a good idea to check on your voter registration."
- "Not every state has an organization like Get Loud Arkansas that is going to go out and harass you into making sure that your voter registration is up to date."
- "If you plan on voting, this is something you might want to just make sure that everything's still in order."
- "The rhetoric surrounding the last election has given cover for a lot of people to, in the name of election integrity, subvert people's voice."
- "Just stop by and do it with enough time to make sure that your new registration, if you need one, has time to process."

# Oneliner

Get Loud Arkansas ensures 104,000 potentially disenfranchised voters in Arkansas can participate, stressing the importance of verifying voter registration nationwide.

# Audience

Voters

# On-the-ground actions from transcript

- Verify your voter registration status before the upcoming election (implied).
- Familiarize yourself with the voter registration laws in your state (implied).

# Whats missing in summary

Importance of vigilance in safeguarding voting rights and preventing disenfranchisement.

# Tags

#VoterRights #VoterRegistration #Disenfranchisement #ElectionIntegrity #GetLoudArkansas