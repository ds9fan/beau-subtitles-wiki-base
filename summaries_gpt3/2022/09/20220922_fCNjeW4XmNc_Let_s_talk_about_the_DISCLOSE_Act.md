# Bits

Beau says:

- Explains the importance of knowing where money in politics is coming from to help voters see through funding sources.
- Talks about the DISCLOSE Act, which aims to shine a light on dark money in politics.
- Mentions that the DISCLOSE Act has bipartisan support historically but is currently mainly supported by Democrats.
- Outlines provisions of the DISCLOSE Act, such as disclosing donations over $10,000 to Super PACs and banning foreign entities from contributing.
- Notes that the top five donors must be listed at the end of TV ads and donors influencing judicial nominees must be disclosed.
- Raises the question of whether it's worth investing time and effort into this Act, considering $2.6 billion was spent this way in 2020.
- Mentions that the Senate may vote on the DISCLOSE Act soon to restore faith in electoral politics and the judicial system.
- Expresses uncertainty about how effective the Act will be, especially in reshaping politics and influencing voters.
- Recommends the website Open Secrets as a tool to track who funds elected representatives and how their votes may be influenced by donations.
- Emphasizes that democracy requires advanced citizenship, where citizens need to pay attention and make informed choices.

# Quotes

- "Is this really worth putting some time and effort into?"
- "Democracy, having a representative democracy, a republic like we're supposed to have, it's advanced citizenship."
- "You know, that may have some impact."
- "The reason we got here is part corruption and part because a whole lot of people don't pay attention."
- "It's just a thought."

# Oneliner

Beau explains the significance of the DISCLOSE Act in shedding light on political funding to empower voters and restore faith in democracy and the judicial system, but questions its effectiveness in reshaping politics.

# Audience

American voters

# On-the-ground actions from transcript

- Contact your senators to express support for the DISCLOSE Act (implied)
- Use Open Secrets website to track donations to elected representatives (implied)

# Whats missing in summary

Full context and depth of Beau's analysis on the impact of the DISCLOSE Act and political funding transparency.