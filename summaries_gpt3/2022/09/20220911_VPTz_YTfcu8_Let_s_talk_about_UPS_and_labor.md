# Bits

Beau says:

- UPS is in the news due to upcoming contract negotiations with the Teamsters union, with speculation of a potential strike.
- The possibility of a strike is determined by management's stance in negotiations, not the union.
- Recent leadership changes and a $300 million strike fund suggest the Teamsters may indeed go on strike.
- A potential strike could start at 12:01 AM on August 1 and could significantly impact the US GDP due to UPS's role in facilitating 6% of it.
- The average pay for 350,000 Teamsters is $95,000, but the strike isn't just about pay; it also involves demands like air conditioning in trucks.
- Unions like the Teamsters strike not just for pay but for better working conditions and representation.
- The high pay of Teamsters is a result of collective bargaining, showcasing the effectiveness of unions in ensuring fair compensation.
- The coverage of their high pay should not be used as a reason against their right to strike, as it is the result of strong union representation.

# Quotes

- "The possibility of a strike is determined by management's stance in negotiations, not the union."
- "Unions like the Teamsters strike not just for pay but for better working conditions and representation."
- "The high pay of Teamsters is a result of collective bargaining, showcasing the effectiveness of unions."
- "Tell me again how unions don't work."
- "You get all of these articles talking about the fact that they're the highest paid and therefore they shouldn't strike."

# Oneliner

UPS faces potential strike as Teamsters negotiate, showcasing unions' effectiveness beyond pay.

# Audience

Workers, Union Members, Negotiators

# On-the-ground actions from transcript

- Support Teamsters by staying informed about the negotiations and showing solidarity with their cause (suggested).
- Reach out to local unions to understand the importance of collective bargaining and worker representation (implied).

# Whats missing in summary

The full transcript provides additional insights on the importance of unions in advocating for fair working conditions and representation beyond just salary negotiations.

# Tags

#UPS #Teamsters #Strike #Union #Negotiations #WorkerRights