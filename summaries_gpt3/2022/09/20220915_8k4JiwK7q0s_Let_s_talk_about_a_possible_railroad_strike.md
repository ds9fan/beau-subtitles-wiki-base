# Bits

Beau says:
- There is a potential railroad strike brewing, with a possible start on Friday, but an immediate work stoppage seems unlikely.
- Freight train strikes have a massive impact on the supply chain, much more than a UPS strike.
- The management of railroad companies may want the federal government to intervene to prevent the strike.
- Biden cannot stop the strike; it will require Congress to act.
- Some unions are pointing to staffing issues, requiring workers to be on call seven days a week, as a primary concern.
- Unions are adamant about changes in staffing and scheduling, even more so than pay.
- Beau believes that people involved in critical services like freight trains should be taken care of if shutdowns occur.
- The potential strike's impact extends beyond freight trains, affecting services like Amtrak.
- Biden and the Secretary of Labor are engaging with unions and management, but progress is slow.
- Without a resolution, engineers and conductors going on strike over staffing and scheduling could halt train operations.

# Quotes

- "If you're talking about something, and if it shuts down, the whole country stops, I mean, those people should probably be taken care of, is just my opinion."
- "I think we'll see some movement from the management side. But we're going to have to wait and see."
- "So anyway, it's just a thought. Y'all have a good day."

# Oneliner

There's a looming railroad strike with potential impacts on the supply chain, requiring Congress to act, especially on staffing and scheduling issues beyond just pay.

# Audience

Supply chain stakeholders

# On-the-ground actions from transcript

- Contact local representatives to urge them to address staffing and scheduling concerns in the railroad industry (implied)
- Stay informed about the situation and its potential impacts on transportation and supply chains (implied)

# Whats missing in summary

Insights on the potential long-term consequences of failing to address staffing and scheduling issues in the railroad industry.

# Tags

#RailroadStrike #SupplyChain #CongressAction #StaffingIssues #TransportationConcerns