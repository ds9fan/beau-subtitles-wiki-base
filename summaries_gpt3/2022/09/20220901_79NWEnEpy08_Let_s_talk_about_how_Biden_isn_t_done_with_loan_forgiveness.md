# Bits

Beau says:

- Explains the topic of student loan forgiveness and addresses some misconceptions in the media.
- Talks about overlooked borrowers, specifically those with federal family education loans held commercially.
- Mentions that the Biden administration might extend forgiveness to these loans through executive order.
- Emphasizes that this potential extension is not just beneficial for the directly impacted borrowers but everyone.
- Points out that Biden administration is still working on revamping education costs post the initial forgiveness announcement.
- Mentions the uncertainty around whether the forgiven amount will be taxed and lists states that are undecided on taxing it.
- States that New York has clarified they won't tax the forgiven amount and might amend the law accordingly.
- Advises caution on assuming definite taxation on forgiven loans, as some states on the list might not tax it.
- Suggests keeping an eye on further developments regarding taxation and the administration's education cost plans post-midterms.
- Beau ends by reiterating that the Biden administration is not done and advises against premature budgeting based on potential tax implications.

# Quotes

- "The Biden administration isn't finished."
- "They're not finished."
- "The Biden administration isn't finished."
- "The reporting about the taxes is kind of up in the air."
- "I hope you all have a good day."

# Oneliner

Beau explains student loan forgiveness, overlooked borrowers, potential taxes on forgiven loans, and ongoing efforts by the Biden administration.

# Audience

Students, Loan Borrowers

# On-the-ground actions from transcript

- Keep informed on updates regarding student loan forgiveness and potential tax implications (implied).
- Stay engaged in understanding how policy changes may impact student loans (implied).

# Whats missing in summary

Further insights on specific state actions regarding taxing forgiven loans can be gained from watching the full transcript.

# Tags

#StudentLoans #Forgiveness #BidenAdministration #EducationCosts #Taxes