# Bits

Beau says:

- Story from Bozier City, Louisiana, involving a cop who is also the president of the police union, taken into custody by the feds.
- Initial reports suggested the investigation was related to the union and fundraising, but the affidavit reveals it's about controlled substances, specifically painkillers.
- Sergeant Harold B.J. Sanford has not been indicted yet, charged via complaint based on allegations in the affidavit.
- Transcripts of text messages in the affidavit indicate someone was obtaining and selling pills to the cop, potentially tied to using union funds to support an alleged habit rather than widespread corruption.
- No evidence in the current information points to a larger corruption scheme within the union; it seems more like a typical addiction scenario involving a cop in a position of authority.
- Beau hints at the possibility of undisclosed angles in the case, considering the lack of transparency in legal documents.
- Despite ongoing monitoring of the situation, it doesn't seem likely that there will be a significant investigation into the union or police department.
- Beau concludes with the observation that people are most interested in whether there will be a deep dive into the union itself or the police department.

# Quotes

- "It looks just like a very typical story when it comes to this type of thing."
- "And I think that's really what people want to know."

# Oneliner

A cop's arrest involving controlled substances sparks interest, but initial allegations hint at personal addiction rather than widespread corruption within the union.

# Audience

Community members, curious individuals

# On-the-ground actions from transcript

- Stay informed on the developments of the case and any potential investigations (suggested)
- Support efforts for transparency and accountability within law enforcement (implied)

# Whats missing in summary

Insights on the implications of addressing addiction and personal behavior within positions of authority.

# Tags

#Louisiana #PoliceUnion #Addiction #Transparency #LawEnforcement