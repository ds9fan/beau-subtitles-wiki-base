# Bits

Beau says:

- Talks about Red Wolf Week, red wolves, reintroduction efforts, and how people can help.
- Describes the troubled history of red wolves and their decline to little pockets in Louisiana and Texas by 1970.
- Explains Fish and Wildlife's unique idea of catching red wolves for a breeding program to reintroduce them to the wild.
- Mentions the successful reintroduction of wolves in North Carolina in 1987, becoming a wildlife conservation role-model.
- Criticizes Fish and Wildlife for stopping protection efforts, leading to a decline in numbers due to poaching.
- Notes legal battles and federal court intervention prompting Fish and Wildlife to resume protection efforts.
- Encourages support for wolf conservation efforts at nywolf.org through symbolic adoptions, donations, or shopping at their gift shop.
- Stresses the importance of continuous action and not solely relying on the government for animal protection.

# Quotes

- "People will always devolve if they're not encouraged to protect these animals."
- "You can't just count on the government to do what's right for animals that need protection."

# Oneliner

Beau talks about Red Wolf Week, successful reintroduction efforts in North Carolina, the importance of continuous action, and supporting wolf conservation efforts at nywolf.org.

# Audience

Animal lovers, conservation enthusiasts.

# On-the-ground actions from transcript

- Visit nywolf.org to symbolically adopt a wolf, make a donation, or support by shopping at their gift shop (suggested).

# Whats missing in summary

The emotional connection Beau establishes with the audience in advocating for continuous action and support for red wolf conservation efforts.

# Tags

#RedWolf #Conservation #Wildlife #Reintroduction #Support #AnimalProtection