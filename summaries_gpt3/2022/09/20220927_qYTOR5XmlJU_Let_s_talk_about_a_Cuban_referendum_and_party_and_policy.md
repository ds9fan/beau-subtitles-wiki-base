# Bits

Beau says:

- Examines how party allegiance can overshadow supporting policies in one's own country.
- Cuba had a referendum on a new family code, including same-sex marriage and adoption rights.
- 74.1% voter turnout with two-thirds in favor of the new code.
- Those opposed to the government claimed only 47% of eligible voters voted yes.
- Government supported expanding protections, while anti-government groups opposed it out of habit.
- The opposition movement framed themselves as against rights for vulnerable groups.
- Emphasizes the issue of putting party over policy to cast oneself as pro-freedom.
- Notes the dynamics of social conservatism and evangelical involvement in Cuba's opposition.
- Draws parallels with the U.S., where parties claim to support freedom but may undermine rights.
- Urges individuals to think independently about their beliefs and not blindly follow party lines.

# Quotes

- "It was that habit of putting party over policy."
- "Don't listen to the talking heads. Think about the world you want to leave behind."
- "Maybe it's not really the pro-freedom party."

# Oneliner

Examining how party allegiance can overshadow supporting policies, using Cuba's referendum as a case study, Beau urges individuals to think independently about their beliefs rather than following party lines blindly.

# Audience

Voters, Political Activists, Community Members

# On-the-ground actions from transcript

- Question your beliefs and ideologies, independent of party influence (suggested)
- Advocate for policies that support marginalized groups in your community (implied)

# Whats missing in summary

The full transcript provides deeper insights into the dynamics of party allegiance versus policy support, urging individuals to critically analyze their beliefs and political stances.

# Tags

#PartyAllegiance #PolicySupport #Independence #CubaReferendum #PoliticalBeliefs