# Bits

Beau says:

- An advisor for the committee, Riggleman, gave an interview with 60 Minutes, revealing his analysis of communications and connections between different groups related to the events of January 6th.
- Riggleman, a former Republican congressperson endorsed by Trump, had good credentials for this work but left the Republican Party in June.
- He analyzed communications, created com graphs, and identified groups like Team Trump, state legislatures, alternate electors, rally goers, and more to establish coordination.
- Texts provided by Meadows were referred to as a roadmap to a coup attempt, resembling foreign opposition groups' communication methods.
- During the events of January 6th, someone from the White House called a person at the Capitol, identified as a rioter, indicating potential coordination.
- The committee might not have been happy about Riggleman's interview and book release, as they likely planned to disclose this information during the next hearing.
- The interview serves as a preview to the upcoming hearing, where more context and examination will be provided on the data analysis.
- The committee aims to generate public interest by releasing bits and pieces of information before the hearing, potentially implicating other individuals like a Supreme Court Justice's spouse.
- The call from the White House switchboard to a person on scene raises significant concerns and suggests possible involvement by high-ranking members of the identified groups.
- Riggleman's analysis indicates a high level of coordination among various groups related to the events of January 6th.

# Quotes

- "The rumor mill says the committee is unhappy about this interview and unhappy that he wrote a book."
- "The call from the switchboard to somebody on scene, that's pretty big."
- "There aren't a lot of ways to explain that."

# Oneliner

An advisor's analysis reveals concerning levels of coordination among various groups related to the January 6th events, potentially implicating high-ranking individuals and prompting anticipation for the upcoming hearing.

# Audience

Committee members

# On-the-ground actions from transcript

- Contact committee members to express support for thorough investigations and transparency (implied).

# Whats missing in summary

The emotional impact and tension surrounding the revelations, along with the potential consequences for those implicated, can be better understood by watching the full transcript. 

# Tags

#CommitteeInvestigation #CoordinationRevealed #January6thEvents #Transparency #PublicInterest