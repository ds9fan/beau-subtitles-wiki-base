# Bits

Beau says:

- The Democratic Party used an unusual tactic during the Republican primaries by spending tens of millions of dollars to support far-right candidates to improve the chances of the Democratic nominee in the general election.
- In Maryland, the Democratic Party supported a candidate named Cox, described as a "Q whack job," against Democratic nominee Wes Moore, a veteran and Rhodes Scholar.
- Despite initial skepticism, Moore is currently leading by more than 20 points in Maryland, showing the success of the strategy.
- Similar tactics were used in New Hampshire and Illinois, resulting in Democratic candidates being ahead in the polls by double digits.
- Michigan and Nevada are also part of this strategy, with Democrats having the edge, although the races are more competitive.
- Republicans planned to support candidates who were more polished and less openly authoritarian, hoping to push them forward with voter support.
- The Republicans' plan was disrupted by the Democratic Party's tactic of boosting far-right candidates in the primaries.
- The success of the Democratic strategy has left Republicans unhappy with the outcome.
- Overall, the tactic of supporting far-right candidates in Republican primaries to benefit Democratic nominees has proven effective in several states.

# Quotes

- "It has definitely paid off there."
- "Their plan was to ignore the more openly authoritarian, the Trumpists and support candidates that were a little bit more polished."
- "They probably would have got away with it too, if it weren't for those meddling dims."

# Oneliner

The Democratic Party's tactic of boosting far-right candidates in Republican primaries proved successful, leading to significant leads for Democratic nominees in multiple states.

# Audience

Political strategists

# On-the-ground actions from transcript

- Support and get involved in local Democratic Party efforts to strategize and support candidates in primary elections (suggested)
- Stay informed about political strategies and tactics being used in primary elections (suggested)

# Whats missing in summary

The full transcript provides more detailed insights into the specific races and outcomes in various states, giving a comprehensive view of the Democratic Party's strategic approach during primary elections.