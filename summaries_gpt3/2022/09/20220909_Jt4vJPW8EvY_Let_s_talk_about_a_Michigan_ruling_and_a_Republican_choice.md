# Bits

Beau says:

- Michigan ruling combined with promise from Attorney General puts the ball in the Republican Party's court.
- Judge in Michigan ruled 1931 law prohibiting abortion violates due process and equal protection.
- Attorney General in Michigan promised not to appeal the ruling, so the law will be struck down.
- Governor of Michigan asking Supreme Court to rule on lawsuit protecting family planning rights.
- Supreme Court of Michigan expected to rule on a constitutional amendment regarding the ballot.
- Republican politicians in Michigan have the choice to appeal and fight against the ruling.
- Republicans may have to put their names on the decision to take away half the state's rights.
- Impact on re-election chances is now a concern for Republican politicians.
- Some Republican politicians are starting to backtrack on their rhetoric on this topic.
- Question arises whether Republicans are willing to take action themselves or want constituents to do it.

# Quotes

- "It's up to the GOP."
- "Whether or not all of that rage that they manufactured, all of that anger, all of that kicking down at people, whether or not they're willing to do it themselves or they just want their constituents to do it."
- "Anyway, it's just a thought."

# Oneliner

Michigan ruling and Attorney General's promise leave Republican Party with a choice: stand up against rights-stripping decision or hide behind the Supreme Court, impacting re-election chances.

# Audience

Michigan residents, Republicans

# On-the-ground actions from transcript

- Contact Republican politicians in Michigan to urge them to uphold rights (implied)
- Stay informed about the developments and decisions regarding the court rulings in Michigan (implied)

# Whats missing in summary

The emotional impact and personal stakes involved in the decision-making process for Republican politicians in Michigan. 

# Tags

#Michigan #AbortionRights #RepublicanParty #SupremeCourt #Ruling