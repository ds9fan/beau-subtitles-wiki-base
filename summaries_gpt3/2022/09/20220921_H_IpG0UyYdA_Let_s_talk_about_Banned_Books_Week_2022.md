# Bits

Beau says:

- Introduces Banned Books Week, focusing on books that some in power don't want people to read.
- Mentions the PEN America report covering bans from July 2021 to June 2022.
- Reveals that during this period, there were 2,532 instances of book bans impacting 5,049 schools and over 4 million students.
- Points out Texas, Florida, and Tennessee as the worst offenders in book bans.
- Notes that these bans are often linked to a few conservative groups, impacting the free speech of many authors and illustrators.
- Shows that authors of color and LGBTQ authors are often targeted in these bans.
- Questions why certain groups are trying to silence these voices, suggesting it's due to an inability to defend their ideology.
- Believes that promoting curiosity through books challenges parents who prefer ignorance for their children.
- Suggests that the root cause of banning these books is to uphold an ideology that can't withstand questioning.
- Encourages reading the report and reminds that while reading, "bad books are the best books."

# Quotes

- "Just remember while you're reading that, that bad books are the best books."
- "Those books, those books they're trying to remove, they want to avoid the questions."
- "Parents are making a choice to keep their children ignorant."
- "The worst thing you can do for an idea you support is to present it and defend it poorly."
- "If your idea is indefensible, well, it's easy for them to poke holes in it and that ideology collapses."

# Oneliner

Beau talks about Banned Books Week, revealing the impact of book bans on authors and students while questioning the motives behind silencing certain voices.

# Audience

Book enthusiasts, activists

# On-the-ground actions from transcript

- Read the PEN America report on book bans (suggested)
- Celebrate Banned Books Week by reading a banned book (implied)

# Whats missing in summary

Deeper insights into the contents of the PEN America report and specific examples of banned books.

# Tags

#BannedBooksWeek #Censorship #FreeSpeech #Ideas #Reading