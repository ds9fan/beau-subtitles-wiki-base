# Bits

Beau says:

- Attorney General James of New York filed a lawsuit against Trump and others, seeking $250 million in relief and requesting sanctions including barring certain individuals from being directors and canceling the Trump Organization's corporate certificate.
- The lawsuit alleges a 10-year pattern of false financial statements aiming to boost Trump's net worth annually, mentioning a couple hundred false or misleading statements.
- Examples include valuing Trump's Wall Street building at $524 million in 2010, a huge jump from $200 million, and overstating the size of Trump's apartment to inflate its value to $327 million.
- These misrepresentations were not minor errors but intentional, according to the allegations, affecting documents used for financial purposes by lenders and insurers.
- While this is a civil case, referrals have been made to the IRS and Department of Justice, potentially leading to further investigation.
- Despite Trump's tendency to exaggerate, this case involving financial documents presents a more serious issue that might result in significant consequences for him.
- Beau believes that although this case won't bring Trump down completely, it will likely lead to significant financial penalties.
- The ongoing legal issues add to Trump's troubles and could potentially escalate into a criminal case in New York.

# Quotes

- "If that happens, they don't work in New York anymore. It's kind of a big deal."
- "There's a word for that."
- "This is going to be a giant, giant pain for Trump."

# Oneliner

Attorney General of New York files a lawsuit against Trump and others for a 10-year pattern of false financial statements, potentially leading to significant consequences for Trump.

# Audience

Activists, Legal Experts

# On-the-ground actions from transcript

- Contact local authorities or legal organizations to stay informed about the developments in this case (suggested).
- Join advocacy groups working on transparency and accountability in financial matters (implied).

# Whats missing in summary

More insights on the potential impacts of the case on Trump's future legal and financial standing.

# Tags

#Trump #NewYork #lawsuit #financialmisrepresentation #legalconsequences