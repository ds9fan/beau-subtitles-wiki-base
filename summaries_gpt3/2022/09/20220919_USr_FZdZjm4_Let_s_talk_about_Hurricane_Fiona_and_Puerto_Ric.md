# Bits

Beau says:

- Hurricane Fiona is causing flooding in Puerto Rico and the Dominican Republic, with rainfall being the main issue.
- The excessive rain, ranging from 12 to 30 inches in some areas, is leading to major flooding, including urban areas and rivers.
- The flooding has triggered landslides, mudslides, and power outages across the island.
- Puerto Rico's power grid is still struggling to recover from Hurricane Maria, with nearly one and a half million homes losing power during Fiona.
- President Biden has declared a state of emergency, but the island's past experiences with inadequate federal responses are causing skepticism among residents.
- Efforts are being made to restore power and establish command functions, but concerns remain about the effectiveness of the response.
- It's uncertain how the situation will unfold, and there may be a need for widespread assistance in Puerto Rico.
- Monitoring the situation in the coming days will provide a clearer picture of the extent of the damage and opportunities for individuals to help.
- Beau commits to keeping everyone informed about ways to support the affected communities.
- The aftermath of Hurricane Fiona serves as a reminder of the ongoing challenges faced by Puerto Rico and the importance of proactive assistance from both government and individuals.

# Quotes

- "It's not so much the wind. It's the rain."
- "The flooding has caused landslides, mudslides, power outages."
- "There are probably a lot of people in Puerto Rico right now that are less than optimistic about the federal government's response."
- "Sometimes past performance does predict future results."
- "We'll know what we as individuals can do to help out."

# Oneliner

Hurricane Fiona triggers major flooding in Puerto Rico and the Dominican Republic, raising concerns about the effectiveness of federal response efforts amidst past inadequacies.

# Audience

Disaster relief organizations

# On-the-ground actions from transcript

- Support local relief efforts in Puerto Rico (implied)
- Stay informed about the situation to understand how to provide assistance (implied)
- Volunteer with organizations working to aid those affected in Puerto Rico (implied)

# Whats missing in summary

The emotional impact on individuals and communities affected by the flooding and power outages in Puerto Rico. 

# Tags

#HurricaneFiona #PuertoRico #DisasterRelief #FederalResponse #CommunitySupport