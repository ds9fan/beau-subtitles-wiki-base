# Bits

Beau says:

- Beau introduces the topic of fiction and how stories like Lord of the Rings, Game of Thrones, and Harry Potter can teach us about building a better world.
- An individual shares their journey of transitioning from reading fantasy books like Harry Potter to feeling the need to catch up on more informative reading material for community development.
- The comparison between authors and activists is made, stating they both share the job of envisioning and bringing a world to life through different mediums - pen and paper for authors and actions for activists.
- Beau suggests that immersive, world-building books like 1984, Lord of the Rings, Game of Thrones, and Harry Potter could be inspiring for someone interested in community development.
- Fiction, regardless of the topic, can have a profound impact on individuals and alter the way they perceive the world, even through small realizations gained in a fantasy setting.

# Quotes

- "Fiction is this unique thing because it doesn't matter what the topic is. If the fiction is any good at all, it's going to have an impact on you."
- "Sure, these are not books that people perceive as being incredibly deep, so if that's what inspires you, go for it."
- "You say information can get you to fact, but fiction can get you to truth."

# Oneliner

Beau shares insights on how fiction, including fantasy books like Harry Potter, can inspire and guide individuals towards building a better world through immersive storytelling and world-building experiences.

# Audience

Students, aspiring community developers

# On-the-ground actions from transcript

- Read immersive and world-building books like 1984, Lord of the Rings, Game of Thrones, and Harry Potter to gain inspiration for community development (suggested).
- Utilize audiobooks through apps like LibriVox to access a wide range of books amidst a busy schedule (suggested).

# Whats missing in summary

Beau's engaging delivery and encouragement to embrace fiction as a tool for personal growth and societal change can best be experienced by watching the full video. 

# Tags

#Fiction #CommunityDevelopment #BuildingABetterWorld #FantasyBooks #ImmersiveReading