# Bits

Beau says:

- Recounts a familiar story that plays out too often, leading to questions and statements that result in little change.
- Raises the possibility of a different outcome by utilizing existing technology after an incident in Memphis.
- Suggests sending a text to the entire city to alert them of what was happening, potentially altering the course of events.
- Points out that a bill addressing this technology was proposed in the House but faced pushback.
- Criticizes individuals who may express regret and send thoughts and prayers but resist practical solutions when presented.
- Notes that the bill might still be pending in the Senate Judiciary Committee.
- Emphasizes that simple actions, like utilizing technology for alerts, could significantly impact outcomes but are not being implemented.
- Criticizes politicians for prioritizing rhetoric over implementing tools that could save lives.
- Anticipates future similar incidents until necessary actions, like sending texts for alerts, are put in place.
- Concludes with a reflection on the government's tendency to delay action until exhausting other options.

# Quotes

- "What if after that first or that second incident yesterday, a text went out to the entire city telling them what was happening?"
- "Those tasked with governing seem incapable of applying the simplest things to help the action."
- "There will be another one. And probably another one."
- "It will do the right thing after it tries everything else first and ignores the problem for years."

# Oneliner

Beau suggests utilizing existing technology, like sending texts for alerts, to potentially alter outcomes in recurring incidents, criticizing the lack of action and prioritization of rhetoric over practical solutions.

# Audience

Advocates for Change

# On-the-ground actions from transcript

- Advocate for the implementation of alert systems utilizing existing technology (suggested)

# Whats missing in summary

The full transcript provides a deeper insight into the recurring cycle of incidents and the potential for simple technological solutions to prevent harm and save lives.

# Tags

#Technology #AlertSystems #GovernmentInaction #PolicyChange #CommunitySafety