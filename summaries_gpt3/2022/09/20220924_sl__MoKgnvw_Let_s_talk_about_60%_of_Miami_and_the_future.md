# Bits

Beau says:

- Talks about the future over the next 40 years and the impact of climate change on internal displacement in the US.
- Mentions an example of an island off the coast of Louisiana disappearing due to climate change, leading to people being relocated.
- Shares a shocking statistic that by 2060, 60% of Miami is expected to be underwater, causing 1.6 million internally displaced people just from that area.
- Expresses concern about the lack of real action and deep systemic change needed to address the impending crisis.
- Observes the mistaken belief of showmanship for leadership, pointing out the need for genuine solutions.
- Stresses the urgency of the situation and the need to prioritize climate change in elections due to running out of time.
- Talks about the reluctance to accept internally displaced Americans as refugees, indicating a need for a shift in perspective on a global scale.
- Points out that ignoring the problem will only exacerbate it and that action is necessary to address the looming crisis.

# Quotes

- "We have real problems on the horizon. They're big and it's going to take more than talking points."
- "Ignoring it will make it worse because we won't make the changes that are necessary."
- "This problem isn't going to be solved by ignoring it."
- "There's a lot of problems on the horizon. They won't be solved by talking points."
- "We know what's happening. We know what's causing it."

# Oneliner

Beau talks about the urgent need for deep systemic change to address the impending climate change crisis, with 60% of Miami expected to be underwater by 2060, leading to 1.6 million internally displaced people.

# Audience

Climate activists, policymakers, citizens

# On-the-ground actions from transcript

- Contact local representatives to prioritize climate change action in policies and elections (suggested)
- Join or support organizations advocating for climate change mitigation and support for internally displaced people (suggested)

# Whats missing in summary

The full transcript provides a detailed insight into the impending crisis of internal displacement due to climate change in the US and the urgent need for systemic change and action to address this growing issue.

# Tags

#ClimateChange #InternalDisplacement #SystemicChange #Miami #Urgency