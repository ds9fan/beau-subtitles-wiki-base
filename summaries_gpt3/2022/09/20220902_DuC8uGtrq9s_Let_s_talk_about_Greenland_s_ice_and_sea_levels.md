# Bits

Beau says:

- Describes a study on Greenland's ice that is essentially "zombie ice" - ice that won't stay ice because it's cut off from the process that maintains it.
- The study predicts a conservative estimate of a 10-inch rise in sea levels solely from Greenland's ice, regardless of current actions taken to combat climate change.
- Attributes the inevitability of this sea level rise to the rise in average temperature in Greenland over the past 40 years, about 1.5 degrees per decade.
- Compares the study's prediction to Noah's estimate of a 10 to 12-inch rise in sea levels over the next 30 years.
- Explains that a 10-inch rise doesn't equate to a uniform increase everywhere, as it can lead to broader flooding in some areas while causing water levels to recede in others.
- Emphasizes the importance of understanding that we are now beyond stopping or avoiding climate change; instead, the focus is on mitigating its effects due to past inaction.
- Points out that the longer action is delayed, the more inevitable and severe the effects of climate change become, as the impacts take time to manifest.
- Suggests that studies like the one on Greenland's ice can raise awareness about the irreversible aspects of climate change and potentially encourage more people to take action to mitigate it.
- Uses the analogy of a train without brakes to illustrate the imminent melting of Greenland's ice and subsequent sea level rise.
- Encourages further research and studies conducted in a similar vein to bolster efforts in combating climate change.

# Quotes

- "There's more than 100 trillion tons of ice that won't stay ice."
- "We are past that point. We're at mitigate climate change."
- "We see that train coming but we also know it doesn't have any brakes."
- "This ice is still there at the moment but it is going to melt."
- "If more research is conducted in this fashion, we might be able to encourage more people to join the fight to mitigate it."

# Oneliner

Greenland's "zombie ice" will inevitably raise sea levels by 10 inches, showing the urgency to mitigate climate change effects as stopping it is now unrealistic.

# Audience

Climate activists and concerned citizens

# On-the-ground actions from transcript

- Join the fight to mitigate climate change by supporting research and studies that raise awareness (suggested)
- Take concrete actions in your community to reduce emissions and adapt to climate change (implied)

# Whats missing in summary

The full transcript provides a detailed explanation of the inevitability of sea level rise due to Greenland's ice melt and stresses the importance of immediate action to mitigate climate change effects.

# Tags

#ClimateChange #SeaLevelRise #GreenlandIce #Mitigation #Research