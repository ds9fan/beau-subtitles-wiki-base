# Bits

Beau says:

- Polls rely on likely voters, determined by pollsters, but motivating factors may drive unlikely voters to the polls in Georgia.
- The latest polling in Georgia suggests unlikely voters will decide the next governor, as Kemp and Abrams are neck and neck.
- With changing demographics in Georgia, unlikely voters who typically stay home may now have a significant impact on election outcomes.
- The potential for unlikely voters to sway the election is heightened by the fact that some are willing to cross party lines.
- If unlikely voters lean blue and combine with those crossing party lines, Georgia may turn blue.
- Abrams, known for mobilizing voters, could win if these unlikely voters turn out and support her campaign.

# Quotes

- "It is the unlikely voter who decides who runs that state."
- "Georgia's turning blue."
- "If the vote gets out, if those unlikely voters skew blue, she wins."

# Oneliner

Unlikely voters could decide Georgia's next governor as demographics shift, potentially turning the state blue.

# Audience

Georgia residents

# On-the-ground actions from transcript

- Mobilize unlikely voters to vote (implied)
- Support voter turnout efforts (implied)
- Encourage voter engagement and education (implied)

# Whats missing in summary

The importance of voter mobilization efforts and engaging unlikely voters in Georgia's election process.

# Tags

#Georgia #Election #Voting #Demographics #Abrams