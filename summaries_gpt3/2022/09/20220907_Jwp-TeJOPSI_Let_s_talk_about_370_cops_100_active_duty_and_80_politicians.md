# Bits

Beau says:

- Analyzing the numbers of 370 current cops, 100 active service members, and 80 public officials identified in the roster of the group, the oath keepers.
- The group had 38,000 names on its roster, so the percentages are not as high as they may seem.
- Questions if the numbers should be considered high, given the target demographic of the group.
- Considers that the numbers are low, even if you apply standard math to estimate a higher count.
- Raises the importance of analyzing former law enforcement and military members to get a clearer picture.
- Expresses surprise that the numbers weren't higher, considering the target demographic and recruitment efforts.
- Mentions recent news about the arrest of a longtime attorney for the group and efforts to delay trials.
- Suggests that perceived capability of such groups may not match their actual influence.
- Calls for perspective on the numbers and the need for lower figures.
- Concludes with a reminder to keep these factors in mind.

# Quotes

- "These numbers are low. Even if you were to take the standard math when it comes to active versus sympathizer and all of that stuff and multiply it, 3,700 cops nationwide, it's actually not as influential an organization as a lot of people may think."
- "The recruitment efforts of this group were specifically catered to reach a certain group of people. Going by what we see here that recruitment effort didn't succeed."
- "Perceived capability is being substituted for an accurate analysis of actual capability."

# Oneliner

Analyzing the numbers of identified individuals in the oath keepers' roster reveals lower percentages than expected, prompting a call for perspective and accurate analysis over perceived capability.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Analyze the demographics and percentages of former law enforcement and military members related to groups like the oath keepers (suggested).
- Maintain awareness and vigilance regarding recruitment efforts by extremist groups (implied).

# Whats missing in summary

The full transcript provides detailed insights into the analysis of identified individuals in extremist groups, shedding light on the importance of accurate assessments over perceived threats.

# Tags

#Analysis #Extremism #LawEnforcement #Military #Recruitment