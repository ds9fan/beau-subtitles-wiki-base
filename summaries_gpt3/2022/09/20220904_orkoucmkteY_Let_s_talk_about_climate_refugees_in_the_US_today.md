# Bits

Beau says:

- Talks about the impacts of climate change that are already affecting communities.
- Mentions Isle de Jean Charles, an island off the coast of Louisiana inhabited by a native group.
- Describes how the residents of Isle de Jean Charles are being relocated to a new development called New Isle due to their island disappearing underwater.
- Explains that New Isle came into being through a $48 million grant from HUD in 2016.
- Notes that Louisiana loses an area the size of Manhattan every year due to coastal erosion.
- Points out that the residents of Isle de Jean Charles are now considered climate change refugees.
- Emphasizes that this won't be the last story of people being forced to move due to climate change impacts.
- Urges for attention and action on climate change to prevent such situations from worsening.
- Concludes by stressing the importance of keeping climate change issues at the forefront of our concerns.

# Quotes

- "We now have official within the United States climate change refugees that were assisted in their move by the federal government."
- "This won't be the last coverage about a group of people who had to move because their home became uninhabitable."
- "This has to be on the ballot because it's gonna get worse."

# Oneliner

Beau warns about the real impacts of climate change through the relocation of Isle de Jean Charles residents, now climate change refugees, and calls for urgent attention and action as these situations worsen.

# Audience

Climate activists, policymakers, community organizers

# On-the-ground actions from transcript

- Advocate for policies addressing climate change impacts (implied)
- Support and amplify voices of communities affected by climate change (implied)
- Take action to reduce carbon footprint and mitigate climate change effects (implied)

# Whats missing in summary

The emotional toll on displaced communities and the need for global cooperation on climate action.

# Tags

#ClimateChange #EnvironmentalJustice #CommunityImpact #ClimateRefugees #Louisiana