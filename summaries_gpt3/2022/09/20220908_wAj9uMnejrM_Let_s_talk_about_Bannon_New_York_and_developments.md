# Bits

Beau says:

- Bannon is expected to surrender to face a new indictment related to the build the wall fundraising efforts, but the details of the indictment are sealed.
- Steve Bannon is framing the indictment as a political hit job, fueling the far right's views on a politicized criminal justice system.
- Bannon's arrest is likely to energize the far right, viewing it as an attack on their people through the justice system.
- The reaction and statements from Trump regarding Bannon's case will significantly influence how the far right base responds.
- Republican strategists hope that Trump remains quiet as his reaction could either motivate or harm the far right base, impacting the midterm strategy.
- Bannon is also facing two counts of contempt, adding to the legal issues he is dealing with.
- The unfolding details of Bannon's case are expected to dominate news cycles in the coming days as more information emerges.

# Quotes

- "It's a weaponized criminal justice system."
- "His arrest is likely to energize the far right."
- "This is a moment where Trump might come out and have a reaction to this that really hurts Republicans."
- "His reaction is going to definitely steer the far right reaction."
- "But it's likely that this may end up becoming the dominant story over the next few days."

# Oneliner

Beau outlines Bannon's new indictment, his framing of it as a political hit job, and the potential impact on the far right base depending on Trump's reaction.

# Audience

Politically engaged individuals

# On-the-ground actions from transcript

- Stay informed about the unfolding details of Bannon's case (implied).

# Whats missing in summary

Insight into the potential broader implications of Bannon's legal troubles on political landscapes and movements.

# Tags

#Bannon #SteveBannon #Indictment #FarRight #Trump #RepublicanStrategists