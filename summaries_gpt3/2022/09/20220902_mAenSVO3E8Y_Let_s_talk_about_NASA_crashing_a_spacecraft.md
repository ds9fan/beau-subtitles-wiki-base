# Bits

Beau says:

- NASA has a spacecraft on course to collide with an asteroid as part of the Double Asteroid Redirection Test (DART).
- The test aims to develop the first step in a planetary defense system to alter the course of asteroids heading towards Earth.
- The current mission is a proof of concept, taking place 6.8 million miles from Earth.
- The spacecraft will impact an asteroid orbiting another asteroid, with the target asteroid being 520 feet long.
- The spacecraft, moving at 15,000 miles per hour, acts as a space battering ram.
- This test is a precautionary measure in case an asteroid threat arises in the future.
- Although there is no immediate threat, it's a proactive step for planetary defense.
- The live broadcast of the test release an observation craft on September 26 at 7.14 PM Eastern.
- Viewers can watch the event live on NASA's website, YouTube, Facebook, and Twitter.
- Beau anticipates various theories and speculations arising from the test.

# Quotes

- "I cannot wait to see something smash into an asteroid at 15,000 miles an hour."
- "This is a test. We've talked about it before on the channel."

# Oneliner

NASA's spacecraft is set to collide with an asteroid as part of a planetary defense test, showcasing a proactive approach to potential future threats.

# Audience

Space enthusiasts, science lovers.

# On-the-ground actions from transcript

- Watch the live broadcast of the spacecraft impacting the asteroid on September 26 at 7.14 PM Eastern (suggested).

# Whats missing in summary

The full transcript provides more context and details about NASA's Double Asteroid Redirection Test and the significance of this test in developing a planetary defense system.