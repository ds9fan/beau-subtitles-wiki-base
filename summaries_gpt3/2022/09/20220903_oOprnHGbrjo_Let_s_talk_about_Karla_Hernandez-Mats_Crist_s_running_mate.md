# Bits

Beau says:

- Overview of the Florida governor's race dynamics, comparing DeSantis and Christ as wild authoritarian versus centrist.
- Introduction of Chris' running mate, Carla Hernandez, president of United Teachers of Dade, signaling a focus on family planning, education, and labor.
- Hernandez's background as a teacher and union leader with immigrant parents from Honduras.
- Speculation on the potential line of attack against Hernandez by the Republican Party.
- Mention of a tweet involving Castro that the Republican Party tried to use to paint Hernandez as a communist.
- Reference to Hernandez's humanitarian work overseas with limited details available.
- The possibility of Hernandez swinging votes in Florida, traditionally viewed as a red state.

# Quotes

- "The successful line of attack against DeSantis as being family planning, education, and labor."
- "She's a teacher. She's very energetic."
- "She has a strong background if that's what they're going to run on."
- "It's an interesting choice."
- "Y'all have a good day."

# Oneliner

In the Florida governor's race, Chris' running mate, Carla Hernandez, a teacher and union leader, may shift the focus to family planning, education, and labor, potentially swinging votes in the traditionally viewed red state.

# Audience

Florida voters

# On-the-ground actions from transcript

- Contact education organizations and unions to support Carla Hernandez (suggested)
- Stay informed about the Florida governor's race and the candidates' platforms (implied)

# Whats missing in summary

Detailed analysis of DeSantis' and Christ's positions and policies.