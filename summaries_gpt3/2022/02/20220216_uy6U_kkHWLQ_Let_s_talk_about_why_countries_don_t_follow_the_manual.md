# Bits

Beau says:

- Explains why countries sometimes deviate from the manual in dealing with civil disturbances and demonstrations.
- Mentions examples of situations where following or not following the manual leads to expected outcomes.
- Addresses a viewer's question about why heavy-handed methods are sometimes used against movements that threaten the status quo.
- Talks about the existence of literal manuals on civil disturbance and counterinsurgency.
- Points out that race plays a significant role in how different groups are treated by governments.
- Mentions the influence of public health experts and advisors in policymaking.
- Criticizes the aggressive approach often depicted in movies and how it influences decision-making.
- Notes the disconnect between expert advice and political actions, especially in Western countries.
- Emphasizes that brute force is not always effective in dealing with civil disturbances.
- Stresses the importance of understanding that the ability to endure, not to overpower, is key in such situations.

# Quotes

- "It isn't the group that can dish out the most that wins. It's the group that can take the most."
- "There are literal manuals about this on civil disturbance, counterinsurgency, stuff like that."
- "Race always has something to do with it."
- "In Western countries, in the United States especially, there is the idea that to win, you have to be aggressive."
- "Most of the major demonstrations you are aware of, that you know of, you know about them because of an overreaction by state forces."

# Oneliner

Beau explains why countries deviate from manuals in dealing with civil disturbances, citing race and aggressive tendencies as key influencers, ultimately stressing the importance of endurance over brute force.

# Audience

Policy influencers, activists

# On-the-ground actions from transcript

- Organize peaceful demonstrations to raise awareness and push for policy changes (suggested)
- Support marginalized groups facing unequal treatment by advocating for fair and equal enforcement of laws (implied)

# Whats missing in summary

Importance of addressing implicit biases and challenging the status quo to create a more just and equitable society.

# Tags

#CivilDisturbances #ManualDeviation #Race #PolicyMaking #ImplicitBias