# Bits

Beau says:

- Addressing a quote from Arizona about a new piece of legislation introduced by Steve Kaiser, HB 2161.
- The bill aims to require teachers to inform parents if their child expresses a different identity or orientation.
- This bill creates an atmosphere where the government monitors and controls personal information about students.
- Failure to comply with this requirement could lead to lawsuits, fostering an environment of spying and informing on neighbors.
- Many anti-LGBTQ groups assisted in drafting this legislation, revealing its discriminatory nature.
- When questioned about the lack of collaboration with education stakeholders, Kaiser's response was dismissive and concerning.
- The legislation seems to have been proposed without the support of any education groups, indicating its flawed nature.
- This bill is part of a broader agenda to establish an informer system reminiscent of authoritarian regimes.
- It serves as a divisive tactic to appeal to a certain base by targeting vulnerable LGBTQ children.
- Republicans resort to attacking LGBTQ kids as a way to garner support, lacking genuine leadership.
- Individuals in Arizona are encouraged to look into the implications of this bill.

# Quotes

- "The purpose of the bill is to require people at schools, teachers, so on and so forth, to call in parents if they find out their student, if they find out their child, is expressing a different identity or orientation."
- "When asked why he didn't work with real stakeholders, with education groups, with groups that represent educators and teachers, that was his answer."
- "If you're proposing legislation and the group that's going to be impacted by it wouldn't support it, that's probably an indicator that it's a bad piece of legislation."
- "This is, A, part of the desire to create that informer system in the United States, to bring about that type of system that authoritarians like to use to keep the populace in check."
- "Republicans, at this point, are limited to kicking down at LGBTQ kids."

# Oneliner

Arizona bill requires teachers to inform parents about their child's identity, sparking concerns of government intrusion and discrimination.

# Audience

Activists, Educators, Parents

# On-the-ground actions from transcript

- Contact local education advocacy groups to understand their stance on the bill (suggested)
- Stay informed about local legislative actions affecting schools and children (exemplified)

# Whats missing in summary

The full transcript provides additional context on the discriminatory nature of the legislation and the lack of genuine stakeholder involvement.

# Tags

#Arizona #HB2161 #LGBTQ #Discrimination #Education #Legislation