# Bits

Beau says:

- Raises concerns about the use of DNA evidence obtained from sexual assault kits against victims years later.
- Points out the chilling effect this practice may have on victims coming forward to provide evidence.
- Questions law enforcement's prioritization of information collection over solving serious crimes.
- Argues that deterring victims from providing evidence through DNA kits may lead to perpetrators committing more crimes.
- Calls attention to the ethical implications of using victims' DNA against them without their consent.
- Expresses worry that this issue in San Francisco may be happening statewide and potentially nationwide.
- Suggests that widespread knowledge of this practice could further discourage victims from seeking justice.
- Raises broader societal questions about the collection and storage of personal information by authorities.
- Contemplates the implications of a society where the government has total information awareness.
- Urges reflection on the impact of such practices on freedom and privacy.

# Quotes

- "Why would they not use it?"
- "It seems to be prioritizing information collection over solving an incredibly serious crime."
- "We're going to have to figure out where the lines are, how long information is going to be kept."
- "How free of a society can it really be?"
- "Y'all have a good day."

# Oneliner

Beau raises concerns about law enforcement using DNA evidence from sexual assault kits against victims, leading to chilling effects and ethical dilemmas, with potential broader societal implications.

# Audience

Advocates for victims' rights

# On-the-ground actions from transcript

- Advocate for clear guidelines and consent procedures in the use of DNA evidence from sexual assault kits (implied).
- Support organizations working to protect victims' rights in criminal investigations (suggested).

# Whats missing in summary

The full transcript provides a deeper dive into the ethical and privacy concerns surrounding the use of DNA evidence from sexual assault kits and prompts reflection on the broader societal implications of information collection by authorities.

# Tags

#DNAevidence #VictimsRights #LawEnforcement #PrivacyConcerns #InformationAwareness