# Bits

Beau says:

- Beau introduces Nina Morrison, a Biden nominee for a lifetime appointment in a US district court, with qualifications from her work with the Innocence Project.
- Morrison has freed 30 innocent people, including those on death row, showing a commitment to justice beyond the legal system.
- Senators Cruz and Hawley criticize Morrison in unique ways: Cruz links her to rising crime rates in Philadelphia due to advising on conviction integrity, while Hawley ties her to 2020 protests based on an op-ed praising an innocent man's release.
- Beau points out the irony in Cruz's argument, noting that freeing innocent people actually helps reduce crime rates.
- Hawley's attempt to connect Morrison to the 2020 protests seems baseless, especially considering his own controversial actions during the Capitol riots.
- Morrison is highly qualified for the position and dedicated to ensuring justice prevails, despite the obstructionist tactics used by Republicans to oppose her nomination.
- Beau criticizes the Republican tactics as a way to appease their inflamed voter base and manufacture opposition without valid grounds.
- Beau underscores the importance of supporting nominees like Morrison who prioritize justice and work to prevent innocent people from suffering.
- Beau reveals his support for the Innocence Project and provides a link for donations, encouraging viewers to contribute to the cause.

# Quotes

- "A person who is truly committed to justice rather than just the legal system."
- "What you have is normal obstructionist tactics from Republicans in an attempt to manufacture opposition to somebody that there's no real grounds to oppose."

# Oneliner

Beau sheds light on Biden nominee Nina Morrison’s dedication to justice and exposes Republican obstructionist tactics in opposing her appointment without valid grounds.

# Audience

Justice advocates

# On-the-ground actions from transcript

- Support the Innocence Project by donating (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of the qualifications and unjust criticisms faced by Biden nominee Nina Morrison, urging viewers to support nominees committed to justice and condemning baseless opposition tactics by Republicans.