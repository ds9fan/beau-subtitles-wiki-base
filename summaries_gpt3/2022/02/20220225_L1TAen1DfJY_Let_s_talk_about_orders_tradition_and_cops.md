# Bits

Beau says:

- Addresses the topic of orders, tradition, and perception, surprised to still be discussing it in this context.
- Explains the difference between military and law enforcement in terms of following orders and accountability.
- Talks about the warrior cop mentality and the spread of militaristic culture within law enforcement.
- Explains the historical origins of following orders from superiors and the lack of accountability in some cases.
- Contrasts military objectives with those of law enforcement, pointing out the need for different systems of accountability.
- Mentions the importance of training in the military on disobeying unlawful orders and suggests a similar training for law enforcement.
- Criticizes the blurred lines between law enforcement and the military, pointing out the dangers of adopting militaristic traditions in policing.
- Emphasizes that law enforcement should not be seen as an occupying army and should not adopt a warrior cop culture without the necessary discipline.
- Raises concerns about the misconceptions around following orders in law enforcement and the need for clear boundaries between police and military roles.

# Quotes

- "Law enforcement is not the same as the military."
- "There should never be a point where a comparison between peace officers and the military makes sense."
- "They're not the same."

# Oneliner

Beau explains the dangers of blurring the lines between law enforcement and the military, stressing the importance of differentiating between peace officers and warriors. 

# Audience

Law Enforcement Officials

# On-the-ground actions from transcript

- Implement training for law enforcement on disobeying unlawful orders (suggested)
- Advocate for clear boundaries between law enforcement and military roles (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the historical origins of following orders in law enforcement and the dangers of adopting militaristic traditions within the police force.

# Tags

#LawEnforcement #Orders #Tradition #Military #Accountability