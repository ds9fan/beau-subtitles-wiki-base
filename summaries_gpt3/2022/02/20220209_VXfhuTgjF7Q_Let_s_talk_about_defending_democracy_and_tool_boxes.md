# Bits

Beau says:

- Questions the support for U.S. democracy by those wanting a radically different world.
- Compares different toolboxes as symbolic of different forms of government/society.
- Describes his toolbox as representative democracy, with limitations but some abilities.
- Warns against far-right government models where people serve the government, not vice versa.
- Acknowledges flaws in the current U.S. government and Constitution but sees potential for change.
- Stresses the importance of maintaining the current toolbox (representative democracy) to build a better society.
- Recognizes individuals who advocate for change while supporting the existing system behind the scenes.
- Shares a moment of realization about the precarious state of American democracy.
- Notes the efforts of many pushing for progressive change while maintaining the status quo.
- Concludes by reflecting on the complex dynamics of politics and alliances throughout history.

# Quotes

- "Government should serve the people, not the other way around."
- "That's the starting place."
- "Politics makes strange bedfellows."
- "It's probably happened a lot throughout history."
- "It's just a thought."

# Oneliner

Beau questions support for U.S. democracy amidst the desire for radical change, comparing government to different toolboxes and stressing the importance of maintaining the current system to build a better society.

# Audience

Political activists, progressives, democracy advocates.

# On-the-ground actions from transcript

- Maintain support for representative democracy (implied).
- Advocate for progressive change within the existing system (implied).
- Stay informed about the state of American democracy and actively support positive changes (implied).

# Whats missing in summary

Nuances of Beau's storytelling and personal reflections on the state of American democracy and progressive activism.

# Tags

#Democracy #ProgressiveChange #PoliticalActivism #Government #Society