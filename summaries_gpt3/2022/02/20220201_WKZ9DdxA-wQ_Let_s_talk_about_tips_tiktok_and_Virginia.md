# Bits

Beau says:

- Governor Yonkin campaigned on giving parents a say in education, only to have the state take full control and declare certain topics off-limits as thought crimes.
- Yonkin aims to eliminate divisive subjects like history and literature to shape young minds by restricting access to information.
- In an effort to bolster state power, citizens are encouraged to report on each other through an email address established by the state.
- TikTokers flooded the state's education email address with misinformation, overwhelming the system and rendering it ineffective.
- This form of activism is a temporary solution to challenge policies aiming to manipulate education and limit historical truths.

# Quotes

- "You know, if you want to have thought crime, you gotta have the thought police."
- "People just have to spend too much effort sifting through the information."
- "We have to hide our history because we can't be proud of it, I guess."
- "If you teach that, students might want to continue to change, and a whole bunch of people at the top who enjoy the status quo, well, they really don't want that, now do they?"

# Oneliner

Governor Yonkin manipulates education, restricting historical truths and encouraging citizens to report on each other, countered by activists flooding the state's email with misinformation.

# Audience

Educators, Activists, Citizens

# On-the-ground actions from transcript

- Flood official channels with information to overwhelm systems (exemplified)

# Whats missing in summary

The emotional impact of restricting education and historical truths, the dangers of manipulating information for political gain.

# Tags

#Education #GovernorYonkin #ThoughtPolice #Activism #Misinformation