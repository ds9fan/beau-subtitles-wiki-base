# Bits

Beau says:

- There is a group of eight or nine people in Alabama who want to make positive changes in their community, such as putting in wheelchair ramps, helping single moms, and paying off fines for people leaving jail to prevent them from getting stuck in the system.
- They face challenges in fundraising for their activities, especially in an incredibly religious area where helping single moms and criminals might be seen negatively.
- Despite having assets like the ability to make T-shirts and a relative with a stand at a flea market, they struggled to raise funds through progressive-themed merchandise.
- They considered selling right-wing merchandise to fund their left-wing activities, but faced backlash online for compromising their values.
- Beau suggests that if making and selling right-wing merchandise is what it takes to fund their projects and spread progressive ideas, they should go ahead with it.
- He argues that in a war on poverty situation, they should use any tool available to them, even if it means compromising on ideological purity.
- Beau encourages them to prioritize helping people in need over passing an ideological purity test imposed by others online.
- He points out the importance of taking money from opponents legally to do good work, spread progressive messages, and challenge those profiting from selling right-wing merchandise.
- Beau advises not to disclose this fundraising method to those who may have issues with it, focusing instead on the positive impact their projects will have on the community.
- Drawing parallels to Elvis facing resistance in the South, Beau urges the group to focus on their mission of helping those in need, even if it means making uncomfortable compromises.

# Quotes

- "Wars cost money. And if you are, as they are, behind enemy lines, surrounded, use any tool the opposition will give you."
- "People get their fines paid. People get their ramps. Kids get food. Seems like a small price to pay."
- "You're surrounded. You are outnumbered. You're outfinanced."

# Oneliner

A group in Alabama faces fundraising challenges for their community projects, prompting Beau to advocate for utilizing any means necessary, including selling right-wing merchandise, to support their vital initiatives.

# Audience

Community activists

# On-the-ground actions from transcript

- Sell merchandise at flea markets to raise funds (suggested)
- Prioritize community projects over ideological purity tests (implied)

# Whats missing in summary

The importance of prioritizing community impact over ideological purity and utilizing creative means to fund vital projects.

# Tags

#CommunityActivism #FundraisingChallenges #IdeologicalPurity #ProgressiveIdeas #SupportingCommunity