# Bits

Beau says:

- A bill in California, AB257, passed in the state assembly and will impact over half a million fast food workers if it goes through the Senate.
- The bill sets up a council to protect fast food workers, ensuring fair pay, wage standards, training, safety, and more.
- This council will serve as a de facto union representative for the industry.
- Joint liability is created between franchisees and big companies for labor violations at fast food joints.
- Big companies might push back, claiming they lack control over local franchises, but standardization across all locations implies influence.
- The consistency in fast food chains suggests that big companies have more influence than they claim.
- The big companies demand uniformity to create familiarity and bring customers back.
- Money seems to be the primary reason for big companies not ensuring fair treatment of workers.
- Big companies are expected to pour money into defeating the bill, indicating awareness of widespread violations.
- Judging by the effort to defeat it, the bill's worth can be inferred.

# Quotes

- "It's the big company that demands that because they know that familiarity is what brings people back."
- "Sometimes, you can judge the worth of a bill by how much effort goes into defeating it."

# Oneliner

A bill in California aims to protect over half a million fast food workers by establishing a council and joint liability between franchisees and big companies.

# Audience

Californian residents

# On-the-ground actions from transcript

- Review AB257 and reach out to state Senators in support of the bill (suggested)
- Make a call, send an email, or tweet to show support for the bill (suggested)

# Whats missing in summary

The full transcript provides a deeper understanding of the influence big companies have on fast food workers' treatment and the importance of supporting legislation like AB257.

# Tags

#California #FastFoodWorkers #LaborRights #JointLiability #SupportLegislation