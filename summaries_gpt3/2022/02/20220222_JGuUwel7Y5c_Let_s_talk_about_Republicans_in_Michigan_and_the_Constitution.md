# Bits

Beau says:

- Three Republican candidates for Attorney General in Michigan were asked about Griswold v. Connecticut, known as the birth control case for striking down a law prohibiting married couples from obtaining birth control.
- The Supreme Court didn't establish a right to birth control in Griswold v. Connecticut but found a right to marital privacy based on the first, third, fourth, and fifth amendments.
- The court's decision on marital privacy was rooted in previous cases like Meyer v. Nebraska and Pierce v. Society of Sisters, which established parental rights.
- Republicans who oppose the right to marital privacy are ultimately seeking to control what happens in people's bedrooms as a way to control their lives.
- By opposing the right to marital privacy, the Republican candidates for Attorney General in Michigan are going against fundamental rights protected by the Constitution.
- Republicans' focus on undermining privacy rights is driven by their desire for control and maintaining the status quo.
- Their arguments about states' rights are essentially about giving states the power to restrict freedoms rather than expand them.
- The push against privacy rights is not just about birth control but about the broader idea of government interference in personal matters.
- Republicans' efforts against privacy rights are seen as a strategy to pave the way for anti-LGBTQ legislation and further control over individuals' lives.
- The candidates' stance against privacy rights is a deliberate move that could lead to increased state control over individuals in all aspects of life.

# Quotes

- "Republicans don't care about rights or freedom or the Constitution."
- "They want things to stay the way they are."
- "If you are concerned by the undermining of privacy in this country, this is something you need to pay attention to."
- "They want the state to have the right to determine what goes on inside your bedroom."
- "The states want to be able to exercise control over you in every facet."

# Oneliner

Three Michigan Attorney General candidates opposing privacy rights reveal a deeper agenda of control and undermining fundamental freedoms.

# Audience

Michigan Voters

# On-the-ground actions from transcript

- Pay attention to candidates' stances on privacy rights and fundamental freedoms (suggested).
- Advocate for the protection of privacy rights and constitutional freedoms in political discourse (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how political candidates' stances on privacy rights can reveal deeper intentions and threats to individual freedoms. 

# Tags

#Michigan #AttorneyGeneral #PrivacyRights #Constitution #Republicans