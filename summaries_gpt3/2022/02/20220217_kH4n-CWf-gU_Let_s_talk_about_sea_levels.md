# Bits

Beau says:

- Outlines a new report on sea levels released by six US government agencies.
- Notes a concerning trend of sea level rise being greater than previously thought.
- Mentions the expected increase in sea levels over the next 30 years, surpassing the rise seen in the entire 20th century.
- Details the average rise of sea levels around the US as 10 to 12 inches, with some areas facing up to 18 inches.
- Projects even higher sea level rises when looking beyond 30 years, particularly by 2060.
- Points out specific areas like Galveston and St. Petersburg facing substantial rises in sea levels.
- Emphasizes the impact of rising sea levels on buildings, homes, and commerce, especially when combined with storm surges.
- Quotes a National Ocean Service representative stating that sea level rise is already happening.
- Urges for immediate action to address climate change and its consequences.
- Advocates for making climate change a significant campaign issue in upcoming elections.

# Quotes

- "Sea level rise is upon us."
- "Once it starts rolling, you can't just stop it when you decide it gets too bad."
- "We are seeing the impacts of climate change today, now."
- "If we wait for them to get unbearable [...] it's gonna be too late to really stop it."
- "This is something that needs to be a campaign issue in the midterms."

# Oneliner

A call to action on rising sea levels and climate change impacts, urging immediate attention and political action.

# Audience

Climate advocates, voters

# On-the-ground actions from transcript

- Contact local representatives to prioritize climate change in upcoming elections (implied)

# Whats missing in summary

The urgency and importance of taking immediate action on climate change, using sea level rise as a tangible example.