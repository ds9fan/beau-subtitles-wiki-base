# Bits

Beau says:

- Talks about historical realities and the Russian and U.S. Strategic Command being on alert daily.
- Mentions Putin's announcement of putting his nuclear arsenal on alert, which is not a significant change in posture.
- Emphasizes the importance of knowledge in alleviating concerns, especially in the event of a nuclear blast.
- Criticizes traditional training on nuclear blast response for providing too much information that may hinder decision-making.
- Simplifies the response to a nuclear blast into two rings: "you felt it" and "you saw it."
- Explains actions to take if you are in the "you felt it" ring, including seeking medical attention.
- Provides guidance for those in the "you saw it" ring, debunking Hollywood myths about fallout and advising to seek shelter in a sturdy building.
- Advises staying away from windows, being in the center of the building, and taking precautions against fallout.
- Recommends using dirt to cover windows as a barrier against radiation.
- Suggests actions to take if caught outside during fallout, such as getting inside immediately and removing contaminated clothing carefully.
- Encourages listening to battery-powered radios for civil defense instructions and staying tuned for guidance.
- Assures that most cars should still function post-blast, contrary to movie depictions of EMP effects.
- Stresses the importance of limiting exposure to fallout and waiting for further information before taking action.
- Mentions the survival rates post-Little Boy bombing and the significance of staying inside to limit exposure.
- Recommends accessing archive.org for more information on nuclear blast survival strategies.

# Quotes

- "Your key thing is to limit your exposure to fallout for as long as possible."
- "Either you're here or you're not."
- "It's the Cold War."

# Oneliner

Beau simplifies nuclear blast response into two rings, advises on seeking medical attention or finding shelter, and stresses the importance of limiting fallout exposure.

# Audience

Emergency responders and civilians

# On-the-ground actions from transcript

- Seek medical attention if in the "you felt it" ring (implied)
- Find a sturdy building and shelter inside if in the "you saw it" ring (implied)
- Listen to battery-powered radios for civil defense instructions (implied)
- Limit exposure to fallout by staying indoors (implied)

# Whats missing in summary

Detailed information on varying survivability statistics for different rings and further resources available on archive.org.

# Tags

#NuclearBlast #EmergencyResponse #FalloutProtection #CivilDefense #SurvivalStrategies