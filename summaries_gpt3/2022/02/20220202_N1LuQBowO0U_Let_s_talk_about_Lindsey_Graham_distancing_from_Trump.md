# Bits

Beau says:

- Lindsey Graham is making strategic moves within the Republican Party, distancing himself from Trump and Trumpism.
- Graham, a long-time politician, is positioning himself away from Trump and those similar to him.
- Other Republicans, including Dan Crenshaw, are also starting to distance themselves from Trump.
- The Republican Party is akin to a mob where loyalty is based on benefits and performance.
- Trump is losing support within the party due to his dwindling numbers and potential legal issues.
- Clones of Trump, aspiring governors, are not politically viable as they lack the polish and may face backlash from Trump himself.
- Graham's actions, such as condemning Trump's statements and supporting Biden, are strategic moves to distance himself.
- Republicans are likely to distance themselves from Trump and his ideologies due to potential future consequences.
- The establishment Republicans have learned not to underestimate Trumpism after being surprised by Trump's initial rise.
- There is a shift happening within the Republican Party away from Trump and towards a different direction.

# Quotes

- "The Republican Party is a lot like the mob. You are only as good as your last envelope."
- "He is definitely starting to distance himself in a lot of different ways. Not just from Trump, but from those like Trump."
- "Former President Trump, well, he is a petty, petty man."
- "They probably understand that the clones of Trump, the various governors who want to be the next Trump, they probably understand that they're not politically viable either."
- "I don't think that the establishment Republicans are going to underestimate Trumpism again."

# Oneliner

Lindsey Graham and other Republicans strategically distance themselves from Trump and Trumpism within the Republican Party, foreseeing potential consequences and shifts in direction.

# Audience

Republican voters

# On-the-ground actions from transcript

- Position yourself strategically within your community or organization (implied).

# Whats missing in summary

Insights on the potential implications of the Republican Party distancing itself from Trump and the impact on future political landscapes.

# Tags

#RepublicanParty #LindseyGraham #Trump #Trumpism #GOP