# Bits

Beau says:

- Biden administration authorized a $30 million grant program for harm reduction in addiction.
- Grant program had eight different categories for spending, including safe smoking kits.
- Headlines falsely claimed $30 million spent on crack pipes to provoke outrage.
- Other approved uses of the grant money include harm reduction vending machines, medications, condoms, and more.
- Framing of the coverage was inaccurate and designed to provoke outrage among the Republican base.
- The history of othering people with addiction issues and the failure of the war on drugs are discussed.
- Beau advocates for harm reduction strategies as the future of combating addiction.
- Safe smoking kits do not necessarily mean pipes; they can include protective rubber pieces.
- The inaccurate coverage does not represent the future of addiction combatting efforts in the country.

# Quotes

- "Manufactured outrage, because the Republican base now, it has to be angry."
- "The war on drugs is an unmitigated failure. We lost. The plants won, right?"
- "This is what the future of combating addiction in the country is going to look like."

# Oneliner

The Biden administration's $30 million grant program for harm reduction in addiction was inaccurately framed as purchasing crack pipes to provoke outrage, but it actually includes various harm reduction strategies representing the future of combating addiction.

# Audience

Advocates for harm reduction

# On-the-ground actions from transcript

- Advocate for harm reduction strategies in combating addiction (implied)
- Educate others on the importance of harm reduction programs (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the inaccuracies in media coverage surrounding the Biden administration's grant program for harm reduction and advocates for effective harm reduction strategies in combating addiction.

# Tags

#BidenAdministration #HarmReduction #WarOnDrugs #MediaCoverage #AddictionCombat