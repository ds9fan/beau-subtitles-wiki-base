# Bits

Beau says:

- Recapping how to stay warm during a power outage in extreme cold, focusing on dressing in layers and heating a small area.
- Designating a room away from exterior doors and windows to keep warm and setting up a tent inside with extra insulation layers.
- Using a candle for additional heat but being cautious with fire hazards.
- Creating a makeshift tent by moving a dining room table into the designated room and insulating it with blankets and clothes.
- Heating rocks or smaller pots outside the home to bring warmth inside, being cautious of potential explosions.
- Ensuring the designated room is big enough and comfortable for everyone, avoiding sweating from overdressing, and staying hydrated.
- Reminding to take medications and checking on neighbors who may need help during outages.
- Emphasizing community support and proactive action during prolonged outages, as government assistance may be delayed or insufficient.
- Urging immediate action on the prepared plan once an outage starts, regardless of the expected duration or government response.
- Advising to practice the preparedness plan even if the power is restored quickly, to be ready for future outages and starting the setup while the home is still warm.

# Quotes

- "Sometimes there is justice, and sometimes there is just us."
- "The government's not coming to help. Ask the people in Texas."
- "Don't wait for a response from them. As soon as it starts, put your plan into action."
- "Because getting this set up while your home is still somewhat warm, that's ideal."
- "Y'all have a good day."

# Oneliner

Recapping tips for staying warm during a power outage in extreme cold, urging proactive community action and caution with fire hazards.

# Audience

Community members

# On-the-ground actions from transcript

- Designate a warm room away from doors and windows with insulation layers (suggested).
- Set up a makeshift tent using blankets and clothes for extra insulation (implied).
- Heat rocks or pots outside to bring warmth inside (suggested).
- Practice the preparedness plan immediately when an outage starts (suggested).
- Stay hydrated and ensure everyone's comfort and safety in the designated warm room (implied).

# Whats missing in summary

Tips on dealing with power outages, community support, and proactive preparation for extreme cold situations.

# Tags

#PowerOutage #CommunitySupport #ColdWeatherSafety #EmergencyPreparedness #ProactiveAction