# Bits

Beau says:

- Critiques the Republican habit of creating legislation allowing people to sue each other in cases where damages wouldn't normally exist.
- Provides examples to illustrate the potential consequences of such legislation, such as holding someone accountable for leaving keys in a stolen car that causes harm.
- Argues that the civil system already covers damages and penalties, making additional legislation unnecessary.
- Points out the flaws in assuming that this legislation will be used wisely, citing past examples where it was not.
- Raises concerns about the inequality that monetary penalties create, making justice a privilege for the wealthy.
- Warns against making the justice system about money, as it allows the wealthy to circumvent consequences.
- Advocates for changing behavior through education rather than coercive measures like monetary penalties.
- Emphasizes the danger of using legislation as a tool to target marginalized individuals who lack resources to defend themselves.
- Expresses skepticism about the effectiveness of legislative changes in influencing behavior, suggesting that education is a more sustainable approach.

# Quotes

- "If you have a system that is based on money, the way this legislation proposes, you create two sets of people."
- "You cannot make the justice system about money."
- "It's not a penalty, it's a fee to do whatever they want and get away with it."
- "In an ideal world, changing behavior is done by convincing through education, not coercive means."
- "This is a tool to kick down those who don't have the means to fight back."

# Oneliner

Beau criticizes Republican legislation allowing frivolous lawsuits, pointing out flaws in enforcement and advocating for education over coercive measures in changing behavior.

# Audience

Legislators, activists, community organizers

# On-the-ground actions from transcript

- Challenge legislation that allows frivolous lawsuits (implied)
- Advocate for education-based approaches to behavior change (implied)
- Support marginalized individuals who may be targeted by unjust laws (implied)

# Whats missing in summary

The full transcript provides in-depth analysis and examples on the dangers of legislation enabling frivolous lawsuits and the implications for justice and equality.