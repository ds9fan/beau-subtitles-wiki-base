# Bits

Beau says:

- Explains why countries deviate from the playbook despite knowing it.
- Mentions the Canadian truckers' situation and indigenous causes as examples.
- Points out that racism and capitalism play a significant role in these deviations.
- Illustrates how movements are seen as threats to the status quo.
- Emphasizes that the response to movements depends on their perceived impact on the existing system.
- Compares the relevance of truckers to the status quo with that of indigenous causes.
- Notes the high vaccination rate in Canada and the expected relaxation of restrictions without protests.
- Stresses the difference in treatment between movements advocating for the status quo versus those aiming for real change.
- Addresses the power dynamics and historical overreactions by governments.
- Encourages individuals, especially from the majority group, to show up in support of marginalized groups to counter public opinion manipulation.

# Quotes

- "The reason it historically gets visited upon marginalized groups is because they're smaller in number."
- "Marginalized groups tend to be on the right side of history."
- "If you ever needed a reason to show up, that's it."

# Oneliner

Countries deviate from the playbook based on perceived threats to the status quo, with racism and capitalism playing pivotal roles, impacting responses to movements advocating for real change versus those supporting the system.

# Audience

Activists, Allies, Advocates

# On-the-ground actions from transcript

- Show up in support of marginalized groups (implied)
- Attend protests and demonstrations to counter public opinion manipulation (implied)
- Join movements advocating for real change (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of how power dynamics, racism, capitalism, and threats to the status quo influence government responses to different movements.

# Tags

#DeviationFromPlaybook #Racism #Capitalism #MarginalizedGroups #Support