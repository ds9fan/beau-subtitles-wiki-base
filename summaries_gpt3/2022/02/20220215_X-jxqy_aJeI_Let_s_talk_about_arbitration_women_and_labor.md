# Bits

Beau says:

- Explains the significance of labor legislation related to arbitration heading to President Biden's desk.
- Mentions the common inclusion of arbitration clauses in new hire packets.
- Describes arbitration as a private means of settling disputes outside the judicial system.
- Notes industries where arbitration makes sense, like those involving trade secrets or intellectual property.
- Points out that arbitration clauses are not suitable for disputes related to sexual harassment or assault.
- Emphasizes how the new legislation renders these clauses unenforceable, particularly benefiting victims of harassment.
- Acknowledges the long process behind this legislation, dating back to around 2017 or 2018.
- Comments on the importance of bringing accountability to those who previously avoided it through arbitration.
- Stresses that the new legislation will provide increased protection, especially for women working under powerful individuals.
- Concludes by wishing everyone a good day.

# Quotes

- "This legislation says those clauses, well, they can't be used anymore."
- "This is going to offer a wider level of protection, particularly for women who are working for rich and powerful men who think they can get away with anything."

# Oneliner

Beau explains the impact of impending labor legislation banning arbitration in cases of sexual harassment, providing vital protection for victims.

# Audience

Employees, advocates, activists

# On-the-ground actions from transcript

- Contact organizations supporting victims of sexual harassment to understand how this legislation can benefit them (suggested)
- Join advocacy groups pushing for similar legislative changes in other areas (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of the labor legislation related to arbitration, offering insights into its implications for workplace protections and accountability.