# Bits

Beau says:

- Hospital administrators, politicians, and executives are being called out for legislation designed to cap what companies can charge for travel nurses.
- The legislation could push nurses' wages down, impacting even those who aren't travel nurses.
- The focus of the legislation seems to protect profits rather than solve the real issues in the nursing industry.
- There is concern about patient ratios, which directly affect patient safety, not being addressed in the legislation.
- Beau suggests the need for an RN bill similar to the GI bill to alleviate the nursing shortage.
- The nursing shortage is partly due to how hospitals are run, leading to nurses leaving the profession for other jobs.
- Beau criticizes the prioritization of profit over the well-being of nurses and patients.
- Nurses face emotionally challenging situations regularly, like helping patients say goodbye without adequate support.
- The possibility of nurses organizing a protest on May 12, Florence Nightingale's birthday, is mentioned.
- Hospital administrators are advised to prepare for potential disruptions during this time.

# Quotes

- "There's nothing in it to, I don't know, address the widespread concern from nurses about patient ratios..."
- "It's just designed to protect the profits of the large companies at the expense of the little people."
- "For nurses, that's just Tuesday. That's just something they do."
- "I think they will do it. I disagree."
- "Not addressing any of the concerns of the nurses. Not doing anything to solve the nursing shortage, just moving to protect the profits of the large companies."

# Oneliner

Hospital administrators and lawmakers are criticized for legislation that protects profits over nurses' well-being, potentially leading to a protest on May 12.

# Audience

Nurses, healthcare workers

# On-the-ground actions from transcript

- Prepare for potential disruptions and show support for nurses during a possible protest on May 12 (implied).

# Whats missing in summary

The full transcript provides a deeper understanding of the challenges faced by nurses and the urgent need for legislative action to address their concerns effectively.

# Tags

#Nursing #Legislation #PatientSafety #Protest #HealthcareWorkers