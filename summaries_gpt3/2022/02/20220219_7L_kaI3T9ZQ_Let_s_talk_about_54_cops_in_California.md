# Bits

Beau says:

- Prosecutors uncovered a group of bad apples within the California Highway Patrol, dating back to 2016-2018.
- Allegations include an overtime scheme, falsifying work hours, issuing fake tickets, and involvement in a bribery scheme.
- 54 cops are set to be arraigned on March 17th and 18th for a total of 302 counts.
- The public's trust in law enforcement is eroding due to discrepancies between reports and cell phone footage.
- Beau calls for a shift towards consent-based policing and reallocating funds to better-trained professionals.
- He suggests establishing a more robust oversight system to ensure accountability for past actions.

# Quotes

- "A lot of bad apples."
- "It's probably time, past time, to rethink policing in this country."
- "This is a moment for law enforcement to change."
- "Reallocating some of the funding."
- "A whole lot of bad apples to be alleged."

# Oneliner

Prosecutors uncover 54 cops in California Highway Patrol for various misconduct, prompting Beau to call for a reevaluation of policing practices and funding allocation.

# Audience

Law Enforcement Reform Advocates

# On-the-ground actions from transcript

- Advocate for consent-based policing and reallocation of funds to better-trained professionals (suggested)
- Support the establishment of a more robust oversight system for accountability (suggested)

# Whats missing in summary

The emotional impact and detailed examples can be best understood by watching the full video. 

# Tags

#LawEnforcement #PoliceMisconduct #Accountability #FundingReallocation #Oversight