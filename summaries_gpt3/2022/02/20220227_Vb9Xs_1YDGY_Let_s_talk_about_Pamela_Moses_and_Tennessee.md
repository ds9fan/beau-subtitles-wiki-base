# Bits

Beau says:

- Overview of Pamela Moses' story in Shelby County, Tennessee regarding voting.
- Pamela Moses, an activist, ran for mayor in Memphis.
- She believed she was eligible to vote as she was off probation, supported by a certificate from the Department of Corrections.
- However, her voting rights were questioned, leading to a trial where she was sentenced to six years in prison for attempting to vote.
- The judge ordered the sentence vacated, granting a new trial due to a document not being turned over by the Department of Corrections.
- The document's content remains unknown, causing a shift in the case.
- The excessive nature of the original sentence is pointed out by Beau, suggesting it is cruel and unusual.
- Contrasts Pamela Moses' sentence with shorter sentences for those who attempted to undermine legal votes or used violence.
- Beau hints at a possible political motive behind the harsh sentence, implying Tennessee politics may be at play.
- Beau believes public awareness played a role in revisiting Pamela Moses' case.
- Encourages vigilance and ongoing attention to the case, indicating it's not yet resolved.

# Quotes

- "Six years is probably excessive."
- "This battle isn't won yet."
- "Let's not let this drop out of our eyesight here."

# Oneliner

Beau sheds light on Pamela Moses' voting rights battle in Tennessee, questioning the excessive sentence and hinting at potential political motives, urging continued attention to the case.

# Audience

Activists, Voters, Community Members

# On-the-ground actions from transcript

- Stay informed on Pamela Moses' case and outcomes (implied).

# Whats missing in summary

The emotional impact and frustration over the unjust system and the importance of public vigilance in holding authorities accountable.

# Tags

#VotingRights #Justice #Activism #CommunityPolicing #Tennessee