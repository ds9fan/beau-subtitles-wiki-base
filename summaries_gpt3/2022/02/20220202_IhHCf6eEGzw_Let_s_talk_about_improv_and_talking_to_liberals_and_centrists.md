# Bits

Beau says:

- Explains how to use British comedy and improv rules to reach centrists and liberals and guide them towards a more progressive position.
- The five rules of improv are discussed: "Yes, and", no open-ended questions, you don't have to be funny, make your partner look good, and tell a story.
- Emphasizes the importance of not making jokes or using in-group humor that the other person may not understand.
- Advocates for being kind, asking guiding questions, and moving slowly to help centrists and liberals shift towards a more progressive stance.
- Provides a practical example using the issue of police reform to demonstrate how to navigate a centrist position towards defunding the police through thoughtful questioning and agreement-building.

# Quotes

- "Yes, and move them."
- "Ask questions that drive them in the direction they want."
- "Nobody has the perfect take about everything."
- "Don't berate them."
- "Talk about it in terms of right and wrong."

# Oneliner

Using British comedy and improv rules, Beau guides how to gently steer centrists and liberals towards more progressive viewpoints through kindness, questions, and agreement-building.

# Audience

Activists, Progressives, Advocates

# On-the-ground actions from transcript

- Ask guiding questions to gently shift perspectives towards progressivism (exemplified).
- Avoid making jokes or using in-group humor that may alienate the listener (exemplified).
- Be kind and patient in guiding individuals towards more progressive viewpoints (exemplified).

# Whats missing in summary

Full understanding of Beau's practical approach to engaging centrists and liberals through kindness, questions, and agreement-building.

# Tags

#BritishComedy #ImprovRules #Progressivism #GuidingQuestions #Centrists