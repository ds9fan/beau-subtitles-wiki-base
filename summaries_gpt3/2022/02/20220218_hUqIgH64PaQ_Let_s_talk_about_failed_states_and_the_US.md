# Bits

Beau says:

- Explains the elements of a failed state: loss of control of territory, erosion of ability to make collective decisions, inability to provide public services, and inability to interact with other nations.
- Points out that losing the monopoly on violence is a significant warning sign of becoming a failed state.
- Notes that in the United States, the government is willingly giving away its monopoly on violence for political gains.
- Mentions how the government is encouraging non-state actors to enforce punishment, which is detrimental to preserving the republic.
- Talks about the erosion of the ability to make collective decisions, citing the active attempts to cast doubt on election results.
- References Texas as an example of the inability to provide public services.
- States that the United States can still interact with other nations due to its powerful military.
- Warns that political failures causing misuse of the military could lead other nations to avoid interacting with the US.
- Concludes that while the US is not currently a failed state, elements of becoming one are actively occurring due to certain political agendas.

# Quotes

- "The government is giving away its monopoly on violence."
- "A lot of the Republican agenda you're going to find out is guaranteed to destroy [the republic]."
- "The United States is definitely on that path."

# Oneliner

The United States is showing signs of becoming a failed state through the erosion of key elements, driven by political agendas.

# Audience

Citizens, policymakers, activists

# On-the-ground actions from transcript

- Monitor political actions and hold elected officials accountable for decisions that could lead to the erosion of democratic principles (implied).
- Advocate for policies that strengthen public services and ensure equal access for all citizens (implied).
- Support organizations working to uphold democratic values and principles in the face of political challenges (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the elements contributing to the potential decline of the United States into a failed state, urging viewers to stay vigilant and engaged in preserving democratic norms.

# Tags

#FailedState #MonopolyOnViolence #PoliticalAgenda #PublicServices #Democracy