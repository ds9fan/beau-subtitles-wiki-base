# Bits

Beau says:

- Trump's absence as president contributed to the ongoing conflict between Ukraine and Russia.
- The Kertch Strait incident in November 2018 marked the first time Russia and Ukraine shot at each other directly.
- Trump's inaction during his presidency allowed Russia to act without consequences.
- Russia used a program called Passports Dissertation during Trump's presidency to justify its actions in contested areas.
- Trump's lack of response to Putin's actions indicates he wouldn't have countered Russia's moves.
- Under Trump, there wouldn't have been a NATO response to Russia's actions.
- Trump consistently undermined NATO during his presidency.

# Quotes

- "Trump's absence as president contributed to the ongoing conflict between Ukraine and Russia."
- "The first time Russia and Ukraine shot at each other directly was under Trump."
- "Under Trump, there wouldn't have been a NATO response to Russia's actions."

# Oneliner

Trump's absence allowed Russia's actions in Ukraine, with no NATO response under his presidency.

# Audience

Foreign policy analysts

# On-the-ground actions from transcript

- Contact foreign policy experts for insights on potential impacts of leadership changes (suggested)
- Join organizations advocating for strong international alliances (suggested)
- Organize events to raise awareness about the importance of upholding international agreements (suggested)

# Whats missing in summary

The detailed analysis and context provided by Beau in the full transcript.

# Tags

#Ukraine #Russia #Trump #ForeignPolicy #NATO