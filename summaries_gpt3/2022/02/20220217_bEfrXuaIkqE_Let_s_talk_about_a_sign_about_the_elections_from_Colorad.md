# Bits

Beau says:

- Tina Peters, the Mesa County clerk in Colorado, known for echoing Trump's false election claims, is under investigation for alleged election security breaches.
- A judge barred Tina Peters from overseeing the election in her county and she was recently arrested on an obstruction charge.
- Tina Peters announced her candidacy for Secretary of State of Colorado, potentially putting the entire state's election under her control if she wins.
- This situation is not unique; similar instances are happening in other states where Trump loyalists are running for election control positions.
- The current Secretary of State of Colorado, Jenna Griswold, labeled Tina Peters as unfit for the role and a danger to Colorado elections.
- Jenna Griswold emphasized that Colorado needs a Secretary of State who upholds the will of the people and doesn't embrace conspiracies.
- Trump loyalists targeting Secretary of State positions in swing states is part of a strategy to control elections.
- It's vital for all citizens, especially in swing states, to pay attention to and prevent Trump loyalists from controlling state-level elections.
- Preventing Trump loyalists from controlling elections is key to defeating Trumpism, as altering outcomes through means other than the ballot may be attempted.
- Being vigilant and active in preventing Trump loyalists from gaining election control is necessary to safeguard democratic processes.

# Quotes

- "Colorado needs a Secretary of State to be the Secretary of State."
- "Making sure that Trump loyalists do not control the elections at the state level is an imperative."
- "Preventing Trump loyalists from controlling elections is key to defeating Trumpism."

# Oneliner

Tina Peters' candidacy for Secretary of State of Colorado raises concerns about election security and the spread of falsehoods, echoing a broader trend of Trump loyalists vying for election control positions nationwide.

# Audience

Voters, Activists

# On-the-ground actions from transcript

- Contact local election officials to raise concerns about election security and transparency (implied)
- Support candidates who prioritize upholding democratic processes and rejecting conspiracy theories (implied)
- Stay informed about candidates running for election control positions in your state and their stances on election integrity (implied)

# Whats missing in summary

The full transcript provides detailed insight into the concerning trend of Trump loyalists seeking election control positions and the importance of safeguarding democratic processes at the state level. Viewing the full transcript can offer a comprehensive understanding of the risks associated with individuals like Tina Peters gaining power in critical election roles.

# Tags

#ElectionSecurity #TrumpLoyalists #SecretaryOfState #Colorado #Democracy