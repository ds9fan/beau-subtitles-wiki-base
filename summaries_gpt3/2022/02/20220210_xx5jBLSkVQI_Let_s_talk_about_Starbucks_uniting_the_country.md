# Bits

Beau says:

- Starbucks employees are organizing themselves to start a union to advocate for better pay, benefits, and conditions.
- The idea that certain jobs are "lesser" and do not deserve fair compensation stems from societal beliefs about education and labor devaluation.
- Beau challenges the notion of "unskilled labor" and argues that every job requires skill of some kind.
- He points out the importance of valuing all types of work and ensuring that workers can live decently.
- Beau criticizes the mindset that suggests certain jobs do not require fair wages or benefits based on perceived hierarchy.
- He illustrates the impact of undervaluing labor on depressing wages and creating disparities between different professions.
- Beau questions the concept of unskilled labor, suggesting that it may refer more to uncredentialed work rather than lacking skills.
- He underscores the need for collective bargaining to achieve a decent standard of living for many workers in a wealthy country.
- Beau points out the concerning wealth gap between the haves and have-nots, indicating underlying issues in society.
- He challenges individuals to rethink their perspectives on labor, education, and societal inequalities.

# Quotes

- "The people who provide that service, they deserve a decent life."
- "Every job requires skill of some kind."
- "If the gap between the haves and the have-nots is that big, there's probably something wrong with it."
- "If you being one of the haves in your own eyes is dependent on somebody who provides you a service being a have-not, you're already a have-not."
- "Have you ever been to a new Starbucks, one that just opened up and all the employees are new? Does it run well? No."

# Oneliner

Starbucks workers organizing for better conditions challenge societal views on labor value and advocate for fair treatment in a wealthy country with widening wealth disparities.

# Audience

Workers, advocates, activists

# On-the-ground actions from transcript

- Support unionization efforts among workers (exemplified)
- Advocate for fair wages and benefits for all types of labor (implied)

# Whats missing in summary

The emotional depth and personal anecdotes shared by Beau are best experienced by watching the full transcript.

# Tags

#LaborRights #Unionization #SocietalInequality #FairWages #CollectiveBargaining