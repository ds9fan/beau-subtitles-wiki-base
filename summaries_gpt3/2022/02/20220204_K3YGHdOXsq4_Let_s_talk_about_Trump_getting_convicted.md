# Bits

Beau says:

- Explains the message he received from Germany questioning why Trump hasn't been indicted yet, given the number of people in prison.
- Mentions the running joke that a federal prosecutor can indict a ham sandwich, implying it shouldn't be difficult to indict Trump.
- Raises the question of whether they can convict Trump, especially with a portion of the jury pool still loyal to him.
- Comments on the Department of Justice (DOJ) possibly dragging their feet due to the challenges of convicting Trump.
- Expresses doubts about the committee's ability to craft a narrative that breaks the spell Trump has on his supporters.
- Emphasizes the committee's duty to inform the public and ensure accountability while reshaping Trump's image from "dear leader" to a threat to democracy.
- Suggests that the evidence alone may not be enough to secure a conviction due to partisan loyalty influencing jurors.
- Proposes potential strategies for DOJ, including slamming Trump with charges or getting prominent right-wingers to testify against him.
- Stresses the urgency for the committee to act swiftly in presenting a compelling case before DOJ proceeds.
- Concludes by underscoring the importance of constructing a cohesive narrative to sway those still under Trump's influence.

# Quotes

- "If they can indict a ham sandwich, odds are they can indict a mango Mussolini."
- "They have to be able to recast him from dear leader to the existential threat to the republic that he was."
- "They still have to account for that one person, one person on that jury can vote not guilty. Not based on the evidence, but based on partisan loyalty."
- "Because you have all of this evidence, you have all the reporting about it, everything that we've seen."
- "They have to say, this led to this, that led to this, that led to this."

# Oneliner

Beau analyzes the challenges in convicting Trump amidst partisan loyalty and the need for a compelling narrative to sway public opinion.

# Audience

Political analysts, activists, voters

# On-the-ground actions from transcript

- Push the committee to swiftly present a compelling case (implied)
- Advocate for prominent figures to assist in crafting a narrative against Trump (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the obstacles in convicting Trump and underscores the critical need for a persuasive narrative to counter his influence effectively.

# Tags

#ConvictingTrump #DOJ #NarrativeBuilding #PartisanLoyalty #Justice