# Bits

Beau says:

- Introduces Mariah Parker, also known as Lingua Franca, a County Commissioner in Athens-Clarke County, Georgia, who is a hip hop artist and nearing completion of her PhD.
- Mariah talks about her multifaceted roles as a public servant, artist, and scholar, including her recent re-election, upcoming album release, and advocacy for workers.
- She shares the inspiration behind her song "Work," which references the labor movement and calls for unity among workers.
- Mariah delves into the themes of her upcoming album, touching on historical and international movements for liberation and personal struggles.
- The interview covers Mariah's unique swearing-in ceremony with Malcolm X's autobiography and her priorities as County Commissioner, focusing on labor rights and collective bargaining.
- She expresses the need for direct action and community involvement to address issues like evictions and economic exploitation.
- Mariah reveals her plans for a potential freedom school and stresses the importance of using music and education for social change.
- The discussion extends to Mariah's work on police accountability, her reflections on the prison system, and her call for more creative forms of collective action.
- Beau inquires about Mariah's experiences as a new parent and her upcoming projects, including her podcast and album release.

# Quotes

- "I think using music as a tool to educate."
- "I just encourage people to think creatively about how that could be done."
- "You can use the master's tools to unmake the master's house."
- "I have a feeling that this is gonna take off in the sense that there's gonna be a coordinated push this year for organized labor and collective bargaining."
- "I hope that was enlightening and entertaining."

# Oneliner

Beau interviews Mariah Parker, a County Commissioner and hip hop artist, discussing her multifaceted roles, advocacy for workers, and plans for a potential freedom school to fight for collective liberation through music and education.

# Audience

Activists, Community Members

# On-the-ground actions from transcript

- Support worker organizing efforts in your community by encouraging collective bargaining and the formation of worker-owned cooperatives (implied).
- Engage in direct action to protect individuals facing eviction by showing up and preventing their displacement (implied).
- Take part in creative forms of collective action, such as blocking eviction proceedings or protesting against harmful development projects, to address urgent social issues (implied).

# Whats missing in summary

A deeper exploration of Mariah's journey into politics, music, and academia, along with her emphasis on embracing personal struggles to drive meaningful change.

# Tags

#CountyCommissioner #HipHopArtist #LaborRights #CollectiveAction #CommunityOrganizing