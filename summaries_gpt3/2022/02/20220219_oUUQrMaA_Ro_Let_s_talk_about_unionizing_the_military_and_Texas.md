# Bits

Beau Young says:

- Explains the exception to federal troops unionizing and how troops can unionize when on state active duty.
- Mentions the hypothetical scenario of National Guard troops in Texas being called up for a political mission called Operation Lone Star.
- Notes that Operation Lone Star has faced complaints about various issues like living conditions, pay, leadership, and equipment.
- Points out that the Texas State Employees Union Military Caucus is set to meet next week, providing an avenue for National Guard troops to organize.
- Describes the ongoing disaster of Operation Lone Star and how the Texas military department has claimed helplessness in addressing the issues.
- Suggests that with the troops potentially gaining representation through the union, it might be wise for the governor to cancel the mission to avoid further repercussions.
- Mentions that those involved in organizing the union are maintaining a low profile and speaking anonymously to the press due to fears of retaliation from the Texas Military Department.
- Raises the question of whether the people of Texas will support the troops or the politicians who have failed to address the situation adequately.

# Quotes

- "Troops can unionize when they are on state active duty."
- "Operation Lone Star has been plagued by complaints about living conditions, pay, leadership, mission scope, equipment, pretty much everything."
- "It's probably a really good idea for the governor to cancel this little stunt before they get representation."
- "What's gonna happen when the people of Texas have to decide whether to support the troops or support the politicians who couldn't even keep their lights on."

# Oneliner

Troops in Texas face challenges organizing under an existing union due to ongoing mission issues, raising questions about support from politicians.

# Audience

Texans, National Guard members

# On-the-ground actions from transcript

- Attend and support the Texas State Employees Union Military Caucus meeting next week (exemplified).
- Advocate for better living conditions, pay, leadership, and equipment for National Guard troops (exemplified).
- Call for transparency and accountability from the Texas Military Department (exemplified).

# Whats missing in summary

Full context and nuances of ongoing issues faced by National Guard troops in Texas.

# Tags

#Texas #NationalGuard #Unionizing #StateActiveDuty #MilitaryCaucus