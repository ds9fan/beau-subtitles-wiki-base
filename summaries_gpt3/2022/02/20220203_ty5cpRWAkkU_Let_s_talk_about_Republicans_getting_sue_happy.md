# Bits

Beau says:

- Explains the connection between new laws proposed by Republicans and his piece of the Berlin Wall keychain from East Germany.
- Describes the new method Republicans are using to push their agenda through state levels by allowing citizens to sue.
- Draws parallels between the authoritarian tactics of the Stasi in East Germany and the incentivized citizen lawsuits in the US.
- Warns about the dangerous implications of these laws, which aim to circumvent constitutional frameworks and manipulate citizens into enforcing state desires.
- Raises concerns about the chilling effect on freedom of speech and behavior, as citizens could be sued for things deemed offensive or against state preferences.
- Predicts a slippery slope where the lawsuit mechanism can be expanded to target various behaviors beyond just teaching subjects like gender.
- Compares the incentivized citizens under these laws to those who collaborated with the Stasi in East Germany, betraying neighbors for personal gain or to avoid trouble.
- Emphasizes that once such machinery is introduced, it will inevitably be used against those who initially supported it, leading to a more authoritarian society.
- Concludes by drawing attention to the historical tactic used by authoritarian regimes to control and suppress dissent by turning citizens against each other for rewards.

# Quotes

- "They can't really do anything about it, not directly. It would get shot down in the courts. But if they just created a mechanism for citizens to sue that teacher, well that would have a chilling effect."
- "It's how they chill speech in society where only your betters get to decide what you can do, what you can talk about, what is socially acceptable, and what behaviors you need to engage in."
- "Believe me, for $10,000, your neighbor will probably turn you in."

# Oneliner

New laws incentivizing citizen lawsuits mirror authoritarian tactics, chilling speech and behavior, ultimately turning neighbors against each other.

# Audience

Citizens, Activists

# On-the-ground actions from transcript

- Organize community education sessions on the importance of protecting freedom of speech and resisting authoritarian measures (suggested).
- Join or support organizations advocating for civil liberties and protection against state overreach (exemplified).
- Foster a strong community network that resists divisive tactics aimed at turning citizens against each other for personal gain (implied).

# Whats missing in summary

The full transcript provides a deep dive into the historical context of authoritarian control and the potential consequences of incentivized citizen lawsuits on society. Watching the full video can offer a more comprehensive understanding of these complex issues.

# Tags

#Authoritarianism #FreedomOfSpeech #IncentivizedLawsuits #HistoricalParallels #CommunityResistance