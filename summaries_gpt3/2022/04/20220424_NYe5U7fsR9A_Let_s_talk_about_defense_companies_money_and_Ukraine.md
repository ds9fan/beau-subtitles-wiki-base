# Bits

Beau says:

- Explains the common talking point that the US provides military aid to help defense contractors, acknowledging its accuracy at times.
- Points out that defense companies profit from war, which can be seen as unethical or as blood money.
- Mentions the military-industrial complex created by the defense industry and Congress, leading to continuous cycles of profit.
- Provides a perspective on the amount of aid provided to Ukraine compared to the revenue of major defense contracting companies.
- Emphasizes that the aid to Ukraine, at $3 billion, is a very small percentage of the total revenue of these companies.
- States that for big defense contracting companies like Raytheon, the aid amount is insignificant and more like a tax write-off.
- Notes that while some companies may profit from developing new military technology like drones, they often end up being acquired by larger corporations.
- Suggests that while US policy is sometimes influenced by money in the defense industry, in this case, the aid to Ukraine is not a significant money-making venture for these companies.
- Warns about potential future increased spending in the defense industry post-conflict, where companies might profit substantially.
- Anticipates that if defense industry interests start influencing aid packages significantly, it will lead to a surge in total spending.

# Quotes

- "Defense companies make money off of war. It's literally the drive behind their product."
- "The amount of aid that we have provided to Ukraine is what gets rounded off when you're talking about their revenue."
- "To them, that's a tax write-off."
- "This isn't defense industry money."
- "It's not really happening with the aid."

# Oneliner

Beau explains the nuances of US military aid, clarifying that while defense contractors profit from war, the aid to Ukraine is not a significant money-maker for them.

# Audience

Public, Policy Makers

# On-the-ground actions from transcript

- Monitor future spending post-conflict for potential excessive defense industry influence (implied).
- Stay informed about military aid and defense industry dynamics (implied).

# Whats missing in summary

Insight into potential future implications of defense industry influence on military aid and spending.

# Tags

#USmilitaryaid #DefenseContractors #MilitaryIndustrialComplex #DefenseIndustryInfluence #FutureSpending