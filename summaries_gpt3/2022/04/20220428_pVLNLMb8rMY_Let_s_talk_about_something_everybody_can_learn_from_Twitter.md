# Bits

Beau says:

- Twitter's global reach means it must abide by various countries' laws to remain viable.
- Musk has committed to following local laws that may restrict certain content on Twitter.
- Total deregulation on social media could lead to toxicity, causing marginalized groups to leave first.
- Diversity of thought is vital for social media platforms to thrive.
- Interesting individuals leaving a platform can result in its demise, as seen in social media history.
- Users hold the collective power that makes social media networks influential.
- Collective action by users can drive policy changes on platforms like Twitter.
- Social media networks prioritize making money, necessitating a diverse user base for success.
- Beau is not concerned about worst-case scenarios on social media platforms.
- Users have the ability to shape the direction and success of social media networks through their presence and actions.

# Quotes

- "You make Twitter powerful, not the other way around."
- "Social media networks run on Highlander rules."
- "Your work created that, and they're giving you back some of it."
- "Making money on social media requires a lot of users. That means you need a diverse group."
- "That new network, it gets its power from you, not the other way around."

# Oneliner

Twitter's global reach mandates adherence to local laws, while user diversity and collective action shape social media power dynamics.

# Audience

Social media users

# On-the-ground actions from transcript

- Advocate for diverse voices and viewpoints on social media platforms (implied)
- Participate in collective actions to influence policy changes on social media networks (implied)
- Support marginalized groups and ensure their voices are heard on social media (implied)

# Whats missing in summary

The full transcript provides deeper insights into the importance of user actions in shaping social media platforms and the potential consequences of neglecting diversity and moderation.

# Tags

#SocialMedia #Diversity #PowerDynamics #CollectiveAction #UserInfluence