# Bits

Beau says:

- West Virginia is getting a massive 3,000 acre solar panel farm built on top of an old coal mine.
- The energy sector is facing the issue of stranded assets as the world shifts away from dirty energy sources.
- Stranded assets occur when investments made in an asset no longer produce the expected returns due to changes in the market.
- Companies may face liabilities if the value of their assets drops significantly, making them worth less than what was spent on them.
- As the world moves away from coal and oil, the demand for these products decreases, leading to assets becoming stranded.
- Personal investments in solar panels can also impact traditional energy companies, affecting their expected revenue.
- Beau finds it noteworthy that a former coal mine is being repurposed for a solar panel farm, showcasing adaptation to changing energy trends.
- States reliant on supplying energy can still thrive by transitioning to cleaner energy sources.
- Beau suggests re-evaluating investments in oil and dirty energy companies due to the impending shift towards cleaner energy.
- While not an expert, Beau encourages awareness of the potential impact of stranded assets in the energy sector.

# Quotes

- "West Virginia is getting a 3,000 acre solar panel farm. 3,000 acres, that is huge!"
- "As the world moves away from dirty energy, a whole lot of stuff isn't going to have the value it used to."
- "The state just has to keep up with times, and it looks like West Virginia is at least going to make the attempt to do that."
- "Y'all have a good day."

# Oneliner

West Virginia transitions from coal to solar, signaling the rise of stranded assets in the shifting energy sector and urging investment reconsideration towards cleaner energy.

# Audience

Investors, Energy Sector

# On-the-ground actions from transcript

- Re-evaluate investments in oil and dirty energy companies (suggested)
- Support and advocate for the transition to cleaner energy sources in your community (implied)

# Whats missing in summary

The full transcript provides a deeper insight into the challenges and opportunities arising from the transition to cleaner energy sources and the implications for various stakeholders in the energy sector.