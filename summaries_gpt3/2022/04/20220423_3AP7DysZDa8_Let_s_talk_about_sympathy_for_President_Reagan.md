# Bits

Beau says:

- A high school student asked for help with a project on President Reagan depicting him as a hero or villain.
- The replies on Twitter gave the student a list of Reagan's deeds and misdeeds to help with the project.
- Beau criticizes the assignment for framing historical figures as heroes or villains, calling it mythology rather than history.
- He warns of the danger of overlooking the flaws of historical figures when classifying them in simplistic terms.
- Reagan, often depicted as a hero in American history mythology, had misdeeds that could be glossed over.
- Beau points out that historical figures like Reagan were flawed individuals, not purely good or evil.
- He cautions against viewing historical figures as gods or devils, urging to understand them as flawed people.
- Beau advocates using historical figures as teaching tools to learn from their mistakes and not repeat them.
- He warns against perpetuating the myth that historical figures were faultless, as this can lead to dangerous black-and-white thinking.
- Beau encourages acknowledging the flaws of historical figures and using them to learn deeper aspects of history.

# Quotes

- "But the reality is that's not history, that's mythology."
- "It's fine to use people as a teaching tool to get people to understand the story, the general narrative."
- "Every cop is a criminal, and all the sinner saints."
- "Historical figures were just people, not gods, not deities, not devils."
- "We can't shape history that way, because then it's not history, it's mythology."

# Oneliner

A critical reflection on the dangers of simplistically categorizing historical figures as heroes or villains, urging to view them as flawed individuals to learn from their mistakes.

# Audience

Students, Educators, Historians

# On-the-ground actions from transcript

- Question assignments that oversimplify historical figures as heroes or villains (exemplified).
- Encourage critical thinking in historical analysis (implied).
- Teach history as a complex narrative of real individuals with flaws and virtues (exemplified).

# Whats missing in summary

Importance of learning from the mistakes of historical figures to avoid repeating them in the present and future.

# Tags

#History #HistoricalFigures #Teaching #CriticalThinking #Mythology