# Bits

Beau says:

- Ukraine events have influenced U.S. training at the National Training Center.
- U.S. forces are currently at war with a fake country, Danovia, mimicking Russia.
- Russian combat tactics, including shelling civilian areas, are integrated into U.S. training.
- Danovians in the scenario will have cell phones and focus on information operations.
- Individuals can shape narratives through social media, impacting the outcome of events.
- Social media empowers non-state actors to generate propaganda and shape narratives.
- The importance of social media in influencing thought and creating change is recognized by the U.S. military.
- Activism and sharing information online can have a significant impact on society.
- Social media is a powerful tool that should be acknowledged in strategies for creating a better world.
- The U.S. military is strategizing on countering social media influence in conflicts.

# Quotes

- "Information is power."
- "Your cell phone, your ability to document what's happening and get that record created and put it out on social media, That is, that's so important."
- "Social media has handed individual people, non-state actors, the same power to generate propaganda, negative or positive."
- "Your activism, your desire for that better world. Your strategies for achieving it should probably acknowledge this."
- "On a long enough timeline, we win."

# Oneliner

Ukraine events shape U.S. training at National Training Center, integrating lessons on combat tactics and information operations, showcasing the power of social media in shaping narratives and fostering change.

# Audience

Activists, Social Media Users

# On-the-ground actions from transcript

- Document and share information on social media platforms to shape narratives and create awareness (exemplified).
- Utilize social media for activism and advocating for positive change in society (exemplified).

# Whats missing in summary

The full transcript delves into the significance of social media in shaping narratives, the power of individual activism, and the evolving nature of information warfare in modern conflicts.

# Tags

#Ukraine #USMilitary #SocialMedia #Activism #InformationWarfare