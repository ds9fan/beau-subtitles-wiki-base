# Bits

Beau says:

- A trend of removing texts from libraries is not new in the US, with 2021 seeing 1,597 challenges or removals according to the American Library Association.
- Librarians have banned book week to bring attention to frequently challenged texts.
- It has become increasingly difficult to access certain texts in many places.
- The Brooklyn Public Library offers a virtual library card for individuals aged 13-21, granting access to 400,000 e-books, audio books, and other online resources.
- To obtain the virtual library card, individuals can email booksunbanned@bklynlibrary.org.
- Supporting this initiative can be done by donating through bklynlibrary.org.
- This access is available for individuals between 13-21 from New York State, with availability outside the state possibly limited.
- The initiative aims to ensure people can access the texts they want and serves as a temporary solution.
- Censorship in the US often involves denying access to texts rather than physically destroying them.
- Denying access to texts is a way to control who reads them without resorting to book burning.

# Quotes

- "Librarians have banned book week where they actually promote texts that are frequently challenged."
- "If you are between the ages of 13 and 21, you have a way to get a whole lot of them now."
- "They don't have to burn the book, just remove it."

# Oneliner

The Brooklyn Public Library provides virtual library cards to individuals aged 13-21, offering access to a wide range of texts amidst a trend of book removals.

# Audience

Book enthusiasts, Youth

# On-the-ground actions from transcript

- Email booksunbanned@bklynlibrary.org to obtain a virtual library card (suggested).
- Donate to support the Brooklyn Public Library's initiative through bklynlibrary.org (suggested).

# Whats missing in summary

The full transcript provides more context on the issue of book censorship and the challenges faced in accessing certain texts in the US.