# Bits

Beau says:

- Legislation in Tennessee criminalizes homelessness.
- Makes camping on local public property a felony, punishable by up to six years in prison.
- Demonstrations against racial injustice led to the legislation.
- Asking for change or camping alongside highways also criminalized.
- The legislation fails to address the root causes of homelessness.
- Supporters and opponents of the legislation lack comprehensive answers to homelessness.
- The real problem is homelessness itself.
- Providing homes and addressing underlying issues like rehab and mental health care is the solution.
- Incarcerating homeless individuals for up to six years is not a solution.
- Housing homeless individuals in a humane way is more cost-effective and productive than incarceration.
- Politicians target homeless individuals with legislation to appease their base.
- Homeless individuals are not the source of societal problems; the government is.
- The focus should be on holding politicians accountable rather than targeting the homeless.
- Housing homeless individuals in a helpful way benefits everyone and is cost-effective.
- It's cheaper and better to address homelessness compassionately.

# Quotes

- "Homelessness. I mean, not to put too fine a point on it, but that's what it does."
- "The problem is in the name. Homelessness."
- "It's cheaper to be a good person."
- "Stop looking under the bridge and start looking up at that Capitol."
- "Those people with less institutional power than you have, less money, less resources, less everything than you have are never the source of your problem."

# Oneliner

Legislation in Tennessee criminalizes homelessness, targeting vulnerable individuals instead of addressing root issues, advocating for a compassionate approach to provide housing and support.

# Audience

Tennessee Residents

# On-the-ground actions from transcript

- Contact local representatives to oppose the legislation (suggested).
- Support organizations providing housing and support for the homeless (exemplified).

# Whats missing in summary

The full transcript provides a detailed analysis of how legislation in Tennessee criminalizes homelessness and the importance of addressing root issues for a compassionate and cost-effective solution.

# Tags

#Tennessee #Homelessness #Legislation #RootIssues #Compassion