# Bits

Beau says:

- Mallory McMorrow, a Democrat in the Michigan state legislature, faced a political attack for advocating history education that includes teaching about slavery's wrongs.
- Beau questions why some parents want their children taught to identify with figures who upheld slavery rather than those who fought against it.
- He points out that history doesn't inherently make children uncomfortable; it's the parents' moral choices in upholding certain figures as heroes.
- Beau challenges the notion that teaching history equates to blaming white children for past atrocities, instead advocating for honoring those who opposed systems like slavery.
- He urges parents to guide their children towards identifying with those who stood up against injustice throughout history, regardless of skin color.
- The concern lies in children being taught to view heroes as villains and vice versa, perpetuating a harmful narrative.
- Beau stresses the importance of dismantling the remnants of systems like slavery that still benefit certain groups today.
- He concludes by encouraging listeners to prioritize identifying with the morally upright individuals in history.

# Quotes

- "History doesn't make your child feel bad. You siding with the villain does."
- "I don't know why people want their children to identify with the villain in the story when there are heroes."
- "They existed throughout American history but they're overshadowed by indoctrination that said that slavery was some necessary evil which is garbage."

# Oneliner

Beau challenges parents to rethink why they want their children to identify with figures who upheld slavery instead of those who fought against it, urging a shift towards honoring heroes who stood up against injustice throughout history.

# Audience

Parents, educators, activists

# On-the-ground actions from transcript

- Guide children to identify with historical figures who fought against injustice, regardless of skin color (exemplified)
- Advocate for inclusive and accurate history education in schools (exemplified)

# Whats missing in summary

The emotional impact of parents upholding figures who supported slavery on their children.

# Tags

#History #Education #Injustice #Heroes #Slavery