# Bits

Beau says:

- Talks about the concept of the Ukrainian resistance using roadside devices.
- Mentions people questioning the morality of such tactics.
- Shares a story from St. Augustine about pirates and emperors to illustrate moral perception.
- Questions the moral difference between roadside devices and artillery.
- Points out that the effectiveness of a tactic like roadside devices is why they are used.
- Advises not to base morality on a nation-state's perspective.
- Emphasizes that one can support or oppose actions of their government.
- Encourages opposing government morality to strive for improvement.

# Quotes

- "You are not your government."
- "You can support pieces of something and oppose others."
- "If you want to be an advanced citizen, you have to oppose your government's morality."

# Oneliner

Beau challenges the moral perceptions surrounding the use of roadside devices and urges individuals to think beyond their government's stance to be truly advanced citizens.

# Audience

Citizens

# On-the-ground actions from transcript

- Oppose your government's morality to push for better standards (suggested).
- Support or oppose specific actions of your government (exemplified).

# Whats missing in summary

Exploration of the importance of questioning and understanding the morality behind government actions.

# Tags

#UkrainianResistance #Morality #Government #Citizenship #MoralPerception