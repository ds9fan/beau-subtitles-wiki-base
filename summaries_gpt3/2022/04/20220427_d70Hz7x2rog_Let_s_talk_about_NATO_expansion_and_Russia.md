# Bits

Beau says:

- Explains the popular talking point of using NATO expansion to justify Russia's actions in Ukraine.
- Breaks down the concept of imperialism today, including force of arms, puppet governments, threats, and economic control.
- Clarifies that NATO expansion is not imperialism, but rather individual NATO countries may have engaged in imperialist actions.
- Points out that Russia utilized all forms of imperialism in its actions, branding it as engaged in imperialism.
- Dismisses the argument of Russia's preemptive strike due to NATO expansion as hollow and revealing an imperialist mindset.
- Criticizes justifying Russia's actions by comparing them to what the U.S. might do, pointing out U.S. history of imperialism.
- Challenges scenarios suggesting U.S. response to alliances with Russia as flawed logic.
- Addresses past U.S. politicians' views on NATO expansion and their imperialist mindset.
- Compares Russia's behavior to that of the U.S., both engaging in empire building and imperialism.
- Condemns the misguided argument of using NATO expansion to justify imperialism and aggression.

# Quotes

- "NATO expanding and accepting new members is not imperialism."
- "Russia is engaged in imperialism. Period."
- "This whole talking point of trying to blame it on NATO expansion is a colonizer imperialist mindset."
- "Russia is a capitalist oligarchy engaged in imperialist aggression."
- "Saying that it's okay is justifying imperialism."

# Oneliner

Beau explains the flaws in justifying Russian actions through NATO expansion, pointing out the imperialist mindset behind it and condemning imperialism.

# Audience

Activists, policymakers, educators

# On-the-ground actions from transcript

- Revisit and adjust your stance on justifying imperialism and aggression based on flawed comparisons and historical contexts (suggested).
- Research and understand the dynamics of NATO expansion and imperialist actions to prevent misguided justifications (exemplified).

# Whats missing in summary

Deeper insights into the implications of justifying imperialism and aggression, urging for critical examination of historical parallels and global power dynamics.

# Tags

#NATO #Imperialism #Russia #US #Aggression #Colonialism #GlobalPolitics #Policy #Activism