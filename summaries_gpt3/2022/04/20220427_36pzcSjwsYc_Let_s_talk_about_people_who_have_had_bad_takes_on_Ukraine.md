# Bits

Beau says:

- Addressing bad takes on Russia and Ukraine, particularly from leftist individuals supporting Russia in the conflict.
- People are doubting the beliefs of these individuals due to their support for an authoritarian state in an imperialist conquest.
- The association of Russia with the USSR and leftist ideologies is prevalent among some individuals.
- Many commentators who stray into topics outside their expertise make mistakes, particularly those focusing on philosophy and political science.
- Commentary on the conflict is influenced by pressure to talk about it and connections to the Soviet Union.
- Individuals discussing military affairs without expertise often make errors, like using outdated terminology such as "cauldron battle."
- The conflict in Ukraine requires a current understanding, not historical references from the past.
- U.S. propaganda and misinformation impact commentary on war, leading to overcorrections by commentators.
- Being wrong about a particular topic does not invalidate everything else a person has said.
- Ideas should be judged based on their merit, regardless of who presents them.

# Quotes

- "It's not unforgivable that somebody had a bad take on this."
- "Ideas stand and fall on their own."
- "Nobody is going to be right on it all the time."

# Oneliner

Addressing bad takes on Russia and Ukraine, commentators stray outside their expertise, influenced by pressure and historical connections, but ideas should stand on their merit alone.

# Audience

Commentators and viewers

# On-the-ground actions from transcript

- Fact-check information and avoid spreading misinformation (implied).
- Encourage critical thinking and independent analysis of ideas presented by commentators (implied).
- Seek out diverse sources of information to gain a well-rounded understanding of conflicts and political issues (implied).

# Whats missing in summary

The nuanced exploration of how individuals' areas of expertise can influence their commentary and the importance of separating ideas from the messenger in evaluating viewpoints.

# Tags

#Russia #Ukraine #Commentary #Propaganda #IdeasEvaluation