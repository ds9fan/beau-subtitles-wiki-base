# Bits

Beau says:

- Criticizes Mitt Romney's jest about forgiving trillions in student loans and other debt as not funny.
- Mentions forgiving medical debt as another significant aspect.
- Suggests that Romney's disconnectedness, having an elevator for his cars, hinders his understanding of income inequality.
- Emphasizes that forgiving student loan debt is not a bribe but a benefit to constituents and the country.
- Advocates for encouraging education as it is vital for democracy and benefits various aspects like the economy and national security.
- Points out that politicians may not want an educated populace to maintain a subservient underclass.
- Expresses disregard for prioritizing banks over addressing the massive wealth gap in the country.

# Quotes

- "Doing things that benefit your constituents, that's not a bribe. That's literally your job."
- "The only reason a politician wouldn't want an educated populace is if they wanted to create a permanent underclass."
- "Maybe it's time to do stuff for those on the bottom."

# Oneliner

Beau criticizes Mitt Romney's jest about forgiving debt, advocates for education, and addresses income inequality.

# Audience

Politically engaged individuals.

# On-the-ground actions from transcript

- Contact your representatives to advocate for forgiving student loan debt and promoting education (suggested).
- Support initiatives that aim to address income inequality and benefit those at the bottom (implied).

# Whats missing in summary

Beau's passionate delivery and nuanced arguments can be fully appreciated in the complete video. 

# Tags

#MittRomney #StudentLoans #Education #IncomeInequality #WealthGap