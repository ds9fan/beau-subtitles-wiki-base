# Bits

Beau says:

- Addresses aid going to Ukraine and a talking point about the defense industry making money from it.
- Demonstrates through numbers that the aid to Ukraine isn't significant for the defense industry's bottom line.
- Mentions that the defense industry will start making real money as the new aid package for Ukraine is $20 billion.
- Explains that although some of the aid is old stuff, the defense industry will benefit financially from providing it.
- Notes that the defense industry will profit significantly from the aid, indicating a shift in policy influence.
- Speculates on the defense industry influencing policy decisions through potential incentives like building factories in senators' hometowns.
- Suggests that the defense industry is now making substantial profits due to the aid package increase.
- Implies that the defense industry's influence may have played a role in the significant increase in military aid to Ukraine.
- Concludes that while Ukraine needs the equipment, the US defense industry is taking the lead in providing it.
- Leaves viewers with a thought-provoking perspective on the situation.

# Quotes

- "It is blood that makes the cash grow green."
- "The defense industry is now going to make a dump truck full of cash."
- "Perhaps we build a factory in your hometown, something along those lines."
- "Y'all have a good day."

# Oneliner

Beau addresses the increase in military aid to Ukraine and speculates on the defense industry's significant financial gains, implying potential policy influence.

# Audience

Advocates for Accountability

# On-the-ground actions from transcript

- Question policy decisions influenced by industries (implied)
- Stay informed about aid distribution and potential motives behind it (implied)

# Whats missing in summary

The full video provides a detailed breakdown of the aid situation in Ukraine, including implications of defense industry involvement and policy influences.

# Tags

#Ukraine #Aid #DefenseIndustry #USGovernment #PolicyInfluence