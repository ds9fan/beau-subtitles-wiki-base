# Bits

Beau says:

- Explains the current status of tanks and addresses the question of whether the age of the tank is over.
- Mentions that the end of the tank era is not primarily due to the conflict in Ukraine.
- Talks about the poor functionality and survivability of Russian tanks, attributing it to improper deployment and advances in anti-tank munitions.
- Points out that anti-tank rockets have advanced far beyond the capabilities of many tanks worldwide.
- Emphasizes the importance of deploying tanks properly, including having infantry screens.
- Notes that it's now more cost-effective to counter tanks with infantry-portable rockets and missiles rather than with other tanks.
- Mentions the changing landscape of conflict due to advances in anti-tank munitions and drone technology.
- Foresees the rise of unmanned tanks in the future, which will be remote-controlled and different in appearance.
- Compares the trajectory of tanks to battleships, stating that they are becoming easier targets in today's world.
- Predicts that tank arsenals worldwide will be phased out in the next 25 years, replaced by unmanned vehicles.

# Quotes

- "We're moving to an age of unmanned vehicles."
- "In some ways, the tank as we know it is going the way of the battleship."
- "It's changing. It's going to look different. It's going to be deployed differently."
- "It's not an end to that type of equipment. It's just shifting to a newer version of it."

# Oneliner

The age of the tank is changing, moving towards unmanned vehicles, as anti-tank munitions advance and drones reshape conflict dynamics.

# Audience

Military analysts, defense enthusiasts.

# On-the-ground actions from transcript

- Stay informed about advancements in military technology and their implications (implied).

# Whats missing in summary

Further insights on the potential implications of transitioning to unmanned tanks and the impact on warfare strategies.

# Tags

#Military #Technology #UnmannedVehicles #TankArsenals #FutureWarfare