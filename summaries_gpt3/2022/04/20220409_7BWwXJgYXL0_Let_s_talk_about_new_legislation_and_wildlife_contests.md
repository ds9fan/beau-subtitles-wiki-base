# Bits

Beau says:

- Introduces new federal legislation called the Prohibit Wildlife Killing Contests Act aimed at banning events where groups of people hunt predators like coyotes or foxes.
- Points out that the legislation applies to public lands, protecting half a billion acres.
- Mentions that these contests are not about conservation, but more about portraying an image of rugged mountain men.
- Criticizes the lack of skill involved in these contests due to technology making it easy to hunt predators en masse.
- Notes that winners of these contests merely receive a participation trophy and it's more about luck than skill.
- States that eight states have already banned this practice and two more states have stopped holding such events.
- Counters the argument that banning these contests on public lands will lead to increased hunting on private lands.
- Suggests using a camera instead of a gun if the goal is to challenge oneself against smart animals.
- Comments on knowing people who participated in these events and implies they could benefit from exploring their creative side.

# Quotes

- "It's not about skill anymore. It has nothing to do with that."
- "The winner of these things is really just getting a participation trophy."
- "You could always shoot them with a camera if that's really what it was about."

# Oneliner

Beau explains why wildlife killing contests are more about image than skill, advocating for a ban on public lands to protect predators.

# Audience

Conservationists, Wildlife Activists

# On-the-ground actions from transcript

- Advocate for wildlife protection laws (exemplified)
- Encourage creative pursuits as an alternative to hunting contests (implied)

# Whats missing in summary

The emotional impact of moving away from hunting contests towards more ethical and skill-based interactions with nature.

# Tags

#WildlifeProtection #Conservation #BanHuntingContests #PublicLands #AnimalRights