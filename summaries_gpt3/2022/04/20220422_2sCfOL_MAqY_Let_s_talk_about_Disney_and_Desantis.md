# Bits

Beau says:

- The state of Florida had a long-standing deal with Disney, allowing Disney to control the infrastructure, a critical aspect.
- DeSantis requested to terminate this deal, setting a distant date for after the election to avoid immediate consequences.
- Potential negative outcomes include residents being financially responsible for the bonds associated with this termination.
- Beau predicts that Disney will take legal action and likely win, reverting everything back to normal.
- If Disney doesn't win, there's a belief they may have to leave due to the inability to maintain their brand without controlling the infrastructure.
- The surrounding areas may struggle to meet Disney's standards without the necessary funds.
- Beau questions if Disney could work out a deal with the county but ultimately believes they may have to relocate.
- The damage to Disney's brand and the economic impact on the state could be significant if they have to leave.
- A signal has been sent to corporate America, warning against investing in Florida due to the uncertainty of deals with the state.
- This situation may have lasting negative effects on Florida's economy, regardless of the outcome with Disney.

# Quotes

- "Disney's lawyers will take this to court, and they'll win, and everything will go back to normal."
- "No company is going to come to Florida and invest billions when it can all be snatched away."
- "It is damage that can't be undone, regardless of what happens with Disney."

# Oneliner

The state of Florida's decision to end its long-standing deal with Disney sends a damaging message to corporate America and may have lasting negative effects on the state's economy.

# Audience

Florida residents, corporate America

# On-the-ground actions from transcript

- Advocate for transparency and accountability in government decisions (implied)
- Support businesses and organizations impacted by political decisions (implied)
- Stay informed about government actions that may affect local economies (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of Florida's decision to terminate its deal with Disney and the broader implications for the state's economy and corporate investments. Viewing the full transcript offers a comprehensive understanding of Beau's insights and predictions regarding this situation.

# Tags

#Florida #Disney #Economy #CorporateAmerica #LegalAction