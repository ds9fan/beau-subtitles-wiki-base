# Bits

Beau says:

- The Republican Party is moving away from democracy openly.
- The framing of the new Cold War is democracy versus authoritarianism.
- The Republican Party faces a dilemma because many members are authoritarians.
- The CPAC conference, a significant event for the Republican Party, will have Orban as the keynote speaker in Hungary, known to be sympathetic to Putin.
- 63 Republican congresspeople voted against a non-binding resolution that supported democracy.
- The resolution called for NATO to support democratic principles and institutions within its member countries.
- The Republican Party is embracing ideologies similar to authoritarian leaders globally.
- There are concerns that the Republican Party is being led by an authoritarian figure amid the new Cold War dynamics.
- Beau urges individuals within the Republican Party to re-evaluate their stance on democracy and their party affiliation.

# Quotes

- "The Republican Party is openly kind of shying away from democracy."
- "They couldn't even bring themselves to support democracy in words."
- "The Republican Party has become filled with people who share ideologies with strongmen, authoritarian goons all over the world."
- "The Republican Party wants to have Putin's man in NATO as their keynote speaker."
- "Those people who fashion themselves patriots, those people who say that they love this country, they've got some soul-searching to do."

# Oneliner

Beau reveals how the Republican Party is drifting from democracy, embracing authoritarian sympathies, and facing a critical identity crisis amid global political shifts.

# Audience

Republicans, Democrats, Activists

# On-the-ground actions from transcript

- Re-evaluate your stance on democracy and your party affiliation (suggested)
- Support democratic principles and institutions within your community (exemplified)
- Advocate for political leaders who uphold democratic values (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's current shift away from democracy and the implications of embracing authoritarian sympathies.

# Tags

#RepublicanParty #Democracy #Authoritarianism #CPAC #NATO