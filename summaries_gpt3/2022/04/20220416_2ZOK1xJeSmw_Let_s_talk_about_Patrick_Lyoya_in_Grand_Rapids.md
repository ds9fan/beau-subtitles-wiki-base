# Bits

Beau says:

- Analyzing a police shooting in Grand Rapids involving the driver Patrick Leoia and a cop.
- Communication issues arise from the beginning of the interaction.
- Officer grabs Patrick from behind, initiating violence.
- Patrick breaks free and runs instead of fighting the cop.
- Cop escalates the situation by chasing Patrick instead of waiting for backup.
- Commands like "don't" and "stop" are ineffective in high-stress situations.
- Officer shoots Patrick in the base of the skull during a struggle.
- The officer's potential justification for the shooting may focus on fear of harm due to a taser threat.
- Beau questions the reasonableness of fearing someone who repeatedly tries to run away.
- The legal system's focus on what the law allows may override common sense and best practices.

# Quotes

- "What's going to happen is the argument is going to be made that Patrick was trying to get that taser so he could then turn it on the officer, use it, and get his gun."
- "There is a huge gap between what's best practices, what policy says, what training dictates, what common sense says, and what the law allows."
- "But it's what currently exists."
- "Even if there's a whole video showing that he has no intention of using violence."
- "Y'all have a good day."

# Oneliner

Beau analyzes a police shooting in Grand Rapids, questioning the escalation and potential justifications in a system that often favors legal technicalities over common sense and best practices.

# Audience

Community members, activists

# On-the-ground actions from transcript

- Contact local officials to demand accountability for police actions (exemplified)
- Organize community meetings to address police violence and advocate for policy changes (exemplified)

# Whats missing in summary

The full transcript provides a detailed breakdown of a police shooting incident, raising questions about escalation, fear justifications, and the gap between legal standards and common sense.

# Tags

#PoliceShooting #Accountability #CommunityPolicing #LegalSystem #Justice