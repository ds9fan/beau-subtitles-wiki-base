# Bits

Beau says:

- The Department of Defense (DOD) is known for being on the cutting edge of social acceptance historically, desegregating before the rest of the country on many issues.
- Many states have passed bigoted anti-LGBTQ legislation, prompting the Air Force to offer medical and legal aid to personnel affected by these laws.
- The Air Force is willing to transfer personnel to different bases outside of states with discriminatory laws, indicating a potential impact on readiness.
- The Department of Defense is apolitical but focuses on the needs of the Air Force, suggesting that bases may cease to exist if they cannot fulfill their functions due to personnel relocation.
- Beau warns residents in states with discriminatory laws, such as South Florida or the Panhandle, about the possible economic consequences if bases like McDeal or Eglin shut down.
- Politicians pushing discriminatory laws are criticized for diverting attention from their lack of policy ideas and job performance, potentially causing economic instability in affected areas.
- Residents in states with discriminatory laws are urged to pay attention as the warning from the Air Force indicates potential mass transfers of personnel and negative impacts on economic activity.
- Failure of politicians to recognize the warning may imply the need for smarter political choices to avoid drastic consequences.

# Quotes

- "If you live in one of those states that has passed this ridiculous legislation, you need to pay attention."
- "You had better start paying attention, like your checkbook depends on it, because it does."
- "The number of people that have to be moved depends entirely on whether or not these ridiculous laws impact readiness."
- "You're not going to get a warning beyond this."
- "If the politicians in your state don't see that, you probably should have elected smarter politicians."

# Oneliner

The Air Force's warning about potential base closures due to discriminatory laws serves as a wake-up call for residents to pay attention before economic stability is at risk.

# Audience

Residents in states with discriminatory laws

# On-the-ground actions from transcript

- Pay attention to the impact of discriminatory laws and be prepared for potential economic consequences (implied).
- Advocate for smarter political choices to avoid negative outcomes (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the potential economic and social ramifications of discriminatory laws on military bases and surrounding communities.