# Bits

Beau says:

- Moscow promised not to invade Ukraine but later invaded and took land, breaking the promise.
- Japan historically refrained from criticizing Russia internationally due to hopes of getting the Kuril Islands back.
- Japan's response to Russia's invasion of Ukraine was different this time, with sanctions and diplomatic protests.
- Japan is sending Ukraine drones and CBRN equipment to protect against chemical, biological, radiological, and nuclear threats from Russia.
- Putin responded by saying Russia won't talk about giving the Kuril Islands back.
- Moscow's historical imperialism is concerning for Ukraine since they tend to keep any land they take and hold.
- Ukraine might push into Crimea or other contested areas as they regain the initiative, with increasing international support.
- The longer the conflict drags on, the more support Ukraine might gather.
- Russia has a history of changing maps and holding onto conquered territories.
- Ukraine can learn from Japan's experience with Russia in understanding the importance of historical precedents.

# Quotes

- "Moscow signs a statement of, you know, hey, don't worry, we're not gonna, we're not gonna come after you, we're gonna be neutral, don't worry about it."
- "They've apparently given up on that, or they realize that they're not going to get the islands back by remaining quiet."
- "We're just not going to even talk about giving this land back."

# Oneliner

Moscow's historical imperialism poses a threat as Japan's response to Russia's invasion of Ukraine shifts, with Ukraine likely to gain international support against Russian aggression.

# Audience

Foreign policy analysts

# On-the-ground actions from transcript

- Send aid and support to Ukraine (suggested)
- Advocate for diplomatic efforts to resolve conflicts (suggested)

# Whats missing in summary

The full transcript provides a detailed historical context for understanding the dynamics of Russian-Japanese relations and their impact on Ukraine.