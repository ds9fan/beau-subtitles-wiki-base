# Bits

Beau says:

- Explaining why some people believe that everything is going according to Russia's plan in Ukraine, despite contradictory actions by Putin and the reality of the situation.
- Mentioning how both far-right and far-left individuals are repeating the lie that everything is going according to Russia's plan.
- Pointing out that certain survival channels continue to spread claims that Russia is winning and that the US could be next, fueling fear to keep their audience engaged.
- Debunking the idea that Russia poses a real invasion threat to the United States, citing population numbers and logistics.
- Emphasizing that the current military landscape and global awareness make a large-scale invasion impractical and unlikely.
- Noting that fear-mongering channels prioritize selling fear over educating their audience on more realistic threats like climate change.
- Stating that maintaining fear is profitable for these channels, even if it means perpetuating falsehoods and sensationalism.

# Quotes

- "They sell fear."
- "It's not profitable to tell the truth in that case."
- "Keeping that dream alive."
- "The days of having to worry about somebody invading the United States, those are long gone."
- "Rather than taking the time to educate their audience on how to prepare for stuff that might actually happen, like climate change."

# Oneliner

Beau explains the profit-driven fear tactics of survival channels spreading false invasion narratives to maintain audience engagement.

# Audience

Content Creators, Viewers

# On-the-ground actions from transcript

- Educate your audience on real threats like climate change, encouraging preparedness beyond fear-based scenarios (implied).

# Whats missing in summary

The full transcript provides an in-depth analysis of the profit-driven fear tactics used by certain channels, contrasting sensationalism with the reality of military capabilities and global awareness.

# Tags

#FearTactics #DebunkingInvasionThreats #ClimatePreparedness #ProfitOverTruth