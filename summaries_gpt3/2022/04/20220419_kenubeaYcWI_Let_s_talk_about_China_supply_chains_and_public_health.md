# Bits

Beau says:

- Series of lockdowns in China started on April 2, impacting about 373 million people, more than the US population.
- Lockdowns cover regions responsible for about 40% of China's GDP.
- World's largest and fourth largest ports are not operating due to lockdowns.
- 300 container ships and 500 bulk ships off the coast of China waiting to be unloaded.
- Impending supply chain issues globally due to the backlog of orders.
- China will rush production to meet backed-up orders once lockdowns lift.
- Delays expected in unloading ships in the US, causing backups in trucking and rail industries.
- Experts suggest potential for worse disruptions than seen before.
- Consumers might face longer wait times for products from China and potential price increases.
- Combined with inflation and political issues, a rough month is anticipated.

# Quotes

- "There have been a string of lockdowns in China."
- "So we will have ships sitting off of our coast waiting to be unloaded again."
- "If you're waiting on products from China, you may be waiting longer."
- "It's just kind of what we're expecting now in the 2020s."
- "We don't have an end date in sight for the lockdowns."

# Oneliner

Lockdowns in China impacting 373 million people with significant supply chain disruptions, potentially leading to longer wait times and price increases for products globally.

# Audience

Global Consumers

# On-the-ground actions from transcript

- Monitor your orders from China for potential delays (implied).
- Adjust expectations for product arrival times and potential price increases (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of the supply chain disruptions caused by lockdowns in China and their potential global impacts, along with the uncertainty surrounding the duration of these disruptions.