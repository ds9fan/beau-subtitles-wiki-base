# Bits

Beau says:

- Beau addresses the new referrals for contempt from the January 6th committee, managing expectations surrounding them.
- The committee can refer individuals to the Department of Justice for indictment but cannot indict on its own.
- Four people have been referred by the committee: Steve Bannon, Mark Meadows, Dan Scavino, and Peter Navarro.
- Steve Bannon has been indicted and is going to trial, while the others have not faced indictments yet.
- If indictments are delayed beyond the midterms, Republicans retaking the House could potentially squash the proceedings.
- Lack of accountability and delays in indictments erode faith in the system and the prospect of justice for those involved in the January 6th events.
- The Department of Justice's actions and investigations are not transparent, leading to uncertainty and skepticism among the public.
- Beau notes that Attorney General Garland's approach is not focused on frequent press releases or updates, which may contribute to public frustration.
- The longer the delays in accountability and indictments persist, the more it reinforces the perception that those in power are not held responsible for their actions.
- Additional referrals from the committee may not hold significance if the Department of Justice does not proceed with indictments.

# Quotes

- "The longer these indictments don't exist, the longer DOJ just doesn't follow up on the referral, doesn't issue a statement as to why they're not going to indict or a statement that they're going to indict something, the more people lose faith in that system."
- "The longer this goes on without developments, the more Americans start to see that as what's happening."
- "The longer things draw out, the longer this goes on without developments, the more Americans start to see that as what's happening."

# Oneliner

Beau manages expectations around January 6th committee referrals, expressing concerns over accountability delays and eroding public faith in the justice system.

# Audience

Concerned citizens

# On-the-ground actions from transcript

- Stay informed about developments in the January 6th committee referrals (suggested).
- Advocate for transparency and accountability in the Department of Justice's investigations (implied).

# Whats missing in summary

Insights into the potential consequences of prolonged delays in accountability and indictments for January 6th events.

# Tags

#January6th #CommitteeReferrals #DepartmentOfJustice #Accountability #PublicFaith