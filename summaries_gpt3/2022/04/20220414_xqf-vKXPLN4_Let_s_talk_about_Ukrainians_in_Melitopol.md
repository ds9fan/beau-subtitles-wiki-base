# Bits

Beau says:

- Recent focus on expected Russian advance in the East overshadows news from other areas.
- Maritopol reports over 70 Russian troop casualties in three weeks to Ukrainian resistance.
- Ukrainian resistance likely comprises well-organized or semi-organized groups conducting surveillance and operations at night.
- Resistance groups are becoming better armed by keeping equipment from fallen troops.
- Low-level harassment by the resistance undermines Russian military authority.
- Russia must acknowledge and address the losses to avoid losing the city.
- Options for Russia include ceding the night to resistance or doubling up patrols.
- Russian troops live in constant fear and vulnerability, akin to a horror movie.
- Continuous losses are unsustainable for the Russian military.
- Resistance operations behind Russian lines create paranoia, demoralization, and logistical challenges.

# Quotes

- "Never say I'll be right back because you won't."
- "They're signing on for a very long, very nasty conflict."
- "These types of resistance operations change the course of wars."
- "Going after civilians as punishment never works."
- "It always strengthens the resistance."

# Oneliner

Recent focus on expected Russian advance overlooks Ukrainian resistance's effective low-level harassment, which undermines Russian military authority and may change the course of the conflict.

# Audience

Military analysts, policymakers, activists

# On-the-ground actions from transcript

- Support Ukrainian resistance efforts through aid and resources (implied)
- Raise awareness about the impact of resistance operations on the conflict (suggested)

# Whats missing in summary

The full transcript provides detailed insights into the strategic implications of Ukrainian resistance and the challenges faced by the Russian military in occupied territories.

# Tags

#RussianMilitary #UkrainianResistance #Conflict #Occupation #StrategicImplications