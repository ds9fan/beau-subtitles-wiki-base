# Bits

Beau says:

- Provides historical context on the roads to Ukraine, going back to the Cold War and detailing events that shaped the country's current situation.
- Describes how major powers like the U.S. and Russia conducted intelligence operations and interfered in Ukraine to further their interests.
- Talks about the U.S. running a 25-year ad campaign in Ukraine to influence democratic values and Western interests.
- Explains events like the Budapest memos, the Orange Revolution, and the Maidan protests that marked significant shifts in Ukrainian politics.
- Analyzes Russia's actions, including annexing Crimea in violation of agreements, and Ukraine's response to resist Russian influence.
- Addresses misconceptions about the Maidan protests being labeled as a U.S. coup and stresses the agency of the Ukrainian people in shaping their own destiny.
- Emphasizes Ukraine's fight against Russian invasion and interference, showcasing their determination and resilience.

# Quotes

- "Ukrainians do not want to be under the thumb of Moscow."
- "The thing that's on loop is that Ukrainians do not want to be under the thumb of Moscow."
- "That's why they're fighting the way they are."
- "When you try to take that away, you are erasing them."
- "Don't take that away from them because it doesn't fit your narrative."

# Oneliner

Beau provides historical context on Ukraine, discussing major powers' interference, Ukrainian agency, and the ongoing fight against Russian influence.

# Audience

History enthusiasts, geopolitics followers.

# On-the-ground actions from transcript

- Support Ukrainian communities (suggested)
- Educate yourself on Ukrainian history and current events (implied)
- Advocate for international support for Ukraine's sovereignty (implied)

# Whats missing in summary

The full transcript provides a comprehensive overview of the geopolitical dynamics surrounding Ukraine, offering valuable insights into historical events that have shaped the country's current reality. Viewing the entire transcript can provide a deeper understanding of the context behind Ukraine's struggles and resilience against foreign interference.

# Tags

#Ukraine #Geopolitics #Russia #US #History #Maidan #Independence #Interference #Sovereignty