# Bits

Beau says:

- Attorney General of South Carolina, Jason Roundsburg, involved in a possible impeachment due to conduct in 2020.
- Roundsburg hit a pedestrian while driving, claimed he didn't know what he hit, and reported it to 911 before going home.
- State troopers indicate the pedestrian was on the hood of the vehicle for about a hundred feet, raising skepticism about Roundsburg's account.
- Roundsburg settled privately with the pedestrian's widow and pled no contest to misdemeanor crimes.
- Some legislators plan to pursue impeachment despite a select committee finding that Roundsburg's conduct did not warrant it.
- Roundsburg, supported by Trump, is likely to run for re-election despite the controversy.
- State troopers' evidence cast doubt on Roundsburg's narrative, potentially driving the impeachment effort.
- Vote on impeachment likely to occur on Tuesday.
- Roundsburg's decision to run for re-election despite the scandal might be fueling the impeachment push.
- Political scandal in state politics usually resolves quicker, making Roundsburg's situation unique.

# Quotes

- "State troopers are skeptical of his chain of events there."
- "It looks like the vote as to whether or not to pursue the impeachment will take place on Tuesday."

# Oneliner

Attorney General Jason Roundsburg faces possible impeachment in South Carolina over a 2020 incident involving a pedestrian, with state troopers' evidence casting doubt on his narrative and leading to a vote on Tuesday.

# Audience

Voters, legislators, community.

# On-the-ground actions from transcript

- Contact your legislators to express support or opposition to the impeachment proceedings (suggested).
- Stay informed about the developments and outcome of the impeachment vote (implied).

# Whats missing in summary

Details on the potential consequences of Roundsburg's impeachment and the impact on South Carolina politics. 

# Tags

#SouthCarolina #Impeachment #StatePolitics #JasonRoundsburg #Scandal