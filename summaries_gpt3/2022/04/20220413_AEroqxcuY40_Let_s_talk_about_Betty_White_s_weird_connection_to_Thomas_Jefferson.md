# Bits

Beau says:

- Explains how Americans, as a young country, tend to view history as ancient and far removed.
- Shares a personal anecdote of meeting people who were alive in the 1800s to illustrate the proximity of historical events.
- Breaks down the timeline between Thomas Jefferson, Harriet Tubman, Ronald Reagan, and Betty White to show how recent historical figures overlap.
- Emphasizes how close America's troubling past, like segregation, is to the present.
- Encourages history teachers to put history in perspective for students to understand the impact and relevance of past events.
- Suggests that understanding the overlap between historical figures' lives can spark curiosity and interest in history.
- Urges viewers to acknowledge how recent historical events are and to actively participate in shaping future history.

# Quotes

- "It really wasn't that long ago."
- "We like to pretend like all of that horrible stuff happened forever ago."
- "Just a couple of lives separates the time of Thomas Jefferson from the time of your students."

# Oneliner

Beau explains the proximity of historical events to challenge the perception of history as ancient, urging active participation in shaping future history.

# Audience

History teachers

# On-the-ground actions from transcript

- Challenge students to research and understand the overlaps between the lives of historical figures (suggested).
- Encourage students to actively participate in shaping future history by learning from the past (implied).

# Whats missing in summary

The full transcript provides a compelling perspective on the proximity of historical events, urging individuals to actively participate in shaping future history. Watching the full video can offer a deeper understanding of Beau's message and the importance of acknowledging the closeness of historical events to the present.

# Tags

#History #Teaching #Perspective #ActiveParticipation #AmericanHistory