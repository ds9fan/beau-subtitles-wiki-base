# Bits

Beau says:

- Analyzing the Republican strategy to avoid a repeat of the midterms.
- The plan includes raising the voting age to 21, revealing their priorities.
- Beau finds this plan brilliant as it exposes the Republican Party's true motives for power.
- Republicans are willing to deny people their rights to maintain power.
- Beau suggests openly admitting to raising the voting age to show their intentions.
- Pointing out the irony of not raising the age to buy a rifle but wanting to raise the voting age.
- Younger Americans are more progressive and less likely to conform to Republican ideals.
- Negative voter turnout is a result of the Republican Party's outdated tactics.
- Referencing the 26th Amendment's background in empowering young voters.
- Beau criticizes the Republican Party for valuing power over representing constituents.

# Quotes

- "They don't care about the Republic. They have no concept."
- "Power for power's sake."
- "Denying people their rights, it's not gonna work."
- "Not representing their constituents, but ruling them."
- "Y'all have a good day."

# Oneliner

Analyzing the Republican strategy to raise the voting age to 21 reveals their power-hungry motives, disregarding the rights of citizens and alienating younger, more progressive voters.

# Audience

American voters

# On-the-ground actions from transcript

- Advocate for fair voting rights (exemplified)
- Support voter education initiatives (implied)

# Whats missing in summary

Full context and depth of Beau's analysis on the Republican Party's strategy and motivations.

# Tags

#RepublicanStrategy #VotingAge #PowerHungry #ProgressiveVoters #26thAmendment