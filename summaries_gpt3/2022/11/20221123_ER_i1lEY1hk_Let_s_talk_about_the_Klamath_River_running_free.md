# Bits

Beau says:

- California and Oregon's Klamath River is set for a groundbreaking $500 million project to remove four dams, the largest dam removal and river restoration project ever.
- The Klamath River has been constrained for over 100 years, but the dams' removal will commence this summer.
- National news often simplifies the issue as farmers needing water versus endangered salmon, but the real story centers on the Yurok, a Native American group for whom salmon is life.
- Salmon isn't just a food source for the Yurok; it's a vital part of their culture and existence.
- Removing the dams will likely result in a healthier salmon population, preserving not just the fish but also the Yurok way of life.
- The Yurok people have been fighting for dam removal for at least a decade, with the understanding that if the salmon disappear, so will they.
- The significance of the salmon to the Yurok can be compared to the cultural importance of the buffalo to Native Americans, as seen in "Dances with Wolves."
- This project represents a rare win, with the final major hurdle cleared, marking the end of a long fight for river restoration.
- By restoring the river, not only the fish but also a way of life is being preserved.
- The approval for dam removal marks the end of one story and the beginning of a new chapter in the Klamath River's restoration process.

# Quotes

- "The salmon or the buffalo. It's not just a food source. It's an integral part of the culture."
- "If the salmon go, the Yurok go."
- "This is a win."

# Oneliner

The Klamath River's dam removal project marks a significant win for the Yurok people, preserving their culture and way of life by ensuring the salmon's return.

# Audience

Environmental activists, Indigenous rights advocates

# On-the-ground actions from transcript

- Support Indigenous-led environmental restoration projects (exemplified)
- Advocate for similar dam removal projects in other regions (exemplified)

# Whats missing in summary

The emotional depth and cultural significance of the salmon to the Yurok people can be fully understood by watching the full speech.