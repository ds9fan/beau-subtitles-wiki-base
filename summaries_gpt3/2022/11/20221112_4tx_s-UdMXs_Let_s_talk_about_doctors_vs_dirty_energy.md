# Bits

Beau says:

- Doctors are attributing various health issues to climate change caused by dirty energy companies.
- Mosquitoes have an extended range due to changing temperatures, leading to an increase in Zika and dengue cases in the U.S.
- Particulate air pollution caused 1.2 million deaths worldwide, including 11,840 in the United States.
- Extreme heat has led to 98 million cases of hunger globally.
- Heat-related deaths in the U.S. have increased by 68%, rising to 74% for those over 65.
- Climate change and emissions are causing a wide array of health issues globally.
- Dirty energy companies, subsidized by governments, are making massive profits while contributing to medical problems.
- The Lancet releases annual reports detailing the impact of dirty energy companies on health.
- Transitioning away from dirty energy is imperative for these companies, akin to the tobacco industry facing lawsuits.
- Urgent decisions are needed from dirty energy companies as time is running out for change.

# Quotes

- "The dirty energy companies, their smartest move is to transition themselves as fast as possible."
- "The situation here is pretty similar to tobacco."
- "The dirty energy companies, they have a decision to make and they're running out of time to make it."

# Oneliner

Doctors link health issues to climate change caused by dirty energy companies, urging urgent transition away from harmful practices.

# Audience

Climate advocates, policymakers, activists.

# On-the-ground actions from transcript

- Read the Lancet report on the impact of dirty energy companies on health (suggested).
- Advocate for transitioning away from dirty energy sources in your community (implied).

# Whats missing in summary

The full transcript provides more in-depth information and statistics on the health impacts of climate change caused by dirty energy companies.