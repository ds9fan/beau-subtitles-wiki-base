# Bits

Beau says:

- Addressing the question of whether unions are for everybody and why one should support or join them.
- Responding to a viewer questioning the necessity of unions when well-paid workers go on strike for more.
- Explaining the perception of unions always wanting more and workers being dissatisfied despite being well-paid.
- Pointing out that companies always strive to make more profit each year, so why shouldn't workers want a share of that success?
- Clarifying that unions help workers negotiate for a fair share of the company's increased profits.
- Emphasizing that unions are formed not when conditions are great, but when they need improvement.
- Asserting that union workers deserve more because they are the ones generating the wealth for the company.
- Advocating for supporting and possibly joining unions to ensure workers receive their fair share of the value they produce.
- Mentioning that even the Starbucks union, though new, may eventually follow suit in seeking fair compensation.
- Encouraging viewers to understand and back the collective bargaining power of unions to improve workers' lives.

# Quotes

- "Union workers deserve more because they are the ones making the money."
- "Divided you beg, united you bargain."
- "Support and join unions to get your fair share of the value you produce."

# Oneliner

Beau explains why unions are necessary, even for well-paid workers, by shedding light on the power of collective bargaining to secure fair compensation.

# Audience

Workers, supporters

# On-the-ground actions from transcript

- Support a union (suggested)
- Join a union (suggested)

# Whats missing in summary

The full transcript provides a detailed explanation of why unions are vital for all workers, regardless of their current pay, to secure fair compensation and improve working conditions.

# Tags

#Unions #CollectiveBargaining #FairCompensation #SupportWorkers #JoinUnions