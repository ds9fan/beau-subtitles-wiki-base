# Bits

Beau says:

- Conservative media and the Republican Party are considering parting ways with Trump.
- The shift is not due to moral or ethical reasons but because he lost.
- Trump's brand is fading, and his ability to rally supporters is diminishing.
- The decision to distance from Trump is driven by a desire for power, not values.
- Those seeking power for the far right prioritize power over principles.
- They are willing to overlook Trump's actions as long as they are winning.
- The shift signifies a focus on maintaining power rather than condemning Trump's behavior.
- The country's direction depends on whether people support those who prioritize power over values.
- Individuals have the choice to lead towards a better future that doesn't rely on divisive tactics.
- The decision to break ties with Trump is solely based on his loss of influence rather than a moral stance.

# Quotes

- "It's not a moral position. It's not an ethical one. It's not a philosophical one. He lost."
- "They care about power and Trump can't deliver anymore."
- "If you want to follow people who will accept everything that Trump did as long as they're winning."
- "He's just not their ticket to power anymore."

# Oneliner

Conservative media and the Republican Party are distancing from Trump not due to morals but because he lost, revealing a power-driven agenda over principles.

# Audience

Voters, Activists

# On-the-ground actions from transcript

- Question who you support based on values, not just winning (implied).
- Lead towards a better future without relying on divisive tactics (implied).
- Advocate for leaders who prioritize principles over power (implied).

# Whats missing in summary

The full transcript provides a deeper understanding of the motivations behind conservative media and the Republican Party's shift away from Trump, focusing on power dynamics over moral considerations.

# Tags

#ConservativeMedia #RepublicanParty #Trump #PowerVsValues #Leadership