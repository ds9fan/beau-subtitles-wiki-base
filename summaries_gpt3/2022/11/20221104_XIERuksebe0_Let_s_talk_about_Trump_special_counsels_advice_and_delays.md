# Bits

Beau says:

- Department of Justice considering special counsel for Trump cases and bringing in high-end advisors.
- Special counsel may be used to negate political appearances if Trump announces candidacy.
- Beau believes using a special counsel could be viewed as political, deviating from the norm.
- Bringing in high-end advisors suggests upcoming activity shortly after the election.
- Expect a flood of activity, more searches, and potentially indictments of high-profile individuals.
- The Department of Justice is closing the circle around Trump, preparing for action after the election.
- Unofficial rule of not taking action right before an election to avoid impropriety.
- Advisers are there for the possibility of going after Trump through normal processes.
- Special counsel is being considered in case Trump declares his 2024 candidacy.
- Beau expresses concerns about potential delays and the country's ability to afford them.
- Waiting for the verdict on the current sedition trial before proceeding might be wise.
- Beau advocates for sticking to the normal process and avoiding unnecessary delays.
- The documents case is not seen as complicated by Beau.
- Beau suggests that the Department of Justice needs to focus on the normal process with the advisers they have.

# Quotes

- "Two things: One is they're definitely going after Trump."
- "I personally think that's a bad idea."
- "The only delay at this point that makes sense to me is waiting for the verdict on the current sedition trial."
- "But a delay in putting together a special counsel's office and all of that stuff, it's just going to make the process run longer."
- "The normal process is probably what they need to stick to."

# Oneliner

Department of Justice considers special counsel and advisors for Trump cases, preparing for post-election activity, despite concerns about political appearances and unnecessary delays.

# Audience

Legal analysts, political commentators

# On-the-ground actions from transcript

- Wait for the verdict on the current sedition trial before proceeding (suggested)
- Stick to the normal process with the advisers in place (implied)

# Whats missing in summary

Further insights on the potential implications of using a special counsel and high-end advisors in Trump cases.