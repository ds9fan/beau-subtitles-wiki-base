# Bits

Beau says:

- Exploring the reaction to advertisers pulling out from Twitter due to recent changes.
- Representative Marjorie Taylor Greene's tweet accusing corporations of "corporate communism."
- Greene's claim that Elon Musk opening Twitter for free speech levels the playing field.
- Beau's disagreement with the notion that all ideas deserve a level playing field.
- Dissecting the concept of "corporate communism" and why it doesn't hold up.
- Pointing out the inclusion of companies like American Express and BlackRock on Greene's list.
- The essence of capitalism in the marketplace of ideas and companies choosing to distance themselves from objectionable ideas.
- Distinguishing between corporate influence by representatives and true fascism.
- The marketplace analogy, where the rejection of certain ideas is a result of capitalism, not communism.
- The rejection of objectionable ideas by the majority in the marketplace.
- Why the term "corporate communism" is flawed and doesn't apply in this context.
- Linking Greene's accusations to power dynamics and governmental influence on corporations.
- The role of capitalism in companies making decisions based on public perception and association.
- Clarifying that the rejection of certain ideas is a capitalist concept, not corporate communism.
- Summarizing the misunderstanding around the concept of "corporate communism."

# Quotes

- "Opening Twitter for everyone's speech doesn't promote one side. It opens the town square and levels the playing field."
- "Their product didn't sell. In fact, it was seen as objectionable to the majority of the people in the marketplace."
- "So your ideas are bad and you should feel bad."
- "It's not communism. It's not corporate communism because that doesn't exist. It's just good old capitalism."
- "Y'all have a good day."

# Oneliner

Beau dissects the flawed concept of "corporate communism" while discussing the rejection of objectionable ideas in the marketplace of ideas through a capitalist lens.

# Audience

Social media users

# On-the-ground actions from transcript

- Support platforms that uphold free speech and diverse viewpoints (exemplified)
- Educate others on the nuances between capitalism and communism in corporate settings (exemplified)

# Whats missing in summary

The full transcript provides a comprehensive breakdown of the term "corporate communism" and its misapplication in the context of marketplace dynamics, offering insights into capitalism's role in idea rejection.

# Tags

#CorporateCommunism #MarketplaceofIdeas #Capitalism #FreeSpeech #Misconceptions