# Bits

Beau says:

- Trump's ambiguous stance on potentially running against DeSantis and threatening to reveal damaging information about him.
- Speculation on whether Trump's tactic is a form of coercion against DeSantis.
- Trump's claim of possessing information about DeSantis that could be damaging to Republicans.
- Questioning the ethics of withholding damaging information until politically advantageous.
- Doubts on trusting Trump if he prioritizes his political interests over those of Republican voters.
- Trump's perceived disregard for an informed public and preference for uneducated voters.
- Implication that Trump's actions reveal his character to Republicans.
- Overall, Beau criticizes Trump's potential manipulative tactics and lack of transparency.

# Quotes

- "If there is something and Trump knows about it and Ron DeSantis is doing something that Republicans would find morally objectionable or perhaps it's damaging the state of Florida, how can you trust Trump at that point?"
- "He's flat out saying he is putting his personal political interests above that of Republican voters."
- "I guess having an informed public, having informed voters, I guess that's not important to Trump."
- "If you have any questions about who Trump is and you're a Republican, he just told you."

# Oneliner

Beau questions Trump's ethics in potentially using damaging information against DeSantis, revealing a lack of transparency and prioritizing political gain over Republican voters' interests.

# Audience

Republican voters

# On-the-ground actions from transcript

- Question Trump's motives and transparency (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of Trump's potential manipulation tactics and lack of transparency, urging Republican voters to question his ethics and priorities.

# Tags

#Trump #DeSantis #Republican #Transparency #Manipulation