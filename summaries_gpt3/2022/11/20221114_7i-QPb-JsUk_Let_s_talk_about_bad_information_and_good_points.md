# Bits

Beau says:

- Addressing the need to debunk misinformation on a specific topic concerning civilian casualties.
- Acknowledging the importance of presenting arguments accurately to support valid points.
- Explaining the misconception that the US is worsening in protecting civilians during wartime, when in reality, it is improving.
- Clarifying the misconception that drones cause 90% civilian casualties and providing scenarios to demonstrate the fallacy.
- Emphasizing the challenge of determining accurate numbers regarding drones and civilian casualties due to the complex nature of targeting.
- Pointing out that the issue is not the precision of the weapons but rather the intelligence failures leading to civilian casualties.
- Advocating for focusing on anecdotes and intelligence failures rather than unreliable statistics to address the problem effectively.
- Mentioning the preference for politicians to use drones due to avoiding risks associated with boots on the ground, leading to compromised intelligence.
- Describing the historical changes in rules of engagement under different administrations, noting Trump's loosening of regulations.
- Expressing cautious optimism about Biden's new guidance on drone strikes but underscoring the uncertainty of its effectiveness without adherence over time.

# Quotes

- "The precision of the weapon was fine. The precision of the intelligence, the information that led them to say, yes, hit it, that's the problem."
- "Alter your argument because the point you're trying to make is 100% valid."
- "Don't get caught up in bad information when you're trying to make a good point."

# Oneliner

Addressing misconceptions on civilian casualties, Beau navigates through flawed data to underscore the significance of accurate arguments and intelligence in addressing the issue effectively.

# Audience

Advocates, Activists, Educators

# On-the-ground actions from transcript

- Advocate for better intelligence and approval processes in military operations (implied).

# What's missing in summary

The full transcript provides detailed insights on the challenges of addressing civilian casualties in warfare, stressing the importance of accurate information and intelligence in making valid arguments.

# Tags

#CivilianCasualties #Misinformation #Drones #MilitaryIntelligence #Advocacy