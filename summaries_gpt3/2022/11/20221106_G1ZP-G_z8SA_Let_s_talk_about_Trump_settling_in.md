# Bits

Beau says:

- Trump settled a case dating back to 2015 involving disparaging remarks about people from Mexico.
- Demonstrators were allegedly assaulted by Trump's private security during a protest.
- Michael Cohen reportedly witnessed Trump instructing security to "get rid of the protesters."
- A sign carried by demonstrators read, "make America racist again," with claims that it was taken back into the building as a trophy.
- Despite Trump's claim of never settling cases, his team settled this one, showcasing his legal strategy.
- Trump's delaying tactics in legal cases were evident, with him being deposed in 2020 and settling right as the jury selection began in 2022.
- Both sides claimed victory after the settlement.
- The lawyer for the demonstrators emphasized that while rich and powerful people can put their names on buildings, sidewalks will always belong to the people.

# Quotes

- "I don't settle cases. I don't do it because that's why I don't get sued very often, because I don't settle, unlike a lot of other people."
- "Trump's overall legal strategy in action."
- "Delay, delay, delay. It's what Trump does."
- "Rich and powerful people can put their name on buildings. But the sidewalks, well those will always belong to the people."

# Oneliner

Trump settled a case involving disparaging remarks, showcasing his delaying legal strategy, despite claiming he never settles.

# Audience

Legal observers

# On-the-ground actions from transcript

- Contact legal aid organizations for assistance (implied)
- Support demonstrators' rights by joining protests or providing aid (implied)

# Whats missing in summary

Details on the specific disparaging remarks made by Trump and the impact on the demonstrators.

# Tags

#Trump #LegalStrategy #Settlement #Demonstrators #DelayingTactics