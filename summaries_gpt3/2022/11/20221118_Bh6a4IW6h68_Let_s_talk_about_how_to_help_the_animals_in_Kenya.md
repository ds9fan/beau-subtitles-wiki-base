# Bits

Beau says:

- Addressing how to help with the drought situation in Africa, specifically focusing on animals like elephants and hippos.
- Recommending support for David Sheldrick's Wildlife Trust, a massive organization known for its work with elephants but extends help to various animals.
- Mentioning the Trust's efforts in building solar-powered water distribution plants and trucking in water to support wildlife during the drought.
- Noting the importance of funding in addressing the crisis, especially due to the lack of political will.
- Encouraging people to support the Trust through donations or symbolic adoptions of animals like elephants, rhinos, or giraffes.
- Pointing out that the Trust has a wish list of needed items and is currently caring for an influx of orphaned elephants.
- Emphasizing the impact of grass scarcity on hippos due to the water shortage in the region.

# Quotes

- "You want to assist, you want to help these animals that very well may not make it because of the drought, look to David Sheldrick's Wildlife Trust."
- "When funding is the problem, you absolutely can [help by providing money]. And in this case, the funding is the problem."
- "If this is something that is motivating you, you want to get involved, you want to help, you want to try to make a difference when it comes to these animals."

# Oneliner

Beau addresses how to support animals impacted by drought in Africa, recommending David Sheldrick's Wildlife Trust for donations or symbolic adoptions.

# Audience

Animal conservation supporters

# On-the-ground actions from transcript

- Support David Sheldrick's Wildlife Trust through donations or symbolic animal adoptions (suggested).

# Whats missing in summary

Details about the specific ways David Sheldrick's Wildlife Trust uses funds to support wildlife during the drought.

# Tags

#AnimalConservation #DroughtSupport #DavidSheldricksWildlifeTrust #Donations #SymbolicAdoptions