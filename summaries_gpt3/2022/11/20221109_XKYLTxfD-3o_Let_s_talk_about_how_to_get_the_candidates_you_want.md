# Bits

Beau says:

- Talks about civic engagement and electoralism.
- Mentions the challenge of getting a candidate who truly represents your beliefs through the primary and general election.
- States that electoralism is the least effective form of civic engagement.
- Suggests changing society through other forms of civic engagement like capacity building, direct service, research, advocacy, and education.
- Emphasizes the importance of changing societal thought to shift the Overton window.
- Lists various forms of civic engagement including personal responsibility, philanthropy, and participation and association.
- Advocates for using all forms of civic engagement to create deep systemic change.
- Stresses the need to build a power structure outside of existing ones to leverage for societal change.

# Quotes

- "Electoralism is the least effective form of civic engagement."
- "To change society, you have to change thought."
- "You can't rely on electoralism. You have to use every tool at your disposal."

# Oneliner

Beau explains why electoralism is the least effective form of civic engagement and advocates for utilizing all tools available to create deep systemic change by changing societal thought.

# Audience

Community members, activists, voters

# On-the-ground actions from transcript

- Build capacity at the local level to support chosen candidates (suggested)
- Engage in direct service to immediately impact outcomes (exemplified)
- Conduct research to provide necessary information for civic engagement (implied)
- Advocate through petitions and demonstrations to alter public opinion (exemplified)
- Demonstrate personal responsibility by setting an example of desired change (exemplified)
- Engage in philanthropy to address issues needing financial support (exemplified)
- Leverage contacts and networks on a social level to influence change (implied)

# Whats missing in summary

Importance of utilizing various forms of civic engagement to create lasting societal change.

# Tags

#CivicEngagement #Electoralism #SocietalChange #CommunityBuilding #Activism