# Bits

Beau says:

- Addresses political advertising and the exorbitant spending on specific issues.
- Focuses on the Republican Party spending a minimum of $50 million on anti-LGBTQ advertising.
- Points out that $50 million was spent solely on advertising and doesn't include other platforms like social media.
- Reveals that the target of the advertising was trans kids, with a small number of kids starting hormone therapy.
- Emphasizes the discrepancy between the amount spent and the actual number of affected kids.
- Breaks down the cost to bully each kid, which amounts to around $12,000.
- Criticizes this behavior as playground bully mentality and propaganda.
- Condemns those who support such divisive and harmful tactics.
- Calls out the manipulation and shame associated with supporting these actions.
- Concludes by urging viewers to be aware of manipulation and not fall for divisive propaganda.

# Quotes

- "They did it to manipulate you."
- "The propaganda is bad and you should feel bad."
- "If you're looking down at those people, you're not paying attention to them as they beg you for money."

# Oneliner

The Republican Party spent $50 million on anti-LGBTQ advertising, targeting trans kids, showcasing a playground bully mentality to manipulate public opinion and divide the country.

# Audience

Voters, Activists, Allies

# On-the-ground actions from transcript

- Stand against divisive propaganda (exemplified)
- Support LGBTQ youth (exemplified)

# Whats missing in summary

Deeper insights into the impact of political advertising on vulnerable communities and the importance of standing up against such divisive tactics.

# Tags

#PoliticalAdvertising #LGBTQ #Anti-bullying #CommunitySupport #Propaganda