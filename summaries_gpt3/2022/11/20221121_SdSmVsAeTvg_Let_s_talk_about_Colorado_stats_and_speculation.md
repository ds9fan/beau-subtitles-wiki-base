# Bits

Beau says:

- Speculated on a potential domestic violence connection in recent Colorado incident causing pushback.
- Many mass incidents have a domestic violence connection, with statistics showing as high as 68.2%.
- Noted that domestic violence is underreported and a significant factor in mass incidents.
- Advocated for closing all loopholes related to domestic violence in gun control legislation.
- Encouraged individuals with past domestic violence incidents to support exploring the link between domestic violence and mass incidents.
- Believed that restorative justice is possible but emphasized the need for safeguards.
- Stressed the importance of studying the domestic violence connection to find effective solutions.
- Asserted that addressing domestic violence links may be more impactful than focusing on specific weapon types.
- Mentioned a fundraiser for domestic violence shelters coming up soon.

# Quotes

- "When it comes to mass incidents of this sort, these statistics are wild."
- "This is where you need to look."
- "There's a link. It needs to be explored."
- "The solution is there."
- "It's just a thought."

# Oneliner

Beau speculates on the domestic violence connection in the recent Colorado incident, urging exploration of the link to find effective solutions in addressing mass incidents.

# Audience

Advocates, activists, community members.

# On-the-ground actions from transcript

- Support exploring the link between domestic violence and mass incidents (suggested).
- Advocate for closing all loopholes related to domestic violence in gun control legislation (implied).
- Get ready to participate in the upcoming fundraiser for domestic violence shelters (implied).

# Whats missing in summary

Beau's personal anecdotes and experiences related to the topic.

# Tags

#Colorado #DomesticViolence #GunControl #Statistics #Advocacy