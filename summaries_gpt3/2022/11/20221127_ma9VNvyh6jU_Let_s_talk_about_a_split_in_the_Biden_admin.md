# Bits

Beau says:

- There is a perceived split within the Biden administration regarding foreign policy on Ukraine.
- The Biden administration's policy is "Nothing about Ukraine without Ukraine."
- General Milley's suggestion for Ukraine to negotiate is not a split in the administration, but a military perspective.
- Milley believes that Russia has two options: withdraw or face a prolonged and devastating war.
- Ukraine has entered a protracted conflict, making it difficult for Russia to advance further.
- Milley gives Russia less than a 1% chance of winning in the long run.
- Media misreported Milley's statement about Ukraine not pushing Russia out anytime soon.
- The situation in Ukraine is turning into a hammer and anvil scenario for Russia.
- Russia faces a high likelihood of losing if the conflict continues.
- Milley emphasized the need for a political option due to the current situation in Ukraine.

# Quotes

"Nothing about Ukraine without Ukraine."

"War is a continuation of politics by other means."

"Russia has two options. They withdraw or they get themselves embroiled into a years-long war."

"If this goes on, it's going to be a long war."

"Ukraine can't push Russia out anytime soon."

# Oneliner

There is no split in Biden's team on Ukraine; it's about understanding the military perspective amidst a protracted conflict.

# Audience

Foreign policy enthusiasts

# On-the-ground actions from transcript

- Contact local representatives to advocate for a diplomatic resolution (suggested)
- Join organizations supporting peace efforts in Ukraine (implied)

# Whats missing in summary

The full transcript provides a detailed analysis of the military perspective on the Ukraine conflict and the potential outcomes based on current situations.

# Tags

#Ukraine #BidenAdministration #ForeignPolicy #MilitaryPerspective #Diplomacy