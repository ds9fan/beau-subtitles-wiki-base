# Bits

Beau says:

- Addresses the question of why leftists should vote, pointing out comments that question the differences between political parties and their economic policies.
- Distinguishes between leftists and liberals on the channel, focusing on economic perspectives.
- Illustrates the economic perspective by categorizing both parties as right-wing from a leftist viewpoint, citing lack of support for worker control.
- Raises the scenario of attending a place where people wear "vote blue no matter who" shirts and questions whether one feels comfortable leaving a friend alone based on the crowd's political affiliations.
- Emphasizes the differences in treatment towards marginalized groups by the political parties.
- Encourages reflection on what drove individuals to embrace a leftist ideology, likely rooted in an egalitarian outlook and a desire to alleviate suffering.
- Acknowledges the economic similarities between parties but stresses that there are more aspects to the electoral process and having a leftist perspective.
- Concludes by leaving the decision to vote or not based on individual considerations and perspectives.

# Quotes

- "The parties aren't the same."
- "You have that difference that's pretty remarkable between the two parties."
- "But they're not identical."

# Oneliner

Beau explains why leftists should vote by delineating the differences in treatment towards marginalized groups by political parties, beyond just economic policies.

# Audience

Leftist voters

# On-the-ground actions from transcript

- Have open and honest discussions with friends and peers about the importance of voting and the distinctions between political parties (suggested).
- Advocate for marginalized groups and ensure their voices are heard in the electoral process (implied).

# Whats missing in summary

The full transcript provides a nuanced perspective on why leftists should vote, focusing on the treatment of marginalized groups by political parties and the broader aspects of having a leftist ideology.

# Tags

#Voting #Leftists #PoliticalParties #EconomicPolicies #MarginalizedGroups