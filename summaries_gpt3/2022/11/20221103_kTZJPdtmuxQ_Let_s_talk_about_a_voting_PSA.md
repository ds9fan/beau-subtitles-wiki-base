# Bits

Beau says:

- Providing a public service announcement regarding misleading text messages about voting locations.
- Urging people to fact-check information received via text messages with real sources.
- Warning about a company sending inaccurate voting information in Kansas, New Jersey, Illinois, North Carolina, and Virginia.
- Mentioning NBC's report about a similar issue in Oregon with the same company.
- Advising recipients of unsolicited texts to block the number, potentially missing correction texts.
- Emphasizing the importance of verifying voting locations to avoid unintentional or intentional voter suppression.
- Noting that this election's outcome relies on unlikely voters, making informed choices based on accurate information critical.
- Encouraging individuals, especially those who don't normally vote, to ensure their voting information is correct and have a plan to cast their vote.
- Pointing out the potential impact of misinformation via text messages on voters who are less familiar with the voting process.
- Stressing the significance of taking control of one's voting information to prevent malicious actors from suppressing votes.

# Quotes

- "Don't trust the text messages."
- "Make sure you are checking with real sources about where you're supposed to go vote if you plan to."
- "It's the unlikely voters who are going to rule this election."
- "Make sure you know where to go, you have a plan to get there, and all of that stuff."
- "There are certainly going to be malicious actors trying to suppress the vote, and this is a good way to do it."

# Oneliner

Beau provides a public service announcement: Verify voting locations, as misleading text messages could lead to voter suppression, impacting the voice of unlikely voters critical in this election.

# Audience

Voters

# On-the-ground actions from transcript

- Verify your voting location with official sources (suggested).
- Block unsolicited numbers sending voting information and seek accurate sources (suggested).
- Ensure you have a plan to cast your vote and know where to go (suggested).

# Whats missing in summary

Importance of staying vigilant against voter suppression tactics.

# Tags

#VoterSuppression #Election2022 #Misinformation #PublicService #VotingRights