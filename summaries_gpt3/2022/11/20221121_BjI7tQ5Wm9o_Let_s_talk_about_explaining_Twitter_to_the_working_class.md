# Bits

Beau says:

- Critiques the notion that it's impossible to explain Twitter's situation to the working class.
- Mentions reading think pieces claiming the working class can't comprehend certain concepts.
- Questions the idea of dividing the working class into "laptop class" and working class.
- Illustrates a scenario involving a construction worker to explain complex changes at work.
- Expresses skepticism towards those who believe the working class cannot grasp certain economic concepts.
- Emphasizes the importance of directly engaging with the working class rather than making assumptions.

# Quotes

- "Just because we talk slow doesn't mean we think slow."
- "Your boss is asking you to show up for work and you say no? And you're already making 125 grand a year."
- "If you are part of the think peace crowd in these magazines, maybe talk to the working class."

# Oneliner

Beau explains Twitter's situation using a construction scenario, challenging assumptions about the working class's understanding.

# Audience

Working-class individuals

# On-the-ground actions from transcript

- Talk directly to the working class to understand their perspectives (suggested)
- Bridge the gap between different socioeconomic groups through open communication (implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on the working class's ability to comprehend complex issues when explained effectively.