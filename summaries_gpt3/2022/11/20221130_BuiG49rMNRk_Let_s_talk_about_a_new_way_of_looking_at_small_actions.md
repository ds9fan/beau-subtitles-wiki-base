# Bits

Beau says:

- Introducing the concept of changing the world through small steps and a new way to frame it.
- Encouraging small acts that may seem insignificant but collectively can make a difference.
- Emphasizing the idea of doing what you can, where you can, for as long as you can to make a positive impact.
- Comparing the impact of small present actions on the future to the concept of changing the present by altering something in the past.
- Noting how time travel tropes in TV shows and movies often illustrate how small actions can greatly impact outcomes.
- Presenting a unique perspective by linking present actions to future consequences through the lens of time travel tropes.
- Encouraging reflection on the significance of individual actions and their potential long-term effects on the world.
- Posing a rhetorical question about the potential outcomes if certain actions were taken decades ago.
- Humorously mentioning the desire for time travel to fix past mistakes, adding a lighthearted touch to the thought-provoking concept.

# Quotes

- "Be the change you want to see in the world."
- "If somebody had done it 50 years ago, what would the outcome be today?"
- "Time travel. When? Really doesn't matter."

# Oneliner

Beau introduces changing the world through small steps, linking present actions to future consequences akin to time travel tropes, urging reflection on individual impact.

# Audience

Change-makers, Future-thinkers

# On-the-ground actions from transcript

- Encourage and practice small acts of kindness and positive change in your daily life (exemplified).
- Take a moment to think about how your present actions might shape the future in a significant way (suggested).

# Whats missing in summary

The full transcript dives deeper into the idea of individual agency and the potential long-term impact of seemingly small actions on the world.

# Tags

#ChangeTheWorld #SmallSteps #TimeTravel #Impact #Reflection