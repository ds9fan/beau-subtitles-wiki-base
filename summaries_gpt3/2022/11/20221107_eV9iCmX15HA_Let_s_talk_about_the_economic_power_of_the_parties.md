# Bits

Beau says:
- Addresses a comment on economic power and political leanings in the US.
- Mentions a message received about conservative economic power and implications.
- Breaks down the GDP share between Republicans and Democrats based on county wins.
- Analyzes the economic disparity between Republican and Democratic counties.
- Suggests that the perception of conservative economic power is manufactured and not real.
- Points out that companies cater to diverse and tolerant viewpoints due to economic reasons.

# Quotes

- "Not nice word about me. Yeah, you bunch of not nice words about me are brainwashing the young."
- "Your math is wrong. Your belief is wrong. Your ideas are really bad."
- "The reason large companies are catering to the more diverse, more tolerant, more accepting viewpoints is because that's where the money is."

# Oneliner

Beau addresses misconceptions about conservative economic power and exposes the economic disparity between Republican and Democratic counties in the US.

# Audience

Political analysts, concerned citizens

# On-the-ground actions from transcript

- Fact-check economic claims and share accurate information (implied)
- Support policies that address economic disparities (implied)

# Whats missing in summary

Deeper insights on the impact of economic disparities on political perceptions and societal divisions.

# Tags

#EconomicPower #PoliticalLeanings #USPolitics #IncomeInequality #FactChecking