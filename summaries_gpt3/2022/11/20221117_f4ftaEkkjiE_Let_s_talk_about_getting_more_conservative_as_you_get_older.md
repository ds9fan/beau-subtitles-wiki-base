# Bits

Beau says:

- Addresses the misconception that people become more conservative as they age.
- Explains that older demographics used to become more conservative due to amassing wealth, but this is no longer the case.
- Notes that getting older doesn't mean gaining more wealth or capital in the current socio-political climate.
- Points out that the Republican Party, despite the image of being fiscally conservative, has shifted away from this stance.
- Describes how the Republican Party has embraced culture war issues, moving away from fiscal conservatism.
- Talks about the progressive trend in society over time and how conservatives are dragged into the future.
- Mentions how individuals who do not evolve with society do not become more conservative, but rather remain static in their beliefs.
- Explains that the conservative party has slowly become more progressive, making older individuals identify more with them.
- Comments on the Republican Party's resistance to progress and how it alienates voters, particularly the younger demographic.
- Concludes that the Republican Party's outdated positions and failure to advance are causing them to lose elections and alienate their base.

# Quotes

- "People don't become more conservative. Not as a general trend. That's not a thing."
- "The world, as a trend, becomes more progressive, more accepting, more open."
- "Conservatives are back there at the backside of society, and they're getting just dragged into the future."
- "The conservative party slowly became more progressive. And then they eventually line up."
- "The longer they hold on to this, the worse it's going to get."

# Oneliner

Beau dismantles the myth that people naturally become more conservative with age, revealing how societal progress shapes political ideologies.

# Audience

Voters, political observers

# On-the-ground actions from transcript

- Reassess political affiliations based on current stances and not historical perceptions (implied).
- Encourage ongoing education and evolution of personal beliefs to stay in line with societal progress (implied).

# Whats missing in summary

The full transcript provides a detailed breakdown of how societal progress influences political ideologies, urging individuals to critically analyze their beliefs in the context of evolving social norms.

# Tags

#Conservatism #Progressivism #RepublicanParty #SocietalChange #PoliticalBeliefs