# Bits

Beau says:

- Explains the importance of reestablishing social networks when a social media platform disappears, specifically addressing communities within Twitter.
- Advises maintaining continuity by using the same profile picture and handle when transitioning to a new social media platform.
- Suggests hashtagging your community or group to make it easier for others to find you on new platforms.
- Recommends interacting with individuals who have large followings within your community to reconnect with familiar faces.
- Encourages creating cross-platform communities to strengthen networks and increase resources.
- Acknowledges the fear and concern within disabled Twitter communities regarding losing their network.
- Emphasizes the shared passion and common interests that bind communities together, making it easier to reconnect.
- Urges individuals to establish their community on multiple social media outlets to prepare for any platform changes.
- Reminds people that while social media platforms may change, the supportive community will remain strong.
- Advocates for helping one another and maintaining connections during platform transitions.

# Quotes

- "Make yourself easy to find for people that are looking for you."
- "Y'all have something in common. Y'all have something that y'all share."
- "It's just people helping people."
- "Twitter was never really your friend or your home. It was just a company."
- "Just make yourself identifiable to those within your community, and they'll find you."

# Oneliner

Beau advises on maintaining continuity and finding community leaders to reestablish social networks when transitioning between social media platforms.

# Audience

Social media users

# On-the-ground actions from transcript

- Maintain continuity by using the same profile picture and handle on new social media platforms (implied)
- Hashtag your community or group to make it easier for others to find you on different platforms (implied)
- Interact with individuals who have large followings within your community to reconnect with familiar faces (implied)
- Establish your community on multiple social media outlets to prepare for any platform changes (implied)

# Whats missing in summary

The full transcript provides detailed guidance on reestablishing social networks when transitioning between social media platforms, focusing on continuity, community leaders, and cross-platform presence.

# Tags

#SocialMedia #CommunityBuilding #OnlineNetworks #Transition #CrossPlatform