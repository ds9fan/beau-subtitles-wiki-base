# Bits

Beau says:

- Beau talks about a NASA official's statement regarding people living on the moon in this decade.
- The official from the Orion Lunar Space Program mentioned habitats and rovers on the moon's surface for extended durations.
- Technological advances are propelling us towards what was previously considered science fiction.
- The expectation is that by 2030, or by 2032 at the latest, people will be living on the moon.
- This progress signifies the first step towards real space exploration, expanding beyond current limitations.
- Beau expresses his surprise at the idea of people living on the moon and how it will impact technological advancements on Earth.
- The increased space exploration will likely lead to more innovations benefiting daily life.
- There is a belief in a forthcoming new era of exploration with vast possibilities beyond current understanding.
- Beau concludes by saying this marks a significant moment in space exploration.
- The focus is on the rapid rate of technological advancements guiding us toward what was once deemed science fiction.

# Quotes

- "Certainly in this decade, we are going to have people living for durations… They will have habitats. They will have rovers on the ground."
- "To me, that is just wild."
- "We're kind of on the verge of a new era when it comes to this sort of exploration."

# Oneliner

A NASA official predicts people will live on the moon within this decade, marking a significant leap in space exploration and technological progress.

# Audience

Space enthusiasts, scientists

# On-the-ground actions from transcript

- Stay updated on space exploration developments and research (suggested)
- Support initiatives that advance space exploration and technology (implied)
- Encourage STEM education and interest in space sciences (implied)

# Whats missing in summary

Importance of supporting scientific advancements and space exploration for future innovation and discovery.

# Tags

#SpaceExploration #NASA #MoonLiving #TechnologicalAdvancements #STEM