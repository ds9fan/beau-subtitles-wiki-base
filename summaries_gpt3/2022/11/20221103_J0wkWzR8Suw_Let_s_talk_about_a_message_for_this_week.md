# Bits

Beau says:

- Urges the audience to understand a critical message within a week due to current events.
- Addresses political violence and the quest for power through symbolic targets and candidates.
- Emphasizes the importance of not allowing violent rhetoric to be a path to power.
- Warns that those currently unaffected will eventually become targets.
- Stresses the impact of authoritarianism on free enterprise and small businesses.
- Condemns those who deny election results and use violent rhetoric for perpetuating dangerous ideologies.
- Expresses the failure to send a strong enough message in the 2020 election.
- Calls for a rejection of election deniers and promoters of violent rhetoric to protect freedom.

# Quotes

- "Anywhere is a threat to freedom everywhere."
- "This is freedom and tyranny."
- "Those people should not get your vote."

# Oneliner

Beau stresses the urgent need to reject violent rhetoric as a path to power and confront the threat to democracy before it impacts everyone.

# Audience

Every concerned citizen

# On-the-ground actions from transcript

- Reject election deniers and promoters of violent rhetoric (exemplified)
- Educate others on the dangers of authoritarianism (exemplified)
- Advocate for democracy and freedom in your community (suggested)

# Whats missing in summary

The emotional depth and urgency conveyed by Beau's message can be best experienced in the full transcript.

# Tags

#CommunitySafety #Democracy #Authoritarianism #PoliticalViolence #Freedom