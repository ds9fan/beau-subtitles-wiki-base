# Bits

Beau says:

- 150 artifacts were returned to the Oglala and Cheyenne River Sioux from a museum in Massachusetts.
- These artifacts were believed to have been taken from a grave site at Wounded Knee and include clothing, weapons, and personal effects.
- The museum, although not legally obligated, returned the artifacts because it was the right thing to do.
- The incident at Wounded Knee in 1890 resulted from a lack of understanding by the U.S. of Native culture, leading to a tragic massacre.
- Museums, colleges, and the federal government possess around 850,000 artifacts, with more than 100,000 being human remains, still not returned to their rightful owners.
- The theft of Native culture is an ongoing issue with numerous human remains and artifacts not where they belong.
- Despite the clear ownership of these artifacts, delays in returning them are often due to financial reasons or the belief that the original owners don't need them back.
- The return of these artifacts is critical, and stories like this need more attention to drive further action.
- The rightful owners of the artifacts are indisputable, and it's imperative to continue shedding light on this issue.
- The return process may be prolonged due to various reasons, but efforts must persist until justice is served.

# Quotes

- "The theft of Native culture is an ongoing issue."
- "More than 100,000 articles that are human remains are in the possession of museums."
- "These stories will happen more and more often, hopefully in larger quantities."
- "There's no dispute about who the rightful owners of this property are."
- "It is vital to keep the focus on it."

# Oneliner

Beau sheds light on the ongoing issue of artifacts taken from Native grave sites, stressing the importance of their return and the need for continued awareness and action.

# Audience

History enthusiasts, activists

# On-the-ground actions from transcript

- Contact museums and institutions to urge the return of artifacts and human remains (suggested)
- Support initiatives advocating for the repatriation of stolen cultural items (implied)

# Whats missing in summary

The emotional weight and historical significance conveyed by Beau's storytelling.

# Tags

#ArtifactReturn #NativeCulture #Repatriation #CulturalHeritage #Awareness