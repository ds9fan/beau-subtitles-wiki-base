# Bits

Beau says:

- Evangelicals are turning on Trump, distancing themselves from the former president since the midterms.
- Mike Evans and James Robinson, prominent figures, criticize Trump for his actions and behavior.
- Everett Piper from The Washington Times bluntly states that Trump has to go, predicting destruction in 2024 if he's the nominee.
- The focus is not on Trump's declining poll numbers but on the impact on declining church memberships.
- Right-wing talking points often reference decreasing church memberships, with Beau attributing it to the support of Trump.
- Beau criticizes the church for preaching hate instead of the gospel and using political power to enforce beliefs.
- Inserting religious organizations into politics leads to distrust, hypocrisy, and manipulation, undermining their core mission.
- Beau points out the hypocrisy of church leaders who had a change of heart about Trump post-midterms but didn't rule out supporting him later.
- The intertwining of church and state leads to church leadership behaving like politicians, causing failing church memberships.
- Beau suggests failing church leadership is a significant factor in declining church memberships.

# Quotes

- "Donald Trump has to go."
- "The problem isn't that you're turning on Trump. The problem is that you ever supported him to begin with."
- "When it is this transparent, all of these church leaders, they become politicians."
- "You want to talk about failing church memberships? It tends to be a result of failing church leadership."
- "It's just a thought."

# Oneliner

Evangelicals turning on Trump post-midterms impacts declining church memberships due to intertwining of politics and religion.

# Audience

Religious community members

# On-the-ground actions from transcript

- Reassess church leadership roles and behaviors (suggested)
- Advocate for separation of church and state (implied)

# Whats missing in summary

The full transcript provides a deeper dive into the relationship between politics, religion, and declining church memberships.

# Tags

#Evangelicals #DonaldTrump #ChurchLeadership #ReligionInPolitics #SeparationOfChurchAndState