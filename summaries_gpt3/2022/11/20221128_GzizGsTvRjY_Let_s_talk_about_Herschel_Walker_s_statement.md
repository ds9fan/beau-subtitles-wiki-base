# Bits

Beau says:

- Ignored Herschel Walker's campaign due to lack of policy statements, resembling stand-up comedy routines.
- Walker expressed the belief that young people born after 1990 haven't earned the right to change the country.
- Beau disagrees with Walker's sentiment, stating that people don't die for the national anthem but for those around them.
- Beau mentions friends with prosthetics, a diverse group that got their injuries in their early 20s.
- American KIA are typically young, and statistics show the average age of individuals in wars is not in the age group Walker mentioned.
- Beau argues that the younger generation has every right to try to change the country as they will bear the consequences if it remains unchanged.
- He suggests that the younger generation, having grown up with unlimited access to information, may have different ideas because they are better informed.
- Beau points out that people who grew up with the internet have the right to exercise their voting rights.
- He questions the impact of Walker's statement on the election, particularly in a state like Georgia with numerous military installations.
- Beau implies that Walker's views may not resonate well in areas like Columbus, which have a keen interest in building a better future rather than clinging to an idealized past.

# Quotes

- "Nobody has ever died for the national anthem. They died for the people around them."
- "The younger crop, they have every right to try to change this country, because they're going to be the ones who pay the price if it doesn't change."
- "Maybe they have different ideas because they're better informed."

# Oneliner

Beau challenges Walker's belief that young people haven't earned the right to change the country, advocating for their voices and perspectives based on informed decisions and future implications.

# Audience

Voters, Young Activists

# On-the-ground actions from transcript

- Challenge outdated beliefs and encourage informed decision-making (implied)
- Support and empower young voices in shaping the future of the country (implied)

# Whats missing in summary

The full transcript provides additional context on Herschel Walker's statements and Beau's insightful analysis on the younger generation's right to create change based on informed decision-making.

# Tags

#YouthEmpowerment #VotingRights #InformativeActivism #ChallengingBeliefs #FutureLeaders