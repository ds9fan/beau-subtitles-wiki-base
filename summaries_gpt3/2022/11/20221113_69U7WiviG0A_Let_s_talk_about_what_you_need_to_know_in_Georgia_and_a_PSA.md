# Bits

Beau says:

- Talks about the control of the Senate with all eyes on Georgia.
- Mentions the possibility of the Democratic Party maintaining control without Georgia, but it's slim.
- Emphasizes the significant impact of the runoff in Georgia on Senate control.
- Points out the massive amounts of money both parties will invest in the Georgia runoff.
- Expects efforts to discourage voting and lots of mudslinging due to the high stakes.
- Notes the national attention on Georgia's runoff election.
- Provides information on Georgia's absentee ballot application deadlines.
- Believes that if people show up, the results will favor the Democratic Party.
- Points out reasons why the Democratic Party may have an edge in Georgia.
- Warns about historical efforts in Georgia to suppress certain votes and dissuade people from voting.

# Quotes

- "Both parties are going to just dump untold amounts of money into it."
- "The results should trend towards the Democratic Party, as long as people show up."
- "Just be aware of the obstacles that the state establishment is likely to throw up in your way."

# Oneliner

Both parties focus on Georgia's runoff, expect mudslinging, and obstacles to voting.

# Audience

Georgia voters

# On-the-ground actions from transcript

- Submit absentee ballot applications by deadlines (implied)
- Be aware of potential obstacles and efforts to discourage voting (implied)

# Whats missing in summary

Importance of active voter participation in the Georgia runoff. 

# Tags

#SenateControl #GeorgiaRunoff #VotingObstacles #DemocraticParty #BeInformed