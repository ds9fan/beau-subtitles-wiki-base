# Bits

Beau says:

- Explains the different approaches used by Midas Touch and Stone Kettle in analyzing data related to polling places.
- Midas Touch utilized data analysis, including early voting numbers and registrations, to make predictions.
- Stone Kettle's approach involved pointing out the limitations of polling, such as only reaching those willing to answer unknown phone numbers.
- Beau shares his method of analyzing YouTube analytics and social media engagement to predict an increase in voter turnout among younger demographics.
- Criticizes the media's heavy reliance on polls, suggesting a need for new polling methods and less emphasis on polling results.
- Advocates for journalists to give candidates a platform to speak freely about their platforms, allowing viewers to make informed decisions.
- Believes that democracy requires informed citizenship and accurate polling to capture the whole picture.
- Notes that younger voters, often overlooked in polls, showed up unexpectedly in the recent election, catching pollsters off guard.

# Quotes

- "The analytics that YouTube provides is scary."
- "Democracy requires advanced citizenship."
- "Polls have to be accurate, but they were only measuring part of the picture."

# Oneliner

Beau shares insights on different data analysis approaches, media reliance on polls, and the need for informed citizenship in democracy.

# Audience

Media consumers

# On-the-ground actions from transcript

- Analyze data from various sources to understand trends and patterns in your community (implied).
- Encourage journalists to provide platforms for candidates to speak directly about their platforms (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of different data analysis approaches, the limitations of polling, and the importance of informed citizenship in democracy.

# Tags

#DataAnalysis #Polling #Media #Democracy #InformedCitizenship