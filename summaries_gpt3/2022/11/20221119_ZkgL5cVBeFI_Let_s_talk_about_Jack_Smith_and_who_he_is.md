# Bits

Beau says:

- Introducing Jack Smith, a significant figure soon to be in the news with an impressive background.
- Jack Smith grew up in New York, attended Harvard Law, and worked with the New York District Attorney's Office.
- He later moved on to the US Attorney's Office in Brooklyn and then to the International Criminal Court.
- Smith also served as chief of Public Integrity for five years and became Assistant United States Attorney for the Middle District of Tennessee.
- In 2018, he took an appointment at The Hague regarding Kosovo and now holds a new appointment as special counsel.
- While at Public Integrity, Smith was known for being apolitical, which is a critical aspect of his character.
- His reputation suggests he is not someone looking to gain political favors or play political games.
- Smith's background and experience make him a potential candidate for those hoping for a prosecution, particularly involving Trump.
- Despite uncertainties, Smith's background indicates seriousness rather than a mere show of authority.
- The possibility of Smith being involved in prosecuting Trump, especially in cases regarding documents, seems promising.

# Quotes

- "This isn't the background of somebody who is looking to score chips in the big game and get political favors later."
- "If you were actually looking to prosecute, this is kind of the background you would look for."

# Oneliner

Beau introduces Jack Smith, hinting at his potential role in upcoming news, showcasing his extensive legal background, particularly his apolitical stance, making him a promising candidate for those seeking prosecutions.

# Audience

Legal enthusiasts

# On-the-ground actions from transcript

- Stay informed about the developments involving Jack Smith and his potential role in legal proceedings (implied).

# Whats missing in summary

Full context and depth of analysis.

# Tags

#Legal #Prosecution #JackSmith #Background #Apolitical