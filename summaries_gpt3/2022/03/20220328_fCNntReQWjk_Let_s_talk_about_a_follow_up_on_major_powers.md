# Bits

Beau says:

- Responds to a viewer's message about NATO and whether major powers are inherently good.
- Compares Russia and the US in terms of imperialism and maintaining empire.
- Explains how the US maintains its empire primarily through economic power.
- Talks about China's strategy of state-to-state deals for stability and economic gain.
- Emphasizes that foreign policy is about power, not right or wrong.
- Stresses the need to move past nationalism for a better world.
- Advocates for a broader perspective that transcends borders and focuses on commonalities among people.

# Quotes

- "No major power is good."
- "Foreign policy isn't about right and wrong, good and evil. It's about power."
- "Nationalism is politics for basic people."
- "We have more in common with the soldiers in Ukraine than with our own representative in DC."
- "We have to start thinking broader than a border."

# Oneliner

Beau responds to a viewer's message, comparing major powers' imperialism, explaining US economic empire maintenance, and advocating for a broader perspective to transcend nationalism for a better world.

# Audience

Global citizens

# On-the-ground actions from transcript

- Communicate with people globally to foster understanding and unity (implied)
- Shift focus from nationalism to global commonalities for international political change (implied)

# Whats missing in summary

Beau's detailed analysis and insights on major powers, imperialism, and the need to transcend nationalism for a better world.

# Tags

#ForeignPolicy #Empire #Nationalism #GlobalUnity #PowerRelations