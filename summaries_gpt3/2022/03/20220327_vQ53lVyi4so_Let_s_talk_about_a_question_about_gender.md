# Bits

Beau Young says:

- Addressing a question on gender and biology that has arisen in various platforms, including Congress.
- Explaining societal norms around gendered colors and toys, showcasing the lack of biological link.
- Pointing out how politicians use gender and biology debates as a tool for manipulation.
- Gender is a societal perception that changes as societal views evolve.
- Gender is not inherently tied to biology; it's a perception shaped by society.
- Pink and blue being associated with girls and boys is a societal norm, not a biological fact.
- The gendering of products like toys, coffee cups, and marketing strategies is based on societal norms, not biology.
- Society's perception of gender changes over time, showing its lack of biological basis.
- Politicians manipulate the gender and biology debate to control certain groups by creating division and distraction.
- Gender is not fixed and has been viewed differently in various societies throughout history.

# Quotes

- "Gender is a perception. It's not linked to biology."
- "It's a societal perception."
- "Politicians manipulate the gender and biology debate to control those people who see this as a serious affront to their values."
- "Gender is not linked to biology in any way."
- "A whole lot of societies throughout history had more than two genders, just so you know."

# Oneliner

Beau Young explains how gender is a societal perception, not linked to biology, debunking the manipulation by politicians.

# Audience

Educators, Activists, Parents

# On-the-ground actions from transcript

- Challenge gender stereotypes by promoting gender-neutral colors and toys (exemplified)
- Educate others on the societal perception of gender (suggested)
- Advocate for inclusive policies and representations of gender diversity (suggested)

# Whats missing in summary

In-depth examples of how societal norms shape perceptions of gender and the historical context of gender fluidity could be better understood by watching the full transcript.

# Tags

#Gender #Society #Biology #Perception #Politics