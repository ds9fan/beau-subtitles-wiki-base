# Bits

Beau says:

- Addresses the struggle of staying positive amidst doomscrolling and feeling helpless.
- Acknowledges the importance of celebrating small wins in the midst of chaos.
- Shares a heartwarming example of a little girl singing in a bomb shelter bringing warmth and hope.
- Encourages people to find and acknowledge moments of positivity in their lives.
- Advises to not compare activism efforts with others; it's not a competition.
- Advocates for doing what you can, where you can, for as long as you can.
- Emphasizes the importance of taking breaks and not letting awareness consume you.

# Quotes

- "Acknowledge the wins."
- "It's not a competition."
- "There's nothing wrong with feeling sad or angry."
- "There's nothing wrong with acknowledging the wins, even if they're small."
- "Y'all have a good day."

# Oneliner

Beau addresses staying positive amidst chaos, celebrating small wins, and avoiding comparison in activism efforts.

# Audience

Individuals seeking guidance on staying positive amidst challenging times.

# On-the-ground actions from transcript

- Acknowledge the wins in your life, no matter how small (implied).
- Celebrate moments of positivity and hope (implied).
- Take breaks when needed to prevent feeling overwhelmed (implied).

# Whats missing in summary

The importance of finding moments of joy and hope amidst difficult circumstances.

# Tags

#StayingPositive #CelebratingWins #Activism #SelfCare #CommunitySupport