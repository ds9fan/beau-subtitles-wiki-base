# Bits

Beau says:

- Russia has become the most sanctioned country globally, surpassing Iran, with 5,500 different sanctions.
- The chief of the Russian space agency announced that they will stop delivering rocket engines to US customers for space.
- NASA's program called Artemis is set to return humans to the moon.
- Artemis allows people to have their names flown around the moon by signing up on their website.
- This initiative aims to involve young people and kids in space exploration.
- It serves as a unique diversion from the constant issues on Earth.
- People overwhelmed by world events can find hope and distraction in projects like Artemis.
- Beau suggests signing up for Artemis' boarding pass if you have kids or are interested.
- He believes the US will manage fine despite Russia's sanctions and it won't impact NASA significantly.
- Beau encourages viewers to find moments of positivity and cheer in the midst of the relentless news cycle.

# Quotes

- "There are slivers of hope. There are things that can cheer you up and can divert your attention from the constant news cycle."
- "So if you have kids, it might be something worth doing with them, getting them a boarding pass for it."
  
# Oneliner

Russia, the most sanctioned country, retaliates against the US while NASA's Artemis program offers hope and diversion through lunar involvement for all, especially the youth.

# Audience

Space enthusiasts, parents, curious minds.

# On-the-ground actions from transcript

- Sign up for NASA's Artemis program to have your name flown around the moon (suggested).
- Involve kids in space exploration by getting them a boarding pass for Artemis (suggested).

# Whats missing in summary

Details on how individuals can sign up for NASA's Artemis program and get involved in space exploration firsthand.

# Tags

#Russia #Sanctions #NASA #Artemis #SpaceExploration #Hope