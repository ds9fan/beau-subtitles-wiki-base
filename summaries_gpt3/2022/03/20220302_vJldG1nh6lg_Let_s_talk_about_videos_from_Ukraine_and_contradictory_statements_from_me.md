# Bits

Beau says:

- Addresses seemingly contradictory statements he has made and the need for additional context to understand them.
- Explains the advantage of amateurs over professional soldiers in terms of predictability and surprise.
- Talks about the importance of speed, surprise, and violence of action to win an engagement.
- Mentions the Ukrainian resistance confusing the Russian military with unpredictable tactics.
- Describes videos from Ukraine showing unconventional tactics against armored vehicles.
- Points out that amateurs in combat often have the element of surprise but may not always be effective.
- Emphasizes that in unconventional conflicts, it's about breaking resolve rather than traditional military victory.
- States the importance of endurance and resilience in prolonged conflicts.
- Warns against glorifying civil conflict and encourages reflecting on the true nature of such conflicts.
- Recommends picturing the reality of conflict through images like cars with "children" signs rather than romanticizing videos of resistance fighters.

# Quotes

- "Professional soldiers are predictable, but the world is full of amateurs."
- "In these types of conflicts, it's not about who can dish out the most, it's about who can take the most."
- "Eventually, either the hammer or the anvil is going to break. It's normally the hammer."

# Oneliner

Beau delves into the dynamics of predictability, surprise, and endurance in unconventional conflicts, cautioning against romanticizing resistance videos and urging a realistic perspective on civil conflict.

# Audience

Observers of conflicts

# On-the-ground actions from transcript

- Analyze and understand the dynamics of unconventional conflicts (implied)
- Avoid glorifying or romanticizing civil conflict (implied)

# Whats missing in summary

In watching the full transcript, one can better grasp the nuanced realities of unconventional conflicts and the dangers of glorifying civil conflict.

# Tags

#ConflictDynamics #AmateurVsProfessional #UnconventionalTactics #CivilConflict #Realism