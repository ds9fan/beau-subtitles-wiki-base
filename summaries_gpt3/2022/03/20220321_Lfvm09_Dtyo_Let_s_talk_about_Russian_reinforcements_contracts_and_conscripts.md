# Bits

Beau says:

- Explains the structure of the Russian military and details about contract troops and conscripted troops.
- Contract troops, around 350,000 to 410,000, are the core and elite units of the Russian military.
- Conscripted troops, around 260,000 additional forces drafted twice a year, are not well-trained and only serve for a year.
- Russia's total forces on the high end are around 670,000, not all of which are combat-ready.
- Due to commitments and reserves needed for defensive purposes, Russia may only have about 150,000 troops available for offensive actions.
- Russian reserves of 2 million consist mainly of individuals who were in the military and are still young enough to be recalled.
- Only around 5,000 out of 2 million Russian reserves actively train regularly, unlike traditional reserves seen in other countries.
- Russia lacks the numbers and trained personnel required for a successful large-scale military operation in Ukraine.
- A potential massive call-up could train more troops, but the success of such training remains questionable.
- The likelihood of Russia effectively running a professional military operation with its current resources seems bleak.

# Quotes

- "They don't have the numbers."
- "It's math at this point."
- "The only choice Russia is going to have is to do what they've been doing in Mariupol."

# Oneliner

Beau breaks down the Russian military's numbers, revealing challenges in mounting a successful operation in Ukraine due to lack of trained personnel and numbers.

# Audience

Military analysts

# On-the-ground actions from transcript

- Monitor developments in the conflict in Ukraine (implied)
- Stay informed about military capabilities and limitations (implied)

# Whats missing in summary

Insights on the potential geopolitical impacts of Russia's military limitations.

# Tags

#RussianMilitary #Conscripts #ContractTroops #MilitaryReserves #UkraineConflict