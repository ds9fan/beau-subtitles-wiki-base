# Bits

Beau says:

- Different countries have their own national lens that influences how they view the world and themselves.
- The lens is shaped by media discourse and narratives pushed by politicians, impacting societal views.
- Examples like Russians not calling the war in Ukraine a war and states in the US avoiding the term climate change show how narratives can shape policy.
- Authoritarian regimes use tactics like controlling narratives to maintain the status quo and prevent demands for change.
- In the US, there are instances where topics are avoided in fear of upsetting the status quo and sparking demands for change.
- Media and politicians play a significant role in shaping public perception and what topics are deemed acceptable for discourse.
- The playbook of authoritarian leaders involves restricting certain topics to prevent challenges to their power.
- The goal is to ensure that those without much power do not demand change by marginalizing certain topics and people.
- By examining how other countries handle certain issues, it provides a reflection for individuals to analyze their own national lens.
- The avoidance of discussing certain topics is a common tactic used to maintain the status quo and prevent societal change.

# Quotes

- "Don't say climate change. Don't say war. Don't say history. Don't say gay."
- "The only thing that really changes is what they don't want you to talk about."
- "They care about maintaining the status quo and making sure that those without a lot of institutional power, well, they don't ask for change."

# Oneliner

Different national lenses shape views globally, impacting policies and narratives to maintain the status quo and deter change.

# Audience

Global citizens

# On-the-ground actions from transcript

- Challenge narratives pushed by media and politicians (implied)
- Encourage open discourse on topics that are marginalized (implied)

# Whats missing in summary

The full transcript provides a deeper dive into how narratives influence policy and societal views, urging individuals to critically analyze their national lens. 

# Tags

#NationalLens #Narratives #PolicyInfluence #Authoritarianism #SocietalChange