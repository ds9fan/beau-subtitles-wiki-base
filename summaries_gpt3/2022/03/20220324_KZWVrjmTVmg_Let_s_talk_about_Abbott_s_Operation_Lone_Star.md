# Bits

Beau says:

- Operation Lone Star in Texas uses the Texas Military Department and National Guard troops under state authority.
- Troops are attempting to unionize due to poor conditions.
- ProPublica, a news outlet, found issues with Operation Lone Star's success claims.
- Texas is taking credit for arrests and seizures that occurred before the operation.
- Governor Abbott's claims of success with Operation Lone Star are being called into question.
- Many of the claimed arrests are unrelated to the border situation.
- Only a fraction of the claimed fentanyl seizures actually came from Operation Lone Star.
- The operation is seen as a political stunt and an election device.
- It aims to portray certain people as bad and the governor as taking action.
- The operation costs Texas $2.5 million a week, totaling $3 billion.
- Beau suggests that the money could be better spent, possibly on improving the electric grid.
- Operation Lone Star's lack of success is indicated by the troops' attempt to unionize.

# Quotes

- "Operation Lone Star is using the Texas Military Department, National Guard troops under state authority."
- "It's a stunt. It's an election device."
- "Imagine what else Texas could use that money for."
- "Operation that is so bad, it has National Guard troops trying to unionize."
- "Not really that successful."

# Oneliner

Beau breaks down Operation Lone Star in Texas, exposing its flaws and political motives while questioning its claimed success and high cost.

# Audience

Texans, Activists

# On-the-ground actions from transcript

- Contact local representatives to express concerns about Operation Lone Star's effectiveness and cost (implied).
- Support investigative journalism like ProPublica by sharing their reports and subscribing (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of Operation Lone Star, shedding light on its shortcomings, financial implications, and political nature.

# Tags

#Texas #OperationLoneStar #PoliticalStunt #GovernorAbbott #ProPublica