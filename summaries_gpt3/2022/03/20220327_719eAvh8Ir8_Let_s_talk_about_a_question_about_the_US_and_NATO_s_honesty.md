# Bits

Beau says:

- Talks about NATO, the US, and foreign policy, particularly addressing the perception younger people have.
- Shares a message he received from a viewer questioning why the US isn't seen as the bad guy in conflicts.
- Beau challenges the perception that only the US has lied about wars throughout history.
- Explains that all major powers engaging in military interventions lie, deceive, and manufacture consent.
- Points out that the behavior associated with the US and NATO is not unique to them; all major powers behave similarly.
- Emphasizes that interventions by major powers are inherently problematic, regardless of the country or alliance involved.
- Notes that the viewer's perception is shaped by the dominance of the US and NATO on the international stage in recent history.
- Suggests that as other countries become more active internationally, similar behavior will become more visible.
- Draws parallels between the actions of different countries in international conflicts.
- Concludes by stating that the behavior of large powers in international affairs has been consistent throughout history.

# Quotes

- "Our entire lives, it's been the US lying about wars."
- "It isn't that a particular state using violence on the international scene is bad. It's that all states using violence on the international scene are bad."
- "It's just that this is what large powers do."
- "Every country does it."
- "Now that other countries are starting to make plays on the international scene, you're going to see it more and more."

# Oneliner

Beau challenges the perception that only the US lies about wars, stating that all major powers engaging in military interventions behave similarly on the international stage.

# Audience

Younger viewers under 30

# On-the-ground actions from transcript

- Challenge perceptions about specific countries' actions (suggested)
- Advocate for transparency in international affairs (implied)

# Whats missing in summary

The full transcript provides a nuanced perspective on international conflicts and challenges viewers to rethink their perceptions based on historical behaviors of major powers.

# Tags

#NATO #US #foreignpolicy #perception #internationalconflicts