# Bits

Beau says:

- Update on union news in Bessemer, Alabama regarding Amazon's second vote, results possibly coming in today.
- National Labor Relations Board found Amazon unfairly influenced the last vote, prompting a new one.
- Union expected to do better this time since they were able to talk to people without pandemic restrictions.
- Amazon faces more union votes in New York City, likely to succeed due to local support.
- Grievances at Amazon not primarily about pay (employees make more than $15/hour), but about working conditions and standard of living.
- Similar to Warrior Met coal mine workers still on strike, Amazon workers seek better conditions and the promise of a decent living.
- Union activity organizing in the U.S. gaining momentum beyond expected places like Amazon and Starbucks.
- People are looking beyond just dollar amounts, seeking a better standard of living and ownership opportunities.
- Hopeful for a union win at Amazon in Bessemer, with potential for success in New York as well.
- Momentum is building for unionization at Amazon regardless of the outcome in Bessemer.

# Quotes

- "It's fight for 15 in a union."
- "They want the dream, the promise that they were handed."
- "I'm hopeful for the vote in Amazon."
- "I think that the momentum has started and we're going to see a unionized Amazon in the near future."
- "Y'all have a good day."

# Oneliner

Update on Bessemer, Alabama union vote on Amazon, reflecting growing momentum for union activity in the U.S., beyond wage fights towards better living standards.

# Audience

Workers, activists, supporters

# On-the-ground actions from transcript

- Support union organizing efforts in your area (implied)
- Stay informed about labor rights and support workers' rights movements (implied)

# Whats missing in summary

Details on the potential impacts of Amazon unionization on workers' rights and company practices.

# Tags

#UnionNews #Amazon #LaborRights #WorkerEmpowerment #LivingWage