# Bits

Beau says:

- Explains the importance of starting with a basic understanding of social constructs to explain concepts like transgenderism.
- Emphasizes that once people understand social constructs, they tend to become more accepting.
- Suggests finding an example from an older person's past where a social construct shifted to help them understand the concept.
- Uses the example of public pools and segregation to illustrate the shift in social constructs.
- Mentions the significance of understanding that many beliefs are just peer pressure from past bigots.
- Recommends focusing on the fluidity and arbitrary nature of social constructs like race to broaden understanding.
- Points out that many accepted beliefs are not grounded in anything concrete but are societal perceptions.
- Encourages starting with the core concept of social constructs before delving into more specific issues.
- Urges individuals to recognize their place in history and how their views may be judged in the future.
- Stresses the need to confront uncomfortable truths about the past and present to move towards a more accepting society.

# Quotes

- "A lot of these things that you believe are just something that can't be changed, it's really just peer pressure from dead bigots."
- "This is why gender roles and gender norms and all of this stuff, it's kind of up in the air because we get to determine what it is."
- "People have to understand that a whole lot of the things we accept as something that is just set in stone and cannot be changed is made up."
- "You want that basic understanding and the understanding that it's OK."
- "They may not be a great ally, they may not get out there and join the fight or anything like that, but they're going to be accepting."

# Oneliner

Starting with a basic understanding of social constructs can pave the way for acceptance and broader societal change, as Beau explains.

# Audience

All individuals seeking to foster understanding and acceptance in their communities.

# On-the-ground actions from transcript

- Share stories of social construct shifts with older individuals to help them understand and accept new concepts (exemplified).
- Encourage open dialogues about the fluidity of social constructs, like race, to broaden perspectives and challenge ingrained beliefs (exemplified).
- Initiate educational sessions or workshops on social constructs to deepen community understanding and acceptance (suggested).

# Whats missing in summary

The full transcript provides detailed insights on using social constructs to foster acceptance and understanding, especially among older generations, with practical examples and strategies.

# Tags

#SocialConstructs #Acceptance #Understanding #Community #Change