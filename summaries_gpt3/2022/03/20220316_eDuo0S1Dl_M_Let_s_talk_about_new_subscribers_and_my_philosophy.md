# Bits

Beau says:

- Welcoming new subscribers interested in foreign policy coverage, history, philosophy, politics, and social issues.
- Shares a disturbing message received regarding their stance on BLM with racial slurs.
- Clarifies his social and political stance, challenging stereotypes associated with his Southern background.
- Expresses disgust towards memes mocking progressive ideas, wishing Biden was as cool as portrayed.
- Advocates for social progressiveness, stating the importance of using means to help and not turning against those with less institutional power.
- Emphasizes the changing world and the need to adapt, offering advice to viewers to challenge themselves in understanding social issues.
- Encourages viewers to watch videos on social issues, suggesting it may shift perspectives and prevent bigotry.

# Quotes

- "Do not let my accent fool you. We are not on the same team."
- "If you have the means at hand to help, you have the responsibility to do so."
- "Maybe it stops you from being a bigot and that would be a good start."

# Oneliner

Beau challenges stereotypes, advocates for social progressiveness, and encourages viewers to broaden perspectives on social issues to combat bigotry.

# Audience

Viewers

# On-the-ground actions from transcript

- Watch one video a week on social issues to challenge yourself (suggested)
- Research and look into topics discussed to broaden understanding (suggested)

# Whats missing in summary

The emotional impact of challenging stereotypes and advocating for social progressiveness.

# Tags

#SocialIssues #Progressiveness #ChallengeStereotypes #Education #Community