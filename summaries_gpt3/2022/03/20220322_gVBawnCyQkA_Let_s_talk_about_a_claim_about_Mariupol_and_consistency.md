# Bits

Beau says:

- Addressing a claim about Ukraine using people as shields in Mariupol.
- Uncertain about the claim's validity but stresses the need to not justify Russia's actions.
- Exploring the idea of civilians being held against their will in Mariupol and its implications.
- Condemning the use of civilians as shields and holding those accountable.
- Challenging individuals who support Russian targeting of civilians based on this claim.
- Emphasizing the importance of consistency in condemning all violations of laws of armed conflict.
- Urging against justifying atrocities in war under any circumstances.
- Stating that the presence of unwilling civilians in Mariupol worsens Russian actions but does not justify them.
- Questioning the moral stance of individuals who fail to condemn deliberate targeting of civilians.
- Reminding that justifying such actions leads to a slippery slope of supporting atrocities.

# Quotes

- "It's war, and horrible, horrible stuff happens in war."
- "If something is true, it's true across all systems."
- "The idea that there are civilians being held in Mariupol that don't want to be there makes the Russian attack, the bombardment, worse."
- "If you can't condemn Russia deliberately targeting civilians, what does that say?"
- "We don't know that this is happening. We don't know that it's happening on any widespread level."

# Oneliner

Beau addresses the claim of Ukraine using people as shields in Mariupol, stressing the importance of not justifying Russia's actions and condemning violations of laws of armed conflict.

# Audience

Advocates for peace

# On-the-ground actions from transcript

- Hold those who hold civilians against their will accountable (implied).
- Condemn deliberate targeting of civilians (implied).

# Whats missing in summary

Deeper insights on the nuances of justifying atrocities in war.

# Tags

#Mariupol #War #Accountability #CivilianProtection #HumanRights #PeaceAdvocacy