# Bits

Beau says:

- Tennessee's HB 800 is a diversion tactic by politicians to scapegoat and blame the LGBTQ+ community to deflect from their poor performance.
- The bill aims to avoid offending Christian constituents by prohibiting the promotion or normalization of LGBTQ+ issues in public schools.
- Tennessee's legislature is failing in education and healthcare, but they want people to focus on culture wars instead.
- The bill mirrors Russia's 2013 gay propaganda law, aiming to control and manipulate public opinion.
- Politicians in Tennessee are using authoritarian tactics to keep constituents in line and secure re-election.
- The bill violates the spirit of the Constitution by targeting a specific demographic and undermining the separation of church and state.
- It's a manipulative tactic to distract from the state's failures and manipulate easily influenced individuals for political gains.
- The bill is a means for failed politicians to maintain power rather than serve the best interests of the people.
- Tennessee ranks poorly in education and healthcare, yet the focus is shifted to divisive issues like LGBTQ+ representation in schools.
- The bill is an example of how politicians prioritize their own agendas over the well-being and rights of the people.

# Quotes

- "Tennessee's HB 800 is a diversion tactic by politicians to scapegoat and blame the LGBTQ+ community to deflect from their poor performance."
- "The bill aims to avoid offending Christian constituents by prohibiting the promotion or normalization of LGBTQ+ issues in public schools."
- "It's a manipulative tactic to distract from the state's failures and manipulate easily influenced individuals for political gains."

# Oneliner

Tennessee's HB 800 scapegoats the LGBTQ+ community to divert attention from political failures, manipulating constituents for re-election.

# Audience

Tennessee residents, LGBTQ+ supporters

# On-the-ground actions from transcript

- Contact local representatives to voice opposition to HB 800 (suggested)
- Support LGBTQ+ organizations and initiatives in Tennessee (exemplified)
- Educate others about the harmful impact of discriminatory bills like HB 800 (implied)

# Whats missing in summary

The full transcript provides a detailed breakdown of how politicians use divisive tactics to manipulate public opinion and distract from real issues.

# Tags

#Tennessee #HB800 #LGBTQ+ #Politics #Manipulation