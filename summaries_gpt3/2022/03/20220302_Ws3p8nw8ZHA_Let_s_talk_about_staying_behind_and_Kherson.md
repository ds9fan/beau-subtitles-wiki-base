# Bits

Beau says:

- Talks about the concept of staying behind when others leave, a topic previously discussed historically.
- Mentions the city of Kherson in Ukraine coming under Russian control and the potential development of stay-behind tactics.
- Describes the goal of stay-behind campaigns as being a thorn and draining the resolve of the opposition.
- Explains that stay-behind organizations wait, watch, and learn before becoming active.
- Points out that the success of such campaigns depends on making the occupying forces relax and divert resources.
- Differentiates between organized and less organized campaigns in terms of how they start.
- Explains that success for resistance forces involves demoralizing the opposition and forcing them to divert resources back.
- Notes the irregular phase where conventional forces may fall, leaving irregular forces to continue resistance.
- Mentions the comparison to tactics used by American forces in the Mideast.
- Speculates on the success of the campaign based on the training and caliber of troops involved.
- Acknowledges uncertainty about whether the campaign will start and its coverage due to being behind Russian lines.
- Suggests getting ready to see potential developments in Ukraine.

# Quotes

- "It's likely something we're going to see start to develop in the coming weeks."
- "This is the beginning of the truly irregular phase."
- "It's incredibly likely that in the beginning it is going to be very, very successful."
- "We probably won't know right away because unlike other events, this isn't going to get a lot of coverage."
- "I don't think that Ukraine's ready to give up yet."

# Oneliner

Beau talks about the potential development of stay-behind tactics in Ukraine, draining the resolve of the opposition in a campaign that may go unnoticed.

# Audience

Ukrainian Resistance Supporters

# On-the-ground actions from transcript

- Prepare for potential developments in Ukraine (implied).

# Whats missing in summary

Details on the historical context of stay-behind tactics and their potential impact on the conflict.

# Tags

#Ukraine #Conflict #Resistance #StayBehindTactics #Russia