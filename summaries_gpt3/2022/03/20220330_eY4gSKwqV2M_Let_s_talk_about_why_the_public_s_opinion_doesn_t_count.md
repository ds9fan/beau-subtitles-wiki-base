# Bits

Beau says:

- Explains the irrelevance of polling American citizens on foreign policy issues and how leaders don't take it into account.
- Breaks down three questions from a poll about Ukraine, showing how differently they are answered based on phrasing.
- Points out the inconsistency between public opinion and headlines about Biden's handling of the Ukraine situation.
- Stresses the importance of informed opinions in decision-making and how less informed opinions are discounted.
- Compares discounting less informed opinions to medical experts not listening to self-appointed medical experts on Facebook.
- Talks about Biden's intentional ambiguity in his statements, aimed at keeping Moscow off balance in understanding US intentions.
- Mentions past criticisms of Trump's approach in Afghanistan and how giving away intent led to predictable outcomes during the withdrawal.
- Concludes by explaining why polling is often disregarded in decision-making.

# Quotes

- "It's good to understand where the American people are sitting, but those who are making these decisions, they do not care what these polls say."
- "This is why education in the US matters. This is why people really should understand this stuff."
- "When he is sitting there and he's talking, most of that information is for Moscow. It's not for us."
- "For those who think this is just me trying to defend Biden, which isn't really something I do, but go back and look at my criticisms when it comes to Trump."
- "So while people may be criticizing it, doesn't mean it's not the right move."

# Oneliner

Polling American citizens on foreign policy issues is irrelevant to decision-makers, as demonstrated by the varied responses to differently phrased questions on Ukraine and the discrepancy between public opinion and headlines about Biden's actions.

# Audience

Citizens, policymakers, voters

# On-the-ground actions from transcript

- Understand foreign policy issues more deeply and form informed opinions (implied)
- Be critical of headlines and seek to understand the full context behind political decisions (implied)

# Whats missing in summary

The full transcript provides detailed insights into the discrepancies between public opinion and foreign policy decisions, as well as the intentional ambiguity in political statements for strategic purposes. Viewing the full text offers a comprehensive understanding of the nuances in polling and decision-making processes.

# Tags

#Polling #ForeignPolicy #PublicOpinion #DecisionMaking #Biden #Trump #Education