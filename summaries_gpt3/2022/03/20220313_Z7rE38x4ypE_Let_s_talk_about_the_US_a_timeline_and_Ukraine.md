# Bits

Beau says:

- Recalls memories of disinfecting in the kitchen triggering thoughts about the early days of the COVID-19 pandemic when his wife worked in a COVID wing.
- Mentions the decontamination ritual his wife had to undergo every day after work.
- Talks about the initial advice to disinfect packages and leave them outside in the sun due to lack of information about the virus.
- Shares an encounter at a gas station where people express disbelief that a crisis like Ukraine could happen in the US due to its wealth and power.
- Describes American exceptionalism as the belief that the US could handle any crisis because of its financial and political standing.
- Provides a timeline of major events during the COVID-19 pandemic, including milestones like the declaration of a global pandemic by the World Health Organization and vaccine developments.
- Notes the significant number of COVID-19 cases and deaths in the US compared to other countries despite its wealth and power.
- Criticizes American exceptionalism for leading to complacency and inefficient handling of crises.
- Urges Americans to re-evaluate their priorities, focusing on community, policy, and leadership rather than empty patriotism.
- Encourages reflection on the need for genuine leadership to address national and global challenges.

# Quotes

- "Money and power couldn't stop a virus."
- "The United States has succumbed to bumper sticker patriotism."
- "We're still looking around as countries all over the world pass us."
- "We've become apathetic to everything around us."
- "It's probably something that is way past time to address."

# Oneliner

Recalling the early days of COVID-19, Beau criticizes American exceptionalism and calls for reevaluation of priorities and genuine leadership in facing global challenges.

# Audience

Americans

# On-the-ground actions from transcript

- Invest more in policy than in party and personality (implied)
- Care about local communities by actively engaging and supporting them (implied)
- Prioritize genuine leadership over empty patriotism (implied)

# Whats missing in summary

Importance of reflecting on past failures and redefining American priorities for effective crisis management.

# Tags

#COVID-19 #AmericanExceptionalism #Leadership #Policy #Community