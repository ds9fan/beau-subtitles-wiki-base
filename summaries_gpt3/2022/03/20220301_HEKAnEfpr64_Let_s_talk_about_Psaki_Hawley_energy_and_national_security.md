# Bits

Beau says:

- Analyzing statements made by Senator Hawley and Press Secretary Psaki regarding national security and energy.
- Senator Hawley criticizes President Biden for shutting down American energy production and greenlighting Russian energy production.
- Beau fact-checks Senator Hawley's claim by pointing out that the US has increased natural gas and oil production under Biden.
- Beau explains that Biden's focus on renewables actually leads to more energy production, not less.
- Beau suggests that Senator Hawley may be referring to Biden's desire to stop federal leases, which was halted by a court order.
- Press Secretary Psaki argues that national security is tied to transitioning to green, independent energy sources produced within the country.
- Beau supports Psaki's statement by citing the Department of Defense's emphasis on the importance of green energy for national security.
- Beau warns that failure to switch to cleaner energy sources could lead to conflicts over energy resources in the future.

# Quotes

- "If you are against protecting the environment, if you are against green energy, you are also against American national security, period, full stop."
- "The Department of Defense tends to know a little bit about national security."
- "Wars will be fought over this."

# Oneliner

Beau fact-checks Senator Hawley's claims on energy production, while supporting Psaki's argument that green energy is vital for national security.

# Audience

Internet viewers

# On-the-ground actions from transcript

- Contact your representatives to support policies that prioritize green energy and national security (implied)

# Whats missing in summary

The detailed breakdown and analysis provided by Beau in the full transcript.