# Bits

Beau says:

- Addresses a specific group in Ukraine, the Ukrainian resistance, labeled as fascists due to their ideology.
- Makes a comparison to the United States, hypothetical scenario of civilians organizing due to the military being incapacitated.
- Points out that the groups leaning into organizing in the scenario are mostly far-right-wing with aesthetics similar to those at the US Capitol on January 6th.
- Raises the possibility of a similar scenario occurring in the US if faced with a conflict like Ukraine's.
- Explores the challenges of cleaning up such groups historically and proposes potential solutions for Ukraine.
- Expresses concern over the attention and coverage given to the group, potentially turning them into heroes through media portrayal.
- Provides statistics on the group's size, political presence, and electoral success to contextualize their impact in Ukraine.
- Identifies two main reasons for the heightened attention on this group: Russian propaganda and the inherent danger of fascism.
- Advises focusing on uplifting left-wing, progressive groups to counterbalance the narrative surrounding the fascist group.
- Emphasizes the importance of not ignoring the fascist group despite their relatively small numbers due to the inherent risks associated with fascism.

# Quotes

- "Fascists are a lot like lead in your drinking water. There is no acceptable safe level."
- "There's no acceptable safe level."
- "This is something that actually occurs way more than, I guess, people realize."
- "No acceptable safe level."
- "Rather than looking at them and sharing articles about them, there are left-wing, progressive groups there as well. Get the press talking about those."

# Oneliner

Beau sheds light on the dangers of fascist groups, the intersection of ideology and reality, and the importance of countering their narrative with progressive voices in Ukraine.

# Audience

Activists, Progressives, Community Leaders

# On-the-ground actions from transcript

- Elevate left-wing, progressive groups in Ukraine through sharing and discussing their work (suggested)
- Counterbalance media coverage by promoting progressive voices and initiatives in Ukraine (suggested)

# Whats missing in summary

The full transcript provides a comprehensive analysis of the risks associated with fascist groups, the intersection of ideology and reality, and the potential impacts on post-war scenarios. Viewing the complete transcript offers a deeper understanding of the nuances surrounding these issues.

# Tags

#Fascism #Ukraine #ProgressiveGroups #RussianPropaganda #MediaCoverage