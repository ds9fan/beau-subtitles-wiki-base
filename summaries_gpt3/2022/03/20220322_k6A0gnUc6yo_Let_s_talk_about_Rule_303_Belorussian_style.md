# Bits

Beau says:

- Belarusian railway workers disrupt rail traffic between Belarus and Ukraine to impede Putin's invasion.
- Chief of Ukraine's railway service publicly thanks Belarusian railway workers for their swift action.
- Some tracks in Belarus are in poor condition, contributing to the disruption of rail traffic.
- Signaling equipment has been attacked, central switching stations disrupted, and tracks fallen into disrepair quickly.
- The disruption has completely severed all rail traffic between Belarus and Ukraine, giving Ukrainians breathing room.
- Partisan groups in Belarus have also contributed to disrupting rail traffic.
- The people of Belarus are actively resisting being a pawn for Putin, showing resistance beyond the government's control.
- Continued disruption of rail traffic could have a significant impact on impeding Russian supplies to Ukraine.
- Every day without Russian resupply is a win for Ukraine during this critical phase.
- The collective efforts of Belarusian railway workers and partisan groups are making a difference in the conflict.

# Quotes

- "Belarusian railway workers disrupt rail traffic between Belarus and Ukraine to impede Putin's invasion."
- "The people of Belarus are actively resisting being a pawn for Putin."
- "Every day without Russian resupply is a win for Ukraine."

# Oneliner

Belarusian railway workers and partisan groups disrupt rail traffic to impede Putin's invasion, showing active resistance beyond the government's control.

# Audience

Activists, Allies

# On-the-ground actions from transcript

- Support Belarusian railway workers in their efforts to disrupt rail traffic (exemplified)
- Stand in solidarity with the people of Belarus resisting external control (exemplified)

# Whats missing in summary

The full transcript provides more context on the role of partisan groups and the potential impact of continued disruption on impeding Russian supplies.

# Tags

#Belarus #RailwayWorkers #Ukraine #Resistance #Putin #PartisanGroups