# Bits

Beau says:

- Describes the new normal in American foreign policy involving a three-way power struggle between China, the United States, and Russia.
- Mentions how the presence of a near-peer like Russia influences American foreign policy decisions, shifting focus away from other opposition nations.
- Talks about the historical trend of American politicians using external threats to rally votes and consolidate voter support by othering certain groups.
- Points out the negative impact of anti-Russian sentiment on American society and foreign policy, urging against playing into it.
- Suggests reinstating programs like the Sister City program to foster solidarity between people caught in the crossfire of international tensions.
- Warns against politicians exploiting anti-Russian sentiment to prolong conflicts and stay in power, potentially leading to a prolonged Cold War scenario.

# Quotes

- "Just once, it would be great to get through a major foreign policy shift without having to apologize to a demographic of Americans."
- "Don't play into this. It's not the Russian people, it's certainly not Russian Americans, it's the Russian leadership."

# Oneliner

Be cautious of playing into anti-Russian sentiment in American foreign policy to avoid repeating historical mistakes and perpetuating harmful othering dynamics.

# Audience

Foreign policy observers

# On-the-ground actions from transcript

- Reinstate programs like the Sister City program to show solidarity with people caught in the crossfire (suggested).
- Avoid perpetuating anti-Russian sentiment in American society and politics to prevent further harm (implied).

# Whats missing in summary

The full transcript provides a detailed analysis of how anti-Russian sentiment can impact American foreign policy decisions and societal dynamics, urging caution and advocating for a more nuanced approach.

# Tags

#AmericanForeignPolicy #AntiRussianSentiment #SisterCityProgram #Solidarity #PoliticalCaution