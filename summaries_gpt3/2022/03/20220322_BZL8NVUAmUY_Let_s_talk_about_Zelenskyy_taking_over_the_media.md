# Bits

Beau says:
- President Zelensky taking control of TV stations in Ukraine sparked concern among Americans.
- Americans were alarmed by the idea of Zelensky becoming tyrannical for controlling media.
- Beau draws a parallel to the long-standing emergency broadcast system in the United States.
- The emergency alert system has been in place for over 70 years to counter propaganda and communicate during wartime.
- Beau points out that the system can take over radio and TV stations, as well as send messages to phones.
- He mentions that during wartime, such actions are common to manage communication and propaganda.
- Beau criticizes American media for sensationalizing a non-issue about Zelensky's actions.
- He notes that most countries have similar systems in place for emergencies and wartime.
- Beau expresses surprise that the US didn't implement such control on day one of a conflict.
- He concludes by underscoring that the outrage over Zelensky's actions is misplaced, as it mirrors what the US system does.

# Quotes

- "This is an example of the American media taking a story that isn't a story and trying to reframe it so Americans get outraged by it."
- "If they're on network TV, they have to know that this system exists."

# Oneliner

President Zelensky's control of TV stations in Ukraine sparks concern among Americans, but Beau points out the longstanding emergency broadcast system in the US mirrors this action, criticizing the sensationalism by American media.

# Audience

Media Consumers

# On-the-ground actions from transcript

- Educate others about the purpose and history of emergency broadcast systems (implied).
- Share information on social media to debunk sensationalized stories (implied).

# Whats missing in summary

Beau's detailed explanation and historical context on emergency broadcast systems and media sensationalism.

# Tags

#Media #EmergencyBroadcastSystem #Propaganda #Sensationalism #Communication