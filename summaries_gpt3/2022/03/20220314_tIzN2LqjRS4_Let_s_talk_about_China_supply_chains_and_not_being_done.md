# Bits

Beau says:

- China's megacity Shenzhen, with a population of 17-18 million, is going back on lockdown due to community spread of a "stealth variant."
- Despite reporting 66 new cases on Sunday, totaling over 400 cases in the last couple of weeks, they believe they caught it relatively early.
- Shenzhen is a vital tech manufacturing hub, and its lockdown could lead to disruptions in the global supply chain.
- The impact could affect tech companies like Apple initially but may escalate to other sectors as well.
- This disruption adds to the existing inflation concerns globally.
- The geopolitical situation with Russia and Ukraine reaching out to China for help adds another layer of uncertainty to China's decisions.
- The revenue loss from the lockdown may influence China's future choices.
- The supply chain interruption could lead to higher prices as supply diminishes.
- Beau suggests everyone should prepare for interruptions in products that rely on electricity.
- He plans to monitor the situation and provide updates as more information becomes available.

# Quotes

- "Shenzhen is pretty much the factory for every tech company."
- "Everybody around the world should just go ahead and get prepared for another interruption."
- "The revenue loss from the lockdown may influence China's decisions."
- "Prices will go up, exacerbating the inflation issue."
- "We will keep an eye on this, I'll keep an eye on this."

# Oneliner

China's megacity Shenzhen goes back on lockdown due to a "stealth variant," impacting global tech supply chains and potentially worsening inflation, with broader implications on revenue and geopolitics.

# Audience

Global citizens, tech industry players

# On-the-ground actions from transcript

- Monitor news updates on the situation in Shenzhen and its impact globally (suggested)
- Prepare for potential disruptions in tech products and anticipate price increases (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of how Shenzhen's lockdown can affect global tech supply chains, inflation, revenue, and geopolitical decisions. Watching the full video can offer a deeper understanding of these interconnected issues.

# Tags

#China #Shenzhen #Lockdown #TechIndustry #SupplyChain #Inflation #GlobalImpact