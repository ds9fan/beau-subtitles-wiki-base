# Bits

Beau says:

- Putin recently used the term "fifth column," sparking curiosity about its origin.
- "Fifth column" refers to a small group within a larger one working to undermine it.
- The term's origin story involves a general claiming to have four approaching columns and a fifth inside the city ready to open the gates.
- Putin, however, was referring to the protesters in Russia criticizing his actions in Ukraine as the fifth column.
- The invasion into Ukraine has diminished Russia's global reputation, reducing it from a potential superpower to a military joke.
- Despite disagreement about the protesters being labeled the fifth column, Beau points out that Putin and his oligarchs fit this description perfectly.
- Putin and his circle are the ones undermining Russia from within due to corruption and ineffective leadership.
- Beau concludes by suggesting that Putin himself is the real fifth column, causing damage for his own ego and refusing to acknowledge his failures.

# Quotes

- "Putin is the fifth column."
- "It's those who refuse to face reality."
- "Putin and his oligarchs are undermining a large group from within."
- "Russia brought to its knees by corruption and ineffective leadership."
- "A major power, not a superpower."

# Oneliner

Putin's misuse of "fifth column" reveals the true underminers of Russia: Putin and his oligarchs, not the protesters. 

# Audience

Activists, Russia watchers

# On-the-ground actions from transcript

- Organize protests against corruption and ineffective leadership within Russia (exemplified)
- Support independent journalism exposing the truth about Putin and his oligarchs (exemplified)

# Whats missing in summary

Beau's emotional tone and emphasis on the damaging impact of Putin's leadership can be best understood by watching the full video.

# Tags

#Russia #Putin #FifthColumn #Oligarchs #Corruption