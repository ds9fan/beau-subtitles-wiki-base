# Bits

Beau says:

- Zelensky and the National Security Council in Ukraine suspended 11 political parties in Ukraine due to their pro-Russian sympathies.
- The opposition party for life, a significant banned party, has ties to Moscow, including a main figure with connections to Vladimir Putin.
- These banned parties were suspected of collaborating with Russia, possibly sharing information or being potential puppet governments.
- Beau questions the decision to ban the parties, suggesting imprisonment might have been a more suitable action.
- He points out the realities of war in Ukraine, with death and destruction, justifying the need to remove potential threats.
- Beau draws parallels to historical actions in the US during times of threat, challenging notions of freedom of speech versus national security.
- He warns against glorifying violent rhetoric and politicians posing with guns, as seen in Ukraine, and urges self-reflection for Americans.
- Beau stresses the importance of acknowledging the harsh realities of war and avoiding the idealization of violence and conflict.
- He calls for a mirror to be held up to American exceptionalism and a recognition of the potential for similar actions in the US.
- Beau concludes by urging viewers to learn from Ukraine’s situation and recognize the dangers of escalating rhetoric and political tensions.

# Quotes

- "War is bad. It is dirty. It is nasty. It is ugly. It is cruel."
- "If you are calling to restore the Republic for the Glorious Revolution that requires violence, you don't love this country."
- "We acknowledge that we are much closer to looking like that than we may really want to admit."

# Oneliner

Beau examines Ukraine's political crackdown through a lens of war realities, cautioning against violent rhetoric and urging Americans to self-reflect on potential parallels. 

# Audience

Americans

# On-the-ground actions from transcript

- Hold a mirror up to American exceptionalism and violent rhetoric (suggested)
- Refrain from glorifying violence and militaristic politicians (implied)
- Acknowledge the risks of escalating political tensions (implied)

# Whats missing in summary

The detailed nuances and historical comparisons in Beau's analysis can best be grasped by watching the full transcript. 

# Tags

#Ukraine #PoliticalCrackdown #WarRealities #ViolentRhetoric #AmericanExceptionalism