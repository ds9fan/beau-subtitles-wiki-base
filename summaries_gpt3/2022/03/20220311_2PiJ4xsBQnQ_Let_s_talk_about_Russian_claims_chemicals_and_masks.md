# Bits

Beau says:

- Talks about Russian equipment and masks, providing a PSA about surplus Russian masks.
- Mentions looking at photos and footage of Russian equipment, including gas masks from the mid-Soviet era.
- Shares the distribution of obsolete or less than modern equipment by Russians, like Mosins.
- Expresses skepticism towards Russian claims based on past instances of misinformation.
- Addresses whether the presence of masks indicates Russia's intention to use them.
- Warns about the dangers of using Russian surplus filters due to lead and asbestos content.
- Advises against using Russian filters and suggests alternatives for actual protection.
- Emphasizes the importance of not using filters with Cyrillic writing, even for costume purposes.
- Comments on the dated equipment being handed out by Russians, including steel helmets and aged weapons.
- Concludes by speculating on Russia's motives for distributing such equipment and masks.

# Quotes

- "Don't use them. Don't use them."
- "If I had any alternative whatsoever, I would not use one, even a new one."
- "Don't do it."
- "Do not use those filters."
- "Don't use it, even just to play around in a costume."

# Oneliner

Beau explains the dangers of using Russian surplus filters and advises against their use, suggesting alternative filters for protection.

# Audience

Preppers, Surplus Collectors

# On-the-ground actions from transcript

- Replace Russian surplus filters with new ones for CBRN protection (implied).
- Avoid using filters with Cyrillic writing, even for costume purposes (implied).

# Whats missing in summary

Beau's thorough explanation and warnings about the risks associated with using Russian surplus filters can be best understood by watching the full video.

# Tags

#Russian #MilitaryEquipment #GasMasks #PSA #FilterSafety