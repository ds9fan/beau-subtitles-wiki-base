# Bits

Beau says:

- Advocates for planting yard for food rather than lawns, promoting the idea of replacing lawns with food-producing plants like fruit trees and raised beds.
- Shares experiences of planting peach and pear trees, humorously noting the inevitable dirtiness that comes with gardening.
- Emphasizes the long time it takes for fruit trees to bear fruit, suggesting focusing on the time spent together rather than just the end result.
- Points out the benefits of planting fruit trees to increase food security, have control over the food on the table, and build a strong community network.
- Encourages making planting trees a community or family activity to strengthen relationships and create lasting bonds.
- Views planting fruit trees as a starting point for building a strong community network that can provide support in everyday life and natural disasters.
- Mentions the importance of developing a strong community network to alleviate worries and solve problems collectively.
- Suggests approaching planting trees as family time and a screen-free activity that brings everyone together.
- Stresses the numerous benefits of planting fruit trees, mentioning that once people start, they rarely stop due to the positive outcomes it brings.

# Quotes

- "Make it about the time rather than just the fruit."
- "The best time to plant a tree was twenty years ago. The second best time is today."
- "Approach it as family time."
- "There are so many benefits to this that there's got to be something that appeals to them."
- "Once people start, I've never known anybody to start doing this who stopped."

# Oneliner

Beau advocates for planting fruit trees not just for food but to build a strong community network, encouraging focusing on the time spent together rather than just the end result.

# Audience

Gardeners, community builders, families

# On-the-ground actions from transcript

- Organize a community gardening day where everyone plants fruit trees together, creating a strong community bond (implied).

# Whats missing in summary

The importance of building a strong community network through planting fruit trees and the long-lasting benefits it can bring to individuals and communities.

# Tags

#CommunityBuilding #FoodSecurity #Gardening #FamilyTime #CommunityNetworks