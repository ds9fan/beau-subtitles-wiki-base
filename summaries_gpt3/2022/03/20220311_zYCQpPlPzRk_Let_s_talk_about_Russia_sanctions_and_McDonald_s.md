# Bits

Beau says:

- Explains the impact of Western companies leaving Russia due to economic policies.
- Mentions how thousands of employees lost their jobs, affecting the GDP.
- Uses McDonald's as an example to illustrate the ripple effects of a company leaving.
- Describes the interconnected nature of various industries affected by a single company's departure.
- Points out that economic pressure is aimed at leadership, not the average person.
- Emphasizes that reducing economic activity limits tax revenue for Putin's war efforts.
- Suggests that economic pressure sets conditions that make it harder for Putin to maintain support for the war.
- Notes that while economic pressure alone may not shift leadership, it creates conditions for change.
- Speculates on the need for Western banks to assist in rebuilding Russia's economy post-sanctions.

# Quotes

- "Most economies are like a house of cards. Once large segments yank themselves out, everything else falls."
- "Reducing economic activity limits tax revenue which reduces the amount of money Putin can spend on the war."
- "It makes it harder for Putin to sell his war at home when people are getting economically devastated because of it."
- "Russia is now the most sanctioned country on the planet and they're still planning on piling on more sanctions."
- "All the king's horses and all the king's men are going to have to try to put that egg of an economy back together again."

# Oneliner

Western companies leaving Russia creates a domino effect impacting industries, GDP, and Putin's war efforts, setting conditions for economic change and potentially requiring Western banks for recovery.

# Audience

Economic observers

# On-the-ground actions from transcript

- Support local businesses affected by economic policies (implied)
- Advocate for policies that prioritize economic stability for all (implied)

# Whats missing in summary

The detailed examples and nuances of the interconnected economic impacts discussed by Beau.

# Tags

#EconomicImpact #WesternCompanies #Putin #Sanctions #RebuildingEconomy