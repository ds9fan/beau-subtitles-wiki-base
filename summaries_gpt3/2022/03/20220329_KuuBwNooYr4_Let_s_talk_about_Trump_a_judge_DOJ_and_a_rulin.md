# Bits

Beau says:

- Trump World news and a ruling about emails hint at shaping events.
- Committee sought access to emails; lawyer cited attorney-client privilege.
- Judge ordered the emails to be turned over, citing evidence of Trump's corrupt obstruction on January 6, 2021.
- The judge's use of "corruptly" implies a federal crime by the former president.
- DOJ now faces pressure to act with a federal judge pointing to high likelihood of a crime.
- DOJ can act independently of the January 6th committee's referral.
- American people may question DOJ's inaction following the judge's ruling.
- Protecting the presidency institution is a longstanding tradition in the U.S.
- DOJ's handling of this case will determine its commitment to upholding this tradition.
- Failure to properly investigate actions undermining the presidency goes against protecting the institution.
- DOJ must decide its course of action post this ruling.
- The ruling may have significant impacts despite being seemingly minor.
- DOJ's lack of action may lead to public uproar if not transparent in their investigations.
- The importance lies in the implications and potential consequences of the judge's ruling.

# Quotes

- "Based on the evidence, the court finds it more likely than not that President Trump corruptly attempted to obstruct the joint session of Congress on January 6, 2021."
- "Allowing those actions to not be properly investigated or to be swept under the rug is the exact opposite of protecting the institution."
- "This ruling, while minor in a lot of ways, is probably going to have some big impacts."

# Oneliner

Beau breaks down a ruling hinting at Trump's obstructive actions on January 6, urging DOJ to act and uphold the presidency institution.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact your representatives to demand transparency and accountability from the DOJ (suggested).
- Stay informed about developments in this case and share them with your community (implied).

# Whats missing in summary

Detailed analysis and legal implications of the judge's ruling.

# Tags

#DOJ #Trump #Obstruction #PresidencyInstitution #Accountability