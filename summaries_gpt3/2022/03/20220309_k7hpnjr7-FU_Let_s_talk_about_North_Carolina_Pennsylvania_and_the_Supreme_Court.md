# Bits

Beau says:

- Three Supreme Court cases, two decided, one pending, involving North Carolina, Pennsylvania, and Wisconsin, all about redistricting for elections.
- Republican Party pushing for Supreme Court permission to redraw maps as they see fit.
- North Carolina case involved heavily gerrymandered maps rejected by state courts.
- Republicans argued that only state legislature should draw maps, not state courts.
- Acceptance of this doctrine could lead to extreme gerrymandering and less representation for voters.
- Similar situation in Pennsylvania with the governor vetoing maps drawn by state legislature.
- Wisconsin case likely to have a similar outcome as the Supreme Court ruled against Republicans in the other cases.
- Republican Party aiming to control state-level power to influence federal elections.
- Possibility of maps being drawn based on party affiliation rather than voter preference.
- Concerns about less responsive government if gerrymandering continues unchecked.

# Quotes

- "Republican Party pushing for Supreme Court permission to redraw maps as they see fit."
- "Acceptance of this doctrine could lead to extreme gerrymandering and less representation for voters."
- "Republican Party aiming to control state-level power to influence federal elections."

# Oneliner

Three Supreme Court cases on redistricting show Republican Party's push for extreme gerrymandering and control over elections.

# Audience

Voters, election watchdogs

# On-the-ground actions from transcript

- Watchdog groups: Monitor closely for further developments on redistricting cases (implied).

# Whats missing in summary

Detailed analysis on the potential long-term impacts of unchecked gerrymandering and lack of representation.

# Tags

#SupremeCourt #Redistricting #Gerrymandering #RepublicanParty #Elections