# Bits

Beau says:

- The US and its Western allies have historically been the main countries capable of projecting force, but now other countries are gaining that capability.
- The story of Hassan, an 11-year-old boy who fled violence alone, illustrates a stark contrast in how different countries treat migrants and refugees.
- Beau criticizes the US for its treatment of migrants, contrasting it with the help offered to refugees in other countries like Slovakia.
- He points out the hypocrisy in differentiating between refugees and migrants based on race or shade, citing examples from European countries like Hungary.
- Beau draws parallels between American actions in conflicts like Afghanistan and Iraq and criticizes the media's portrayal of similar actions by other countries.
- He stresses the importance of acknowledging past mistakes and changing course to lead the world in a better direction.
- Beau urges Americans and Western societies to embrace discomfort, as it signals an opportunity to correct past wrongs and strive for a more just future.

# Quotes

- "If something is wrong, it is wrong. If something is right and good and true, it is."
- "We haven't been guiding the world to a better place, but we could if we wanted to."
- "It's OK to be uncomfortable."
- "We haven't been doing what we should as a society, but we can fix it."
- "There are going to be topics coming up that are going to make people uncomfortable."

# Oneliner

The US and its Western allies must confront uncomfortable truths about their past actions to lead the world in a better direction.

# Audience

Policy Makers, Activists, Global Citizens

# On-the-ground actions from transcript

- Acknowledge past mistakes and work towards leading the world in a better direction (implied).

# Whats missing in summary

The full transcript provides a deeper exploration of the need for self-reflection and uncomfortable truths to pave the way for positive change on a global scale.

# Tags

#US #WesternAllies #Migrants #Refugees #GlobalLeadership