# Bits

Beau says:

- Discovers legislation allowing President Biden to issue letters of mark and reprisal to go after Russian oligarchs.
- Explains letters of mark as documents authorizing private citizens to act as privateers with legal backing.
- Mentions potential effectiveness of the bill in prompting oligarchs to move their assets out of foreign countries.
- Points out the constitutional basis for issuing letters of mark under Article 1, section 8, clause 11 of the US Constitution.
- Connects the bill's concept to historical practices and the use of private contractors.
- Suggests amending the bill to target financial assets like cryptocurrencies for increased effectiveness.

# Quotes

- "Is this constitutional? Surprisingly, yeah."
- "Those that are sanctioned will remove their assets."
- "It might be a really good idea to amend this to include the ability to go after financial assets in, say, I don't know, cryptocurrencies."

# Oneliner

Beau explains legislation allowing President Biden to issue letters of mark to target Russian oligarchs, proposing effectiveness and a potential amendment to include cryptocurrencies.

# Audience

Legislative enthusiasts

# On-the-ground actions from transcript

- Track the progress of bill 6869 (suggested)
- Stay informed about the implications of the bill on targeting oligarchs (suggested)
- Advocate for potential amendments to include cryptocurrencies in the legislation (suggested)

# Whats missing in summary

The full transcript provides a detailed analysis of a unique legislative proposal and its potential impact on targeting Russian oligarchs, suggesting room for further effectiveness through amendments.

# Tags

#Legislation #LettersOfMark #RussianOligarchs #Constitution #Cryptocurrencies