# Bits

Beau says:

- Explains the impact of the current state of the world on wheat, corn, and soybeans.
- Notes that Russia and Ukraine, significant wheat exporters, are facing disruptions that may affect the global market.
- Mentions factors like disruptions in transportation, lack of fertilizer, and combat affecting crop transportation.
- Shares that wheat prices in Chicago are at an all-time high, with corn and soybeans prices up as well.
- Warns of potential social unrest due to a lack of bread caused by the wheat shortage.
- Anticipates heavy impacts in the Middle East and Africa due to the shortage.
- Predicts ripple effects worldwide as a result of the wheat supply disruptions.
- Foresees an increase in bread prices in the United States.
- Advises people to stock up on ingredients if they took up bread making during the pandemic.
- Stresses the importance of addressing food insecurity globally.

# Quotes

- "A lack of wheat means a lack of bread."
- "The higher the percentage of income that goes to food, the more it's going to hurt because this is a staple and it has been interrupted."

# Oneliner

Beau explains the global impact of wheat shortages, warning of potential social unrest and advising preparation for rising bread prices.

# Audience

Global citizens

# On-the-ground actions from transcript

- Support relief agencies that may need help due to food supply disruptions (implied)
- Prepare by stocking up on ingredients for staple foods (implied)

# Whats missing in summary

Potential strategies for addressing the global wheat supply chain disruptions. 

# Tags

#WheatShortage #GlobalImpact #FoodInsecurity #Prepare #SocialUnrest