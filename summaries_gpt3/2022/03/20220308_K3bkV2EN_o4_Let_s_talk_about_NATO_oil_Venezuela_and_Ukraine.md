# Bits

Beau says:
- Reads a message from a leftist critiquing his views on imperialism and sanctions.
- Responds to the message by discussing NATO expansion, imperialism, and capitalist aggression.
- Explains the importance of understanding military strategy and the capitalist economy.
- Criticizes the idea that Russia is synonymous with leftism, pointing out Russia's capitalist nature.
- Advises engaging in productive discourse without resorting to insults and understanding others' perspectives.

# Quotes
- "If you want to reach somebody, you have to kind of meet them where they're at."
- "Russia is a capitalist oligarchy. It is right-wing."
- "A lot of this is being parroted by people on the right."

# Oneliner
Beau addresses leftist criticisms on imperialism and sanctions, discussing NATO expansion, capitalist aggression, and the misconception of Russia as left-wing.

# Audience
Online activists and political commentators

# On-the-ground actions from transcript
- Engage in productive discourse with those holding different views (exemplified)
- Avoid insulting others in opening paragraphs to facilitate constructive dialogues (implied)

# Whats missing in summary
The full transcript provides detailed insights on addressing leftist criticisms, understanding military strategy, and engaging in respectful discourse.

# Tags
#Imperialism #Leftism #Capitalism #Discourse #Sanctions