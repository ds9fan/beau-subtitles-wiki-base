# Bits

Beau says:

- Defines oligarchs and oligarchies as power structures where a small number of people hold corrupt and selfish power.
- Differentiates financial oligarchs who use money to influence government decisions from other forms of oligarchs like military dictatorships.
- Points out the focus on Russian and Ukrainian oligarchs who gained power through privatization post-Soviet Union.
- Mentions a 2014 study that showed US billionaires influencing policy decisions to favor business interests over public opinion.
- Refers to the US as a civil oligarchy where financial power is used selfishly and corruptly within legal boundaries.
- Notes former President Jimmy Carter referred to the US as an oligarchy in 2015.
- Suggests a significant difference between US billionaires and Eastern European oligarchs is the latter's willingness to use violence outside of state power.
- Emphasizes the concentration of wealth in fewer hands leading to a more apparent oligarchical system.

# Quotes

- "As we look at a world where wealth is being concentrated in fewer and fewer and fewer hands, and those hands end up shaking a lot."
- "I don't know how much weight you want to put on that distinction."
- "So whether you want to adopt that terminology now or wait a little bit, hope that things get better, I guess that's up to you."

# Oneliner

Beau explains oligarchs and oligarchies, focusing on financial influence, corrupt power, and the concentration of wealth leading to a more apparent oligarchical system.

# Audience

Activists, Educators, Advocates

# On-the-ground actions from transcript

- Advocate for anti-corruption initiatives in your community (suggested)
- Support policies that prioritize public interest over business interests (implied)

# Whats missing in summary

The full transcript provides a detailed historical context and comparison between different types of oligarchs and their methods of wielding power.