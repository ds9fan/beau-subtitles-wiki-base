# Bits

Beau says:

- Explains the current process of nominating a new justice to the Supreme Court, the first-ever black woman nominee.
- Notes the lack of diversity on the current Supreme Court and the unique legal experience of the nominee.
- Criticizes attempts to discredit the nominee by focusing on her LSAT scores, despite her extensive legal background.
- Provides insights into what the LSAT test is and its purpose for law school admission.
- Mentions the nominee's acceptance into Harvard Law School and her graduation with honors.
- Points out the manufactured controversy around questioning the nominee's qualifications based on irrelevant factors like LSAT scores.
- Concludes that the opposition is more about her being a black woman rather than her qualifications.

# Quotes

- "There's no reason to be questioning her LSAT score. It was good enough to get her into Harvard."
- "She has more experience, a wider range of experience, than anybody on the court."
- "It's just manufacturing an issue to turn people against someone that they can other."

# Oneliner

Beau explains the unjust focus on LSAT scores to discredit the highly qualified black woman Supreme Court nominee.

# Audience

Legal system observers

# On-the-ground actions from transcript

- Support and advocate for qualified diverse Supreme Court nominees (exemplified)
- Challenge manufactured controversies and biases in judicial nominations (exemplified)

# Whats missing in summary

The full transcript provides a detailed analysis of the unjust scrutiny faced by a highly qualified Supreme Court nominee based on irrelevant factors like LSAT scores.