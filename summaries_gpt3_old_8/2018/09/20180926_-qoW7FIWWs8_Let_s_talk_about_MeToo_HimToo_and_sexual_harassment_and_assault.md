# Oneliner
Beau offers advice on avoiding sexual harassment, challenging victim-blaming narratives and stressing accountability for actions in the #MeToo era.

# Bits
Beau advises on avoiding sexual harassment and assault:
- Beau addresses sexual harassment and assault in light of the Kavanaugh nomination.
- Advises safeguarding reputation to attract the right people.
- Suggests ending dates at the front door for safety.
- Warns against taking anyone home when drinking.
- Stresses watching what you say to avoid unintentional implications.
- Urges being cautious with signals sent and keeping hands to yourself.
- Addresses men's fear of false accusations with the "him-too" hashtag.
- States that false accusations of sexual harassment are rare.
- Calls for men's accountability in actions to avoid situations of false accusations.
- Counters victim-blaming narratives surrounding clothing and rape.
- Concludes by asserting that rapists are the sole cause of rape.

# Audience
Men, women, individuals

# On-the-ground actions from transcript
- Practice safeguarding reputation to attract the right people (exemplified)
- End dates at the front door for safety (exemplified)
- Avoid taking anyone home while drinking (exemplified)
- Watch what you say to avoid unintentional implications (exemplified)
- Be cautious with signals sent and keep hands to yourself (exemplified)
- Hold oneself accountable for actions to avoid false accusations (exemplified)

# Quotes
- "There is one cause of rape, and that's rapists."
- "Be very careful with the signals you send."
- "Don't put yourself in a situation where you can be falsely accused 2% of the time."
- "The miniskirt said, am I really to blame? And the hijab said, no, it happened to me too."

# Whats missing in summary
The full transcript provides a deeper exploration of sexual harassment, addressing victim-blaming and stressing accountability for actions. 

# Tags
#SexualHarassment #MeToo #VictimBlaming #Accountability #RapeAwareness