# Oneliner
Beau breaks down the shooting in Dallas, pointing out inconsistencies and raising questions about justice and police corruption.

# Bits
Beau says:
- Analyzes the shooting in Dallas, questioning the officer's story and potential motives.
- Raises concerns about the officer's actions and the possibility of it being a home invasion.
- Mentions Texas law and the potential consequences the officer might face.
- Recalls a past experience and a poignant question about Black Lives Matter.
- Expresses dismay at the search of the victim's home and police corruption.
- Condemns the lack of accountability within the Dallas Police Department.
- Addresses the erosion of public trust in law enforcement due to unjust actions.

# Audience
Activists, Community Members, Advocates

# On-the-ground actions from transcript
- Advocate for police accountability and transparency by demanding thorough investigations into cases of police misconduct (implied).
- Support organizations working towards police reform and community policing initiatives (exemplified).
- Attend or organize community meetings to address concerns about police practices and advocate for change (suggested).

# Quotes
- "If this is the amount of justice they get, they don't. They don't."
- "It's a few bad apples spoils the bunch and that certainly appears to have happened in Dallas."
- "That is why people don't back the blue anymore."

# What's missing in summary
Insights on the importance of accountability and transparency in law enforcement for building community trust and ensuring justice.

# Tags
#PoliceShooting #Dallas #PoliceReform #Justice #CommunityPolicing