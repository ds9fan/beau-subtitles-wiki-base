# Oneliner

On September 11th, Beau shares his experience and reflections on the aftermath, loss of freedom, and the importance of community action to preserve liberty.

# Bits

Beau says:

- Recalls witnessing the 9/11 attacks and the immediate realization that it was intentional, not accidental.
- Notes the casualty of freedom post-9/11, as laws restricted rights and surveillance increased.
- Describes how terrorism strategically provokes overreactions that limit freedoms.
- Urges against normalizing loss of rights and underscores the need for a community-based approach to reclaiming freedom.
- Suggests teaching children about the importance of freedom and self-reliance during emergencies.
- Advocates for counter-economics to reduce government influence and the importance of surrounding oneself with like-minded individuals.
- Encourages supporting marginalized individuals and becoming a force multiplier in the fight for freedom.

# Audience

Community members, activists, parents

# On-the-ground actions from transcript

- Teach children about the importance of freedom and rights (implied).
- Prepare for emergencies and become self-reliant to reduce dependence on the government (implied).
- Practice counter-economics by supporting local businesses and initiatives to limit government influence (implied).
- Surround yourself with like-minded individuals to build a supportive community (implied).
- Support marginalized individuals and help them get involved in advocating for freedom (implied).

# Quotes

- "We can't let this become normal."
- "Defeat an overreaching government by ignoring it."
- "Y'all been keeping freedom on CPR."
- "The U.S. Army Special Forces, it's the most feared fighting unit in the world, the Green Berets, the reason they're the most feared is because they're teachers."
- "If your community is strong enough, what happens in DC doesn't matter because you can ignore it."

# Whats missing in summary

The full transcript provides detailed examples and insights on the impact of post-9/11 measures on freedom, along with practical strategies for grassroots activism and community empowerment.

# Tags

#September11 #Freedom #CommunityAction #CounterEconomics #TeachChildren