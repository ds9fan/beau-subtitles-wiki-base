# Oneliner
Beau responds to a story attacking a woman for kneeling during the national anthem, challenging the author's understanding of patriotism and calling out his lack of awareness about the purpose of protests.

# Bits
Beau says:
- Responds to an op-ed attacking a woman for kneeling during the national anthem at a county fair.
- Expresses irritation at the author and questions their understanding of patriotism and protests.
- Points out the author's criticism of the woman for seeking attention during the protest, questioning the purpose of protests.
- Challenges the author's viewpoint on patriotism, stating that it involves correcting the government when it is wrong.
- Shares a story about a civilian who believed patriotism is displayed through actions, not symbols.
- Encourages the author to speak to the civilian before giving further lectures on patriotism.
- Concludes by distinguishing between nationalism and patriotism, rejecting the author's views and asserting his own patriotism.

# Audience
Activists, Patriots, Community Members

# On-the-ground actions from transcript
- Challenge government actions (implied)
- Have meaningful dialogues about patriotism with others in your community (implied)
- Support individuals exercising constitutionally protected rights (implied)

# Quotes
- "Patriotism is correcting your government when it is wrong."
- "Patriotism was displayed through actions, not worship of symbols."
- "There is a very marked difference between nationalism and patriotism."

# Whats missing in summary
Deeper insights on the importance of understanding and practicing patriotism through actions rather than mere symbolism.

# Tags
#Patriotism #Protests #Community #NationalAnthem #Government