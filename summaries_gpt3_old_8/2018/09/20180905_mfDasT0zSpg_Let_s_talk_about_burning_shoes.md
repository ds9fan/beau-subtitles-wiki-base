# Oneliner
Burning Nike shoes as a symbol of protest - a thought-provoking take on freedom and activism.

# Bits
Beau talks about burning Nike shoes to make a point about liberty and freedom.
He mentions not forcing his views on his son, who owns Nikes.
Beau criticizes the emotional response of burning shoes as a protest.
He suggests giving the shoes to those in need instead of burning them.
Beau questions if burning the shoes really targets the right people.
He draws a parallel between burning Nike shoes and burning the American flag.
Beau challenges those who prioritize symbols over real issues like sweatshops.
He points out the hypocrisy of disliking a Nike commercial but not the company's use of sweatshops.
Beau questions if those who burn shoes truly understand freedom.
He ends by urging listeners to think critically about their actions.

# Audience
Activists, Consumers, Parents

# On-the-ground actions from transcript
- Donate shoes to shelters or thrift stores near military bases (suggested)
- Support homeless veterans by giving them shoes (suggested)

# Quotes
- "You're loving that symbol of freedom more than you love freedom."
- "It's one of those things. You're loving that symbol of freedom more than you love freedom."

# Whats missing in summary
The full transcript provides more depth on the complexities of using symbols in activism and the importance of understanding the true impact of one's actions.

# Tags
#Freedom #Activism #Nike #Symbolism #Sweatshops #Community #CriticalThinking