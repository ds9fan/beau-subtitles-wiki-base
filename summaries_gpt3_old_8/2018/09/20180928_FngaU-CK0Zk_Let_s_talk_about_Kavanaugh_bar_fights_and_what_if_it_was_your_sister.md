# Oneliner
When faced with a potentially dangerous situation, Beau stepped in to protect a woman at a bar, using the anecdote to shed light on the importance of empathy and understanding in the context of sexual assault and victim blaming.

# Bits
Beau shares a story from his college days at a bar called Eight Seconds where he intervened to protect a girl he vaguely knew from a pushy guy.
He threw a beer bottle in the trash to diffuse the situation without resorting to physical violence.
Drawing parallels to the Kavanaugh hearing, Beau brings attention to how people's comments can discourage sexual assault survivors from coming forward.
He uses a math exercise to illustrate the prevalence of sexual assault among women, urging empathy and understanding.
Beau questions why male survivors of sexual assault are not questioned about their delayed disclosure, unlike female survivors.
He criticizes the emotional behavior displayed by Kavanaugh during the hearing, contrasting it with societal expectations of men.
Beau points out that sexual assault is about power and control, not just sex, making it a compelling reason for survivors to come forward.
He stresses the impact of victim-blaming comments on survivors and the importance of building trust and understanding.
Beau concludes by encouraging listeners to reconsider their attitudes towards sexual assault survivors and the potential consequences of their actions and words.
He warns against perpetuating harmful narratives that could further isolate and harm survivors.

# Audience
Advocates, Allies, Bystanders

# On-the-ground actions from transcript
- Reach out to sexual assault survivors in your community, offer support, and believe them (suggested).
- Educate yourself on the prevalence and impact of sexual assault, especially on women (implied).
- Challenge victim-blaming attitudes and comments in your social circles to create a safer environment for survivors (implied).

# Quotes
- "It's about power and control."
- "Because they're not out there alone, facing your comments."
- "You might not want to do it again because you're altering real-life relationships over bumper sticker politics."
- "How many of them do you know? How many of them talked about it with you?"
- "You already ruined that."

# Whats missing in summary
The emotional weight and authenticity of Beau's storytelling and his call for introspection and empathy are best experienced through watching the full transcript.

# Tags
#Empathy #SexualAssault #VictimBlaming #CommunitySupport #BelieveSurvivors