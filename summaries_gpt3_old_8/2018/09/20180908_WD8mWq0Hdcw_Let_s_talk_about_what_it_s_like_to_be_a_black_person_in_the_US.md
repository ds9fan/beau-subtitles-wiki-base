# Oneliner
Beau delves into the complexities of cultural identity and the lasting impact of slavery, challenging viewers to understand the deep-rooted experiences of Black individuals in the U.S.

# Bits
Beau says:
- Unexpected wide reach of Nike video led to insults and deeper reflection.
- Acknowledges inability to fully grasp the Black experience as a white person.
- Challenges the notion that understanding statistics equates to understanding lived experiences.
- Examines the impact of slavery on cultural identity and pride.
- Compares cultural identities of different ethnicities and their influence.
- Questions lack of pride in African roots and the enduring effects of slavery.
- Describes the lasting cultural impact of slavery on Black cuisine and identity.
- Expresses the ongoing struggle and cultural scars resulting from slavery.
- Calls for acknowledging and addressing the uncomfortable history of slavery.
- Encourages reflection on personal heritage and the absence of pride for some.
- Praises "Black Panther" movie for providing a source of pride and representation.
- Reminds viewers to recognize the deep-seated effects of historical injustices on cultural identity.

# Audience
White allies

# On-the-ground actions from transcript
- Acknowledge the deep-rooted impact of historical injustices on cultural identity (implied).
- Learn about and respect diverse cultural heritages within your community (implied).

# Quotes
- "You may get the statistics, you may understand the numbers. Doesn't mean you understand what it's like to be black in the U.S."
- "Your entire cultural identity was ripped away."
- "It's gone, it was stripped away, just vanished in the air."

# Whats missing in summary
The full transcript provides a deeper understanding of the impact of slavery on cultural identity and challenges viewers to empathize with the experiences of others.

# Tags
#CulturalIdentity #Slavery #BlackExperience #Understanding #Heritage