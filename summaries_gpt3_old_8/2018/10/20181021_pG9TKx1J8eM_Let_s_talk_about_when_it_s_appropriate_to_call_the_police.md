# Oneliner
Calling the police can escalate situations with deadly consequences; only do so when death is an appropriate response.

# Bits
Beau says:
- Police may resort to violence over even the most trivial laws, like jaywalking.
- Nonviolent actions can lead to being put in a cage or killed if resistance occurs.
- Barbecue Becky, Permit Patty, and similar incidents put lives at risk by involving the police.
- Only involve law enforcement when death is a fitting consequence.
- If nobody is harmed, it's not truly a crime, regardless of legality.
- Patience and calmness can differ based on race, impacting interactions with the police.
- Escalating encounters with the police can lead to violence and death.
- Calling the police should only be considered if death is the appropriate outcome of the situation.

# Audience
Community members

# On-the-ground actions from transcript
- Be cautious when considering involving law enforcement (implied)
- Advocate for community-based solutions (implied)

# Quotes
- "Every law is backed up by penalty of death."
- "Nobody's being harmed by these actions. Nobody's being hurt."
- "If nobody's being harmed, it's not really a crime."
- "Don't call the law unless death is an appropriate response because it's a very real possibility every time you dial 9-11."

# Whats missing in summary
Additional context on racial disparities in interactions with law enforcement

# Tags
#CommunityPolicing #PoliceAccountability #CallingThePolice #RacialJustice #Beau