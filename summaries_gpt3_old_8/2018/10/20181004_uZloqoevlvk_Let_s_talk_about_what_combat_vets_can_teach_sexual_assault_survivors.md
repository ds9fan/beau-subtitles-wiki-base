# Oneliner
Combat vet experiences are compared to those of sexual assault survivors, challenging stigmas and reframing trauma with empathy and understanding.

# Bits
Beau acknowledges the challenging messages in his inbox and expresses gratitude for the trust.
He draws parallels between combat vets and sexual assault survivors, addressing misconceptions about both groups.
Beau dismantles the stigma around dating sexual assault survivors by likening their experiences to combat vets.
He calls out the double standard in how these two groups are viewed and urges empathy and understanding.
Beau shares a personal story about a combat experience to illustrate the impact of trauma on memory.
He concludes by encouraging reflection on trauma and its effects on individuals.

# Audience
Supporters of trauma survivors.

# On-the-ground actions from transcript
- Support survivors with empathy and understanding (exemplified).
- Challenge stigmas and misconceptions around trauma survivors (exemplified).
- Advocate for mental health resources for survivors (suggested).

# Quotes
- "You are not damaged goods."
- "Everybody's broke in some way."
- "There's nothing wrong with you."

# What's missing in summary
Exploration of trauma's lasting effects on memory and the importance of empathy in understanding survivors' experiences.

# Tags
#CombatVets #SexualAssaultSurvivors #Trauma #Empathy #Stigma