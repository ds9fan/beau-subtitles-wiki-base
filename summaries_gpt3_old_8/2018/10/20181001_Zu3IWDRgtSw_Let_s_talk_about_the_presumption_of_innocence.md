# Oneliner
Kavanaugh's support for violating rights and undermining the Constitution challenges the presumption of innocence, exposing partisan politics' danger to democracy.

# Bits
Beau says:
- Comment section filled with presumption of innocence defenders after discussing Kavanaugh in a video on sexual assault.
- Kavanaugh supports law enforcement stopping, demanding ID, and searching individuals.
- He backs NSA collecting metadata without a warrant.
- Implicated in indefinite detention and misleading statements during confirmation hearings.
- Importance of presumption of innocence emphasized as cornerstone of American justice system.
- Those supporting Kavanaugh's nomination may not be familiar with his rulings and history of undermining rights.
- Party politics sacrificing rights for power illustrated through Kavanaugh's potential impact on future administrations.
- Warning about executive power granted by Kavanaugh to Trump potentially being wielded by a future Democratic president.
- Encourages critical thinking about supporting candidates based on party affiliation rather than principles.
- Criticizes blind support for candidates without understanding their impact on constitutional rights.
- Condemns prioritizing party politics over safeguarding the Constitution and American values.

# Audience
Citizens, Voters, Activists

# On-the-ground actions from transcript
- Question party politics and prioritize principles over party affiliation (implied)
- Stay informed on candidates' stances on constitutional rights and values (implied)

# Quotes
- "You've bought into bumper sticker politics and you're trading away your country for a red hat."
- "Seems odd that all of a sudden he cares."
- "In just a few years, it's not going to be a Republican president, it's going to be a Democrat."
- "You can sit there and you can wave your flag and you can talk about making America great again."
- "It's just a thought, something you need to think about."

# Whats missing in summary
The emotional delivery and tone of Beau's message

# Tags
#PresumptionOfInnocence #Kavanaugh #Constitution #PartyPolitics #RightsProtection #Democracy