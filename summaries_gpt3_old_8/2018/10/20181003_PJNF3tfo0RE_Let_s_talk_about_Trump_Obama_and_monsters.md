# Oneliner
President Trump's low bar for decency on display as Beau criticizes his mocking of sexual assault survivors and lack of accountability.

# Bits
Beau criticizes President Trump for mocking sexual assault survivors on television, setting a low bar for decency. He points out Trump's efforts to silence sexual assault claimants and questions his integrity and accountability. Beau contrasts Trump with Obama, expressing concern over Trump's behavior and attitudes towards women and justice.

- President Trump mocked sexual assault survivors on TV, setting a low decency bar.
- Beau criticizes Trump's silencing of sexual assault claimants and lack of integrity.
- Beau questions if honor and integrity are associated with President Trump.
- He contrasts Trump with Obama, expressing concern over Trump's behavior.
- Beau raises concerns about Trump's attitude towards women and justice.
- Trump's comments on sexual assault survivors draw criticism from Beau.
- Beau questions the lack of accountability and decency in Trump's actions.
- He points out Trump's past behaviors and attitudes towards women.
- Beau criticizes Trump's entitlement and inability to take responsibility.
- He contrasts Trump with Obama, noting differences in behavior and integrity.
- Trump's focus on presumption of innocence is discussed in relation to past actions.
- Beau questions the perception of Kavanaugh and the dangerous implications.
- He warns against dehumanizing individuals accused of sexual assault.
- Beau draws parallels between dehumanization of Nazis and rape predators.
- He stresses the danger of overlooking evil due to party politics and nationalism.

# Audience
Advocates for accountability and decency.

# On-the-ground actions from transcript
- Stand against silencing sexual assault survivors (implied).
- Advocate for accountability in leadership (implied).
- Challenge dehumanization of individuals accused of sexual assault (implied).

# Quotes
- "There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim."
- "After eight years of Obama, you wanted a man's man. It's not really what you got though, is it?"
- "The scariest Nazi wasn't a monster. He was your neighbor."

# What's missing in summary
The emotional depth and nuances of Beau's delivery and sincerity.

# Tags
#Accountability #Decency #SexualAssault #Trump #Obama #Dehumanization