# Bits

- Patty Hearst was kidnapped 45 years ago, and her family gave up $2 million for negotiations with the SLA, which was to be turned into food for the needy (contextual, specified)
- Patty Hearst's grandfather was worth $30 billion when he died (specified)
- Is it moral to be a billionaire in society? (philosophical)
- Jeff Bezos earned $78.5 billion last year, but most of that wealth is not liquid (specified)
- Bezos could end homelessness in the US and give each homeless person $200 a week for a year with around $5.7 billion, leaving him with about $50 billion (specified, calculated)
- Question of taxing ultra-wealthy at higher rates and the moral implications (ethical)
- Income inequality is growing, and when does it become self-defense to act against it? (philosophical, ethical)
- Mention of the 70% tax proposal as a potential solution to income inequality (specified)
- Reference to the entitled nature of some wealthy individuals in the US and their lack of understanding of financial struggles of others (specified)
- Warning that when people get desperate, they act, and the wealthy should be aware that change might be forced if things don't improve (warning)

# On-the-ground actions from transcript

- Contact your local representatives to advocate for policies that address income inequality (implied)
- Join local community efforts to support homeless populations (suggested)
- Give someone in need a helping hand by donating to organizations that assist the homeless (suggested)
- Go to events or rallies advocating for fair taxation of the ultra-wealthy (suggested)

# Oneliner

45 years after Patty Hearst's kidnapping, the conversation on billionaire morality and income inequality continues, prompting reflections on wealth distribution and societal responsibility.

# Quotes

- "Is it moral to be a billionaire in this society?"
- "One guy can end homelessness and give them $200 a week. It's crazy. It's insane."
- "The entitled nature of the wealthy in the United States now."
- "When does it become moral to act?"
- "Those pitchforks are coming."

# What's missing

The full conversation provides a detailed analysis of income inequality, the moral implications of extreme wealth, and potential actions for addressing societal disparities. The nuances of these arguments are best understood through the entire discussion.

# Tags

#IncomeInequality #Billionaires #WealthDistribution #Taxation #SocialResponsibility #Homelessness