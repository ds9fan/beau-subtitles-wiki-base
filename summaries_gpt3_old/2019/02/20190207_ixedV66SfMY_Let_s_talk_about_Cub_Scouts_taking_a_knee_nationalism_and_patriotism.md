# Bits

- A young Cub Scout took a knee during the Pledge of Allegiance at a city council meeting, challenging traditional responses to such actions.
- The boy's action showed independence, self-reliance, and free thought, characteristics that scouting is meant to instill.
- The intersection of racism and nationalism has led some young people to associate the American flag with racism.
- Patriotism is about correcting your country when it's wrong, not blind obedience.
- The boy's act of kneeling demonstrated a profound understanding of patriotism at a young age.
- People's reactions to the boy's action showed a clash between traditional beliefs and the changing perspectives of younger generations.
- The significance of the boy's action lies in challenging the blending of racism and nationalism and in upholding values of independence and self-reliance.

# On-the-ground actions from transcript

- Contact your local Scout troop to learn more about their values and activities. (suggested)
- Join a community group focused on promoting diversity and inclusion in your area. (suggested)
- Give someone the space and support to express their beliefs, even if they differ from your own. (suggested)

# Oneliner

A young Cub Scout's act of taking a knee during the Pledge of Allegiance challenges traditional responses and embodies values of independence and self-reliance.

# Quotes

- "Patriotism is correcting your country when it's wrong. It's not blind obedience, that's nationalism."
- "Scouts are doing just fine because that is independent thought, that is self-reliance."
- "There are people in this country that want to hang on to this. They want to build this racist monument down on the border. They can. And they can milk it for all it's worth for about the next eight years, and then y'all are going to be in."

# Whats missing

Insight into the potential impact of the younger generation's changing perspectives on patriotism and nationalism.

# Tags

#Scouting #Patriotism #Nationalism #Racism #YouthPerspectives