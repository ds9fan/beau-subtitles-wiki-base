# Bits

- Domestic violence affects everyone, regardless of race, sex, or gender.
- Leaving an abusive relationship is not as simple as it seems; many factors like finances, children, and pets come into play.
- Domestic violence shelters lower the risk of harm and provide a fresh start for survivors.
- 78% of women killed at work from 2003 to 2008 were victims of domestic violence.
- Domestic violence accounts for 15% of all violent crime in the United States.
- Organizations like Shelter House of Northwest Florida address obstacles faced by survivors and provide innovative solutions, like housing pets.
- Supporting domestic violence shelters through donations, whether monetary or items like old cell phones, can make a significant impact.
- Abused individuals may not seek refuge with friends or family due to the risk of harm to their loved ones.
- Shelterhouse of Northwest Florida also provides kits for rape victims in hospitals, including fresh clothes for comfort.
- There should be no stigma associated with domestic violence, and support is vital for all survivors.

# On-the-ground actions from transcript

- Donate money or old cell phones to domestic violence shelters like Shelter House of Northwest Florida. (suggested)
- Contact Shelter House of Northwest Florida to inquire about ways to support or donate. (suggested)
- Look for donation links on social media platforms to contribute to domestic violence shelters. (implied)
- Support organizations that provide kits for rape victims in hospitals. (implied)

# Oneliner

Leaving an abusive relationship is complex; support domestic violence shelters like Shelter House of Northwest Florida through donations to help survivors start fresh and overcome obstacles.

# Quotes

- "It is not as simple as just leaving."
- "They lower that risk dramatically and they solve a lot of the problems."
- "They address the way people think about domestic violence as well."
- "Odds are you have an old cell phone sitting in a drunk drawer, mail it to them."
- "There should be no stigma about this."

# What's missing

In-depth insights into the impact of domestic violence on survivors and the importance of community support.

# Tags

#DomesticViolence #SupportSurvivors #Donate #ShelterHouse #CommunitySupport