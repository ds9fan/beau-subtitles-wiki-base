# Bits

- The United States is as politically divided today as it was during desegregation, with no unity.
- Trump's wall dominates headlines despite many Americans feeling it isn't a priority.
- Government and corporations preach conservation while destroying the environment.
- $3.4 billion is spent on lobbying and corruption in the US.
- News media often manipulates emotions rather than informing.
- The drug war continues to harm innocent people.
- Masculinity is fragile, leading to absurd displays like photos holding guns over a razor ad.
- Teachers are preparing for combat in schools.
- Systemic racism persists, with efforts to discredit and mock the youth.
- Hate crimes are rising, and literal Nazis are visible in the streets.
- Americans are taking action by sharing memes, buying land to tie up the wall in court, and fighting for safety and freedom.
- Independent journalists work to separate fact from fiction amidst social media purges.
- Communities are strengthening themselves internally, reducing the power of DC politicians.
- People are uniting across races to combat systemic racism and speak out against oppression.
- Defeating tyranny involves creating alternative systems rather than complying with existing ones.

# On-the-ground actions from transcript

- Contact local organizations fighting for safety and freedom (suggested)
- Join diverse community initiatives combatting violence in schools (suggested)
- Support independent journalists working to separate fact from fiction (suggested)
- Speak out against systemic racism in your community (suggested)
- Take action to strengthen your community from within (suggested)

# Oneliner

The United States remains politically divided with pressing issues like environmental destruction, systemic racism, and fragile masculinity, prompting citizens to take action towards unity and change.

# Quotes

- "We don't defeat tyranny and oppression by complying with it while we fight it. We defeat it by ignoring it while we quietly create our own systems to replace it."
- "The state of those people who care? Well, they're in a state of defiance."

# What's missing

The full transcript provides a detailed analysis of the current state of the United States, including political, social, and environmental issues, along with examples of citizen actions and responses. Viewing the entire conversation will provide a comprehensive understanding of the challenges and opportunities for change in the country.

# Tags

#UnitedStates #Politics #Environment #SystemicRacism #CommunityStrength #Defiance