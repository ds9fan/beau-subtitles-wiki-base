# Bits

- President Trump wants to put the Bible back in schools, sparking discussions among students.
- Christian students didn't like the idea, feeling they already receive enough religious education on Sundays.
- Modern church is indicted by the lack of interest from Christian students in having the Bible taught in schools.
- Advocating for teaching religious texts in schools, as long as all texts are included and focus is on philosophical aspects rather than enforcing moral codes.
- Different religious texts preach similar values, such as the Golden Rule, showing the commonality among religions.
- The downfall of Christianity in the United States is attributed to hypocrisy, especially in politics.
- Hypocrisy and failure to follow ethical teachings are leading to the decline of Christianity.
- Church of Satan may challenge the teaching of only Christian texts in schools, advocating for inclusion of their own texts based on legal rights.
- Philosophy, including religious philosophy, is seen as beneficial for education to teach people how to think rather than what to think.

# On-the-ground actions from transcript

- Contact your local school board and advocate for inclusive education of various religious texts (implied).
- Join your local community discussions on religious education in schools and share perspectives on the importance of teaching philosophical aspects (suggested).
- Give someone information about the Church of Satan's legal actions regarding religious texts in schools (generated).

# Oneliner

Discussing the importance of teaching religious texts in schools, focusing on philosophical aspects and avoiding moral enforcement to foster critical thinking and understanding of different beliefs.

# Quotes

- "Teaching the philosophy is education. The other, that's indoctrination. That's wrong. It is wrong."
- "Why do something to other people that you wouldn't want done to you?"
- "The goal of education should not be to teach you what to think. It should be to teach you how to think."

# What's missing

A deeper dive into the specific examples of hypocrisy in politics within Christianity and the potential impact of inclusive religious education on fostering understanding and unity among diverse belief systems.

# Tags

#ReligiousEducation #Philosophy #Inclusion #Hypocrisy #CriticalThinking #ChurchOfSatan