# Bits

- Historical figure discussed, reason for watching the video.
- Brother in Hitler Youth, her involvement in a similar organization, father's criticism of Hitler.
- Background as an artist, kindergarten teacher, and University of Munich student.
- Formation of White Rose Society with brother Hans and friends.
- Writing leaflets for passive resistance to Nazi regime.
- Execution of historical figure at 21, 76 years ago on February 22nd.
- Lessons learned: new ideas should change thinking, difference between legality and morality, patriotism vs. nationalism.
- Importance of passive resistance and meeting people where they are.
- Encouragement to engage in passive resistance by slowing down work or being less efficient.

# On-the-ground actions from transcript

- Contact your local historical society to learn more about resistance movements during WWII. (implied)
- Join a book club or discussion group to delve deeper into the concepts of legality and morality. (suggested)
- Give someone a history lesson on the White Rose Society and their impact on passive resistance. (generated)
- Go to a museum exhibit on WWII resistance movements to further understand the historical context. (suggested)

# Oneliner

Learn about the White Rose Society member who taught the importance of passive resistance and meeting people where they are, even during WWII.

# Quotes

- "How can we expect righteousness to prevail when there is hardly anybody willing to give himself up individually to a righteous cause?"
- "There is a difference between legality and morality."
- "Patriotism and nationalism aren't the same."
- "If you've got the means, you've got the responsibility."
- "Somebody after all had to make a start."

# Whats missing

The emotional impact and personal reflection that can best be experienced by watching the full video.

# Tags

#WhiteRoseSociety #PassiveResistance #WWII #ResistanceMovements #HistoryLessons #Morality