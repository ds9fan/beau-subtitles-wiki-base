# Bits

- The United States is trying to start a coup in Venezuela to institute regime change.
- Claims about preserving democracy in Venezuela by recognizing unelected individuals as the legitimate government are questioned.
- Low voter turnout in the 2018 election suggests that it was not rigged, as the opposition party boycotted it.
- The CIA director mentioned plans for regime change in Venezuela back in July 2017, showing that democracy is not the real motivator.
- Human rights violations in Venezuela are used as a pretext for U.S. intervention, but historical actions show the U.S. does not prioritize human rights in its foreign policy.
- Financial interests, particularly involving oil companies like Exxon and Chevron, may be a significant factor in U.S. involvement in Venezuela.
- The potential success of the coup could lead to a refugee crisis, with many refugees heading towards the U.S. southern border.
- The current border apprehension statistics do not support Trump's claims of a national emergency at the border.
- The debate between socialism and capitalism in Venezuela should also question the economic warfare tactics being used and the impact on the country.
- The U.S. involvement in triggering a coup in Venezuela showcases America's hypocrisy regarding foreign influence and election interference.

# On-the-ground actions from transcript

- Contact your representatives to express opposition to U.S. intervention in Venezuela (suggested).
- Join local advocacy groups or movements that support diplomatic solutions in foreign affairs (suggested).
- Give someone information on the historical context of U.S. foreign policy and regime change efforts (implied).
- Go to a protest or rally against military intervention in Venezuela (suggested).

# Oneliner

The U.S. attempts to trigger regime change in Venezuela under the guise of democracy and human rights, revealing underlying financial interests and potential humanitarian consequences.

# Quotes

- "U.S. doesn't care about human rights. It never has." 
- "It's normally about money, without money, it doesn't matter who gets hurt in the pursuit of that money."
- "There is no national emergency at the border. That's a lie. Trump is lying."
- "Maybe this isn't our job. It certainly showcases America's hypocrisy."
- "The only time human rights gets brought up is when it's time to convince you it's time to go invade some country."

# Whats missing

Deeper insights into the geopolitical implications of U.S. involvement in Venezuela that can be best understood through watching the full discussion.

# Tags

#Venezuela #USforeignpolicy #RegimeChange #HumanRights #CapitalismVsSocialism #RefugeeCrisis