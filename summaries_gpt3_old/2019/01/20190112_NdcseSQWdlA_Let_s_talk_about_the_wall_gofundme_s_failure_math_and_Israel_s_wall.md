# Bits

- The GoFundMe money for the wall is being refunded because it couldn't be given to the government to dictate policy.
- People raised only a small fraction of what was needed for the wall, proving that the majority don't support it.
- Americans understand the moral, ethical, legal, and economic wrongness of the wall idea.
- Comparing the proposed US wall with Israel's wall, the effectiveness and intent differ vastly.
- Israel's wall was for counter-terrorism, not immigration control, and involved tactics illegal in the US.
- The example of Israel's wall does not prove effectiveness, as it has not stopped incidents or changed methods.

# On-the-ground actions from transcript

- Contact your local representatives to express your opposition to the wall and support for ethical immigration policies. (generated)
- Join a community organization working towards promoting understanding and empathy for immigration-related issues. (implied)
- Give someone a fact-based perspective on the ineffectiveness of walls in controlling immigration and terrorism. (suggested)

# Oneliner

GoFundMe money for the wall refunded; majority opposes wall morally, legally, economically; Israel's wall example shows ineffectiveness and ethical concerns.

# Quotes

- "Americans don't want the wall. They know that it's morally wrong, ethically wrong, legally wrong, economically wrong, and they know that you're math wrong."
- "Name one country that has a wall that worked. Israel has a wall and it works. I'm not going to get into the moral arguments surrounding the Israeli-Palestinian conflict."
- "Their wall surrounds their opposition. They also control the waters off the coast. They also control the air. And it wasn't designed to stop immigration. It was designed to stop terrorism."

# What's missing

Insights on the long-term impact of public funds and the moral implications of using donations for political purposes.

# Tags

#GoFundMe #WallFunding #ImmigrationPolicy #EthicalConcerns #PublicFunds #IsraelWall #TerrorismControl