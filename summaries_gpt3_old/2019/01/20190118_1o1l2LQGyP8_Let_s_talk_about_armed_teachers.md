# Bits

- Armed teachers in schools are not a practical solution, as it requires intense training and resources that are not typically available in schools.
- Teachers considering being armed need to change their mindset from educator to warrior and must train extensively in secret.
- Mobile cover can be created using a modified filing cabinet filled with armor plates to protect against potential threats.
- Use frangible ammo to reduce the risk of ricochets and harm to students in the event of a school shooting.
- Teachers must weigh the risks of a school shooting against the possibility of losing control of their weapon in the classroom.

# On-the-ground actions from transcript

- Contact your local metal manufacturer or welder to build mobile cover using a filing cabinet with armor plates (suggested).
- Obtain frangible ammo for your weapon to reduce the risk of ricochets (suggested).
- Watch the video "Buying a Gun for Political Violence" to understand the concepts discussed (suggested).

# Oneliner

Arming teachers is not a practical solution for school safety due to the extensive training and resources required to effectively respond to potential threats.

# Quotes

- "When you're walking through the hallway, you're not a teacher anymore, you're a warrior."
- "You have to change your entire mindset."
- "If it works, it's because you made it work."
- "Teachers must weigh the risks of a school shooting against the likelihood of losing control of their weapon in that classroom."
- "Liability and effectiveness don't go hand in hand."

# What's missing

The full video provides detailed insights into the challenges and complexities of arming teachers in schools, along with practical suggestions on how to enhance safety measures in educational settings.