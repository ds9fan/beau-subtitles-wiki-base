# Bits

- President Trump is using negotiation tactics similar to that of a hostage taker by offering to release children in exchange for funding for his wall.
- Dreamers, referring to DACA recipients, are being offered three years of protection in exchange for the wall funding, even though they are not seen as a threat by the President.
- The offer of three years of protection for Dreamers raises questions about why a path to citizenship can't be established for them.
- People under Temporary Protected Status (TPS) are also being offered protection in exchange for funding, despite being legal and abiding by the law.
- President Trump's tactics are likened to extortion and thuggery, using federal law enforcement as leverage.
- The lack of action from Congress and the Senate to call out this behavior is deemed embarrassing.
- The speaker advocates for establishing a path to citizenship for Dreamers and opposes punishing individuals for their parents' actions.
- The President's approach of shutting down the government for his wall is criticized, with a call for following proper legislative procedures.

# On-the-ground actions from transcript

- Contact your representatives and urge them to establish a path to citizenship for Dreamers and individuals under Temporary Protected Status. (suggested)
- Join local advocacy groups supporting immigrants' rights to contribute to creating awareness and pushing for change. (suggested)
- Give support to organizations aiding individuals affected by immigration policies to help those in need. (suggested)

# Oneliner

President Trump's negotiation tactics likened to hostage taking by offering protection in exchange for wall funding, with calls for establishing a path to citizenship and criticism of government inaction.

# Quotes

- "The President of the United States is acting like a criminal, and he's using the guns of federal law enforcement as his soldiers in the street."
- "I don't think they should be punished for the sins of their father."
- "Not disrupt the lives of federal employees, of people in need, of everybody. Because you want a monument to yourself, Mr. President."

# What's missing

The full video provides deeper insights into the speaker's analysis and perspective on immigration policies and the government's handling of related issues.

# Tags

#Immigration #PresidentTrump #NegotiationTactics #PathToCitizenship #Advocacy #GovernmentInaction