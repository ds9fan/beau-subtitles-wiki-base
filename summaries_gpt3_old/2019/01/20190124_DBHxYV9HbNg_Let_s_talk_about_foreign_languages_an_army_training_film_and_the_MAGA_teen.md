# Bits

- Switching to someone's native languages can aid in communication, especially if they are struggling in English ( exemplified ).
- Activists and academics should pay attention to speaking the same "languages" as the people they are addressing to ensure effective communication ( generated ).
- Effective communication involves using terminology that the audience understands, even if it means adjusting your own terminology ( implied ).
- The way something is presented can be more significant than the information itself ( exemplified ).
- Being the right messenger is key in delivering a message effectively ( exemplified ).
- Presenting information in a relatable manner and using the right messenger can lead to better reception and understanding ( exemplified ).
- Messaging and perception can often be more impactful than the actual facts presented ( exemplified ).
- It's vital to be conscious of how messages are perceived, especially in the fight against fake narratives and emergencies ( exemplified ).
- Jumping to conclusions based on initial presentations without considering multiple viewpoints can lead to misunderstandings ( exemplified ).
- The messenger can affect how a message is received, sometimes more than the message itself ( exemplified ).

# On-the-ground actions from transcript

- Contact local activist groups and organizations to discuss effective communication strategies ( suggested ).
- Join community forums or groups to practice relatable communication styles ( suggested ).
- Give someone a platform to share their stories and relate information effectively ( exemplified ).
- Go to a communication workshop or training to improve messaging and presentation skills ( suggested ).

# Oneliner

Effective communication involves speaking the audience's "languages" to ensure understanding; the messenger can be more critical than the message itself.

# Quotes

- "Effective communication involves using terminology that the audience understands, even if it means adjusting your own terminology."
- "The way something is presented can be more significant than the information itself."
- "Being the right messenger is key in delivering a message effectively."
- "Messaging and perception can often be more impactful than the actual facts presented."
- "It's vital to be conscious of how messages are perceived, especially in the fight against fake narratives and emergencies."

# What's missing

The full conversation provides valuable insights into the importance of effective communication and messaging strategies for activists and academics. Viewing the entire transcript will offer a deeper understanding of the examples provided and how they can be applied in various contexts.

# Tags

#EffectiveCommunication #Messaging #Activism #CommunityEngagement #FakeNarratives #Perception