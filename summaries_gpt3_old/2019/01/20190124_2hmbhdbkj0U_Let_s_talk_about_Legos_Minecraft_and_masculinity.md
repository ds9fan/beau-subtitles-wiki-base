# Bits

- A man mocks kids for playing with Legos instead of football, facing backlash on Twitter for his comments.
- Bo praises the creativity and skill involved in building with Legos, citing his son's impressive creation of a temporary castle without plans or diagrams.
- Bo contrasts the man's mocking behavior with the pride he should have felt for his son's robot-building skills, encouraging him to apologize for his actions.
- The transcript underscores the importance of valuing and encouraging creative and technical skills in children rather than mocking them for societal perceptions of masculinity.

# On-the-ground actions from transcript

- Contact the man to apologize to his son and the other kids he mocked (suggested).
- Join a local Lego building competition or robotics club to support creative endeavors in children (generated).
- Give someone praise for their creative or technical skills, especially children (implied).

# Oneliner

Mocking children's creative skills is not manly; valuing and encouraging them is. Apologize and support creative endeavors in kids.

# Quotes

- "Your kid built a robot. A working robot. That is awesome. You should be proud."
- "You might want to get on Twitter and apologize to your son and all the other kids that got caught in a crossfire."
- "You're mocking the creative aspects of masculinity. That's probably a bad idea."

# Whats missing

The emotional impact and nuances of the conversation, as well as the broader implications of societal attitudes towards creativity and masculinity. 

# Tags

#Creativity #Masculinity #SupportChildren #Apologize #Encourage