# Bits

- To change the world, one must address the topic of money and resources, even if it's not the most popular subject. (introduction)
- Those with limited means are often most aware of the failings of the system but lack resources to make a significant change. (who)
- Saving small amounts like $50 a month on minimum wage won't lead to substantial change in one's life. (what)
- The key to financial independence is not just saving but increasing the amount of money coming in. (how)
- Building networks and being recommended by others within those networks can lead to more opportunities and income. (how)
- Recommendations from active community members carry weight and can lead to the growth of micro-businesses. (how)
- Starting businesses with low or no startup costs can be a way to increase income and achieve financial independence. (how)
- Complementary skills within a network can lead to successful collaborations and ventures, such as firefighters remodeling houses. (how)
- Investing windfalls like tax returns into low-cost businesses can be a step towards increasing income. (how)
- Supporting someone in your network, like helping them start a business, can also be a way to increase your own income. (how)

# On-the-ground actions from transcript

- Increase the amount of money you have coming in by starting low or no-cost businesses. (implied)
- Build networks and be active in your community to receive recommendations that can lead to more opportunities. (implied)
- Support someone in your network by investing in their business to potentially increase your own income. (implied)

# Oneliner

To change the world and achieve financial independence, focus on increasing the amount of money coming in, building networks for recommendations, and starting low-cost businesses.

# Quotes

- "The solution is increasing the amount of money you have coming in."
- "That recommendation carries a lot of weight."
- "Sometimes the skills of the entire network complement each other."
- "Invest windfalls into low-cost businesses to increase income."
- "You want to break away from that system, you don't have to be employed by somebody else."

# Whats missing

The full transcript provides practical insights on how individuals with limited means can work towards financial independence by increasing their income through network building and low-cost business ventures.

# Tags

#FinancialIndependence #CommunityNetworking #IncomeGeneration #ResourceManagement #SmallBusinesses