# Bits

- Beau strives to bring original ideas or concepts to the conversation in his videos, avoiding the expected topics.
- He points out the importance of the oath to defend the Constitution rather than the country itself.
- The Constitution is emphasized as a living document that includes mechanisms for change, even though flawed individuals created it.
- Beau discusses the revolutionary nature of the Constitution, particularly in establishing a government of the people and providing a framework for future change.
- He envisions a world where government is unnecessary, advocating for humanity to advance to a point where governance is no longer needed.
- Beau sees speaking out against injustices and corruption in government as a duty of citizens to uphold the principles of the Constitution.

# On-the-ground actions from transcript

- Contact your representatives to speak out against injustices and corruption in government (implied)
- Join local advocacy groups or organizations that work towards upholding constitutional principles (implied)
- Give someone a platform to share their voice and concerns about government actions (implied)

# Oneliner

Beau challenges the notion of blind nationalism, advocating for a focus on upholding the living Constitution and speaking out against government injustices.

# Quotes

- "Getting rid of government is not destroying the Constitution. It's carrying it to its logical conclusion."
- "Today, we the people can ensure tranquility, provide for the common defense, and secure the blessings of liberty everywhere."
- "When fascism comes to the United States, it'll be wrapped in a flag and carried across. I believe that when freedom comes to the world, it'll originate here."

# What's missing

The full video provides a deep dive into the importance of upholding constitutional principles, the flaws and evolution of governance, and the role of citizens in preserving democratic values.

# Tags

#Constitution #Government #Citizenship #Advocacy #Change #Nationalism