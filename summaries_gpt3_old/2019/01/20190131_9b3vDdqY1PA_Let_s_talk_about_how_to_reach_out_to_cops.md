# Bits

- ACAB and FTP slogans are used to express the idea that all cops are bad due to their profession or complicity in silence regarding bad cops.
- Slogans like ACAB can be effective in energizing like-minded individuals but may turn others off from listening.
- Focus on harm reduction and self-interest when discussing police reform, rather than using divisive slogans.
- The drug war has led to police militarization and negative perceptions of law enforcement as the enemy.
- Decriminalization and harm reduction strategies, like in Portugal, have been successful in reducing drug-related issues.
- Discussing the failures of the drug war and its impact on officers can be a more effective way to engage with law enforcement.
- Addressing the image and culture of law enforcement, including their patriotism and pride, can be key in fostering dialogue for change.

# On-the-ground actions from transcript

- Contact your local representatives to advocate for harm reduction strategies in drug policy and police reform. (suggested)
- Join a community organization working on police accountability and drug policy reform. (suggested)
- Give someone information about the success of decriminalization efforts in countries like Portugal. (implied)

# Oneliner

Using divisive slogans like ACAB may energize some but turn others off from engaging in productive conversations on police reform and harm reduction strategies.

# Quotes

- "Slogans like ACAB can energize the base, but they also turn off listeners who may not agree with that ideology." 
- "The drug war is without a doubt the focal point of our problems with law enforcement in this country."
- "Not waging the drug war is more effective than waging it if you actually believe in harm reduction."
- "Addressing the image and culture of law enforcement can be key in fostering dialogue for change."
- "Most of them see themselves as true patriots, and you see it all the time when you see that thin blue line sticker next to the 3% logo."

# Whats missing

In-depth examples and personal anecdotes from individuals who have successfully engaged with law enforcement on issues of police reform and harm reduction.

# Tags

#Police #ACAB #HarmReduction #DrugWar #Portugal #CommunityEngagement