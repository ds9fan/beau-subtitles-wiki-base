# Bits

- Bo discusses the movie "The Breakfast Club" with his dad, referencing how people express themselves when they were younger. (who, what, when)
- Bo reflects on the evolution of people as they age and change. (who, what, when, why)
- Bo mentions a video of a congressperson dancing online. (who, what)
- Bo praises the First Lady for changing people's perceptions of her through a video where she dances, challenging negative stereotypes. (who, what, why)
- Bo subtly criticizes those who judge the First Lady based on her appearance and age, implying underlying biases. (who, what, why)
- Bo humorously deflects his dad's attempt to ask about the First Lady when she was younger. (who, what)
- Bo ends with a sarcastic but thought-provoking comment about people's reactions to the First Lady's video. (what)

# On-the-ground actions from transcript

- Contact the Republican Party to express congratulations on changing perceptions positively (generated)
- Join a discussion group on ageism and biases in society (implied)
- Go watch "The Breakfast Club" movie to understand cultural references (implied)

# Oneliner

Bo discusses perception changes and biases with humor and insight, touching on aging, stereotypes, and expression through a video.

# Quotes

- "With one video, she has been turned into the most adorable woman in the country."
- "everybody knows the only reason you guys are angry is because she looks the way she does and she still has enough energy to dance because she's not dying of dehydration."

# Whats missing

Insight into societal biases and perceptions, humor intertwined with thought-provoking commentary.

# Tags

#Perception #Biases #Aging #Stereotypes #Humor