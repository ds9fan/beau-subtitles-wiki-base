# Bits

- Bo, a history buff who loves to travel, expresses concern about the US pulling out of UNESCO, an organization that protects historical and cultural sites.
- Bo discusses various reasons Trump supporters gave for pulling out of UNESCO, such as taxation, anti-Semitism, and climate change.
- Bo explains the connection between UNESCO's focus on climate change and the protection of historical sites near waterways from rising sea levels.
- Bo delves into the debate on climate change, pointing out the influence of propaganda funded by oil companies on climate change denial.
- Bo advocates for sustainable practices like stopping deforestation, moving towards renewable energy, and cutting down on pollution, regardless of beliefs on climate change.
- Bo criticizes the role of partisan politics and propaganda in influencing individuals, especially rural communities, to argue against their own interests in environmental conservation.
- Bo suggests that politicians' ties to campaign contributors hinder necessary changes towards environmental sustainability, urging individuals to reconsider their stance for a cleaner world.

# On-the-ground actions from transcript

- Contact local environmental organizations to get involved in conservation efforts (implied)
- Join community initiatives focused on sustainable practices like renewable energy and reducing pollution (implied)
- Give someone information on the importance of protecting historical and cultural sites (suggested)
- Go to local events or workshops on climate change to educate oneself and others (suggested)

# Oneliner

Bo discusses the US withdrawal from UNESCO, debates on climate change, and the importance of sustainable practices, urging individuals to reconsider arguing against their own interests in environmental conservation.

# Quotes

- "The propaganda is that thick."
- "You're arguing against your own interests."
- "Lowering your own light bill. Making sure that you have a job. Blows my mind."
- "You work on your car in your garage and you want to test it, see if it works. You crank it up and leave it running with the door closed. No, of course not. That's stupid."
- "Y'all have a good night."

# What's missing

The full video provides a deep dive into the reasons behind the US withdrawal from UNESCO, the debate on climate change, and the impact of propaganda on individuals' environmental beliefs.

# Tags

#USWithdrawal #ClimateChangeDebate #Sustainability #HistoricalSites #EnvironmentalConservation #Propaganda #PoliticalInterests #CommunityInvolvement