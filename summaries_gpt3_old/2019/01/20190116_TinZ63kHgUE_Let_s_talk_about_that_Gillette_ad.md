# Bits

- Bo's son understood the Gillette ad message about not sexually assaulting, belittling, or bullying, showing that these concepts are not problematic for men.
- The ad was not an attack on masculinity but a promotion of real masculinity, with components like honor and integrity.
- Bo believes that a razor company like Gillette is suitable to engage in conversations about social issues like rites of passage and masculinity.
- He contrasts American rites of passage with those from other cultures, like the Amazon's tradition of enduring painful rituals to become men.
- Bo criticizes American masculinity for struggling with discomfort and endurance, contrasting it with the toughness displayed by boys in other cultures during their rites of passage.

# On-the-ground actions from transcript

- Contact your local community organization to discuss promoting positive masculinity (implied)
- Join your local men's group to engage in conversations about masculinity and rites of passage (suggested)
- Go to a cultural event or workshop to learn about different rites of passage from around the world (generated)

# Oneliner

Bo discusses how the Gillette ad was about promoting positive masculinity and contrasts American rites of passage with those from other cultures, critiquing American masculinity's response to discomfort.

# Quotes

- "My 12-year-old boy was more of a man than the people that are upset with this because he didn't identify with the bad guy."
- "If you felt attacked by that ad, it says more about you than it does Gillette."
- "In the Amazon, you've got these young boys becoming men who endure some of the most painful bites known to man for an extended period of time."
- "American men look at this and say, our boys need to be tough like that."
- "Any worthy goal is going to require you to suffer."

# Whats missing

Deeper insights into the impact of cultural rituals on shaping masculinity and societal perceptions.

# Tags

#GilletteAd #Masculinity #RitesofPassage #CulturalTraditions #PositiveMasculinity