# Bits

- Four cops shot during a drug raid in Houston due to potential lack of proper threat assessment or ignoring warnings.
- Police union president threatens to track and hold accountable those criticizing police officers.
- A group in Texas offers free training for 15 officers if the police union president resigns, citing importance of freedom of speech.
- Freedom of speech is seen as a critical aspect of community safety and holding government accountable.

# On-the-ground actions from transcript

- Contact the Texas group offering free training for officers to inquire about their training programs (suggested).
- Join local community discussions on police accountability and freedom of speech (suggested).
- Give support to organizations advocating for freedom of speech and police accountability (suggested).

# Oneliner

Police union president threatens to track critics; Texas group offers free training if he resigns—linking freedom of speech to community safety.

# Quotes

- "If you don't want to be cast as the enemy, don't be the enemy of freedom."
- "You representing an armed branch of government do not get to tell people what they can and cannot talk about."
- "Freedom is the bedrock of safety."

# Whats missing

Deeper insights on the training programs offered by the Texas group and potential impacts on police-community relations.

# Tags

#Houston #PoliceShooting #FreedomOfSpeech #PoliceAccountability #CommunitySafety