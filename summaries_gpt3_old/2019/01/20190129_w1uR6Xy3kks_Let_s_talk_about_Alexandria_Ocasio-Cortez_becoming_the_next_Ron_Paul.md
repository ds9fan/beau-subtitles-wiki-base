# Bits

- Bo compared Alexandria Ocasio-Cortez (AOC) to Ron Paul, causing a reaction among his libertarian friends. (introduced)
- Ron Paul, a doctor turned congressman from Texas, is seen as a philosophical giant in the libertarian community for his strict adherence to the Constitution. (introduced)
- Ron Paul was known as "Dr. No" for voting against anything not expressly authorized by the Constitution, like the Patriot Act. (explained)
- AOC is viewed as a young commie from New York by conservative circles due to her beliefs about corporate greed and its impact on the little guy. (explained)
- Bo suggests that millennials, who have faced economic challenges for years, are drawn to AOC's views on corporate greed and its effects on the economy. (implied)
- While AOC and Ron Paul may seem ideologically opposite, both may have valid points about the establishment and its impact on society. (suggested)
- Bo questions whether the real divide is not between corporate and government establishments but between the establishment and the people. (suggested)
- He compares the current political situation to "Weekend at Bernie's," with the establishment controlling the body of democracy. (suggested)
- Bo expresses hope that AOC will grow and evolve politically, just as Ron Paul did over time. (implied)
- He hints at the possibility of AOC becoming a powerhouse in politics, raising questions about who will fill Ron Paul's shoes and whether the establishment will allow it. (implied)

# On-the-ground actions from transcript

- Contact your local political representatives to express your views on corporate greed and government regulations. (suggested)
- Join a community organization or political group to discuss and advocate for policies that address economic inequality. (suggested)
- Give someone in need a helping hand, whether it's through volunteering at a food bank or supporting initiatives that assist those facing financial difficulties. (implied)

# Oneliner

Bo compares AOC to Ron Paul, sparking controversy among his libertarian friends, and raises questions about the establishment versus the people in politics.

# Quotes

- "What if it's not corporate establishment versus government establishment? What if it's just establishment versus you?"
- "With a little bit of refinement and a little bit of time, she'll grow just like everybody does."
- "The question is, is there going to be anybody to fill Ron Paul's shoes at the same time?"
- "What if over here you've got the corporate establishment working the head of that body and over here you've got the government establishment working the arms..."
- "I think that we're looking at a powerhouse in the making."

# Whats missing

The full conversation provides a nuanced perspective on the political landscape, discussing the influence of corporate greed, government regulations, and the role of the establishment in American politics.

# Tags

#Bo #AOC #RonPaul #politics #establishment #corporategreed #governmentregulations