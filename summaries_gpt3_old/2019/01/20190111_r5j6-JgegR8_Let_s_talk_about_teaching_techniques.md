# Bits

- Beau shared a story about a woman who went from being anti-gun to purchasing firearms after a synagogue shooting.
- The woman learned how to shoot from a sixty-something year old man who took an academic approach to teaching.
- The man taught her by explaining the mechanics of guns, different shooting stances, and techniques like changing magazines before ever putting bullets in the gun.
- He made the learning experience about the information rather than himself, allowing each phase of information to be digested.
- The most critical aspect of his teaching was not making it confrontational and letting the ideas stand on their own.
- The woman learned a lot more about shooting from this method compared to shooting with military personnel.

# On-the-ground actions from transcript

- Contact your local shooting range to inquire about firearm safety and training classes. (implied)
- Join a firearm safety course to understand the responsibilities of owning a gun. (implied)
- Give someone interested in firearms the advice to start with proper training before purchasing a gun. (implied)

# Oneliner

Beau shares a story of a woman transitioning from anti-gun to purchasing firearms, learning the importance of academic teaching and focusing on information rather than confrontation.

# Quotes

- "Ideas and information stand and fall on their own."
- "Those who have seen the most say the least."
- "Make it a conversation rather than a debate."

# What's missing

The video provides insight into effective teaching techniques for sharing information across ideological lines, focusing on making discussions academic rather than confrontational.

# Tags

#FirearmSafety #TeachingTechniques #AcademicApproach #InformationSharing #ConversationVsDebate