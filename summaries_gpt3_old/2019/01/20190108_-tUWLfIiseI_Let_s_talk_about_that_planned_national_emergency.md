# Bits

- Border apprehensions at a 40-year low.
- Constitution defines process for project funding.
- Draw comparisons between Trump and Hitler in declaring national emergencies.
- Fascism defined by specific elements such as blending government and economic power, disdain for human rights, nationalism, etc.
- The importance of Congress as a check on presidential power.
- Trump's threat to declare national emergency for border wall funding.
- Choice between supporting the Constitution or the President's actions.

# On-the-ground actions from transcript

- Contact your representatives and urge them to uphold the Constitution (suggested).
- Join local civics groups to stay informed on political matters (suggested).
- Attend town hall meetings to voice your concerns about potential misuse of power (suggested).

# Oneliner

Comparison between Trump and Hitler in declaring national emergencies leads to a choice: support the Constitution or the President's actions.

# Quotes

- "Either you support the constitution of the United States or you betray it."
- "You're a patriot or a traitor."
- "Circumventing Congress, it's a betrayal of the very ideas this country was founded on."

# Whats missing

In-depth historical context and analysis of fascism as applied to current political situations.

# Tags

#BorderWall #NationalEmergency #Fascism #Congress #Constitution #PoliticalAnalysis