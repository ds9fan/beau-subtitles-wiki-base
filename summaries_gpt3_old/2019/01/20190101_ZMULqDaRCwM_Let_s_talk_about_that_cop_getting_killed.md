# Bits

- Bo discusses the lack of recognition for police officers killed in the line of duty, especially when they don't fit a certain narrative.
- He contrasts the attention given to a fallen officer who was an illegal immigrant versus others, questioning the underlying biases in reporting.
- Bo challenges the notion of holding entire demographics responsible for the actions of individuals, pointing out the flaws in such generalizations.
- He delves into the racial undertones in discussions around immigration and law enforcement, particularly critiquing organizations like FAIR and CIS.
- Bo draws parallels between different incidents, like the deaths of police officers and military personnel, to showcase inconsistencies in public reactions and narratives.
- The core takeaway is to stop using tragedies and sacrifices for political agendas and to treat all lives with equal respect and recognition.

# On-the-ground actions from transcript

- Contact your local officials to advocate for fair and unbiased reporting on incidents involving law enforcement and immigrants. (implied)
- Join your local community dialogues on race relations and immigration to foster understanding and empathy. (generated)
- Support organizations that work towards fair and equal treatment for all individuals regardless of their background. (suggested)

# Oneliner

Bo criticizes the unequal recognition of fallen officers, challenges biased narratives on immigration, and urges for respect and equality in honoring sacrifices for political gain.

# Quotes

- "Stop standing on the graves, the corpses, the flag-covered coffins of people far more honorable than you to make a political point they might not agree with."
- "You don't care about the law. You don't. You don't even know what it is."
- "It's numbers at that point. Not saying it's right. I'm saying that's the reality of it."
- "Reminds me a lot of Benghazi too."
- "You're not outraged about a cop getting killed, you didn't know the names of the other ones."

# What's missing

The full video provides a deep dive into the complexities of media representation, biases in reporting, and the human aspect behind tragic incidents like the deaths of law enforcement officers and military personnel.

# Tags

#LawEnforcement #Immigration #Bias #Equality #Narratives #Respect