# Bits

- SNAP benefits can be used to purchase items like seeds, fruit trees, and berry bushes to grow food, providing a sustainable food source for some individuals. (who, what, where)
- More than 10% of Americans rely on food stamps, with the average household receiving about $250 a month in benefits. (who, what, when)
- Misconceptions surrounding food stamps include assumptions about the types of food purchased and the demographics of recipients. (what, why)
- Stereotypes about food stamp recipients being lazy or not working are debunked, as many working families and even active-duty military members rely on SNAP benefits due to income levels. (what, why)
- The prevalence of food stamps is a result of a broken system that shifts blame onto recipients rather than addressing systemic issues and corporate decisions. (why, dynamics)
- Growing your own food through vertical gardening or community gardens can be a practical solution for individuals in urban areas lacking space for traditional gardening. (what, where, dynamics)
- Collaborating with community centers, churches, or landowners to grow food can strengthen social networks and provide access to fresh produce for individuals on food stamps. (what, where, why)

# On-the-ground actions from transcript

- Contact local community centers or churches to set up a community garden where everyone can contribute. (generated)
- Join or establish a community gardening initiative to grow food collectively and build a stronger network within your community. (implied)
- Reach out to landowners to discuss the possibility of growing fruit trees or bushes on their property, benefiting both parties. (implied)
- Go to gardening stores or online platforms to purchase seeds, fruit trees, or berry bushes using SNAP benefits to start your own sustainable food source. (generated)

# Oneliner

Using SNAP benefits to purchase seeds and fruit trees can provide a sustainable food source, dispelling myths about food stamps and promoting self-sufficiency through urban gardening initiatives.

# Quotes

- "It's not what you think it is."
- "The prevalence of food stamps has to do with the fact that the system is broken."
- "Once you're locked into that cycle of poverty, it's very hard to get out."
- "Every little bit helps in that regard."
- "It increases your food security, hopefully cuts down your dependence, and maybe can contribute to getting you out."

# Whats missing

The full video provides detailed insights into the misconceptions and realities of food stamps, the benefits of growing food using SNAP, and practical solutions for urban gardening.