# Bits

- Matoaka, also known as Pocahontas, had a tragic life where she was kidnapped, held hostage, forced into marriage, and ultimately met an early death due to poisoning or disease.
- The popularized version of Native stories often romanticizes or distorts the harsh realities faced by indigenous people, like in the case of Pocahontas being portrayed as a love story when in reality, it was a story of abuse and tragedy.
- Native stories in popular culture frequently involve a narrative of a white savior coming in to save the day, overshadowing the true experiences and identities of Native individuals.
- The distortion of Native stories and culture in mainstream media contributes to the perpetuation of inaccurate myths and stereotypes about indigenous people.
- Thanksgiving serves as a reminder to critically reflect on the historical inaccuracies and misrepresentations of Native culture that persist in society.

# On-the-ground actions from transcript

- Contact Native activists or organizations to learn more about accurate Native history (suggested)
- Join events or discussions that shed light on the true narratives and experiences of indigenous people (suggested)
- Give someone the platform to share their personal stories and perspectives on Native culture (implied)
- Attend cultural events or workshops that aim to educate about and honor Native traditions (suggested)

# Oneliner

The real story of Pocahontas (Matoaka) reveals the tragic reality of indigenous experiences often romanticized in popular culture, urging reflection on the misrepresentation of Native stories and the impact of white savior narratives.

# Quotes

- "Most of what you know of Native stories is false. It's completely false, completely made up."
- "There's not a whole lot of stories about natives just being native."
- "It's pretty clear he probably didn't do, at least not to the extent they're saying."
- "Thanksgiving is a good time to bring it up and just kind of rememeber that most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."
- "Not a whole lot of it's true in popular."

# Whats missing

A deeper understanding and appreciation of Native history, culture, and experiences can be gained from watching the full transcript.