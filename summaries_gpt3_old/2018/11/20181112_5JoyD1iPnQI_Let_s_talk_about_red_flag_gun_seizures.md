# Bits

- Red flag gun seizures involve local cops obtaining court orders to temporarily seize guns from individuals deemed a threat based on their behavior.
- The concept is sound in theory, but the current execution is flawed, as it lacks due process and can lead to dangerous situations.
- The tactical implementation of these seizures, like showing up early in the morning, mirrors military tactics and can escalate rather than de-escalate situations.
- Law enforcement should receive proper training and not just equipment from the Department of Defense to avoid unnecessary harm during these operations.
- The speaker suggests a better way to implement red flag gun seizures by ensuring due process, avoiding early morning confrontations, and focusing on mitigating risks while respecting individuals' rights.

# On-the-ground actions from transcript

- Contact your representative to advocate for fixing the execution of red flag gun seizures immediately. (suggested)
- Join advocacy groups focused on gun control to work towards improving the laws around red flag gun seizures. (suggested)
- Give someone in law enforcement proper training on handling red flag gun seizures to avoid harmful situations. (implied)

# Oneliner

Red flag gun seizures have a sound concept but flawed execution; advocate for due process and safer tactics to prevent unnecessary harm and ensure effectiveness.

# Quotes

- "The idea is sound, okay, the execution isn't."
- "You've got to know something about him. You should know where he works at least."
- "That due process is so very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very..." (continues for a while)
- "If due process is there, what's the problem?"
- "It's a good idea. The execution is bad and the execution can be fixed."

# Whats missing

The full video can provide more in-depth examples of flawed execution and further insights into the speaker's proposed solutions.

# Tags

#RedFlagLaws #GunControl #DueProcess #Advocacy #CommunitySafety