# Bits

- Claiming asylum by crossing the border without permission is lawful and moral.
- Many were misled by news outlets and the President into thinking seeking asylum is illegal.
- Trump attempted to change asylum law by issuing a proclamation, acting like a dictator.
- The judicial branch stopped Trump's attempt to circumvent the Constitution.
- People need to understand the importance of upholding the Constitution over partisan politics.
- Defenders of Trump's actions are criticized for not truly caring about the Constitution.

# On-the-ground actions from transcript

- Contact your representatives to ensure they uphold the Constitution (generated)
- Join local advocacy groups focused on immigration and constitutional rights (implied)
- Give someone accurate information about seeking asylum in the US (implied)
- Go to a protest or rally supporting the judiciary's role in upholding the Constitution (suggested)

# Oneliner

Seeking asylum by crossing the border is lawful, Trump's attempt to change it like a dictator was stopped by the judiciary, revealing the importance of upholding the Constitution over partisan politics.

# Quotes

- "Claiming asylum is lawful, completely legal, completely moral more importantly."
- "That is what he attempted to do. Make no mistake about it."
- "He attempted to undermine the Constitution of the United States."
- "If it's your party, they can set fire to the Constitution and you'll roast a marshmallow over it."
- "Enjoy your holiday. It's just a thought."

# Whats missing

The full video provides an in-depth analysis of how Trump's attempt to change asylum law reflects a challenge to the Constitution and the importance of upholding democratic principles.

# Tags

#Asylum #Trump #Constitution #Judiciary #Immigration #PartisanPolitics #Advocacy #Rights #Protest #ConstitutionalCrisis