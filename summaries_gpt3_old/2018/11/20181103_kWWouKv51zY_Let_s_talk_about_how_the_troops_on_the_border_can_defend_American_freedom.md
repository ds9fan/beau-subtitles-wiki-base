# Bits

- An anecdote about a general during the Tet Offensive illustrates the importance of clear orders and implementation on the ground.
- War fighters at the border have a unique chance to defend American freedom, a rare occurrence throughout history.
- Mission creep is exemplified through the evolving objectives in Afghanistan, showing how initial missions can expand indefinitely.
- The Department of Homeland Security's request for U.S. soldiers to act as emergency law enforcement personnel was rightfully declined due to its illegality.
- Soldiers are warned against mission creep and reminded to adhere to the laws of war and rules of engagement, especially when dealing with non-combatants.
- Soldiers are advised to keep their weapons slung and on safe, avoiding involvement in unclear or potentially illegal actions.
- Those at the border are encouraged to learn about legal asylum processes in the U.S., noting that seeking asylum is a legal right even if arriving without prior applications elsewhere.
- Republicans controlling branches of government could change asylum laws but haven't, indicating a desire to exploit the situation at the border for political gain.
- Soldiers are cautioned against blindly following vague or illegal orders, as superiors may not support them when legal consequences arise.
- Soldiers are urged to seek clarification on orders, maintain records of communication, and prioritize legality and adherence to the law over potentially unlawful actions.

# On-the-ground actions from transcript

- Contact USCIS website to learn about legal asylum processes in the U.S. (suggested)
- Seek clarification via email or other means on vague orders to maintain a record for potential defense. (recommended)
- Keep weapons slung and on safe, avoiding involvement in unclear or potentially illegal actions. (implied)

# Oneliner

Soldiers are advised to prioritize legality and adherence to the law over potentially unlawful actions, seek clarification on orders, and avoid being manipulated for political agendas.

# Quotes

- "You want to defend American freedom, you keep that weapon slung and you keep it on safe."
- "It's bred into you, right? You wouldn't do anything illegal. You wouldn't circumvent the law, earn that trust, keep that weapon slung and keep it on safe."
- "Your honor, being used to legitimize them circumventing the law."
- "It's bred into you, right? Honor, integrity, those things."
- "Because if you allow that mission creep to happen, you're going to need it. You're going to need a defense."

# Whats missing

Insight into the potential repercussions faced by soldiers who stray from legal and ethical boundaries could be best understood by watching the full transcript.

# Tags

#Soldiers #LegalAdherence #AsylumProcess #MissionCreep #BorderSituation #PoliticalManipulation #USCIS #ClarifyOrders