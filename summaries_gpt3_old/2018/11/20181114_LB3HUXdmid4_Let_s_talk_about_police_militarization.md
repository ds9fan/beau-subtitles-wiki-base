# Bits

- The concept of the warrior cop is problematic, as it equates being a warrior with being a killer (implied).
- Only 4% of the military are killers, but police forces are increasingly militarized to mimic the military (implied).
- Police officers should be public servants, not just enforcers of the law (implied).
- Propaganda within law enforcement leads to a belief in a war on cops, justifying militarization and aggressive tactics (implied).
- The focus on being warrior cops leads to a lack of proper training with the advanced equipment they possess (implied).
- The discrepancy between perceived and actual capability of SWAT teams can lead to disastrous outcomes (implied).
- Police violence is not as dangerous as portrayed, and there is no actual war on cops (implied).
- Law enforcement needs to reconsider their approach to policing to prevent unnecessary fatalities (implied).

# On-the-ground actions from transcript

- Contact your local representatives to advocate for demilitarization of police forces (suggested).
- Join your local community policing initiatives to foster a more positive relationship between law enforcement and the public (suggested).
- Give someone the book "On Killing" by Lieutenant Colonel Grossman to provide insights into the misconceptions around warrior mentality (suggested).
- Go to a community forum on police reform to discuss the need for a shift in law enforcement mindset from warriors to public servants (suggested).

# Oneliner

Law enforcement's shift towards a warrior mentality and militarization raises concerns about accountability and public safety, urging a reevaluation of policing approaches.

# Quotes

- "If your interest is no longer protecting the public and it's only enforcing the law, you're just accepting orders and taking a paycheck to do it using violence."
- "SWAT teams have the toys but lack the training to go with it, leading to dangerous situations and unnecessary fatalities."
- "There is no war on cops. There should be no warrior cops."
- "Police violence is not as dangerous as portrayed, and there is no actual war on cops."
- "The more they step away from their role as public servants and protectors of the public and more into enforcing laws with weapons, there's going to be more fatalities."

# Whats missing

Insights on how communities can actively engage with law enforcement to foster better understanding and collaboration for safer neighborhoods.

# Tags

#PoliceMilitarization #LawEnforcement #PublicSafety #CommunityPolicing #Accountability