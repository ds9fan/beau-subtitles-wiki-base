# Bits

- Bo shares a tale of two neighborhoods, one rough and impoverished, and the other relatively affluent.
- In the rough neighborhood, Bo encounters gang members who efficiently coordinate to help their community in need.
- Bo contrasts the fear-driven behavior of a man with a gun in the affluent neighborhood with the collaborative effort in the impoverished one.
- Bo reflects on the prevalence of fear in society, leading to misguided actions and a reliance on government rather than local community initiatives.
- He stresses the importance of taking personal initiative to address problems rather than waiting for government intervention.
- Bo advocates for overcoming fear and prejudices towards marginalized groups like Central American migrants.
- He underlines the value of modeling good behavior for children to instill a sense of personal responsibility and community engagement.

# On-the-ground actions from transcript

- Contact Project Hope Incorporated to inquire about volunteering or supporting their supply depot efforts. (Contact, exemplified)
- Join a local initiative or community group focused on disaster relief or community support. (Join, suggested)
- Give someone in need supplies or assistance without waiting for government intervention. (Give someone, exemplified)
- Go to a neighborhood cleanup or assistance event to support those in need directly. (Go to, exemplified)

# Oneliner

In a tale of two neighborhoods, Bo illustrates the power of community collaboration in overcoming fear and advocating for personal responsibility to address societal challenges.

# Quotes

- "We're not the sons of the pioneers and the daughters of Rosie the Riveter anymore because we're not going to fix the problem ourselves."
- "If you got a problem in your community, fix it, do it yourself."
- "A gun doesn't make you a hero. A gun doesn't make you tough."
- "We've got to stop being afraid of everything."
- "If not you, who? Who's going to solve the problems if it's not you?"

# What's missing

The emotional impact and nuances of Bo's interactions with individuals in different neighborhoods can be best understood by watching the full video.

# Tags

#CommunityEngagement #Fear #PersonalResponsibility #Collaboration #LocalInitiatives #DisasterRelief #OvercomingPrejudice #Empathy