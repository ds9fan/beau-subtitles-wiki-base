# Bits

- Asylum seekers following the law by claiming asylum cannot be punished for entering a country illegally (implied).
- The U.S. Constitution, federal law, and international treaties are the supreme law of the land.
- Those trying to claim asylum are following the law, while those trying to stop them are violating it.
- The U.S. spent half a billion dollars on the Office of Refugee Resettlement, but Trump allocated even more money for them.
- It's cheaper to be a good person than to spend on border stunts.
- Mexico offered benefits to asylum seekers, showing it's not all about money.
- U.S. involvement in Honduras led to the current asylum seeker situation.
- Individuals should take responsibility for their country's actions and work towards fixing it.
- The scale of tragedy from asylum seekers' countries is beyond U.S. comprehension.
- Internet tough guys boasting about violence lack real understanding of combat and violence.

# On-the-ground actions from transcript

- **Google the U.S. Constitution and read Article 6, Section 2** to understand the supreme law of the land (suggested).
- **Research the 1967 protocols on refugees and read Article 31** to learn about asylum seekers' rights (suggested).
- **Contact your government representatives to advocate for proper budget allocation for refugee resettlement** (suggested).
- **Join local initiatives supporting refugees and asylum seekers** (suggested).
- **Give someone in need support, whether a homeless vet or an adopted child** (suggested).

# Oneliner

Understanding the legal and humanitarian aspects of asylum seeking reveals the importance of compassion and responsibility in handling global crises.

# Quotes

- "It's cheaper to be a good person."
- "Stop projecting your greed onto them. Maybe they just want safety."
- "Accept some responsibility for your apathy and stay and fix it."
- "We have no idea how to put that in scale."
- "Die. That's it."

# What's missing

The emotional impact and personal stories behind the asylum seeker issue can be best understood by watching the full transcript.

# Tags

#AsylumSeekers #USLaw #RefugeeResettlement #Responsibility #HumanitarianAid #GlobalCrisis