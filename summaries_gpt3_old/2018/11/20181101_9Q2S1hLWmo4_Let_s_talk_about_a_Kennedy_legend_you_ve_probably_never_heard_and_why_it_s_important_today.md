# Bits

- President Kennedy allegedly met with selected Green Berets in 1962 and made them take an oath to help Americans fight back against a tyrannical government (implied).
- 46 Green Berets were sent to DC after Kennedy's assassination, with 21 serving as his honor guard (implied).
- Some Green Berets allegedly stayed with Kennedy's casket after everyone left, with Command Sergeant Major Ruddy possibly placing his beret on the casket (implied).
- A group of active-duty Green Berets still visit Kennedy's grave annually for a brief ceremony (implied).
- A conspiracy theory called the Special Forces Underground was investigated by the Department of Defense (suggested).
- The suspension of habeas corpus, mentioned by Kennedy in the legend, is linked to current political discussions and actions (generated).

# On-the-ground actions from transcript

- Contact the JFK Presidential Library for information on Command Sergeant Major Barretti's beret (implied).
- Join a group of active-duty Green Berets in their annual visit to Kennedy's grave site (implied).
- Go online and research the suspension of habeas corpus to understand its implications (implied).
- Give someone a brief on the legend of President Kennedy and the Green Berets (suggested).
- Contact your local representatives to voice your concerns about the suspension of habeas corpus (generated).

# Oneliner

President Kennedy allegedly initiated a secret oath with Green Berets to fight tyranny, linking to current discussions on habeas corpus suspension and national security.

# Quotes

- "The suspension of habeas corpus is the end of the rule of law."
- "The suspension of habeas corpus is a threat to national security and your very way of life."
- "At this point, you can support your country or you can support your president."
- "The rule of law will be erased completely. It's not hyperbole."
- "You need to realize exactly what it means for that [habeas corpus] to be gone."

# What's missing

The full context and details behind the legend, the investigation of the Special Forces Underground, and the implications of habeas corpus suspension.

# Tags

#PresidentKennedy #GreenBerets #ConspiracyTheory #HabeasCorpus #NationalSecurity #GovernmentAction