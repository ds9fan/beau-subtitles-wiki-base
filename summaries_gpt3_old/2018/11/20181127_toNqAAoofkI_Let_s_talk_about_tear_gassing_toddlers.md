# Bits

- United States is no stranger to violence, with justifications found on social media for violent acts over trivial matters. (who, what, where)
- Raises the question of how one might respond if their child was tear-gassed by someone in authority. (why, dynamics)
- Discusses the implications of lack of violence from certain groups and questions whether fear mongering has turned the nation into cowards. (why)
- Suggests that the presence of cartels in certain areas poses a significant threat and questions potential future outcomes. (what, when)
- Expresses concerns about government actions leading to a decline in poll numbers and compares behavior to that of dictatorial regimes. (who, what)
- Reflects on the possibility that lack of violence from a particular group may indicate they are better than the general population. (what, why)

# On-the-ground actions from transcript

- Contact local representatives to express concerns about violence and use of tear gas on children. (generated)
- Join local advocacy groups working to protect human rights and children's safety. (implied)
- Attend community forums or events discussing border issues and potential solutions. (implied)

# Oneliner

Bo Ginn questions the nation's response to violence, fear mongering, and tear gas on children, challenging perceptions of bravery and morality.

# Quotes

- "What are you going to do when it doesn't happen?"
- "We tear gassed kids like any other two-bit dictatorship."
- "They don't like seeing their government behave like Saddam's."
- "Good night."

# What's missing

Insights on potential actions individuals can take to address the issues raised and support the protection of children's rights.

# Tags

#Violence #TearGas #Children #BorderPatrol #FearMongering #HumanRights #Advocacy