# Bits

- Bo shares a story from the Vietnam War about humanity in the midst of conflict, where American troops offered water to a Viet Cong trooper despite being in combat. (illustrates humanity in times of war)
- He criticizes the inhuman actions of Border Patrol agents who intentionally dump out water left by volunteers for migrants crossing the border, showcasing a lack of compassion. (sheds light on the cruelty of Border Patrol agents)
- Bo condemns the Border Patrol agents for targeting vulnerable groups like the elderly, sick, and children by depriving them of essentials like water in the desert. (points out the impact on vulnerable populations)
- He argues that no physical barrier can deter people's drive for safety, freedom, and opportunities, and calls for Border Patrol agents to resign from their roles due to their actions. (discusses the futility of border barriers)

# On-the-ground actions from transcript

- Contact your local Border Patrol office and express your concern about the inhumane actions of some agents. (suggested)
- Join local volunteer groups that provide aid to migrants in need along border routes. (generated)
- Give someone in need water, food, or essentials if you encounter them in distress. (implied)
- Go to community events or protests advocating for humane treatment of migrants at the border. (suggested)

# Oneliner

Bo recounts a Vietnam War story of humanity in conflict and condemns the inhuman actions of Border Patrol agents who deprive vulnerable migrants of water, stressing that physical barriers cannot deter people's drive for safety and freedom.

# Quotes

- "You need to turn in your resignation. Just following orders isn't going to cut it."
- "Fixed fortifications or monuments of the stupidity of man."
- "Your badge is not going to protect you from that."
- "The human spirit will find a way."
- "Dumping these out in the statute what that's called is depraved indifference."

# What's missing

The emotional impact and deeper reflection on how dehumanizing actions have long-lasting consequences and the need for systemic change. 

# Tags

#Humanity #BorderPatrol #MigrantRights #VietnamWar #Injustice #Resignation