# Bits

- Bo discusses the misconceptions around revolutions and the Yellow Vest Movement.
- Not every riot is a revolution; revolutions bring new ideas and forms of government.
- Western civilization trends towards decentralizing power and giving more power to more people through revolutions.
- Bo warns that a violent revolution in the United States could lead to catastrophic consequences due to cultural, ethnic, and political divides.
- He points out the potential collapse of infrastructure and supply chains in the event of a revolution, leading to severe shortages.
- Bo advocates for focusing on changing minds with new ideas rather than violence to bring about governmental change.

# On-the-ground actions from transcript

- Contact local community organizations to discuss peaceful methods of governmental change. (suggested)
- Join local dialogue groups to exchange ideas on improving the current governmental system. (suggested)
- Give someone in your community space to share their perspectives on political issues. (implied)
- Attend public forums or town hall meetings to engage in discussions about government reform. (suggested)

# Oneliner

Bo discusses the risks and consequences of violent revolutions, advocating for new ideas over violence to bring about government change in the United States.

# Quotes

- "You can put a bullet in it, or you can put a new idea in it."
- "A revolution is bringing a new idea to the table."
- "If you're going to change the government, yeah, it can be done with bullets and lamp posts."
- "In the United States, a violent revolution will be horrible."
- "Most of the people who think that they are ready for it, you've got $15,000 worth of guns, 50,000 rounds of ammo for each one of them, you will die to a .30 bullet."

# What's missing

The emotional impact and depth of understanding that comes from listening to Bo's nuanced take on revolutions and their implications.

# Tags

#Revolution #Violence #GovernmentChange #CommunityDialogue #PoliticalActivism