# Bits

- Anti-feminist memes online sparked a discussion on patriarchy and feminism.
- Two common types of anti-feminist memes: "make me a sandwich" and "feminism makes you ugly."
- Anti-feminist memes often objectify women and belittle the feminist movement.
- The concept of patriarchy is discussed in terms of power distribution between genders in society.
- Examples of gender disparities in political representation are cited to illustrate patriarchy's existence.
- The rebuttal to gender disparities often focuses on qualifications rather than acknowledging systemic issues.
- Recognition of diversity in feminist tactics to address ongoing gender inequalities.
- Women should be free to be who and what they want, regardless of societal judgments or expectations.

# On-the-ground actions from transcript

- Contact your local feminist organization and ask how you can support their cause (suggested).
- Join a discussion group on patriarchy and feminism to deepen your understanding (suggested).
- Give someone a book on feminist theory to spark conversations and awareness (suggested).
- Attend a local event or rally that advocates for gender equality (suggested).

# Oneliner

Exploring the impact of anti-feminist memes on patriarchy, gender disparities, and the importance of women defining themselves.

# Quotes

- "All women have to be two things, and that's it. Who and what they want."
- "The fact that you don't find her attractive doesn't matter."
- "That's your hangup, not hers."

# What's missing

The full video provides a deeper dive into the nuances of anti-feminist rhetoric and its implications on gender dynamics, patriarchy, and the feminist movement.

# Tags

#Feminism #Patriarchy #GenderEquality #AntiFeminism #GenderDisparities #Activism