# Bits

- MS-13 was born as a street gang in California among Salvadoran refugees. (who, where)
- The gang grew in power rapidly in El Salvador after the Civil War ended. (where, when)
- A key figure named Satan (Ernesto Deris) transformed MS-13 into a transnational criminal enterprise through military training. (who, what)
- The US military's involvement in training MS-13 leadership significantly contributed to its evolution. (who, what)
- The School of Americas, a US government facility, played a role in training individuals linked to various atrocities in Central America. (what, where)
- American foreign policy, including a coup in El Salvador and deportations to a country with no police, fueled the rise of MS-13. (what, why)
- The speaker suggests reevaluating foreign policy rather than building a wall to address issues like MS-13. (why)
- Calls for scrutinizing individuals trained by the US military for harmful activities as a preventive measure. (why)

# On-the-ground actions from transcript

- Contact your representatives to advocate for a review of American foreign policy. (suggested)
- Join organizations pushing for transparency and accountability in military training programs. (suggested)
- Give someone information about the history of MS-13 and the role of US policies in its formation. (implied)

# Oneliner

MS-13's evolution into a transnational criminal enterprise, rooted in American foreign policy actions, challenges the narrative that a wall is the solution.

# Quotes

- "MS-13 is an American creation, not just founded here, but every step of the way, it was American foreign policy that built it."
- "Maybe a better idea is to curtail our foreign policy instead of building a wall."
- "All of these people want really strict scrutiny and very thorough vetting of people who want to come in and claim asylum."
- "You never hear them calling for strict scrutiny or vetting of those people that the U.S. military trains to murder, torture, assassinate, and dominate a community by fear."
- "Just stop the problem before it starts."

# Whats missing

The full conversation provides a deeper understanding of the historical context and the impact of American foreign policy on the emergence of MS-13.