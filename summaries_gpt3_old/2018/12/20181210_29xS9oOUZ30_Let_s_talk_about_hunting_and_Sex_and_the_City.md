# Bits

- Not all individuals from the South hunt, hunting is a stereotype (people from the South and hunting).
- Trophy hunting, especially among the wealthy, involves going after exotic animals, sometimes endangered ones.
- Wealthy individuals pay tens of thousands of dollars for canned hunts where most of the work is done for them.
- The money spent on these hunts is argued to go towards conservation efforts, but it also fuels a market for artifacts and poaching.
- Poachers linked to warlords profit from the market created by canned hunts, leading to violence.
- The speaker suggests investing the hunting money in local communities instead, promoting self-sufficiency and economic growth.
- Volunteering with anti-poaching task forces is proposed as an alternative to hunting dangerous game.
- Following Kristen Davis, a wildlife advocate, is recommended for those interested in animal conservation.
- Supporting organizations like the David Sheldrick Wildlife Trust can contribute positively to conservation efforts.

# On-the-ground actions from transcript

- Invest the hunting money in local communities instead of going on canned hunts (suggested).
- Volunteer with an anti-poaching task force while in Africa (suggested).
- Follow Kristen Davis for advocacy on animal conservation (suggested).
- Support organizations like the David Sheldrick Wildlife Trust financially (suggested).

# Oneliner

Hunting stereotypes and the dark side of trophy hunting exposed, advocating for community investment and conservation efforts over canned hunts.

# Quotes

- "Invest the hunting money in the community where you were gonna go."
- "You want to hunt dangerous game? Hunt the most dangerous animal of all."
- "That stops this from being just an advanced form of colonialism."
- "They don't want aid, they want business."
- "It's about being the great white hunter. It's about proving your masculinity."

# What's missing

In-depth understanding of the impact of canned hunts on conservation efforts and local communities.

# Tags

#Hunting #TrophyHunting #Conservation #CommunityInvestment #AnimalAdvocacy #WildlifeProtection