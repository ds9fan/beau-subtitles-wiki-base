# Bits

- Duty is a prevailing theme in recent major news, like the Florida court ruling that law enforcement has no duty to protect citizens and General Mattis's resignation due to his sense of duty.
- Mattis's honor, integrity, and sense of duty were highly praised by Marines, showcasing the Marine Corps' emphasis on these values.
- Despite disagreements, Beau surprisingly agrees with President Trump on the withdrawal from Syria, believing it's the right decision and that the U.S. should not have been there initially.
- The Kurdish people, spread across countries like Turkey, Syria, Iraq, and Iran, are seen as being abandoned by the U.S., with no homeland of their own and facing challenges in achieving independence due to geopolitical interests.
- The U.S. departure from the region might present an unexpected chance for Kurdish independence, as they'll have to act independently without U.S. influence.
- A federal court ruling affirms that law enforcement has no duty to protect individuals, even in extreme cases like mass shootings, underlining the government's control rather than protection role.

# On-the-ground actions from transcript

- Contact your local representatives to advocate for legislation that empowers citizens to protect themselves ( suggested, exemplified ).
- Join organizations or movements that support marginalized groups like the Kurdish people ( suggested ).
- Give someone in your community resources or information on self-defense and safety measures ( exemplified ).
- Go to community meetings or town halls to discuss the role of law enforcement and government in protecting citizens ( suggested ).

# Oneliner

Recent news events, from court rulings on law enforcement duties to Mattis's resignation, underscore the importance of duty and the complexities of government protection versus control.

# Quotes

- "Honor, integrity, and a sense of duty - that's the norm in the Marine Corps." 
- "Law enforcement is not there to protect you. We do not live under a protectorment. We live under a government."
- "Government is there to control you, not protect you."

# What's missing

The full transcript provides a deeper understanding of the nuances around duty, government role, and international relations.

# Tags

#Duty #Government #Protection #KurdishIndependence #LawEnforcement #CommunitySafety #MarineCorps