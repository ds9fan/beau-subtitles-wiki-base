# Bits

- Bo discusses the Dunning-Kruger effect in relation to different levels of confidence in one's knowledge and skills.
- He also explains the bystander effect and how it can hinder individuals from taking action in emergency situations due to diffusion of responsibility.
- Bo recommends owning a first aid kit or bug out bag to counteract the bystander effect and be prepared to assist in emergencies.
- He advises against purchasing first aid kits from big box stores like Walmart or Target and suggests a reputable company, Bear Paw Tac Med, for quality kits.
- Bo mentions the importance of understanding the theory behind medical procedures, even if you may not have experience performing them.
- He addresses misconceptions about the safety of using veterinarian medications for humans and strongly advises against it due to potential risks and additives that can harm humans.

# On-the-ground actions from transcript

- Contact Bear Paw Tac Med for quality first aid kits (suggested)
- Avoid purchasing first aid kits from big box stores like Walmart or Target (suggested)
- Understand the theory behind medical procedures in case you need to perform them in an emergency (suggested)
- Avoid using veterinarian medications for humans unless under extreme circumstances and with proper knowledge (suggested)
- Be prepared to take action in emergency situations rather than assuming someone else will help (suggested)

# Oneliner

Bo delves into the Dunning-Kruger effect, the bystander effect, and the importance of being prepared with quality first aid kits to counteract potential dangers and save lives.

# Quotes

- "More knowledge, much less confident."
- "If not you, who? Make sure that somebody else is helping before you just walk on by."
- "You carry what you understand the theory of because at some point it may just be you."
- "Not a lot of people know how to secure an airway."
- "The diffusion of responsibility can lead to nobody helping, kind of inoculates you against it."

# What's missing

The emotional impact and personal anecdotes shared by Bo can be best experienced by watching the full video to grasp the importance of being prepared in emergency situations.

# Tags

#FirstAid #EmergencyPreparedness #DunningKrugerEffect #BystanderEffect #VeterinarianMedications