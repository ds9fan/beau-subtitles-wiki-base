# Bits

- Internet tough guys are a symptom and a perfect analogy, insulated from consequences of their actions, much like American foreign policy.
- American foreign policy can sometimes ignore consequences due to being insulated behind the military machine.
- Many Americans can ignore or back horrible foreign policy because they don't directly suffer the repercussions.
- Injustice in the world often stems from the wealthy, and change could happen if they cared about it.
- Money plays a significant role in affecting change, as wealthier individuals have more power to make a difference.
- Mass corporations contribute to global injustice, and individuals can influence change through their purchasing habits.
- The yellow movement is discussed, with the idea that any revolution must be global in our interconnected world.

# On-the-ground actions from transcript

- Contact your local representatives to voice concerns about American foreign policy (implied).
- Join your local advocacy group to work towards holding massive corporations accountable (generated).
- Give someone a different perspective on the impact of their purchasing choices (implied).
- Go to a community event discussing global injustice and ways to address it (suggested).

# Oneliner

Internet tough guys and American foreign policy demonstrate insulation from consequences, reflecting the need for global awareness and individual actions against injustice.

# Quotes

- "Money makes the world go round, right?"
- "Every time we go to the Walmart or the grocery store or the gas station, we are telling those massive corporations what kind of world we want."
- "If you want to change, it has to start with us."

# Whats missing

The full video provides a deeper exploration of the parallels between internet tough guys and American foreign policy, offering insights into global injustice and the role of individuals in creating change.

# Tags

#InternetToughGuys #AmericanForeignPolicy #GlobalInjustice #IndividualActions #Revolution