# Bits

- Bo, a former law enforcement trainer, shared a story about police militarization and lack of proper training among officers.
- He emphasized the importance of time, distance, and cover in police work to prevent unnecessary violence.
- Bo discussed the concept of auditory exclusion and its impact on officers during high-stress situations.
- He warned against excessive use of force and stressed the need for proper training to avoid tragic outcomes.
- Positional asphyxiation was explained as a risk during arrests, urging officers to handle suspects with care.
- Wearing protective gear like vests was strongly advised to prevent impulsive actions due to fear.
- Bo shared a personal anecdote about unjust laws and the importance of understanding the spirit versus the letter of the law.
- He cautioned officers about ideologically motivated individuals and the potential dangers they pose.
- Lastly, he advised against seeking advanced training within one's department due to potential repercussions.

# On-the-ground actions from transcript

- **Contact Bo for training:** Reach out to Bo for advanced training on time, distance, cover, and other critical aspects of law enforcement. (suggested)
- **Implement time, distance, and cover:** Practice the principles of time, distance, and cover in police operations to ensure safety and prevent unnecessary harm. (implied)
- **Understand auditory exclusion:** Educate yourself on auditory exclusion and its effects during high-stress situations to improve communication and decision-making. (generated)
- **Wear protective gear:** Ensure you wear proper protective gear like vests to enhance safety and reduce impulsive actions driven by fear. (suggested)
- **Examine laws before enforcement:** When encountering individuals breaking laws without victims due to unjust legislation, evaluate the situation before taking action. (implied)

# Oneliner

Bo shares insights on police training, urging officers to prioritize safety through proper tactics, gear, and understanding the spirit of the law.

# Quotes

- "Time, distance, and cover, please start to use it."
- "The most dangerous people on the planet are true believers."
- "Wear your vest. If you are one of the extremely few cops that gets in a shootout, stay behind cover."
- "When you run across somebody that is ideologically motivated, and they are breaking the law, but the crime has no victim, keep rolling."
- "The fact that that cop exercised the spirit of the law, rather than the letter of the law, is the only reason he's alive."

# What's missing

In-depth examples and case studies could provide a deeper understanding of how applying these principles effectively impacts law enforcement practices.

# Tags

#LawEnforcement #Training #PoliceSafety #AuditoryExclusion #ProtectiveGear #TimeDistanceCover #UnderstandingLaws #IdeologicalMotivation