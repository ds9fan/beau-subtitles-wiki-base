# Bits

- Globalism is often used as a code word by some individuals to imply support for a Jewish agenda within certain communities.
- The term "globalist" can also refer to those who support a new world order with a global government and elite decision-makers.
- Nationalism operates on a smaller scale than globalism but shares similarities in motivating people through different means.
- Both globalism and nationalism can divert attention from holding those in power accountable by directing anger towards others, like foreign workers or immigrants.
- Nationalists are essentially low-ambition globalists who prefer control over their own corner of the world.
- The distinction between a Nationalist and a Globalist can be blurry, especially when those criticizing one end up embodying similar ideologies.

# On-the-ground actions from transcript

- Contact your representatives and hold them accountable for their actions (implied)
- Resist the urge to blame individuals from other countries for economic challenges and instead focus on systemic issues (generated)

# Oneliner

Globalism and nationalism both have their complexities, but at their core, they can serve as distractions from holding true power accountable while directing anger towards perceived 'others'.

# Quotes

- "All a nationalist is, is a low-ambition globalist."
- "It's just a method of motivating people through different means to kick down instead of punching up."
- "I think that there are probably better ways to run the world than in trusting the few to make decisions for everybody."

# What's missing

A deeper dive into the historical context and real-world implications of the globalism vs. nationalism debate.

# Tags

#Globalism #Nationalism #Power #Accountability #PoliticalIdeologies