# Bits

- Bo uses Elf on the Shelf to address social issues and receives backlash, comparing it to the criticism feminists face for addressing social issues in comic books.
- Bo discusses the themes of masculinity, trauma, PTSD, and feminism in comic books like Punisher, G.I. Joe, X-Men, and Superman.
- He points out the strong feminist and social issue undercurrent in G.I. Joe and how it addresses various societal topics through its characters and storylines.
- Bo analyzes how Cobra in G.I. Joe, portrayed as the antagonist, actually addresses social issues like embracing renewable energy and ending the Federal Reserve.
- He mentions how X-Men tackles civil rights issues, with mutants representing various marginalized groups, and Superman's immigrant background reflecting societal views on immigrants.
- Bo suggests that social issues have always been present in comics, but some readers may have overlooked them until they began challenging the core audience's identity.
- He reflects on the privilege some individuals may have and how losing that privilege can sometimes be perceived as oppression.
- Bo contrasts the intense reactions to his use of Elf on the Shelf with the lack of attention given to real-life social issues, indicating a disconnect in empathy and awareness.
- The story of Libertad and Esperanza is referenced as a true story, shedding light on the daily struggles faced by many individuals.

# On-the-ground actions from transcript

- Contact local comic book stores or online communities to discuss the social issues addressed in comic books like Punisher, G.I. Joe, X-Men, and Superman. (suggested)
- Join a feminist discussion group or organization to delve deeper into the feminist themes present in popular culture, including comic books. (suggested)
- Give someone a comic book featuring strong female characters to encourage conversations around feminism and representation. (implied)
- Go to a local event or convention focused on comics and social issues to engage with like-minded individuals. (suggested)

# Oneliner

Bo discusses the presence of social issues and feminism in comic books, challenging perceptions and sparking reflection on societal themes within popular culture.

# Quotes

- "Social issues have always been in comics. You were just too dense to catch it."
- "You know, if you have a privilege for so long, and you start to lose it, it feels like oppression."
- "Those two characters are now more human than the actual humans who are going through this down at the border."

# What's missing

Insights on how comics serve as a reflection of societal norms and values, offering a platform for examining complex issues in a creative and engaging manner.

# Tags

#Comics #Feminism #SocialIssues #Representation #Privilege #PopularCulture #Community #Reflection #Empathy