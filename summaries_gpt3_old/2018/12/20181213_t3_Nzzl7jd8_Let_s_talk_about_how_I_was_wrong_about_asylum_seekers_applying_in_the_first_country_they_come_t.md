# Bits

- Bo admits he was wrong about asylum seekers having to apply for asylum in the first country they come to.
- Found out the idea comes from an EU court ruling and the Safe Third Country Agreement between the US and Canada.
- The Safe Third Country Agreement only applies to the US and Canada, nowhere else.
- Bo warns against getting legal information from memes and encourages reading the full text of treaties or laws.
- Trump attempted to designate Mexico as a safe third country, which was met with ridicule globally.
- Canadians, including the Conservative Party, are trying to scrap the agreement with the US due to their treatment of asylum seekers and refugees.
- Bo stresses the importance of accurate immigration law knowledge and the dangers of widespread misinformation.

# On-the-ground actions from transcript

- Contact your local immigration lawyer for accurate legal information (suggested).
- Join your local immigration advocacy group to stay informed on immigration policies (suggested).
- Go to a legal library to read full treaties or laws for accurate understanding (suggested).

# Oneliner

Bo delves into the origins of the belief that asylum seekers must apply in their first country, debunking myths and stressing the importance of accurate legal knowledge.

# Quotes

- "Knowledge is power. Don't get your legal information from memes."
- "People that know immigration law were so dumbfounded by this widespread belief."
- "The real thing you need to really let sink in is that people were curious where the initial idea came from."
- "Trump knows this. President Trump is completely aware of this."
- "Unless you're moving along the U.S.-Canadian border. That's the only place this treaty applies."

# What's missing

Insight into the potential impacts of misinformation on asylum seekers and the importance of fact-checking before spreading legal information.

# Tags

#AsylumSeekers #ImmigrationLaw #SafeThirdCountryAgreement #Misinformation #LegalKnowledge