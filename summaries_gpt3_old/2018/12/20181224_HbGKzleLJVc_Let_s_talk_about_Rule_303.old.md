# Bits

- Rule 303 originated from the caliber of a rifle, not an actual rule, signaling the responsibility to act with the means at hand.
- The evolution of Rule 303 from military contractors to law enforcement, where it has skewed towards "might makes right."
- Examples of Rule 303 in action include the French Mirage jets scaring off enemies and military teams rescuing refugees despite no legal duty.
- The discussion on law enforcement's duty to protect after the Parkland shooting and the implications of officers not taking action.
- The philosophical aspect of legal obligation removing an officer's ability to assess risk and prioritize self-preservation.
- Addressing excuses like not being paid enough or having inferior weaponry when it comes to protecting lives.
- The message behind putting out excuses like weapon superiority and the impact on the mindset of potential school shooters.
- The reminder to those in law enforcement, especially school resource officers, about the duty to protect students and the willingness to lay down their lives if necessary.

# On-the-ground actions from transcript

- Contact your local law enforcement agencies and ask about their protocols for responding to active threats. (implied)
- Join your local community safety meetings or neighborhood watch groups to stay informed and involved in security discussions. (generated)
- Give someone in law enforcement a platform to discuss the challenges and expectations they face in protecting communities. (suggested)
- Go to a self-defense or active shooter response training to understand how to react in emergency situations. (implied)

# Oneliner

Understanding the essence of Rule 303: the responsibility to act with the means at hand, from military contractors to law enforcement, especially in contexts like the Parkland shooting.

# Quotes

- "Your duty is not bounded by risk. Rule 303: you have the means at hand, you have the responsibility to act."
- "If you cannot truly envision yourself laying down your life to protect those kids, you need to get a different assignment."
- "The gun makes the man. Don't you think that might be reinforcing the thought process of those kids that go into schools and shoot the place up?"

# Whats missing

The full video will provide a deeper insight into the examples and nuances of Rule 303 and the discussion around law enforcement's duty to protect in critical situations.