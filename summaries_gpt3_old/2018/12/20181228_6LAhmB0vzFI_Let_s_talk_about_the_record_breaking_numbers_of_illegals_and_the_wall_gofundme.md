# Bits

- Border Patrol uses border apprehensions as a gauge for the number of illegal immigrants coming into the country. (explained)
- The number of illegal border crossings is at a 40-year low historically. (stated)
- Trump's campaign on tough policies towards illegal immigrants didn't significantly alter the numbers. (argued)
- The push for the border wall is based on fear-mongering rather than actual need. (claimed)
- The idea of crowdfunding for the wall reflects a lack of critical thinking in the population. (pointed out)
- Attempting to influence politicians through private donations can lead to worrisome fascist tendencies. (warned)
- The wall may inadvertently strengthen criminal enterprises like cartels and make illegal immigration easier. (analyzed)
- Fear tactics and lack of critical thinking are driving forces behind the push for the wall. (discussed)

# On-the-ground actions from transcript

- Contact your representatives to express opposition to funding the border wall. (implied)
- Join local activism groups advocating for sensible immigration policies. (suggested)
- Give someone a civics lesson or encourage them to take one. (generated)
- Go to community events or forums to discuss the implications of border policies. (implied)

# Oneliner

Border wall funding is driven by fear rather than real need, reflecting a lack of critical thinking in immigration policy decisions and potentially empowering criminal enterprises.

# Quotes

- "We are people who elect rulers instead of leaders."
- "Fascism doesn't actually mean government by violence."
- "The wall, assuming it works, will actually pump more money into a criminal cartel."
- "Nobody in DC is going to look out for you."
- "How many people right now terrified of MS-13 coming to the United States? Probably the same number of people that don't know it was started in the United States."

# What's missing

The detailed breakdown and analysis of the impacts of border policies on neighboring countries and the economy are best understood through watching the full transcript.

# Tags

#Immigration #BorderWall #CriticalThinking #Fascism #FearMongering #PolicyInsights