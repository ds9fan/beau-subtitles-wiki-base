# Bits

- Rule 303 originated from the caliber of a rifle, not an actual rule, signaling the responsibility to act with the means at hand.
- The evolution of Rule 303 from military contractors to law enforcement, where it has skewed towards "might makes right."
- Examples of Rule 303 in action include the ambush of green berets in Western Africa and the movie "Tears of the Sun."
- Discussion on law enforcement's duty to protect in light of the Parkland shooting and the ruling that officers have no obligation to intervene.
- The philosophical debate around officers' duty to act regardless of personal risk and the profession's focus on protecting others over self-preservation.
- Addressing common excuses for not acting in critical situations, such as inadequate pay, the presence of a weapon, or fear of engaging.

# On-the-ground actions from transcript

- Contact your local community policing department to understand their protocols and responsibilities (suggested)
- Join your local school safety committee to advocate for adequate protection measures (suggested)
- Give someone in law enforcement feedback on the importance of prioritizing protection over personal safety (implied)

# Oneliner

Rule 303: The responsibility to act with the means at hand, a vital principle for military contractors and law enforcement in protecting others despite personal risk.

# Quotes

- "Your duty is not bounded by risk. Your duty is not to get up and go to work every day." 
- "If you cannot truly envision yourself laying down your life to protect those kids, you need to get a different assignment."
- "The gun makes the man. Don't you think that might be reinforcing the thought process of those kids that go into schools and shoot the place up?"

# What's missing

The full conversation provides a deep dive into the ethical responsibilities of those in positions of protection and the complexities of duty in high-risk situations.

# Tags

#Rule303 #Responsibility #Protection #LawEnforcement #Duty