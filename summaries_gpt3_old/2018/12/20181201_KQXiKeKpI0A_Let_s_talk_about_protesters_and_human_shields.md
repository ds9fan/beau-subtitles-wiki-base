# Bits

- Bo discusses protest and the use of human shields in the U.S.
- Protesters are upset with the government for not upholding laws and rights.
- The protesters face militarized opposition who open fire on them.
- Bo references the Boston Massacre as a historical example of government violence against protesters.
- Bo criticizes the use of human shields and questions the morality of such actions.
- He condemns the violence of opening fire on unarmed crowds.
- Bo expresses disappointment in how current events betray American values.
- The discussion touches on the importance of honor and integrity in American actions.
- Bo challenges those who support actions against protesters to reflect on their stance.
- The conversation ends with a reflection on the state of America and its deviation from its founding principles.

# On-the-ground actions from transcript

- Contact local advocacy groups to support protesters' rights (implied)
- Join peaceful protest movements in your area (implied)
- Give someone a platform to share their experiences with protests (suggested)
- Go to a community meeting to discuss human rights and civil liberties (implied)

# Oneliner

Bo discusses protest, human shields, and government violence, questioning the morality of current actions against protesters in the U.S.

# Quotes

- "You do not punish the child for the sins of the father."
- "Betraying everything that this country stood for, at least pretended to stand for."
- "That's America. That's how we make America great, betraying everything that this country stood for."
- "You do not open fire on unarmed crowds."
- "Y'all have a good day."

# What's missing

A deeper dive into historical and international perspectives on the use of human shields could be best understood by watching the full conversation.

# Tags

#Protest #HumanShield #CivilRights #GovernmentViolence #AmericanValues