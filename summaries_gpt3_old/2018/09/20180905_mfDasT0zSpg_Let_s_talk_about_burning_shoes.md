# Bits

- Bo discusses the idea of burning Nikes as a form of protest against the company.
- He challenges the notion of symbolic protests and encourages more meaningful actions.
- Bo questions the true motivations behind burning products and suggests donating them instead.
- He criticizes those who focus on symbols rather than addressing real issues like sweatshops and slave labor.
- Bo points out the irony of being willing to disassociate with a company over a commercial but ignoring deeper ethical issues.
- The importance of understanding and valuing true freedom is emphasized throughout the monologue.

# On-the-ground actions from transcript

- Donate unwanted items to a shelter or homeless vet (suggested)
- Drop off items at a thrift store near military bases for those in need (suggested)

# Oneliner

Bo challenges the symbolism of burning Nikes, urging for meaningful actions to address real issues like sweatshops and slave labor, questioning the true essence of freedom.

# Quotes

- "You're loving that symbol of freedom more than you love freedom."
- "Maybe give them to a homeless vet, you know, those people you pretend you care about."
- "It's a symbol. You're just mad at them."

# Whats missing

Bo's engaging and thought-provoking delivery and the nuances of his commentary can be best experienced by watching the full video.

# Tags

#Protest #Symbolism #EthicalActions #Freedom #SocialResponsibility