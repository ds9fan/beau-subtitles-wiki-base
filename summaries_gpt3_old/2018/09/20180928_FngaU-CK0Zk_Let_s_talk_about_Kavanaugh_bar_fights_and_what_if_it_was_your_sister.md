# Bits

- Bo shares a story about intervening when a girl was being harassed at a bar, posing as her brother to get the guy to back off ( exemplified )
- Bo makes a point about the importance of respecting women's autonomy and property rights over them ( generated )
- Bo discusses the relevance of his story to the Kavanaugh hearing and the importance of empathy towards survivors of sexual assault ( exemplified )
- Bo uses a math exercise to illustrate the prevalence of sexual assault among women and the impact of victim-blaming comments ( suggested )
- Bo contrasts societal reactions to male and female survivors of sexual assault and criticizes the lack of empathy towards survivors ( exemplified )
- Bo points out the power dynamics of sexual assault being about control rather than just sex ( exemplified )
- Bo challenges individuals to reflect on their attitudes towards sexual assault survivors and the impact of their words on real-life relationships ( suggested )
  
# On-the-ground actions from transcript

- Contact your friends and have a conversation about the prevalence of sexual assault and the impact of victim-blaming comments ( suggested )
- Join local initiatives or organizations that support survivors of sexual assault and work towards creating a safe environment for them ( suggested )
- Give someone who has experienced sexual assault your support and understanding without judgment ( exemplified )
- Go to events or workshops that educate about consent, respect, and empathy towards survivors of sexual assault ( suggested )
  
# Oneliner

Bo shares a personal story to illustrate the importance of respecting women's autonomy and calls for empathy towards survivors of sexual assault, challenging individuals to reflect on their attitudes and words. #RespectWomen #EmpathyMatters

# Quotes

- "The real reason is that he respected my property rights over her than he respected hers as a person."
- "You, if you made those kind of comments, knowing nothing about what happened, you're the reason they don't come forward."
- "You're altering real-life relationships over bumper sticker politics by proclaiming you know what happened when you don't."
- "It's about power and control."
- "How many of them do you know? How many of them talked about it with you? I don't think any of them will now because they're not gonna trust you."

# Whats missing

The emotional impact and depth of understanding gained from listening to Bo's personal experiences and reflections on sexual assault and empathy towards survivors.