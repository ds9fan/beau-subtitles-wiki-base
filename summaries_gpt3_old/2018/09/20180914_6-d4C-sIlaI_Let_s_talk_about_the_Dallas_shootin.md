# Bits

- Bo discusses the shooting in Dallas and his experience with liars, mentioning there are two kinds - bad liars and prepared liars.
- He analyzes the case of a police officer who entered the wrong apartment and killed the occupant, suggesting there might have been a motive due to past noise complaints against the victim.
- Bo questions the legality of the officer's actions, pointing out that even though she was a cop, entering someone's home unlawfully is considered a home invasion.
- Mention of the potential capital murder charge for killing someone during a home invasion in Texas, which could result in execution.
- Bo shares a thought-provoking moment from a conversation with Kevin Crosby about Black Lives Matter, questioning if black lives truly matter based on the justice they receive.
- He expresses concern about the police searching the victim's home after the shooting, insinuating potential corruption and lack of accountability within the Dallas PD.

# On-the-ground actions from transcript

- Contact a local advocacy group to support justice for victims of police violence (suggested).
- Join a community organization working towards police reform and accountability (suggested).
- Give support to organizations advocating for fair treatment and justice for all individuals (suggested).

# Oneliner

Bo discusses the shooting in Dallas, questioning the officer's actions, potential motives, and the system's handling of the case, urging for justice and accountability in the face of police violence.

# Quotes

- "There's two kinds of liars in the world. There's a bad liar, and a bad liar you gotta ask him that question over and over and over again to finally get to the truth."
- "When she resorted to violence, assaulted him, it's a home invasion."
- "If this is the amount of justice they get, they don't. They don't."
- "Not one cop has crossed the thin blue line to say that's wrong. Not one."
- "It's not a few bad apples you know and you can just throw those away and everything will be okay. It's a few bad apples spoils the bunch."

# Whats missing

A deeper understanding of the nuances of police accountability and potential systemic issues in law enforcement.