# Bits

- Bo addresses the conversation around sexual harassment and assault in light of the Kavanaugh nomination, providing advice on how to avoid such situations.
- He stresses the importance of safeguarding one's reputation to attract the right kind of people.
- Bo advises ending dates at the front door and avoiding taking anyone home, especially if drinking is involved.
- He warns about the implications of the signals sent through words and actions, urging to be cautious with both.
- Bo directs his advice towards men, acknowledging the fear around false accusations but also stressing the need for self-accountability.
- He challenges the notion that clothing or behavior is a cause of rape, asserting that the only cause is the rapist themselves.

# On-the-ground actions from transcript

- Safeguard your reputation to attract the right kind of people. (advice)
- End dates at the front door, especially in the initial stages. (advice)
- Avoid taking anyone home or going home with someone when drinking. (advice)
- Watch what you say and be cautious with your words, especially when it comes to anything sexual. (advice)
- Be mindful of the signals you send, keeping your hands to yourself to avoid any misinterpretations. (advice)
- Stress self-accountability and avoid situations where false accusations could arise. (advice)

# Oneliner

Bo gives advice on avoiding sexual harassment situations, stressing the importance of reputation, caution with words and actions, and self-accountability.

# Quotes

- "If you leave your bicycle in front of the meth head's house and don't put a lock on it, you can't really be surprised when it's not there in the morning."
- "Be very careful with the signals you send. The easiest way to do that is keep your hands to yourself."
- "Those type of accusations, they turn out to be false about 2% of the time, as near as I can tell. They're pretty much always true."
- "Gentlemen there is one cause of rape, and that's rapists."

# Whats missing

Viewers can gain a deeper understanding of personal accountability and how to navigate situations to avoid sexual harassment by watching the full conversation.

# Tags

#SexualHarassment #Avoidance #Accountability #SelfCare #Advice #BoTalks