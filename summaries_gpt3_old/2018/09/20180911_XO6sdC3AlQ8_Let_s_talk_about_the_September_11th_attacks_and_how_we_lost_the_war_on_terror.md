# Bits

- Bo reflects on the events of September 11th, the aftermath, and the erosion of freedom post-9/11.
- Post 9/11, government actions led to a loss of rights and freedoms, affecting daily life and society.
- Terrorism is strategic, aimed at provoking overreaction and eroding freedoms.
- Bo urges people to resist the normalization of surveillance and militarization, especially in communities.
- Actions at the community level, like teaching children about freedom, self-reliance, and counter-economics, are proposed.
- Building strong communities can counter government overreach and protect freedoms.

# On-the-ground actions from transcript

- Teach children about freedom and the importance of resisting the normalization of surveillance and militarization. (suggested)
- Prepare for natural disasters and become self-reliant to reduce dependence on government. (suggested)
- Engage in counter-economics like bartering, cryptocurrency, or supporting local markets to reduce government influence. (suggested)
- Surround yourself with like-minded individuals who support freedom and community resilience. (suggested)
- Support and assist individuals who have been marginalized or impacted by loss of freedom. (suggested)
- Engage in conversations both online and in real life to raise awareness about lost freedoms and community building. (suggested)

# Oneliner

Reflecting on 9/11's aftermath, Bo urges community-level actions like teaching children about freedom, self-reliance, and engaging in counter-economics to resist government overreach.

# Quotes

- "Among the casualties of 9/11, there's one casualty that never gets listed, never gets named, it's not on any memorial, but it was mortally wounded that day."
- "We can't let the idea of soldiers on street corners and constant surveillance become normal. We have to fight it."
- "Defeat an overbearing government by ignoring it; build strong communities that can resist external influence."
- "Surround yourself with others who support freedom; support marginalized individuals impacted by loss of freedom."
- "The U.S. Army Special Forces are feared not for killing but for teaching; be a force multiplier in your community."

# Whats missing

Full context and depth of Bo's personal reflections on the impact of 9/11 and the erosion of freedoms post-attack.

# Tags

#September11 #Freedom #CommunityResilience #CounterEconomics #TeachChildren #SelfReliance