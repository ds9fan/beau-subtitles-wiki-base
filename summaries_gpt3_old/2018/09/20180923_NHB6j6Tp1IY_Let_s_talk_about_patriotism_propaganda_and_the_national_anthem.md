# Bits

- Bo responds to a story about a woman being attacked for taking a knee during the national anthem.
- He questions the author's lecture on patriotism and the purpose of protests.
- Bo criticizes the lack of understanding shown by the spokesman for Law Enforcement and questions his competence.
- Bo argues that patriotism is about correcting the government when it's wrong and defending constitutionally protected rights.
- He shares a story about patriotism being displayed through actions rather than symbols.
- Bo distinguishes between nationalism and patriotism and defends the woman's actions as constitutionally protected.
- He offers to connect the spokesman for Law Enforcement with someone who served in the military for insights on patriotism.

# On-the-ground actions from transcript

- Contact Bo to get connected with the military veteran who can provide insights on patriotism. ( suggested )
- Understand the difference between nationalism and patriotism. ( exemplified )
- Defend constitutionally protected rights and correct the government when it's wrong. ( exemplified )

# Oneliner

Bo challenges misconceptions about patriotism and defends constitutionally protected rights in response to a woman being attacked for taking a knee during the national anthem.

# Quotes

- "Patriotism is a lot more than standing and looking at the flag."
- "Patriotism is correcting your government when it is wrong."
- "Patriotism was displayed through actions, not worship of symbols."
- "There is a very marked difference between nationalism and patriotism."
- "You can kiss my patriotic behind, okay?"

# What's missing

The passion and conviction behind Bo's defense of constitutionally protected rights and his call to understand the true meaning of patriotism can be felt more by watching the full video.

# Tags

#Patriotism #Constitution #Protests #NationalAnthem #Misconceptions #MilitaryVeteran