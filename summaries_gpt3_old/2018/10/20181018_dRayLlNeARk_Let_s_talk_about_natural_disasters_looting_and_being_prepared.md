# Bits

- Bo discusses the aftermath of Hurricane Michael and the impact on his community, stressing the importance of individual preparation for natural disasters.
- Law enforcement responses to looting during disasters often hinder volunteer relief efforts by shutting down areas, preventing much-needed help from reaching affected communities.
- Bo criticizes the prioritization of protecting corporate inventory over saving lives during disasters, questioning the decision-making process behind it.
- He praises the quick response and aid provided by the Florida National Guard in the wake of the hurricane, acknowledging their critical role in assisting those in need.
- Bo underscores the necessity for individuals and families to create emergency kits with essentials like food, water, shelter, medical supplies, and a knife, urging preparedness for unexpected situations.

# On-the-ground actions from transcript

- Contact local organizations to inquire about volunteering opportunities during natural disasters (implied).
- Join community disaster response teams to provide aid and support during emergencies (implied).
- Give someone in need access to emergency supplies such as food, water, and medical necessities (implied).
- Go to your local store and gather supplies to create an emergency kit for your household (implied).

# Oneliner

Bo reflects on the aftermath of Hurricane Michael, criticizes law enforcement responses to looting during disasters, praises the Florida National Guard's aid efforts, and urges individuals to prepare emergency kits for unforeseen situations.

# Quotes

- "In the event of a natural disaster, protecting the inventory of some large corporation should pretty much be at the bottom of any law enforcement agencies list."
- "You can't count on government response. In fact, some of the things that your government agencies may do will make it worse for you."
- "Putting together a kit, a bag, for emergencies is something that every family should do."
- "It's not a big process. We're not talking about building a bunker and preparing for doomsday."
- "You need food, water, fire shelter, medical supplies, and a knife."

# What's missing

Bo's personal experiences and insights provide a valuable perspective on the challenges faced during and after a natural disaster, shedding light on the importance of individual preparedness and community support.

# Tags

#HurricaneMichael #NaturalDisasters #EmergencyPreparedness #CommunitySupport #Volunteering #FloridaNationalGuard