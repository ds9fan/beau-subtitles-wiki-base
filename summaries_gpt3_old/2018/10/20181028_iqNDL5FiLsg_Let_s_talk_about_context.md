# Bits

- Kennedy's speech is often reduced to sound bites that are taken out of context and misinterpreted, leading to conspiracy theories about his assassination.
- Context is vital in understanding speeches and statements, as isolated sound bites can distort the intended message.
- Social media and 24-hour news cycles have shortened attention spans, promoting the spread of sound bites without context.
- Kennedy's speech about a conspiracy actually referred to the Soviet Union, showcasing the importance of understanding the full context.
- The U.S. and the U.K. involvement in starting the Syrian Civil War was driven by geopolitical interests and not the reasons commonly portrayed in sound bites.
- Propaganda thrives on sound bites, and it's critical to look beyond them to grasp the bigger picture.

# On-the-ground actions from transcript

- Contact your local journalists, editors, and publishers to encourage responsible reporting and self-censorship (suggested).
- Join a community group focused on media literacy and critical thinking skills (suggested).
- Go to a workshop or seminar on analyzing propaganda and media manipulation (suggested).

# Oneliner

Understanding the full context behind speeches and statements is vital in a world dominated by out-of-context sound bites, as demonstrated by Kennedy's speech and the Syrian Civil War case.

# Quotes

- "Context, it's really                                                                                  — Beau"
- "If you want to understand what's going on in the world, you have to look beyond the sound bites." — Beau
- "Propaganda is coming into a golden age right now. It's infectious." — Beau

# Whats missing

Deeper insights into how media manipulation through sound bites impacts public perception and geopolitical decisions.

# Tags

#Context #MediaLiteracy #Propaganda #SoundBites #KennedySpeech #SyrianCivilWar