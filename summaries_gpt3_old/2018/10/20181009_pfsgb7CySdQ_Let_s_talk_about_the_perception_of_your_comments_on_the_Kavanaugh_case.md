# Bits

- Young lady received private message about father's comments on Ford Cavanaugh case, causing her distress and longing for different perspective (examples: relationship damage, lack of credibility).
- One out of five women have experienced sexual assault; men need to understand impact of their comments and justifications on women in their lives.
- Urges men to reflect on their justifications for sexual assault and have honest conversations with women in their lives instead of trying to justify harmful comments.
- Encourages men to apologize for past harmful comments and actions regarding sexual assault, acknowledging the impact on relationships.

# On-the-ground actions from transcript

- Have a conversation with the women in your life to understand their perspectives and experiences (suggested).
- Reflect on past comments and justifications related to sexual assault, and apologize for any harmful remarks (suggested).
- Take responsibility for the impact of your words and actions on relationships with women (implied).

# Oneliner

Men urged to reflect on their justifications for sexual assault comments, have conversations with women in their lives, and apologize for past harmful remarks to salvage relationships and prevent further harm.

# Quotes

- "You don't get to rape her."
- "Not credible."
- "You've given your sons a giant list of ways to get away with sexually assaulting people."
- "You need to have that talk."
- "Just apologize."

# What's missing

The full video provides a deep dive into the impact of men's comments and justifications for sexual assault, urging introspection and action to improve relationships and prevent harm.

# Tags

#SexualAssault #ToxicMasculinity #Relationships #Apology #GenderRoles