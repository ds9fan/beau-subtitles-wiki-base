# Bits

- Beau addresses a viewer's question about purchasing a gun following an incident at a synagogue.
- Beau discusses the sexism that can be present in gun recommendations for women.
- He explains the subtext behind the advice to choose a revolver or a shotgun for ease of use.
- Beau stresses the importance of firearm training and understanding what you're dealing with.
- He advises choosing a firearm based on individual needs, environment, and purpose.
- Beau compares selecting a firearm to choosing shoes for specific purposes.
- Training with the firearm is emphasized, including firing different positions and scenarios.
- Beau underscores the importance of training to kill, not just for sport.
- He discusses tactics for engaging in a firefight, focusing on speed, surprise, and violence of action.
- Beau gives advice on home defense strategies, like staying behind cover and protecting loved ones.
- He recommends using military firearms for simplicity and reliability in high-stress situations.

# On-the-ground actions from transcript

- Find someone with firearm experience to try out different firearms at a range (suggested)
- Train with the chosen firearm in various positions and scenarios (suggested)
- Practice shooting at paper plates to simulate human chest and head targets (suggested)
- Learn firearm maintenance, disassembly, and cleaning (suggested)
- Seek a range that allows training in realistic scenarios (suggested)

# Oneliner

Beau provides practical advice on choosing, training with, and using firearms for personal protection, focusing on preparation and responsibility.

# Quotes

- "A revolver doesn't have a safety. The subtext of that is, you don't have to train because it's easy to use." 
- "Each firearm is designed with a specific purpose in mind. Your purpose is to kill a person."
- "When you're actually engaging somebody, anybody worth shooting once is worth shooting twice."
- "You're talking about combat, there's no honor in combat, there's no playing fair, you win or you die."
- "Never leave cover to engage somebody in the open. There is never a reason to do that."

# What's missing

The full video provides detailed insights on firearm selection, training, and tactics for personal protection in high-stress situations.

# Tags

#FirearmSafety #SelfDefense #Training #Responsibility #GenderBias #TacticalAdvice #HomeDefense