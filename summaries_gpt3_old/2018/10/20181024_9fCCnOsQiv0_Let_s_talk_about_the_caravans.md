# Bits

- Caravans have been coming for 15 years, not a new phenomenon, but the first president to pick on them.
- Caravans will continue as long as drug war policy remains unchanged and US intervention in other countries continues.
- The current caravan is from Honduras, a country with high violence and murder rates pushing people to flee.
- Tax dollars, specifically half a billion dollars a year, are used to help refugees resettle in the US.
- It's cheaper and more humane to help refugees get on their feet rather than detain them.
- Safety in numbers explains why refugees travel in large groups.
- Carrying other countries' flags and wearing military fatigues is practical and not conspiratorial.
- Claiming asylum is a legal process that must be done at a port of entry or within the country, as per US law.
- Blaming refugees for seeking asylum overlooks the impact of US foreign policy and drug war on their countries.
- Sending refugees back to dangerous situations contradicts the principles of freedom and humanity.

# On-the-ground actions from transcript

- Contact your representative in DC to discuss how US foreign policy impacts other countries (implied)
- Call your representative to address the impact of the drug war on gangs in countries like Honduras (implied)
- Support policies that help refugees resettle and integrate into US society (implied)

# Oneliner

Caravans have been coming for 15 years, driven by violence and US policy impacts; help refugees integrate instead of blaming them.

# Quotes

- "It's cheaper to be a good person."
- "They are doing it legally. That's how the law works."
- "You can't preach freedom and then deny it, send people back to their death for no good reason."
- "Blaming the victim."
- "These people will die. Anyway, they will die."

# What's missing

A deeper understanding of the historical context and impact of US interventions on countries like Honduras.

# Tags

#Caravans #Refugees #USForeignPolicy #AsylumSeekers #Humanity #Integration #SafetyInNumbers #BlameGame #LegalProcess