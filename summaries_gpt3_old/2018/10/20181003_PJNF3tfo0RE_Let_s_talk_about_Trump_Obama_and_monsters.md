# Bits

- President of the United States mocked sexual assault survivors on television, setting a shockingly low bar.
- There is a concerted effort in the country to silence sexual assault survivors, led by the President.
- Reflects on the dangerous thought of portraying sexual assault perpetrators as monsters, rather than people.
- Draws parallels between calling Nazis monsters and calling sexual assault predators monsters, stressing the importance of recognizing them as people.
- Urges to speak up against evil, drawing a chilling comparison to how ordinary people enabled atrocities in history.

# On-the-ground actions from transcript

- Contact your local representative to advocate for policies that support sexual assault survivors. (implied)
- Join a community organization that focuses on supporting survivors of sexual assault. (implied)
- Go to a local event or workshop on consent and healthy relationships. (generated)

# Oneliner

President mocking sexual assault survivors sets a dangerously low bar, urging the importance of recognizing perpetrators as people, not monsters.

# Quotes

- "The scariest Nazi wasn't a monster. He was your neighbor."
- "Scariest rapist or rape-apologist, well they're your neighbor too."
- "There is a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim."
- "They are just people. Just people."

# What's missing

The emotional impact and depth of the message can be best understood by watching the full video.

# Tags

#SexualAssault #President #Nazis #Silence #Advocacy #CommunitySupport