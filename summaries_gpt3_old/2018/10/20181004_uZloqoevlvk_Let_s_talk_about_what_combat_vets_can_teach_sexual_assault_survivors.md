# Bits

- Bo acknowledges the difficulty of responding to personal messages he has received, indicating a sense of responsibility towards his audience. (acknowledgment - exemplified)
- Bo discusses the challenges combat veterans face in relationships, citing detachment, fear of intimacy, and emotional issues. (discussion - exemplified)
- Combat veterans and sexual assault survivors experience similar traumatic stress and emotional responses, leading to unfair stigmas. (comparison - exemplified)
- Bo calls out the stigma around dating sexual assault survivors, questioning why there is no similar stigma around dating combat veterans. (challenge - exemplified)
- Bo dispels the notion of survivors being "damaged goods" and unworthy of love, noting that everyone carries some form of trauma. (empowerment - exemplified)
- Bo shares a story about a combat veteran's experience, illustrating the impact of trauma on memory and emotional responses. (narrative - exemplified)

# On-the-ground actions from transcript

- Reach out to support individuals sharing personal stories, showing empathy and understanding. (contact - exemplified)
- Challenge stigmas and stereotypes surrounding survivors of sexual assault by promoting understanding and empathy. (challenge - exemplified)
- Support survivors and combat veterans in relationships by fostering open communication and empathy. (support - exemplified)

# Oneliner

Bo discusses the parallels between the experiences of combat veterans and sexual assault survivors, challenging stigmas and promoting empathy and understanding.

# Quotes

- "You are not unclean. You are not damaged goods. You are certainly not unworthy of being loved."
- "We are all damaged goods in some way."
- "It may not be as severe as a combat vet or a survivor of sexual assault, everybody's broke in some way."
- "There's nothing wrong with you."
- "It is amazing what trauma can do to your memory."

# Whats missing

Insight into ways to support individuals dealing with trauma in relationships and combat situations.

# Tags

#Empathy #CombatVeterans #SexualAssaultSurvivors #Stigma #Trauma #Relationships