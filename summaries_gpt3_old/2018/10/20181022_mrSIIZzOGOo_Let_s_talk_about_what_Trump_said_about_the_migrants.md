# Bits

- The President announced cutting off aid to Central American countries for not stopping people from coming to the US, which is scary as it implies a duty to keep people in the country.
- The wall along the southern border could be used to keep Americans in, not just keep others out, signaling a potential loss of freedom.
- The slow creep of tyranny is concerning, with subtle signs of authoritarianism starting to show.
- Supporting restrictive measures like the wall could mean compromising on ideals of freedom and liberty.
- The wall is seen as a prison wall rather than just a border wall, symbolizing restrictions on movement.

# On-the-ground actions from transcript

- Contact your representatives to express concerns about the implications of cutting off aid to countries in Central America. (implied)
- Join local advocacy groups working on immigration issues to support fair and humane policies. (implied)
- Give someone information about the potential consequences of policies that restrict movement and freedoms. (suggested)

# Oneliner

The President's decision to cut off aid to Central American countries reveals a concerning duty to keep people in the US, with the border wall symbolizing potential restrictions on freedom that could affect Americans too.

# Quotes

- "That wall is a prison wall. It's gonna work both ways."
- "If you believe in the ideals of freedom and liberty, you can't if you've chosen to you've sold out your ideals and your principles and your country for a red hat and a cute slogan."

# Whats missing

The full video provides a deeper insight into the implications of restrictive policies and the importance of defending ideals of freedom and liberty in the face of potential authoritarianism.

# Tags

#USPolitics #CentralAmericaAid #BorderWall #Freedom #Tyranny