# Bits

- Bump stocks can make a semi-automatic rifle mimic fully automatic fire, significantly increasing the rate of fire and making the weapon seem like a machine gun.
- Fully automatic fire is highly inaccurate, with shooters able to control only three to five rounds at a time while staying on target.
- The most well-known use of a bump stock in a mass shooting was in Vegas, where its use likely resulted in more injuries than deaths due to the inaccurate firing.
- In mass shootings that occur in enclosed spaces at close range, the ability to fire rapidly can make the shooter more lethal without necessarily saving lives.
- The debate over the necessity of bump stocks under the Second Amendment's intent divides gun enthusiasts into two groups: those who see it as a necessary tool for potential government resistance and those who believe it is not needed for insurgency tactics.
- While some argue against banning bump stocks on ideological grounds, the practical implications and potential risks associated with their ownership raise concerns about who might possess them.
- Bump stock ownership is often seen as a red flag among shooters, with many considering individuals who own them as people they may not want to associate with.

# On-the-ground actions from transcript

- Contact your local representatives to express your views on the banning of bump stocks. (implied)
- Join your local gun control advocacy group to participate in discussions on regulating firearm accessories like bump stocks. (implied)
- Give someone information about how bump stocks can increase the lethality of shooters in mass shooting situations. (implied)

# Oneliner

Debate on bump stocks: they increase firing rates but reduce accuracy, dividing opinions on necessity and raising concerns about ownership implications.

# Quotes

- "Bump stocks can make a semi-automatic rifle mimic fully automatic fire."
- "In mass shootings that occur in enclosed spaces, rapid firing can make the shooter more lethal."
- "Bump stock ownership may raise concerns about who might possess them."

# Whats missing

Insights from watching the full transcript include a deeper understanding of the nuances of the gun control debate, particularly around the practical implications of firearm accessories like bump stocks.

# Tags

#GunControl #BumpStocks #SecondAmendment #MassShootings #Accuracy