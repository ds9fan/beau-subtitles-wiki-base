# Bits

- Bo addresses the issue of offensive Halloween costumes, such as blackface, sexy Pocahontas, Trayvon Martin, and Adolf Hitler. (introduced)
- Bo points out that being offensive and edgy is not a substitute for having a sense of humor. (explained)
- He criticizes individuals who intentionally do offensive things and then act surprised when there is backlash or consequences. (analyzed)
- Bo reflects on the past when offensive jokes were more common, like making fun of the slow kid or tormenting the gay kid. (reflected)
- He discusses the impact of the internet and social media on people's behavior and attitudes towards being edgy. (examined)
- Bo encourages people to be shocking by being good people instead and suggests that normal people do not want to associate with those who engage in offensive behavior. (proposed)
- He addresses the idea that political correctness is about changing flawed ways of thinking, particularly regarding bigotry. (argued)
- Bo warns that actions have consequences, and people will not easily forgive or overlook offensive behavior, especially in today's cultural climate. (stressed)

# On-the-ground actions from transcript

- Contact your local school or workplace to report instances of offensive costumes or behavior. (suggested)
- Join local community efforts that combat bigotry and offensive behavior. (suggested)
- Give someone feedback if they engage in offensive jokes or behavior. (implied)
- Go to events or discussions that address social issues and encourage respectful behavior. (generated)

# Oneliner

Bo addresses the issue of offensive Halloween costumes and warns against the consequences of engaging in offensive behavior, stressing the importance of being a good person over being edgy.

# Quotes

- "Being offensive and edgy is not actually a substitute for having a sense of humor."
- "You can do whatever you want. But the rest of us are going to look down on you."
- "Bigotry is out. It's something that sane people have given up on."
- "Nobody's going back to that. Nobody's going to think that's OK."
- "You can do what you want, it's just a joke. But you can't be mad when you suffer the consequences of your actions."

# What's missing

The tone and passion of Bo's delivery, along with the personal anecdotes shared, can be best experienced by watching the full video.

# Tags

#HalloweenCostumes #OffensiveBehavior #Consequences #Bigotry #SocialMedia #CulturalChange #RespectfulBehavior