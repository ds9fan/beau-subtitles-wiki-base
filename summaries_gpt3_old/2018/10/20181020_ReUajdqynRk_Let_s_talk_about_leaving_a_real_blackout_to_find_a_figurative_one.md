# Bits

- Bo expresses concern over Facebook and Twitter censoring independent news outlets opposed to government policies.
- He points out the difference between outlets being censored now and Alex Jones' situation, stressing that the former did not advocate harmful actions like the latter.
- Bo mentions specific outlets affected by the censorship, such as Anti-Media and Free Thought Project.
- He suggests following journalist Carrie Wedler, who was personally affected by the ban.
- Bo warns that Facebook and Twitter are no longer platforms for free discussion and advises finding alternative social media networks like Steemit and MeWe.
- He underscores the importance of supporting independent news outlets and moving away from platforms that censor them.

# On-the-ground actions from transcript

- Contact Carrie Wedler and show support by following her work. (suggested)
- Join alternative social media networks like Steemit and MeWe to access information from independent news outlets. (suggested)
- Give someone a heads-up about the censorship of independent news outlets on Facebook and Twitter. (implied)

# Oneliner

Bo raises concerns about the censorship of independent news outlets by Facebook and Twitter, urging users to shift to alternative platforms for unrestricted information access.

# Quotes

- "Don't compare them to Alex Jones. Even the worst among them wasn't that level, not even close."
- "Facebook and Twitter are no longer free discussion platforms."
- "This is going to be the end of free discussion and free discourse on Facebook."

# What's missing

The emotional impact and personal stories behind the journalists and outlets affected by the censorship can best be understood by watching the full transcript.

# Tags

#Censorship #SocialMedia #IndependentNews #AlternativePlatforms #FreeSpeech #BoStandUp