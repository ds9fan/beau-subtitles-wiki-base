# Bits

- Story of a history professor in Japan who predicted World War II and moved his family to a remote island called Iwo Jima to avoid the impending disaster.
- Illustrates the importance of not remaining silent in the face of tyranny or oppression.
- Remaining silent is seen as choosing the side of the oppressor and those in power.
- Calls for engaging in discussions about taboo topics like money, politics, and religion, instead of avoiding them.
- Urges people to start talking amongst themselves rather than letting politicians dictate the narrative.

# On-the-ground actions from transcript

- Contact local community groups to initiate discussions on taboo topics (suggested)
- Join relevant social platforms or community forums to engage in meaningful conversations (suggested)
- Give someone a platform to share their views on significant issues (implied)
- Go to local events or town hall meetings to participate in discussions on politics, money, and religion (suggested)

# Oneliner

A history professor's foresight before World War II on Iwo Jima serves as a powerful reminder to speak out against oppression and engage in discussions on critical but often avoided topics, challenging the culture of silence.

# Quotes

- "When you remain silent in the face of tyranny or oppression, you have chosen a side."
- "Tyranny anywhere is a threat to freedom everywhere."
- "Maybe it's time that we start talking about all the things we're not supposed to."
- "Your silence is only helping them."
- "They've got us trained in this country."

# What's missing

The emotional impact and storytelling nuances can be best experienced by watching the full video.

# Tags

#Silence #Tyranny #Oppression #Discussion #TabooTopics #Empowerment