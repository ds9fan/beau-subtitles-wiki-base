# Bits

- Bo Ginn is down on the Mexican border with his militia commander to "protect America."
- They won't stop refugees on the Mexican side as it violates U.S., Mexican, and international law.
- They won't send them back at gunpoint, respecting rights and due process.
- The militia is backing up DHS, despite being labeled as potential terrorists.
- Bo Ginn questions the wisdom of showing DHS their operations, fearing repercussions.
- The militia aims for good optics while being armed on the border ready to confront asylum seekers.

# On-the-ground actions from transcript

- Contact your local community outreach organizations to support refugees (suggested).
- Join your local advocacy groups to help protect immigrant rights (suggested).
- Report any suspicious militia activities to law enforcement (implied).

# Oneliner

Bo Ginn's militia walks a fine line on the border, respecting laws while asserting readiness to protect America with guns.

# Quotes

- "Of course, we're not actually going to stop any of the refugees on the Mexican side of the border."
- "We wouldn't do that unless we were traitors, so we're not going to do that."
- "Ready to kill a bunch of unarmed asylum seekers who are following the law."

# What's missing

The full conversation provides insight into the complexities of border security and militia involvement.

# Tags

#BorderSecurity #Militia #Refugees #ImmigrantRights #DueProcess #DHS