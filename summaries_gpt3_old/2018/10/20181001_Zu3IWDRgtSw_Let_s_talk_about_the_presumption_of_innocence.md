# Bits

- Beau discusses the presumption of innocence in relation to the Kavanaugh confirmation.
- Kavanaugh's support for law enforcement stop and search tactics, warrantless searches, and indefinite detention is brought up.
- Beau questions the sudden defense of Kavanaugh's presumption of innocence by those not familiar with his rulings.
- The impact of party politics on compromising rights and the potential consequences of executive power shifts are emphasized.
- The importance of understanding candidates' rulings and upholding the Constitution is stressed over blind party support.

# On-the-ground actions from transcript

- Contact your representatives to express concerns about nominees' stances and rulings (implied)
- Join local advocacy groups working to uphold civil liberties (implied)
- Give someone a rundown of Kavanaugh's controversial rulings and actions (implied)

# Oneliner

Beau delves into the presumption of innocence, Kavanaugh's controversial stances, and the dangers of blind party support compromising rights.

# Quotes

- "You've bought into bumper sticker politics and you're trading away your country for a red hat."
- "A man who spent his entire career trying to undermine it."
- "Knowing that somebody that doesn't believe in the Fourth Amendment is sitting on the Supreme Court."

# Whats missing

The emotional impact of compromising rights for party politics can be best understood by watching the full transcript.

# Tags

#PresumptionOfInnocence #Kavanaugh #PartyPolitics #CivilLiberties #Constitution