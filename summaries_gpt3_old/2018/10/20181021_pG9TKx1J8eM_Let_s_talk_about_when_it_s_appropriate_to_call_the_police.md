# Bits

- Every law, no matter how insignificant, is backed up by penalty of death, as exemplified by the scenario of jaywalking leading to potential police violence.
- Calling the police should only be done when death is an appropriate response to the situation at hand, as otherwise it puts lives in jeopardy needlessly.
- Strong fences make good neighbors; if nobody is being harmed, it's not really a crime, even if it may be illegal.
- Patience and calmness are required in dealing with situations like the one involving "Keyfob Katie," but the dynamics change drastically based on race, as exemplified by potential outcomes for a black person acting similarly.
- The potential consequences of a black person getting loud and confrontational with a white person versus the reverse show the stark disparity in perceived threats and police involvement.
- Avoid calling the police unless there is a real threat of death, as doing so can escalate situations needlessly and put individuals at risk.

# On-the-ground actions from transcript

- Contact your local community organizations for alternatives to calling the police (implied)
- Join your local neighborhood watch or community safety group for non-police intervention options (implied)
- Give someone a heads-up about the potential risks of calling the police in non-life-threatening situations (generated)

# Oneliner

The importance of only calling the police when death is an appropriate response, as every law carries the risk of violence, especially considering racial dynamics and potential escalation.

# Quotes

- "Every law is backed up by penalty of death."
- "If death is an appropriate response to what's happening, that's it. That's when you call the cops."
- "Don't call the law unless death is an appropriate response because it's a very real possibility every time you dial 9-11."

# What's missing

The full context and examples illustrating the disparity in police response based on race could be best understood by watching the entire conversation.

# Tags

#CallThePolice #RacialDynamics #CommunitySafety #PoliceResponse #LawEnforcement