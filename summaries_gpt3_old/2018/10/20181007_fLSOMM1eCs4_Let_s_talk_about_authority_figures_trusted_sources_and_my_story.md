# Bits

- People often want to know the backstory of individuals to determine how much trust to place in them (illustrated)
- Blind obedience to authority figures can be dangerous and can lead to harmful consequences (explained)
- It's vital to check facts, run them through filters, and think for yourself rather than blindly trusting authority (stressed)
- A reference to the Milgram experiments, where many people obeyed authority figures to the extent of causing harm (referenced)
- Encouragement to trust your own judgment over blindly following authority (advised)

# On-the-ground actions from transcript

- Check your sources before believing information (stressed)
- Run facts through filters like logic, morality, and ethics (advised)
- Trust your own judgment instead of blindly following authority figures (illustrated)
- Encourage critical thinking in others by promoting fact-checking and independent thought (explained)

# Oneliner

Questioning blind obedience to authority and promoting critical thinking to avoid harmful consequences and encourage individual judgment.

# Quotes

- "Trust your facts, and trust yourself."
- "Don't trust your sources, trust your facts, and trust yourself."
- "Trust yourself. Trust yourself."
- "Ideas stand or fall on their own."
- "I don't want you to think like me. I want you to think for yourself."

# Whats missing

The full video provides a deeper exploration of the dangers of blind obedience to authority and the importance of independent critical thinking in avoiding harmful consequences.

# Tags

#Authority #Trust #CriticalThinking #FactChecking #IndividualJudgment