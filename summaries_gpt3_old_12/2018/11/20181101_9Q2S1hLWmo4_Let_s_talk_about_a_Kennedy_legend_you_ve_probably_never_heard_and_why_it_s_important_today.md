# Bits

Beau says:

- Tells a legend about President Kennedy meeting with selected Green Berets in 1961, making them take an oath to fight back against a tyrannical government.
- Mentions that evidence supporting this theory includes Kennedy's visit to Fort Bragg in October 1961 and the subsequent departure of 46 Green Berets to DC after Kennedy's assassination.
- Notes the peculiar presence of Green Berets as Kennedy's honor guard, a unique situation for a president.
- Speculates on the secretive actions of the 25 Green Berets not part of Kennedy's honor guard.
- Talks about Command Sergeant Major Ruddy supposedly placing his green beret on Kennedy's casket, with the beret now displayed at the JFK Presidential Library.
- Mentions a supposed investigation into a group called the Special Forces Underground, debunking claims of racism within the organization.
- Points out the annual visit by active-duty Green Berets to Kennedy's grave site for a ceremony, a unique tribute not given to other presidents.
- Connects Kennedy's criteria for fighting tyranny, including the suspension of habeas corpus, to current considerations by the president.
- Raises concerns about the suspension of habeas corpus being a threat to national security and the rule of law.
- Urges viewers to understand the significance of habeas corpus and its potential impact on the country's democratic values.

# Oneliner

President Kennedy's legend of Green Berets taking an oath to fight tyranny resurfaces, raising concerns about the suspension of habeas corpus today.

# Audience

Citizens, activists, history enthusiasts

# On-the-ground actions from transcript

- Contact local representatives to express opposition to the suspension of habeas corpus (implied)
- Educate others about the importance of habeas corpus and its role in upholding democratic values (implied)

# Quotes

- "The suspension of habeas corpus is though, that is a threat to national security. It's a threat to your very way of life."
- "The suspension of habeas corpus is something that can't be tolerated."
- "The rule of law will be erased completely."

# Whats missing in summary

Deeper insights into the potential implications of suspending habeas corpus and the need for civic engagement to protect democratic principles.

# Tags

#PresidentKennedy #GreenBerets #SuspensionOfHabeasCorpus #Tyranny #NationalSecurity