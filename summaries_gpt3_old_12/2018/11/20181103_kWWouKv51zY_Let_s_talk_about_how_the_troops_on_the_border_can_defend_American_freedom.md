# Bits

Beau says:
- Recounts a story from the Tet Offensive in 1968 where a general orders a lieutenant to take a hill crawling with enemy forces, and the lieutenant accomplishes it without casualties.
- Points out the chain of command in military operations: vague orders are clarified by those on the ground who must implement them.
- Stresses the unique chance for soldiers to defend American freedom by joining an exclusive club of those who truly defended it throughout history.
- Mentions the concept of Mission Creep using Afghanistan as an example, warning against vague orders leading to unforeseen consequences.
- Condemns the Department of Homeland Security's request for soldiers to act as emergency law enforcement personnel, which is illegal.
- Warns soldiers about the potential consequences of following vague orders and mission creep, urging them to stay within the law and rules of engagement.
- Advises soldiers to keep their weapons safe and slung, not getting involved in questionable actions that could lead to legal repercussions.
- Encourages soldiers involved in border activities to learn about legal asylum processes and clarifies the legality of migrants seeking asylum in the United States.
- Criticizes the political manipulation of soldiers and their uniforms for ulterior motives, cautioning against blindly following orders that may be unlawful.
- Urges soldiers to seek clarification on vague orders, maintain a paper trail for defense, and prioritize following the law over potential mission creep.

# Oneliner

A cautionary tale on vague orders, mission creep, and the importance of upholding the law for soldiers at the border.

# Audience

Soldiers at the border.

# On-the-ground actions from transcript
- Seek clarification on vague orders (suggested).
- Maintain a paper trail for defense with emails requesting clarification (implied).

# Quotes
- "You want to defend American freedom, you want to defend the freedoms of Americans, you keep that weapon slung and you keep it on safe."
- "If you allow that mission creep to happen, you're going to need it. You're going to need a defense."
- "It's your uniform, your honor being used to legitimize them circumventing the law."

# Whats missing in summary

The full transcript provides a detailed narrative on the risks of following vague orders, mission creep, and the importance of upholding the law, especially for soldiers at the border.

# Tags

#Soldiers #Border #VagueOrders #MissionCreep #UpholdTheLaw #AmericanFreedom