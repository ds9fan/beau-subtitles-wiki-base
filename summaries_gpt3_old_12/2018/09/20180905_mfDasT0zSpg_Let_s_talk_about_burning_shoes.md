# Bits

Beau says:

- Beau wants to burn something inside because it's raining and he feels it's really important to burn it today.
- He mentions not owning Nikes due to his dislike of the company, but his son has a pair which he plans to burn.
- Beau sarcastically talks about the importance of burning the shoes for the Skycloth to create liberty next time the magic song plays.
- He criticizes the idea of burning shoes as emotional and entitled behavior.
- Beau suggests donating the shoes to a shelter or giving them to a homeless vet instead of burning them.
- Questions are raised about whether burning the shoes is meant to insult the workers who made them or if it's directed towards the company.
- Beau draws a parallel between burning shoes and burning the American flag as symbolic acts.
- He questions those who are willing to disassociate with Nike over a commercial but ignore the issues of sweatshops and slave labor.
- Beau implies that some people prioritize symbols of freedom over actual freedom.
- He concludes by wishing everyone a good day.

# Oneliner

Beau wants to burn shoes in the rain for a symbolic act but questions the true meaning behind such actions, urging people to think deeper about their choices and values.

# Audience

Consumers, activists

# On-the-ground actions from transcript

- Donate shoes to a shelter or give them to a homeless vet (suggested)
- Put shoes in a thrift store near a military base for enlisted individuals in need (suggested)

# Quotes

"Y'all have a good day."

# Whats missing in summary

The full transcript provides a deeper reflection on consumer choices, symbolism, and true understanding of freedom.

# Tags

#ConsumerChoices #Symbolism #Freedom #Activism #EthicalConsumption