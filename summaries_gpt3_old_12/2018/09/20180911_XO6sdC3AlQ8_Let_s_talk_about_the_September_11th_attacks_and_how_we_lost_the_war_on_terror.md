# Bits

Beau says:

- Recalls his experience on September 11th, watching the attacks unfold on TV.
- Notes the casualty of that day that often goes unnamed - the erosion of freedom.
- Expresses how freedoms have been chipped away since 9/11, with torture and surveillance becoming normalized.
- Emphasizes that terrorism aims to provoke overreactions to incite rebellion against the government.
- Warns against the normalization of excessive government control and surveillance.
- Urges for actions at the community level to safeguard freedom.
- Suggests educating children about the importance of freedom and resisting normalization of oppressive measures.
- Advocates for self-reliance in preparing for emergencies, reducing dependence on the government.
- Recommends counter-economics to reduce government control over financial transactions.
- Encourages building a supportive community focused on preserving freedom and helping those in need.
- Proposes being a force multiplier by spreading awareness, discussing lost freedoms, and strengthening communities to resist government overreach.

# Oneliner

9/11 survivor Beau warns against the erosion of freedom post-attack, advocating for community action to preserve liberty and resist government overreach.

# Audience

Community members

# On-the-ground actions from transcript

- Educate children about the importance of freedom and resisting normalization (suggested)
- Prepare for emergencies and be self-reliant in times of crisis (exemplified)
- Engage in counter-economics to reduce government control (implied)
- Build a supportive community to preserve freedom and aid those in need (exemplified)

# Quotes

- "We can't let this become normal."
- "You're going to defeat an overreaching government by ignoring it."
- "The face of tyranny is always mild at first."
- "If your community is strong enough, what happens in DC doesn't matter because you can ignore it."
- "Y'all the ones been keeping it alive."

# Whats missing in summary

The full transcript provides a comprehensive insight into the erosion of freedoms post-9/11 and the importance of community action in preserving liberty and resisting government overreach.

# Tags

#September11 #Freedom #CommunityAction #Terrorism #GovernmentOverreach