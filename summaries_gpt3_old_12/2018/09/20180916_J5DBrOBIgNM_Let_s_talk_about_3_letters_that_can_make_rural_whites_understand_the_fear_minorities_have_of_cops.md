# Bits

Beau says:

- Explains the distrust that minorities have for law enforcement compared to white country folk.
- Illustrates how in rural areas, law enforcement is more accountable and part of a tighter community.
- Mentions the ability to hold law enforcement accountable through the ballot box or the cartridge box.
- Points out that minority groups lack institutional power to hold unaccountable men with guns in law enforcement accountable.
- Draws parallels between the distrust towards law enforcement and federal agencies like ATF and BLM.
- Suggests that those worried about ATF and BLM should take action by getting involved in local police departments.
- Urges people to care enough to make a difference by removing corruption at the local level.
- Emphasizes the need for different communities to come together to address the common problem of unaccountable men with guns in law enforcement.

# Oneliner

Explaining distrust towards law enforcement, Beau urges communities to work together to hold unaccountable men with guns accountable, starting at the local level.

# Audience

Community members

# On-the-ground actions from transcript

- Get involved in local police departments to prevent corruption (suggested)
- Work together with different communities to address the issue of unaccountable law enforcement (implied)

# Quotes

- "They're a minority. So they can't swing an election."
- "We can hold them accountable one way or the other, the ballot box or the cartridge box."
- "It's unaccountable men with guns. We can work together and we can solve that."

# Whats missing in summary

The full transcript provides a detailed explanation of why minorities distrust law enforcement and suggests actionable steps to hold unaccountable men with guns accountable at the local level.

# Tags

#LawEnforcement #CommunityPolicing #Accountability #MinorityDistrust #LocalAction