# Bits

Beau says:
- Analyzes the shooting in Dallas from the perspective of a civilian, not a cop, but someone with experience dealing with liars.
- Points out the inconsistencies in the officer's story about entering the wrong apartment and shooting the man inside.
- Mentions the officer's previous noise complaints against the victim as a possible motive for the shooting.
- Raises concerns about the officer's actions constituting a home invasion and facing capital murder charges in Texas.
- Recalls a past interaction where the importance of Black Lives Matter was questioned and relates it to the lack of justice in Dallas.
- Criticizes the police for searching the victim's home post-shooting, suggesting corruption and lack of accountability within the department.
- Condemns the lack of intervention from other officers in the Dallas Police Department, equating their silence to complicity in wrongdoing.
- Expresses disappointment in law enforcement for not upholding justice and tarnishing their reputation by defending unjust actions.

# Oneliner
Analyzing the Dallas shooting, Beau questions the officer's motives, criticizes police corruption, and challenges the lack of justice, questioning if Black Lives truly matter in this context.

# Audience
Community members, activists, allies

# On-the-ground actions from transcript
- Challenge police corruption by demanding accountability within law enforcement organizations (implied)
- Support justice for victims of police violence by engaging in advocacy efforts and seeking transparency in investigations (implied)
- Amplify voices calling for police reform and systemic change to address issues of injustice and inequality (implied)

# Quotes
- "If this is the amount of justice they get, they don't. They don't."
- "Not one cop has crossed the thin blue line to say that's wrong. Not one."
- "It's not a few bad apples spoils the bunch, and that certainly appears to have happened in Dallas."

# Whats missing in summary
Full context and emotional depth from Beau's analysis of the Dallas shooting, along with a call for accountability and justice in the face of police corruption.

# Tags
#DallasShooting #PoliceCorruption #BlackLivesMatter #Justice #Accountability