# Bits

Beau says:
- Gun control has failed due to being written by those unfamiliar with firearms, like the ineffective assault weapons ban.
- The term "assault weapon" is made up and lacks a clear definition, leading to ineffective regulations.
- Banning assault weapons based on arbitrary criteria like collapsible stocks and pistol grips doesn't impact lethality.
- High-capacity magazine bans are flawed; changing magazines in firearms is quick, making rushing the shooter ineffective.
- Banning specific firearms like the AR-15 may lead to the use of more powerful weapons like the Garand or M14.
- Banning semi-automatic rifles entirely is impractical due to their prevalence and ease of production.
- Comparing the civilian AR-15 to the military M16 shows the limitations of bans in preventing access to certain firearms.
- Bans on firearms create black markets, making weapons more accessible to those who shouldn't have them.
- Raising the age limit for purchasing firearms to 21, suggested after the Parkland shooting, is a simple and effective measure.
- Addressing loopholes in laws regarding domestic violence offenders' access to firearms is vital.

# Oneliner

Beau argues against ineffective gun control measures and proposes raising the age limit for firearm purchases as a simple yet effective solution.

# Audience

Legislators, policymakers, gun control advocates

# On-the-ground actions from transcript

- Raise the age limit for purchasing firearms from 18 to 21 (suggested)
- Close loopholes in laws regarding domestic violence offenders' access to firearms (suggested)

# Quotes

- "Bans on firearms create black markets, making weapons more accessible to those who shouldn't have them."
- "Raising the age limit for purchasing firearms to 21 is a simple and effective measure."

# Whats missing in summary

The full transcript provides detailed explanations on the flaws of existing gun control measures and proposes practical solutions for reducing gun violence.

# Tags

#GunControl #AssaultWeapons #HighCapacityMagazines #AgeLimit #DomesticViolence #BlackMarket