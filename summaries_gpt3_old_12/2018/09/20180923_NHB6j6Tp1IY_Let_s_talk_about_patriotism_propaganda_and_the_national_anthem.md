# Bits

Beau says:

- Responding to an op-ed story sent by two people who knew a woman attacked for kneeling during the national anthem.
- Expresses irritation and disbelief at the author's perspective on the woman's actions.
- Questions the author's understanding of patriotism and purpose of protest.
- Criticizes the author's focus on flag etiquette rather than the essence of patriotism.
- Challenges the author's role as a spokesperson for law enforcement and their lack of understanding of the protest's meaning.
- Emphasizes that patriotism involves correcting the government when necessary.
- Recounts a story about patriotism being displayed through actions, not symbols.
- Encourages the author to learn from someone with extensive military experience before giving lectures on patriotism.
- Concludes by distinguishing between nationalism and patriotism and asserting his own stance on the matter.

# Oneliner

Beau challenges misconceptions about patriotism and criticizes an author's perspective on kneeling during the national anthem.

# Audience

Patriotic individuals

# On-the-ground actions from transcript

- Contact Beau for connection to the military veteran mentioned in the transcript (suggested)

# Quotes

"Sir, patriotism is not doing what the government tells you to, Mr. Lecturer on patriotism."

# Whats missing in summary

The full transcript provides a detailed analysis of patriotism, protest, and the significance of actions over symbols.

# Tags

#Patriotism #Protest #NationalAnthem #Misconceptions #LawEnforcement