| Input     | Output |
| --------- | ------ |
| Link      | {{Url}} |
| Published | {{Date}} |
| Theme     |  |
| Status    | article incomplete |

## Short Summary
{{Summary}}
## Transcript
{{Transcript}}
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}
