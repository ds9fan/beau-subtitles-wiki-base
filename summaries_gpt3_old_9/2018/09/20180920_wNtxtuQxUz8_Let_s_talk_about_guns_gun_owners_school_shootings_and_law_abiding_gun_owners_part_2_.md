# Bits

Beau says:
- Contrasts failed gun control measures written by those ignorant of firearms with proposed solutions.
- Discredits the idea of banning assault weapons due to the arbitrary nature of the term.
- Explains the inefficacy of banning high-capacity magazines by demonstrating how quickly they can be changed.
- Warns against banning the AR-15, as it may lead to more powerful weapons like the Garand being used.
- Argues that banning semi-automatic rifles may be ineffective due to their widespread availability and ease of production.
- Points out that banning items creates a black market, making them more accessible to undesirables.
- Suggests raising the minimum age for purchasing firearms from 18 to 21 as a potential solution.
- Addresses loopholes in laws regarding domestic violence and gun ownership.

# Oneliner

Proposed solutions to gun control issues are simple yet effective, contrasting failed measures due to lack of firearm knowledge by proposing raising the minimum age for firearm purchase and addressing domestic violence loopholes.

# Audience

Legislators, Gun Control Advocates

# On-the-ground actions from transcript

- Raise the minimum age for firearm purchase from 18 to 21 (exemplified).
- Close loopholes in laws regarding domestic violence and gun ownership (exemplified).

# Quotes

- "If you ban them, they get cheaper, more accessible to the people you don't want to have them."
- "Legislation is not the answer here in any way shape or form."
- "Raising the age from 18 to 21, that's a great idea, that's brilliant."
- "It's going to make people more violent when they have one."
- "Legislation is not the answer here in any way shape or form."

# Whats missing in summary

The full transcript provides detailed insights into the failures of previous gun control measures and suggests practical solutions grounded in firearm knowledge and societal understanding.

# Tags

#GunControl #Firearms #Legislation #DomesticViolence #AgeRestrictions