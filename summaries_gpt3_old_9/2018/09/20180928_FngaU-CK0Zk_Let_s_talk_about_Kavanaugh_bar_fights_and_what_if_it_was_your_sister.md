# Bits

Beau says:

- Recalls a story from his college days at a bar named Eight Seconds where he intervened to help a girl who was being bothered by a guy.
- Describes how he pretended to be the girl's brother to deter the guy, who eventually walked away.
- Talks about how the guy respected Beau's property rights over the girl's autonomy as a person.
- Draws parallels between his story and the current situation, specifically referencing the Kavanaugh hearing and comments dismissing women's assault claims.
- Encourages viewers to imagine if the victims were their friends and presents a calculation showing the potential number of sexually assaulted women among their acquaintances.
- Points out that victim-blaming comments on assault cases discourage survivors from coming forward.
- Mentions the delayed reporting of sexual assault by men in the Catholic priest case, challenging stereotypes about male emotions.
- Argues that coming forward about assault is motivated by preventing perpetrators from further power and control.
- Suggests that victims gain courage to speak up when others share their stories.
- Concludes by urging viewers to reconsider their stance on assault cases and the impact of their comments on real-life relationships.

# Oneliner

Beau uses a personal anecdote to shed light on victim-blaming attitudes in assault cases and calls for empathy and reflection on one's influence in encouraging survivors to speak out.

# Audience

Activists, Allies, Advocates

# On-the-ground actions from transcript

- Reach out to survivors in your community and offer support (implied)
- Challenge victim-blaming comments and beliefs in your social circles (implied)
- Educate yourself on sexual assault statistics and survivor experiences (implied)

# Quotes

- "It's about power and control."
- "You're altering real-life relationships over bumper sticker politics."
- "Do the math and then think about it."

# Whats missing in summary

The full transcript provides a nuanced perspective on victim-blaming, the impact of comments on survivors, and the importance of empathy and understanding in assault cases.

# Tags

#SexualAssault #VictimBlaming #Empathy #CommunitySupport #GenderJustice