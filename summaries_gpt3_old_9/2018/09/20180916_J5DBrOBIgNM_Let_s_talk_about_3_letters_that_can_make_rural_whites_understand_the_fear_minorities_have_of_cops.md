# Bits

Beau says:

- Explains the distrust minorities have for law enforcement, contrasting it with the accountability of sheriffs in rural communities.
- Mentions the close-knit community relationships in rural areas where law enforcement is more accountable.
- Talks about the ability to hold law enforcement accountable through elections or other means in rural areas.
- Describes how minority groups lack institutional power and are unable to hold unaccountable police officers responsible.
- Draws parallels between minorities' distrust of law enforcement and white country folks' distrust of entities like ATF and BLM.
- Points out the frequency of unjust killings by unaccountable men with guns.
- Stresses the need for white communities to care enough to get involved in holding law enforcement agencies accountable.
- Suggests that getting rid of corruption at the local police level can prevent future issues at federal agencies.
- Encourages people to see their common problems and work together to address unaccountable men with guns.
- Urges people to take action and work together to solve the issue of police accountability.

# Oneliner

Understanding police distrust: Beau explains how accountability in rural communities contrasts with minority groups' lack of power, urging all to work together for police accountability.

# Audience

Community members, activists.

# On-the-ground actions from transcript

- Contact your local police department to inquire about their accountability measures and policies (suggested).
- Organize community meetings to raise awareness about police accountability and potential actions to take (implied).
- Join or support organizations working towards police reform and accountability (suggested).

# Quotes

- "We've got to start talking to each other. We got the same problems."
- "It's unaccountable men with guns. We can work together and we can solve that."
- "Get involved, we got to start working together."

# Whats missing in summary

The full transcript provides a detailed explanation of the differences in police accountability between rural communities and minority groups, urging white communities to care enough to get involved in addressing police accountability issues.

# Tags

#PoliceAccountability #CommunityAction #Accountability #MinorityDistrust #SocialJustice