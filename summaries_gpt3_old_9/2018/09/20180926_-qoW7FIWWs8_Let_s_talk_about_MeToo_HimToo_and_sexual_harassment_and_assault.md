# Bits

Beau says:

- Addresses sexual harassment and assault in the context of the Kavanaugh nomination.
- Offers advice on preventing such situations, suggesting safeguarding reputation and ending dates at the front door.
- Advises against taking someone home while drinking and advocates for staying with the party in such situations.
- Warns to be mindful of words spoken, as they could be misinterpreted sexually.
- Stresses the importance of being cautious with signals sent and keeping hands to oneself.
- Directs advice towards men, referencing the "him-too" hashtag and men's fear of false accusations.
- Asserts that false accusations of sexual harassment or assault are rare, around 2%.
- Calls for accountability from both men and women in avoiding situations that could lead to false accusations.
- Contrasts societal attitudes towards women's actions leading to assault with the accountability expected from men.
- Argues against victim-blaming based on clothing choices and states that rapists are the sole cause of rape.

# Oneliner

Beau's advice on avoiding sexual harassment and assault: safeguard reputation, end dates at the front door, stay with the party while drinking, watch words and signals, and keep hands to oneself.

# Audience

Men

# On-the-ground actions from transcript

- Hold accountable for actions (implied)
- Avoid situations that could lead to false accusations (implied)

# Quotes

- "The miniskirt said, am I really to blame? And the hijab said, no, it happened to me too. And the diaper didn't say anything."
- "Gentlemen there is one cause of rape, and that's rapists."

# Whats missing in summary

Full context and nuances of Beau's perspective on addressing sexual harassment and assault.

# Tags

#SexualHarassment #AssaultPrevention #Accountability #RapeCulture #MenResponsibility