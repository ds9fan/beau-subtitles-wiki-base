# Bits

Beau says:

- Responding to an op-ed story about a woman attacked for kneeling during the national anthem.
- Expresses irritation and decides to respond after reading the author's bio.
- Questions the purpose of a protest and lectures on patriotism and the Constitution.
- Calls out the author for focusing on flag etiquette rather than the true meaning of patriotism.
- Questions the competence or motives of a spokesman for law enforcement who criticizes the protester.
- Defines patriotism as correcting the government when it is wrong, not blindly following orders.
- Recounts a story about patriotism being displayed through actions, not symbols.
- Suggests speaking to a veteran to understand true patriotism.
- Differentiates between nationalism and patriotism.
- Ends with a strong statement asserting his own patriotism.

# Oneliner

Beau challenges a critic's patriotism, defends a protester's rights, and urges a deeper understanding of true patriotism.

# Audience

American citizens

# On-the-ground actions from transcript

- Contact Beau to connect with the veteran mentioned for a deeper understanding of patriotism (suggested)

# Quotes

- "Patriotism is not doing what the government tells you to."
- "Patriotism is correcting your government when it is wrong."
- "Patriotism was displayed through actions, not worship of symbols."
- "You can kiss my patriotic behind."

# Whats missing in summary

The full video provides a detailed exploration of the distinction between nationalism and patriotism, using real-life examples and personal anecdotes.

# Tags

#Patriotism #Protest #FreedomOfSpeech #CommunityPolicing #Constitution