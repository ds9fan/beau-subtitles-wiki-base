# Bits

Beau says:

- Inside due to rain, needs to burn something today.
- Doesn't like Nike for various reasons but son has Nike shoes.
- Talks about burning son's Nike shoes but clarifies it's a metaphor.
- Urges not to burn the shoes but donate them to those in need.
- Questions the intention behind burning the shoes - insulting workers or company executives.
- Draws parallels between burning Nike shoes and burning the American flag.
- Points out hypocrisy of being mad at Nike commercials but not at their use of sweatshops.
- Concludes by criticizing those who prioritize symbols over true freedom.

# Oneliner

Don't burn Nike shoes - donate them instead; prioritize helping those in need over symbolic gestures. 

# Audience

Consumers, social justice advocates

# On-the-ground actions from transcript

- Donate unwanted items to shelters or thrift stores near military bases (implied)

# Quotes

- "You're loving that symbol of freedom more than you love freedom."
- "If you're mad about this and you're willing to disassociate with Nike because of their commercial with the guy who milled during the magic song, but you don't have a problem with sweatshops, slave labor that they've used all those years, it's what built that company."

# Whats missing in summary

Full video provides deeper analysis on consumer activism and the importance of actions over symbols.

# Tags

#ConsumerActivism #SymbolicGestures #Nike #SocialJustice #Donations