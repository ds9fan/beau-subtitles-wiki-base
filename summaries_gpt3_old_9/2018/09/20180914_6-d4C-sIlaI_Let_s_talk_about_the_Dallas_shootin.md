# Bits

Beau says:

- Analyzes a shooting in Dallas involving a cop entering the wrong apartment and killing the resident.
- Mentions two types of liars: bad liars who need to be asked multiple times to tell the truth, and good liars who mix in some truth with lies.
- Suggests that the cop may have had a motive due to previous noise complaints against the victim.
- Points out that the cop's actions could be seen as a home invasion, regardless of her profession.
- Raises the potential for a capital murder charge in Texas due to killing someone during a home invasion.
- Recalls a past interaction regarding Black Lives Matter, questioning if black lives truly matter based on the justice they receive.
- Condemns the police searching the victim's home post the shooting to justify the killing.
- Expresses disappointment in the lack of accountability within the Dallas PD for the actions of their officers.
- Argues that defending unjust actions and corruption tarnishes the entire law enforcement community's reputation.
- Concludes by discussing the loss of public support for law enforcement due to actions like those in Dallas.

# Oneliner

Analyzing a police shooting in Dallas, Beau questions motives, calls out injustice, and challenges the lack of accountability in law enforcement.

# Audience

Law enforcement accountability advocates

# On-the-ground actions from transcript

- Contact organizations working for police accountability and justice (implied)
- Advocate for transparency and accountability within law enforcement (implied)
- Support movements like Black Lives Matter advocating for justice and equality (implied)

# Quotes

- "That certainly appears to have happened in Dallas because if you're wearing one of those uniforms and you let this slide, you're just as crooked as they are."
- "That is why people don't back the blue anymore."
- "You're trying to slander and villainize an unarmed man that was gunned down in his home."

# Whats missing in summary

The emotional depth and intensity of Beau's analysis, especially his disappointment and frustration with the lack of accountability within law enforcement, can be best understood by watching the full transcript.

# Tags

#PoliceShooting #Accountability #Justice #BlackLivesMatter #Corruption