# Bits

Beau says:

- Recalls his experience on September 11th, watching the World Trade Center attacks unfold on TV.
- Mentions the casualty of that day that never gets recognized - the erosion of freedom.
- Talks about the loss of rights and freedoms post-9/11 due to laws passed.
- Points out the normalizing of extreme measures like torture and surveillance.
- Describes terrorism as strategic, aiming to provoke overreactions from governments.
- Notes how the government capitalized on fear post-9/11 to curb freedoms.
- Expresses concern about the public's acceptance of increasing government control.
- Urges viewers not to rely on politicians to restore lost freedoms.
- Suggests instilling the value of freedom in children by teaching them about government overreach.
- Advocates for self-reliance in preparing for disasters to reduce dependence on the government.
- Proposes counter-economics strategies like bartering and cryptocurrency to minimize government control.
- Encourages surrounding oneself with like-minded individuals to support and strengthen the fight for freedom.
- Advocates for helping marginalized individuals who understand the loss of freedom.
- Mentions the effectiveness of US Special Forces in training local populations in other countries.
- Calls for community-building to resist government overreach and maintain autonomy.

# Oneliner

Post-9/11 erosion of freedoms requires grassroots efforts to instill values of freedom, self-reliance, and community cohesion to combat government overreach.

# Audience

Community members

# On-the-ground actions from transcript

- Teach children about government overreach and the importance of freedom quietly (suggested)
- Prepare for disasters and be self-reliant to reduce dependence on the government (implied)
- Practice counter-economics by bartering, using cryptocurrency, or supporting local businesses (suggested)
- Surround yourself with like-minded individuals to strengthen the fight for freedom (suggested)
- Support and help marginalized individuals who understand the loss of freedom (suggested)
- Build community resilience to resist government overreach and maintain autonomy (implied)

# Quotes

- "We can't let this become normal."
- "We've got to do this from the bottom up."
- "Surround yourself with other people who are in it."
- "Build your community."
- "Defeat a government that is very overreaching by ignoring it."

# Whats missing in summary

The full transcript provides detailed insights into the erosion of freedoms post-9/11, the strategic nature of terrorism, and the importance of grassroots efforts in combating government overreach.

# Tags

#Freedom #GovernmentOverreach #CommunityResilience #GrassrootsEfforts #SelfReliance