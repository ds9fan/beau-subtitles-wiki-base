# Bits

Beau says:

- President of the United States mocked sexual assault survivors on television.
- Not shocking because the bar was set low.
- Trump's actions contribute to silencing sexual assault survivors.
- Reference to Trump's "Grab-Em-By-The-Birds-Of-A-Feather" type of behavior.
- Examination of the word "let" in Trump's defense.
- Comparison between Obama and Trump's characters and actions.
- Doubts about Trump's integrity and honor compared to Obama.
- Mention of Trump casting doubt on women coming forth with claims.
- Addressing the presumption of innocence versus job interviews.
- Mention of Kavanaugh and the dangerous thought of portraying predators as monsters.
- Warning against dehumanizing predators, drawing a parallel to Nazis.
- The danger of remaining silent or passive in the face of evil.

# Oneliner

President Trump's actions on sexual assault survivors set a dangerous precedent, while Beau warns against dehumanizing predators and the perils of staying silent in the face of evil.

# Audience

Advocates for survivors

# On-the-ground actions from transcript

- Support survivors who come forth (implied)
- Speak up against dehumanizing rhetoric towards predators (implied)
- Advocate for active bystander intervention training in communities (implied)

# Quotes

- "The scariest Nazi wasn't a monster. He was your neighbor."
- "The scariest rapist or rape-apologist, well they're your neighbor too."

# Whats missing in summary

The full transcript provides a deeper exploration of the dangers of dehumanizing individuals and remaining silent in the face of injustice.

# Tags

#SexualAssault #Dehumanization #Silence #Survivors #BystanderIntervention