# Bits

Beau says:

- Addresses comments on presumption of innocence after discussing Kavanaugh and sexual assault.
- Points out Kavanaugh's support for law enforcement's ability to stop individuals on the street and search them.
- Mentions Kavanaugh's support for NSA collecting metadata without a warrant and involvement in detaining innocent people.
- Questions Kavanaugh's credibility regarding his knowledge of detaining people without a lawyer.
- Stresses the importance of the presumption of innocence in the American justice system.
- Criticizes individuals supporting Kavanaugh despite his history of undermining rights.
- Condemns party politics and the impact on rights and executive power.
- Warns about the potential consequences of granting executive power to a future Democrat.
- Urges critical thinking about supporting Kavanaugh based on party politics.
- Calls out blindly supporting candidates without understanding their rulings or the Constitution.

# Oneliner

Beau criticizes blind support for Kavanaugh based on party politics, warning about the erosion of rights and future consequences under a potential Democrat president.

# Audience

Activists, Voters, Advocates

# On-the-ground actions from transcript

- Challenge blind political allegiance (implied)
- Advocate for safeguarding constitutional rights (implied)
- Stay informed about candidates' rulings and stances on key issues (implied)

# Quotes

- "You've bought into bumper sticker politics and you're trading away your country for a red hat."
- "You can sit there and you can wave your flag and you can talk about making America great again."

# Whats missing in summary

The full transcript provides a deeper understanding of the implications of blind party politics on rights and the justice system.

# Tags

#PresumptionOfInnocence #Kavanaugh #PartyPolitics #ConstitutionalRights #Justice