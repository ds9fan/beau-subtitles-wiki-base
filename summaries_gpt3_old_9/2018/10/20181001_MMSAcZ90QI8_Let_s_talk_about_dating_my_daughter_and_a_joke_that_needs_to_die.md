# Bits

Beau says:

- Addresses the trend of fathers posing with firearms next to their daughter's dates.
- Questions the message being sent by this action.
- Challenges the idea that a firearm is needed to protect one's daughter.
- Raises concerns about trust and communication with daughters.
- Suggests that using guns in this context is more about the father's image than actual protection.
- Advocates for empowering daughters and fostering trust and communication.
- Shares a personal experience of dealing with a teenage boy without using a firearm.
- Argues against using guns as props or intimidation tools.
- Encourages fathers to prioritize communication and empowerment over intimidation.
- Urges fathers to reconsider the message they are sending to their daughters and their daughters' partners.

# Oneliner

Fathers holding guns in daughter's date photos send harmful messages; prioritize trust, communication, and empowerment instead.

# Audience

Parents

# On-the-ground actions from transcript

- Have open and honest communication with your children about trust, empowerment, and safety (implied).
- Prioritize building a trusting relationship with your children where they can come to you with anything (implied).
- Avoid using guns as props or intimidation tools in interactions with your children or their partners (implied).

# Quotes

- "It's time to let this joke die, guys. It's not a good look. It's not a good joke."
- "She doesn't need a man to protect her. She's got this."
- "You probably want her to have all the information she needs."

# Whats missing in summary

The full transcript provides detailed insights on father-daughter relationships, trust, communication, and empowerment that cannot be fully captured in a brief summary.

# Tags

#Parenting #Communication #Trust #Empowerment #Firearms