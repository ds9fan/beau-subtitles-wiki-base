# Bits

Beau says:

- Addresses the confusion around when it's appropriate to call the police.
- Points out that every law, no matter how insignificant, is backed up by the penalty of death.
- Gives the example of jaywalking as a law that should not exist.
- Describes the escalation from receiving a ticket for crossing the street to potentially being thrown in a cage.
- Explains that even without violence, refusing to comply can result in death.
- Mentions how calling the police on minor issues like Barbecue Becky or Permit Patty can endanger lives.
- States that if death is not an appropriate response, then one should not involve the police.
- Advocates for minding one's business unless someone is being harmed.
- Mentions the saying "strong fences make good neighbors" and applies it to the concept of non-harmful actions not being crimes.
- Talks about the racial dynamics involved in interactions with authorities and the potential danger for Black individuals.

# Oneliner

Call the cops only if death is warranted; minor laws can result in lethal consequences, so mind your business unless harm is present.

# Audience

Community members

# On-the-ground actions from transcript

- Respect your neighbors' privacy and autonomy (implied)
- Advocate for non-violent conflict resolution strategies (implied)

# Quotes

- "Every law is backed up by penalty of death."
- "If death is an appropriate response to what's happening, that's it."
- "Mind your business."
- "Nobody's being harmed by these actions. Nobody's being hurt."
- "Don't call the law unless death is an appropriate response."

# Whats missing in summary

The nuances and depth of understanding around the potential lethal consequences of involving law enforcement in minor situations.

# Tags

#Police #CommunityPolicing #RacialJustice #NonViolence #Justice